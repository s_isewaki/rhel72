<?php
//************************************************************************
// 勤務シフト作成　勤務シフト作成画面
// 自動シフト作成(分散)ＣＬＡＳＳ
//************************************************************************
require_once("about_postgres.php");
require_once("about_error.php");
require_once("duty_shift_auto_need_class.php");
require_once("duty_shift_auto_pattern_class.php");
require_once("duty_shift_auto_hindo_class.php");

// 月毎公休数を得るため timecard_legal_hol_cnt.phpより流用
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");

define("PATTERN_IS_VACATION", 10);				// パターン、休暇
define("REASON_IS_LEGAL", 22);					// 事由、法定休暇
define("REASON_IS_STATUTORY", 23);				// 事由、所定休暇
define("REASON_IS_PUBLIC", 24);					// 事由、公休
define("REASON_IS_PUBLIC_HALF", 44);			// 事由、公休半休
define("REASON_IS_NONE", 34);					// 事由、事由なし

define("SELECT_ALL", "1");						// 処理選択		全て
define("SELECT_INDIVIDUAL", "2");				// 処理選択		勤務シフト希望
define("SELECT_KOBETU", "3");					// 処理選択		個別定型条件
define("SELECT_PVAC", "4");						// 処理選択		公休取り込み

class duty_shift_auto_bunsan_common_class
{
    /*************************************************************************/
    // コンストラクタ
    /*************************************************************************/
    function duty_shift_auto_bunsan_common_class($group_id, $con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;

        // 勤務シフトグループID
        $this->group_id = $group_id;

        // 勤務グループの情報を取得する
        $this->group_table = $this->duty_shift_group_table();

        // 勤務パターンの情報を取得する
        $this->atdptn_table = $this->duty_shift_pattern_table();

        // 組み合わせ指定の情報を取得する
        $this->okptn_obj = new ok_pattern_class($group_id, $con, $fname);

        // 組み合わせ禁止の情報を取得する
        $this->ngptn_obj = new ng_pattern_class($group_id, $con, $fname);

		// 勤務シフトの上限チェック 2012.3.15
		$this->continue_check = array();

		// 職員相性設定 2012.3.15
		$this->pair_compatible = array();

		// 自動公休日設定で職員毎の公休日数が超過したときの情報 2012.3.15
		$this->auto_pvac_emp_info = array();

		// 自動公休日設定で同日上限公休人数超過時の情報 2012.3.15
		$this->auto_pvac_date_info = array();
		$this->auto_pvac_date_limit = array();
    }
	/*************************************************************************/
	// 自動シフト作成
	// @param	$plan_array				勤務シフト情報
	// @param	$duty_y					表示年
	// @param	$duty_m					表示月
	// @param	$start_month_flg		シフト作成期間(前月/当月)
	// @param	$start_date				開始日
	// @param	$end_date				終了日
	// @param	$auto_start_day			自動シフト作成開始日
	// @param	$auto_end_day			自動シフト作成終了日
	// @param	$auto_ptn_id			自動シフト作成対象とするシフト 0:全て その他はマスタから取得したID
	// @param	$auto_btn_flg			自動シフト作成ボタンフラグ 1:自動作成 2:一括登録 3:クリア
	// @param	$auto_ptn_id			職員設定->勤務条件より、職員毎の勤務形態と勤務日チェックのリスト
	// @return	$ret	自動作成勤務シフト情報。
	/*************************************************************************/
	function set_auto(
		$plan_array,
		$duty_y,
		$duty_m,
		$start_month_flg,
		$start_date,
		$end_date,
		$auto_start_day,
		$auto_end_day,
		$auto_ptn_id,		// この値の機能を変更 2012.4.16 '1'のときは「希望以外をクリアーして割当」
		$auto_btn_flg, 		// 2012.4.16 未使用に変更
		$emp_cond_list,		//「職員設定」の職員毎の勤務条件
		$shift_case			// 自動シフト作成ブロック分割 20140106
		) {
		///-----------------------------------------------------------------------------
		// 一括登録、クリア 2012.4.16廃止
		///-----------------------------------------------------------------------------
/*
*/
		$fname =$this->file_name;

		///-----------------------------------------------------------------------------
		// 勤務スタッフの情報を取得する
		///-----------------------------------------------------------------------------
		$this->staff_table = $this->duty_shift_staff_table($plan_array, $duty_y, $duty_m);

		///-----------------------------------------------------------------------------
		// 集計開始日〜終了日
		///-----------------------------------------------------------------------------
		//開始日
		$wk_minus_m = 0;
		if ($start_month_flg == "0") {
			$wk_minus_m = -1;
		}
		$wk_minus_m -= (int)$this->group_table["auto_check_months"];
		$wk_start_date = date("Ym", strtotime("$wk_minus_m month", $this->to_timestamp(sprintf("%04d%02d%02d", $duty_y, $duty_m, 1)))). substr($start_date, 6, 2);
		//終了日(表示日の前日)
		$wk_end_date = date("Ymd", strtotime("-1 day", $this->to_timestamp($start_date)));

		///-----------------------------------------------------------------------------
		// 発生頻度分布データの作成
		///-----------------------------------------------------------------------------
		$this->hindo_obj = new duty_shift_auto_hindo_class(
			$this->group_id,
			$wk_start_date,
			$wk_end_date,
			$this->staff_table,
			$this->atdptn_table,
			$this->_db_con,
			$this->file_name
			);

		///-----------------------------------------------------------------------------
		/// 頻度クラス作成用に常勤・非常勤情報を生成する 2012.5.16
		///-----------------------------------------------------------------------------
		$duty_form_type = array();
		foreach($this->staff_table as $wk_emp_id=>$staff) {
			$duty_form_type[$wk_emp_id] = $emp_cond_list[$wk_emp_id]["duty_form"];
		}
		$this->hindo_obj->set_duty_form( $duty_form_type );

		///-----------------------------------------------------------------------------
		// 必要人数オブジェクト
		///-----------------------------------------------------------------------------
		$this->need_obj = new duty_shift_auto_need_class($this->group_id, $duty_y, $duty_m, $start_date, $end_date, $this->_db_con, $this->file_name);

		// 日付テーブル
		$date_table = $this->need_obj->get_date_table();

		$this->shift_case = $shift_case;	// 勤務シフト作成パターン

		///-----------------------------------------------------------------------------
		//
		// 自動公休割当処理
		//
		// $public_vac_day				月間公休数・調整日数を含む
		// $emp_cond_list[$emp_id]=>array		常勤、非常勤、勤務日チェック。このメソッド呼び出しの引数
		// $auto_vac_setlist=>array			シフトグループ設定->休暇自動割当設定「土日に公休を割り当てるチェック」「同一日の常勤者の休暇上限数」
		// $this->staff_table[emp_id]["atdbk"][date]["pattern"]	勤務シフト予定
		// $shift_individual_list[$emp_id]=>array	勤務シフト希望日リスト、公休日のみ。pattern=10,reason=24
		//
		// $staff_table配列にキー'autotype'を追加。'individual'は勤務シフト希望により、'weekcheck'は非勤務日により、'auto'は自動割り当てにより公休を設定
		//
		///-----------------------------------------------------------------------------
		$public_vac_day 	= $this->month_vacation_count($date_table,$duty_y,$duty_m);	// 月間公休数を得る。カレンダー上の所定休日+法定休日+祝日の合計+調整日数
		$auto_vac_setlist 	= $this->auto_vac_set_get($date_table);						// 「シフトグループ設定」->「休暇自動割当設定」を得る
		$shift_individual_list 	= $this->shift_individual_get( $duty_y , $duty_m, $start_date, $end_date );		// 勤務シフト希望日を得る
		$emp_team_list		= $this->get_emp_teamlist();								// 職員設定のチームを得る
		$need_table		= $this->need_obj->get_need_table();							// 当日の「必要人数」の勤務シフトを取得
		$fixed_atdptn_list	= $this->get_staff_fixed_atdptn();							// 個人定型勤務条件を取得(職員毎・曜日別) 2012.6.5追加

		// 公休日自動割り振り
		// 自動シフト設定期間（当月）を4分割して処理する
		// 期間が31日の場合は8-8-8-7に分割、設定されている月内の公休日が10日の場合3-3-2-2に割り振る
		// 予め設定されている、勤務シフト予定や勤務シフト希望または職員毎の曜日非勤務チェックに公休日を設定
		// その時点で月内の公休日を超過した場合は職員毎に警告を設定する。
		// 月間公休日数に満たない場合はランダムに公休日を設定する

		$part_day= array();									// シフト設定期間を4分割。4分割毎の日数を持つ[0][1][2][3]
		$part_vac= array();									// シフト設定期間を4分割したそれぞれの職員毎公休残日数を持つ[emp_id][0][1][2][3]
		$wkd	 = floor( count($date_table) / 4 );			// 当月を４分割。小数以下切捨て
		$wkv	 = floor( $public_vac_day / 4 );			// 公休日数を４分割。小数以下切捨て

		// 4分割毎の日数と公休日数設定。例えば当月日数が31日の場合は 8-8-8-7 日となる
		// 				例えば当月公休10日の場合は 3-3-2-2となる。公休日数は職員毎
		for($c=0 ; $c<4 ; $c++) {
			$part_day[$c] = $wkd;
			foreach($this->staff_table as $wk_emp_id=>$staff) {
				$part_vac[$wk_emp_id][$c]=$wkv;	// 公休残日数
			}
		}
		$wkd = count($date_table) % 4 ;
		$wkv = $public_vac_day % 4;
		for($c=0 ; $c < $wkd ; $c++) { // 4で割った余り日数を加算
			$part_day[$c] ++;
		}
		for($c=0 ; $c < $wkv ; $c++) { // 4で割った余り公休日数を加算
			foreach($this->staff_table as $wk_emp_id=>$staff) {
				$part_vac[$wk_emp_id][$c]++;
			}
		}

		$auto_part_calendar = array();		// 1日〜月末までの各日が分割したパートのどこに含まれるかを示すカレンダー
		$part_day_element = array();		// 4パートが持つ日。例えば2012年5月の第0パートは$part_day_element[0]=array(0=>20120501,1=>20120502,.....,7=>20120508);
		$day_array = array();				// 処理日初日〜最終日までの配列。一時的に使用
		$c=1;
		foreach ( $date_table as $date => $date_row ) {
			$day_array[$c]=$date;
			$c++;
		}
		$temp_day = 1;
		$wkm = sprintf("%02d",$duty_m);
		$duty_ym = $duty_y.$wkm;
		for($c=0; $c<4; $c++) {
			$temp_array = array();
			for($cc=1 ; $cc<=$part_day[$c]; $cc++ ) {
				$wday = $day_array[$temp_day];
				$auto_part_calendar[$wday] = $c;
				$temp_array[$cc] = $wday;
				$temp_day++;
			}
			$part_day_element[$c] = $temp_array;
		}
		$emp_pvac_cnt = array(); // 職員毎の公休日数カウンタ[emp_id]

		foreach($this->staff_table as $wk_emp_id=>$staff) {
			$emp_pvac_cnt[$wk_emp_id] = 0; // 初期値
		}

		$wk_emp_idx = 0; //応援追加と下書きを変更しない 20130315

			// 設定休暇・公休日取り込み
		foreach($this->staff_table as $wk_emp_id=>$staff) {		// 職員毎

			$sub_next = 0;										// パート内で公休日数を割り当てると残日数を減らすが当該パートで減らしきれないときは次へ持ち越す
			$before_current_part= 0;
			$day_cnt = 0;										//応援追加と下書きを変更しない 20130315

			foreach ( $date_table as $date => $date_row ) {							// 日付毎

				$current_part = $auto_part_calendar[$date];							// 4分割したパートの現在の位置
				if( $current_part != $before_current_part ) {						// 4分割パートが次へ進んだかの判断
					$sub_next = 0;
					if( $part_vac[$wk_emp_id][$before_current_part] < 0 ) {			// 前パート内の公休残数がマイナスになったとき
						$sub_next = $part_vac[$wk_emp_id][$before_current_part];	// 現在パートの残数を減らすための準備
						$part_vac[$wk_emp_id][$before_current_part] = 0;			// 前パート内の公休残数をゼロにする
					}
					$part_vac[$wk_emp_id][$current_part] += $sub_next;				// 差し引ききれなかった公休残数は現在のパートの残数を減らす。負値なので加算
				}

				//応援追加と下書きを変更しない 20130315 start
				$day_cnt++;
				if ($plan_array[$wk_emp_idx]["assist_group_$day_cnt"] != $this->group_id) {
					continue;
				}
				//応援追加と下書きを変更しない 20130315 end

				// 「希望以外をクリアーして割当」のチェックがある場合は勤務予定等、既設定の勤務シフトを消去
				if ( $auto_ptn_id == '1' ) {
					unset($this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern']);
					unset($this->staff_table[$wk_emp_id]['atdbk'][$date]['reason']);
					unset($this->staff_table[$wk_emp_id]['atdbk'][$date]['autoset']);
					unset($this->staff_table[$wk_emp_id]['atdbk'][$date]['filler']);
				}
				else {
					// $plan_arrayから既存のデータをコピーする 201401  ///////////////////////////////////////
					if ( $plan_array[$wk_emp_idx]["atdptn_ptn_id_$day_cnt"] != "" ) {
						$this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] = $plan_array[$wk_emp_idx]["atdptn_ptn_id_$day_cnt"];
						$this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] = $plan_array[$wk_emp_idx]["reason_$day_cnt"];
						$this->staff_table[$wk_emp_id]['atdbk'][$date]['autoset'] = "";
						$this->staff_table[$wk_emp_id]['atdbk'][$date]['filler'] = "";
					}
				}

				//応援追加と下書きを変更しない 20130315 start
				//下書き分は、設定済みとして処理
				if ($plan_array[$wk_emp_idx]["draft_$day_cnt"] == "1") {
					$this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] = $plan_array[$wk_emp_idx]["atdptn_ptn_id_$day_cnt"];
					$this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] = $plan_array[$wk_emp_idx]["reason_$day_cnt"];
				}
				//応援追加と下書きを変更しない 20130315 end

				// 勤務シフト予定の休暇日を判定、休暇日残数減らす。カウンタ加減
				if ( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == PATTERN_IS_VACATION ) {
					if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] == REASON_IS_PUBLIC) {
						$this->staff_table[$wk_emp_id]['atdbk'][$date]['autotype'] = 'schedule';
						$emp_pvac_cnt[$wk_emp_id]++;
						$part_vac[$wk_emp_id][$current_part] -- ;
					}else if($this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] == REASON_IS_PUBLIC_HALF ) {
						$emp_pvac_cnt[$wk_emp_id] += 0.5;
						$part_vac[$wk_emp_id][$current_part] -= 0.5 ;
					}
				}

				//--------------------------------------------------------------------------------------------------
				// ここからが処理
				//--------------------------------------------------------------------------------------------------
				if(($shift_case == SELECT_ALL) || ($shift_case == SELECT_INDIVIDUAL)) {			// 機能分割（全て 又は 勤務希望取り込み）

					if(($this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == "") && (!empty($shift_individual_list[$wk_emp_id][$date]['atdptn_ptn_id']))) {
						// 3,4引数の1,1はダミー。戻り値も見ない
						$wk_pattern = $shift_individual_list[$wk_emp_id][$date]['atdptn_ptn_id'];
						$wk_reason = $shift_individual_list[$wk_emp_id][$date]['reason'];
						$ret = $this->set_pattern2( $wk_emp_id , $date , 1, 1, $wk_pattern, $wk_reason, "individual", $fp);
						// 休暇日のときは休暇日残数減らす。休暇日カウンタ加減
						if ( $shift_individual_list[$wk_emp_id][$date]['atdptn_ptn_id'] == PATTERN_IS_VACATION ){
							if ($shift_individual_list[$wk_emp_id][$date]['reason'] == REASON_IS_PUBLIC ) {
								$emp_pvac_cnt[$wk_emp_id]++;
								$part_vac[$wk_emp_id][$current_part] -- ;
							} else if ($shift_individual_list[$wk_emp_id][$date]['reason'] == REASON_IS_PUBLIC_HALF ) {
								$emp_pvac_cnt[$wk_emp_id] += 0.5;
								$part_vac[$wk_emp_id][$current_part] -= 0.5 ;
							}
						}
					}
				}


				if(($shift_case == SELECT_ALL) || ($shift_case == SELECT_KOBETU)) {			// 機能分割（全て 又は 個人定型条件）

					$week = $date_row['week'];												// 今日の曜日
					if ( $week2 = $date_row['week2'] == 7 ){								// 今日は祝日かどうか
						$week = 7;
					}
					// Integer値を参照 20140128 START
					$fixed_atdptn = $fixed_atdptn_list[$wk_emp_id][$week]['atdptn'];
//					if(($this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == "") && (!empty($fixed_atdptn_list[$wk_emp_id][$week]['atdptn']))) {
					if(($this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == "") && ($fixed_atdptn > 0)) {
					// Integer値を参照 END
						// 3,4引数の1,1はダミー。戻り値も見ない
						$wk_pattern = $fixed_atdptn_list[$wk_emp_id][$week]['atdptn'];
						$wk_reason = $fixed_atdptn_list[$wk_emp_id][$week]['reason'];
						$ret = $this->set_pattern2( $wk_emp_id , $date ,1,1, $wk_pattern , $wk_reason ,"fixed", $fp);
						// 休暇日のときは休暇日残数減らす。休暇日カウンタ加減
						if ( $wk_pattern == PATTERN_IS_VACATION ) {
							if ($wk_reason == REASON_IS_PUBLIC ) {
								$emp_pvac_cnt[$wk_emp_id]++;
								$part_vac[$wk_emp_id][$current_part] -- ;
							} else if ($wk_reason == REASON_IS_PUBLIC_HALF) {
								$emp_pvac_cnt[$wk_emp_id] += 0.5;
								$part_vac[$wk_emp_id][$current_part] -= 0.5 ;
							}
						}
					}
				}


				if(($shift_case == SELECT_ALL) || ($shift_case == SELECT_PVAC)) {	// 機能分割（公休取り込み）

					// 職員設定->曜日別の非勤務日を公休日とする処理、月〜土日祝
					if(( $emp_cond_list[$wk_emp_id]["duty_mon_day_flg"]   != 1 && $date_row["week"] == 1 ) // 左辺は曜日別の非勤務設定、右辺は毎日の曜日。月火水木金土日祝
					|| ( $emp_cond_list[$wk_emp_id]["duty_tue_day_flg"]   != 1 && $date_row["week"] == 2 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_wed_day_flg"]   != 1 && $date_row["week"] == 3 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_thurs_day_flg"] != 1 && $date_row["week"] == 4 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_fri_day_flg"]   != 1 && $date_row["week"] == 5 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_sat_day_flg"]   != 1 && $date_row["week"] == 6 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_sun_day_flg"]   != 1 && $date_row["week"] == 0 )
					|| ( $emp_cond_list[$wk_emp_id]["duty_hol_day_flg"]   != 1 && $date_row["week2"] == 7 )) {

						if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == "" ) { // 非勤務日(勤務不可日)に勤務（予定と希望）が設定されていることがある

							$return_array = $this->set_pattern2($wk_emp_id, $date, $part_vac[$wk_emp_id][$current_part], $emp_pvac_cnt[$wk_emp_id],
																	PATTERN_IS_VACATION, REASON_IS_PUBLIC, "weekcheck", $fp);
							// 休暇日残数減らす。休暇日カウンタ加減
							$emp_pvac_cnt[$wk_emp_id] = $return_array["add"];
							$part_vac[$wk_emp_id][$current_part] = $return_array["sub"];
						}
					}
				}

				$before_current_part = $current_part;
			}
            $wk_emp_idx++; //応援追加と下書きを変更しない 20130315
		}


		// 割り当て→土日祝チェック→同日上限日数チェック
		if(($shift_case == SELECT_ALL) || ($shift_case == SELECT_PVAC)) {		// 機能分割（全て 又は 公休取り込み）

			// 割り当てた公休日数が月毎公休日数に足りない場合は公休日を自動で割り当てる処理
			foreach($this->staff_table as $wk_emp_id=>$staff) { // 職員毎
				if( $emp_cond_list[$wk_emp_id]["duty_form"] == 2 ) { // 非常勤を除く
					continue;
				}
				// 公休残日数があるときは自動割り当て処理。無いときは割り当てない
				if ( $emp_pvac_cnt[$wk_emp_id] >= $public_vac_day ) {
					continue;
				}
				$return_array = $this->auto_pvac_allocate($wk_emp_id, $part_day_element, $public_vac_day,
												$part_vac[$wk_emp_id], $emp_pvac_cnt[$wk_emp_id], $emp_team_list, "", $fp, "", $auto_ptn_id );
				$part_vac[$wk_emp_id]     = $return_array["part_vac"];
				$emp_pvac_cnt[$wk_emp_id] = $return_array["emp_pvac_cnt_unit"];
				$return_sts		  = $return_array["status"];
			}

			// 土日祝に公休を割り当てるチェックがあるときは、土日祝の公休日数を数える
			// 土日祝に公休日が無いときは公休日の自動割り当てを解除して再割り当てをして、土日祝の公休日数を数える
			if ($auto_vac_setlist["force_hol_set"] == "c" ) {
				foreach($this->staff_table as $wk_emp_id=>$staff) { // 土日祝の公休日数カウンタがゼロの職員を割り当てなおす
					if( $emp_cond_list[$wk_emp_id]["duty_form"] == 2 ) { // 非常勤を除く
						continue;
					}
					$limit_counter = 0;
					while( $limit_counter < 30 ) { // 30回処理してだめだったらあきらめる
						// 土日祝の休暇日数を数える
						$hol_counter = $this->auto_pvac_sshcount($wk_emp_id, $date_table, PATTERN_IS_VACATION, $fp);
						// 土日祝に休暇日が設定されているときは終わり。次の職員
						if ( $hol_counter != 0 ) {
							break;
						}

						// 自動割り当ての公休日を解除
						$return_array =
							$this->auto_pvac_deallocate($wk_emp_id, $auto_part_calendar,
														$part_vac[$wk_emp_id], $emp_pvac_cnt[$wk_emp_id], PATTERN_IS_VACATION, REASON_IS_PUBLIC, $fp);
						$realloc_count = $return_array["realloc_count"];
						// 自動割り当ての公休日数が無い。再設定できないので次の職員へ
						if ( $realloc_count == 0 ) {
							break ;
						}

						$part_vac[$wk_emp_id]     = $return_array["part_vac"];
						$emp_pvac_cnt[$wk_emp_id] = $return_array["emp_pvac_cnt_unit"];

						// 公休日の自動割り当て、再設定
						$return_array = $this->auto_pvac_allocate($wk_emp_id, $part_day_element, $public_vac_day, $part_vac[$wk_emp_id],
												$emp_pvac_cnt[$wk_emp_id], $emp_team_list, "", $fp, "", $auto_ptn_id);
						$part_vac[$wk_emp_id]     = $return_array["part_vac"];
						$emp_pvac_cnt[$wk_emp_id] = $return_array["emp_pvac_cnt_unit"];

						$limit_counter++; // 30回処理してだめだったらあきらめる
					}
					if ( $limit_counter > 30 ) {
						$this->auto_pvac_emp_info[$wk_emp_id]["info"] = "vacation over";
						// 警告処理
					}
				}
			}
			// 縦計チェック。同一日上限人数
			$ret = $this->auto_pvac_colcheck($emp_cond_list, $date_table, $auto_part_calendar, $part_day_element, $public_vac_day,
												$auto_vac_setlist, $part_vac, $emp_pvac_cnt,$emp_team_list, $fp, $auto_ptn_id);
		}
		// 自動公休処理おわり *****************

		$shift_case_val = (int)$shift_case;											// 引数を数値化する
		$setting_no = $shift_case_val - 50;											// オフセット(50)を除いて必要人数設定行番号を取得する
																					// 必要人数設定１行目 -> 51
																					// 必要人数設定２行目 -> 52

		///-----------------------------------------------------------------------------
		// 自動で埋める日付ループ
		//
		// 選択リスト「全て実行」の場合
		// 選択リスト「必要人数設定」の場合
		///-----------------------------------------------------------------------------
		if(($shift_case == SELECT_ALL) || ($setting_no > 0)) {						// 機能分割（全て 又は 必要人数設定）

			if($setting_no > 0) {	// 必要人数設定を選択されている時
				// 勤務グループの情報を取得する（必要人数設定）
				$this->group_table = $this->duty_shift_group_table($setting_no);
			}

			$cnt = 0;
			$day_cnt = 0;
//			$first_time = true;	// 20140114 未使用

			// シフト上限日数テーブル初期化
			foreach ( $this->staff_table as $emp_id=>$staff ) {
				$this->check_shift_limit_set( $emp_id, $this->staff_table[$emp_id]["continue_limit"] );
			}

			foreach ( $date_table as $date => $date_row ) {

				$day_cnt++;

				// 開始日と終了日
				if ( $day_cnt < $auto_start_day or $day_cnt > $auto_end_day ) {
					continue;
				}

				//7日おきに頻度更新
				if ( $cnt % 7 == 0 ) {
					$this->hindo_obj->load_hindo_group();
				}

				$ng_staff = array();
				$sex_pattern = array();
				$wk_emp_idx = 0; //応援追加と下書きを変更しない 20130315

				foreach ( $this->staff_table as $emp_id => $staff ) {

					//応援追加と下書きを変更しない 20130315 start
					if ($plan_array[$wk_emp_idx]["assist_group_$day_cnt"] != $this->group_id) { //応援追加は以下の処理をしない
						$wk_emp_idx++;
						continue;
					}//下書き分は以下の処理をする
					//応援追加と下書きを変更しない 20130315 end

					// すでに割り当てられているシフトを必要人数から引く
					if ( $staff['atdbk'][$date]['pattern'] != "" ) {
						$this->need_obj->minus($date, $staff['atdbk'][$date]['pattern'], $staff['job_id'], $staff['team_id'], $staff['sex'], $staff['st_id']);
						$ng_staff[$emp_id] = 1;
						// 性別固定
						if ($this->group_table["sex_pattern"][$staff['atdbk'][$date]['pattern']] && empty($sex_pattern[$staff['atdbk'][$date]['pattern']])) {
							$sex_pattern[$staff['atdbk'][$date]['pattern']] = $staff['sex'];
						}
					}
					//禁止条件・連続勤務日数上限
					elseif( ! $this->check_limit_day($date_row, $emp_id) ) {
						//休暇を割り当てる
						$hdate = $this->get_holiday($date_row, $emp_id);
						$this->set_pattern($hdate, $emp_id, "10", REASON_IS_PUBLIC, 1);
						$this->need_obj->minus($hdate["date"], "10", $staff['job_id'], $staff['team_id'], $staff['sex'], $staff['st_id']);
						if ( $hdate["date"] == $date ) {
							$ng_staff[$emp_id] = 1;
						}
					}
					$wk_emp_idx++; //応援追加と下書きを変更しない 20130315
				}

				// 当日の必要な勤務シフトを取得
				$need_shift = $this->need_obj->get_need_shift_array($date);

				foreach ( $need_shift as $shift ) {

					// 休暇処理は、自動休暇割り当て処理と矛盾するため処理しない
					if ( $shift["atdptn_ptn_id"] == PATTERN_IS_VACATION ) {
						continue;
					}

					// 設定行指定の場合、指定勤務パターン以外中止
					if ($setting_no > 0) {
						if ($shift["atdptn_ptn_id"] != $this->group_table["default"]["pattern"]) {
							continue;
						}
					}

					//頻度順に並べ替えた職員リスト(グループごとにランダム化)
					$staff_list = $this->hindo_obj->get_staff_array($date_row["week2"], $shift["atdptn_ptn_id"]);

					//割り当てループ
					foreach ( $staff_list as $emp_id ) {
						// 割り当て済み
						if ( !empty($ng_staff[$emp_id]) ) {
							continue;
						}

						// 職種
						if ( (!empty($shift["job_id"]) or !empty($shift["job_id2"]) or !empty($shift["job_id3"])) and
							$shift["job_id"] != $this->staff_table[$emp_id]["job_id"] and
							$shift["job_id2"] != $this->staff_table[$emp_id]["job_id"] and
							$shift["job_id3"] != $this->staff_table[$emp_id]["job_id"] ) {
								continue;
						}
						// 要求スキルレベル 2012.3.14追加
						// 要求スキルレベル（２つめ） 2014.1.21追加  Ａ or Ｂ
						//if(((($shift["skill"] != '' && $shift["skill"] != '0') && $shift["skill"] != $this->staff_table[$emp_id]["skill"])) ||
						//	((($shift["skill2"] != '' && $shift["skill2"] != '0') && $shift["skill2"] != $this->staff_table[$emp_id]["skill"]))) {
						//		continue;
						//}
						//2つめのスキルがある場合の不具合対応 20140207 
						//1つめのスキルだけの場合
						if(($shift["skill"] != '' && $shift["skill"] != '0') && ($shift["skill2"] == '' || $shift["skill2"] == '0')) {
							if($shift["skill"] != $this->staff_table[$emp_id]["skill"]) {
								continue;
							}
						}
						//2つめのスキルだけの場合
						if(($shift["skill"] == '' || $shift["skill"] == '0') && ($shift["skill2"] != '' && $shift["skill2"] != '0')) {
							if($shift["skill2"] != $this->staff_table[$emp_id]["skill"]) {
								continue;
							}
						}
						//両方のスキルが設定された場合
						if(($shift["skill"] != '' && $shift["skill"] != '0') && ($shift["skill2"] != '' && $shift["skill2"] != '0')) {
							if ($shift["skill"] != $this->staff_table[$emp_id]["skill"] &&
								$shift["skill2"] != $this->staff_table[$emp_id]["skill"]) {
								continue;
							}
						}
						// チーム
						if ( !empty($shift["team"]) && $shift["team"] != $this->staff_table[$emp_id]["team_id"] ) {
							continue;
						}

						// 性別
						if ( !empty($shift["sex"]) && $shift["sex"] != $this->staff_table[$emp_id]["sex"] ) {
							continue;
						}

						// 性別固定シフト
						if ( !empty($sex_pattern[$shift["atdptn_ptn_id"]]) && $sex_pattern[$shift["atdptn_ptn_id"]] != $this->staff_table[$emp_id]["sex"] ) {
							continue;
						}

						// 役職
						if ( !empty($shift["st_id"]) && $shift["st_id"] != $this->staff_table[$emp_id]["st_id"] ) {
							continue;
						}

						// 勤務条件・夜勤
						if ( ! $this->check_night_duty($shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 勤務条件・勤務可能なシフト
						if ( ! $this->check_duty_shift($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 禁止条件・連続勤務シフト上限
						if ( ! $this->check_limit_shift($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 禁止条件・勤務シフト間隔
						if ( ! $this->check_interval_shift($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 禁止条件・組み合わせ禁止(前日)
						if ( ! $this->check_ng_prev_day($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 禁止条件・組み合わせ禁止(翌日)
						if ( ! $this->check_ng_next_day($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							continue;
						}
						// 勤務シフトの上限チェック 2012.3.22
						if( !$this->check_shift_limit( $emp_id ,  $shift["atdptn_ptn_id"], $day_cnt) ) {
							continue;
						}
						// 相性チェック
						if ( ! $this->check_pair_set($date , $emp_id , 1 , '' ) ) { // 他の人との相性をチェック
							continue; // ○×いずれかが設定されていたら次の人へ。
						} // ○×設定が無いときは通常処理 → ○の時は相手方を設定して処理を継続 20140127

						// パターンを割り当てる
						$reason = ($shift["atdptn_ptn_id"] == PATTERN_IS_VACATION) ? REASON_IS_PUBLIC : "";
						$this->set_pattern($date_row, $emp_id, $shift["atdptn_ptn_id"], $reason, 1, "", "");
						$ng_staff[$emp_id] = 1;

						// 組み合わせ
						if ( ! $this->sequence($date_row, $shift["atdptn_ptn_id"], $emp_id) ) {
							//組み合わせがうまくいかない場合は、パターン割り当てをリセットする
							//ただし、登録済みのものはリセットしないように
							if ( $this->staff_table[$emp_id]["atdbk"][$date]["autoset"] == 1 ) {
								$this->staff_table[$emp_id]["atdbk"][$date]["pattern"] = "";
								$this->staff_table[$emp_id]["atdbk"][$date]["reason"] = "";
								$this->staff_table[$emp_id]["atdbk"][$date]["autoset"] = "";
								$this->staff_table[$emp_id]["atdbk"][$date]["filler"] = "";
								$ng_staff[$emp_id] = null;
								$this->hindo_obj->minus($date_row["week2"], $shift["atdptn_ptn_id"], $emp_id);
							}
							continue;
						}
						// 性別固定登録
						if ($this->group_table["sex_pattern"][$shift["atdptn_ptn_id"]] && empty($sex_pattern[$shift["atdptn_ptn_id"]])) {
							$sex_pattern[$shift["atdptn_ptn_id"]] = $this->staff_table[$emp_id]["sex"];
						}
						break;
					}
				}

				// シフトが決まっていないスタッフにデフォルトのシフトを割り当てる
				foreach ( $this->staff_table as $emp_id => $staff ) {

					if($setting_no > 0) {		// 設定行指定の場合は分散処理をしない
						continue;
					}

					if( $emp_cond_list[$emp_id]["duty_form"] == 1 ) { 	// 常勤はとりあえず公休
						$set_pattern = PATTERN_IS_VACATION;
						$set_reason  = REASON_IS_PUBLIC;
					}else{ 							// 非常勤は未設定
						$set_pattern = "";
						$set_reason  = "";
					}

					if ( empty($staff['atdbk'][$date]['pattern']) && !empty($this->group_table["default"]["pattern"]) ) {

						// 禁止条件・連続勤務日数上限
						if (! $this->check_limit_day($date_row, $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition1");
							continue;
						}
						// 勤務条件・夜勤
						if (! $this->check_night_duty($this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition2");
							continue;
						}
						// 勤務条件・勤務可能なシフト
						if (! $this->check_duty_shift($date_row, $this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition3");
							continue;
						}
						// 禁止条件・連続勤務シフト上限
						if (! $this->check_limit_shift($date_row, $this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition4");
							continue;
						}
						// 禁止条件・勤務シフト間隔
						if (! $this->check_interval_shift($date_row, $this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition5");
							continue;
						}
						// 禁止条件・組み合わせ禁止(前日)
						if (! $this->check_ng_prev_day($date_row, $this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition6");
							continue;
						}
						// 禁止条件・組み合わせ禁止(翌日)
						if (! $this->check_ng_next_day($date_row, $this->group_table["default"]["pattern"], $emp_id)) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition7");
							continue;
						}
						// 連続勤務シフトの上限チェック 2012.3.22
						if( ! $this->check_shift_limit($emp_id, $this->group_table["default"]["pattern"] , $day_cnt )) {
							$this->set_pattern($date_row, $emp_id, $set_pattern,$set_reason, 1, "", "prohibition8");
							continue;
						}
						// 相性チェック
						if ( ! $this->check_pair_set($date , $emp_id , 1 , 1 ) ) { // 他の人との相性をチェック
							continue; // ○×いずれかが設定されていたら次の人へ。
						}	// ○×設定が無いときは通常処理 → ○の時は相手方を設定して処理を継続 20140127

						// デフォルトのシフトをセット
						if( $emp_cond_list[$emp_id]["duty_form"] == 1 ) { // 常勤職員のみ設定する。非常勤・短時間職員は設定なし
							$this->set_pattern($date_row, $emp_id, $this->group_table["default"]["pattern"], $this->group_table["default"]["reason"], 1, 1 );
						}
					}
				}
			}
			$cnt++;
		}		// 機能分割（全て）

		//-----------------------------------------------------------------------------------
		// 共通処理
		//-----------------------------------------------------------------------------------

		// 戻すために旧形式のデータに変換
		$data = array();
        $wk_emp_idx = 0; //応援追加と下書きを変更しない 20130315
		foreach ( $this->staff_table as $staff_id => $staff ) {
			$row = array(
					"staff_id" => $staff_id,
					"job_id" => $staff["job_id"],
					);

			$k = 1;
			foreach ( $date_table as $date => $date_row ) {
				//応援追加と下書きを変更しない 20130315 start
				if ($plan_array[$wk_emp_idx]["assist_group_$k"] == $this->group_id) {
					$ptn = $staff["atdbk"][$date];
					$row["pattern_id_$k"]		= $this->group_table["pattern_id"];
					$row["atdptn_ptn_id_$k"]	= $ptn["pattern"];
					$row["reason_$k"]			= $ptn["reason"];
					$row["reason_2_$k"]			= "";
				}
				$row["autotype_$k"]			= $ptn["autotype"];
				$row["assist_group_$k"] = $plan_array[$wk_emp_idx]["assist_group_$k"];
				//応援追加と下書きを変更しない 20130315 end
				$k++;
			}
			$data[] = $row ;
			$wk_emp_idx++;
		}

		return $data;
	}


	/*************************************************************************/
	// 勤務パターンをセット
	/*************************************************************************/
	function set_pattern($date, $emp_id, $ptn, $reason, $auto="", $filler="" , $type="")
	{
		$this->staff_table[$emp_id]["atdbk"][$date["date"]]["pattern"] = $ptn;
		$this->staff_table[$emp_id]["atdbk"][$date["date"]]["reason"]  = $reason;
		$this->staff_table[$emp_id]["atdbk"][$date["date"]]["autoset"] = $auto;
		$this->staff_table[$emp_id]["atdbk"][$date["date"]]["filler"]  = $filler;
		$this->staff_table[$emp_id]['atdbk'][$date["date"]]['autotype'] = $type; // 2012.4.19
		$this->hindo_obj->plus($date["week2"], $ptn, $emp_id);
	}

	/*************************************************************************/
	// 勤務シフトスタッフデータ取得
	/*************************************************************************/
	function duty_shift_staff_table($plan_array, $duty_y, $duty_m)
	{
		$duty_date = sprintf("%04d%02d", $duty_y, $duty_m);
		$wk_sdate = date("Ym", strtotime("-1 month", $this->to_timestamp("{$duty_date}01")));
		$wk_edate = date("Ym", strtotime(" 1 month", $this->to_timestamp("{$duty_date}01")));
		$sql = <<<_SQL_END_
SELECT
c1.*,
TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') + interval '1 days','YYYYMMDD') AS next,
TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') - interval '1 days','YYYYMMDD') AS prev,
DATE_PART('DOW',TO_DATE(c1.date,'YYYYMMDD')) AS week,
CASE
  WHEN c1.type='6' THEN 7
  WHEN c2.type='6' THEN 8
  ELSE DATE_PART('DOW',TO_DATE(c1.date,'YYYYMMDD'))
END AS week2
FROM calendar c1
LEFT JOIN calendar c2 ON c2.date=TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') + interval '1 days','YYYYMMDD')
_SQL_END_;
		$cond = "WHERE (c1.date LIKE '$wk_sdate%' OR c1.date LIKE '$wk_edate%' OR c1.date LIKE '$duty_date%') ORDER BY c1.date";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM calendar -->");
			exit;
		}
		$date_array = array();
		while ($row = pg_fetch_assoc($sel)) {
			$date_array[ $row["date"] ] = $row;
		}

		// d_s_plan_staffの有無をチェックするSQL
		$sql = "SELECT COUNT(*) as cnt FROM duty_shift_plan_staff";
		$cond = "WHERE group_id='{$this->group_id}' AND duty_yyyy='$duty_y' AND duty_mm='$duty_m'";
		$sel_cnt = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_cnt == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$count = pg_fetch_result($sel_cnt, 0, 'cnt');

		// 職員ループ
		$staff_array = array();

		foreach ( $plan_array as $i => $plan ) {
			// d_s_plan_staffがある
			if ( $count ) {
				$sql = <<<_SQL_END_
SELECT
*,
e.emp_job AS job_id,
e.emp_sex AS sex,
e.emp_st AS st_id,
e.emp_id AS emp_id
FROM empmst e
LEFT JOIN duty_shift_plan_staff s ON s.group_id='{$this->group_id}' AND e.emp_id=s.emp_id AND s.duty_yyyy=$duty_y AND s.duty_mm=$duty_m
LEFT JOIN duty_shift_plan_team pt ON pt.group_id=s.group_id AND pt.emp_id=s.emp_id AND pt.duty_yyyy=s.duty_yyyy AND pt.duty_mm=s.duty_mm
_SQL_END_;
				$cond = <<<_COND_END_
WHERE e.emp_id='{$plan["staff_id"]}'
ORDER BY no
_COND_END_;
			}
			// d_s_plan_staffがない
			else {
				$sql = <<<_SQL_END_
SELECT
*,
e.emp_job AS job_id,
e.emp_sex AS sex,
e.emp_st AS st_id,
e.emp_id AS emp_id,
COALESCE(pt.team_id,s.team_id) AS team_id
FROM empmst e
LEFT JOIN duty_shift_staff s ON s.group_id='{$this->group_id}' AND e.emp_id=s.emp_id
LEFT JOIN duty_shift_plan_team pt ON pt.group_id=s.group_id AND pt.emp_id=s.emp_id AND pt.duty_yyyy=$duty_y AND pt.duty_mm=$duty_m
_SQL_END_;
				$cond = <<<_COND_END_
WHERE e.emp_id='{$plan["staff_id"]}'
ORDER BY no
_COND_END_;
			}

			// 勤務シフトスタッフデータをSELECT
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$row = pg_fetch_assoc($sel);

			//個人勤務条件を取得
			$sql = "SELECT * FROM duty_shift_staff_employment";
			//グループ移動した場合取得できないため条件からグループを削除 20111006
			$cond = "WHERE emp_id='{$row["emp_id"]}'";
			$sel_employment = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_employment == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			//array_mergeの前にデータがあるか確認する。PHP5では引数がarrayでないと結果がnullになるため 20111005
			$num = pg_num_rows(sel_employment);
			if ($num > 0) {
				$row2 = pg_fetch_assoc($sel_employment);
				$row = array_merge($row, $row2);
			}

			//個人勤務条件・出勤パターンを取得
			$sql = "SELECT * FROM duty_shift_staff_employment_atdptn";
			//グループ移動した場合取得できないため条件からグループを削除 20111006
			$cond = "WHERE emp_id='{$row["emp_id"]}'";
			$sel_atdptn = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_atdptn == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$atdptn_array = array();
			while ( $atdptn_row = pg_fetch_assoc($sel_atdptn) ) {
				$atdptn_array[ $atdptn_row["week_flg"] ][ $atdptn_row["atdptn_ptn_id"] ] = $atdptn_row["use_flg"];
			}
			$row["emp_atdptn"] = $atdptn_array;

			// 職員別スキル情報を取得 2012.3.14追加
			$sql = "SELECT emp_skill_level FROM emp_skill";
			$cond = "WHERE emp_id='{$row["emp_id"]}'";
			$sel_skill = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_employment == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$row["skill"] = pg_result($sel_skill,0,"emp_skill_level");

			// 職員別連続シフトの上限日数を取得 2012.3.14追加
			$sql = "SELECT atdptn_id , limit_day_count FROM emp_shift_limit";
			$cond = "WHERE emp_id='{$row["emp_id"]}'";
			$sel_conti_limit = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_conti_limit == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sel_cnt = pg_numrows($sel_conti_limit);
			$wk_array = array();
			for($r=0;$r<$sel_cnt;$r++) {
				$wk_atdptn = pg_result($sel_conti_limit,$r,"atdptn_id");
				$wk_array[$wk_atdptn] = pg_result($sel_conti_limit,$r,"limit_day_count");
			}
			$row["continue_limit"] = $wk_array;

			// 職員毎の相性設定情報を取得 2012.3.14追加
//			$sql = "SELECT emp_id , emp_id_2 , compatibility FROM emp_pair_set ";
			$sql = "select emp_id, emp_id_2, compatibility, atdptn_id from emp_pair_set ";			// 相性設定に勤務シフトを加味する 2014.01

			$cond = "WHERE emp_id='{$row["emp_id"]}' ORDER BY emp_id";
			$sel_compat = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_compat == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sel_cnt = pg_numrows($sel_compat);
			$wk_array = array();
			for($r=0;$r<$sel_cnt;$r++) {
				$wk_emp_id = pg_result($sel_compat,$r,"emp_id");
				$wk_emp_id2 = pg_result($sel_compat,$r,"emp_id_2");
																									// 相性設定に勤務シフトを加味する 2014.01 START

//				$wk_atdptn = pg_result($sel_compat,$r,"atdptn_id");
				$wk_atdptn = mb_substr(pg_result($sel_compat, $r, "atdptn_id"), 0, 2);				// atdptn_id(4) -> (2)
				if($wk_atdptn == ''){																// 勤務パターン登録がない場合
					$wk_atdptn = '00';
				}
				$wk_array[$wk_emp_id][$wk_emp_id2][$wk_atdptn] = pg_result($sel_compat, $r, "compatibility");
			}
			// データは設定分しか無いので反対の組み合わせも得る
//			$sql = "SELECT emp_id , emp_id_2 , compatibility FROM emp_pair_set ";
			$sql = "select emp_id, emp_id_2, compatibility, atdptn_id from emp_pair_set ";			// 相性設定に勤務シフトを加味する 2014.01

			$cond = "WHERE emp_id_2='{$row["emp_id"]}' ORDER BY emp_id_2";
			$sel_compat = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel_compat == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sel_cnt = pg_numrows($sel_compat);
			for($r=0;$r<$sel_cnt;$r++) {
				$wk_emp_id = pg_result($sel_compat,$r,"emp_id_2");
				$wk_emp_id2 = pg_result($sel_compat,$r,"emp_id");
																									// 相性設定に勤務シフトを加味する 2014.01 START
//				$wk_atdptn = pg_result($sel_compat,$r,"atdptn_id");
				$wk_atdptn = mb_substr(pg_result($sel_compat, $r, "atdptn_id"), 0, 2);				// atdptn_id(4) -> (2)
				if($wk_atdptn == ''){																// 勤務パターン登録がない場合
					$wk_atdptn = '00';
				}
				$wk_array[$wk_emp_id][$wk_emp_id2][$wk_atdptn] = pg_result($sel_compat, $r, "compatibility");
				// 相性設定に勤務シフトを加味する END
			}

			$row["pair_compati"] = $wk_array;

			$row["atdbk"] = $date_array;
			$staff_table[ $row["emp_id"] ] = $row;
			$staff_array[] = sprintf("'%s'", $row["emp_id"]);
		}
		$staff_list = implode(",", $staff_array);

		//勤務予定(atdbk) / 勤務実績(atdbkrslt)
		if (!empty($staff_list)) {
			$sql = <<<_SQL_END_
SELECT
*,
COALESCE(ar.pattern,a.pattern) AS pattern2,
COALESCE(ar.reason,a.reason) AS reason2,
CASE ar.pattern WHEN null THEN 1 ELSE 0 END as rslt_flg
FROM atdbk a
LEFT JOIN atdbkrslt ar USING (emp_id,date)
_SQL_END_;
			$cond = <<<_COND_END_
WHERE emp_id IN ($staff_list)
AND (date LIKE '$wk_sdate%' OR date LIKE '$wk_edate%' OR date LIKE '$duty_date%')
ORDER BY emp_id, date
_COND_END_;
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ( $row = pg_fetch_assoc($sel) ) {
				$row['pattern'] = $row['pattern2'];
				$row['reason'] = $row['reason2'];
				$staff_table[ $row["emp_id"] ]["atdbk"][ $row["date"] ] = array_merge($staff_table[ $row["emp_id"] ]["atdbk"][ $row["date"] ], $row);
			}
		}

		return $staff_table;
	}

	/*************************************************************************/
	// 勤務シフトグループデータ取得
	// 必要人数設定番号を追加				20140114
	//
	// $need_no 参照する必要人数設定の行番号
	/*************************************************************************/
	function duty_shift_group_table($need_no)
	{
		// duty_shift_group
		$sql = "SELECT * FROM duty_shift_group";
		$cond = "WHERE group_id='{$this->group_id}'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group -->");
			exit;
		}
		$row = pg_fetch_assoc($sel);

		// 勤務シフトの間隔
		$sql = "SELECT * FROM duty_shift_group_interval WHERE group_id='{$this->group_id}'";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group_interval -->");
			exit;
		}
		$intarval_array = array();
		while ( $int_row = pg_fetch_assoc($sel) ) {
			$intarval_array[ $int_row["atdptn_ptn_id"] ] = $int_row;
		}
		$row['interval'] = $intarval_array;

		// 連続勤務シフトの上限
		$sql = "SELECT * FROM duty_shift_group_upper_limit WHERE group_id='{$this->group_id}'";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group_upper_limit -->");
			exit;
		}
		$upper_limit_array = array();
		while ( $upp_row = pg_fetch_assoc($sel) ) {
			$upper_limit_array[ $upp_row["atdptn_ptn_id"] ] = $upp_row;
		}
		$row['upper_limit'] = $upper_limit_array;

		// 同時勤務の性別を固定するシフト
		$sql = "SELECT * FROM duty_shift_group_sex_pattern WHERE group_id='{$this->group_id}'";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group_sex_pattern -->");
			exit;
		}
		$sex_pattern_array = array();
		while ( $row2 = pg_fetch_assoc($sel) ) {
			$sex_pattern_array[ sprintf("%d",substr($row2["ptn_id"],0,2)) ] = true;
		}
		$row['sex_pattern'] = $sex_pattern_array;


		// 必要人数設定から任意の設定を優先する場合 START 201401
		if ( (int)$need_no != 0) {
				$sql = "SELECT atdptn_ptn_id as pattern FROM duty_shift_need_cnt_standard WHERE group_id='{$this->group_id}' and no=$need_no";
				$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
				if ($sel == 0) {
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$row['default'] = pg_fetch_assoc($sel);
				$row['default']['reason'] = "";
		} else {

			// 自動割り当てできなかったときのシフト
			switch ($row["auto_shift_filler"]) {
				// 勤務記号の一番上
				case "1":
					$sql = "SELECT atdptn_ptn_id as pattern, reason FROM duty_shift_pattern p LEFT JOIN duty_shift_group g USING (pattern_id) WHERE g.group_id='{$this->group_id}' ORDER BY p.order_no, atdptn_ptn_id LIMIT 1";
					$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
					if ($sel == 0) {
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$row['default'] = pg_fetch_assoc($sel);
					break;

				// 必要人数の一番上
				case "2":
					$sql = "SELECT atdptn_ptn_id as pattern FROM duty_shift_need_cnt_standard WHERE group_id='{$this->group_id}' ORDER BY no, atdptn_ptn_id LIMIT 1";
					$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
					if ($sel == 0) {
						pg_close($this->_db_con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$row['default'] = pg_fetch_assoc($sel);
					$row['default']['reason'] = "";
					break;

				// 設定無し
				case "3":
					$row['default'] = array("pattern" => "", "reason" => "");
					break;
			}
		}

		return $row;
	}

	/*************************************************************************/
	// 勤務シフトパターンデータ取得
	/*************************************************************************/
	function duty_shift_pattern_table()
	{
		//SQL実行
		$sql = <<<_SQL_END_
SELECT
a.*
FROM atdptn a,duty_shift_group g
_SQL_END_;
		$cond = <<<_COND_END_
WHERE g.pattern_id=a.group_id
AND g.group_id='{$this->group_id}'
ORDER BY atdptn_id
_COND_END_;
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//fetch
		$atdptn_table = array();
		while ( $row = pg_fetch_assoc($sel) ) {
			$atdptn_table[ $row["atdptn_id"] ] = $row;
		}

		return $atdptn_table;
	}

	/*************************************************************************/
	// 勤務条件・勤務日チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_duty_day($date, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];

		//祝日
		if ( $date["week2"] == 7 ) {
			$key = "duty_hol_day_flg";
		}
		//月
		elseif ( $date["week"] == 1 ) {
			$key = "duty_mon_day_flg";
		}
		//火
		elseif ( $date["week"] == 2 ) {
			$key = "duty_tue_day_flg";
		}
		//水
		elseif ( $date["week"] == 3 ) {
			$key = "duty_wed_day_flg";
		}
		//木
		elseif ( $date["week"] == 4 ) {
			$key = "duty_thurs_day_flg";
		}
		//金
		elseif ( $date["week"] == 5 ) {
			$key = "duty_fri_day_flg";
		}
		//土
		elseif ( $date["week"] == 6 ) {
			$key = "duty_sat_day_flg";
		}
		//日
		else {
			$key = "duty_sun_day_flg";
		}

		//チェック
		if ( ! array_key_exists($key,$staff) ) {
			return true;
		}
		elseif ( $staff[$key] == 1 ) {
			return true;
		}
		return false;
	}

	/*************************************************************************/
	// 勤務条件・夜勤チェック
	// 夜勤の設定を見て夜勤可不可を判定するのは無しになりました。(2011.12.10)
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_night_duty($ptn_id, $emp_id)
	{
//		$staff = $this->staff_table[$emp_id];
//		$ptn_name = $this->atdptn_table[$ptn_id]["atdptn_nm"];
//		$workday_count = $this->atdptn_table[$ptn_id]["workday_count"];
//
//		// 夜勤明け対応、換算日数が0の場合はチェック不要
//		if ($ptn_id != "10" and $workday_count == 0) {
//			return true;
//		}
//
//		// 夜勤なのに、フラグが立っていなかったらNG
//		if ( substr_count($ptn_name,"夜") > 0 ) {
//			if ( empty($staff["night_duty_flg"]) ) {
//				return false;
//			}
//		}
//
//		// 夜専なのに、夜勤以外のパターンだったらNG
//		if ( $staff["night_duty_flg"] == 2 ) {
//			if ( substr_count($ptn_name,"夜") <= 0 ) {
//				return false;
//			}
//		}

		return true;
	}

	/*************************************************************************/
	// 勤務条件・勤務可能なシフトチェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_duty_shift($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];

		//祝日
		if ( $date["week2"] == 7 ) {
			if ( ! array_key_exists(4, $staff["emp_atdptn"]) ) {
				return true;
			}
			if ( ! array_key_exists($ptn_id, $staff["emp_atdptn"][4]) ) {
				return true;
			}
			if ( $staff["emp_atdptn"][4][$ptn_id] == 1 ) {
				return true;
			}
		}

		//土
		if ( $date["week"] == 6 ) {
			if ( ! array_key_exists(2, $staff["emp_atdptn"]) ) {
				return true;
			}
			if ( ! array_key_exists($ptn_id, $staff["emp_atdptn"][2]) ) {
				return true;
			}
			if ( $staff["emp_atdptn"][2][$ptn_id] == 1 ) {
				return true;
			}
		}
		//日
		elseif ( $date["week"] == 0 ) {
			if ( ! array_key_exists(3, $staff["emp_atdptn"]) ) {
				return true;
			}
			if ( ! array_key_exists($ptn_id, $staff["emp_atdptn"][3]) ) {
				return true;
			}
			if ( $staff["emp_atdptn"][3][$ptn_id] == 1 ) {
				return true;
			}
		}
		//平日
		else {
			if ( ! array_key_exists(1, $staff["emp_atdptn"]) ) {
				return true;
			}
			if ( ! array_key_exists($ptn_id, $staff["emp_atdptn"][1]) ) {
				return true;
			}
			if ( $staff["emp_atdptn"][1][$ptn_id] == 1 ) {
				return true;
			}
		}
		return false;
	}

	/*************************************************************************/
	// 禁止条件・前日との関係チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_ng_prev_day($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$prev_date = $staff["atdbk"][$date["date"]]["prev"];
		$prev_ptn = $staff["atdbk"][$prev_date];

		//休暇なら24
		$reason = ($ptn_id == "10") ? REASON_IS_PUBLIC : "";

		//前日のパターンに一致する組み合わせがある
		if ( $this->ngptn_obj->is_prev_day($ptn_id, $reason, $prev_ptn["pattern"], $prev_ptn["reason"]) ) {
			return false;
		}
		return true;
	}

	/*************************************************************************/
	// 禁止条件・翌日との関係チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_ng_next_day($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$next_date = $staff["atdbk"][$date["date"]]["next"];
		$next_ptn = $staff["atdbk"][$next_date];

		//休暇なら24
		$reason = ($ptn_id == "10") ? REASON_IS_PUBLIC : "";

		//翌日のパターンに一致する組み合わせがある
		if ( $this->ngptn_obj->is_next_day($ptn_id, $reason, $next_ptn["pattern"], $next_ptn["reason"]) ) {
			return false;
		}
		return true;
	}

	/*************************************************************************/
	// 禁止条件・連続勤務日数上限チェック
	// @param	$date		日付データ
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_limit_day($date, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$limit_day = $this->group_table["duty_upper_limit_day"];
		$prev = $date["prev"];
		$next = $date["next"];

		//limit_dayが設定されていなければOK
		if ( empty($limit_day) or $limit_day <= 0 ) {
			return true;
		}

		// チェック日が休暇ならOK
		if ($staff["atdbk"][$date["date"]]["pattern"] == "10") {
			return true;
		}
		// チェック日が換算日数が0ならOK
		if ((!empty($staff["atdbk"][$date["date"]]["pattern"])) && ($this->atdptn_table[$staff["atdbk"][$date["date"]]["pattern"]]["workday_count"] == 0)) {
			return true;
		}

		//連続勤務日数の数だけ進んでパターンを調べる
		$prev_limit_day = $limit_day;
		for ( $i=0; $i<$limit_day; $i++ ) {
			// 空ならOK
			if ( empty($staff["atdbk"][$next]["pattern"]) ) {
				break;
			}
			// 休暇ならOK
			if ( $staff["atdbk"][$next]["pattern"] == "10" ) {
				break;
			}
			// 換算日数が0ならOK
			if ( $this->atdptn_table[$staff["atdbk"][$next]["pattern"]]["workday_count"] == 0 ) {
				break;
			}
			$next = $staff["atdbk"][$next]["next"];
			$prev_limit_day--;
		}
		if ($prev_limit_day <= 0) {
			return false;
		}

		//連続勤務日数の数だけ遡ってパターンを調べる
		for ( $i=0; $i<$prev_limit_day; $i++ ) {
			// 空ならOK
			if ( empty($staff["atdbk"][$prev]["pattern"]) ) {
				return true;
			}
			// 休暇ならOK
			if ( $staff["atdbk"][$prev]["pattern"] == "10" ) {
				return true;
			}
			// 換算日数が0ならOK
			if ( $this->atdptn_table[$staff["atdbk"][$prev]["pattern"]]["workday_count"] == 0 ) {
				return true;
			}
			$prev = $staff["atdbk"][$prev]["prev"];
		}
		return false;
	}

	/*************************************************************************/
	// 休日設定できる日付を取得
	// @param	$date		日付データ
	// @param	$emp_id		スタッフID
	//
	// @return	休日設定できる日付
	/*************************************************************************/
	function get_holiday($date, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$limit_day = $this->group_table["duty_upper_limit_day"];
		$prev = $staff["atdbk"][$date["date"]]["prev"];
		$next = $staff["atdbk"][$date["date"]]["next"];

		//limit_dayが設定されていなければOK
		if ( empty($limit_day) or $limit_day <= 0 ) {
			return $date;
		}

		//連続勤務日数の数だけ進んでパターンを調べる
		$prev_limit_day = $limit_day;
		for ( $i=0; $i<=$limit_day; $i++ ) {
			// 空ならOK
			if ( empty($staff["atdbk"][$next]["pattern"]) ) {
				break;
			}
			// 休暇ならOK
			if ( $staff["atdbk"][$next]["pattern"] == "10" ) {
				break;
			}
			// 換算日数が0ならOK
			if ( $this->atdptn_table[$staff["atdbk"][$next]["pattern"]]["workday_count"] == 0 ) {
				break;
			}
			$next = $staff["atdbk"][$next]["next"];
			$prev_limit_day--;
		}
		if ($prev_limit_day <= 0) {
			return $date;
		}

		//連続勤務日数の数だけ遡ってパターンを調べる
		$fillers = array();
		for ( $i=0; $i<$prev_limit_day; $i++ ) {
			// fillerならOK
			if ( $staff["atdbk"][$prev]["filler"] == 1 ) {
				$fillers[] = $prev;
			}
			$prev = $staff["atdbk"][$prev]["prev"];
		}
		if (!empty($fillers)) {
			shuffle($fillers);
			return $fillers[0] ;
		}
		return $date;
	}

	/*************************************************************************/
	// 禁止条件・連続勤務シフト上限チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_limit_shift($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$limit_array = $this->group_table["upper_limit"];

		foreach ( $limit_array as $limit_pattern => $row ) {
			//$limit_patternと異なるなら次へ
			if ( $ptn_id != $limit_pattern ) {
				continue;
			}

			//連続勤務シフト数の数だけ進んでパターンを調べる
			$next = $staff["atdbk"][$date["date"]]["next"];
			$prev_limit_day = $row["upper_limit_day"];
			for ( $i=0; $i<=$row["upper_limit_day"]; $i++ ) {
				if ( $staff["atdbk"][$next]["pattern"] != $limit_pattern ) {
					break;
				}
				$next = $staff["atdbk"][$next]["next"];
				$prev_limit_day--;
			}
			if ($prev_limit_day <= 0) {
				return false;
			}

			//連続勤務シフト数の数だけ遡ってパターンを調べる
			$prev = $staff["atdbk"][$date["date"]]["prev"];
			for ( $i=0; $i<$prev_limit_day; $i++ ) {
				if ( $staff["atdbk"][$prev]["pattern"] != $limit_pattern ) {
					return true;
				}
				$prev = $staff["atdbk"][$prev]["prev"];
			}
			return false;
		}
		return true;
	}

	/*************************************************************************/
	// 禁止条件・勤務シフト間隔チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_interval_shift($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$interval_array = $this->group_table["interval"];

		foreach ( $interval_array as $interval_pattern => $row ) {
			//$interval_patternと異なるなら次へ
			if ( $ptn_id != $interval_pattern ) {
				continue;
			}

			//勤務シフト間隔数の数だけ進んでパターンを調べる
			$next = $staff["atdbk"][$date["date"]]["next"];
			for ( $i=0; $i<$row["interval_day"]; $i++ ) {
				if ( $staff["atdbk"][$next]["pattern"] == $interval_pattern ) {
					return false;
				}
				$next = $staff["atdbk"][$next]["next"];
			}

			//勤務シフト間隔数の数だけ遡ってパターンを調べる
			$prev = $staff["atdbk"][$date["date"]]["prev"];
			for ( $i=0; $i<$row["interval_day"]; $i++ ) {
				if ( $staff["atdbk"][$prev]["pattern"] == $interval_pattern ) {
					return false;
				}
				$prev = $staff["atdbk"][$prev]["prev"];
			}
			return true;
		}
		return true;
	}

	/*************************************************************************/
	// 組み合わせ指定処理
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function sequence($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$next_date = $staff["atdbk"][$date["date"]]["next"];
		$next_date_ptn = $staff["atdbk"][$next_date]["pattern"];

		//指定パターンを取得
		$reason = ($ptn_id == "10") ? REASON_IS_PUBLIC : "";
		$ok_ptn = $this->okptn_obj->get_next_pattern($ptn_id, $reason);

		//指定パターンがなければOK
		if ( empty($ok_ptn) ) {
			return true;
		}

		//翌日パターンが指定パターンと一致しないとNG
		if ((!empty($next_date_ptn)) && ($next_date_ptn != $ok_ptn["pattern"])) {
			return false;
		}

		// 勤務日数上限チェック
		if ( !$this->check_limit_day($staff["atdbk"][$next_date], $emp_id) ) {
			return false;
		}

		// 勤務条件・勤務日
		if ( !$this->check_duty_day($staff["atdbk"][$next_date], $emp_id) ) {
			return false;
		}

		// 勤務条件・夜勤
		if ( ! $this->check_night_duty($next_ptn["pattern"], $emp_id) ) {
			return false;
		}
		// 勤務条件・勤務可能なシフト
		if ( ! $this->check_duty_shift($staff["atdbk"][$next_date], $next_ptn["pattern"], $emp_id) ) {
			return false;
		}
		// 禁止条件・連続勤務シフト上限
		if ( ! $this->check_limit_shift($staff["atdbk"][$next_date], $next_ptn["pattern"], $emp_id) ) {
			return false;
		}
		// 禁止条件・勤務シフト間隔
		if ( ! $this->check_interval_shift($staff["atdbk"][$next_date], $next_ptn["pattern"], $emp_id) ) {
			return false;
		}
		// 禁止条件・組み合わせ禁止(前日)
		if ( ! $this->check_ng_prev_day($staff["atdbk"][$next_date], $next_ptn["pattern"], $emp_id) ) {
			return false;
		}
		// 禁止条件・組み合わせ禁止(翌日)
		if ( ! $this->check_ng_next_day($staff["atdbk"][$next_date], $next_ptn["pattern"], $emp_id) ) {
			return false;
		}

		// 空なら割り当て
		if ( empty($staff["atdbk"][$next_date]["pattern"]) ) {
			$this->staff_table[$emp_id]["atdbk"][$next_date]["pattern"] = $ok_ptn["pattern"];
			$this->staff_table[$emp_id]["atdbk"][$next_date]["reason"] = $ok_ptn["reason"];
			$this->staff_table[$emp_id]["atdbk"][$next_date]["autoset"] = 1;
			$this->hindo_obj->plus($staff["atdbk"][$next_date]["week2"], $ok_ptn["pattern"], $emp_id);
//			$this->need_obj->minus($next_date, $ok_ptn["pattern"], $this->staff_table[$emp_id]['job_id'], $this->staff_table[$emp_id]['team_id'], $this->staff_table[$emp_id]['sex'], $this->staff_table[$emp_id]['st_id']);
		}

		// 翌々日チェック
		if ( ! $this->sequence($staff["atdbk"][$next_date], $ok_ptn["pattern"], $emp_id) ) {
			if ( $this->staff_table[$emp_id]["atdbk"][$next_date]["autoset"] == 1 ) {
				$this->staff_table[$emp_id]["atdbk"][$next_date]["pattern"] = "";
				$this->staff_table[$emp_id]["atdbk"][$next_date]["reason"] = "";
				$this->staff_table[$emp_id]["atdbk"][$next_date]["autoset"] = "";
				$this->hindo_obj->minus($staff["atdbk"][$next_date]["week2"], $ok_ptn["pattern"], $emp_id);
//				$this->need_obj->plus($next_date, $ok_ptn["pattern"], $this->staff_table[$emp_id]['job_id'], $this->staff_table[$emp_id]['team_id'], $this->staff_table[$emp_id]['sex'], $this->staff_table[$emp_id]['st_id']);
			}
			return false;
		}
		return true;
	}

	/*************************************************************************/
	// 組み合わせ指定・翌日との関係チェック
	// @param	$date		日付データ
	// @param	$ptn_id		パターンID
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_ok_next_day($date, $ptn_id, $emp_id)
	{
		$staff = $this->staff_table[$emp_id];
		$next_date = $staff["atdbk"][$date["date"]]["next"];
		$next_ptn = $staff["atdbk"][$next_date];

		//指定パターンを取得
		$reason = ($ptn_id == "10") ? REASON_IS_PUBLIC : "";
		$ok_ptn = $this->okptn_obj->get_next_pattern($ptn_id, $reason);

		//指定パターンがなければOK
		if ( empty($ok_ptn) ) {
			return true;
		}

		//翌日パターンが未定なければOK
		if ( empty($next_ptn["pattern"]) ) {
			return true;
		}

		//翌日のパターンに一致する組み合わせがある
		if ( $this->okptn_obj->is_next_day($ptn_id, $reason, $next_ptn["pattern"], $next_ptn["reason"]) ) {
			return true;
		}
		return false;
	}

	/*************************************************************************/
	// 4週8休チェック
	// @param	$date		基準日
	// @param	$emp_id		スタッフID
	//
	// @return	true: OK  false:NG
	/*************************************************************************/
	function check_4week_day($date, $emp_id, $hol_ptn)
	{
		$staff = $this->staff_table[$emp_id];
		$prev = $staff["atdbk"][$date]["prev"];
		$next = $staff["atdbk"][$date]["date"];

		//28日進んでパターンを調べる
		$next_cnt = 0;
		$next_max = 8;
		for ( $i=1; $i<=28; $i++ ) {
			// 祝日(土日以外)
			if (($staff["atdbk"][$next]["week2"] == 7) && ($staff["atdbk"][$next]["week"] != 0) && ($staff["atdbk"][$next]["week"] != 6)) {
				$next_max++;
			}

			// 空ならOK
			if ( empty($staff["atdbk"][$next]["pattern"]) ) {
				$next_cnt++;
			}

			// 休暇ならOK
			if (($staff["atdbk"][$next]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$next]["reason"] == REASON_IS_NONE)) {
				$next_cnt++;
			}
			// 休暇（法定）ならOK
			if (($staff["atdbk"][$next]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$next]["reason"] == REASON_IS_LEGAL)) {
				$next_cnt++;
			}
			// 休暇（所定）ならOK
			if (($staff["atdbk"][$next]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$next]["reason"] == REASON_IS_STATUTORY)) {
				$next_cnt++;
			}
			// 休暇（公休）ならOK
			if (($staff["atdbk"][$next]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$next]["reason"] == REASON_IS_PUBLIC)) {
				$next_cnt++;
			}
			// 指定パターンならOK
			foreach ($hol_ptn as $hol) {
				if (($staff["atdbk"][$next]["pattern"] == $hol["ptn"]) && ($staff["atdbk"][$next]["reason"] == $hol["reason"])) {
					$next_cnt++;
				}
			}
			$next = $staff["atdbk"][$next]["next"];
		}

		//28日遡ってパターンを調べる
		$prev_cnt = 0;
		$prev_max = 8;
		for ( $i=1; $i<=28; $i++ ) {
			// 祝日(土日以外)
			if (($staff["atdbk"][$prev]["week2"] == 7) && ($staff["atdbk"][$prev]["week"] != 0) && ($staff["atdbk"][$prev]["week"] != 6)) {
				$prev_max++;
			}

			// 空ならOK
			if ( empty($staff["atdbk"][$prev]["pattern"]) ) {
				$prev_cnt++;
			}

			// 休暇ならOK
			if (($staff["atdbk"][$prev]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$prev]["reason"] == REASON_IS_NONE)) {
				$prev_cnt++;
			}
			// 休暇（法定）ならOK
			if (($staff["atdbk"][$prev]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$prev]["reason"] == REASON_IS_LEGAL)) {
				$prev_cnt++;
			}
			// 休暇（所定）ならOK
			if (($staff["atdbk"][$prev]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$prev]["reason"] == REASON_IS_STATUTORY)) {
				$prev_cnt++;
			}
			// 休暇（公休）ならOK
			if (($staff["atdbk"][$prev]["pattern"] == PATTERN_IS_VACATION) && ($staff["atdbk"][$prev]["reason"] == REASON_IS_PUBLIC)) {
				$prev_cnt++;
			}
			// 指定パターンならOK
			foreach ($hol_ptn as $hol) {
				if (($staff["atdbk"][$prev]["pattern"] == $hol["ptn"]) && ($staff["atdbk"][$prev]["reason"] == $hol["reason"])) {
					$prev_cnt++;
				}
			}
			$prev = $staff["atdbk"][$prev]["prev"];
		}

		$err = array();
		if ($next_cnt < $next_max) {
			$err[] = array("date" => $staff["atdbk"][$date]["date"], "cnt" => $next_cnt, "max" => $next_max);
		}
		if ($prev_cnt < $prev_max) {
			$err[] = array("date" => $staff["atdbk"][$prev]["next"], "cnt" => $prev_cnt, "max" => $prev_max);
		}

		if (!empty($err)) {
			return array("status" => false, "err" => $err);
		}
		return array("status" => true);
	}

	/*************************************************************************/
	// 日付をタイムスタンプに変換
	/*************************************************************************/
	function to_timestamp($yyyymmdd) {
		$y = substr($yyyymmdd, 0, 4);
		$m = substr($yyyymmdd, 4, 2);
		$d = substr($yyyymmdd, 6, 2);
		return mktime(0, 0, 0, $m, $d, $y);
	}

	/*************************************************************************/
	// 公休日自動割り振り処理、職員設定のチームを得る
	/*************************************************************************/
	function get_emp_teamlist() {
		$get_list = array();
		$sql = "SELECT  emp_id,team_id   FROM duty_shift_staff ";
		$cond = "WHERE group_id='{$this->group_id}' ORDER BY no ";
		$sel = select_from_table($this->_db_con, $sql, $cond , $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group_interval -->");
			exit;
		}
		$fcnt = pg_numrows($sel);
		for( $c=0 ; $c < $fcnt ; $c++ ) {
			$wk_emp_id = pg_result($sel, $c, "emp_id");
			$get_list[$wk_emp_id] = pg_result($sel, $c, "team_id");
			if ( $get_list[$wk_emp_id] == "" ) {
				$get_list[$wk_emp_id] = 0;
			}
		}
		return $get_list;
	}
	/*************************************************************************/
	// 勤務シフト上限チェック・１ヶ月内の割当上限日数
	/*************************************************************************/
	function check_shift_limit($emp_id,  $ptn , $day_cnt ) {
		$return_sts = true;
		if ($this->continue_check[$emp_id][$ptn]['limit_day_count'] != '' ) { /* 上限値が未登録のときは計算しない */
 			if( $this->continue_check[$emp_id][$ptn]['count'] >= $this->continue_check[$emp_id][$ptn]['limit_day_count'] ) { // 上限に達した
				$return_sts = false;
			}else{
				if( $this->continue_check[$emp_id]['day_cnt_check'] != $day_cnt ) {// && 同日に2回以上呼ばれたときも計算しない
					$this->continue_check[$emp_id][$ptn]['count']++;
				}
			}
		}
		$this->continue_check[$emp_id]['day_cnt_check'] = $day_cnt;
		return $return_sts;
	}
	// 勤務シフト上限チェック・初期化処理
	function check_shift_limit_set($emp_id, $limit_ptn_array) {
		foreach($limit_ptn_array as $limit_atd_id=>$limit_day_count) {
			$this->continue_check[$emp_id][$limit_atd_id]['count']=0;
			$this->continue_check[$emp_id][$limit_atd_id]['limit_day_count']=$limit_day_count;
		}
	}
	/*************************************************************************/
	// 職員相性設定チェック、○だったら同一シフトを割り当て×なら除外
	//
	// ○の時は相手方を設定して処理を継続 20140127
	/*************************************************************************/
	function check_pair_set($date , $emp_id ,  $auto="", $filler="" ) {

		foreach($this->staff_table as $wk_emp_id=>$shift) {
			if ( $wk_emp_id == $emp_id ) {
				continue;
			}

			$pattern = $shift["atdbk"][$date]["pattern"];
			$reason  = $shift["atdbk"][$date]["reason"];

			$return_sts = true;

			if ( $pattern != "" ) {

				$compat_array = $shift["pair_compati"];
																			// 相性設定に勤務シフトを加味する 201401 START
//				$compat = $compat_array[$wk_emp_id][$emp_id];
				$compat = $compat_array[$wk_emp_id][$emp_id][$pattern];		// 相性を取得する（シフトパターンの指定有り）

				if($compat == "") {											// パターン指定で取得できない場合
					$compat = $compat_array[$wk_emp_id][$emp_id]['0'];		// 相性を取得する（シフトパターン指定なし）
				}
																			// 相性設定に勤務シフトを加味する END


				if ( $compat == '×' && $pattern != 10 ){					// 相性×のときは候補から外す。但し相手が休暇の場合を除く
					// 相性×の時、クリアしない START 20140127
//					if( $filler == 1 ){										// デフォルトのシフトを割り当てる処理のとき
//						$this->staff_table[$emp_id]["atdbk"][$date]["pattern"] = '';
//						$this->staff_table[$emp_id]["atdbk"][$date]["reason"]  = '';
//						$this->staff_table[$emp_id]["atdbk"][$date]["autoset"] = $auto;
//						$this->staff_table[$emp_id]["atdbk"][$date]["filler"]  = $filler;
//						$this->hindo_obj->plus($date["week2"], $pattern, $emp_id);
//					}
					// 相性×の時、クリアしない END
					$return_sts = false;
					break;
				}
				if ( $compat == '○' ){										// 相性○のときは同一のパターンを割り当てる
					$this->staff_table[$emp_id]["atdbk"][$date]["pattern"] = $pattern;
					$this->staff_table[$emp_id]["atdbk"][$date]["reason"]  = $reason;
					$this->staff_table[$emp_id]["atdbk"][$date]["autoset"] = $auto;
					$this->staff_table[$emp_id]["atdbk"][$date]["filler"]  = $filler;
					$this->hindo_obj->plus($date["week2"], $pattern, $emp_id);
//					$return_sts = false;
					$return_sts = true; // ○の時は相手方を設定して処理を継続 20140127
					break;
				}
			}
		}

		return $return_sts; // 相性未設定のときは通常処理
	}

	/*************************************************************************/
	/* 公休日自動割り振り処理、勤務シフト希望を得る
	/*************************************************************************/
    //取得範囲の条件には開始日終了日を使用する(月またがり対応) 20130319
    function shift_individual_get( $duty_y ,$duty_m, $start_date, $end_date ) {
		$get_list = array();
		// yyyyとm1桁をyyyymmに編集する
		//$month = sprintf("%02d",$duty_m);
		//$month = $duty_y.$month;
		//$month = substr($month,0,6);

		$sql = "SELECT duty_date,atdptn_ptn_id,reason  FROM duty_shift_plan_individual ";
		foreach($this->staff_table as $wk_emp_id=>$id) {
			//$cond = "WHERE emp_id='{$wk_emp_id}' AND duty_date LIKE'{$month}%' ";
            $cond = "WHERE emp_id='{$wk_emp_id}' AND duty_date >= '{$start_date}' AND duty_date <= '{$end_date}' ";
            $sel = select_from_table($this->_db_con, $sql, $cond , $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				echo("<!-- FROM duty_shift_group_interval -->");
				exit;
			}
			$fcnt = pg_numrows($sel);
			for($c=0; $c<$fcnt; $c++) {
				$day = pg_result($sel, $c, "duty_date");
				$get_list[$wk_emp_id][$day]["atdptn_ptn_id"] = pg_result($sel, $c, "atdptn_ptn_id");
				$get_list[$wk_emp_id][$day]["reason"] = pg_result($sel, $c, "reason");
			}
		}
		return $get_list;
	}

	/*************************************************************************/
	/* 公休日自動割り振り処理、休暇自動割当設定取得
	/*************************************************************************/
	function auto_vac_set_get($date_table) {
		$get_list = array();
		$sql = "SELECT * FROM duty_shift_vacation_limit_day WHERE group_id='{$this->group_id}'";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			echo("<!-- FROM duty_shift_group_interval -->");
			exit;
		}
		$get_list[1] = pg_result($sel, 0, "holiday_1");
		$get_list[2] = pg_result($sel, 0, "holiday_2");
		$get_list[3] = pg_result($sel, 0, "holiday_3");
		$get_list[4] = pg_result($sel, 0, "holiday_4");
		$get_list[5] = pg_result($sel, 0, "holiday_5");
		$get_list[6] = pg_result($sel, 0, "holiday_6");
		$get_list[0] = pg_result($sel, 0, "holiday_7");
		$get_list[7] = pg_result($sel, 0, "holiday_8");
		$get_list["force_hol_set"] = pg_result($sel, 0, "force_hol_set");
		foreach ( $date_table as $date => $date_row ) {
			$week = $date_row["week"];
			if( $date_row["week2"] == 7 ) {
				$week = 7;
			}
			$this->auto_pvac_date_limit[$date] = $get_list[$week]; // 各日の同時休暇取得者、上限人数
		}
		return $get_list;
	}

	//*********************************************************************************************
	// 公休日自動割り振り処理、月別公休日数取得。調整日を含む。
	//*********************************************************************************************
	function month_vacation_count($date_table,$year,$month) {

		$topday = sprintf("%04d%02d01",$year,$month);			// 朔日
		$endday = date("Ymd", mktime(0, 0, 0, $month+1, 0, $year));	// 月末
		$auto_endday=end($date_table);
		$auto_topday=reset($date_table);
		$auto_endday=$auto_endday['date'];
		$auto_topday=$auto_topday['date'];
		if ($topday == $auto_topday && $endday == $auto_endday) {	// 処理日が１日〜月末の公休日を得る。timecard_legal_hol_cnt.phpより流用
			//タイムカード情報の取得
			$timecard_bean = new timecard_bean();
			$timecard_bean->select($this->_db_con, $this->file_name);
			//年設定
			$default_start_year = 2006;
			$cur_year = date("Y");
			//年一覧開始年
			if ($start_year == "") {
				//当年を元に開始年を求める
				$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
			}
			$end_year = $start_year + 6;
			$year = $cur_year;

			//公休数取得 月毎、属性毎の配列
			$arr_legal_hol_cnt = get_legal_hol_cnt($this->_db_con, $this->file_name, $year, $timecard_bean->closing, $timecard_bean->closing_month_flg);
			$arr_holwk = get_timecard_holwk_day($this->_db_con, $this->file_name);
			$arr_data = array();

			for ($i=0; $i<count($arr_holwk); $i++) {
				//属性別の件数を計算で求めるため、フラグを1,0に変換
				$wk_legal_flg = ($arr_holwk[$i]["legal_holiday"] == "t") ? 1 : 0;
				$wk_national_flg = ($arr_holwk[$i]["national_holiday"] == "t") ? 1 : 0;
				$wk_newyear_flg = ($arr_holwk[$i]["newyear_holiday"] == "t") ? 1 : 0;
				$wk_prescribed_flg = ($arr_holwk[$i]["prescribed_holiday"] == "t") ? 1 : 0;

				//月データ
				for ($j=0; $j<12; $j++) {
					$arr_data[$i][$j] = $arr_legal_hol_cnt[$j]["4"] * $wk_legal_flg +
						$arr_legal_hol_cnt[$j]["5"] * $wk_prescribed_flg +
						$arr_legal_hol_cnt[$j]["6"] * $wk_national_flg +
						$arr_legal_hol_cnt[$j]["7"] * $wk_newyear_flg;
				}
			}

			$public_vac_month_count = $arr_data[0][$month - 1]; // 当月公休日数

			// 当月公休日数に調整日数を加減する
			$arr_group_data = get_legal_hol_group($this->_db_con, $this->file_name, $year, "");

			$data_count=count($arr_group_data);
			for($idx=0 ; $idx<$data_count ; $idx++) { // すべての勤務パターングループのデータが来ているので $idxで回転させてpattern_idを当て欲しいグループを得る
				if( $this->group_table["pattern_id"] == $arr_group_data[$idx]["group_id"] ) {
					$wk1 = $arr_group_data[$idx]["sign_{$month}"]; // <-- 1のときは +、2のときは-
					$wk2 = $arr_group_data[$idx]["mon_{$month}"];  // 調整日数。プラスマイナスで調整する
					switch ($wk1) {
					case 1:	$public_vac_month_count += $wk2;
						break;
					case 2:	$public_vac_month_count -= $wk2;
						break;
					default:;

					}
					break; // 1グループしか取らないのでヒットしたら回転を止める
				}
			}
		}else{	// 処理開始日が１日以外または月末日以外。土日祝を数える。公休調整日は含まれない
			$public_vac_month_count = 0;
			foreach ( $date_table as $date => $date_row ) {
				if( $date_row["week"] == 6 || $date_row["week"] == 0 ) { // 土曜、日曜
					$public_vac_month_count++;
				}else if ($date_row["week2"] == 7) { // 祝日（月〜金）
					$public_vac_month_count++;
				}
			}
		}
		return $public_vac_month_count;
	}

	/*************************************************************************/
	// 公休日自動割り振り処理、公休日割り当て。1日〜月末
	/*************************************************************************/
	function auto_pvac_allocate(
	     $wk_emp_id,		// 職員id
	     $part_day_element,		// 4パートが持つ日。例えば2012年5月の第0パートは$part_day_element[0]=array(0=>20120501,1=>20120502,.....,7=>20120508);
	     $public_vac_day,		// 月間公休日数、カレンダー上の所定休日+法定休日+祝日の合計+調整日数
	     $part_vac_unit,		// シフト設定期間を4分割した分割パート毎の職員毎の休暇残日数
	     $emp_pvac_cnt_unit,	// 職員毎の休暇日数取得カウンタ
	     $emp_team_list,		// 職員設定のチームが設定されているリスト
	     $pattern_is_vacation,	// '10'が設定されている。勤務シフトパターン「休暇」
	     $reason_is_public,		// '24'が設定されている。事由「公休」
	     $not_day_list,		// 休暇を設定できない日のリスト
	     $fp,			// デバッグ用
	     $ngpart,			// 休暇割り当てができないパート
	     $auto_ptn_id		// 「希望以外をクリアーして割当」のチェック。チェックがあるときは'1'
	    ) {
	    $return_array = array(); // 戻り値用
	    $return_array["status"] = "";
	    $return_array["part"] = "";
	    $return_array["part sts"] = "";

	    $inner_part_cnt = 0;
	    $part_full_check = $this->auto_pvac_emp_info[$wk_emp_id]['partfull'][0]
				 + $this->auto_pvac_emp_info[$wk_emp_id]['partfull'][1]
				 + $this->auto_pvac_emp_info[$wk_emp_id]['partfull'][2]
				 + $this->auto_pvac_emp_info[$wk_emp_id]['partfull'][3];
	    if( $emp_pvac_cnt_unit < $public_vac_day || $part_full_check == 4 ) { // 公休残あり、または、割り当てはいっぱいではなければ処理開始。

		$temp_array = array();
		$set_date_list = array(); // 公休にできる日のリスト[0][1][2][3]
		$need_table = $this->need_obj->get_need_table(); // 当日の「必要人数」の勤務シフトを取得

		// 公休に設定できない日を除外して、設定できる日のリストを作成する
		for($current_part = 0 ; $current_part < 4 ; $current_part++) {
			if ( $part_vac_unit[$current_part] < 0 ) { // || $ngpart == $current_part ) { // 公休日数が無いまたは割当できないパート
				$set_date_list[$current_part] = ""; // 設定できる日は無い
				continue;
			}
			$temp_array = $part_day_element[$current_part];
			$count = 0;
			foreach($temp_array as $part_num=>$date) {
				// すでに勤務シフトが設定されている
				if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] != "" ) {
					continue;
				}
				// 設定できない日のリストがある
				if( $not_day_list != "" ) {
					for($ndc=0 ; $ndc<count($not_day_list); $ndc++) {
						if( $date == $not_day_list[$ndc] ) {
							continue ;
						}
					}
				}
				//「必要人数」の勤務シフト設定がある ＆「希望以外をクリアーして割当」のチェックが無いとき
/*
*/
				// 公休日に設定できる日のリストを生成
				$set_date_list[$current_part][$count] = $date;
				$count++;
			}
		}
		// 設定できる日のリストが出来たので公休残日数を調整する
		$part_vac_unit = $this->part_vac_reset($part_vac_unit,$set_date_list,$fp);

		// 公休日自動割り当て処理
		$number = array(0,1,2,3);// 4分割したパートをランダムに選ぶ準備
		shuffle($number);	 // ランダムに並べ替える
		for($c = 0 ; $c < 4 ; $c++) {
			$current_part = $number[$c];// 4分割したパートをランダムに順に選ぶ
			// 4分割パート内から重複しないよう任意の n 日を得て公休日にする
			$temp_array = $set_date_list[$current_part]; // 任意の n 日を得る準備
			shuffle($temp_array); // ランダムに並べ替える
			$part_day_counter=0;  // 4分割したパート内の日数カウンタ
			// ランダムに並べ替えたら先頭から割り当て日数分だけ取得して公休日にする（ランダムに公休日を選ぶ）
			$set_day_count = $part_vac_unit[$current_part]; // 公休日にする日数
			while( $part_day_counter <  $set_day_count ) {

				$shuffle_date = $temp_array[$part_day_counter] ; // この日に休暇を割り当てる

				if ( $shuffle_date == "" ) { // 持分の日付が尽きた(エラー)
					$return_array["status"] = "full";
					$return_array["part"] = $current_part; // 割当できなかったパート
					$this->auto_pvac_emp_info[$wk_emp_id]['partfull'][$current_part] = 1; // このパートはもう割当てられない
					break;
				}

				//-----------------------------------------------------------
				// 休暇を割り当てる
				//-----------------------------------------------------------
				$return_array = $this->set_pattern2($wk_emp_id, $shuffle_date, $part_vac_unit[$current_part], $emp_pvac_cnt_unit,
															PATTERN_IS_VACATION, REASON_IS_PUBLIC, "auto", $fp);
				$part_day_counter++;

				// 公休日残数減らす。公休日取得日加算
				$part_vac_unit[$current_part] = $return_array["sub"];
				$emp_pvac_cnt_unit = $return_array["add"];
				if ( $part_vac_unit[$current_part] == 0 ) { break; } // このパートの公休残日数が無くなったので次のパートへ
			}
		}
	        $return_array["part_vac"] = $part_vac_unit;
	        $return_array["emp_pvac_cnt_unit"] = $emp_pvac_cnt_unit;
	   }
	   return $return_array;
	}
	/*****************************************************************************/
	// 公休日自動割り振り処理、公休日割り当て解除。自動設定した公休を未設定へ戻す
	// どのパートで何日未設定にしたかを記録する
	/*****************************************************************************/
	function auto_pvac_deallocate(  $wk_emp_id  , $auto_part_calendar ,  $part_vac_unit , $emp_pvac_cnt_unit ,$pattern_is_vacation , $reason_is_public ,$fp) {
		$realloc_count=0;
		$return_array=array();
		foreach($auto_part_calendar as $cday=>$current_part) {
			if( $this->staff_table[$wk_emp_id]['atdbk'][$cday]['pattern'] == $pattern_is_vacation
			 && $this->staff_table[$wk_emp_id]['atdbk'][$cday]['reason'] == $reason_is_public
			 && $this->staff_table[$wk_emp_id]['atdbk'][$cday]['autotype'] == "auto" ) {

				$return_array = $this->unset_pattern2($wk_emp_id, $cday, $part_vac_unit[$current_part], $emp_pvac_cnt_unit, $fp);
				// 公休日残数とカウンタを元に戻す
				$part_vac_unit[$current_part] = $return_array["add"];
				$emp_pvac_cnt_unit = $return_array["sub"];
				$realloc_count ++ ; // 解除した日数合計
			}
		}
		$return_array["part_vac"] = $part_vac_unit;
		$return_array["emp_pvac_cnt_unit"] = $emp_pvac_cnt_unit;
		$return_array["realloc_count"] = $realloc_count;

		return $return_array;
	}
	//*************************************************************************/
	// 公休日自動割り振り処理、内部処理用
	//*************************************************************************/
	function over_check($part_vac_unit,$set_date_list) {
		$ret = false;
		if ( $part_vac_unit[0] > count($set_date_list[0])
		 &&  $part_vac_unit[1] > count($set_date_list[1])
		 &&  $part_vac_unit[2] > count($set_date_list[2])
		 &&  $part_vac_unit[3] > count($set_date_list[3]) ) {
			$ret = true ;
		}
		return $ret;
	}
	// 設定できる日数と公休残日数を比較して、設定できる日数のほうが少ないときは公休残日数を振りなおす
	// 但し、完全に振りなおすことはできない。残日数を余らすことがある。
	function part_vac_reset($part_vac_unit,$set_date_list,$fp) {
		for($current_part=0; $current_part<4; $current_part++) {
			// 公休残日数よりも設定できる日数のほうが少ない
			if ( $part_vac_unit[$current_part] > count($set_date_list[$current_part]) ) {
				// 足りない設定日数（超過する公休日数）
				$over_day = $part_vac_unit[$current_part] - count($set_date_list[$current_part]) ;
				// 他のパートへ割り振る
				for($next_part = 0; $next_part < 4;$next_part++) {
					// 他のパートが公休残日数よりも設定できる日数のほうが多ければ設定
					if ( $next_part != $current_part && $part_vac_unit[$next_part] < count($set_date_list[$next_part]) ) {
						$part_vac_unit[$next_part]++;
						$part_vac_unit[$current_part]--;
					}
				}
			}
		}
		return $part_vac_unit;
	}

	/*************************************************************************/
	// 公休日自動割り振り処理、縦計チェック。同一日上限人数
	/*************************************************************************/
	function auto_pvac_colcheck(
		 $emp_cond_list,			// 職員リスト
		 $date_table,				// 当月カレンダー情報
		 $auto_part_calendar,		// 1日〜月末までの各日が分割したパートのどこに含まれるかを示すカレンダー
		 $part_day_element,			// 月間公休日数、カレンダー上の所定休日+法定休日+祝日の合計+調整日数
		 $public_vac_day,			// 月間公休数。カレンダー上の所定休日+法定休日+祝日の合計+調整日数
		 $auto_vac_setlist,			// 「シフトグループ設定」->「休暇自動割当設定」
		 $part_vac,					// シフト設定期間を4分割したそれぞれの職員毎公休残日数を持つ
		 $emp_pvac_cnt,				// 職員毎の公休日数カウンタ[emp_id]
		 $emp_team_list,			// 職員設定のチームが設定されているリスト
		 $fp,						// デバッグ用
		 $auto_ptn_id				// 「希望以外をクリアーして割当」のチェック。チェックがあるときは'1'
		) {

		$temp_emp_list = array(); // 一時的な職員idリスト
		$c=0;
		foreach($this->staff_table as $wk_emp_id=>$v) {
			if( $emp_cond_list[$wk_emp_id]["duty_form"] == 2 ) { // 非常勤を除く
				continue;
			}
			$temp_emp_list[$c] = $wk_emp_id;
			$c++;
		}

		for($reset=0; $reset<10; $reset++) { // 再配置を10回処理してみる
		  foreach($auto_part_calendar as $date=>$current_part) {// 日付毎
			$day_vac_cnt=0;
			foreach($temp_emp_list as $num=>$wk_emp_id) { // 職員毎
				if ($this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == PATTERN_IS_VACATION ) { // 休暇日に設定されている
					$day_vac_cnt++;
				}
			}

			$week_limit = $auto_vac_setlist[$date_table[$date]["week"]];
			$this->auto_pvac_date_info["day_limit_over"][$date] = 0; // 警告メッセージ、初期値

			if( $day_vac_cnt > $week_limit ) {

				$over_count = $day_vac_cnt - $auto_vac_setlist[$date_table[$date]["week"]]; // 超過人数
				$this->auto_pvac_date_info["day_limit_over"][$date] = "over_count"; // 警告メッセージ
				$realloc_emp_count = 0 ; // 再設定人数
				shuffle($temp_emp_list); // 一時的職員リストをランダムに並べ替える
				foreach($temp_emp_list as $num=>$wk_emp_id) { // 職員毎
					if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern']  == PATTERN_IS_VACATION
					 && $this->staff_table[$wk_emp_id]['atdbk'][$date]['reason']   == REASON_IS_PUBLIC
					 && $this->staff_table[$wk_emp_id]['atdbk'][$date]['autotype'] == "auto" ) {

						if ($auto_vac_setlist["force_hol_set"] == "c" ) { // 土日祝チェックがあるとき
							// 曜日に関係なく"auto"の公休設定済み日数を得る
							$vac_counter = $this->auto_pvac_setcount($wk_emp_id, $date_table, PATTERN_IS_VACATION, REASON_IS_PUBLIC, $fp);
							// 土日祝の"auto"の公休設定済み日数を得る
							$hol_counter = $this->auto_pvac_sshcount2($wk_emp_id, $date_table, PATTERN_IS_VACATION, REASON_IS_PUBLIC, $fp);
							// "auto"の公休全設定日数と土日祝の日数が1日または無い場合はこの職員を一時的職員リストから除外(再割り当ての対象外にする)
							if ( $hol_counter <= 1 && $vac_counter <= 1 ) {
								array_splice($temp_emp_list , $num , 1 );
								continue;
							}
						}
						if( $realloc_emp_count < $over_count ) { // 超過した人数だけ解除する
							// 公休設定を解除
							$return_array = $this->unset_pattern2($wk_emp_id, $date,  $part_vac[$wk_emp_id][$current_part], $emp_pvac_cnt[$wk_emp_id], $fp );
							$part_vac[$wk_emp_id][$current_part] = $return_array["add"];
							$emp_pvac_cnt[$wk_emp_id] = $return_array["sub"];
							$realloc_emp_count++; // 公休日"auto"を再設定する職員数
						}
					}
				}
			}
		}

		// 公休日を再割り当て
		$not_day_list = array();
		$ndc = 0;
		foreach($auto_part_calendar as $date=>$current_part) { // 公休日を割り当ててはいけない日のリスト
			if( strcmp($this->auto_pvac_date_info["day_limit_over"][$date] , "over_count") == 0 ) {
				$not_day_list[$ndc] = $date;
				$ndc++;
			}
		}

		foreach($temp_emp_list as $num=>$wk_emp_id) { // 職員毎
			$return_status = "status" ; // 初期値
			$ngpart="";
			$call_count = 0;
			for ( $call_count=0; $call_count < 4; $call_count++ ) { // 割り当て失敗したらやり直し。4回呼び出して失敗したらあきらめる
				$return_array = $this->auto_pvac_allocate($wk_emp_id, $part_day_element, $public_vac_day, $part_vac[$wk_emp_id], $emp_pvac_cnt[$wk_emp_id],
																$emp_team_list, $not_day_list, $fp ,$ngpart,$auto_ptn_id );
				$part_vac[$wk_emp_id]     = $return_array["part_vac"];
				$emp_pvac_cnt[$wk_emp_id] = $return_array["emp_pvac_cnt_unit"];
				$return_status = $return_array["status"];
				if ( $return_status == "" ) {
					break; // 次の職員
				}
				$call_count++;
				$ngpart = $return_array["part"];
			}
		  }
		  if ( $ndc==0 ) {
		  	break;
		  }
		}
	}

	/****************************************************************************/
	// 公休日自動割り振り処理、同時休暇取得人数の上限人数(各日)を返す(警告処理用)
	/****************************************************************************/
	function auto_pvac_day_limit( $date ) {
		$ret = $this->auto_pvac_date_limit[$date];
		return $ret;
	}
	/*************************************************************************/
	// 割り当て済みパターンの土日祝の日数を数える
	/*************************************************************************/
	function auto_pvac_sshcount($wk_emp_id, $date_table, $_pattern, $fp) { // ssh->sun,sat,hol
		$hol_counter = 0;
		foreach ( $date_table as $date => $date_row ) { // 日付毎
			if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == $_pattern ) {
				if( $date_row["week"] == 0 || $date_row["week"] == 6 || $date_row["week2"] == 7 ) {
					$hol_counter++;
				}
			}
		}
		return $hol_counter;
	}

	/*************************************************************************/
	// 割り当て済みパターン・事由の土日祝の日数を数える
	/*************************************************************************/
	function auto_pvac_sshcount2($wk_emp_id, $date_table, $_pattern, $_reason, $fp) { // ssh->sun,sat,hol
		$hol_counter = 0;
		foreach ( $date_table as $date => $date_row ) { // 日付毎
			if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == $_pattern
			 &&  $this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] == $_reason ) {
				if( $date_row["week"] == 0 || $date_row["week"] == 6 || $date_row["week2"] == 7 ) {
					$hol_counter++;
				}
			}
		}
		return $hol_counter;
	}
	/*************************************************************************/
	// 割り当て済みパターン・事由の日数を数える。曜日関係無い
	/*************************************************************************/
	function auto_pvac_setcount($wk_emp_id, $date_table, $_pattern, $_reason, $fp) {
		$hol_counter = 0;
		foreach ( $date_table as $date => $date_row ) { // 日付毎
			if( $this->staff_table[$wk_emp_id]['atdbk'][$date]['pattern'] == $_pattern
			 &&  $this->staff_table[$wk_emp_id]['atdbk'][$date]['reason'] == $_reason ) {
				$hol_counter++;
			}
		}
		return $hol_counter;
	}
	/*************************************************************************/
	// 勤務シフト設定２
	/*************************************************************************/
	function set_pattern2($_wk_emp_id, $_date, $_sub_count, $_add_count, $_pattern, $_reason, $_type, $fp) {

		$return_array = array();

		// 勤務シフト設定
		$this->staff_table[$_wk_emp_id]['atdbk'][$_date]['pattern']  = $_pattern;
		$this->staff_table[$_wk_emp_id]['atdbk'][$_date]['reason']   = $_reason;
		$this->staff_table[$_wk_emp_id]['atdbk'][$_date]['autotype'] = $_type;
		$this->staff_table[$_wk_emp_id]['atdbk'][$_date]['autoset']  =  "";
		$this->staff_table[$_wk_emp_id]['atdbk'][$_date]['filler']   =  "";
		// カウンタ加減
		$_sub_count--;
		$_add_count++;
		$return_array["sub"] = $_sub_count;
		$return_array["add"] = $_add_count;

		return $return_array;
	}
	/************************************************************************************/
	// 勤務シフト解除。unset_patternメソッドは無いが上記のset_pattern2と合わせて2とする
	// set_pattern2()とは_add_countと_sub_countの位置が入れ替わっているので注意
	/************************************************************************************/
	function unset_pattern2($_wk_emp_id, $_date, $_add_count, $_sub_count, $fp) {
		$return_array = array();
		unset($this->staff_table[$_wk_emp_id]['atdbk'][$_date]['pattern']);
		unset($this->staff_table[$_wk_emp_id]['atdbk'][$_date]['reason']);
		unset($this->staff_table[$_wk_emp_id]['atdbk'][$_date]['autotype']);
		unset($this->staff_table[$_wk_emp_id]['atdbk'][$_date]['autoset']);
		unset($this->staff_table[$_wk_emp_id]['atdbk'][$_date]['filler']);
		$_sub_count--;
		$_add_count++;
		$return_array["sub"] = $_sub_count;
		$return_array["add"] = $_add_count;
		return $return_array;
	}
	/************************************************************************************/
	/* 個人定型勤務条件として、曜日、祝日別の勤務パターンを取得
	/*
	/* 事由コードを追加する 20140124
	/************************************************************************************/
	function get_staff_fixed_atdptn() {

		$return_array = array();

//		$sql  = "SELECT atdptn_1,atdptn_2,atdptn_3,atdptn_4,atdptn_5,atdptn_6,atdptn_0,atdptn_7 FROM duty_shift_staff_fixed_atdptn ";
		$sql = "select ";
		$sql .= "atdptn_1,atdptn_2,atdptn_3,atdptn_4,atdptn_5,atdptn_6,atdptn_0,atdptn_7,";
		$sql .= "reason_1,reason_2,reason_3,reason_4,reason_5,reason_6,reason_0,reason_7 ";
		$sql .= "from duty_shift_staff_fixed_atdptn ";

		foreach($this->staff_table as $wk_emp_id=>$val) {
			$cond = "where emp_id='$wk_emp_id' ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
//			$return_array[$wk_emp_id][1] = pg_result($sel,0,"atdptn_1");
//			$return_array[$wk_emp_id][2] = pg_result($sel,0,"atdptn_2");
//			$return_array[$wk_emp_id][3] = pg_result($sel,0,"atdptn_3");
//			$return_array[$wk_emp_id][4] = pg_result($sel,0,"atdptn_4");
//			$return_array[$wk_emp_id][5] = pg_result($sel,0,"atdptn_5");
//			$return_array[$wk_emp_id][6] = pg_result($sel,0,"atdptn_6");
//			$return_array[$wk_emp_id][0] = pg_result($sel,0,"atdptn_0");
//			$return_array[$wk_emp_id][7] = pg_result($sel,0,"atdptn_7");

																					// 事由コードを追加する 20140124
			$return_array[$wk_emp_id][1]['atdptn'] = pg_result($sel,0,"atdptn_1");	$return_array[$wk_emp_id][1]['reason'] = pg_result($sel,0,"reason_1");
			$return_array[$wk_emp_id][2]['atdptn'] = pg_result($sel,0,"atdptn_2");	$return_array[$wk_emp_id][2]['reason'] = pg_result($sel,0,"reason_2");
			$return_array[$wk_emp_id][3]['atdptn'] = pg_result($sel,0,"atdptn_3");	$return_array[$wk_emp_id][3]['reason'] = pg_result($sel,0,"reason_3");
			$return_array[$wk_emp_id][4]['atdptn'] = pg_result($sel,0,"atdptn_4");	$return_array[$wk_emp_id][4]['reason'] = pg_result($sel,0,"reason_4");
			$return_array[$wk_emp_id][5]['atdptn'] = pg_result($sel,0,"atdptn_5");	$return_array[$wk_emp_id][5]['reason'] = pg_result($sel,0,"reason_5");
			$return_array[$wk_emp_id][6]['atdptn'] = pg_result($sel,0,"atdptn_6");	$return_array[$wk_emp_id][6]['reason'] = pg_result($sel,0,"reason_6");
			$return_array[$wk_emp_id][0]['atdptn'] = pg_result($sel,0,"atdptn_0");	$return_array[$wk_emp_id][0]['reason'] = pg_result($sel,0,"reason_0");
			$return_array[$wk_emp_id][7]['atdptn'] = pg_result($sel,0,"atdptn_7");	$return_array[$wk_emp_id][7]['reason'] = pg_result($sel,0,"reason_7");
		}


		return $return_array;
	}

}
/*
function debug_log($debug) {
	error_log("[".date("Y/m/d H:i:s")."]: $debug\n", 3, "log/autoshift.debug.log");
}
*/

?>
