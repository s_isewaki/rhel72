<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_hiyari_util.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//エンコード
//====================================
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}


//====================================
//ファイル名
//====================================
$filename = 'hiyari_hat.csv';
$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());

// アクセスログ（「内容」項目にダウンロードするファイル名を表示させる）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$filename);
                
//====================================
//CSVデータの取得
//====================================
$hiyari_obj = new hiyari_hiyari_util($con, $fname);
$download_data = $hiyari_obj->get_hiyari_output_csv_data($folder_id);


//====================================
//CSV出力
//====================================
ob_clean();
header("Content-Type: text/csv");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();



pg_close($con);
?>