<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cas_application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

/*
for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "approve_name$i";
	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
}
*/

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>

<?
$fname=$PHP_SELF;

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//sekiai
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");

	exit;
}

// 添付ファイルの確認
if (!is_dir("cas_apply")) {
	mkdir("cas_apply", 0755);
}
if (!is_dir("cas_apply/tmp")) {
	mkdir("cas_apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "cas_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
//sekiai
//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") { 
	$ext = ".php";
	$savexmlfilename = "cas_workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}
	// ワークフロー情報取得
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );




}

// 承認状況チェック
$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	echo("<script language=\"javascript\">alert('他の承認状況が変更されたため、更新できません。');</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");		
}


// 申請更新
$obj->update_apply($apply_id, $content, $apply_title);

// 承認削除・登録
$obj->delete_applyapv($apply_id);

for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$emp_id = ($$varname == "") ? null : $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;


	// 所属、役職も登録する
	$infos = get_empmst($con, $emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];

	$arr = array(
				"wkfw_id" => $wkfw_id,
				"apply_id" => $apply_id,
				"apv_order" => $apv_order,
				"emp_id" => $emp_id,
				"apv_stat" => "0",
				"apv_date" => "",
				"delete_flg" => "f",
				"apv_comment" => "",
				"st_div" => $st_div,
				"deci_flg" => "t",
				"emp_class" => $emp_class, 
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept, 
				"emp_st" => $emp_st,
				"apv_fix_show_flg" => "t", 
				"emp_room" => $emp_room, 
				"apv_sub_order" => $apv_sub_order, 
				"multi_apv_flg" => $multi_apv_flg,
				"next_notice_div" => $next_notice_div,
				"parent_pjt_id" => $parent_pjt_id,
				"child_pjt_id" => $child_pjt_id,
				"other_apv_flg" =>  "f"
				);

	$obj->regist_applyapv($arr);
}

/*
// 承認更新
for($i=1; $i<=$approve_num; $i++)
{

	$varname = "regist_emp_id$i";
	$emp_id = $$varname;

	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;

	// 所属、役職も登録する
	$infos = get_empmst($con, $emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];

	$arr = array(
	              "apply_id" => $apply_id,
	              "apv_order" => $apv_order,
                  "apv_sub_order" => $apv_sub_order,
                  "emp_id" => $emp_id,
                  "st_div" => $st_div,
                  "emp_class" => $emp_class,
                  "emp_attribute" => $emp_attribute,
                  "emp_dept" => $emp_dept,            
                  "emp_st" => $emp_st,
                  "emp_room" => $emp_room, 
                  "parent_pjt_id" => $parent_pjt_id,
	              "child_pjt_id" => $child_pjt_id
	             );

	$obj->update_applyapv($arr);
}
*/

// 添付ファイル削除・登録
$obj->delete_applyfile($apply_id);
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 申請結果通知削除登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// 病棟評価表
if($obj->is_ward_template($short_wkfw_name))
{
	$obj->update_ward_grade_data($apply_id, $_POST);
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 添付ファイルの移動
foreach (glob("cas_apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cas_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cas_apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("cas_apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
?>