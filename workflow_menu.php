<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_workflow_category_list.ini");
require_once("referer_common.ini");
require_once("workflow_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 遷移元の設定
if ($workflow != "") {
    set_referer($con, $session, "workflow", $workflow, $fname);
    $referer = $workflow;
} else {
    $referer = get_referer($con, $session, "workflow", $fname);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | ワークフロー一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/treeview/assets/folders/tree.css">
<script type="text/javascript" src="js/yui_0.12.2/build/treeview/treeview-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
//ロード時の処理
function init_action() {
    //ドラッグアンドドロップを初期設定
    constructDragDrop();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="init_action()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
    <a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
    <a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
    <a href="workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

    <?show_wkfw_menuitem($session, $fname, "");?>

    <form name="wkfw" action="#" method="post">
    <? show_workflow_list($con, $session, $fname, $selected_cate, $selected_folder,$selected_parent,$select_box, $page); ?>
    <input type="hidden" name="session" value="<? echo($session); ?>">
    </form>
    </td>
    </tr>
    </table>

</td>
</tr>
</table>

</body>
<?
// エイリアス対応2007/09/25
// ワークフロー一覧画面を初回起動のみフォルダ作成する。
require_once("application_workflow_common_class.php");
$obj = new application_workflow_common_class($con, $fname);
$obj->create_wkfwreal_directory();

pg_close($con);
?>
</html>
