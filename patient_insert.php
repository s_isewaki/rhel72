<?
switch ($path) {
case "C":
	$ret_url = "inpatient_sub_menu2_patient.php";
	break;
case "R":
case "I":
	$ret_url = "inpatient_patient_register.php";
	break;
default:
	$ret_url = "patient_register.php";
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="<? echo $ret_url; ?>">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" name="lt_nm" value="<? echo $lt_nm; ?>">
<input type="hidden" name="ft_nm" value="<? echo $ft_nm; ?>">
<input type="hidden" name="lt_kana_nm" value="<? echo $lt_kana_nm; ?>">
<input type="hidden" name="ft_kana_nm" value="<? echo $ft_kana_nm; ?>">
<input type="hidden" name="birth_yr" value="<? echo $birth_yr; ?>">
<input type="hidden" name="birth_yr2" value="<? echo $birth_yr2; ?>">
<input type="hidden" name="birth_mon" value="<? echo $birth_mon; ?>">
<input type="hidden" name="birth_day" value="<? echo $birth_day; ?>">
<input type="hidden" name="sex" value="<? echo $sex; ?>">
<input type="hidden" name="job" value="<? echo $job; ?>">
<input type="hidden" name="wareki_flg" value="<? echo $wareki_flg; ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="room_no" value="<? echo($room_no); ?>">
<input type="hidden" name="bed_no" value="<? echo($bed_no); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");
require("./get_values.ini");
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者登録権限のチェック
$reg_auth = check_authority($session,22,$fname);
if($reg_auth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 患者ID未入力チェック
if($pt_id == ""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 患者ID文字種チェック
if (preg_match("/[^0-9a-zA-Z]/", $pt_id) > 0) {
	echo("<script type=\"text/javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}に使える文字は半角英数字のみです。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//患者ID 文字数 チェック
if(strlen($pt_id) > 12){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"{$_label_by_profile["PATIENT_ID"][$profile_type]}が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//姓（漢字）null チェック
if($lt_nm ==""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"姓（漢字）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//姓（漢字）文字数チェック
if(strlen($lt_nm) > 20){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"姓（漢字）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）null チェック
if($ft_nm ==""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"名前（漢字）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）文字数チェック
if(strlen($ft_nm) > 20){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"名前（漢字）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//姓（かな）null チェック
if($lt_kana_nm ==""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"姓（かな）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$lt_kana_nm = mb_convert_kana($lt_kana_nm, "HVc");

//姓（かな）文字数チェック
if(strlen($lt_kana_nm) > 20){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"姓（かな）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（かな）null チェック
if($ft_kana_nm ==""){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"名前（かな）を入力してください\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
$ft_kana_nm = mb_convert_kana($ft_kana_nm, "HVc");

//名前（かな）文字数チェック
if(strlen($ft_kana_nm) > 20){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"名前（かな）が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//生年月日 実在日 チェック
if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"生年月日が無効です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

//生年月日 未来日 チェック
$birth = "$birth_yr$birth_mon$birth_day";
$today = date(Ymd);
if ($birth > $today) {
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"生年月日が未来日付です\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// 職業 文字列長チェック
if(strlen($job) > 100){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"職業が長すぎます\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}

// トランザクションの開始
pg_query($con,"begin transaction");

// 患者ID 重複チェック
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con,$SQL81,$cond,$fname);
if($sel==0){
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dup = pg_numrows($sel);
if($dup > 0){
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"指定された{$_label_by_profile["PATIENT_ID"][$profile_type]}は既に使用されています\");</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}


$sql = "insert into ptifmst (ptif_id, ptif_in_out, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_keywd, ptif_birth, ptif_sex, ptif_buis) values (";
$content_ptif = array($pt_id, 1, $lt_kana_nm, $ft_kana_nm, $lt_nm, $ft_nm, get_keyword($lt_kana_nm), $birth, $sex, $job);
$ins = insert_into_table($con, $sql, $content_ptif, $fname);
if($ins==0){
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

if ($wareki_flg == "") {
	$wareki_flg = "f";
}
// 画面遷移
$key1 = urlencode($pt_id);
if ($path == "C") {
	echo("<script language=\"javascript\">location.href = 'inpatient_sub_menu2.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no&key1=$key1&mode=search';</script>");
} else if ($path == "R") {
	$url = "inpatient_reserve_register.php?session=$session&pt_id=$key1&path=1";
	if ($bldg_cd != "" && $ward_cd != "") {
		$url .= "&ward={$bldg_cd}-{$ward_cd}&bedundec1=f";
	} else {
		$url .= "&bedundec1=t";
	}
	if ($room_no != "") {
		$url .= "&ptrm=$room_no&bedundec2=f";
	} else {
		$url .= "&bedundec2=t";
	}
	if ($bed_no != "") {
		$url .= "&bed=$bed_no&bedundec3=f";
	} else {
		$url .= "&bedundec3=t";
	}
	echo("<script language=\"javascript\">opener.location.href = '$url'; self.close();</script>");
} else if ($path == "I") {
	$url = "inpatient_register.php?session=$session&pt_id=$key1&path=3";
	if ($bldg_cd != "" && $ward_cd != "") {
		$url .= "&ward={$bldg_cd}-{$ward_cd}&bedundec1=f";
	} else {
		$url .= "&bedundec1=t";
	}
	if ($room_no != "") {
		$url .= "&ptrm=$room_no&bedundec2=f";
	} else {
		$url .= "&bedundec2=t";
	}
	if ($bed_no != "") {
		$url .= "&bed=$bed_no&bedundec3=f";
	} else {
		$url .= "&bedundec3=t";
	}
	echo("<script language=\"javascript\">opener.location.href = '$url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">location.href = 'patient_detail.php?session=$session&pt_id=$pt_id&wareki_flg=$wareki_flg';</script>");
}
?>
</body>
