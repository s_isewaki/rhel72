<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//勤務シフト作成の個人勤務条件からの場合を除く
if ($wherefrom != "1") { 
    // 権限のチェック
    $checkauth = check_authority($session, 18, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
    
    // 職員登録権限を取得
    $reg_auth = check_authority($session, 19, $fname);
}
// 他部署兼務
$list_other_post_flg = array(
        "1" => "有り",
        "" => "無し"
        );

// 入力内容のチェック
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

$wk_other_post_flg = $_POST["other_post_flg"];
if ($wk_other_post_flg == "") {
    $wk_other_post_flg = null;
}
// UPDATE
if (!empty($_POST["empcond_other_post_flg_history_id"])) {
    $sql = "UPDATE empcond_other_post_flg_history SET ";
    $cond = "WHERE empcond_other_post_flg_history_id={$_POST["empcond_other_post_flg_history_id"]}";
    $upd = update_set_table($con, $sql, array('emp_id', 'other_post_flg', 'histdate'), array($_POST["emp_id"], $wk_other_post_flg, $_POST["histdate"]), $cond, $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// INSERT
else {
    $sql = "insert into empcond_other_post_flg_history (emp_id, other_post_flg, histdate) values(";
    $content = array($_POST["emp_id"], $wk_other_post_flg, $_POST["histdate"]);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
<?
//勤務シフト作成の個人勤務条件からの場合
 if ($wherefrom == "1") { ?>
location.href = 'employee_cond_op_history_list.php?session=<?=$session?>&emp_id=<?=$emp_id?>&wherefrom=<?=$wherefrom?>';
<? }
else {
    ?>
location.href = 'employee_condition_setting.php?session=<?=$session?>&emp_id=<?=$emp_id?>';
<? } ?>
</script>