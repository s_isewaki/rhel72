<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//重複チェック
$dup_flg = 0;
for ($i=0; $i<14; $i++) {
	$var_name = "group_id_".$i;
	$group_id1 = $$var_name;
	if ($group_id1 != "") {
		for ($j=$i+1; $j<15; $j++) {
			$var_name = "group_id_".$j;
			$group_id2 = $$var_name;
			if ($group_id1 == $group_id2) {
				$dup_flg = 1;
				break;
			}
		}
	}
	if ($dup_flg == 1) {
		break;
	}
}
if ($dup_flg == 1)
{
	echo("<script type=\"text/javascript\">alert('重複しているグループがあります。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 削除
$sql = "delete from tmcd_night_end_time";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 繰り返す
for ($i=0; $i<15; $i++) {
	$var_name = "group_id_".$i;
	$group_id = $$var_name;
	if ($group_id != "") {
		$var_name2 = "time_".$i;
		$night_end_time = $$var_name2;
		$sql = "insert into tmcd_night_end_time (".
				"no, group_id, night_end_time) values (";
		$content = array(
				$i + 1, $group_id, $night_end_time);	

		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}


// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_night_end_time.php?session=$session';</script>");
?>
