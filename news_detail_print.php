<?
require_once("about_comedix.php");
require("show_class_name.ini");
require("get_values.ini");
require("news_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// お知らせ情報を取得
$sql = "
    select
        news.*,
        newscomment.comment,
        newscate.newscate_name,
        newsnotice.notice_name,
        newsnotice2.notice2_name,
        empmst.emp_lt_nm,
        empmst.emp_ft_nm,
        stmst.st_nm,
        srcempmst.emp_lt_nm as src_emp_lt_nm,
        srcempmst.emp_ft_nm as src_emp_ft_nm,
        (select class_nm from classmst where classmst.class_id = news.src_class_id) as src_class_nm,
        (select atrb_nm from atrbmst where atrbmst.atrb_id = news.src_atrb_id) as src_atrb_nm,
        (select dept_nm from deptmst where deptmst.dept_id = news.src_dept_id) as src_dept_nm,
        (select room_nm from classroom where classroom.room_id = news.src_room_id) as src_room_nm,
        srcstmst.st_nm as src_st_nm
    from
        ((((news
        inner join empmst on news.emp_id = empmst.emp_id)
        left join newsnotice on news.notice_id = newsnotice.notice_id
        left join newsnotice2 on news.notice2_id = newsnotice2.notice2_id)
        left join empmst srcempmst on news.src_emp_id = srcempmst.emp_id)
        left join stmst on empmst.emp_st = stmst.st_id)
        left join stmst srcstmst on srcempmst.emp_st = srcstmst.st_id
        inner join newscate on news.news_add_category = newscate.newscate_id
        left join newscomment on newscomment.news_id = $news_id and newscomment.emp_id = '$emp_id'
    ";
$cond = "where news.news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news_title = pg_fetch_result($sel_news, 0, "news_title");
$news_category = pg_fetch_result($sel_news, 0, "news_add_category");
$notice_name = pg_fetch_result($sel_news, 0, "notice_name");
$notice2_name = pg_fetch_result($sel_news, 0, "notice2_name");
$news = pg_fetch_result($sel_news, 0, "news");
$src_class_nm = pg_fetch_result($sel_news, 0, "src_class_nm");
$src_atrb_nm = pg_fetch_result($sel_news, 0, "src_atrb_nm");
$src_dept_nm = pg_fetch_result($sel_news, 0, "src_dept_nm");
$src_room_nm = pg_fetch_result($sel_news, 0, "src_room_nm");
$news_begin = pg_fetch_result($sel_news, 0, "news_begin");
$news_end = pg_fetch_result($sel_news, 0, "news_end");
$emp_name = pg_fetch_result($sel_news, 0, "emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "emp_ft_nm");
$news_date = pg_fetch_result($sel_news, 0, "news_date");
$src_emp_nm = pg_fetch_result($sel_news, 0, "src_emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "src_emp_ft_nm");
$newscate_name = pg_fetch_result($sel_news, 0, "newscate_name");
$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
$src_emp_st = pg_fetch_result($sel_news, 0, "src_st_nm");
$emp_st = pg_fetch_result($sel_news, 0, "st_nm");
$comment_flg = pg_fetch_result($sel_news, 0, "comment_flg");
$comment = pg_fetch_result($sel_news, 0, "comment");
$deadline = pg_fetch_result($sel_news, 0, "deadline");
$public_flg = pg_fetch_result($sel_news, 0, "public_flg");

// 添付ファイル情報を取得
$sql = "select newsfile_no, newsfile_name from newsfile";
$cond = "where news_id = $news_id order by newsfile_no";
$sel_file = select_from_table($con, $sql, $cond, $fname);
if ($sel_file == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$file_id = array();
$filename = array();
while ($row = pg_fetch_array($sel_file)) {
    array_push($file_id, $row["newsfile_no"]);
    array_push($filename, $row["newsfile_name"]);
}

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// お知らせ区分一覧を取得
$notice2_list = get_notice2_list($con, $fname);

// お知らせ確認済み？
if ($record_flg == "t") {
    $sql = "select count(*) from newsref";
    $cond = "where news_id = $news_id and emp_id = '$emp_id'";
    $sel_ref = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_fetch_result($sel_ref, 0, 0) == 0) {
        $referred_flg = false;
    }
    else {
        $referred_flg = (pg_fetch_result($sel, 0, "ref_time") != "");
    }
}

// 日付の表示設定
$news_begin = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_begin);
$news_end = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_end);
$news_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_date);

// カテゴリの表示設定
switch ($news_category) {

    case "1":
        $news_sub_category = "";
        break;

    case "2":
        $news_sub_category = get_sub_category_label($con, $news_id, $fname);
        break;

    case "3":
        $news_sub_category = get_sub_category_label($con, $news_id, $fname);
        break;
}

// 役職情報取得
$st_nms = "";
$newsst_info = get_newsst_info($con, $fname, $news_id);
foreach ($newsst_info as $st_id => $st_nm) {
    if ($st_nms != "") {
        $st_nms .= "、";
    }
    $st_nms .= $st_nm;
}
$arr_checked = array();
$qa_cnt = 0;
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$arr_checked = array();
if ($record_flg == "t") {
    $sql = "select question, select_num, multi_flg from newsenquet_q";
    $cond = "where news_id = $news_id order by question_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $i = 1;
    while ($row = pg_fetch_array($sel)) {
        $question[$i] = $row["question"];
        $select_num[$i] = $row["select_num"];
        $multi_flg[$i] = $row["multi_flg"];
        $qa_cnt++;
        $i++;
    }

    for ($i = 1; $i <= $qa_cnt; $i++) {
        $sql = "select answer from newsenquet_a";
        $cond = "where news_id = $news_id and question_no = $i order by item_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $j = 1;
        while ($row = pg_fetch_array($sel)) {
            $answer[$i][$j] = $row["answer"];
            $j++;
        }
    }
    // 回答済み番号の取得
    if ($referred_flg == true) {
        for ($i = 1; $i <= $qa_cnt; $i++) {
            $sql = "select item_no from newsanswer";
            $cond = "where news_id = $news_id and question_no = $i and emp_id = '$emp_id' order by item_no";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            while ($row = pg_fetch_array($sel)) {
                $j = intval($row["item_no"]);
                $arr_checked[$i][$row["item_no"]] = " checked";
            }
        }
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<? if ($qa_cnt > 0 ) { ?>
<script type="text/javascript">
var init_value = new Array();
<?
for ($i=1; $i<=$qa_cnt; $i++) {
	//
	if ($multi_flg[$i] == "t") {
		for ($j=1; $j<=$select_num[$i]; $j++) {
			echo("init_value['answer{$i}_{$j}'] = '{$arr_checked[$i][$j]}';\n");
		}
	} else {
		for ($j=1; $j<=$select_num[$i]; $j++) {
			if ($arr_checked[$i][$j] != "") {
				echo("init_value['answer{$i}'] = '{$j}';\n");
			}
		}
	}
}
?>
function funcOnclick(obj) {
	if (obj.type == 'checkbox') {
		if (init_value[obj.name] != '') {
			obj.checked = true;
		} else {
			obj.checked = false;
		}
	} else if (obj.type == 'radio') {
		name = obj.name;
		value = init_value[name];
		for(i=0; i<document.mainform.elements[name].length; i++) {
			if(document.mainform.elements[name][i].value == value) {
				document.mainform.elements[name][i].checked = true;
			} else {
				document.mainform.elements[name][i].checked = false;
			}
		}
	}
}
</script>
<? } ?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
p.attach {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print(); self.close();">
<center>
<table width="640" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><b>お知らせ</b></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="640" border="0" cellspacing="0" cellpadding="4" class="list">
<? if ($comment_flg == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">コメント</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo(str_replace("\n", "<br>", $comment)); ?></font></td>
</tr>
<? } ?>
<tr height="22">
<td width="25%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">タイトル</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? eh($news_title); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">対象カテゴリ</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding-right:5px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($newscate_name); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($news_sub_category); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">対象役職</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($st_nms); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">通達区分</font></td>
<td colspan="3" width="180"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($notice_name); ?></font></td>
</tr>
<? if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">お知らせ区分</font></td>
<td colspan="3" width="180"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($notice2_name); ?></font></td>
</tr>
<? }?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">発信部署</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($src_class_nm); ?><? if ($src_atrb_nm != "") { ?> &gt; <? echo($src_atrb_nm); ?><? } ?><? if ($src_dept_nm != "") { ?> &gt; <? echo($src_dept_nm); ?><? } ?><? if ($src_room_nm != "") { ?> &gt; <? echo($src_room_nm); ?><? } ?></font></td>
</tr>
<tr height="22">
<td width="24%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">発信者</font></td>
<td width="27%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($src_emp_nm); ?></font></td>
<td width="22%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">発信者役職</font></td>
<td width="27%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($src_emp_st); ?></font></td>
</tr>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">内容</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><? echo(str_replace("\n", "<br>", preg_replace("/>(\r?\n)+</", "><", $news))); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">添付ファイル</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<?
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\" class=\"attach\">\n");
	echo("{$tmp_filename}\n");
	echo("</p>\n");
}
?>
</font></td>
</tr>
<? if ($record_flg == "t" && $qa_cnt > 0) { ?>
<tr height="22">
<td align="right" valign="top" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">アンケート</font></td>
<td colspan="3">
<form name="mainform">
<table id="qas" width="100%" border="0" cellspacing="0" cellpadding="2">
<?
for ($i = 1; $i <= $qa_cnt; $i++) {

	echo("<tr><td>\n");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">");
	echo("質問{$i} {$question[$i]}<br>&nbsp;\n");

	for ($j = 1; $j <= $select_num[$i]; $j++) {
		if ($multi_flg[$i] == "t") {
			echo("<input type=\"checkbox\" name=\"answer{$i}_{$j}\" value=\"{$j}\" {$arr_checked[$i][$j]} onclick=\"funcOnclick(this);\">{$answer[$i][$j]}&nbsp;");
		} else {
			echo("<input type=\"radio\" name=\"answer{$i}\" value=\"{$j}\" {$arr_checked[$i][$j]} onclick=\"funcOnclick(this);\">{$answer[$i][$j]}&nbsp;");
		}
	}
	echo("</font><br></td></tr>\n");
	echo("<tr><td><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr>\n");
}
?>
</table>

<table border="0" cellspacing="0" cellpadding="2">
<? if ($deadline != "") { ?>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">締切り日時&nbsp;</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?
$deadline_str = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $deadline);
 echo($deadline_str); ?></font></td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">集計結果の公開&nbsp;</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?
if ($public_flg == "t") {
	echo("公開");
} else {
	echo("非公開");
}
?></font></td>
</tr>
</table>
</form>
</td>
</tr>
<?
}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">掲載期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($news_begin); ?> 〜 <? echo($news_end); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録者</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($emp_name); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録者役職</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($emp_st); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($news_date); ?></font></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
