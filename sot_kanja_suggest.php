<?
$error = "";

// 画面パラメータ取得
$session = @$_REQUEST["session"];

$fname = $_SERVER["PHP_SELF"];

$sel_enti_id  = (int)trim(@$_REQUEST["sel_enti_id"]);
$sel_sect_id  = (int)trim(@$_REQUEST["sel_sect_id"]);
$sel_dr_id    = (int)trim(@$_REQUEST["sel_dr_id"]);
$sel_bldg_cd  = (int)trim(@$_REQUEST["sel_bldg_cd"]);
$sel_ward_cd  = (int)trim(@$_REQUEST["sel_ward_cd"]);
$sel_room_no  = (int)trim(@$_REQUEST["sel_room_no"]);

$sel_ptif_name_list = explode(" ", mb_convert_encoding(urldecode(urldecode(@$_REQUEST["sel_ptif_name"])), "EUC-JP", "UTF-8"));
$sel_dr_key   = trim(@$_REQUEST["sel_dr_key"]);
$sel_sect_key = trim(@$_REQUEST["sel_sect_key"]);
$sel_sort_by_byoto = trim(@$_REQUEST["sel_sort_by_byoto"]);
$today_yyyymmdd = (int)Date("Ymd");

for (;;){
	// リクワイア   リクワイア先でのecho/print文を除去
	ob_start();
	require_once("about_postgres.php");
	require_once("about_session.php");
	require_once("about_authority.php");
	ob_end_clean();

	// DB接続
	$con = connect2db($fname);
	if($con == "0") { $error = "03"; break; }

	//セッションのチェック
	$session = qualify_session(@$session,$fname);
	if($session == "0") { $error = "01 / ". $session; break; }

	//権限チェック
	$summary_check=check_authority($session,57,$fname);
	if($summary_check == "0") { $error = "02"; break; }


	$sql = array();
	$sql[] = "select inpt.ptif_id, bldg.bldg_name, ward.ward_name, ptrm.ptrm_name";
	$sql[] = ",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm";
	if ($sel_bldg_cd || $sel_enti_id || $sel_sect_id || $sel_dr_id){
		$sql[] = "from inptmst inpt";
		$sql[] = "left outer join ptifmst pt on (pt.ptif_id = inpt.ptif_id)";
	} else {
		$sql[] = "from ptifmst pt";
		$sql[] = "left outer join inptmst inpt on (inpt.ptif_id = pt.ptif_id)";
	}
	$sql[] = "left outer join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)";
	$sql[] = "left outer join wdmst ward on (ward.bldg_cd = inpt.bldg_cd and ward.ward_cd = inpt.ward_cd)";
	$sql[] = "left outer join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)";
	$sql[] = "where inpt.inpt_in_dt <= '$today_yyyymmdd'";
	$sql[] = "and inpt.inpt_in_dt is not null and inpt.inpt_in_dt <> ''";
	$sql[] = "and (inpt.inpt_out_res_dt is null or inpt.inpt_out_res_dt >= '$today_yyyymmdd')";
	$sql[] = "and (inpt.inpt_out_dt is null or inpt.inpt_out_dt >= '$today_yyyymmdd')";

	// もし患者名も事業所も選択していなければ、検索しない。
	if (trim(@$_REQUEST["sel_ptif_name"])=="" && !$sel_bldg_cd && !$sel_sect_id){
		$sql[] = "and false";
	}

	if ($sel_bldg_cd) $sql[] = "and inpt.bldg_cd = ".$sel_bldg_cd;
	if ($sel_ward_cd) $sql[] = "and inpt.ward_cd = ".$sel_ward_cd;
	if ($sel_room_no) $sql[] = "and inpt.ptrm_room_no = ".$sel_room_no;
	if ($sel_enti_id) $sql[] = "and inpt.inpt_enti_id = ".$sel_enti_id;
	if ($sel_sect_id) $sql[] = "and inpt.inpt_sect_id = ".$sel_sect_id;
	if ($sel_dr_id)   $sql[] = "and inpt.dr_id = ".$sel_dr_id;

	foreach ($sel_ptif_name_list as $idx => $ptif_name){
		$ptif_name = pg_escape_string(trim($ptif_name));
		if ($ptif_name=="") continue;
		$sql[] = "and(";
		$sql[] = "  inpt.ptif_id like '".$ptif_name."%'";
		$sql[] = "  or pt.ptif_lt_kaj_nm like '%".$ptif_name."%'";
		$sql[] = "  or pt.ptif_ft_kaj_nm like '%".$ptif_name."%'";
		$sql[] = "  or pt.ptif_lt_kaj_nm || pt.ptif_ft_kaj_nm like '%".$ptif_name."%'";
		$sql[] = ")";
	}

	if ($sel_sort_by_byoto){
		$sql[] = "order by inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no";
	} else {
		$sql[] = "order by pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm";
	}
	$sql[] = "limit 10";
	$sel = select_from_table($con, implode("\n",$sql), "", null);

	$out = array();
	while ($row = pg_fetch_array($sel)){
		$out[] = '{"id":"'.$row["ptif_id"].'","name":"'.$row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"].'"}';
	}
	break;
}
if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{"error":"<?=$error?>","list_length":<?=count($out)?>,"list":[<?=implode(",",$out)?>]}
