<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 予約</title>
<?
//ini_set("display_errors","1");
require_once("about_comedix.php");
require("show_select_values.ini");
require("show_class_name.ini");
require("holiday.php");
require("facility_common.ini");
require("show_facility.ini");
require("mygroup_common.php");
require("label_by_profile_type.ini");
require_once("yui_calendar_util.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// スケジュール権限の取得
$schd_auth = check_authority($session, 2, $fname);

// 初期表示時の日付設定
if ($ymd != "") {
    $year = substr($ymd, 0, 4);
    $month = substr($ymd, 4, 2);
    $day = substr($ymd, 6, 2);

// ポストバック時の日付設定
} else {
    $ymd = "$year$month$day";
}

// 時刻のデフォルトは9時
if ($s_hour == "") {
    $s_hour = "09";
    $s_minute = "00";
}
if ($e_hour == "") {
    $e_hour = sprintf("%02d", intval($s_hour) + 1);
    $e_minute = $s_minute;
    if ($e_hour >= "24") {
        $e_hour = "24";
        $e_minute = "00";
    }
}

// 繰り返し項目の初期値
if ($repeat_type == "") {
    $repeat_type = "2";
}
if ($rpt_end_year == "") {
    $rpt_end_year = $year;
}
if ($rpt_end_month == "") {
    $rpt_end_month = $month;
}
if ($rpt_end_day == "") {
    $rpt_end_day = $day;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// ログインユーザの内線番号を取得
$sql = "select emp_ext from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_ext = pg_fetch_result($sel, 0, "emp_ext");

// 予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_reservable_categories($con, $emp_id, $fname);

// 引数の設備IDが未設定時、初期値を先頭とする
if ($cate_id == "" || $cate_id == "all") {
    $cate_id = key($categories);
}
if ($facility_id == "") {
    $facility_id = key($categories[$cate_id]["facilities"]);
}

// オプション設定情報を取得
$sql = "select calendar_start1 from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

$schd_emp_id = $emp_id;

// 対象職員情報を配列に格納
$arr_target = array();
if ($target_id_list != "") {
    $arr_target_id = split(",", $target_id_list);
    for ($i = 0; $i < count($arr_target_id); $i++) {
        $tmp_emp_id = $arr_target_id[$i];
        $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
        $cond = "where emp_id = '$tmp_emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        }
        $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
        array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
    }
}

// 種別一覧を取得
$sql = "select * from rsvtype";
$cond = "order by type_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_type = array();
while ($row = pg_fetch_array($sel)) {
    $arr_type[$row["type_id"]] = $row["type_name"];
}

if ($calendar == "") {
    $calendar = "off";
}
if ($repeat == "") {
    $repeat = "off";
}

$employees = array();
global $employees;

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);

// 設備IDごとの内容形式を取得する。種別表示フラグも取得。
$sql = "select facility.facility_id, fclhist.fclhist_content_type, facility.show_type_flg, facility.show_repeat_flg, facility.show_timeless_flg from facility left join fclhist on fclhist.facility_id = facility.facility_id and fclhist.fclhist_id = facility.fclhist_id";
$cond = "where facility_del_flg = 'f' order by facility.facility_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$arr_content_type = array();
$arr_show_type_flg = array();
$arr_show_repeat_flg = array();
$arr_show_timeless_flg = array();
while ($row = pg_fetch_array($sel)) {
    $arr_content_type[$row["facility_id"]] = ($row["fclhist_content_type"] == "") ? "1" : $row["fclhist_content_type"];
    $arr_show_type_flg[$row["facility_id"]] = $row["show_type_flg"];
    $arr_show_repeat_flg[$row["facility_id"]] = $row["show_repeat_flg"];
    $arr_show_timeless_flg[$row["facility_id"]] = $row["show_timeless_flg"];
}

// カテゴリ別テンプレート使用確認
$arr_content_type_cate = array();
foreach ($categories as $tmp_category_id => $tmp_category) {
    $tmp_content_type = "1";
    foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
        // 設備のうち1件でもある場合、使用ありとする
        if ($arr_content_type["$tmp_facility_id"] == "2") {
            $tmp_content_type = "2";
            break;
        }
    }
    $arr_content_type_cate["$tmp_category_id"] = $tmp_content_type;
}
// テンプレート履歴情報を取得
$fclhist_info = get_fclhist($con, $fname, $facility_id, 0);

// 公開範囲用のグループ一覧を作成
$pub_limit_group = array();
$sql = "select m.group_id, m.group_nm from comgroupmst m";
$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$emp_id') order by m.group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $pub_limit_group["c" . $row["group_id"]] = $row["group_nm"];
}

if ($back != "t" && $pub_limit != "") {
    switch (substr($pub_limit, 0, 1)) {
    case "d":
        $pub_limit_dept = substr($pub_limit, 1);
        $pub_limit = "d";
        break;
    case "c":
        if (isset($pub_limit_group[$pub_limit])) {
            $pub_limit_group_id = $pub_limit;
            $pub_limit = "g";
        }
        break;
    }
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
require("schedule_registration_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
// 設備ID別の内容形式
var arr_content_type = new Array();
// カテゴリ別の内容形式
var arr_content_type_cate = new Array();
// 設備ID別の種別表示フラグ
var arr_show_type_flg = new Array();
// 設備ID別の繰返し表示フラグ
var arr_show_repeat_flg = new Array();
// 設備ID別の「指定しない」表示フラグ
var arr_show_timeless_flg = new Array();
<?
foreach($arr_content_type as $tmp_facility_id => $tmp_content_type) {
    echo("arr_content_type['$tmp_facility_id'] = $tmp_content_type;\n");
}
foreach($arr_content_type_cate as $tmp_cate_id => $tmp_content_type) {
    echo("arr_content_type_cate['$tmp_cate_id'] = $tmp_content_type;\n");
}
foreach($arr_show_type_flg as $tmp_facility_id => $tmp_show_type_flg) {
    echo("arr_show_type_flg['$tmp_facility_id'] = '$tmp_show_type_flg';\n");
}
foreach($arr_show_repeat_flg as $tmp_facility_id => $tmp_show_repeat_flg) {
    echo("arr_show_repeat_flg['$tmp_facility_id'] = '$tmp_show_repeat_flg';\n");
}
foreach($arr_show_timeless_flg as $tmp_facility_id => $tmp_show_timeless_flg) {
    echo("arr_show_timeless_flg['$tmp_facility_id'] = '$tmp_show_timeless_flg';\n");
}
?>
function initPage() {
    setDayOptions('<? echo($day); ?>');

<?
if ($datelist != "") {
    $tmp_arr_date = split(",", $datelist);
    foreach ($tmp_arr_date as $tmp_date) {
        echo("\tdateOnClick('$tmp_date')\n");
    }
}
?>

    for (var i = 0, j = document.mainform.cate_id.options.length; i < j; i++) {
        if (document.mainform.cate_id.options[i].value == '<? echo($cate_id); ?>') {
            document.mainform.cate_id.options[i].selected = true;
        }
    }
    cateOnChange('<? echo($facility_id); ?>');

<?
foreach ($sync_fcls as $sync_fcl) {
    echo("\tcheckSyncFcl('$sync_fcl');\n");
}
?>

<? if ($arr_show_repeat_flg["$facility_id"] == "t") { ?>
    setRepeatDisplay('<? echo($repeat); ?>');
<? } ?>
    setCalendarDisplay('<? echo($calendar); ?>');

    //登録対象者を設定する。
    update_target_html();

    groupOnChange();
    setTimeDisabled();
    setMailDisabled();
}

function setDayOptions(selectedDay) {
    var dayPulldown = document.mainform.day;
    if (!selectedDay) {
        selectedDay = dayPulldown.value;
    }
    deleteAllOptions(dayPulldown);

    var year = parseInt(document.mainform.year.value, 10);
    var month = parseInt(document.mainform.month.value, 10);
    var daysInMonth = new Date(year, month, 0).getDate();

    for (var d = 1; d <= daysInMonth; d++) {
        var tmpDayValue = d.toString();
        if (tmpDayValue.length == 1) {
            tmpDayValue = '0'.concat(tmpDayValue);
        }

        var tmpDayText = d.toString();

        var tmpDate = new Date(year, month - 1, d);
        var tmpWeekDay = tmpDate.getDay();
        switch (tmpWeekDay) {
        case 0:
            tmpDayText = tmpDayText.concat('（日）');
            break;
        case 1:
            tmpDayText = tmpDayText.concat('（月）');
            break;
        case 2:
            tmpDayText = tmpDayText.concat('（火）');
            break;
        case 3:
            tmpDayText = tmpDayText.concat('（水）');
            break;
        case 4:
            tmpDayText = tmpDayText.concat('（木）');
            break;
        case 5:
            tmpDayText = tmpDayText.concat('（金）');
            break;
        case 6:
            tmpDayText = tmpDayText.concat('（土）');
            break;
        }

        addOption(dayPulldown, tmpDayValue, tmpDayText);
    }

    while (parseInt(selectedDay, 10) > daysInMonth) {
        selectedDay = (parseInt(selectedDay, 10) - 1).toString();
    }
    dayPulldown.value = selectedDay;
}

function cateOnChange(facility_id) {

    // 施設・設備セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.facility_id);

    // 施設・設備セレクトボックスのオプションを作成
    var category_id = document.mainform.cate_id.value;
<?
foreach ($categories as $tmp_category_id => $tmp_category) {
    echo("\tif (category_id == '$tmp_category_id') {\n");
    foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
        echo("\t\taddOption(document.mainform.facility_id, '$tmp_facility_id', '{$tmp_facility["name"]}', facility_id);\n");
    }
    if ($tmp_category["pt_flg"] == "t") {
        echo("\tsetPatientDisplay(true);\n");
    } else {
        echo("\tsetPatientDisplay(false);\n");
    }
    echo("\t}\n");
}
?>

    // オプションが1件も作成されなかった場合
    var opt_cnt = document.mainform.facility_id.options.length;
    if (opt_cnt == 0) {
        addOption(document.mainform.facility_id, '0', '（なし）', facility_id);
    }

    // 施設・設備がなしの場合、登録ボタンを押下不可にする
    var sbmtDisabled = (opt_cnt == 0);
    document.mainform.sbmt[0].disabled = sbmtDisabled;
    document.mainform.sbmt[1].disabled = sbmtDisabled;

    var tmp_content_type = arr_content_type_cate[category_id];
    // 施設・設備が複数ある場合、同時予約チェックボックスを選択可能にする
    var syncDisabled = (opt_cnt < 2 || tmp_content_type == 2) ;
    document.mainform.sync.disabled = syncDisabled;
    if (syncDisabled) {
        document.mainform.sync.checked = false;
    }

    syncFclsDisplayChange();
}

function syncFclsDisplayChange() {
    var div = document.getElementById('sync_fcls');
    div.innerHTML = '';

    if (document.mainform.sync.disabled || !document.mainform.sync.checked) {
        div.style.display = 'none';
        return;
    }

    var fcls = document.mainform.facility_id.options;
    for (var i = 0, j = fcls.length; i < j; i++) {
        if (fcls[i].selected) {
            continue;
        }
        div.innerHTML += '<input type="checkbox" name="sync_fcls[]" value="' + fcls[i].value + '">';
        div.innerHTML += fcls[i].text;
        div.innerHTML += '&nbsp;&nbsp;';
    }
    div.style.display = '';

}

function dispTextOrTemplate() {

// 種別表示フラグにより変更する
    if (arr_show_type_flg[document.mainform.facility_id.value] == 't') {
        document.getElementById('type_row').style.display = '';
        document.mainform.show_type_flg.value = "t";
    } else {
        document.getElementById('type_row').style.display = 'none';
        document.mainform.show_type_flg.value = "f";
    }
    if (arr_show_repeat_flg[document.mainform.facility_id.value] == 't') {
        setRepeatDisplay('off');
    } else {
        document.getElementById('repeat_on_row').style.display = 'none';
        document.getElementById('repeat_off_row').style.display = 'none';
    }

// 「指定しない」表示フラグにより変更する
    if (arr_show_timeless_flg[document.mainform.facility_id.value] == 't') {
        document.getElementById('show_timeless').style.display = '';
        var disabled = document.mainform.timeless.checked;
        document.mainform.s_hour.disabled = disabled;
        document.mainform.s_minute.disabled = disabled;
        document.mainform.e_hour.disabled = disabled;
        document.mainform.e_minute.disabled = disabled;
    } else {
        document.getElementById('show_timeless').style.display = 'none';
        document.mainform.timeless.checked = false;
        document.mainform.s_hour.disabled = false;
        document.mainform.s_minute.disabled = false;
        document.mainform.e_hour.disabled = false;
        document.mainform.e_minute.disabled = false;
    }

// テキストからテキストの場合は、再表示なし。それ以外は、再表示。
    if (document.mainform.content_type.value == 1 &&
        arr_content_type[document.mainform.facility_id.value] == 1) {
        return;
    }

    var url = 'facility_reservation_register.php?session=<?=$session?>';
    url += '&cate_id=' + document.mainform.cate_id.value;
    url += '&facility_id=' + document.mainform.facility_id.value;
    url += '&ymd=<?=$ymd?>&path=<?=$path?>&range=<?=$range?>';

    location.href = url;

}

function checkSyncFcl(sync_fcl) {
    var fcls;
    if (document.mainform.elements['sync_fcls[]'].length) {
        fcls = document.mainform.elements['sync_fcls[]'];
    } else {
        fcls = new Array(document.mainform.elements['sync_fcls[]']);
    }

    for (var i = 0, j = fcls.length; i < j; i++) {
        if (fcls[i].value == sync_fcl) {
            fcls[i].checked = true;
            return;
        }
    }
}

function setPatientDisplay(pt_flg) {
    if (pt_flg) {
        document.getElementById('pt_off_row').style.display = 'none';
        document.getElementById('pt_on_row').style.display = '';
    } else {
        document.getElementById('pt_on_row').style.display = 'none';
        <? // 連携を使用しない場合は行を表示しない ?>
        document.getElementById('pt_off_row').style.display = 'none';
    }
}

function groupOnChange() {
    var group_id = getSelectedValue(document.mainform.mygroup);

    // 職員一覧セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.emplist);

    // 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
    if (group_id == '<? echo $tmp_group_id; ?>') {
    <? foreach($arr as $tmp_emp) { ?>
        addOption(document.mainform.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
    <? } ?>
    }
<? } ?>
}

function addEmp() {
    var emp_id_str = "";
    var emp_name_str = "";
    var first_flg = true;
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        if (document.mainform.emplist.options[i].selected) {
            var emp_id = document.mainform.emplist.options[i].value;
            var emp_name = document.mainform.emplist.options[i].text;
            if (first_flg == true) {
                first_flg = false;
            } else {
                emp_id_str += ", ";
                emp_name_str += ", ";
            }
            emp_id_str += emp_id;
            emp_name_str += emp_name;
        }
    }
    if (emp_id_str != "") {
        set_wm_counter(emp_id_str);
        add_target_list(emp_id_str, emp_name_str);
    }
}


function deleteEmp() {

    for (var i = document.mainform.target.options.length - 1; i >= 0; i--) {
        if (document.mainform.target.options[i].selected) {
            document.mainform.target.options[i] = null;
        }
    }
    setTargetList();

}

function deleteAllEmp() {

    deleteAllOptions(document.mainform.target);
    setTargetList();

}

function selectAllEmp() {

    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        document.mainform.emplist.options[i].selected = true;
    }

}

function setTargetList() {

    document.mainform.target_id_list.value = '';
    for (var i = 0, j = document.mainform.target.options.length; i < j; i++) {
        if (i > 0) {
            document.mainform.target_id_list.value += ',';
        }
        document.mainform.target_id_list.value += document.mainform.target.options[i].value;
    }
    setMailDisabled();

}

function deleteAllOptions(box) {

    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }

}

function addOption(box, value, text, selected) {

    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        if (window.opera) {
            opt.defaultSelected = true;
        } else {
            opt.selected = true;
        }
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';

}

function getSelectedValue(sel) {

    return sel.options[sel.selectedIndex].value;

}

function registerReservation() {
    if (document.mainform.target_id_list.value != '') {
        var s_hour = document.mainform.s_hour.value;
        var s_minute = document.mainform.s_minute.value;
        var e_hour = document.mainform.e_hour.value;
        var e_minute = document.mainform.e_minute.value;
        document.mainform.schd_s_time.value = s_hour + ':' + s_minute;
        document.mainform.schd_e_time.value = e_hour + ':' + e_minute;
    }

    if (document.mainform.title.value == '') {
        alert('タイトルが入力されていません。');
        return;
    }

    if (window.InputCheck) {
        if (!InputCheck()) {
            return;
        }
    }

<? if (is_array($log)) { ?>
    if (!confirm('新たな予約が追加されますが、よろしいですか？')) {
        return;
    }
<? } ?>

    var with_patient = (document.getElementById('pt_on_row').style.display != 'none');
    if (with_patient) {
        document.mainform.pt_nms.value = document.getElementById('ptname').innerHTML;
    } else {
        document.mainform.pt_ids.value = '';
        document.mainform.pt_nms.value = '';
    }
    closeEmployeeList();
    document.mainform.submit();
}

function checkType() {
    var check_count = 0;
    for (var i = 0, j = document.mainform.elements['type_ids[]'].length; i < j; i++) {
        if (document.mainform.elements['type_ids[]'][i].checked) {
            check_count++;
        }
    }
    if (check_count > 3) {
        alert('種別の指定は3つまでです。');
        return false;
    }
    return true;
}

function setRepeatDisplay(repeat) {
    if (repeat == 'on') {
        document.getElementById('repeat_off_row').style.display = 'none';
        document.getElementById('repeat_on_row').style.display = '';
        document.mainform.repeat.value = 'on';
    } else {
        document.getElementById('repeat_on_row').style.display = 'none';
        document.getElementById('repeat_off_row').style.display = '';
        document.mainform.repeat.value = 'off';
    }
}

function setCalendarDisplay(calendar) {
    if (calendar == 'on') {
        document.getElementById('date_row').style.display = 'none';
        document.getElementById('calendar_row').style.display = '';
        document.mainform.calendar.value = 'on';
        document.getElementById('repeat_off_row').style.display = 'none';
        document.getElementById('repeat_on_row').style.display = 'none';
    } else {
        document.getElementById('date_row').style.display = '';
        document.getElementById('calendar_row').style.display = 'none';
        document.mainform.calendar.value = 'off';
        if (arr_show_repeat_flg[document.mainform.facility_id.value] == 't') {
            setRepeatDisplay(document.mainform.repeat.value);
        }
    }
}

function dateOnClick(ymd) {
    var add;
    var datelist = document.mainform.datelist.value;
    if (datelist.length == 0) {
        datelist += ymd;
        add = true;
    } else if (datelist.indexOf(ymd) == -1) {
        datelist += ',' + ymd;
        add = true;
    } else {
        if (datelist.length == 8) {
            datelist = '';
        } else {
            datelist = datelist.replace(',' + ymd, '');
        }
        add = false;
    }
    document.mainform.datelist.value = datelist;

    var cell = document.getElementById('id' + ymd);
    if (add) {
        cell.style.backgroundColor = '#ccffcc';
    } else {
        cell.style.backgroundColor = '';
    }
}

function setMailDisabled() {
    var disabled = (
        document.mainform.target_id_list.value == '<? echo($schd_emp_id); ?>' ||
        document.mainform.target_id_list.value == ''
    );
    document.mainform.sendmail.disabled = disabled;
}

function checkEndMinute() {
    if (document.mainform.e_hour.value == '24' &&
        document.mainform.e_minute.value != '00') {
        document.mainform.e_minute.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}

var childwin = null;
function openEmployeeList() {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './schedule_address_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode=2';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    $script = "m_target_list['ID'] = new Array(";
    $is_first = true;
    foreach($arr_target as $row)
    {
        if($is_first)
        {
            $is_first = false;
        }
        else
        {
            $script .= ",";
        }
        $emp_id = $row["id"];
        $emp_name = $row["name"];
        $script .= "new user_info('$emp_id','$emp_name')";
    }
    $script .= ");\n";
    print $script;
?>

function setTimeDisabled() {
    var disabled = document.mainform.timeless.checked;
    document.mainform.s_hour.disabled = disabled;
    document.mainform.s_minute.disabled = disabled;
    document.mainform.e_hour.disabled = disabled;
    document.mainform.e_minute.disabled = disabled;
}

</script>
<?
// カレンダー用JS処理
$num = 0;
if ($fclhist_info["fclhist_content_type"] == "2") {
    $tmpl_content = $fclhist_info["fclhist_content"];
    $pos = 0;
    while (1) {
        $pos = strpos($tmpl_content, 'show_cal', $pos);
        if ($pos === false) {
            break;
        } else {
            $num++;
        }
        $pos++;
    }
}
if ($num > 0) {
    // 外部ファイルを読み込む
    write_yui_calendar_use_file_read_0_12_2();
}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);

?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();<? if ($fclhist_info["fclhist_content_type"] == "2") { ?>
initcal();
<? } ?>
if (window.OnloadSub) { OnloadSub(); }">
<center>
<form name="mainform" action="facility_reservation_insert.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>予約</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="closeEmployeeList();window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="660" border="0" cellpadding="2" cellspacing="0">
<tr height="22">
<td align="right"><input type="button" name="sbmt" value="登録" onclick="registerReservation();"></td>
</tr>
</table>
<table width="660" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="cate_id" onchange="cateOnChange();dispTextOrTemplate();"><? show_category_options($categories, $cate_id); ?></select><select name="facility_id" onchange="syncFclsDisplayChange();dispTextOrTemplate();"></select>
<input type="checkbox" name="sync" value="t"<? if ($sync == "t") {echo(" checked");} ?> onclick="syncFclsDisplayChange();">同時予約
<div id="sync_fcls"></div>
</font></td>
</tr>
<tr height="22" id="date_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td style="border-right-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="year" onchange="setDayOptions();"><? show_select_years_span(min(date("Y"), $year), date("Y") + 10, $year); ?></select>/<select name="month" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="day"></select></font></td>
<td colspan="2" align="right" style="border-left-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('on');">カレンダー指定</a></font></td>
</tr>
<tr height="22" id="calendar_row" style="display:none;">
<td colspan="4">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="inner">
<tr>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 0); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 1); ?></td>
<td width="5"></td>
<td align="center" valign="top"><? show_calendar($year, $month, $start_wd, 2); ?></td>
</tr>
<tr>
<td colspan="5" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="setCalendarDisplay('off');">日付指定</a></font></td>
</tr>
</table>
</td>
</tr>
<? // 設備テーブルfacilityの「指定しない」表示フラグにより表示を変更
$show_timeless_disp = ($arr_show_timeless_flg["$facility_id"] == "t") ? "" : "none";
?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="s_hour"><? show_select_hrs_0_23($s_hour); ?></select>：<select name="s_minute"><? show_minute_options($s_minute); ?></select>〜<select name="e_hour" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $e_hour); ?></select>：<select name="e_minute" onchange="checkEndMinute();"><? show_minute_options($e_minute); ?></select>
<span id="show_timeless" style="display:<?=$show_timeless_disp ?>;"><input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない</span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="title" value="<? eh($title); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" width="100" align="right" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font></td>
<td>
<select name="marker">
<option value="0" style="background-color:white;color:black;"<? if ($marker == 0) {echo(" selected");} ?>>なし
<option value="1" style="background-color:red;color:white;"<? if ($marker == 1) {echo(" selected");} ?>>赤
<option value="2" style="background-color:aqua;color:blue;"<? if ($marker == 2) {echo(" selected");} ?>>青
<option value="3" style="background-color:yellow;color:blue;"<? if ($marker == 3) {echo(" selected");} ?>>黄
<option value="4" style="background-color:lime;color:blue;"<? if ($marker == 4) {echo(" selected");} ?>>緑
<option value="5" style="background-color:fuchsia;color:white;"<? if ($marker == 5) {echo(" selected");} ?>>ピンク
</select>
</td>
</tr>
<? // 設備テーブルfacilityの種別表示フラグにより表示を変更
$show_type_disp = ($arr_show_type_flg["$facility_id"] == "t") ? "" : "none";
?>
<tr height="22" id="type_row" style="display:<?=$show_type_disp?>;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別<br><font color="red">※3つまで指定可</font></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
foreach ($arr_type as $type_id => $type_name) {
    echo("<input type=\"checkbox\" name=\"type_ids[]\" value=\"$type_id\"");
    if (in_array($type_id, $type_ids)) {
        echo(" checked");
    }
    echo(" onclick=\"return checkType();\">$type_name");
    if ($type_id % 5 == 0) {echo("<br>");}
    echo("\n");
}
?>
</font></td>
</tr>
<tr height="22" id="pt_off_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="gray">患者氏名</font></td>
<td colspan="3"></td>
</tr>
<tr height="22" id="pt_on_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('facility_patient_select.php?session=<? echo($session); ?>', 'ptsel', 'width=640,height=480,scrollbars=yes');"><?
// 患者氏名/利用者氏名
echo $_label_by_profile["PATIENT_NAME"][$profile_type];
?></a></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="ptname"><? echo($pt_nms); ?></span></font></td>
</tr>
<tr height="22" id="repeat_off_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/plus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('on');">繰り返し</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="gray">（指定しない）</font></td>
</tr>
<tr height="22" id="repeat_on_row" style="display:none;">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><img src="img/icon/minus.gif" alt="" width="20" height="20" style="margin-right:3px;vertical-align:middle;" onclick="setRepeatDisplay('off');">繰り返し</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="repeat_type" value="2"<? if ($repeat_type == "2") {echo(" checked");} ?>><select name="rpt2_1" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_1 == "1") {echo(" selected");} ?>>毎
<option value="2"<? if ($rpt2_1 == "2") {echo(" selected");} ?>>隔
</select><select name="rpt2_2" onchange="this.form.repeat_type[0].click();">
<option value="1"<? if ($rpt2_2 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt2_2 == "2") {echo(" selected");} ?>>週
<option value="3"<? if ($rpt2_2 == "3") {echo(" selected");} ?>>月
<option value="4"<? if ($rpt2_2 == "4") {echo(" selected");} ?>>月、水、金
<option value="5"<? if ($rpt2_2 == "5") {echo(" selected");} ?>>火、木
<option value="6"<? if ($rpt2_2 == "6") {echo(" selected");} ?>>月〜金
<option value="7"<? if ($rpt2_2 == "7") {echo(" selected");} ?>>土、日
</select><br>
<input type="radio" name="repeat_type" value="3"<? if ($repeat_type == "3") {echo(" checked");} ?>><select name="rpt3_1" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_1 == "1") {echo(" selected");} ?>>毎月（まいつき）
<option value="2"<? if ($rpt3_1 == "2") {echo(" selected");} ?>>毎隔月
<option value="3"<? if ($rpt3_1 == "3") {echo(" selected");} ?>>毎3ヶ月ごと
</select><select name="rpt3_2" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_2 == "1") {echo(" selected");} ?>>第1
<option value="2"<? if ($rpt3_2 == "2") {echo(" selected");} ?>>第2
<option value="3"<? if ($rpt3_2 == "3") {echo(" selected");} ?>>第3
<option value="4"<? if ($rpt3_2 == "4") {echo(" selected");} ?>>第4
<option value="5"<? if ($rpt3_2 == "5") {echo(" selected");} ?>>最終
</select><select name="rpt3_3" onchange="this.form.repeat_type[1].click();">
<option value="1"<? if ($rpt3_3 == "1") {echo(" selected");} ?>>日
<option value="2"<? if ($rpt3_3 == "2") {echo(" selected");} ?>>月
<option value="3"<? if ($rpt3_3 == "3") {echo(" selected");} ?>>火
<option value="4"<? if ($rpt3_3 == "4") {echo(" selected");} ?>>水
<option value="5"<? if ($rpt3_3 == "5") {echo(" selected");} ?>>木
<option value="6"<? if ($rpt3_3 == "6") {echo(" selected");} ?>>金
<option value="7"<? if ($rpt3_3 == "7") {echo(" selected");} ?>>土
</select><br>
　終了日：<select name="rpt_end_year">
<? show_select_years_future(11, $rpt_end_year); ?>
</select>/<select name="rpt_end_month">
<? show_select_months($rpt_end_month); ?>
</select>/<select name="rpt_end_day">
<? show_select_days($rpt_end_day); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者内線番号</font></td>
<td colspan="3"><input name="emp_ext" type="text" size="10" maxlength="10" value="<? echo $emp_ext; ?>" style="ime-mode:inactive;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<?
/*<td colspan="3">
*/
?>
<td colspan="4">
<?
// 内容形式によりテキストとテンプレートに表示を変更。
// テキストの場合
if ($fclhist_info["fclhist_content_type"] == "1") {
    $content = $fclhist_info["fclhist_content"];
?>
<textarea name="content" cols="65" rows="5" style="ime-mode:active;"><? eh($content); ?></textarea>
<? } else {
// テンプレートの場合
    $tmpl_content = $fclhist_info["fclhist_content"];

    $tmpl_content_html = ereg_replace("<\? // XML.*\?>", "", $tmpl_content);
    // プレビュー用ファイル保存
    $savefilename = "fcl/tmp/{$session}_p.php";

    if (!$fp = fopen($savefilename, "w")) {
        echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    if (strlen($tmpl_content_html) == 0) {
        $tmpl_content_html = " ";
    }
    if (!fwrite($fp, $tmpl_content_html, 2000000)) {
        echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }

    fclose($fp);

    include($savefilename);

}
?>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用人数</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="users" value="<? echo($users); ?>" size="4" maxlength="4" style="ime-mode:inactive;">名</font></td>
</tr>
<? if ($schd_auth == "1") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font><br>
<input type="button" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList();"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target_all();"></td>
<td colspan="3">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
    <tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area"></span>
</font>

</td>
<td>
<input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width=4.5em;" onclick="addEmp();">
</td>
<td width="10">
</td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr>
<td height="45%" align="center" valign="bottom">

<select name="mygroup" onchange="groupOnChange();">
<?
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?
        if ($mygroup == $tmp_mygroup_id) {
            echo(" selected");
        }
?>
><?=$tmp_mygroup?></option>
<?
}
?>
</selelct>
</td></tr>
<tr><td>
<select name="emplist" size="7" multiple style="width:150px;">
</select>

</td>
</tr>
<tr>
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr>
<td colspan="3">

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
公開範囲：<?
echo("<input type=\"radio\" name=\"pub_limit\" value=\"\"");
if ($pub_limit == "") echo(" checked");
echo(">全て\n");

echo("<input type=\"radio\" name=\"pub_limit\" value=\"d\"");
if ($pub_limit == "d") echo(" checked");
echo(">所属 ");
echo("<select name=\"pub_limit_dept\" onchange=\"this.form.pub_limit[1].checked = true;\">\n");
    echo("<option value=\"1\"");
    if ($pub_limit_dept == "1") echo(" selected");
    echo(">" . $arr_class_name["class_nm"] . "\n");

    echo("<option value=\"2\"");
    if ($pub_limit_dept == "2") echo(" selected");
    echo(">" . $arr_class_name["atrb_nm"] . "\n");

    echo("<option value=\"3\"");
    if ($pub_limit_dept == "3") echo(" selected");
    echo(">" . $arr_class_name["dept_nm"] . "\n");

    if ($arr_class_name["class_cnt"] == 4) {
        echo("<option value=\"4\"");
        if ($pub_limit_dept == "4") echo(" selected");
        echo(">" . $arr_class_name["room_nm"] . "\n");
    }
echo("</select>\n");

if (count($pub_limit_group) > 0) {
    echo("<input type=\"radio\" name=\"pub_limit\" value=\"g\"");
    if ($pub_limit == "g") echo(" checked");
    echo(">");
    echo("<select name=\"pub_limit_group_id\" onchange=\"this.form.pub_limit[2].checked = true;\">\n");
    foreach ($pub_limit_group as $tmp_pub_limit_group_id => $tmp_pub_limit_group_nm) {
        echo("<option value=\"$tmp_pub_limit_group_id\"");
        if ($pub_limit_group_id == $tmp_pub_limit_group_id) echo(" selected");
        echo(">" . $tmp_pub_limit_group_nm . "\n");
    }
    echo("</select>\n");
}
?>
</font></td>
</tr>
</table>

</td>
</tr>
<? } ?>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録通知メール</font>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="sendmail" value="t"<? if ($sendmail == "t") {echo(" checked");} ?>>送信する<font color="gray">（自分以外の登録対象者にウェブメールを送信します）</font></font></td>
</tr>
</table>
<table width="660" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" name="sbmt" value="登録" onclick="registerReservation();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="schd_s_time">
<input type="hidden" name="schd_e_time">
<input type="hidden" name="calendar" value="">
<input type="hidden" name="repeat" value="">
<input type="hidden" name="datelist" value="">
<input type="hidden" name="pt_ids" value="<? echo($pt_ids); ?>">
<input type="hidden" name="pt_nms" value="">
<input type="hidden" id="target_id_list"   name="target_id_list" value="">
<input type="hidden" id="target_name_list" name="target_name_list" value="">
<input type="hidden" name="content_type" value="<? echo($fclhist_info["fclhist_content_type"]); ?>">
<input type="hidden" name="show_type_flg" value="<? echo($arr_show_type_flg["$facility_id"]); ?>">
</form>
<?
if (is_array($log)) {
    show_log($log, $sync, $sync_fcls);
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// カテゴリオプションを出力
function show_category_options($categories, $cate_id) {
    foreach ($categories as $tmp_category_id => $tmp_category) {
        echo("<option value=\"$tmp_category_id\">{$tmp_category["name"]}\n");
    }
}

// 分オプションを出力
function show_minute_options($minute) {

    $arr_minute = array();
    for ($i = 0; $i < 60; $i += 5) {
        $arr_minute[] = sprintf("%02d", $i);
    }

    foreach ($arr_minute as $tmp_minute) {
        echo("<option value=\"$tmp_minute\"");
        if ($minute == $tmp_minute) {
            echo " selected";
        }
        echo(">$tmp_minute");
    }

}

// ログを表示
function show_log($log, $sync, $sync_fcls) {
/*
    echo("<script type=\"text/javascript\">\n");
    echo("var msg = '登録結果：\\n\\n';\n");
    foreach ($log as $row) {
        list($date, $msg) = split(",", $row);
        echo("msg += '$date $msg\\n';\n");
        echo("alert(msg);\n");
    }
    echo("</script>\n");
*/

    $only_one_facility = (($sync != "t") || (count($sync_fcls) == 0));
    echo("<table id=\"result\" width=\"660\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" style=\"margin-top:5px;\">\n");
    echo("<tr>\n");
    echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>登録結果</b></font></td>\n");
    echo("</tr>\n");
    echo("<tr bgcolor=\"#f6f9ff\">\n");
    echo("<td height=\"22\" width=\"30\"></td>\n");
    echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n");
    if (!$only_one_facility) {
        echo("<td width=\"240\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">施設・設備</font></td>\n");
    }
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">結果</font></td>\n");
    echo("</tr>\n");

    foreach ($log as $row) {
        list($date, $fcl, $msg) = split(",", $row);

        echo("<tr>\n");
        echo("<td height=\"22\" width=\"30\"></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$date</font></td>\n");
        if (!$only_one_facility) {
            echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$fcl</font></td>\n");
        }
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$msg</font></td>\n");
        echo("</tr>\n");
    }

    echo("</table>\n");
}

function show_target_options($arr_target) {
    foreach ($arr_target as $row) {
        echo("<option value=\"{$row["id"]}\">{$row["name"]}\n");
    }
}

function show_target_list($arr_target) {
    for ($i = 0; $i < count($arr_target); $i++) {
        if ($i > 0) {
            echo(",");
        }
        echo($arr_target[$i]["id"]);
    }
}

// 1か月分のカレンダーを出力
function show_calendar($year, $month, $start_wd, $offset) {

    // 処理対象年月を求める
    for ($i = 0; $i < $offset; $i++) {
        if ($month == "12") {
            $month = "01";
            $year++;
        } else {
            $month = substr("0" . ($month + 1), -2);
        }
    }

    // 日付配列を取得
    $arr_month = get_arr_month("$year$month", $start_wd);

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    echo("<tr>\n");
    echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font></td>\n");
    echo("</tr>\n");
    echo("<tr>\n");
    echo("<td>\n");
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
    echo("<tr>\n");
    if ($start_wd == 0) {
        echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
    }
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">月</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">火</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">水</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">木</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fefcdf\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">金</font></td>\n");
    echo("<td width=\"14%\" align=\"center\" bgcolor=\"#defafa\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">土</font></td>\n");
    if ($start_wd == 1) {
        echo("<td width=\"14%\" align=\"center\" bgcolor=\"#fadede\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日</font></td>\n");
    }
    echo("</tr>\n");

    foreach ($arr_month as $arr_week) {
        echo("<tr>\n");

        foreach ($arr_week as $tmp_ymd) {
            $tmp_y = substr($tmp_ymd, 0, 4);
            $tmp_m = substr($tmp_ymd, 4, 2);
            if ($tmp_m != $month) {
                echo("<td>&nbsp;</td>\n");
            } else {
                if (ktHolidayName(to_timestamp($tmp_ymd)) != "") {
                    $bgcolor = "#fadede";
                } else {
                    $bgcolor = "#f6f9ff";
                }
                $tmp_d = intval(substr($tmp_ymd, 6, 2));
                echo("<td id=\"id$tmp_ymd\" align=\"center\" bgcolor=\"$bgcolor\" style=\"cursor:pointer;\" onclick=\"dateOnClick('$tmp_ymd');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"id{$tmp_ymd}_a\" href=\"javascript:void(0);\">$tmp_d</a></font></td>\n");
            }
        }

        echo("</tr>");
    }

    echo("</table>\n");
    echo("</td>\n");
    echo("</tr>\n");
    echo("</table>\n");
}

// 指定年月分の日付配列を返す
function get_arr_month($ym, $start_wd) {

    // 当月1日〜末日の配列を作成
    $year = substr($ym, 0, 4);
    $month = substr($ym, 4, 2);
    $arr_date = array();
    for ($i = 1; $i <= 31; $i++) {
        $day = substr("0" . $i, -2);
        if (checkdate($month, $day, $year)) {
            array_push($arr_date, "$year$month$day");
        } else {
            break;
        }
    }

    // 空白セル分の日付を配列の先頭に追加
    $first_day = mktime(0, 0, 0, $month, "01", $year);
    $empty = date("w", $first_day) - $start_wd;
    if ($empty < 0) {
        $empty += 7;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
    }

    // 空白セル分の日付を配列の末尾に追加
    $day = substr($arr_date[count($arr_date) - 1], -2);
    $end_day = mktime(0, 0, 0, $month, $day, $year);
    $empty = 7 - (count($arr_date) % 7);
    if ($empty == 7) {
        $empty = 0;
    }
    for ($i = 1; $i <= $empty; $i++) {
        array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
    }

    // 週単位に分割した配列を返す
    return array_chunk($arr_date, 7);
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return mktime(0, 0, 0, $m, $d, $y);
}

function get_emp_id_str($mygroup_id){
  global $employees;
  $ids = $employees[$mygroup_id];
  $emp_ids = array();
  foreach($ids as $id){
    $emp_id = $id['emp_id'];
    array_push($emp_ids, $emp_id);
  }
  $rtn = join(", ", $emp_ids);
  return $rtn;
}

function get_emp_name_str($mygroup_id){
  global $employees;
  $ids = $employees[$mygroup_id];
  $names = array();
  foreach($ids as $id){
    $name = $id['name'];
    array_push($names, $name);
  }
  $rtn = join(", ", $names);
  return $rtn;
}
?>
