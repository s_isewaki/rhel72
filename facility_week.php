<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 週（表）</title>
<?
//ini_set("display_errors","1");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_week_common.ini");
require("holiday.php");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
$this_week = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
	$date = $this_week;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select facility_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$facility_default = pg_fetch_result($optsel, 0, "facility_default");

if ($facility_default === '2') {
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 処理週のスタート日のタイムスタンプを求める
$start_day = get_start_day($date, $start_wd);

// 前月・前週・翌週・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$last_week = strtotime("-7 days", $date);
$next_week = strtotime("+7 days", $date);
$next_month = get_next_month($date, $start_wd);

// 患者氏名/利用者氏名
$patient_name_title = $_label_by_profile["PATIENT_NAME"][$profile_type];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.view.date.value = dt;
	document.view.submit();
}

function popupReservationDetail(facility, date, time, title, type, text, user_count, reg_emp, upd_emp, e, with_patient, patient, show_type, ext, tmp_class) {

	var arr_value = new Array(
			'施設・設備', facility,
			'日付', date,
			'時刻', time,
			'タイトル', title
		);

	if (show_type) {
		arr_value.push( '種別', type );
	}
	if (with_patient) {
		arr_value.push( '<?=$patient_name_title?>', patient );
	}
	arr_value.push(
			'内容', text,
			'利用人数', user_count,
			'登録者', reg_emp + '<br />' + tmp_class,
			'登録者内線番号', ext,
			'最終更新者', upd_emp
		);

	popupDetailBlue(arr_value, 400, 100, e);

}

function openPrintWindow() {
	window.open('facility_print_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>', 'newwin', 'width=840,height=600,scrollbars=yes');
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'facility_csv.php';
    document.csv.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>週（表）</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrintWindow();"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="view" action="facility_week.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲
<select name="range" style="vertical-align:middle;" onchange="this.form.submit();"><? show_range_options($categories, $range); ?></select><img src="img/spacer.gif" width="50" height="1" alt=""><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_month); ?>">&lt;&lt;前月</a>
<a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_week); ?>">&lt;前週</a>
<a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($this_week); ?>">今週</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options($date, $start_day, $session, $range); ?></select>
<a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_week); ?>">翌週&gt;</a>
<a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_month); ?>">翌月&gt;&gt;</a>
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<?
$start_date = date("Ymd", $start_day);
$end_date = date("Ymd", strtotime("+6 days", $start_day));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

$holiday_memo_flg = false;
foreach($arr_calendar_memo as $tmp_date => $tmp_memo) {
	if ($tmp_memo != "" && strlen($tmp_date) == 8) {
		$y = substr($tmp_date, 0, 4);
		$m = substr($tmp_date, 4, 2);
		$d = substr($tmp_date, 6, 2);
		$holiday_name = ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
		if ($holiday_name != "") {
			$holiday_memo_flg = true;
			break;
		}
	}
}
if ($holiday_memo_flg == true) {
	$tmp_height = "58";
} else {
	$tmp_height = "40";
}
?>
<div style="height:<?=$tmp_height?>px; overflow: auto; border:#CCCCCC solid 1px;">
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="<?=($tmp_height +2) ?>">
<td colspan="2">&nbsp;</td>
<?
for ($i = 0; $i <= 6; $i++) {
	show_date($start_day, $start_wd, $i, $session, $range, $arr_calendar_memo);
}
?>
</tr>
<?
// 予約データをまとめて取得。キー：設備ID_年月日
$reservations = get_reservation($con, $categories, $range, $emp_id, $start_day, $date, $session, $fname);

// 1カテゴリでも予定内容を表示するがある場合
$show_content_flg = "f";
// カテゴリをループ
foreach ($categories as $tmp_category_id => $tmp_category) {
	$tmp_show_content = $tmp_category["show_content"];
	if ($tmp_show_content == "t") {
		$show_content_flg = "t";
		break;
	}
}
if ($show_content_flg == "t") {
	show_reservation($con, $categories, $range, $emp_id, $start_day, $date, $session, $fname, $reservations);
}
?>
</table>
</div>

<div style="height:500px; overflow: scroll;border:#CCCCCC solid 1px;">
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<? show_reservation($con, $categories, $range, $emp_id, $start_day, $date, $session, $fname, $reservations); ?>
</table>
</div>
</td>
</tr>
</table>
</body>
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo $start_date ?>">
    <input type="hidden" name="end_date" value="<?php echo $end_date ?>">
    <input type="hidden" name="range" value="<?php echo $range; ?>">
</form>

<? pg_close($con); ?>
</html>
<?
// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $start_day, $date, $session, $fname, $reservations) {
	$today = date("Ymd");

	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	$start_day_ymd = date("Ymd", $start_day);

	// カテゴリをループ
	foreach ($categories as $tmp_category_id => $tmp_category) {
		$tmp_category_name = $tmp_category["name"];
		$tmp_bg_color = $tmp_category["bg_color"];
		$show_content = $tmp_category["show_content"];

		// カテゴリ行を出力
		echo("<tr height=\"22\" bgcolor=\"#$tmp_bg_color\">\n");
		echo("<td colspan=\"16\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>$tmp_category_name</b></font></td>\n");
		echo("</tr>\n");

		// 設備をループ
		foreach ($tmp_category["facilities"] as $tmp_facility_id => $tmp_facility) {
			$tmp_facility_name = $tmp_facility["name"];
			$tmp_reservable = $tmp_facility["reservable"];
			$tmp_admin_flg = $tmp_facility["admin_flg"];
			$show_type = ($tmp_facility["show_type_flg"] == 't') ? "true" : "false";

			// 施設・設備行を出力
			echo("<tr height=\"22\">\n");
			echo("<td width=\"13%\" valign=\"top\" style=\"border-right-style:none;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('facility_detail.php?session=$session&facility_id=$tmp_facility_id', 'newwin', 'width=730,height=480,scrollbars=yes');\">$tmp_facility_name</a></font></td>\n");
			echo("<td width=\"3%\" valign=\"top\" style=\"border-left-style:none;\"><a href=\"facility_week_chart.php?session=$session&range=$range&date=$date&cate_id=$tmp_category_id&fcl_id=$tmp_facility_id\"><img src=\"img/icon/week.gif\" alt=\"チャート\" width=\"20\" height=\"20\" style=\"position:relative;top:-3px;border-style:none;\"></a></td>\n");

			// 1週間をループ
			for ($i = 0; $i <= 6; $i++) {
				if ($i > 0) {
					$tmp_date = strtotime("+$i days", $start_day);
				} else {
					$tmp_date = $start_day;
				}
				$ymd = date("Ymd", $tmp_date);
				$wd = date("w", $tmp_date);

				// 祝日の情報を取得 20141030TN
				$y = substr($ymd, 0, 4);
				$m = substr($ymd, 4, 2);
				$d = substr($ymd, 6, 2);
				$holiday_name = ktHolidayName(mktime(0, 0, 0, $m, $d, $y));

				// 予約情報を出力
				if ($ymd == $today) {
					$bgcolor = " bgcolor=\"#ccffcc\"";
				} else if ($wd == 0 || $holiday_name != "") {  // 日曜日もしくは祝日は背景色を赤にする 20141030TN
					$bgcolor = " bgcolor=\"#fadede\"";
				} else if ($wd == 6 ) {
					$bgcolor = " bgcolor=\"#defafa\"";
				} else {
					$bgcolor = "";
				}
				echo("<td height=\"22\" width=\"12%\" valign=\"top\"$bgcolor>\n");
				echo("<div style=\"position:relative;\">");
				echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
				// 予約情報配列を参照

				$tmp_reservation = $reservations["{$tmp_facility_id}_{$ymd}"];
				$rsv_count = count($tmp_reservation);

				if ($rsv_count > 0) {
					$first_flg = true;
					for ($j = 0; $j < $rsv_count; $j++) {
						echo("<tr>\n");

						// ボーダーの設定
						if ($j == 0) {
							if ($rsv_count == 1) {
								$border = "";
							} else {
								$border = "border-bottom-style:none;padding-bottom:2px;";
							}
						} else if ($j < $rsv_count - 1) {
							$border = "border-top:silver dotted 1px;border-bottom-style:none;padding-bottom:2px;";
						} else {
							$border = "border-top:silver dotted 1px;";
						}

						// ラベルセルの表示
						$tmp_rsv_id = $tmp_reservation[$j]["reservation_id"];
						$tmp_rsv_emp_id = $tmp_reservation[$j]["emp_id"];
						$tmp_s_time = $tmp_reservation[$j]["start_time"];
						$tmp_e_time = $tmp_reservation[$j]["end_time"];
						$tmp_title = $tmp_reservation[$j]["title"];

						$tmp_type1_nm = $tmp_reservation[$j]["type1_nm"];
						$tmp_type2_nm = $tmp_reservation[$j]["type2_nm"];
						$tmp_type3_nm = $tmp_reservation[$j]["type3_nm"];
						$tmp_content = $tmp_reservation[$j]["content"];
						$tmp_users = $tmp_reservation[$j]["users"];
						$tmp_reg_emp_nm = $tmp_reservation[$j]["reg_emp_nm"];
						$tmp_upd_emp_nm = $tmp_reservation[$j]["upd_emp_nm"];
						$tmp_pt_flg = $tmp_reservation[$j]["pt_flg"];
						$tmp_marker = $tmp_reservation[$j]["marker"];
						$tmp_ext = $tmp_reservation[$j]["emp_ext"];

						$sql = "select class_nm, atrb_nm, dept_nm, room_nm from empmst inner join classmst on empmst.emp_class = classmst.class_id inner join atrbmst on empmst.emp_attribute = atrbmst.atrb_id inner join deptmst on empmst.emp_dept = deptmst.dept_id left join classroom on empmst.emp_room = classroom.room_id";
						$cond = "where emp_id = '{$tmp_rsv_emp_id}'";
						$sel = select_from_table($con, $sql, $cond, $fname);
						if ($sel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$tmp_class = pg_fetch_result($sel, 0, "class_nm");
						$tmp_atrb = pg_fetch_result($sel, 0, "atrb_nm");
						$tmp_dept = pg_fetch_result($sel, 0, "dept_nm");
						$tmp_room = pg_fetch_result($sel, 0, "room_nm");

						$tmp_class_disp = "$tmp_class &gt; $tmp_atrb &gt; $tmp_dept";
						if ($tmp_room != "") {
							$tmp_class_disp .= " &gt; $tmp_room";
						}

						$tmp_fclhist_content_type = $tmp_reservation[$j]["fclhist_content_type"];

						// 内容編集
						if ($tmp_fclhist_content_type == "2") {
							$tmp_content = get_text_from_xml($tmp_content);
						}

						if ($tmp_s_time != "") {
							$tmp_s_time = substr($tmp_s_time, 0, 2) . ":" . substr($tmp_s_time, 2, 2);
							$tmp_e_time = substr($tmp_e_time, 0, 2) . ":" . substr($tmp_e_time, 2, 2);
							$time_str = "$tmp_s_time-$tmp_e_time<br>";
							$time_str_popup = "$tmp_s_time 〜 $tmp_e_time";
						} else {
							if ($first_flg == true) {
								$br_str = "<br>";
								$first_flg = false;
							} else {
								$br_str = "";
							}
							//
							$time_str = "$br_str<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
							$time_str_popup = "指定なし";
						}

						$tmp_date_str = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $ymd);
						$tmp_types = array();
						if ($tmp_type1_nm != "") {$tmp_types[] = $tmp_type1_nm;}
						if ($tmp_type2_nm != "") {$tmp_types[] = $tmp_type2_nm;}
						if ($tmp_type3_nm != "") {$tmp_types[] = $tmp_type3_nm;}
						$tmp_type_nm = join(", ", $tmp_types);
						$tmp_users = ($tmp_users != "") ? $tmp_users . "名" : "";
						$with_patients = ($tmp_pt_flg == "t") ? "true" : "false";
						if ($tmp_pt_flg == "t") {
							$tmp_patients = $tmp_reservation[$j]["pt_name"];
						}

						$style = get_style_by_marker($tmp_marker);

						if ($tmp_rsv_emp_id == $emp_id || $tmp_admin_flg) {
							$child = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=2&range=$range";
							$height = "'+((screen.availHeight-60<760)? screen.availHeight-60: 760)+'";
						} else {
							$child = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=2&range=$range";
							$height = "480";
						}
						if ($tmp_s_time == "") {
							$child .= "&timeless=t";
						}
						echo("<td height=\"22\" width=\"12%\" valign=\"top\"$bgcolor style=\"$border\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$time_str}<a href=\"javascript:void(0);\" onclick=\"window.open('$child', 'newwin', 'width=730,height=$height,scrollbars=yes');\" onmousemove=\"popupReservationDetail('$tmp_category_name &gt; $tmp_facility_name', '$tmp_date_str', '$time_str_popup', '" . h(h_jsparam($tmp_title)) . "', '$tmp_type_nm', '" . h(preg_replace("/\r?\n/", "<br>", h_jsparam($tmp_content))) . "', '$tmp_users', '$tmp_reg_emp_nm', '$tmp_upd_emp_nm', event, $with_patients, '$tmp_patients', $show_type, '$tmp_ext', '$tmp_class_disp');\" onmouseout=\"closeDetail();\"$style>" . h($tmp_title) . "</a>\n");
						// 予定内容を表示の場合
						if ($show_content == "t") {
							echo("<br>" . preg_replace("/\r?\n/", "<br>", h($tmp_content)));
						}
						echo("</font></td>\n");
						echo("</tr>\n");
					}
				} else {
					echo("<tr><td>&nbsp;</td></tr>");
				}
				echo("</table>\n");
				if ($tmp_reservable) {
					echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"1\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('facility_reservation_register.php?session=$session&cate_id=$tmp_category_id&facility_id=$tmp_facility_id&ymd=$ymd&path=2&range=$range', 'newwin', 'width=730,height='+((screen.availHeight-60<750)? screen.availHeight-60: 750)+' ,scrollbars=yes');\" style=\"cursor:pointer;position:absolute;top:1;right:1;\">");
				}
				echo("</div>\n");
				echo("</td>\n");
			}
			echo("</tr>");
		}
	}
}
?>
