<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="employee_condition_bulk_setting.php" method="post">
<?
foreach ($_POST as $key => $val) {
    if (is_array($val)) {
        foreach ($val as $val2) {
            echo("<input type=\"hidden\" name=\"{$key}[]\" value=\"$val2\">\n");
        }
    } else {
        echo("<input type=\"hidden\" name=\"$key\" value=\"$val\">\n");
    }
}
?>
<input type="hidden" name="result" value="">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if (!is_array($tgt_job)) {
    echo("<script type=\"text/javascript\">alert('職種が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!is_array($tgt_st)) {
    echo("<script type=\"text/javascript\">alert('役職が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 登録値の編集
if ($irrg_app == "1") {$irrg_type = "";}
if ($hol_minus != "t") {$hol_minus = "f";}
if (empty($atdptn_id) || $atdptn_id == "--"){$atdptn_id = null;}
if ($no_overtime != "t") {$no_overtime = "f";}
if ($closing == "--"){$closing = null;}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 変則労働適用時の所定労働時間が登録されているかチェック
if ($irrg_type != "") {
    $col_name = ($irrg_type == "1") ? "irrg_time_w" : "irrg_time_m";
    $sql = "select $col_name from timecard";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $irrg_time = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, $col_name) : "";
    if ($irrg_time == "") {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('変則労働適用時の所定労働時間が登録されていません。\\nマスター管理者にご連絡ください。');</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
}

// 処理対象の職員一覧を取得
$cond_detail = "";
if (is_array($tgt_job)) {
    $cond_detail .= " and emp_job in (" . implode(", ", $tgt_job) . ")";
}
if (is_array($tgt_st)) {
    $cond_detail .= " and emp_st in (" . implode(", ", $tgt_st) . ")";
}
if ($tgt_class != "") {
    $cond_detail .= " and emp_class = $tgt_class";
}
if ($tgt_atrb != "") {
    $cond_detail .= " and emp_attribute = $tgt_atrb";
}
if ($tgt_dept != "") {
    $cond_detail .= " and emp_dept = $tgt_dept";
}
if ($tgt_room != "") {
    $cond_detail .= " and emp_room = $tgt_room";
}
$cond_detail = substr($cond_detail, 5);
$sql = "select emp_id from empmst";
$cond = "where $cond_detail and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query("rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 該当職員をループ
while ($row = pg_fetch_array($sel)) {
    // 現在の勤務条件を取得
    $sql = "SELECT * from empcond WHERE emp_id = '{$row["emp_id"]}'";
    $sel_condition = select_from_table($con, $sql, "", $fname);
    if ($sel_condition == 0) {
        pg_query("rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // データ
    $data = array(
        'emp_id'                => $row["emp_id"],
        'wage'                  => $wage,
        'tmcd_group_id'         => $tmcd_group_id,
        'atdptn_id'             => ($atdptn_id == "") ? null : $atdptn_id,
        'irrg_type'             => $irrg_type,
        'hol_minus'             => $hol_minus,
        'duty_form'             => $duty_form,
        'no_overtime'           => $no_overtime,
        'paid_hol_tbl_id'       => $paid_hol_tbl_id,
        'csv_layout_id'         => $csv_layout_id,
        'closing'               => $closing,
//        'unit_price1'           => $unit_price1,
//        'unit_price2'           => $unit_price2,
//        'unit_price3'           => $unit_price3,
//        'unit_price4'           => $unit_price4,
//        'specified_time'        => $specified_time,
//        'paid_hol_start_date'   => $paid_hol_start_date,
//        'am_time'               => $am_time,
//        'pm_time'               => $pm_time,
//        'unit_price5'           => $unit_price5,
    );

    // UPDATE
    if (pg_num_rows($sel_condition) > 0) {
        $row = pg_fetch_assoc($sel_condition);

        if ($wage_omit == "t") {
            $data["wage"] = $row["wage"];
        }
        if ($tmcd_group_id_omit == "t") {
            $data["tmcd_group_id"] = $row["tmcd_group_id"];
        }
        if ($atdptn_id_omit == "t") {
            $data["atdptn_id"] = $row["atdptn_id"];
        }
        if ($irrg_app_omit == "t") {
            $data["irrg_type"] = $row["irrg_type"];
            $data["hol_minus"] = $row["hol_minus"];
        }
        if ($duty_form_omit == "t") {
            $data["duty_form"] = $row["duty_form"];
        }
        if ($no_overtime_omit == "t") {
            $data["no_overtime"] = $row["no_overtime"];
        }
        if ($paid_hol_tbl_id_omit == "t") {
            $data["paid_hol_tbl_id"] = $row["paid_hol_tbl_id"];
        }
        if ($csv_layout_id_omit == "t") {
            $data["csv_layout_id"] = $row["csv_layout_id"];
        }
        if ($closing_omit == "t") {
            $data["closing"] = $row["closing"];
        }

        // 勤務条件レコードを更新
        $sql = "UPDATE empcond SET";
        $cond = "WHERE emp_id='{$data["emp_id"]}'";
        unset($data['emp_id']);
        $upd = update_set_table($con, $sql, array_keys($data), array_values($data), $cond, $fname);
        if ($upd == 0) {
            pg_query("rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    // INSERT
    else {
        // 勤務条件レコードを作成
        $sql = "INSERT INTO empcond (" . implode(",",array_keys($data)) . ") values (";
        $ins = insert_into_table($con, $sql, array_values($data), $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 権限一括設定画面を再表示
echo("<script type=\"text/javascript\">document.items.result.value = 't';</script>");
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
