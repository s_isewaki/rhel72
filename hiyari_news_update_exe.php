<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");
?>
<body>
<form name="items" action="hiyari_news_update.php" method="post">
<input type="hidden" name="news_title" value="<? echo(h($news_title)) ?>">
<input type="hidden" name="news_category" value="<? echo($news_category) ?>">
<input type="hidden" name="class_id" value="<? echo($class_id) ?>">
<input type="hidden" name="job_id" value="<? echo($job_id) ?>">
<input type="hidden" name="news" value="<? echo(h($news)) ?>">
<input type="hidden" name="record_flg" value="<? echo($record_flg) ?>">
<input type="hidden" name="news_begin1" value="<? echo($news_begin1) ?>">
<input type="hidden" name="news_begin2" value="<? echo($news_begin2) ?>">
<input type="hidden" name="news_begin3" value="<? echo($news_begin3) ?>">
<input type="hidden" name="news_end1" value="<? echo($news_end1) ?>">
<input type="hidden" name="news_end2" value="<? echo($news_end2) ?>">
<input type="hidden" name="news_end3" value="<? echo($news_end3) ?>">
<input type="hidden" name="emp_name" value="<? echo($emp_name) ?>">
<input type="hidden" name="news_date1" value="<? echo($news_date1) ?>">
<input type="hidden" name="news_date2" value="<? echo($news_date2) ?>">
<input type="hidden" name="news_date3" value="<? echo($news_date3) ?>">
<input type="hidden" name="session" value="<? echo($session) ?>">
<input type="hidden" name="news_id" value="<? echo($news_id) ?>">
<input type="hidden" name="page" value="<? echo($page) ?>">
<input type="hidden" name="marker" value="<? echo($marker) ?>">
<input type="hidden" name="back" value="t">
<?
foreach ($filename as $tmp_filename) {
    echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
    echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
?>
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
$news_title = trim($news_title);
if ($news_title == "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($news_title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news = trim($news);
if ($news == "") {
    echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
//if (strlen($news) > 500) {
//  echo("<script type=\"text/javascript\">alert('内容が長すぎます。');</script>");
//  echo("<script type=\"text/javascript\">document.items.submit();</script>");
//  exit;
//}
if (!checkdate($news_begin2, $news_begin3, $news_begin1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（From）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_end2, $news_end3, $news_end1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（To）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news_begin = "$news_begin1$news_begin2$news_begin3";
$news_end = "$news_end1$news_end2$news_end3";
if ($news_begin > $news_end) {
    echo("<script type=\"text/javascript\">alert('掲載期間が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_date2, $news_date3, $news_date1)) {
    echo("<script type=\"text/javascript\">alert('登録日が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 添付ファイルの確認
create_newsfile_folder();
$newsfile_folder_name = get_newsfile_folder_name();
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $ext = strrchr($filename[$i], ".");

    $tmp_filename = "{$newsfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
    if (!is_file($tmp_filename)) {
        echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
        echo("<script language=\"javascript\">document.items.submit();</script>");
        exit;
    }
}



// 登録値の編集
switch ($news_category) {
case "1":  // 全館
    $class_id = null;
    $job_id = null;
    break;
case "2":  // 部門
    $job_id = null;
    break;
case "3":  // 職種
    $class_id = null;
    break;
}

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// お知らせ情報を更新
$sql = "update inci_news set";
$set = array("date", "news_date", "news_begin", "news_end", "news_title", "news_category", "news", "class_id", "job_id", "record_flg", "marker");
$setvalue = array(date("YmdHi"), "$news_date1$news_date2$news_date3", $news_begin, $news_end, pg_escape_string($news_title), $news_category, pg_escape_string($news), $class_id, $job_id, $record_flg,$marker);
$cond = "where news_id = $news_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}


// 添付ファイル情報を削除
$sql = "delete from inci_newsfile";
$cond = "where news_id = '$news_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
    $sql = "insert into inci_newsfile (news_id, newsfile_no, newsfile_name) values (";
    $content = array($news_id, $no, pg_escape_string($tmp_filename));
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $no++;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
foreach (glob("{$newsfile_folder_name}/{$news_id}_*.*") as $tmpfile) {
    unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $tmp_fileno = $i + 1;
    $ext = strrchr($tmp_filename, ".");

    $tmp_filename = "{$newsfile_folder_name}/tmp/{$session}_{$tmp_file_id}{$ext}";
    copy($tmp_filename, "{$newsfile_folder_name}/{$news_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("{$newsfile_folder_name}/tmp/{$session}_*.*") as $tmpfile) {
    unlink($tmpfile);
}



// お知らせ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'hiyari_news_menu.php?session=$session&page=$page';</script>");
?>
</body>
