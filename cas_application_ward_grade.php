<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cas_common.ini");
require_once("cas_application_common.ini");
require_once("cas_application_workflow_common_class.php");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CAS_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


$obj = new cas_application_workflow_common_class($con, $fname);
//====================================
// 初期化処理
//====================================
//モード 1:本人 2:指導者 3:師長
if ($mode == "") {
	$mode = "1";
}

// ログインユーザの職員IDを取得
$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];

if($defalut == "on")
{
    $apply_emp_id = $login_emp_id;
    $apply_emp_nm = $arr_empmst[0]["emp_lt_nm"]." ".$arr_empmst[0]["emp_ft_nm"];
}

// 指導者
$grader_flg = $obj->is_grader($login_emp_id);
// 師長
$nursing_commander_flg = $obj->is_nursing_commander($login_emp_id);

$emplist_btn_flg = false;
if($mode == "1") {
	if($obj->is_particular_employee($login_emp_id) || $grader_flg || $nursing_commander_flg)
	{
	    // 職員名簿ボタン活性にする
	    $emplist_btn_flg = true;
	}
}

$cas_title = get_cas_title_name();

//評価期間中をデフォルトとする
if ($term == "") {
	$term = "1";
}
//対象者取得
if ($mode == "2") {
	$arr_emp = get_arr_emp($con, $fname, $login_emp_id, $term);
} elseif ($mode == "3") {
	$arr_emp = get_arr_emp_head_nurse($con, $fname, $login_emp_id, $term, $obj);
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | 病棟評価表</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function ward_grade_search()
{
    document.apply.action = "cas_application_ward_grade.php?session=<?=$session?>&mode=<?=$mode?>&emp_id=<?=$emp_id?>";
    document.apply.submit();
}

var childwin = null;
function openEmployeeList(input_div) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cas_emp_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div + '&call_screen=WARD_GRADE';
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id, emp_name)
{
    var emp_ids = emp_id.split(", ");
    var emp_names = emp_name.split(", ");

    if (emp_ids.length > 1 || emp_names.length > 1){
        document.apply.apply_emp_id.value = '';
        document.apply.apply_emp_nm.value = '';
    }
    else {
        document.apply.apply_emp_id.value = emp_ids[0];
        document.apply.apply_emp_nm.value = emp_names[0];
    }

    document.apply.action="cas_application_ward_grade.php?session=<?=$session?>";
	document.apply.submit();
}

function show_ward_achivement()
{
<?
if ($mode == "1") {
?>
    emp_id = document.getElementById('apply_emp_id').value;
<? } else { ?>
    emp_id = '<?=$emp_id?>';
<? } ?>
    var url = "cas_ward_achivement_regist.php?session=<?=$session?>&emp_id="+emp_id;
    show_sub_window(url);
}

function show_sub_window(url)
{
    var h = '600';
    var w = '980';

    var win_width = (screen.width - w) / 2;
    var win_height = (screen.height - h) / 2;

    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left="+win_width+",top="+win_height+",width=" + w + ",height=" + h;
    childwin = window.open(url, 'wardwin',option);
    childwin.focus();
}

function change_achivement_period(order, status, level)
{
    document.getElementById('order').value = order;
    document.getElementById('cond_status').value = status;
    document.getElementById('cond_level').value = level;
//    document.getElementById('emp_id').value = '<?=$emp_id?>';
    document.apply.action="cas_application_ward_grade.php?session=<?=$session?>&mode=<?=$mode?>&emp_id=<?=$emp_id?>";
    document.apply.submit();
}
// この関数は別画面から呼び出されます。
function reload_page()
{
    location.href="cas_application_ward_grade.php?session=<?=$session?>&defalut=on&mode=<?=$mode?>&emp_id=<?=$emp_id?>";
}

function change_term() {
    document.apply.action="cas_application_ward_grade.php?session=<?=$session?>&mode=<?=$mode?>";
    document.apply.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list th {border:#A0D25A solid 1px;}
.list td {border:#A0D25A solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}
table.block_in th {border:#A0D25A solid 0px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_application_ward_grade.php?session=<? echo($session); ?>"><b>病棟評価表</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
show_applycation_menuitem($session,$fname,"");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" id="order" name="order" value="<?=$order?>">

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<? //指導者か師長の場合
if ($grader_flg || $nursing_commander_flg) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr>
<td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($mode == "1") { ?>
<b>本人用</b>
<? } else { ?>
<a href="cas_application_ward_grade.php?session=<? echo($session); ?>&mode=1&defalut=on">本人用</a>
<? } ?>
</font></td>
<?
if ($grader_flg) {
?>
<td width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($mode == "2") { ?>
<b>指導者用</b>
<? } else { ?>
<a href="cas_application_ward_grade.php?session=<? echo($session); ?>&mode=2">指導者用</a>
<? } ?>
</font></td>
<? } ?>
<?
if ($nursing_commander_flg) {
?>
<td width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($mode == "3") { ?>
<b>師長用</b>
<? } else { ?>
<a href="cas_application_ward_grade.php?session=<? echo($session); ?>&mode=3">師長用</a>
<? } ?>
</font></td>
<? } ?>
<td width="500"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td>

<!-- 入力項目START -->
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td width="120" bgcolor="#E5F6CD">
<?
if ($mode == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font>
<?
} else {
?>
<select name="term" onchange="change_term();">
<option value="1" <? if ($term == "1") echo("selected");?>>評価期間中</option>
<option value="2" <? if ($term == "2") echo("selected");?>>前年度評価分</option>
<option value="3" <? if ($term == "3") echo("selected");?>>全て</option>
</select>
<? } ?>
</td>
<td width="300">
<?
if ($mode == "1") {
?>
<input type="text" size="30" maxlength="30" id="apply_emp_nm" name="apply_emp_nm" value="<?=htmlspecialchars($apply_emp_nm)?>" readonly="readonly"><input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');" <?if(!$emplist_btn_flg){?>disabled<?}?>>
<?
} else {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
foreach ($arr_emp as $tmp_id => $tmp_name) {

	if ($tmp_id == $emp_id) {
		echo("<b>$tmp_name</b>&nbsp;");
	} else {
		echo("<a href=\"cas_application_ward_grade.php?session=$session&mode=$mode&emp_id=$tmp_id&term=$term\">$tmp_name</a>&nbsp;");
	}
}
?>
</font>
<? } ?>
<input type="hidden" id="apply_emp_id" name="apply_emp_id" value="<?=$apply_emp_id?>">
</td>
<td width="120" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象</font></td>
<td>
<select name="cond_status" id="cond_status">
<?show_status_options($cond_status)?>
</select>
<select name="cond_level" id="cond_level">
<?show_level_options($cond_level)?>
</select>
<select name="cond_category" id="cond_category" onchange="ward_grade_search();">
<?show_category_options($cond_category)?>
</select>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr align="right"><td>
<input type="button" value="表示" onclick="ward_grade_search();">
</td>
</tr>
</table>
<!-- 入力項目END -->
</td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="1">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- 達成期間START -->
<td width="150" valign="top">

<table width="150" border="0" cellspacing="0" cellpadding="1" class="list">
<tr height="15">
<th bgcolor="#E5F6CD" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成期間</font>
</th>
</tr>

<tr>
<td align="center">
<input type="button" value="新規作成・変更" onclick="show_ward_achivement();">
</td>
</tr>

<tr>
<td valign="top" height="300">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<?
if ($mode == "1") {
	$tmp_emp_id = $apply_emp_id;
} else {
	$tmp_emp_id = $emp_id;
}

$arr_achievement_period = $obj->get_cas_achievement_period($tmp_emp_id, "", "", "");

if($defalut != "on")
{
	if(count($arr_achievement_period) > 0)
	{
	    if($order == "")
	    {
	        $order = "1";
	    }
	}
}
foreach($arr_achievement_period as $period)
{
	$start_year  = substr($period["start_period"], 0, 4);
	$start_month = substr($period["start_period"], 4, 2);

	$end_year  = substr($period["end_period"], 0, 4);
	$end_month = substr($period["end_period"], 4, 2);

	$period_name = $start_year."/".$start_month."〜".$end_year."/".$end_month;

    $achievement_order = $period["achievement_order"];

    $status = $period["status"];
    $level = $period["level"];

    $selected_td_bgcolor = "";
    $a_style = "";
    if($achievement_order == $order)
    {
        $date_y1 = $start_year;
        $date_m1 = $start_month;
        $date_y2 = $end_year;
        $date_m2 = $end_month;

        $selected_td_bgcolor = 'bgcolor="#FFDDFD"';
        $a_style = 'style="color:#000000;font-weight:bold"';
    }
?>

<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■</font>
</td>
<td <?=$selected_td_bgcolor?>>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<a href="javascript:change_achivement_period('<?=$achievement_order?>', '<?=$status?>', <?=$level?>);" <?=$a_style?>>
<?=$period_name?>
</a>
</font>

</td>
<td width="5">
&nbsp;
</td>
</tr>
<?
}
?>
</table>
</td>
</tr>
</table>
</td>

<td width="5">
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>

<!-- 達成期間END -->

<!-- 病棟評価表START -->
<td valign="top">
<?
search_ward_grade($con, $session, $fname, $cond_status, $cond_level, $cond_category, pg_escape_string($tmp_emp_id), $date_y1, $date_m1, $date_y2, $date_m2, $obj);
?>
</td>
<!-- 病棟評価表END -->

</tr>
</table>

</form>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</body>
<? pg_close($con); ?>
</html>

<?
// 状況オプションを出力
function show_status_options($status) {

	$status_nm = array("急性期","回復期");
	$status_id = array("1","2");

//	echo('<option value="">');
	for($i=0;$i<count($status_nm);$i++) {

		echo("<option value=\"$status_id[$i]\"");
		if($status == $status_id[$i]) {
			echo(" selected");
		}
		echo(">$status_nm[$i]\n");
	}
}

// レベルオプションを出力
function show_level_options($level) {

	$level_nm = array("レベルI","レベルII","レベルIII","レベルIV");
	$level_id = array("1","2","3","4");

//	echo('<option value="">');
	for($i=0;$i<count($level_nm);$i++) {

		echo("<option value=\"$level_id[$i]\"");
		if($level == $level_id[$i]) {
			echo(" selected");
		}
		echo(">$level_nm[$i]\n");
	}
}

// カテゴリオプションを出力
function show_category_options($category) {

	$category_nm = array(
		"ナーシングプロセス",
		"教育能力自己学習能力",
		"リーダーシップ能力",
		"専門職業としての自覚行動"
	);
	$category_id = array("1","2","3","4");

//	echo('<option value="">');
	for($i=0;$i<count($category_nm);$i++) {

		echo("<option value=\"$category_id[$i]\"");
		if($category == $category_id[$i]) {
			echo(" selected");
		}
		echo(">$category_nm[$i]\n");
	}
}


// 病棟評価一覧を検索・表示する。
function search_ward_grade($con, $session, $fname,
							$status, $level, $category, $apply_emp_id,
							$date_y1, $date_m1, $date_y2, $date_m2, $obj)
{

	if($apply_emp_id == "" || $date_y1 == "" || $date_m1 == "" || $date_y2 == "" || $date_m2 == "")
    {
		return;
    }

	require_once("cas_common.ini");

	// テンプレートを元に表示行うため、該当テンプレートを取得
    $kanri_no = get_kanri_no($status.$level.$category);
	$short_wkfw_name = pg_escape_string($kanri_no);

	$tmpl_sql = "select wkfw_content from cas_wkfwmst where short_wkfw_name = '{$short_wkfw_name}'";
	$tmpl_res = select_from_table($con, $tmpl_sql, "", $fname);
	if ($tmpl_res == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$tmpl_data = pg_result($tmpl_res, 0, "wkfw_content");
	if (!$tmpl_data) return;

	// 値の取得・表示用に年月を整形・作成する
	$date1 = $date_y1 . $date_m1;
	$date2 = $date_y2 . $date_m2;

	$show_ym = array();
	$tmp_y  = $date_y1;
	$tmp_m  = $date_m1;
	$tmp_ym = $tmp_y . $tmp_m;
	$show_ym[] = array('y' => $tmp_y, 'm' => $tmp_m, 'ym' => $tmp_ym);
	while ($tmp_ym < $date2){
		$tmp_m = sprintf("%02d", $tmp_m + 1);
		if (!($tmp_m % 13 )){
			$tmp_y++;
			$tmp_m = "01";
		}
		$tmp_ym = $tmp_y . $tmp_m;
		$show_ym[] = array('y' => $tmp_y, 'm' => $tmp_m, 'ym' => $tmp_ym);
	}

    $ward_ym = array();
    $total = "0";
    // 年月単位の病棟評価申請回数
    foreach($show_ym as $ym)
    {
        $ward_exist_count = $obj->get_ward_exist_count("cas_wkfw_".$kanri_no, $apply_emp_id, $ym["y"], $ym["m"]);
        if($ward_exist_count > 0)
        {
            $total = $total + $ward_exist_count;
            $ward_ym[] = array($ym["y"] => $ym["y"], 'm' => $ym["m"], 'ym' => $ym["y"].$ym["m"], 'ward_exist_count' => $total);
        }
//        $show_ym_apply_cnt[] = $obj->get_ward_exist_count("cas_wkfw_".$kanri_no, $apply_emp_id, $ym["y"], $ym["m"]);
    }


	// CAS_DBより値の取得
    $cas_data = $obj->get_ward_grade_data($apply_emp_id, $short_wkfw_name, $date1, $date2);

/*
	$cas_sql = <<< EOC
select a.eval_year, a.eval_month, a.eval_year || to_char(a.eval_month, 'FM00') AS eval_date, a.var_name, a.value from cas_wkfw_$kanri_no a
where a.emp_id = '{$apply_emp_id}'
and a.eval_year || to_char(a.eval_month, 'FM00') between $date1 and $date2
and a.kanri_no = '{$short_wkfw_name}'
and exists(select * from cas_apply b where b.apply_id = a.apply_id and not b.delete_flg)
order by a.eval_year, a.eval_month, a.cas_val_id
EOC;

	$cas_res = select_from_table($con, $cas_sql, "", $fname);
	$cas_data = pg_fetch_all($cas_res);
*/
	// テンプレート内の不要な部分を除去
	$tmpl_data = preg_replace('/^<table.*?>.*?<\/table>/s', '', $tmpl_data);
	$tmpl_data = preg_replace('/<\?.*?\?>/s', '', $tmpl_data);
    $tmpl_data = ereg_replace("<script .*</script>", "", $tmpl_data);

	// 表示形式を他のページと合わせる
	$tmpl_data = preg_replace('/<table.*?>/s', '<table width="500" border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed;" class="list">', $tmpl_data, 1);
	$tmpl_data = preg_replace('/<tr>/s', '<tr bgcolor="#E5F6CD">', $tmpl_data, 1);

	$tr_blocks = explode('</tr>', $tmpl_data);

	// ヘッダ部の表示
//	global $CAS_WARD_GRADE_HEADER_PREG_TARGET;
//	$preg_target = $CAS_WARD_GRADE_HEADER_PREG_TARGET;
	$preg_target = '/評価<\/font>/s';
	$preg_td = '/<td/s';
	$tr_header = array_shift($tr_blocks);
	$td_header = explode('</td>', $tr_header);


	foreach ($td_header AS $td){
		if (preg_match($preg_target, $td)){
			break;
		}

//echo(htmlspecialchars($td));
//echo("<BR>");
		// 表示幅の調整
/*
		if (preg_match('/<br>/', $td))
		{
//		    $td = preg_replace($preg_td, '<td width="20"', $td);
		}
*/
		if(preg_match('/項目/s', $td))
		{
		    $td = preg_replace($preg_td, '<th width="150" rowspan="2"', $td);
		}
		else if(preg_match('/実践目標/s', $td))
		{
		    $td = preg_replace($preg_td, '<th width="200" rowspan="2"', $td);
		}
		else if(preg_match('/評価者/s', $td))
		{
		    $td = preg_replace($preg_td, '<th width="60" rowspan="2"', $td);
		}
        else
        {
		    $td = preg_replace($preg_td, '<th width="50" rowspan="2"', $td);
        }
		echo($td."</th>\n");
	}

    // 月
    echo('<th width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></th>'."\n");
/*
	foreach ($show_ym AS $ym){
		echo('<th width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'. ($ym['m']+0) . "</font></th>\n");
	}
*/
	foreach ($ward_ym AS $ym){
		echo('<th width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'. ($ym['m']+0) . "</font></th>\n");
	}


	echo('</tr>');

	echo('<tr bgcolor="#E5F6CD">');
    // 回
    echo('<th width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回</font></th>'."\n");

	foreach ($ward_ym AS $ym){
		echo('<td align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'.$ym['ward_exist_count']."</font></td>\n");
	}
	echo('</tr>');

	// データ部の表示
	$preg_target = '/ name="(.+?)"/s';

    $idx1 = 1;

	foreach ($tr_blocks AS $tr){
		$td_blocks = explode('</td>', $tr);
		$match_name = array();

		// テンプレート部から必要な部分だけを表示
		foreach ($td_blocks AS $td){
			if (preg_match($preg_target, $td, $match_name)){
				break;
			}
			echo($td . '</td>');
		}


		// CAS_DBの値を表示
		if (count($match_name) > 0){

            $ward_summary_text = "";
            $ward_summary = get_ward_summary($match_name[1], $cas_data);
            $ward_summary_text = get_ratoku_text($ward_summary);
        	// 指導者評価行を薄い青色で表示
	        if($idx1 % 2 == 0)
	        {
		        echo('<td align="center" width="20" bgcolor="#B0C4DE"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'."\n");
	        }
	        else
	        {
		        echo('<td align="center" width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">'."\n");
	        }
			echo($ward_summary_text);
			echo("</font></td>\n");

			foreach ($ward_ym AS $ym){
				$show_text = "";

                $tmp_value = "";
				foreach ($cas_data AS $cas){

					if ($ym['ym'] == $cas['eval_date'] && $match_name[1] == $cas['var_name'])
					{
                        // 「○」の場合
                        if($cas['var_name'] == "0")
                        {
                            $tmp_value = $cas['value'];
                            break;
                        }
                        //「△」「×」の場合
                        else
                        {
                            if($tmp_value != "")
                            {
		                        if($tmp_value > $cas['value'])
		                        {
                                    $tmp_value = $cas['value'];
		                        }
                            }
                            else
                            {
	                            $tmp_value = $cas['value'];
                            }
                        }
					}
				}

                $show_text = get_ratoku_text($tmp_value);

                // 指導者評価行を薄い青色で表示
		        if($idx1 % 2 == 0)
		        {
				    echo('<td align="center" bgcolor="#B0C4DE"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">');
		        }
		        else
		        {
				    echo('<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">');
		        }
				echo($show_text);
				echo("</font></td>\n");

			}
		}
		echo('</tr>');
        $idx1++;
	}
echo('</table>');
}

function get_kanri_no($cd)
{
    $arr = array(
                 "111" => "c001", "112" => "c002", "113" => "c003", "114" => "c004",
                 "121" => "c005", "122" => "c006", "123" => "c007", "124" => "c008",
                 "131" => "c009", "132" => "c010", "133" => "c011", "134" => "c012",
                 "141" => "c013", "142" => "c014", "143" => "c015", "144" => "c016",
                 "211" => "c017", "212" => "c018", "213" => "c019", "214" => "c020",
                 "221" => "c021", "222" => "c022", "223" => "c023", "224" => "c024",
                 "231" => "c025", "232" => "c026", "233" => "c027", "234" => "c028",
                 "241" => "c029", "242" => "c030", "243" => "c031", "244" => "c032"
                 );

    return $arr[$cd];
}


function get_ward_summary($var_name, $arr_cas_data)
{
    $tmp_value = "";
	foreach($arr_cas_data as $cas)
    {
        if($var_name == $cas["var_name"])
        {
            if($cas["value"] == "0")
            {
                $tmp_value = $cas["value"];
                break;
            }
	        //「△」「×」の場合
	        else
	        {
	            if($tmp_value != "")
	            {
	                if($tmp_value > $cas['value'])
	                {
	                    $tmp_value = $cas['value'];
	                }
	            }
	            else
	            {
	                $tmp_value = $cas['value'];
	            }
	        }
        }
    }
    return $tmp_value;
}

//指導対象者取得
//$term 1:評価期間中 2:前年度評価分 3:全て
function get_arr_emp($con, $fname, $emp_id, $term) {

	$sql = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm ";
	$sql .= "from cas_achievement_period a ";
	$sql .= "left join empmst b on b.emp_id = a.emp_id ";
	$cond = "where a.approval_emp_ids like '%$emp_id%' ";
	//評価期間中
	if ($term == "1") {
		$yyyymm = date("Ym");
		$cond .= " and a.start_period <= '$yyyymm' and '$yyyymm' <= a.end_period ";
	} else if ($term == "2") {
		$this_year = date("Y");
		$last_year = $this_year - 1;
		$cond .= " and a.start_period <= '{$last_year}04' and '{$this_year}03' <= a.end_period ";
	}
	$cond .= "order by b.emp_kn_lt_nm, b.emp_kn_ft_nm";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);

	$arr_emp = array();
	for ($i=0; $i<$num; $i++) {
		$id = pg_result($sel, $i, "emp_id");
		$name = pg_result($sel, $i, "emp_lt_nm")." ".pg_result($sel, $i, "emp_ft_nm");
		$arr_emp[$id] = $name;
	}

	return $arr_emp;

}


//師長用対象者取得
//$term 1:評価期間中 2:前年度評価分 3:全て
function get_arr_emp_head_nurse($con, $fname, $emp_id, $term, $obj) {

	$arr_emp = array();
	// 病棟評価表・職員参照範囲設定情報取得
	$class_div = $obj->get_cas_nursing_commander_class();
	if($class_div == "")
	{
	     // 組織階層情報取得
	     $arr_class_name = get_class_name_array($con, $fname);
	     $class_div = $arr_class_name["class_cnt"];
	}

	// 部署役職取得(複数部署対応)
	switch($class_div)
	{
		case "1":
			$class_column = "emp_class";
			break;
		case "2":
			$class_column = "emp_attribute";
			break;
		case "3":
			$class_column = "emp_dept";
			break;
		case "4":
			$class_column = "emp_room";
			break;
	}

	//評価期間中／前年度評価分
	if ($term == "1" || $term == "2") {
		$sql = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm ";
		$sql .= "from cas_achievement_period a ";
		$sql .= "left join empmst b on b.emp_id = a.emp_id ";
	} else {
	//全て
		$sql = "select b.emp_id, b.emp_lt_nm, b.emp_ft_nm ";
		$sql .= "from empmst b ";
	}

	$cond = "where (exists (select * from ";
	$cond .= "(";
	$cond .= "select * from empmst where exists (select st_id from stmst where stmst.st_id = empmst.emp_st and stmst.st_nm like '%師長%') and empmst.emp_id = '$emp_id' ";
	$cond .= ") emp_sub1 ";
	$cond .= "where emp_sub1.$class_column = b.$class_column) ";

	$cond .= "or exists (select * from ";
	$cond .= "(";
	$cond .= "select * from concurrent where exists (select st_id from stmst where stmst.st_id = concurrent.emp_st and stmst.st_nm like '%師長%') and concurrent.emp_id = '$emp_id' ";
	$cond .= ") emp_sub2  ";
	$cond .= "where emp_sub2.$class_column = b.$class_column)) ";

	//評価期間中
	if ($term == "1") {
		$yyyymm = date("Ym");
		$cond .= " and a.start_period <= '$yyyymm' and '$yyyymm' <= a.end_period ";
	//前年度評価分
	} else if ($term == "2") {
		$this_year = date("Y");
		$last_year = $this_year - 1;
		$cond .= " and a.start_period <= '{$last_year}04' and '{$this_year}03' <= a.end_period ";
	//全て
	} else {
		$cond  .= " and b.emp_id != '$emp_id' ";
	}
	$cond .= "order by b.emp_kn_lt_nm, b.emp_kn_ft_nm";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);

	for ($i=0; $i<$num; $i++) {
		$id = pg_result($sel, $i, "emp_id");
		$name = pg_result($sel, $i, "emp_lt_nm")." ".pg_result($sel, $i, "emp_ft_nm");
		$arr_emp[$id] = $name;
	}

	return $arr_emp;

}

?>
