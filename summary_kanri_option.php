<? ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	// 役職登録権限チェック
	$checkauth = check_authority($session,59,$fname);
	if(!$checkauth) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

if (@$_REQUEST["mode"] == "regist"){
	$sel = select_from_table($con,"select * from summary_option","",$fname);
	$row = pg_fetch_array($sel);
	if (!$row) update_set_table($con, "insert into summary_option (is_activate_access_log) values (null);", array(), null, "", $fname);

	$sql =
		" update summary_option set".
		" is_hide_checklist_window = " . (int)$_REQUEST["is_hide_checklist_window"].
		",is_hide_eiyou_monsin_window = " . (int)$_REQUEST["is_hide_eiyou_monsin_window"].
		",is_activate_access_log = " . (int)$_REQUEST["is_activate_access_log"].
		",is_use_top_information_html = " . (int)$_REQUEST["is_use_top_information_html"].
		",top_information_html = '" . pg_escape_string($_REQUEST["top_information_html"])."'".
		",is_hide_byoureki_window = " . (int)$_REQUEST["is_hide_byoureki_window"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	ob_end_clean();
	header("Location: summary_kanri_option.php?session=".$session);
	die;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';




// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo($med_report_title); ?>管理 | オプション</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
	function popupUserImageViewer(){
		window.open("summary_user_image_viewer.php?session=<?=$session?>","",'width=600,height=600,scrollbars=no');
	}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th { border:#5279a5 solid 1px; white-space:nowrap; font-weight:normal; }
.list td { border:#5279a5 solid 1px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_OPTION"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#5279a5"><a href="./summary_kanri_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>


<?//==========================================================================?>
<?// 設定項目                                                                 ?>
<?//==========================================================================?>
<? $sel = select_from_table($con,"select * from summary_option","",$fname); ?>
<? $row = @pg_fetch_array($sel); ?>
<form action="summary_kanri_option.php" method="get">
<input type="hidden" name="session" value="<?=$session ?>">
<input type="hidden" name="mode" value="regist">
<table class="list" style="width:700px">
<tr>
	<td><?=$font?>病歴画面</font></td>
	<td><?=$font?>
		<label><input type="radio" name="is_hide_byoureki_window" value="0" <?=@$row["is_hide_byoureki_window"]?"":" checked"?>>表示する</label>
		<label><input type="radio" name="is_hide_byoureki_window" value="1" <?=@$row["is_hide_byoureki_window"]?" checked":""?>>表示しない</label>
	</font></td>
</tr>
<tr>
	<td><?=$font?>チェックリスト画面</font></td>
	<td><?=$font?>
		<label><input type="radio" name="is_hide_checklist_window" value="0" <?=@$row["is_hide_checklist_window"]?"":" checked"?>>表示する</label>
		<label><input type="radio" name="is_hide_checklist_window" value="1" <?=@$row["is_hide_checklist_window"]?" checked":""?>>表示しない</label>
	</font></td>
</tr>
<tr>
	<td><?=$font?>栄養問診表</font></td>
	<td><?=$font?>
		<label><input type="radio" name="is_hide_eiyou_monsin_window" value="0" <?=@$row["is_hide_eiyou_monsin_window"]?"":" checked"?>>表示する</label>
		<label><input type="radio" name="is_hide_eiyou_monsin_window" value="1" <?=@$row["is_hide_eiyou_monsin_window"]?" checked":""?>>表示しない</label>
	</font></td>
</tr>
<tr>
	<td><?=$font?>アクセスログ</font></td>
	<td><?=$font?>
		<label><input type="radio" name="is_activate_access_log" value="1" <?=@$row["is_activate_access_log"]?" checked":""?>>記録する</label>
		<label><input type="radio" name="is_activate_access_log" value="0" <?=@$row["is_activate_access_log"]?"":" checked"?>>記録しない</label>
	</font></td>
</tr>
<tr>
	<td style="vertical-align:top"><?=$font?>キャラクター表示</font></td>
	<td><?=$font?>
		<label><input type="radio" name="is_use_top_information_html" value="1" <?=@$row["is_use_top_information_html"]?" checked":""?>>表示する</label>
		<label><input type="radio" name="is_use_top_information_html" value="0" <?=@$row["is_use_top_information_html"]?"":" checked"?>>表示しない</label>
		<input type="button" onclick="popupUserImageViewer();" value="関連画像">
		<br/>
		<textarea name="top_information_html" cols="70" rows="5"><?=@$row["top_information_html"]?></textarea>
	</font></td>
</tr>
</table>

<div style="text-align:right; margin-top:4px; width:700px"><input type="submit" value="更新"/></div>

</form>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
