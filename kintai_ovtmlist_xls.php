<?php
require_once("about_session.php");
require_once("about_authority.php");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//出張有無表示フラグ 20130304
$opt_trip_flag = file_exists("opt/trip_flag");

//フォームデータ取得
$emp_id 	= $_REQUEST["emp_id"];
$yyyymm		= $_REQUEST["yyyymm"];

//年月
$yyyy = substr($yyyymm, 0, 4);
$mm   = intval(substr($yyyymm, 4, 2));


//データアクセス
try {

	MDBWrapper::init(); // おまじない
	$log = new common_log_class(basename(__FILE__));
	$db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

	$db->beginTransaction(); // トランザクション開始
	
	//部署階層数
	$class_cnt = MasterUtil::get_organization_level_count();
	
	//職員ID・氏名を取得
	$user_info  = UserInfo::get($emp_id, "u");
	$base_info  = $user_info->get_base_info();
	$job_info   = $user_info->get_job_info();
	$class_info  = $user_info->get_class_info();
	$attr_info   = $user_info->get_attr_info();
	$dept_info   = $user_info->get_dept_info();
	$croom_info  = $user_info->get_class_room_info();
	$cond_info   = $user_info->get_emp_condition();

	//所属
	$shozoku = $class_info->class_nm." ".$attr_info->atrb_nm." ".$dept_info->dept_nm;
	if($class_cnt == 4){
		if($croom_info->room_nm != ""){
			$shozoku .= " ".$croom_info->room_nm;
		}
	}

	//締め日情報を取得
	$closing_info  = MasterUtil::get_closing();
	if(is_null($closing_info)){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if($cond_info->duty_form == "2"){
		if($closing_info["closing_parttime"] != ""){
			$closing = $closing_info["closing_parttime"];
		}else{
			$closing = $closing_info["closing"];
		}
	}else{
		$closing = $closing_info["closing"];
	}
	
	//期間取得（月度考慮）
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);
	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);
	$prdArr = DateUtil::get_cutoff_period($cond_info->duty_form, $year, $month);
	
	//kintai_ovtmkbn_mst
	$sql  = "";
	$sql .= "SELECT * FROM kintai_ovtmkbn_mst ORDER BY order_no";
	$retKbn = $db->find($sql);
	//集計用配列初期化
	for($k=0;$k<count($retKbn);$k++){
		$ttlKbn[$k] = 0;
	}

	//データ取得
	$sql  = "";
	$sql .= "SELECT ";
	$sql .= " atdbkrslt.emp_id, ";
	$sql .= " atdbkrslt.date, ";
	$sql .= " atdbkrslt.pattern, ";
	$sql .= " atdbkrslt.reason, ";
	$sql .= " atdbkrslt.tmcd_group_id, ";
	$sql .= " atdbkrslt.meeting_start_time, ";
	$sql .= " atdbkrslt.meeting_end_time, ";
    if ($opt_trip_flag) {
        $sql .= " atdbk_trip.trip_flag, ";
    }
    $sql .= " atdptn.atdptn_nm, ";
	$sql .= " atdptn.workday_count, ";
	$sql .= " atdptn.atdptn_order_no, ";
	$sql .= " atdbk_reason_mst.default_name, ";
	$sql .= " atdbk_reason_mst.display_name, ";
	$sql .= " timecard_paid_hol_hour.calc_minute, ";
	$sql .= " wktmgrp.group_name, ";
	$sql .= " kintai_ovtm.apply_date, ";
	$sql .= " kintai_ovtm.apply_stat, ";
	$sql .= " kintai_ovtm.cmmd_start_time1, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_end_time1, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag1, ";
	$sql .= " kintai_ovtm.cmmd_rest_time1, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.cmmd_start_time2, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_end_time2, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag2, ";
	$sql .= " kintai_ovtm.cmmd_rest_time2, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.cmmd_start_time3, ";
	$sql .= " kintai_ovtm.cmmd_start_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_end_time3, ";
	$sql .= " kintai_ovtm.cmmd_end_next_flag3, ";
	$sql .= " kintai_ovtm.cmmd_rest_time3, ";
	$sql .= " kintai_ovtm.cmmd_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.rslt_start_time1, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_end_time1, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag1, ";
	$sql .= " kintai_ovtm.rslt_rest_time1, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.rslt_start_time2, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_end_time2, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag2, ";
	$sql .= " kintai_ovtm.rslt_rest_time2, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.rslt_start_time3, ";
	$sql .= " kintai_ovtm.rslt_start_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_end_time3, ";
	$sql .= " kintai_ovtm.rslt_end_next_flag3, ";
	$sql .= " kintai_ovtm.rslt_rest_time3, ";
	$sql .= " kintai_ovtm.rslt_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_start_time1, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_end_time1, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag1, ";
	$sql .= " kintai_ovtm.mdfy_rest_time1, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id1, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag1, ";
	$sql .= " kintai_ovtm.mdfy_start_time2, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_end_time2, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag2, ";
	$sql .= " kintai_ovtm.mdfy_rest_time2, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id2, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag2, ";
	$sql .= " kintai_ovtm.mdfy_start_time3, ";
	$sql .= " kintai_ovtm.mdfy_start_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_end_time3, ";
	$sql .= " kintai_ovtm.mdfy_end_next_flag3, ";
	$sql .= " kintai_ovtm.mdfy_rest_time3, ";
	$sql .= " kintai_ovtm.mdfy_ovtmrsn_id3, ";
	$sql .= " kintai_ovtm.mdfy_cancel_flag3 ";
	$sql .= "FROM ";
	$sql .= " atdbkrslt ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdptn ";
	$sql .= "ON ";
	$sql .= " CAST(atdptn.atdptn_id AS varchar) = atdbkrslt.pattern AND ";
	$sql .= " atdptn.group_id  = atdbkrslt.tmcd_group_id ";
	$sql .= "LEFT JOIN ";
	$sql .= " atdbk_reason_mst ";
	$sql .= "ON ";
	$sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
	$sql .= "LEFT JOIN ";
	$sql .= " timecard_paid_hol_hour ";
	$sql .= "ON ";
	$sql .= " timecard_paid_hol_hour.emp_id       = atdbkrslt.emp_id AND ";
	$sql .= " timecard_paid_hol_hour.start_date   = atdbkrslt.date ";
	$sql .= "LEFT JOIN ";
	$sql .= " kintai_ovtm ";
	$sql .= "ON ";
	$sql .= " kintai_ovtm.emp_id = atdbkrslt.emp_id AND ";
	$sql .= " kintai_ovtm.date   = atdbkrslt.date ";
    if ($opt_trip_flag) {
        $sql .= "LEFT JOIN ";
        $sql .= " atdbk_trip ";
        $sql .= "ON ";
        $sql .= " atdbkrslt.emp_id = atdbk_trip.emp_id AND ";
        $sql .= " atdbkrslt.date   = atdbk_trip.date ";
    }
	$sql .= "LEFT JOIN ";
	$sql .= " wktmgrp ";
	$sql .= "ON ";
	$sql .= " wktmgrp.group_id   = atdbkrslt.tmcd_group_id ";
	$sql .= "WHERE ";
	$sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
	$sql .= " (atdbkrslt.date >= '".$prdArr["start"]."' AND atdbkrslt.date <= '".$prdArr["end"]."') ";
	$retRec = $db->find($sql);

	$rData = array();	//データ
	//表示日数
	$dcnt = DateUtil::date_diff($prdArr["start"], $prdArr["end"]);
	for($i=0;$i<$dcnt;$i++){
		$arcnt = date("Ymd", strtotime($prdArr["start"]." ".$i." day")); 
		$rData[$i]["Ymd"] = $arcnt;
	}

	$ttlType = array();	//集計データ
	$ovtm_ttl = 0; // 月次超勤時間
	$ttlMeeting = 0;	//会議集計
	$ttlTimehol = 0;	//時間休集計
	for($out=0;$out<count($rData);$out++){
		//日付
		$rData[$out]["date"] = intval(substr($rData[$out]["Ymd"], 4, 2))."月".intval(substr($rData[$out]["Ymd"], 6, 2))."日";
		//曜日
		$rData[$out]["wday"] = DateUtil::day_of_week($rData[$out]["Ymd"]);
		//背景色
		$rData[$out]["bgcolor"] = getBgColor($rData[$out]["Ymd"]);

		for($i=0;$i<count($retRec);$i++){
			if($rData[$out]["Ymd"] == $retRec[$i]["date"]){
				//勤務実績
				$rData[$out]["atdptn_nm"] = $retRec[$i]["atdptn_nm"];
				//勤務実績集計
				$g = $retRec[$i]["tmcd_group_id"];
				//$p = $retRec[$i]["pattern"];
				$o = $retRec[$i]["atdptn_order_no"];	//表示順考慮
				//$ttlType[$g][$p]["name"] = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["group"] = $retRec[$i]["group_name"];
				$ttlType[$g][$o]["name"]  = $retRec[$i]["atdptn_nm"];
				$ttlType[$g][$o]["gid"]   = $retRec[$i]["tmcd_group_id"];
				$ttlType[$g][$o]["pid"]   = $retRec[$i]["pattern"];
				$ttlType[$g][$o]["order"] = $retRec[$i]["atdptn_order_no"];
				$ttlType[$g][$o]["cnt"]   = floatval($ttlType[$g][$o]["cnt"]) + floatval($retRec[$i]["workday_count"]);
				
				//事由
				if($retRec[$i]["display_name"] != ""){
					$rData[$out]["reason_nm"] = $retRec[$i]["display_name"];
				}else{
					$rData[$out]["reason_nm"] = $retRec[$i]["default_name"];
				}
				
                if ($opt_trip_flag) {
                    if ($retRec[$i]["trip_flag"] == "1") {
                        $rData[$out]["trip_flag"] = "有";
                    } else {
                        $rData[$out]["trip_flag"] = "";
                    }
                }
                
                //申請状態 0 1 2 3
				if(/*$retRec[$i]["apply_stat"] == 0 || $retRec[$i]["apply_stat"] == 1*/ in_array($retRec[$i]["apply_stat"], array(0,1,2,3))){
					//申請状態
					$rData[$out]["apply_stat"] = CodeUtil::get_apply_status($retRec[$i]["apply_stat"]);
					for($j=1;$j<=3;$j++){
						if($retRec[$i]["mdfy_cancel_flag".$j] == 1){
							$rData[$out]["ovtm_t".$j] = "";
							$rData[$out]["ovtm_r".$j] = "";
							$rData[$out]["ovtm_b".$j] = "";
						}else{
							$fldName = "";
							if($retRec[$i]["mdfy_start_time".$j] != ""){
								$fldName = "mdfy";
							}else if($retRec[$i]["rslt_start_time".$j] != ""){
								$fldName = "rslt";
							}else{
								$fldName = "";
								$rData[$out]["ovtm_t".$j] = "";
								$rData[$out]["ovtm_r".$j] = "";
								$rData[$out]["ovtm_b".$j] = "";
							}
							if($fldName != ""){
								//時間外
								$rData[$out]["ovtm_t".$j] = "";
								if($retRec[$i][$fldName."_start_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_start_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_start_time".$j], 2, 2);
								$rData[$out]["ovtm_t".$j] .= "〜";
								if($retRec[$i][$fldName."_end_next_flag".$j] == 1){
									$rData[$out]["ovtm_t".$j] .= "翌";
								}
								$rData[$out]["ovtm_t".$j] .= intval(substr($retRec[$i][$fldName."_end_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_end_time".$j], 2, 2);
								//理由
								if(intval($retRec[$i][$fldName."_ovtmrsn_id".$j]) > 0){
									$sql  = "";
									$sql .= "SELECT ";
									$sql .= " reason ";
									$sql .= "FROM ";
									$sql .= " ovtmrsn ";
									$sql .= "WHERE ";
									$sql .= " reason_id = ".$retRec[$i][$fldName."_ovtmrsn_id".$j];
									$retRsn = $db->find($sql);
									$rData[$out]["ovtm_r".$j] = $retRsn[0]["reason"];
									
								}else{
									$rData[$out]["ovtm_r".$j] = "";
								}
								//休憩
								if($retRec[$i][$fldName."_rest_time".$j] != "" && $retRec[$i][$fldName."_rest_time".$j] != "0000"){
									$rData[$out]["ovtm_b".$j] = intval(substr($retRec[$i][$fldName."_rest_time".$j], 0, 2)).":".substr($retRec[$i][$fldName."_rest_time".$j], 2, 2);
								}else{
									$rData[$out]["ovtm_b".$j] = "";
								}
							}
						}
		
					}
					//時間外
					$sql = "";
					$sql .= "SELECT ";
					$sql .= " sum(minute) ";
					$sql .= "FROM ";
					$sql .= " kintai_ovtm_sum ";
					$sql .= "WHERE ";
					$sql .= " emp_id = '".$base_info->emp_id."' AND ";
					$sql .= " date = '".$retRec[$i]["date"]."'";
					$retOvtm = $db->one($sql);
						
					if(count($retOvtm) > 0 && $retRec[$i]["apply_stat"] == 1){
						$rData[$out]["ovtm_time"] = DateUtil::convert_time($retOvtm);
						$ovtm_ttl += $retOvtm;
					}else{
						$rData[$out]["ovtm_time"] = "";
					}
				}else{
					//申請状態
					$rData[$out]["apply_stat"] = "";
					//時間外、理由、休憩1〜3
					for($j=1;$j<=3;$j++){
						$rData[$out]["ovtm_t".$j] = "";
						$rData[$out]["ovtm_r".$j] = "";
						$rData[$out]["ovtm_b".$j] = "";
					}
		
				}
				//会議研修
				$rData[$out]["meeting"] = DateUtil::diff_time($retRec[$i]["meeting_start_time"], $retRec[$i]["meeting_end_time"]);
				$ttlMeeting += DateUtil::convert_min($rData[$out]["meeting"]);
				//時間有給
				$rData[$out]["hol_min"] = DateUtil::convert_time($retRec[$i]["calc_minute"]);
				$ttlTimehol += DateUtil::convert_min($rData[$out]["hol_min"]);
			}
		}
	
	}

	$db->endTransaction(); // トランザクション終了

} catch (BusinessException $bex) {
	$message = $bex->getMessage();
	echo("<script type='text/javascript'>alert('".$message."');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} catch (FatalException $fex) { 
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

function getBgColor($date) {
	if(is_cal_holiday($date) == true){
		return "FADEDE";
	}else{
		return "FFFFFF";
	}

}
function is_cal_holiday($date) {
	$sql = "select type from calendar where date = :date";
	$value = get_mdb()->one(
		$sql, array("text"), array("date" => $date));

	if (in_array($value, array("6", "7"))) {
		return true;
	}
	return false;
}
function get_mdb() {
	$log = new common_log_class(basename(__FILE__));
	return new MDB($log);
}

//====================================
//	エクセル出力クラス
//====================================
require_once("./kintai/common/kintai_print_excel_class.php");
// Excelオブジェクト生成
$excelObj = new ExcelWorkShop();
// Excel列名。列名をloopで連続するときに使うと便利です。
$excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
						 'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
						 'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
						 'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
						 'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
// 用紙の向き。横方向の例
$excelObj->PageSetUp("Horizon");
// 用紙サイズ
$excelObj->PaperSize( "A4" );
// 倍率と自動或いは利用者指定。利用者が指定して95%で印刷する場合
//$excelObj->PageRatio( "USER" , "70" );
$excelObj->PageRatio( "AUTO" , "" );
// 余白指定、左,右,上,下,ヘッダ,フッタ
$excelObj->SetMargin( "0.5" , "0.5", "1.5" , "1.5" , "0.5" , "0.5" );
// ヘッダー情報
$HeaderMessage = '&"ＭＳ ゴシック"&11&B出勤簿(超勤区分)';
$HeaderMessage = $HeaderMessage . "&R".date("Y.n.j");
$excelObj->SetPrintHeader( $HeaderMessage );
//フッター
$strFooter  = '&C&P / &N';
$excelObj->SetPrintFooter($strFooter);
// シート名
$excelObj->SetSheetName(mb2("出勤簿(超勤区分)"));
// 列幅
for($i=0;$i<count($excelColumnName);$i++){
	$excelObj->SetColDim($excelColumnName[$i], 2.2);
}
//デフォルトフォント
$excelObj->setDefaultFont("ＭＳ ゴシック", 9, false);

//基本■■■■■■■■■■■■■■■■■■
//開始位置
$ROW_ST = 3;
//行のタイトル
$excelObj->setRowTitle(1, $ROW_ST);
//ウィンド枠固定
$excelObj->setPane(0,4);

//期間
$excelObj->CellMerge2("A1:E1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A1", "対象期間：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F1:M1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("F1", $yyyy."年".$mm."月度", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("N1:AD1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("N1", "開始日 ".date("n月j日", strtotime($prdArr["start"]))." 〜 締め日 ".date("n月j日", strtotime($prdArr["end"]))."", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("AE1:AG1", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("AE1", "所属：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->SetArea("AH1"); 
$excelObj->SetFontBold();
$excelObj->SetValueJP2("AH1", $shozoku, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);


//職員
$excelObj->CellMerge2("A2:E2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("A2", "職員ID：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("F2:M2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("F2", $base_info->emp_personal_id, "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$excelObj->CellMerge2("N2:P2", "", "", "");
$excelObj->SetFontBold();
$excelObj->SetValueJP2("N2", "氏名：", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$excelObj->SetArea("Q2"); 
$excelObj->SetFontBold();
$excelObj->SetValueJP2("Q2", $base_info->emp_lt_nm." ".$base_info->emp_ft_nm."（".$job_info->job_nm." ".CodeUtil::get_duty_form($cond_info->duty_form)." ".CodeUtil::get_salary_type($cond_info->wage)."）", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);

//現在行位置
$y = $ROW_ST;

//タイトル行
$arrName = array("日付", "曜日", "勤務実績", "事由", "申請状態", "会議研修", "時間有休"); //, "出張有無",
//				 "時間外1", "理由1", "休憩1", "時間外2", "理由2", "休憩2", "時間外3", "理由3", "休憩3", "合計(月次)");
//
if ($opt_trip_flag) {
    $arrName = array_merge($arrName, array("出張有無"));
}
$arrName = array_merge($arrName, array("時間外1", "理由1", "休憩1", "時間外2", "理由2", "休憩2", "時間外3", "理由3", "休憩3", "合計(月次)"));

$arrLen  = array(4, 2, 5, 5, 4, 4, 4);
//, 4, 6, 5, 2, 6, 5, 2, 6, 5, 2, 4);
if ($opt_trip_flag) {
    $arrLen = array_merge($arrLen, array(4));
}
$arrLen = array_merge($arrLen, array(6, 5, 2, 6, 5, 2, 6, 5, 2, 4));
$col = 0;
for($i=0;$i<count($arrName);$i++){
	$col = getRange($arrLen[$i], $col, $y, $retArr);
	$excelObj->CellMerge2($retArr["range"], "", "", "");
	$excelObj->SetValueJP2($retArr["start"], $arrName[$i], "VCENTER HCENTER", mb1("ＭＳ ゴシック"), 9);
}

//データ
$arrHPos = array("HCENTER", "HCENTER", "HLEFT", "HLEFT", "HCENTER", "HRIGHT", "HRIGHT");//, "HCENTER",
				 //"HCENTER", "HLEFT", "HRIGHT", "HCENTER", "HLEFT", "HRIGHT", "HCENTER", "HLEFT", "HRIGHT", "HRIGHT");
if ($opt_trip_flag) {
    $arrHPos = array_merge($arrHPos, array("HCENTER"));
}
$arrHPos = array_merge($arrHPos, array("HCENTER", "HLEFT", "HRIGHT", "HCENTER", "HLEFT", "HRIGHT", "HCENTER", "HLEFT", "HRIGHT", "HRIGHT"));
$arrFld  = array("date", "wday", "atdptn_nm", "reason_nm", "apply_stat", "meeting", "hol_min");//, "trip_flag",
				 //"ovtm_t1", "ovtm_r1", "ovtm_b1", "ovtm_t2", "ovtm_r2", "ovtm_b2", "ovtm_t3", "ovtm_r3", "ovtm_b3", "ovtm_time");
if ($opt_trip_flag) {
    $arrFld = array_merge($arrFld, array("trip_flag"));
}
$arrFld = array_merge($arrFld, array("ovtm_t1", "ovtm_r1", "ovtm_b1", "ovtm_t2", "ovtm_r2", "ovtm_b2", "ovtm_t3", "ovtm_r3", "ovtm_b3", "ovtm_time"));
for($i=0;$i<count($rData);$i++){
	$y++;
	$col = 0;
	for($j=0;$j<count($arrName);$j++){
		$col = getRange($arrLen[$j], $col, $y, $retArr);

		$excelObj->CellMerge2($retArr["range"], "", "", $rData[$i]["bgcolor"]);
		$excelObj->SetValueJP2($retArr["start"], $rData[$i][$arrFld[$j]], "VCENTER ".$arrHPos[$j], mb1("ＭＳ ゴシック"), 9);
	}
}

//合計欄
$arrLenTotal = array(20, 4, 4); // , 4, 39, 4);
if ($opt_trip_flag) {
    $arrLenTotal = array_merge($arrLenTotal, array(4));
}
$arrLenTotal = array_merge($arrLenTotal, array(39, 4));

$y++;
$col = 0;
$col = getRange($arrLenTotal[0], $col, $y, $retArr);
$excelObj->CellMerge2($retArr["range"], "", "", "");
$excelObj->SetValueJP2($retArr["start"], "合計", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$col = getRange($arrLenTotal[1], $col, $y, $retArr);
$excelObj->CellMerge2($retArr["range"], "", "", "");
$excelObj->SetValueJP2($retArr["start"], DateUtil::convert_time($ttlMeeting), "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$col = getRange($arrLenTotal[2], $col, $y, $retArr);
$excelObj->CellMerge2($retArr["range"], "", "", "");
$excelObj->SetValueJP2($retArr["start"], DateUtil::convert_time($ttlTimehol), "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
$col = getRange($arrLenTotal[3], $col, $y, $retArr);
$excelObj->CellMerge2($retArr["range"], "", "", "");
if ($opt_trip_flag) {
    $excelObj->SetValueJP2($retArr["start"], "", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
    $col = getRange($arrLenTotal[4], $col, $y, $retArr);
    $excelObj->CellMerge2($retArr["range"], "", "", "");
}
$excelObj->SetValueJP2($retArr["start"], "合計", "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);
if ($opt_trip_flag) {
    $col = getRange($arrLenTotal[5], $col, $y, $retArr);
}
else {
    $col = getRange($arrLenTotal[4], $col, $y, $retArr);
}
$excelObj->CellMerge2($retArr["range"], "", "", "");
$excelObj->SetValueJP2($retArr["start"], DateUtil::convert_time($ovtm_ttl), "VCENTER HRIGHT", mb1("ＭＳ ゴシック"), 9);

//罫線
$end_column = split(":", $retArr["range"]);
$excelObj->SetArea("A".$ROW_ST.":".$end_column[1]); 
$excelObj->SetBorder("THIN", "", "all");
$excelObj->SetArea("A".$y.":".$end_column[1]); 
$excelObj->setLineBottomTOP();

//集計
$y++;
$excelObj->SetArea("A".$y); 
$excelObj->SetValueJP2("A".$y, "〔勤務実績集計 〕", "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
$ttlArr = array();
$ttlI = 0;
for($i=0;$i<=max(array_keys($ttlType));$i++){
	$group = "";
	$total = "";
	$ttlJ = 0;
	if(max(array_keys($ttlType[$i])) > 0){
		for($j=0;$j<=max(array_keys($ttlType[$i]));$j++){
			if($ttlType[$i][$j]["cnt"] > 0){
				if(count($ttlType) > 1){
					$ttlArr[$ttlI]["gname"] = $ttlType[$i][$j]["group"];
				}
				$ttlArr[$ttlI]["name"][$ttlJ] = $ttlType[$i][$j]["name"];
				$ttlArr[$ttlI]["cnt"][$ttlJ] = $ttlType[$i][$j]["cnt"];
				$ttlJ++;
			}
		}
		$ttlI++;
	}
}
for($i=0;$i<count($ttlArr);$i++){
	if(count($ttlArr) > 1){
		$y++;
		$excelObj->SetArea("B".$y); 
		$excelObj->SetValueJP2("B".$y, $ttlArr[$i]["gname"], "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
	}
	$col = 2;
	$y++;
	for($j=0;$j<count($ttlArr[$i]["name"]);$j++){
		if($col > 64){
			$col = 2;
			$y++;
		}
		$col = getRange(5, $col, $y, $retArr);
		$excelObj->CellMerge2($retArr["range"], "", "", "");
		$excelObj->SetValueJP2($retArr["start"], $ttlArr[$i]["name"][$j], "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
		$col = getRange(2, $col, $y, $retArr);
		$excelObj->CellMerge2($retArr["range"], "", "", "");
		$excelObj->SetValueJP2($retArr["start"], $ttlArr[$i]["cnt"][$j], "VCENTER HLEFT", mb1("ＭＳ ゴシック"), 9);
		$col = getRange(2, $col, $y, $retArr);
		$excelObj->CellMerge2($retArr["range"], "", "", "");
		
	}	

}
/*
echo "<pre>";
var_export($ttlArr);
echo "</pre>";
exit;
*/

$excelObj->SetArea("A1"); 
// 出力ファイル名
$filename = "List_of_overtime.xls";
// ダウンロード形式
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

function mb1($str){
	return mb_convert_encoding($str, "UTF-8", "EUC-JP");
}
function mb2($str){
	return mb_convert_encoding(mb_convert_encoding($str, "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
}
function getRange($len, $now, $y, &$retArr){
	$colArr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
					'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
					'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
					'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
					'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
	$retArr = array();
	//開始位置
	$retArr["start"] = $colArr[$now].$y;
	//範囲
	$retArr["range"] = $colArr[$now].$y.":".$colArr[$now+$len-1].$y;

	return $now + $len;
}

?>
