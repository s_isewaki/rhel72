<?php
//=========================================================
// シフトグループ設定
//
// ※元々、自動シフト条件設定画面だったため、
//   duty_shift_auto.phpというファイル名になっています。
//=========================================================
require_once("about_comedix.php");
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_yui_calendar_util.ini");
require_once("duty_shift_common_class.php");

//---------------------------------------------------------
//ページ名
$fname = $_SERVER['PHP_SELF'];

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
$adminauth = check_authority($session, 70, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
//---------------------------------------------------------
//利用するCLASS
$obj = new duty_shift_common_class($con, $fname);

// 休暇種別
$hol_pattern = array(
	"24" => "公休",
	"22" => "法定",
	"23" => "所定",
	"1" => "有給",
	"37" => "年休",
	"4" => "代替",
	"17" => "振替",
	"5" => "特別",
	"6" => "欠勤",
	"7" => "病欠",
	"40" => "リフレ",
	"41" => "初盆",
	"8" => "その他",
	"44" => "半有半公",
	"45" => "希望(公休)",
	"46" => "待機(公休)",
	"47" => "管理当直前(公休)",
	"25" => "産休",
	"26" => "育児",
	"27" => "介護",
	"28" => "傷病",
	"29" => "学業",
	"30" => "忌引",
	"31" => "夏休",
	"32" => "結婚",
	"61" => "年末年始"
);

// 職員データの取得
$sql = "SELECT e.* FROM empmst e JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp = pg_fetch_assoc($sel);

// 勤務シフトグループデータの取得
$sql = "SELECT * FROM duty_shift_group g
WHERE person_charge_id='{$emp["emp_id"]}'
 OR caretaker_id='{$emp["emp_id"]}'
 OR caretaker2_id='{$emp["emp_id"]}'
 OR caretaker3_id='{$emp["emp_id"]}'
 OR caretaker4_id='{$emp["emp_id"]}'
 OR caretaker5_id='{$emp["emp_id"]}'
 OR caretaker6_id='{$emp["emp_id"]}'
 OR caretaker7_id='{$emp["emp_id"]}'
 OR caretaker8_id='{$emp["emp_id"]}'
 OR caretaker9_id='{$emp["emp_id"]}'
 OR caretaker10_id='{$emp["emp_id"]}'
 ORDER BY order_no, group_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$grouplist = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();
$tab_menuitem_group_id = "";

if (!empty($grouplist)) {
	// 表示するグループデータの取得
	$group = array();
	$group_id = !isset($_REQUEST["group_id"]) ? $grouplist[0]["group_id"] : $_REQUEST["group_id"];
	foreach($grouplist as $data) {
		if ($group_id === $data["group_id"]) {
			$group = $data;
			break;
		}
	}
	if(empty($group)) {
		$group = $grouplist[0];
	}
    $group['_disp'] = unserialize($group['display']);
	$tab_menuitem_group_id = "&group_id={$group["group_id"]}";

	// 勤務パターンデータの取得
	$sql = "SELECT * FROM atdptn a JOIN duty_shift_pattern d ON a.atdptn_id=d.atdptn_ptn_id AND a.group_id=d.pattern_id LEFT JOIN atdbk_reason_mst r ON d.reason=r.reason_id WHERE group_id='{$group["pattern_id"]}' AND (r.display_flag IS NULL OR r.display_flag='t') ORDER BY order_no,atdptn_id";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$pattern = array();
	while ($row = pg_fetch_assoc($sel)) {
		$id = sprintf("%02d%02d", $row["atdptn_ptn_id"], $row["reason"]);
		$name = $row["atdptn_nm"];

		if($row["reason"] >= "44" and $row["reason"] <= "47") {
			$name = $hol_pattern[ $row["reason"] ];
		}
		elseif(!empty($hol_pattern[ $row["reason"] ])) {
			$name .= "(". $hol_pattern[ $row["reason"] ]. ")";
		}
		$pattern[] = array(
			"id" => $id,
			"name" => $name
		);
	}

	// 組み合わせ指定データの取得
	// (ID=10でreason未設定の場合、reasonは34にする)
	$sql = "SELECT *,
			CASE WHEN today_atdptn_ptn_id=10 AND today_reason=null THEN 34 WHEN today_atdptn_ptn_id=10 AND today_reason=0 THEN 34 ELSE today_reason END AS treason,
			CASE WHEN nextday_atdptn_ptn_id=10 AND nextday_reason=null THEN 34 WHEN nextday_atdptn_ptn_id=10 AND nextday_reason=0 THEN 34 ELSE nextday_reason END AS nreason
			FROM duty_shift_group_seq_pattern
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$ok_pattern = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 組み合わせ禁止データの取得
	// (ID=10でreason未設定の場合、reasonは34にする)
	$sql = "SELECT *,
			CASE WHEN today_atdptn_ptn_id=10 AND today_reason=null THEN 34 WHEN today_atdptn_ptn_id=10 AND today_reason=0 THEN 34 ELSE today_reason END AS treason,
			CASE WHEN nextday_atdptn_ptn_id=10 AND nextday_reason=null THEN 34 WHEN nextday_atdptn_ptn_id=10 AND nextday_reason=0 THEN 34 ELSE nextday_reason END AS nreason
			FROM duty_shift_group_ng_pattern
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$ng_pattern = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 連続勤務シフト上限データの取得
	// (ID=10でreason未設定の場合、reasonは34にする)
	$sql = "SELECT *,
			CASE WHEN atdptn_ptn_id=10 AND reason=null THEN 34 WHEN atdptn_ptn_id=10 AND reason=0 THEN 34 ELSE reason END AS reason2
			FROM duty_shift_group_upper_limit
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$up_limit = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 勤務シフト間隔データの取得
	// (ID=10でreason未設定の場合、reasonは34にする)
	$sql = "SELECT *,
			CASE WHEN atdptn_ptn_id=10 AND reason=null THEN 34 WHEN atdptn_ptn_id=10 AND reason=0 THEN 34 ELSE reason END AS reason2
			FROM duty_shift_group_interval
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$interval = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 性別固定シフトデータの取得
	// (ID=10でreason未設定の場合、reasonは34にする)
	$sql = "SELECT *
			FROM duty_shift_group_sex_pattern
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$sex_pattern = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// ４週８休シフト種類の取得
	$sql = "SELECT *
			FROM duty_shift_group_hol_pattern
			WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$hol_pattern = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();

	// 決済欄役職データの取得
	$sql = "SELECT * FROM duty_shift_group_st_name WHERE group_id='{$group_id}' ORDER BY no";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$st_name = pg_fetch_all($sel) ? pg_fetch_all($sel) : array(array("st_name" => "院長"), array("st_name" => "看護部長"), array("st_name" => "師長"));

	// 休暇自動割当設定登録
	$sql = "SELECT * FROM duty_shift_vacation_limit_day WHERE group_id='{$group_id}' ";
	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$vacation_set = pg_fetch_all($sel) ? pg_fetch_all($sel) : array();
	if ( $vacation_set[0]["force_hol_set"] == 'c' ){
		$checked = "checked";
	}else{
		$checked = "";
	}
}
//作成者情報取得
$createEmpDispFlg = $obj->conf_get($group_id, "createEmpDispFlg");
if ($createEmpDispFlg == "") {
	$createEmpDispFlg = "f";
}
$createEmpStat = $obj->conf_get($group_id, "createEmpStat");
$createEmpName = $obj->conf_get($group_id, "createEmpName");

// データベースを切断
pg_close($con);
ob_end_clean();
//---------------------------------------------------------

function show_shift_pattern($pattern, $id=null, $reason=null)
{
	foreach($pattern as $data) {
		$selected = "";
		if(!empty($id) and $data["id"] === sprintf("%02d%02d",$id,$reason)) {
			$selected = 'selected';
		}
		printf('<option %s value="%s">%s</option>', $selected, $data["id"], $data["name"]);
	}
}
//---------------------------------------------------------
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜自動シフト条件設定</title>

<script type="text/javascript" src="js/fontsize.js"></script>
<? duty_shift_write_yui_calendar_use_file_read_0_12_2();?>
<script type="text/javascript" src="js/duty_shift/auto.js"></script>
<script type="text/javascript">
<!--
  var shift_pattern = '<? show_shift_pattern($pattern)?>';

	function AddVal(_value ){
		_value ++;
		if( isNaN(_value)){ _value = 0; }
		if( _value >= 99 ){ _value = 99; }
		return (_value);
	}
	function SubVal(_value){
		_value --;
		if( isNaN(_value)){ _value = 0;}
		if( _value <= 0  ){ _value = 0;}
		return (_value);
	}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
#tabmenu {border-bottom: 2px solid #5279a5;}
#calContainer {position:absolute;display:none;z-index:10000;}
.listheader {width:600px; margin:10px 0 0 0;}
.listheader button {float:right;}
.list {border-collapse:collapse; width:600px; margin:2px;}
.list td {border:#5279a5 solid 1px; padding:3px; font-size:10px;}
.listtitle {width:190px; text-align:right;}
.listcell {width:320px;}
.listcell2 {width:160px;}
.listcell75 {width:75px;}
.listbutton {text-align:right; vertical-align:top;}
input.bsize1 {width:21px;height:16px;font-size:75%;padding:0px 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- ヘッダ -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_user_title($session, $adminauth); ?>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tabmenu">
<? show_user_tab_menuitem($session, $fname, $tab_menuitem_group_id); ?>
</table>

<?if (empty($grouplist)) {?>
<p>勤務シフトグループ情報が未設定です。管理画面で登録してください。</p>
<?} else {?>

<form id="form" method="post" action="duty_shift_auto_update_exe.php">
<input type="hidden" id="session" name="session" value="<?eh($session)?>">
<input type="hidden" id="emp_id" name="emp_id" value="<?eh($emp["emp_id"])?>">

<p>シフトグループ(病棟)名:
<select id="group_select" name="group_id">
<? foreach($grouplist as $data) { ?>
<option value="<?eh($data["group_id"])?>" <?echo ($group_id===$data["group_id"])?'selected':'';?>><?eh($data["group_name"])?>
<? } //endforeach ?>
</select>
</p>

<!-- シフトグループ設定 -->
<div class="listheader">
■シフトグループ設定<button type="button" onclick="check_form();">登録</button>
</div>

<table class="list">

<tr>
<td class="listtitle">勤務希望締切日</td>
<td><input type="text" size="2" maxlength="2" name="close_day" value="<?eh($group["close_day"])?>" style="ime-mode:disabled;">日</td>
</tr>

<tr>
<td class="listtitle">連続勤務日上限</td>
<td><input type="text" size="2" maxlength="2" name="duty_upper_limit_day" value="<?eh($group["duty_upper_limit_day"])?>" style="ime-mode:disabled;">日</td>
</tr>

<tr>
<td class="listtitle">シフト作成期間</td>
<td>
	<select id="start_month_flg1" name="start_month_flg1">
		<option value="1" <?echo ($group["start_month_flg1"]==="1")?'selected':'';?>>当月</option>
		<option value="0" <?echo ($group["start_month_flg1"]==="0")?'selected':'';?>>前月</option>
	</select>

	<select id="start_day1" name="start_day1">
	<option value="0"></option>
	<?foreach(range(1,31) as $day) {?>
		<option value="<?=$day?>" <?echo ($group["start_day1"]==$day)?'selected':'';?>><?=$day?></option>
	<?} //endforeach ?>
	</select>
〜
	<select id="month_flg1" name="month_flg1">
		<option value="1" <?echo ($group["month_flg1"]==="1")?'selected':'';?>>当月</option>
		<option value="2" <?echo ($group["month_flg1"]==="2")?'selected':'';?>>翌月</option>
	</select>

	<select id="end_day1" name="end_day1">
	<option value="0"></option>
	<?foreach(range(1,31) as $day) {?>
		<option value="<?=$day?>" <?echo ($group["end_day1"]==$day)?'selected':'';?>><?=$day?></option>
	<?} //endforeach ?>
		<option value="99" <?echo ($group["end_day1"]==="99")?'selected':'';?>>月末</option>
	</select>
</td>
</tr>

<tr>
<td class="listtitle">事由設定の表示</td>
<td>
	<label><input type="checkbox" name="reason_setting_flg" value="f" <?echo ($group["reason_setting_flg"]=="f")?"checked":"";?>> 事由設定欄を表示しない</label>
</td>
</tr>
</table>

<!-- シフト条件設定 -->
<div class="listheader">
■シフト条件設定<button type="button" onclick="check_form();">登録</button>
</div>

<table class="list" id="ok_pattern">
<tr>
<td class="listtitle">＜組み合わせ設定＞</td>
<td class="listcell2">当日</td>
<td class="listcell2">翌日</td>
<td><button type="button" onclick="add_ok_pattern(shift_pattern);">追加</button></td>
</tr>
<? foreach($ok_pattern as $data) {?>
<tr>
<td class="listtitle"><?=$data["no"]?></td>
<td><select name="seq_today[]"><? show_shift_pattern($pattern, $data["today_atdptn_ptn_id"], $data["treason"])?></select></td>
<td><select name="seq_next[]"><? show_shift_pattern($pattern, $data["nextday_atdptn_ptn_id"], $data["nreason"])?></select></td>
<td class="listbutton"><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} //endforeach?>
</table>

<table class="list" id="ng_pattern">
<tr>
<td class="listtitle">＜組み合わせ禁止＞</td>
<td class="listcell2">当日</td>
<td class="listcell2">翌日</td>
<td><button type="button" onclick="add_ng_pattern(shift_pattern);">追加</button></td>
</tr>
<? foreach($ng_pattern as $data) {?>
<tr>
<td class="listtitle"><?=$data["no"]?></td>
<td><select name="ng_today[]"><? show_shift_pattern($pattern, $data["today_atdptn_ptn_id"], $data["treason"])?></select></td>
<td><select name="ng_next[]"><? show_shift_pattern($pattern, $data["nextday_atdptn_ptn_id"], $data["nreason"])?></select></td>
<td class="listbutton"><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} //endforeach?>
</table>

<table class="list" id="up_limit">
<tr>
<td class="listtitle">＜連続勤務シフトの上限＞</td>
<td class="listcell2">シフト</td>
<td class="listcell2">上限</td>
<td><button type="button" onclick="add_up_limit(shift_pattern);">追加</button></td>
</tr>
<? foreach($up_limit as $data) {?>
<tr>
<td class="listtitle"><?=$data["no"]?></td>
<td><select name="limit_id[]"><? show_shift_pattern($pattern, $data["atdptn_ptn_id"], $data["reason2"])?></select></td>
<td><input type="text" name="limit_day[]" value="<?=$data["upper_limit_day"]?>" size="3">日</td>
<td class="listbutton"><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} //endforeach?>
</table>

<table class="list" id="interval">
<tr>
<td class="listtitle">＜勤務シフトの間隔＞</td>
<td class="listcell2">シフト</td>
<td class="listcell2">間隔日数</td>
<td><button type="button" onclick="add_interval(shift_pattern);">追加</button></td>
</tr>
<? foreach($interval as $data) {?>
<tr>
<td class="listtitle"><?=$data["no"]?></td>
<td><select name="interval_id[]"><? show_shift_pattern($pattern, $data["atdptn_ptn_id"], $data["reason2"])?></select></td>
<td><input type="text" name="interval_day[]" value="<?=$data["interval_day"]?>" size="3">日</td>
<td class="listbutton"><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} //endforeach?>
</table>

<table class="list" id="sex_pattern">
<tr>
<td class="listtitle">＜同時勤務の性別を固定するシフト＞</td>
<td class="listcell2">シフト</td>
<td class="listcell2">-</td>
<td><button type="button" onclick="add_sex_pattern(shift_pattern);">追加</button></td>
</tr>
<? foreach($sex_pattern as $data) {?>
<tr>
<td class="listtitle"><?=$data["no"]?></td>
<td><select name="sex_pattern[]"><? show_shift_pattern($pattern, substr($data["ptn_id"],0,2), substr($data["ptn_id"],2,2))?></select></td>
<td>-</td>
<td class="listbutton"><button type="button" onclick="remove_table_row(this);">削除</button></td>
</tr>
<?} //endforeach?>
</table>
<BR>
<!-- 休暇自動割当設定 -->
<div class="listheader">
■休暇自動割当設定<button type="button" onclick="check_form();">登録</button>
</div>
<table class="list">
<tr>
<td class="listcell" colspan="8">
<label><input type="checkbox" name="force_hol_set" value="c" <? echo $checked; ?>>土日、祝日に１日は必ず休暇（公休）を割り当てる</label>
</td>
<tr>
 <td class="listcell" colspan="8">同一日の常勤者の休暇数上限数</td>
</tr>
<tr>
<?
// 休暇自動割当設定が未登録のシフトグループ(病棟名)のデータに0を表示させる
?>

 <td class="listcell75" align="center" valign="center">月<BR><input type="text" size="2" maxlength="2" name="holiday_1" value=<? $d=$vacation_set[0]["holiday_1"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">火<BR><input type="text" size="2" maxlength="2" name="holiday_2" value=<? $d=$vacation_set[0]["holiday_2"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">水<BR><input type="text" size="2" maxlength="2" name="holiday_3" value=<? $d=$vacation_set[0]["holiday_3"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">木<BR><input type="text" size="2" maxlength="2" name="holiday_4" value=<? $d=$vacation_set[0]["holiday_4"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">金<BR><input type="text" size="2" maxlength="2" name="holiday_5" value=<? $d=$vacation_set[0]["holiday_5"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">土<BR><input type="text" size="2" maxlength="2" name="holiday_6" value=<? $d=$vacation_set[0]["holiday_6"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">日<BR><input type="text" size="2" maxlength="2" name="holiday_7" value=<? $d=$vacation_set[0]["holiday_7"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>
 <td class="listcell75" align="center" valign="center">祝<BR><input type="text" size="2" maxlength="2" name="holiday_8" value=<? $d=$vacation_set[0]["holiday_8"];echo($d==""? 20:$d);?> style="ime-mode:disabled;"></td>

</tr>
<tr>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_1.value=AddVal(holiday_1.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_1.value=SubVal(holiday_1.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_2.value=AddVal(holiday_2.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_2.value=SubVal(holiday_2.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_3.value=AddVal(holiday_3.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_3.value=SubVal(holiday_3.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_4.value=AddVal(holiday_4.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_4.value=SubVal(holiday_4.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_5.value=AddVal(holiday_5.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_5.value=SubVal(holiday_5.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_6.value=AddVal(holiday_6.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_6.value=SubVal(holiday_6.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_7.value=AddVal(holiday_7.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_7.value=SubVal(holiday_7.value)" class="bsize1">
 </td>
 <td class="listcell75" align="center" valign="center">
	<input type=button value="＋" onclick="holiday_8.value=AddVal(holiday_8.value)" class="bsize1">
	<input type=button value="−" onclick="holiday_8.value=SubVal(holiday_8.value)" class="bsize1">
 </td>
<tr>
</tr>
</table>
<BR>
<!-- チェック設定 -->
<div class="listheader">
■チェック設定<button type="button" onclick="check_form();">登録</button>
</div>
<table class="list">

<tr>
<td class="listtitle">4週8休のチェック</td>
<td colspan="2">
	<label><input type="checkbox" id="four_week_chk_flg" name="four_week_chk_flg" value="t" <?echo ($group["four_week_chk_flg"]==="t")?"checked":"";?> onclick="click_fourweek();"> チェックする</label>
</td>
</tr>

<tr id="four_week_start_date">
<td class="listtitle">4週8休の開始日</td>
<td colspan="2">
	<select id="chk_start_y" name="chk_start_y">
	<option value="-"></option>
	<? foreach(range(Date("Y")+1,Date("Y")-5) as $y) {?>
	<option value="<?=$y?>" <?echo (substr($group["chk_start_date"],0,4)==$y)?'selected':'';?>><?=$y?></option>
	<? }//endforeach ?>
	</select>年
	<select id="chk_start_m" name="chk_start_m">
	<? show_select_months(substr($group["chk_start_date"],4,2), true);?>
	</select>月
	<select id="chk_start_d" name="chk_start_d">
	<? show_select_days(substr($group["chk_start_date"],6,2), true);?>
	</select>日
	<img id="imgCalendar" src="img/calendar_link.gif" width="20" height="20" alt="カレンダー" title="カレンダー" style="cursor: pointer;">
	<div id="calContainer"></div>
</td>
</tr>

<tr id="four_week_hol_pattern">
<td class="listtitle">4週8休で休暇とするシフト</td>
<td class="listcell" id="hol_pattern"><span style="color:red;">※休暇、休暇(公休、法定、所定)は最初から含みます。</span><br>
<? foreach($hol_pattern as $data) {?>
<span><select name="hol_pattern[]"><? show_shift_pattern($pattern, substr($data["ptn_id"],0,2), substr($data["ptn_id"],2,2))?></select><button type="button" onclick="remove_span(this);">削除</button><br></span>
<?} //endforeach?>
</td>
<td class="listbutton">
	<button type="button" onclick="add_hol_pattern(shift_pattern);">追加</button>
</td>
</tr>

<tr>
<td class="listtitle">警告表示</td>
<td colspan="2">
	<label><input type="checkbox" name="need_less_chk_flg" value="t" <?echo ($group["need_less_chk_flg"]==="t")?"checked":"";?>> 必要人数より少ないときに警告表示する</label><br>
	<label><input type="checkbox" name="need_more_chk_flg" value="t" <?echo ($group["need_more_chk_flg"]==="t")?"checked":"";?>> 必要人数より多いときに警告表示する</label><br>
    <!-- ７連勤チェック設定 20131111 YOKOBORI-->
	<label><input type="checkbox" name="seven_work_chk_flg" value="t" <?echo ($group["seven_work_chk_flg"]==="t")?"checked":"";?>> ７日連続勤務をチェックする<span style='color:red'>(別ウインドウで警告)</span></label><br>
	<label><input type="checkbox" name="part_week_chk_flg" value="t" <?echo ($group["part_week_chk_flg"]==="t")?"checked":"";?>> 非常勤の週間勤務時間数をチェックする<span style='color:red'>(別ウインドウで警告)</span></label><br>
    <!-- ７連勤チェック設定 20131111 YOKOBORI-->
</td>
</tr>
</table>

<!-- 自動シフト設定 -->
<div class="listheader">
■自動シフト設定<button type="button" onclick="check_form();">登録</button>
</div>
<table class="list">

<tr>
<td class="listtitle">解析対象とする期間</td>
<td><input type="text" size="2" name="auto_check_months" value="<?eh($group["auto_check_months"] ? $group["auto_check_months"] : 3)?>" style="ime-mode:disabled;">ヶ月間</td>
</tr>

<tr>
<td class="listtitle">自動割り当てできなかったシフト</td>
<td><select name="auto_shift_filler">
<option value="1" <?echo ($group["auto_shift_filler"]==="1")?'selected':'';?>>勤務記号表示の1番目を設定</option>
<option value="2" <?echo ($group["auto_shift_filler"]==="2")?'selected':'';?>>必要人数設定の優先順位1番目を設定</option>
<option value="3" <?echo ($group["auto_shift_filler"]==="3")?'selected':'';?>>設定なし</option>
</select></td>
</tr>
</table>

<!-- 印刷設定 -->
<div class="listheader">
■印刷設定<button type="button" onclick="check_form();">登録</button>
</div>
<table class="list">

<tr>
<td class="listtitle">勤務シフト表タイトル</td>
<td colspan="2">
	<input type="text" name="print_title" size="60" maxlength="50" value="<?eh($group["print_title"] ? $group["print_title"] : "看護職員勤務表")?>" style="ime-mode:active;">
</td>
</tr>

<tr>
<td class="listtitle">決済欄役職(左から)</td>
<td id="st_name" class="listcell">
	<span><input type="text" name="st_name[]" size="30" maxlength="30" value="<?eh($st_name[0]["st_name"])?>" style="ime-mode:active;"><br></span>
	<span><input type="text" name="st_name[]" size="30" maxlength="30" value="<?eh($st_name[1]["st_name"])?>" style="ime-mode:active;"><br></span>
	<span><input type="text" name="st_name[]" size="30" maxlength="30" value="<?eh($st_name[2]["st_name"])?>" style="ime-mode:active;"><br></span>
	<?for($i=3; $i<count($st_name); $i++) {?>
	<span><input type="text" name="st_name[]" size="30" maxlength="30" value="<?eh($st_name[$i]["st_name"])?>" style="ime-mode:active;"><button type="button" onclick="remove_span(this);">削除</button><br></span>
	<?} //endfor ?>
</td>
<td class="listbutton">
	<button type="button" onclick="add_st_name();">追加</button>
</td>
</tr>
<!-- 決裁欄の印字 -->
<tr>
<td class="listtitle">決裁欄の印刷</td>
<td colspan="2">
	<label><input type="radio" name="approval_column_print_flg" value="t"<? if ($group["approval_column_print_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="approval_column_print_flg" value="f"<? if ($group["approval_column_print_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>
<!-- 作成者を表示 -->
<tr>
<td class="listtitle">作成者を表示</td>
<td colspan="2">
	<label><input type="radio" name="createEmpDispFlg" value="t"<? if ($createEmpDispFlg == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="createEmpDispFlg" value="f"<? if ($createEmpDispFlg == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
<br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">役職</font>
<input type="text" size="30" name="createEmpStat" value="<? eh($createEmpStat); ?>" style="ime-mode:active;">
<br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">氏名</font>
<input type="text" size="30" name="createEmpName" value="<? eh($createEmpName); ?>" style="ime-mode:active;">
</td>
</tr>
</table>

<!-- 画面レイアウト設定 -->
<div class="listheader">
■画面レイアウト設定<button type="button" onclick="check_form();">登録</button>
</div>
<table class="list">

<tr>
<td class="listtitle">チーム表示</td>
<td colspan="2">
	<label><input type="radio" name="team_disp_flg" value="t"<? if ($group["team_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="team_disp_flg" value="f"<? if ($group["team_disp_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<tr>
<td class="listtitle">職種表示</td>
<td colspan="2">
	<label><input type="radio" name="job_disp_flg" value="t"<? if ($group["job_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="job_disp_flg" value="f"<? if ($group["job_disp_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<tr>
<td class="listtitle">役職表示</td>
<td colspan="2">
	<label><input type="radio" name="st_disp_flg" value="t"<? if ($group["st_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="st_disp_flg" value="f"<? if ($group["st_disp_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<!-- 雇用・勤務形態追加　20130219 -->
<tr>
<td class="listtitle">雇用・勤務形態表示</td>
<?php if ($group["es_disp_flg"] == "") {$group["es_disp_flg"] = "f";} ?>
<td colspan="2">
	<label><input type="radio" name="es_disp_flg" value="t"<? if ($group["es_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="es_disp_flg" value="f"<? if ($group["es_disp_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<!-- ユニット・繰越公休残日数追加　20131112 -->
<tr>
<td class="listtitle">所属表示</td>
<td>
	<label><input type="radio" name="unit_disp_flg" value="t"<? if ($group["unit_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="unit_disp_flg" value="f"<? if (($group["unit_disp_flg"] == "f") || ($group["unit_disp_flg"] == "")) {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
<td>
	<label><input type="radio" name="unit_disp_level" value="3"<? if ($group["unit_disp_level"] == "3") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">３階層</font></label>&nbsp;
	<label><input type="radio" name="unit_disp_level" value="4"<? if (($group["unit_disp_level"] == "4") || ($group["unit_disp_level"] == "")) {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">４階層</font></label>
</td>
</tr>

<tr>
<td class="listtitle">繰越公休残日数表示</td>
<td colspan="2">
	<label><input type="radio" name="transfer_holiday_days_flg" value="t"<? if ($group["transfer_holiday_days_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する</font></label>&nbsp;
	<label><input type="radio" name="transfer_holiday_days_flg" value="f"<? if (($group["transfer_holiday_days_flg"] == "f") || ($group["transfer_holiday_days_flg"] == "")) {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<tr>
<td class="listtitle">有給休暇残日数表示</td>
<td colspan="2">
	<label><input type="radio" name="paid_hol_disp_flg" value="t"<? if ($group["paid_hol_disp_flg"] == "t") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">する（時間有休は計算されません）</font></label>&nbsp;
	<label><input type="radio" name="paid_hol_disp_flg" value="f"<? if ($group["paid_hol_disp_flg"] == "f") {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">しない</font></label>
</td>
</tr>

<tr>
<td class="listtitle">集計行表示位置</td>
<td colspan="2">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	勤務予定: <input type="text" name="count_row_position" size="3" value="<?=h($group["_disp"]['count_row_position'] + 0);?>" style="ime-mode:disabled;">行目
    (0の場合は画面サイズに合わせた位置になります)<br>
	勤務実績: <input type="text" name="result_row_position" size="3" value="<?=h($group["_disp"]['result_row_position'] + 0);?>" style="ime-mode:disabled;">行目
	</font>
</td>
</tr>

</table>

<div class="listheader">
<button type="button" onclick="check_form();">登録</button>
</div>

</form>

<?} //endif ?>

</body>
</html>