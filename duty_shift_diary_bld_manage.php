<?php
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");
require_once("duty_shift_diary_bld_manage_common.php");
//ini_set("display_errors", "On");
//ini_set("error_reporting", E_ALL);
$conf = new Cmx_SystemConfig();
$pattern = $conf->get('duty_shift.use_pattern');
if ($pattern != "B") {
    ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
     <head>
      <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
      <title>CoMedix 勤務シフト作成｜勤務日誌</title>
      <?php
  }

///-----------------------------------------------------------------------------
  //ページ名
  ///-----------------------------------------------------------------------------
  $fname = $PHP_SELF;
  ///-----------------------------------------------------------------------------
  //セッションのチェック
  ///-----------------------------------------------------------------------------
  $session = qualify_session($session, $fname);
  if ($session == "0") {
      echo("<script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showLoginPage(window);</script>");
      exit;
  }
  ///-----------------------------------------------------------------------------
  // データベースに接続
  ///-----------------------------------------------------------------------------
  $con = connect2db($fname);
  if ($con == "0") {
      echo("<script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showLoginPage(window);</script>");
      exit;
  }
  ///-----------------------------------------------------------------------------
  //利用するCLASS
  ///-----------------------------------------------------------------------------
  $obj = new duty_shift_common_class($con, $fname);
  ///-----------------------------------------------------------------------------
  // 権限のチェック
  ///-----------------------------------------------------------------------------
  //ユーザ画面用
  $chk_flg = $obj->check_authority_user($session, $fname);
  if ($chk_flg == "") {
      pg_close($con);
      echo("<script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showLoginPage(window);</script>");
      exit;
  }
  //管理画面用
  $section_admin_auth = $obj->check_authority_Management($session, $fname);
  ///-----------------------------------------------------------------------------
  //初期処理
  ///-----------------------------------------------------------------------------
  ///-----------------------------------------------------------------------------
  //初期値設定
  ///-----------------------------------------------------------------------------
  $err_flg_1 = "";

  // 期間
  if ($start_yr == "") {
      $start_yr = date('Y');
  }
  if ($start_mon == "") {
      $start_mon = date('m');
  }
  if ($start_day == "") {
      $start_day = date('d');
  }



  ///-----------------------------------------------------------------------------
  //ＤＢ(wktmgrp)より勤務パターン情報を取得
  ///-----------------------------------------------------------------------------
  //$data_emp = $obj->get_empmst_array("");
  $data_emp = array(); //以下の処理で未使用のため
  $data_wktmgrp = $obj->get_wktmgrp_array();
  if (count($data_wktmgrp) <= 0) {
      $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
  } else {
      // 更新権限のあるシフトグループ数
      $create_flg_cnt = 0;

      ///-----------------------------------------------------------------------------
      // 出勤実績を取得
      ///-----------------------------------------------------------------------------
      ///-----------------------------------------------------------------------------
      // ログインユーザの職員ID・氏名を取得
      ///-----------------------------------------------------------------------------
      $sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
      $cond = "where emp_id = (select emp_id from session where session_id = '$session')";
      $sel = select_from_table($con, $sql, $cond, $fname);
      if ($sel == 0) {
          pg_close($con);
          echo("<script type='text/javascript' src='./js/showpage.js'></script>");
          echo("<script language='javascript'>showErrorPage(window);</script>");
          exit;
      }
      $emp_id = pg_fetch_result($sel, 0, "emp_id");
      $emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");


      // 更新権限のあるシフトグループ数（変数の初期化）
      $create_flg_cnt = 0;
      ///-----------------------------------------------------------------------------
      // 勤務シフトグループ情報を取得
      ///-----------------------------------------------------------------------------
      $group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);

      // 更新権限あり、職員設定されているグループ取得（引数は全勤務シフトグループ、自職員ID）
      //→自分が所属しており、且つ自分が権限(責任者、代行者、職員設定、応援)を持つグループを取得
      $group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
      if (count($group_array) <= 0) {
          $err_flg_1 = "1";
      } else {
          //更新権限があるグループを選択し、変数へ設定する

          for ($i = 0; $i < count($group_array); $i++) {

              //責任者か代行者であれば更新権限があると判断する
              $person_charge_id = $group_array[$i]["person_charge_id"];   //シフト管理者ID（責任者）
              $caretaker_id = $group_array[$i]["caretaker_id"];     //シフト管理者ID（代行者）
              $caretaker2_id = $group_array[$i]["caretaker2_id"];
              $caretaker3_id = $group_array[$i]["caretaker3_id"];
              $caretaker4_id = $group_array[$i]["caretaker4_id"];
              $caretaker5_id = $group_array[$i]["caretaker5_id"];
              $caretaker6_id = $group_array[$i]["caretaker6_id"];
              $caretaker7_id = $group_array[$i]["caretaker7_id"];
              $caretaker8_id = $group_array[$i]["caretaker8_id"];
              $caretaker9_id = $group_array[$i]["caretaker9_id"];
              $caretaker10_id = $group_array[$i]["caretaker10_id"];

              if (($emp_id == $person_charge_id) || ($emp_id == $caretaker_id) || ($emp_id == $caretaker2_id) || ($emp_id == $caretaker3_id) || ($emp_id == $caretaker4_id) || ($emp_id == $caretaker5_id) || ($emp_id == $caretaker6_id) || ($emp_id == $caretaker7_id) || ($emp_id == $caretaker8_id) || ($emp_id == $caretaker9_id) || ($emp_id == $caretaker10_id)) {
                  $group_array[$i]["create_flg"] = "1";

                  //更新権限があるのでカウントする
                  $create_flg_cnt++;
              }
          }

          ///-----------------------------------------------------------------------------
          //勤務シフトグループ情報ＤＢより情報取得
          ///-----------------------------------------------------------------------------
          if ($group_id == "") {
              $group_id = $group_array[0]["group_id"];
          }
      }

      $conf = new Cmx_SystemConfig();
      $pattern = $conf->get('duty_shift.use_pattern');
      if ($pattern == "B") {
          include_once("opt/kanrinishi_config.php");
          include_once(APP_BASE . "controller/duty_shift_diary_bld_manage.php");
          $ptnB = new duty_shift_diary_bld_manage("duty_shift_diary_bld_manage");
          $ptnB->Control($group_array, $session, $_POST, $emp_name, $emp_id);
          return;
      }

      if ($actionDispShift == "表示") {

          $start_date = $start_yr . $start_mon . $start_day;

          $arr_ya_ake_jokin_kango = array();   //夜勤明け、常勤、看護師メンバー
          $arr_ya_ake_hi_jokin_kango = array();  //夜勤明け、非常勤、看護師メンバー

          $arr_ya_ake_jokin_jun_kango = array();  //夜勤明け、常勤、准看護師メンバー
          $arr_ya_ake_hi_jokin_jun_kango = array(); //夜勤明け、非常勤、准看護師メンバー

          $arr_ya_ake_jokin_hojo = array();   //夜勤明け、常勤、補助メンバー
          $arr_ya_ake_hi_jokin_hojo = array();  //夜勤明け、非常勤、補助メンバー

          $arr_ya_iri_jokin_kango = array();   //夜勤入り、常勤、看護師メンバー
          $arr_ya_iri_hi_jokin_kango = array();  //夜勤入り、非常勤、看護師メンバー

          $arr_ya_iri_jokin_jun_kango = array();  //夜勤入り、常勤、准看護師メンバー
          $arr_ya_iri_hi_jokin_jun_kango = array(); //夜勤入り、非常勤、准看護師メンバー

          $arr_ya_iri_jokin_hojo = array();   //夜勤入り、常勤、補助メンバー
          $arr_ya_iri_hi_jokin_hojo = array();  //夜勤入り、非常勤、補助メンバー

          $arr_nikkin_jokin_kango = array();   //日勤、常勤、看護師メンバー
          $arr_nikkin_hi_jokin_kango = array();  //日勤、非常勤、看護師メンバー

          $arr_nikkin_jokin_jun_kango = array();  //日勤、常勤、准看護師メンバー
          $arr_nikkin_hi_jokin_jun_kango = array(); //日勤、非常勤、准看護師メンバー

          $arr_nikkin_jokin_hojo = array();   //日勤、常勤、補助メンバー
          $arr_nikkin_hi_jokin_hojo = array();  //日勤、非常勤、補助メンバー

          $arr_hankin_jokin_kango = array();   //半勤、常勤、看護師メンバー
          $arr_hankin_hi_jokin_kango = array();  //半勤、非常勤、看護師メンバー

          $arr_hankin_jokin_jun_kango = array();  //半勤、常勤、准看護師メンバー
          $arr_hankin_hi_jokin_jun_kango = array(); //半勤、非常勤、准看護師メンバー

          $arr_hankin_jokin_hojo = array();   //半勤、常勤、補助メンバー
          $arr_hankin_hi_jokin_hojo = array();  //半勤、非常勤、補助メンバー

          $arr_jokin_kokyu = array();     //常勤、公休メンバー
          $arr_jokin_nenkyu = array();     //常勤、年休メンバー

          $arr_hi_jokin_kokyu = array();    //非常勤、公休メンバー
          $arr_hi_jokin_nenkyu = array();    //非常勤、年休メンバー
          //その他 20130205
          $arr_jokin_other = array();     //常勤、その他メンバー
          $arr_hi_jokin_other = array();    //非常勤、その他メンバー
          //指定された日付、シフトグループで勤務パターンごとのメンバーを取得する
          list($arr_ya_ake_jokin_kango, $arr_ya_ake_hi_jokin_kango,
                  $arr_ya_ake_jokin_jun_kango, $arr_ya_ake_hi_jokin_jun_kango,
                  $arr_ya_ake_jokin_hojo, $arr_ya_ake_hi_jokin_hojo,
                  $arr_ya_iri_jokin_kango, $arr_ya_iri_hi_jokin_kango,
                  $arr_ya_iri_jokin_jun_kango, $arr_ya_iri_hi_jokin_jun_kango,
                  $arr_ya_iri_jokin_hojo, $arr_ya_iri_hi_jokin_hojo,
                  $arr_nikkin_jokin_kango, $arr_nikkin_hi_jokin_kango,
                  $arr_nikkin_jokin_jun_kango, $arr_nikkin_hi_jokin_jun_kango,
                  $arr_nikkin_jokin_hojo, $arr_nikkin_hi_jokin_hojo,
                  $arr_hankin_jokin_kango, $arr_hankin_hi_jokin_kango,
                  $arr_hankin_jokin_jun_kango, $arr_hankin_hi_jokin_jun_kango,
                  $arr_hankin_jokin_hojo, $arr_hankin_hi_jokin_hojo,
                  $arr_jokin_kokyu, $arr_jokin_nenkyu,
                  $arr_hi_jokin_kokyu, $arr_hi_jokin_nenkyu,
                  $arr_jokin_other, $arr_hi_jokin_other
                  ) = get_building_manage_diary_data($con, $start_date, $group_id, $fname);
      }
  }
  ?>

  <!-- ************************************************************************ -->
  <!-- JavaScript -->
  <!-- ************************************************************************ -->
  <script type="text/javascript" src="js/fontsize.js"></script>
  <script type="text/javascript" src="js/duty_shift/duty_shift_diary_bld_manage.js"></script>

  <script type="text/javascript">

      function openPrintPage()
      {
          window.open('duty_shift_diary_bld_manage_print.php?session=<? echo($session); ?>&group_id=<? echo($group_id); ?>&yyyymmdd=<? echo("$start_date"); ?>&wherefrom=1', 'newwin', 'width=840,height=700,scrollbars=yes');
              }

  </script>

  <!-- ************************************************************************ -->
  <!-- HTML -->
  <!-- ************************************************************************ -->
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <style type="text/css">
   .list {border-collapse:collapse;}
   .list td {border:#5279a5 solid 1px;}
  </style>
 </head>
 <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td>
     <!-- ------------------------------------------------------------------------ -->
     <!-- 画面遷移／タブ -->
     <!-- ------------------------------------------------------------------------ -->
     <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
        <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
        echo("</table>\n");

        // タブ
        if ($group_id == "nothing_group") 
        {
        $wk_group_id = "";
        } 
        else 
        {
        $wk_group_id = $group_id;
        }
        $arr_option = "&group_id=" . $wk_group_id;
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
        ?>
       </td></tr></table>
     <!-- ------------------------------------------------------------------------ -->
     <!-- データ不備時 -->
     <!-- ------------------------------------------------------------------------ -->
     <?
     if ($err_flg_1 == "1") 
     {
     echo("勤務シフトグループ情報が未設定です。管理画面で登録してください。");
     } 
     elseif ($create_flg_cnt == 0) 
     {
     echo("更新権限のあるシフトグループがありません。");
     } 
     else 
     {
     ?>
     <!-- ------------------------------------------------------------------------ -->
     <!-- 入力エリア -->
     <!-- ------------------------------------------------------------------------ -->
     <form name="mainform" method="post" action="duty_shift_diary_bld_manage.php">
      <table width="960" border="0" cellspacing="0" cellpadding="2" class="">
       <tr>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病棟管理日誌</b></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary.php?session=<?= $session ?>">勤務データ出力</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_allotment.php?session=<?= $session ?>">勤務分担表</a></font></td>
        <td align="left" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_sfc.php?session=<?= $session ?>">管理日誌（SFC形式）</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_list.php?session=<?= $session ?>">在院者一覧</a></font></td>
        <td align="left" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_diary_duty.php?session=<?= $session ?>">当直者一覧</a></font></td>
        <td align="left" width=""></td>
       </tr>
      </table>
      <!-- ------------------------------------------------------------------------ -->
      <!-- 勤務シフトグループ名 -->
      <!-- ------------------------------------------------------------------------ -->
      <table width="960" border="0" cellspacing="0" cellpadding="2">
       <tr height="22">
        <!-- 勤務シフトグループ名 -->
        <td width="250" align="left" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ</font>
         <select name="group_id">
          <option value="nothing_group">　</option>
          <?
          for($i=0;$i<count($group_array);$i++) 
          {
          //更新権限確認
          if ($group_array[$i]["create_flg"] != "1") 
          {
          continue;
          }
          $wk_id= $group_array[$i]["group_id"];
          $wk_name = $group_array[$i]["group_name"];
          echo("<option value=\"$wk_id\"");
          if ($group_id == $wk_id) 
          {
          echo(" selected");
          }
          echo(">$wk_name\n");
          }
          ?>
         </select>
        </td>
        <td width="460" align="left" nowrap>
         <select id='start_yr' name='start_yr'><? 
          show_select_years(15, $start_yr, false);
          ?></select>年
         <select id='start_mon' name='start_mon'><? 
          show_select_months($start_mon, false);
          ?></select>月
         <select id='start_day' name='start_day'><? 
          show_select_days($start_day, false);
          ?></select>日
         <!-- 表示ボタン -->
         <input type="submit" name="actionDisp" value="表示"  onclick="displayList();">
        </td>

        <td align="right" width="250">
         <?
         // 表示、検索後
         if ($actionDispShift == "表示" && $group_id != nothing_group) 
         {
         ?>

         <input type="button" value="印刷" onclick="openPrintPage();">
         <input type="button" value="EXCEL出力"  onclick="makeExcel();">
         <? 
         } 
         ?>
        </td>
       </tr>

       <?
       // 表示、検索後
       if ($actionDispShift == "表示" && $group_id != nothing_group) 
       {
       ?>

       <tr height="22">
        <!-- 当直医師名 -->
        <td colspan="19" align="left" nowrap>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title2["duty"] ?>　<?= $arr_title2["internist"] ?>　　　　　　　　　　　　　　　<?= $arr_title2["surgeon"] ?>　　　　　　　　　　　　　　　<?= $arr_title2["charge"] ?></font>
        </td>
       </tr>
       <? 
       } 
       ?>
      </table>
      <?
      // 表示、検索後
      if ($actionDispShift == "表示" && $group_id != nothing_group) 
      {
      ?>
      <!-- ------------------------------------------------------------------------ -->
      <!-- 表 -->
      <!-- ------------------------------------------------------------------------ -->
      <table width="960" border="0" cellspacing="0" cellpadding="2" class="list">
       <!-- ------------------------------------------------------------------------ -->
       <!-- パターン一覧（見出し） -->
       <!-- ------------------------------------------------------------------------ -->

       <tr align="center">
        <td colspan="19"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head1"] ?></font></td>
       </tr>
       <tr align="center">
        <td rowspan="2">　</td>
        <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head2-1"] ?></font></td>
        <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head2-2"] ?></font></td>
        <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head2-3"] ?></font></td>
        <td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head2-4"] ?></font></td>
        <td>　</td>
        <td colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head2-5"] ?></font></td>
        <td rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["gokei"] ?></font></td>
       </tr>
       <tr align="center">
        <?
        //4回ループ…夜勤明、日勤、半勤、夜勤入のそれぞれに看准補をそれぞれぶら下げる 
        for ($i=0; $i<4 ;$i++) 
        {
        ?>

        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-1"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-2"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-3"] ?></font></td>
        <?
        }
        ?>

        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-4"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-5"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-6"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-7"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["head3-4"] ?></font></td>
       </tr>

       <!-- ------------------------------------------------------------------------ -->
       <!-- パターン一覧（データ） -->
       <!-- ------------------------------------------------------------------------ -->
       <?
       //常勤用表示エリア
       $cnt=0;
       if(0 < count($data))
       {
       ?>
       <tr align="center">
        <td colspan="19"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["joukin"] ?></font></td>
       </tr>
       <?
       for ($i=0; $i<count($data); $i++) 
       {
       echo("<tr height=\"22\"> \n");
       for ($j=0; $j<18; $j++) 
       { // 18 = 看准補 X 4 ,小計 X 2 , 公休 ,年休 , スペース ,合計 
       // width=\"8%\"
       echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
       echo($data[$i][$j]);
       echo("</font></td> \n");
       }
       echo("</tr>\n");
       }
       }
       ?>

       <?
       //非常勤用表示エリア
       $cnt=0;
       if(0 < count($data))
       {
       ?>
       <tr align="center">
        <td colspan="19"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["hijoukin"] ?></font></td>
       </tr>
       <?
       for ($i=0; $i<count($data); $i++) 
       {
       echo("<tr height=\"22\"> \n");
       for ($j=0; $j<18; $j++) 
       { // 18 = 看准補 X 4 ,小計 X 2 , 公休 ,年休 , スペース ,合計 
       // width=\"8%\"
       echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
       echo($data[$i][$j]);
       echo("</font></td> \n");
       }
       echo("</tr>\n");
       }
       }

       //合計用表示エリア
       $cnt=0;
       if(0 < count($data))
       {
       ?>

       <tr align="center">
        <td colspan="19"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title["gokei"] ?></font></td>
       </tr>
       <?
       for ($i=0; $i<count($data); $i++) 
       {
       echo("<tr height=\"22\"> \n");
       for ($j=0; $j<18; $j++) 
       { // 18 = 看准補 X 4 ,小計 X 2 , 公休 ,年休 , スペース ,合計 
       // width=\"8%\"
       echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n");
       echo($data[$i][$j]);
       echo("</font></td> \n");
       }
       echo("</tr>\n");
       }
       }
       ?>
       <tr align="left" valign="top">
        <td valign="middle" align="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">常勤</font></td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、常勤、看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_jokin_kango); $j++) 
          {
          echo($arr_ya_ake_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、常勤、准看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_jokin_jun_kango); $j++) 
          {
          echo($arr_ya_ake_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>

         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_jokin_hojo); $j++) 
          {
          echo($arr_ya_ake_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、常勤、看護師メンバー
          for ($j=0; $j<count($arr_nikkin_jokin_kango); $j++) 
          {
          echo($arr_nikkin_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_nikkin_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>

         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、常勤、准看護師メンバー
          for ($j=0; $j<count($arr_nikkin_jokin_jun_kango); $j++) 
          {
          echo($arr_nikkin_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_nikkin_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>

         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_nikkin_jokin_hojo); $j++) 
          {
          echo($arr_nikkin_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_nikkin_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、常勤、看護師メンバー
          for ($j=0; $j<count($arr_hankin_jokin_kango); $j++) 
          {
          echo($arr_hankin_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_hankin_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、常勤、准看護師メンバー
          for ($j=0; $j<count($arr_hankin_jokin_jun_kango); $j++) 
          {
          echo($arr_hankin_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_hankin_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_hankin_jokin_hojo); $j++) 
          {
          echo($arr_hankin_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_hankin_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入り、常勤、看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_jokin_kango); $j++) 
          {
          echo($arr_ya_iri_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入り、常勤、准看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_jokin_jun_kango); $j++) 
          {
          echo($arr_ya_iri_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入り、常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_jokin_hojo); $j++) 
          {
          echo($arr_ya_iri_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤　小計
          $jokin_total_count= count($arr_ya_ake_jokin_kango) + 
          count($arr_ya_ake_jokin_jun_kango) + 
          count($arr_ya_ake_jokin_hojo) + 
          count($arr_nikkin_jokin_kango) + 
          count($arr_nikkin_jokin_jun_kango) + 
          count($arr_nikkin_jokin_hojo) + 
          count($arr_hankin_jokin_kango) + 
          count($arr_hankin_jokin_jun_kango) + 
          count($arr_hankin_jokin_hojo) + 
          count($arr_ya_iri_jokin_kango) + 
          count($arr_ya_iri_jokin_jun_kango) + 
          count($arr_ya_iri_jokin_hojo) ;

          echo($jokin_total_count);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、公休メンバー
          for ($j=0; $j<count($arr_jokin_kokyu); $j++) 
          {
          echo($arr_jokin_kokyu[$j]["emp_lt_nm"]);
          echo($arr_jokin_kokyu[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、年休メンバー
          for ($j=0; $j<count($arr_jokin_nenkyu); $j++) 
          {
          echo($arr_jokin_nenkyu[$j]["emp_lt_nm"]);
          echo($arr_jokin_nenkyu[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <!-- その他行 --><? // その他追加 20130205 ?>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、その他メンバー
          for ($j=0; $j<count($arr_jokin_other); $j++) 
          {
          echo($arr_jokin_other[$j]["emp_lt_nm"]);
          echo($arr_jokin_other[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font></td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤　休暇小計
          $jokin_kyuka_total_count= count($arr_jokin_kokyu) + count($arr_jokin_nenkyu) + count($arr_jokin_other);

          echo($jokin_kyuka_total_count);

          ?>
         </font>
        </td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤　合計
          $jokin_all_total_count= $jokin_total_count + $jokin_kyuka_total_count;

          echo($jokin_all_total_count);

          ?>
         </font>
        </td>

       </tr>


       <tr align="left" valign="top">
        <td align="middle" valign="middle"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非常勤</font></td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、非常勤、看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_hi_jokin_kango); $j++) 
          {
          echo($arr_ya_ake_hi_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_hi_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、非常勤、准看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_hi_jokin_jun_kango); $j++) 
          {
          echo($arr_ya_ake_hi_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_hi_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明け、非常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_ya_ake_hi_jokin_hojo); $j++) 
          {
          echo($arr_ya_ake_hi_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_ya_ake_hi_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、非常勤、看護師メンバー
          for ($j=0; $j<count($arr_nikkin_hi_jokin_kango); $j++) 
          {
          echo($arr_nikkin_hi_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_nikkin_hi_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

          <?
          //日勤、非常勤、准看護師メンバー
          for ($j=0; $j<count($arr_nikkin_hi_jokin_jun_kango); $j++) 
          {
          echo($arr_nikkin_hi_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_nikkin_hi_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、非常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_nikkin_hi_jokin_hojo); $j++) 
          {
          echo($arr_nikkin_hi_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_nikkin_hi_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、非常勤、看護師メンバー
          for ($j=0; $j<count($arr_hankin_hi_jokin_kango); $j++) 
          {
          echo($arr_hankin_hi_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_hankin_hi_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、非常勤、准看護師メンバー
          for ($j=0; $j<count($arr_hankin_hi_jokin_jun_kango); $j++) 
          {
          echo($arr_hankin_hi_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_hankin_hi_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、非常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_hankin_hi_jokin_hojo); $j++) 
          {
          echo($arr_hankin_hi_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_hankin_hi_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入り、非常勤、看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_hi_jokin_kango); $j++) 
          {
          echo($arr_ya_iri_hi_jokin_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_hi_jokin_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入り、非常勤、准看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_hi_jokin_jun_kango); $j++) 
          {
          echo($arr_ya_iri_hi_jokin_jun_kango[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_hi_jokin_jun_kango[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

          <?
          //夜勤入り、非常勤、補助看護師メンバー
          for ($j=0; $j<count($arr_ya_iri_hi_jokin_hojo); $j++) 
          {
          echo($arr_ya_iri_hi_jokin_hojo[$j]["emp_lt_nm"]);
          echo($arr_ya_iri_hi_jokin_hojo[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤　小計
          $hi_jokin_total_count= count($arr_ya_ake_hi_jokin_kango) + 
          count($arr_ya_ake_hi_jokin_jun_kango) + 
          count($arr_ya_ake_hi_jokin_hojo) + 
          count($arr_nikkin_hi_jokin_kango) + 
          count($arr_nikkin_hi_jokin_jun_kango) + 
          count($arr_nikkin_hi_jokin_hojo) + 
          count($arr_hankin_hi_jokin_kango) + 
          count($arr_hankin_hi_jokin_jun_kango) + 
          count($arr_hankin_hi_jokin_hojo) + 
          count($arr_ya_iri_hi_jokin_kango) + 
          count($arr_ya_iri_hi_jokin_jun_kango) + 
          count($arr_ya_iri_hi_jokin_hojo) ;

          echo($hi_jokin_total_count);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

          <?
          //非常勤、公休メンバー
          for ($j=0; $j<count($arr_hi_jokin_kokyu); $j++) 
          {
          echo($arr_hi_jokin_kokyu[$j]["emp_lt_nm"]);
          echo($arr_hi_jokin_kokyu[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>

         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

          <?
          //非常勤、年休メンバー
          for ($j=0; $j<count($arr_hi_jokin_nenkyu); $j++) 
          {
          echo($arr_hi_jokin_nenkyu[$j]["emp_lt_nm"]);
          echo($arr_hi_jokin_nenkyu[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font>
        </td>


        <!-- その他行 -->
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //非常勤、その他メンバー
          for ($j=0; $j<count($arr_hi_jokin_other); $j++) 
          {
          echo($arr_hi_jokin_other[$j]["emp_lt_nm"]);
          echo($arr_hi_jokin_other[$j]["emp_ft_nm"]);
          echo("<br>");
          }
          ?>
         </font></td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //非常勤　休暇小計
          $hi_jokin_kyuka_total_count= count($arr_hi_jokin_kokyu) + count($arr_hi_jokin_nenkyu) + count($arr_hi_jokin_other);

          echo($hi_jokin_kyuka_total_count);

          ?>
         </font>
        </td>

        <td align="middle" valign="middle">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //非常勤　合計
          $hi_jokin_all_total_count= $hi_jokin_total_count + $hi_jokin_kyuka_total_count;

          echo($hi_jokin_all_total_count);

          ?>
         </font>
        </td>

       </tr>


       <tr align="center">
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">合計</font></td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明、看護師、合計
          $yakin_ake_kango_gokei= count($arr_ya_ake_jokin_kango) + count($arr_ya_ake_hi_jokin_kango);

          echo($yakin_ake_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明、准看護師、合計
          $yakin_ake_jun_kango_gokei= count($arr_ya_ake_jokin_jun_kango) + count($arr_ya_ake_hi_jokin_jun_kango);

          echo($yakin_ake_jun_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤明、補助看護師、合計
          $yakin_ake_hojo_kango_gokei= count($arr_ya_ake_jokin_hojo) + count($arr_ya_ake_hi_jokin_hojo);

          echo($yakin_ake_hojo_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、看護師、合計
          $nikkin_kango_gokei= count($arr_nikkin_jokin_kango) + count($arr_nikkin_hi_jokin_kango);

          echo($nikkin_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、准看護師、合計
          $nikkin_jun_kango_gokei= count($arr_nikkin_jokin_jun_kango) + count($arr_nikkin_hi_jokin_jun_kango);

          echo($nikkin_jun_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //日勤、補助看護師、合計
          $nikkin_hojo_kango_gokei= count($arr_nikkin_jokin_hojo) + count($arr_nikkin_hi_jokin_hojo);

          echo($nikkin_hojo_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、看護師、合計
          $hankin_kango_gokei= count($arr_hankin_jokin_kango) + count($arr_hankin_hi_jokin_kango);

          echo($hankin_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、准看護師、合計
          $hankin_jun_kango_gokei= count($arr_hankin_jokin_jun_kango) + count($arr_hankin_hi_jokin_jun_kango);

          echo($hankin_jun_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //半勤、補助看護師、合計
          $hankin_hojo_kango_gokei= count($arr_hankin_jokin_hojo) + count($arr_hankin_hi_jokin_hojo);

          echo($hankin_hojo_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入、看護師、合計
          $yakin_iri_kango_gokei= count($arr_ya_iri_jokin_kango) + count($arr_ya_iri_hi_jokin_kango);

          echo($yakin_iri_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入、准看護師、合計
          $yakin_iri_jun_kango_gokei= count($arr_ya_iri_jokin_jun_kango) + count($arr_ya_iri_hi_jokin_jun_kango);

          echo($yakin_iri_jun_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //夜勤入、補助看護師、合計
          $yakin_iri_hojo_kango_gokei= count($arr_ya_iri_jokin_hojo) + count($arr_ya_iri_hi_jokin_hojo);

          echo($yakin_iri_hojo_kango_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、非常勤　合計
          $joukin_hi_joukin_gokei= $jokin_total_count + $hi_jokin_total_count;

          echo($joukin_hi_joukin_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、非常勤　公休合計
          $kokyu_gokei= count($arr_jokin_kokyu) + count($arr_hi_jokin_kokyu);

          echo($kokyu_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、非常勤　年休合計
          $nenkyu_gokei= count($arr_jokin_nenkyu) + count($arr_hi_jokin_nenkyu);

          echo($nenkyu_gokei);

          ?>
         </font>
        </td>

        <!-- その他行 -->
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //常勤、非常勤　その他合計
          $other_gokei= count($arr_jokin_other) + count($arr_hi_jokin_other);

          echo($other_gokei);

          ?>
         </font></td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //公休・年休合計
          $kokyu_nenkyu_gokei= $kokyu_gokei + $nenkyu_gokei + $other_gokei;

          echo($kokyu_nenkyu_gokei);

          ?>
         </font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
          <?
          //全合計
          $all_gokei= $jokin_all_total_count + $hi_jokin_all_total_count;

          echo($all_gokei);

          ?>
         </font>
        </td>
       </tr>
      </table>

      <!-- ------------------------------------------------------------------------ -->
      <!-- 表(重症・要注意・手術・その他以下) -->
      <!-- ------------------------------------------------------------------------ -->
      <table width="960" border="0" cellspacing="0" cellpadding="2" class="list">

       <tr align="center">
        <td colspan="8"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head1"] ?></font></td>
       </tr>

       <tr align="center">
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-1"] ?></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-2"] ?></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-3"] ?></font></td>
        <td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-4"] ?></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-5"] ?></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-6"] ?></font></td>
        <td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-7"] ?></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head2-8"] ?></font></td>

       </tr>


       <?
       //13行ぶん空白行を表示する
       for ($bk_cnt=0; $bk_cnt<13; $bk_cnt++) 
       {
       ?>
       <tr align="center">
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　</font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>


       </tr>

       <?
       }
       ?>
      </table>

      <!-- ------------------------------------------------------------------------ -->
      <!-- 表(巡視以下) -->
      <!-- ------------------------------------------------------------------------ -->
      <table width="960" cellspacing="0" cellpadding="2" class="list">

       <tr align="center">
        <td rowspan="3" width="30">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-1"] ?></font>
        </td>
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-2"] ?></font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">0</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">1</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">2</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">3</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">4</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">5</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">6</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">7</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">8</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">9</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">10</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">11</font>
        </td>
       </tr>

       <tr align="center">
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-3"] ?></font>
        </td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
       </tr>
       <tr align="center">
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-4"] ?></font>
        </td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
       </tr>

       <tr align="center">
        <td rowspan="3" width="30">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-1"] ?></font>
        </td>
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-2"] ?></font>
        </td>

        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">12</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">13</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">14</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">15</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">16</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">17</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">18</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">19</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">20</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">21</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">22</font>
        </td>
        <td>
         <font size="3" face="ＭＳ Ｐゴシック, Osaka">23</font>
        </td>
       </tr>

       <tr align="center">
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-3"] ?></font>
        </td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
       </tr>
       <tr align="center">
        <td width="100">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-4"] ?></font>
        </td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
        <td>　</td>
       </tr>
       <tr align="left">
        <td colspan="14">
         <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr_title3["head3-5"] ?><br><br><br><br></font>
        </td>
       </tr>
      </table>
      <? 
      } 
      ?>
      <!-- ------------------------------------------------------------------------ -->
      <!-- ＨＩＤＤＥＮ -->
      <!-- ------------------------------------------------------------------------ -->
      <input type="hidden" name="session" value="<? echo($session); ?>">
      <input type="hidden" name="yyyymmdd" value="<? echo($start_date); ?>">
      <input type="hidden" name="actionDispShift">


     </form>
     <?
     // target="download"
     ?>
     <iframe name="download" width="0" height="0" frameborder="0"></iframe>

     <? 
     } 
     ?>

    </td>
   </tr>


  </table>
 </body>
 <? pg_close($con); ?>
</html>
