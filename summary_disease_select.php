<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜傷病名選択</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);

// 登録済みの傷病名を検索
$sql = "select * from sum_pt_disease";
$cond = q("where ptif_id = '%s' and (to_date(end_ymd, 'YYYYMMDD') >= current_date or end_ymd is null) order by start_ymd", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/swapimg.js"></script>
<script type="text/javascript">
// 傷病名選択時の処理
function selectDisease(byoumei, start_ymd) {
    if (opener && !opener.closed && opener.selectDisease) {
        opener.selectDisease(byoumei, start_ymd);
    }
}

// セルハイライトON
function highlightCells(tr) {
    changeCellsColor(tr, '#fefc8f');
}

// セルハイライトOFF
function dehighlightCells(tr) {
    changeCellsColor(tr, '');
}

// セルの色を設定
function changeCellsColor(tr, color) {
    tr.style.backgroundColor = color;
}
</script>
</head>
<body style="background-color:#f4f4f4">
<div style="padding:4px">
<div>
  <table class="dialog_titletable"><tr>
    <th>傷病名選択</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
  </tr></table>
</div>

<form name="frm" action="summary_kensakekka_timeseries.php" method="post">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" style="width:100%;" name="itm_ord" id="itm_ord" value="">
</form>
<form id="mainform">
<table class="prop_table" cellspacing="1">
<tr>
<th>傷病名</th>
<th>開始年月日</th>
<th>主</th>
<th>特疾</th>
</tr>
<?
   $line_script = 'onmouseover="highlightCells(this.parentNode);" onmouseout="dehighlightCells(this.parentNode);"';
   while ($row = pg_fetch_array($sel)) {
?>
<tr onclick="selectDisease('<? echo h_jsparam($row["disease_name"]); ?>', '<?=eh(format_date($row["start_ymd"]))?>');">
<td style="cursor: pointer;" <? echo $line_script; ?>><? eh($row["disease_name"]); ?></td>
<td style="text-align: center; cursor: pointer;" <? echo $line_script; ?>><? eh(format_date($row["start_ymd"])); ?></td>
<td style="text-align: center; cursor: pointer;" <? echo $line_script; ?>><? echo ($row["is_primary"] === "t") ? '主' : ''; ?></td>
<td style="text-align: center; cursor: pointer;" <? echo $line_script; ?>><? echo ($row["is_specified"] === "t") ? '特' : ''; ?></td>
</tr>
<? } ?>
</table>
</form>
</div>
</body>
</html>
<? pg_close($con); ?>
<?
function format_date($ymd) {
    if (strlen($ymd) === 8) {
        return preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $ymd);
    }
    return "";
}
