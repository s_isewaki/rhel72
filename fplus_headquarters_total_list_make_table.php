<?
//list_type が c,f 以外の場合
function gettable_a($data_list,$horz_field_definision,$date_this_y,$data_count,$xlstitle){
	$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

	foreach ($horz_field_definision as $idx => $row){
		$colwidth_sum += (int)$row["colwidth"];
	}
	$tableWidth = "width:{$colwidth_sum}px;";
	echo("<table class=\"list1\" style=\"".$tableWidth." background-color:#fff;\">");
	echo("<tr>");
	echo("<td style=\"text-align:center; border:0;\" colspan=\"".count($horz_field_definision)."\">".$font.h($xlstitle)."</font></td>");
	echo("</tr>");
	echo("<tr>");
	echo("<td style=\"text-align:left; border:0;\" colspan=\"".count($horz_field_definision)."\">".$font.$date_this_y."年度</font></td>");
	echo("</tr>");
	// 列ヘッダ行
	echo("<tr style=\"background-color:#ffdfff;\">");
	for ($hh = 0; $hh < count($horz_field_definision); $hh++){
		$cel_no=$hh+1;
		echo("<td id=\"cell_".$cel_no."_0\" style=\"text-align:center; width:".$horz_field_definision[$hh]["colwidth"]."px;\">".$font);
		echo($horz_field_definision[$hh]["text"]);
		echo("</font></td>");
	}
	echo("</tr>");
	if ($data_count == 0){
		echo("<tr>");
		echo("<td style=\"background-color:#eee;text-align:center;\" colspan=\"".count($horz_field_definision)."\">");
		echo("<div style=\"color:#999; text-align:center; margin-top:0px\">".$font."該当データはありません。</font></div>");
		echo("</td>");
		echo("</tr>");
		echo("</table>");
	} else {
		for($i=0;$i<$data_count;$i++){
			echo("<tr>");
			for ($j = 0; $j < count($horz_field_definision); $j++){
				if($j < 2){
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;background-color:#ffdfff;\">".$font.h($data_list[$i][$j])."</font></td>");
				}else{
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;\">".$font.h($data_list[$i][$j])."</font></td>");
				}
			}
			echo("</tr>");
		}
	echo("</table>");
	}
}

//list_type が c の場合
function gettable_c($data_list,$horz_field_definision,$job,$date_this_y,$data_count,$xlstitle){
	$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

	foreach ($horz_field_definision as $idx => $row){
		$colwidth_sum += (int)$row["colwidth"];
	}
	$tableWidth = "width:{$colwidth_sum}px;";
	echo("<table class=\"list1\" style=\"".$tableWidth." background-color:#fff;\">");
	echo("<tr>");
	echo("<td id=\"cell_5_0\" style=\"text-align:center;border:0;\" colspan=\"3\">".$font);
	echo("</font></td>");
	echo("<td style=\"text-align:left; border:0;\" colspan=\"".(count($horz_field_definision)-3)."\">".$font.h($xlstitle)."</font></td>");
	echo("</tr>");
	echo("<tr>");
	echo("<td style=\"width:100%;text-align:left; border:0;\" colspan=\"".count($horz_field_definision)."\">".$font.$date_this_y."年度</font></td>");
	echo("</tr>");
	// 列ヘッダ行
	echo("<tr style=\"background-color:#ffdfff;\">");
	for ($hh = 0; $hh < 4; $hh++){
		$cel_no=$hh+1;
		echo("<td id=\"cell_".cel_no."_0\" style=\"text-align:center; width:".$horz_field_definision[$hh]["colwidth"]."px;\">".$font);
		echo("</font></td>");
	}
	echo("<td id=\"cell_5_0\" style=\"text-align:center;\" colspan=\"".(count($job)+1)."\">".$font);
	echo("総数");
	echo("</font></td>");
	echo("<td id=\"cell_6_0\" style=\"text-align:center;\" colspan=\"".(count($job)+1)."\">".$font);
	echo("レベル0");
	echo("</font></td>");
	echo("</tr>");
	echo("<tr style=\"background-color:#ffdfff;\">");
	for ($hh = 0; $hh < count($horz_field_definision); $hh++){
		$cel_no=$hh+1;
		echo("<td id=\"cell_".cel_no."_1\" style=\"text-align:center; width:".$horz_field_definision[$hh]["colwidth"]."px;\">".$font);
		echo($horz_field_definision[$hh]["text"]);
		echo("</font></td>");
	}
	echo("</tr>");
	if ($data_count == 0){
		echo("</table>");
		echo("<div style=\"background-color:#eee;color:#999; text-align:center; margin-top:0px\">".$font."該当データはありません。</font></div>");
	} else {
		for($i=0;$i<$data_count;$i++){
			echo("<tr>");
			for ($j = 0; $j < count($horz_field_definision); $j++){
				if($j < 2){
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;background-color:#ffdfff;\">".$font.h($data_list[$i][$j])."</font></td>");
				}else{
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;\">".$font.h($data_list[$i][$j])."</font></td>");
				}
			}
			echo("</tr>");
		}
	echo("</table>");
	}
}

//list_type が f の場合
function gettable_f($data_list,$horz_field_definision,$date_this_y,$data_count,$xlstitle){
	$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

	foreach ($horz_field_definision as $idx => $row){
		$colwidth_sum += (int)$row["colwidth"];
	}
	$tableWidth = "width:{$colwidth_sum}px;";
	echo("<table class=\"list1\" style=\"".$tableWidth." background-color:#fff;\">");
	echo("<tr>");
	echo("<td id=\"cell_5_0\" style=\"text-align:center;border:0;\" colspan=\"3\">".$font);
	echo("</font></td>");
	echo("<td style=\"text-align:left; border:0;\" colspan=\"".(count($horz_field_definision)-3)."\">".$font.h($xlstitle)."</font></td>");
	echo("</tr>");
	echo("<tr>");
	echo("<td style=\"width:100%;text-align:left; border:0;\" colspan=\"".count($horz_field_definision)."\">".$font.$date_this_y."年度</font></td>");
	echo("</tr>");
	// 列ヘッダ行
	echo("<tr>");
	for ($hh = 0; $hh < 5; $hh++){
		echo("<td style=\"text-align:center;border:0;width:".$horz_field_definision[$hh]["colwidth"]."px;\">".$font);
		echo("</font></td>");
	}
	echo("<td style=\"background-color:#ffdfff;text-align:center;\" colspan=\"3\">".$font);
	echo("要件");
	echo("</font></td>");
	echo("<td style=\"background-color:#ffdfff;text-align:center;\" colspan=\"7\">".$font);
	echo("概要");
	echo("</font></td>");
	echo("</tr>");
	echo("<tr style=\"background-color:#ffdfff;\">");
	for ($hh = 0; $hh < count($horz_field_definision); $hh++){
		$cel_no=$hh+1;
		echo("<td id=\"cell_".cel_no."_1\" style=\"text-align:center; width:".$horz_field_definision[$hh]["colwidth"]."px;\">".$font);
		echo($horz_field_definision[$hh]["text"]);
		echo("</font></td>");
	}
	echo("</tr>");
	if ($data_count == 0){
		echo("<tr>");
		echo("<td style=\"background-color:#eee;text-align:center;\" colspan=\"".count($horz_field_definision)."\">");
		echo("<div style=\"color:#999; text-align:center; margin-top:0px\">".$font."該当データはありません。</font></div>");
		echo("</td>");
		echo("</tr>");
		echo("</table>");
	} else {
		for($i=0;$i<$data_count;$i++){
			echo("<tr>");
			for ($j = 0; $j < count($horz_field_definision); $j++){
				if($j < 2){
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;background-color:#ffdfff;\">".$font.h($data_list[$i][$j])."</font></td>");
				}else{
					echo("<td style=\"text-align:".$horz_field_definision[$j]["data_align"]."; width:".$horz_field_definision[$j]["colwidth"]."px;\">".$font.h($data_list[$i][$j])."</font></td>");
				}
			}
			echo("</tr>");
		}
	echo("</table>");
	}
}

//概要別事故報告件数
function gettable_g($data_list, $horz_field_definision, $date_this_y, $data_count, $xlstitle){
	$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
	foreach ($horz_field_definision as $idx => $row){
		$colwidth_sum += (int)$row["colwidth"];
	}
	?>
	<table width="500" class="list1" style="background-color: #fff;">
		<tr>
			<td colspan="<?= count($horz_field_definision) ?>" style="text-align: center; border: 0px">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= h($xlstitle) ?>
				</font>
			</td>
		</tr>
		<tr>
			<td colspan="<?= count($horz_field_definision) ?>" style="border: 0px">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= $date_this_y . "年度" ?>
				</font>
			</td>
		</tr>
		<tr style="background-color: #ffdfff">
			<?
			for ($j = 0; $j < count($horz_field_definision); $j++) {
			?>
			<td width="<?= $horz_field_definision[$j]["colwidth"] ?>%" style="text-align: center">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= $horz_field_definision[$j]["text"] ?>
				</font>
			</td>
			<?
			}
			?>
		</tr>
		<?
		if ($data_count == 0) {
		?>
			<tr>
				<td colspan="<?= count($horz_field_definision) ?>" style="text-align: center;background-color:#eee;">
					<div style="color:#999; text-align:center; margin-top:0px">
						<?=$font?>該当データはありません。</font>
					</div>
				</td>
			</tr>
		
		<?
		} else {
			for ($i = 0; $i < count($data_list); $i++) {
			?>
				<tr>
					<?
					for ($j = 0; $j < count($horz_field_definision); $j++) {
						if ($j == 0) {
							$bg_color = "#ffdfff";
						} else {
							$bg_color = "#ffffff";
						}
						?>
						<td align="<?= $horz_field_definision[$j]["data_align"] ?>" style="background-color: <?= $bg_color ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
							<?= h($data_list[$i][$j]) ?>
							</font>
						</td>
					<?
					}
					?>
				</tr>
			<?
			}
		}
		?>
	</table>
<?
}

//年度別事故報告件数
function gettable_h($data_list,$horz_field_definision,$data_count,$xlstitle,$date_this_y){
	$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
	foreach ($horz_field_definision as $idx => $row){
		$colwidth_sum += (int)$row["colwidth"];
	}
	$tableWidth = $colwidth_sum."px";
	?>
	<table width="<?=$tableWidth?>" class="list1" style="background-color: #fff;">
		<tr>
			<td colspan="<?= count($horz_field_definision) ?>" style="text-align: center; border: 0px">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= h($xlstitle) ?>
				</font>
			</td>
		</tr>
		<tr>
			<td colspan="<?= count($horz_field_definision) ?>" style="border: 0px">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= $date_this_y . "年度" ?>
				</font>
			</td>
		</tr>
		<tr style="background-color: #ffdfff">
			<?
			for ($j = 0; $j < count($horz_field_definision); $j++) {
			?>
			<td width="<?= $horz_field_definision[$j]["colwidth"] ?>px" style="text-align: center ">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<?= $horz_field_definision[$j]["text"] ?>
				</font>
			</td>
			<?
			}
			?>
		</tr>
		<?
		if ($data_count == 0) {
		?>
			<tr>
				<td colspan="<?= count($horz_field_definision) ?>" style="background-color:#eee;text-align: center;">
					<div style="color:#999; text-align:center; margin-top:0px">
						<?=$font?>該当データはありません。</font>
					</div>
				</td>
			</tr>
		<?
		} else {
		?>
			<tr>
				<?
				for ($i = 0; $i < count($data_list); $i++) {
					for($j = 0; $j < count($horz_field_definision); $j++){
						if ($j == 0) {
							$bg_color = "#ffdfff";
						} else {
							$bg_color = "#ffffff";
						}
						?>
						<td align="<?= $horz_field_definision[$j]["data_align"] ?>" style="background-color: <?= $bg_color ?>">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
							<?= h($data_list[$i][$j]) ?>
							</font>
						</td>
				<?
					}
				}
				?>
			</tr>
		<?
		}
		?>
	</table>
<?
}

?>