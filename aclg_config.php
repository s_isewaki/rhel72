<?
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

require("get_values.php");
require("label_by_profile_type.ini");
require("summary_common.ini");
require_once("sot_util.php");
require_once("get_menu_label.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 92, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//DB接続
$con = connect2db($fname);
if ($con == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
    $lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// system_config
$conf = new Cmx_SystemConfig();

if($regstat == "1")
{
	//ログの重要度を変更

    $sql = "select * from aclg_data_link ";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {

		foreach ($_POST as $tmp_key => $tmp_data) {

			//POSTするとドットがアンスコに変更されるので再変換する
			//$rcd = ereg_replace("[/\.]", "_", $row["log_config"]);
			//$rcd = $row["log_cate"]."_".$row["log_detail"];

			if($row["cate_id"].$row["detail_id"] === $tmp_key)
			{
				//echo("check:".$tmp_key);
				//exit();

				// 設定を更新
				$sql = "update aclg_data_link set";
				$set = array("priority");
				$setvalue = array($tmp_data);
				$cond = "where (cate_id = '{$row["cate_id"]}' and detail_id = '{$row["detail_id"]}')";

				// ファントルくんで「報告書をゴミ箱へ移動」の時はドラッグドロップでもログを取得する
				if ($row["cate_id"].$row["detail_id"] === '02017') {
					$cond .= " or (cate_id = '0201' and detail_id = '15')";
				}

				// ファントルくんで「報告書をゴミ箱から戻す」の時はドラッグドロップでもログを取得する
				else if ($row["cate_id"].$row["detail_id"] === '02018') {
					$cond .= " or (cate_id = '0201' and detail_id = '16')";
				}

				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
	}
}
elseif($regstat == "2")
{
	//ログの保存期間を修正
	//echo($regstat);

	// 表示設定をINSERT
	$ret_val = $conf->set("aclg_set.term_save",$date_y);
}

$date_y = $conf->get("aclg_set.term_save");
//echo($date_y);


function cbInfo($cate,$detail){
	// DBコネクションの作成
	$con = connect2db($fname);

	//$action="委員会・WG.委員会の削除";
	$sql = "select priority from aclg_data_link";
	$cond = "where cate_id = '$cate' and detail_id= '$detail'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$priority = @pg_fetch_result($sel, 0, "priority");

	if ($cate !== '0006') {
		echo "<select name=\"$cate$detail\">";
		echo "<option value=\"4\"";
		echo ($priority === '4') ? "selected>取得しない</option>" : ">取得しない</option>";
		echo "<option value=\"1\"";
		echo ($priority === '1') ? "selected>重要情報</option>" : ">重要情報</option>";
		echo "<option value=\"2\"";
		echo ($priority === '2') ? "selected>一般情報</option>" : ">一般情報</option>";
		echo "<option value=\"3\"";
		echo ($priority === '3') ? "selected>付加情報</option>" : ">付加情報</option>";
		echo "</select>";
	}
	else {
		echo "<select name=\"$cate$detail\">";
		echo "<option value=\"4\"";
		echo ($priority === '4') ? "selected>取得しない</option>" : ">取得しない</option>";
		echo "<option value=\"1\"";
		echo ($priority === '1') ? "selected>取得する</option>" : ">取得する</option>";
		echo "</select>";
	}
}
?>
<title>CoMedix アクセスログ | 設定</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">

<style type="text/css">
.list { border-collapse:collapse; }
.list td { border:#5279a5 solid 1px; }
.tool { width:125px; }
.ope { width:400px; }
</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onresize="adjustListWidth()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="header_menu_table">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="aclg_menu.php?session=<? echo($session); ?>"><img src="img/icon/b_dmy_log.gif" width="32" height="32" border="0" alt="アクセスログ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="aclg_menu.php?session=<? echo($session); ?>"><b>アクセスログ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->


<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="150" align="center" bgcolor="#bdd1e7"><a href="./aclg_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧（ユーザ）</font></a></td>
<td width="5">&nbsp;</td>
<td width="150" align="center" bgcolor="#bdd1e7"><a href="./aclg_menu_admin.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧（管理画面）</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./aclg_tools.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">機能別詳細</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="./aclg_config.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>設定</b></font></a></td>
<td></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<?
//==========================================================================
// 検索条件
//==========================================================================
//if (@$_REQUEST["mode"]!="search"){
//    $date_y = date("Y");
//    $date_m = date("m");
//}
//$date_ym = ($date_y*100)+$date_m;
//$day_max = date("d", strtotime($date_y."-".($date_m<12?$date_m+1:1)."-1 -1 day"));
?>

<div style="padding:4px;" id="div_search_box">
</div>

<?
//==========================================================================
// 一覧
//==========================================================================
//$mst = select_from_table($con,"select * from optype where optype_id between 200 and 299","",$fname);
//while($mstrow = pg_fetch_array($mst)) {
//    $opnames[$mstrow["optype_id"]] = $mstrow["optype_proc"];
//}
//$sel = select_from_table($con,"select * from oplog_daily where op_ymd like '".$date_ym."__' and optype_id between 200 and 299 order by optype_id","",$fname);
//$data = array();
//while($row = pg_fetch_array($sel)) {
//    $data[$row["optype_id"]][(int)substr($row["op_ymd"],6)] = $row["op_count"];
//}
?>

<div id="div_data_field">
<form name="aclgform" action="aclg_config.php" method="post">

<!--
<table border="0" cellspacing="0" cellpadding="2" class="list">
	<tr><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログの保存期間</font></td>
		<td><select name="date_y">
			<option value="0" <?php if($date_y =="0") {echo("selected");}?>>保存しない</option>
			<option value="1" <?php if($date_y =="1") {echo("selected");}?>>1日</option>
			<option value="2" <?php if($date_y =="2") {echo("selected");}?>>3日</option>
			<option value="3" <?php if($date_y =="3") {echo("selected");}?>>1週間</option>
			<option value="4" <?php if($date_y =="4") {echo("selected");}?>>1ヶ月</option>
			<option value="5" <?php if($date_y =="5") {echo("selected");}?>>3ヶ月</option>
			<option value="6" <?php if($date_y =="6") {echo("selected");}?>>無制限</option>
			</select><input type="button" value="保存" onclick="regist_setting('2');">
		</td>
	</tr>
</table>
-->


<img src="img/spacer.gif" alt="" width="1" height="10"><br>
	<table border="0" cellspacing="0" cellpadding="0">
		<tr height="22">
			<td><input type="button" value="登録" onclick="regist_setting('1');"><br>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本機能</b></font></td>
		</tr>
	</table>

	<? $space="&nbsp;"; ?>


	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr><td rowspan="1" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
		<td>
		<? //cbInfo("ウェブメール.メール送信（添付ファイルサイズ付き）"); ?>
		<? cbInfo("0101","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>メール送信（宛先一覧・添付ファイルサイズ付き）</font>
		</td></tr>

		<tr><td rowspan="13" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
		<td>
		<? //cbInfo("スケジュール.共通グループの作成"); ?>
		<? cbInfo("0103","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>共通グループの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.共通グループの編集"); ?>
		<? cbInfo("0103","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>共通グループの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.共通グループの削除"); ?>
		<? cbInfo("0103","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>共通グループの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.種別の更新"); ?>
		<? cbInfo("0103","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>種別の更新</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.行き先の作成"); ?>
		<? cbInfo("0103","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>行き先の作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.行き先の更新"); ?>
		<? cbInfo("0103","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>行き先の更新</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.行き先の削除"); ?>
		<? cbInfo("0103","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>行き先の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.マイグループの作成"); ?>
		<? cbInfo("0103","10"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>マイグループの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.マイグループの編集"); ?>
		<? cbInfo("0103","11"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>マイグループの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.マイグループの削除"); ?>
		<? cbInfo("0103","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>マイグループの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.スケジュールの作成"); ?>
		<? cbInfo("0103","12"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>スケジュールの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.スケジュールの編集"); ?>
		<? cbInfo("0103","13"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>スケジュールの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("スケジュール.スケジュールの削除"); ?>
		<? cbInfo("0103","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>スケジュールの削除</font>
		</td></tr>

		<tr><td rowspan="13" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></td>
		<td>
		<? //cbInfo("委員会・WG.委員会の作成"); ?>
		<? cbInfo("0104","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会の作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会の編集"); ?>
		<? cbInfo("0104","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.WGの作成"); ?>
		<? cbInfo("0104","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>WGの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.WGの編集"); ?>
		<? cbInfo("0104","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>WGの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会・WGの削除"); ?>
		<? cbInfo("0104","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会・WGの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会・WGのスケジュール作成"); ?>
		<? cbInfo("0104","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会・WGスケジュール作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会・WGのスケジュール編集"); ?>
		<? cbInfo("0104","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会・WGスケジュール編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会・WGスケジュールの削除"); ?>
		<? cbInfo("0104","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>委員会・WGスケジュールの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.委員会・種別の更新"); ?>
		<? cbInfo("0104","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>種別の更新</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.議事録作成"); ?>
		<? cbInfo("0104","10"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>議事録作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.議事録編集"); ?>
		<? cbInfo("0104","11"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>議事録編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.議事録承認"); ?>
		<? cbInfo("0104","12"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>議事録承認</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("委員会・WG.議事録差戻し"); ?>
		<? cbInfo("0104","13"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>議事録差戻し</font>
		</td></tr>

		<tr><td rowspan="16" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ・回覧板</font></td>
		<td>
		<? //cbInfo("お知らせ・回覧板.通達区分の登録"); ?>
		<? cbInfo("0105","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>通達区分の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.通達区分の編集"); ?>
		<? cbInfo("0105","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>通達区分の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.通達区分の削除"); ?>
		<? cbInfo("0105","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>通達区分の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.カテゴリの登録"); ?>
		<? cbInfo("0105","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.カテゴリの編集"); ?>
		<? cbInfo("0105","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.カテゴリの削除"); ?>
		<? cbInfo("0105","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.ゴミ箱から元に戻す・削除"); ?>
		<? cbInfo("0105","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ゴミ箱から元に戻す</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.ゴミ箱から元に戻す・削除"); ?>
		<? cbInfo("0105","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ゴミ箱から削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.お知らせの登録"); ?>
		<? cbInfo("0105","10"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.お知らせの編集"); ?>
		<? cbInfo("0105","11"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.お知らせの削除"); ?>
		<? cbInfo("0105","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.CSVダウンロード（集計）"); ?>
		<? cbInfo("0105","12"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>CSVダウンロード（集計）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.CSVダウンロード（明細）"); ?>
		<? cbInfo("0105","13"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>CSVダウンロード（明細）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.お知らせ一覧CSVダウンロード""); ?>
		<? cbInfo("0105","14"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせ一覧CSVダウンロード</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.お知らせ督促メール送信"); ?>
		<? cbInfo("0105","15"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせ督促メール送信</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("お知らせ・回覧板.オプション設定の編集（管理画面）"); ?>
		<? cbInfo("0105","16"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>オプション設定の編集（管理画面）</font>
		</td></tr>

		<tr><td rowspan="3" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タスク</font></td>
		<td>
		<? //cbInfo("タスク.タスクの登録"); ?>
		<? cbInfo("0106","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>タスクの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("タスク.タスクの編集"); ?>
		<? cbInfo("0106","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>タスクの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("タスク.タスクの削除"); ?>
		<? cbInfo("0106","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>タスクの削除</font>
		</td></tr>

		<tr><td rowspan="4" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ</font></td>
		<td>
		<? //cbInfo("伝言メモ.伝言メモの作成"); ?>
		<? cbInfo("0107","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>伝言メモの送信</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("伝言メモ.伝言メモの再送"); ?>
		<? cbInfo("0107","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>伝言メモの再送</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("伝言メモ.伝言メモの削除（受信履歴）"); ?>
		<? cbInfo("0107","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>伝言メモの削除（受信履歴）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("伝言メモ.伝言メモの削除（送信履歴）"); ?>
		<? cbInfo("0107","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>伝言メモの削除（送信履歴）</font>
		</td></tr>

		<tr><td rowspan="15" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
		<td>
		<? //cbInfo("設備予約.カテゴリの登録"); ?>
		<? cbInfo("0108","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.カテゴリの編集"); ?>
		<? cbInfo("0108","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.施設・設備の登録"); ?>
		<? cbInfo("0108","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設・設備の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.施設・設備の編集"); ?>
		<? cbInfo("0108","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設・設備の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.カテゴリ・施設の削除"); ?>
		<? cbInfo("0108","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリ・施設の削除</font>
		</td></tr>
		<!--
		<tr><td>
		<? //cbInfo("設備予約.施設・設備の削除"); ?>
		<? cbInfo("0108","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設・設備の削除</font>
		</td></tr>
		-->
		<tr><td>
		<? //cbInfo("設備予約.テンプレートの登録"); ?>
		<? cbInfo("0108","11"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>テンプレートの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.テンプレートの編集"); ?>
		<? cbInfo("0108","12"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>テンプレートの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.テンプレートの削除"); ?>
		<? cbInfo("0108","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>テンプレートの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.一括登録"); ?>
		<? cbInfo("0108","10"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>一括登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.設備予約の登録"); ?>
		<? cbInfo("0108","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>設備予約の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.設備予約の編集"); ?>
		<? cbInfo("0108","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>設備予約の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.設備予約の削除"); ?>
		<? cbInfo("0108","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>設備予約の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.権限設定の編集"); ?>
		<? cbInfo("0108","13"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>権限設定の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.管理者設定の編集"); ?>
		<? cbInfo("0108","14"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>管理者設定の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("設備予約.種別の編集"); ?>
		<? cbInfo("0108","15"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>種別の編集</font>
		</td></tr>


		<tr><td rowspan="17" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁・申請</font></td>
		<td>
		<? //cbInfo("決裁・申請.フォルダの作成"); ?>
		<? cbInfo("0109","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.フォルダの編集"); ?>
		<? cbInfo("0109","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.フォルダの削除"); ?>
		<? cbInfo("0109","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.ワークフローの作成（フォーマットファイル名）"); ?>
		<? cbInfo("0109","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ワークフローの作成（フォーマットファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.ワークフローの編集（フォーマットファイル名）"); ?>
		<? cbInfo("0109","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ワークフローの編集（フォーマットファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.ワークフローの削除"); ?>
		<? cbInfo("0109","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ワークフローの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請一覧からの申請書削除"); ?>
		<? cbInfo("0109","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請一覧からの申請書削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.印影の登録"); ?>
		<? cbInfo("0109","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>印影の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.承認依頼通知文書の編集"); ?>
		<? cbInfo("0109","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>承認依頼通知文書の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の編集"); ?>
		<? cbInfo("0109","10"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請内容更新"); ?>
		<? cbInfo("0109","11"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請内容更新</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請一覧からのCSV書き出し"); ?>
		<? cbInfo("0109","12"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請一覧からのCSV書き出し</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の申請（添付ファイル名）"); ?>
		<? cbInfo("0109","13"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の申請（添付ファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の再申請（添付ファイル名）"); ?>
		<? cbInfo("0109","14"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の再申請（添付ファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の承認"); ?>
		<? cbInfo("0109","15"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の承認</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の否認"); ?>
		<? cbInfo("0109","16"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の否認</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("決裁・申請.申請書の差戻し"); ?>
		<? cbInfo("0109","17"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>申請書の差戻し</font>
		</td></tr>


		<tr><td rowspan="9" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲示板・電子会議室</font></td>
		<td>
		<? //cbInfo("掲示板・電子会議室.カテゴリの作成"); ?>
		<? cbInfo("0110","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.カテゴリの編集"); ?>
		<? cbInfo("0110","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.テーマの作成"); ?>
		<? cbInfo("0110","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>テーマの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.テーマの編集"); ?>
		<? cbInfo("0110","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>テーマの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.カテゴリ・テーマの削除"); ?>
		<? cbInfo("0110","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリ・テーマの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.投稿登録（添付ファイル名）"); ?>
		<? cbInfo("0110","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>投稿登録（添付ファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.投稿編集（添付ファイル名）"); ?>
		<? cbInfo("0110","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>投稿編集（添付ファイル名）</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.投稿削除"); ?>
		<? cbInfo("0110","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>投稿削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("掲示板・電子会議室.オプション設定の更新（管理画面）"); ?>
		<? cbInfo("0110","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>オプション設定の編集（管理画面）</font>
		</td></tr>


		<tr><td rowspan="9" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A</font></td>
		<td>
		<? //cbInfo("Q&A.カテゴリの作成"); ?>
		<? cbInfo("0111","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.カテゴリの編集"); ?>
		<? cbInfo("0111","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.カテゴリの削除"); ?>
		<? cbInfo("0111","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.質問の登録"); ?>
		<? cbInfo("0111","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>質問の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.質問の編集"); ?>
		<? cbInfo("0111","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>質問の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.質問の削除"); ?>
		<? cbInfo("0111","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>質問の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.回答の登録"); ?>
		<? cbInfo("0111","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>回答の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.回答の編集"); ?>
		<? cbInfo("0111","9"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>回答の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("Q&A.回答の削除"); ?>
		<? cbInfo("0111","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>回答の削除</font>
		</td></tr>


		<tr><td rowspan="7" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
		<td>
		<? //cbInfo("文書管理.フォルダの作成"); ?>
		<? cbInfo("0112","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの作成</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.フォルダの編集"); ?>
		<? cbInfo("0112","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.フォルダの削除"); ?>
		<? cbInfo("0112","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>フォルダの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.文書の登録"); ?>
		<? cbInfo("0112","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>文書の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.文書の編集"); ?>
		<? cbInfo("0112","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>文書の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.文書の削除"); ?>
		<? cbInfo("0112","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>文書の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("文書管理.オプション設定の編集（管理画面）"); ?>
		<? cbInfo("0112","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>オプション設定の編集（管理画面）</font>
		</td></tr>

		<tr><td rowspan="6" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス帳</font></td>
		<td>
		<? //cbInfo("アドレス帳.アドレス帳の登録"); ?>
		<? cbInfo("0113","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>アドレス帳の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("アドレス帳.アドレス帳の編集"); ?>
		<? cbInfo("0113","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>アドレス帳の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("アドレス帳.アドレス帳の削除"); ?>
		<? cbInfo("0113","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>アドレス帳の削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("アドレス帳.アドレス帳の一括登録の実行"); ?>
		<? cbInfo("0113","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>アドレス帳の一括登録の実行</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("アドレス帳.アドレス帳を共有へコピー"); ?>
		<? cbInfo("0113","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>アドレス帳を共有へコピー</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("アドレス帳.本人連絡先更新"); ?>
		<? cbInfo("0113","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>本人連絡先更新</font>
		</td></tr>


		<tr><td rowspan="2" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
		<td>
		<? //cbInfo("リンクライブラリ.カテゴリの更新"); ?>
		<? cbInfo("0114","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>カテゴリの更新</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("リンクライブラリ.リンクの更新"); ?>
		<? cbInfo("0114","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>リンクの更新</font>
		</td></tr>

		<tr><td rowspan="8" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳</font></td>
		<td>
		<? //cbInfo("内線電話帳.施設連絡先の登録（添付ファイル名）"); ?>
		<? cbInfo("0115","3"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設連絡先の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.施設連絡先の編集（添付ファイル名）"); ?>
		<? cbInfo("0115","4"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設連絡先の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.施設連絡先の削除"); ?>
		<? cbInfo("0115","1"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>施設連絡先の削除</font>
		</td></tr>
		<td>
		<? //cbInfo("内線電話帳.部門連絡先の登録"); ?>
		<? cbInfo("0115","5"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>部門連絡先の登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.番号リストの登録（添付ファイル名）"); ?>
		<? cbInfo("0115","6"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>番号リストの登録</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.番号リストの削除"); ?>
		<? cbInfo("0115","2"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>番号リストの削除</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.連絡先番号の編集"); ?>
		<? cbInfo("0115","7"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>連絡先番号の編集</font>
		</td></tr>
		<tr><td>
		<? //cbInfo("内線電話帳.オプション設定の編集（管理画面）"); ?>
		<? cbInfo("0115","8"); ?>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>オプション設定の編集（管理画面）</font>
		</td></tr>
<tr><td style="border-style:none;"><img src="img/spacer.gif" alt="" width="1" height="10"></td></tr>
<tr height="22">
<td style="border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務機能</b></font></td>
</tr>















<tr><td rowspan="57" valign="top" class="tool"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん</font></td>
<!--
<tr><td>
<? cbInfo("0201","2"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>管理項目変更の登録・編集・削除</font>
</td></tr>
<tr><td>
<? cbInfo("0201","3"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニング感想の削除</font>
</td></tr>
-->
<td>
<? //cbInfo("ファントルくん.お知らせの登録"); ?>
<? cbInfo("0201","20"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの登録</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.お知らせの編集"); ?>
<? cbInfo("0201","21"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.お知らせの削除"); ?>
<? cbInfo("0201","4"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>お知らせの削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書メールの送信"); ?>
<? cbInfo("0201","24"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書メールの送信</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書メールの返信"); ?>
<? cbInfo("0201","25"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書メールの返信</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書メールの転送"); ?>
<? cbInfo("0201","26"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書メールの転送</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書の更新"); ?>
<? cbInfo("0201","17"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書の更新</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書メールの削除（受信フォルダ）"); ?>
<? cbInfo("0201","5"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書メールの削除（受信フォルダ）</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書メールの削除（送信フォルダ）"); ?>
<? cbInfo("0201","6"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書メールの削除（送信フォルダ）</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書をゴミ箱へ移動"); ?>
<? cbInfo("0201","7"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書をゴミ箱へ移動</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書をゴミ箱から戻す"); ?>
<? cbInfo("0201","8"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書をゴミ箱から戻す</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書をゴミ箱から削除"); ?>
<? cbInfo("0201","9"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書をゴミ箱から削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.分類フォルダの作成"); ?>
<? cbInfo("0201","22"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>分類フォルダの作成</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.分類フォルダの編集"); ?>
<? cbInfo("0201","23"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>分類フォルダの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.分類フォルダの削除"); ?>
<? cbInfo("0201","10"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>分類フォルダの削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニングフォルダの作成"); ?>
<? cbInfo("0201","27"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニングフォルダの作成</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニングフォルダの編集"); ?>
<? cbInfo("0201","28"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニングフォルダの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニングコンテンツの登録"); ?>
<? cbInfo("0201","29"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニングコンテンツの登録</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニングコンテンツの編集"); ?>
<? cbInfo("0201","30"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニングコンテンツの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（CSV）のCSV書き出し"); ?>
<? cbInfo("0201","31"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（CSV）のCSV書き出し</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（CSV）のフォルダ作成"); ?>
<? cbInfo("0201","32"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（CSV）のフォルダ作成</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（CSV）のフォルダ編集"); ?>
<? cbInfo("0201","33"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（CSV）のフォルダ編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（CSV）のフォルダ削除"); ?>
<? cbInfo("0201","11"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（CSV）のフォルダ削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（XML）のXML書き出し"); ?>
<? cbInfo("0201","34"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（XML）のXML書き出し</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（XML）のフォルダ作成"); ?>
<? cbInfo("0201","35"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（XML）のフォルダ作成</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（XML）のフォルダ編集"); ?>
<? cbInfo("0201","36"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（XML）のフォルダ編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.データ提出（XML）のフォルダ削除"); ?>
<? cbInfo("0201","12"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ提出（XML）のフォルダ削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書の印刷"); ?>
<? cbInfo("0201","53"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書の印刷</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.メールの印刷"); ?>
<? cbInfo("0201","54"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>メールの印刷</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書のCSV書き出し"); ?>
<? cbInfo("0201","56"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書のCSV書き出し</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書のExcel書き出し"); ?>
<? cbInfo("0201","57"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書のExcel書き出し</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書の進捗管理の編集（GRM以外編集ロック含む）"); ?>
<? cbInfo("0201","13"); ?>
<?
// GRMの略称を取得
$sql = "select auth_short_name from inci_auth_mst";
$cond = "where auth='SM'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$auth_short_name = pg_fetch_result($sel, 0, "auth_short_name");
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書の進捗管理の編集（<? echo $auth_short_name; ?>以外編集ロック含む）</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書単位での未読・既読の変更"); ?>
<? cbInfo("0201","55"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書単位での未読・既読の変更</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.統計分析画面でのExcel書き出し（クロス集計）"); ?>
<? cbInfo("0201","59"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>統計分析画面でのExcel書き出し（クロス集計）</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.統計分析画面でのExcel書き出し（定型集計）"); ?>
<? cbInfo("0201","62"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>統計分析画面でのExcel書き出し（定型集計）</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.統計分析画面での印刷"); ?>
<? cbInfo("0201","60"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>統計分析画面での印刷</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.代行者設定の編集"); ?>
<? cbInfo("0201","61"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>代行者設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.出来事分析の削除"); ?>
<? cbInfo("0201","14"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>出来事分析の削除</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.担当者名称の更新"); ?>
<? cbInfo("0201","37"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>担当者名称の更新</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.担当者設定の更新"); ?>
<? cbInfo("0201","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>担当者設定の更新</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.担当者の登録"); ?>
<? cbInfo("0201","38"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>担当者の登録</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.ボタン設定の編集"); ?>
<? cbInfo("0201","39"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>ボタン設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.運用フローの編集"); ?>
<? cbInfo("0201","40"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>運用フローの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.匿名設定の編集"); ?>
<? cbInfo("0201","41"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>匿名設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.送信先設定の編集"); ?>
<? cbInfo("0201","42"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>送信先設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.進捗設定初期登録の編集"); ?>
<? cbInfo("0201","43"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>進捗設定初期登録の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書様式の登録"); ?>
<? cbInfo("0201","18"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書様式の登録</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.報告書様式の編集"); ?>
<? cbInfo("0201","19"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>報告書様式の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.様式管理の編集"); ?>
<? cbInfo("0201","44"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>様式管理の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.様式割当の編集"); ?>
<? cbInfo("0201","45"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>様式割当の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.分析画面設定の編集"); ?>
<? cbInfo("0201","46"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>分析画面設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.プロフィール設定の編集"); ?>
<? cbInfo("0201","47"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>プロフィール設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.プロフィール個人設定の編集"); ?>
<? cbInfo("0201","48"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>プロフィール個人設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.プロフィールボタン表示設定の編集"); ?>
<? cbInfo("0201","49"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>プロフィールボタン表示設定の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.部署表示の編集"); ?>
<? cbInfo("0201","50"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>部署表示の編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニングオプションの編集"); ?>
<? cbInfo("0201","51"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニングオプションの編集</font>
</td></tr>
<tr><td>
<? //cbInfo("ファントルくん.e-ラーニング感想の編集"); ?>
<? cbInfo("0201","52"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>e-ラーニング感想の編集</font>
</td></tr>




<? if ($lcs_func[31]=="t") { ?>
<? // スタッフ・ポートフォリオの詳細ログは一旦保留 ?>
<!--
<tr><td rowspan="2" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタッフ・ポートフォリオ</font></td>
<td>
<? cbInfo("0202","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ更新</font>
</td></tr>
<tr><td>
<? cbInfo("0202","2"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$space; ?>データ削除</font>
</td></tr>
-->
<? } ?>





<tr><td style="border-style:none;"><img src="img/spacer.gif" alt="" width="1" height="10"></td></tr>
<tr height="22">
<td style="border-style:none;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理機能</b></font></td>
</tr>
<tr><td rowspan="1" valign="top" class="tool"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン</font></td>
<td class="ope">
<? //cbInfo("ログイン.ログイン"); ?>
<? cbInfo("0001","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン</font>
</td></tr>

<tr><td rowspan="1" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録</font></td>

<td>
<? cbInfo("0002","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザの利用停止</font>
</td>

</tr>

<!--
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザの更新</font>
</td></tr>

<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザの登録</font>
</td></tr>

<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ情報のCSVファイル書き出し</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ一括更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">トップページ一括更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー表示一括更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件一括更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理CSVファイル書き出し</font>
</td></tr>
-->


<tr><td rowspan="3" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスターメンテナンス</font></td>
<td>
<? //cbInfo("マスターメンテナンス.組織の削除"); ?>
<? cbInfo("0003","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織の削除</font>
</td></tr>

<!--
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織の更新</font>
</td></tr>

<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織の登録</font>
</td></tr>

<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種の登録</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種の更新</font>
</td></tr>
-->

<tr><td>
<? //cbInfo("マスターメンテナンス.職種の削除"); ?>
<? cbInfo("0003","2"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種の削除</font>
</td></tr>


<!--
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職の登録</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職の更新</font>
</td></tr>
-->


<tr><td>
<? //cbInfo("マスターメンテナンス.役職の削除"); ?>
<? cbInfo("0003","3"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職の削除</font>
</td></tr>

<!--
<tr><td rowspan="3" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">環境設定</font></td>
<td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面の更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイドメニューの更新</font>
</td></tr>
<tr><td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトIDの更新</font>
</td></tr>

<tr><td rowspan="1" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス管理</font></td>
<td>
<? cbInfo("99","99"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンスの更新</font>
</td></tr>
-->
<tr><td rowspan="2" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧</font></td>
<td>
<? cbInfo("0006","1"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧(ユーザ)</font>
</td></tr>

<tr><td>
<? cbInfo("0006","2"); ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧(管理)</font>
</td></tr>



</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="regstat" value="">

</form>

</div>
<input type="button" value="登録" onclick="regist_setting('1');">
<script type="text/javascript">
function adjustListWidth(){
	if (!document.getElementById("div_data_field")) return;
	document.getElementById("div_data_field").style.width =
		document.getElementById("header_menu_table").clientWidth - 20;
}

function regist_setting(status){
//	confirm("data"+status);
	document.aclgform.regstat.value=status;

	document.aclgform.submit();

}

</script>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
