<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟情報権限チェック
$ward = check_authority($session,21,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//----------Nullチェック----------
	if($bldg_name == ""){
		echo("<script language=\"javascript\">alert(\"棟名が入力されていません。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);

//----------Transaction begin----------
pg_exec($con,"begin transaction");

//----------bldgmstからbldg_cdを取得----------
$sel = select_from_table($con,$SQL62,$cond,$fname);			//max値を取得
	if($sel==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$max = pg_result($sel,0,"max");
	
	if($max == ""){
		$bldg_cd = "1";
	}else{
		$bldg_cd = $max+1;
	}

	$sql = "insert into bldgmst (bldg_cd, bldg_type, bldg_name, bldg_floor, bldg_del_flg, enti_id) values(";
	$content = array($bldg_cd,1,$bldg_name,$floor,f,1);

	$in_bldg = insert_into_table($con,$sql,$content,$fname);
	if($in_bldg==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	pg_exec($con, "commit");
	pg_close($con);
	echo("<script language=\"javascript\">location.href=\"building_list.php?session=$session\"</script>");
?>