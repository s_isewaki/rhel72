<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 管理 | 同期設定</title>

<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
$obj = new duty_shift_common_class($con, $fname);

// 権限のチェック
if ($obj->check_authority_Management($session, $fname) == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$section_admin_auth = $obj->check_authority_user($session, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function sync(mode) {
	var link = (mode == '1') ? '勤務シフト　→　職員所属' : '職員所属　→　勤務シフト';
	if (!confirm(link + '\n\n実行すると元に戻せません。実行しますか？')) {
		return;
	}
	document.mainform.mode.value = mode;
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
img {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_manage_title($session, $admin_auth); ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_manage_tab_menuitem($session, $fname, null); ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width"1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td>
</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="duty_shift_sync_setting.php?session=<? echo($session); ?>">関連付け設定</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>同期処理</b></font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="30" alt=""><br>

<form name="mainform" action="duty_shift_sync_exe.php" method="post">

<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:15px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフト</font></td>
<td style="padding-right:15px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">→</font></td>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員所属</font></td>
<td style="padding-right:20px;"><input type="button" value="実行" onclick="sync('1');"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? if ($result == "success" && $mode == 1) {echo("完了しました。");} ?></font><td>
</tr>
<tr height="30">
<td colspan="5"></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員所属</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">→</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフト</font></td>
<td><input type="button" value="実行" onclick="sync('2');"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? if ($result == "success" && $mode == 2) {echo("完了しました。");} ?></font><td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="">
</form>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
