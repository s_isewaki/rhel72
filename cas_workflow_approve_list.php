<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("mygroup_common.php");
require_once("label_by_profile_type.ini");
require_once("cas_application_workflow_select_box.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$wkfw=check_authority($session, $CAS_MANAGE_AUTH, $fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//======================================
// 組織階層情報取得
//======================================
$arr_org_level = array();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel,0,"class_nm");
$arr_org_level[1] = pg_result($sel,0,"atrb_nm");
$arr_org_level[2] = pg_result($sel,0,"dept_nm");
$arr_org_level[3] = pg_result($sel,0,"room_nm");
$level_cnt = pg_result($sel,0,"class_cnt");

//======================================
// 役職マスタ情報取得(リスト項目の値取得)
//======================================
$arr_manager_post = array();
$sql = "select st_id,st_nm from stmst ";
$cond = "where st_del_flg = 'f' order by order_no";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {

	$tmp_st_id = $row["st_id"];
	$tmp_st_nm = $row["st_nm"];
	array_push($arr_manager_post, array("id" => $tmp_st_id, "name" => $tmp_st_nm));
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

$cas_title = get_cas_title_name();

?>

<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?>｜承認者選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">


var childwin = null;
function openEmployeeList(input_div) {

	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'cas_emp_list.php';
	url += '?session=<?=$session?>&input_div=' + input_div;
	childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function openProjectList()
{
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './cas_workflow_project_list.php';
	url += '?session=<?=$session?>';
	childwin = window.open(url, 'popup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(",");
	var emp_names = emp_name.split(",");
	var target_length = document.wkfw.target.options.length;

	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.wkfw.target.options.length; k < l; k++) {
			if (document.wkfw.target[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.wkfw.target, emp_id, emp_name);
		}
	}
}

function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

// 追加ボタン
function addEmp() {

	for (var i = 0, j = document.wkfw.emplist.options.length; i < j; i++)
	{
		if (document.wkfw.emplist.options[i].selected)
		{
			var emp_id = document.wkfw.emplist.options[i].value;
			var emp_name = document.wkfw.emplist.options[i].text;
	
			var in_group = false;
			for (var k = 0, l = document.wkfw.target.options.length; k < l; k++)
			{
				if (document.wkfw.target.options[k].value == emp_id)
				{
					in_group = true;
					break;
				}
			}
	
			if (!in_group)
			{
				set_wm_counter(emp_id);
				addOption(document.wkfw.target, emp_id, emp_name);
			}
		}
	}
}

// 削除ボタン
function deleteEmp() {

	for (var i = document.wkfw.target.options.length - 1; i >= 0; i--) {
		if (document.wkfw.target.options[i].selected) {
			document.wkfw.target.options[i] = null;
		}
	}
}


function click_use_check(div)
{
	id = 'apv_div' + div + '_flg';
	var flg = 't';
	if(!document.getElementById(id).checked)
	{
		flg = 'f';
	}

	if(div == '0')
	{
		set_st_div0(flg);
	}
	else if(div == '1')
	{
		set_st_div1(flg);
	}
	else if(div == '2')
	{
		set_st_div2(flg);
	}
	else if(div == '3')
	{
		set_st_div3(flg);
	}
	else if(div == '4')
	{
		set_st_div4(flg);
	}
}

// 部署役職(申請者所属)指定制御
function set_st_div0(st_div_flg)
{
	st_post_flg = true;
	if(st_div_flg == 't')
	{
		st_post_flg = false;
	}

	// 部署役職(申請者所属)
	for (var i = 0; i < document.wkfw.target_class_div.length; i++) {
		document.wkfw.target_class_div[i].disabled = st_post_flg;
	}

	for (var i = 0; i < document.wkfw.mng_post.length; i++) {
		document.wkfw.mng_post[i].disabled = st_post_flg;
	}
}

// 職員指定制御
function set_st_div1(st_div_flg)
{
	st_staff_flg = true;
	if(st_div_flg == 't')
	{
		st_staff_flg = false;
	}

	// 職員
    document.wkfw.target.disabled = st_staff_flg;
    document.wkfw.emplist.disabled = st_staff_flg;
    document.wkfw.add_emp.disabled = st_staff_flg;
    document.wkfw.delete_emp.disabled = st_staff_flg;
    document.wkfw.mygroup.disabled = st_staff_flg;
    document.wkfw.emplist_btn.disabled = st_staff_flg;

}

// その他指定制御
function set_st_div2(st_div_flg)
{
	st_apv_num_flg = true;
	if(st_div_flg == 't')
	{
		st_apv_num_flg = false;
	}
	document.wkfw.apv_num.disabled = st_apv_num_flg;
}

function check_st_div2(flg)
{
	if(flg == 't')
	{
	    if(document.getElementById('apv_div0_flg').checked
	    	|| document.getElementById('apv_div1_flg').checked
	    	|| document.getElementById('apv_div3_flg').checked
	    	|| document.getElementById('apv_div4_flg').checked)
		{
			alert('');
		}
	}
}

// 委員会・ＷＧ指定制御
function set_st_div3(st_div_flg)
{
	st_pjt_flg = true;
	if(st_div_flg == 't')
	{
		st_pjt_flg = false;
	}

	// 委員会・ＷＧ
    document.wkfw.pjtlist_btn.disabled = st_pjt_flg;
}

// 部署役職(部署指定)制御
function set_st_div4(st_div_flg)
{
	st_post_sect_flg = true;
	if(st_div_flg == 't')
	{
		st_post_sect_flg = false;
	}

	// 部署役職(部署指定)
	document.getElementById('class_sect').disabled = st_post_sect_flg;
	document.getElementById('atrb_sect').disabled = st_post_sect_flg;
	document.getElementById('dept_sect').disabled = st_post_sect_flg;

	if(document.getElementById('room_sect') != null)
	{
		document.getElementById('room_sect').disabled = st_post_sect_flg;
	}
	for (var i = 0; i < document.wkfw.mng_post.length; i++) {
		document.wkfw.mng_post_sect[i].disabled = st_post_sect_flg;
	}
}

function initPage() {

	groupOnChange();

	// 複数承認者
	var multi_apv_flg = window.opener.document.wkfw.multi_apv_flg<? echo($approve); ?>.value;
    for (i = 0; i < document.wkfw.multi_apv_flg.length; i++)
    {
		if(multi_apv_flg == document.wkfw.multi_apv_flg[i].value)
		{
			document.wkfw.multi_apv_flg[i].checked = true;
		}
    }

	// 次の承認者への通知タイミング
	var next_notice_div = window.opener.document.wkfw.next_notice_div<? echo($approve); ?>.value;

	if(next_notice_div != '')
	{
	    for (i = 0; i < document.wkfw.next_notice_div.length; i++)
	    {
			if(next_notice_div == document.wkfw.next_notice_div[i].value)
			{
				document.wkfw.next_notice_div[i].checked = true;
			}
	    }
	}
	else
	{
		document.wkfw.next_notice_div[1].checked = true;	
	}
	changeNextNotice(multi_apv_flg);

	var apv_div0_flg = window.opener.document.wkfw.apv_div0_flg<? echo($approve); ?>.value;
	if(apv_div0_flg == 't')
	{
		document.getElementById('apv_div0_flg').checked = true;
	}
	var apv_div1_flg = window.opener.document.wkfw.apv_div1_flg<? echo($approve); ?>.value;
	if(apv_div1_flg == 't')
	{
		document.getElementById('apv_div1_flg').checked = true;
	}

	var apv_div2_flg = window.opener.document.wkfw.apv_div2_flg<? echo($approve); ?>.value;
	if(apv_div2_flg == 't')
	{
		document.getElementById('apv_div2_flg').checked = true;
	}

	var apv_div3_flg = window.opener.document.wkfw.apv_div3_flg<? echo($approve); ?>.value;
	if(apv_div3_flg == 't')
	{
		document.getElementById('apv_div3_flg').checked = true;
	}

	var apv_div4_flg = window.opener.document.wkfw.apv_div4_flg<? echo($approve); ?>.value;
	if(apv_div4_flg == 't')
	{
		document.getElementById('apv_div4_flg').checked = true;
	}


	var target_class_div = window.opener.document.wkfw.target_class_div<? echo($approve); ?>.value;
   	for (i = 0; i < document.wkfw.target_class_div.length; i++) {
		if(document.wkfw.target_class_div[i].value == target_class_div) {
			target_class_div = document.wkfw.target_class_div[i].checked = true;
		}
   	}

	// 部署役職、職員、委員会・ＷＧの活性・非活性制御
	set_st_div0(apv_div0_flg);
	set_st_div1(apv_div1_flg);
	set_st_div2(apv_div2_flg);
	set_st_div3(apv_div3_flg);
	set_st_div4(apv_div4_flg);


	if(apv_div0_flg == 't')
	{

		var st_id = window.opener.document.wkfw.st_id<? echo($approve); ?>.value;
		arr_st_id = st_id.split(',');
		for(i=0; i<arr_st_id.length; i++) {

			cnt = 0;
			<?foreach($arr_manager_post as $row) {?>
				if(<?echo($row["id"]);?> == arr_st_id[i]) {
					document.wkfw.mng_post[cnt].checked = true;
				}
				cnt++;
			<?}?>
		}
	}

	if(apv_div1_flg == 't')
	{
		var emp_id = window.opener.document.wkfw.emp_id<? echo($approve); ?>.value;
		var emp_nm = window.opener.document.wkfw.emp_nm<? echo($approve); ?>.value;
		if(emp_id != "") {
			var obj_target = document.wkfw.target;
			deleteAllOptions(obj_target);

			arr_emp_id = emp_id.split(",");
			arr_emp_nm = emp_nm.split(", ");

			for(i=0; i<arr_emp_id.length; i++)
			{
				addOption(obj_target, arr_emp_id[i], arr_emp_nm[i], '');
			}
		}
	}

    if(apv_div2_flg == 't')
    {
		apv_num = window.opener.document.wkfw.apv_num<? echo($approve); ?>.value;
		document.getElementById('apv_num').value = apv_num;
    }

    if(apv_div3_flg == 't')
	{

		pjt_parent_id = window.opener.document.wkfw.pjt_parent_id<? echo($approve); ?>.value;
		pjt_child_id = window.opener.document.wkfw.pjt_child_id<? echo($approve); ?>.value;
		pjt_parent_nm = window.opener.document.wkfw.pjt_parent_nm<? echo($approve); ?>.value;
		pjt_child_nm = window.opener.document.wkfw.pjt_child_nm<? echo($approve); ?>.value;

		document.getElementById('pjt_parent_id').value = pjt_parent_id;
		document.getElementById('pjt_child_id').value = pjt_child_id;
		document.getElementById('pjt_parent_nm').value = pjt_parent_nm;
		document.getElementById('pjt_child_nm').value = pjt_child_nm;

		if(pjt_child_id == "")
		{
			document.getElementById('project').innerHTML = pjt_parent_nm;
		}
		else
		{
			document.getElementById('project').innerHTML = pjt_parent_nm + " > " + pjt_child_nm;
		}
	}

    if(apv_div4_flg == 't')
	{
		var st_sect_id = window.opener.document.wkfw.st_sect_id<? echo($approve); ?>.value;
		arr_st_sect_id = st_sect_id.split(',');
		for(i=0; i<arr_st_sect_id.length; i++) {

			cnt = 0;
			<?foreach($arr_manager_post as $row) {?>
				if(<?echo($row["id"]);?> == arr_st_sect_id[i]) {
					document.wkfw.mng_post_sect[cnt].checked = true;
				}
				cnt++;
			<?}?>
		}
	}
}


function setApprove()
{

	var approve_content = "";
	var target_class_div = "0";
	var target_class_nm = "";
	var st_id = "";
	var emp_id = "";
	var emp_nm = "";

	var pjt_parent_id = "";
	var pjt_child_id = "";
	var pjt_parent_nm = "";
	var pjt_child_nm = "";

	var class_sect_id = "";
	var atrb_sect_id = "";
	var dept_sect_id = "";
	var room_sect_id = "";

	var st_sect_id = "";

	var apv_num = document.getElementById('apv_num').value;

	// 部署役職(申請者所属)、職員、委員会・ＷＧ、その他、部署役職(部署指定)チェック
	var apv_div0_flg = 'f';
	if(document.getElementById('apv_div0_flg').checked)
	{
		apv_div0_flg = 't';
	}
	var apv_div1_flg = 'f';
	if(document.getElementById('apv_div1_flg').checked)
	{
		apv_div1_flg = 't';
	}
	var apv_div2_flg = 'f';
	if(document.getElementById('apv_div2_flg').checked)
	{
		apv_div2_flg = 't';
	}

	var apv_div3_flg = 'f';
	if(document.getElementById('apv_div3_flg').checked)
	{
		apv_div3_flg = 't';
	}

	var apv_div4_flg = 'f';
	if(document.getElementById('apv_div4_flg').checked)
	{
		apv_div4_flg = 't';
	}

	if(apv_div0_flg == 'f' && apv_div1_flg == 'f' && apv_div2_flg == 'f' && apv_div3_flg == 'f' && apv_div4_flg == 'f')
	{
        alert('承認グループを選択してください。');
        return false;
	}

	// 複数承認者指定
	multi_apv_flg = "f";
	for(i=0; i<document.wkfw.multi_apv_flg.length; i++)
	{
		if(document.wkfw.multi_apv_flg[i].checked) {
			multi_apv_flg = document.wkfw.multi_apv_flg[i].value;
		}
	}

	// 複数承認者を許可しない場合
	if(multi_apv_flg == 'f')
	{
		// 承認者指定「その他」項目チェック
		if(document.getElementById('apv_div2_flg').checked)
		{
		    if(document.getElementById('apv_div0_flg').checked
		    	|| document.getElementById('apv_div1_flg').checked
		    	|| document.getElementById('apv_div3_flg').checked
		    	|| document.getElementById('apv_div4_flg').checked)
			{
				alert('複数承認者を許可しない場合、「その他」項目は他の承認グループと同時に設定できません。');
				return false;
			}
		}

		if(apv_num > 1)
		{
			alert('複数承認者を許可しない場合、「その他」項目の人数設定で複数人の指定はできません。');
			return false;
		}
	}

	// 部署役職(申請者所属)、職員、委員会・ＷＧ、その他、部署役職(部署指定)
	 // 部署役職(申請者所属)
	if(apv_div0_flg == 't')
	{
		var div_nm = "";

    	for (i = 0; i < document.wkfw.target_class_div.length; i++) {
			if(document.wkfw.target_class_div[i].checked) {
				target_class_div = document.wkfw.target_class_div[i].value;
			}
    	}

		<?for($i=1; $i<=$level_cnt; $i++) {?>
			if('<?echo ($i);?>' == target_class_div) {
				target_class_nm = '<?echo ($arr_org_level[$i-1]);?>';
			}
		<?}?>

		var chkcnt=0;
	    for (i = 0; i<document.wkfw.mng_post.length; i++) {
			if(document.wkfw.mng_post[i].checked) {
				var div_id = document.wkfw.mng_post[i].value;
				<?foreach($arr_manager_post as $row) {?>
					if(<?echo($row["id"]);?> == div_id) {

						if(chkcnt > 0) {
							div_nm += ", ";
							st_id += ",";
						}
						div_nm += '<?echo($row["name"]);?>';
						st_id += '<?echo($row["id"]);?>'
					}
				<?}?>
				chkcnt++;
			}
    	}
		if(chkcnt == 0) {
			alert('申請者所属の役職を指定してください。');
			return false;
		}

		if(target_class_nm != "") {
			approve_content = '申請者の所属する【' + target_class_nm + '】の' + div_nm;
		} else {
			<?
			// 病院内/所内
			if($_label_by_profile["IN_HOSPITAL2"][$profile_type] != "")
			{
			?>
			approve_content = '【<?=$_label_by_profile["IN_HOSPITAL2"][$profile_type]?>】の' + div_nm;
			<?
			}
			else
			{
			?>
			approve_content = div_nm;
			<?
			}
			?>
		}
	}


	// 部署役職(部署指定)
	if(apv_div4_flg == 't')
	{
		if(approve_content != '')
		{
			approve_content += "<BR>";
		}

		obj = document.getElementById('class_sect');
		class_sect_id = obj.options[obj.selectedIndex].value;
		if(class_sect_id == "")
		{
			alert('部署・役職(部署指定)の部署を指定してください。');
            return false;
		}

		var class_sect_name = obj.options[obj.selectedIndex].text;
		if(class_sect_id != "")
		{
			approve_content += class_sect_name;
		}

		obj = document.getElementById('atrb_sect');
		atrb_sect_id = obj.options[obj.selectedIndex].value;
		var atrb_sect_name = obj.options[obj.selectedIndex].text;
		if(atrb_sect_id != "")
		{
			if(approve_content != "")
			{
				approve_content += " > ";
			}
			approve_content += atrb_sect_name;
		}

		obj = document.getElementById('dept_sect');
		dept_sect_id = obj.options[obj.selectedIndex].value;
		var dept_sect_name = obj.options[obj.selectedIndex].text;
		if(dept_sect_id != "")
		{
			if(approve_content != "")
			{
				approve_content += " > ";
			}
			approve_content += dept_sect_name;
		}

		obj = document.getElementById('room_sect');
		if(obj)
		{
			room_sect_id = obj.options[obj.selectedIndex].value;
			var room_sect_name = obj.options[obj.selectedIndex].text;
			if(room_sect_id != "")
			{
				if(approve_content != "")
				{
					approve_content += " > ";
				}
				approve_content += room_sect_name;
			}
		}

		var chkcnt=0;
		div_nm = "";
	    for (i = 0; i<document.wkfw.mng_post_sect.length; i++) {
			if(document.wkfw.mng_post_sect[i].checked) {
				var div_id = document.wkfw.mng_post_sect[i].value;
				<?foreach($arr_manager_post as $row) {?>
					if(<?echo($row["id"]);?> == div_id) {

						if(chkcnt > 0) {
							div_nm += ", ";
							st_sect_id += ",";
						}
						div_nm += '<?echo($row["name"]);?>';
						st_sect_id += '<?echo($row["id"]);?>'
					}
				<?}?>
				chkcnt++;
			}
    	}
		if(chkcnt == 0) {
			alert('部署・役職(部署指定)の役職を指定してください。');
			return false;
		}

		approve_content += 'の';
		approve_content += div_nm;
	}

	// 職員指定
	if(apv_div1_flg == 't')
	{
		if(approve_content != '')
		{
			approve_content += "<BR>";
		}
        var target_length = document.wkfw.target.options.length;
        if(target_length == 0)
        {
            alert('職員を選択してください。');
            return false;
        }

		for(i=0; i<target_length; i++)
		{
			if(i > 0)
			{
				emp_id += ",";
				emp_nm += ", ";
			}
	        emp_id += document.wkfw.target.options[i].value;
    	    emp_nm += document.wkfw.target.options[i].text;
		}
		approve_content += emp_nm;

	}

	// 委員会ＷＧ
	if(apv_div3_flg == 't')
	{
		if(approve_content != '')
		{
			approve_content += "<BR>";
		}

		pjt_parent_id = document.getElementById('pjt_parent_id').value;
		if(pjt_parent_id == "")
		{
            alert('委員会・ＷＧを選択してください。');
            return false;
		}
		approve_content += document.getElementById('project').innerHTML;
		pjt_parent_id = document.getElementById('pjt_parent_id').value;
		pjt_child_id  = document.getElementById('pjt_child_id').value;
		pjt_parent_nm = document.getElementById('pjt_parent_nm').value;
		pjt_child_nm  = document.getElementById('pjt_child_nm').value;

	}

	// その他
	if(apv_div2_flg == 't')
	{
		if(approve_content != '')
		{
			approve_content += "<BR>";
		}
		approve_content += '申請時に承認者を指定する';
		approve_content += '(';
		approve_content += apv_num;
		approve_content += '名)';
	}


	window.opener.document.getElementById('approve_content<?=$approve?>').innerHTML = approve_content;
	window.opener.document.wkfw.approve<? echo($approve); ?>.value = approve_content;

	window.opener.document.wkfw.apv_div0_flg<? echo($approve); ?>.value = apv_div0_flg;
	window.opener.document.wkfw.apv_div1_flg<? echo($approve); ?>.value = apv_div1_flg;
	window.opener.document.wkfw.apv_div2_flg<? echo($approve); ?>.value = apv_div2_flg;
	window.opener.document.wkfw.apv_div3_flg<? echo($approve); ?>.value = apv_div3_flg;
	window.opener.document.wkfw.apv_div4_flg<? echo($approve); ?>.value = apv_div4_flg;

	window.opener.document.wkfw.target_class_div<? echo($approve); ?>.value = target_class_div;
	window.opener.document.wkfw.st_id<? echo($approve); ?>.value = st_id;

	window.opener.document.wkfw.emp_id<? echo($approve); ?>.value = emp_id;
	window.opener.document.wkfw.emp_nm<? echo($approve); ?>.value = emp_nm;

	window.opener.document.wkfw.pjt_parent_id<? echo($approve); ?>.value = pjt_parent_id;
	window.opener.document.wkfw.pjt_child_id<? echo($approve); ?>.value = pjt_child_id;

	window.opener.document.wkfw.pjt_parent_nm<? echo($approve); ?>.value = pjt_parent_nm;
	window.opener.document.wkfw.pjt_child_nm<? echo($approve); ?>.value = pjt_child_nm;


	window.opener.document.wkfw.class_sect_id<? echo($approve); ?>.value = class_sect_id;
	window.opener.document.wkfw.atrb_sect_id<? echo($approve); ?>.value = atrb_sect_id;
	window.opener.document.wkfw.dept_sect_id<? echo($approve); ?>.value = dept_sect_id;
	window.opener.document.wkfw.room_sect_id<? echo($approve); ?>.value = room_sect_id;

	window.opener.document.wkfw.st_sect_id<? echo($approve); ?>.value = st_sect_id;

	// 複数承認者
	window.opener.document.wkfw.multi_apv_flg<? echo($approve); ?>.value = multi_apv_flg;

	// 次の承認者への通知タイミング
	if(multi_apv_flg == 't')
	{
		for(i=0; i<document.wkfw.next_notice_div.length; i++)
		{
			if(document.wkfw.next_notice_div[i].checked) {
				next_notice_div = document.wkfw.next_notice_div[i].value;
			}
		}
		window.opener.document.wkfw.next_notice_div<? echo($approve); ?>.value = next_notice_div;
	}
	else
	{
		window.opener.document.wkfw.next_notice_div<? echo($approve); ?>.value = '';
	}

	window.opener.document.wkfw.apv_num<? echo($approve); ?>.value = apv_num;

	closeEmployeeList();

	self.close();
}

function groupOnChange() {
	var group_id = getSelectedValue(document.wkfw.mygroup);

	// 職員一覧セレクトボックスのオプションを全削除
	deleteAllOptions(document.wkfw.emplist);

	// 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
	if (group_id == '<? echo $tmp_group_id; ?>') {
	<? foreach($arr as $tmp_emp) { ?>
		addOption(document.wkfw.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
	<? } ?>
	}
<? } ?>
}

function set_wm_counter(ids)
{
	
	var url = 'emplist_address_counter.php';
	var params = $H({'session':'<?=$session?>','emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url, 
		{
			method: 'post',
			postBody: params
		});
}

function changeNextNotice(val)
{
	if(val == 't')
	{
		notice_flg = false;
	}
	else
	{
		notice_flg = true;
	}

	for (var i = 0; i < document.wkfw.next_notice_div.length; i++) {
		document.wkfw.next_notice_div[i].disabled = notice_flg;
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}
table.block_in td td {border-width:1;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3" onload="initPage();">
<form name="wkfw" action="cas_workflow_approve_list.php" method="post">
<input type="hidden" name="approve" value="<? echo($approve); ?>">


<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>承認者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="closeEmployeeList();window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td>
<table width="20%" border="0" cellspacing="0" cellpadding="0"  class="list">
<tr height="22">
<td bgcolor="#E5F6CD" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層<? echo($approve); ?>の設定</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="設定" onclick="setApprove();">
</td>
</tr>
</table>


<!--複数承認者指定、通知タイミング設定 -->
<table border='0' cellspacing='0' cellpadding='0' class="block_in">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■複数承認者の指定</font></td>
<td colspan="2">
<input type="radio" name="multi_apv_flg" value="t" onclick="changeNextNotice(this.value);"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">許可する</font>
<input type="radio" name="multi_apv_flg" value="f" onclick="changeNextNotice(this.value);"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">許可しない</font>
</td>
</tr>

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■次の承認者への通知タイミング</font></td>
<td><input type="radio" name="next_notice_div" value="1"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非同期(承認者ごとに個々に通知)</font></td>
<td><input type="radio" name="next_notice_div" value="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同期(全員が承認したら通知)</font></td>
</tr>
<tr>
<td></td>
<td colspan="2"><input type="radio" name="next_notice_div" value="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限並列(承認者の一人でも承認すれば承認完了、通知)</font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>


<table width="100%" border='0' cellspacing='0' cellpadding='0' class="block">
<!--部署役職・申請者所属指定 -->
<tr>
<td rowspan="2" width="20%" height="100" cellspacing="0" cellpadding="0" border="0" bgcolor="#E5F6CD">
<table border='0' cellspacing='0' cellpadding='0' class="block_in">
<tr>
<td>

<input type="checkbox" id="apv_div0_flg" name="apv_div0_flg" value="t" onclick="click_use_check('0');">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font><BR>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="margin-left:2em">(申請者所属)</font>
</td>
</tr>
</table>
</td>

<td width="40%">
<table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#FFFFFF">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
</tr>
</table>
</td>

<td width="40%">
<table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#FFFFFF">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
</table>
</td>
</tr>

<tr>
<td>
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者の所属する<BR></font></td>
<td>
<table>
<?for ($i=0; $i<$level_cnt; $i++) {?>
<tr><td><input type="radio" name="target_class_div" value="<?echo($i + 1);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_org_level[$i]?><BR></td></tr>
<?}?>
<tr><td><input type="radio" name="target_class_div" value="0"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署指定しない<BR></td></tr>
</table>
</td>
</tr>
</table>
</td>


<td>
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
<td>
<?foreach($arr_manager_post as $row) {?>
<input type="checkbox" name="mng_post" value="<?echo($row["id"]);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo($row["name"]);?></font><BR>
<?}?>
</td>
</tr>
</table>
</td>
</tr>

<!-- 部署役職・部署指定 -->

<tr>
<td rowspan="2" width="20%" height="100" cellspacing="0" cellpadding="0" border="0" bgcolor="#E5F6CD">
<table border='0' cellspacing='0' cellpadding='0' class="block_in">
<tr>
<td>

<input type="checkbox" id="apv_div4_flg" name="apv_div4_flg" value="t" onclick="click_use_check('4');">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職</font><BR>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" style="margin-left:2em">(部署指定)</font>
</td>
</tr>
</table>
</td>

<td width="40%">
<table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#FFFFFF">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
</tr>
</table>
</td>

<td width="40%">
<table width="100%" height="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#FFFFFF">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
</table>
</td>
</tr>

<tr>
<td>
<?
show_post_select_box_for_approve_list($con, $fname);
?>
</td>
<script type="text/javascript">
    class_sect_id = window.opener.document.wkfw.class_sect_id<? echo($approve); ?>.value;
    atrb_sect_id = window.opener.document.wkfw.atrb_sect_id<? echo($approve); ?>.value;
    dept_sect_id = window.opener.document.wkfw.dept_sect_id<? echo($approve); ?>.value;
    room_sect_id = window.opener.document.wkfw.room_sect_id<? echo($approve); ?>.value;

    set_post_select_box_obj_key("post_section","class_sect","atrb_sect","dept_sect","room_sect");
    setClassOptions("post_section",class_sect_id,atrb_sect_id,dept_sect_id,room_sect_id);
</script>

<td>
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
<td>
<?foreach($arr_manager_post as $row) {?>
<input type="checkbox" name="mng_post_sect" value="<?echo($row["id"]);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo($row["name"]);?></font><BR>
<?}?>
</td>
</tr>
</table>
</td>
</tr>





<!-- 職員指定 -->
<tr>
<td width="20%" bgcolor="#E5F6CD">
<table>
<tr>
<td>
<input type="checkbox" id="apv_div1_flg" name="apv_div1_flg" value="t" onclick="click_use_check('1');">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
</td>
</tr>
</table>
</td>

<td colspan="2" bgcolor="#FFFFFF">
<table width="100%" border="0" cellspacing="3" cellpadding="0">
<tr>

<td width="160" height="22" valign="bottom">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象者</font>
</td>

<td width="30"></td>
<td width="*">
<select name="mygroup" onchange="groupOnChange();">
<?
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?
if($mygroup == $tmp_mygroup_id) {
 echo(" selected");
}
?>
><?=$tmp_mygroup?></option>
<?
}
?>
</selelct>
</td>
</tr>
<tr>
<td>
<select name="target" size="10" multiple style="width:150px;"></select>
</td>
<td>
<input type="button" name="add_emp" value="&nbsp;&lt;&nbsp;追加" onclick="addEmp();">
<br><br>
<input type="button" name="delete_emp" value="削除&nbsp;&gt;&nbsp;" onclick="deleteEmp();">
</td>
<td>
<select name="emplist" size="10" style="width:150px;" multiple></select>
</td>
</tr>
</table>
</td>
</tr>

<!-- 委員会・ＷＧ -->
<tr>
<td height="60" width="20%" bgcolor="#E5F6CD">
<table>
<tr>
<td>
<input type="checkbox" id="apv_div3_flg" name="apv_div3_flg" value="t" onclick="click_use_check('3');">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・ＷＧ</font><br>
<input type="button" name="pjtlist_btn" value="一覧" style="margin-left:2em;width=5.5em;" onclick="openProjectList();"><br>
</td>
</tr>
</table>
</td>
<td colspan="2" bgcolor="#FFFFFF">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="project"></span></font>
</td>
</tr>
<input type="hidden" id="pjt_parent_id" name="pjt_parent_id" value="">
<input type="hidden" id="pjt_child_id" name="pjt_child_id" value="">
<input type="hidden" id="pjt_parent_nm" name="pjt_parent_nm" value="">
<input type="hidden" id="pjt_child_nm" name="pjt_child_nm" value="">

<!-- その他 -->
<tr>
<td width="20%" bgcolor="#E5F6CD">
<table>
<tr>
<td>
<input type="checkbox" id="apv_div2_flg" name="apv_div2_flg" value="t" onclick="click_use_check('2');">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font>
</td>
</tr>
</table>
</td>
<td colspan="2" bgcolor="#FFFFFF">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">書類申請時に承認者を指定する。
<select name="apv_num" id="apv_num">
<?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($apv_num == $i) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select>
名</font>
</td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="設定" onclick="setApprove();">
</td>
</tr>
</table>
</td>
</tr>
</table>


</form>
</body>
</html>

<?
pg_close($con);
?>