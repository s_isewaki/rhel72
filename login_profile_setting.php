<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ログイン | プロフィール設定</title>
<?
//ini_set("display_errors","1");
require("about_postgres.php");
require("show_select_values.ini");
require("show_class_name.ini");
require("news_common.ini");
require("get_values.ini");
require("show_newsuser_search_from_login.ini");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// カテゴリ一覧を取得
$category_list = get_category_list($class_called);

// 部門一覧を取得
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// 通達区分一覧を取得
$notice_list = get_notice_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

// cookie取得
$tmp_class_id=$_COOKIE["cookie_class_id"];
$class_id = split(",", $tmp_class_id);

$srch_dest_class_id = $class_id[0];
$srch_dest_atrb_id = $class_id[1];
$srch_dest_dept_id = $class_id[2];

$srch_job_id=$_COOKIE["cookie_job_id"];
$cate_id=$_COOKIE["cookie_cate_id"];
$arr_cate_id = split(",", $cate_id);
$notice_id=$_COOKIE["cookie_notice_id"];
$arr_notice_id = split(",", $notice_id);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setDestAtrbOptions('<? echo($srch_dest_atrb_id); ?>', '<? echo($srch_dest_dept_id); ?>', '<? echo($srch_dest_room_id); ?>');
}

function setDestAtrbOptions(atrb_id, dept_id, room_id) {
	var atrb_elm = document.mainform.srch_dest_atrb_id;
	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', '　　　', atrb_id);

	var class_id = document.mainform.srch_dest_class_id.value;
	switch (class_id) {
<?
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setDestDeptOptions(dept_id, room_id);
}

function setDestDeptOptions(dept_id, room_id) {
	var dept_elm = document.mainform.srch_dest_dept_id;
	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', '　　　', dept_id);

	var atrb_id = document.mainform.srch_dest_atrb_id.value;
	switch (atrb_id) {
<?
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setDestRoomOptions(room_id);
}

function setDestRoomOptions(room_id) {
<? if ($arr_class_name['class_cnt'] != 4) { ?>
	return;
<? } ?>

	var room_elm = document.mainform.srch_dest_room_id;
	deleteAllOptions(room_elm);
	addOption(room_elm, '0', '　　　', room_id);

	var dept_id = document.mainform.srch_dest_dept_id.value;
	switch (dept_id) {
<?
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
	echo("\tcase '$tmp_dept_id':\n");
	foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
		echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function selectEmployee() {
	window.open('news_employee_select_from_login.php?session=<? echo($session); ?>&class_id='.concat(document.mainform.srch_src_class_id.value), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	if (selected == value) {
		box.options[box.length - 1].selected = true;
	}
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function changeCategory() {
	var id = document.mainform.srch_news_category.value;
	
	var tr_srch_dest_class = document.getElementById('srch_dest_class');
	var tr_srch_job = document.getElementById('srch_job');
	
	if (id == "0") {
		tr_srch_dest_class.style.display = '';
		tr_srch_job.style.display = '';
	} else if (id == "1") {
		tr_srch_dest_class.style.display = 'none';
		tr_srch_job.style.display = 'none';
	} else if (id == "2") {
		tr_srch_dest_class.style.display = '';
		tr_srch_job.style.display = 'none';
	} else if (id == "3") {
		tr_srch_dest_class.style.display = 'none';
		tr_srch_job.style.display = '';
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="2" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff">&nbsp;<b>パソコン（ＰＣ）のプロフィール設定</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="login_profile_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr id="srch_dest_class" height="22" style="display:">
<td align="right" bgcolor="#f6f9ff" width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">このＰＣの設置場所</font></td>
<td><select name="srch_dest_class_id" onchange="setDestAtrbOptions();"><? show_options($class_list, $srch_dest_class_id, true); ?></select><select name="srch_dest_atrb_id" onchange="setDestDeptOptions();"></select><select name="srch_dest_dept_id" onchange="setDestRoomOptions();"></select><div id="dest_room" style="display:none"><? if ($arr_class_name['class_cnt'] == 4) { ?><select name="srch_dest_room_id"></select><? } ?></div>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（下位の階層については設定しないこともできます）</font>
</td>
</tr>
<tr id="srch_job" height="22" style="display:">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">このＰＣの主な利用職種</font></td>
<td><select name="srch_job_id"><? show_options($job_list, $srch_job_id, true); ?></select></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>お知らせ表示抽出設定（チェックしなければ、全て表示）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
// 通達区分のチェックボックス表示
foreach($notice_list as $id => $name) {
	$checked = "";
	foreach($arr_notice_id as $tmp_id) {
		if ($id == $tmp_id) {
			$checked = " checked";
			break;
		}
	}
?>
<input type="checkbox" name="notice[]" value="<?=$id?>" <?=$checked?>><?=$name?> 
<? } ?>
</font>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
</form>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
