<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_time_guide_list.ini");
require("time_guide_next.ini");
require("referer_common.ini");
require("show_date_navigation_month.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// デフォルト日付はシステム日付
if ($date == "") {$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));}
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($event != "") {
	set_referer($con, $session, "event", $event, $fname);
	$referer = $event;
} else {
	$referer = get_referer($con, $session, "event", $fname);
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_1 = pg_fetch_result($sel, 0, "menu2_1");

// 施設一覧を取得
$sql = "select fcl_id, name from timegdfcl";
$cond = "order by fcl_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 院内行事登録/法人内行事登録
$event_reg_title =  $_label_by_profile["EVENT_REG"][$profile_type];
?>
<title><? echo($event_reg_title); ?> | 行事一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteEvents() {
	if (confirm('削除してよろしいですか？')) {
		document.mainform.action = 'time_guide_delete.php';
		document.mainform.submit();
	}
}

function changeDate(dt) {
	var fcl_id = document.mainform.fcl_id.value;
	location.href = 'time_guide_list.php?session=<? echo($session); ?>&fcl_id=' + fcl_id + '&date=' + dt + '&mode=<? echo($mode); ?>';
}

function displayEventList() {
	document.mainform.action = 'time_guide_list.php';
	document.mainform.mode.value = 'display';
	document.mainform.page.value = '';
	document.mainform.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="time_guide_list.php?session=<? echo($session); ?>"><img src="img/icon/b22.gif" width="32" height="32" border="0" alt="<? echo($event_reg_title); ?>"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="time_guide_list.php?session=<? echo($session); ?>"><b><? echo($event_reg_title); ?></b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_event.php?session=<? echo($session); ?>"><b><? echo($menu2_1); ?></b></a> &gt; <a href="time_guide_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_info_event.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#5279a5"><a href="time_guide_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>行事一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行事登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteEvents();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設：<select name="fcl_id">
<option value="-">全体
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_fcl_id = $row["fcl_id"];
	$tmp_name = $row["name"];
	echo("<option value=\"$tmp_fcl_id\"");
	if ($tmp_fcl_id == $fcl_id) {echo(" selected");}
	echo(">$tmp_name\n");
}
?>
</select>
<span style="margin-left:20px;"><a href="javascript:void(0);" onclick="changeDate('<? echo($last_month); ?>');return false;">&lt;前月</a>&nbsp;<select name="date" onchange="changeDate(this.value);"><? show_date_options_m($date); ?></select>&nbsp;<a href="javascript:void(0);" onclick="changeDate('<? echo($next_month); ?>');return false;">翌月&gt;</a></span>
<span style="margin-left:30px;"><input type="button" value="表示" onclick="displayEventList();"></span>
</font></td>
</tr>
</table>
<? if ($mode == "display") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff" height="22">
<td width="8%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
<td width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="15%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行事名</font></td>
</tr>
<? show_time_guide_list($con, $fcl_id, $date, $session, $page, $fname); ?>
</table>
<? show_next($con, $fcl_id, $date, $session, $page, $fname); ?>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
