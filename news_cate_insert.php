<?php
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");
?>
<body>
<form name="items" action="news_cate_register.php" method="post">
<input type="hidden" name="newscate_name" value="<? echo($newscate_name) ?>">
<input type="hidden" name="news_category" value="<? echo($news_category) ?>">
<?php
if (is_array($class_id)) {
	for ($i = 0, $j = count($class_id); $i < $j; $i++) {
		echo("<input type=\"hidden\" name=\"class_id[]\" value=\"$class_id[$i]\">\n");
		echo("<input type=\"hidden\" name=\"atrb_id[]\" value=\"$atrb_id[$i]\">\n");
		echo("<input type=\"hidden\" name=\"dept_id[]\" value=\"$dept_id[$i]\">\n");

		if ($atrb_id[$i] == "0") {$atrb_id[$i] = null;}
		if ($dept_id[$i] == "0") {$dept_id[$i] = null;}
	}
}
if (is_array($job_id)) {
	for ($i = 0, $j = count($job_id); $i < $j; $i++) {
		echo("<input type=\"hidden\" name=\"job_id[]\" value=\"$job_id[$i]\">\n");
	}
}
if (is_array($st_id)) {
	for ($i = 0, $j = count($st_id); $i < $j; $i++) {
		echo("<input type=\"hidden\" name=\"st_id[]\" value=\"$st_id[$i]\">\n");
	}
}
?>
<input type="hidden" name="session" value="<? echo($session) ?>">
<input type="hidden" name="back" value="t">
</form>
<?php

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($newscate_name == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('カテゴリ名称が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($newscate_name) > 40) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('カテゴリ名称が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$st_id_cnt = 0;
for ($i = 0, $j = count($st_id); $i < $j; $i++) {
	if ($st_id[$i] != "") {
		$st_id_cnt++;
	}
}
if ($news_category == "1" && $st_id_cnt == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('全館の場合には役職を必ず指定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// トランザクションを開始
pg_query($con, "begin");

// お知らせカテゴリIDの採番
$sql = "select coalesce(max(newscate_id) + 1, 1) from newscate where newscate_id < 10000";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$newscate_id = pg_fetch_result($sel, 0, 0);

$dept_id_cnt = 0;
if ($news_category == "2") {
	for ($i = 0, $j = count($class_id); $i < $j; $i++) {
		if ($class_id[$i] != "") {
			$dept_id_cnt++;
		}
	}
}
// お知らせカテゴリの登録
$sql = "insert into newscate (newscate_id, newscate_name, standard_cate_id, job_id, dept_id_cnt, st_id_cnt, delete_flg, no_disp_flg) values (";
$content = array($newscate_id, $newscate_name, $news_category, null, $dept_id_cnt, $st_id_cnt, 'f', 'f');
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// お知らせ配信先部署情報を登録
if ($news_category == "2") {
	for ($i = 0, $j = count($class_id); $i < $j; $i++) {
		$sql = "delete from newscatedept";
		$cond = "where newscate_id = $newscate_id and class_id = $class_id[$i]";
		if (!is_null($atrb_id[$i])) {
			$cond .= " and atrb_id = $atrb_id[$i]";
		} else {
			$cond .= " and atrb_id is null";
		}
		if (!is_null($dept_id[$i])) {
			$cond .= " and dept_id = $dept_id[$i]";
		} else {
			$cond .= " and dept_id is null";
		}
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "insert into newscatedept (newscate_id, class_id, atrb_id, dept_id) values (";
		$content = array($newscate_id, $class_id[$i], $atrb_id[$i], $dept_id[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// お知らせ職種情報を登録
if ($news_category == "3") {
	for ($i = 0, $j = count($job_id); $i < $j; $i++) {
		$sql = "insert into newscatejob (newscate_id, job_id) values (";
		$content = array($newscate_id, $job_id[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 役職情報を更新
$sql = "delete from newscatest";
$cond = "where newscate_id = $newscate_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if ($st_id_cnt > 0) {
	for ($i = 0, $j = count($st_id); $i < $j; $i++) {
		if ($st_id[$i] == "") {
			continue;
		}
		$sql = "insert into newscatest (newscate_id, st_id) values (";
		$content = array($newscate_id, $st_id[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// お知らせカテゴリ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_cate_list.php?session=$session';</script>");
?>
</body>
