<?php
//require_once('holiday.php');
/**
 * 管理指標 休日取得クラス
 * このクラスはソース以下のコメントアウトのファイルを
 * PHP4 用の class に置き換えたものです。
 *
 * PHP5 への移行時に参考になるように access を記述してあります。
 * @author r-yotsumoto
 */
class IndicatorHoliday
{
    /**
     * @access const
     */
    var $monday = 2;

    /**
     * @access const
     */
    var $tuesday = 3;

    /**
     * @access const
     */
    var $wednesday = 4;

    /**
     * コンストラクタ
     * @access public
     * @return void
     */
    function IndicatorHoliday()
    {
    }

    /**
     * @access public
     * @param  string $yyyymmdd
     * @return string
     */
    function get_holiday_name($yyyymmdd)
    {
        $y = substr($yyyymmdd, 0, 4);
        $m = substr($yyyymmdd, 4, 2);
        $d = substr($yyyymmdd, 6, 2);
        return $this->ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string    $result
     */
    function ktHolidayName($myDate)
    {
        $result = '';
        $cstImplementHoliday = mktime(0, 0, 0, 4, 12, 1973); # 振替休日施行

        $HolidayName = $this->prvHolidayChk($myDate);
        if ($HolidayName == "") {
            if ($this->weekday($myDate) === $this->monday) {
                # 月曜以外は振替休日判定不要
                # 5/6(火,水)の判定はprvHolidayChkで処理済
                # 5/6(月)はここで判定する
                if ($myDate >= $cstImplementHoliday) {
                    $YesterDay   = mktime(0, 0, 0, $this->month($myDate), ($this->day($myDate) - 1), $this->year($myDate));
                    $HolidayName = $this->prvHolidayChk($YesterDay);
                    if ($HolidayName != "") { $result = "振替休日"; }
                }
            }
        }
        else { $result = $HolidayName; }
        return $result;
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string    $result
     */
    function prvHolidayChk($myDate)
    {
        $cstImplementTheLawOfHoliday = mktime(0, 0, 0,  7, 20, 1948); # 祝日法施行
        $cstShowaTaiso               = mktime(0, 0, 0,  2, 24, 1989); # 昭和天皇大喪の礼
        $cstAkihitoKekkon            = mktime(0, 0, 0,  4, 10, 1959); # 明仁親王の結婚の儀
        $cstNorihitoKekkon           = mktime(0, 0, 0,  6,  9, 1993); # 徳仁親王の結婚の儀
        $cstSokuireiseiden           = mktime(0, 0, 0, 11, 12, 1990); # 即位礼正殿の儀

        $myYear  = $this->year($myDate);
        $MyMonth = $this->month($myDate);
        $myDay   = $this->day($myDate);

        if ($myDate < $cstImplementTheLawOfHoliday) { return ""; } # 祝日法施行以前

        $result = "";
        switch ($MyMonth) {
            # 1月
            case 1:
                if ($myDay == 1) { $result = "元日"; }
                else {
                    if ($myYear >= 2000) {
                        $strNumberOfWeek = (floor(($myDay - 1) / 7) + 1) . $this->weekday($myDate);
                        if ($strNumberOfWeek == "22") { $result = "成人の日"; } # monday:2
                    }
                    else { if ($myDay == 15) { $result = "成人の日"; } }
                }
                break;

            # 2月
            case 2:
                if ($myDay == 11) { if ($myYear >= 1967) { $result = "建国記念の日"; } }
                else if ($myDate == $cstShowaTaiso) { $result = "昭和天皇の大喪の礼"; }
                break;

            # 3月
            case 3:
                # 1948〜2150以外は[99]が返るので､必ず≠になる
                if ($myDay == $this->prvDayOfSpringEquinox($myYear)) { $result = "春分の日"; }
                break;

            # ４月
            case 4:
                if ($myDay == 29) {
                    if ($myYear >= 2007) { $result = "昭和の日"; }
                    else if ($myYear >= 1989) { $result = "みどりの日"; }
                    else { $result = "天皇誕生日"; }
                }
                else if ($myDate == $cstAkihitoKekkon) { $result = "皇太子明仁親王の結婚の儀"; }
                else;
                break;

            # ５月
            case 5:
                if ($myDay == 3) { $result = "憲法記念日"; }
                else if ($myDay == 4) {
                    if ($myYear >= 2007) { $result = "みどりの日"; }
                    else if ($myYear >= 1986) {
                        # 5/4が日曜日は『只の日曜』､月曜日は『憲法記念日の振替休日』(〜2006年)
                        if ($this->weekday($myDate) > $this->monday) { $result = "国民の休日"; }
                        else;
                    }
                    else;
                }
                else if ($myDay == 5) { $result = "こどもの日"; }
                else if ($myDay == 6) {
                    # [5/3,5/4が日曜]ケースのみ、ここで判定
                    if ($myYear >= 2007) { if ($this->weekday($myDate) == $this->tuesday || $this->weekday($myDate) == $this->wednesday) { $result = "振替休日"; } }
                    else;
                }
                else;
                break;

            # ６月
            case 6:
                if ($myDate == $cstNorihitoKekkon) { $result = "皇太子徳仁親王の結婚の儀"; }
                else;
                break;

            # ７月
            case 7:
                if ($myYear >= 2003) {
                   $strNumberOfWeek = (floor(($myDay - 1) / 7) + 1) . $this->weekday($myDate);
                    if ($strNumberOfWeek == "32") { $result = "海の日"; } # Monday:2
                    else;
                }
                else if ($myYear >= 1996) {
                    if ($myDay == 20) { $result = "海の日"; }
                    else;
                }
                else;
                break;

            # ９月
            case 9:
                # 第３月曜日(15〜21)と秋分日(22〜24)が重なる事はない
                $MyAutumnEquinox = $this->prvDayOfAutumnEquinox($myYear);
                # 1948〜2150以外は[99]が返るので､必ず≠になる
                if ($myDay == $MyAutumnEquinox) { $result = "秋分の日"; }
                else {
                    if ($myYear >= 2003) {
                        $strNumberOfWeek = (floor(($myDay - 1) / 7) + 1) . $this->weekday($myDate);
                        if ($strNumberOfWeek == "32") { $result = "敬老の日"; } # Monday:2
                        else if ($this->weekday($myDate) == $this->tuesday) {
                            if ($myDay == ($MyAutumnEquinox - 1)) { $result = "国民の休日"; }
                            else;
                        }
                        else;
                    }
                    else if ($myYear >= 1966) {
                        if ($myDay == 15) { $result = "敬老の日"; }
                        else;
                    }
                    else;
                }
                break;

            # １０月
            case 10:
                if ($myYear >= 2000) {
                    $strNumberOfWeek = (floor(( $myDay - 1) / 7) + 1) . $this->weekday($myDate);
                    if ($strNumberOfWeek == "22") { $result = "体育の日"; } # Monday:2
                    else;
                }
                else if ($myYear >= 1966) {
                    if ($myDay == 10) { $result = "体育の日"; }
                    else;
                }
                else;
                break;

            # １１月
            case 11:
                if ($myDay == 3) { $result = "文化の日"; }
                else if ($myDay == 23) { $result = "勤労感謝の日"; }
                else if ($myDate == $cstSokuireiseiden) { $result = "即位礼正殿の儀"; } # 07/04/11 $抜け修正
                else;
                break;

            # １２月
            case 12:
                if ($myDay == 23) {
                    if ($myYear >= 1989) { $result = "天皇誕生日"; }
                    else;
                }
                else;
                break;
        }
        return $result;
    }

    /**
     * @access protected
     * @param  integer   $myYear
     * @return integer / float
     */
    function prvDayOfSpringEquinox($myYear)
    {
        if ($myYear <= 1947)
            $result = 99; # 祝日法施行前
        elseif ($myYear <= 1979)
            $result = floor(20.8357 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4)); # floor 関数は[VBAのInt関数]に相当
        elseif ($myYear <= 2099)
            $result = floor(20.8431 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4));
        elseif ($myYear <= 2150)
            $result = floor(21.851 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4));
        else
            $result = 99; # 2151年以降は略算式が無いので不明

        return $result;
    }

    /**
     * @access protected
     * @param  integer   $myYear
     * @return integer / float
     */
    function prvDayOfAutumnEquinox($myYear)
    {
        if ($myYear <= 1947)
            $result = 99; # 祝日法施行前
        elseif ($myYear <= 1979)
            $result = floor(23.2588 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4)); # floor 関数は[VBAのInt関数]に相当
        elseif ($myYear <= 2099)
            $result = floor(23.2488 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4));
        elseif ($myYear <= 2150)
            $result = floor(24.2488 + (0.242194 * ($myYear - 1980)) - floor(($myYear - 1980) / 4));
        else
            $result = 99; //2151年以降は略算式が無いので不明

        return $result;
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string
     */
    function weekday($myDate)
    {
        return strftime("%w",$myDate) + 1;  # 日(1),月(2)‥‥土(7)
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string
     */
    function year($myDate)
    {
        return strftime("%Y",$myDate) - 0;  # 数値で返す
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string
     */
    function month($myDate)
    {
        return strftime("%m",$myDate) - 0;  # 数値で返す
    }

    /**
     * @access protected
     * @param  timestamp $myDate
     * @return string
     */
    function day($myDate)
    {
        return strftime("%d",$myDate) - 0;  # 数値で返す
    }
}
/*
<?
function get_holiday_name($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return ktHolidayName(mktime(0, 0, 0, $m, $d, $y));
}

// ＰＨＰ版
// オリジナルはyy/mm/dd文字列ですが、シリアルで入れてください

// sample code
//     $aa = mktime(0,0,0,12,23,2004);
//     echo ktHolidayName($aa);
// --- end

//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
//_/
//_/  CopyRight(C) K.Tsunoda(AddinBox) 2001 All Rights Reserved.
//_/  ( http://www.h3.dion.ne.jp/~sakatsu/index.htm )
//_/
//_/    この祝日判定コードは『Excel:kt関数アドイン』で使用している
//_/    ＶＢＡマクロを[PHP]に移植したものです。
//_/    この関数では、２００７年施行の改正祝日法(昭和の日)までを
//_/  　サポートしています(９月の国民の休日を含む)。
//_/
//_/  (*1)このコードを引用するに当たっては、必ずこのコメントも
//_/      一緒に引用する事とします。
//_/  (*2)他サイト上で本マクロを直接引用する事は、ご遠慮願います。
//_/      【 http://www.h3.dion.ne.jp/~sakatsu/holiday_logic.htm 】
//_/      へのリンクによる紹介で対応して下さい。
//_/  (*3)[ktHolidayName]という関数名そのものは、各自の環境に
//_/      おける命名規則に沿って変更しても構いません。
//_/
//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

function ktHolidayName($MyDate)
{
  define("MONDAY","2");

  $cstImplementHoliday = mktime(0,0,0,4,12,1973); // 振替休日施行

  $HolidayName = prvHolidayChk($MyDate);
  if ($HolidayName == "") {
      if (Weekday($MyDate) == MONDAY) {
          // 月曜以外は振替休日判定不要
          // 5/6(火,水)の判定はprvHolidayChkで処理済
          // 5/6(月)はここで判定する
          if ($MyDate >= $cstImplementHoliday) {
              $YesterDay = mktime(0,0,0,Month($MyDate),
                                      (Day($MyDate) - 1),Year($MyDate));
              $HolidayName = prvHolidayChk($YesterDay);
              if ($HolidayName != "") {
                  $Result = "振替休日";
              } else {
                  $Result = "";
              }
          } else {
              $Result = "";
          }
      } else {
          $Result = "";
      }
  } else {
      $Result = $HolidayName;
  }

  return $Result;
}

//========================================================================

function prvHolidayChk($MyDate)
{
  define("MONDAY","2");
  define("TUESDAY","3");
  define("WEDNESDAY","4");

  $cstImplementTheLawOfHoliday = mktime(0,0,0,7,20,1948);  // 祝日法施行
  $cstShowaTaiso = mktime(0,0,0,2,24,1989);                // 昭和天皇大喪の礼
  $cstAkihitoKekkon = mktime(0,0,0,4,10,1959);             // 明仁親王の結婚の儀
  $cstNorihitoKekkon = mktime(0,0,0,6,9,1993);             // 徳仁親王の結婚の儀
  $cstSokuireiseiden = mktime(0,0,0,11,12,1990);           // 即位礼正殿の儀

  $MyYear = Year($MyDate);
  $MyMonth = Month($MyDate);
  $MyDay = Day($MyDate);

  if ($MyDate < $cstImplementTheLawOfHoliday)
      return "";    // 祝日法施行以前
  else;

  $Result = "";
  switch ($MyMonth) {
  // １月 //
  case 1:
      if ($MyDay == 1) {
          $Result = "元日";
      } else {
          if ($MyYear >= 2000) {
              $strNumberOfWeek =
                        (floor(($MyDay - 1) / 7) + 1) . Weekday($MyDate);
              if ($strNumberOfWeek == "22") {    //Monday:2
                  $Result = "成人の日";
              } else;
          } else {
              if ($MyDay == 15) {
                  $Result = "成人の日";
              } else;
          }
      }
      break;
  // ２月 //
  case 2:
      if ($MyDay == 11) {
          if ($MyYear >= 1967) {
              $Result = "建国記念の日";
          } else;
      } elseif ($MyDate == $cstShowaTaiso) {
          $Result = "昭和天皇の大喪の礼";
      } else;
      break;
  // ３月 //
  case 3:
      if ($MyDay == prvDayOfSpringEquinox($MyYear)) {    // 1948〜2150以外は[99]
          $Result = "春分の日";                        // が返るので､必ず≠になる
      } else;
      break;
  // ４月 //
  case 4:
      if ($MyDay == 29) {
          if ($MyYear >= 2007) {
              $Result = "昭和の日";
          } elseif ($MyYear >= 1989) {
              $Result = "みどりの日";
          } else {
              $Result = "天皇誕生日";
          }
      } elseif ($MyDate == $cstAkihitoKekkon) {
          $Result = "皇太子明仁親王の結婚の儀";
      } else;
      break;
  // ５月 //
  case 5:
      if ($MyDay == 3) {
          $Result = "憲法記念日";
      } elseif ($MyDay == 4) {
          if ($MyYear >= 2007) {
              $Result = "みどりの日";
          } elseif ($MyYear >= 1986) {
              if (Weekday($MyDate) > MONDAY) {
                  // 5/4が日曜日は『只の日曜』､月曜日は『憲法記念日の振替休日』(〜2006年)
                  $Result = "国民の休日";
              } else;
          } else;
      } elseif ($MyDay == 5) {
          $Result = "こどもの日";
      } elseif ($MyDay == 6) {
          if ($MyYear >= 2007) {
              if (Weekday($MyDate) == TUESDAY || Weekday($MyDate) == WEDNESDAY) {
                  $Result = "振替休日";    // [5/3,5/4が日曜]ケースのみ、ここで判定
              } else;
          } else;
      } else;
      break;
  // ６月 //
  case 6:
      if ($MyDate == $cstNorihitoKekkon) {
          $Result = "皇太子徳仁親王の結婚の儀";
      } else;
      break;
  // ７月 //
  case 7:
      if ($MyYear >= 2003) {
          $strNumberOfWeek =
                    (floor(($MyDay - 1) / 7) + 1) . Weekday($MyDate);
          if ($strNumberOfWeek == "32") {    //Monday:2
              $Result = "海の日";
          } else;
      } elseif ($MyYear >= 1996) {
          if ($MyDay == 20) {
              $Result = "海の日";
          } else;
      } else;
      break;
  // ９月 //
  case 9:
      //第３月曜日(15〜21)と秋分日(22〜24)が重なる事はない
      $MyAutumnEquinox = prvDayOfAutumnEquinox($MyYear);
      if ($MyDay == $MyAutumnEquinox) {    // 1948〜2150以外は[99]
          $Result = "秋分の日";            // が返るので､必ず≠になる
      } else {
          if ($MyYear >= 2003) {
              $strNumberOfWeek =
                      (floor(($MyDay - 1) / 7) + 1) . Weekday($MyDate);
              if ($strNumberOfWeek == "32") {    //Monday:2
                  $Result = "敬老の日";
              } elseif (Weekday($MyDate) == TUESDAY) {
                  if ($MyDay == ($MyAutumnEquinox - 1)) {
                      $Result = "国民の休日";
                  } else;
              } else;
          } elseif ($MyYear >= 1966) {
              if ($MyDay == 15) {
                  $Result = "敬老の日";
              } else;
          } else;
      }
      break;
  // １０月 //
  case 10:
      if ($MyYear >= 2000) {
          $strNumberOfWeek =
                    (floor(( $MyDay - 1) / 7) + 1) . Weekday($MyDate);
          if ($strNumberOfWeek == "22") {    // Monday:2
              $Result = "体育の日";
          } else;
      } elseif ($MyYear >= 1966) {
          if ($MyDay == 10) {
              $Result = "体育の日";
          } else;
      } else;
      break;
  // １１月 //
  case 11:
      if ($MyDay == 3) {
          $Result = "文化の日";
      } elseif ($MyDay == 23) {
          $Result = "勤労感謝の日";
      } elseif ($MyDate == $cstSokuireiseiden) {    // 07/04/11 $抜け修正
          $Result = "即位礼正殿の儀";
      } else;
      break;
  // １２月 //
  case 12:
      if ($MyDay == 23) {
          if ($MyYear >= 1989) {
              $Result = "天皇誕生日";
          } else;
      } else;
      break;
  }
  return $Result;
}

//======================================================================
//  春分/秋分日の略算式は
//    『海上保安庁水路部 暦計算研究会編 新こよみ便利帳』
//  で紹介されている式です。
function prvDayOfSpringEquinox($MyYear)
{
  if ($MyYear <= 1947)
      $Result = 99; //祝日法施行前
  elseif ($MyYear <= 1979)
      // floor 関数は[VBAのInt関数]に相当
      $Result = floor(20.8357 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  elseif ($MyYear <= 2099)
      $Result = floor(20.8431 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  elseif ($MyYear <= 2150)
      $Result = floor(21.851 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  else
      $Result = 99; //2151年以降は略算式が無いので不明

  return $Result;
}

//========================================================================
function prvDayOfAutumnEquinox($MyYear)
{
  if ($MyYear <= 1947)
      $Result = 99; //祝日法施行前
  elseif ($MyYear <= 1979)
      // floor 関数は[VBAのInt関数]に相当
      $Result = floor(23.2588 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  elseif ($MyYear <= 2099)
      $Result = floor(23.2488 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  elseif ($MyYear <= 2150)
      $Result = floor(24.2488 +
            (0.242194 * ($MyYear - 1980)) - floor(($MyYear - 1980) / 4));
  else
      $Result = 99; //2151年以降は略算式が無いので不明

  return $Result;
}

//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
//_/  CopyRight(C) K.Tsunoda(AddinBox) 2001 All Rights Reserved.
//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

function Weekday($MyDate){
  return strftime("%w",$MyDate) + 1;  // 日(1),月(2)‥‥土(7)
}

function Year($MyDate){
  return strftime("%Y",$MyDate) - 0;  // 数値で返す
}

function Month($MyDate){
  return strftime("%m",$MyDate) - 0;  // 数値で返す
}

function Day($MyDate){
  return strftime("%d",$MyDate) - 0;  // 数値で返す
}

//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
//_/  Visual Basic Compatibility functions for PHP
//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

?>
*/
?>