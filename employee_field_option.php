<?
define("C2_SESSION_GROUP_NAME", "employee");
// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(__FILE__)."/class/Cmx/Core2/C2FW.php");
$c2app = c2GetC2App();
$EAL = $c2app->getEmpAuthority(C2_LOGIN_EMP_ID); // ログイン者の機能権限有無リスト
if(!$EAL["emp_reg_flg"]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者に管理者権限(19)が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行




define("SUCCEED_MSG",  "employee_field_options_succeed_msg");




// 拡張empmst項目1から5のラベル取得
$sql = "select * from spfm_field_struct where table_id = 'spft_empmst' and field_id like 'empmst_ext%'";
$_rows = c2dbGetRows($sql);
$fieldLabels = array();
foreach ($_rows as $row) $fieldLabels[$row["field_id"]] = $row["field_label"];



// 拡張empmst項目1から5の表示可能判定と編集可能判定を取得
// ・利用判定：disp_empがall_okか、そうでないかを判定
// ・編集判定：edit_empがall_okか、そうでないかを判定
$sql =
" select field_id, disp_emp, edit_emp from spfm_field_struct_auth".
" where table_id = 'spft_empmst' and field_id like 'empmst_ext%' and target_place = 'all'";
$rows = c2dbGetRows($sql);
$fieldStructAuthRows = array();
$ret = array();
foreach ($rows as $row) $fieldStructAuthRows[$row["field_id"]] = $row;




$errmsg = array();
if ($_REQUEST["try_update"]) {
	for ($idx=1; $idx<=5; $idx++) {
		// 再表示用にレコードを上書き
		$fieldLabels["empmst_ext".$idx] = $_REQUEST["empmst_ext".$idx];
		$fieldStructAuthRows["empmst_ext".$idx] = array(
			"disp_emp"=>$_REQUEST["disp_emp".$idx],
			"edit_emp"=>$_REQUEST["edit_emp".$idx]
		);
		// ついでに入力チェック
		if ($_REQUEST["disp_emp".$idx]=="all_ok" && !$_REQUEST["empmst_ext".$idx]) {
			$errmsg[]= "項目".$idx."を利用する場合は、項目名を指定してください。";
		}
	}

	// エラーがなければ更新
	if (!count($errmsg)) {
		for ($idx=1; $idx<=5; $idx++) {
			$sql =
			" update spfm_field_struct set".
			" field_label = " . c2dbStr($fieldLabels["empmst_ext".$idx]).
			" where table_id = 'spft_empmst'".
			" and field_id = 'empmst_ext".$idx."'";
			c2dbExec($sql);

			$row = $fieldStructAuthRows["empmst_ext".$idx];
			$sql =
			" update spfm_field_struct_auth set".
			" disp_emp = '".($row["disp_emp"]=="all_ok" ? "all_ok":"")."'".
			",edit_emp = '".($row["edit_emp"]=="all_ok" ? "all_ok":"")."'".
			" where table_id ='spft_empmst'".
			" and field_id = 'empmst_ext".$idx."'".
			" and target_place = 'all'";
			c2dbExec($sql);
		}

		c2Env::setGlobalSession(SUCCEED_MSG, "登録しました。");
		header("Location: employee_field_option.php");
		die;
	}

}

$succeed_msg = c2Env::getGlobalSession(SUCCEED_MSG);
if (!count($errmsg) && $succeed_msg) $errmsg[]= $succeed_msg;
c2Env::unsetGlobalSession(SUCCEED_MSG);




c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
c2env::echoStaticHtmlTag("HTML5_HEAD_META");
//**********************************************************************************************************************
//**********************************************************************************************************************
//**********************************************************************************************************************
?>
<title>CoMedix 職員登録 | 新規登録</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
	body, table { font-size:13px }
	.list {border-collapse:collapse; }
	.list td, .list th { border:#5279a5 solid 1px; }
	.prop_list tr { height:22px }
	.prop_list th { text-align:right; font-weight:normal; background-color:#f6f9ff }
	.basic_tab_menu { border-bottom:2px solid #5279a5 }
	.basic_tab_menu th { text-align:center; font-weight:normal; background-color:#bdd1e7 }
	.basic_tab_menu th.current { background-color:#5279a5 }
	.basic_tab_menu th.current a { font-weight:bold; color:#ffffff }
</style>
<script type="text/javascript">
<? if (count($errmsg)) {?> alert("<?=implode("\\n", $errmsg)?>"); <? } ?>
</script>
</head>
<body style="margin:0 1px 0 0; padding:0"><div id="CLIENT_FRAME_CONTENT">



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" style="padding:0 0 0 6px"><a href="employee_info_menu.php?session=<?=hh($session)?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録" style="vertical-align:top"></a></td>
<td width="100%" style="font-size:16px; padding:0">&nbsp;<a href="employee_info_menu.php?session=<?=hh($session)?>"><b>職員登録</b></a></td>
</tr>
</table>




<table width="100%" border="0" cellspacing="0" cellpadding="0" class="basic_tab_menu" style="margin-bottom:5px;">
<tr height="22">
<th width="70"><a href="employee_info_menu.php?session=<?=hh($session)?>">検索</a></th>
<td width="5">&nbsp;</td>
<th width="70"><a href="employee_list.php?session=<?=hh($session)?>">一覧</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_register.php?session=<?=hh($session)?>">新規登録</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_bulk_register.php?session=<?=hh($session)?>">一括登録</a></th>
<td width="5">&nbsp;</td>
<th width="140"><a href="employee_db_list.php?session=<?=hh($session)?>">職員登録用DB操作</a></th>
<td width="5">&nbsp;</td>
<th width="100"><a href="employee_auth_group.php?session=<?=hh($session)?>">権限グループ</a></th>
<td width="5">&nbsp;</td>
<th width="100"><a href="employee_display_group.php?session=<?=hh($session)?>">表示グループ</a></th>
<td width="5">&nbsp;</td>
<th width="100"><a href="employee_menu_group.php?session=<?=hh($session)?>">メニューグループ</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_auth_bulk_setting.php?session=<?=hh($session)?>">一括設定</a></th>
<td width="5">&nbsp;</td>
<th width="80"><a href="employee_auth_list.php?session=<?=hh($session)?>">権限管理</a></th>
<td width="5">&nbsp;</td>
<th width="80" class="current"><a href="employee_field_option.php?session=<?=hh($session)?>">項目管理</a></th>
<td width="">&nbsp;</td>
</tr>
</table>



<div style="width:700px">
<form name="frm" method="post">
<input type="hidden" name="try_update" value="1" />

	<div><b>■連絡先：追加項目</b></div>

	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list prop_list">
		<tr>
			<th style="text-align:left">追加項目</th>
			<th style="text-align:left">項目利用</th>
			<th style="text-align:left">項目名</th>
			<th style="text-align:left">「パスワード・本人情報」での職員本人による変更許可</th>
		</tr>
		<? for ($idx=1; $idx<=5; $idx++) { ?>
		<?     $row = $fieldStructAuthRows["empmst_ext".$idx]; ?>
		<tr>
			<th>項目<?=$idx?></th>

			<td><label><input type="checkbox" name="disp_emp<?=$idx?>" value="all_ok"
				<?=($row["disp_emp"]=="all_ok"?" checked":"")?> />利用する</label></td>

			<td><input type="text" name="empmst_ext<?=$idx?>" value="<?=hh($fieldLabels["empmst_ext".$idx])?>" maxlength="50" /></td>

			<td><label><input type="checkbox" name="edit_emp<?=$idx?>" value="all_ok"
				<?=($row["edit_emp"]=="all_ok"?" checked":"")?> />変更可</label></td>
		</tr>
		<? } ?>
	</table>


	<div style="text-align:right; padding-top:2px"><button type="button" onclick="document.frm.submit()">登録</button></div>

</form>
</div>



</div>
</body>
</html>
