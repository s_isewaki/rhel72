<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$wardreg = check_authority($session, 14, $fname);
if ($wardreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$arr_input = array();
$arr_input["target"] = "目標値";
$arr_input["in_week"] = "入棟患者数平均（平日）";
$arr_input["in_sat"] = "入棟患者数平均（土曜）";
$arr_input["in_sun"] = "入棟患者数平均（日曜）";
$arr_input["out_week"] = "退棟患者数平均（平日）";
$arr_input["out_sat"] = "退棟患者数平均（土曜）";
$arr_input["out_sun"] = "退棟患者数平均（日曜）";

foreach ($arr_input as $input_name => $input_title) {
	for ($i = 1; $i <= 5; $i++) {
		$var_name = "$input_name$i";
		if (strlen($$var_name) > 0) {
			if (preg_match("/^[0-9]*$/", $$var_name) == 0) {
				echo("<script language=\"javascript\">alert(\"{$input_title}は半角数字で入力してください\");</script>\n");
				echo("<script language=\"javascript\">history.back();</script>\n");
				exit;
			}
		} else {
			$$var_name = 0;
		}
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクション開始
pg_query($con, "begin transaction");

$sql = "delete from bedconf";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$sql = "insert into bedconf (target_days1, target_days2, target_days3, target_days4, target_days5, in_weekday1, in_weekday2, in_weekday3, in_weekday4, in_weekday5, in_saturday1, in_saturday2, in_saturday3, in_saturday4, in_saturday5, in_sunday1, in_sunday2, in_sunday3, in_sunday4, in_sunday5, out_weekday1, out_weekday2, out_weekday3, out_weekday4, out_weekday5, out_saturday1, out_saturday2, out_saturday3, out_saturday4, out_saturday5, out_sunday1, out_sunday2, out_sunday3, out_sunday4, out_sunday5) values (";
$content = array($target1, $target2, $target3, $target4, $target5, $in_week1, $in_week2, $in_week3, $in_week4, $in_week5, $in_sat1, $in_sat2, $in_sat3, $in_sat4, $in_sat5, $in_sun1, $in_sun2, $in_sun3, $in_sun4, $in_sun5, $out_week1, $out_week2, $out_week3, $out_week4, $out_week5, $out_sat1, $out_sat2, $out_sat3, $out_sat4, $out_sat5, $out_sun1, $out_sun2, $out_sun3, $out_sun4, $out_sun5);
$in = insert_into_table($con, $sql, $content, $fname);
if ($in == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// 接続を閉じる
pg_close($con);

// 画面遷移
echo "<script language='javascript'>location.href = 'bed_date_target.php?session=$session';</script>";
?>
