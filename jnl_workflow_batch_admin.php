<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 一括表示閲覧権限設定</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("jnl_workflow_common.ini");
require("jnl_batch.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 81, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}




// データベースに接続
$con = connect2db($fname);


require("show_class_name.ini");

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);


if($jnl_upd_flg == "1")
{
	$test = "aaa";
	
	// 部門一覧を取得
	$sql = "select max(jnl_batch_admin_id) as maxcnt from jnl_batch_admin group by jnl_batch_admin_id";
	$cond = "";
	$sel_id = select_from_table($con, $sql, $cond, $fname);
	if ($sel_id == 0) 
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel_id) > 0) 
	{
		$cnt = (pg_fetch_result($sel_id, 0, "maxcnt") + 1);
	}
	else
	{
		//一件目
		$cnt = 1;
	}
	
	$sql = "insert into jnl_batch_admin (jnl_batch_admin_id, class_id, atrb_id, dept_id, room_id) values (";
	$content = array($cnt, $cls, $atrb, $dept, $room);
	$ins = insert_into_table($con, $sql, $content, $fname);
	
	if ($ins == 0) 
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	
}
else if($jnl_out_data != null)
{
	//バッチ処理実行
	exec_fuction_jnl($session,$con,$display_year,$display_month);
}
else if($jnl_del_flg != null)
{
	$sql = "delete from jnl_batch_admin";
	$cond = "where jnl_batch_admin_id = $jnl_del_flg";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//==============================
// （内部用）：部門一覧を取得する
//==============================

	// 部門一覧を取得
	$sql = "select * from classmst";
	$cond = "where class_del_flg = 'f' order by order_no";
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) 
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

//==============================
// （内部用）：課一覧を取得する
//==============================
// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) 
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//==============================
// （内部用）：科一覧を取得する
//==============================
// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) 
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//==============================
// （内部用）：室一覧を取得する
//==============================
// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) 
{
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) 
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
} else {
	$sel_room = null;
}


?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function setAttributeOptions(class_box, atrb_box, dept_box, room_box, atrb_id, dept_id, room_id) 
{
	delAllOptions(atrb_box);

	var class_id = class_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_atrb)) { 
	$ar_atrb[$wcnt]["atrb_id"] = $row["atrb_id"];
	$ar_atrb[$wcnt]["atrb_nm"] = $row["atrb_nm"];
	$wcnt++;
?>
	if (class_id == '<? echo $row["class_id"]; ?>') {
		addOption(atrb_box, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>', atrb_id);
	}
<? } ?>

	if (atrb_box.options.length == 0) {
		addOption(atrb_box, '0', '（未登録）');
	}
	
	setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id);
}


function delAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}


function setDepartmentOptions(atrb_box, dept_box, room_box, dept_id, room_id) {
	delAllOptions(dept_box);

	var atrb_id = atrb_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_dept)) { 
	
	$ar_dept[$wcnt]["dept_id"] = $row["dept_id"];
	$ar_dept[$wcnt]["dept_nm"] = $row["dept_nm"];
	$wcnt++;
	
	
?>
	if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
		addOption(dept_box, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>', dept_id);
	}
<? } ?>

	if (dept_box.options.length == 0) {
		addOption(dept_box, '0', '（未登録）');
	}

	setRoomOptions(dept_box, room_box, room_id);
}


function setRoomOptions(dept_box, room_box, room_id) {
	if (!room_box) {
		return;
	}

	delAllOptions(room_box);

	var dept_id = dept_box.value;
<? 
$wcnt = 0;
while ($row = pg_fetch_array($sel_room)) { 
	
	$ar_room[$wcnt]["room_id"] = $row["room_id"];
	$ar_room[$wcnt]["room_nm"] = $row["room_nm"];
	$wcnt++;
	
?>
	if (dept_id == '<? echo $row["dept_id"]; ?>') {
		if (room_box.options.length == 0) {
			addOption(room_box, '0', '　　　　　');
		}
		addOption(room_box, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>', room_id);
	}
<? } ?>

	if (room_box.options.length == 0) {
		addOption(room_box, '0', '（未登録）');
	}
	
}

</script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
table.list td {border:solid #35B341 1px;}
</style>
<style type="text/css">


.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#6090ef solid 1px;padding:1px;}
table.block3 td {border-width:1;}

table.block4 {border-collapse:collapse;}
table.block4 td {border:#32cd32 solid 1px;padding:1px;}
table.block4 td {border-width:1;}

p {margin:0;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<? show_wkfw_menuitem($session, $fname, ""); ?>
		<td></td>
		</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="main_form" action="jnl_workflow_batch_admin.php" method="post">

<?
//if($display_month == undefined)
//{
$systemDate = getDate();
$display_year = $systemDate[year];
$display_month = $systemDate[mon];
//}
//開始は2009年
$year_cnt = $display_year - 2008;



?>


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="block4">
	<tr height="22">
		<td colspan="2">
			<input type="button" value="データ出力" onclick="outData();">
								<select id='display_year' name='display_year'><?show_select_years($year_cnt, $display_year, false);?></select>年
								<select id='display_month' name='display_month'><?show_select_months($display_month, false);?></select>月
				
			</td>
	</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="50"><br>

<table width="100%" cellspacing="0" cellpadding="2"  class="block3">
<tr height="22">

	<td align="left" bgcolor="#f6f9ff">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（患者数総括一覧画面表示権限）所属<? echo($arr_class_name[0]); ?></font>
		<select name="cls" onchange="setAttributeOptions(this.form.cls, this.form.atrb, this.form.dept, this.form.room);">
			<option value=""></option>
			<?
			$wcnt = 0;
			while ($row = pg_fetch_array($sel_class)) 
			{
				
				$ar_cls[$wcnt]["cls_id"] = $row["class_id"];
				$ar_cls[$wcnt]["cls_nm"] = $row["class_nm"];
				$wcnt++;
	
				echo("<option value=\"{$row["class_id"]}\"");
				if ($row["class_id"] == $cls) {
					echo(" selected");
				}
	echo(">{$row["class_nm"]}</option>");
}
			?>
		</select>

		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[1]); ?></font>
		<select name="atrb" onchange="setDepartmentOptions(this.form.atrb, this.form.dept, this.form.room);"></select>
		
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[2]); ?></font>
		<select name="dept" onchange="setRoomOptions(this.form.dept, this.form.room);"></select>
		
		
		<? if ($arr_class_name["class_cnt"] == 4) { ?>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name[3]); ?></font>
			<select name="room"></select>
		<? } ?>
	</td>
</tr>
<tr height="22">
	<td align="left" class="block2" style="border-style:none;">
		<input type="button" value="設定" onclick="setData();">
		<input type="hidden" name="jnl_upd_flg" id="jnl_upd_flg" value="">
		<input type="hidden" name="jnl_del_flg" id="jnl_del_flg" value="">
		<input type="hidden" name="jnl_out_data" id="jnl_out_data" value="">
		<input type="hidden" name="session" value="<? echo($session); ?>">
	</td>
</tr>

</table>

<img src="img/spacer.gif" alt="" width="1" height="10"><br>


<?
// 部門一覧を取得
$sql = "select classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, aaa.*  from jnl_batch_admin aaa 
left join classmst on aaa.class_id=classmst.class_id
left join atrbmst on aaa.atrb_id=atrbmst.atrb_id
left join deptmst on aaa.dept_id=deptmst.dept_id
left join classroom on aaa.room_id=classroom.room_id
order by classmst.order_no, atrbmst.order_no, deptmst.order_no, classroom.order_no";
$cond = "";
$sel_id = select_from_table($con, $sql, $cond, $fname);
if ($sel_id == 0) 
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

if (pg_num_rows($sel_id) > 0) 
{
	$cnt = (pg_fetch_result($sel_id, 0, "maxcnt") + 1);
?>


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="block2">
<?
	while ($row = pg_fetch_array($sel_id)) 
	{
		$str_out = "";
		$str_out = $row["class_nm"]." > ".$row["atrb_nm"]." > ".$row["dept_nm"];
		if($row["room_nm"] != "")
		{
			$str_out .= " > ".$row["room_nm"];
		}
?>
	<tr height="22">
		<td><input type="button" value="削除" onclick="delData('<?echo($row["jnl_batch_admin_id"])?>');"></td><td><?echo($str_out)?></td>
	</tr>
<?
	}
?>
</table>

<?
}
?>

</form>

</td>
</tr>
</table>
</center>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>
</tr>
</table>


</body>
</html>
<script type="text/javascript">

function setData() 
{
	if(document.main_form.cls.value == "")
	{
		alert("組織を選択してください");
		return false;
	}
	document.main_form.jnl_upd_flg.value = "1";
	document.main_form.submit();
}

function delData(del_id) 
{
	ret = confirm("削除してもよろしいですか");
	if(ret == true)
	{
		document.main_form.jnl_del_flg.value = del_id;
		document.main_form.submit();
	}
}

function outData() 
{
	document.main_form.jnl_out_data.value = "action";
	document.main_form.submit();

}



</script>
