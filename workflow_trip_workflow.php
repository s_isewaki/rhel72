<?php
// 出張旅費
// ワークフロー設定
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

// ワークフローの取得
$sql = "SELECT * FROM trip_workflow RIGHT JOIN wkfwmst USING (wkfw_id) WHERE wkfw_del_flg='f' ORDER BY wkfw_type,wkfw_id";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | ワークフロー設定</title>
<script type="text/javascript" src="js/yui_2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/animation/animation-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/DDTableRow.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<form id="mstform" action="workflow_trip_workflow_submit.php" method="post">

<div class="button">
<button type="submit">更新</button>
</div>

<table id="msttable">
<thead>
<tr>
	<td>ワークフロー</td>
	<td>出張申請タイプ</td>
</tr>
</thead>
<tbody>
<?
while ($r = pg_fetch_assoc($sel)) {
?>
<tr>
	<td><?eh($r["wkfw_title"]);?><input name="wkfw_id[]" type="hidden" value="<?eh($r["wkfw_id"]);?>"/></td>
	<td>
		<label><input type="radio" name="type[<?eh($r["wkfw_id"]);?>]" value="0" <?($r["type"] == 0 or empty($r["type"])) ? e('checked') : '' ;?> />出張申請ではない</label>
		<label><input type="radio" name="type[<?eh($r["wkfw_id"]);?>]" value="1" <?($r["type"] == 1) ? e('checked') : '' ;?> />出張届</label>
		<label><input type="radio" name="type[<?eh($r["wkfw_id"]);?>]" value="2" <?($r["type"] == 2) ? e('checked') : '' ;?> />出張報告書</label>
	</td>
</tr>
<?}?>
</tbody>
</table>

<div class="button">
<button type="submit">更新</button>
</div>

<input type="hidden" name="session" value="<?=$session?>" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);