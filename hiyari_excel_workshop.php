<?php
//set_include_path(get_include_path() . PATH_SEPARATOR . './libs/phpexcel/Classes/');
include './libs/phpexcel/Classes/PHPExcel.php';
include './libs/phpexcel/Classes/PHPExcel/IOFactory.php';

// PHP Excel のライブラリ・クラスオブジェクトを扱うクラス 2012.1.11
// シートは1つのみ。複数シートを出力する際は改造してください。
class HiyariExcelWorkShop{
	private $xl;
	private $excelSheet;
	private $CellArea;
	private $borderStyleArrayTHICK;
	private $borderStyleArrayTHIN;
	private $borderType;
	private $defFont , $defFontSize , $defCharColor , 
		$defBackColor , $defBorderThick , $defBorderType , $defBorderColor , 
		$defVirticalPos , $defHorizonPos ,
		$defCellFormatStyle;
	public  $writer;
	// コンストラクタ
	function __construct(){
		// エクセルオブジェクト
		$this->xl = new PHPExcel();
		// エクセルオブジェクトにsheet1を設定
		$this->xl->setActiveSheetIndex(0);
		// シートオブジェクト
		$this->excelSheet = $this->xl->getActiveSheet();
		// 出力オブジェクト
		$this->writer = PHPExcel_IOFactory::createWriter($this->xl, "Excel5");
		// 既定値フォント
		$this->xl->getDefaultStyle()->getFont()->setName(mb_convert_encoding('ＭＳ ゴシック','UTF-8','EUC-JP'));		// 標準フォント設定
		$this->xl->getDefaultStyle()->getFont()->setSize(12); // 標準フォントサイズ12p
		$this->defBackColor = "FFFFFF" ; // セルの既定値色は白
		$this->defdefCharColor = "000000" ; // セルの既定値フォント色は黒、フォント書式はセル単位でしか扱えない
		// 枠線非表示。動的に切り替えたい場合はメソッドを追加してください
		$this->xl->getActiveSheet()->setShowGridlines(false);
	}

	// sheet名
	function SetSheetName( $_name ){
		$this->excelSheet->setTitle( $_name );
	}

	// セルに対して何かするときは必ずこのSetAreaを設定すること
	// セル修飾を規定値で使うときはSetValueWithAttrib()を使うこと
	function SetArea( $_cell_area ){
		$this->CellArea = $_cell_area;
	}

	// 数値で行、列を指定されたセルの取得
	function SetAreaCell( $_cell_area_column, $_cell_area_row ){
		$this->CellArea = $this->excelSheet->getCellByColumnAndRow($_cell_area_column, $_cell_area_row)->getCoordinate();
	}

	// デバッグ用
	function GetArea(){	
		return ($this->CellArea);
	}

	// セルへ値を設定
	// セル修飾を規定値で使うときはSetValueWithAttrib()を使うこと
	function SetValue( $_value ){
		$this->excelSheet->setCellValue( $this->CellArea , $_value );
	}

	// セルへ値を設定、EUC-JP日本語専用
	// 特に�´◆ΑΑΔ覆百殄佞�数字などEUC-JPで文字化けしそうな字を扱うときはこれを使うこと
	function SetValueJP( $_value ){
		$this->excelSheet->setCellValue( $this->CellArea , mb_convert_encoding(mb_convert_encoding($_value,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));
	}

	// 前に指定した修飾を有効としたまま値をセルへ設定する。性能が悪いので要注意
	// フォント、フォントサイズ、セル文字の色、背景色、セル内の縦位置、セル内の横位置
	function SetValueWithAttrib( $_cell_area , $_value ){ // セルの位置 , 値
		$this->CellArea=$_cell_area;
		$this->excelSheet->setCellValue( $_cell_area , $_value );
		$this->SetFont( $this->defFont , $this->defFontSize);
		$this->SetCharColor( $this->defCharColor );
		$this->SetBackColor( $this->defBackColor );
		$this->SetPosH( $this->defHorizonPos );
		$this->SetPosV( $this->defVirticalPos );
	}

	// フォント名とサイズ、セルへ反映
	function SetFont( $_type , $_size ){
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setName( $_type );
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFont()->setSize( $_size );
		$this->defFont = $_type;
		$this->defFontSize = $_size;
	}

	// フォント名とサイズ、指定だけ
	function SetFontDef( $_type , $_size ){
		$this->defFont = $_type;
		$this->defFontSize = $_size;
	}

	// 文字色。セル内の全文字が対象、セルへ反映
	function SetCharColor( $_color ){ // rrggbb
		$this->xl->getActiveSheet()->getStyle($this->CellArea)->getFont()->getColor()->setARGB( $_color );
		$this->defCharColor = $_color ;
	}

	// 文字色、指定だけ
	function SetCharColorDef( $_color ){ // rrggbb
		$this->defCharColor = $_color ;
	}

	// セルの色、セルへ反映
	// SetFillTypeを実行後にsetRGBしないと色がつかない
	function SetBackColor( $_color ){ // rrggbb
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->SetFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getFill()->getStartColor()->setRGB( $_color );
		$this->defBackColor = $_color;
	}

	// セルの色、指定だけ
	function SetBackColorDef( $_color ){ // aarrggbb
		$this->defBackColor = $_color;
	}

	// セルの書式設定、桁数など、セルへ反映
	function SetFormatStyle( $_style ){ // 'format style ex) 0.00 #,###,000 etc
		$this->xl->GetActiveSheet()->getStyle($this->CellArea)->getNumberFormat()->setFormatCode( $_style );
		$this->defCellFormatStyle = $_style;
	}

	// 罫線
	// THICK:太い MEDIUM:普通 THIN:細い , 色。色を省略したら黒 , outline:外周、all:範囲内縦横
	// 上下左右が必要なときは追加してください。
	function SetBorder( $_thick , $_color  , $_type ){ 

		if( $_color == "" ){
			$_color='FF000000';
		}
		switch ($_type){
		case 'outline': $this->borderType = $_type;
				break;
		case 'top': $this->borderType = $_type;
				break;
		default	      :	$this->borderType = 'allborders';
		}
		$this->borderStyleArrayTHICK = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_THICK,
					'color' => array('argb' => $_color),
				),
			),
		);
		$this->borderStyleArrayMEDIUM = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
					'color' => array('argb' => $_color),
				),
			),
		);
		$this->borderStyleArrayTHIN = array(
			'borders' => array(
				$this->borderType => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('argb' => $_color),
				),
			),
		);

		switch ($_thick){
		case 'THIN': 	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayTHIN);
				break;
		case 'MEDIUM': 	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayMEDIUM);
				break;
		default	   :	$this->excelSheet->getStyle($this->CellArea)->applyFromArray($this->borderStyleArrayTHICK);
		}
		$this->defBorderThick = $_thick;
		$this->defBorderType  = $_color;
		$this->defBorderColor = $_type;

	}

	// 縦位置、セルへ反映
	function SetPosV( $_v  ) {    // 'CENTER','TOP','BOTTOM'
		switch ( $_v ){
		case 'CENTER':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				break;
		case 'TOP':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				break;
		default   :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
		}
		$this->defVirticalPos = $_v;
	}

	// 横位置、セルへ反映
	function SetPosH( $_h ) {     // 'LEFT','CENTER','RIGHT'
		switch ( $_h ){
		case 'CENTER':	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				break;
		case 'LEFT'  :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				break;
		default	     :	$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		}
		$this->defHorizonPos = $_h;
	}

	// 縦位置、指定だけ
	function SetPosVDef( $_v  ) {    // 'CENTER','TOP','BOTTOM'
		$this->defVirticalPos = $_v;
	}

	// 横位置、指定だけ
	function SetPosHDef( $_h ) {     // 'LEFT','CENTER','RIGHT'
		$this->defHorizonPos = $_h;
	}

	// セル結合、指定済みの領域を結合
	function CellMergeReady(){
		$this->CellMerge($this->CellArea);
	}

	// セル結合、領域を指定して結合
	function CellMerge($_area){ // 
		$this->excelSheet->mergeCells($_area);
	}

	// セル内で折り返す
	function SetWrapText(){
		$this->xl->getActiveSheet()->getStyle($this->CellArea)->getAlignment()->setWrapText(true);
	}

	// 列幅 ※ Excel2003の場合、指定値に0.62を加算する
	function SetColDim( $_col , $_csize ){ // 列名、サイズ
		$this->xl->getActiveSheet()->getColumnDimension($_col)->setWidth($_csize);
	}

	// 行高さ
	function SetRowDim( $_row , $_rsize ){ // 行番号、サイズ
		$this->xl->getActiveSheet()->getRowDimension($_row)->setRowHeight($_rsize);
	}

	// ここからはブック全体の指定
	// 用紙サイズ、ここではA3,A4,B4を扱う、他のサイズが必要なときは追加してください。
	function PaperSize( $_name ){
		switch ( $_name ){
			case "A4":$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);break;
			case "B4":$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_B4);break;
			default	 :$this->xl->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);break; // 既定値
		}
	} 

	// 用紙方向
	function PageSetUp( $_name ){ // 縦virticalか横horizontialか
		if( $_name == 'virtical'){
			$this->xl->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetUp::ORIENTATION_PORTRAIT); // 縦
		}else{
			$this->xl->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetUp::ORIENTATION_LANDSCAPE); // 横
		}
	}

	// 拡大縮小と倍率
	function PageRatio( $_name , $_value){ // 'USER' 'AUTO', 縮小拡大倍率。'AUTO'のときは倍率$_valueは無視される
		switch ($_name){
			case "USER":	$this->xl->getActiveSheet()->getPageSetup()->setFitToPage(false); // 倍率指定のほうへチェック
					$this->xl->getActiveSheet()->getPageSetup()->setScale($_value);
					break;
			default	   :	$this->xl->getActiveSheet()->getPageSetup()->setFitToPage(true); // 倍率を縦横に合わせて自動設定
					$this->xl->getActiveSheet()->getPageSetup()->setFitToWidth(1);   // 合わせるページは縦横ともに1
					$this->xl->getActiveSheet()->getPageSetup()->setFitToHeight(1);
		}
	}

	// 余白、インチ単位で指定
	function SetMargin( $_left , $_right , $_top , $_bottom , $_head , $_foot){ // 左右上下頭足
		// 日本語版Excelではメートル法で表示される。計算で変換する。1in=25.4mm
		$this->xl->getActiveSheet()->getPageMargins()->setHeader($_head / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setFooter($_foot / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setTop($_top / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setRight($_right / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setLeft($_left / 2.54);
		$this->xl->getActiveSheet()->getPageMargins()->setBottom($_bottom / 2.54);
	}

	// ダウンロード形式で出力
	function OutPut(){
		$this->writer->save('php://output');
	}
}