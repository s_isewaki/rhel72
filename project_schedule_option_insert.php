<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$time_chart_start2 = "$time_chart_start_hour$time_chart_start_min";
$time_chart_end2 = "$time_chart_end_hour$time_chart_end_min";
if ($time_chart_start2 >= $time_chart_end2) {
	echo("<script type=\"text/javascript\">alert('タイムチャート表示時間が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($con);

// オプション設定情報を更新
$sql = "update option set";
$set = array("calendar_start2", "time_chart_start2", "time_chart_end2");
$setvalue = array($calendar_start, $time_chart_start2, $time_chart_end2);
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// オプション画面を再表示
echo("<script type=\"text/javascript\">location.href = 'project_schedule_option.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg';</script>");
?>
