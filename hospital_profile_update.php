<?php

require_once("about_comedix.php");

define("IMAGETYPE_GIF", 1);
define("IMAGETYPE_JPEG", 2);
define("IMAGETYPE_PNG", 3);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 組織プロフィール権限のチェック
$auth = check_authority($session, 37, $fname);
if ($auth == "0") {
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//----- 入力チェック -----

switch ($type) {
    case "1":
        $name_label = "医療機関名称";
        $org_cd_label = "医療機関コード";
        break;
    case "2":
        $name_label = "企業名";
        $org_cd_label = "契約番号";
        break;
    case "3":
        $name_label = "法人名";
        $org_cd_label = "契約番号";
        break;
}

//// 医療機関名称／企業名
$name_len = strlen($name);
if ($name_len == 0) {
    echo("<script language='javascript'>alert('{$name_label}を入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
else if ($name_len > 100) {
    echo("<script language='javascript'>alert('{$name_label}が長すぎます');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 郵便番号
if (preg_match("/^\d{3}$/", $zip1) == 0 || preg_match("/^\d{4}$/", $zip2) == 0) {
    echo("<script language='javascript'>alert('郵便番号を半角数字7桁で入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 住所1
$addr1_len = strlen($addr1);
if ($addr1_len == 0) {
    echo("<script language='javascript'>alert('住所1を入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
else if ($addr1_len > 100) {
    echo("<script language='javascript'>alert('住所1が長すぎます');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 住所2
if (strlen($addr2) > 100) {
    echo("<script language='javascript'>alert('住所2が長すぎます');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 電話番号
if (preg_match("/^\d{1,6}$/", $tel1) == 0 || preg_match("/^\d{1,6}$/", $tel2) == 0 || preg_match("/^\d{1,6}$/", $tel3) == 0) {
    echo("<script language='javascript'>alert('電話番号を半角数字で入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// FAX
if (
    ($fax1 != "" && preg_match("/^\d{1,6}$/", $fax1) == 0) || ($fax2 != "" && preg_match("/^\d{1,6}$/", $fax2) == 0) || ($fax3 != "" && preg_match("/^\d{1,6}$/", $fax3) == 0)
) {
    echo("<script language='javascript'>alert('FAXを半角数字で入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 医療機関コード／契約番号
if (preg_match("/^\d{10}$/", $org_cd) == 0) {
    echo("<script language='javascript'>alert('{$org_cd_label}を半角数字10桁で入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

//// 病床数
if ($type == "1") {
    if (preg_match("/^\d{1,4}$/", $bed_cnt) == 0) {
        echo("<script language='javascript'>alert('病床数を半角数字4桁以内で入力してください');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
}

//// ロゴ画像
$logo_name_len = strlen($logo_name);
if ($logo_name_len > 0) {
    $logo_info = @getimagesize($logo);
    if ($logo_info === false) {
        $logo_format = "";
    }
    else {
        $logo_format = $logo_info[2];
    }

    if ($logo_format != IMAGETYPE_GIF && $logo_format != IMAGETYPE_JPEG && $logo_format != IMAGETYPE_PNG) {
        echo("<script language='javascript'>alert('ロゴ画像として登録できるのはGIF、JPEG、PNGのみです');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
}

//// URL
if (strlen($url) > 120) {
    echo("<script language='javascript'>alert('URLが長すぎます');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}
else if ($url != "" && substr($url, 0, 4) != "http") {
    echo("<script language='javascript'>alert('「http://」から始まるURLを入力してください');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin transaction");

// 既存プロフィールレコードを削除
$sql = "delete from profile";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

// プロフィールレコードを作成
if ($type == 1) {
    $sql = "insert into profile (prf_name, prf_zip1, prf_zip2, prf_prv, prf_addr1, prf_addr2, prf_tel1, prf_tel2, prf_tel3, prf_fax1, prf_fax2, prf_fax3, prf_org_cd, prf_bed_cnt, prf_url, prf_logo_type, prf_type) values (";
    $content = array($name, $zip1, $zip2, $prv, $addr1, $addr2, $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, $org_cd, $bed_cnt, $url, $logo_type, $type);
}
else {
    $sql = "insert into profile (prf_name, prf_zip1, prf_zip2, prf_prv, prf_addr1, prf_addr2, prf_tel1, prf_tel2, prf_tel3, prf_fax1, prf_fax2, prf_fax3, prf_org_cd, prf_url, prf_logo_type, prf_type) values (";
    $content = array($name, $zip1, $zip2, $prv, $addr1, $addr2, $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, $org_cd, $url, $logo_type, $type);
}
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続の切断
pg_close($con);

//---------- ロゴファイルの保存 ----------

if ($logo_name_len > 0) {

    //// ファイル名の作成
    switch ($logo_format) {
        case IMAGETYPE_GIF:
            $file_name = "logo.gif";
            $del_files = array("logo.jpg", "logo.png");
            break;
        case IMAGETYPE_JPEG:
            $file_name = "logo.jpg";
            $del_files = array("logo.gif", "logo.png");
            break;
        case IMAGETYPE_PNG:
            $file_name = "logo.png";
            $del_files = array("logo.gif", "logo.jpg");
            break;
    }

    //// 保存先ディレクトリがなければ作成
    if (!is_dir("profile")) {
        mkdir("profile", 0755);
    }
    if (!is_dir("profile/img")) {
        mkdir("profile/img", 0755);
    }

    //// ファイルの保存
    copy($logo, "profile/img/$file_name");

    //// 不要ファイルの削除
    foreach ($del_files as $del_file) {
        if (is_file("profile/img/$del_file")) {
            unlink("profile/img/$del_file");
        }
    }
}

// 全画面をリフレッシュ
$f_url = urlencode("hospital_profile_menu.php?session=$session");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
