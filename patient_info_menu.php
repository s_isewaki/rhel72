<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理 | 検索</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_patient_list.ini"); ?>
<? require("show_patient_next.ini"); ?>
<? require("label_by_profile_type.ini"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$ptif = check_authority($session, 15, $fname);
$inout = check_authority($session, 33, $fname);
$dishist = check_authority($session, 34, $fname);
$outreg = check_authority($session, 35, $fname);
$disreg = check_authority($session, 36, $fname);
$ptreg = check_authority($session, 22, $fname);
if ($ptif == "0" && $inout == "0" && $dishist == "0" && $outreg == "0" && $disreg == "0" && $ptreg == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$ptauth = array("ptif" => $ptif, "inout" => $inout, "dishist" => $dishist, "outreg" => $outreg, "disreg" => $disreg, "ptreg" => $ptreg);

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function deletePatient() {
	if (document.del.elements['del_pt[]'] == undefined) {
		alert('削除可能な<? echo($_label_by_profile["PATIENT"][$profile_type]); ?>が存在しません');
		return;
	}

	if (document.del.elements['del_pt[]'].length == undefined) {
		if (!document.del.elements['del_pt[]'].checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.del.elements['del_pt[]'].length; i < j; i++) {
			if (document.del.elements['del_pt[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	}

	if (confirm('削除してよろしいですか？')) {
		document.del.key1.value = document.srch.key1.value;
		document.del.key2.value = document.srch.key2.value;
		document.del.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#5279a5"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>検索</b></font></a></td>
<? if ($ptreg == 1) { ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7">
<a href="patient_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7">
<a href="patient_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="patient_waiting_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">待機患者一覧</font></a></td>
<td width="">&nbsp;</td>
<td align="center"><? show_next($con,$session,$key1,$key2,$page,$fname); ?></td>
<td width="50" align="right"><? if ($ptreg == 1) { ?><input type="button" value="削除" onclick="deletePatient();"><? } ?></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="srch" action="patient_info_menu.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font><input type="text" name="key1" maxlength="15" value="<? echo($key1); ?>" style="ime-mode: inactive;">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font><input type="text" name="key2" maxlength="15" value="<? echo($key2); ?>" style="ime-mode: active;">&nbsp;<input type="submit" value="検索"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="del" action="patient_delete.php" method="post">
<? show_patient_list($con, $ptauth, $session, $key1, $key2, $page, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="key1">
<input type="hidden" name="key2">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
