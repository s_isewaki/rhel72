<?php
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = ($mmode != "usr") ? 42 : 5;
	break;
default:
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (is_array($start_hour)) {
	for ($i = 0; $i < count($start_hour); $i++) {
		if (($start_hour[$i] == "--" && $start_min[$i] != "--") || ($start_hour[$i] != "--" && $start_min[$i] == "--") || ($end_hour[$i] == "--" && $end_min[$i] != "--") || ($end_hour[$i] != "--" && $end_min[$i] == "--")) {
			echo("<script type=\"text/javascript\">alert('時と分の一方のみは登録できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		if (($start_hour[$i] != "--" && $end_hour[$i] == "--") || ($start_hour[$i] == "--" && $end_hour[$i] != "--")) {
			echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻の一方のみは登録できません。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		if ($start_hour[$i] != "--" && $start_hour[$i] == $end_hour[$i] && $start_min[$i] == $end_min[$i]) {
			echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻が同じです。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
	}
}

// データベースに接続
$con = connect2db($fname);

// ************ oose add start *****************
//職員本人による修正可否フラグを取得
$sql = "select * from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
// 本人による修正が可か確認する 20141027
$timecard_modify_plan_flg = pg_fetch_result($sel, 0, "modify_plan_flg");
//現在日付の取得
$wk_date = getdate();
$now_yyyy = $wk_date["year"];
$now_mm = $wk_date["mon"];
$now_dd = $wk_date["mday"];
$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm, $now_dd);
// ************ oose add end *****************

// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

// 本人による修正が可か確認する 20141027
if (($wherefrom == "1" && $timecard_modify_plan_flg == "t") || $wherefrom == "2") {
	// 期間をループ
	for ($i = 0; $i < count($date); $i++) {

		// 処理日付
		$tmp_date = $date[$i];
		// 出勤予定を登録
		$tmp_pattern = $pattern[$i];
		if ($tmp_pattern == "--") {
			$tmp_pattern = "";
		}
		$tmp_reason = $reason[$i];
		if ($tmp_reason == "--") {
			$tmp_reason = "";
		}
		$tmp_night_duty = $night_duty[$i];
		if ($tmp_night_duty == "--") {
			$tmp_night_duty = "";
		}
		$tmp_start_time = ($start_hour[$i] != "--") ? $start_hour[$i] . $start_min[$i] : "";
		$tmp_end_time = ($end_hour[$i] != "--") ? $end_hour[$i] . $end_min[$i] : "";
		$tmp_allow_id = ($allow_ids[$i] == "--") ? NULL : $allow_ids[$i];

		$tmp_list_tmcd_group_id = $list_tmcd_group_id[$i];
		if ($tmp_list_tmcd_group_id == "--") {
			$tmp_list_tmcd_group_id = "";
		}

		pg_query($con, "begin transaction");

		$sql = "select * from atdbk";
		$cond = "where emp_id = '$emp_id' and date = '$tmp_date' ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$num = pg_num_rows($sel);

		//既にある場合、更新
		if ($num == 1) {
			$row = pg_fetch_array($sel);
			
			$old_pattern = $row["pattern"];
			$old_reason = $row["reason"];
			$old_night_duty = $row["night_duty"];
			$old_allow_id = $row["allow_id"];
			$old_prov_start_time = $row["prov_start_time"];
			$old_prov_end_time = $row["prov_end_time"];
			$old_tmcd_group_id = $row["tmcd_group_id"];
			//変更確認
			if ($old_pattern != $tmp_pattern ||
				$old_reason != $tmp_reason ||
				$old_night_duty != $tmp_night_duty ||
				$old_allow_id != $tmp_allow_id ||
				$old_prov_start_time != $tmp_start_time ||
				$old_prov_end_time != $tmp_end_time ||
				$old_tmcd_group_id != $tmp_list_tmcd_group_id) {
				
				$sql = "update atdbk set";
				$set = array("pattern", "reason", "night_duty", "allow_id", "prov_start_time", "prov_end_time", "tmcd_group_id");
				$setvalue = array($tmp_pattern, $tmp_reason, $tmp_night_duty, $tmp_allow_id, $tmp_start_time, $tmp_end_time, $tmp_list_tmcd_group_id);
				$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
			}
		}
		//ない場合、追加
		else {
			$sql = "insert into atdbk (emp_id, date, pattern, reason, night_duty, allow_id, prov_start_time, prov_end_time, tmcd_group_id) values (";
			$content = array($emp_id, $tmp_date, $tmp_pattern, $tmp_reason, $tmp_night_duty, $tmp_allow_id, $tmp_start_time, $tmp_end_time, $tmp_list_tmcd_group_id);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				$error = pg_last_error($con);
				//既にある場合
				if (strpos($error, "duplicate")) {
					pg_query($con, "rollback");
					pg_query($con, "begin transaction");

					$sql = "update atdbk set";
					$set = array("pattern", "reason", "night_duty", "allow_id", "prov_start_time", "prov_end_time", "tmcd_group_id");
					$setvalue = array($tmp_pattern, $tmp_reason, $tmp_night_duty, $tmp_allow_id, $tmp_start_time, $tmp_end_time, $tmp_list_tmcd_group_id);
					$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				} else {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
			}
		}
		pg_query($con, "commit");

	}
} //本人による修正が可か確認する 20141027


// データベース接続を切断
pg_close($con);

// 画面を再表示
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	echo("<script type=\"text/javascript\">location.href = 'atdbk_register.php?session=$session&yyyymm=$yyyymm';</script>");
	break;
case "2":  // タイムカード修正画面より
	$url_srch_name = urlencode($srch_name);
        echo("<script type=\"text/javascript\">location.href = 'work_admin_atdbk.php?session=$session&emp_id=$emp_id&yyyymm=$yyyymm&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&page=$page&group_id=$group_id&shift_group_id=$shift_group_id&csv_layout_id=$csv_layout_id&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&srch_id=$srch_id&sus_flg=$sus_flg&mmode=$mmode';</script>");
	break;
}
?>
