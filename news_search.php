<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("get_values.ini");
require_once("show_news_search.ini");
require_once("referer_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ページ番号の設定
if ($page == "") {$page = 1;}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// カテゴリ一覧を取得
//$category_list = get_category_list_from_db($con, $fname);
$category_list_db = get_category_list_from_db($con, $fname);
$category_list = array();
foreach ($category_list_db as $category_id => $category_name) {
    $category_list[$category_id] = $category_name["newscate_name"];
}

// 部門一覧を取得
$class_list = get_class_list($con, $fname);
$atrb_list = get_atrb_list($con, $fname);
$dept_list = get_dept_list($con, $fname);
$room_list = get_room_list($con, $fname);

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

// 通達区分一覧を取得
$notice_list = get_notice_list($con, $fname);
$notice2_list = get_notice2_list($con, $fname);

// 登録者名を取得
$emp_name = ($srch_emp_id != "") ? get_emp_kanji_name($con, $srch_emp_id, $fname) : "";

// 発信者名を取得
$src_emp_nm = ($src_emp_id != "") ? get_emp_kanji_name($con, $src_emp_id, $fname) : "";

// 役職一覧を取得
$st_list = get_st_list($con, $fname);

// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

// お知らせ集計機能設定を取得
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if($setting_henshin != "t"){
	$setting_henshin = "f";
}
if($setting_aggregate != "t"){
	$setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | お知らせ検索</title>
<? include_js("js/fontsize.js"); ?>
<? include_js("js/jquery/jquery-1.9.1.min.js"); ?>
<script type="text/javascript">
function initPage() {
	showSubCategory();
	setSrcAtrbOptions('<? echo($srch_src_atrb_id); ?>', '<? echo($srch_src_dept_id); ?>', '<? echo($srch_src_room_id); ?>');
}

function showSubCategory() {
	switch (document.mainform.srch_news_category.value) {
	case '0':  // 未選択
	case '1':  // 全館
		document.mainform.srch_class_id.style.display = 'none';
		document.mainform.srch_job_id.style.display = 'none';
		break;
	case '2':  // 部門
		document.mainform.srch_class_id.style.display = '';
		document.mainform.srch_job_id.style.display = 'none';
		break;
	case '3':  // 職種
		document.mainform.srch_class_id.style.display = 'none';
		document.mainform.srch_job_id.style.display = '';
		break;
	}
}

function setSrcAtrbOptions(atrb_id, dept_id, room_id) {
	var atrb_elm = document.mainform.srch_src_atrb_id;
	deleteAllOptions(atrb_elm);
	addOption(atrb_elm, '0', '　　　', atrb_id);

	var class_id = document.mainform.srch_src_class_id.value;
	switch (class_id) {
<?
foreach ($atrb_list as $tmp_class_id => $atrbs_in_class) {
	echo("\tcase '$tmp_class_id':\n");
	foreach ($atrbs_in_class as $tmp_atrb_id => $tmp_atrb_nm) {
		echo("\t\taddOption(atrb_elm, '$tmp_atrb_id', '$tmp_atrb_nm', atrb_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcDeptOptions(dept_id, room_id);
}

function setSrcDeptOptions(dept_id, room_id) {
	var dept_elm = document.mainform.srch_src_dept_id;
	deleteAllOptions(dept_elm);
	addOption(dept_elm, '0', '　　　', dept_id);

	var atrb_id = document.mainform.srch_src_atrb_id.value;
	switch (atrb_id) {
<?
foreach ($dept_list as $tmp_atrb_id => $depts_in_atrb) {
	echo("\tcase '$tmp_atrb_id':\n");
	foreach ($depts_in_atrb as $tmp_dept_id => $tmp_dept_nm) {
		echo("\t\taddOption(dept_elm, '$tmp_dept_id', '$tmp_dept_nm', dept_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}

	setSrcRoomOptions(room_id);
}

function setSrcRoomOptions(room_id) {
<? if ($arr_class_name['class_cnt'] != 4) { ?>
	return;
<? } ?>

	var room_elm = document.mainform.srch_src_room_id;
	deleteAllOptions(room_elm);
	addOption(room_elm, '0', '　　　', room_id);

	var dept_id = document.mainform.srch_src_dept_id.value;
	switch (dept_id) {
<?
foreach ($room_list as $tmp_dept_id => $rooms_in_dept) {
	echo("\tcase '$tmp_dept_id':\n");
	foreach ($rooms_in_dept as $tmp_room_id => $tmp_room_nm) {
		echo("\t\taddOption(room_elm, '$tmp_room_id', '$tmp_room_nm', room_id);\n");
	}
	echo("\t\tbreak;\n");
}
?>
	}
}

function selectEmployee() {
	window.open('news_employee_select.php?session=<? echo($session); ?>&class_id='.concat(document.mainform.srch_src_class_id.value), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	if (selected == value) {
		box.options[box.length - 1].selected = true;
	}
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function downloadCSV() {
	var original_action = document.mainform.action;
	var original_target = document.mainform.target;
	document.mainform.action = 'news_search_csv.php';
	document.mainform.target = 'download';
	document.mainform.submit();

	document.mainform.action = original_action;
	document.mainform.target = original_target;
}

$(function() {
	$('#check').click(function() {
		$('input.delete').prop('checked', $(this).prop('checked'));
	});
});

function delNews() {
	if (confirm('ゴミ箱に移動します。よろしいですか？')) {
		document.mainform.action = 'news_delete.php';
		document.mainform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<? echo($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>お知らせ検索</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
<?php //オプション機能で集計表を使用するにチェックを入れることでタブに集計表が表示される ?>
<?php if($setting_aggregate == 't'){?>
	<td width="70" align="center" bgcolor="#bdd1e7">
		<a href="news_aggregate.php?session=<? echo($session); ?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font>
		</a>
	</td>
<?php }?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10">

<form name="mainform" action="news_search.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="25%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="srch_news_title" value="<? echo($srch_news_title); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td><input type="text" name="srch_news" value="<? echo($srch_news); ?>" size="50" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
<td><select name="srch_news_category" onchange="showSubCategory();"><? show_options($category_list, $srch_news_category, true); ?></select>&nbsp;<select name="srch_class_id" style="display:none;"><? show_options($class_list, $srch_class_id); ?></select><select name="srch_job_id" style="display:none;"><? show_options($job_list, $srch_job_id); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象役職</font></td>
<td><select name="srch_st_id"><? show_options($st_list, $srch_st_id, true); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<td><select name="srch_notice_id"><? show_options($notice_list, $srch_notice_id, true); ?></select></td>
</tr>
<? if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分</font></td>
<td colspan="3"><select name="srch_notice2_id"><? show_options($notice2_list, $srch_notice2_id, true); ?></select></td>
</tr>
<? }?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信部署</font></td>
<td><select name="srch_src_class_id" onchange="setSrcAtrbOptions();"><? show_options($class_list, $srch_src_class_id, true); ?></select><select name="srch_src_atrb_id" onchange="setSrcDeptOptions();"></select><select name="srch_src_dept_id" onchange="setSrcRoomOptions();"></select><? if ($arr_class_name['class_cnt'] == 4) { ?><select name="srch_src_room_id"></select><? } ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="selectEmployee();">発信者</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" name="src_emp_nm" size="30" value="<? echo($src_emp_nm); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.src_emp_nm.value = ''; document.mainform.src_emp_id.value = ''; document.mainform.hid_src_emp_nm.value = '';">クリア</a>
<input type="hidden" name="src_emp_id" value="<? echo($src_emp_id); ?>">
<input type="hidden" name="hid_src_emp_nm" value="<? echo($src_emp_nm); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date_from1"><? show_select_years(10, $srch_news_date_from1, true); ?></select>/<select name="srch_news_date_from2"><? show_select_months($srch_news_date_from2, true); ?></select>/<select name="srch_news_date_from3"><? show_select_days($srch_news_date_from3, true); ?></select> 〜 <select name="srch_news_date_to1"><? show_select_years(10, $srch_news_date_to1, true); ?></select>/<select name="srch_news_date_to2"><? show_select_months($srch_news_date_to2, true); ?></select>/<select name="srch_news_date_to3"><? show_select_days($srch_news_date_to3, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載日（ログイン画面）</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date_login_from1"><? show_select_years(10, $srch_news_date_login_from1, true); ?></select>/<select name="srch_news_date_login_from2"><? show_select_months($srch_news_date_login_from2, true); ?></select>/<select name="srch_news_date_login_from3"><? show_select_days($srch_news_date_login_from3, true); ?></select> 〜 <select name="srch_news_date_login_to1"><? show_select_years(10, $srch_news_date_login_to1, true); ?></select>/<select name="srch_news_date_login_to2"><? show_select_months($srch_news_date_login_to2, true); ?></select>/<select name="srch_news_date_login_to3"><? show_select_days($srch_news_date_login_to3, true); ?></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('news_member_list.php?session=<? echo($session); ?>&from_page_id=1', 'newwin', 'width=640,height=480,scrollbars=yes')">登録者</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" name="emp_name" size="30" value="<? echo($emp_name); ?>" disabled>
<a href="javascript:void(0);" onclick="document.mainform.emp_name.value = ''; document.mainform.srch_emp_id.value = '';">クリア</a>
<input type="hidden" name="srch_emp_id" value="<? echo($srch_emp_id); ?>">
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="srch_news_date1"><? show_select_years(10, $srch_news_date1, true); ?></select>/<select name="srch_news_date2"><? show_select_months($srch_news_date2, true); ?></select>/<select name="srch_news_date3"><? show_select_days($srch_news_date3, true); ?></select></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="CSVダウンロード" onclick="downloadCSV();"> <input type="submit" value="検索"></td>
</tr>
</table>
<? if ($srch_flg == "t") { ?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" value="ゴミ箱に移動" onclick="delNews();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="25" align="center"><input type="checkbox" id="check"></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象カテゴリ</font></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分</font></td>
<? if (!empty($notice2_list)) {?>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ<br>区分</font></td>
<? } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発信者</font></td>
<td width="175" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間<br />（掲載期間：ログイン画面）</font></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
<?php if($setting_henshin == 't'){?>
	<td width="55" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">返信一覧</font></td>
<? } ?>
</tr>
<? show_news_search_result_list($con, $page, $class_called, $session, $fname, $notice2_list, $setting_henshin); ?>
</table>
<? show_news_search_page_list($con, $page, $session, $fname); ?>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="t">
<input type="hidden" name="frompage" value="2">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</body>
<? pg_close($con); ?>
</html>
