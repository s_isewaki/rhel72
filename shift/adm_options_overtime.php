<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$warning_time = $conf->get('shift.warning_time');
$warning_message = $conf->get('shift.warning_message');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
//$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($warning_message === '') {
    $warning_message = "今月の残業時間が規定を超えています。";
}

if ($mode === 'confirm') {
    $warning_time = $_POST["warning_time"];
    $warning_message = $_POST["warning_message"];

    // 警告時間
    if ($warning_time and preg_match("/\D/", $warning_time)) {
        $err["warning_time"] = '異常な値です';
    }
    // 警告メッセージ
    if ($warning_message === '' && $warning_time !== '') {
        $err["warning_message"] = '「警告時間」を入力した時は必須です';
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $conf->set('shift.warning_time', $warning_time);
    $conf->set('shift.warning_message', $warning_message);

    header("Location: " . CMX_BASE_URL . "/shift/adm_options_overtime.php");
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);
$view->assign('err', $err);
$view->assign('warning_time', $warning_time);
$view->assign('warning_message', $warning_message);
$view->display('shift/adm_options_overtime.tpl');
