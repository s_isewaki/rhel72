<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Personal.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Model/Shift/Pattern.php';
require_once 'Cmx/Model/Timecard/Overtime.php';
require_once 'Cmx/Model/Timecard/OvertimeReason.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$date_ymd = $_REQUEST["date"];
$mode = $_POST["mode"];
$submit = $_POST["submit_status"];

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();

//----------------------------------------------------------
// YYYY-MM-DD
//----------------------------------------------------------
$date = preg_replace('/^(\d\d\d\d)(\d\d)(\d\d)/', '$1-$2-$3', $date_ymd);

//----------------------------------------------------------
// 残業入力期限
//----------------------------------------------------------
$zangyo_limit_days = $conf->get('shift.zangyo_limit_days');
if ($zangyo_limit_days) {
    $zan_limit = date('Ymd', strtotime(date('Y-m-d')) - ($zangyo_limit_days * 86400));
    if ($date_ymd >= $zan_limit) {
        $is_overtime = true;
    }
    else {
        $is_overtime = false;
    }
}
else {
    $is_overtime = true;
}

//----------------------------------------------------------
// 残業時間警告
//----------------------------------------------------------
$warning_time = $conf->get('shift.warning_time');
$warning_message = $conf->get('shift.warning_message');

//----------------------------------------------------------
// 個人
//----------------------------------------------------------
$person = new Cmx_Shift_Personal($emp->emp_id());
$person->set_login_id($emp->emp_id());
$person->table($date_ymd);
$table = $person->get_table();

//----------------------------------------------------------
// 残業合計
//----------------------------------------------------------
list($lists, $total) = $person->lists(substr($date_ymd, 0, 4), substr($date_ymd, 4, 2), 'month');
if ($total['overtime'] > $warning_time * 60) {
    $is_overtime_warn = 1;
}

//----------------------------------------------------------
// 実績変更可
//----------------------------------------------------------
$ward = $person->get_ward();
$is_result_edit = $ward->result_edit_flg();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = $person->get_staff();

//----------------------------------------------------------
// 時短対象
//----------------------------------------------------------
$is_shorter = $staff->is_shorter($date_ymd);

//----------------------------------------------------------
// 入力内容チェック
//----------------------------------------------------------
if ($mode === 'confirm') {
    $err = $person->validate_results($_POST);
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $person->save_results($date_ymd, $emp->emp_id(), $_POST);

    $view = new Cmx_View();
    $view->display('shift/personal_edit_close.tpl');
    exit;
}

//----------------------------------------------------------
// 勤務実績
//----------------------------------------------------------
if (empty($err)) {
    $results = $table['results'];
}
else {
    $results = $_POST;
    $results['pattern'] = (int)substr($_POST['ptn'], 0, 2);
    $results['reason'] = (int)substr($_POST['ptn'], 2, 2);

    // 残業詰める
    $results['over'] = array();
    foreach ($_POST["over"] as $over) {
        if ($over['over_start_hour']) {
            $results['over'][] = $over;
        }
    }
}

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern($results['tmcd_group_id']);
$pattern_hash = $pattern->ptn4_hash();
$results['group_name'] = $ward->group_name();
if ($results['ptn'] && $results['ptn'] !== '0000') {
    $results['pattern_name'] = $pattern_hash[$results['ptn']]->ptn_name();
}

//----------------------------------------------------------
// 残業理由
//----------------------------------------------------------
$ovtm_reason = new Cmx_Timecard_OvertimeReason();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 実績変更可
$view->assign('is_result_edit', $is_result_edit);

// 残業入力期限
$view->assign('is_overtime', $is_overtime);

// 残業警告
$view->assign('is_overtime_warn', $is_overtime_warn);
$view->assign('warning_message', $warning_message);

// 時短対象
$view->assign('is_shorter', $is_shorter);

// プルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $pattern->sign_lists());

// 勤務パターングループリスト
$view->assign('pgroup_lists', $pattern->grp_lists());

// 残業理由
$view->assign('ovtm_reason_lists', $ovtm_reason->lists($results['tmcd_group_id']));

// 時間、分
$view->assign('hours24', pulldown_24hours());
$view->assign('hours48', pulldown_48hours());
$view->assign('minutes', range(0, 59));
$view->assign('fifteen_minutes', range(0, 45, 15));

// 個人勤務データ
$view->assign('emp_id', $staff->emp_id());
$view->assign('date', $date_ymd);
$view->assign('emp', $table['emp']);
$view->assign('results', $results);
$view->assign('update_emp_id', new Cmx_Shift_Staff($table['results']['update_emp_id']));

// エラー
$view->assign('err', $err);

$view->display('shift/personal_edit_result.tpl');
