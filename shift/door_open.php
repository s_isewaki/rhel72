<?php

/*
 * 電気錠システム連携
 */

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Shift/DutyDate.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/Shift/Log.php';

// 勤務日object
$dutydate = new Cmx_Shift_DutyDate();

// データベース障害
if (!$dutydate->is_connect()) {
    echo('10');
    exit;
}

// 作業ログ
$dutydate->beginTransaction();
$log = $dutydate->log();

// 空電文
if ($_POST['data'] === '') {
    $log->log('alived');
    echo('00');
    exit;
}

// CSVチェック(カンマが含まれていない)
if (strpos($_POST['data'], ",") === false) {
    $log->log('illegal data', $_POST['data']);
    echo('11');
    exit;
}

// 行処理
$data = explode("\n", $_POST['data']);
$error = false;
$carryover = array();
foreach ($data as $row) {
    $row = rtrim($row);

    if (empty($row)) {
        continue;
    }

    list($emp_id, $date) = explode(",", $row, 2);

    if ($emp_id === '' || $date === '') {
        $error = true;
        break;
    }

    if (!$dutydate->is_date($date)) {
        $error = true;
        break;
    }

    $return = $dutydate->dakoku($emp_id, $date, 'door');
    if (!$return) {
        $dutydate->rollback();
        $log->log('db data error', $row);
        echo('10');
        exit;
    }

    $carryover[] = $row;
}

if ($error) {
    $dutydate->rollback();
    $log->log('illegal data', $_POST['data']);
    echo('11');
    exit;
}

$exec = $dutydate->exec();
if (!$exec) {
    $dutydate->rollback();
    $log->log('exec error');
    echo('10');
    exit;
}

// 打刻処理
foreach ($carryover as $row) {
    list($emp_id, $date) = explode(",", $row, 2);
    $emp = $dutydate->get_emp($emp_id);
    $obj = new Cmx_Shift_Staff($emp['emp_id']);
    $obj->set_carryover(substr($date, 0, 8));
}

// 成功
$dutydate->commit();
$log->log('alived');
echo('00');
