<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$group_id = $_REQUEST['group_id'];
$year = $_REQUEST['y'];
$month = $_REQUEST['m'];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    //-- 勤務希望締切日
    if (preg_match("/[^0-9]/", $_POST["close_day"]) > 0) {
        $err["close_day"]['数値以外が設定されています。'] = true;
    }
    //-- 連続勤務日上限
    if (preg_match("/[^0-9]/", $_POST["duty_upper_limit_day"]) > 0) {
        $err["duty_upper_limit_day"]['数値以外が設定されています。'] = true;
    }
    //-- シフト作成期間
    if ($_POST["start_month_flg1"] === "1" and $_POST["month_flg1"] === "1") {
        if ($_POST["start_day1"] !== "0" and $_POST["end_day1"] !== "0" and $_POST["start_day1"] > $_POST["end_day1"]) {
            $err["start_month"]['終了日を開始日以降にしてください。'] = true;
        }
    }
    if ($_POST["start_month_flg1"] === "0" && $_POST["month_flg1"] === "2") {
        $err["start_month"]['終了日を開始日の翌月同日以前にしてください。'] = true;
    }
    if (($_POST["start_month_flg1"] === "0" and $_POST["month_flg1"] === "1") or ( $_POST["start_month_flg1"] === "1" and $_POST["month_flg1"] === "2")) {
        if ($_POST["start_day1"] !== "0" and $_POST["end_day1"] !== "0" and $_POST["start_day1"] < $_POST["end_day1"]) {
            $err["start_month"]['終了日を開始日の翌月同日以前にしてください。'] = true;
        }
    }
    //-- 組み合わせ指定
    for ($i = 0; $i < count($_POST["seq_today"]); $i++) {
        $err_flg = false;
        if (empty($_POST["seq_today"][$i]) or empty($_POST["seq_next"][$i])) {
            $err["ok_pattern"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }
        if ($_POST["seq_today"][$i] === $_POST["seq_next"][$i]) {
            $err["ok_pattern"]['「当日」「翌日」に同じシフトは指定できません。'] = true;
            $err_flg = true;
        }

        $_POST['pattern_ok'][] = array(
            'group_id' => $group_id,
            'today_atdptn_ptn_id' => (int)substr($_POST["seq_today"][$i], 0, 2),
            'nextday_atdptn_ptn_id' => (int)substr($_POST["seq_next"][$i], 0, 2),
            'today_reason' => (int)substr($_POST["seq_today"][$i], 2, 2),
            'nextday_reason' => (int)substr($_POST["seq_next"][$i], 2, 2),
            'err' => $err_flg,
            'no' => $i + 1,
        );
    }
    //-- 組み合わせ禁止
    for ($i = 0; $i < count($_POST["ng_today"]); $i++) {
        $err_flg = false;
        if (empty($_POST["ng_today"][$i]) or empty($_POST["ng_next"][$i])) {
            $err["ng_pattern"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }

        // 組み合わせ指定と禁止の重複チェック
        for ($j = 0; $j < count($_POST["seq_today"]); $j++) {
            if ($_POST["seq_today"][$j] === $_POST["ng_today"][$i] and $_POST["seq_next"][$j] === $_POST["ng_next"][$i]) {
                $err["ng_pattern"]['「組み合わせ指定」と同じ組み合わせが存在します。'] = true;
                $err_flg = true;
                break;
            }
        }

        $_POST['pattern_ng'][] = array(
            'group_id' => $group_id,
            'today_atdptn_ptn_id' => (int)substr($_POST["ng_today"][$i], 0, 2),
            'nextday_atdptn_ptn_id' => (int)substr($_POST["ng_next"][$i], 0, 2),
            'today_reason' => (int)substr($_POST["ng_today"][$i], 2, 2),
            'nextday_reason' => (int)substr($_POST["ng_next"][$i], 2, 2),
            'err' => $err_flg,
            'no' => $i + 1,
        );
    }
    //-- 連続勤務シフトの上限
    for ($i = 0; $i < count($_POST["limit_id"]); $i++) {
        $err_flg = false;
        if (empty($_POST["limit_id"][$i]) or empty($_POST["limit_day"][$i])) {
            $err["up_limit"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }
        if (preg_match("/[^0-9]/", $_POST["limit_day"][$i]) > 0) {
            $err["up_limit"]['上限に数値以外が設定されています。'] = true;
            $err_flg = true;
        }
        for ($j = 0; $j < count($_POST["limit_id"]); $j++) {
            if ($i != $j and $_POST["limit_id"][$i] == $_POST["limit_id"][$j]) {
                $err["up_limit"]['重複設定されています。'] = true;
                $err_flg = true;
            }
        }

        $_POST['upper_limit'][] = array(
            'group_id' => $group_id,
            'atdptn_ptn_id' => (int)substr($_POST["limit_id"][$i], 0, 2),
            'upper_limit_day' => (int)substr($_POST["limit_day"][$i], 0, 2),
            'reason' => (int)substr($_POST["limit_id"][$i], 2, 2),
            'err' => $err_flg,
            'no' => $i + 1,
        );
    }
    //-- 勤務シフトの間隔
    for ($i = 0; $i < count($_POST["interval_id"]); $i++) {
        $err_flg = false;
        if (empty($_POST["interval_id"][$i]) or empty($_POST["interval_day"][$i])) {
            $err["interval"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }
        if (preg_match("/[^0-9]/", $_POST["interval_day"][$i]) > 0) {
            $err["interval"]['間隔日数に数値以外が設定されています。'] = true;
            $err_flg = true;
        }
        for ($j = 0; $j < count($_POST["interval_id"]); $j++) {
            if ($i != $j and $_POST["interval_id"][$i] == $_POST["interval_id"][$j]) {
                $err["interval"]['重複設定されています。'] = true;
                $err_flg = true;
            }
        }

        $_POST['interval'][] = array(
            'group_id' => $group_id,
            'atdptn_ptn_id' => (int)substr($_POST["interval_id"][$i], 0, 2),
            'interval_day' => (int)substr($_POST["interval_day"][$i], 0, 2),
            'reason' => (int)substr($_POST["interval_id"][$i], 2, 2),
            'err' => $err_flg,
            'no' => $i + 1,
        );
    }
    //-- 性別固定シフト
    for ($i = 0; $i < count($_POST["sex_pattern"]); $i++) {
        $err_flg = false;
        if (empty($_POST["sex_pattern"][$i]) or empty($_POST["sex_pattern"][$i])) {
            $err["sex_pattern"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }
        for ($j = 0; $j < count($_POST["sex_pattern"]); $j++) {
            if ($i != $j and $_POST["sex_pattern"][$i] == $_POST["sex_pattern"][$j]) {
                $err["sex_pattern"]['重複設定されています。'] = true;
                $err_flg = true;
            }
        }

        $_POST['pattern_sex'][] = array(
            'group_id' => $group_id,
            'ptn_id' => $_POST["sex_pattern"][$i],
            'err' => $err_flg,
            'no' => $i + 1,
        );
    }
    //-- 4週8休の開始日
    if ($_POST["four_week_chk_flg"] == "t") {
        if (empty($_POST["chk_start_date"])) {
            $err["chk_start_date"]['4週8休のチェックをする場合は開始日を指定してください。'] = true;
        }
        else if ($_POST["chk_start_date"] !== date('Y-m-d', strtotime($_POST["chk_start_date"]))) {
            $err["chk_start_date"]['正しくない日付です'] = true;
        }
        //-- ４週８休シフト種類
        for ($i = 0; $i < count($_POST["hol_pattern"]); $i++) {
            $err_flg = false;
            if (empty($_POST["hol_pattern"][$i]) or empty($_POST["hol_pattern"][$i])) {
                $err["hol_pattern"]['未設定の項目が存在します。'] = true;
                $err_flg = true;
            }
            for ($j = 0; $j < count($_POST["hol_pattern"]); $j++) {
                if ($i != $j and $_POST["hol_pattern"][$i] == $_POST["hol_pattern"][$j]) {
                    $err["hol_pattern"]['重複設定されています。'] = true;
                    $err_flg = true;
                }
            }

            $_POST['pattern_holiday'][] = array(
                'group_id' => $group_id,
                'ptn_id' => $_POST["hol_pattern"][$i],
                'err' => $err_flg,
                'no' => $i + 1,
            );
        }
    }
    else {
        $_POST["chk_start_date"] = "";
        $_POST["pattern_holiday"] = array();
    }
    //-- 解析対象とする期間
    if (preg_match("/[^0-9]/", $_POST["auto_check_months"]) > 0) {
        $err["auto_check_months"]['数値以外が設定されています。'] = true;
    }
    //-- 勤務シフト表タイトル
    if (empty($_POST["print_title"])) {
        $err["print_title"]['入力してください。'] = true;
    }
    if (mb_strwidth($_POST["print_title"], 'euc-jp') > 100) {
        $err["print_title"]['100文字以内で入力して下さい。'] = true;
    }
    //-- 決裁欄役職
    $no = 1;
    foreach ($_POST["st_name"] as $st_name) {
        $err_flg = false;
        if (empty($st_name)) {
            continue;
        }
        if (mb_strwidth($st_name, 'euc-jp') > 60) {
            $err["st_name"]['60文字以内で入力して下さい。'] = true;
        }

        $_POST['print_st_names'][] = array(
            'group_id' => $group_id,
            'st_name' => $st_name,
            'err' => $err_flg,
            'no' => $no++,
        );
    }
}
//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $ward = new Cmx_Shift_Ward($_POST);
    $ward->ward_update();
    header("Location: " . CMX_BASE_URL . "/shift/ward.php?y={$year}&m={$month}&group_id={$group_id}");
    exit;
}

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
if (is_null($this_ward) and $ward_lists) {
    $this_ward = $ward_lists[0];
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', array());
    $view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');
    $view->display('shift/ward.tpl');
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
$sdate = $this_ward->shift_date();
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
    list($year, $month) = $sdate->months_add($year, $month, 1);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('err', $err);

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('month', $month);

// 勤務パターンプルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $this_ward->pattern_sign_list());

// 病棟
if ($mode === 'confirm') {
    $view->assign('this_ward', new Cmx_Shift_Ward($_POST));

    // 勤務シフト間隔
    $view->assign('interval', $_POST['interval']);

    // 連続勤務シフト上限
    $view->assign('upper_limit', $_POST['upper_limit']);

    // 組み合わせシフト
    $view->assign('pattern_ok', $_POST['pattern_ok']);

    // 組み合わせ禁止シフト
    $view->assign('pattern_ng', $_POST['pattern_ng']);

    // 同時勤務の性別を固定するシフト
    $view->assign('pattern_sex', $_POST['pattern_sex']);

    // ４週８休シフト種類
    $view->assign('pattern_holiday', $_POST['pattern_holiday']);

    // 決済欄役職
    $view->assign('print_st_names', $_POST['print_st_names']);
}
else {
    $view->assign('this_ward', $this_ward);

    // 勤務シフト間隔
    $view->assign('interval', $this_ward->pattern_interval());

    // 連続勤務シフト上限
    $view->assign('upper_limit', $this_ward->pattern_upper_limit());

    // 組み合わせシフト
    $view->assign('pattern_ok', $this_ward->pattern_ok());

    // 組み合わせ禁止シフト
    $view->assign('pattern_ng', $this_ward->pattern_ng());

    // 同時勤務の性別を固定するシフト
    $view->assign('pattern_sex', $this_ward->pattern_sex());

    // ４週８休シフト種類
    $view->assign('pattern_holiday', $this_ward->pattern_holiday());

    // 決済欄役職
    $view->assign('print_st_names', $this_ward->print_st_names());
}

$view->display('shift/ward.tpl');
