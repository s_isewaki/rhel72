<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/TableHistory.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_REQUEST['group_id'];
$year = $_REQUEST['year'];
$month = $_REQUEST['month'];

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$history = new Cmx_Shift_TableHistory($group_id, $year, $month);
$lists = $history->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 勤務パターンリスト
$view->assign('lists', $lists);

$view->display('shift/plan_pdf_history.tpl');
