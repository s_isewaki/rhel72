<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Personal.php';
require_once 'Cmx/View/Smarty.php';
require_once 'Cmx/Shift/PDF.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];
$month = $_GET["month"];
if ($month === '13') {
    $month = 12;
}

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);

//----------------------------------------------------------
// 職員
//----------------------------------------------------------
if ($_GET["staff"] !== '') {
    $person = new Cmx_Shift_Personal($_GET["staff"], 'month');
    list($lists, $total) = $person->lists($year, $month);
    $staff_list[] = array(
        'staff' => $person->get_staff(),
        'lists' => $lists,
        'total' => $total,
        'filler' => 27 - $total['print'],
    );
}
else {
    foreach (explode(':', $_GET["staffs"]) as $staffs_id) {
        $person = new Cmx_Shift_Personal($staffs_id, 'month');
        list($lists, $total) = $person->lists($year, $month);
        $staff_list[] = array(
            'staff' => $person->get_staff(),
            'lists' => $lists,
            'total' => $total,
            'filler' => 27 - $total['print'],
        );
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$pdf = new Shift_PDF('L', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetFont('kozgopromedium', '', 8);

$view = new Cmx_View();
$view->assign('today', date('Y-m-d'));
$view->assign('year', $year);
$view->assign('month', $month);
$view->assign('ward', $ward);

foreach ($staff_list as $data) {
    $view->assign($data);
    $html = $view->fetch('shift/print/pdf04000.tpl');
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
}

$pdf->Output("pdf04000.pdf", "I");
