<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_REQUEST['group_id'];
$year = $_REQUEST['y'];
$month = $_REQUEST['m'];


//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff_list = $staff->lists($group_id, $year, $month);

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm') {
    $staff->plan_staff_update($_POST);

    header("Location: " . CMX_BASE_URL . "/shift/staff_plan.php?y={$year}&m={$month}&group_id={$group_id}");
    exit;
}
//----------------------------------------------------------
// 病棟管理者
//----------------------------------------------------------
$is_administrator = $ward->is_administrator($emp->emp_id());

//----------------------------------------------------------
// 病棟チーム
//----------------------------------------------------------
$team = new Cmx_Shift_Team();
$team_list = $team->lists();

//----------------------------------------------------------
// 表示用クール
//----------------------------------------------------------
$sdate = $ward->shift_date();
list($vyear, $vmonth) = $sdate->view_cours($year, $month);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 年月
$view->assign('year', $year);
$view->assign('month', $month);
$view->assign('vyear', $vyear);
$view->assign('vmonth', $vmonth);

// 病棟
$view->assign('this_ward', $ward);

// 病棟スタッフリスト
$view->assign('staff', $staff_list);

// 病棟チームリスト
$view->assign('team', $team_list);
$view->assign('team_rows', count($team_list) + 1);

$view->display('shift/staff_plan.tpl');
