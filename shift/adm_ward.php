<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/Shift/Date/Cours.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// 病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$base_year = $conf->get('shift.cours.base_year');
$base_date = $conf->get('shift.cours.base_date');
$base_count = $conf->get('shift.cours.base_count');
if (is_null($base_count)) {
    $base_count = 13;
}

//----------------------------------------------------------
// 年月リスト
//----------------------------------------------------------
$sdate = new Cmx_Shift_Date_Cours($base_date, $base_year, $base_count);
$year = date('Y');
$month = date('m');
$yearmonth = array();
foreach (range(-2, 1) as $i) {
    list($y, $m) = $sdate->months_add($year, $month, $i);
    list($vy, $vm) = $sdate->view_cours($y, $m);
    $yearmonth[] = array(
        'year' => $y,
        'month' => $m,
        'vyear' => $vy,
        'vmonth' => $vm,
        'rmonth' => ($m === 13 ? 12 : $m),
        'i' => $i,
    );
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// 病棟リスト
$view->assign('lists', $ward->lists());

// 年月
$view->assign('yearmonth', $yearmonth);

$view->display('shift/adm_ward.tpl');
