<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Model/Calendar.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$ptn = (int)substr($_REQUEST['ptn'], 0, 2);
$gid = $_REQUEST['gid'];
$date = substr($_REQUEST['date'], 1);

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern($gid);
$officehours = $pattern->officehours();
$nightduty = $pattern->night_duty_connect();

//----------------------------------------------------------
// 日付属性
//----------------------------------------------------------
$cal = new Cmx_Calendar();
$cdate = $cal->date($date);

$type = $cdate['type'];
if ($cdate['type'] === '8') {
    switch ($cdate['w']) {
        case '0':
            $type = 3;
            break;
        case '6':
            $type = 2;
            break;
        default:
            $type = 1;
            break;
    }
}

//----------------------------------------------------------
// 勤務記号ID
//----------------------------------------------------------
$p = $ptn;
if (substr($p, 0, 2) !== '10') {
    $p = sprintf('%02d00', $p);
}

//----------------------------------------------------------
// 出退勤時間
//----------------------------------------------------------
$hours = $officehours[$ptn][$type];

//----------------------------------------------------------
// 送信データ
//----------------------------------------------------------
//-- 終了時
if (!empty($hours['officehours2_end'])) {
    if ($hours['officehours2_start'] >= $hours['officehours2_end']) {
        $end_hour = sprintf('%02d', substr($hours['officehours2_end'], 0, 2) + 24);
    }
    else {
        $end_hour = substr($hours['officehours2_end'], 0, 2);
    }
}
else {
    $end_hour = '';
}

$send = array(
    'ptn' => $p,
    'start_hour' => !empty($hours['officehours2_start']) ? substr($hours['officehours2_start'], 0, 2) : "",
    'start_min' => !empty($hours['officehours2_start']) ? substr($hours['officehours2_start'], 3, 2) : "",
    'end_hour' => $end_hour,
    'end_min' => !empty($hours['officehours2_end']) ? substr($hours['officehours2_end'], 3, 2) : "",
    'rest_start_hour' => !empty($hours['officehours4_start']) ? substr($hours['officehours4_start'], 0, 2) : "",
    'rest_start_min' => !empty($hours['officehours4_start']) ? substr($hours['officehours4_start'], 3, 2) : "",
    'rest_end_hour' => !empty($hours['officehours4_end']) ? substr($hours['officehours4_end'], 0, 2) : "",
    'rest_end_min' => !empty($hours['officehours4_end']) ? substr($hours['officehours4_end'], 3, 2) : "",
    'night_duty' => $nightduty[$ptn] ? 1 : 0,
);

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
print cmx_json_encode($send);
