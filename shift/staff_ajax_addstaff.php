<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/License.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_POST['group_id'];
$year = $_POST['year'];
$month = $_POST['month'];
$emp_id_text = $_POST['emp_id'];
$add_staff_count = $_POST['add_staff_count'] ? $_POST['add_staff_count'] : 0;

//----------------------------------------------------------
// ライセンス
//----------------------------------------------------------
$license = new Cmx_License();
$now_staff_count = $license->shift_count();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$emp_ids = array_unique(explode(', ', $emp_id_text));
$count = 0;
$list = array();
$err = '';
foreach ($emp_ids as $emp_id) {
    $staff = new Cmx_Shift_Staff($emp_id);
    $add = 1;

    // 所属済みなら追加しない
    if ($staff->group_id()) {
        if ($group_id !== $staff->group_id()) {
            $ward = new Cmx_Shift_Ward($staff->group_id());
            if ($ward->group_id()) {
                $err .= sprintf('%sは [%s] に所属済みです。<br />', $staff->emp_name(), $ward->group_name());
                continue;
            }
            $add = 0;
        }
        else {
            $err .= sprintf('%sは所属済みです。<br />', $staff->emp_name());
            continue;
        }
    }

    // ライセンス数
    if (!$license->is_shift_count($now_staff_count + $add_staff_count + $count + $add)) {
        $err .= sprintf('%sはライセンス数上限のため追加できませんでした。<br />', $staff->emp_name());
        continue;
    }

    array_push($list, $staff);
    $count++;
}

//----------------------------------------------------------
// 病棟チーム
//----------------------------------------------------------
$team = new Cmx_Shift_Team();
$team_list = $team->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// グループID
$view->assign('group_id', $group_id);

// 年月
$view->assign('year', $year);
$view->assign('month', $month);

// 病棟スタッフ
$view->assign('staff', $list);

// 病棟チームリスト
$view->assign('team', $team_list);

$table = $view->fetch('shift/staff_addstaff.tpl');

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
print cmx_json_encode(
        array(
            'table' => $table,
            'err' => $err,
            'count' => $count,
        )
);
