<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$pattern_id = $_REQUEST['pattern_id'];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern();

//----------------------------------------------------------
// 記号リスト
//----------------------------------------------------------
$grp_list = $pattern->grp_lists();
if (!$pattern_id) {
    $pattern_id = $grp_list[0]->group_id();
}
$pattern->set_group_id($pattern_id);
$lists = $pattern->sign_lists();
$atdptn_lists = $pattern->atdptn_lists();
$officehours = $pattern->officehours();

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $pattern->update($_POST);
    header("Location: " . CMX_BASE_URL . "/shift/adm_sign.php?pattern_id={$pattern_id}");
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// 勤務パターングループID
$view->assign('pattern_id', $pattern_id);

// 勤務パターングループ
$view->assign('pgroup_lists', $pattern->grp_lists());
$view->assign('lists', $lists);
$view->assign('officehours', $officehours);

// 分担表表示順
$view->assign('allotment_cnt', count($atdptn_lists));

// 勤務記号色
$view->assign('font_color_list', $pattern->font_color_list());
$view->assign('back_color_list', $pattern->back_color_list());

$view->display('shift/adm_sign.tpl');
