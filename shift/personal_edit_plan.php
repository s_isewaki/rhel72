<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Personal.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$date_ymd = $_REQUEST["date"];
$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// YYYY-MM-DD
//----------------------------------------------------------
$date = preg_replace('/^(\d\d\d\d)(\d\d)(\d\d)/', '$1-$2-$3', $date_ymd);

//----------------------------------------------------------
// 個人
//----------------------------------------------------------
$person = new Cmx_Shift_Personal($emp->emp_id());
$person->set_login_id($emp->emp_id());
$person->table($date_ymd);

//----------------------------------------------------------
// 入力内容チェック
//----------------------------------------------------------
if ($mode === 'confirm') {
    // なし
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $person->save_plan($date_ymd, $_POST);

    $view = new Cmx_View();
    $view->display('shift/personal_edit_close.tpl');
    exit;
}

$table = $person->get_table();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = $person->get_staff();

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = $person->get_ward();
$sdate = $ward->shift_date();
list($year, $month) = $sdate->months($date);

//----------------------------------------------------------
// 勤務予定
//----------------------------------------------------------
if (empty($err)) {
    $plan = $table['plan'];
}
else {
    $_POST['pattern'] = (int)substr($_POST['ptn'], 0, 2);
    $_POST['reason'] = (int)substr($_POST['ptn'], 2, 2);
    $plan = $_POST;
}

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern($plan['tmcd_group_id']);
$pattern_hash = $pattern->ptn4_hash();
$plan['group_name'] = $ward->group_name();
if ($plan['ptn'] && $plan['ptn'] !== '0000') {
    $plan['pattern_name'] = $pattern_hash[$plan['ptn']]->ptn_name();
}


//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// プルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $pattern->sign_lists());

// 勤務パターングループリスト
$view->assign('pgroup_lists', $pattern->grp_lists());

// 個人勤務データ
$view->assign('date', $date_ymd);
$view->assign('emp', $table['emp']);
$view->assign('plan', $plan);

// エラー
$view->assign('err', $err);

$view->display('shift/personal_edit_plan.tpl');
