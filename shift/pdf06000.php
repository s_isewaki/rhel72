<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];

//----------------------------------------------------------
// 部署
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$adminlist = $ward->administrator();
$shift_admin = $ward->is_administrator($emp->emp_id());
$ward->dates($year, $month);

//----------------------------------------------------------
// 職員
//----------------------------------------------------------
if ($_GET["staff"] !== '') {
    $get_staff = new Cmx_Shift_Staff($_GET["staff"]);
    $staff_list = array($get_staff);
}
else {
    foreach (explode(':', $_GET["staffs"]) as $staff_id) {
        $staff_list[$staff_id] = new Cmx_Shift_Staff($staff_id);
    }
}

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$calendar = $table->year_table($year, $staff_list);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('year', $year);
$view->assign('month', array(4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3));
$view->assign('days', range(1, 31));
$view->assign('month_limit', array(
    1 => 31,
    2 => date('t', strtotime($year + 1 . "-2-1")),
    3 => 31,
    4 => 30,
    5 => 31,
    6 => 30,
    7 => 31,
    8 => 31,
    9 => 30,
    10 => 31,
    11 => 30,
    12 => 31,
));

require_once 'Cmx/Shift/PDF.php';
$pdf = new Shift_PDF('L', 'mm', 'A4', true, 'UTF-8', false);

foreach ($staff_list as $staff) {
    $view->assign('staff', $staff);
    $view->assign('calendar', $calendar[$staff->emp_id()]);

    $html = $view->fetch('shift/print/pdf06000.tpl');
    $pdf->AddPage();
    $pdf->writeHTML(to_utf8($html), true, 0, false, 0);
}

$pdf->Output("pdf06000.pdf", "I");
