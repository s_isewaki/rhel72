<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/Model/Shift/WorkingHours.php');
require_once('Cmx/Model/Shift/Approval.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$id = $_GET['id'];

//----------------------------------------------------------
// 承認データ
//----------------------------------------------------------
$appr = new Cmx_Shift_Approval();
list($group_id, $year, $month) = $appr->fetch_approval_by_id($id);

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$this_ward = new Cmx_Shift_Ward($group_id);
$sdate = $this_ward->shift_date('month');

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff_list = $staff->lists($group_id, $year, $month);

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($emp->is_shift_admin());
$table->set_ward($this_ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);
$table->month_table($year, $month);
$table->total_count();

//----------------------------------------------------------
// 月次承認済み
//----------------------------------------------------------
$approved = $appr->approved_group($this_ward->group_id(), $year, $month);

//----------------------------------------------------------
// 月次承認ボタン
//----------------------------------------------------------
if ($table->is_month_approval() && $approved[1] && !$approved[2]) {
    $is_month_approval = 1;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('shift_admin', $this_ward->is_administrator($emp->emp_id()));

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('lastyear', $year - 1);
$view->assign('nextyear', $year + 1);
$view->assign('month', $month);
$view->assign('yearmonth', $yearmonth);

// 病棟スタッフリスト
$view->assign('staff', $staff_list);

// 日付リスト
$view->assign('start_date', $sdate->start_date($year, $month));
$view->assign('end_date', $sdate->end_date($year, $month));
$view->assign('dates', $this_ward->dates($year, $month));

// 病棟
$count_row_lists = $this_ward->count_row_lists();
$view->assign('this_ward', $this_ward);
$view->assign('pattern_list', $this_ward->pattern_list());
$view->assign('pattern_json', $this_ward->pattern_json());
$view->assign('pattern_ok', cmx_json_encode($this_ward->pattern_ok_hash()));
$view->assign('count_row', $count_row_lists);
$view->assign('count_row_count', count($count_row_lists));
$view->assign('count_col', $this_ward->count_col_lists());

// 勤務表
$view->assign('table', $table);

// 承認済み
$view->assign('approved', $approved);

// 月次承認ボタン
$view->assign('is_month_approval', $is_month_approval);

// 全画面
$view->assign('fullscreen', 1);

//部門長承認
$view->assign('approve', 1);
$view->assign('approve_id', $id);

$view->display('shift/results_month.tpl');
