<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$id = $_REQUEST['id'];

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern($id);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 勤務パターンプルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $pattern->lists());

$view->display('shift/pattern_pulldown.tpl');
