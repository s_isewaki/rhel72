<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Shift/Yukyu.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$year = $_POST["year"];
$month = $_POST["month"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$yukyu = new Cmx_Shift_Yukyu();
$data = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    $data = $yukyu->fuyo($year, $month);
}

//----------------------------------------------------------
// CSV
//----------------------------------------------------------
else if ($mode === 'csv') {
    $csv = to_sjis($yukyu->csv($year, $month));
    ob_clean();
    header("Content-Disposition: attachment; filename=yukyu-{$year}.csv");
    header("Content-Type: application/octet-stream; name=yukyu-{$year}.csv");
    header("Content-Length: " . strlen($csv));
    echo($csv);
    ob_end_flush();
    exit;
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
else if ($mode === 'update') {
    $data = $yukyu->fuyo($year, $month, 'update');
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);
$view->assign('err', $err);
$view->assign('years', range(date('Y'), date('Y') + 1));
$view->assign('year', $year);
$view->assign('month', $month);
$view->assign($data);
$view->display('shift/adm_options_yukyu.tpl');
