<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Approval.php');

// セッション
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// 権限
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// 日付の整形
$after_approval_date_org = $after_approval_date;
$after_approval_date = str_replace("_"," ",$after_approval_date);
$before_approval_date = str_replace("_"," ",$before_approval_date);

$weekday = array( "日", "月", "火", "水", "木", "金", "土" );

// キャンセル
if ($mode === 'cancel'){
    $msg = date('Y年n月j日(', strtotime($before_approval_date));
    $msg .= $weekday[date('w', strtotime($before_approval_date))];
    $msg .= date(') H:i', strtotime($before_approval_date));
    print($msg);
    return;
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$appr = new Cmx_Shift_Approval();

// 変更がない場合
if ($after_approval_date === $before_approval_date){
    $update_flg = false;
    $msg = "変更されていません";
}
// 日時のフォーマットが誤っている場合
else if ($after_approval_date !== date('Y-m-d H:i', strtotime($after_approval_date))) {
    $update_flg = false;
    $msg = "不正な入力です";
}
else if (isset($id) && isset($after_approval_date)){
    // UPDATE
    $appr->update_approval_date($id, $after_approval_date);
    $update_flg = true;

    $msg = date('Y年n月j日(', strtotime($after_approval_date));
    $msg .= $weekday[date('w', strtotime($after_approval_date))];
    $msg .= date(') H:i', strtotime($after_approval_date));
}
else {
    $update_flg = false;
    $msg = "更新に失敗しました";
}

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
print cmx_json_encode(
        array(
            'update_flg' => $update_flg,
            'changed_date' => $after_approval_date_org,
            'msg'        => $msg,
        )
);

