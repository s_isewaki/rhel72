<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
// に看護部病棟が含まれているか
//----------------------------------------------------------
$kango = false;
foreach ($ward_lists as $w) {
    $code = $w->sfc_group_code();
    if (!empty($code)) {
        $kango = true;
        break;
    }
}

//----------------------------------------------------------
// 管理病棟があればplan or results、なければpersonal
//----------------------------------------------------------
if ($ward_lists) {
    if ($kango) {
        print header("Location: plan.php");
    }
    else {
        print header("Location: results.php");
    }
}
else {
    print header("Location: personal.php");
}