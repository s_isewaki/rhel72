<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$group_id = $_REQUEST['group_id'];
$emp_id = $_REQUEST['emp_id'];
$year = $_REQUEST['y'];
$month = $_REQUEST['m'];

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
$group_id = $_GET['group_id'];
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
if (is_null($this_ward) and $ward_lists) {
    $this_ward = $ward_lists[0];
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', array());
    $view->display('shift/staff_employee.tpl');
    exit;
}

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    //-- 長期休暇
    for ($i = 0; $i < count($_POST["holiday"]); $i++) {
        $err_flg = false;
        if (empty($_POST["holiday"][$i]) or empty($_POST["start_date"][$i]) or empty($_POST["end_date"][$i])) {
            $err["holiday"]['未設定の項目が存在します。'] = true;
            $err_flg = true;
        }
        for ($j = 0; $j < count($_POST["holiday"]); $j++) {
            if ($i != $j and $_POST["holiday"][$i] == $_POST["holiday"][$j]) {
                $err["holiday"]['重複設定されています。'] = true;
                $err_flg = true;
            }
        }
        if ($_POST["start_date"][$i] and $_POST["start_date"][$i] !== date('Y-m-d', strtotime($_POST["start_date"][$i]))) {
            $err["holiday"]['正しくない開始日です'] = true;
        }
        if ($_POST["end_date"][$i] and $_POST["end_date"][$i] !== date('Y-m-d', strtotime($_POST["end_date"][$i]))) {
            $err["holiday"]['正しくない終了日です'] = true;
        }

        $_POST['holiday'][] = array(
            'group_id' => $group_id,
            'emp_id' => $emp_id,
            'atdptn_ptn_id' => (int)substr($_POST["holiday"][$i], 0, 2),
            'start_date' => (int)substr($_POST["start_date"][$i], 0, 2),
            'end_date' => (int)substr($_POST["end_date"][$i], 2, 2),
            'err' => $err_flg,
        );
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {

    $staff = new Cmx_Shift_Staff($emp_id);
    $ward->staff_update($_POST);

    header("Location: " . CMX_BASE_URL . "/shift/staff_employee.php?y={$year}&m={$month}&group_id={$group_id}&emp_id={$emp_id}");
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
$sdate = $this_ward->shift_date();
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
    list($year, $month) = $sdate->months_add($year, $month, 1);
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff($emp_id);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('month', $month);


// 勤務パターンプルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $this_ward->pattern_sign_list());

// 病棟
$view->assign('this_ward', $this_ward);

// 病棟スタッフリスト
$view->assign('staff', $staff);

// 時短設定
$view->assign('shorter', $shorter[0]);

// 時短設定の選択肢
$view->assign('shorter_list', array('30' => '30分', '60' => '60分', '90' => '90分'));

// 曜日一覧
$view->assign('week_list', array(0 => '日', 1 => '月', 2 => '火', 3 => '水', 4 => '木', 5 => '金', 6 => '土', 7 => '祝'));

$view->display('shift/staff_employee.tpl');
