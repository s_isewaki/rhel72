<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Timecard/Overtime.php';
require_once 'Cmx/Model/Timecard/OvertimeReason.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_REQUEST['group_id'];

//----------------------------------------------------------
// 残業理由
//----------------------------------------------------------
$ovtm_reason = new Cmx_Timecard_OvertimeReason();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 残業理由リスト
$view->assign('reason_list', $ovtm_reason->lists($group_id));

$view->display('shift/overtime_reason_pulldown.tpl');
