<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Approval.php';
require_once 'Cmx/View/Smarty.php';

if(!$_GET){
    $year = date('Y');
    $month = date('m');
}
else{
    $year = $_GET['year'];
    $month = $_GET['month'];
}

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$appr = new Cmx_Shift_Approval();
$lists = $appr->fetch_month_approval($year, $month);

//----------------------------------------------------------
// session
//----------------------------------------------------------
session_cache_limiter('');
session_name('shift_approval');
session_start();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('lists', $lists);
$view->assign('year', $year);
$view->assign('nextyear', date('Y') + 3);
$view->assign('month', $month);

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

$view->display('shift/adm_approval.tpl');
