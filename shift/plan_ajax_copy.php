<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
$session->qualify(0);

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_POST["group_id"];
$year = $_POST["year"];
$month = $_POST["month"];
$copy_mode = $_POST["copy_mode"];
$copy_from = $_POST["copy_from"];
$copy_to = $_POST["copy_to"];
$copy_days = $_POST["copy_days"];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);

//----------------------------------------------------------
// 病棟管理者の確認
//----------------------------------------------------------
if (!$ward->is_administrator($emp->emp_id())) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_login_id($emp->emp_id());
$table->set_ward($ward);
$table->set_staff(new Cmx_Shift_Staff());
$table->set_year($year);
$table->set_month($month);
$table->copy_table($copy_mode, $copy_from, $copy_to, $copy_days);

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
header('Content-Type: application/json; charset=EUC-JP');
print $table->table_json();
