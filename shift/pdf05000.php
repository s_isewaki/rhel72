<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('fpdf153/mbfpdf.php');

define("KYUKA_KEKKON", "1032");         //結婚
define("KYUKA_KIBIKI", "1030");         //忌引
define("KYUKA_SONOTA", "1005");         //その他
define("KYUKA_ROUSAI", "1027");         //労災
define("KYUKA_BYOUKETU", "1007");       //病欠
define("KYUKA_JIKETU", "1006");         //事欠
define("KYUKA_SANKYU", "1025");         //産休
define("KYUKA_IKUKYU", "1026");         //育休
define("KYUKA_KYUSHOKU", "1028");       //休職
define("KYUKA_SYUKYU", "1040");         //週休
define("KYUKA_SYUKYU2", "1041");        //週休２
define("KYUKA_YUKYU", "1001");          //有休
define("KYUKA_YUKYUS", "1038");         //土曜有休
define("KYUKA_NATUKYU", "1031");        //夏休
define("KYUKA_1009", "1009");           //有宿
define("KYUKA_1010", "1010");           //夏宿
define("KYUKA_1011", "1011");           //週休土半
define("KYUKA_1012", "1012");           //週休土宿
define("KYUKA_1071", "1071");           //土週休＋待機

define("KYUKA_KANGO", "PI");            //看護(SFC_CODE)
define("KYUKA_KAIGO", "PJ");            //介護(SFC_CODE)
define("KYUKA_NONPAYBYOUKETU", "PK");   //病●(無給病欠)
define("KYUKA_KAIGOKYUGYOU", "PL");     //介●(介護休業)
define("KYUKA_SYUKYU_TOCHOKU", "PH");   //□当(週休＋宿直)

define("KINMU_PMSYUKYU", "OB");         //午後週休(SFC_CODE)
define("KINMU_AMSYUKYU", "OC");         //午前週休(SFC_CODE)
define("KINMU_PMYUKYU", "OD");          //午後有休(SFC_CODE)
define("KINMU_AMYUKYU", "OE");          //午前有休(SFC_CODE)
define("KINMU_PMNATUKYU", "OF");        //午後夏休(SFC_CODE)
define("KINMU_AMNATUKYU", "OG");        //午前夏休(SFC_CODE)
define("KINMU_SYUCCHOU", "OT");         //出張(SFC_CODE)
//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];
$month = $_GET["month"];
$length = $_GET['length'];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$shift_admin = $ward->is_administrator($emp->emp_id());
$ward->dates($year, $month);

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->range_table($year, $month, $length);
$table->total_count();

$Calendar = $table->dates();
$Data = $table->_tbl;
$Ptn = $table->_ptn;

//----------------------------------------------------------
// PDF
//----------------------------------------------------------
class CustomMBFPDF extends MBFPDF
{

    var $MOST_LEFT = 12;
    var $MOST_TOP = 5;
    var $MOST_BTM = 279;
    var $HeaderData;

    function CustomMBFPDF($data)
    {
        parent::MBFPDF('L', 'mm', 'A3'); // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある
        $this->AddMBFont(GOTHIC, "EUC-JP");
        $this->SetFont(GOTHIC, "", 10);
        $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP);
        $this->SetFillColor(210);
        $this->data = $data;
    }

    function Header()
    {

        $h1 = 5;

        $today = getdate();

        $this->Cell(400, 1, '', 0, 1, 'C');
        $this->SetFont(GOTHIC, '', 9);
        $this->Cell(325, $h1, '', 0, 0, 'C');
        $this->Cell(15, $h1, '事務局長', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '事務部長', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '人事課長', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '部長', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '所属長', 'LTR', 1, 'C');

        $this->Cell(150, $h1, '', 0, 0, 'C');
        $this->SetFont(GOTHIC, '', 16);
        $this->Cell(30, $h1, '集計表', 0, 0, 'C');
        $this->Cell(145, $h1, $this->data['group_name'], 0, 0, 'L');
        $this->Cell(15, $h1, '', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '', 'LTR', 0, 'C');
        $this->Cell(15, $h1, '', 'LTR', 1, 'C');

        $this->SetFont(GOTHIC, '', 12);
        if ($this->data['length'] > 1) {
            $this->Cell(150, $h1, $this->data['start_date'] . ' 〜 ' . $this->data['end_date'], 0, 0, 'L');
        }
        else {
            $this->Cell(150, $h1, $this->data['year'] . '年' . $this->data['month'] . '月', 0, 0, 'L');
        }
        $this->Cell(170, $h1, $today[year] . '年' . $today[mon] . '月' . $today[mday] . '日作成', 0, 0, 'R');
        $this->Cell(5, $h1, '', 0, 0, 'C');
        $this->Cell(15, $h1, '', 'LR', 0, 'C');
        $this->Cell(15, $h1, '', 'LR', 0, 'C');
        $this->Cell(15, $h1, '', 'LR', 0, 'C');
        $this->Cell(15, $h1, '', 'LR', 0, 'C');
        $this->Cell(15, $h1, '', 'LR', 1, 'C');

        $this->Cell(325, $h1, '', 0, 0, 'C');
        $this->Cell(15, $h1, '', 'LBR', 0, 'C');
        $this->Cell(15, $h1, '', 'LBR', 0, 'C');
        $this->Cell(15, $h1, '', 'LBR', 0, 'C');
        $this->Cell(15, $h1, '', 'LBR', 0, 'C');
        $this->Cell(15, $h1, '', 'LBR', 1, 'C');

        //---------------------------------------------------------------
        // 一覧ヘッダ部
        //---------------------------------------------------------------

        $this->Cell(30, $h1, '', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '', 'LTR', 0, 'C');
        $this->Cell(48, $h1, '特別休暇', 'LTR', 0, 'C');
        $this->Cell(64, $h1, 'その他', 'LTR', 0, 'C');
        $this->Cell(215, $h1, '休暇消化状況', 'LTR', 0, 'C');
        $this->Cell(35, $h1, '', 'LTR', 1, 'C');

        $this->Cell(30, $h1, '氏名', 'LR', 0, 'C');
        $this->Cell(8, $h1, '実', 'LR', 0, 'C');
        $this->Cell(8, $h1, '結', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '忌', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '看', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '介', 'LTR', 0, 'C');
        $this->Cell(8, $h1, 'そ', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '合', 'LR', 0, 'C');
        $this->Cell(8, $h1, '出', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '労', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '病', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '事', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '産', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '育', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '休', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '合', 'LR', 0, 'C');
        $this->Cell(56, $h1, '週休', 'LTR', 0, 'C');
        $this->Cell(18, $h1, '夏休', 'LTR', 0, 'C');
        $this->Cell(141, $h1, '年次有休休暇', 'LTR', 0, 'C');
        $this->Cell(35, $h1, '備考', 'LR', 1, 'C');

        $this->Cell(30, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, 'の', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');
        $this->Cell(8, $h1, '', 'LR', 0, 'C');

        $this->Cell(38, $h1, '当月 消化日', 'LTR', 0, 'C');
        $this->Cell(9, $h1, '合', 'LTR', 0, 'C');
        $this->Cell(9, $h1, '累', 'LTR', 0, 'C');
        $this->Cell(9, $h1, '合', 'LTR', 0, 'C');
        $this->Cell(9, $h1, '累', 'LTR', 0, 'C');
        $this->Cell(93, $h1, '当月消化日', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '合', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '繰', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '本', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '合', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '消', 'LTR', 0, 'C');
        $this->Cell(8, $h1, '残', 'LTR', 0, 'C');
        $this->SetFont(GOTHIC, '', 7);
        $this->Cell(35, $h1, '(夏休当月消化日は[ ]括り)', 'LR', 1, 'C');
        $this->SetFont(GOTHIC, '', 12);

        $this->Cell(30, $h1, '', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '働', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '婚', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '引', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '護', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '護', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '他', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '張', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '災', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '欠', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '欠', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '休', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '休', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '職', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '計', 'LBR', 0, 'C');
        $this->SetFont(GOTHIC, '', 7);
        $this->Cell(38, $h1, '(□以外は日付＋AMPM,A,P)', 'LBR', 0, 'C');
        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(9, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(9, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(9, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(9, $h1, '計', 'LBR', 0, 'C');
        $this->SetFont(GOTHIC, '', 7);
        $this->Cell(93, $h1, '(日付＋S(半日),A,P)', 'LBR', 0, 'C');
        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(8, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '越', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '年', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '計', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '化', 'LBR', 0, 'C');
        $this->Cell(8, $h1, '日', 'LBR', 0, 'C');
        $this->Cell(35, $h1, '', 'LBR', 1, 'C');
    }

}

$HeaderData_ = array(
    'group_name' => $ward->group_name(),
    'year' => $year,
    'month' => $month,
    'length' => $length,
    'start_date' => preg_replace("/^(\d\d\d\d)(\d\d)\d\d/", "$1年$2月", $table->start_ymd()),
    'end_date' => preg_replace("/^(\d\d\d\d)(\d\d)\d\d/", "$1年$2月", $table->end_ymd()),
);

$pdf = new CustomMBFPDF($HeaderData_);
$pdf->Open();
$pdf->AddPage();

$h1 = 4;
$w1 = 8;
$w2 = 9;

//---------------------------------------------------------------
// 一覧データ部
//---------------------------------------------------------------
$pdf->SetFont(GOTHIC, '', 9);

// 職員数分ループ
foreach ($Data['emp'] as $emp_id => $emp) {

    // 集計
    $total[0] = 0;        //実働
    $total[1] = 0;        //結婚
    $total[2] = 0;        //忌引
    $total[24] = 0;       //看護
    $total[25] = 0;       //介護
    $total[3] = 0;        //その他
    $total[4] = 0;        //出張
    $total[5] = 0;        //労災
    $total[6] = 0;        //入病欠
    $total[7] = 0;        //病欠
    $total[8] = 0;        //事欠
    $total[9] = 0;        //産休
    $total[10] = 0;       //育休
    $total[11] = 0;       //休職
    $total[12] = 0;       //合計-特別休暇
    $total[13] = 0;       //合計-その他

    $total[14] = 0;       //合計-週休
    $total[15] = 0;       //合計-夏休
    $total[16] = 0;       //合計-有休
    $total[17] = 0;       //週休-累計
    $total[18] = 0;       //夏休-累計
    $total[19] = 0;       //有休-合計
    $total[20] = 0;       //有休-消化
    $total[21] = 0;       //有休-残数
    $total[22] = 0;       //有休-繰越
    $total[23] = 0;       //有休-本年

    $syukyu = "";
    $natukyu = "";
    $yukyu = "";

    // 日付ループ
    foreach ($Calendar as $day => $cval1) {
        $ptn = $emp['dates']['d' . $day];
        $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $hash;
        $value = $ptn['results'];
        $value_sign = ($value && $pattern[$value]) ? $pattern[$value]->sfc_code() : "";
        $value_reason = ($value && $pattern[$value]) ? $pattern[$value]->atd_reason() : "";

        switch ($value_sign) {
            case KINMU_PMSYUKYU:          //午後週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'P ';
                }
                break;
            case KINMU_AMSYUKYU:          //午前週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'A ';
                }
                break;
            case KINMU_PMNATUKYU:         //午前夏休
                $total[15] += 0.5;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . 'P]';
                }
                break;
            case KINMU_AMNATUKYU:         //午前夏休
                $total[15] += 0.5;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . 'A]';
                }
                break;
            case KINMU_PMYUKYU:           //午後有休
                $total[16] += 0.5;
                if ($length === '1') {
                    $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'P ';
                }
                break;
            case KINMU_AMYUKYU:           //午前有休
                $total[16] += 0.5;
                if ($length === '1') {
                    $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'A ';
                }
                break;
            case KINMU_SYUCCHOU:          //出張
                $total[4] ++;
                break;
            case KYUKA_KANGO:             //看護
                $total[24] ++;
                break;
            case KYUKA_KAIGO:             //介護
                $total[25] ++;
                break;
            case KYUKA_NONPAYBYOUKETU:    //病●(無給病欠)
                $total[7] ++;
                break;
            case KYUKA_KAIGOKYUGYOU:      //介●(介護休業)
                $total[11] ++;
                break;
            case KYUKA_SYUKYU_TOCHOKU;    //週休＋宿直
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            default:
                null;
        }

        //実働
        if ($value && substr(strval($value), 0, 2) !== '10') {
            $total[0] ++;
        }

        switch (strval($value)) {
            case KYUKA_KEKKON:          //結婚
                $total[1] ++;
                break;
            case KYUKA_KIBIKI:          //忌引
                $total[2] ++;
                break;
            case KYUKA_SONOTA:          //その他
                $total[3] ++;
                break;
            case KYUKA_ROUSAI:          //労災
                $total[5] ++;
                break;
            case KYUKA_NYUBYOUKETU:     //入病欠
                $total[6] ++;
                break;
            case KYUKA_BYOUKETU:        //病欠
                $total[7] ++;
                break;
            case KYUKA_JIKETU:          //事欠
                $total[8] ++;
                break;
            case KYUKA_SANKYU:          //産休
                $total[9] ++;
                break;
            case KYUKA_IKUKYU:          //育休
                $total[10] ++;
                break;
            case KYUKA_KYUSHOKU:        //休職
                $total[11] ++;
                break;
            case KYUKA_SYUKYU;          //週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            case KYUKA_SYUKYU2;         //週休２
                $total[14]+=2;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'AMPM ';
                }
                break;
            case KYUKA_YUKYU;           //有休
            case KYUKA_YUKYUS;          //土曜有休
                if ($Calendar[$day]['type'] === '2') {
                    $total[16] += 0.5;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'S ';
                    }
                }
                else {
                    $total[16] ++;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . ' ';
                    }
                }
                break;
            case KYUKA_NATUKYU;         //夏休
                $total[15] ++;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . ']';
                }
                break;
            default:
                null;
        }
        switch ($value_reason) {
            case KYUKA_SYUKYU;          //週休
            case KYUKA_1011;            //週休土半
            case KYUKA_1012;            //週休土宿
            case KYUKA_1071;            //週休土待
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            case KYUKA_SYUKYU2;         //週休２
                $total[14]+=2;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'AMPM ';
                }
                break;
            case KYUKA_YUKYU;           //有休
            case KYUKA_YUKYUS;          //土曜有休
            case KYUKA_1009;            //有宿
                if ($Calendar[$day]['type'] === '2') {
                    $total[16] += 0.5;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'S ';
                    }
                }
                else {
                    $total[16] ++;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . ' ';
                    }
                }
                break;
            case KYUKA_NATUKYU;         //夏休
            case KYUKA_1010;            //夏宿
                $total[15] ++;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . ']';
                }
                break;
            default:
                null;
        }
    }
    //実働 (※出張を除く)
    $total[0] = $total[0] - $total[4];
    //合計-特別休暇
    $total[12] = $total[1] + $total[2] + $total[3] + $total[24] + $total[25];
    //合計-その他
    $total[13] = $total[4] + $total[5] + $total[6] + $total[7] + $total[8] + $total[9] + $total[10] + $total[11];

    //週休累計
    $total[17] = $emp['paid_w_get']['results'];
    //夏休累計
    $total[18] = $emp['paid_s_get']['results'];
    //有休合計
    $total[19] = $emp['paid_h_all']['results'];
    //有休消化
    $total[20] = $emp['paid_h_get']['results'];
    //有休残数
    $total[21] = $total[19] - $total[20];

    //有休繰越
    $total[22] = $emp['paid_h_kuri']['results'];
    //有休本年
    $total[23] = $emp['paid_h_fuyo']['results'];

    $cnt++;
    $pdf->SetFont(GOTHIC, '', 7);
    $pdf->Cell(30, $h1, $emp['name'], 'LBR', 0, 'L');
    $pdf->Cell($w1, $h1, strval($total[0]), 'LBR', 0, 'C');     //実働
    $pdf->Cell($w1, $h1, strval($total[1]), 'LBR', 0, 'C');     //結婚
    $pdf->Cell($w1, $h1, strval($total[2]), 'LBR', 0, 'C');     //忌引
    $pdf->Cell($w1, $h1, strval($total[24]), 'LBR', 0, 'C');    //看護
    $pdf->Cell($w1, $h1, strval($total[25]), 'LBR', 0, 'C');    //介護
    $pdf->Cell($w1, $h1, strval($total[3]), 'LBR', 0, 'C');     //その他
    $pdf->Cell($w1, $h1, strval($total[12]), 'LBR', 0, 'C');    //合計
    $pdf->Cell($w1, $h1, strval($total[4]), 'LBR', 0, 'R');     //出張
    $pdf->Cell($w1, $h1, strval($total[5]), 'LBR', 0, 'R');     //労災
    $pdf->Cell($w1, $h1, strval($total[7]), 'LBR', 0, 'R');     //病欠
    $pdf->Cell($w1, $h1, strval($total[8]), 'LBR', 0, 'R');     //事欠
    $pdf->Cell($w1, $h1, strval($total[9]), 'LBR', 0, 'R');     //産休
    $pdf->Cell($w1, $h1, strval($total[10]), 'LBR', 0, 'R');    //育休
    $pdf->Cell($w1, $h1, strval($total[11]), 'LBR', 0, 'R');    //休職
    $pdf->Cell($w1, $h1, strval($total[13]), 'LBR', 0, 'R');    //合計
    $pdf->Cell(38, $h1, $syukyu, 'LBR', 0, 'L');                //当月消化日
    $pdf->Cell($w2, $h1, strval($total[14]), 'LBR', 0, 'R');    //週休-合計
    $pdf->Cell($w2, $h1, strval($total[17]), 'LBR', 0, 'R');    //週休-累計
    $pdf->Cell($w2, $h1, strval($total[15]), 'LBR', 0, 'R');    //夏休-合計
    $pdf->Cell($w2, $h1, strval($total[18]), 'LBR', 0, 'R');     //夏休-累計
    if (strlen($yukyu) > 40) {
        $pdf->SetFont(GOTHIC, '', 6);
        $pdf->Cell(93, $h1, $yukyu, 'LBR', 0, 'L');               //当月消化日
        $pdf->SetFont(GOTHIC, '', 7);
    }
    else {
        $pdf->Cell(93, $h1, $yukyu, 'LBR', 0, 'L');              //当月消化日
    }
    $pdf->Cell($w1, $h1, strval($total[16]), 'LBR', 0, 'R');     //合計
    $pdf->Cell($w1, $h1, strval($total[22]), 'LBR', 0, 'R');     //繰越
    $pdf->Cell($w1, $h1, strval($total[23]), 'LBR', 0, 'R');     //本年
    $pdf->Cell($w1, $h1, strval($total[19]), 'LBR', 0, 'R');     //合計
    $pdf->Cell($w1, $h1, strval($total[20]), 'LBR', 0, 'R');     //消化
    $pdf->Cell($w1, $h1, strval($total[21]), 'LBR', 0, 'R');     //残日
    $pdf->Cell(35, $h1, $natukyu, 'LBR', 1, 'L');                //備考
}


if ($cnt == 0) {
    $b = 0;
}
else {
    if ($cnt > 55) {
        $b = ($cnt % 55);
    }
    else {
        $b = $cnt;
    }
}

for ($a = $b; $a < 55; $a++) {
    $pdf->Cell(30, $h1, '', 'LBR', 0, 'L');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell(38, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell(93, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell(35, $h1, '', 'LBR', 1, 'C');
}

$pdf->Output();
