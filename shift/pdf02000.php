<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/Model/Shift/WorkingHours.php');
require_once('fpdf153/mbfpdf.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$adminlist = $ward->administrator();
$shift_admin = $ward->is_administrator($emp->emp_id());
$sdate = $ward->shift_date($_GET["mode"]);
list($year, $month) = $sdate->convert_old_cours($_GET["year"], $_GET["month"]);
$Calendar = $ward->dates($year, $month);

$count_row_lists = $ward->count_row_lists();
$count_col_lists = $ward->count_col_lists();
$idx_night = 0;
$idx_day2 = 0;
$idx_middle = 0;

$start_date = $sdate->start_date($year, $month);
$end_date = $sdate->end_date($year, $month);

//----------------------------------------------------------
// 要稼働時間
//----------------------------------------------------------
$wh = new Cmx_Shift_WorkingHours($year, $month);
$whours = $wh->fetch();

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);

$table->table();
$table->total_count();

$Data = $table->_tbl;
$Ptn = $table->_ptn;

//----------------------------------------------------------
// PDF
//----------------------------------------------------------
class CustomMBFPDF extends MBFPDF
{

    var $MOST_LEFT = 12;
    var $MOST_TOP = 10;
    var $HeaderData;

    function CustomMBFPDF($HeaderData_)
    {
        parent::MBFPDF('L', 'mm', 'A3'); // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある
        $this->AddMBFont(GOTHIC, "EUC-JP");
        $this->SetFont(GOTHIC, "", 10);
        $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP);
        $this->SetFillColor(210);
        $this->HeaderData = $HeaderData_;
    }

    function Header()
    {

        $h1 = 4;
        $w1 = 8;
        $w2 = 10;

        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(340, 3, '', 0, 0, 'C');
        $this->Cell(30, 3, '　 部 長 　', 0, 0, 'C');
        $this->Cell(30, 3, '所 属 長', 0, 1, 'C');
        $this->Cell(340, 3, '', 0, 0, 'C');
        $this->Cell(30, 15, '印', 0, 0, 'C');
        $this->Cell(30, 15, '印', 0, 1, 'C');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->HeaderData[3], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 16);
        $this->Cell(30, 10, $this->HeaderData[4] . '/' . $this->HeaderData[5], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->HeaderData[0] . ' - ' . $this->HeaderData[1], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 16);
        $title = '出勤簿（実績簿)';
        $this->Cell(120, 10, htmlspecialchars($title), 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, '管理者:' . $this->HeaderData[2], 0, 1, 'L');

        //---------------------------------------------------------------
        // 一覧ヘッダ部
        //---------------------------------------------------------------
        $wk_Calendar = $this->HeaderData[6];
        $wk_count_row_lists = $this->HeaderData[7];

        $this->SetFont(GOTHIC, '', 9);
        $this->Cell($w1, $h1, '', 'LTR', 0, 'C');
        $this->Cell(18, $h1, '職位', 'LTR', 0, 'L');
        $this->Cell(27, $h1, '氏名', 'LTR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '日付', 1, 0, 'C');

        foreach ($wk_Calendar as $key1 => $val1) {
            $this->func_backColor($wk_Calendar[$key1]['type'], $wk_Calendar[$key1]['w']);
            $this->Cell($w1, $h1, $wk_Calendar[$key1]['d'], 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h1, '前月', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '当月', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '実働', 'LTR', 0, 'C');

        foreach ($wk_count_row_lists as $count_row_idx => $count_row) {
            $this->Cell($w2, $h1, $count_row['name'], 'LTR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');

        // ２段目
        $this->Cell($w1, $h1, '', 'LBR', 0, 'C');
        $this->Cell(18, $h1, '職種', 'LBR', 0, 'L');
        $this->Cell(27, $h1, '', 'LBR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '曜日', 1, 0, 'C');

        foreach ($wk_Calendar as $key1 => $val1) {
            $this->func_backColor($wk_Calendar[$key1]['type'], $wk_Calendar[$key1]['w']);
            $this->Cell($w1, $h1, $wk_Calendar[$key1]['wj'], 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h1, '繰越', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '実働', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '差', 'LBR', 0, 'C');

        foreach ($wk_count_row_lists as $key1 => $val1) {
            $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');
    }

    function func_backColor($type, $w)
    {
        //買取日・日曜・祝日
        if ($type == '8' or $type == '3') {
            $this->SetFillColor(245, 205, 166);
        }
        else {
            //土曜
            if ($w == '6') {
                $this->SetFillColor(157, 204, 224);
            }
            //日曜
            else if ($w == '0') {
                $this->SetFillColor(245, 205, 166);
            }
            //平日
            else {
                $this->SetFillColor(240, 240, 240);
            }
        }
    }

    function Footer()
    {
        $this->Ln(3);
        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(0, 5, '出力日時: ' . date('Y-m-d H:i:s'), 0, 1, 'R');
    }

}

$HeaderData_ = array(
    $start_date     //期間start
    , $end_date      //期間end
    , $adminlist[0]->emp_name()  //担当者名
    , $ward->group_name()
    , $_GET['year']
    , $_GET['month']
    , $Calendar
    , $count_row_lists
);

$pdf = new CustomMBFPDF($HeaderData_);
$pdf->Open();
$pdf->AddPage();

$h1 = 4;
$h2 = 1;
$w1 = 8;
$w2 = 10;

//---------------------------------------------------------------
// 一覧データ部
//---------------------------------------------------------------
$pdf->SetFont(GOTHIC, '', 9);

$cnt_nurse = 0;
$cnt_nurse165 = 0;
$cnt_staff = 0;

$cnt_night = 0;
$cnt_day2 = 0;
$cnt_middle = 0;
$cnt_holiday = 0;
$cnt_workhours = 0;
$cnt_nighthours = 0;

// 職員数分ループ
foreach ($Data['emp'] as $emp_id => $emp) {

    if (!$emp['name']) {
        continue;
    }

    $cnt++;

    //一行目[予定]
    $pdf->SetFont(GOTHIC, '', 8);
    $pdf->Cell($w1, $h1, strval($cnt), 'LTR', 0, 'R');
    $pdf->Cell(18, $h1, $emp['st_name'], 'LTR', 0, 'L');
    $pdf->Cell(27, $h1, $emp['name'], 'LTR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LTR', 0, 'C');

    $pdf->Cell($w2, $h1, '予定', 1, 0, 'C');

    foreach ($Calendar as $date => $dates) {
        $ptn = $emp['dates']['d' . $date];
        $pattern = $ptn['plan_pattern_id'] ? $Ptn[$ptn['plan_pattern_id']]->hash() : $hash;
        $plan = $ptn['plan'];
        $plan_sign = ($plan && $pattern[$plan]) ? $pattern[$plan]->font_name() : "";

        //文字数確認
        if (strlen($plan_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 7);
        }
        else {
            $pdf->SetFont(GOTHIC, '', 9);
        }

        $pdf->func_backColor($Calendar[$date]['type'], $Calendar[$date]['w']);

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates']['d' . $date]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $date]['assist']) and $emp['dates']['d' . $date]['assist'] !== $group_id)) {
            $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
        }
        else {
            $pdf->Cell($w1, $h1, $plan_sign, 1, 0, 'C', 1);
        }
    }

    $pdf->SetFont(GOTHIC, '', 7);

    //[予定]前月繰越
    $pdf->Cell($w2, $h1, $emp['wh_over']['plan'], 1, 0, 'R');

    //[予定]当月実働
    $pdf->Cell($w2, $h1, $emp['wh_hour']['plan'], 1, 0, 'R');

    //[予定]実働差
    $pdf->Cell($w2, $h1, $emp['wh_diff']['plan'], 1, 0, 'R');

    //[予定]日勤II・夜勤・中勤・週休 等
    foreach ($count_row_lists as $count_row_idx => $count_row) {
        $count = $count_row['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['plan'])) {
            $total = $emp['count_row']['c' . $count]['plan'];
        }
        $pdf->Cell($w2, $h1, strval($total), 1, 0, 'R');

        if ($count_row['name'] === '夜勤') {
            $idx_night = $count;
        }
        if ($count_row['name'] === '日勤II') {
            $idx_day2 = $count;
        }
        if ($count_row['name'] === '中勤') {
            $idx_middle = $count;
        }
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');

    //二行目[実績]
    $pdf->SetFont(GOTHIC, '', 8);
    $pdf->Cell($w1, $h1, '', 'LR', 0, 'C');
    $pdf->Cell(18, $h1, $emp['job_name'], 'LR', 0, 'L');
    $pdf->Cell(27, $h1, '', 'LR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LR', 0, 'C');

    $pdf->Cell($w2, $h1, '実績', 1, 0, 'C');

    foreach ($Calendar as $date => $dates) {
        $ptn = $emp['dates']['d' . $date];
        $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $hash;
        $results = $ptn['results'];
        $results_sign = ($results && $pattern[$results]) ? $pattern[$results]->font_name() : "";

        if (strlen($results_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 7);
        }
        else {
            $pdf->SetFont(GOTHIC, '', 9);
        }

        $pdf->func_backColor($Calendar[$date]['type'], $Calendar[$date]['w']);

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates']['d' . $date]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $date]['assist']) and $emp['dates']['d' . $date]['assist'] !== $group_id)) {
            $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
        }
        else {
            $pdf->Cell($w1, $h1, $results_sign, 1, 0, 'C', 1);
        }

        // 休暇回数
        if (substr($results, 0, 2) === '10') {
            $cnt_holiday++;
        }
    }

    $pdf->SetFont(GOTHIC, '', 7);

    //[実績]前月繰越
    $pdf->Cell($w2, $h1, $emp['wh_over']['results'], 1, 0, 'R');

    //[実績]当月実働
    $pdf->Cell($w2, $h1, $emp['wh_hour']['results'], 1, 0, 'R');

    //[実績]実働差
    $pdf->Cell($w2, $h1, $emp['wh_diff']['results'], 1, 0, 'R');

    // [実績]日勤II・夜勤・中勤・週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $count = $count_row_lists[$rkey1]['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['results'])) {
            $total = $emp['count_row']['c' . $count]['results'];
        }
        $pdf->Cell($w2, $h1, strval($total), 1, 0, 'R');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');

    //三行目[空行]
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell(18, $h1, '', 'LBR', 0, 'L');
    $pdf->Cell(27, $h1, '', 'LBR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');

    foreach ($Calendar as $ckey1 => $cval1) {
        $pdf->func_backColor($Calendar[$ckey1]['type'], $Calendar[$ckey1]['w']);
        $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
    }

    $pdf->Cell($w2, $h1, '', 1, 0, 'R');
    $pdf->Cell($w2, $h1, '', 1, 0, 'R');
    $pdf->Cell($w2, $h1, '', 1, 0, 'R');

    foreach ($count_row_lists as $rkey1 => $rval1) {
        $pdf->Cell($w2, $h1, '', 1, 0, 'R');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');

    // カウント
    if (in_array($emp['job_code'], array('0001', '0002', '0003', '0004'))) {
        // 夜勤回数
        $cnt_night += (float)$emp['count_row']['c' . $idx_night]['results'];

        // 日勤II回数
        $cnt_day2 += (float)$emp['count_row']['c' . $idx_day2]['results'];

        // 中勤回数
        $cnt_middle += (float)$emp['count_row']['c' . $idx_middle]['results'];
    }

    // 職員数
    $cnt_staff++;

    // 実働(予定)のある看護師数(保健師、助産師、看護師、准看護師)
    if ($emp['wh_hour']['plan'] !== '0.00' and in_array($emp['job_code'], array('0001', '0002', '0003', '0004'))) {
        $cnt_nurse++;

        // 実働時間
        $cnt_workhours += (float)$emp['wh_hour']['results'];

        // 夜勤16.5時間以上の看護師
        if ((float)$emp['wh_night']['results'] > 16.5) {
            $cnt_nurse165++;

            // 夜勤時間
            $cnt_nighthours += (float)$emp['wh_night']['results'];
        }
    }
}

//---------------------------------------------------------------
// 一覧フッタ部 (空の１行)
//---------------------------------------------------------------
$pdf->Cell($w1 + 18 + 27, $h2, '', 'LTB', 0, 'L');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');

foreach ($Calendar as $key1 => $val1) {
    $pdf->func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
    $pdf->Cell($w1, $h2, '', 1, 0, 'C', 1);
}

$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');

foreach ($count_row_lists as $key1 => $val1) {
    $pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
}
$pdf->Cell(1, $h2, '', 0, 1, 'C');

//---------------------------------------------------------------
foreach ($count_col_lists as $key1 => $val1) {
    $name = $count_col_lists[$key1]['name'];
    $count = $count_col_lists[$key1]['count'];

    $pdf->Cell($w1 + 18, $h1, $name, 1, 0, 'L');
    $pdf->Cell(27, $h1, '', 1, 0, 'L');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');

    foreach ($Calendar as $key1 => $val1) {
        $pdf->func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
        $total = '0';
        if (isset($Data[date]['d' . $key1]['count_col']['c' . $count]['results'])) {
            $total = $Data[date]['d' . $key1]['count_col']['c' . $count]['results'];
        }
        $pdf->Cell($w1, $h1, strval($total), 1, 0, 'R', 1);
    }
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');

    foreach ($count_row_lists as $key1 => $val1) {
        $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'C');
}

//---------------------------------------------------------------
// 平均
//---------------------------------------------------------------
$pdf->SetFont(GOTHIC, '', 10);
$foot_format = '＜要稼働時間＞ %.2f     ＜平均回数＞ 夜勤 ＝ %.2f  日勤II = %.2f  中勤 = %.2f  休暇 = %.2f     ＜平均時間＞ 実働 = %.2f  夜勤 = %.2f';
$foot = sprintf($foot_format, $whours['working_hours'] / 60, ($cnt_nurse !== 0 ? $cnt_night / $cnt_nurse : 0), ($cnt_nurse !== 0 ? $cnt_day2 / $cnt_nurse : 0), ($cnt_nurse !== 0 ? $cnt_middle / $cnt_nurse : 0), ($cnt_staff !== 0 ? $cnt_holiday / $cnt_staff : 0), ($cnt_nurse !== 0 ? $cnt_workhours / $cnt_nurse : 0), ($cnt_nurse165 !== 0 ? $cnt_nighthours / $cnt_nurse165 : 0)
);
$pdf->Ln(3);
$pdf->Cell(0, 6, $foot, 0, 1, 'L');
$pdf->Cell(0, 6, "夜勤回数: $cnt_night  日勤II回数: $cnt_day2  中勤回数: $cnt_middle  実働合計: $cnt_workhours  夜勤合計: $cnt_nighthours", 0, 1, 'L');
$pdf->Cell(0, 6, "職員数: $cnt_staff  看護師数: $cnt_nurse  夜勤16.5時間以上の看護師数: $cnt_nurse165", 0, 1, 'L');

$pdf->Output();
