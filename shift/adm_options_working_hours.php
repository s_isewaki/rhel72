<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/Model/Shift/WorkingHours.php');
require_once('Cmx/Shift/Date/Cours.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$base_year = $conf->get('shift.cours.base_year');
$base_date = $conf->get('shift.cours.base_date');
$base_count = $conf->get('shift.cours.base_count');
if (is_null($base_count)) {
    $base_count = 13;
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();

// 年
$nowyear = !empty($_REQUEST["y"]) ? $_REQUEST["y"] : date('Y');

// データベース
$whours = new Cmx_Shift_WorkingHours();
$whours_list = $whours->lists($nowyear);

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    foreach (range(1, $base_count) as $cours) {

        // 要稼働時間
        if (empty($_POST['working_hours'][$cours])) {
            $err["mes"]["working_hours"][0] = '要稼働時間が入力されていません。';
            $err["working_hours"][$cours] = true;
        }
        if (preg_match("/[^0-9\.]/", $_POST['working_hours'][$cours]) !== 0) {
            $err["mes"]["working_hours"][1] = '要稼働時間に異常な値があります。';
            $err["working_hours"][$cours] = true;
        }
        if (($_POST['working_hours'][$cours] * 60) % 15 > 0) {
            $err["mes"]["working_hours"][2] = '要稼働時間には0.25で割り切れる値を設定してください。';
            $err["working_hours"][$cours] = true;
        }

        // 週休日数
        if ($_POST['holiday_count'][$cours] === '') {
            $err["mes"]["holiday_count"][0] = '週休日数が入力されていません。';
            $err["holiday_count"][$cours] = true;
        }
        if (preg_match("/[^0-9]/", $_POST['holiday_count'][$cours]) !== 0) {
            $err["mes"]["holiday_count"][1] = '週休日数に異常な値があります。';
            $err["holiday_count"][$cours] = true;
        }
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $whours->update($nowyear, $_POST['working_hours'], $_POST['holiday_count']);

    header("Location: " . CMX_BASE_URL . "/shift/adm_options_working_hours.php?y={$nowyear}");
    exit;
}

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$lists = array();
if ($base_year) {
    $date = new Cmx_Shift_Date_Cours($base_date, $base_year, $base_count);

    foreach (range(1, $base_count) as $cours) {
        $lists[] = array(
            'cours' => $cours,
            'view' => $date->view_cours($nowyear, $cours),
            'start_date' => $date->start_date($nowyear, $cours),
            'end_date' => $date->end_date($nowyear, $cours),
            'working_hours' => isset($_POST['working_hours'][$cours]) ? $_POST['working_hours'][$cours] : $whours_list[$cours]["working_hours"] / 60,
            'holiday_count' => isset($_POST['holiday_count'][$cours]) ? $_POST['holiday_count'][$cours] : $whours_list[$cours]["holiday_count"],
        );
    }
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);
$view->assign('err', $err);
$view->assign('base_year', $date->_base_year);
$view->assign('base_date', $date->_base_date);
$view->assign('nowyear', $nowyear);
$view->assign('nextyear', $nowyear + 1);
$view->assign('lastyear', $nowyear - 1);
$view->assign('lists', $lists);

$view->display('shift/adm_options_working_hours.tpl');
