<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_POST['group_id'];
$emp_id = $_POST['emp_id'];

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
$_POST["duty_mon_day_flg"] = $_POST["duty_mon_day_flg"] === "1" ? 1 : 0;
$_POST["duty_tue_day_flg"] = $_POST["duty_tue_day_flg"] === "1" ? 1 : 0;
$_POST["duty_wed_day_flg"] = $_POST["duty_wed_day_flg"] === "1" ? 1 : 0;
$_POST["duty_thurs_day_flg"] = $_POST["duty_thurs_day_flg"] === "1" ? 1 : 0;
$_POST["duty_fri_day_flg"] = $_POST["duty_fri_day_flg"] === "1" ? 1 : 0;
$_POST["duty_sat_day_flg"] = $_POST["duty_sat_day_flg"] === "1" ? 1 : 0;
$_POST["duty_sun_day_flg"] = $_POST["duty_sun_day_flg"] === "1" ? 1 : 0;
$_POST["duty_hol_day_flg"] = $_POST["duty_hol_day_flg"] === "1" ? 1 : 0;

//-- 長期休暇
for ($i = 0; $i < count($_POST["holiday"]); $i++) {
    if (empty($_POST["holiday"][$i]) or empty($_POST["start_date"][$i]) or empty($_POST["end_date"][$i])) {
        $err["holiday"]['未設定の項目が存在します。<br />'] = true;
    }
    if ($_POST["start_date"][$i] and $_POST["start_date"][$i] !== date('Y-m-d', strtotime($_POST["start_date"][$i]))) {
        $err["holiday"]['正しくない開始日があります。<br />'] = true;
    }
    if ($_POST["end_date"][$i] and $_POST["end_date"][$i] !== date('Y-m-d', strtotime($_POST["end_date"][$i]))) {
        $err["holiday"]['正しくない終了日があります。<br />'] = true;
    }
}

//-- 時短終了日
if ($_POST["end_1y_date"] and $_POST["end_1y_date"] !== date('Y-m-d', strtotime($_POST["end_1y_date"]))){
    $err["end_date"]['正しくない終了日があります。<br />'] = true;
}
else if($_POST["end_3y_date"] and $_POST["end_3y_date"] !== date('Y-m-d', strtotime($_POST["end_3y_date"]))) {
    $err["end_date"]['正しくない終了日があります。<br />'] = true;
}

//----------------------------------------------------------
// エラー
//----------------------------------------------------------
$errmes = array();
foreach ($err as $key => $msg) {
    $errmes["err_{$key}"] = implode("", array_keys($msg));
}
if ($errmes) {
    print cmx_json_encode($errmes);
    exit;
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff->employee_update($_POST);
exit;
