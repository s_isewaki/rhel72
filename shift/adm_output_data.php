<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Shift/DataCSV.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$csv = new Cmx_Shift_DataCSV($_POST);

//----------------------------------------------------------
// ファイル名
//----------------------------------------------------------
$file = $csv->filename();

//----------------------------------------------------------
// CSVデータ
//----------------------------------------------------------
$data = $csv->output();

//----------------------------------------------------------
// CSV出力
//----------------------------------------------------------
ob_clean();
header("Content-Disposition: attachment; filename={$file}.csv");
header("Content-Type: application/octet-stream; name={$file}.csv");
header("Content-Length: " . strlen($data));
echo($data);
ob_end_flush();
exit;
