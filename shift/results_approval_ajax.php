<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Approval.php';

// セッション
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// 権限
$emp = $session->emp();
if (!$emp->is_shift()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

$appr = new Cmx_Shift_Approval();

if ($_POST['type'] === '1') {
    $appr->manager_approval_group($_POST['group_id'], $_POST['year'], $_POST['month'], $emp->emp_id());
}
else {
    $appr->boss_approval_group($_POST['group_id'], $_POST['year'], $_POST['month'], $emp->emp_id());
}
