<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Model/EmployeeClass.php');
require_once('Cmx/Model/EmployeeJob.php');
require_once('Cmx/Model/EmployeeStatus.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$group_id = $_GET["group_id"];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    // 部門名
    if (empty($_POST["group_name"])) {
        $err["group_name"] = '入力されていません';
    }
    if (mb_strwidth($_POST["group_name"], 'EUC-JP') > 50) {
        $err["group_name"] = '半角50文字以内で入力してください';
    }

    // 施設基準
    if ($_POST["standard_id"] > 10) {
        $err["standard_id"] = '選択されていません';
    }

    // シフト管理者
    if (empty($_POST["administrator"])) {
        $err["administrator"] = '選択されていません';
    }
    if (count($_POST["administrator"]) > 11) {
        $err["administrator"] = '11名以内で設定してください';
    }

    // 勤務パターン
    if (empty($_POST["pattern_id"])) {
        $err["pattern_id"] = '選択されていません';
    }

    // 周期
    if (empty($_POST["month_type"])) {
        $err["month_type"] = '選択されていません';
    }

    // 連携コード
    if ($_POST["sfc_group_code"]) {
        if (strlen($_POST["sfc_group_code"]) > 6) {
            $err["sfc_group_code"] = '6文字以内で設定してください';
        }
    }

    // 買取日
    if (!isset($_POST["kaitori_day"])) {
        $err["kaitori_day"] = '選択されていません';
    }

    // 部門長
    if (empty($_POST["manager"])) {
        $err["manager"] = '選択されていません';
    }

    // 予定の変更
    if (!isset($_POST["plan_edit_flg"])) {
        $err["plan_edit_flg"] = '選択されていません';
    }

    // 実績の変更
    if (!isset($_POST["result_edit_flg"])) {
        $err["result_edit_flg"] = '選択されていません';
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $ward = new Cmx_Shift_Ward($_POST);
    $ward->update();

    header("Location: " . CMX_BASE_URL . "/shift/adm_ward.php");
    exit;
}

//----------------------------------------------------------
// 部門
//----------------------------------------------------------
if ($mode === 'confirm') {
    $ward = new Cmx_Shift_Ward($_POST);
}
else if (!empty($group_id)) {
    $ward = new Cmx_Shift_Ward($group_id);
}
else {
    $ward = new Cmx_Shift_Ward();
}

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern();

//----------------------------------------------------------
// 部署、職種、役職
//----------------------------------------------------------
$emp_class = new Cmx_EmployeeClass();
$emp_job = new Cmx_EmployeeJob();
$emp_status = new Cmx_EmployeeStatus();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// プルダウンリスト(関数)
$view->register_function('pulldown', 'smarty_pulldown');
$view->register_function('class_pulldown', 'smarty_class_pulldown');

$view->assign('group_id', $group_id);
$view->assign('ward', $ward);
$view->assign('inst_lists', range(1, 10));
$view->assign('pgroup_lists', $pattern->grp_lists());

$view->assign('emp_class', $emp_class);
$view->assign('emp_job', $emp_job);
$view->assign('emp_status', $emp_status);

$view->assign('err', $err);

$view->display('shift/adm_ward_form.tpl');
