<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/License.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_POST['group_id'];
$year = $_POST['year'];
$month = $_POST['month'];
$emp_id_text = $_POST['emp_id'];
$add_staff_count = $_POST['add_staff_count'] ? $_POST['add_staff_count'] : 0;
$emp_lists = $_POST['emp_lists'];

//----------------------------------------------------------
// ライセンス
//----------------------------------------------------------
$license = new Cmx_License();
$now_staff_count = $license->shift_count();

//----------------------------------------------------------
// 現在の職員リスト
//----------------------------------------------------------
$now_emp = array();
foreach ($emp_lists as $row) {
    $now_emp[$row['value']] = 1;
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$emp_ids = explode(', ', $emp_id_text);
$count = 0;
$list = array();
$err = '';
foreach ($emp_ids as $emp_id) {

    $staff = new Cmx_Shift_Staff($emp_id);

    // 現在のリストにいるなら追加しない
    if ($now_emp[$emp_id] === 1) {
        $err .= sprintf('%sは所属済みです。<br />', $staff->emp_name());
        continue;
    }

    // 所属済みで無ければ、ライセンス数チェック
    if (!$staff->group_id() and ! $license->is_shift_count($now_staff_count + $add_staff_count + $count + 1)) {
        $err .= sprintf('%sはライセンス数上限のため追加できませんでした。<br />', $staff->emp_name());
        continue;
    }

    array_push($list, $staff);
    $count++;
}

//----------------------------------------------------------
// 病棟チーム
//----------------------------------------------------------
$team = new Cmx_Shift_Team();
$team_list = $team->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// グループID
$view->assign('group_id', $group_id);

// 年月
$view->assign('year', $year);
$view->assign('month', $month);

// 病棟スタッフ
$view->assign('staff', $list);

// 病棟チームリスト
$view->assign('team', $team_list);

$table = $view->fetch('shift/staff_plan_addstaff.tpl');

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
print cmx_json_encode(
        array(
            'table' => $table,
            'err' => $err,
            'count' => $count,
        )
);
