<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');

// セッション
$session = new Cmx_Session();
if (!$session->qualify()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

// 権限
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

$ward = new Cmx_Shift_Ward();

$mode = $_POST["mode"];

// 病棟削除
if ($mode === 'remove') {
    $group_id = $_POST["group_id"];

    if ($ward->is_exists_group_id($group_id)) {
        $res = $ward->remove($group_id);
        if (!$res) {
            header('HTTP/1.1 500 Internal Server Error');
            exit;
        }
    }
    else {
        header('HTTP/1.1 500 Internal Server Error');
        exit;
    }
}

// 並べ替え
else if ($mode === 'order') {
    $group_ids = explode(",", $_POST["group_id"]);

    $order = 1;
    foreach ($group_ids as $group_id) {
        if ($ward->is_exists_group_id($group_id)) {
            $res = $ward->update_order($group_id, $order);
            $order++;
        }
    }
}
