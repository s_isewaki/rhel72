<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$dakoku_use = $conf->get('shift.dakoku_use');
$dakoku_url = $conf->get('shift.dakoku_url');
$personal_hope_use = $conf->get('shift.personal_hope_use');
$zangyo_limit_days = $conf->get('shift.zangyo_limit_days');
$launcher_dakoku = $conf->get('shift.launcher_dakoku');
$notice_company_use = $conf->get('shift.notice.company_use');
$notice_company_url = $conf->get('shift.notice.company_url');
$notice_company_timeout = $conf->get('shift.notice.company_timeout');
$notice_company_transition_url = $conf->get('shift.notice.company_transition_url');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    $dakoku_use = $_POST["dakoku_use"];
    $dakoku_url = $_POST["dakoku_url"];
    $personal_hope_use = $_POST["personal_hope_use"];
    $zangyo_limit_days = $_POST["zangyo_limit_days"];
    $launcher_dakoku = $_POST["launcher_dakoku"];
    $notice_company_use = $_POST["notice_company_use"];
    $notice_company_url = $_POST["notice_company_url"];
    $notice_company_timeout = $_POST["notice_company_timeout"];
    $notice_company_transition_url = $_POST["notice_company_transition_url"];

    // 打刻システム連携
    if ($dakoku_use === '') {
        $err["dakoku_use"] = '選択されていません';
    }

    // 個人勤務表:残業入力期限
    if ($zangyo_limit_days === '') {
        $err["zangyo_limit_days"] = '入力されていません';
    }
    if ($zangyo_limit_days && preg_match("/\D/", $zangyo_limit_days)) {
        $err["zangyo_limit_days"] = '異常な値です';
    }

    // 個人勤務表:勤務希望入力
    if ($personal_hope_use === '') {
        $err["personal_hope_use"] = '選択されていません';
    }

    // ランチャーによる打刻
    if ($launcher_dakoku === '' || is_null($launcher_dakoku)) {
        $err["launcher_dakoku"] = '選択されていません';
    }

    // COMPANY連携利用
    if ($notice_company_use === '' || is_null($notice_company_use)) {
        $err["notice_company_use"] = '選択されていません';
    }

    // COMPANY連携URL
    if ($notice_company_use && !$notice_company_url) {
        $err["notice_company_url"] = 'COMPANY連携を利用する時は必須です';
    }

    // COMPANY連携タイムアウト時間
    if ($notice_company_timeout === '' || is_null($notice_company_timeout)) {
        $err["notice_company_timeout"] = '入力されていません';
    }
    if ($notice_company_timeout && preg_match("/\D/", $notice_company_timeout)) {
        $err["notice_company_timeout"] = '異常な値です';
    }

    // COMPANY連携利用
    if ($notice_company_transition_url === '' || is_null($notice_company_transition_url)) {
        $err["notice_company_transition_url"] = '選択されていません';
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $conf->set('shift.dakoku_use', $dakoku_use);
    $conf->set('shift.dakoku_url', $dakoku_url);
    $conf->set('shift.personal_hope_use', $personal_hope_use);
    $conf->set('shift.zangyo_limit_days', $zangyo_limit_days);
    $conf->set('shift.launcher_dakoku', $launcher_dakoku);
    $conf->set('shift.notice.company_use', $notice_company_use);
    $conf->set('shift.notice.company_url', $notice_company_url);
    $conf->set('shift.notice.company_timeout', $notice_company_timeout);
    $conf->set('shift.notice.company_transition_url', $notice_company_transition_url);

    header("Location: " . CMX_BASE_URL . "/shift/adm_options.php");
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);
$view->assign('err', $err);
$view->assign('dakoku_use', $dakoku_use);
$view->assign('dakoku_url', $dakoku_url);
$view->assign('zangyo_limit_days', $zangyo_limit_days);
$view->assign('personal_hope_use', $personal_hope_use);
$view->assign('launcher_dakoku', $launcher_dakoku);
$view->assign('notice_company_use', $notice_company_use);
$view->assign('notice_company_url', $notice_company_url);
$view->assign('notice_company_timeout', $notice_company_timeout);
$view->assign('notice_company_transition_url', $notice_company_transition_url);

$view->display('shift/adm_options.tpl');
