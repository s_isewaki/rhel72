<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/Personal.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/Model/Shift/Staff.php';
require_once 'Cmx/Model/Shift/Pattern.php';
require_once 'Cmx/Model/Timecard/Overtime.php';
require_once 'Cmx/Model/Timecard/OvertimeReason.php';
require_once 'Cmx/Model/SystemConfig.php';
require_once 'Cmx/Shift/Utils.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$emp_id = $_REQUEST["emp_id"];
$date_ymd = $_REQUEST["date"];
$mode = $_POST["mode"];
$submit = $_POST["submit_status"];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// YYYY-MM-DD
//----------------------------------------------------------
$date = preg_replace('/^(\d\d\d\d)(\d\d)(\d\d)/', '$1-$2-$3', $date_ymd);

//----------------------------------------------------------
// 打刻URL
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$dakoku_use = $conf->get('shift.dakoku_use');
$dakoku_url = $conf->get('shift.dakoku_url');

//----------------------------------------------------------
// 個人
//----------------------------------------------------------
$person = new Cmx_Shift_Personal($emp_id);
$person->set_login_id($emp->emp_id());
$person->table($date_ymd);
$table = $person->get_table();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = $person->get_staff();

//----------------------------------------------------------
// 時短対象
//----------------------------------------------------------
$is_shorter = $staff->is_shorter($date_ymd);

//----------------------------------------------------------
// 入力内容チェック
//----------------------------------------------------------
if ($mode === 'confirm') {
    $err = $person->validate_results($_POST);
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $person->save_results($date_ymd, $emp->emp_id(), $_POST);

    // 勤務パターン
    $ptn = (int)substr($_POST['ptn'], 0, 2);
    if ($ptn === 10) {
        $ptn .= substr($_POST['ptn'], 2, 2);
    }

    // 残業
    list($time, $reason) = $person->overtime($date_ymd);

    // update array
    $new_pattern = new Cmx_Shift_Pattern($_POST['tmcd_group_id']);
    $res = array(
        'results_update' => TRUE,
        'res_emp_id' => $emp_id,
        'res_date' => $date_ymd,
        'res_pattern' => $ptn,
        'res_duty' => $_POST['night_duty'],
        'res_tmcd_group_id' => $_POST['tmcd_group_id'],
        'res_ovtm' => $time ? $time : "",
        'res_ovtmrsn' => mb_substr($reason[0], 0, 4, 'EUC-JP'),
        'pattern_json' => $new_pattern->json(),
    );

    switch ($submit) {
        case 'close':
            $view = new Cmx_View();
            $view->assign($res);
            $view->display('shift/results_edit_close.tpl');
            exit;

        case 'prev':
            $person = new Cmx_Shift_Personal($_POST['prev_emp_id']);
            break;

        case 'next':
            $person = new Cmx_Shift_Personal($_POST['next_emp_id']);
            break;
    }
}
$person->table($date_ymd);
$table = $person->get_table();

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($staff->group_id());
$sdate = $ward->shift_date();
list($year, $month) = $sdate->months($date);

//----------------------------------------------------------
// 前後の職員
//----------------------------------------------------------
$prev_emp_id = $staff->prev($year, $month);
$next_emp_id = $staff->next($year, $month);

//----------------------------------------------------------
// 勤務実績
//----------------------------------------------------------
if (empty($err)) {
    $results = $table['results'];
}
else {
    $results = $_POST;
    $results['pattern'] = (int)substr($_POST['ptn'], 0, 2);
    $results['reason'] = (int)substr($_POST['ptn'], 2, 2);

    // 残業詰める
    $results['over'] = array();
    foreach ($_POST["over"] as $over) {
        if ($over['over_start_hour']) {
            $results['over'][] = $over;
        }
    }
}

//----------------------------------------------------------
// 勤務パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern($results['tmcd_group_id']);

//----------------------------------------------------------
// 残業理由
//----------------------------------------------------------
$ovtm_reason = new Cmx_Timecard_OvertimeReason();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 時短対象
$view->assign('is_shorter', $is_shorter);

// プルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $pattern->sign_lists());

// 勤務パターングループリスト
$view->assign('pgroup_lists', $pattern->grp_lists());

// 残業理由
$view->assign('ovtm_reason_lists', $ovtm_reason->lists($results['tmcd_group_id']));

// 時間、分
$view->assign('hours24', pulldown_24hours());
$view->assign('hours48', pulldown_48hours());
$view->assign('minutes', range(0, 59));
$view->assign('fifteen_minutes', range(0, 45, 15));

// 個人勤務データ
$view->assign('emp_id', $staff->emp_id());
$view->assign('date', $date_ymd);
$view->assign('emp', $table['emp']);
$view->assign('results', $results);
$view->assign('update_emp_id', new Cmx_Shift_Staff($table['results']['update_emp_id']));

// 前後の職員
$view->assign('prev_emp_id', $prev_emp_id);
$view->assign('next_emp_id', $next_emp_id);

// 打刻システム
$view->assign('dakoku_use', $dakoku_use);
$view->assign('dakoku_url', $dakoku_url);

// エラー
$view->assign('err', $err);

// 実績画面更新
$view->assign($res);

$view->display('shift/results_edit.tpl');
