<?php

set_include_path(dirname(dirname(__FILE__)));
require_once 'Cmx.php';
require_once 'Cmx/Shift/Alert.php';

$shift_alert = new Cmx_Shift_Alert();

// カレントの言語を日本語に設定する
mb_language("ja");
// 内部文字エンコードを設定する
mb_internal_encoding("EUC-JP");

$message = "";

if ($shift_alert->is_dead()) {
    $message .= "・電気錠システムからの通信が一定時間ありませんでした。\n";
}

if ($shift_alert->is_illigal_data()) {
    $message .= "・不正電文を検出しました。\n";
}

if (!empty($message)) {
    require_once 'Cmx/Shift/Notice.php';
    $shift_notice = new Cmx_Shift_Notice();
    $date = date('Y-m-d H:i');

    $subject = "【電気錠システム異常】" . $date;
    $send_message = <<<__MES_END__
【警告】
$message
【検出日時】
$date

__MES_END__;

    // メール送信
    $shift_notice->send($subject, $send_message);
}
