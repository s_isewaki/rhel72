<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$base_year = $conf->get('shift.cours.base_year');
$base_date = $conf->get('shift.cours.base_date');
$base_count = $conf->get('shift.cours.base_count');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    $base_year = $_POST["base_year"];
    $base_date = $_POST["base_date"];
    $base_count = $_POST["base_count"];

    // 基準となる年
    if (empty($base_year)) {
        $err["base_year"] = '入力されていません';
    }
    if (preg_match("/^\d\d\d\d$/", $base_year) === 0) {
        $err["base_year"] = '異常な値です';
    }

    // 基準年の第1クールの初日の日付
    if (empty($base_date)) {
        $err["base_date"] = '入力されていません';
    }
    if (preg_match("/^\d\d\d\d\-\d\d\-\d\d$/", $base_date) === 0) {
        $err["base_date"] = '異常な値です';
    }
    if ($_POST["base_date"] !== date('Y-m-d', strtotime($base_date))) {
        $err["base_date"] = '正しくない日付です';
    }

    // 1年間のクール数
    if (empty($base_count)) {
        $err["base_count"] = '入力されていません';
    }
    if (preg_match("/\D/", $base_count) !== 0) {
        $err["base_count"] = '異常な値です';
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $conf->set('shift.cours.base_year', $base_year);
    $conf->set('shift.cours.base_date', $base_date);
    $conf->set('shift.cours.base_count', $base_count);

    header("Location: " . CMX_BASE_URL . "/shift/adm_options_cours.php");
    exit;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);
$view->assign('err', $err);
$view->assign('base_year', $base_year);
$view->assign('base_date', $base_date);
$view->assign('base_count', $base_count);
$view->display('shift/adm_options_cours.tpl');
