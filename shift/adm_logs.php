<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// ログobj
//----------------------------------------------------------
$log = new Cmx_Shift_Log('log');

//----------------------------------------------------------
// session
//----------------------------------------------------------
session_cache_limiter('');
session_name('shift_log');
session_start();

//----------------------------------------------------------
// 検索条件
//----------------------------------------------------------
// 検索ボタン
if ($_POST['btn_search']) {
    $_SESSION["srh_exists"] = 1;
    $_SESSION["srh_name"] = $_POST['srh_name'];
    $_SESSION["srh_keyword"] = $_POST['srh_keyword'];
    $_SESSION["srh_function"] = $_POST['srh_function'];
    $_SESSION["srh_start_date"] = $_POST['srh_start_date'];
    $_SESSION["srh_last_date"] = $_POST['srh_last_date'];
    $p = 1;
}
else if ($_GET['p']) {
    $p = $_GET['p'];
}
else {
    $_SESSION = array(
        "srh_start_date" => date('Y-m-d', strtotime('-2 week')),
    );
    $p = 1;
}

//----------------------------------------------------------
// pager
//----------------------------------------------------------
$pages = 20;
$max_count = $log->count($_SESSION);
$max_pages = ceil($max_count / $pages);
if ($max_pages > 10) {
    if ($p > 5) {
        $start_pages = $p - 4;
    }
    else {
        $start_pages = 1;
    }
    $limit_pages = 10 + $start_pages - 1;
    if ($limit_pages > $max_pages) {
        $limit_pages = $max_pages;
        $start_pages = $max_pages - 10;
    }
}
else {
    $start_pages = 1;
    $limit_pages = $max_pages;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

$view->assign(array(
    'lists' => $log->lists(($p - 1) * $pages, $pages, $_SESSION),
    'function' => $log->function_name(),
    'pages' => range($start_pages, $limit_pages),
    'max_pages' => $max_pages,
));
$view->assign($_SESSION);

$view->display('shift/adm_logs.tpl');
