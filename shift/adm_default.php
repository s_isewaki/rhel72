<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/PatternCount.php');
require_once('Cmx/Model/EmployeeJob.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');
//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------

$pattern_id = $_REQUEST['pattern_id'];

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$default_kigo = $conf->get('shift.default.kigo');
$default_kigo = unserialize( $default_kigo );

//----------------------------------------------------------
// パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern();

//----------------------------------------------------------
// 記号リスト
//----------------------------------------------------------
$grp_list = $pattern->grp_lists();
if (!$pattern_id) {
    $pattern_id = $grp_list[0]->group_id();
}
$pattern->set_group_id($pattern_id);
$lists = $pattern->sign_lists();
$atdptn_lists = $pattern->atdptn_lists();

//----------------------------------------------------------
// 曜日
//----------------------------------------------------------
$weekday = array( '日','月','火','水','木','金','土','祝' );

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if( $mode==='confirm' ){
    $default_kigo[$pattern_id] = $_POST['default_kigo'];
    ksort($default_kigo);
    $conf->set('shift.default.kigo', serialize($default_kigo));
    header("Location: " . CMX_BASE_URL . "/shift/adm_default.php?pattern_id={$pattern_id}");
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 曜日
$view->assign('weekday', $weekday);

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// 勤務パターングループID
$view->assign('pattern_id', $pattern_id);

// 勤務パターングループ
$view->assign('pgroup_lists', $pattern->grp_lists());

// 勤務パターンリスト
$view->assign('lists', $lists);

// 曜日毎デフォルトパターン
$view->assign( 'default_kigo', $default_kigo[$pattern_id] );

$view->display('shift/adm_default.tpl');
