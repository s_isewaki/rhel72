<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/DataLayout.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$dl = new Cmx_Shift_DataLayout();

//----------------------------------------------------------
// 保存
//----------------------------------------------------------
if ($_POST['save'] === 'save') {
    $dl->save($_POST);
}

//----------------------------------------------------------
// レイアウト
//----------------------------------------------------------
if ($_POST['id']) {
    $layout = $dl->layout($_POST['id']);
}
else {
    $layout = array();
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

$view->assign('id', $_POST['id']);
$view->assign('layout', $layout);

// プルダウンリスト(関数)
$view->assign('pattern_lists', $dl->pattern_lists());
$view->assign('layout_lists', $dl->layout_lists());
$view->assign('layout_date_lists', $dl->layout_date_lists());
$view->register_function('pulldown', 'smarty_pulldown');

$view->display('shift/adm_output_setting_edit.tpl');
