<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
$session->qualify(0);

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$group_id = $_POST["group_id"];
$year = $_POST["year"];
$month = $_POST["month"];
$data = $_POST["data"];
mb_convert_variables('EUC-JP', "UTF-8,SJIS-win", $data);

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$shift_admin = $ward->is_administrator($emp->emp_id());
if ($_POST["cours"] === 'month') {
    $ward->shift_date('month');
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_login_id($emp->emp_id());
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);

//----------------------------------------------------------
// 処理
//----------------------------------------------------------
switch ($mode) {
    // 下書き
    case 'draft':
        if ($ward->is_administrator($emp->emp_id())) {
            $table->save_draft($data);
        }
        $table->total_count();
        break;

    // 予定登録
    case 'plan':
        if ($ward->is_administrator($emp->emp_id())) {
            $table->save_plan($data);
        }
        $table->total_count();
        break;

    // 実績登録
    case 'results':
        if ($ward->is_administrator($emp->emp_id())) {
            $table->save_results($data);
        }
        $table->total_count();
        break;

    // 当直登録
    case 'duty':
        if ($ward->is_administrator($emp->emp_id())) {
            $table->save_duty($data);
        }
        $table->total_count();
        break;

    // 再計算
    case 'reload':
        $table->set_table($data);
        $table->total_count();
        break;

    // チェック
    case 'check':
        break;

    // デフォルト
    default:
        $table->table();
        $table->total_count();
        break;
}

//----------------------------------------------------------
// JSON
//----------------------------------------------------------
header('Content-Type: application/json; charset=EUC-JP');
print $table->table_json();
