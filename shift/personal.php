<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Personal.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$year = $_REQUEST['y'];
$month = $_REQUEST['m'];
$form_mode = $_REQUEST["mode"];

//----------------------------------------------------------
// submit
//----------------------------------------------------------
if (!empty($_POST["submit_hope"])) {
    $submit = 'hope';
}
if (!empty($_POST["submit_approval"])) {
    $submit = 'approval';
}
if (!empty($_POST["submit_manager_approval"])) {
    $submit = 'manager_approval';
}
if (!empty($_POST["submit_boss_approval"])) {
    $submit = 'boss_approval';
}

//----------------------------------------------------------
// 表示モード
//----------------------------------------------------------
if (isset($_REQUEST['view_mode']) && $_REQUEST['view_mode'] !== '0') {
    $emp_id = $_REQUEST['view_emp_id'];
    $mode = 'month';
    $view_mode = $_REQUEST['view_mode'];
    $approve_id = $_REQUEST['approve_id'];
}
else {
    $emp_id = $emp->emp_id();
    $view_mode = 0;
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$personal_hope_use = $conf->get('shift.personal_hope_use');
$zangyo_limit_days = $conf->get('shift.zangyo_limit_days');

//----------------------------------------------------------
// ログインユーザの職員データ
//----------------------------------------------------------
$person = new Cmx_Shift_Personal($emp_id, $mode);
$person->set_login_id($emp->emp_id());
$staff = $person->get_staff();
$ward = $person->get_ward();
$sdate = $person->sdate();

// 表示できる病棟無し
if (!$staff->group_id()) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward', $ward);
    $view->assign('staff', $staff);
    $view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');
    $view->display('shift/personal.tpl');
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
}

//----------------------------------------------------------
// 希望入力期限
//----------------------------------------------------------
$hope_limit = false;
$hope_limit_day = date('Y-m-d', strtotime($sdate->start_date($year, $month)) - (86400 * ($ward->close_day() + 1)));
if ($hope_limit_day >= date('Y-m-d')) {
    $hope_limit = true;
}

//----------------------------------------------------------
// 勤務希望登録
//----------------------------------------------------------
if ($hope_limit && $form_mode === 'confirm' && $submit === 'hope') {
    $person->save_hope($_POST);
}

//----------------------------------------------------------
// 月次承認
//----------------------------------------------------------
if ($form_mode === 'confirm' && $submit === 'approval') {
    $person->save_self_approval($year, $month);
}
else if ($form_mode === 'confirm' && $submit === 'manager_approval') {
    $person->save_manager_approval($year, $month);
}
else if ($form_mode === 'confirm' && $submit === 'boss_approval') {
    $person->save_boss_approval($year, $month);
}

//----------------------------------------------------------
// 残業入力期限
//----------------------------------------------------------
$zan_limit = $zangyo_limit_days ? date('Ymd', strtotime(date('Y-m-d')) - ($zangyo_limit_days * 86400)) : '00000000';

//----------------------------------------------------------
// 時短入力
//----------------------------------------------------------
$is_shorter = $staff->is_shorter($sdate->start_date($year, $month));

//----------------------------------------------------------
// 年月リスト
//----------------------------------------------------------
$yearmonth = array();
foreach (range(-4, 4) as $i) {
    list($y, $m) = $sdate->months_add($year, $month, $i);
    list($vy, $vm) = $sdate->view_cours($y, $m);
    $yearmonth[] = array(
        'year' => $y,
        'month' => $m,
        'vyear' => $vy,
        'vmonth' => $vm,
        'i' => $i,
    );
}

//----------------------------------------------------------
// クール or 月
//----------------------------------------------------------
if ($mode === 'month' or $ward->month_type() !== '2') {
    $tmpl = 'shift/personal_month.tpl';
}
else {
    $tmpl = 'shift/personal.tpl';
}

//----------------------------------------------------------
// リスト＆合計
//----------------------------------------------------------
list($lists, $total) = $person->lists($year, $month);
$holiday = $person->holiday($year, $month);

//----------------------------------------------------------
// 月次承認済み
//----------------------------------------------------------
$approved = $person->approved($year, $month);

//----------------------------------------------------------
// 最終承認末日
//----------------------------------------------------------
$last_approved_date = $person->last_approved_date();

//----------------------------------------------------------
// 承認ボタン有効
//----------------------------------------------------------
if (empty($approved[0]) && !$total['is_empty_result']) {
    $is_month_approval = 1;
}
if ($staff->retire_date()) {
    // 退職日が入っていたら承認可能にする
    $is_month_approval = 1;
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 年月
$view->assign('year', $year);
$view->assign('lastyear', $year - 1);
$view->assign('nextyear', $year + 1);
$view->assign('month', $month);
$view->assign('yearmonth', $yearmonth);

// 勤務パターンプルダウンリスト(関数)
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $ward->pattern_sign_list());

// 病棟
$view->assign('ward', $ward);

// 職員
$view->assign('staff', $staff);

//リスト
$view->assign('lists', $lists);
$view->assign('total', $total);

//休暇日数
$view->assign('holiday', $holiday);

// 残業入力期限
$view->assign('zan_limit', $zan_limit);

// 時短入力権限
$view->assign('is_shorter', $is_shorter);

// 承認ボタン
$view->assign('is_month_approval', $is_month_approval);

// 承認済み
$view->assign('approved', $approved);
$view->assign('last_approved_date', $last_approved_date);

// 希望入力
$view->assign('personal_hope_use', $personal_hope_use);

// 希望入力期限
$view->assign('hope_limit', $hope_limit);
$view->assign('hope_limit_day', $hope_limit_day);

// 曜日
$view->assign('wj', array('日', '月', '火', '水', '木', '金', '土'));

// 今日
$view->assign('today', date('Ymd'));

// 表示モード
$view->assign('view_emp_id', $emp_id);
$view->assign('view_mode', $view_mode);

//全画面
if ($view_mode === '999') {
    $view->assign('fullscreen', 1);
    $view->assign('approve_id', $approve_id);
}

$view->display($tmpl);
