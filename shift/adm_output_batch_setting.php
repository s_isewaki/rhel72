<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// ダウンロード用データ
//----------------------------------------------------------
$files = glob(trim(dirname(__FILE__)) . '/csv/*.csv');
$filelist = array();
foreach ($files as $key => $file) {
    if (is_file($file)) {
        $filelist[$key]['name'] = basename($file);
        $filelist[$key]['timestamp'] = date("Y-m-d H:i:s", filemtime($file));
        $sort[] = $filelist[$key]['timestamp'];
    }
}
array_multisort($sort, SORT_DESC, $filelist);

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$csv_output_month_ago = $conf->get('shift.csv_output_month_ago');

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// 登録確認
//----------------------------------------------------------
if ($mode === 'confirm') {
    $csv_output_month_ago = $_POST["csv_output_month_ago"];

    // CSV出力月数
    if ($csv_output_month_ago === '' || is_null($csv_output_month_ago)) {
        $err["csv_output_month_ago"] = '入力されていません';
    }
    if (($csv_output_month_ago && preg_match("/\D/", $csv_output_month_ago)) || $csv_output_month_ago === '0') {
        $err["csv_output_month_ago"] = '異常な値です';
    }
    else if ((int)$csv_output_month_ago > 6) {
        $err["csv_output_month_ago"] = '6以下に設定して下さい';
    }
}

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' && empty($err)) {
    $conf->set('shift.csv_output_routinedate', $_POST['csv_output_routinedate']);
    $conf->set('shift.csv_output_month_ago', $_POST['csv_output_month_ago']);
    $conf->set('shift.csv_output_nextday', $_POST['csv_output_nextday']);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

$view->assign('err', $err);

if(!$conf->get('shift.csv_output_nextday') && !$conf->get('shift.csv_output_routinedate')) {
    $view->assign('message', "CSVファイルは出力されません");
}
else if($conf->get('shift.csv_output_nextday') && !$conf->get('shift.csv_output_routinedate')) {
    $view->assign('message', "CSVファイルは翌日1時に一度だけ出力されます");
}

$view->assign('csv_output_nextday', $conf->get('shift.csv_output_nextday'));
$view->assign('csv_output_month_ago', $csv_output_month_ago);
$view->assign('csv_output_routinedate', $conf->get('shift.csv_output_routinedate'));

$view->assign('days', range(1, 31));
$view->assign('filelist', $filelist);

$view->display('shift/adm_output_batch_setting.tpl');
