<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Pattern.php');
require_once('Cmx/Model/Shift/PatternCount.php');
require_once('Cmx/Model/EmployeeJob.php');
require_once('Cmx/Shift/Utils.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];
$pattern_id = $_REQUEST['pattern_id'];

//----------------------------------------------------------
// 初期値
//----------------------------------------------------------
$err = array();

//----------------------------------------------------------
// パターン
//----------------------------------------------------------
$pattern = new Cmx_Shift_Pattern();

//----------------------------------------------------------
// 記号リスト
//----------------------------------------------------------
$grp_list = $pattern->grp_lists();
if (!$pattern_id) {
    $pattern_id = $grp_list[0]->group_id();
}
$pattern->set_group_id($pattern_id);

//----------------------------------------------------------
// 集計
//----------------------------------------------------------
$pattern_count = new Cmx_Shift_PatternCount($pattern_id);

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm' and empty($err)) {
    $pattern_count->update($_POST);
    header("Location: " . CMX_BASE_URL . "/shift/adm_count.php?pattern_id={$pattern_id}");
    exit;
}

//----------------------------------------------------------
// 職種
//----------------------------------------------------------
$emp_job = new Cmx_EmployeeJob();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// 勤務パターングループID
$view->assign('pattern_id', $pattern_id);

// 勤務パターングループ
$view->assign('pgroup_lists', $pattern->grp_lists());

// 勤務パターンプルダウンリスト(関数)
$view->register_function('pulldown', 'smarty_pulldown');
$view->register_function('pattern_pulldown', 'smarty_pattern_pulldown');

// 勤務パターンリスト
$view->assign('pattern_list', $pattern->sign_lists());

// 集計名リスト
$view->assign('count_name_list', $pattern_count->name_lists());
$view->assign('count_pattern_list', $pattern_count->pattern_lists($pattern->ptn4_hash()));

// 集計値
$view->assign('count_value', $pattern_count->values());

// 職種リスト
$view->assign('emp_job', $emp_job);

$view->display('shift/adm_count.tpl');
