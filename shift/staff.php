<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Team.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_REQUEST['group_id'];
$year = $_REQUEST['y'];
$month = $_REQUEST['m'];

//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($mode === 'confirm') {
    $ward = new Cmx_Shift_Ward($group_id);
    $ward->staff_update($_POST);

    header("Location: " . CMX_BASE_URL . "/shift/staff.php?y={$year}&m={$month}&group_id={$group_id}");
    exit;
}

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
$group_id = $_GET['group_id'];
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
if (is_null($this_ward) and $ward_lists) {
    $this_ward = $ward_lists[0];
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', array());
    $view->display('shift/staff.tpl');
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
$sdate = $this_ward->shift_date();
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
    list($year, $month) = $sdate->months_add($year, $month, 1);
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff_list = $staff->master_lists($this_ward->group_id());

//----------------------------------------------------------
// 病棟チーム
//----------------------------------------------------------
$team = new Cmx_Shift_Team();
$team_list = $team->lists();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('month', $month);

// 病棟
$view->assign('this_ward', $this_ward);

// 病棟スタッフリスト
$view->assign('staff', $staff_list);

// 病棟チームリスト
$view->assign('team', $team_list);
$view->assign('team_rows', count($team_list) + 1);

$view->display('shift/staff.tpl');
