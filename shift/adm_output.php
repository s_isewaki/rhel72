<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/DataLayout.php';
require_once 'Cmx/Model/Shift/Ward.php';
require_once 'Cmx/View/Smarty.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// 年月
//----------------------------------------------------------
if (!$_GET) {
    $year = date('Y');
    $month = date('m');
}
else {
    $year = $_GET['year'];
    $month = $_GET['month'];
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$dl = new Cmx_Shift_DataLayout();
$ward = new Cmx_Shift_Ward();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

// 病棟
$view->assign('ward_lists', $ward->lists());

// レイアウト
$view->assign('layout_lists', $dl->lists());

$view->display('shift/adm_output.tpl');
