<?php

/**
 * CIS 病棟管理日誌連携データ 出力プログラム
 * shift/relation_batch_cis.php
 */
set_include_path('/var/www/html/comedix/');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/Shift/Date/Cours.php');

// 書出し先の定義
define("_EXP_FILE_PATH_", "/renkei/cis/KINMU/");

/**
 * 開発用フラグ
 * ・TRUE:開発 / FALSE:本番
 * ・書出し時、カンマ区切りにする
 */
$devflg = false;

// SystemConfig
$obj_conf = new Cmx_SystemConfig();
$base_year = $obj_conf->get('shift.cours.base_year');
$base_date = $obj_conf->get('shift.cours.base_date');
$base_count = $obj_conf->get('shift.cours.base_count');

// クールクラスのオブジェクト作成
$obj_cours = new Cmx_Shift_Date_Cours($base_date, $base_year, $base_count);

// 勤務表：病棟クラスのオブジェクト作成
$obj_ward = new Cmx_Shift_Ward();

// 病棟リスト取得
$list_ward = $obj_ward->kango_lists();

$all_data = array();
$kinmu_data = array();

// 5クール(先月、今月、翌月)分まわす
$year = date("Y");
$month = date("m");
foreach (range(-2, 2) as $i) {
    list($y, $m) = $obj_cours->months_add($year, $month, $i);
    echo("[$y/$m]\n");

    // 病棟分まわす
    foreach ($list_ward AS $value) {
        $group_id = $value->group_id();
        $sfc_group_code = $value->sfc_group_code();
        echo("$group_id: $sfc_group_code\n");

        // 勤務表：病棟スタッフのオブジェクト作成
        $obj_staff = new Cmx_Shift_Staff();

        // 勤務コードハッシュ生成
        $pattern = $value->pattern();
        $hash_pattern = $pattern->hash();

        // 勤務表：勤務表クラスのオブジェクト作成
        $obj_table = new Cmx_Shift_Table();
        $obj_table->set_ward($value);
        $obj_table->set_staff($obj_staff);
        $obj_table->set_year($y);
        $obj_table->set_month($m);
        $obj_table->table();

        //病棟に属してる職員データ
        $emp_arr = $obj_table->get_table_emp();
        if (!is_array($emp_arr)) {
            echo "Cmx_Shift_Table Error!\n";
            exit;
        }

        $dates = $obj_table->_ward->dates($y, $m);

        // 職員分回す
        $disp_num = "0";
        foreach ($emp_arr AS $emp_id => $data_arr) {
            echo("$emp_id: ");

            $disp_num++;
            $assist_flg = ($data_arr["assist"] == 1) ? TRUE : FALSE;

            // データ(日数)分回す
            foreach ($dates AS $day_of_duty => $date) {
                $shift_arr = $data_arr['dates']['d' . $day_of_duty];

                // 実行日より前は実績、実行日以降は空白
                if (strtotime($day_of_duty) < strtotime(date("Ymd"))) {
                    if ($assist_flg) {
                        if (isset($shift_arr["assist"]) && $shift_arr["assist"] == $group_id) {
                            if (isset($shift_arr["results"]) and $shift_arr["results"] !== '0') {
                                $day_code = $hash_pattern[$shift_arr["results"]]->sfc_code();
                            }
                            else {
                                //実行日以前だが、実績がない場合
                                $day_code = "  ";
                            }
                        }
                        else {
                            $day_code = "  ";
                        }
                    }
                    else {
                        if (isset($shift_arr["assist"]) and $shift_arr["assist"] != $group_id) {
                            $day_code = "  ";
                        }
                        else {
                            if (isset($shift_arr["results"]) and $shift_arr["results"] !== '0' and array_key_exists($shift_arr["results"], $hash_pattern)) {
                                $day_code = $hash_pattern[$shift_arr["results"]]->sfc_code();
                            }
                            else {
                                //実行日以前だが、実績がない場合
                                $day_code = "  ";
                            }
                        }
                    }
                }
                // 実行日後だったら予定
                else {
                    if ($assist_flg) {
                        if (isset($shift_arr["assist"]) && $shift_arr["assist"] == $group_id) {
                            if (isset($shift_arr["results"]) and $shift_arr["results"] !== '0' and array_key_exists($shift_arr["results"], $hash_pattern)) {
                                $day_code = $hash_pattern[$shift_arr["results"]]->sfc_code();
                            }
                            elseif (isset($shift_arr["plan"]) and $shift_arr["plan"] !== '0' and array_key_exists($shift_arr["plan"], $hash_pattern)) {
                                $day_code = $hash_pattern[$shift_arr["plan"]]->sfc_code();
                            }
                            else {
                                //実行日以前だが、実績がない場合
                                $day_code = "  ";
                            }
                        }
                        else {
                            $day_code = "  ";
                        }
                    }
                    else {
                        if (isset($shift_arr["assist"]) and $shift_arr["assist"] != $group_id) {
                            $day_code = "  ";
                        }
                        else {
                            if (isset($shift_arr["results"]) and $shift_arr["results"] !== '0' and array_key_exists($shift_arr["results"], $hash_pattern)) {
                                $day_code = $hash_pattern[$shift_arr["results"]]->sfc_code();
                            }
                            elseif (isset($shift_arr["plan"]) and $shift_arr["plan"] !== '0' and array_key_exists($shift_arr["plan"], $hash_pattern)) {
                                $day_code = $hash_pattern[$shift_arr["plan"]]->sfc_code();
                            }
                            else {
                                //実行日以前だが、実績がない場合
                                $day_code = "  ";
                            }
                        }
                    }
                }

                $day_of_first = substr($day_of_duty, 0, 6) . '01';
                $all_data[$group_id][$emp_id][$day_of_first] = makeDataArr($day_code, $sfc_group_code, $data_arr, $day_of_first, $disp_num);
                $kinmu_data[$group_id][$emp_id][$day_of_duty] = $day_code;
                echo("$day_code|");
            }
            echo("\n");
        }
        $obj_table = null;
        $obj_staff = null;
    }
}

// 整形
// ３か月(先月、今月、翌月)分まわす
$exp_data = array();
for ($i = 0; $i <= 2; $i++) {
    $year = date("Y");
    $month = date("m");

    if ($i == 0) {
        //先月分の年月
        $year = date("Y", mktime(0, 0, 0, $month - 1, 1, $year));
        $month = date("m", mktime(0, 0, 0, $month - 1, 1, $year));
    }
    elseif ($i == 2) {
        //来月分の年月
        $year = date("Y", mktime(0, 0, 0, $month + 1, 1, $year));
        $month = date("m", mktime(0, 0, 0, $month + 1, 1, $year));
    }
    $days_this_month = cal_days_in_month(CAL_GREGORIAN, $month, $year); // 該当月の日数
    // 病棟分まわす
    foreach ($all_data AS $group_id => $emp_arr) {
        // 職員分まわす
        $seq = 1;
        foreach ($emp_arr AS $emp_id => $date_arr) {
            if (!isset($all_data[$group_id][$emp_id][$year . $month . '01'])) {
                continue;
            }
            $month_code = "";

            // 職員情報
            $all_data[$group_id][$emp_id][$year . $month . '01']['DISP_SEQ'] = sprintf('%02d', $seq++);
            $exp_data[$group_id . "-" . $emp_id . "-" . $year . $month] = $all_data[$group_id][$emp_id][$year . $month . '01'];

            // 該当月の日数分回す
            for ($daynum = 1; $daynum <= $days_this_month; $daynum++) {
                $day = sprintf("%02d", $daynum);
                $Ymd = $year . $month . $day;
                if ($kinmu_data[$group_id][$emp_id][$Ymd]) {
                    $month_code .= $kinmu_data[$group_id][$emp_id][$Ymd];
                }
                else {
                    $month_code .= "  ";
                }
            }

            // 2Byte/1日*31,左詰め,後ろスペース埋め対応
            if ($days_this_month < '31') {
                for ($addspace = $days_this_month; $addspace < 31; $addspace++) {
                    $month_code .= "  ";
                }
            }
            $exp_data[$group_id . "-" . $emp_id . "-" . $year . $month]["WS_CODE"] = $month_code;
        }
    }
}

// データ書き出し
$fdata = "";
foreach ($exp_data AS $row => $data_arr) {
    $tmp_data = "";
    foreach ($data_arr AS $column => $val) {
        if (!preg_match("/^_/", $column)) {
            $tmp_data .= ($devflg) ? $val . "," : $val;
        }
    }
    if ($devflg) {
        $tmp_data = rtrim($tmp_data, ",");
    }
    $fdata .= $tmp_data . "\n";
}

$tx_date = date("Ymd");
$tx_time = date("H.i.s");
$fpath = _EXP_FILE_PATH_;
$fname = "HCECA" . $tx_date . ".dat";
$fp = fopen($fpath . $fname, 'ab');
flock($fp, LOCK_EX);
ftruncate($fp, 0);

$search = array("_SPACE3_", "_SPACE6_", "_TX_DATE_", "_TX_TIME_");
$replace = array("   ", "      ", $tx_date, $tx_time);

fwrite($fp, mb_convert_encoding(str_replace($search, $replace, $fdata), 'SJIS', 'EUC-JP'));
fclose($fp);
chmod($fpath . $fname, 0666);

echo("OK\n");

/**
 * 勤務日から該当月の初日、該当月の最終日を取得
 *
 * @param  str $day_of_duty 勤務日
 * @return str $firstday    該当月初日
 * @return str $lastday     該当月最終日
 */
function getDates($day_of_duty)
{
    $firstday = date("Ym", strtotime($day_of_duty)) . "01";
    $lastday = date("Ymt", strtotime($firstday));

    return array($firstday, $lastday);
}

/**
 * 職員氏名・カナの固定長化したものを取得
 *
 * ・渡された値が20バイトを超えた場合、20バイトで後ろを切る
 * ・渡された値が20バイト以下の場合は半角空白で埋める
 *
 * @param  str $val  職員氏名、またはカナ
 * @param  int $type 種別 [FALSE]氏名 [TRUE]カナ
 * @return str $name 固定長対応された職員氏名、またはカナ
 */
function fixedLengthName($val, $type = FALSE)
{
    $name = $val;
    if ($type) {
        $name = mb_convert_encoding($name, "SJIS", "EUC-JP"); // EUC-JP=>SJIS に変換
        $name = mb_convert_kana($name, "khnrs", "SJIS");      // SJISのまま文字を半角に変換
    }
    $name = mb_strcut($name, 0, 20);
    $name = sprintf("%-20.20s", $name);
    if ($type) {
        $name = mb_convert_encoding($name, "EUC-JP", "SJIS"); // SJIS=>EUC-JPに変換
    }

    return $name;
}

/**
 * 書出し時に必要なデータを配列にして返す
 *
 * @param  str $day_code    勤務コード
 * @param  str $sfc_g_code  病棟・部門コード
 * @param  arr $data        職員情報
 * @param  str $day_of_duty 勤務日
 * @param  int $disp_num    表示順
 * @return arr array()
 */
function makeDataArr($day_code, $sfc_g_code, $data, $day_of_duty, $disp_num)
{
    // 職員氏名・カナ 固定長対応
    $name = "";
    $kana = "";
    $name = fixedLengthName($data["name"]);
    $kana = fixedLengthName($data["kana"], TRUE);

    // 月初日、月末日の取得
    list($firstday, $lastday) = getDates($day_of_duty);

    return array(
        //"_assist_flg_" => $assist_flg,
        //"_group_name_" => $group_name,
        "_day_of_duty_" => $day_of_duty, // 勤務日
        "_code_" => $day_code, // 勤務コード
        "YYYYMM" => date("Ym", strtotime($day_of_duty)), // 該当年月
        "WARD_CODE" => sprintf("%-3s", $sfc_g_code), // 病棟・部門コード
        "SH_ID" => sprintf("%-8s", $data["login"]), // 職員ID
        "DISP_SEQ" => '99', // 表示順序
        "NS_TEAM" => "9999", // チーム
        "SH_ST_DATE" => $firstday, // 有効開始日
        "SH_ED_DATE" => $lastday, // 有効終了日
        "SH_NAME" => $name, // 看護職員名
        "SH_KANA_NAME" => $kana, // 半角カナ氏名
        "PS_ST_DATE" => $firstday, // 職位開始年月日
        "PS_ED_DATE" => $lastday, // 職位終了年月日
        "PS_CODE" => "9999", // 職位コード
        "AD_ST_DATE" => $firstday, // 採用開始年月日
        "AD_ED_DATE" => $lastday, // 採用終了年月日
        "AD_CODE" => "9999", // 採用コード
        "TYPE_ST_DATE" => $firstday, // 職種開始年月日
        "TYPE_ED_DATE" => $lastday, // 職種終了年月日
        "TYPE_CODE" => sprintf('%-4s', $data["job_code"]), // 職種コード
        "WD_ST_DATE" => $firstday, // 配置開始年月日
        "WD_ED_DATE" => $lastday, // 配置終了年月日
        "WARD_CODE1" => sprintf("%-3s", $sfc_g_code), // 病棟・部門コード(WARD_CODEと同じ)
        "WARD_CODE2" => "_SPACE3_", // 病棟・部門コード２
        "DTLMYYYYMM" => date("Ym", strtotime($day_of_duty)), // 勤務年月
        "WS_CODE" => "", // 勤務コード
        "ANS_YYYYMM" => "_SPACE6_", // 専門(准看)学歴卒業年月
        "NS_YYYYMM" => "_SPACE6_", // 専門(看)学歴卒業年月
        "MW_YYYYMM" => "_SPACE6_", // 専門(助)学歴卒業年月
        "PHN_YYYYMM" => "_SPACE6_", // 専門(保)学歴卒業年月
        "LAST_OP_ID" => "KINMU   ", // 更新者ＩＤ
        "LAST_DATE" => "_TX_DATE_", // 更新日
        "LAST_TIME" => "_TX_TIME_", // 更新時刻
    );
}

/**
 * ダメ人間関数
 */
// var_dumpのエイリアス
function d()
{
    foreach (func_get_args() as $v) var_dump($v);
}

// var_dump後にExitする
function e()
{
    foreach (func_get_args() as $v) var_dump($v);
    exit;
}

// CSV出力
function csv($data_arr, $type = 0)
{
    $sp = ($type == 0) ? "," : "\t";

    foreach ($data_arr AS $column => $val) {
        $tmp_data .= $val . $sp;
    }
    $tmp_data = rtrim($tmp_data, ",");
    echo $tmp_data . "\n";
}
