<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/SystemConfig.php');
require_once('Cmx/Shift/Notice.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$employee = unserialize($conf->get('shift.notice.employee'));
$email = unserialize($conf->get('shift.notice.email'));

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$mode = $_POST["mode"];

//----------------------------------------------------------
// 初期化
//----------------------------------------------------------
$err = array();


//----------------------------------------------------------
// 登録
//----------------------------------------------------------
if ($_POST['btn'] === 'save') {
    $conf->set('shift.notice.employee', serialize($_POST['employee']));
    $conf->set('shift.notice.email', serialize($_POST['email']));

    header("Location: " . CMX_BASE_URL . "/shift/adm_options_notice.php");
    exit;
}
else if ($_POST['btn'] === 'testmail') {
    $notice = new Cmx_Shift_Notice();
    $notice->test();

    header("Location: " . CMX_BASE_URL . "/shift/adm_options_notice.php");
    exit;
}

//----------------------------------------------------------
// 一覧
//----------------------------------------------------------
$emplist = array();
foreach ($employee as $emp_id) {
    $emplist[] = new Cmx_Employee($emp_id);
}

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('admin', true);

$view->assign('email', $email);
$view->assign('emplist', $emplist);

$view->display('shift/adm_options_notice.tpl');
