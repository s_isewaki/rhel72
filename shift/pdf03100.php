<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('fpdf153/mbfpdf.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];
$month = $_GET["month"];
if ($month === '13') {
    $month = 12;
}

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$adminlist = $ward->administrator();
$shift_admin = $ward->is_administrator($emp->emp_id());
$ward->dates($year, $month);

$count_row_lists = $ward->count_row_lists();
$count_col_lists = $ward->count_col_lists();

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->month_table($year, $month);
$table->total_count();

$start_date = date("Y/m/d", strtotime($table->start_ymd()));
$end_date = date("Y/m/d", strtotime($table->end_ymd()));

$Calendar = $table->dates();
$Data = $table->_tbl;
$Ptn = $table->_ptn;

//----------------------------------------------------------
// PDF
//----------------------------------------------------------
class CustomMBFPDF extends MBFPDF
{

    var $MOST_LEFT = 12;
    var $MOST_TOP = 10;
    var $HeaderData;

    function CustomMBFPDF($HeaderData_)
    {
        parent::MBFPDF('L', 'mm', 'A3'); // PHPでは継承元クラスのコンストラクタは明示的に呼び出す必要がある
        $this->AddMBFont(GOTHIC, "EUC-JP");
        $this->SetFont(GOTHIC, "", 10);
        $this->SetMargins($this->MOST_LEFT, $this->MOST_TOP);
        $this->SetFillColor(210);
        $this->HeaderData = $HeaderData_;
    }

    function Header()
    {

        $h1 = 4;
        $w1 = 8;
        $w2 = 11;

        $this->SetFont(GOTHIC, '', 8);
        $this->Cell(275, 3, '', 0, 0, 'C');

        $this->Cell(25, 3, '事 務 局 長', 0, 0, 'C');
        $this->Cell(25, 3, '事 務 部 長', 0, 0, 'C');
        $this->Cell(25, 3, '人 事 課 長', 0, 0, 'C');
        $this->Cell(25, 3, '　 部 長 　', 0, 0, 'C');
        $this->Cell(25, 3, '所 属 長', 0, 1, 'C');

        $this->Cell(275, 3, '', 0, 0, 'C');
        $this->Cell(25, 15, '印', 0, 0, 'C');
        $this->Cell(25, 15, '印', 0, 0, 'C');
        $this->Cell(25, 15, '印', 0, 0, 'C');
        $this->Cell(25, 15, '印', 0, 0, 'C');
        $this->Cell(25, 15, '印', 0, 1, 'C');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->HeaderData[3], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 16);
        $this->Cell(30, 10, $this->HeaderData[4] . '/' . $this->HeaderData[5], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, $this->HeaderData[0] . ' - ' . $this->HeaderData[1], 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 16);
        $title = '実績簿';
        $this->Cell(50, 10, htmlspecialchars($title), 0, 0, 'L');

        $this->SetFont(GOTHIC, '', 12);
        $this->Cell(70, 10, '管理者:' . $this->HeaderData[2], 0, 1, 'L');

        //---------------------------------------------------------------
        // 一覧ヘッダ部
        //---------------------------------------------------------------
        $wk_Calendar = $this->HeaderData[6];
        $wk_count_row_lists = $this->HeaderData[7];

        $this->SetFont(GOTHIC, '', 9);
        $this->Cell($w1, $h1, '', 'LTR', 0, 'C');
        $this->Cell(18, $h1, '職位', 'LTR', 0, 'L');
        $this->Cell(27, $h1, '氏名', 'LTR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '日付', 1, 0, 'C');

        foreach ($wk_Calendar as $key1 => $val1) {
            $type = $wk_Calendar[$key1]['type'];
            $d = $wk_Calendar[$key1]['d'];
            $w = $wk_Calendar[$key1]['w'];
            $this->func_backColor($type, $w);
            $this->Cell($w1, $h1, $d, 1, 0, 'C', 1);
        }


        $this->Cell($w2, $h1, '当月', 'LTR', 0, 'C');
        $this->Cell($w2, $h1, '祝日等', 'LTR', 0, 'C');

        foreach ($wk_count_row_lists as $key1 => $val1) {
            $name = $wk_count_row_lists[$key1]['name'];
            $this->Cell($w2, $h1, $name, 'LTR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');

        // ２段目
        $this->Cell($w1, $h1, '', 'LBR', 0, 'C');
        $this->Cell(18, $h1, '職種', 'LBR', 0, 'L');
        $this->Cell(27, $h1, '', 'LBR', 0, 'L');
        $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '曜日', 1, 0, 'C');

        foreach ($wk_Calendar as $key1 => $val1) {
            $type = $wk_Calendar[$key1]['type'];
            $wj = $wk_Calendar[$key1]['wj'];
            $w = $wk_Calendar[$key1]['w'];
            $this->func_backColor($type, $w);
            $this->Cell($w1, $h1, $wj, 1, 0, 'C', 1);
        }

        $this->Cell($w2, $h1, '実働', 'LBR', 0, 'C');
        $this->Cell($w2, $h1, '実働', 'LBR', 0, 'C');
        foreach ($wk_count_row_lists as $key1 => $val1) {
            $this->Cell($w2, $h1, '', 'LBR', 0, 'C');
        }
        $this->Cell(1, $h1, '', 0, 1, 'C');
    }

    function func_backColor($type, $w)
    {
        //買取日・日曜・祝日
        if ($type == '8' or $type == '3') {
            $this->SetFillColor(245, 205, 166);
        }
        else {
            //土曜
            if ($w == '6') {
                $this->SetFillColor(157, 204, 224);
            }
            //日曜
            else if ($w == '0') {
                $this->SetFillColor(245, 205, 166);
            }
            //平日
            else {
                $this->SetFillColor(240, 240, 240);
            }
        }
    }

}

$HeaderData_ = array(
    $start_date     //期間start
    , $end_date      //期間end
    , $adminlist[0]->emp_name()  //担当者名
    , $ward->group_name()
    , $year
    , $month
    , $Calendar
    , $count_row_lists
);

$pdf = new CustomMBFPDF($HeaderData_);
$pdf->Open();
$pdf->AddPage();

$h1 = 4;
$h2 = 1;
$w1 = 8;
$w2 = 11;

//---------------------------------------------------------------
// 一覧データ部
//---------------------------------------------------------------
$pdf->SetFont(GOTHIC, '', 9);


// 職員数分ループ
foreach ($Data['emp'] as $emp_id => $emp) {

    $cnt++;

    //-------------------------------------------------------------------------------------
    //一行目[予定]
    $pdf->Cell($w1, $h1, strval($cnt), 'LTR', 0, 'R');
    $pdf->Cell(18, $h1, $emp['st_name'], 'LTR', 0, 'L');
    $pdf->Cell(27, $h1, $emp['name'], 'LTR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LTR', 0, 'C');
    $pdf->Cell($w2, $h1, '予定', 1, 0, 'C');

    foreach ($Calendar as $ckey1 => $cval1) {
        $type = $Calendar[$ckey1]['type'];
        $wj = $Calendar[$ckey1]['wj'];
        $w = $Calendar[$ckey1]['w'];
        $wk_date = 'd' . $ckey1;

        $ptn = $emp['dates']['d' . $ckey1];
        $pattern = $ptn['plan_pattern_id'] ? $Ptn[$ptn['plan_pattern_id']]->hash() : $hash;
        $plan = $ptn['plan'];
        $plan_sign = ($plan && $pattern[$plan]) ? $pattern[$plan]->font_name() : "";

        //文字数確認
        if (strlen($plan_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 7);
        }

        $pdf->func_backColor($type, $w);

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates'][$wk_date]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates'][$wk_date]['assist']) and $emp['dates'][$wk_date]['assist'] !== $group_id)) {
            $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
        }
        else {
            $pdf->Cell($w1, $h1, $plan_sign, 1, 0, 'C', 1);
        }

        if (strlen($plan_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 9);
        }
    }

    //[予定]当月実働
    $pdf->Cell($w2, $h1, $emp['wh_hour']['plan'], 1, 0, 'R');

    //[予定]祝日等実働
    $pdf->Cell($w2, $h1, $emp['wh_holi']['plan'], 1, 0, 'R');

    //[予定]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $count = $count_row_lists[$rkey1]['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['plan'])) {
            $total = $emp['count_row']['c' . $count]['plan'];
        }
        $pdf->Cell($w2, $h1, strval($total), 1, 0, 'R');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');

    //-------------------------------------------------------------------------------------
    //二行目[実績]
    $pdf->Cell($w1, $h1, '', 'LR', 0, 'C');
    $pdf->Cell(18, $h1, $emp['job_name'], 'LR', 0, 'L');
    $pdf->Cell(27, $h1, '', 'LR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LR', 0, 'C');
    $pdf->Cell($w2, $h1, '実績', 1, 0, 'C');

    foreach ($Calendar as $ckey1 => $cval1) {
        $type = $Calendar[$ckey1]['type'];
        $wj = $Calendar[$ckey1]['wj'];
        $w = $Calendar[$ckey1]['w'];
        $wk_date = 'd' . $ckey1;

        $ptn = $emp['dates']['d' . $ckey1];
        $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $hash;
        $results = $ptn['results'];
        $results_sign = ($results && $pattern[$results]) ? $pattern[$results]->font_name() : "";

        //文字数確認
        if (strlen($results_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 7);
        }

        $pdf->func_backColor($type, $w);

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates'][$wk_date]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates'][$wk_date]['assist']) and $emp['dates'][$wk_date]['assist'] !== $group_id)) {
            $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
        }
        else {
            $pdf->Cell($w1, $h1, $results_sign, 1, 0, 'C', 1);
        }

        if (strlen($results_sign) > 4) {
            $pdf->SetFont(GOTHIC, '', 9);
        }
    }

    //[実績]当月実働
    $pdf->Cell($w2, $h1, $emp['wh_hour']['results'], 1, 0, 'R');

    //[実績]祝日等実働
    $pdf->Cell($w2, $h1, $emp['wh_holi']['results'], 1, 0, 'R');

    //[実績]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $count = $count_row_lists[$rkey1]['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['results'])) {
            $total = $emp['count_row']['c' . $count]['results'];
        }
        $pdf->Cell($w2, $h1, strval($total), 1, 0, 'R');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');

    //-------------------------------------------------------------------------------------
    //三行目
    $pdf->Cell($w1, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell(18, $h1, '', 'LBR', 0, 'L');
    $pdf->Cell(27, $h1, '', 'LBR', 0, 'L');
    $pdf->Cell($w2, $h1, '', 'LBR', 0, 'C');
    $pdf->Cell($w2, $h1, '残業', 1, 0, 'C');

    $total_results_ovtm = 0;
    foreach ($Calendar as $ckey1 => $cval1) {
        $type = $Calendar[$ckey1]['type'];
        $w = $Calendar[$ckey1]['w'];
        $wk_date = 'd' . $ckey1;
        $results_ovtm = strval($emp['dates'][$wk_date]['results_ovtm']);    //残業時間(分)
        $total_results_ovtm = $total_results_ovtm + intval($results_ovtm);

        //文字数確認
        if (strlen($results_ovtm) > 4) {
            $pdf->SetFont(GOTHIC, '', 7);
        }

        $pdf->func_backColor($type, $w);

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates'][$wk_date]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates'][$wk_date]['assist']) and $emp['dates'][$wk_date]['assist'] !== $group_id)) {
            $pdf->Cell($w1, $h1, '', 1, 0, 'C', 1);
        }
        else {
            $pdf->Cell($w1, $h1, $results_ovtm, 1, 0, 'R', 1);
        }

        if (strlen($results_ovtm) > 4) {
            $pdf->SetFont(GOTHIC, '', 9);
        }
    }

    if ($total_results_ovtm == 0) {
        $pdf->Cell($w2, $h1, '', 1, 0, 'R');
    }
    else {
        $pdf->Cell($w2, $h1, strval($total_results_ovtm), 1, 0, 'R');
    }
    $pdf->Cell($w2, $h1, '', 1, 0, 'R');

    foreach ($count_row_lists as $rkey1 => $rval1) {
        $pdf->Cell($w2, $h1, '', 1, 0, 'R');
    }
    $pdf->Cell(1, $h1, '', 0, 1, 'R');
}

//---------------------------------------------------------------
// 一覧フッタ部 (空の１行)
//---------------------------------------------------------------
$pdf->Cell($w1 + 18 + 27, $h2, '', 'LTB', 0, 'L');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');

foreach ($Calendar as $key1 => $val1) {
    $pdf->func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
    $pdf->Cell($w1, $h2, '', 1, 0, 'C', 1);
}

$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
$pdf->Cell($w2, $h2, '', 'TB', 0, 'C');

$cnt = 0;
$cnt2 = count($count_row_lists);
foreach ($count_row_lists as $rkey1 => $rval1) {
    $cnt++;
    if ($cnt != $cnt2) {
        $pdf->Cell($w2, $h2, '', 'TB', 0, 'C');
    }
    else {
        $pdf->Cell($w2, $h2, '', 'TBR', 1, 'C');
    }
}

//---------------------------------------------------------------

foreach ($count_col_lists as $key1 => $val1) {
    $name = $count_col_lists[$key1]['name'];
    $count = $count_col_lists[$key1]['count'];

    $pdf->Cell($w1 + 18, $h1, $name, 1, 0, 'L');
    $pdf->Cell(27, $h1, '', 1, 0, 'L');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');

    foreach ($Calendar as $ckey1 => $cval1) {
        $type = $Calendar[$ckey1]['type'];
        $w = $Calendar[$ckey1]['w'];
        $pdf->func_backColor($type, $w);
        $total = '0';
        if (isset($Data[date]['d' . $ckey1]['count_col']['c' . $count]['plan'])) {
            $total = $Data[date]['d' . $ckey1]['count_col']['c' . $count]['plan'];
        }
        $pdf->Cell($w1, $h1, strval($total), 1, 0, 'R', 1);
    }
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $pdf->Cell($w2, $h1, '', 1, 0, 'C');
    $cnt = 0;
    $cnt2 = count($count_row_lists);
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $cnt++;
        if ($cnt != $cnt2) {
            $pdf->Cell($w2, $h1, '', 1, 0, 'C');
        }
        else {
            $pdf->Cell($w2, $h1, '', 1, 1, 'C');
        }
    }
}

$pdf->Output();
