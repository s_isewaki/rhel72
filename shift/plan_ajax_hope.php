<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');

//----------------------------------------------------------
// ���å����
//----------------------------------------------------------
$session = new Cmx_Session();
$session->qualify(0);

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// �ѥ�᡼��
//----------------------------------------------------------
$group_id = $_POST["group_id"];
$emp_id = substr($_POST["emp_id"], 1);
$date = substr($_POST["date"], 1);
$pattern = $_POST["pattern"];

//----------------------------------------------------------
// ����
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);

//----------------------------------------------------------
// ��������Ԥγ�ǧ
//----------------------------------------------------------
if (!$ward->is_administrator($emp->emp_id())) {
    header('HTTP/1.1 500 Internal Server Error');
    exit;
}

//----------------------------------------------------------
// ��̳ɽ
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_login_id($emp->emp_id());
$table->set_ward($ward);

//----------------------------------------------------------
// ��̳��˾����¸
//----------------------------------------------------------
$table->save_hope($emp_id, $date, $pattern);
