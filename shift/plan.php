<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/WardAccessUser.php');
require_once('Cmx/Model/Shift/WardAccessPermission.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/Model/Shift/WorkingHours.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET['group_id'];
$year = $_GET['y'];
$month = $_GET['m'];
$fullscreen = $_GET['fullscreen'];

//----------------------------------------------------------
// ログインユーザ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff($emp->emp_id());

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// ログインユーザの所属病棟
// $ward_listsに含まれていなかった場合は追加する
//----------------------------------------------------------
$staff_ward = $ward->staff_ward($emp->emp_id());
if (!is_null($staff_ward)) {
    $staff_ward_flg = false;
    foreach ($ward_lists as $ward) {
        if ($ward->group_id() === $staff_ward->group_id()) {
            $staff_ward_flg = true;
            break;
        }
    }
    if (!$staff_ward_flg) {
        $ward_lists[] = $staff_ward;
    }
}

//----------------------------------------------------------
// 応援病棟リスト
// $ward_listsに含まれていなかった場合は追加する
//----------------------------------------------------------
$ouen_ward = $ward->ouen_ward($emp->emp_id(), $year, $month);
if ($ouen_ward) {
    foreach ($ouen_ward as $ouen) {
        $ouen_ward_flg = false;
        foreach ($ward_lists as $ward) {
            if ($ward->group_id() === $ouen->group_id()) {
                $ouen_ward_flg = true;
                break;
            }
        }
        if (!$ouen_ward_flg) {
            $ward_lists[] = $ouen;
        }
    }
}

//----------------------------------------------------------
// 参照可能病棟リスト(職員)
// $ward_listsに含まれていなかった場合は追加する
//----------------------------------------------------------
$shift_access = false;
$access_user = new Cmx_Shift_WardAccessUser($group_id);
$auser_ward = $access_user->access_lists($emp->emp_id());
if ($auser_ward) {
    foreach ($auser_ward as $award) {
        $ward_flg = false;
        if ($group_id === $award->group_id()) {
            $shift_access = true;
        }
        foreach ($ward_lists as $ward) {
            if ($ward->group_id() === $award->group_id()) {
                $ward_flg = true;
                break;
            }
        }
        if (!$ward_flg) {
            $ward_lists[] = $award;
        }
    }
}

//----------------------------------------------------------
// 参照可能病棟リスト(属性)
// $ward_listsに含まれていなかった場合は追加する
//----------------------------------------------------------
$access_perm = new Cmx_Shift_WardAccessPermission($group_id);
$aperm_ward = $access_perm->access_lists($emp->emp_id());
if ($aperm_ward) {
    foreach ($aperm_ward as $aperm) {
        $ward_flg = false;
        if ($group_id === $aperm->group_id()) {
            $shift_access = true;
        }
        foreach ($ward_lists as $ward) {
            if ($ward->group_id() === $aperm->group_id()) {
                $ward_flg = true;
                break;
            }
        }
        if (!$ward_flg) {
            $ward_lists[] = $aperm;
        }
    }
}

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
if (is_null($this_ward) and $ward_lists) {
    if ($staff->group_id()) {
        $this_ward = new Cmx_Shift_Ward($staff->group_id());
    }
    else {
        $this_ward = $ward_lists[0];
    }
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', array());
    $view->display('shift/plan.tpl');
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
$sdate = $this_ward->shift_date();
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
}

//----------------------------------------------------------
// 未来の予定表を見て良いか
//----------------------------------------------------------
$shift_admin = $this_ward->is_administrator($emp->emp_id());
if (!$shift_admin and ! $shift_access and $this_ward->status($year, $month) !== 1) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', $ward_lists);
    $view->assign('this_ward', $this_ward);
    $view->assign('year', $year);
    $view->assign('month', $month);
    $view->assign('viewym', $sdate->view_cours($year, $month));
    $view->assign('future', 1);
    $view->display('shift/plan.tpl');
    exit;
}

//----------------------------------------------------------
// 年月リスト
//----------------------------------------------------------
$yearmonth = array();
$prev_year = null;
$prev_month = null;
$prev_start_date = null;
$prev_end_date = null;
foreach (range(-4, 4) as $i) {
    list($y, $m) = $sdate->months_add($year, $month, $i);
    if ($i === -1) {
        list($prev_year, $prev_month) = array($y, $m);
        $prev_start_date = $sdate->start_date($prev_year, $prev_month);
        $prev_end_date = $sdate->end_date($prev_year, $prev_month);
    }
    if (!$shift_admin and ! $shift_access and $i > 0 and $this_ward->status($y, $m) !== 1) {
        break;
    }
    list($vy, $vm) = $sdate->view_cours($y, $m);
    $yearmonth[] = array(
        'year' => $y,
        'month' => $m,
        'vyear' => $vy,
        'vmonth' => $vm,
        'i' => $i,
    );
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff_list = $staff->lists($this_ward->group_id(), $year, $month);

//----------------------------------------------------------
// 要稼働時間
//----------------------------------------------------------
$wh = new Cmx_Shift_WorkingHours($year, $month);
$whours = $wh->fetch();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($this_ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);
$table->table();
$table->total_count();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('shift_admin', $shift_admin);
$view->assign('shift_access', $shift_access);

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('lastyear', $year - 1);
$view->assign('nextyear', $year + 1);
$view->assign('month', $month);
$view->assign('yearmonth', $yearmonth);
$view->assign('viewym', $sdate->view_cours($year, $month));

// 病棟スタッフリスト
$view->assign('staff', $staff_list);

// 日付リスト
$view->assign('start_date', $sdate->start_date($year, $month));
$view->assign('end_date', $sdate->end_date($year, $month));
$view->assign('dates', $this_ward->dates($year, $month));

// 病棟
$count_row_lists = $this_ward->count_row_lists();
$view->assign('this_ward', $this_ward);
$view->assign('pattern_list', $this_ward->pattern_list());
$view->assign('pattern_json', $this_ward->pattern_json());
$view->assign('pattern_ok', cmx_json_encode($this_ward->pattern_ok_hash()));
$view->assign('count_row', $count_row_lists);
$view->assign('count_row_count', count($count_row_lists));
$view->assign('count_col', $this_ward->count_col_lists());

// 要稼働時間
$view->assign('whours', $whours);

// 勤務表
$view->assign('table', $table);

$arr_key = array("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
    "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
    "A", "S", "D", "F", "G", "H", "J", "K", "L",
    "Z", "X", "C", "V", "B", "N", "M");

// Key入力
$view->assign('arr_key', $arr_key);

// 全画面
$view->assign('fullscreen', $fullscreen);

// コピー
$view->assign(array(
    'prev_year' => $prev_year,
    'prev_month' => $prev_month,
    'prev_start_date' => $prev_start_date,
    'prev_end_date' => $prev_end_date,
));

$view->display('shift/plan.tpl');
