<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET['group_id'];
$year = $_GET['y'];
$month = $_GET['m'];
$fullscreen = $_GET['fullscreen'];

//----------------------------------------------------------
// ログインユーザが管理者になっている病棟リスト
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward();
$ward_lists = $ward->lists($emp->emp_id());

//----------------------------------------------------------
// 表示する病棟
// パラメータ指定があればその病棟
// 無ければリストの先頭の病棟
//----------------------------------------------------------
$this_ward = null;
foreach ($ward_lists as $ward) {
    if ($ward->group_id() === $group_id) {
        $this_ward = $ward;
    }
}
if (is_null($this_ward) and $ward_lists) {
    $this_ward = $ward_lists[0];
}
// 表示できる病棟無し
if (is_null($this_ward)) {
    $view = new Cmx_View();
    $view->assign('auth_user', $emp->is_shift());
    $view->assign('auth_admin', $emp->is_shift_admin());
    $view->assign('ward_lists', array());
    $view->display('shift/duty.tpl');
    exit;
}

//----------------------------------------------------------
// 日付
//----------------------------------------------------------
$sdate = $this_ward->shift_date();
if (empty($year)) {
    list($year, $month) = $sdate->months(date('Y-m-d'));
    list($year, $month) = $sdate->months_add($year, $month, 1);
}

//----------------------------------------------------------
// 年月リスト
//----------------------------------------------------------
$yearmonth = array();
foreach (range(-4, 4) as $i) {
    list($y, $m) = $sdate->months_add($year, $month, $i);
    list($vy, $vm) = $sdate->view_cours($y, $m);
    $yearmonth[] = array(
        'year' => $y,
        'month' => $m,
        'vyear' => $vy,
        'vmonth' => $vm,
        'i' => $i,
    );
}

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff_list = $staff->lists($this_ward->group_id(), $year, $month);

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($emp->is_shift_admin());
$table->set_ward($this_ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);
$table->table();
$table->total_count();

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();
// 権限
$view->assign('auth_user', $emp->is_shift());
$view->assign('auth_admin', $emp->is_shift_admin());
$view->assign('shift_admin', $this_ward->is_administrator($emp->emp_id()));

// 病棟リスト
$view->assign('ward_lists', $ward_lists);

// 年月
$view->assign('year', $year);
$view->assign('lastyear', $year - 1);
$view->assign('nextyear', $year + 1);
$view->assign('month', $month);
$view->assign('yearmonth', $yearmonth);

// 病棟スタッフリスト
$view->assign('staff', $staff_list);

// 日付リスト
$view->assign('start_date', $sdate->start_date($year, $month));
$view->assign('end_date', $sdate->end_date($year, $month));
$view->assign('dates', $this_ward->dates($year, $month));

// 病棟
$view->assign('this_ward', $this_ward);
$view->assign('pattern_json', $this_ward->pattern_json());

// 勤務表
$view->assign('table', $table);

// 全画面
$view->assign('fullscreen', $fullscreen);

$view->display('shift/duty.tpl');
