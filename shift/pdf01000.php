<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/Model/Shift/TablePdf.php');
require_once('Cmx/Model/Shift/WorkingHours.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$adminlist = $ward->administrator();
$shift_admin = $ward->is_administrator($emp->emp_id());
$sdate = $ward->shift_date($_GET["mode"]);
list($year, $month) = $sdate->convert_old_cours($_GET['year'], $_GET['month']);
$Calendar = $ward->dates($year, $month);

$count_row_lists = $ward->count_row_lists();
$count_col_lists = $ward->count_col_lists();

$start_date = $sdate->start_date($year, $month);
$end_date = $sdate->end_date($year, $month);

//----------------------------------------------------------
// 要稼働時間
//----------------------------------------------------------
$wh = new Cmx_Shift_WorkingHours($year, $month);
$whours = $wh->fetch();

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->set_year($year);
$table->set_month($month);

$table->table();
$table->total_count();

$pdf = new Cmx_Shift_TablePDF();
$pdf->set_group_id($group_id);
$pdf->set_start_date($start_date);
$pdf->set_end_date($end_date);
$pdf->set_admin_name($adminlist[0]->emp_name());
$pdf->set_group_name($ward->group_name());
$pdf->set_ym($_GET["year"], $_GET["month"]);
$pdf->set_calendar($Calendar);
$pdf->set_row_list($count_row_lists);
$pdf->set_col_list($count_col_lists);
$pdf->set_pattern($hash);
$pdf->set_whours($whours);

$pdf->Open();
$pdf->AddPage();
$pdf->Pdf_create($table->get_table(), $table->get_pattern());
$pdf->Output();
