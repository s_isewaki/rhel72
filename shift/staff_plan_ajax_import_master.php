<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/Staff.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_POST['group_id'];
$year = $_POST['year'];
$month = $_POST['month'];

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$staff->master_staff_copy($group_id, $year, $month);
