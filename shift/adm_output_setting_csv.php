<?php

set_include_path('../');
require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Model/Shift/DataLayout.php';

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift_admin()) {
    js_login_exit();
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$dl = new Cmx_Shift_DataLayout();

//----------------------------------------------------------
// レイアウト
//----------------------------------------------------------
$data = to_sjis($dl->layout_csv($_POST['id']));

//----------------------------------------------------------
// CSV
//----------------------------------------------------------
ob_clean();
header("Content-Disposition: attachment; filename=data_layout.csv");
header("Content-Type: application/octet-stream; name=data_layout.csv");
header("Content-Length: " . strlen($data));
echo($data);
ob_end_flush();
exit;
