<?php

set_include_path(dirname(dirname(__FILE__)));

require_once 'Cmx.php';
require_once 'Cmx/Model/Shift/DutyDate.php';

//-----------------------------------------------
// 処理年月日
//-----------------------------------------------
$this_date = $argv[1];
if (!$this_date) {
    $this_date = date('Ymd');
}

//-----------------------------------------------
// 明け処理
//-----------------------------------------------
$dutydate = new Cmx_Shift_DutyDate('ake_dakoku');
$dutydate->ake($this_date);
$dutydate->exec();
$dutydate->commit();
