<?php

ob_start();
set_include_path('../');
require('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');

//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];
$month = $_GET["month"];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$adminlist = $ward->administrator();
$shift_admin = $ward->is_administrator($emp->emp_id());
$ward->dates($year, $month);

$count_row_lists = $ward->count_row_lists();
$count_col_lists = $ward->count_col_lists();

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);

$table->set_year($year);
$table->set_month($month);


$table->month_table($year, $month);
$table->table();
$table->total_count();

$start_date = date("Y/m/d", strtotime($table->start_ymd()));
$end_date = date("Y/m/d", strtotime($table->end_ymd()));

$Calendar = $table->dates();
$Data = $table->_tbl;
$Ptn = $table->_ptn;

//----------------------------------------------------------
// ファイル名
//----------------------------------------------------------
$filename = 'xls03100_' . $duty_yyyy . sprintf("%02d", $duty_mm) . '.xls';
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
    $filename, $encoding, mb_internal_encoding());

//----------------------------------------------------------
// HTML作成
//----------------------------------------------------------
$data = "";
$data.="<html>";
$data.="<body>";

$data.="<style type=\"text/css\">";
$data.="table {";
$data.="table-layout:fixed;";
$data.="width:0;";
$data.="}";
$data.=".list td{border:#5279a5 solid 1px;}";
$data.="</style>";

//---------------------------------------------------------------
// 一覧ヘッダ部
//---------------------------------------------------------------
$data.=func_head($ward->group_name(), $year, $month, $start_date, $end_date, $adminlist[0]->emp_name(), $Calendar, $count_row_lists);

$cnt = 0;


// 職員数分ループ
foreach ($Data['emp'] as $emp_id => $emp) {

    $cnt++;
    $data.="<tr>";
    $data.="<td style=\"text-align:right;vertical-align:top;\" rowspan=\"3\">" . strval($cnt) . "</td>";
    $data.="<td style=\"vertical-align:top;\" rowspan=\"3\">" . $emp['st_name'] . "<br>" . $emp['job_name'] . "</td>";
    $data.="<td style=\"vertical-align:top;\" rowspan=\"3\">" . $emp['name'] . "</td>";
    $data.="<td rowspan=\"3\">&nbsp;</td>";
    $data.="<td style=\"text-align: center;\">予定</td>";

    foreach ($Calendar as $ckey1 => $cval1) {
        $ptn = $emp['dates']['d' . $ckey1];
        $pattern = $ptn['plan_pattern_id'] ? $Ptn[$ptn['plan_pattern_id']]->hash() : $hash;
        $plan = $ptn['plan'];
        $plan_sign = ($plan && $pattern[$plan]) ? $pattern[$plan]->font_name() : "&nbsp;";

        if (strlen($plan_sign) > 2 && $plan_sign != "&nbsp;") {
            $plan_sign = substr($plan_sign, 0, 4);
        }

        $rtn = func_backColor($Calendar[$ckey1]['type'], $Calendar[$ckey1]['w']);
        $backcolor = "background-color:" . $rtn;

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $ckey1]['assist']) and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)) {
            $data.="<td width=\"20\" style=\"text-align:center;font-size:8px;" . $backcolor . "\"></td>";
        }
        else {
            $data.="<td width=\"20\" style=\"text-align:center;font-size:8px;" . $backcolor . "\">" . $plan_sign . "</td>";
        }
    }

    $data.="<td style=\"text-align: right;\">" . $emp['wh_hour']['plan'] . "</td>";  //[予定]当月実働
    $data.="<td style=\"text-align: right;\">" . $emp['wh_holi']['plan'] . "</td>";  //[予定]祝日等実働
    //[予定]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $count = $count_row_lists[$rkey1]['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['plan'])) {
            $total = $emp['count_row']['c' . $count]['plan'];
        }
        $data.="<td style=\"text-align: right;\">" . strval($total) . "</td>";
    }

    $data.="</tr>";
    //二行目[実績]
    $data.="<tr>";
    $data.="<td style=\"text-align: center;\">実績</td>";

    foreach ($Calendar as $ckey1 => $cval1) {
        $ptn = $emp['dates']['d' . $ckey1];
        $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $hash;
        $results = $ptn['results'];
        $results_sign = ($results && $pattern[$results]) ? $pattern[$results]->font_name() : "&nbsp;";

        $rtn = func_backColor($Calendar[$ckey1]['type'], $Calendar[$ckey1]['w']);
        $backcolor = "background-color:" . $rtn;

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $ckey1]['assist']) and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)) {
            $data.="<td width=\"20\" style=\"text-align:center;font-size:8px;" . $backcolor . "\"></td>";
        }
        else {
            $data.="<td width=\"20\" style=\"text-align:center;font-size:8px;" . $backcolor . "\">" . $results_sign . "</td>";
        }
    }

    $data.="<td style=\"text-align: right;\">" . $emp['wh_hour']['results'] . "</td>";     //[実績]当月実働
    $data.="<td style=\"text-align: right;\">" . $emp['wh_holi']['results'] . "</td>";     //[実績]祝日等実働
    //[実績]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $count = $count_row_lists[$rkey1]['count'];
        $total = '0';
        if (isset($emp['count_row']['c' . $count]['results'])) {
            $total = $emp['count_row']['c' . $count]['results'];
        }
        $data.="<td style=\"text-align: right;\">" . strval($total) . "</td>";
    }

    $data.="</tr>";
    //三行目[空行]
    $data.="<tr>";
    $data.="<td style=\"text-align: center;\">残業</td>";

    $total_results_ovtm = 0;
    foreach ($Calendar as $ckey1 => $cval1) {
        $wk_date = 'd' . $ckey1;
        $results_ovtm = strval($emp['dates'][$wk_date]['results_ovtm']);    //残業時間(分)
        $total_results_ovtm = $total_results_ovtm + intval($results_ovtm);

        $rtn = func_backColor($Calendar[$ckey1]['type'], $Calendar[$ckey1]['w']);
        $backcolor = "background-color:" . $rtn;

        // 他病棟勤務は表示しない
        if (($emp['assist'] === '1' and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)
            or ( $emp['assist'] !== '1' and ! empty($emp['dates']['d' . $ckey1]['assist']) and $emp['dates']['d' . $ckey1]['assist'] !== $group_id)) {
            $data.="<td style=\"" . $backcolor . "\"></td>";
        }
        else {
            $data.="<td style=\"" . $backcolor . "\">" . strval($results_ovtm) . "</td>";
        }
    }

    if ($total_results_ovtm == 0) {
        $data.="<td>&nbsp;</td>";
    }
    else {
        $data.="<td>" . strval($total_results_ovtm) . "</td>";
    }

    $data.="<td>&nbsp;</td>";

    //[実績]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $data.="<td>&nbsp;</td>";
    }
    $data.="</tr>";
}

//---------------------------------------------------------------
// 一覧フッタ部 (空の１行)
//---------------------------------------------------------------
$data.="</tr>";
$data.="<tr height=\"5\">";
$data.="<td>&nbsp;</td>";
$data.="<td>&nbsp;</td>";
$data.="<td>&nbsp;</td>";
$data.="<td>&nbsp;</td>";
$data.="<td>&nbsp;</td>";
foreach ($Calendar as $key1 => $val1) {
    $rtn = func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
    $backcolor = "background-color:" . $rtn;
    $data.="<td width=\"20\" style=\"" . $backcolor . "\">&nbsp;</td>";
}
$data.="<td>&nbsp;</td>";
$data.="<td>&nbsp;</td>";

//[実績]日勤�供μ覿弌γ羔弌�週休 等
foreach ($count_row_lists as $rkey1 => $rval1) {
    $data.="<td>&nbsp;</td>";
}
$data.="</tr>";

//---------------------------------------------------------------

foreach ($count_col_lists as $key1 => $val1) {
    $name = $count_col_lists[$key1]['name'];
    $count = $count_col_lists[$key1]['count'];

    $data.="<tr>";
    $data.="<td colspan=\"2\">" . $name . "</td>";
    $data.="<td>&nbsp;</td>";
    $data.="<td>&nbsp;</td>";
    $data.="<td>&nbsp;</td>";

    foreach ($Calendar as $key1 => $val1) {
        $total = '0';
        if (isset($Data[date]['d' . $key1]['count_col']['c' . $count]['plan'])) {
            $total = $Data[date]['d' . $key1]['count_col']['c' . $count]['plan'];
        }
        $rtn = func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
        $backcolor = "background-color:" . $rtn;
        $data.="<td width=\"20\" style=\"text-align: right;" . $backcolor . "\">" . strval($total) . "</td>";
    }

    $data.="<td>&nbsp;</td>";
    $data.="<td>&nbsp;</td>";
    //[実績]日勤�供μ覿弌γ羔弌�週休 等
    foreach ($count_row_lists as $rkey1 => $rval1) {
        $data.="<td>&nbsp;</td>";
    }
    $data.="</tr>";
}

$data.="</table>";
$data.="</body>";
$data.="</html>";

///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------

ob_clean();

header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo to_utf8(nl2br($data));
ob_end_flush();

///-----------------------------------------------------------------------------
/// ヘッダ部生成
///-----------------------------------------------------------------------------
function func_head($group_name, $year, $month, $start_date, $end_date, $emp_name, $Calendar, $count_row_lists)
{

    $data = "<table border=0 style=\"\">";
    $data.="<tr height=\"2\">";
    $data.="</tr>";
    $data.="<tr height=\"45\">";
    $data.="<td width=\"20\">&nbsp;</td>";
    $data.="<td width=\"900\" colspan=\"27\">&nbsp;</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"4\">事 務 局 長</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"4\">事 務 部 長</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">人 事 課 長</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">部 長</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">所 属 長</td>";
    $data.="</tr>";
    $data.="<tr height=\"45\">";
    $data.="<td width=\"20\">&nbsp;</td>";
    $data.="<td width=\"900\" colspan=\"27\">&nbsp;</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"4\">印</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"4\">印</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">印</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">印</td>";
    $data.="<td width=\"100\" style=\"text-align:center;font-size:15px;\" colspan=\"2\">印</td>";
    $data.="</tr>";
    $data.="<tr>";
    $data.="</tr>";
    $data.="</table>";

    $data.="<table border=0 style=\"\">";
    $data.="<tr>";
    $data.="<td width=\"150\" style=\"text-align:left;font-size:12px;\" colspan=\"2\">" . $group_name . "</td>";
    $data.="<td width=\"300\" colspan=\"6\">&nbsp;" . $year . "/" . $month . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . str_replace('-', '/', $start_date) . "&nbsp;-&nbsp;" . str_replace('-', '/', $end_date) . "</td>";
    $data.="<td width=\"250\" style=\"text-align:center;font-size:18px;\" colspan=\"18\">実績簿</td>";
    $data.="<td width=\"200\" colspan=\"7\">管理者:" . $emp_name . "</td>";
    $data.="</tr>";
    $data.="<tr height=\"10\">";
    $data.="</tr>";
    $data.="</table>";

    $data.="<table border=1 rules=\"all\" style=\"font-size:10px;table-layout:fixed;\">";

    //横幅を揃える為
    $data.="<tr height=\"0\">";
    $data.="<td>" . nbsp(7) . "</td>";
    $data.="<td>" . nbsp(22) . "</td>";
    $data.="<td>" . nbsp(38) . "</td>";
    $nbsp = nbsp(7);
    $data.="<td>" . $nbsp . "</td>";
    $data.="<td>" . $nbsp . "</td>";
    $nbsp = nbsp(4);
    foreach ($Calendar as $key1 => $val1) {
        $data.="<td>" . $nbsp . "</td>";
    }
    $data.="<td>" . nbsp(14) . "</td>";
    $data.="<td>" . nbsp(14) . "</td>";
    $nbsp = nbsp(11);
    foreach ($count_row_lists as $key1 => $val1) {
        $data.="<td>" . $nbsp . "</td>";
    }
    $data.="</tr>";

    $data.="<tr>";
    $data.="<td width=\"20\" rowspan=\"2\">&nbsp;</td>";
    $data.="<td width=\"80\" rowspan=\"2\" style=\"vertical-align:top;\">職位<BR>職種</td>";
    $data.="<td width=\"150\" rowspan=\"2\" style=\"vertical-align:top;\">氏名</td>";
    $data.="<td width=\"40\" rowspan=\"2\">&nbsp;</td>";
    $data.="<td width=\"40\" style=\"text-align: center;\">日付</td>";
    foreach ($Calendar as $key1 => $val1) {

        $rtn = func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
        $backcolor = "background-color:" . $rtn;

        $data.="<td width=\"20\" style=\"text-align:center;" . $backcolor . "\">" . $Calendar[$key1]['d'] . "</td>";
    }
    $data.="<td width=\"60\" style=\"text-align: center;\" rowspan=\"2\">当月<br>実働</td>";
    $data.="<td width=\"60\" style=\"text-align: center;\" rowspan=\"2\">祝日等<br>実働</td>";

    foreach ($count_row_lists as $key1 => $val1) {
        $name = $count_row_lists[$key1]['name'];
        $data.="<td width=\"60\" style=\"text-align:center;\" rowspan=\"2\">" . $name . "</td>";
    }
    $data.="</tr>";

    // ２段目
    $data.="<tr>";
    $data.="<td style=\"text-align: center;\">曜日</td>";
    foreach ($Calendar as $key1 => $val1) {

        $rtn = func_backColor($Calendar[$key1]['type'], $Calendar[$key1]['w']);
        $backcolor = "background-color:" . $rtn;

        $data.="<td width=\"20\" style=\"text-align:center;" . $backcolor . "\">" . $Calendar[$key1]['wj'] . "</td>";
    }
    $data.="</tr>";

    return $data;
}

///-----------------------------------------------------------------------------
/// 背景色選択
///-----------------------------------------------------------------------------
function func_backColor($type, $w)
{
    $color = "#FFFFFF";
    //買取日・日曜・祝日
    if ($type == '8' or $type == '3') {
        $color = "#FFCC99";       //ベージュ
    }
    else {
        //土曜
        if ($w == '6') {
            $color = "#E0FFFF";   //薄い青
        }
        //日曜
        else if ($w == '0') {
            $color = "#C8FFD5";   //薄い緑
        }
        //平日
        else {
            $color = "#FFFFFF";   //白
        }
    }
    return $color;
}

function nbsp($cnt)
{
    $value = "";
    for ($i = 0; $i < $cnt; $i++) {
        $value = $value . "&nbsp;";
    }
    return $value;
}
