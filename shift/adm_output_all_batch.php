<?php

set_include_path(dirname(dirname(__FILE__)));

require_once 'Cmx.php';
require_once 'Cmx/Model/Session.php';
require_once 'Cmx/Shift/DataCSV.php';
require_once 'Cmx/Shift/Log.php';
require_once 'Cmx/Model/Shift/Ward.php';

//----------------------------------------------------------
// log
//----------------------------------------------------------
$log = new Cmx_Shift_Log('csv_batch');

//----------------------------------------------------------
// layout をコマンドライン引数で指定
//----------------------------------------------------------
// 月次
if (isset($argv[1]) && preg_match("/\D/", $argv[1])) {
    $layout = $argv[1];
}
else {
    $layout = '1';
}

// 遡及
if (isset($argv[2]) && preg_match("/\D/", $argv[2])) {
    $ret_layout = $argv[2];
}
else {
    $ret_layout = '2';
}

//----------------------------------------------------------
// SystemConfig
//----------------------------------------------------------
$conf = new Cmx_SystemConfig();
$csv_output_routinedate = $conf->get('shift.csv_output_routinedate');
$csv_output_month_ago = $conf->get('shift.csv_output_month_ago');
$csv_output_nextday = $conf->get('shift.csv_output_nextday');

if ($csv_output_tourinedate === '99') {
    $csv_output_routinedate = date('d', strtotime(date('Y-m-1') . '-1 day'));
}

if ($csv_output_nextday !== 'on' && date('d') !== $csv_output_routinedate) {
    exit;
}

//----------------------------------------------------------
// CSV格納ディレクトリ
//----------------------------------------------------------
$path = dirname(__FILE__) . "/csv/";
if (!file_exists($path)) {
    mkdir($path, 0755);
    chmod($path, 0755);
}

//----------------------------------------------------------
// obj
//----------------------------------------------------------
$log->log("出力開始", $argv);

$data = array();
for ($i = 0; $i < (int)$csv_output_month_ago; $i++) {
    $data['start_date'] = date('Y-m-d', strtotime(date('Y-m-1') . ' -' . $i . ' month'));
    $data['end_date'] = date('Y-m-d', strtotime(date('Y-m-1') . ' -' . $i . ' month, +1 month, -1 day'));
    if ($i < 2) {
        $data['layout'] = $layout;
    }
    else {
        $data['layout'] = $ret_layout;
    }
    foreach (array('4', '0') as $wage) {
        $data['wage'] = $wage;

        $csv = new Cmx_Shift_DataCSV($data);
        $csv_data = $csv->output(true);

        //----------------------------------------------------------
        // ファイル名
        // 時間勤務の場合は _t をつける
        // 遡及の場合は _k をつける
        //----------------------------------------------------------
        if ($wage === '0') {
            $filename = date('Ym', strtotime($data['start_date'])) . "_all";
        }
        else {
            $filename = date('Ym', strtotime($data['start_date'])) . "_all_t";
        }
        if ($data['layout'] === $ret_layout) {
            $filename = $filename . "_k";
        }

        //----------------------------------------------------------
        // CSV出力
        //----------------------------------------------------------
        $fp = fopen($path . $filename . ".csv", 'wb');
        fwrite($fp, $csv_data);
        fclose($fp);
    }

    // 時短
    $data['layout'] = 4;
    foreach (array('4', '0') as $wage) {
        $data['wage'] = $wage;

        $csv = new Cmx_Shift_DataCSV($data);
        $csv_data = $csv->output(true);

        // ファイル名
        if ($wage === '0') {
            $filename = date('Ym', strtotime($data['start_date'])) . "_all_jitan";
        }
        else {
            $filename = date('Ym', strtotime($data['start_date'])) . "_all_t_jitan";
        }

        // CSV出力
        $fp = fopen($path . $filename . ".csv", 'wb');
        fwrite($fp, $csv_data);
        fclose($fp);
    }
}

$conf->set('shift.csv_output_nextday', "");
$log->log("出力完了");
