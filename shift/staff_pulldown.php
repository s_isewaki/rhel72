<?php

set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');
require_once('Cmx/View/Smarty.php');

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_REQUEST['group_id'];
$year = $_REQUEST['year'];
$month = $_REQUEST['month'];
$mode = $_REQUEST['mode'];

//----------------------------------------------------------
// 年月から月末の年クールを割り出す
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();
$table = new Cmx_Shift_Table();
$table->set_ward($ward);
$table->set_staff($staff);
$lists = $table->month_staff_lists($year, $month);

//----------------------------------------------------------
// VIEW
//----------------------------------------------------------
$view = new Cmx_View();

// 勤務パターンリスト
$view->assign('lists', $lists);

if ($mode === 'pulldown') {
    $view->display('shift/staff_pulldown.tpl');
}
else {
    $view->display('shift/staff_checkbox.tpl');
}