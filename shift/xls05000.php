<?php

ob_start();
set_include_path('../');
require_once('Cmx.php');
require_once('Cmx/Model/Session.php');
require_once('Cmx/Model/Shift/Ward.php');
require_once('Cmx/Model/Shift/Staff.php');
require_once('Cmx/Model/Shift/Table.php');

define("KYUKA_KEKKON", "1032");         //結婚
define("KYUKA_KIBIKI", "1030");         //忌引
define("KYUKA_SONOTA", "1005");         //その他
define("KYUKA_ROUSAI", "1027");         //労災
define("KYUKA_BYOUKETU", "1007");       //病欠
define("KYUKA_JIKETU", "1006");         //事欠
define("KYUKA_SANKYU", "1025");         //産休
define("KYUKA_IKUKYU", "1026");         //育休
define("KYUKA_KYUSHOKU", "1028");       //休職
define("KYUKA_SYUKYU", "1040");         //週休
define("KYUKA_SYUKYU2", "1041");        //週休２
define("KYUKA_YUKYU", "1001");          //有休
define("KYUKA_YUKYUS", "1038");         //土曜有休
define("KYUKA_NATUKYU", "1031");        //夏休
define("KYUKA_1009", "1009");           //有宿
define("KYUKA_1010", "1010");           //夏宿
define("KYUKA_1011", "1011");           //週休土半
define("KYUKA_1012", "1012");           //週休土宿
define("KYUKA_1071", "1071");           //土週休＋待機

define("KYUKA_KANGO", "PI");            //看護(SFC_CODE)
define("KYUKA_KAIGO", "PJ");            //介護(SFC_CODE)
define("KYUKA_NONPAYBYOUKETU", "PK");   //病●(無給病欠)
define("KYUKA_KAIGOKYUGYOU", "PL");     //介●(介護休業)
define("KYUKA_SYUKYU_TOCHOKU", "PH");   //□当(週休＋宿直)

define("KINMU_PMSYUKYU", "OB");         //午後週休(SFC_CODE)
define("KINMU_AMSYUKYU", "OC");         //午前週休(SFC_CODE)
define("KINMU_PMYUKYU", "OD");          //午後有休(SFC_CODE)
define("KINMU_AMYUKYU", "OE");          //午前有休(SFC_CODE)
define("KINMU_PMNATUKYU", "OF");        //午後夏休(SFC_CODE)
define("KINMU_AMNATUKYU", "OG");        //午前夏休(SFC_CODE)
define("KINMU_SYUCCHOU", "OT");         //出張(SFC_CODE)
//----------------------------------------------------------
// セッション
//----------------------------------------------------------
$session = new Cmx_Session();
if (!$session->qualify()) {
    js_login_exit();
}

//----------------------------------------------------------
// 権限
//----------------------------------------------------------
$emp = $session->emp();
if (!$emp->is_shift()) {
    js_login_exit();
}

//----------------------------------------------------------
// パラメータ
//----------------------------------------------------------
$group_id = $_GET["group_id"];
$year = $_GET["year"];
$month = $_GET["month"];
$length = $_GET['length'];

//----------------------------------------------------------
// 病棟
//----------------------------------------------------------
$ward = new Cmx_Shift_Ward($group_id);
$shift_admin = $ward->is_administrator($emp->emp_id());
$ward->dates($year, $month);

//----------------------------------------------------------
// pattern
//----------------------------------------------------------
$pattern = $ward->pattern();
$hash = $pattern->hash();

//----------------------------------------------------------
// 病棟スタッフ
//----------------------------------------------------------
$staff = new Cmx_Shift_Staff();

//----------------------------------------------------------
// 勤務表
//----------------------------------------------------------
$table = new Cmx_Shift_Table();
$table->set_shift_admin($shift_admin);
$table->set_ward($ward);
$table->set_staff($staff);
$table->range_table($year, $month, $length);
$table->total_count();

$Calendar = $table->dates();
$Data = $table->_tbl;
$Ptn = $table->_ptn;

//----------------------------------------------------------
// ファイル名
//----------------------------------------------------------
$filename = 'xls05000_' . $duty_yyyy . sprintf("%02d", $duty_mm) . '.xls';
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
    $filename, $encoding, mb_internal_encoding());

//----------------------------------------------------------
// HTML作成
//----------------------------------------------------------
$data = "";
$data.="<html>";
$data.="<body>";

$data.="<style type=\"text/css\">";
$data.="table {";
$data.="table-layout:fixed;";
$data.="width:0;";
$data.="}";
$data.=".list td{border:#5279a5 solid 1px;}";
$data.="</style>";


//---------------------------------------------------------------
// 一覧ヘッダ部
//---------------------------------------------------------------

$today = getdate();

$data.="<table border=0 rules=\"all\" style=\"font-size:12px;table-layout:fixed;font-weight:bold;\">";
$data.="<tr height=\"2\">";
$data.="</tr>";
$data.="<tr>";
$data.="<td width=\"20\" style=\"border:0\">&nbsp;</td>";
$data.="<td width=\"900\" colspan=\"24\" style=\"border:0\">&nbsp;</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"2\">事務局長</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"2\">事務部長</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\">人事課長</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\">部長</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\">所属長</td>";
$data.="</tr>";

$data.="<tr>";
$data.="<td width=\"20\"></td>";
$data.="<td width=\"\" colspan=\"24\" style=\"border:0;text-align:center;font-size:18px;\">集計表&nbsp;&nbsp;&nbsp;" . $ward->group_name() . "</td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"2\" rowspan=\"3\"></td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"2\" rowspan=\"3\"></td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\" rowspan=\"3\"></td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\" rowspan=\"3\"></td>";
$data.="<td width=\"100\" style=\"text-align:center;font-size:10px;border:1\" colspan=\"1\" rowspan=\"3\"></td>";
$data.="</tr>";

$data.="<tr>";
if ($length > 1) {
    $data.="<td width=\"20\" style=\"border:0;text-align:left;font-size:12px;\">" . preg_replace("/^(\d\d\d\d)(\d\d)\d\d/", "$1年$2月", $table->start_ymd()) . " 〜 " . preg_replace("/^(\d\d\d\d)(\d\d)\d\d/", "$1年$2月", $table->end_ymd()) . "</td>";
}
else {
    $data.="<td width=\"20\" style=\"border:0;text-align:left;font-size:12px;\">" . $year . "年" . $month . "月" . "</td>";
}
$data.="<td width=\"\" colspan=\"24\" style=\"border:0;text-align:right;font-size:12px;\">" . $today[year] . "年" . $today[mon] . "月" . $today[mday] . "日作成" . "</td>";
$data.="</tr>";

$data.="<tr>";
$data.="<td width=\"\"></td>";
$data.="<td colspan=\"14\"></td>";
$data.="<td width=\"\"></td>";
$data.="<td colspan=\"4\"></td>";
$data.="<td width=\"\"></td>";
$data.="<td colspan=\"6\"></td>";
$data.="<td colspan=\"2\"></td>";
$data.="</tr>";

$data.="</table>";


$data.="<table border=1 rules=\"all\" style=\"font-size:12px;table-layout:fixed;font-weight:bold;\">";
$data.="<tr>";
$data.="<td rowspan=\"4\" style=\"text-align:center;\">氏名</td>";
$data.="<td rowspan=\"4\" style=\"text-align:center;\"><br>&nbsp;実&nbsp;<br>働</td>";
$data.="<td colspan=\"6\" style=\"text-align:center;\">特別休暇</td>";
$data.="<td colspan=\"8\" style=\"text-align:center;\">その他</td>";
$data.="<td colspan=\"12\" style=\"text-align:center;\">休暇消化状況</td>";

$data.="<td rowspan=\"4\" colspan=\"4\" style=\"text-align:center;\"><br>備考<br>(夏休当月消化日は[]括り)</td>";

$data.="</tr>";

$data.="<tr>";
$data.="<td rowspan=\"3\" style=\"text-align:center;vertical-align:center;\"><br>&nbsp;結&nbsp;<br>婚</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;忌&nbsp;<br>引</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;看&nbsp;<br>護</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;介&nbsp;<br>護</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\">&nbsp;そ&nbsp;<br>の<br>他</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;合&nbsp;<br>計</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;出&nbsp;<br>張</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;労&nbsp;<br>災</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;病&nbsp;<br>欠</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;事&nbsp;<br>欠</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;産&nbsp;<br>休</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;育&nbsp;<br>休</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;休&nbsp;<br>職</td>";
$data.="<td rowspan=\"3\" style=\"text-align:center;\"><br>&nbsp;合&nbsp;<br>計</td>";

$data.="<td colspan=\"3\" style=\"text-align:center;\">週休</td>";
$data.="<td colspan=\"2\" style=\"text-align:center;\">夏休</td>";
$data.="<td colspan=\"7\" style=\"text-align:center;\">年次有休休暇</td>";
$data.="</tr>";

$data.="<tr>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">当月 消化日<br>(□以外は日付＋AMPM,A,P)</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;合&nbsp;<br>計</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;累&nbsp;<br>計</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;合&nbsp;<br>計</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;累&nbsp;<br>計</td>";

$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;当月 消化日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>(日付＋S(半日),A,P)</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;合&nbsp;<br>計</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;繰&nbsp;<br>越</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;本&nbsp;<br>年</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;合&nbsp;<br>計</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;消&nbsp;<br>化</td>";
$data.="<td rowspan=\"2\" style=\"text-align:center;\">&nbsp;残&nbsp;<br>日</td>";
$data.="</tr>";
$data.="</table>";

$data.="<table border=1 rules=\"all\" style=\"font-size:10px;table-layout:fixed;\">";


// 職員数分ループ
foreach ($Data['emp'] as $emp_id => $emp) {

    // 集計
    $total[0] = 0;        //実働
    $total[1] = 0;        //結婚
    $total[2] = 0;        //忌引
    $total[24] = 0;       //看護
    $total[25] = 0;       //介護
    $total[3] = 0;        //その他
    $total[4] = 0;        //出張
    $total[5] = 0;        //労災
    $total[6] = 0;        //入病欠
    $total[7] = 0;        //病欠
    $total[8] = 0;        //事欠
    $total[9] = 0;        //産休
    $total[10] = 0;       //育休
    $total[11] = 0;       //休職
    $total[12] = 0;       //合計-特別休暇
    $total[13] = 0;       //合計-その他

    $total[14] = 0;       //合計-週休
    $total[15] = 0;       //合計-夏休
    $total[16] = 0;       //合計-有休
    $total[17] = 0;       //週休-累計
    $total[18] = 0;       //夏休-累計
    $total[19] = 0;       //有休-合計
    $total[20] = 0;       //有休-消化
    $total[21] = 0;       //有休-残数
    $total[22] = 0;       //有休-繰越
    $total[23] = 0;       //有休-本年

    $syukyu = "";
    $natukyu = "";
    $yukyu = "";

    // 日付ループ
    foreach ($Calendar as $day => $cval1) {
        $ptn = $emp['dates']['d' . $day];
        $pattern = $ptn['results_pattern_id'] ? $Ptn[$ptn['results_pattern_id']]->hash() : $hash;
        $value = $ptn['results'];
        $value_sign = ($value && $pattern[$value]) ? $pattern[$value]->sfc_code() : "";
        $value_reason = ($value && $pattern[$value]) ? $pattern[$value]->atd_reason() : "";

        switch ($value_sign) {
            case KINMU_PMSYUKYU:          //午後週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'P ';
                }
                break;
            case KINMU_AMSYUKYU:          //午前週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'A ';
                }
                break;
            case KINMU_PMNATUKYU:         //午前夏休
                $total[15] += 0.5;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . 'P]';
                }
                break;
            case KINMU_AMNATUKYU:         //午前夏休
                $total[15] += 0.5;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . 'A]';
                }
                break;
            case KINMU_PMYUKYU:           //午後有休
                $total[16] += 0.5;
                if ($length === '1') {
                    $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'P ';
                }
                break;
            case KINMU_AMYUKYU:           //午前有休
                $total[16] += 0.5;
                if ($length === '1') {
                    $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'A ';
                }
                break;
            case KINMU_SYUCCHOU:          //出張
                $total[4] ++;
                break;
            case KYUKA_KANGO:             //看護
                $total[24] ++;
                break;
            case KYUKA_KAIGO:             //介護
                $total[25] ++;
                break;
            case KYUKA_NONPAYBYOUKETU:    //病●(無給病欠)
                $total[7] ++;
                break;
            case KYUKA_KAIGOKYUGYOU:      //介●(介護休業)
                $total[11] ++;
                break;
            case KYUKA_SYUKYU_TOCHOKU;    //週休＋宿直
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            default:
                null;
        }

        //実働
        if ($value && substr(strval($value), 0, 2) !== '10') {
            $total[0] ++;
        }

        switch (strval($value)) {
            case KYUKA_KEKKON:          //結婚
                $total[1] ++;
                break;
            case KYUKA_KIBIKI:          //忌引
                $total[2] ++;
                break;
            case KYUKA_SONOTA:          //その他
                $total[3] ++;
                break;
            case KYUKA_ROUSAI:          //労災
                $total[5] ++;
                break;
            case KYUKA_NYUBYOUKETU:     //入病欠
                $total[6] ++;
                break;
            case KYUKA_BYOUKETU:        //病欠
                $total[7] ++;
                break;
            case KYUKA_JIKETU:          //事欠
                $total[8] ++;
                break;
            case KYUKA_SANKYU:          //産休
                $total[9] ++;
                break;
            case KYUKA_IKUKYU:          //育休
                $total[10] ++;
                break;
            case KYUKA_KYUSHOKU:        //休職
                $total[11] ++;
                break;
            case KYUKA_SYUKYU;          //週休
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            case KYUKA_SYUKYU2;         //週休２
                $total[14]+=2;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . 'AMPM ';
                }
                break;
            case KYUKA_YUKYU;           //有休
            case KYUKA_YUKYUS;          //土曜有休
                if ($Calendar[$day]['type'] === '2') {
                    $total[16] += 0.5;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . 'S ';
                    }
                }
                else {
                    $total[16] ++;
                    if ($length === '1') {
                        $yukyu = $yukyu . intval(substr($day, 6, 2)) . ' ';
                    }
                }
                break;
            case KYUKA_NATUKYU;         //夏休
                $total[15] ++;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . ']';
                }
                break;
            default:
                null;
        }
        switch ($value_reason) {
            case KYUKA_1011;            //週休土半
            case KYUKA_1012;            //週休土宿
            case KYUKA_1071;            //週休土待
                $total[14] ++;
                if ($length === '1') {
                    $syukyu = $syukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            case KYUKA_1009;            //有宿
                $total[16] ++;
                if ($length === '1') {
                    $yukyu = $yukyu . intval(substr($day, 6, 2)) . ' ';
                }
                break;
            case KYUKA_1010;            //夏宿
                $total[15] ++;
                if ($length === '1') {
                    $natukyu = $natukyu . '[' . intval(substr($day, 6, 2)) . ']';
                }
                break;
            default:
                null;
        }
    }
    //実働 (※出張を除く)
    $total[0] = $total[0] - $total[4];
    //合計-特別休暇
    $total[12] = $total[1] + $total[2] + $total[3] + $total[24] + $total[25];
    //合計-その他
    $total[13] = $total[4] + $total[5] + $total[6] + $total[7] + $total[8] + $total[9] + $total[10] + $total[11];

    //週休累計
    $total[17] = $emp['paid_w_get']['results'];
    //夏休累計
    $total[18] = $emp['paid_s_get']['results'];
    //有休合計
    $total[19] = $emp['paid_h_all']['results'];
    //有休消化
    $total[20] = $emp['paid_h_get']['results'];
    //有休残数
    $total[21] = $total[19] - $total[20];

    //有休繰越
    $total[22] = $emp['paid_h_kuri']['results'];
    //有休本年
    $total[23] = $emp['paid_h_fuyo']['results'];

    $cnt++;

    $data.="<tr>";
    $data.="<td style=\"vertical-align:top;\" rowspan=\"1\">" . $emp['name'] . "</td>";
    $data.="<td>" . strval($total[0]) . "</td>";     //実働
    $data.="<td>" . strval($total[1]) . "</td>";     //結婚
    $data.="<td>" . strval($total[2]) . "</td>";     //忌引
    $data.="<td>" . strval($total[24]) . "</td>";    //看護
    $data.="<td>" . strval($total[25]) . "</td>";    //介護
    $data.="<td>" . strval($total[3]) . "</td>";     //その他
    $data.="<td>" . strval($total[12]) . "</td>";    //合計
    $data.="<td>" . strval($total[4]) . "</td>";     //出張
    $data.="<td>" . strval($total[5]) . "</td>";     //労災
    $data.="<td>" . strval($total[7]) . "</td>";     //病欠
    $data.="<td>" . strval($total[8]) . "</td>";     //事欠
    $data.="<td>" . strval($total[9]) . "</td>";     //産休
    $data.="<td>" . strval($total[10]) . "</td>";    //育休
    $data.="<td>" . strval($total[11]) . "</td>";    //休職
    $data.="<td>" . strval($total[13]) . "</td>";    //合計
    $data.="<td>" . $syukyu . "</td>";               //当月消化日
    $data.="<td>" . strval($total[14]) . "</td>";    //週休-合計
    $data.="<td>" . strval($total[17]) . "</td>";    //週休-累計
    $data.="<td>" . strval($total[15]) . "</td>";    //夏休-合計
    $data.="<td>" . strval($total[18]) . "</td>";    //夏休-累計
//★

    $data.="<td>" . $yukyu . "</td>";                //当月消化日



    $data.="<td>" . strval($total[16]) . "</td>";    //合計
    $data.="<td>" . strval($total[22]) . "</td>";    //繰越
    $data.="<td>" . strval($total[23]) . "</td>";    //本年
    $data.="<td>" . strval($total[19]) . "</td>";    //合計
    $data.="<td>" . strval($total[20]) . "</td>";    //消化
    $data.="<td>" . strval($total[21]) . "</td>";    //残日
    $data.="<td colspan=\"4\">" . $natukyu . "</td>"; //備考
    $data.="</tr>";
}


$data.="</table>";
$data.="</body>";
$data.="</html>";

///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo to_utf8(nl2br($data));
ob_end_flush();
