<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($mygroup_nm == "") {
	echo("<script type=\"text/javascript\">alert('マイグループ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($mygroup_nm) > 30) {
	echo("<script type=\"text/javascript\">alert('マイグループ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// マイグループ名を更新
$sql = "update mygroupmst set";
$set = array("mygroup_nm");
$setvalue = array($mygroup_nm);
$cond = "where owner_id = (select emp_id from session where session_id = '$session') and mygroup_id = $mygroup_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = 'option_mygroup.php?session=$session&mygroup_id=$mygroup_id';</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
