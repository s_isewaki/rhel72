<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}
	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();
$calendar = '';
$calendar .= write_calendar('from', 'date_y_from', 'date_m_from', 'date_d_from');
$calendar .= write_calendar('to', 'date_y_to', 'date_m_to', 'date_d_to');
$log->debug("calendar：".$calendar);

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

$training_id_search	= $_POST['training_id_search'];
$training_name		= $_POST['training_name'];
$teacher_name		= $_POST['teacher_name'];

/**
 * 必須レベルオプションHTML取得
 *
 */
$level_options_str	= get_select_levels($_POST['level']);


/**
 * 申請年月日（開始）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y_from=cl_get_select_years($_POST['date_y_from']);

// 月オプションHTML取得
$option_date_m_from=cl_get_select_months($_POST['date_m_from']);

// 日オプションHTML取得
$option_date_d_from=cl_get_select_days($_POST['date_d_from']);

/**
 * 申請年月日（終了）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y_to=cl_get_select_years($_POST['date_y_to']);

// 月オプションHTML取得
$option_date_m_to=cl_get_select_months($_POST['date_m_to']);

// 日オプションHTML取得
$option_date_d_to=cl_get_select_days($_POST['date_d_to']);

// 2012/11/16 Yamagawa add(s)
/**
 * 申込期間オプションHTML取得
 */
$option_term_div=get_select_term_div($_POST['term_div']);
// 2012/11/16 Yamagawa add(e)

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";
$login_user = $emp_id;

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_inside_training_model.php");
$training_model = new cl_mst_inside_training_model($mdb2,$emp_id);
$log->debug("院内研修用DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
$education_committee_model = new cl_mst_nurse_education_committee_model($mdb2,$emp_id);
$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_setting_council_model.php");
$council_model = new cl_mst_setting_council_model($mdb2,$emp_id);
$log->debug("審議会DBA取得",__FILE__,__LINE__);

// 看護教育委員会チェック
$education_committee = $education_committee_model->check_education_committee($emp_id);

// 審議会チェック
$council = $council_model->check_council($emp_id);

// ログインユーザーが看護教育委員会でない場合
if (count($education_committee) == 0 && count($council) == 0){
	$mode = 'readOnly';
} else {
	$mode = '';
}

if ($_POST['delete_flg'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	define("DELETE_FLAG_PREFIX","delete_flag");

	$arr_field_name = array_keys($_POST);
	foreach($arr_field_name as $field_name){
		$pos = strpos($field_name,DELETE_FLAG_PREFIX);
		if ($pos === 0) {
			$arr_training_id = substr($field_name,strlen(DELETE_FLAG_PREFIX));
			if ($_POST[$field_name] != "") {
				$training_model->logical_delete($_POST[$field_name]);
			}
		}
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * 研修講座一覧取得
 */

// 日付オプション
if ($_POST['date_y_from'] == "-") {
	$date_y_from = "";
} else {
	$date_y_from = $_POST['date_y_from'];
}

if ($_POST['date_m_from'] == "-") {
	$date_m_from = "";
} else {
	$date_m_from = $_POST['date_m_from'];
}

if ($_POST['date_d_from'] == "-") {
	$date_d_from = "";
} else {
	$date_d_from = $_POST['date_d_from'];
}

if ($_POST['date_y_to'] == "-") {
	$date_y_to = "";
} else {
	$date_y_to = $_POST['date_y_to'];
}

if ($_POST['date_m_to'] == "-") {
	$date_m_to = "";
} else {
	$date_m_to = $_POST['date_m_to'];
}

if ($_POST['date_d_to'] == "-") {
	$date_d_to = "";
} else {
	$date_d_to = $_POST['date_d_to'];
}

if (
	$_POST['training_id_search']	== "" &&
	$_POST['training_name']			== "" &&
	$_POST['teacher_name']			== "" &&
	$_POST['level']					== "" &&
	$date_y_from					== "" &&
	$date_m_from					== "" &&
	$date_d_from					== "" &&
	$date_y_to						== "" &&
	$date_m_to						== "" &&
	$date_d_to						== ""
	// 2012/11/16 Yamagawa add(s)
	&&	$_POST['term_div']			== ""
	// 2012/11/16 Yamagawa add(e)
) {
	// 抽出条件無の場合、対象全取得
	$data = $training_model->getManagementList();
	$log->debug("一覧取得：条件無",__FILE__,__LINE__);
} else {
	// 抽出条件有
	if (
		$date_y_from	== "" ||
		$date_m_from	== "" ||
		$date_d_from	== ""
	) {
		$date_from = "";
	} else {
		$date_from = $date_y_from."-".$date_m_from."-".$date_d_from;
	}

	if (
		$date_y_to	== "" ||
		$date_m_to	== "" ||
		$date_d_to	== ""
	) {
		$date_to = "";
	} else {
		$date_to = $date_y_to."-".$date_m_to."-".$date_d_to;
	}

	$param = array(
		'training_id'		=> $_POST['training_id_search'],
		'training_name'		=> $_POST['training_name'],
		'level'				=> $_POST['level'],
		'teacher_name'		=> $_POST['teacher_name'],
		'date_from'			=> $date_from,
		'date_to'			=> $date_to
		// 2012/11/16 Yamagawa add(s)
		,'term_div'			=> $_POST['term_div']
		// 2012/11/16 Yamagawa add(e)
	);
	$data = $training_model->getManagementList_conditional($param);
	$log->debug("一覧取得：条件有",__FILE__,__LINE__);
}

// DB切断
$mdb2->disconnect();


// 研修名
for($i=0; $i<count($data); $i++){
	$data[$i]['training_name'] = str_replace('\'\'',   '&#039;', $data[$i]['training_name']);
	$data[$i]['training_name'] = str_replace('"',      '&quot;', $data[$i]['training_name']);
	$data[$i]['training_name'] = str_replace('\\\\',   '\\',     $data[$i]['training_name']);
	$data[$i]['training_name'] = str_replace('&lt;',   '<',      $data[$i]['training_name']);
	$data[$i]['training_name'] = str_replace('&gt;',   '>',      $data[$i]['training_name']);
}


/**
 * テンプレートマッピング
 */
$smarty->assign( 'cl_title'				, $cl_title				);
$smarty->assign( 'session'				, $session				);
$smarty->assign( 'login_user'			, $login_user			);
$smarty->assign( 'yui_cal_part'			, $yui_cal_part			);
$smarty->assign( 'calendar'				, $calendar				);
$smarty->assign( 'workflow_auth'		, $workflow_auth		);
$smarty->assign( 'aplyctn_mnitm_str'	, $aplyctn_mnitm_str	);
$smarty->assign( 'mode'					, $mode					);

$smarty->assign( 'training_id_search'	, $training_id_search	);
$smarty->assign( 'training_name'		, $training_name		);
$smarty->assign( 'level_options_str'	, $level_options_str	);
$smarty->assign( 'teacher_name'			, $teacher_name			);

$smarty->assign( 'option_date_y_from'	, $option_date_y_from	);
$smarty->assign( 'option_date_m_from'	, $option_date_m_from	);
$smarty->assign( 'option_date_d_from'	, $option_date_d_from	);

$smarty->assign( 'option_date_y_to'		, $option_date_y_to		);
$smarty->assign( 'option_date_m_to'		, $option_date_m_to		);
$smarty->assign( 'option_date_d_to'		, $option_date_d_to		);

// 2012/11/16 Yamagawa add(s)
$smarty->assign( 'option_term_div'		, $option_term_div		);
// 2012/11/16 Yamagawa add(e)

$smarty->assign( 'js_str'				, $js_str				);

$smarty->assign( 'data'					, $data					);

/**
 * テンプレート出力
 */
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 必須レベルオプションHTML取得
 */
function get_select_levels($level)
{

	ob_start();
	show_levels_options($level);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_levels_options($level) {

	$arr_levels_nm = array("","I","II","III","IV","V");
	$arr_levels_id = array("","1","2","3","4","5");

	for($i=0;$i<count($arr_levels_nm);$i++) {

		echo("<option value=\"$arr_levels_id[$i]\"");
		if($level == $arr_levels_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_levels_nm[$i]\n");
	}
}

// 2012/11/16 Yamagawa add(s)
/**
 * 申込期間オプションHTML取得
 */
function get_select_term_div($term_div)
{

	ob_start();
	show_term_div_options($term_div);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_term_div_options($term_div) {

	$arr_term_div_nm = array("","上期","下期");
	$arr_term_div_id = array("","1","2");

	for($i=0;$i<count($arr_term_div_nm);$i++) {

		echo("<option value=\"$arr_term_div_id[$i]\"");
		if($term_div == $arr_term_div_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_term_div_nm[$i]\n");
	}
}
// 2012/11/16 Yamagawa add(e)

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

	ob_start();
	write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
	$str_buff = ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
