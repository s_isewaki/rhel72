<?
//ini_set("display_errors", "1");
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_attendance_pattern.ini");
require_once("atdbk_common_class.php");
require_once("html_utils.php");
require_once("holiday.php");
require_once("timecard_common_class.php");
require_once("get_values.ini");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("ovtm_class.php");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);

// タイトル
switch ($wherefrom) {
	case "0":  // タイムカードの出勤簿入力画面より 20130129
	case "1":  // タイムカード入力画面より
	case "6":

		$doc_title = "出勤表";
		break;
	case "2":  // タイムカード修正画面より
	case "5":
	case "9":
		$doc_title = "勤務管理";
		break;
	case "3":  // トップ画面より
		$doc_title = "タイムカード";
		break;
	case "4":  // 勤務シフト作成より
	case "7":  // 勤務シフト実績入力からタイムカード修正画面
	case "8":
	case "10":
		$doc_title = $shift_menu_label;
		break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($doc_title); ?> | タイムカード修正</title>
<?

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$user_flg = false;
switch ($wherefrom) {
case "0":  // タイムカードの出勤簿入力画面より 20130129
case "1":  // タイムカード入力画面より
case "6":
	$checkauth = check_authority($session, 5, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	$user_flg = true;
	break;
case "2":  // タイムカード修正画面より
case "5":
case "9":
	$checkauth = check_authority($session, 42, $fname);
	if ($checkauth == "0" && $mmode != "usr") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	break;
case "3":  // トップ画面より
		break;
case "4":  // 勤務シフト作成の実績入力画面より
case "7":
case "8":
case "10":
		$checkauth = check_authority($session, 69, $fname);
		if ($checkauth == "0") {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
			exit;
		}
		break;
default:
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
	break;
}

// 日付の設定
$year = substr($date, 0, 4);
$month = substr($date, 4, 2);
$day = substr($date, 6, 2);
$int_month = intval($month);
$int_day = intval($day);


//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date, $date);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//残業申請関連クラス
$ovtm_class = new ovtm_class($con, $fname);

// 出勤グループを取得
$tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);

// 勤務日種別を取得
$sql = "select type from calendar";
$cond = "where date = '$date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$type = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "type") : "";

// 氏名を取得
$sql  = "SELECT EMP_LT_NM ||' '|| EMP_FT_NM AS NAME ";
$sql .= "  FROM EMPMST ";
$cond = " WHERE EMP_ID = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_name = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "name") : "";


// 勤務日種別名称の設定
if ($type == "") {
	$type_name = "";
} else if ($type == "1") {
	$type_name = "通常勤務日";
} else if ($type == "6") {
	$type_name = get_holiday_name($date);
} else if ($type == "7") {
	$type_name = "年末年始休暇";
} else if ($type == "8") {
    $type_name = "買取日";
} else {
	switch ($type) {
	case "2":
		$fld = "day4";
		$default = "勤務日1";
		break;
	case "3":
		$fld = "day5";
		$default = "勤務日2";
		break;
	case "4":
		$fld = "day2";
		$default = "法定休日";
		$holiday_name = get_holiday_name($date);
		break;
	case "5":
		$fld = "day3";
		$default = "所定休日";
		break;
	}
	//振替休日
	if ($type == "4" && $holiday_name == "振替休日") {
		$type_name = $holiday_name;
	} else {
		$sql = "select $fld from calendarname";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$type_name = pg_fetch_result($sel, 0, 0);
		} else {
			$type_name = $default;
		}
	}
}

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);

$err_back_page_flg = ($back == "t");

if ($err_back_page_flg || $postback == "t") {
	//登録エラーから戻った場合
	$work_tmcd_group_id = $groupId;
	$allow_id = $allow_ids[0];
	//手当回数追加 20100114
	$allow_count = $allow_counts[0];
	$allow_id2 = $allow_ids2[0];
	$allow_count2 = $allow_counts2[0];
	$allow_id3 = $allow_ids3[0];
	$allow_count3 = $allow_counts3[0];
}
else{
	// 勤務実績を取得
	$sql = "select * from atdbkrslt";
	$cond = "where emp_id = '$emp_id' and date = '$date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$pattern = pg_fetch_result($sel, 0, "pattern");
		$reason = pg_fetch_result($sel, 0, "reason");
		$start_time = pg_fetch_result($sel, 0, "start_time");
		$out_time = pg_fetch_result($sel, 0, "out_time");
		$ret_time = pg_fetch_result($sel, 0, "ret_time");
		$end_time = pg_fetch_result($sel, 0, "end_time");
        //追加
        $rest_start_time = pg_fetch_result($sel, 0, "rest_start_time");
        $rest_end_time = pg_fetch_result($sel, 0, "rest_end_time");
        for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = pg_fetch_result($sel, 0, "$start_var");
			$$end_var = pg_fetch_result($sel, 0, "$end_var");
		}
		$status = pg_fetch_result($sel, 0, "status");
		$allow_id = pg_fetch_result($sel, 0, "allow_id");
		$night_duty = pg_fetch_result($sel, 0, "night_duty");
		$meeting_time = pg_fetch_result($sel, 0, "meeting_time");
		$work_tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
		$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
		$next_day_flag = pg_fetch_result($sel, 0, "next_day_flag");
		$start_btn_time = pg_fetch_result($sel, 0, "start_btn_time");
		$end_btn_time = pg_fetch_result($sel, 0, "end_btn_time");
		//会議研修時間を開始終了時刻とする 20091008
		$meeting_start_time = pg_fetch_result($sel, 0, "meeting_start_time");
		$meeting_end_time = pg_fetch_result($sel, 0, "meeting_end_time");
		//手当回数、残業時刻追加 20100114
		$allow_count = pg_fetch_result($sel, 0, "allow_count");
		$over_start_time = pg_fetch_result($sel, 0, "over_start_time");
		$over_end_time = pg_fetch_result($sel, 0, "over_end_time");
		$over_start_next_day_flag = pg_fetch_result($sel, 0, "over_start_next_day_flag");
		$over_end_next_day_flag = pg_fetch_result($sel, 0, "over_end_next_day_flag");
		//残業時刻追加 20110621
		$over_start_time2 = pg_fetch_result($sel, 0, "over_start_time2");
		$over_end_time2 = pg_fetch_result($sel, 0, "over_end_time2");
		$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "over_start_next_day_flag2");
		$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "over_end_next_day_flag2");
		$update_time = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日 $4:$5",pg_fetch_result($sel, 0, "update_time"));

		$update_emp_id = pg_fetch_result($sel, 0, "update_emp_id");
//項目追加 20140717
        $start_btn_time2 = pg_fetch_result($sel, 0, "start_btn_time2");
        $end_btn_time2 = pg_fetch_result($sel, 0, "end_btn_time2");
        $start_btn_date1 = pg_fetch_result($sel, 0, "start_btn_date1");
        $end_btn_date1 = pg_fetch_result($sel, 0, "end_btn_date1");
        $start_btn_date2 = pg_fetch_result($sel, 0, "start_btn_date2");
        $end_btn_date2 = pg_fetch_result($sel, 0, "end_btn_date2");

		//所属
		$update_org_name = get_orgname($con, $fname, $update_emp_id, $timecard_bean);
		if ($update_org_name != "") {
			$update_org_name = substr($update_org_name, 1);
			$update_org_name = str_replace(" ", " &gt; ", $update_org_name);
		}
		//更新者氏名
		$update_emp_name = get_emp_kanji_name($con, $update_emp_id, $fname);

		//時間有休追加 20111207
		if ($obj_hol_hour->paid_hol_hour_flag == "t") {
			//時間有休データ取得
			$arr_hol_start_hour = $obj_hol_hour->get_paid_hol_hour($emp_id, $date);
			$paid_hol_start_hour = substr($arr_hol_start_hour["start_time"], 0, 2);
			$paid_hol_start_min = substr($arr_hol_start_hour["start_time"], 2, 2);
			$paid_hol_hour =  $arr_hol_start_hour["use_hour"];
			$paid_hol_min =  $arr_hol_start_hour["use_minute"];
		}
		//手当２、３追加
		$allow_id2 = pg_fetch_result($sel, 0, "allow_id2");
		$allow_count2 = pg_fetch_result($sel, 0, "allow_count2");
		$allow_id3 = pg_fetch_result($sel, 0, "allow_id3");
		$allow_count3 = pg_fetch_result($sel, 0, "allow_count3");

	} else {
		$pattern = "";
		$reason = "";
		$start_time = "";
		$out_time = "";
		$ret_time = "";
		$end_time = "";
		for ($i = 1; $i <= 10; $i++) {
			$start_var = "o_start_time$i";
			$end_var = "o_end_time$i";
			$$start_var = "";
			$$end_var = "";
		}
		$status = "";
		$allow_id = "";
		$night_duty = "";
		$meeting_time = "";
		$work_tmcd_group_id = "";
		$previous_day_flag = 0;
		$next_day_flag = 0;
		$start_btn_time = "";
		$end_btn_time = "";
        $start_btn_time2 = "";
        $end_btn_time2 = "";
        $start_btn_date1 = "";
        $end_btn_date1 = "";
        $start_btn_date2 = "";
        $end_btn_date2 = "";
		$meeting_start_time = "";
		$meeting_end_time = "";
		//手当回数、残業時刻追加 20100114
		$allow_count = "1";
		$over_start_time = "";
		$over_end_time = "";
		$over_start_next_day_flag = "";
		$over_end_next_day_flag = "";
		$over_start_time2 = "";
		$over_end_time2 = "";
		$over_start_next_day_flag2 = "";
		$over_end_next_day_flag2 = "";

		$paid_hol_start_hour = "";
		$paid_hol_start_min = "";
		$paid_hol_hour =  "";
		$paid_hol_min =  "";

		$allow_id2 = "";
		$allow_count2 = "1";
		$allow_id3 = "";
		$allow_count3 = "1";
	}
	//実績にパターンがない場合、予約側から取得
	if ($pattern == "") {
		$arr_atdbk = $timecard_common_class->get_atdbk_array($con, $date, $emp_id, $fname);

		$pattern = $arr_atdbk["pattern"];
		$reason = $arr_atdbk["reason"];
		$night_duty = $arr_atdbk["night_duty"];
		$allow_id = $arr_atdbk["allow_id"];
		$work_tmcd_group_id = $arr_atdbk["tmcd_group_id"];
	}
	//勤務シフトからの場合
	if ($wherefrom == "4") {
		$pattern = intval(substr($atdptn_ptn_id, 0, 2));
		//	$reason = $reason_2; 事由の引継ぎ不具合 20100330
		if ($reason_2 != "") {
			$reason = $reason_2;
		} else {
			if (substr($atdptn_ptn_id, 2, 2) > 0) {
				$reason = intval(substr($atdptn_ptn_id, 2, 2));
			} else {
				$reason = "";
			}
		}

	}
}

//前日チェックがあるかフラグ
$previous_day_flag_checked = "";
if ($previous_day_flag == 1){
	$previous_day_flag_checked = "checked";
}

//翌前日チェックがあるかフラグ
$next_day_flag_checked = "";
if ($next_day_flag == 1){
	$next_day_flag_checked = "checked";
}
//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
if ($work_tmcd_group_id == null && strlen($work_tmcd_group_id) == 0 && $err_back_page_flg == false){
	$work_tmcd_group_id = $tmcd_group_id;
}

// 出勤パターン名を取得
$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($work_tmcd_group_id);

if ($err_back_page_flg == false && $postback != "t"){
	// 時刻を時・分に分割
	list($start_hour, $start_min) = split_hm($start_time);
	list($out_hour, $out_min) = split_hm($out_time);
	list($ret_hour, $ret_min) = split_hm($ret_time);
	list($end_hour, $end_min) = split_hm($end_time);
    list($rest_start_hour, $rest_start_min) = split_hm($rest_start_time);
    list($rest_end_hour, $rest_end_min) = split_hm($rest_end_time);

    list($meeting_start_hour, $meeting_start_min) = split_hm($meeting_start_time);
	list($meeting_end_hour, $meeting_end_min) = split_hm($meeting_end_time);
	for ($i = 1; $i <= 10; $i++) {
		$start_time_var = "o_start_time$i";
		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		list($$start_hour_var, $$start_min_var) = split_hm($$start_time_var);
		$end_time_var = "o_end_time$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";
		list($$end_hour_var, $$end_min_var) = split_hm($$end_time_var);
	}

	if ($meeting_time != null ){
		$meeting_time_hh = substr($meeting_time, 0, 2);
		$meeting_time_mm = substr($meeting_time, 2, 2);
	}

	//残業時刻をDBから設定
	list($over_start_hour, $over_start_min) = split_hm($over_start_time);
	list($over_end_hour, $over_end_min) = split_hm($over_end_time);

	list($over_start_hour2, $over_start_min2) = split_hm($over_start_time2);
	list($over_end_hour2, $over_end_min2) = split_hm($over_end_time2);
}

list($start_btn_hour, $start_btn_min) = split_hm($start_btn_time);
list($end_btn_hour, $end_btn_min) = split_hm($end_btn_time);

//翌日フラグデフォルト設定 20100114
$over_start_next_day_flag_checked = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_checked = "checked";
}
$over_end_next_day_flag_checked = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_checked = "checked";
}
$over_start_next_day_flag2_checked = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_checked = "checked";
}
$over_end_next_day_flag2_checked = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_checked = "checked";
}
// 分の"--"を""とする
if ($over_start_min == "--") {
	$over_start_min = "";
}
if ($over_end_min == "--") {
	$over_end_min = "";
}
if ($over_start_min2 == "--") {
	$over_start_min2 = "";
}
if ($over_end_min2 == "--") {
	$over_end_min2 = "";
}


// 「退勤後復帰」ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

// 退勤後復帰時刻表示フラグを設定
$ret_show_flg = ($ret_btn_flg == "t" || $o_start_hour1 != "");

// 残業理由一覧を取得
$sql = "select reason_id, reason from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$ovtmrsn_sel = select_from_table($con, $sql, $cond, $fname);
if ($ovtmrsn_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 勤務条件テーブルより残業管理を取得
$sql = "select no_overtime from empcond";
$cond = "where emp_id = '$emp_id' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";

//残業理由表示フラグ
if (((($over_start_hour != "" && $over_start_hour != "--") ||
			($over_start_min != "" && $over_start_min != "--") ||
			($over_end_hour != "" && $over_end_hour != "--") ||
			($over_end_min != "" && $over_end_min != "--") ||
			($over_start_hour2 != "" && $over_start_hour2 != "--") ||
			($over_start_min2 != "" && $over_start_min2 != "--") ||
			($over_end_hour2 != "" && $over_end_hour2 != "--") ||
			($over_end_min2 != "" && $over_end_min2 != "--")
			)
		&& $timecard_bean->over_time_apply_type != "0"
		&& $no_overtime != "t") // 残業管理をする場合
		|| $ovtm_class->ovtm_reason2_input_flg == "t"  //20150108
	){

	$ovtm_rsn_id_disp = "";

	//エラーで再表示でない場合、残業申請情報を取得
    if ($err_back_page_flg == false && $postback != "t"){
		$sql = "select apply_id, reason_id, reason, reason_detail, apply_status from ovtmapply";
		$cond = "where emp_id = '$emp_id' and target_date = '$date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0)
		{
			$apply_id = pg_fetch_result($sel, 0, "apply_id");
			$ovtm_reason_id = pg_fetch_result($sel, 0, "reason_id");
			$ovtm_reason = pg_fetch_result($sel, 0, "reason");
			//$ovtm_reason_detail = pg_fetch_result($sel, 0, "reason_detail");
			//$apply_status = pg_fetch_result($sel, 0, "apply_status");
		}
	}

}
else {
	$ovtm_rsn_id_disp = "none";
}
// 勤務シフト作成の勤務実績入力画面から呼ばれた場合、対象年月の締め状況を取得し登録ボタン無効化 20130128
$atdbk_closed = false;
if ($wherefrom == "4") {
	$sql = "select count(*) from atdbkclose";
	$cond = "where emp_id = '$emp_id' and yyyymm = '$yyyymm'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$atdbk_closed = (pg_fetch_result($sel, 0, 0) > 0);
}
//
// 勤務実績情報を取得、所定時間等
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $date);
if ($postback != "t") {
    $start_hour = ($arr_result["start_time"] != "") ? intval(substr($arr_result["start_time"], 0, 2), 10) : "";
    $start_min = substr($arr_result["start_time"], 2, 2);
    $end_hour = ($arr_result["end_time"] != "") ? intval(substr($arr_result["end_time"], 0, 2), 10) : "";
    $end_min = substr($arr_result["end_time"], 2, 2);
    $rest_start_hour = ($rest_start_time != "") ? intval(substr($rest_start_time, 0, 2), 10) : "";
    $rest_start_min = substr($rest_start_time, 2, 2);
    $rest_end_hour = ($rest_end_time != "") ? intval(substr($rest_end_time, 0, 2), 10) : "";
    $rest_end_min = substr($rest_end_time, 2, 2);
    $out_hour = ($out_time != "") ? intval(substr($out_time, 0, 2), 10) : "";
    $out_min = substr($out_time, 2, 2);
    $ret_hour = ($ret_time != "") ? intval(substr($ret_time, 0, 2), 10) : "";
    $ret_min = substr($ret_time, 2, 2);
}

//$table_width = ($ovtm_class->keyboard_input_flg != "t" && $ovtm_class->ovtm_rest_disp_flg == "t") ? 720 : 610;
$table_width = 720;
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
//	$table_width += 310;
//	$table_width += 210;
}
$maxline = ($_POST["ovtmadd_disp_flg"] == '1') ? 5 : 2;

$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $date, "input");

// 未設定時のデフォルト取得
if ($arr_result["over_start_time"] == "" || $arr_result["over_end_time"] == "") {
    $emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);

    $arr_default_time = $timecard_common_class->get_default_overtime($arr_result["start_time"], $arr_result["end_time"], $arr_result["next_day_flag"], $arr_result["tmcd_group_id"], $arr_result["pattern"], $arr_result["type"], $arr_result["previous_day_flag"], $emp_officehours["office_end_time"] );
}

$arr_rest_data = $timecard_common_class->get_office_rest_data($emp_id, $date);
//追加タブリンクで移動時、勤務パターンが変更されていたら、チェック用情報を設定
if ($postback == "t") {
    if ($pattern != $arr_result["pattern"] || $groupId != $arr_result["tmcd_group_id"]) {
        $arr_officetime_info = $timecard_common_class->get_officetime_info($emp_id, $groupId, $pattern, $date);
        $arr_result["office_start_time"] = $arr_officetime_info["office_start_time"];
        $arr_result["office_end_time"] = $arr_officetime_info["office_end_time"];
        $arr_result["office_rest_start_time"] = $arr_officetime_info["rest_start_time"];
        $arr_result["office_rest_end_time"] = $arr_officetime_info["rest_end_time"];
        $arr_rest_data["office_time_min"] = $arr_officetime_info["office_time_min"];
        $arr_rest_data["rest_time_min"] = $arr_officetime_info["rest_time_min"];
    }
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/focustonext.js"></script>
<script type="text/javascript" src="js/atdbk_timecard_edit.js?<? echo time(); ?>"></script>
<script type="text/javascript">
<!--
//resizeTo(<? echo $table_width+55; ?>, 740);

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $wk_reason) {
		if ($wk_reason != "") {
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$wk_reason';\n");
		}
	}
}
?>
var arr_m_start_time = new Array();
var arr_m_end_time = new Array();
<?
//会議・研修時間情報を出力
$arr_meeting_time1 = $atdbk_common_class->get_atdbk_meeting_time();
foreach ($arr_meeting_time1 as $wk_tmcd_group_id => $arr_meeting_time2) {
    foreach ($arr_meeting_time2 as $wk_atdptn_id => $wk_times) {
        echo("arr_m_start_time['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '{$wk_times["meeting_start_time"]}';\n");
        echo("arr_m_end_time['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '{$wk_times["meeting_end_time"]}';\n");
    }
}
?>
function setReason(atdptn_id) {

	if (atdptn_id == '--') {
		document.mainform["reason"].value = '--';
		if (document.mainform["meeting_start_hour"]) {
			document.mainform["meeting_start_hour"].value = '--';
			document.mainform["meeting_start_min"].value = '--';
			document.mainform["meeting_end_hour"].value = '--';
			document.mainform["meeting_end_min"].value = '--';
		}
		return;
	}
	wk_tmcd_group_id = document.mainform["list_tmcd_group_id"].value;
	wk_id = wk_tmcd_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.mainform["reason"].value = '--';
	}
    else {
	    document.mainform["reason"].value = arr_atdptn_reason[wk_id];
    }
	if (document.mainform["meeting_start_hour"]) {
		if (arr_m_start_time[wk_id] == undefined) {
			document.mainform["meeting_start_hour"].value = '--';
			document.mainform["meeting_start_min"].value = '--';
			document.mainform["meeting_end_hour"].value = '--';
			document.mainform["meeting_end_min"].value = '--';
		}
	    else {
			document.mainform["meeting_start_hour"].value = arr_m_start_time[wk_id].substring(0, 2);
			document.mainform["meeting_start_min"].value = arr_m_start_time[wk_id].substring(2, 4);
			document.mainform["meeting_end_hour"].value = arr_m_end_time[wk_id].substring(0, 2);
			document.mainform["meeting_end_min"].value = arr_m_end_time[wk_id].substring(2, 4);
	    }
	}

    //ajax、所定時間取得 20140826
    var emp_id = document.getElementById('emp_id').value;
    var tmcd_group_id = document.getElementById('list_tmcd_group_id').value;
    var pattern = document.getElementById('pattern').value;
    var date = document.getElementById('date').value;
    get_officetime_ajax(emp_id, tmcd_group_id, pattern, date);

//alert(office_time_min);
//alert(base_time);
}
//残業理由その他表示
function setOvtmReasonDisabled(item_idx) {
	var ovtm_reason_id_nm = 'ovtm_reason_id'+item_idx;
	var ovtm_reason_div_nm = 'ovtm_reason_div'+item_idx;

	if (document.getElementById(ovtm_reason_div_nm)) {
		document.getElementById(ovtm_reason_div_nm).style.display = (document.getElementById(ovtm_reason_id_nm).value == 'other') ? '' : 'none';
	}
}

//残業理由表示
function setOvtmReasonIdDisabled() {
	var disp = 'none';
	if ((document.getElementById('over_start_hour').value != '' &&
		 document.getElementById('over_start_hour').value != '--') ||
		(document.getElementById('over_start_hour2').value != '' &&
		 document.getElementById('over_start_hour2').value != '--')) {
		disp = '';
	}

	document.getElementById('ovtm_rsn_id').style.display = disp;
}

function ovtmAddDisp(flg) {
    var val = (flg) ? '1' : '0';
    document.mainform.ovtmadd_disp_flg.value = val;
    document.mainform.action = 'atdbk_timecard_edit.php';
    document.mainform.submit();
    return false;
}

//休憩不足時間（分）
var rest_fusoku_min = 0;
//残業限度超時間（メッセージ用）
var over_limit_str = '';

var msg = '';
function regist() {
    //休憩時間、残業時間チェック
    chk_flg = checkTime(1);
	if(chk_flg)
	{
        if (chk_flg == 1) {
            return false;
        }

		if (!confirm(msg))
		{
			return;
		}
	}

    document.mainform.action = 'atdbk_timecard_edit_exe.php';
    document.mainform.submit();
}
<? //翌日フラグチェック ?>
var base_time = '<?
$base_time = $arr_result["office_end_time"];
if ($arr_result["office_end_time"] < $arr_result["office_start_time"]) {
    $base_time = $arr_result["office_start_time"];
}
$base_time = str_replace(":", "", $base_time);
echo $base_time; ?>';
    //労働基準法の休憩時間が取れていない場合は警告する
    var rest_check_flg = '<? echo $ovtm_class->rest_check_flg; ?>';
    //36協定の残業限度時間をこえたら警告する
    var ovtm_limit_check_flg = '<? echo $ovtm_class->ovtm_limit_check_flg; ?>';

    //行数
    var maxline = <? echo $maxline; ?>;
    //休憩時間チェック
    var kinmu_total_min = 0;
    var rest_total_min = 0;
    var office_time_min = <? echo $arr_rest_data["office_time_min"]; ?>;
    var rest_time_min = <? echo $arr_rest_data["rest_time_min"]; ?>;

    //36協定残業限度時間
var ovtm_limit_month = <?
$limit_month = ($ovtm_class->ovtm_limit_month == '') ? 0 : $ovtm_class->ovtm_limit_month;
echo $limit_month;
    ?>;
    var ovtm_limit_month_min = ovtm_limit_month * 60;
    //当月残業累計時間申請中含む
    var ovtm_approve_apply_min = <? echo $arr_ovtm_data["ovtm_approve_apply_min"]; ?>;

    //当月残業累計時間承認のみ
    var ovtm_approve_min = <? echo $arr_ovtm_data["ovtm_approve_min"]; ?>;

    var ovtm_rest_disp_flg = '<? echo $ovtm_class->ovtm_rest_disp_flg; ?>';

    var rest_disp_flg = '<? echo $ovtm_class->rest_disp_flg; ?>';

    var over_time_apply_type = '<? echo $timecard_bean->over_time_apply_type; ?>';

    var no_overtime = '<? echo $no_overtime; ?>';

    var apply_update_flg = '2'; //1:申請 2:更新

    var _session = '<? echo $session; ?>';
    var office_start_time = '<? echo $arr_result["office_start_time"]; ?>';
    var office_end_time = '<? echo $arr_result["office_end_time"]; ?>';
    var rest_start_time = '<? echo $arr_result["office_rest_start_time"]; ?>';
    var rest_end_time = '<? echo $arr_result["office_rest_end_time"]; ?>';
    var org_tmcd_group_id = '<? echo $arr_result["tmcd_group_id"]; ?>';
    var org_pattern = '<? echo $pattern; ?>';
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="resizeTo(document.getElementById('main_table').offsetWidth+40, 740);document.onkeydown=focusToNext;document.mainform.start_hour.focus();setOvtmReasonDisabled('');<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	for ($i=2; $i<=5; $i++) {
		echo "setOvtmReasonDisabled('$i');";
	}
}
?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード修正</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="atdbk_timecard_edit_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="left">
<? //タブを追加
if ($ovtm_class->ovtm_tab_flg == "t") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($ovtmadd_disp_flg == '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(false);">
<? } ?>
タイムカード修正
<? if ($ovtmadd_disp_flg == '1') { ?>
</a>
<? } ?>
&nbsp;
<? if ($ovtmadd_disp_flg != '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(true);">
<? } ?>
残業追加
<? if ($ovtmadd_disp_flg != '1') { ?>
</a>
<? } ?>
</font>
<? } ?>
</td>
<td align="right"><input type="button" onclick="regist();" value="登録"<?
//青森県中様用、申請機能でのみ更新させたいためボタン無効化 20130123
$opt_aomori_flag = file_exists("opt/aomori_flag");
//締め済みの場合、無効化 20130128
if ($atdbk_closed || $opt_aomori_flag) {
    echo(" disabled");
}
?>
></td>
</tr>
</table>
<?php
$colspan_num = ($ovtm_class->ovtm_reason2_input_flg == "t") ? "2" : "1";
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" id="main_table">
<tr height="22">
<? // 180 ?>
<td width="90" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_name); ?></font></td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($int_month); ?>月<? echo($int_day); ?>日&nbsp;<? echo(get_weekday(to_timestamp($date))); ?>曜</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($type_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<?
	//グループコンボボックス出力
	$atdbk_common_class->setGroupOption("groupId", "list_tmcd_group_id", $work_tmcd_group_id, "pattern");
?>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td colspan="<?php echo $colspan_num; ?>"><select name="pattern" id="pattern" <? echo("onChange=\"setReason(this.value);\"");?>><? show_pattern_options($arr_attendance_pattern, $pattern); ?></select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td colspan="<?php echo $colspan_num; ?>"><? $atdbk_common_class->set_reason_option("reason", $reason); ?></td>
</tr>
<?php
if ($ovtm_class->duty_input_nodisp_flg != "t" || !$user_flg) {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td colspan="<?php echo $colspan_num; ?>"><select name="night_duty"><?show_night_duty_options($night_duty);?></td>
</tr>
<?php } ?>
<?php
if ($ovtm_class->allowance_input_nodisp_flg != "t" || !$user_flg) {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td colspan="<?php echo $colspan_num; ?>"><nobr>
<?
show_allowance_list2($allow_id, $arr_allowance, 1);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count, 1);
echo "&nbsp";
show_allowance_list2($allow_id2, $arr_allowance, 2);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count2, 2);
echo "&nbsp";
show_allowance_list2($allow_id3, $arr_allowance, 3);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count3, 3);
?>

</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="start_hour"  id="start_hour" value="<? echo $start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='start_min';backElement='<?
        //
        $backElement =($ovtm_class->ovtm_rest_disp_flg == "t") ? "rest_end_min$maxline" : "over_end_min$maxline";
        echo $backElement;
    ?>';">：<input type="text" name="start_min"  id="start_min" value="<? echo $start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='<?
    $backElement = ($ovtm_class->rest_disp_flg == "t") ? "rest_start_hour" : "end_hour";
    echo $backElement;
              ?>';backElement='start_hour';">
        <?
    }
    else {
    ?>
		<select name="start_hour" id="start_hour"><? show_hour_options_0_23($start_hour); ?></select>：<select name="start_min" id="start_min"><? show_min_options_00_59($start_min); ?></select>
        <? } ?>
		<label for="zenjitsu"><input type="checkbox" name="previous_day_flag" id="zenjitsu" <?=$previous_day_flag_checked ?> value="1">前日</label>
	</font>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
    echo ("　　打刻&nbsp;");
    $tmp_start_btn_time = "";
    if ($start_btn_hour != "") {
        echo($ovtm_class->get_btn_date_format($start_btn_date1));
        if ($start_btn_hour < 10) {
            $tmp_start_btn_time = "&nbsp;".(int)$start_btn_hour.":".$start_btn_min;
        } else {
            $tmp_start_btn_time = $start_btn_hour.":".$start_btn_min;
        }
    }
    echo($tmp_start_btn_time);
    if ($start_btn_time2 != "") {
        echo ("　　　出勤2&nbsp;");
        echo($ovtm_class->get_btn_date_format($start_btn_date2));
        $tmp_start_btn_time2 = $ovtm_class->get_btn_time_format($start_btn_time2);
        echo($tmp_start_btn_time2);
    }

?>
</font>
</td>

</tr>
<?
if ($ovtm_class->rest_disp_flg == "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩時間</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="rest_start_hour"  id="rest_start_hour" value="<? echo $rest_start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_start_min';backElement='start_min';">：<input type="text" name="rest_start_min"  id="rest_start_min" value="<? echo $rest_start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_end_hour';backElement='rest_start_hour';"> 〜
        <input type="text" name="rest_end_hour"  id="rest_end_hour" value="<? echo $rest_end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_end_min';backElement='rest_start_min';">：<input type="text" name="rest_end_min"  id="rest_end_min" value="<? echo $rest_end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_hour';backElement='rest_end_hour';">
        <?
    }
    else {
    ?>
		<select name="rest_start_hour" id="rest_start_hour"><? show_hour_options_0_23($rest_start_hour); ?></select>：<select name="rest_start_min" id="rest_start_min"><? show_min_options_00_59($rest_start_min); ?></select> 〜
		<select name="rest_end_hour" id="rest_end_hour"><? show_hour_options_0_23($rest_end_hour); ?></select>：<select name="rest_end_min" id="rest_end_min"><? show_min_options_00_59($rest_end_min); ?></select>
        <? } ?>
	</font>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
    <input type="text" name="end_hour"  id="end_hour" value="<? echo $end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_min';backElement='<?
    $backElement = ($ovtm_class->rest_disp_flg == "t") ? "rest_end_min" : "start_min";
    echo $backElement;
              ?>';">：<input type="text" name="end_min"  id="end_min" value="<? echo $end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='<?
        $nextElement = ($ovtm_class->out_input_nodisp_flg != "t" || !$user_flg) ? "out_hour" : "over_start_hour";
        echo $nextElement;
              ?>';backElement='end_hour';">
        <?
    }
    else {
    ?>
		<select name="end_hour" id="end_hour"><? show_hour_options_0_23($end_hour); ?></select>：<select name="end_min" id="end_min"><? show_min_options_00_59($end_min); ?></select>
        <? } ?>
		<label for="yokujitsu"><input type="checkbox" name="next_day_flag" id="yokujitsu" <?=$next_day_flag_checked ?> value="1">翌日</label>
	</font>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
    echo ("　　打刻&nbsp;");
    $tmp_end_btn_time = "";
    if ($end_btn_hour != "") {
        echo($ovtm_class->get_btn_date_format($end_btn_date1));
        if ($end_btn_hour < 10) {
            $tmp_end_btn_time = "&nbsp;".(int)$end_btn_hour.":".$end_btn_min;
        } else {
            $tmp_end_btn_time = $end_btn_hour.":".$end_btn_min;
        }
    }
    echo($tmp_end_btn_time);
    if ($end_btn_time2 != "") {
        echo ("　　　退勤2&nbsp;");
        echo($ovtm_class->get_btn_date_format($end_btn_date2));
        $tmp_end_btn_time2 = $ovtm_class->get_btn_time_format($end_btn_time2);
        echo($tmp_end_btn_time2);
    }
?>
	</font>
</td>
</tr>
<?php
if ($ovtm_class->out_input_nodisp_flg != "t" || !$user_flg) {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="out_hour"  id="out_hour" value="<? echo $out_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='out_min';backElement='end_min';">：<input type="text" name="out_min"  id="out_min" value="<? echo $out_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='ret_hour';backElement='out_hour';"> 〜
        <input type="text" name="ret_hour"  id="ret_hour" value="<? echo $ret_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='ret_min';backElement='out_min';">：<input type="text" name="ret_min"  id="ret_min" value="<? echo $ret_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='over_start_hour';backElement='ret_hour';">
        <?
    }
    else {
    ?>
<select name="out_hour" id="out_hour"><? show_hour_options_0_23($out_hour); ?></select>：<select name="out_min" id="out_min"><? show_min_options_00_59($out_min); ?></select> 〜 <select name="ret_hour" id="ret_hour"><? show_hour_options_0_23($ret_hour); ?></select>：<select name="ret_min" id="ret_min"><? show_min_options_00_59($ret_min); ?></select>
        <? } ?>
</font>
</td>
</tr>
<?php } ?>
<?
$ovtm_class->disp_btn_time_data($arr_result, "input");
?>
<? //20140620
//残業理由用、実績がない場合の、勤務パターングループ設定 20150127
if ($arr_result["tmcd_group_id"] == "") {
	$arr_result["tmcd_group_id"] = $work_tmcd_group_id;
}
$ovtm_class->disp_input_form($arr_result, $timecard_bean, $arr_default_time, $apply_status, array(), "1", $no_overtime, $obj_hol_hour->paid_hol_hour_flag, $ret_show_flg, $apply_id); //20150107$apply_id追加
$ovtm_class->disp_ovtm_total_time($arr_ovtm_data);
?>
<? //時間有休追加 20111207
if ($ovtm_class->hol_hour_input_nodisp_flg != "t" || !$user_flg) {
	$obj_hol_hour->show_input_item($paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min, $emp_id, $colspan_num);
}
?>
<?
if ($ret_show_flg) {
	$break_flg = false;
	$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";
	for ($i = 1; $i <= 10; $i++) {
		if ($break_flg) {
			break;
		}

		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";

		echo("<tr height=\"22\">\n");
		echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}時刻{$i}</font></td>\n");
        echo("<td colspan=\"{$colspan_num}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j10\"><select name=\"$start_hour_var\" id=\"$start_hour_var\">");
		show_hour_options_0_23($$start_hour_var);
        echo("</select>：<select name=\"$start_min_var\" id=\"$start_min_var\">");
		show_min_options_00_59($$start_min_var);
        echo("</select> 〜 <select name=\"$end_hour_var\" id=\"$end_hour_var\">");
		show_hour_options_0_23($$end_hour_var);
        echo("</select>：<select name=\"$end_min_var\" id=\"$end_min_var\">");
		show_min_options_00_59($$end_min_var);
		echo("</select></font></td>\n");
		echo("</tr>\n");

		if ($$start_hour_var == "") {
			$break_flg = true;
		}

		//エラーで戻った場合は次の値が空の場合ループを止める
		$err_start_hour_var = "o_start_hour".($i + 1);
		if (($err_back_page_flg || $postback == "t") && $$err_start_hour_var == "") {
			$break_flg = true;
		}
	}
}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤状況</font></td>
<td colspan="<?php echo $colspan_num; ?>"><select name="status" id="status"><? show_status_options($status); ?></select></td>
</tr>
<?php
if ($ovtm_class->meeting_input_nodisp_flg != "t" || !$user_flg) {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・<br>病棟外勤務</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<select name="meeting_start_hour" id="meeting_start_hour"><? show_hour_options_0_23($meeting_start_hour); ?></select>：<select name="meeting_start_min" id="meeting_start_min"><? show_min_options_00_59($meeting_start_min); ?></select> 〜 <select name="meeting_end_hour" id="meeting_end_hour"><? show_hour_options_0_23($meeting_end_hour); ?></select>：<select name="meeting_end_min" id="meeting_end_min"><? show_min_options_00_59($meeting_end_min); ?></select>
</font>
</td>
</tr>
<?php } ?>
<?php
//残業時刻2以降にも理由を入力する場合は既存の理由を表示しない
if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
<tr height="22" id="ovtm_rsn_id" style="display:<? echo($ovtm_rsn_id_disp); ?>;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業理由</font></td>
<td>
<select name="ovtm_reason_id" id="ovtm_reason_id" onchange="setOvtmReasonDisabled('');">
<?
$str = $ovtm_class->get_ovtmrsn_option($arr_result["tmcd_group_id"], $ovtm_reason_id);
echo $str;

//理由が入力済みの場合、その他とする
$selected = ($ovtm_reason != "") ? " selected" : "";
$ovtm_reason_disp = ($ovtm_reason != "" || pg_num_rows($ovtmrsn_sel) == 0) ? "" : "none"; //残業理由が０件はその他の入力ができるようにする 20120125
$onkeydown_str = " onblur=\"document.onkeydown=focusToNext;\" onfocus=\"document.onkeydown='';\""; //20141017
?>
<option value="other" <? echo($selected); ?>>その他
</select><br>
<div id="ovtm_reason_div" style="display:<? echo($ovtm_reason_disp); ?>;"><input type="text" name="ovtm_reason" value="<? echo($ovtm_reason); ?>" size="50" maxlength="100" style="ime-mode:active;" <?php echo $onkeydown_str; ?>></div>
</td>
</tr>
<?php } ?>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新日時</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_time?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>最終更新者所属</nobr></font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_org_name?>
</font>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">最終更新者</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$update_emp_name?>
</font>
</td>
</tr>

</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" onclick="regist();" value="登録"<?
//青森県中様用、申請機能でのみ更新させたいためボタン無効化 20130123
//締め済みの場合、無効化 20130128
if ($atdbk_closed || $opt_aomori_flag) {
    echo(" disabled");
}
?>
></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" id="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="date" id="date" value="<? echo($date); ?>">
<input type="hidden" name="pre_end_time" value="<? echo("$end_hour$end_min"); ?>">
<input type="hidden" name="pre_next_day_flag" value="<? echo("$next_day_flag"); ?>">
<input type="hidden" name="pre_night_duty_flag" value="<? echo(($night_duty == 1)); ?>">
<?
if ($ret_show_flg) {
	for ($i = 1; $i <= 10; $i++) {
		$hour_var = "o_start_hour$i";
		$min_var = "o_start_min$i";
		echo("<input type=\"hidden\" name=\"pre_o_start_time$i\" value=\"${$hour_var}${$min_var}\">\n");
	}
}
?>
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="start_btn_time" value="<? echo($start_btn_time); ?>">
<input type="hidden" name="end_btn_time" value="<? echo($end_btn_time); ?>">
<input type="hidden" name="start_btn_time2" value="<? echo($start_btn_time2); ?>">
<input type="hidden" name="end_btn_time2" value="<? echo($end_btn_time2); ?>">
<input type="hidden" name="start_btn_date1" value="<? echo($start_btn_date1); ?>">
<input type="hidden" name="end_btn_date1" value="<? echo($end_btn_date1); ?>">
<input type="hidden" name="start_btn_date2" value="<? echo($start_btn_date2); ?>">
<input type="hidden" name="end_btn_date2" value="<? echo($end_btn_date2); ?>">
<input type='hidden' name='plan_hope_flg' value='<?= $plan_hope_flg ?>'>
<input type="hidden" name="emp_idx" value="<? echo($emp_idx); ?>">
<input type="hidden" name="day_idx" value="<? echo($day_idx); ?>">
<input type="hidden" name="atdptn_ptn_id" value="<? echo($atdptn_ptn_id); ?>">
<input type="hidden" name="reason_2" value="<? echo($reason_2); ?>">
<input type="hidden" name="staff_ids" value="<? echo($staff_ids); ?>">
<input type="hidden" name="check_work_flg" value="<? echo($check_work_flg); ?>">
<input type="hidden" name="empid_list" value="<? echo($empid_list); ?>">
<input type="hidden" name="postback" value="t">
<input type="hidden" name="ovtmadd_disp_flg" value="<? echo($ovtmadd_disp_flg); ?>">
<input type="hidden" name="shift_admin_flg" value="<?php echo($shift_admin_flg); ?>">
<input type="hidden" name="shift_from_flg" value="<?php echo($shift_from_flg); ?>">
<input type="hidden" name="mypage" value="<?php  echo($mypage); ?>">
<input type="hidden" name="shift_group_id"		 value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id"		 value="<? echo($csv_layout_id); ?>">
<input type="hidden" name="duty_form_jokin"		 value="<? echo($duty_form_jokin); ?>">
<input type="hidden" name="duty_form_hijokin"	 value="<? echo($duty_form_hijokin); ?>">
<input type="hidden" name="sus_flg"				 value="<? echo($sus_flg); ?>">
<input type="hidden" name="mmode"				 value="<? echo($mmode); ?>">

</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?

// 出勤予定オプションを出力
function show_pattern_options($arr_attendance_pattern, $selected_pattern) {
	echo("<option value=\"--\"");
	if ($selected_pattern == "") {
		echo(" selected");
	}
	echo(">\n");
	foreach ($arr_attendance_pattern as $id => $val) {
		echo("<option value=\"$id\"");
		if ($id == $selected_pattern) {
			echo(" selected");
		}
		echo(">$val");
	}
}
// 時刻を時・分に分割
function split_hm($time) {
	if ($time != "") {
		return array(substr($time, 0, 2), substr($time, 2, 2));
	} else {
		return array("", "");
	}
}

// 出勤状況オプションを出力
function show_status_options($status) {
	$status_values = array("", "1", "2", "3", "4", "5");
	$status_names = array("", "勤務中", "外出中", "退勤済み", "休暇中", "欠勤");
	for ($i = 0; $i < count($status_values); $i++) {
		$status_value = $status_values[$i];
		$status_name = $status_names[$i];

		echo("<option value=\"$status_value\"");
		if ($status_value == $status) {
			echo(" selected");
		}
		echo(">$status_name");
	}
}

// 当直オプションを出力
function show_night_duty_options($night_duty) {
	$arr_night_duty_value = array("", "1", "2");
	$arr_night_duty_name = array("", "有り", "無し");
	for ($i = 0; $i < count($arr_night_duty_value); $i++) {
		$night_duty_value = $arr_night_duty_value[$i];
		$night_duty_name = $arr_night_duty_name[$i];

		echo("<option value=\"$night_duty_value\"");
		if ($night_duty_value == $night_duty) {
			echo(" selected");
		}
		echo(">$night_duty_name");
	}
}

?>
