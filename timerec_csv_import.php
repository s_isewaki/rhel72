<?
//====================================================================
// 勤怠管理
// タイムレコーダCSVデータ自動収集 php
//====================================================================
echo("Time Recorder CSV Auto Import [START]\n");

// 引数の取得
$dakoku_dir = $argv[1];	// 打刻ファイル格納ディレクトリ
$csv_save_dir = $argv[2];// 処理用ディレクトリ	
$ext = $argv[3];			// 拡張子
$maker_flg = $argv[4];	// タイムレコーダの種別 1:マックス 2:アマノ 3:Fitware 4:セコム

// ディレクトリの存在を確認
if ( !is_dir($dakoku_dir) ) {
    echo "dakoku_dir is not found: $dakoku_dir\n" ;
    exit;
}
if ( !is_dir($csv_save_dir) ) {
    echo "csv_save_dir is not found: $csv_save_dir\n" ;
    exit;
}

// タイムレコーダ連携ライブラリの読み込み
ob_start();
require_once('about_postgres.php');
require_once('timerec_class.php');
ob_end_clean();

//プログラム名
$fname = $argv[0];
//ＤＢのコネクション作成
$con = connect2db($fname);
$timerec = new timerec_class($con, $fname);

// ファイルの処理
$files = glob("$dakoku_dir/*.$ext");
foreach( $files as $file ) {
    echo basename($file). "\n";

    // ファイルの移動
    $move_file = "$csv_save_dir/".date('Ymd_His_').basename($file) ;
    rename($file, $move_file);
    echo "  file move: $dakoku_dir --> $csv_save_dir\n";

    // タイムレコーダ処理
	/**
	 * 打刻データインポート処理
	 *
	 * @param string $csv_file_name データファイル名
	 * @param string $mode 処理モード 1:画面 2:バッチ処理
	 * @param string $encoding 文字コード 1:SJIS 2:EUC 3:UTF-8
     * @param string $maker_flg  タイムレコーダの種別 1:マックス 2:アマノ 3:Fitware 4:セコム
	 * @return 処理結果 0:正常終了 1:パラメータエラー -1:DBエラー（$timerec->get_dberr_msg()でDBエラー内容取得）
	 * 　　　　　$timerec->get_result_msg()で画面用の処理結果を取得できる。
	 */
	$res = $timerec->dakoku_file($move_file, "2", "1", $maker_flg);
    if ( $res == 0 ) {
        echo "  csv import: OK\n";
    } else {
        echo "  csv import: NG\n";
    }
}

//ＤＢのコネクションクローズ
pg_close($con);

echo("Time Recorder CSV Auto Import [END]\n");
?>