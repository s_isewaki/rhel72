<?/*

画面パラメータ
	$session セッションID

*/?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_tmpl_header.ini");
require("summary_common.ini");
require("label_by_profile_type.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療記録使用権限のチェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//診療区分一覧の取得(論理未削除データのみ)
$emp_id = get_emp_id($con, $session, $fname);
$tbl = "select * from divmst";
//$cond = "where (emp_id = '$emp_id') and div_del_flg = 'f' order by div_id";
$cond = "where div_del_flg = 'f' order by div_id";
$kubun_data_set = select_from_table($con,$tbl,$cond,$fname);//データリスト
$kubun_data_len = pg_numrows($kubun_data_set);//データ件数

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療記録区分/記録区分
$summary_record_kubun_title = $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo($med_report_title); ?>管理 | <?=$summary_record_kubun_title?>関連付け</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function diag_sel(){
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.in_list {border-collapse:collapse;}
.in_list td {border:#FFFFFF solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_TMPL"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<?
show_sinryo_tmpl_header($session,$fname,"KUBUN_LINK");
?>
<form action="summary_tmpl_regist.php" method="post">
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff">
<td width="200" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$summary_record_kubun_title?>
</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用するテンプレート名称</font></td>
</tr>
<?
//診療区分ループ(一覧用)
for($i=0;$i<$kubun_data_len;$i++){
	$div_id      = pg_result($kubun_data_set,$i,"div_id");
	$div_name    = pg_result($kubun_data_set,$i,"diag_div");
	$old_tmpl_id = @pg_result($kubun_data_set,$i,"tmpl_id");
	$div_name_html = h($div_name);

	//診療区分に紐付けられているテンプレートを取得
	$tbl = "select tmplmst.tmpl_id, tmplmst.tmpl_name, tmpl_div_link.default_flg"
			." from tmplmst,tmpl_div_link";
	$cond = "where tmpl_div_link.div_id = '$div_id' and tmpl_div_link.tmpl_id = tmplmst.tmpl_id "
			."order by tmplmst.tmpl_id";
	$link_data_set = select_from_table($con,$tbl,$cond,$fname);//データリスト
	$link_data_len = pg_numrows($link_data_set);//データ件数

	//1つもデフォルトテンプレートがない場合は先頭テンプレートをデフォルトとする。
	$is_first_default = true;//先頭デフォルトフラグ
	for($itmpl=0;$itmpl<$link_data_len;$itmpl++){
		$default_flg = pg_result($link_data_set,$itmpl,"default_flg");
		if($default_flg == "t")
		{
			$is_first_default = false;
			break;
		}
	}

	?>
	<tr height="22">
	<td width="200" align="right" bgcolor="#f6f9ff">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="sinryo_tmpl_div.php?session=<? echo($session) ?>&div_id=<? echo($div_id) ?>">
		<? echo($div_name_html) ?>
		</a>
		</font>
		</td>
	<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	if($link_data_len == 0){
		?>
		&nbsp;なし
		<?
	}
	else{
		?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="in_list">
		<?
		//テンプレートループ(ラジオボタンリスト用)
		for($itmpl=0;$itmpl<$link_data_len;$itmpl++){
			$tmpl_id   = pg_result($link_data_set,$itmpl,"tmpl_id");
			$tmpl_name = pg_result($link_data_set,$itmpl,"tmpl_name");
			$default_flg = pg_result($link_data_set,$itmpl,"default_flg");
			$tmpl_name_html = h($tmpl_name);

			//選択判定
			$checked_element = "";
			if($default_flg == "t")//デフォルトフラグ=TRUEの場合
			{
				$checked_element = "checked";
			}
			elseif(@$tmpl==0 && $is_first_default)//先頭テンプレート＆先頭デフォルトフラグ=TRUEの場合
			{
				$checked_element = "checked";
			}

			?>
			<tr>
			<td width="10"><input type="radio" name="default_tmpl_of_div<? echo($div_id); ?>" value="<? echo($tmpl_id); ?>" <? echo($checked_element); ?>></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmpl_name_html); ?></font></td>
			</tr>
			<?
		}
		?>
		</table>
		<?
	}
	?>
		</font></td>
	</tr>
	<?
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
</form>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
※<?=$summary_record_kubun_title?>名称をクリックするとテンプレート選択画面が表示されます。<br>
※テンプレートを複数使用する場合はデフォルトを指定してください。
</font>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
