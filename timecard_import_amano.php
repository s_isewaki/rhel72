<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 勤務実績（アマノ）</title>
<?php
ini_set("max_execution_time", 0);
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("timecard_import_amano_common.php");
require_once("timecard_import_menu_common.php");
require_once("atdbk_menu_common.ini");
require_once("date_utils.php");
require_once("atdbk_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//削除
if ($mode == "delete") {
	deleteImportSetDataAmano($con, $fname, $no);
}

$cnvdata = get_cnvdata_amano($con, $fname, "");
$arr_cnvtbl = array(); //key[{$duty_ptn}_{$absence_reason}]["tmcd_group_id" or "atdptn_ptn_id" or "reason"]

for ($i=0; $i<count($cnvdata); $i++) {
    $wk_duty_ptn = ($cnvdata[$i]["duty_ptn"] == "") ? "0" : $cnvdata[$i]["duty_ptn"];
    $wk_absence_reason = ($cnvdata[$i]["absence_reason"] == "") ? "0" : $cnvdata[$i]["absence_reason"];
    $wk_key = $wk_duty_ptn."_".$wk_absence_reason;
    
    $arr_cnvtbl[$wk_key]["tmcd_group_id"] = $cnvdata[$i]["tmcd_group_id"];
    $arr_cnvtbl[$wk_key]["atdptn_ptn_id"] = $cnvdata[$i]["atdptn_ptn_id"];
    $arr_cnvtbl[$wk_key]["reason"] = $cnvdata[$i]["reason"];
    $arr_cnvtbl[$wk_key]["previous_day_possible_flag"] = $cnvdata[$i]["previous_day_possible_flag"];
    $arr_cnvtbl[$wk_key]["previous_day_flag"] = $cnvdata[$i]["previous_day_flag"];
}

//インポート
if ($mode == "import") {
	// ファイルが正常にアップロードされたかどうかチェック
	$uploaded = false;
	if (array_key_exists("csvfile", $_FILES)) {
		if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
			$uploaded = true;
		}
	}
	if (!$uploaded) {
		echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'timecard_import_amano.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	// 文字コードの設定
	switch ($encoding) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
	}
    //出勤表関連共通クラス
    $atdbk_common_class = new atdbk_common_class($con, $fname);
    //会議・研修時間情報
    $arr_meeting_time = $atdbk_common_class->get_atdbk_meeting_time();
    
	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character(0x30FB);
	// トランザクションを開始
	pg_query($con, "begin");

	// CSVデータを配列に格納
	$shifts = array();
	$no = 1;
	// 職員ID
	$emp_personal_ids = array();

	$lines = file($_FILES["csvfile"]["tmp_name"]);
	//休暇申請確認用対象期間
	$target_start_date = "";
	$target_end_date = "";
	
	foreach ($lines as $line) {

        // 先頭に、BOMがある場合は除く
        if ($no == 1) {
            $wk_bom1 = ord(substr($line, 0, 1));
            $wk_bom2 = ord(substr($line, 1, 1));
            $wk_bom3 = ord(substr($line, 2, 1));
            
            if ($wk_bom1 == 0xef && $wk_bom2 == 0xbb && $wk_bom3 == 0xbf) {
                $line = substr($line, 3);
            }
        }
        // 文字コードを変換 ID日時等、数値のみのためコメント化、もし2バイト文字が追加されたら有効にする
		//$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));
        $line = trim($line);
        
		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		// 空行は無視
		if ($line == "") {
			continue;
		}

		// カンマで分割し配列へ設定
		$shift_data = split(",", $line);

		$shifts[$no] = $shift_data;
		$wk_id = $shift_data[1];
		$emp_personal_ids[$wk_id] = $wk_id;
		
		if ($target_start_date == "" ||
			$target_start_date > $shift_data[0]) {
			$target_start_date = $shift_data[0];
		}
		if ($target_end_date == "" ||
			$target_end_date < $shift_data[0]) {
			$target_end_date = $shift_data[0];
		}

		$no++;
	}

	//内部用職員ID取得
	$arr_emp_id = get_arr_emp_id($con, $fname, $emp_personal_ids);
	//休暇申請情報取得
	$arr_hol_info = get_hol_info($con, $fname, $target_start_date, $target_end_date);

	//処理件数
	$reg_cnt = 0;
	$err_cnt = 0;

	//メッセージ
	$errmsgs = array();
	$errmsgs[] = "インポート処理開始<br>";
	foreach ($shifts as $no => $shift_data) {

        $rtn_info = update_shift_data($con, $fname, $session, $shift_data, $arr_cnvtbl, $arr_emp_id, $arr_meeting_time, $arr_hol_info);

		if ($rtn_info["rtncd"] == 0) {
			$reg_cnt++;
		} elseif ($rtn_info["rtncd"] == 1) {
			$arr_msg = $rtn_info["errmsg"];
			for ($i=0; $i<count($arr_msg); $i++) {
				$errmsgs[] = "{$no}行目：".$arr_msg[$i]."<br>\n";
			}
			$err_cnt++;
		}
		//$rtn_info["rtncd"] == 2は処理なし
	}
	// トランザクションをコミット
	pg_query($con, "commit");

	$errmsgs[] = "インポート処理終了<br>";
	$errmsgs[] = "正常登録：{$reg_cnt}件<br>";
	$errmsgs[] = "エラー　：{$err_cnt}件<br>";
}

if ($encoding == "") {
	$encoding = "1";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var childwin = null;
function openUpdateWin(ins_upd_flg, no) {
	dx = screen.width;
	dy = screen.top;
	wx = 860;
	wy = 240;
	base = screen.height / 2 - (wy / 2);
	var url = './timecard_import_set_amano.php';
	url += '?session=<?=$session?>';
	url += '&ins_upd_flg='+ins_upd_flg;
	url += '&no='+no;
	childwin = window.open(url, 'timecardimpopup', 'left='+((dx-wx)/2)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeUpdateWin() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function deleteData(no) {

	if (confirm('削除してよろしいですか？')) {

		document.mainform.mode.value = "delete";
		document.mainform.no.value = no;
		document.mainform.action = "timecard_import_amano.php";
		document.mainform.submit();

	}
}

function reload_list() {
	document.mainform.mode.value = "";
	document.mainform.action = "timecard_import_amano.php";
	document.mainform.submit();
}

function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	document.mainform.mode.value = "import";
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//インポートメニュータブ表示
show_timecard_import_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form id="mainform" name="mainform" action="timecard_import_amano.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アマノ就業管理からの勤務実績インポート</font>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<?
if ($mode == "import") {
?>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録結果メッセージ<br>
<?
	foreach ($errmsgs as $errmsg) {
		echo($errmsg);
	}
}
?>
</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="document.getElementById('csv_fmt').style.display = (document.getElementById('csv_fmt').style.display == 'none') ? '' : 'none';"><b>CSVのフォーマット</b></a></font>
<div id="csv_fmt" style="display:none">
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行してください。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">処理年月日（YYYYMMDD形式）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID（8桁0埋めなし）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務区分（3桁0埋めなし）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不在理由（2桁0埋めなし）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻（hmmまたはhhmm形式）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻（hmmまたはhhmm形式）</font></li>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">20130708,1234,1,0,802,1743</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">20130708,1234,1,11,,</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">20130708,1234,15,0,1602,904</font></dd>
</dl>
</dd>
</dl>
</div>
<br>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフト種類変換設定</font>

<table width="860" border="0" cellspacing="0" cellpadding="2" class="list">

<tr>
<td width="150" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務区分</b></font></td>
<td width="150" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>不在理由</b></font></td>
<td width="40" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>&nbsp;</b></font></td>
<td width="160" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン<br>グループ</b></font></td>
<td width="90" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン</b></font></td>
<td width="90" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>事由</b></font></td>
<td width="180" bgcolor="#f6f9ff" align="right"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font><input type="button" value="追加" onclick="openUpdateWin('1', 0);"></td>
</tr>
<?
// 表示処理
for ($i=0; $i<count($cnvdata); $i++) {
?>
<tr>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["duty_ptn"]); ?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["absence_reason"]); ?></font></td>
<td align="center"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">→</font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["tmcd_group_name"]); ?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
if ($cnvdata[$i]["atdptn_nm"] != "") {
	echo($cnvdata[$i]["atdptn_nm"]);
} else {
	echo("未設定");
}
?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["reason_name"]); ?></font></td>
<td align="right"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font><input type="button" value="削除" onclick="deleteData(<?echo($i+1);?>);">
<input type="button" value="変更" onclick="openUpdateWin('2', <?echo($i+1);?>);">
<input type="button" value="追加" onclick="openUpdateWin('1', <?echo($i+1);?>);"></td>
</tr>

<?
}
?>

</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="no" value="">

</form>
</td>
</tr>
</table>


</body>
<? pg_close($con); ?>
</html>

<?php
/**
 * 勤務実績データ登録
 *
 * @param object $con DBコネクション
 * @param string $fname ファイル名
 * @param string $session セッション
 * @param array $shift_data インポートデータの配列
 * @param array $arr_cnvtbl 変換用情報
 * @param array $arr_emp_id 職員IDの配列
 * @param array $arr_meeting_time 会議・研修時間情報の配列
 * @param array $arr_hol_info 休暇申請情報の配列
 * @return array 登録結果の配列 key:rtncd,errmsg
 *
 */
function update_shift_data($con, $fname, $session, $shift_data, $arr_cnvtbl, $arr_emp_id, $arr_meeting_time, $arr_hol_info)
{

	$arr_msg = array();
	$rtn_info = array();
	$rtn_info["rtncd"] = 0;

	//入力データチェック
	//カラム数チェック
	$column_count = 6;
	if (count($shift_data) != $column_count) {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "カラム数が不正です。".join(",", $shift_data);
		$rtn_info["errmsg"] = $arr_msg;
		return $rtn_info;
	}

	//実績データ確認
	//職員ID確認
	$emp_personal_id = $shift_data[1];
	$emp_id = $arr_emp_id["$emp_personal_id"]["emp_id"];
	if ($emp_id == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "職員($shift_data[1])がマスタに登録されていません。";
	}
	//日付 範囲、妥当性
	if ($shift_data[0] == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "日付が設定されていません。";
	} else {
		$wk_yyyy = substr($shift_data[0], 0, 4);
		$wk_mm = substr($shift_data[0], 4, 2);
		$wk_dd = substr($shift_data[0], 6, 2);
		$date = $wk_yyyy.$wk_mm.$wk_dd;
		if (!checkdate($wk_mm, $wk_dd, $wk_yyyy)) {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "日付($shift_data[0])が正しくありません。";
		}
	}

	//変換設定確認
    $wk_duty_ptn = ($shift_data[2] == "") ? "0" : $shift_data[2];
    $wk_absence_reason = ($shift_data[3] == "") ? "0" : $shift_data[3];
    $wk_key = $wk_duty_ptn."_".$wk_absence_reason;
    
	$tmcd_group_id = $arr_cnvtbl[$wk_key]["tmcd_group_id"];
	$pattern = $arr_cnvtbl[$wk_key]["atdptn_ptn_id"];
	$reason = $arr_cnvtbl[$wk_key]["reason"];
    $previous_day_possible_flag = $arr_cnvtbl[$wk_key]["previous_day_possible_flag"];
    $atdptn_previous_day_flag = $arr_cnvtbl[$wk_key]["previous_day_flag"];
    
	//変換設定に存在しない
	if ($tmcd_group_id == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "勤務シフト種類変換設定($shift_data[2],$shift_data[3])が登録されていません。";
	}
    //時刻妥当性チェック
    $start_time = $shift_data[4];
    if ($start_time != "") {
        if (strlen($start_time) == 3) {
            $start_time = "0".$start_time;
        }
        $start_hh = intval(substr($start_time, 0, 2));
        $start_mm = intval(substr($start_time, 2, 2));
        if ($start_hh < 0 || $start_hh > 23 || $start_mm < 0 || $start_mm > 59) {
    		$rtn_info["rtncd"] = 1;
            $arr_msg[] = "出勤時刻($shift_data[4])が正しくありません。";
        }
    }
    $end_time = $shift_data[5];
    if ($end_time != "") {
        if (strlen($end_time) == 3) {
            $end_time = "0".$end_time;
        }
        $end_hh = intval(substr($end_time, 0, 2));
        $end_mm = intval(substr($end_time, 2, 2));
        if ($end_hh < 0 || $end_hh > 23 || $end_mm < 0 || $end_mm > 59) {
            $rtn_info["rtncd"] = 1;
            $arr_msg[] = "退勤時刻($shift_data[5])が正しくありません。";
        }
    }
    
	//エラー時
	if ($rtn_info["rtncd"] == 1) {
		$rtn_info["errmsg"] = $arr_msg;
		return $rtn_info;
	}
    //前日フラグ
    $previous_day_flag = null;
    //翌日フラグ
    $next_day_flag = null;
    if ($start_time != "" && $end_time != "") {
        if ($start_time > $end_time) {
            //前日になる場合があるフラグ、前日であるフラグ確認
            if ($previous_day_possible_flag == "1" || $atdptn_previous_day_flag == "1") {
                $previous_day_flag = 1;
            }
            else {
                $next_day_flag = 1;
            }
        }       
    }
    //出勤時刻のみ、かつ、前日になる場合、時間帯の所定終了時刻を判定に使用
    if ($start_time != "" && $end_time == "" &&
            ($previous_day_possible_flag == "1" || $atdptn_previous_day_flag == "1")) {
        $sql = "select officehours2_end from officehours ";
        $cond = "where tmcd_group_id = $tmcd_group_id and pattern = '$pattern' and officehours_id = '1' ";
        
        $sel = select_from_table($con,$sql,$cond,$fname);
        if($sel == 0){
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) > 0) {
            $wk_atdptn_end_time = pg_fetch_result($sel, 0, "officehours2_end");
            $wk_atdptn_end_time = str_replace(":", "", $wk_atdptn_end_time);
            if ($start_time > $wk_atdptn_end_time) {
                $previous_day_flag = 1;
            }
        }
    }
    $start_date = ($previous_day_flag == 1) ? date("Ymd", strtotime("-1 day", date_utils::to_timestamp_from_ymd($date))) : $date;
    $end_date = ($next_day_flag == 1) ? date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($date))) : $date;
    
    //休暇申請がある場合はスキップする
    $wk_hol_key = $emp_id."_".$date;
    if ($arr_hol_info[$wk_hol_key] == 1) {
		$rtn_info["rtncd"] = 2;
		return $rtn_info;
    }
    
	//実績データ確認
    $sql = "select pattern, reason, tmcd_group_id, start_time, end_time, meeting_start_time, meeting_end_time from atdbkrslt ";
	$cond = "where emp_id = '$emp_id' and date = '$date' ";

	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num == 0) {
	//追加
        $sql = "insert into atdbkrslt (emp_id, date, pattern, reason, tmcd_group_id, next_day_flag, previous_day_flag ";
        $content = array($emp_id, $date, $pattern, $reason, $tmcd_group_id, $next_day_flag, $previous_day_flag);
        //出勤時刻
        if ($start_time != "") {
            $sql .= ", start_time, start_btn_time, start_date ";
            array_push($content, $start_time);
            array_push($content, $start_time);
            array_push($content, $start_date);
        }
        //退勤時刻
        if ($end_time != "") {
            $sql .= ", end_time, end_btn_time, end_date ";
            array_push($content, $end_time);
            array_push($content, $end_time);
            array_push($content, $end_date);
        }
        //会議・研修
        if ($arr_meeting_time[$tmcd_group_id][$pattern]["meeting_start_time"] != "" && 
                $arr_meeting_time[$tmcd_group_id][$pattern]["meeting_end_time"] != "") {
            $sql .= " ,meeting_start_time, meeting_end_time ";
            array_push($content, $arr_meeting_time[$tmcd_group_id][$pattern]["meeting_start_time"]);
            array_push($content, $arr_meeting_time[$tmcd_group_id][$pattern]["meeting_end_time"]);
        }
        
		$sql .= ") values (";
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		//内容が変更されている時更新
		$old_pattern = pg_fetch_result($sel, 0, "pattern");
		$old_reason = pg_fetch_result($sel, 0, "reason");
		$old_tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
        $old_start_time = pg_fetch_result($sel, 0, "start_time");
        $old_end_time = pg_fetch_result($sel, 0, "end_time");
        //会議・研修
        $old_meeting_start_time = pg_fetch_result($sel, 0, "meeting_start_time");
        $old_meeting_end_time = pg_fetch_result($sel, 0, "meeting_end_time");
        $meeting_start_time = $arr_meeting_time[$tmcd_group_id][$pattern]["meeting_start_time"];
        $meeting_end_time = $arr_meeting_time[$tmcd_group_id][$pattern]["meeting_end_time"];
        
        if ($pattern != $old_pattern ||
                $reason != $old_reason ||
                $tmcd_group_id != $old_tmcd_group_id ||
                $start_time != $old_start_time ||
                $end_time != $old_end_time ||
                $meeting_start_time != $old_meeting_start_time ||
                $meeting_end_time != $old_meeting_end_time) {
			$sql = "update atdbkrslt set ";
            $set = array("pattern", "reason", "tmcd_group_id", "start_time", "start_btn_time", "start_date", "end_time", "end_btn_time", "end_date", "next_day_flag", "previous_day_flag", "meeting_start_time", "meeting_end_time");
            $setvalue = array($pattern, $reason, $tmcd_group_id, $start_time, $start_time, $date, $end_time, $end_time, $end_date, $next_day_flag, $previous_day_flag, $meeting_start_time, $meeting_end_time);

			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	return $rtn_info;
}

/**
 * 内部用職員ID取得
 *
 * @param object $con DBコネクション
 * @param string $fname ファイル名
 * @param array $emp_personal_ids 職員IDの配列
 * @return array 内部用職員IDの配列
 *
 */
function get_arr_emp_id($con, $fname, $emp_personal_ids) {

	$arr_id = array();

	if (count($emp_personal_ids) == 0) {
		return $arr_id;
	}

	$cond_add = "";
	foreach ($emp_personal_ids as $emp_personal_id) {
		if ($cond_add != "") {
			$cond_add .= " or ";
		}
        $cond_add .= " a.emp_personal_id = '$emp_personal_id' ";
	}
	$sql = "select a.emp_id, a.emp_personal_id from empmst a  ";
	$cond = "where $cond_add ";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$data = pg_fetch_all($sel);
	for ($i=0; $i<count($data); $i++) {
        $wk_emp_id = $data[$i]["emp_personal_id"]; //0詰めなし
        $arr_id["$wk_emp_id"]["emp_id"] = $data[$i]["emp_id"];
	}

	return $arr_id;
}
/**
 * 休暇申請情報取得
 *
 * @param object $con DBコネクション
 * @param string $fname ファイル名
 * @param string $target_start_date 対象開始日
 * @param string $target_end_date 対象終了日
 * @return array 休暇申請情報の配列 $arr[$emp_id."_".$date]=1 key:内部用職員ID_日付 val:1（存在する場合）
 *
 */
function get_hol_info($con, $fname, $target_start_date, $target_end_date) {

	$arr_hol_info = array();

	if ($target_start_date == "" ||
		$target_end_date == "") {
		return $arr_hol_info;
	}

	$sql = "select emp_id, start_date, end_date from kintai_hol ";
	$cond = "where (start_date >= '$target_start_date' and start_date <= '$target_end_date') or (end_date >= '$target_start_date' and end_date <= '$target_end_date') order by emp_id, start_date";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$data = pg_fetch_all($sel);
	for ($i=0; $i<count($data); $i++) {
        $wk_emp_id = $data[$i]["emp_id"];
        $wk_start_date = $data[$i]["start_date"];
        $wk_end_date = $data[$i]["end_date"];
        $tmp_date = $wk_start_date;
        while ($tmp_date <= $wk_end_date) {

        	$wk_key = $wk_emp_id."_".$tmp_date;
        	$arr_hol_info[$wk_key] = 1;
        	$tmp_date = date("Ymd", strtotime("+1 day", date_utils::to_timestamp_from_ymd($tmp_date)));

        }
	}

	return $arr_hol_info;
}
?>
