<?php
// 出張旅費
// 経路マスタ・データ更新
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

switch ($_REQUEST["mode"]) {
	// 表示順
	case 'no':
		$cnt = 1;
		foreach ($_REQUEST["no"] as $id) {
			$set = array(
				"no" => $cnt++,
				"updated_on" => date('Y-m-d H:i:s')
			);
			$sql = "UPDATE trip_transit SET ";
			$cond = "WHERE transit_id={$id}";
			$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				js_error_exit();
			}
		}
		break;

	// 表示フラグ
	case 'view':
		foreach ($_REQUEST["no"] as $id) {
			$set = array(
					"view_flg" => ($_REQUEST["view"][$id] == 't') ? 't' : 'f',
					"updated_on" => date('Y-m-d H:i:s')
					);
			$sql = "UPDATE trip_transit SET ";
			$cond = "WHERE transit_id={$id}";
			$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				js_error_exit();
			}
		}
		break;

	// 削除
	case 'del':
		foreach ($_REQUEST["delete"] as $id) {
			$sql = "DELETE FROM trip_transit WHERE transit_id={$id}";
			$del = delete_from_table($con, $sql, "", $fname);
			if ($del == "0") {
				pg_query($con, "rollback");
				pg_close($con);
				js_error_exit();
			}
		}
		break;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_transit.php?session=<?=$session?>';
</script>