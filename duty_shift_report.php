<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 届出書添付書類</title>

<?
ini_set("max_execution_time", 0);
require_once("about_comedix.php");
require_once("duty_shift_yui_calendar_util.ini");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
    ///-----------------------------------------------------------------------------
    //作成日付(現在日付)の取得
    ///-----------------------------------------------------------------------------
    $date = getdate();
    $create_yyyy = $date["year"];
    $create_mm = $date["mon"];
    $create_dd = $date["mday"];
    ///-----------------------------------------------------------------------------
    //表示年／月設定
    ///-----------------------------------------------------------------------------
    if ($duty_yyyy == "") { $duty_yyyy = $create_yyyy; }
    if ($duty_mm == "") { $duty_mm = $create_mm; }
    $day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
    ///-----------------------------------------------------------------------------
    //初期値設定
    ///-----------------------------------------------------------------------------
    //４週計算用のカレンダー年月日
    if ($date_y1 == "") { $date_y1 = $duty_yyyy; };
    if ($date_m1 == "") { $date_m1 = $duty_mm; };
    if ($date_d1 == "") { $date_d1 = 1; };
    $night_start_day = 1;

    if ($recomputation_flg == "") { $recomputation_flg = "1"; }
    $excel_flg = "";            //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
    //予定実績フラグのデフォルト設定
    if ($plan_result_flag == "") {
        $plan_result_flag = "3"; // デフォルトを3:実績と予定の組合せとする 20120326
    }

    //「看護師／准看護師」
    $sum_night_b = 0.0;             //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
    $sum_night_c = 0.0;             //月延べ勤務時間数の計〔Ｃ〕
    $sum_night_d = 0.0;             //月延べ夜勤時間数の計〔Ｄ〕
    $sum_night_e = 0.0;             //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
    $sum_night_f = 0.0;             //1日看護配置数〔(A ／届出区分の数)×３〕
    $sum_night_g = 0.0;             //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
    $sum_night_h = 0.0;             //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
    $sum_night_i = 0.0;             //１日看護夜間配置数　〔A ／１２〕
    //「看護補助者」
    $sum_sub_night_b = 0.0;         //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
    $sum_sub_night_c = 0.0;         //月延べ勤務時間数の計〔Ｃ〕
    $sum_sub_night_d = 0.0;         //月延べ夜勤時間数の計〔Ｄ〕
    $sum_sub_night_e = 0.0;         //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
    $sum_sub_night_f = 0.0;         //1日看護配置数〔(A ／届出区分の数)×３〕
    $sum_sub_night_g = 0.0;         //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕

    $ward_cnt = "0";                //病棟数
    $sickbed_cnt = "0";             //病床数
    $report_kbn = "";               //届出区分
    $report_patient_cnt = "0";      //届出時入院患者数
    $hosp_patient_cnt = "0";        //一日平均入院患者数
    $nursing_assistance_flg = "";   //看護補助加算の有無
    $hospitalization_cnt = "0";     //平均在院日数
    $prov_start_hh = "";            //夜勤時間帯（開始時間）
    $prov_start_mm = "";            //夜勤時間帯（開始分）
    $prov_end_hh = "";              //夜勤時間帯（終了時間）
    $prov_end_mm = "";              //夜勤時間帯（終了分）
    $labor_cnt = "0";               //所定労働時間

    //直近の1年間
    $compute_start_yyyy = $duty_yyyy - 1;
    $compute_start_mm = $duty_mm;
    $compute_end_yyyy = $duty_yyyy;
    $compute_end_mm = $duty_mm - 1;
    if ((int)$compute_end_mm < 1) {
        $compute_end_yyyy--;
        $compute_end_mm = 12;
    }

    //直近の三ヵ月
    $ave_start_yyyy = $duty_yyyy;
    $ave_start_mm = $duty_mm - 2;
    if ((int)$ave_start_mm < 1) {
        $ave_start_yyyy--;
        $ave_start_mm += 12;
    }
    $ave_end_yyyy = $duty_yyyy;
    $ave_end_mm = $duty_mm;
    ///-----------------------------------------------------------------------------
    //終了年月
    ///-----------------------------------------------------------------------------
    $end_yyyy = (int)$duty_yyyy;
    $end_mm = (int)$duty_mm;
    $end_dd = (int)$day_cnt;
    if ($recomputation_flg == "2") {
        ///-----------------------------------------------------------------------------
        //４週計算時の年月チェック
        ///-----------------------------------------------------------------------------
        $wk_err_flg = $obj_report->checkYm4Week($duty_yyyy, $duty_mm, $date_y1, $date_m1);
        
        ///-----------------------------------------------------------------------------
        //４週計算用の終了年月日設定
        ///-----------------------------------------------------------------------------
        $night_start_day = $date_d1;
        if ($wk_err_flg == "") {
            $obj_report->setEndYmd4Week($night_start_day, $duty_yyyy, $duty_mm, $end_yyyy, $end_mm, $end_dd);
        }
        $day_cnt = 28;
    } else {
        $date_d1 = 1;
    }
    ///-----------------------------------------------------------------------------
    //医療機関情報の取得
    ///-----------------------------------------------------------------------------
    $sql = "select * from profile";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $num = pg_num_rows($sel);
    if ($num > 0) {
        $hospital_name = pg_fetch_result($sel, 0, "prf_name");
    }
    ///-----------------------------------------------------------------------------
    //届出区分情報の取得
    ///-----------------------------------------------------------------------------
    $report_kbn_array = $obj->get_report_kbn_array();

    ///-----------------------------------------------------------------------------
    //施設基準情報（履歴）の取得
    ///-----------------------------------------------------------------------------
    //念の為、配列を初期化
    $specific_data = array(    //特定入院料関連データ
            "specific_type" => "",
            "nurse_staffing_cnt" => "",
            "assistance_staffing_cnt" => ""
            );
    $send_hm = array(    //申し送り時間
            "send_start_hhmm_1" => "",
            "send_end_hhmm_1" => "",
            "send_start_hhmm_2" => "",
            "send_end_hhmm_2" => "",
            "send_start_hhmm_3" => "",
            "send_end_hhmm_3" => "");

    $standard_array = $obj->get_duty_shift_institution_standard_array("");
    for ($i = 0; $i < count($standard_array); $i++){
        if ($standard_id == "") {
            $standard_id = $standard_array[0]["standard_id"];
        }

        if ($standard_id != $standard_array[$i]["standard_id"]) {
            continue;
        }

        ///-----------------------------------------------------------------------------
        //施設基準情報（履歴）の取得
        ///-----------------------------------------------------------------------------
        $reload_btn_flg = isset($_POST["reload_btn_flg"]) ? $_POST["reload_btn_flg"] : "";
        if ($reload_btn_flg == "1") {    //施設基準再読込時は、マスタを読込み
            $history_array = $obj->get_duty_shift_institution_standard_array($standard_id);
        } else {
            $history_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);
        }
        $ward_cnt = $history_array[0]["ward_cnt"];                              //病棟数
        $sickbed_cnt = $history_array[0]["sickbed_cnt"];                        //病床数
        $report_kbn = $history_array[0]["report_kbn"];                          //届出区分
        $report_patient_cnt = $history_array[0]["report_patient_cnt"];          //届出時入院患者数
        if ($recomputation_btn_flg == "1" || $reload_btn_flg == "1") {
            //再計算、施設基準再読込時は、以下の2項目のみ入力値を引継ぐ
            $hosp_patient_cnt = $_POST["hosp_patient_cnt"];                     //一日平均入院患者数
            $hospitalization_cnt = $_POST["inp_hospitalization_cnt"];           //平均在院日数
        } else {
            $hosp_patient_cnt = $history_array[0]["hosp_patient_cnt"];          //一日平均入院患者数
            $hospitalization_cnt = $history_array[0]["hospitalization_cnt"];    //平均在院日数
        }
        $nursing_assistance_flg = $history_array[0]["nursing_assistance_flg"];  //看護補助加算の有無
        $prov_start_hh = substr($history_array[0]["prov_start_hhmm"], 0, 2);    //夜勤時間帯（開始時間）
        $prov_start_mm = substr($history_array[0]["prov_start_hhmm"], 2, 2);    //夜勤時間帯（開始分）
        $prov_end_hh = substr($history_array[0]["prov_end_hhmm"], 0, 2);        //夜勤時間帯（終了時間）
        $prov_end_mm = substr($history_array[0]["prov_end_hhmm"], 2, 2);        //夜勤時間帯（終了分）
        $labor_cnt = $history_array[0]["labor_cnt"];                            //所定労働時間
        $nurse_staffing_flg = $history_array[0]["nurse_staffing_flg"];          //看護配置加算の有無
        $acute_nursing_flg = $history_array[0]["acute_nursing_flg"];            //急性期看護補助加算の有無
        $acute_nursing_cnt = $history_array[0]["acute_nursing_cnt"];            //急性期看護補助加算の配置比率
        $nursing_assistance_cnt = $history_array[0]["nursing_assistance_cnt"];  //看護補助加算の配置比率
        /* 様式９平成２４年度改訂対応 20120406 */
        $acute_assistance_flg = $history_array[0]["acute_assistance_flg"];  //
        $acute_assistance_cnt = $history_array[0]["acute_assistance_cnt"];  //
        $night_acute_assistance_flg = $history_array[0]["night_acute_assistance_flg"];  //
        $night_acute_assistance_cnt = $history_array[0]["night_acute_assistance_cnt"];  //
        $night_shift_add_flg = $history_array[0]["night_shift_add_flg"];    //
        //「特定入院料」、「特定入院料（看護職員＋看護補助者)」追加対応 20121011
        //(メソッドへ渡す引数をこれ以上増やしたくない為、連想配列にセットして扱うものとする)
        $specific_data["specific_type"] = $history_array[0]["specific_type"];                        //特定入院料種類
        $specific_data["nurse_staffing_cnt"] = $history_array[0]["nurse_staffing_cnt"];              //看護配置
        $specific_data["assistance_staffing_cnt"] = $history_array[0]["assistance_staffing_cnt"];    //看護補助配置
        //申し送り時間 20130418追加
        for ($j = 1; $j <= 3; $j++) {
            $send_hm["send_start_hhmm_{$j}"] = $history_array[0]["send_start_hhmm_{$j}"];
            $send_hm["send_end_hhmm_{$j}"] = $history_array[0]["send_end_hhmm_{$j}"];
        }
    }
    
    ///-----------------------------------------------------------------------------
    //届出区分名称設定
    ///-----------------------------------------------------------------------------
    for($i=0; $i<count($report_kbn_array); $i++) {
        if ($report_kbn_array[$i]["id"] == $report_kbn) {
            $report_kbn_name = $report_kbn_array[$i]["name"];
            break;
        }
    }
    
    ///-----------------------------------------------------------------------------
    // 様式９用　看護職員情報取得
    ///-----------------------------------------------------------------------------
    $data = $obj_report->get_report_array(
                                        $standard_id,
                                        $duty_yyyy,
                                        $duty_mm,
                                        $night_start_day,
                                        $end_yyyy,
                                        $end_mm,
                                        $end_dd,
                                        $day_cnt,
                                        $history_array[0],
                                        "",
                                        $plan_result_flag);
    if (isset($data["error_msg"])) {
        $data_array = array();
        $data_sub_array = array();

        $nurse_cnt = 0;
        $sub_nurse_cnt = 0;
        $assistance_nurse_cnt = 0;

        $nurse_sumtime = 0;
        $sub_nurse_sumtime = 0;
        $assistance_nurse_sumtime = 0;
        $error_msg = $data["error_msg"];
    } else {
        $data_array = $data["data_array"];
        $data_sub_array = $data["data_sub_array"];

        $nurse_cnt = $data["nurse_cnt"];                        //看護師数
        $sub_nurse_cnt = $data["sub_nurse_cnt"];                //准看護師数
        $assistance_nurse_cnt = $data["assistance_nurse_cnt"];  //看護補助者数

        $nurse_sumtime = $data["nurse_sumtime"];                        //看護師総勤務時間
        $sub_nurse_sumtime = $data["sub_nurse_sumtime"];                //准看護師総勤務時間
        $assistance_nurse_sumtime = $data["assistance_nurse_sumtime"];  //看護補助者総勤務時間
    }
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/duty_shift/report.js"></script>
<?
    ///-----------------------------------------------------------------------------
    // 外部ファイルを読み込む
    // カレンダー作成、関数出力
    ///-----------------------------------------------------------------------------
//  write_yui_calendar_use_file_read_0_12_2();
//  write_yui_calendar_script2(1);
    duty_shift_write_yui_calendar_use_file_read_0_12_2();
    duty_shift_write_yui_calendar_script2(1);
?>
<script type="text/javascript">
    ///-----------------------------------------------------------------------------
    /// 看護師協会版届出様式、SubWindowsOpen
    ///-----------------------------------------------------------------------------
    function nurseAssciateSubWin(report_kbn, year, month){
        if (report_kbn == <? echo(REPORT_KBN_SPECIFIC); ?> || report_kbn == <? echo(REPORT_KBN_SPECIFIC_PLUS); ?>
            || report_kbn == <? echo(REPORT_KBN_COMMUNITY_CARE); ?>
            || report_kbn == <? echo(REPORT_KBN_COMMUNITY_CARE2); ?>
            ) {
            // 届出区分が「特定入院料」又は「特定入院料（看護職員＋看護補助者）」の場合、平成２４年４月以降の新様式
            // 地域包括ケア追加
            if (!isNewStylePeriod(year, month)) {
                alert('平成２４年３月以前の様式はサポートしていません');
                return;
            }
        }
        var no_message = 'ok';
        if ((document.nurseForm1.nurse_cnt.value + document.nurseForm1.sub_nurse_cnt.value + document.nurseForm2.assistance_nurse_cnt.value) == 0) {
            no_message = 'no';
        }

            var url = 'duty_shift_report_nurse_associate_excelpara.php';
            url += '?';
            url += no_message;
            url += '&session=<? echo $session; ?>';
//          url += '&emp_id=<? echo $emp_id; ?>';
        childwin_excel = window.open(url , 'CoMedixExcelNurseAssociate', 'width=550,height=500,scrollbars=yes,resizable=yes');
            childwin_excel.focus();
    }
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

table.outer {width: 1050px;}

.basis_shift {background-color: #ffffe0;}
.basis_shift_input {background-color: #ffd700;}
.summary {background-color: #eeffee;}
.labor_time {background-color: #ffe4e1;}
</style>
</head>
    <!-- ************************************************************************ -->
    <!-- 様式３の３ -->
    <!-- ************************************************************************ -->
    <?
        if (count($standard_array) <= 0) {
            echo("<body bgcolor=\"#ffffff\" text=\"#000000\" topmargin=\"0\" leftmargin=\"0\" marginheight=\"0\" marginwidth=\"0\">\n");
        } else {
            echo("<body bgcolor=\"#ffffff\" text=\"#000000\" topmargin=\"0\" leftmargin=\"0\" marginheight=\"0\" marginwidth=\"0\" onload=\"initcal(); updateCal1();\">\n");
        }
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 画面遷移／タブ -->
    <!-- ------------------------------------------------------------------------ -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
    <?
        // 画面遷移
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_user_title($session, $section_admin_auth);         //duty_shift_common.ini
        echo("</table>\n");

        // タブ
        $arr_option = "";
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        show_user_tab_menuitem($session, $fname, $arr_option);  //duty_shift_user_tab_common.ini
        echo("</table>\n");

        // 下線
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
    ?>
    </td></tr></table>
    <!-- ------------------------------------------------------------------------ -->
    <!-- データ不備時 -->
    <!-- ------------------------------------------------------------------------ -->
    <?
        if (count($standard_array) <= 0) {
            echo("施設基準情報が登録されていません。管理画面で登録してください。");
        } else {
    ?>
    <!-- ------------------------------------------------------------------------ -->
    <!-- メニュー（施設基準１〜１０）-->
    <!-- ------------------------------------------------------------------------ -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;">
    <tr height="22" >
    <td style="line-height: 18px;">&nbsp;&nbsp;
    <?
        for ($i=0;$i<count($standard_array);$i++){
            $k = $standard_array[$i]["standard_id"];
            $wk = "施設基準" . $k ."&nbsp;&nbsp;";
            if ($standard_id == $k) { echo($wk); } else {
                echo("<a href=\"duty_shift_report.php?session=$session&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&standard_id=$k\">$wk</a>&nbsp;&nbsp;");
            }
        }
    echo("<br>\n&nbsp;&nbsp;");
    //施設基準が合う病棟を全て表示する
    $data = $obj->get_group_id_from_standard_id($standard_id);
    for ($i=0; $i<count($data); $i++) {
        $group_id = $data[$i]["group_id"];
        $group_name = $data[$i]["group_name"];
        echo("$group_name&nbsp;&nbsp;");
    }
    ?>
    </td>
    </tr>
    </table>
    <!-- ------------------------------------------------------------------------ -->
    <!-- 年月 -->
    <!-- ------------------------------------------------------------------------ -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: ＭＳ Ｐゴシック, Osaka; font-size: 15px;">
    <form name="mainForm" id="mainForm" method="post">
    <?
        $url = "duty_shift_report.php";
        //-------------------------------------------------------------------------------
        // 年
        //-------------------------------------------------------------------------------
        echo("<tr height=\"22\">\n");
        echo("<td width=\"100%\" align=\"left\">&nbsp;&nbsp; \n");
        $wk_next = $duty_yyyy + 1;
        $wk_back = $duty_yyyy - 1;
        echo("<a href=\"$url?session=$session&duty_yyyy=$wk_back&duty_mm=$duty_mm&standard_id=$standard_id\">←</a>&nbsp; \n");
        echo("{$duty_yyyy}年&nbsp; \n");
        echo("<a href=\"$url?session=$session&duty_yyyy=$wk_next&duty_mm=$duty_mm&standard_id=$standard_id\">→</a>&nbsp; \n");
    echo("&nbsp;\n");
    if (($plan_result_flag == "1") || ($plan_result_flag == "")){
        $checked1 = "checked";
    }
    if ($plan_result_flag == "2") {
        $checked2 = "checked";
    }
    if ($plan_result_flag == "3") {
        $checked3 = "checked";
    }
    echo("<label><input type=\"radio\" name=\"plan_result_flag\" id=\"plan_result_flag\" value=\"2\" $checked2 onclick=\"remake(this.value)\">予定から作成</label>\n");
    echo("<label><input type=\"radio\" name=\"plan_result_flag\" id=\"plan_result_flag\" value=\"1\" $checked1 onclick=\"remake(this.value)\">実績から作成</label>\n");
    echo("<label><input type=\"radio\" name=\"plan_result_flag\" id=\"plan_result_flag\" value=\"3\" $checked3 onclick=\"remake(this.value)\">実績と予定を組み合わせて作成</label>\n");
    echo("</td>\n");
        echo("</tr>\n");
        //-------------------------------------------------------------------------------
        // 月
        //-------------------------------------------------------------------------------
        echo("<tr height=\"22\"> \n");
        echo("<td width=\"100%\" align=\"left\">&nbsp;&nbsp; \n");
        for ($i=1;$i<=12;$i++) {
            if ((int)$duty_mm == (int)$i) {
                $wk = $i . "月&nbsp; \n";
                echo($wk);
            } else {
                echo("<a href=\"$url?session=$session&duty_yyyy=$duty_yyyy&duty_mm=$i&standard_id=$standard_id\"> $i 月</a>&nbsp; \n");
            }
        }
        echo("</font></td>\n");
        echo("</tr>\n");
    ?>
    </table>

    <!-- ************************************************************************ -->
    <!-- 入院基本料の施設基準等に係る届出書添付書類(様式９) -->
    <!-- ************************************************************************ -->
        <!-- ------------------------------------------------------------------------ -->
        <!-- 「看護職員表作成」「看護補助者表作成」ボタン -->
        <!-- ------------------------------------------------------------------------ -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;">
        <tr height="22">
        <?
            echo("<td align=\"left\">\n");
            $phpVersionName=phpversion();
            if ( $phpVersionName >= "5.1" ){
                //「看護協会版届出様式」
                $wk_name = "届出様式"; //「看護協会版」を除く(2014年から保険医団体連合会からの配布となったため) 20140901
                $wk_style = "WIDTH: 160px; HEIGHT: 22px;";
//              echo("<input type=\"button\" value=\"$wk_name\" onclick=\"makeAssociate();\" style=\"$wk_style\">\n");
                echo("<input type=\"button\" value=\"$wk_name\" onclick=\"nurseAssciateSubWin($report_kbn, $duty_yyyy, $duty_mm);\" style=\"$wk_style\">\n");
            }

            //「集計表作成」
            $wk_name = "集計表作成";
            $wk_style = "WIDTH: 100px; HEIGHT: 22px;";
    echo("<input type=\"button\" value=\"$wk_name\" onclick=\"document.xlsForm.action='duty_shift_report_nurse_excel.php';makeExcel();\" style=\"$wk_style\">\n");

            $wk_style = "WIDTH: 200px; HEIGHT: 22px;";
            //-------------------------------------------------------------------------------
            //「看護職員表作成」「看護補助者表作成」
            //-------------------------------------------------------------------------------
            if ($recomputation_flg == "2") {
                //-------------------------------------------------------------------------------
                //４週計算
                //-------------------------------------------------------------------------------
                $wk_name = "看護職員表作成　(４週計算)";
                echo("<input type=\"button\" value=\"$wk_name\" onclick=\"editNurse();\" style=\"$wk_style\">\n");
                $wk = "";
                //if ($nursing_assistance_flg == "") { $wk = "disabled"; } //無効化をとめる20150408
                $wk_name = "看護補助者表作成　(４週計算)";
                echo("<input $wk type=\"button\" value=\"$wk_name\" onclick=\"editSubNurse();\" style=\"$wk_style\">&nbsp; \n");
            } else {
                //-------------------------------------------------------------------------------
                //１ヵ月計算
                //-------------------------------------------------------------------------------
                echo("<input type=\"button\" value=\"看護職員表作成\" onclick=\"editNurse();\" style=\"$wk_style\">\n");
                $wk = "";
                //if ($nursing_assistance_flg == "") { $wk = "disabled"; } //無効化をとめる20150408
                echo("<input $wk type=\"button\" value=\"看護補助者表作成\" onclick=\"editSubNurse();\" style=\"$wk_style\">&nbsp; \n");
            }
            //-------------------------------------------------------------------------------
            //「再計算」「登録」
            //-------------------------------------------------------------------------------
            $wk_style = "width: 120px; height: 22px;";
            echo("&nbsp;&nbsp;");
            echo("<input type=\"button\" value=\"施設基準再読込\" onclick=\"dataReload();\" style=\"$wk_style\">&nbsp; \n");
            $wk_style = "width: 130px; height: 22px;";
            echo("<input type=\"button\" value=\"当月施設基準保存\" onclick=\"dataInsert();\" style=\"$wk_style\">&nbsp; \n");
            if ( $phpVersionName >= "5.1" ){
                //「除外リスト」 20140404
                $wk_name = "勤務時間除外リスト";
                $wk_style = "WIDTH: 130px; HEIGHT: 22px;";
                echo("<input type=\"button\" value=\"$wk_name\" onclick=\"document.xlsForm.action='duty_shift_report_exclusion_list.php';makeExcel();\" style=\"$wk_style\">\n");
            }
            echo("</td>\n");
        ?>
        </tr>
        </table>

        <!-- ------------------------------------------------------------------------ -->
        <!-- 入院基本料の施設基準等に係る届出書添付書類(様式９) -->
        <!-- ------------------------------------------------------------------------ -->
        <?
            $create_ymd = compact("create_yyyy", "create_mm", "create_dd");    //作成年月日(yyyymmdd)
            $prt_flg = "1";
            $wk_array = $obj_report->showData(
                $prt_flg,               //表示画面フラグ（１：看護職員表、２：看護補助職員表）
                $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                $data_array,            //看護師／准看護師情報
                $data_sub_array,        //看護補助者情報
                $hospital_name,         //保険医療機関名
                $ward_cnt,              //病棟数
                $sickbed_cnt,           //病床数
                $report_kbn,            //届出区分
                $report_kbn_name,       //届出区分名
                $report_patient_cnt,    //届出時入院患者数
                $hosp_patient_cnt,      //１日平均入院患者数
                $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                $nurse_sumtime,             //勤務時間数（看護師）
                $sub_nurse_sumtime,         //勤務時間数（准看護師）
                $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                $nurse_cnt,             //看護師数
                $sub_nurse_cnt,         //准看護師数
                $assistance_nurse_cnt,  //看護補助者数
                $hospitalization_cnt,   //平均在院日数
                $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                $ave_start_mm,          //平均在院日数 算出期間（開始月）
                $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                $ave_end_mm,            //平均在院日数 算出期間（終了月）
                $prov_start_hh,         //夜勤時間帯 （開始時）
                $prov_start_mm,         //夜勤時間帯 （開始分）
                $prov_end_hh,           //夜勤時間帯 （終了時）
                $prov_end_mm,           //夜勤時間帯 （終了分）
                $duty_yyyy,             //対象年
                $duty_mm,               //対象月
                $day_cnt,               //日数
                $date_y1,               //カレンダー用年
                $date_m1,               //カレンダー用月
                $date_d1,               //カレンダー用日
                $labor_cnt,             //常勤職員の週所定労働時間
                $create_ymd,            //作成年月日(yyyymmdd)
                $nurse_staffing_flg,    //看護配置加算の有無
                $acute_nursing_flg,     //急性期看護補助加算の有無
                $acute_nursing_cnt,     //急性期看護補助加算の配置比率
                $nursing_assistance_flg,    //看護補助加算の有無
                $nursing_assistance_cnt,    //看護補助加算の配置比率
                $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                $night_shift_add_flg,   //看護職員夜間配置加算の有無
                $specific_data,         //特定入院料関連データ
                $send_hm              //申し送り時間
            );
            echo($wk_array["data"]);

            //「看護師」「准看護師」
            $sum_night_b = $wk_array["sum_night_b"];    //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
            $sum_night_c = $wk_array["sum_night_c"];    //月延べ勤務時間数の計〔Ｃ〕
            $sum_night_d = $wk_array["sum_night_d"];    //月延べ夜勤時間数の計〔Ｄ〕
            $sum_night_e = $wk_array["sum_night_e"];    //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
            $sum_night_f = $wk_array["sum_night_f"];    //1日看護配置数〔(Ａ ／届出区分の数)×３〕
            $sum_night_g = $wk_array["sum_night_g"];    //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕
            $sum_night_h = $wk_array["sum_night_h"];    //月平均１日当たり夜間看護配置数〔D／(日数×１６）〕
            $sum_night_i = $wk_array["sum_night_i"];    //１日夜間看護配置数[A/12]※端数切上げ
            //「看護補助者」
            $sum_sub_night_b = $wk_array["sum_sub_night_b"];    //夜勤従事者数(夜勤ありの職員数)〔Ｂ〕
            $sum_sub_night_c = $wk_array["sum_sub_night_c"];    //月延べ勤務時間数の計〔Ｃ〕
            $sum_sub_night_d = $wk_array["sum_sub_night_d"];    //月延べ夜勤時間数の計〔Ｄ〕
            $sum_sub_night_e = $wk_array["sum_sub_night_e"];    //夜勤専従者及び月16時間以下の者の夜勤時間数〔Ｅ〕
            $sum_sub_night_f = $wk_array["sum_sub_night_f"];    //1日看護配置数〔(A ／届出区分の数)×３〕
            $sum_sub_night_g = $wk_array["sum_sub_night_g"];    //月平均１日あたり看護配置数〔Ｃ／(日数×８）〕

            if (isset($error_msg)) {    //エラーメッセージ表示
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>alert('$error_msg');</script>");
            }
        ?>
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
        <input type="hidden" name="session" value="<? echo($session); ?>">
        <input type="hidden" name="standard_id" value="<? echo($standard_id); ?>">
        <input type="hidden" name="duty_yyyy" value="<? echo($duty_yyyy); ?>">
        <input type="hidden" name="duty_mm" value="<? echo($duty_mm); ?>">
        <input type="hidden" name="recomputation_btn_flg" value="">
        <input type="hidden" name="reload_btn_flg" value="<? echo($reload_btn_flg); ?>">
    </form>

    <!-- ************************************************************************ -->
    <!-- 「看護職員表用」「看護補助者表用」 -->
    <!-- ************************************************************************ -->
    <!-- ------------------------------------------------------------------------ -->
    <!-- ＨＩＤＤＥＮ（看護職員表用） -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="nurseForm1" id="nurseForm1" method="post" action="duty_shift_report_nurse1.php" target="nurseFormChild1">
        <?
            $obj_report->showHidden(
                                    $session,               //セッション
                                    $standard_id,           //施設基準ＩＤ
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $night_start_day,       //４週計算用（開始日）
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    $create_ymd);           //作成年月日
        ?>
        <input type="hidden" name="sum_night_b" value="<? echo($sum_night_b); ?>">
        <input type="hidden" name="sum_night_c" value="<? echo($sum_night_c); ?>">
        <input type="hidden" name="sum_night_d" value="<? echo($sum_night_d); ?>">
        <input type="hidden" name="sum_night_e" value="<? echo($sum_night_e); ?>">
        <input type="hidden" name="sum_night_f" value="<? echo($sum_night_f); ?>">
        <input type="hidden" name="sum_night_g" value="<? echo($sum_night_g); ?>">
        <input type="hidden" name="sum_night_h" value="<? echo($sum_night_h); ?>">
        <input type="hidden" name="sum_night_i" value="<? echo($sum_night_i); ?>">
        <input type="hidden" name="plan_result_flag" value="">
    </form>
    <!-- ------------------------------------------------------------------------ -->
    <!-- ＨＩＤＤＥＮ（看護補助者表用） -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="nurseForm2" id="nurseForm2" method="post" action="duty_shift_report_nurse2.php" target="nurseFormChild2">
        <?
            $obj_report->showHidden(
                                    $session,               //セッション
                                    $standard_id,           //施設基準ＩＤ
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $night_start_day,       //４週計算用（開始日）
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    $create_ymd);           //作成日年月日
        ?>
        <input type="hidden" name="sum_sub_night_b" value="<? echo($sum_sub_night_b); ?>">
        <input type="hidden" name="sum_sub_night_c" value="<? echo($sum_sub_night_c); ?>">
        <input type="hidden" name="sum_sub_night_d" value="<? echo($sum_sub_night_d); ?>">
        <input type="hidden" name="sum_sub_night_e" value="<? echo($sum_sub_night_e); ?>">
        <input type="hidden" name="sum_sub_night_f" value="<? echo($sum_sub_night_f); ?>">
        <input type="hidden" name="sum_sub_night_g" value="<? echo($sum_sub_night_g); ?>">
        <input type="hidden" name="plan_result_flag" value="">
    </form>

    <!-- ------------------------------------------------------------------------ -->
    <!-- ＨＩＤＤＥＮ（集計表用） -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="xlsForm" id="xlsForm" method="post" action="duty_shift_report_nurse_excel.php">
        <?
            $obj_report->showHidden(
                                    $session,               //セッション
                                    $standard_id,           //施設基準ＩＤ
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $night_start_day,       //４週計算用（開始日）
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    $create_ymd);           //作成日年月日
        ?>
        <input type="hidden" name="sum_night_b" value="<? echo($sum_night_b); ?>">
        <input type="hidden" name="sum_night_c" value="<? echo($sum_night_c); ?>">
        <input type="hidden" name="sum_night_d" value="<? echo($sum_night_d); ?>">
        <input type="hidden" name="sum_night_e" value="<? echo($sum_night_e); ?>">
        <input type="hidden" name="sum_night_f" value="<? echo($sum_night_f); ?>">
        <input type="hidden" name="sum_night_g" value="<? echo($sum_night_g); ?>">
        <input type="hidden" name="sum_night_h" value="<? echo($sum_night_h); ?>">
        <input type="hidden" name="sum_night_i" value="<? echo($sum_night_i); ?>">
        <input type="hidden" name="sum_sub_night_b" value="<? echo($sum_sub_night_b); ?>">
        <input type="hidden" name="sum_sub_night_c" value="<? echo($sum_sub_night_c); ?>">
        <input type="hidden" name="sum_sub_night_d" value="<? echo($sum_sub_night_d); ?>">
        <input type="hidden" name="sum_sub_night_e" value="<? echo($sum_sub_night_e); ?>">
        <input type="hidden" name="sum_sub_night_f" value="<? echo($sum_sub_night_f); ?>">
        <input type="hidden" name="sum_sub_night_g" value="<? echo($sum_sub_night_g); ?>">

        <input type="hidden" name="nurse_staffing_flg" value="<? echo($nurse_staffing_flg); ?>">
        <input type="hidden" name="acute_nursing_flg" value="<? echo($acute_nursing_flg); ?>">
        <input type="hidden" name="acute_nursing_cnt" value="<? echo($acute_nursing_cnt); ?>">
        <input type="hidden" name="nursing_assistance_flg" value="<? echo($nursing_assistance_flg); ?>">
        <input type="hidden" name="nursing_assistance_cnt" value="<? echo($nursing_assistance_cnt); ?>">
        <input type="hidden" name="plan_result_flag" value=""><? /* タイトルに予定実績の文言追加 20120209 */ ?>
        <? /* 様式９平成２４年度改訂対応 20120406 */ ?>
        <input type="hidden" name="acute_assistance_flg" value="<? echo($acute_assistance_flg); ?>">
        <input type="hidden" name="acute_assistance_cnt" value="<? echo($acute_assistance_cnt); ?>">
        <input type="hidden" name="night_acute_assistance_flg" value="<? echo($night_acute_assistance_flg); ?>">
        <input type="hidden" name="night_acute_assistance_cnt" value="<? echo($night_acute_assistance_cnt); ?>">
        <input type="hidden" name="night_shift_add_flg" value="<? echo($night_shift_add_flg); ?>">
        <? /* 「特定入院料」、「特定入院料（看護職員＋看護補助者)」追加対応 20121011 */ ?>
        <input type="hidden" name="specific_type" value="<? echo($specific_data["specific_type"]); ?>">
        <input type="hidden" name="nurse_staffing_cnt" value="<? echo($specific_data["nurse_staffing_cnt"]); ?>">
        <input type="hidden" name="assistance_staffing_cnt" value="<? echo($specific_data["assistance_staffing_cnt"]); ?>">
        <? /* 入院基本料施設基準へ申し送り時間追加 20130418 */ ?>
        <input type="hidden" name="send_start_hhmm_1" value="<? echo($send_hm["send_start_hhmm_1"]); ?>">
        <input type="hidden" name="send_end_hhmm_1" value="<? echo($send_hm["send_end_hhmm_1"]); ?>">
        <input type="hidden" name="send_start_hhmm_2" value="<? echo($send_hm["send_start_hhmm_2"]); ?>">
        <input type="hidden" name="send_end_hhmm_2" value="<? echo($send_hm["send_end_hhmm_2"]); ?>">
        <input type="hidden" name="send_start_hhmm_3" value="<? echo($send_hm["send_start_hhmm_3"]); ?>">
        <input type="hidden" name="send_end_hhmm_3" value="<? echo($send_hm["send_end_hhmm_3"]); ?>">
    </form>

    <!-- ------------------------------------------------------------------------ -->
    <!-- ＨＩＤＤＥＮ（看護協会版届出様式） -->
    <!-- ------------------------------------------------------------------------ -->
    <form name="associateForm" id="associateForm" method="post" action="duty_shift_report_nurse_associate_excel.php">
        <?
            $obj_report->showHidden(
                                    $session,               //セッション
                                    $standard_id,           //施設基準ＩＤ
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $night_start_day,       //４週計算用（開始日）
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    $create_ymd             //作成年月日
                                    );
        ?>
        <input type="hidden" name="sum_night_b" value="<? echo($sum_night_b); ?>">
        <input type="hidden" name="sum_night_c" value="<? echo($sum_night_c); ?>">
        <input type="hidden" name="sum_night_d" value="<? echo($sum_night_d); ?>">
        <input type="hidden" name="sum_night_e" value="<? echo($sum_night_e); ?>">
        <input type="hidden" name="sum_night_f" value="<? echo($sum_night_f); ?>">
        <input type="hidden" name="sum_night_g" value="<? echo($sum_night_g); ?>">
        <input type="hidden" name="sum_night_h" value="<? echo($sum_night_h); ?>">
        <input type="hidden" name="sum_night_i" value="<? echo($sum_night_i); ?>">
        <input type="hidden" name="sum_sub_night_b" value="<? echo($sum_sub_night_b); ?>">
        <input type="hidden" name="sum_sub_night_c" value="<? echo($sum_sub_night_c); ?>">
        <input type="hidden" name="sum_sub_night_d" value="<? echo($sum_sub_night_d); ?>">
        <input type="hidden" name="sum_sub_night_e" value="<? echo($sum_sub_night_e); ?>">
        <input type="hidden" name="sum_sub_night_f" value="<? echo($sum_sub_night_f); ?>">
        <input type="hidden" name="sum_sub_night_g" value="<? echo($sum_sub_night_g); ?>">

        <? /* 様式９平成２４年度改訂対応 20120502 */ ?>
        <input type="hidden" name="nurse_staffing_flg" value="<? echo($nurse_staffing_flg); ?>">
        <input type="hidden" name="acute_nursing_flg" value="<? echo($acute_nursing_flg); ?>">
        <input type="hidden" name="acute_nursing_cnt" value="<? echo($acute_nursing_cnt); ?>">
        <input type="hidden" name="nursing_assistance_flg" value="<? echo($nursing_assistance_flg); ?>">
        <input type="hidden" name="nursing_assistance_cnt" value="<? echo($nursing_assistance_cnt); ?>">
        <input type="hidden" name="acute_assistance_cnt" value="<? echo($acute_assistance_cnt); ?>">
        <input type="hidden" name="night_acute_assistance_cnt" value="<? echo($night_acute_assistance_cnt); ?>">
        <input type="hidden" name="night_shift_add_flg" value="<? echo($night_shift_add_flg); ?>">
        <input type="hidden" name="recomputation_btn_flg" value="">
        <input type="hidden" name="plan_result_flag" value="<? echo($plan_result_flag); ?>">
        <input type="hidden" name="kango_space_line" value="$kango_space_line">
        <input type="hidden" name="hojo_space_line" value="$hojo_space_line">
        <input type="hidden" name="childWinWait" value="$childWinWait">

        <? /* 「特定入院料」、「特定入院料（看護職員＋看護補助者)」追加対応 20121011 */ ?>
        <input type="hidden" name="specific_type" value="<? echo($specific_data["specific_type"]); ?>">
        <input type="hidden" name="nurse_staffing_cnt" value="<? echo($specific_data["nurse_staffing_cnt"]); ?>">
        <input type="hidden" name="assistance_staffing_cnt" value="<? echo($specific_data["assistance_staffing_cnt"]); ?>">
    </form>
    <? } ?>

</body>
<? pg_close($con); ?>
</html>