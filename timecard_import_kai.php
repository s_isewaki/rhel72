<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | インポート（快決シフト）</title>
<?
//ini_set("display_errors","1");
ini_set("max_execution_time", 0);
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("timecard_import_common.ini");
require_once("duty_shift_common_class.php");
require_once("timecard_import_menu_common.php");
require_once("atdbk_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//削除
if ($mode == "delete") {
	deleteImportSetDataKai($con, $fname, $no);
}

$cnvdata = get_cnvdata_kai($con, $fname, "");
$arr_cnvtbl = array(); //key[{$absence_reason}_{$duty_ptn}]["tmcd_group_id" or "atdptn_ptn_id" or "reason"]

for ($i=0; $i<count($cnvdata); $i++) {
	$wk_key = $cnvdata[$i]["absence_reason"]."_".$cnvdata[$i]["duty_ptn"];
	$arr_cnvtbl[$wk_key]["tmcd_group_id"] = $cnvdata[$i]["tmcd_group_id"];
	$arr_cnvtbl[$wk_key]["atdptn_ptn_id"] = $cnvdata[$i]["atdptn_ptn_id"];
	$arr_cnvtbl[$wk_key]["reason"] = $cnvdata[$i]["reason"];
}

//インポート
if ($mode == "import") {
	// ファイルが正常にアップロードされたかどうかチェック
	$uploaded = false;
	if (array_key_exists("csvfile", $_FILES)) {
		if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
			$uploaded = true;
		}
	}
	if (!$uploaded) {
		echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'timecard_import.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	// 文字コードの設定
	switch ($encoding) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
	}

	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character(0x30FB);
	// トランザクションを開始
	pg_query($con, "begin");

	// CSVデータを配列に格納
	$shifts = array();
	$no = 1;
	// 職員ID
	$emp_personal_ids = array();

	$lines = file($_FILES["csvfile"]["tmp_name"]);
	foreach ($lines as $line) {

		// 文字コードを変換
		$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		// 空行は無視
		if ($line == "") {
			continue;
		}

		// カンマで分割し配列へ設定
		$shift_data = split(",", $line);

		$shifts[$no] = $shift_data;
		$wk_id = $shift_data[1];
		$emp_personal_ids[$wk_id] = $wk_id;

		$no++;
	}

	//シフトグループ
	$arr_emp_group_id = get_emp_group_id($con, $fname, $emp_personal_ids);

	//処理件数
	$reg_cnt = 0;
	$err_cnt = 0;

	//グループ別期間情報。勤務シフト情報（スタッフ情報）登録用
	$group_term = array();
	//メッセージ
	$errmsgs = array();
	$errmsgs[] = "インポート処理開始<br>";
	foreach ($shifts as $no => $shift_data) {

		$rtn_info = update_shift_data($con, $fname, $session, $shift_data, $arr_cnvtbl, $arr_emp_group_id);

		if ($rtn_info["rtncd"] == 0) {
			$reg_cnt++;
			if ($rtn_info["group_id"] != "") {
				$wk_group_id = $rtn_info["group_id"];
				$group_term["$wk_group_id"]["id"] = $wk_group_id;
				if ($group_term["$wk_group_id"]["start_date"] == "" ||
					$group_term["$wk_group_id"]["start_date"] > $shift_data[2]) {
					$group_term["$wk_group_id"]["start_date"] = $shift_data[2];
				}
				if ($group_term["$wk_group_id"]["end_date"] == "" ||
					$group_term["$wk_group_id"]["end_date"] < $shift_data[2]) {
					$group_term["$wk_group_id"]["end_date"] = $shift_data[2];
				}
			}
		} else {
			$arr_msg = $rtn_info["errmsg"];
			for ($i=0; $i<count($arr_msg); $i++) {
				$errmsgs[] = "{$no}行目：".$arr_msg[$i]."<br>\n";
			}
			$err_cnt++;
		}
	}

	if ($reg_cnt > 0 ) {
		$data_wktmgrp = $obj->get_wktmgrp_array();
		// 勤務シフトグループ情報を取得
		$group_array = $obj->get_duty_shift_group_array("", "", $data_wktmgrp);
		//期間変更の範囲を考慮した年月を取得する
		$term_yyyymm = get_term_yyyymm($con, $fname, $obj, $group_array, $group_term);
		// 勤務シフト情報（スタッフ情報）を登録
		insert_duty_shift_plan_staff($con, $fname, $term_yyyymm);

		//下書きを削除
		delete_draft($con, $fname, $term_yyyymm);
	}

	// トランザクションをコミット
	pg_query($con, "commit");

	$errmsgs[] = "インポート処理終了<br>";
	$errmsgs[] = "正常登録：{$reg_cnt}件<br>";
	$errmsgs[] = "エラー　：{$err_cnt}件<br>";
}

if ($encoding == "") {
	$encoding = "1";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var childwin = null;
function openUpdateWin(ins_upd_flg, no) {
	dx = screen.width;
	dy = screen.top;
	wx = 860;
	wy = 240;
	base = screen.height / 2 - (wy / 2);
	var url = './timecard_import_set_kai.php';
	url += '?session=<?=$session?>';
	url += '&ins_upd_flg='+ins_upd_flg;
	url += '&no='+no;
	childwin = window.open(url, 'timecardimpopup', 'left='+((dx-wx)/2)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeUpdateWin() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function deleteData(no) {

	if (confirm('削除してよろしいですか？')) {

		document.mainform.mode.value = "delete";
		document.mainform.no.value = no;
		document.mainform.action = "timecard_import_kai.php";
		document.mainform.submit();

	}
}

function reload_list() {
	document.mainform.mode.value = "";
	document.mainform.action = "timecard_import_kai.php";
	document.mainform.submit();
}

function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	document.mainform.mode.value = "import";
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//インポートメニュータブ表示
show_timecard_import_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form id="mainform" name="mainform" action="timecard_import_kai.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">快決シフトくんからの勤務予定インポート</font>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<?
if ($mode == "import") {
?>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録結果メッセージ<br>
<?
	foreach ($errmsgs as $errmsg) {
		echo($errmsg);
	}
}
?>
</font>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="document.getElementById('csv_fmt').style.display = (document.getElementById('csv_fmt').style.display == 'none') ? '' : 'none';"><b>CSVのフォーマット</b></a></font>
<div id="csv_fmt" style="display:none">
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></dt>
<dd><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記項目をカンマで区切り、レコードごとに改行してください。</font></dd>
<dd>
<ul>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付（勤務の日付、YYYYMMDD形式）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員コード（0埋めで8桁固定）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不在理由（0埋めで2桁固定）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務区分（0埋めで3桁固定）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未使用（ブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未使用（ブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未使用（ブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未使用（ブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未使用（ブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始時刻（出勤時刻、「hhmm」形式。シフト君→CoMedixは読込み時に無視。CoMedix→シフト君はブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了時刻（退勤時刻、「hhmm」形式。シフト君→CoMedixは読込み時に無視。CoMedix→シフト君はブランク）</font></li>
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属コード（シフト君→CoMedixは出勤予定のデータへ保存。CoMedix→シフト君は出勤予定から出力、ない場合は2階層目の所属部署の外部連携キー）</font></li>
</ul>
<dl>
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">例</font></dt>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">20070101,00123456,01,002,,,,,,0900,1830,000201</font></dd>
<dd><font size="3" face="ＭＳ ゴシック, Osaka" class="j12">20070101,00123457,02,003,,,,,,1300,1930,000201</font></dd>
</dl>
</dd>
</dl>
</div>
<br>
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務シフト種類変換設定</font>

<table width="860" border="0" cellspacing="0" cellpadding="2" class="list">

<tr>
<td width="150" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>不在理由</b></font></td>
<td width="150" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務区分</b></font></td>
<td width="40" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>&nbsp;</b></font></td>
<td width="160" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン<br>グループ</b></font></td>
<td width="90" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務パターン</b></font></td>
<td width="90" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>事由</b></font></td>
<td width="180" bgcolor="#f6f9ff" align="right"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font><input type="button" value="追加" onclick="openUpdateWin('1', 0);"></td>
</tr>
<?
// 表示処理
for ($i=0; $i<count($cnvdata); $i++) {
?>
<tr>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["absence_reason"]); ?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["duty_ptn"]); ?></font></td>
<td align="center"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">→</font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["tmcd_group_name"]); ?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
if ($cnvdata[$i]["atdptn_nm"] != "") {
	echo($cnvdata[$i]["atdptn_nm"]);
} else {
	echo("未設定");
}
?></font></td>
<td><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cnvdata[$i]["reason_name"]); ?></font></td>
<td align="right"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font><input type="button" value="削除" onclick="deleteData(<?echo($i+1);?>);">
<input type="button" value="変更" onclick="openUpdateWin('2', <?echo($i+1);?>);">
<input type="button" value="追加" onclick="openUpdateWin('1', <?echo($i+1);?>);"></td>
</tr>

<?
}
?>

</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="no" value="">

</form>
</td>
</tr>
</table>


</body>
<? pg_close($con); ?>
</html>

<?
//データを登録
function update_shift_data($con, $fname, $session, $shift_data, $arr_cnvtbl, $arr_emp_group_id)
{

	$arr_msg = array();
	$rtn_info = array();
	$rtn_info["rtncd"] = 0;

	//入力データチェック
	//カラム数チェック
	$column_count = 12;
	if (count($shift_data) != $column_count) {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "カラム数が不正です。".join(",", $shift_data);
		$rtn_info["errmsg"] = $arr_msg;
		return $rtn_info;
	}

	//予約データ確認
	//職員ID確認
	$emp_personal_id = $shift_data[1];
	$emp_id = $arr_emp_group_id["$emp_personal_id"]["emp_id"];
	$group_id = $arr_emp_group_id["$emp_personal_id"]["group_id"];
	if ($emp_id == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "職員($emp_personal_id,$shift_data[1])がマスタに登録されていません。";
	}
	//日付 範囲、妥当性
	if ($shift_data[0] == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "日付が設定されていません。";
	} else {
		$wk_yyyy = substr($shift_data[0], 0, 4);
		$wk_mm = substr($shift_data[0], 4, 2);
		$wk_dd = substr($shift_data[0], 6, 2);
		$date = $wk_yyyy.$wk_mm.$wk_dd;
		if (!checkdate($wk_mm, $wk_dd, $wk_yyyy)) {
			$rtn_info["rtncd"] = 1;
			$arr_msg[] = "日付($shift_data[2])が正しくありません。";
		}
	}

	//変換設定確認
	$wk_key = $shift_data[2]."_".$shift_data[3];
	$tmcd_group_id = $arr_cnvtbl[$wk_key]["tmcd_group_id"];
	$pattern = $arr_cnvtbl[$wk_key]["atdptn_ptn_id"];
	$reason = $arr_cnvtbl[$wk_key]["reason"];

	$atrb_link_key = $shift_data[11];
	if (strlen($atrb_link_key) > 6) {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "所属コードが長すぎます。（6文字以内）";
	}
	//変換設定に存在しない
	if ($tmcd_group_id == "") {
		$rtn_info["rtncd"] = 1;
		$arr_msg[] = "勤務シフト種類変換設定($shift_data[2],$shift_data[3])が登録されていません。";
	}

	//エラー時
	if ($rtn_info["rtncd"] == 1) {
		$rtn_info["errmsg"] = $arr_msg;
		return $rtn_info;
	}

	//予約データ確認
	$sql = "select pattern, reason, tmcd_group_id, atrb_link_key from atdbk ";
	$cond = "where emp_id = '$emp_id' and date = '$date' ";

	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num == 0) {
	//追加
		$sql = "insert into atdbk (emp_id, date, pattern, reason, tmcd_group_id, atrb_link_key ";
		$sql .= ") values (";
		$content = array($emp_id, $date, $pattern, $reason, $tmcd_group_id, $atrb_link_key);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		//内容が変更されている時更新
		$old_pattern = pg_fetch_result($sel, 0, "pattern");
		$old_reason = pg_fetch_result($sel, 0, "reason");
		$old_tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
		$old_atrb_link_key = pg_fetch_result($sel, 0, "atrb_link_key");
		if ($pattern != $old_pattern ||
				$reason != $old_reason ||
				$tmcd_group_id != $old_tmcd_group_id ||
				$atrb_link_key != $old_atrb_link_key) {
			$sql = "update atdbk set ";
			$set = array("pattern", "reason", "tmcd_group_id", "atrb_link_key");
			$setvalue = array($pattern, $reason, $tmcd_group_id, $atrb_link_key);
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	if ($group_id != "") {
		$rtn_info["group_id"] = $group_id;
	}
	return $rtn_info;
}

//職員別シフトグループ取得
function get_emp_group_id($con, $fname, $emp_personal_ids) {

	$arr_gid = array();

	if (count($emp_personal_ids) == 0) {
		return $arr_gid;
	}

	$cond_add = "";
	foreach ($emp_personal_ids as $emp_personal_id => $dummy) {
		if ($cond_add != "") {
			$cond_add .= " or ";
		}
		//$cond_add .= " a.emp_personal_id = '$emp_personal_id' ";
		//職員IDを数値の長さ分比較する
		$len_key = strlen($emp_personal_id);
		$cond_add .= " a.emp_personal_id ";
		$cond_add .= "  = substr('$emp_personal_id', $len_key - length(a.emp_personal_id) + 1, $len_key) ";
	}
	$sql = "select a.emp_id, a.emp_personal_id, b.group_id from empmst a left join duty_shift_staff b on b.emp_id = a.emp_id ";
	$cond = "where $cond_add ";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$data = pg_fetch_all($sel);
	for ($i=0; $i<count($data); $i++) {
		$wk_emp_id = sprintf("%08s", $data[$i]["emp_personal_id"]); //0詰め8桁
		$arr_gid["$wk_emp_id"]["emp_id"] = $data[$i]["emp_id"];
		$arr_gid["$wk_emp_id"]["group_id"] = $data[$i]["group_id"];
	}

	return $arr_gid;
}

//期間変更の範囲を考慮した年月を取得する
//グループ毎にstart_yyyymm、end_yyyymmを返す
// min_date max_date下書きを削除する範囲
function get_term_yyyymm($con, $fname, $obj, $group_array, $group_term) {

	$term_yyyymm = array();

	foreach ($group_term as $wk_group_id => $wk_info) {

		$term_yyyymm["$wk_group_id"]["id"] = $wk_group_id;

		//期間変更情報
		for ($i=0; $i<count($group_array); $i++) {

			if ($wk_group_id == $group_array[$i]["group_id"]) {
				$start_month_flg1 = $group_array[$i]["start_month_flg1"];
				$start_day1 = $group_array[$i]["start_day1"];
				$month_flg1 = $group_array[$i]["month_flg1"];
				$end_day1 = $group_array[$i]["end_day1"];
				if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
				if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
				if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
				if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
			}
		}
		//開始月確認
		list ($start_yyyy, $start_mm, $start_dd) = split("/", $wk_info["start_date"]);
		//前後確認
		$tmp_date = $start_yyyy.$start_mm."01";
		$tmp_date = strtotime($tmp_date);
		$tmp_date = date("Ymd", strtotime("-1 month", $tmp_date));
		for ($i=0; $i<3; $i++) {
			$wk_yyyy = substr($tmp_date, 0, 4);
			$wk_mm = substr($tmp_date, 4, 2);

			//各月の範囲
			$arr_date = $obj->get_term_date($wk_yyyy, $wk_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

			if ($arr_date[0] <= $start_yyyy.$start_mm.$start_dd &&
				$arr_date[1] >= $start_yyyy.$start_mm.$start_dd) {

				break;
			}
			$tmp_date = strtotime($tmp_date);
			$tmp_date = date("Ymd", strtotime("+1 month", $tmp_date));
		}

		//開始月
		$term_yyyymm["$wk_group_id"]["start_yyyymm"] = $wk_yyyy.$wk_mm;
		$term_yyyymm["$wk_group_id"]["min_date"] = $arr_date[0];

		//終了月確認
		list ($end_yyyy, $end_mm, $end_dd) = split("/", $wk_info["end_date"]);
		//前後確認
		$tmp_date = $end_yyyy.$end_mm."01";
		$tmp_date = strtotime($tmp_date);
		$tmp_date = date("Ymd", strtotime("-1 month", $tmp_date));
		for ($i=0; $i<3; $i++) {
			$wk_yyyy = substr($tmp_date, 0, 4);
			$wk_mm = substr($tmp_date, 4, 2);
			//各月の範囲
			$arr_date = $obj->get_term_date($wk_yyyy, $wk_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
			if ($arr_date[0] <= $end_yyyy.$end_mm.$end_dd &&
				$arr_date[1] >= $end_yyyy.$end_mm.$end_dd) {

				break;
			}
			$tmp_date = strtotime($tmp_date);
			$tmp_date = date("Ymd", strtotime("+1 month", $tmp_date));
		}

		//終了月
		$term_yyyymm["$wk_group_id"]["end_yyyymm"] = $wk_yyyy.$wk_mm;
		$term_yyyymm["$wk_group_id"]["max_date"] = $arr_date[1];

	}

	return $term_yyyymm;
}

// 勤務シフト情報（スタッフ情報）を登録
function insert_duty_shift_plan_staff($con, $fname, $term_yyyymm) {

	foreach ($term_yyyymm as $wk_group_id => $wk_info) {

		$tmp_date = $wk_info["start_yyyymm"]."01";

		$end_date = $wk_info["end_yyyymm"]."01";
		while ($tmp_date <= $end_date) {

		// 勤務シフト情報（スタッフ情報）がなければ勤務シフトスタッフ情報から登録
			$wk_yyyy = substr($tmp_date, 0, 4);
			$wk_mm = intval(substr($tmp_date, 4, 2));

			$sql = "select count(*) as cnt from duty_shift_plan_staff ";
			$cond = "where group_id = '$wk_group_id' and duty_yyyy = $wk_yyyy and duty_mm = $wk_mm ";
			$sel = select_from_table($con,$sql,$cond,$fname);
			if($sel == 0){
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$cnt = pg_fetch_result($sel, 0, "cnt");
			if ($cnt == 0) {
				$sql = "insert into duty_shift_plan_staff (select a.group_id, a.emp_id, $wk_yyyy, $wk_mm, a.no, b.emp_st  from duty_shift_staff a left join empmst b on b.emp_id = a.emp_id where a.group_id = '$wk_group_id' ";

				$result = insert_into_table($con, $sql, "", $fname);
				if ($result == 0) {
					pg_query($con,"rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}

			$tmp_date = strtotime($tmp_date);
			$tmp_date = date("Ymd", strtotime("+1 month", $tmp_date));

		}

	}

}

//下書きを削除
function delete_draft($con, $fname, $term_yyyymm) {

	//グループID分繰り返す
	foreach ($term_yyyymm as $wk_group_id => $wk_info) {

		$wk_start_date = $wk_info["min_date"];
		$wk_end_date = $wk_info["max_date"];

		$sql = "delete from duty_shift_plan_draft ";
		$cond = "where group_id = '$wk_group_id' and duty_date >= '$wk_start_date' and duty_date <= '$wk_end_date' ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

}

?>
