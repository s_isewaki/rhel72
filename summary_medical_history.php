<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID

*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require("show_kanja_joho.ini");
require("label_by_profile_type.ini");
require_once("summary_common.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);


//==============================
//病歴情報を取得する。
//==============================

//現在歴、家族歴
$sql = "select * from medical_history";
$cond = "where medical_history.pt_id='$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0){
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$now_history = "";
$family_history = "";
$num = pg_numrows($sel);
if ($num <= 1){
	$now_history = @pg_result($sel,0,"now_history");
	$family_history = @pg_result($sel,0,"family_history");
}
$now_history_html = h($now_history);
$family_history_html   = h($family_history);

//既往歴
$sql = "select * from medical_history_past";
$cond = "where medical_history_past.pt_id='$pt_id' order by number";
$sel_past = select_from_table($con, $sql, $cond, $fname);
if ($sel_past == 0){
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$num_past = pg_numrows($sel_past);

//==============================
//既往歴の表示件数を決定
//==============================
$past_disp_num = $num_past;
if($past_disp_num < 5) $past_disp_num = 5;

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];
//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜病歴</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">

//既往歴の行を追加します。
function addTableRow() {
	//行インデックスの取得と更新
	var index_num = document.touroku.past_num.value - 0;
	index_num = index_num;
	document.touroku.past_num.value = index_num + 1;

	//テーブル作成
	var table = document.getElementById('past_table');
	var row = table.insertRow(table.rows.length);//最終行に追加

	var cell1 = row.insertCell(row.cells.length);
	cell1.align = "center";
	cell1.innerHTML = '<input type="text" value="" id="past_about_date_' + index_num + '" name="past_about_date_' + index_num + '" style="width:120px; ime-mode:active" maxlength="20">';

	var cell2 = row.insertCell(row.cells.length);
	cell2.align = "center";
	cell2.innerHTML = '<input type="text" value="" id="past_contents_' + index_num + '" name="past_contents_' + index_num + '" style="width:560px; ime-mode:active" maxlength="1000">';

}

</script>
</head>
<body>

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "病歴", "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, "病歴", 0, $pt_id, $pager_param_addition); ?>


<? //患者情報 出力 ?>
<? show_kanja_joho($con,$pt_id,$fname); ?>



<? //フォーム ?>
<div style="width:700px; margin-top:12px; background-color:#f4f4f4">
<form name="touroku" action="summary_medical_history_exe.php" method="post">
<input type ="submit" value="dummy" style="display:none"/>


<div class="search_button_div" style="border-bottom:1px solid #fdfdfd">
	<input type="button" onclick="document.touroku.submit();" value="登録"/>
</div>

<div style="border-top:1px solid #d8d8d8; padding-top:10px; padding-bottom:2px">
	<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; letter-spacing:1px; width:80px">現在歴</div>
</div>

<textarea id="now_history" name="now_history" style="width:690px; height:100px; ime-mode:active"><?=$now_history_html?></textarea>


<div style="padding-top:10px; padding-bottom:2px">
	<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin:2px 0px; letter-spacing:1px; width:80px">既往歴</div>
</div>

<table class="prop_table" style="width:100%" id="past_table" cellspacing="1">
	<tr>
		<th style="width:120px">いつごろ</th>
		<th>内容</th>
	</tr>

	<? for($i=0;$i<$past_disp_num;$i++) { ?>
	<? $about_date = ($i < $num_past) ? h(pg_result($sel_past,$i,"about_date")) : ""; ?>
	<? $contents   = ($i < $num_past) ? h(pg_result($sel_past,$i,"contents"))   : ""; ?>
	<tr>
		<td><input type="text" value="<?=$about_date?>" id="past_about_date_<?=$i?>" name="past_about_date_<?=$i?>" style="width:120px; ime-mode:active" maxlength="20"></td>
		<td><input type="text" value="<?=$contents?>" id="past_contents_<?=$i?>" name="past_contents_<?=$i?>" style="width:560px; ime-mode:active" maxlength="1000"></td>
	</tr>
	<? } ?>
</table>


<div class="search_button_div">
	<input type="button" onclick="addTableRow();" value="追加"/>
</div>


<div style="padding-top:10px; padding-bottom:2px">
	<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin:2px 0px; letter-spacing:1px; width:80px">家族歴</div>
</div>

<textarea id="family_history" name="family_history" style="width:690px; height:100px; ime-mode:active"><?=$family_history_html?></textarea>


<div class="search_button_div" style="border-top:1px solid #d8d8d8; margin-top:5px">
	<input type="button" onclick="document.touroku.submit();" value="登録"/>
</div>

<input type="hidden" id="past_num" name="past_num" value="<?=$past_disp_num?>">
<input type="hidden" id="session" name="session" value="<?=$session?>">
<input type="hidden" id="pt_id" name="pt_id" value="<?=$pt_id?>">
</form>
</div>


</body>
</html>
<?

pg_close($con);
?>
