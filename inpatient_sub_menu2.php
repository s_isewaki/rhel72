<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜登録内容選択</title>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

define("ROW_COUNT", 5);

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 患者登録権限を取得
$pt_reg_auth = check_authority($session, 22, $fname);

// 病棟名を取得
$sql = "select ward_name from wdmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ward_name = pg_fetch_result($sel, 0, "ward_name");

// 病室名を取得
$sql = "select ptrm_name from ptrmmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ptrm_name = pg_fetch_result($sel, 0, "ptrm_name");

// 選択病床への入院情報を取得（入院日時が過ぎていないもの）
$now = date("YmdHis");
$sql = "select ptif_id, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_in_dt, inpt_in_tm from inptmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no and inpt_bed_no = '$bed_no' and ptif_id is not null and ptif_id != '' and inpt_in_dt || inpt_in_tm > '$now'";
$sel_in = select_from_table($con, $sql, $cond, $fname);
if ($sel_in == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 選択病床への入院予定を取得
$sql = "select ptif_id, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_in_res_dt, inpt_in_res_tm from inptres";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd and ptrm_room_no = $room_no and inpt_bed_no = '$bed_no' and inpt_in_res_dt_flg = 't'";
$sel_in_res = select_from_table($con, $sql, $cond, $fname);
if ($sel_in_res == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 選択病床への移転予定を取得
$sql = "select inptmove.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, inptmove.move_dt, inptmove.move_tm from inptmove inner join ptifmst on inptmove.ptif_id = ptifmst.ptif_id";
$cond = "where inptmove.to_bldg_cd = $bldg_cd and inptmove.to_ward_cd = $ward_cd and inptmove.to_ptrm_room_no = $room_no and inptmove.to_bed_no = '$bed_no' and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f'";
$sel_mv = select_from_table($con, $sql, $cond, $fname);
if ($sel_mv == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 予定情報をマージして日時でソート
$reserve_list = array();
while ($row = pg_fetch_array($sel_in)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
	$reserve_info["type"] = "入院";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["inpt_in_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["inpt_in_tm"]);
	$reserve_info["button"] = "済み";
	$reserve_info["js"] = "";
	$reserve_list[] = $reserve_info;
}
while ($row = pg_fetch_array($sel_in_res)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"];
	$reserve_info["type"] = "入院予定";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["inpt_in_res_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["inpt_in_res_tm"]);
	$reserve_info["button"] = "入院";
	$reserve_info["js"] = "registerIn";
	$reserve_list[] = $reserve_info;
}
while ($row = pg_fetch_array($sel_mv)) {
	$reserve_info = array();
	$reserve_info["ptif_id"] = $row["ptif_id"];
	$reserve_info["pt_nm"] = $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"];
	$reserve_info["type"] = "転床予定";
	$reserve_info["datetime"] = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $row["move_dt"]) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $row["move_tm"]);
	$reserve_info["button"] = "転床";
	$reserve_info["js"] = "registerMove";
	$reserve_list[] = $reserve_info;
}
usort($reserve_list, "sort_reserve_list");

function sort_reserve_list($reserve_info1, $reserve_info2) {
	return strcmp($reserve_info1["datetime"], $reserve_info2["datetime"]);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerIn(ptif_id) {
	opener.location.href = 'inpatient_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&ward=<? echo("{$bldg_cd}-{$ward_cd}"); ?>&ptrm=<? echo($room_no); ?>&bed=<? echo($bed_no); ?>&bedundec1=f&bedundec2=f&bedundec3=f&path=3';
	window.close();
}

function reserveIn(ptif_id) {
	opener.location.href = 'inpatient_reserve_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&ward=<? echo("{$bldg_cd}-{$ward_cd}"); ?>&ptrm=<? echo($room_no); ?>&bed=<? echo($bed_no); ?>&bedundec1=f&bedundec2=f&bedundec3=f&path=1';
	window.close();
}

function registerMove(ptif_id) {
	location.href = 'inpatient_move_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録内容選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病棟</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($ward_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病室</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($ptrm_name); ?></font></td>
<td width="" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">ベットNo</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($bed_no); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? if (count($reserve_list) > 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">種別</font></td>
<td width="40%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">予定日時</font></td>
<td width="10%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録</font></td>
</tr>
<? foreach ($reserve_list as $reserve_info) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["pt_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["type"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($reserve_info["datetime"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<? if ($reserve_info["button"] == "済み") { ?>
済み
<? } else { ?>
<input type="button" value="<? echo($reserve_info["button"]); ?>" onclick="<? echo($reserve_info["js"]); ?>('<? echo($reserve_info["ptif_id"]); ?>');">
<? } ?>
</font></td>
</tr>
<? } ?>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="160" align="center" bgcolor="#5279a5"><a href="inpatient_sub_menu2.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18" color="#ffffff"><b>入院予定・入院</b></font></a></td>
<? if ($pt_reg_auth == "1") { ?>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_patient.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">新規登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_sub_menu2_temp.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&room_no=<? echo($room_no); ?>&bed_no=<? echo($bed_no); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">仮抑え</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form action="inpatient_sub_menu2.php" method="get">
<table width="600" border="0" cellspacing="0" cellpadding="4">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
患者ID <input type="text" name="key1" value="<? echo($key1); ?>" style="ime-mode:inactive">
<img src="img/spacer.gif" alt="" width="5" height="1">
患者氏名 <input type="text" name="key2" value="<? echo($key2); ?>" style="ime-mode:active">
<input type="submit" value="検索">
</font></td>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="room_no" value="<? echo($room_no); ?>">
<input type="hidden" name="bed_no" value="<? echo($bed_no); ?>">
<input type="hidden" name="mode" value="search">
</form>
<?
if ($mode == "search") {
	show_patient_list($con, $key1, $key2, $page, $fname);
	show_navigation($con, $key1, $key2, $page, $bldg_cd, $ward_cd, $room_no, $bed_no, $session, $fname);
}
?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_patient_list($con, $key1, $key2, $page, $fname) {
	$key1 = trim($key1);
	$key2 = trim($key2);
	if ($key1 == "" && $key2 == "") {
		return;
	}

	// 検索条件に該当する入院未登録患者一覧を取得
	$sql = "select ptifmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, inptres.reserve_count from ptifmst left join (select ptif_id, count(*) as reserve_count from inptres group by ptif_id) inptres on inptres.ptif_id = ptifmst.ptif_id";
	$cond = "where ptifmst.ptif_del_flg = 'f' and not exists (select * from inptmst where inptmst.ptif_id = ptifmst.ptif_id) and (ptifmst.ptif_id = '$key1'";
	if ($key2 != "") {
		$full_name = str_replace(" ", "", $key2);
		$full_name = str_replace("　", "", $full_name);
		$cond .= " or ptifmst.ptif_ft_kaj_nm like '%$key2%' or ptifmst.ptif_lt_kaj_nm like '%$key2%' or ptifmst.ptif_ft_kana_nm like '%$key2%' or ptifmst.ptif_lt_kana_nm like '%$key2%' or (ptifmst.ptif_lt_kaj_nm || ptifmst.ptif_ft_kaj_nm) like '%$full_name%' or (ptifmst.ptif_lt_kana_nm || ptifmst.ptif_ft_kana_nm) like '%$full_name%'";
	}
	$cond .= ") order by ptifmst.ptif_id";
	$cond .= " limit " . ROW_COUNT . " offset " . ($page * ROW_COUNT);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" class=\"list\">\n");

	// ヘッダ行を出力
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"20%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">患者ID</font></td>\n");
	echo("<td width=\"30%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">患者氏名</font></td>\n");
	echo("<td width=\"25%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">予定登録状況</font></td>\n");
	echo("<td width=\"25%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">登録</font></td>\n");
	echo("</tr>\n");

	// 明細行を出力
	while ($row = pg_fetch_array($sel)) {
		$status = ($row["reserve_count"] > 0) ? "登録済み" : "未登録";

		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">{$row["ptif_id"]}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">{$row["ptif_lt_kaj_nm"]} {$row["ptif_ft_kaj_nm"]}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">$status</font></td>\n");
		echo("<td align=\"center\">\n");
		echo("<input type=\"button\" value=\"入院予定\" onclick=\"reserveIn('{$row["ptif_id"]}');\"");
		if ($row["reserve_count"] > 0) {
			echo(" disabled");
		}
		echo(">\n");
		echo("<input type=\"button\" value=\"入院\" onclick=\"registerIn('{$row["ptif_id"]}');\">\n");
		echo("</td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");
}

function show_navigation($con, $key1, $key2, $page, $bldg_cd, $ward_cd, $room_no, $bed_no, $session, $fname) {
	$key1 = trim($key1);
	$key2 = trim($key2);
	if ($key1 == "" && $key2 == "") {
		return;
	}

	// 検索条件に該当する入院未登録患者数を取得
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_del_flg = 'f' and not exists (select * from inptmst where inptmst.ptif_id = ptifmst.ptif_id) and (ptif_id = '$key1'";
	if ($key2 != "") {
		$full_name = str_replace(" ", "", $key2);
		$full_name = str_replace("　", "", $full_name);
		$cond .= " or ptif_ft_kaj_nm like '%$key2%' or ptif_lt_kaj_nm like '%$key2%' or ptif_ft_kana_nm like '%$key2%' or ptif_lt_kana_nm like '%$key2%' or (ptif_lt_kaj_nm || ptif_ft_kaj_nm) like '%$full_name%' or (ptif_lt_kana_nm || ptif_ft_kana_nm) like '%$full_name%'";
	}
	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$patient_count = intval(pg_fetch_result($sel, 0, 0));

	// 2ページ以上ない場合は何もせず復帰
	$total_page = ceil($patient_count / ROW_COUNT);
	if ($total_page < 2) {
		return;
	}

	// ページ切り替えリンクを出力
	$key1 = urlencode($key1);
	$key2 = urlencode($key2);
	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
	echo("<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">\n");
	echo("<tr>\n");
	echo("<td width=\"25%\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">\n");
	if ($page > 0) {
		echo("<a href=\"$fname?session=$session&mode=search&key1=$key1&key2=$key2&page=" . ($page - 1) . "&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no\">←</a>\n");
	}
	echo("</font></td>\n");
	echo("<td width=\"50%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">\n");
	for ($i = 0; $i < $total_page; $i++) {
		if ($page != $i) {
			echo("<a href=\"$fname?session=$session&mode=search&key1=$key1&key2=$key2&page=$i&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no\">" . ($i + 1) . "</a>\n");
		} else {
			echo("[" . ($i + 1) . "]\n");
		}
	}
	echo("</font></td>\n");

	echo("<td width=\"25%\" align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">\n");
	if ($page < ($total_page - 1)) {
		echo("<a href=\"$fname?session=$session&mode=search&key1=$key1&key2=$key2&page=" . ($page + 1) . "&bldg_cd=$bldg_cd&ward_cd=$ward_cd&room_no=$room_no&bed_no=$bed_no\">→</a>\n");
	}
	echo("</font></td>\n");
	echo("<tr>\n");
	echo("</table>\n");
}
?>
