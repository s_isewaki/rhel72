<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
include_once("hiyari_excel_workshop.php");
require_once("about_postgres.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

//DBコネクション取得
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ファイル名
$filename = "hiyari_rm_stats_pattern_excel.xls";
$session = $_POST["session"];
// アクセスログ（ファイル名を「内容」項目に表示させる）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$filename);

//DBコネクション終了
pg_close($con);

// インスタンス生成
$hiyariExcelWorkShop = new HiyariExcelWorkShop();

header('Content-Disposition: attachment; filename=' . $filename);

// シート名記載
$hiyariExcelWorkShop->SetSheetName( mb_convert_encoding('ファントルくん　統計分析　定型集計','UTF-8','EUC-JP') );

// ==================================================
// 画面データ取得
// ==================================================
// 集計モード
$date_mode = $_POST["date_mode"];

// 集計年月
$date_year = $_POST["date_year"];
$date_month = $_POST["date_month"];
$date_term = $_POST["date_term"];

// 副報告フラグ
$sub_report_use = $_POST["sub_report_use"];

// 所属データ(配列)
$belong_cnt = $_POST["belong_cnt"];
$belong_class = $_POST["belong_class"];
$belong_attribute = $_POST["belong_attribute"];
$belong_dept = $_POST["belong_dept"];
$belong_room = $_POST["belong_room"];
$belong_selected = $_POST["belong_selected"];

// 縦軸組織項目
$vertical_axis = $_POST["vertical_axis"];

// 横軸項目
$horizontal_axis = $_POST["horizontal_axis"];

// 初期処理
$previous_year = $date_year - 1;

// 小数点桁数
$decimal_num = 2;

$vertical_arr = array();
$horizontal_arr = array();
$result_arr = array();
$previous_result_arr = array();

// セッション変数から親画面で生成したデータを取得
$vertical_arr = $_SESSION["HIYARI_RM_STATS_PATTERN"]["VERTICAL"];
$horizontal_arr = $_SESSION["HIYARI_RM_STATS_PATTERN"]["HORIZONTAL"];
$result_arr = $_SESSION["HIYARI_RM_STATS_PATTERN"]["RESULT"];
$previous_result_arr = $_SESSION["HIYARI_RM_STATS_PATTERN"]["PREVIOUS_RESULT"];

// 列計
$col_total_arr = array();
$col_report_total_arr = array();

// 列計(前年)
$previous_col_total_arr = array();
$previous_col_report_total_arr = array();

// 合計
$all_total = 0;
$all_report_total = "";

// 合計(前年)
$previous_all_total = 0;
$previous_all_report_total = "";

$vertical_cnt = count($vertical_arr);
$horizontal_cnt = count($horizontal_arr);

// 集計条件を収集
$search_upper = "";
$search_lower = "";

$search_upper .= $date_year . "年";
$search_upper .= (int) $date_month . "月から";
$search_upper .= (int) $date_term . "ヶ月分統計分析データ";
$search_upper .= "（縦軸組織：「";
switch ($vertical_axis) {
	case "class":
		$search_upper .= "第１階層";
		break;
	case "attribute":
		$search_upper .= "第２階層";
		break;
	case "dept":
		$search_upper .= "第３階層";
		break;
	case "room":
		$search_upper .= "第４階層";
		break;
}
$search_upper .= "」";
$search_upper .= "横軸：「";
switch ($horizontal_axis) {
	case "months":
		$search_upper .= "月別報告件数";
		break;
	case "job":
		$search_upper .= "当事者職種別報告件数";
		break;
	case "summary":
		$search_upper .= "概要別報告件数";
		break;
	case "patient_level":
		$search_upper .= "患者影響レベル別報告件数";
		break;
	case "clinic":
		$search_upper .= "診療科別報告件数";
		break;
}
$search_upper .= "」）";

for ($i = 0; $i < $belong_cnt; $i++) {
	if ($i == 0) {
		$search_lower .= "所属：";
	} else {
		$search_lower .= "｜";
	}
	$search_lower .= $belong_selected[$i];
}

// 集計条件を固定位置に表示
$hiyariExcelWorkShop->SetAreaCell(0, 1);
$hiyariExcelWorkShop->SetValue(mb_convert_encoding($search_upper, 'UTF-8', 'EUC-JP'));

$hiyariExcelWorkShop->SetAreaCell(0, 2);
$hiyariExcelWorkShop->SetValue(mb_convert_encoding($search_lower, 'UTF-8', 'EUC-JP'));

// データセルの基準点
// 表の基準点でないことに注意
$cell_base_column = 2;
$cell_base_row = 4;

// 縦軸組織ループ
// タイトルを表示するので "-1" からループ
// 計を表示するので "<=" までループ
for ($i = -1; $i <= $vertical_cnt; $i++) {
	// 行計
	$row_total = 0;
	
	// 行計(前年)
	$previous_row_total = 0;
	
	// 行の1項目に対して4つの表示(指定年、前年、増減、増減率)
	for ($j = 0; $j < 4; $j++) {
		if ($i == -1 && $j > 0) {
			continue;
		}
		
		// ボーダーの太さ
		if ($j == 0) {
			$border_style = "MEDIUM";
		} else {
			$border_style = "THIN";
		}
		
		// 横軸ループ
		// タイトルを２つ表示するので "-2" からループ
		// 計を表示するので "<=" までループ
		for ($k = -2; $k <= $horizontal_cnt; $k++) {
			$cell_column = $cell_base_column + $k;
			if ($i >= 0) {
				$cell_row = $cell_base_row + (($i * 4) + $j);
			} else {
				$cell_row = $cell_base_row + $i;
			}
			
			// ブランク(左上)
			if ($i == -1 && $k == -2) {
				// セルの結合
				$hiyariExcelWorkShop->CellMerge("A" . ($cell_base_row - 1) . ":B" . ($cell_base_row - 1));
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				$hiyariExcelWorkShop->SetBackColor("DFFFDC");
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
			// ブランク(左上に統合)
			} else if ($i == -1 && $k == -1) {
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				
				continue;
			} else if ($i == -1) {
				$row_header = "";
				
				// 計の列ヘッダ(右上)
				if ($k == $horizontal_cnt) {
					$row_header = "計";
				// 列ヘッダ(上)
				} else {
					$row_header = $horizontal_arr[$k]["name"];
				}
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				$hiyariExcelWorkShop->SetValue(mb_convert_encoding($row_header, 'UTF-8', 'EUC-JP'));
				$hiyariExcelWorkShop->SetPosH("CENTER");
				$hiyariExcelWorkShop->SetBackColor("DFFFDC");
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				
			} else if ($k == -2) {
				if ($j > 0) {
					$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
					$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
					
					continue;
				}
				$column_header = "";
				
				// 計の行ヘッダ(左下)
				if ($i == $vertical_cnt) {
					$column_header = "計";
				// 行ヘッダ(左)
				} else {
					$column_header = $vertical_arr[$i]["name"];
				}
				
				// セルの結合
				$hiyariExcelWorkShop->CellMerge("A" . $cell_row . ":A" . ($cell_row + 3));
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				$hiyariExcelWorkShop->SetValue(mb_convert_encoding($column_header, 'UTF-8', 'EUC-JP'));
				$hiyariExcelWorkShop->SetPosV("CENTER");
				$hiyariExcelWorkShop->SetBackColor("DFFFDC");
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				$hiyariExcelWorkShop->SetBorder($border_style, "FF35B341", "top");
				
			// 行サブヘッダ(左)
			} else if ($k == -1) {
				$sub_header = "";
				switch ($j) {
					case "0":
						$sub_header = $date_year . "年";
						break;
					case "1":
						$sub_header = $previous_year . "年";
						break;
					case "2":
						$sub_header = "増減";
						break;
					case "3":
						$sub_header = "前年比率";
						break;
				}
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				$hiyariExcelWorkShop->SetValue(mb_convert_encoding($sub_header, 'UTF-8', 'EUC-JP'));
				$hiyariExcelWorkShop->SetPosH("CENTER");
				$hiyariExcelWorkShop->SetBackColor("DFFFDC");
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				$hiyariExcelWorkShop->SetBorder($border_style, "FF35B341", "top");
				
			// 計
			} else if ($i == $vertical_cnt || $k == $horizontal_cnt) {
				// 合計(右下)
				if ($i == $vertical_cnt && $k == $horizontal_cnt) {
					$wk_total = $all_total;
					$wk_previous_total = $previous_all_total;
				// 列計(下)
				} else if ($i == $vertical_cnt) {
					$wk_total = $col_total_arr[$k];
					$wk_previous_total = $previous_col_total_arr[$k];
				// 行計(右)
				} else if ($k == $horizontal_cnt) {
					$wk_total = $row_total;
					$wk_previous_total = $previous_row_total;
				}
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				
				switch ($j) {
					case "0":
						$hiyariExcelWorkShop->SetValue($wk_total);
						
						break;
					case "1":
						$hiyariExcelWorkShop->SetValue($wk_previous_total);
						
						break;
					case "2":
						$inc_dec = $wk_total - $wk_previous_total;
						
						$hiyariExcelWorkShop->SetValue($inc_dec);
						
						break;
					case "3":
						if ($wk_previous_total == 0) {
							$inc_dec_rate = "-";
						} else {
							$inc_dec_rate = $wk_total / $wk_previous_total * 100;
							
							$inc_dec_rate = number_format(round($inc_dec_rate, $decimal_num), 2) . mb_convert_encoding('%', 'UTF-8', 'EUC-JP');
						}
						
						$hiyariExcelWorkShop->SetValue($inc_dec_rate);
						
						break;
				}
				
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				$hiyariExcelWorkShop->SetBorder($border_style, "FF35B341", "top");
				
			// データセル
			} else {
				$num = $result_arr["data"][$i][$k];
				$previous_num = $previous_result_arr["data"][$i][$k];
				
				$hiyariExcelWorkShop->SetAreaCell($cell_column, $cell_row);
				
				switch ($j) {
					case "0":
						// 列の開始で初期化
						if ($i == 0) {
							$col_total_arr[$k] = 0;
							$previous_col_total_arr[$k] = 0;
						}
						
						// 対象年データ加算
						$col_total_arr[$k] += $num;
						$row_total += $num;
						$all_total += $num;
						
						// 前年データ加算
						$previous_col_total_arr[$k] += $previous_num;
						$previous_row_total += $previous_num;
						$previous_all_total += $previous_num;
						
						$hiyariExcelWorkShop->SetValue($num);
						
						break;
					case "1":
						$hiyariExcelWorkShop->SetValue($previous_num);
						
						break;
					case "2":
						$inc_dec = $num - $previous_num;
						
						$hiyariExcelWorkShop->SetValue($inc_dec);
						
						break;
					case "3":
						if ($previous_num == 0) {
							$inc_dec_rate = "-";
						} else {
							$inc_dec_rate = $num / $previous_num * 100;
							
							$inc_dec_rate = number_format(round($inc_dec_rate, $decimal_num), 2) . mb_convert_encoding('%', 'UTF-8', 'EUC-JP');
						}
						
						$hiyariExcelWorkShop->SetValue($inc_dec_rate);
						
						break;
				}
				
				$hiyariExcelWorkShop->SetBorder("THIN", "FF35B341", "all");
				$hiyariExcelWorkShop->SetBorder($border_style, "FF35B341", "top");
			}
		}
	}
}

// 用紙方向
$hiyariExcelWorkShop->PageSetUp("horizontial");

// 保存
$hiyariExcelWorkShop->OutPut();
?>