<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 空床検索</title>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./show_sub_vacant_search.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟名の取得
$cond = "order by bldg_cd, ward_cd";
$sel = select_from_table($con,$SQL67,$cond,$fname);
if($sel == 0){
	pg_exec($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$num = pg_numrows($sel);
for($i=0;$i<$num;$i++){
	$b_cd = pg_result($sel,$i,"bldg_cd");
	$w_cd = pg_result($sel,$i,"ward_cd");
	$cd = "$b_cd"."-"."$w_cd";
	if($bldgwd == $cd){
		$ward_nm = pg_result($sel,$i,"ward_name");
	}
}

// 初期表示時は部屋タイプ「一般」「個室」にチェック
if ($back == "") {$room_type = array("1", "2");}

// デフォルトは「入院予定非表示」
if ($show_reservation == "") {$show_reservation = "f";}
?>
<script type="text/javascript">
function selectBed(entiID, bldgwd, roomID, bedNo) {

//	opener.document.inpt.enti.value = entiID;
//	opener.entiOnChange();

	opener.document.inpt.ward.value = bldgwd;
	opener.wardOnChange();

	opener.document.inpt.ptrm.value = roomID;
	opener.ptrmOnChange();

	opener.document.inpt.bed.value = bedNo;

	if (opener.document.inpt.bedundec1 != null) {
		opener.document.inpt.bedundec1.checked = false;
		opener.document.inpt.bedundec2.checked = false;
		opener.document.inpt.bedundec3.checked = false;
		opener.bedundec1OnClick();
	}

	self.close();

}
</script>
</head>
<script type="text/javascript" src="./js/fontsize.js"></script>
<style type="text/css">
form {margin:0;}
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>空床検索</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="sub_vacant_room_list.php" method="post" name="vacant">
<table width="800" border="0" cellspacing="0" cellpadding="1" class="bed">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td><select name="bldgwd">
<option value="0">すべて
<? show_ward($con, $bldgwd, $fname); ?>
</select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="in_yr"><? show_certain_years_future(2,$in_yr); ?></select>/<select name="in_mon"><? show_select_months($in_mon); ?></select>/<select name="in_day"><? show_select_days($in_day); ?></select>&nbsp;<select name="in_hr"><? show_select_hrs_0_23($in_hr); ?></select>：<select name="in_min"><option value="00"<? if ($in_min == "00") {echo " selected";} ?>>00<option value="30"<? if ($in_min == "30") {echo " selected";} ?>>30</select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td><select name="sex">
<option value="1"<? if ($sex == "1") {echo(" selected");} ?>>すべて
<option value="2"<? if ($sex == "2") {echo(" selected");} ?>>男
<option value="3"<? if ($sex == "3") {echo(" selected");} ?>>女
</select></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部屋タイプ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="room_type[]" value="1"<? if (in_array("1", $room_type)) {echo(" checked");} ?> style="vertical-align:middle;">一般
<input type="checkbox" name="room_type[]" value="2"<? if (in_array("2", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">個室
<input type="checkbox" name="room_type[]" value="3"<? if (in_array("3", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">回復室
<input type="checkbox" name="room_type[]" value="4"<? if (in_array("4", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">ICU
<input type="checkbox" name="room_type[]" value="5"<? if (in_array("5", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">HCU
<input type="checkbox" name="room_type[]" value="6"<? if (in_array("6", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">NICU
<input type="checkbox" name="room_type[]" value="7"<? if (in_array("7", $room_type)) {echo(" checked");} ?> style="margin-left:5px;vertical-align:middle;">GCU
</font></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right"><input type="submit" value="検索"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
<table width="800" border="0" cellspacing="0" cellpadding="1">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="show_reservation" value="t"<? if ($show_reservation == "t") {echo(" checked");} ?> onclick="this.form.submit();">入院予定・転床予定を表示</font></td>
</tr>
</table>
<? show_room_detail_list($con, $session, $bldgwd, $in_yr, $in_mon, $in_day, $in_hr, $in_min, $fname, $sex, $room_type, $show_reservation); ?>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
