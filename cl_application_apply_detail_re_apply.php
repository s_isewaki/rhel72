<?php
//ini_set( 'display_errors', 1 );
require_once("cl_common_log_class.inc");
require_once("cl_common_apply.inc");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cl_application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}

/*
for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "approve_name$i";
	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
}
*/

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>


<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");

$fname=$PHP_SELF;


//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// 決裁・申請権限のチェック
//------------------------------------------------------------------------------
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//------------------------------------------------------------------------------
// 入力チェック
//------------------------------------------------------------------------------
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

//------------------------------------------------------------------------------
// データベースに接続
//------------------------------------------------------------------------------
$con = connect2db($fname);
$log->debug('データベースに接続(mdb2)　開始',__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$log->debug('データベースに接続(mdb2)　終了',__FILE__,__LINE__);

$obj = new cl_application_workflow_common_class($con, $fname);

//------------------------------------------------------------------------------
// トランザクションを開始
//------------------------------------------------------------------------------
pg_query($con, "begin");
$log->debug("トランザクション開始(mdb2) START",__FILE__,__LINE__);
$mdb2->beginTransaction();
$log->debug("トランザクション開始(mdb2) END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// ログインユーザ取得
//------------------------------------------------------------------------------
$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];


//------------------------------------------------------------------------------
// 再申請チェック
//------------------------------------------------------------------------------
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
if($apply_stat != "3") {
	echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//------------------------------------------------------------------------------
// 新規申請(apply_id)採番
//------------------------------------------------------------------------------
//$sql = "select max(apply_id) from cl_apply";
//$cond = "";
//$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
//$max = pg_result($apply_max_sel,0,"max");
//if($max == ""){
//	$new_apply_id = "1";
//}else{
//	$new_apply_id = $max + 1;
//}
require_once(dirname(__FILE__) . "/cl/model/sequence/cl_apply_id_seq_model.php");
$cl_apply_id_model = new cl_apply_id_seq_model($mdb2,$login_emp_id);
$new_apply_id=$cl_apply_id_model->getApplyId();
$log->debug("■NEW apply_id:".$apply_id,__FILE__,__LINE__);

/*
//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "cl/template/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );

}
*/
//------------------------------------------------------------------------------
// 'エスケープ
//------------------------------------------------------------------------------
$apply_title   = pg_escape_string($apply_title);
$apply_content = pg_escape_string($content);

//------------------------------------------------------------------------------
// 申請登録
//------------------------------------------------------------------------------
$obj->regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, "DETAIL");


//------------------------------------------------------------------------------
// 承認登録
//------------------------------------------------------------------------------
$obj->regist_re_applyapv($new_apply_id, $apply_id);


//------------------------------------------------------------------------------
// 承認者候補登録
//------------------------------------------------------------------------------
$obj->regist_re_applyapvemp($new_apply_id, $apply_id);


//------------------------------------------------------------------------------
// 添付ファイル登録
//------------------------------------------------------------------------------
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($new_apply_id, $no, $tmp_filename);
	$no++;
}

//------------------------------------------------------------------------------
// 非同期・同期受信登録
//------------------------------------------------------------------------------
$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);


//------------------------------------------------------------------------------
// 申請結果通知登録
//------------------------------------------------------------------------------
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($new_apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

//------------------------------------------------------------------------------
// 前提とする申請書(申請用)登録
//------------------------------------------------------------------------------
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->regist_applyprecond($new_apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

//------------------------------------------------------------------------------
// 元の申請書に再申請ＩＤを更新
//------------------------------------------------------------------------------
$obj->update_re_apply_id($apply_id, $new_apply_id);


//------------------------------------------------------------------------------
// 評価申請の場合
//------------------------------------------------------------------------------
$short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];
if($obj->is_eval_template($short_wkfw_name))
{
	if($obj->is_eval_level2_template($short_wkfw_name))
	{
        $level = "2";
    }
    else if($obj->is_eval_level3_template($short_wkfw_name))
    {
        $level = "3";
    }

    $application_book_apply_id = $obj->get_cl_recognize_schedule_apply_id($apply_id, $short_wkfw_name);
    if($application_book_apply_id > 0)
    {
        $arr_eval_apply_id = $obj->create_arr_eval_apply_id($application_book_apply_id, $new_apply_id, $short_wkfw_name, "ADD");
	    $obj->update_cl_recognize_schedule_for_apply_id($arr_eval_apply_id);
    }
}

//------------------------------------------------------------------------------
// レベルアップ申請の場合
//------------------------------------------------------------------------------
if($obj->is_levelup_apply_template($short_wkfw_name))
{
    // 念のため
    if($levelup_application_book_id != "")
    {
	    $apply_content = $arr_apply_wkfwmst[0]["apply_content"];
		$levelup_application_book_id = $obj->get_levelup_application_book_id($apply_content);
    }
    $obj->update_levelup_apply_id($levelup_application_book_id, $new_apply_id);
}

//------------------------------------------------------------------------------
// テンプレートの項目のみをパラメータに設定
//------------------------------------------------------------------------------
$param=template_parameter_pre_proccess($_POST);

//------------------------------------------------------------------------------
// パラメータに申請ＩＤを追加
//------------------------------------------------------------------------------
$param['apply_id'] = $new_apply_id;
$param['emp_id'] = $login_emp_id;

//------------------------------------------------------------------------------
// テンプレート独自の申請処理を呼び出す
//------------------------------------------------------------------------------
//ハンドラーモジュールロード
$log->debug('ハンドラーモジュールロード　開始　管理番号：'.$short_wkfw_name,__FILE__,__LINE__);
$handler_script=dirname(__FILE__) . "/cl/handler/".$short_wkfw_name."_handler.php";
$log->debug('$handler_script：'.$handler_script,__FILE__,__LINE__);
require_once($handler_script);
$log->debug('ハンドラーモジュールロード　終了',__FILE__,__LINE__);

// 2012/04/21 Yamagawa add(s)
// ハンドラー　クラス化対応
$log->debug('ハンドラークラスインスタンス化　開始',__FILE__,__LINE__);
$handler_name = $short_wkfw_name."_handler";
$handler = new $handler_name();
$log->debug('ハンドラークラスインスタンス化　終了',__FILE__,__LINE__);
// 2012/04/21 Yamagawa add(e)

//------------------------------------------------------------------------------
// 申請詳細　再申請の場合
//------------------------------------------------------------------------------
if( $mode == "re_apply"){

	$log->debug('■ハンドラー申請詳細　再申請　開始',__FILE__,__LINE__);
	// 2012/04/21 Yamagawa upd(s)
	//AplDetail_ReRegist($mdb2, $login_emp_id, $param);
	$handler->AplDetail_ReRegist($mdb2, $login_emp_id, $param);
	// 2012/04/21 Yamagawa upd(e)
	$log->debug('■ハンドラー申請詳細　再申請　終了',__FILE__,__LINE__);

}
//------------------------------------------------------------------------------
// その他
//------------------------------------------------------------------------------
else{

}

//------------------------------------------------------------------------------
// トランザクションをコミット
//------------------------------------------------------------------------------
pg_query($con, "commit");
$log->debug("トランザクション確定 START",__FILE__,__LINE__);
$mdb2->commit();
$log->debug("トランザクション確定 END",__FILE__,__LINE__);

//------------------------------------------------------------------------------
// データベース接続を切断
//------------------------------------------------------------------------------
pg_close($con);
$log->debug("データベース切断 START",__FILE__,__LINE__);
$mdb2->disconnect();
$log->debug("データベース切断 END",__FILE__,__LINE__);


if (!is_dir("cl/apply")) {
	mkdir("cl/apply", 0755);
}

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cl/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cl/apply/{$new_apply_id}_{$tmp_fileno}{$ext}");

}
foreach (glob("cl/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}





// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">close();</script>\n");

?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
$log->info(basename(__FILE__)." END");
?>
