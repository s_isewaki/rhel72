<?php
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

require_once("about_comedix.php");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo('<script type="text/javascript" src="js/showpage.js"></script>');
    echo('<script type="text/javascript">showLoginPage(window);</script>');
    exit;
}

///-----------------------------------------------------------------------------
// 値のチェック
///-----------------------------------------------------------------------------
if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['match_color']) !== 1 and $_POST['match_color'] !== 'noset') {
    $_POST['match_color'] = null;
}
if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['match_bgcolor']) !== 1 and $_POST['match_bgcolor'] !== 'noset') {
    $_POST['match_bgcolor'] = null;
}
if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['unmatch_color']) !== 1 and $_POST['unmatch_color'] !== 'noset') {
    $_POST['unmatch_color'] = null;
}
if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['unmatch_bgcolor']) !== 1 and $_POST['unmatch_bgcolor'] !== 'noset') {
    $_POST['unmatch_bgcolor'] = null;
}
//if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['highlight_color']) !== 1 and $_POST['highlight_color'] !== 'noset') {
//    $_POST['highlight_color'] = null;
//}
//if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['highlight_bgcolor']) !== 1 and $_POST['highlight_bgcolor'] !== 'noset') {
//    $_POST['highlight_bgcolor'] = null;
//}
if (preg_match("/^#[0-9a-f]{6}$/i", $_POST['highlight_bordercolor']) !== 1 and $_POST['highlight_bordercolor'] !== 'noset') {
    $_POST['highlight_bordercolor'] = null;
}

// オプション追加
if ($_POST['tosYotei_button_flg'] !== 't') {
    $_POST['tosYotei_button_flg'] = 'f';
}
if ($_POST['year_paid_hol_btn_flg'] !== 't') {
    $_POST['year_paid_hol_btn_flg'] = 'f';
}
if ($_POST['timecard_error_btn_flg'] !== 't') {
    $_POST['timecard_error_btn_flg'] = 'f';
}

///-----------------------------------------------------------------------------
// 値のセット
///-----------------------------------------------------------------------------
$conf = new Cmx_SystemConfig();
$conf->set('shift.hope.match.color', $_POST['match_color']);
$conf->set('shift.hope.match.bgcolor', $_POST['match_bgcolor']);
$conf->set('shift.hope.unmatch.color', $_POST['unmatch_color']);
$conf->set('shift.hope.unmatch.bgcolor', $_POST['unmatch_bgcolor']);
//$conf->set('shift.highlight.color', $_POST['highlight_color']);
//$conf->set('shift.highlight.bgcolor', $_POST['highlight_bgcolor']);
$conf->set('shift.highlight.bordercolor', $_POST['highlight_bordercolor']);

// オプション追加
$conf->set('shift.tosYotei_button_flg', $_POST['tosYotei_button_flg']);
$conf->set('shift.year_paid_hol_btn_flg', $_POST['year_paid_hol_btn_flg']);
$conf->set('shift.timecard_error_btn_flg', $_POST['timecard_error_btn_flg']);

?>
<script type="text/javascript">
    location.href = 'duty_shift_entity_color.php?session=<? eh($session);?>';
</script>
