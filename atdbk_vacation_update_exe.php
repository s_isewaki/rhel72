<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($days1 != "") {
	if (preg_match("/^\d{1,3}$/", $days1) == 0) {
		echo("<script type=\"text/javascript\">alert('前年度繰越日数（有給休暇）は半角数字1〜3桁で入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$days1 = intval($days1);
}
if ($days2 != "") {
	if (preg_match("/^\d{1,3}$/", $days2) == 0) {
		echo("<script type=\"text/javascript\">alert('本年度付与日数（有給休暇）は半角数字1〜3桁で入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$days2 = intval($days2);
}
if ($days3 != "") {
	if (preg_match("/^\d{1,3}$/", $days3) == 0) {
		echo("<script type=\"text/javascript\">alert('前年度繰越日数（代替出勤）は半角数字1〜3桁で入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$days3 = intval($days3);
}
if ($days4 != "") {
	if (preg_match("/^\d{1,3}$/", $days4) == 0) {
		echo("<script type=\"text/javascript\">alert('前年度繰越日数（代替出勤）は半角数字1〜3桁で入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$days4 = intval($days4);
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 有給休暇レコードを削除
$sql = "delete from emppaid";
$cond = "where emp_id = '$emp_id' and year = '$year'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// 有給休暇レコードを作成
$sql = "insert into emppaid (emp_id, year, days1, days2, days3, days4) values (";
$content = array($emp_id, $year, $days1, $days2, $days3, $days4);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 休暇管理画面を再表示
echo("<script type=\"text/javascript\">location.href = 'atdbk_vacation.php?session=$session&year=$year';</script>");
?>
