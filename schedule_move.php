<?
$fname = $PHP_SELF;

ob_start();
require("about_session.php");
require("about_authority.php");
ob_end_clean();

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("タイムアウトしました。");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("権限がありません。");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "rollback");

// スケジュール情報を取得
$sql = "select * from schdmst";
$cond = "where schd_id = '$schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("システムエラーが発生しました。");
	exit;
}
$org_start_time = pg_fetch_result($sel, 0, "schd_start_time");
$org_end_time = pg_fetch_result($sel, 0, "schd_dur");

// スケジュールの分数を算出
$schd_min = diff_min($org_start_time, $org_end_time);

// 終了時刻を確定
$end_time = calc_end_time($start_time, $schd_min);

// スケジュール情報を更新
$sql = "update schdmst set";
$set = array("schd_start_date", "schd_start_time", "schd_dur");
$setvalue = array($ymd, $start_time, $end_time);
$cond = "where schd_id = '$schd_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("システムエラーが発生しました。");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

echo("done");

function diff_min($start_time, $end_time) {
	list($start_hour, $start_min) = split(":", $start_time);
	list($end_hour, $end_min) = split(":", $end_time);
	return (($end_hour * 60 + $end_min) - ($start_hour * 60 + $start_min));
}

function calc_end_time($start_time, $schd_min) {
	list($start_hour, $start_min) = split(":", $start_time);
	$end_time = ($start_hour * 60 + $start_min) + $schd_min;

	if ($end_time <= (23 * 30 + 30)) {
		return sprintf("%02d:%02d", intval($end_time / 60), $end_time % 60);
	} else {
		return "23:30";
	}
}
?>
