<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

$q1 = @$_REQUEST["q1"];
$q2 = @$_REQUEST["q2"];
$q3 = @$_REQUEST["q3"];
$q4_1 = @$_REQUEST["q4_1"];
$q4_2 = @$_REQUEST["q4_2"];
$q5 = @$_REQUEST["q5"];
$q6_1 = @$_REQUEST["q6_1"];
$q6_2 = @$_REQUEST["q6_2"];
$q7 = @$_REQUEST["q7"];
$q7_com = @$_REQUEST["q7_com"];
$q8_1 = @$_REQUEST["q8_1"];
$q8_2 = @$_REQUEST["q8_2"];


// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if($q1 == ""){
	echo("<script language=\"javascript\">alert(\"質問１を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q2 == ""){
	echo("<script language=\"javascript\">alert(\"質問２を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q3 == ""){
	echo("<script language=\"javascript\">alert(\"質問３を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q4_1 == ""){
	echo("<script language=\"javascript\">alert(\"質問４（有・無）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q4_1 == "f" && $q4_2 == ""){
	echo("<script language=\"javascript\">alert(\"質問４（手渡し済・未）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q5 == ""){
	echo("<script language=\"javascript\">alert(\"質問５を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q6_1 == ""){
	echo("<script language=\"javascript\">alert(\"質問６（有・無）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q6_1 == "f" && $q6_2 == ""){
	echo("<script language=\"javascript\">alert(\"質問６（手渡し済・未）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q7 == ""){
	echo("<script language=\"javascript\">alert(\"質問７を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q8_1 == ""){
	echo("<script language=\"javascript\">alert(\"質問８（有・無）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($q8_1 == "f" && $q8_2 == ""){
	echo("<script language=\"javascript\">alert(\"質問８（手渡し済・未）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 登録値の編集
if ($q4_1 == "t") $q4_2 = "";
if ($q6_1 == "t") $q6_2 = "";
if ($q8_1 == "t") $q8_2 = "";
$up_date = date("Ymd") . date("Hi");

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con,"begin transaction");

// チェックリスト情報の作成（delete - insert）
$sql = "delete from inptq";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$content = array($pt_id,$q1,$q2,$q3,$q4_1,$q4_2,$q5,$q6_1,$q6_2,$q7,$q7_com,$q8_1,$q8_2,$up_date);
$in = insert_into_table($con,$SQL123,$content,$fname);
if($in == 0){
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// チェックリスト画面を再表示
if ($path == "4" || $path == "5") {
	echo("<script language='javascript'>location.href = 'inpatient_check_list.php?session=$session&pt_id=$pt_id&path=$path';</script>\n");
} else if ($path == "M") {
	echo("<script language='javascript'>location.href = 'summary_inpatient_check.php?session=$session&pt_id=$pt_id';</script>\n");
} else {
	$url_pt_nm = urlencode($pt_nm);
	echo "<script language='javascript'>location.href = 'bed_menu_inpatient_check.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>";
}
?>
