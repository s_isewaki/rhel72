<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($day1_hour == "--" && $day1_min == "--") {
    echo("<script type=\"text/javascript\">alert('所定労働時間は必ず指定してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}
if (($day1_hour == "--" && $day1_min != "--") || ($day1_hour != "--" && $day1_min == "--") || ($day4_hour == "--" && $day4_min != "--") || ($day4_hour != "--" && $day4_min == "--") || ($day5_hour == "--" && $day5_min != "--") || ($day5_hour != "--" && $day5_min == "--") || ($am_hour == "--" && $am_min != "--") || ($am_hour != "--" && $am_min == "--") || ($pm_hour == "--" && $pm_min != "--") || ($pm_hour != "--" && $pm_min == "--")) {
	echo("<script type=\"text/javascript\">alert('時間と分の一方のみは登録できません。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if (strlen($day2) > 12) {
	echo("<script type=\"text/javascript\">alert('法定休日名称が長すぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if (strlen($day3) > 12) {
	echo("<script type=\"text/javascript\">alert('所定休日名称が長すぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if (strlen($day4) > 12) {
	echo("<script type=\"text/javascript\">alert('勤務日1名称が長すぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}
if (strlen($day5) > 12) {
	echo("<script type=\"text/javascript\">alert('勤務日2名称が長すぎます。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 登録値の編集
if ($day1_hour == "--") {
	$day1_hour = "";
}
if ($day1_min == "--") {
	$day1_min = "";
}
if ($day4_hour == "--") {
	$day4_hour = "";
}
if ($day4_min == "--") {
	$day4_min = "";
}
if ($day5_hour == "--") {
	$day5_hour = "";
}
if ($day5_min == "--") {
	$day5_min = "";
}
if ($am_hour == "--") {
	$am_hour = "";
}
if ($am_min == "--") {
	$am_min = "";
}
if ($pm_hour == "--") {
	$pm_hour = "";
}
if ($pm_min == "--") {
	$pm_min = "";
}
$day1_time = "$day1_hour$day1_min";
$day4_time = "$day4_hour$day4_min";
$day5_time = "$day5_hour$day5_min";
$am_time = "$am_hour$am_min";
$pm_time = "$pm_hour$pm_min";

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 勤務日レコードがなければ作成、あれば更新
$sql = "select count(*) from calendarname";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) == 0) {
	$sql = "insert into calendarname (day1_time, day2, day3, day4, day4_time, day5, day5_time, am_time, pm_time) values (";
	$content = array($day1_time, $day2, $day3, $day4, $day4_time, $day5, $day5_time, $am_time, $pm_time);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update calendarname set";
	$set = array("day1_time", "day2", "day3", "day4", "day4_time", "day5", "day5_time", "am_time", "pm_time");
	$setvalue = array($day1_time, $day2, $day3, $day4, $day4_time, $day5, $day5_time, $am_time, $pm_time);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 勤務日画面を再表示
echo("<script type=\"text/javascript\">location.href = 'calendar_name.php?session=$session';</script>");
?>
