<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
	th { text-align:left }
</style>
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

$qq1 = @$_REQUEST["qq1"];
$qq1_1 = @$_REQUEST["qq1_1"];
$qq1_2 = @$_REQUEST["qq1_2"];
$qq1_3 = @$_REQUEST["qq1_3"];
$qq1_4 = @$_REQUEST["qq1_4"];
$qq1_5_com = @$_REQUEST["qq1_5_com"];
$qq1_6_com = @$_REQUEST["qq1_6_com"];
$qq2_1 = @$_REQUEST["qq2_1"];
$qq2_2 = @$_REQUEST["qq2_2"];
$qq2_3 = @$_REQUEST["qq2_3"];
$qq2_4 = @$_REQUEST["qq2_4"];
$qq2_5 = @$_REQUEST["qq2_5"];
$qq2_6 = @$_REQUEST["qq2_6"];
$qq2_7 = @$_REQUEST["qq2_7"];
$qq2_8 = @$_REQUEST["qq2_8"];
$qq3 = @$_REQUEST["qq3"];
$qq4 = @$_REQUEST["qq4"];
$qq5_1 = @$_REQUEST["qq5_1"];
$qq5_2 = @$_REQUEST["qq5_2"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 57, $fname);  // メドレポート
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$checkauth = check_authority($session, 14, $fname);  // 病床管理
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者参照権限・入院来院履歴権限を取得
$pt_ref_auth = check_authority($session, 15, $fname);
$inout_auth = check_authority($session, 33, $fname);

// データベースに接続
$con = connect2db($fname);

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);


// 患者情報を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 栄養問診表情報を取得
$sql = "select * from inptnut";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
if (pg_num_rows($sel) > 0) {
	$qq1 = pg_result($sel, 0, "nut_q1");
	$qq1_1 = pg_result($sel, 0, "nut_q1_1");
	$qq1_2 = pg_result($sel, 0, "nut_q1_2");
	$qq1_3 = pg_result($sel, 0, "nut_q1_3");
	$qq1_4 = pg_result($sel, 0, "nut_q1_4");
	$qq1_5_com = pg_result($sel, 0, "nut_q1_5_com");
	$qq1_6_com = pg_result($sel, 0, "nut_q1_6_com");
	$qq2_1 = pg_result($sel, 0, "nut_q2_1");
	$qq2_2 = pg_result($sel, 0, "nut_q2_2");
	$qq2_3 = pg_result($sel, 0, "nut_q2_3");
	$qq2_4 = pg_result($sel, 0, "nut_q2_4");
	$qq2_5 = pg_result($sel, 0, "nut_q2_5");
	$qq2_6 = pg_result($sel, 0, "nut_q2_6");
	$qq2_7 = pg_result($sel, 0, "nut_q2_7");
	$qq2_8 = pg_result($sel, 0, "nut_q2_8");
	$qq3 = pg_result($sel, 0, "nut_q3");
	$qq4 = pg_result($sel, 0, "nut_q4");
	$qq5_1 = pg_result($sel, 0, "nut_q5_1");
	$qq5_2 = pg_result($sel, 0, "nut_q5_2");
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 入院/入所
$in_title = $_label_by_profile["IN"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <? echo($in_title); ?><? echo($patient_title); ?><?
// 栄養問診票/食事チェック票
echo ($_label_by_profile["NUTRITION"][$profile_type]); ?></title>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, $_label_by_profile["NUTRITION"][$profile_type], "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, $_label_by_profile["NUTRITION"][$profile_type], 0, $pt_id, $pager_param_addition); ?>



<table class="prop_table" cellspacing="1">
	<tr height="22">
		<th width="100px"><? echo ($patient_title); ?>ID</th>
		<td width="100px" style="text-align:center"><? echo($pt_id); ?></td>
		<th width="100px"><? echo ($patient_title); ?>氏名</th>
		<td width=""><? echo("$lt_kj_nm $ft_kj_nm"); ?></td>
	</tr>
</table>


<form name="inpt" action="inpatient_nutrition_confirm.php" method="post">


<table class="prop_table" style="width:800px; margin-top:16px" cellspacing="1">
<tr>
<th width="54%">問1-1）&nbsp;食品のアレルギーがありますか？</th>
<td width="46%"><label><input type="radio" class="baseline" name="qq1" value="t" <? if($qq1 == "t"){echo("checked");} ?>>ある</label>&nbsp;<label><input type="radio" class="baseline" name="qq1" value="f" <? if($qq1 == "f"){echo("checked");} ?>>なし</label></td>
</tr>
<tr height="22">
<th>問1-2）&nbsp;問1-1で”ある”とお答えの方はその食品も教えてください</th>
<td><label><input type="checkbox" class="baseline" name="qq1_1" <? if($qq1_1 == "t"){echo("checked");} ?>>卵</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq1_2" <? if($qq1_2 == "t"){echo("checked");} ?>>肉</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq1_3" <? if($qq1_3 == "t"){echo("checked");} ?>>大豆製品</label><br>
<label><input type="checkbox" class="baseline" name="qq1_4" <? if($qq1_4 == "t"){echo("checked");} ?>>魚</label>&nbsp;&nbsp;種類：<input type="text" name="qq1_5_com" value="<? echo($qq1_5_com); ?>">
<div style="padding-top:2px">&nbsp;その他：<input type="text" name="qq1_6_com" value="<? echo($qq1_6_com); ?>"></div></td>
</tr>
<tr height="22">
<th>問2）&nbsp;召し上がれないものがありますか？</th>
<td><label><input type="checkbox" class="baseline" name="qq2_1" <? if($qq2_1 == "t"){echo("checked");} ?>>牛肉</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_2" <? if($qq2_2 == "t"){echo("checked");} ?>>豚肉</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_3" <? if($qq2_3 == "t"){echo("checked");} ?>>鶏肉</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_4" <? if($qq2_4 == "t"){echo("checked");} ?>>魚</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_5" <? if($qq2_5 == "t"){echo("checked");} ?>>卵</label><br><label><input type="checkbox" class="baseline" name="qq2_6" <? if($qq2_6 == "t"){echo("checked");} ?>>豆腐</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_7" <? if($qq2_7 == "t"){echo("checked");} ?>>納豆</label>&nbsp;<label><input type="checkbox" class="baseline" name="qq2_8" <? if($qq2_8 == "t"){echo("checked");} ?>>牛乳</label></td>
</tr>
<tr height="22">
<th>問3）&nbsp;食事を細かくすることを希望しますか？</th>
<td><label><input type="radio" class="baseline" name="qq3" value="t" <? if($qq3 == "t"){echo("checked");} ?>>する</label>&nbsp;<label><input type="radio" class="baseline" name="qq3" value="f" <? if($qq3 == "f"){echo("checked");} ?>>しない</label></td>
</tr>
<tr height="22">
<th>問4）&nbsp;朝食は和食（ご飯食）と洋食（パン食）どちらを希望しますか？</th>
<td><label><input type="radio" class="baseline" name="qq4" value="t" <? if($qq4 == "t"){echo("checked");} ?>>和食</label>&nbsp;<label><input type="radio" class="baseline" name="qq4" value="f" <? if($qq4 == "f"){echo("checked");} ?>>パン食</label></td>
</tr>
<tr height="22">
<th>問5）&nbsp;食事指示はありますか？</th>
<td><label><input type="radio" class="baseline" name="qq5_1" value="t" <? if($qq5_1 == "t"){echo("checked");} ?>>ある</label>（<label><input type="radio" class="baseline" name="qq5_2" value="t" <? if($qq5_2 == "t"){echo("checked");} ?>>治療食</label>&nbsp;<label><input type="radio" class="baseline" name="qq5_2" value="f" <? if($qq5_2 == "f"){echo("checked");} ?>>常食</label>）&nbsp;<label><input type="radio" class="baseline" name="qq5_1" value="f" <? if($qq5_1 == "f"){echo("checked");} ?>>なし</label></td>
</tr>
</table>



<div class="search_button_div" style="width:800px">
	<input type="button" onclick="document.inpt.submit();" value="登録"/>
</div>

<input type="submit" name="dummy" style="display:none"/>
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="M">
<input type="hidden" name="enti" value="1">
</form>

</body>
<? pg_close($con); ?>
</html>
