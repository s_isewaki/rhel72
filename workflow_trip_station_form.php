<?php
// 出張旅費
// 新幹線駅名マスタ・フォーム
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

if (!empty($_REQUEST["id"])) {
	// 新幹線駅名マスタの取得
	$id = $_REQUEST["id"];
	$sql = "SELECT * FROM trip_station WHERE station_id={$id}";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}
	$row = pg_fetch_assoc($sel);
}
else {
	$id = "";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 新幹線駅名マスタ・フォーム</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, array('id' => $id));
show_wkfw_menu2item($session, $fname, array('id' => $id));
?>

<form action="workflow_trip_station_submit.php" method="post">
<p><b>■新幹線駅名の登録・変更</b></p>
<table id="form">
<tr>
	<td class="form_title">駅名</td>
	<td><input type="text" name="title" size="100" value="<?eh(!empty($row) ? $row["title"] : "");?>" style="ime-mode:active;" /></td>
</tr>

</table>
<button type="submit">この内容で登録する</button>

<input type="hidden" name="id" value="<?=$id?>" />
<input type="hidden" name="session" value="<?=$session?>" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);