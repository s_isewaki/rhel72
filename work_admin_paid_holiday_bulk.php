<?php
require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("timecard_common_class.php");
require_once("timecard_bean.php");
require_once("work_admin_paid_holiday_common.php");
require_once("timecard_paid_hol_hour_class.php"); //20120528
require_once("calendar_name_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);

// 職員ID・氏名を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, date("Ymd"), "");
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
//期間確認 yymm形式
if ($timecard_bean->criteria_months == "2") {
    $arr_hikaku_tbl = array("0706", "0606", "0506", "0406", "0306", "0206", "0106", "0006");
} else {
    $arr_hikaku_tbl = array("0700", "0600", "0500", "0400", "0300", "0200", "0100", "0003");
}

// トランザクションを開始
pg_query($con, "begin transaction");

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
while($row = pg_fetch_array($sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=8; $i++) {
		$varname = "add_day".$i;
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120821
        $wk_add_day = $row["$varname"];
        if ($i == 8 && $wk_add_day == "") {
            $wk_add_day = $arr_paid_hol[$paid_hol_tbl_id][7];
        }
        if ($wk_add_day == "") {
            $wk_add_day = "0";
        }
        $arr_paid_hol[$paid_hol_tbl_id][$i] = $wk_add_day;
	}
}

//有休付与日数表入職年度用の読み込み
$sql = "select * from timecard_paid_hol2 order by order_no";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$arr_paid_hol2 = array();

while($row = pg_fetch_array($sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=12; $i++) {
		$varname = "add_day".$i;
		$arr_paid_hol2[$paid_hol_tbl_id][$i] = $row["$varname"];
	}
}

$sql = "select empmst.emp_id, empmst.emp_personal_id, emp_lt_nm, emp_ft_nm, emppaid.days1, emppaid.days2, emppaid.adjust_day ";
$sql .= " , empmst.emp_join, f.paid_hol_tbl_id, g.days1 as prev_days1 ,g.days2 as prev_days2, g.adjust_day as prev_adjust_day, emppaid.year as emppaid_year, emppaid.paid_hol_add_mmdd, emppaid.service_year, emppaid.service_mon, g.paid_hol_tbl_id as prev_tbl_id, g.service_year as prev_service_year, g.service_mon as prev_service_mon, g.year as prev_year, g.paid_hol_add_mmdd as prev_paid_hol_add_mmdd, h.last_add_date, h.curr_carry, h.curr_add, h.curr_use, h.data_migration_date "
    ."  , i.last_carry as hol_hour_last_carry, i.last_use as hol_hour_last_use, i.curr_carry as hol_hour_curr_carry, i.curr_use as hol_hour_curr_use, i.data_migration_date as hol_hour_data_migration_date, f.specified_time, g.carry_time_minute as prev_carry_time_minute "; //時間有休 20120528
$sql .= " from empmst ";

$search_mmdd = "";
if ($select_add_mon != "-" && $select_add_day != "-") {
	$search_mmdd = $select_add_mon.$select_add_day;
	$compare_mmdd = "mmdd";
	$compare_len = 4;
} elseif ($select_add_mon != "-") {
	$search_mmdd = $select_add_mon;
	$compare_mmdd = "mm";
	$compare_len = 2;
}
//2件有り得るので1件に絞り込む 20110404
$sql .= " left join (select distinct on (b.emp_id) b.* from emppaid b  ";
$sql .= " where   ";
$sql .= " CAST(b.year AS varchar) = '$select_add_yr' and substr(b.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd' order by b.emp_id, b.year desc, b.paid_hol_add_mmdd desc ";
$sql .= " ) emppaid on emppaid.emp_id = empmst.emp_id ";

//前回付与データ
$wk_mmdd = $select_add_mon;
if ($select_add_day != "-") {
	$wk_mmdd .= $select_add_day;
} else {
	$wk_mmdd .= "01";
}
$wk_year_before = ($select_add_yr - 1).$wk_mmdd;
$wk_select_ymd = $select_add_yr.$wk_mmdd;

$sql .= " left join (select distinct on (b.emp_id) b.* from emppaid b where CAST(b.year AS varchar)||b.paid_hol_add_mmdd >= '$wk_year_before' and CAST(b.year AS varchar)||b.paid_hol_add_mmdd < '$wk_select_ymd' order by b.emp_id, b.year desc, b.paid_hol_add_mmdd desc ) g on g.emp_id = empmst.emp_id  "; //前回付与データ

$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
$sql .= " left join timecard_paid_hol_import h on h.emp_personal_id = empmst.emp_personal_id ";
$sql .= " left join timecard_paid_hol_hour_import i on i.emp_personal_id = empmst.emp_personal_id ";

$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";
$cond .= " and emppaid.year is null ";
//検索条件　職員ID
if ($srch_id != "") {
    $tmp_srch_id = str_replace(" ", "", $srch_id);
    $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
}
//検索条件　職員名
if ($srch_name != "") {
	$tmp_srch_name = str_replace(" ", "", $srch_name);
	$cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
}
//検索条件　属性：事務所
if ($cls != "" && substr($cls, 0, 1) != "-") {
	$cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
}
//検索条件　属性：課
if ($atrb != "" && substr($atrb, 0, 1) != "-") {
	$cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
}
//検索条件　属性：科
if ($dept != "" && substr($dept, 0, 1) != "-") {
	$cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
}
//検索条件　雇用・勤務形態
if ($duty_form_jokin != $duty_form_hijokin) {
	if ($duty_form_hijokin == "checked") {
		//非常勤
		$cond .= " and ";
	}
	else {
		//常勤
		$cond .= " and NOT ";
	}
	$cond .= "EXISTS(SELECT * FROM empcond WHERE empcond.emp_id = empmst.emp_id AND duty_form = 2)";
}
//検索条件　グループ
if ($group_id != "" && $group_id != "-") {
	$cond .= " and exists (select *  from duty_shift_staff where empmst.emp_id = duty_shift_staff.emp_id and  group_id in (select group_id from duty_shift_group where pattern_id = $group_id))";
}
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$day1_time = $obj_hol_hour->get_specified_time_calendar();

$num = pg_num_rows($sel_emp);
for ($i=0; $i<$num; $i++) {
    $prev_use = 0;
	//今回付与データ
	$year = pg_fetch_result($sel_emp, $i, "emppaid_year");
	if ($year != "") {
		continue;
	}
	$emp_id = pg_fetch_result($sel_emp, $i, "emp_id");

    $prev_days1 = "";
    $prev_days2 = "";
    $prev_adjust_day = "";
    $wk_tbl_id = "";
    $prev_paid_hol_add_mmdd = "";
    $wk_add_year = "";
    $wk_add_mon = "";
    $wk_add_day = "";

	//前回付与データ
	$prev_year = pg_fetch_result($sel_emp, $i, "prev_year");
	if ($prev_year != "") {
		//データ作成
		$prev_days1 = pg_result($sel_emp,$i,"prev_days1");
		$prev_days2 = pg_result($sel_emp,$i,"prev_days2");
		$prev_adjust_day = pg_result($sel_emp,$i,"prev_adjust_day");
		$wk_tbl_id = pg_result($sel_emp,$i,"prev_tbl_id");
		$prev_paid_hol_add_mmdd = pg_result($sel_emp,$i,"prev_paid_hol_add_mmdd");
		$wk_add_year = $prev_year;
		$wk_add_mon = intval(substr($prev_paid_hol_add_mmdd, 0, 2));
		$wk_add_day = intval(substr($prev_paid_hol_add_mmdd, 2, 2));
		//勤続年数、3ヶ月の場合1年、それ以外は＋1年
		$wk_paid_hol_add_mmdd = $prev_paid_hol_add_mmdd;
		$prev_service_year = pg_result($sel_emp,$i,"prev_service_year");
		$prev_service_mon = pg_result($sel_emp,$i,"prev_service_mon");
        //勤続年数が未設定の場合、採用年月日から計算 20140515
        if ($prev_service_year == "" && $prev_service_mon == "") {
            $emp_join = pg_fetch_result($sel_emp,$i,"emp_join");
            if ($emp_join != "") {
                $wk_add_date = $prev_year.$prev_paid_hol_add_mmdd;
                $len_service = get_length_of_service($emp_join, $wk_add_date);
                list($prev_service_year, $prev_service_mon) = split("/", $len_service);
            }
        }

		//設定の有休付与日が4/1基準、付与年月日の月が4月、
		//勤続年数が3ヶ月か6ヶ月前回付与データがある職員、4月のデータ作成
		if ($timecard_bean->closing_paid_holiday == "1" &&
				$select_add_mon == "04" &&
				$prev_service_year == 0 &&
				($prev_service_mon == 3 || $prev_service_mon == 6)
		) {
			//4/1のデータ作成
			$wk_add_mon = 4;
			$wk_add_day = 1;

			//勤続年数1年
			$service_year = 1;
			$service_mon = 0;
		} else {
			if ($timecard_bean->closing_paid_holiday == "3" &&
                $timecard_bean->criteria_months == "1" &&
                $prev_service_year == 0 && $prev_service_mon == 3) {
				$wk_add_mon += 9;
				if ($wk_add_mon > 12) {
					$wk_add_mon -= 12;
					$wk_add_year++;
				}
				$wk_9_month_later = $wk_add_year.sprintf("%02d%02d", $wk_add_mon, $wk_add_day);
				$wk_9_month_later_yymm = $wk_add_year.sprintf("%02d", $wk_add_mon);
				if (($select_add_day != "-" && $wk_9_month_later == $wk_select_ymd) ||
						($select_add_day == "-" &&  $wk_9_month_later_yymm == $select_add_yr.$select_add_mon)) {
					;
				} else {
					continue;
				}

				$service_year = 1;
				$service_mon = 0;
			} else {
				$wk_add_year++;
				$wk_year_later = $wk_add_year.sprintf("%02d%02d", $wk_add_mon, $wk_add_day);
				$wk_year_later_yymm = $wk_add_year.sprintf("%02d", $wk_add_mon);
				if (($select_add_day != "-" && $wk_year_later == $wk_select_ymd) ||
						($select_add_day == "-" &&  $wk_year_later_yymm == $select_add_yr.$select_add_mon)) {
					;
				} else {
					continue;
				}
				$service_year = $prev_service_year + 1;
				$service_mon = $prev_service_mon;
			}
		}
		//付与月日
		$wk_paid_hol_add_mmdd = sprintf("%02d%02d", $wk_add_mon, $wk_add_day);

		//前年使用取得用日付
		$wk_start_date = $prev_year.$prev_paid_hol_add_mmdd;
		$wk_end_date = last_date($wk_add_year.$wk_paid_hol_add_mmdd);
		//移行データ確認用
		$last_add_date = pg_result($sel_emp,$i,"last_add_date");
		list($wk_yyyy, $wk_mm, $wk_dd) = split("/", $last_add_date);
		$wk_last_add_date = $wk_yyyy.sprintf("%02d%02d", $wk_mm, $wk_dd);
		//移行データがある場合
		if ($wk_start_date <= $wk_last_add_date &&
				$wk_last_add_date <= $wk_end_date) {

			//繰越未設定の場合
			if ($prev_days1 == "") {
				$prev_days1 = pg_result($sel_emp,$i,"curr_carry");
			}
			//付与未設定の場合
			if ($prev_days2 == "") {
				$prev_days2 = pg_result($sel_emp,$i,"curr_add");
			}

			$prev_use = pg_result($sel_emp,$i,"curr_use");
			$data_migration_date = pg_result($sel_emp,$i,"data_migration_date");

			//20110414 移行日の翌日からとする
			$tmp_data_migration_date = date("Ymd", strtotime("+1 day", to_timestamp($data_migration_date)));
			$wk_use = $timecard_common_class->get_nenkyu_siyo($emp_id, $tmp_data_migration_date, $wk_end_date);
			$prev_use += $wk_use;

		}
		else {
			//前年使用
			$prev_use = $timecard_common_class->get_nenkyu_siyo($emp_id, $wk_start_date, $wk_end_date);
		}
		//繰越
		//前年残＝前年繰越＋前年付与＋前年調整−有休取得数
		$prev_remain = $prev_days1 + $prev_days2 + $prev_adjust_day - $prev_use;
		//繰越
		//前年残<=前年付与の場合、前年残
		if ($prev_remain <= $prev_days2) {
			$tmp_days1 = $prev_remain;
			if ($tmp_days1 < 0) {
				$tmp_days1 = 0;
			}
		}
		//上記以外は前年付与（前々年分を除く）
		else {
			$tmp_days1 = $prev_days2;
		}
		//有休付与日が採用日の場合
		if ($timecard_bean->closing_paid_holiday == "3") {
			//勤続期間yymm形式
			$service_yymm = sprintf("%02d%02d", $service_year, $service_mon);
			$wk_kinzoku_id = 0;
			$tmp_days2 = 0;
			//付与日数、有休付与日数表から求める
			//7年後から3ヶ月まで勤続の期間を確認
			for ($wk_idx=0; $wk_idx<8; $wk_idx++) {

				$wk_hikaku_kikan = $arr_hikaku_tbl[$wk_idx];
				if ($service_yymm >= $wk_hikaku_kikan) {
					$wk_kinzoku_id = 8 - $wk_idx;
					break;
				}
			}
			if ($wk_kinzoku_id <= 0) {
				$wk_kinzoku_id = 1;
			}
		} else {
			$wk_kinzoku_id = $service_year;
			if ($wk_kinzoku_id > 8) {
				$wk_kinzoku_id = 8;
			}
		}
		$tmp_days2 = $arr_paid_hol[$wk_tbl_id][$wk_kinzoku_id];
	} else {
		$emp_join = pg_result($sel_emp,$i,"emp_join");
		//1年以上前は除く
		if ($emp_join < $wk_year_before) {
			continue;
		}

		$wk_tbl_id = pg_fetch_result($sel_emp, $i, "paid_hol_tbl_id");
		//有休表がなしの場合
		if ($wk_tbl_id == "") {
			continue;
		}

		//nヶ月基準
		$wk_month = ($timecard_bean->criteria_months == "1") ? "3" : "6";

		//採用後nヶ月の場合
		$wk_join_year = substr($emp_join, 0, 4);
		$wk_join_mon = intval(substr($emp_join, 4, 2));
		$emp_join_mon = $wk_join_mon;
		$wk_join_day = intval(substr($emp_join, 6, 2));
		$wk_join_mon += $wk_month;
		if ($wk_join_mon > 12) {
			$wk_join_mon -= 12;
			$wk_join_year++;
		}
		$wk_n_month_later = $wk_join_year.sprintf("%02d", $wk_join_mon);
		$wk_select_ym = $select_add_yr.$select_add_mon;
//年月が違う場合は作成しない 20110526
		if ($timecard_bean->closing_paid_holiday != "1" &&
			$wk_n_month_later != $wk_select_ym) {
			continue;
		}

		$service_year = 0;
		$service_mon = $wk_month;
		$tmp_days1 = 0;
		//付与日数、配列から　入職月により月別の数値にする
		//4月1日基準
		if ($timecard_bean->closing_paid_holiday == "1"){
			//月により日数を変更
			$wk_mm = intval(substr($emp_join, 4, 2));

			//付与月が4月
			if ($select_add_mon == "04") {
				//3ヶ月基準の場合、入職月が1月〜3月
				//6ヶ月基準の場合、入職月が10月〜3月
				if (($timecard_bean->criteria_months == "1" && $emp_join_mon <= 3) ||
						($timecard_bean->criteria_months == "2" &&
							(($emp_join_mon >= 1 && $emp_join_mon <= 3) ||
							($emp_join_mon >= 10 && $emp_join_mon <= 12) ))) {
					//有休付与日数表1年から日数取得
					$tmp_days2 = $arr_paid_hol[$wk_tbl_id][2]; //1始まり、2:1年
					$wk_join_mon = 4;
					$wk_join_day = 1;
					//勤続年数を1年とする
					$service_year = 1;
					$service_mon = 0;
				}
			}
			//付与月が4月以外
			else {
				//3ヶ月基準の場合、入職月が1月〜3月
				//6ヶ月基準の場合、入職月が10月〜3月
				if (($timecard_bean->criteria_months == "1" && $emp_join_mon <= 3) ||
						($timecard_bean->criteria_months == "2" &&
							(($emp_join_mon >= 1 && $emp_join_mon <= 3) ||
								($emp_join_mon >= 10 && $emp_join_mon <= 12) ))) {
					continue;
				}
				else {
					//入職年度用のテーブルから付与日数を取得
					$wk_idx = $wk_mm - 3;
					$tmp_days2 = $arr_paid_hol2[$wk_tbl_id][$wk_idx];
				}
			}

		} else {
			//上記以外（採用日基準、1月1日基準）
			$tmp_days2 = $arr_paid_hol[$wk_tbl_id][1]; //1始まり、1:3ヶ月,6ヶ月
			//1月1日基準の場合
			if ($timecard_bean->closing_paid_holiday == "2"){
				$wk_join_mon = 1;
				$wk_join_day = 1;
				$wk_add_date = $select_add_yr.sprintf("%02d%02d", $wk_join_mon ,$wk_join_day);
				if ($emp_join < $wk_add_date) {
					$len_service = get_length_of_service($emp_join, $wk_add_date);
					list($service_year, $service_mon) = split("/", $len_service);
				} else {
					$service_year = 0;
					$service_mon = 0;
				}

			}
		}
		//月末確認
		if ($wk_join_day > 28) {
			$wk_max_day = days_in_month($wk_join_year, $wk_join_mon);
			if ($wk_join_day > $wk_max_day) {
				$wk_join_day = $wk_max_day;
			}
		}
		$wk_paid_hol_add_mmdd = sprintf("%02d%02d", $wk_join_mon ,$wk_join_day);
	}

    //時間有休対応処理 20120528
    $wk_carry_time = "";
    if ($obj_hol_hour->paid_hol_hour_flag == "t") {
        $hol_hour_data_migration_date = pg_fetch_result($sel_emp, $i, "hol_hour_data_migration_date"); //データ移行日
        $hol_hour_last_carry = pg_fetch_result($sel_emp, $i, "hol_hour_last_carry"); //前年繰越
        $hol_hour_last_use = pg_fetch_result($sel_emp, $i, "hol_hour_last_use"); //前年取得
        $hol_hour_curr_carry = pg_fetch_result($sel_emp, $i, "hol_hour_curr_carry"); //当年繰越
        $hol_hour_curr_use = pg_fetch_result($sel_emp, $i, "hol_hour_curr_use"); //当年取得
        $prev_carry_time_minute = pg_fetch_result($sel_emp, $i, "prev_carry_time_minute"); //前年繰越時間

        $specified_time = pg_fetch_result($sel_emp, $i, "specified_time"); //所定時間

        if ($specified_time == "") {
            // 所定労働時間履歴 20121129
            $arr_timehist = $calendar_name_class->get_calendarname_time($select_add_yr.$wk_paid_hol_add_mmdd);
            $day1_time = $arr_timehist["day1_time"];
            $specified_time = $day1_time;
        }
        //hhmm形式から時分にする
        $wk_hh = substr($specified_time, 0, 2);
        $wk_mm = substr($specified_time, 2, 2);
        //上限日数がある場合に、切り上げとする
        if ($obj_hol_hour->paid_hol_hour_max_flag == "t" && $wk_mm != "00") {
            //切り上げ
            $wk_specified_minute = (intval($wk_hh) + 1) * 60;
        }
        else {
            $wk_specified_minute = intval($wk_hh) * 60 + intval($wk_mm);
        }
        $wk_specified_time = $wk_specified_minute / 60;
        //繰越
        if ($prev_carry_time_minute != "") {
            $wk_carry_time = $prev_carry_time_minute / 60;
        }
        else {
            $wk_carry_time = 0;
        }
        //前回付与データがない場合の日付設定（時間有休移行データだけがあるケース。念のための設定）
        if ($prev_year == "") {
            //前年使用取得用日付
            $wk_start_date = ($select_add_yr-1).$wk_paid_hol_add_mmdd;
            $wk_end_date = last_date($select_add_yr.$wk_paid_hol_add_mmdd);
        }
        $wk_prev_start_date = $wk_start_date;
        $wk_prev_end_date = $wk_end_date;
        //期間確認は、今回付与しようという日付を使用
        $wk_start_date = $select_add_yr.$wk_paid_hol_add_mmdd;
        $wk_chk_start_date = date("Ymd", strtotime("-1 year", to_timestamp($wk_start_date))); //付与日の前の移行日対応 20141104
        $wk_end_date = last_date(($select_add_yr+1).$wk_paid_hol_add_mmdd);
        //移行データがある場合
        if ($hol_hour_data_migration_date != "" &&
                $wk_chk_start_date <= $hol_hour_data_migration_date && //付与日の前の移行日対応 20141104
                $hol_hour_data_migration_date <= $wk_end_date) {

            //使用数
            $tmp_siyo_su_minute = $obj_hol_hour->hh_or_hhmm_to_minute($hol_hour_curr_use);
            $tmp_siyo_su_hour = $tmp_siyo_su_minute / 60;
            //$tmp_siyo_su_hour = $hol_hour_curr_use;
            //移行後の使用数、移行日の翌日からとする
            $tmp_data_migration_date = date("Ymd", strtotime("+1 day", to_timestamp($hol_hour_data_migration_date)));
            $wk_hour = $obj_hol_hour->get_paid_hol_day($emp_id, $tmp_data_migration_date, $wk_end_date, "");
            $tmp_siyo_su_hour += ($wk_hour / 60);
			//使用数がある場合、繰越数を再計算する 20141104
			$wk_carry_time = 0;
			if ($tmp_siyo_su_hour > 0) {
	            //残数 = (繰越 + 付与 - 取得) 時間を考慮して計算
	            $kurikosi_fuyo_day = $prev_days1 + $prev_days2;

	            list($wk_zan_day, $wk_zan_hour, $wk_day_kansan ) = day_hour_subtract($kurikosi_fuyo_day, $wk_carry_time, $prev_use, $tmp_siyo_su_hour, $wk_specified_time);
	            //繰越の確認 残日数>付与日数
	            if ($wk_day_kansan > $prev_days2) {
	                $tmp_days1 = $prev_days2; //付与日数
	                $wk_carry_time = 0;
	            }
	            else {
	                $tmp_days1 = $wk_zan_day; //残日数
	                $wk_carry_time = round($wk_zan_hour * 60);
	            }
			}
			else {
	            //繰越
	            if ($hol_hour_curr_carry != "") {
	                //分換算で計算 20141014
	                $hol_hour_curr_carry_minute = $obj_hol_hour->hh_or_hhmm_to_minute($hol_hour_curr_carry);
	                //所定労働時間以上の場合、日数換算
	                if ($hol_hour_curr_carry_minute >= $wk_specified_minute) {
	                    $wk_day = intval($hol_hour_curr_carry_minute / $wk_specified_minute);
	                    $tmp_days1 += $wk_day;
	                    $wk_carry_time = ($hol_hour_curr_carry_minute - ($wk_specified_minute * $wk_day));
	                }
	                else {
	                    $wk_carry_time = $hol_hour_curr_carry_minute;
	                }
	            }
	            //繰越の確認 残日数>=付与日数 20150501
	            if ($tmp_days1 >= $prev_days2) {
	                $tmp_days1 = $prev_days2; //付与日数
	                $wk_carry_time = 0;
	            }
			}
        }
        else {
            //前年使用、期間確認
            $wk_hour = $obj_hol_hour->get_paid_hol_day($emp_id, $wk_prev_start_date, $wk_prev_end_date, "");
            $tmp_siyo_su_hour = $wk_hour / 60;

            //残数 = (繰越 + 付与 - 取得) 時間を考慮して計算
            $kurikosi_fuyo_day = $prev_days1 + $prev_days2;

            list($wk_zan_day, $wk_zan_hour, $wk_day_kansan ) = day_hour_subtract($kurikosi_fuyo_day, $wk_carry_time, $prev_use, $tmp_siyo_su_hour, $wk_specified_time);
            //繰越の確認 残日数>付与日数
            if ($wk_day_kansan > $prev_days2) {
                $tmp_days1 = $prev_days2; //付与日数
                $wk_carry_time = 0;
            }
            else {
                $tmp_days1 = $wk_zan_day; //残日数
                $wk_carry_time = round($wk_zan_hour * 60);
            }

        }
        //繰越方法
        if ($wk_carry_time > 0) {
            switch ($obj_hol_hour->paid_hol_hour_carry_flg) {
                case "2": //そのまま繰越
                    break;
                case "3": //切捨て
                    $wk_carry_time = 0;
                    break;
                case "4": //半日切上げ
                    //半日時間（分）
                    $half_time_min = $wk_specified_time * 60 / 2;
                    //$wk_carry_time（分換算）
                    if ($wk_carry_time > $half_time_min) {
                        $tmp_days1 += 1;
                        $wk_carry_time = 0;
                    }
                    else if ($wk_carry_time > 0) {
                        $tmp_days1 += 0.5;
                        $wk_carry_time = 0;
                    }
                    break;
                case "1": //切上げ
                default:
                    $tmp_days1 += 1;
                    $wk_carry_time = 0;
                    break;

            }
        }

    }
    //時間有休繰越
    $sql = "insert into emppaid (emp_id, year, days1, days2, paid_hol_tbl_id, paid_hol_add_mmdd, service_year, service_mon, carry_time_minute) values (";
    $content = array($emp_id, $select_add_yr, $tmp_days1, $tmp_days2, $wk_tbl_id, $wk_paid_hol_add_mmdd, $service_year, $service_mon, $wk_carry_time);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

$url_srch_name = urlencode($srch_name);

// 有休管理画面を再表示 検索条件を追加
echo("<script type=\"text/javascript\">location.href = 'work_admin_paid_holiday.php?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&group_id=$group_id&srch_flg=$srch_flg&page=$page&select_add_yr=$select_add_yr&select_add_mon=$select_add_mon&select_add_day=$select_add_day&select_close_yr=$select_close_yr&select_close_mon=$select_close_mon&select_close_day=$select_close_day&srch_id=$srch_id';</script>");

?>