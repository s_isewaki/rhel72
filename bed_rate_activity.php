<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜病床稼動率（区分別）</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("show_calc_bed_by_type.ini");
require("get_values.ini");
require("get_ward_div.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// デフォルト値の設定
if ($mode == "") {
	$from_date = date("Ymd", strtotime("+1 day", strtotime("-1 year")));
	$to_date = date("Ymd");

	$from_yr = substr($from_date, 0, 4);
	$from_mon = substr($from_date, 4, 2);
	$from_day = substr($from_date, 6, 2);
	$to_yr = substr($to_date, 0, 4);
	$to_mon = substr($to_date, 4, 2);
	$to_day = substr($to_date, 6, 2);
	$omit_types = array("3", "4", "5", "6", "7");
}

// 入力チェック
if (!checkdate($from_mon, $from_day, $from_yr)) {
	echo("<script language=\"javascript\">alert(\"From日付が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if (!checkdate($to_mon, $to_day, $to_yr)) {
	echo("<script language=\"javascript\">alert(\"To日付が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
$from_date = sprintf("%04d%02d%02d", $from_yr, $from_mon, $from_day);
$to_date = sprintf("%04d%02d%02d", $to_yr, $to_mon, $to_day);
if ($from_date > $to_date) {
	echo("<script language=\"javascript\">alert(\"期間が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// ベット数除外対象のURLクエリ文字列を作成
$omit_query = "";
foreach ($omit_types as $omit_type) {
	$omit_query .= "&omit_types[]=$omit_type";
}

// DBへのコネクション作成
$con = connect2db($fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#5279a5"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床利用率管理</b></font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="34%" height="22" align="center">
<a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率</font></a>
</td>
<td width="33%" align="center">
<a href="bed_rate_average.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均病床利用率</font></a>
</td>
<td width="34%" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床稼動率</font>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- 期間 -->
<form action="bed_rate_activity.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
期間：<select name="from_yr">
<? show_select_years(3, $from_yr); ?>
</select>/<select name="from_mon">
<? show_select_months($from_mon); ?>
</select>/<select name="from_day">
<? show_select_days($from_day); ?>
</select>〜<select name="to_yr">
<? show_select_years(3, $to_yr); ?>
</select>/<select name="to_mon">
<? show_select_months($to_mon); ?>
</select>/<select name="to_day">
<? show_select_days($to_day); ?>
</select>
<img src="img/spacer.gif" alt="" width="5" height="1">
ベット数除外対象：<input type="checkbox" name="omit_types[]" value="3"<? if (in_array("3", $omit_types)) {echo(" checked");} ?>>回復室
<input type="checkbox" name="omit_types[]" value="4"<? if (in_array("4", $omit_types)) {echo(" checked");} ?>>ICU
<input type="checkbox" name="omit_types[]" value="5"<? if (in_array("5", $omit_types)) {echo(" checked");} ?>>HCU
<input type="checkbox" name="omit_types[]" value="6"<? if (in_array("6", $omit_types)) {echo(" checked");} ?>>NICU
<input type="checkbox" name="omit_types[]" value="7"<? if (in_array("7", $omit_types)) {echo(" checked");} ?>>GCU
<img src="img/spacer.gif" alt="" width="5" height="1">
<input type="submit" value="計算">
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="mode" value="calc2">
</form>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<?
if ($mode == "calc2") {
?>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100" height="22" align="center" bgcolor="#5279a5"><a href="bed_rate_activity.php?session=<? echo($session); ?>&mode=calc2&from_yr=<? echo($from_yr); ?>&from_mon=<? echo($from_mon); ?>&from_day=<? echo($from_day); ?>&to_yr=<? echo($to_yr); ?>&to_mon=<? echo($to_mon); ?>&to_day=<? echo($to_day); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>区分別</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_rate_activity_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟別</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<?
	show_bed_active_rate($con, $from_date, $to_date, $session, $fname, $omit_types);
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// 病床稼動率の出力
//------------------------------------------------------------------------------
function show_bed_active_rate($con, $from_date, $to_date, $session, $fname, $omit_types) {

	$arr_ward_div = get_ward_div($con, $fname);
	$arr_rate = get_arr_bed_active_rate($con, $from_date, $to_date, $fname, $omit_types);
?>
<!-- 病床稼働率 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#f6f9ff">
<td height="22">&nbsp;</td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<?
	for ($i = 0; $i < count($arr_rate); $i++) {
?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["title"]); ?></font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["1"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["2"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["3"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["4"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["5"]); ?>%</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_rate[$i]["ALL"]); ?>%</font></td>
</tr>
<?
	}
?>
</table>
<?
}
?>
