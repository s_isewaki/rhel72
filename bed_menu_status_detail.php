<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜病棟状況一覧表</title>
<?
require_once("about_authority.php");
require_once("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 棟名の取得
$sql = "select bldg_name from bldgmst";
$cond = "where bldg_cd = $bldg_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_name = pg_fetch_result($sel, 0, "bldg_name");

// 病棟名の取得
$sql = "select ward_name from wdmst";
$cond = "where bldg_cd = $bldg_cd and ward_cd = $ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ward_name = pg_fetch_result($sel, 0, "ward_name");

// 並び順のデフォルトは患者氏名の昇順
if ($order == "") {
	$order = "1";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.list {border-collapse:collapse;}
table.list td {border:solid #5279a5 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟状況一覧表</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院患者検索</font></a></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
病棟：<? echo($bldg_name); ?> <? echo($ward_name); ?>
<img src="img/spacer.gif" width="10" height="1" alt="">
日時：<? echo($year); ?>/<? echo($month); ?>/<? echo($day); ?>&nbsp;<? echo($hour); ?>：<? echo($minute); ?>
<img src="img/spacer.gif" width="80" height="1" alt="">
<?
switch ($type) {
case "1":  // 在院患者
	echo("在院患者");
	break;
case "2":  // 入院予定患者
	echo("入院予定患者");
	break;
case "3":  // 退院予定患者
	echo("退院予定患者");
	break;
case "4":  // 一週間以内入院予定患者
	echo("一週間以内入院予定患者");
	break;
case "5":  // 一週間以内退院予定患者
	echo("一週間以内退院予定患者");
	break;
case "6":  // 外泊患者
	echo("外泊患者");
	break;
case "7":  // 外出患者
	echo("外出患者");
	break;
case "8":  // 担送患者
	echo("担送患者");
	break;
case "9":  // 護送患者
	echo("護送患者");
	break;
case "10":  // 独歩患者
	echo("独歩患者");
	break;
case "11":  // 家族付き添いあり患者
	echo("家族付き添いあり患者");
	break;
case "12":  // 人工R器使用患者
	echo("人工R器使用患者");
	break;
}
?>
</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&day=<? echo($day); ?>&hour=<? echo($hour); ?>&minute=<? echo($minute); ?>&session=<? echo($session); ?>">病棟一覧に戻る</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<? show_patient_list($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $type, $order, $session, $fname); ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_patient_list($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $type, $order, $session, $fname) {

	// 患者一覧を取得
	switch ($type) {
	case "1":  // 在院患者
		$patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "2":  // 入院予定患者
		$patients = get_in_res_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "3":  // 退院予定患者
		$patients = get_out_res_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "4":  // 一週間以内入院予定患者
		$patients = get_in_res_patients_week($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "5":  // 一週間以内退院予定患者
		$patients = get_out_res_patients_week($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "6":  // 外泊患者
		$patients = get_go_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "7":  // 外出患者
		$patients = get_stop_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "8":  // 担送患者
		$patients = get_move1_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "9":  // 護送患者
		$patients = get_move2_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "10":  // 独歩患者
		$patients = get_move3_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "11":  // 家族付き添いあり患者
		$patients = get_family_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "12":  // 人工R器使用患者
		$patients = get_ventilator_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);
		break;
	case "13":  // 転入予定患者
		$patients = get_move_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $session, $fname);
		break;
	case "14":  // 転出予定患者
		$patients = get_move_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $session, $fname);
		break;
	}

	// 指定された並び順でソート
	switch ($order) {
	case "1":  // 名前の昇順
		uasort($patients, "sort_pateints_order_by_name");
		break;
	case "2":  // 名前の降順
		uasort($patients, "sort_pateints_order_by_name_desc");
		break;
	case "3":  // 病室の昇順
		uasort($patients, "sort_pateints_order_by_room");
		break;
	case "4":  // 病室の降順
		uasort($patients, "sort_pateints_order_by_room_desc");
		break;
	}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? $tmp_order = ($order == "1") ? "2" : "1"; ?>
<a href="bed_menu_status_detail.php?bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&day=<? echo($day); ?>&hour=<? echo($hour); ?>&minute=<? echo($minute); ?>&session=<? echo($session); ?>&type=<? echo($type); ?>&order=<? echo($tmp_order); ?>">患者氏名</a>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? $tmp_order = ($order == "3") ? "4" : "3"; ?>
<a href="bed_menu_status_detail.php?bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&day=<? echo($day); ?>&hour=<? echo($hour); ?>&minute=<? echo($minute); ?>&session=<? echo($session); ?>&type=<? echo($type); ?>&order=<? echo($tmp_order); ?>">病室</a>
</font></td>
<? if ($type == "11") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族付き添い</font></td>
<? } ?>
</tr>
<?
foreach ($patients as $tmp_ptif_id => $tmp_ptif) {
?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif_id); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif["name"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(get_age($tmp_ptif["birth"])); ?>歳</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif["sect_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif["ptrm_name"]); ?></font></td>
<? if ($type == "11") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_ptif["family"]); ?>名</font></td>
<? } ?>
</tr>
<?
}
?>
</table>
<?
}

// 在院患者一覧を取得
function get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 診療科一覧を配列に格納
	$sql = "select sect_id, sect_nm from sectmst";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sects = array();
	while ($row = pg_fetch_array($sel)) {
		$sects[$row["sect_id"]] = $row["sect_nm"];
	}

	// 病室一覧を配列に格納
	$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name from ptrmmst";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rooms = array();
	while ($row = pg_fetch_array($sel)) {
		$rooms[$row["bldg_cd"] . "-" .$row["ward_cd"] . "-" . $row["ptrm_room_no"]] = $row["ptrm_name"];
	}

	// 指定日に在院していた患者一覧を取得
	$sql = " select inptmst.ptif_id, inptmst.bldg_cd, inptmst.ward_cd, inptmst.ptrm_room_no, inptmst.inpt_in_dt, inptmst.inpt_in_tm, inptmst.inpt_out_dt, inptmst.inpt_out_tm, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, inptmst.inpt_sect_id from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id where inpt_in_dt is not null and ((inpt_in_dt = '$year$month$day' and inpt_in_tm <= '$hour$minute') or (inpt_in_dt < '$year$month$day')) ";
	$sql .= " union select inpthist.ptif_id, inpthist.bldg_cd, inpthist.ward_cd, inpthist.ptrm_room_no,inpthist.inpt_in_dt, inpthist.inpt_in_tm, inpthist.inpt_out_dt, inpthist.inpt_out_tm, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, inpthist.inpt_sect_id from inpthist inner join ptifmst on ptifmst.ptif_id = inpthist.ptif_id where ((inpt_in_dt = '$year$month$day' and inpt_in_tm <= '$hour$minute') or (inpt_in_dt < '$year$month$day')) and ((inpt_out_dt = '$year$month$day' and inpt_out_tm >= '$hour$minute') or (inpt_out_dt > '$year$month$day')) ";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 当日に当該棟に在院していた患者を配列に格納
	$in_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_ptif_id = $row["ptif_id"];
		$tmp_in_dt = $row["inpt_in_dt"];
		$tmp_in_tm = $row["inpt_in_tm"];
		$tmp_out_dt = $row["inpt_out_dt"];
		$tmp_out_tm = $row["inpt_out_tm"];

		// 当該患者の指定日直近の確定済み移転情報を取得
		$sql = "select * from inptmove";
		$cond = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg = 't' and ((move_dt = '$tmp_in_dt' and move_tm >= '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$year$month$day' and move_tm >= '$hour$minute') or (move_dt > '$year$month$day'))";
		if ($tmp_out_dt != "" && $tmp_out_tm != "") {
			$cond .= " and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt'))";
		}
		$cond .= " order by move_dt, move_tm limit 1";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 病室を確定
		if (pg_num_rows($sel2) == 0) {
			$tmp_sect_id = $row["inpt_sect_id"];
			$tmp_bldg_cd = $row["bldg_cd"];
			$tmp_ward_cd = $row["ward_cd"];
			$tmp_ptrm_room_no = $row["ptrm_room_no"];
		} else {
			$tmp_sect_id = pg_fetch_result($sel2, 0, "from_sect_id");
			$tmp_bldg_cd = pg_fetch_result($sel2, 0, "from_bldg_cd");
			$tmp_ward_cd = pg_fetch_result($sel2, 0, "from_ward_cd");
			$tmp_ptrm_room_no = pg_fetch_result($sel2, 0, "from_ptrm_room_no");
		}

		// 病棟が違う場合は次の患者へ
		if ($tmp_bldg_cd != $bldg_cd || $tmp_ward_cd != $ward_cd) {
			continue;
		}

		$in_patients[$tmp_ptif_id] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"keyword" => $row["ptif_keywd"],
			"birth" => $row["ptif_birth"],
			"sect_nm" => $sects[$tmp_sect_id],
			"ptrm_name" => $rooms[$tmp_bldg_cd . "-" . $tmp_ward_cd . "-" . $tmp_ptrm_room_no]
		);
	}

	return $in_patients;
}

// 入院予定患者一覧を取得
function get_in_res_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {
	$sql = " select inptres.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, sectmst.sect_nm, ptrmmst.ptrm_name from inptres inner join ptifmst on ptifmst.ptif_id = inptres.ptif_id left join sectmst on sectmst.sect_id = inptres.inpt_sect_id left join ptrmmst on ptrmmst.bldg_cd = inptres.bldg_cd and ptrmmst.ward_cd = inptres.ward_cd and ptrmmst.ptrm_room_no = inptres.ptrm_room_no";
	$cond = "where inptres.bldg_cd = $bldg_cd and inptres.ward_cd = $ward_cd and inptres.inpt_in_res_dt = '$year$month$day'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$in_res_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$in_res_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"birth" => $row["ptif_birth"],
			"keyword" => $row["ptif_keywd"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $in_res_patients;
}

// 退院予定患者一覧を取得
function get_out_res_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {
	$sql = " select inptmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, sectmst.sect_nm, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id left join sectmst on sectmst.sect_id = inptmst.inpt_sect_id inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
	$cond = "where inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd and inptmst.inpt_out_res_dt = '$year$month$day'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$out_res_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$out_res_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"keyword" => $row["ptif_keywd"],
			"birth" => $row["ptif_birth"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $out_res_patients;
}

// 一週間以内入院予定患者一覧を取得
function get_in_res_patients_week($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {
	$six_days_after = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	$sql = " select inptres.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_birth, ptifmst.ptif_keywd, sectmst.sect_nm, ptrmmst.ptrm_name from inptres inner join ptifmst on ptifmst.ptif_id = inptres.ptif_id left join sectmst on sectmst.sect_id = inptres.inpt_sect_id left join ptrmmst on ptrmmst.bldg_cd = inptres.bldg_cd and ptrmmst.ward_cd = inptres.ward_cd and ptrmmst.ptrm_room_no = inptres.ptrm_room_no";
	$cond = "where inptres.bldg_cd = $bldg_cd and inptres.ward_cd = $ward_cd and inptres.inpt_in_res_dt between '$year$month$day' and '$six_days_after'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$in_res_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$in_res_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"keyword" => $row["ptif_keywd"],
			"birth" => $row["ptif_birth"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $in_res_patients;
}

// 一週間以内退院予定患者一覧を取得
function get_out_res_patients_week($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {
	$six_days_after = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	$sql = " select inptmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, sectmst.sect_nm, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id left join sectmst on sectmst.sect_id = inptmst.inpt_sect_id inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
	$cond = "where inptmst.bldg_cd = $bldg_cd and inptmst.ward_cd = $ward_cd and inptmst.inpt_out_res_dt between '$year$month$day' and '$six_days_after'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$out_res_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$out_res_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"keyword" => $row["ptif_keywd"],
			"birth" => $row["ptif_birth"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $out_res_patients;
}

// 外泊患者一覧を取得
function get_go_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 外泊中の患者に絞り込み
	$go_out_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '2' and out_date <= '$year$month$day' and ret_date >= '$year$month$day'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$go_out_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $go_out_patients;
}

// 外出患者一覧を取得
function get_stop_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 外出中の患者に絞り込み
	$stop_out_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select count(*) from inptgoout";
		$cond = "where ptif_id = '$tmp_ptif_id' and out_type = '1' and out_date = '$year$month$day' and out_time <= '$hour$minute' and ret_time >= '$hour$minute'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
			$stop_out_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $stop_out_patients;
}

// 担送患者一覧を取得
function get_move1_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 担送患者に絞り込み
	$move1_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select ptsubif_move5 from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, "ptsubif_move5") == "t") {
			$move1_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $move1_patients;
}

// 護送患者一覧を取得
function get_move2_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 護送患者に絞り込み
	$move2_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select ptsubif_move1, ptsubif_move2, ptsubif_move3, ptsubif_move4 from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, "ptsubif_move1") == "t" ||
		    pg_fetch_result($sel, 0, "ptsubif_move2") == "t" ||
		    pg_fetch_result($sel, 0, "ptsubif_move3") == "t" ||
		    pg_fetch_result($sel, 0, "ptsubif_move4") == "t"
		) {
			$move2_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $move2_patients;
}

// 独歩患者一覧を取得
function get_move3_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 独歩患者に絞り込み
	$move3_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select ptsubif_move_flg from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, "ptsubif_move_flg") == "t") {
			$move3_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $move3_patients;
}

// 家族付き添いあり患者一覧を取得
function get_family_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 家族付き添いあり患者に絞り込み
	$family_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select sum(head_count) from inptfamily";
		$cond = "where ptif_id = '$tmp_ptif_id' and from_date <= '$year$month$day' and to_date >= '$year$month$day'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_family_count = intval(pg_fetch_result($sel, 0, 0));
		if ($tmp_family_count > 0) {
			$tmp_patient["family"] = $tmp_family_count;
			$family_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $family_patients;
}

// 人工R器使用患者一覧を取得
function get_ventilator_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname) {

	// 在院患者一覧を取得
	$in_patients = get_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $hour, $minute, $session, $fname);

	// 人工R器使用患者に絞り込み
	$ventilator_patients = array();
	foreach ($in_patients as $tmp_ptif_id => $tmp_patient) {
		$sql = "select ptsubif_ventilator from ptsubif";
		$cond = "where ptif_id = '$tmp_ptif_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_fetch_result($sel, 0, "ptsubif_ventilator") == "t") {
			$ventilator_patients[$tmp_ptif_id] = $tmp_patient;
		}
	}

	return $ventilator_patients;
}

// 転入予定患者一覧を取得
function get_move_in_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $session, $fname) {
	$sql = "select inptmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, sectmst.sect_nm, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id left join sectmst on sectmst.sect_id = inptmst.inpt_sect_id inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
	$cond = "where exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.to_bldg_cd = $bldg_cd and inptmove.to_ward_cd = $ward_cd and inptmove.move_dt = '$year$month$day' and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f' and (inptmove.to_bldg_cd <> inptmove.from_bldg_cd or inptmove.to_ward_cd <> inptmove.from_ward_cd))";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$move_in_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$move_in_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"birth" => $row["ptif_birth"],
			"keyword" => $row["ptif_keywd"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $move_in_patients;
}

// 転出予定患者一覧を取得
function get_move_out_patients($con, $bldg_cd, $ward_cd, $year, $month, $day, $session, $fname) {
	$sql = "select inptmst.ptif_id, ptifmst.ptif_lt_kaj_nm, ptifmst.ptif_ft_kaj_nm, ptifmst.ptif_keywd, ptifmst.ptif_birth, sectmst.sect_nm, ptrmmst.ptrm_name from inptmst inner join ptifmst on ptifmst.ptif_id = inptmst.ptif_id left join sectmst on sectmst.sect_id = inptmst.inpt_sect_id inner join ptrmmst on ptrmmst.bldg_cd = inptmst.bldg_cd and ptrmmst.ward_cd = inptmst.ward_cd and ptrmmst.ptrm_room_no = inptmst.ptrm_room_no";
	$cond = "where exists (select * from inptmove where inptmove.ptif_id = inptmst.ptif_id and inptmove.from_bldg_cd = $bldg_cd and inptmove.from_ward_cd = $ward_cd and inptmove.move_dt = '$year$month$day' and inptmove.move_cfm_flg = 'f' and inptmove.move_del_flg = 'f' and (inptmove.to_bldg_cd <> inptmove.from_bldg_cd or inptmove.to_ward_cd <> inptmove.from_ward_cd))";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$move_out_patients = array();
	while ($row = pg_fetch_array($sel)) {
		$move_out_patients[$row["ptif_id"]] = array(
			"name" => $row["ptif_lt_kaj_nm"] . " " . $row["ptif_ft_kaj_nm"],
			"birth" => $row["ptif_birth"],
			"keyword" => $row["ptif_keywd"],
			"sect_nm" => $row["sect_nm"],
			"ptrm_name" => $row["ptrm_name"]
		);
	}

	return $move_out_patients;
}

// 名前の昇順でソート
function sort_pateints_order_by_name($p1, $p2) {
	return strcmp($p1["keyword"], $p2["keyword"]);
}

// 名前の降順でソート
function sort_pateints_order_by_name_desc($p1, $p2) {
	return sort_pateints_order_by_name($p1, $p2) * -1;
}

// 病室の昇順でソート
function sort_pateints_order_by_room($p1, $p2) {
	return strcmp($p1["ptrm_name"], $p2["ptrm_name"]);
}

// 病室の降順でソート
function sort_pateints_order_by_room_desc($p1, $p2) {
	return sort_pateints_order_by_room($p1, $p2) * -1;
}

// 生年月日より年齢を算出
function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
