<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");
require_once("Cmx.php");
require_once("aclg_set.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//**********DB処理**********
//----------ＤＢのコネクション作成----------
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


//----------正規表現チェック----------
	if($place_name == ""){
		echo("<script language=\"javascript\">alert(\"行先名が入力されていません。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	if(strlen($place_name) > 40){
		echo("<script language=\"javascript\">alert(\"行先名が長すぎます。\");</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

//----------Transaction begin----------
pg_exec($con,"begin transaction");

//----------scheduleplaceからevent_idを取得----------
$cond = "";
$sel = select_from_table($con,$SQL154,$cond,$fname);			//max値を取得

	if($sel==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$max = pg_result($sel,0,"max");

	if($max == ""){
		$place_id = "1";
	}else{
		$place_id = $max+1;
	}

	$content = array($place_id,$place_name,f);

	$in_place = insert_into_table($con,$SQL155,$content,$fname);
	if($in_place==0){
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	pg_exec($con, "commit");
	pg_close($con);
	echo("<script language='javascript'>location.href=\"./schedule_place_list.php?session=$session\";</script>");
	exit;
?>