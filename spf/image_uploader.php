<?
//****************************************************************************************************************
// 画像、資料ファイルアップローダ、およびそのためのダイアログ
//
// 資格写真書類などをアップロードする際に利用。 「ユーザ画面」からのみ呼ばれる。
// アップロードしたファイルはspft_imageテーブルに格納される。物理ファイルは存在しない。
//
// よって、アップロードしたファイルにアクセスするには、特殊なURLでないと取得できない。
//
// また、SPFでは、「誰でも閲覧可能な」URLは、2015/9時点では、提供していない。
// 必ずそのファイルに対する「閲覧権限」が設定されていることが必要なようになっている。
// 
//
// 【考え方の概要】
// １）もし、画面で、画像や資料に対する「ハイパーリンク」が表示される場合は、
//     当全、ファイルに対する権限があり、「ダウンロード可能」ということである。
// ２）SPFシステムでは、「このリンクURL」に細工がしてある。
//     URL自体に「ダウンロード可能時間」が組み込まれており、（デフォルトだと、ページを開いて15分後の時刻）
//     この時間内にダウンロードしないとNGなリンクとなっている。
// ３）つまり、リロードするたびに、URLが変わる。
// ４）閲覧者は、このリンクを手動で生成することはできない。
//     ※ソースを解析されれば生成できるが、ソースを解析しても生成できない構成にするための「cmx_crypt」は、
//     メンテ可能な人間がいないため、廃止した。
//
// このあたりは「get_image_id」というキーワードで、「common.php」と、「.htaccess」を調査してください。
//
//****************************************************************************************************************
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にspf権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行


$today_ymdhm = date("YmdHi");


//****************************************************************************************************************
// 画像の保存
//****************************************************************************************************************
$errmsg = "";
if ($_REQUEST["save_image"]) {
    do {
        // スイープ
        $sql =
        " delete from spft_image".
        " where upload_ymdhm < '".date("YmdHi", strtotime("-2 hour"))."'". // 2時間前は消す
        " and is_related <> '1'"; // 正規利用されていない
        c2dbExec($sql);

        $image_id = $_REQUEST["image_id"];
        $new_image_id = $_REQUEST["new_image_id"];
        $emp_id = $_REQUEST["emp_id"];
        if (!$image_id) { $errmsg = "不正なアクセスです。(image_idなし)"; break; }
        if (!$emp_id)   { $errmsg = "ng不正なアクセスです。(emp_idなし)"; break; }

        $tmp_name = @$_FILES["upload_file"]["tmp_name"];
        $image_mime = @$_FILES["upload_file"]["type"];
        $original_file_name = @$_FILES["upload_file"]["name"];
        if (!$tmp_name) { $errmsg = "ファイルを指定してください。"; break; }
        $size = @$_FILES["upload_file"]["size"];
        if (!$size) { $errmsg = "空ファイルを指定したか、ファイルが不正です。"; break; }

        $image_base64 = @base64_encode(file_get_contents($tmp_name));

        if (!$new_image_id) {
            $new_image_id = 1 + c2dbGetOne("select nextval('spft_image_seq')");
            $sql =
            " insert into spft_image (".
            "     image_id, original_file_name, upload_ymdhm, is_related, image_base64, update_emp_id, update_emp_nm, image_mime".
            " ) values (".
            " ".$new_image_id.
            ",".c2dbStr($original_file_name).
            ",'".$today_ymdhm."'".
            ",'0'".// is_related
            ",".c2dbStr($image_base64). // image_base64 画像データ
            ",".c2dbStr(C2_LOGIN_EMP_ID).
            ",".c2dbStr(C2_LOGIN_EMP_LT_NM." ".C2_LOGIN_EMP_FT_NM).
            ",".c2dbStr($image_mime).
            " )";
        } else {
            $sql =
            " update spft_image set".
            " upload_ymdhm = '".$today_ymdhm."'".
            ",is_related = '0'".
            ",original_file_name = ".c2dbStr($original_file_name).
            ",image_base64 = ".c2dbStr($image_base64). // image_base64 画像データ
            ",update_emp_id = ".c2dbStr(C2_LOGIN_EMP_ID).
            ",update_emp_nm = ".c2dbStr(C2_LOGIN_EMP_LT_NM." ".C2_LOGIN_EMP_FT_NM).
            ",image_mime = ".c2dbStr($image_mime).
            " where image_id = ".$new_image_id;
        }
        c2dbExec($sql);
        header("Location: image_uploader.php?image_id=".$image_id."&new_image_id=".$new_image_id."&emp_id=".$emp_id."&iu_callback_func=".$_REQUEST["iu_callback_func"]);
        die;
    } while(0);
}




//****************************************************************************************************************
// 入力パーツ群HTMLを作成（中または小分類の場合）
//****************************************************************************************************************
    $emp_id = $_REQUEST["emp_id"];
    if (!$emp_id) { echo "ng不正なアクセスです。(emp_idなし)"; die; }

    $image_id = $_REQUEST["image_id"];
    if (!strlen($image_id)) { echo "ng不正なアクセスです。(image_idなし)"; die; }

    if (!strlen($_REQUEST["iu_callback_func"])) { echo "ng不正なアクセスです。(iu_callback_funcなし)"; die; }

    $new_image_id = $_REQUEST["new_image_id"];
    $imageRow    = c2dbGetTopRow("select * from spft_image where image_id = ".(int)$image_id);
    $newImageRow = c2dbGetTopRow("select * from spft_image where image_id = ".(int)$new_image_id);

//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<?require_once(dirname(__FILE__)."/js/dynamic.inc");?>
<script type="text/JavaScript">
$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;
function imageSelected(mode) {
    if (mode=="clear") {
        window.parent.<?=$_REQUEST["iu_callback_func"]?>("", "");
        return;
    }
    if (mode=="origin") {
	    var file_name = "<?=c2js($imageRow["original_file_name"])?>";
	    var image_id = "<?=hh($image_id)?>";
	    <? $dlInfo = genDownloadTicket($image_id, $imageRow["original_file_name"]); ?>
	    window.parent.<?=$_REQUEST["iu_callback_func"]?>(image_id, file_name, "<?=$dlInfo["rel_url"]?>");
        return;
    }
    var original_file_name = "<?=c2js($newImageRow["original_file_name"])?>";
    var new_image_id = "<?=hh($new_image_id)?>";
    <? $dlInfo = genDownloadTicket($new_image_id, $newImageRow["original_file_name"]); ?>
    window.parent.<?=$_REQUEST["iu_callback_func"]?>(new_image_id, original_file_name, "<?=$dlInfo["rel_url"]?>");
}
</script>
</head>
<body><div id="CLIENT_FRAME_CONTENT" pageBottomMargin="0" parentBodyOverflowY="hidden" thisBodyOverflowY="hidden" thisBodyOverflowX="hidden">


<div style="padding-top:10px">
<table cellspacing="0" cellpadding="0" class="width100 list_table">
<tr style="height:26px">
    <th class="w150">現在のファイル</th>
    <td>
        <?
            if ($imageRow["original_file_name"]) {
                $dlInfo = genDownloadTicket($image_id, $imageRow["original_file_name"]);
        ?>
        <a class="with_icon" href="./<?=$dlInfo["rel_url"]?>" target="_blank">
            <img src="./img/spacer.gif" width="20" height="16" alt="" class="download_link" />
            <div><nobr><?=hh($imageRow["original_file_name"])?></nobr></div>
        </a>
        <div style="padding-top:5px"><button type="button" onclick="imageSelected('origin')" class="w70">ＯＫ</button></div>
        <? } ?>
    </td>
</tr>
<tr id="tr_upload1" style="<?=($newImageRow?"display:none":"")?>">
    <th>アップロード</th>
    <td>
        <form name="frm" action="image_uploader.php?image_id=<?=$image_id?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="emp_id" value="<?=hh($_REQUEST["emp_id"])?>" />
            <input type="hidden" name="save_image" value="1" />
            <input type="file" name="upload_file" />
            <input type="hidden" name="iu_callback_func" value="<?=hh($_REQUEST["iu_callback_func"])?>" />
            <button type="button" onclick="document.frm.submit()" class="w70">保存</button>
        </form>
    </td>
</tr>
<tr id="tr_upload2" style="<?=(!$newImageRow?"display:none":"")?>">
    <th><nobr>アップロードファイル</nobr></th>
    <td>
        <? $dlInfo = genDownloadTicket($new_image_id, $newImageRow["original_file_name"]); ?>
        <a class="with_icon" href="./<?=$dlInfo["rel_url"]?>" target="_blank">
            <img src="./img/spacer.gif" width="20" height="16" alt="" class="download_link" />
            <div><nobr><?=hh($newImageRow["original_file_name"])?></nobr></div>
        </a>
        <div style="padding-top:5px">
            <span><button type="button" onclick="ee('tr_upload1').style.display=''; ee('tr_upload2').style.display='none';" class="w70">キャンセル</button></span>
            <span style="padding-left:20px"><button type="button" onclick="imageSelected()" class="w70">ＯＫ</button></span>
        </div>
    </td>
</tr>
</table>
</div>


<div style="padding:5px 0"><button type="button" onclick="imageSelected('clear')" class="w70">クリア</button></div>

<div style="color:red"><?=$errmsg?></div>



</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>

