<?
//****************************************************************************************************************
// CSVアップローダ、およびそのためのダイアログ
// 管理画面「データインポート」のみで利用。
//
// アップロードしたデータCSVは、「csv_upload」フォルダに格納されつつ、「spft_csv_upload」テーブルに履歴と状態が格納される。
//
// アップロードしたファイルは消さない。手動で消す必要がある。
// 仮に1Mbのファイルを1000回アップしても1Gbである。総合サイズは知れている。
// 間違って巨大なファイルをアップロードされると問題かもしれないが、そういうこともないだろう。
//****************************************************************************************************************
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にspf管理者権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

$today_ymdhms = date("YmdHis");



$upload_seq = $_REQUEST["upload_seq"];
$csv_encode = $_REQUEST["csv_encode"];
$import_field_csv = $_REQUEST["import_field_csv"];
$import_target = $_REQUEST["import_target"];

if (!$csv_encode) $csv_encode = "UTF-8";


$errmsg = "";
do {
    $csv_upload_dir = dirname(__FILE__)."/csv_upload";
    if (!is_dir($csv_upload_dir)) @mkdir ($csv_upload_dir);
    @chmod ($csv_upload_dir, 0777);
    if (!is_dir($csv_upload_dir)) { $errmsg = "ディレクトリ「csv_upload」が作成できません。"; break; }
    if (!is_writable($csv_upload_dir)) { $errmsg = "ディレクトリ「csv_upload」に書き込み準備ができていません。"; break; }



    //****************************************************************************************************************
    // CSVの保存
    //****************************************************************************************************************
    if ($_REQUEST["save_csv"]) {

        c2dbSetSystemConfig("spf.ImportFieldCsv_".$import_target, $import_field_csv);

        $tmp_name = @$_FILES["upload_file"]["tmp_name"];
        if (!$tmp_name) { $errmsg = "ファイルを指定してください。"; break; }

        $size = @$_FILES["upload_file"]["size"];
        if (!$size) { $errmsg = "空ファイルを指定したか、ファイルが不正です。"; break; }

        $upload_seq = (int)c2dbGetOne("select nextval('spft_csv_upload_seq')");
        $dest_file_path = $csv_upload_dir."/".$upload_seq.".csv";

        @move_uploaded_file ($tmp_name, $dest_file_path);
        @chmod($dest_file_path, 0666);
        if (!is_file($dest_file_path)) { $errmsg = "ファイルのアップロードに失敗しました。"; break; }

        $sql =
        " insert into spft_csv_upload (".
        "     upload_seq, upload_ymdhms, upload_emp_id, upload_emp_nm, exec_ymdhms, csv_encode, csv_file_name".
        " ) values (".
        " ".$upload_seq.
        ",'".$today_ymdhms."'".
        ",".c2dbStr(C2_LOGIN_EMP_ID).
        ",".c2dbStr(C2_LOGIN_EMP_LT_NM." ".C2_LOGIN_EMP_LT_NM).
        ",''".// exec_ymdhms
        ",".c2dbStr($csv_encode).
        ",".c2dbStr(@$_FILES["upload_file"]["name"]).
        " )";
        c2dbExec($sql);
    }
} while(0);


//****************************************************************************************************************
// 入力パーツ群HTMLを作成（中または小分類の場合）
//****************************************************************************************************************
    if (!strlen($_REQUEST["callback_func"])) { echo "ng不正なアクセスです。(callback_funcなし)"; die; }

//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/JavaScript">
function loadInit() {
    <? if (!$errmsg && $upload_seq) { ?>
    window.parent.<?=$_REQUEST["callback_func"]?>(<?=hh($upload_seq)?>);
    return;
    <? } ?>
    ee("upload_seq").value = window.parent.document.getElementById("upload_seq").value;
    ee("import_field_csv").value = window.parent.document.getElementById("import_field_csv").value;
    ee("import_target").value = window.parent.document.getElementById("import_target").value;
}
</script>
</head>
<body onload="loadInit()"><div id="CLIENT_FRAME_CONTENT" pageBottomMargin="0" parentBodyOverflowY="hidden" thisBodyOverflowY="hidden" thisBodyOverflowX="hidden">






<div>
    <form name="frm" action="csv_uploader.php" method="post" enctype="multipart/form-data">
        <input type="hidden" id="upload_seq" name="upload_seq" value="<?=$upload_seq?>" />
        <input type="hidden" id="import_field_csv" name="import_field_csv" value="<?=$import_field_csv?>" />
        <input type="hidden" id="import_target" name="import_target" value="<?=$import_target?>" />

		<div style="padding-top:5px">
        <table cellspacing="0" cellpadding="0" class="width100 list_table">
        <tr>
            <th>ファイルの文字コード</th>
            <td>
                <label><input type="radio" name="csv_encode" value="Shift_JIS"<?=($csv_encode=="Shift_JIS"?" checked":"")?> />Shift_JIS(Windows/Mac標準)</label>
                <label><input type="radio" name="csv_encode" value="EUC-JP"<?=($csv_encode=="EUC-JP"?" checked":"")?> />EUC-JP</label>
                <label><input type="radio" name="csv_encode" value="UTF-8"<?=($csv_encode=="UTF-8"?" checked":"")?> />UTF-8</label>
            </td>
        </tr>
        <tr>
            <th>アップロード</th>
            <td>
                    <input type="hidden" name="save_csv" value="1" />
                    <input type="file" name="upload_file" />
                    <input type="hidden" name="callback_func" value="<?=hh($_REQUEST["callback_func"])?>" />
                    <button type="button" onclick="document.frm.submit()">アップロード</button>
            </td>
        </tr>
        </table>
        </div>
    </form>
</div>


<div style="color:red; height:40px"><?=$errmsg?></div>



</div><!-- // CLIENT_FRAME_CONTENT -->
</body>
</html>
