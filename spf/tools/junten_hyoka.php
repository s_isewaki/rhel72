<?
/*
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
$ss =
'a:2:{i:0;a:3:{s:13:"subhier_count";"s:1:"1";s:12:"subhier_type";s:2:"OR";s:14:"subhier_check1";N;}i:1;a:3:{s:13:"subhier_count";s:1:"1";s:12:"subhier_type";s:2:"OR";s:14:"subhier_check1";N;}}';
$obj = unserialize($ss);
print_r($obj);
die;
*/


set_time_limit(36000);
require_once("../common.php"); // 読込みと同時にログイン判別

c2dbExec("delete from spfm_approval_hier where wf_type = 'HYOKA' and target_id in ( select unit_id from spfm_hyoka_unit where han_id in ( select han_id from spfm_hyoka_han where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm like '★%' )) )");

c2dbExec("delete from spfm_approval_emp where wf_type = 'HYOKA' and target_id in ( select unit_id from spfm_hyoka_unit where han_id in ( select han_id from spfm_hyoka_han where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm like '★%' )) )");

c2dbExec("delete from spfm_hyoka_unit where han_id in ( select han_id from spfm_hyoka_han where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm like '★%' ))");

c2dbExec("delete from spfm_hyoka_han where sheet_id in ( select sheet_id from spfm_hyoka_sheet where sheet_nm like '★%' )");

c2dbExec("delete from spfm_hyoka_sheet where sheet_nm like '★%'");

c2dbExec("delete from spfm_hyoka_items where unit_id not in ( select unit_id from spfm_hyoka_unit)");



//・評価階層：2　（自己評価無し）
//
//被評価者役職　：　一次評価　：　二次評価
//------------------------------------------------------------------
//部長　　　　　：　副院長　　：　院長
//部長補佐　　　：　部長　　　：　副院長
//次長　　　　　：　部長補佐　：　部長
//課長　　　　　：　次長　　　：　部長
//課長補佐　　　：　次長　　　：　部長
//師長　　　　　：　課長　　　：　課長、次長
//主任　　　　　：　師長　　　：　課長
//係員　　　　　：　主任　　　：　師長
//
//※主任、師長は、本人上席にて指定
//※課長保以上は、部署/役職（看護部）にて指定
//※副院長、院長は、部署/役職（順天堂医院）にて指定

$names = array(
"★1号館7階A病棟" => '"20-136-540-30"',
"★1号館7階B病棟" => '"20-136-540-31"',
"★1号館8階A病棟" => '"20-136-540-32"',
"★1号館8階B病棟" => '"20-136-540-33"',
"★1号館8階C病棟" => '"20-136-540-34"',
"★1号館9階A病棟" => '"20-136-540-35"',
"★1号館9階B病棟" => '"20-136-540-36"',
"★1号館10階A病棟" => '"20-136-540-37"',
"★1号館10階B病棟" => '"20-136-540-38"',
"★1号館10階CN病棟" => '"20-136-540-39","20-136-540-40"',
"★1号館11階A病棟" => '"20-136-540-41"',
"★1号館11階B病棟" => '"20-136-540-42"',
"★1号館12階A病棟" => '"20-136-540-43"',
"★1号館12階B病棟" => '"20-136-540-44"',
"★1号館13階A病棟" => '"20-136-540-45"',
"★1号館13階B病棟" => '"20-136-540-46"',
"★1号館14階病棟" => '"20-136-540-47"',
"★B棟6階A病棟" => '"20-136-540-48"',
"★B棟10階病棟" => '"20-136-540-49"',
"★B棟11階病棟" => '"20-136-540-50"',
"★B棟12階病棟" => '"20-136-540-51"',
"★B棟13階病棟" => '"20-136-540-52"',
"★B棟14階病棟" => '"20-136-540-53"',
"★B棟15階病棟" => '"20-136-540-54"',
"★B棟16階病棟" => '"20-136-540-55"',
"★B棟17階病棟" => '"20-136-540-56"',
"★B棟18階病棟" => '"20-136-540-57"',
"★B棟19・20階病棟" => '"20-136-540-58"',
"★救急PC" => '"20-136-539-7"',
"★１A外来" => '"20-136-539-8"',
"★ハートセンター" => '"20-136-539-9"',
"★呼吸器内科" => '"20-136-539-10"',
"★膠原・糖・内分泌" => '"20-136-539-11"',
"★消化器内科" => '"20-136-539-12"',
"★消化器外科・呼吸器外科" => '"20-136-539-13"',
"★乳腺センター" => '"20-136-539-14"',
"★脳神経内科外科" => '"20-136-539-15"',
"★皮膚科・形成外科" => '"20-136-539-16"',
"★整形外科" => '"20-136-539-17"',
"★泌尿器科・腎臓内科" => '"20-136-539-18"',
"★眼科" => '"20-136-539-19"',
"★ペインクリニック" => '"20-136-539-20"',
"★産科・婦人科" => '"20-136-539-21"',
"★耳鼻咽喉科" => '"20-136-539-22"',
"★小児科・小児外科" => '"20-136-539-23"',
"★メンタルクリニック" => '"20-136-539-24"',
"★透析療法室" => '"20-136-539-25"',
"★放射線1号館、B棟" => '"20-136-539-26"',
"★内視鏡室" => '"20-136-539-27"',
"★健康スポーツ室" => '"20-136-539-28"',
"★患者・看護相談室" => '"20-145-568"',
"★化学療法室" => '"20-119-512-2"',
"★がん治療センター" => '"20-119-512-1"',
"★RFA治療室" => '""',
"★看護部" => '""', //
"★手術部：手術室" => '"20-124-523"', //
"★手術部：滅菌室" => '""', //
"★総合診療内科・血液内科・リハビリテーション科" => '""', //
"★外来看護助手" => '""',
"★病棟看護助手" => '""',
"★病棟クラーク" => '""'
);


$han_ids = array();


//----------------------------------------------------------
// エディション採番準備
//----------------------------------------------------------
$han_start_idx = 1 + c2dbGetOne("select max(han_id) from spfm_hyoka_han");
c2dbExec("select setval('spfm_hyoka_han_id_seq', ".$han_start_idx.", false);");

$unit_start_idx = 1 + c2dbGetOne("select max(unit_id) from spfm_hyoka_unit");
c2dbExec("select setval('spfm_hyoka_items_unit_id_seq', ".$unit_start_idx.", false);");

c2dbExec("delete from spfm_approval_hier where wf_type = 'HYOKA' and target_id >= ".$unit_start_idx);
c2dbExec("delete from spfm_approval_emp where wf_type = 'HYOKA' and target_id >= ".$unit_start_idx);


$unit_id_common2 = (int)c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
$unit_id_common3 = (int)c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
$unit_id_common4 = (int)c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");


//**********************************************************************************************************************
// 共通
//**********************************************************************************************************************

//----------------------------------------------------------
// 基礎看護技術シート
//     看護部共通評価＋看護部共通Privilege
//・対象所属　看護部 20-136
//・役職　部長37　師長43　係長44　主任45　係員46
//・職種　看護師9　看護師（派遣）55
//----------------------------------------------------------



$sheet_max_id = 1 + c2dbGetOne("select max(sheet_id) from spfm_hyoka_sheet");
$sql =
" insert into spfm_hyoka_sheet (".
"     sheet_id, sheet_nm, sheet_order, active_composit, del_flg, assign_cad, assign_job, assign_st, assign_job_ary, assign_cad_ary, assign_st_ary".
" ) values (".
" ".$sheet_max_id. // sheet_id
",'★基礎看護技術'". // sheet_nm
",'D00'". // sheet_order
",'hyi,prv'". // active_composit
",'0'". // del_flg
",''". // assign_cad
",''". // assign_job
",''". // assign_st
",'{\"9\",\"55\"}'". // assign_job_ary
",'{\"20-136\"}'". // assign_cad_ary
",'{\"37\",\"43\",\"44\",\"45\",\"46\"}'". // assign_st_ary
")";
c2dbExec($sql);


//----------------------------------------------------------
// 基礎看護技術エディション
//----------------------------------------------------------
$han_id = (int)c2dbGetOne("select nextval('spfm_hyoka_han_id_seq')");

$sql =
" insert into spfm_hyoka_han (".
"     han_id, sheet_id, han_ymd_fr, han_ymd_to, han_term, han_nm, is_used_han".
"    ,job_description_html, han_introduction_html, is_han_active, update_ymdhms".
"    ,tinymce_version, prv_introduction_html, job_description_ph, han_introduction_ph, prv_introduction_ph, rev_id".
" ) values (".
" ".$han_id.
",".$sheet_max_id.
",'20150401'".
",'20160331'".
",'20150401-20160331'".
",'2015年度'".
",'0'".
",''".
",'<table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">職務項目評価基準　（専門職務能力をA,B,C,D,Sで評価）</th></tr><tr><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>未経験・対象外</td></tr></table>'".
",'1'".
",20150907000000".
",''".
",''".
",''".
",''".
",''".
",0".
")";
c2dbExec($sql);

c2dbExec("update spfm_hyoka_han set prv_introduction_html = '<div>【評価方法について】</div><div>�� 自己評価者は、各評価基準をに従い自己評価する　（S・A・B・C・Dのいずれかを選択する）</div><div>�� 評価実施後は、一次評価者に自己評価票を手渡す。</div><div>＊職務項目の評価において、C以下の該当職務は単独実施を禁止する。</div><div>＊教育受講欄にチェックが付く職務においては、指定の教育研修の受講実績が必要である。</div><table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">行動項目評価基準</th><th colspan=\"2\">職務項目評価基準</th></tr><tr><td>S</td><td>指導レベル</td><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>模範レベル</td><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>標準レベル</td><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>課題レベル</td><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>問題レベル</td><td>D</td><td>未経験・対象外</td></tr></table>' where han_id = ".$han_id);

//----------------------------------------------------------
// 基礎看護技術（外来＋病棟）項目は末尾で登録
//----------------------------------------------------------
$sql =
" insert into spfm_hyoka_unit (".
"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
"    ,unit_composit, rev_id".
" ) values (".
"     ".$unit_id_common2. // unit_id,
"    ,'基礎看護技術'". // unit_nm,
"    ,2". // unit_order,
"    ,''". // unit_description,
"    ,".$han_id. // han_id,
"    ,'use_hyoka_flow'". // unit_options,
"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
"    ,5". // abc_count,
"    ,'1'". // is_need_approval_yakusyoku,
"    ,''". // is_need_approval_syozoku,
"    ,''". // is_need_sinsei,
"    ,''". // is_need_jiko_hyoka,
"    ,''". // is_used_unit,
"    ,'UC_HYOKA'". // unit_composit,
"    ,0". // rev_id,
" );";
c2dbExec($sql);
regApproval($unit_id_common2);


//----------------------------------------------------------
// 行動項目（Privilege）（外来＋病棟）項目は末尾で登録
//----------------------------------------------------------
$sql =
" insert into spfm_hyoka_unit (".
"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
"    ,unit_composit, rev_id".
" ) values (".
"     ".$unit_id_common3. // unit_id,
"    ,'行動項目'". // unit_nm,
"    ,3". // unit_order,
"    ,'当院の職員としてふさわしい行動をしているか評価をする。'". // unit_description,
"    ,".$han_id. // han_id,
"    ,'use_hyoka_flow'". // unit_options,
"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
"    ,5". // abc_count,
"    ,'1'". // is_need_approval_yakusyoku,
"    ,''". // is_need_approval_syozoku,
"    ,''". // is_need_sinsei,
"    ,''". // is_need_jiko_hyoka,
"    ,''". // is_used_unit,
"    ,'UC_PRIVILEGE'". // unit_composit,
"    ,0". // rev_id,
" );";
c2dbExec($sql);
regApproval($unit_id_common3);

//----------------------------------------------------------
// 看護共通項目（Privilege）（外来＋病棟）項目は末尾で登録
//----------------------------------------------------------
//$_unit_id = c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
$sql =
" insert into spfm_hyoka_unit (".
"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
"    ,unit_composit, rev_id".
" ) values (".
"     ".$unit_id_common4. // unit_id,
"    ,'看護共通項目'". // unit_nm,
"    ,4". // unit_order,
"    ,'基本となる職務能力の評価を行う。'". // unit_description,
"    ,".$han_id. // han_id,
"    ,'is_kouza_obligate,use_hyoka_flow'". // unit_options,
"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
"    ,5". // abc_count,
"    ,'1'". // is_need_approval_yakusyoku,
"    ,''". // is_need_approval_syozoku,
"    ,''". // is_need_sinsei,
"    ,''". // is_need_jiko_hyoka,
"    ,''". // is_used_unit,
"    ,'UC_PRIVILEGE'". // unit_composit,
"    ,0". // rev_id,
" );";
c2dbExec($sql);
regApproval($unit_id_common4);










//**********************************************************************************************************************
// 各シート
//**********************************************************************************************************************

$sheet_order = 0;
foreach ($names as $nm => $cadr) {

	// ■評価シート名：　各病棟や外来の部署名
	// ・対象所属　各シートに記載の部署名
	// ※1号館10CN病棟は、1号館10C病棟＋1号館10N病棟
	// ※紐づく部署が不明のものは、そのままブランクで構いません。
	// 確認の際に手動で登録します。
	// ・役職　部長　師長　係長　主任　係員 37,43,44,45,46
	// ・職種　看護師　看護師（派遣） 9,55


	$sheet_order++;
	$max_id = 1 + c2dbGetOne("select max(sheet_id) from spfm_hyoka_sheet");
	$sql =
	" insert into spfm_hyoka_sheet (".
	"     sheet_id, sheet_nm, sheet_order, active_composit, del_flg, assign_cad, assign_job, assign_st, assign_job_ary, assign_cad_ary, assign_st_ary".
	" ) values (".
	" ".$max_id. // sheet_id
	",".c2dbStr($nm). // sheet_nm
	",'".sprintf("D%02d",$sheet_order)."'". // sheet_order
	",'jod,hyi'". // active_composit
	",'0'". // del_flg
	",''". // assign_cad
	",''". // assign_job
	",''". // assign_st
	",'{\"9\",\"55\"}'". // assign_job_ary
	",'{".$cadr."}'". // assign_cad_ary
	",'{\"37\",\"43\",\"44\",\"45\",\"46\"}'". // assign_st_ary
	")";
	c2dbExec($sql);

	if  ($nm=="★外来看護助手" || $nm=="★病棟看護助手" || $nm=="★病棟クラーク") {
		c2dbExec("update spfm_hyoka_sheet set active_composit = 'jod,hyi,prv' where sheet_id = ".$max_id);
	}


	if ($nm=="★看護部" || $nm=="★手術部：手術室" || $nm=="★手術部：滅菌室" || $nm=="★総合診療内科・血液内科・リハビリテーション科") {
		c2dbExec("update spfm_hyoka_sheet set active_composit = 'jod' where sheet_id = ".$max_id);
	}
	if ($nm=="★１A外来") {
		c2dbExec("update spfm_hyoka_sheet set active_composit = 'hyi' where sheet_id = ".$max_id);
	}

	$han_id = (int)c2dbGetOne("select nextval('spfm_hyoka_han_id_seq')");

	$sql =
	" insert into spfm_hyoka_han (".
	"     han_id, sheet_id, han_ymd_fr, han_ymd_to, han_term, han_nm, is_used_han".
	"    ,job_description_html, han_introduction_html, is_han_active, update_ymdhms".
	"    ,tinymce_version, prv_introduction_html, job_description_ph, han_introduction_ph, prv_introduction_ph, rev_id".
	" ) values (".
	" ".$han_id.
	",".$max_id.
	",'20150401'".
	",'20160331'".
	",'20150401-20160331'".
	",'2015年度'".
	",'0'".
	",''".
	",''".
	",'1'".
	",20150907000000".
	",''".
	",''".
	",''".
	",''".
	",''".
	",0".
	")";
	c2dbExec($sql);

	$han_ids[$nm] = $han_id;


	if ($nm!="★看護部" && $nm!="★手術部：手術室" && $nm!="★手術部：滅菌室" && $nm!="★総合診療内科・血液内科・リハビリテーション科") {
		c2dbExec("update spfm_hyoka_han set han_introduction_html = '<table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">職務項目評価基準　（専門職務能力をA,B,C,D,Sで評価）</th></tr><tr><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>知識としてわかる・未経験・対象外</td></tr></table>' where han_id = ".$han_id);
//		c2dbExec("update spfm_hyoka_han set prv_introduction_html = '<div>＊職務項目の評価において、C以下の該当職務は単独実施を禁止する。</div><div>＊教育受講欄にチェックが付く職務においては、指定の教育研修の受講実績が必要である。</div><table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">行動項目評価基準</th><th colspan=\"2\">職務項目評価基準</th></tr><tr><td>S</td><td>指導レベル</td><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>模範レベル</td><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>標準レベル</td><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>課題レベル</td><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>問題レベル</td><td>D</td><td>知識としてわかる・未経験・対象外</td></tr></table>' where han_id = ".$han_id);
	}


	//----------------------------------------------------------
	// クラーク系
	//----------------------------------------------------------
	if  ($nm=="★外来看護助手" || $nm=="★病棟看護助手" || $nm=="★病棟クラーク") {
		c2dbExec("update spfm_hyoka_han set han_introduction_html = '<table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">行動項目評価基準</th><th colspan=\"2\">職務項目評価基準</th></tr><tr><td>S</td><td>指導レベル</td><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>模範レベル</td><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>標準レベル</td><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>課題レベル</td><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>問題レベル</td><td>D</td><td>知識としてわかる・未経験・対象外</td></tr></table>' where han_id = ".$han_id);
		c2dbExec("update spfm_hyoka_han set prv_introduction_html = '<div>【評価方法について】</div><div>�� 自己評価者は、各評価基準をに従い自己評価する　（S・A・B・C・Dのいずれかを選択する）</div><div>�� 評価実施後は、一次評価者に自己評価票を手渡す。</div><div>＊職務項目の評価において、C以下の該当職務は単独実施を禁止する。</div><div>＊教育受講欄にチェックが付く職務においては、指定の教育研修の受講実績が必要である。</div><table cellspacing=\"0\" cellpadding=\"0\" class=\"list_table\"><tr><th colspan=\"2\">行動項目評価基準</th><th colspan=\"2\">職務項目評価基準</th></tr><tr><td>S</td><td>指導レベル</td><td>S</td><td>指導できる</td></tr><tr><td>A</td><td>模範レベル</td><td>A</td><td>リスク回避や異常時の対応を含めて単独で実施する能力がある</td></tr><tr><td>B</td><td>標準レベル</td><td>B</td><td>単独で実施できる（異常やリスクを発見し、直ちに他の協力を仰ぐことができる）</td></tr><tr><td>C</td><td>課題レベル</td><td>C</td><td>アシスタントとして行える(単独実施を禁止)</td></tr><tr><td>D</td><td>問題レベル</td><td>D</td><td>知識としてわかる・未経験・対象外</td></tr></table>' where han_id = ".$han_id);
		//----------------------------------------------------------
		// 行動項目（評価）
		//----------------------------------------------------------
		$_unit_id = c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
		$sql =
		" insert into spfm_hyoka_unit (".
		"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
		"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
		"    ,unit_composit, rev_id".
		" ) values (".
		"     ".$_unit_id. // unit_id,
		"    ,'行動項目'". // unit_nm,
		"    ,1". // unit_order,
		"    ,'当院の職員としてふさわしい行動をしているか評価をする。'". // unit_description,
		"    ,".$han_id. // han_id,
		"    ,'use_hyoka_flow'". // unit_options,
		"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
		"    ,5". // abc_count,
		"    ,'1'". // is_need_approval_yakusyoku,
		"    ,''". // is_need_approval_syozoku,
		"    ,''". // is_need_sinsei,
		"    ,''". // is_need_jiko_hyoka,
		"    ,''". // is_used_unit,
		"    ,'UC_HYOKA'". // unit_composit,
		"    ,0". // rev_id,
		" );";
		c2dbExec($sql);
		regApproval($_unit_id);

		regItem("", "UC_HYOKA", $_unit_id, "礼儀・接遇・マナー");
		regItem("", "UC_HYOKA", $_unit_id, "地球環境保全（医療資材の適切利用・節約・環境整備）");
		regItem("", "UC_HYOKA", $_unit_id, "スタッフとの信頼関係・職種間連携");
		regItem("", "UC_HYOKA", $_unit_id, "自己啓発・向上心(教育力、国際対応力・研究力)");
		regItem("", "UC_HYOKA", $_unit_id, "迅速な対応");


		//----------------------------------------------------------
		// 看護共通項目（評価？）
		//----------------------------------------------------------
		$_unit_id = c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
		$sql =
		" insert into spfm_hyoka_unit (".
		"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
		"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
		"    ,unit_composit, rev_id".
		" ) values (".
		"     ".$_unit_id. // unit_id,
		"    ,'看護共通項目'". // unit_nm,
		"    ,2". // unit_order,
		"    ,'基本となる職務能力の評価を行う。'". // unit_description,
		"    ,".$han_id. // han_id,
		"    ,'is_kouza_obligate,use_hyoka_flow'". // unit_options,
		"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
		"    ,5". // abc_count,
		"    ,'1'". // is_need_approval_yakusyoku,
		"    ,''". // is_need_approval_syozoku,
		"    ,''". // is_need_sinsei,
		"    ,''". // is_need_jiko_hyoka,
		"    ,''". // is_used_unit,
		"    ,'UC_HYOKA'". // unit_composit,
		"    ,0". // rev_id,
		" );";
		c2dbExec($sql);
		regApproval($_unit_id);

		if  ($nm=="★外来看護助手") {
			regItem("", "UC_HYOKA", $_unit_id, "輸血用血液の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "鎮静・麻酔のモニタリング", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "麻薬・毒薬・向精神薬の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "ハイアラート薬の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "一次救命の実施（BLS）", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "感染対策の基本を理解し実施している");
			regItem("", "UC_HYOKA", $_unit_id, "医療安全について理解し実施している");
			regItem("", "UC_HYOKA", $_unit_id, "職務を理解している");
			regItem("", "UC_HYOKA", $_unit_id, "業務範疇を理解している");
			regItem("", "UC_HYOKA", $_unit_id, "電話対応ができる");
		}
		if  ($nm=="★病棟看護助手" || $nm=="★病棟クラーク") {
			regItem("", "UC_HYOKA", $_unit_id, "輸血用血液の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "麻薬・毒薬・向精神薬の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "ハイアラート薬の取り扱い", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "一次救命の実施（BLS）", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "感染対策の基本を理解し実施している", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "医療安全について理解し実施している", array("is_kouza_obligate"=>1));
			regItem("", "UC_HYOKA", $_unit_id, "職務を理解している");
			regItem("", "UC_HYOKA", $_unit_id, "業務範疇を理解している");
			regItem("", "UC_HYOKA", $_unit_id, "電話対応ができる");
			regItem("", "UC_HYOKA", $_unit_id, "ナースコールの対応ができる");
			regItem("", "UC_HYOKA", $_unit_id, "面会者の対応ができる");
			regItem("", "UC_HYOKA", $_unit_id, "各部署での日常業務ができる");
			regItem("", "UC_HYOKA", $_unit_id, "各部署での週間業務ができる");
		}


		//----------------------------------------------------------
		// 専門職務項目（評価？）
		//----------------------------------------------------------
		$_unit_id = c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
		$sql =
		" insert into spfm_hyoka_unit (".
		"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
		"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
		"    ,unit_composit, rev_id".
		" ) values (".
		"     ".$_unit_id. // unit_id,
		"    ,'専門職務項目'". // unit_nm,
		"    ,3". // unit_order,
		"    ,'専門職務能力の評価を行う。'". // unit_description,
		"    ,".$han_id. // han_id,
		"    ,'is_kouza_obligate,use_hyoka_flow'". // unit_options,
		"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
		"    ,5". // abc_count,
		"    ,'1'". // is_need_approval_yakusyoku,
		"    ,''". // is_need_approval_syozoku,
		"    ,''". // is_need_sinsei,
		"    ,''". // is_need_jiko_hyoka,
		"    ,''". // is_used_unit,
		"    ,'UC_PRIVILEGE'". // unit_composit,
		"    ,0". // rev_id,
		" );";
		c2dbExec($sql);
		regApproval($_unit_id);

		if  ($nm=="★外来看護助手") {
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者確認の技術");
			regItem("", "UC_PRIVILEGE", $_unit_id, "診察介助の補助");
			regItem("", "UC_PRIVILEGE", $_unit_id, "環境整備の技術");
			regItem("", "UC_PRIVILEGE", $_unit_id, "検査の説明と案内");
			regItem("", "UC_PRIVILEGE", $_unit_id, "歩行介助・移送");
			regItem("", "UC_PRIVILEGE", $_unit_id, "診療カード・書類の取り扱い");
			regItem("", "UC_PRIVILEGE", $_unit_id, "予約変更、受診に関する電話対応");
			regItem("", "UC_PRIVILEGE", $_unit_id, "入院手続きの案内");
			regItem("", "UC_PRIVILEGE", $_unit_id, "看護助手、病棟クラーク研修会参加");
		}
		if  ($nm=="★病棟看護助手") {
			regItem("", "UC_PRIVILEGE", $_unit_id, "病室の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ステーション内の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "パントリーの環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "SPD棚の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "リネン庫・器材庫の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "作業台の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "処置台の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "備品の清掃ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "温度・湿度の調整ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ベッドメイキングができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "シーツ交換体制の理解ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "基準寝具の補充ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "基準寝具の洗濯回収方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "洗濯伝票Bの記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ストレッチャー包布・車椅子用かけ包布交換ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "定数変更伝票の記載方法が理解できる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "消毒依頼伝票の記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ユニフォーム選択伝票の記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者所有の洗濯提出・記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "全身清拭の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "部分浴の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "入浴、シャワー浴の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "洗髪の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "洗面・歯磨きの介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "義歯洗浄ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "おむつ交換の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "排泄の世話の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "便器・尿器の洗浄消毒ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "お茶の準備・配茶ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "配膳・下膳・食事介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "体位変換の介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "冷罨法の作成ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "温罨法の作成ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ストレッチャーでの移送ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "車椅子での移送ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "見守りが必要な歩行患者の付き添い歩行ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "各種外来・検査室・手術室への案内ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "検体受付時間・提出場所がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "主な検査場所がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "検体容器の補充場所がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ME機器の準備・片付けができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "滅菌物品の後片付けと回収準備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "定数供給物品の定数確認ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "物品破損・紛失届の記載ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "滅菌室物品定数変更伝票の記載がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "滅菌室物品臨時借用・返却方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "滅菌依頼の入力ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "消耗品物品の請求方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "点滴スタンド・車椅子・ストレッチャーの清掃ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者の郵便物の授受ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "酸素ボンベの交換ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "入退院・転出入に関する世話ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者の買い物の付き添いができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "歩行患者の散歩に付き添うことができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "不穏患者の見守りができる");
		}
		if  ($nm=="★病棟クラーク") {
			regItem("", "UC_PRIVILEGE", $_unit_id, "入院・転入の取り扱い");
			regItem("", "UC_PRIVILEGE", $_unit_id, "退院・転出の取り扱い");
			regItem("", "UC_PRIVILEGE", $_unit_id, "DPC基本票・調査票入力確認、出力の技術");
			regItem("", "UC_PRIVILEGE", $_unit_id, "スキャン取り込みの技術");
			regItem("", "UC_PRIVILEGE", $_unit_id, "超過勤務指示書の取り扱い");
			regItem("", "UC_PRIVILEGE", $_unit_id, "資材物品（日雑品）の在庫管理");
			regItem("", "UC_PRIVILEGE", $_unit_id, "機密文書の取り扱い");
			regItem("", "UC_PRIVILEGE", $_unit_id, "病棟看護助手との協働業務");
			regItem("", "UC_PRIVILEGE", $_unit_id, "病棟クラーク会への参加");
			regItem("", "UC_PRIVILEGE", $_unit_id, "看護助手・病棟クラーク研修会への参加");
			regItem("", "UC_PRIVILEGE", $_unit_id, "医療法に基づいて業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "健康保険法に基づいて業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "医事関連法に基づいて業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "医療保障制度に基づいて業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "診療報酬請求に精通し業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "DPC請求・調査に精通し業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "保健医療用語に精通し業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者サービスを念頭に業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "書類作成等に精通し業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "関連部署とコミュニケーションを取り業務を行える");
			regItem("", "UC_PRIVILEGE", $_unit_id, "病室の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ステーション内の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "パントリーの環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "SPD棚の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "リネン庫・器材庫の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "作業台の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "処置台の環境整備ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "備品の清掃ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "温度・湿度の調整ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ベッドメイキングができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "シーツ交換体制の理解ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ストレッチャー包布・車椅子用かけ包布交換ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ユニフォーム選択伝票の記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者所有の洗濯提出・記載方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "お茶の準備・配茶ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "配膳・下膳・食事介助ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ストレッチャーでの移送ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "車椅子での移送ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "見守りが必要な歩行患者の付き添い歩行ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "各種外来・検査室・手術室への案内ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "主な検査場所がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "ME機器の準備・片付けができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "消耗品物品の請求方法がわかる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "点滴スタンド・車椅子・ストレッチャーの清掃ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者の郵便物の授受ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "入退院・転出入に関する世話ができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "患者の買い物の付き添いができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "歩行患者の散歩に付き添うことができる");
			regItem("", "UC_PRIVILEGE", $_unit_id, "不穏患者の見守りができる");
		}

		continue;
	}

	//----------------------------------------------------------
	// 職務項目（外来または病棟）
	//----------------------------------------------------------
	$_unit_id = c2dbGetOne("select nextval('spfm_hyoka_items_unit_id_seq')");
	$sql =
	" insert into spfm_hyoka_unit (".
	"     unit_id, unit_nm, unit_order, unit_description, han_id, unit_options".
	"    ,abc_pattern, abc_count, is_need_approval_yakusyoku, is_need_approval_syozoku, is_need_sinsei, is_need_jiko_hyoka, is_used_unit".
	"    ,unit_composit, rev_id".
	" ) values (".
	"     ".$_unit_id. // unit_id,
	"    ,'職務項目'". // unit_nm,
	"    ,1". // unit_order,
	"    ,'専門職務能力の評価を行う。'". // unit_description,
	"    ,".$han_id. // han_id,
	"    ,'use_hyoka_flow'". // unit_options,
	"    ,'a:5:{i:0;a:2:{s:4:\"name\";s:1:\"S\";s:3:\"mul\";s:1:\"5\";}i:1;a:2:{s:4:\"name\";s:1:\"A\";s:3:\"mul\";s:1:\"4\";}i:2;a:2:{s:4:\"name\";s:1:\"B\";s:3:\"mul\";s:1:\"3\";}i:3;a:2:{s:4:\"name\";s:1:\"C\";s:3:\"mul\";s:1:\"2\";}i:4;a:2:{s:4:\"name\";s:1:\"D\";s:3:\"mul\";s:1:\"1\";}}'". // abc_pattern,
	"    ,5". // abc_count,
	"    ,'1'". // is_need_approval_yakusyoku,
	"    ,''". // is_need_approval_syozoku,
	"    ,''". // is_need_sinsei,
	"    ,''". // is_need_jiko_hyoka,
	"    ,''". // is_used_unit,
	"    ,'UC_HYOKA'". // unit_composit,
	"    ,0". // rev_id,
	" );";
	c2dbExec($sql);
	regApproval($_unit_id);




	if ($nm=="★1号館7階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "観血的動脈圧ライン挿入中の採血");
		regItem("", "UC_HYOKA", $_unit_id, "観血的動脈圧ライン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈ｶﾃｰﾃﾙ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "各種ドレーン挿入中の患者の看護（Ｊバックドレーン・SBドレーン・胸腔ドレーン）");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "スワンガンツカテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "心嚢穿刺を行う患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開の介助");
		regItem("", "UC_HYOKA", $_unit_id, "気切チューブ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ミニトラック挿入の介助");
		regItem("", "UC_HYOKA", $_unit_id, "挿管中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "非侵襲的陽圧換気用法中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的心肺補助法（PCPS）挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "大動脈内バルーンパンピング挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "心臓血管外科疾患術後の患者の入室準備（成人）と看護");
		regItem("", "UC_HYOKA", $_unit_id, "心臓血管外科疾患術後の患者の入室準備（小児）と看護");
		regItem("", "UC_HYOKA", $_unit_id, "心血管造影後の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "手術直後の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "心臓リハビリテーション中の患者の看護");
	}
	if ($nm=="★1号館7階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "心臓血管外科患者の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "ペースメーカー挿入患者の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "ｶﾙﾃﾞｨｵﾊﾞｰｼﾞｮﾝの看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "CAG,PCI患者の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "ｽﾊﾟｲﾅﾙﾄﾞﾚﾅｰｼﾞ患者の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "心臓ﾘﾊﾋﾞﾘﾃｰｼｮﾝ患者の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔穿刺の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "心嚢穿刺の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈カテーテル挿入の看護ができる");
		regItem("", "UC_HYOKA", $_unit_id, "低圧持続吸引中の患者の看護ができる");
	}
	if ($nm=="★1号館8階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助（ナーシングスキル）");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助（ナーシングスキル）");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養の介助（ナーシングスキル）");
		regItem("", "UC_HYOKA", $_unit_id, "移植患者の看護(前処置TBI) ");
		regItem("", "UC_HYOKA", $_unit_id, "輸注時の看護（臍帯血・骨髄・末梢血幹細胞）");
		regItem("", "UC_HYOKA", $_unit_id, "自己血輸血時の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "末梢血幹細胞採取（ハーベスト）時の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ベナンバックス吸入時の患者の看護");
	}
	if ($nm=="★1号館8階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "SAP導入患者の看護（糖尿病内科）");
		regItem("", "UC_HYOKA", $_unit_id, "糖尿病教育入院の看護（糖尿病内科）");
		regItem("", "UC_HYOKA", $_unit_id, "心臓カテーテル患者の看護（ナーシングスキル）（循環器内科）");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的冠動脈形成術・経皮的下肢動脈形成術をうける患者の看護（循環器内科）");
		regItem("", "UC_HYOKA", $_unit_id, "ペースメーカー・ICD挿入患者の看護（ナーシングスキル）（循環器内科）");
		regItem("", "UC_HYOKA", $_unit_id, "アブレーション患者の看護（循環器内科）");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助（ナーシングスキル）（血液内科）");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助（ナーシングスキル）（血液内科）");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養の介助（ナーシングスキル）（血液内科）");
		regItem("", "UC_HYOKA", $_unit_id, "ベナンバックス吸入（血液内科）");
	}
	if ($nm=="★1号館8階C病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "身体拘束（上下肢・胴・肩仮縛）");
		regItem("", "UC_HYOKA", $_unit_id, "身体拘束（ミトン）");
		regItem("", "UC_HYOKA", $_unit_id, "身体拘束（安全着）");
		regItem("", "UC_HYOKA", $_unit_id, "身体拘束（車椅子用固定ベルト）");
		regItem("", "UC_HYOKA", $_unit_id, "修正型電気けいれん療法（m-ECT）を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "離床センサーの設置（ツインパッド）");
		regItem("", "UC_HYOKA", $_unit_id, "離床センサーの設置（おきたくん）");
		regItem("", "UC_HYOKA", $_unit_id, "離床センサーの設置（コールマット）");
		regItem("", "UC_HYOKA", $_unit_id, "離床センサーの設置（ベッドコール・コードレス）");
	}
	if ($nm=="★1号館9階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "トパーズ電動式低圧吸引器　使用手順");
		regItem("", "UC_HYOKA", $_unit_id, "呼吸リハビリテーション");
		regItem("", "UC_HYOKA", $_unit_id, "人工呼吸器装着患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "在宅酸素療法導入時の使用手順");
	}
	if ($nm=="★1号館9階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "放射線科　金属植え込み術の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "HIV陽性患者の対応");
		regItem("", "UC_HYOKA", $_unit_id, "HIV感染症・AIDS患者の生活指導");
		regItem("", "UC_HYOKA", $_unit_id, "在宅酸素療法の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "心筋焼灼術の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＰＣＩの患者の看護");
	}
	if ($nm=="★1号館10階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "授乳");
		regItem("", "UC_HYOKA", $_unit_id, "調乳");
		regItem("", "UC_HYOKA", $_unit_id, "特殊ﾐﾙｸ調乳");
		regItem("", "UC_HYOKA", $_unit_id, "食事介助(乳児期〜幼児期)");
		regItem("", "UC_HYOKA", $_unit_id, "オムツ交換(乳児期〜幼児期)　オムツ集計");
		regItem("", "UC_HYOKA", $_unit_id, "浣腸(新生児期〜学童期)");
		regItem("", "UC_HYOKA", $_unit_id, "綿棒刺激（新生児〜幼児期）");
		regItem("", "UC_HYOKA", $_unit_id, "洗腸介助(新生児〜学童期）");
		regItem("", "UC_HYOKA", $_unit_id, "ガス抜き(新生児〜幼児期)");
		regItem("", "UC_HYOKA", $_unit_id, "身体計測(身長・体重・頭囲・胸囲・腹囲）");
		regItem("", "UC_HYOKA", $_unit_id, "点滴挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "採血介助");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養介助");
		regItem("", "UC_HYOKA", $_unit_id, "末梢静脈挿入式中心静脈(PI)ｶﾃｰﾃﾙ挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "末梢静脈挿入式中心静脈(ｸﾞﾛｰｼｮﾝ)ｶﾃｰﾃﾙ挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取(採血)");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取(尿・尿培養）");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（検便・潜血確認・ビリルビン確認）");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（咽頭培養）");
		regItem("", "UC_HYOKA", $_unit_id, "血糖測定（新生児期〜幼児期）");
		regItem("", "UC_HYOKA", $_unit_id, "経鼻胃管ｶﾃｰﾃﾙ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経腸栄養(ED)チューブ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ポータブル低圧持続吸引(J-VAC)ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "開放式(ペンローズ)ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ポータブル低圧持続吸引(スパイラル)ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腸廔挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃空腸廔(G−J)チューブ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈栄養カテーテル(CV)挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱留置ｶﾃｰﾃﾙ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "切断した膀胱留置ｶﾃｰﾃﾙ(カットバルーン)挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "尿管皮膚造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎瘻造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "人工肛門造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的経肝胆管(PTCD）ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "末梢静脈挿入式中心静脈(PI)ｶﾃｰﾃﾙ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "末梢静脈挿入式中心静脈(ｸﾞﾛｰｼｮﾝ)ｶﾃｰﾃﾙ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ﾄﾞﾚｰﾝ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管内吸引(閉鎖式吸引)");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助");
	}
	if ($nm=="★1号館10階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "調乳（一般・特別調乳）");
		regItem("", "UC_HYOKA", $_unit_id, "授乳（準備・脱気・片付け）");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻の管理（ボタン式・ボタン式移行前）");
		regItem("", "UC_HYOKA", $_unit_id, "オムツ交換、尿比重測定（乳児〜幼児期）、");
		regItem("", "UC_HYOKA", $_unit_id, "綿棒刺激(新生児〜乳児期）");
		regItem("", "UC_HYOKA", $_unit_id, "沐浴");
		regItem("", "UC_HYOKA", $_unit_id, "沐浴シャワー浴前の中心静脈カテーテル（CV）挿入部の保護方法");
		regItem("", "UC_HYOKA", $_unit_id, "沐浴シャワー浴前の末梢点滴挿入部保護方法");
		regItem("", "UC_HYOKA", $_unit_id, "身体抑制（体幹抑制帯・大腿抑制・オルソオグラフ入りミトンの作成）");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "脳脊椎腔内注射介助");
		regItem("", "UC_HYOKA", $_unit_id, "腎生検介助（処置室実施）");
		regItem("", "UC_HYOKA", $_unit_id, "肝生検介助（処置室実施）");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ドレーン挿入介助（小児科）");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ドレーン挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "JVACドレーン挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ペンローズドレーン・皮下ドレーン挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外ドレーン挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈カテーテル（CV）挿入介助（小児科）");
		regItem("", "UC_HYOKA", $_unit_id, "末梢中心静脈カテーテル（PI）・グローションカテーテル挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "EDチューブ挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "点滴挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "点滴固定方法");
		regItem("", "UC_HYOKA", $_unit_id, "採血介助（静脈・動脈・踵部）");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養介助");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（尿・尿培）");
		regItem("", "UC_HYOKA", $_unit_id, "化学療法（病棟に届いてから投与前の医師との確認方法）小児科のみ");
		regItem("", "UC_HYOKA", $_unit_id, "小児心臓血管外科患者の看護（イソジン浴、水分出納、ドレーン管理）");
		regItem("", "UC_HYOKA", $_unit_id, "経口与薬（小児）");
	}
	if ($nm=="★1号館10階CN病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "バイタルサイン測定（新生児、乳児）");
		regItem("", "UC_HYOKA", $_unit_id, "身体測定（新生児、乳幼児）");
		regItem("", "UC_HYOKA", $_unit_id, "調乳");
		regItem("", "UC_HYOKA", $_unit_id, "授乳");
		regItem("", "UC_HYOKA", $_unit_id, "胃管挿入と管理");
		regItem("", "UC_HYOKA", $_unit_id, "EDチューブ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "肛門刺激");
		regItem("", "UC_HYOKA", $_unit_id, "ガス抜き");
		regItem("", "UC_HYOKA", $_unit_id, "浣腸(新生児)");
		regItem("", "UC_HYOKA", $_unit_id, "洗腸介助");
		regItem("", "UC_HYOKA", $_unit_id, "人工肛門造設後の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "nasal CPAP使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "nasal DPAP使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "SiPAP使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "挿管管理中の患者の看護（IMV、HFO）");
		regItem("", "UC_HYOKA", $_unit_id, "挿管・抜管介助（新生児・乳児）");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開術施行後の患者の看護（新生児・乳児）");
		regItem("", "UC_HYOKA", $_unit_id, "一酸化窒素吸入療法");
		regItem("", "UC_HYOKA", $_unit_id, "窒素吸入療法");
		regItem("", "UC_HYOKA", $_unit_id, "末梢点滴挿入介助と管理（新生児・乳児）");
		regItem("", "UC_HYOKA", $_unit_id, "末梢中心静脈カテーテル（PI）挿入介助と管理");
		regItem("", "UC_HYOKA", $_unit_id, "臍カテーテル挿入介助と管理");
		regItem("", "UC_HYOKA", $_unit_id, "末梢動脈ルート挿入介助と管理");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈カテーテル（CV）挿入介助と管理（新生児・乳児）");
		regItem("", "UC_HYOKA", $_unit_id, "J-Vac挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "オンマイヤー穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "眼科診察介助");
		regItem("", "UC_HYOKA", $_unit_id, "眼科レーザー治療施行中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "光線療法施行中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "閉鎖式保育器管理");
		regItem("", "UC_HYOKA", $_unit_id, "開放式保育器管理");
		regItem("", "UC_HYOKA", $_unit_id, "コット管理");
		regItem("", "UC_HYOKA", $_unit_id, "新生児のフィジカルアセスメント");
		regItem("", "UC_HYOKA", $_unit_id, "家族支援");
		regItem("", "UC_HYOKA", $_unit_id, "ディベロップメンタルケア");
		regItem("", "UC_HYOKA", $_unit_id, "日本周産期・新生児医学会認定新生児蘇生法（NCPR）Aコースの取得");
	}
	if ($nm=="★1号館11階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "妊産婦：外診技術（レオポルド触診・子宮底・腹囲測定、胎児心拍測定）");
		regItem("", "UC_HYOKA", $_unit_id, "妊産婦：内診技術");
		regItem("", "UC_HYOKA", $_unit_id, "妊産婦：産痛緩和、分娩進行促進への援助");
		regItem("", "UC_HYOKA", $_unit_id, "妊産婦：正常分娩の直接介助");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠期、分娩時の異常時（NRFS、弛緩出血、常位胎盤早期剥離、子宮破裂など）看護");
		regItem("", "UC_HYOKA", $_unit_id, "新生児：バイタルサイン測定（心拍、体温、直腸温、呼吸数、SPO2）");
		regItem("", "UC_HYOKA", $_unit_id, "新生児：身体計測");
		regItem("", "UC_HYOKA", $_unit_id, "新生児処置：口鼻腔・胃内吸引、臍処置");
		regItem("", "UC_HYOKA", $_unit_id, "ビン哺乳");
		regItem("", "UC_HYOKA", $_unit_id, "沐浴");
		regItem("", "UC_HYOKA", $_unit_id, "新生児への予防的薬の与薬：点眼薬、Ｋ２シロップ");
		regItem("", "UC_HYOKA", $_unit_id, "新生児：血糖測定");
		regItem("", "UC_HYOKA", $_unit_id, "新生児：光線療法中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "産褥経過の観察と看護（経腟分娩、帝王切開術後）");
		regItem("", "UC_HYOKA", $_unit_id, "育児指導（授乳指導、沐浴指導、母児同室支援）");
		regItem("", "UC_HYOKA", $_unit_id, "退院指導（産後の生活指導、家族計画指導）");
		regItem("", "UC_HYOKA", $_unit_id, "産後の乳房ケア（乳汁分泌促進、乳房うっ滞、乳房鬱積、乳腺炎の対応）");
		regItem("", "UC_HYOKA", $_unit_id, "搾乳指導（搾乳方法と保存方法、母乳冷凍パックについて）");
		regItem("", "UC_HYOKA", $_unit_id, "子宮復古不全の対処と看護");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠悪阻患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "切迫流早産妊婦への看護");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠高血圧症候群妊婦への看護");
		regItem("", "UC_HYOKA", $_unit_id, "子宮内容除去術の介助および看護");
		regItem("", "UC_HYOKA", $_unit_id, "羊水検査・羊水吸引時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠中期中絶、死産分娩の処置の介助および看護");
		regItem("", "UC_HYOKA", $_unit_id, "無痛分娩（産科麻酔導入）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胎児治療（胎児胸水穿刺、胎児輸血）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "自己血貯血時（妊婦）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "周産期メンタル（妊娠期うつ・マタニティーブルー・産後うつ）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "分娩監視装置の装着、判読");
		regItem("", "UC_HYOKA", $_unit_id, "分娩誘発時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "新生児蘇生法（NCPR）");
	}
	if ($nm=="★1号館11階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "婦人科診察介助");
		regItem("", "UC_HYOKA", $_unit_id, "AUS介助");
		regItem("", "UC_HYOKA", $_unit_id, "産褥の看護");
		regItem("", "UC_HYOKA", $_unit_id, "手術前・後の看護（準備・オリエンテーション・帰室後・離床）");
		regItem("", "UC_HYOKA", $_unit_id, "手術に伴う肺血栓塞栓症の予防と弾性ストッキング装着");
		regItem("", "UC_HYOKA", $_unit_id, "婦人科手術後のリンパ浮腫指導とマッサージ");
		regItem("", "UC_HYOKA", $_unit_id, "胃カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "SBチューブ挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PCAポンプ(術後)使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PCAポンプ(CADD Legacy)使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "シリンジポンプ使用中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ストマ造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸水穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "腹水濾過濃縮再静注法（CART)の介助");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ドレーン挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎瘻造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "自己導尿中の患者指導");
		regItem("", "UC_HYOKA", $_unit_id, "持続皮下注射中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "皮下埋込型ポート造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "放射線療法中の患者の看護");
	}
	if ($nm=="★1号館12階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "回腸導管増設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎瘻増設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "尿管皮膚廔増設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱内灌流患者の看護");
	}
	if ($nm=="★1号館12階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ミエログラフィを行う患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "持続的他動運動（CPM）装着時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腓骨神経麻痺予防の看護");
		regItem("", "UC_HYOKA", $_unit_id, "直達牽引・介達牽引の看護");
		regItem("", "UC_HYOKA", $_unit_id, "アイシングシステム時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "前十字靭帯形成術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "人工股関節全置換術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "側彎症手術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎手術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "頚椎手術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "人工膝関節全置換術を受ける患者の看護");
	}
	if ($nm=="★1号館13階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "点眼・眼軟膏");
		regItem("", "UC_HYOKA", $_unit_id, "全身麻酔患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "局所麻酔の看護");
		regItem("", "UC_HYOKA", $_unit_id, "角膜移植・羊膜移植患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ステロイド内服している患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹臥位患者の看護");
	}
	if ($nm=="★1号館13階B病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "気管孔のある患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "摂食・嚥下障害のある患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "超選択的動注療法を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "放射線療法を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "耳鼻科領域の鼻の手術を受ける患者の周術期看護");
		regItem("", "UC_HYOKA", $_unit_id, "耳鼻科領域の耳の手術を受ける患者の周術期看護");
		regItem("", "UC_HYOKA", $_unit_id, "耳鼻科領域の頭頚部の手術を受ける患者の周術期看護");
	}
	if ($nm=="★1号館14階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ストマ造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "低圧持続吸引施行中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ドレーン類挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "術前オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "術直後のケア（ショックスコア含む）");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "放射線療法患者のケア");
		regItem("", "UC_HYOKA", $_unit_id, "人間ドック入院患者のケア");
		regItem("", "UC_HYOKA", $_unit_id, "接遇、ﾏﾅｰについて");
	}
	if ($nm=="★B棟6階A病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "食道・胃外科のドレーン管理");
		regItem("", "UC_HYOKA", $_unit_id, "肝・胆・膵外科のドレ―ン管理");
		regItem("", "UC_HYOKA", $_unit_id, "大腸肛門外科のドレーン管理");
		regItem("", "UC_HYOKA", $_unit_id, "脳神経外科のドレーン管理");
		regItem("", "UC_HYOKA", $_unit_id, "終夜脳波検査の看護");
		regItem("", "UC_HYOKA", $_unit_id, "肝移植患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＮＩＨＳＳの評価");
	}
	if ($nm=="★B棟10階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "乳腺センターマーキング案内方法");
		regItem("", "UC_HYOKA", $_unit_id, "乳腺RI説明・案内方法");
		regItem("", "UC_HYOKA", $_unit_id, "乳腺患者リハビリDVD視聴・拳上訓練指導");
		regItem("", "UC_HYOKA", $_unit_id, "リンパ浮腫マッサージ指導");
		regItem("", "UC_HYOKA", $_unit_id, "乳腺外科術後看護");
		regItem("", "UC_HYOKA", $_unit_id, "冠動脈造影検査（CAG）出棟方法");
		regItem("", "UC_HYOKA", $_unit_id, "冠動脈造影検査（CAG）後の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的冠動脈形成術（PCI）後の看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部消化管内視鏡を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "下部消化管内視鏡検査を受ける患者の前処置");
		regItem("", "UC_HYOKA", $_unit_id, "下部消化管内視鏡検査を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ステロイドﾞを使用する患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ステロイドパルスを施行する患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "乳がん術後の補整下着フィッティングと療養指導");
		regItem("", "UC_HYOKA", $_unit_id, "糖尿病患者の療養指導");
	}
	if ($nm=="★B棟11階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "経皮的血管形成術（PTA）案内");
		regItem("", "UC_HYOKA", $_unit_id, "シャント造設術案内");
		regItem("", "UC_HYOKA", $_unit_id, "頭部血管造影案内");
		regItem("", "UC_HYOKA", $_unit_id, "腎生検オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "腎生検介助");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈カテーテル挿入介助");
		regItem("", "UC_HYOKA", $_unit_id, "臍処置");
		regItem("", "UC_HYOKA", $_unit_id, "剃毛");
		regItem("", "UC_HYOKA", $_unit_id, "腹膜透析（CAPD）手術案内");
		regItem("", "UC_HYOKA", $_unit_id, "腹膜透析（CAPD）バッグ交換");
		regItem("", "UC_HYOKA", $_unit_id, "腹膜透析（CAPD）カテーテルケア");
		regItem("", "UC_HYOKA", $_unit_id, "腹膜透析（CAPD）バスコート");
		regItem("", "UC_HYOKA", $_unit_id, "中心静脈カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱留置カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "透析の引き継ぎ");
		regItem("", "UC_HYOKA", $_unit_id, "透析終了後迎え");
		regItem("", "UC_HYOKA", $_unit_id, "透析後患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "頭部血管造影後の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "シャント増設術後の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹膜透析の術後患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ブロック注射後の患者の看護");
	}
	if ($nm=="★B棟12階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "スパイナルドレーン挿入の介助と管理(ナーシングスキル)");
	}
	if ($nm=="★B棟13階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻からの経管栄養の注入");
		regItem("", "UC_HYOKA", $_unit_id, "嚥下障害のある患者の食事介助");
		regItem("", "UC_HYOKA", $_unit_id, "行動制限（身体抑制・拘束・離床ｾﾝｻｰﾍﾞｯﾄﾞ）");
		regItem("", "UC_HYOKA", $_unit_id, "歩行障害のある患者の歩行介助");
		regItem("", "UC_HYOKA", $_unit_id, "ストレッチャーを使用しての入浴介助");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養介助");
		regItem("", "UC_HYOKA", $_unit_id, "胃カテーテル挿入の介助");
		regItem("", "UC_HYOKA", $_unit_id, "胃カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱留置カテーテル挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CV挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管吸引（挿管ﾁｭｰﾌﾞ）");
		regItem("", "UC_HYOKA", $_unit_id, "気管吸引（気管切開）");
		regItem("", "UC_HYOKA", $_unit_id, "気管吸引(閉鎖式吸引)");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "滅菌綿棒による鼻前庭からの検体採取");
		regItem("", "UC_HYOKA", $_unit_id, "残尿測定");
		regItem("", "UC_HYOKA", $_unit_id, "自然排尿のための看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃瘻造設術前後の管理");
		regItem("", "UC_HYOKA", $_unit_id, "脳血管造影を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "筋生検術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "難病患者の意思決定支援");
		regItem("", "UC_HYOKA", $_unit_id, "退院支援");
		regItem("", "UC_HYOKA", $_unit_id, "迷惑行為（暴力、セクハラ）のある患者の対応");
	}
	if ($nm=="★B棟14階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ストーマ装具交換");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管低圧持続吸引管理");
		regItem("", "UC_HYOKA", $_unit_id, "褥瘡の処置");
		regItem("", "UC_HYOKA", $_unit_id, "持続硬膜外麻酔");
		regItem("", "UC_HYOKA", $_unit_id, "手術前オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "手術直後のケア");
		regItem("", "UC_HYOKA", $_unit_id, "バイタルサインの測定（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "輸液管理（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "創傷浸出液の管理（パウチ、ドレーン）");
		regItem("", "UC_HYOKA", $_unit_id, "陰圧閉鎖療法");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡検査");
		regItem("", "UC_HYOKA", $_unit_id, "注腸造影検査");
		regItem("", "UC_HYOKA", $_unit_id, "腹部超音波検査");
	}
	if ($nm=="★B棟15階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "包交介助（サルベージ）");
		regItem("", "UC_HYOKA", $_unit_id, "包交介助（肝移植後）");
		regItem("", "UC_HYOKA", $_unit_id, "膵管チューブの管理");
		regItem("", "UC_HYOKA", $_unit_id, "ﾄﾞﾚｰﾝの管理（肝切離面・肝下面・横隔膜・腸瘻など）");
		regItem("", "UC_HYOKA", $_unit_id, "PTCD・PTBDドレーンの管理");
		regItem("", "UC_HYOKA", $_unit_id, "ENBDドレーンの管理");
		regItem("", "UC_HYOKA", $_unit_id, "腸瘻クランプ時の管理");
		regItem("", "UC_HYOKA", $_unit_id, "肝移植前の看護");
		regItem("", "UC_HYOKA", $_unit_id, "肝移植後の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃カテーテル抜去");
		regItem("", "UC_HYOKA", $_unit_id, "肝動脈化学塞栓療法の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜外ｶﾃｰﾃﾙ挿入中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PCAﾎﾟﾝﾌﾟ使用中の看護");
	}
	if ($nm=="★B棟16階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ＳＢチューブ挿入中の患者の看護");
	}
	if ($nm=="★B棟17階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "GSを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CSを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "注腸を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ERCPを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFAを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "TACE（TAE）を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ENBD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PTCD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "メラサキュームの管理");
		regItem("", "UC_HYOKA", $_unit_id, "ポリペクトミーを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部ESDを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "下部ESDを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "EIS・EVLを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "EUS‐FNAを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "エコー下肝腫瘍生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "エコー下肝生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "パテンシーカプセルを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "カプセル内視鏡を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "モニラック浣腸");
		regItem("", "UC_HYOKA", $_unit_id, "膵液細胞診の採取方法");
		regItem("", "UC_HYOKA", $_unit_id, "手術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔鏡下肝生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CARTを受ける患者の看護");
	}
	if ($nm=="★B棟18階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "GSを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CSを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "注腸を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ERCPを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFAを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "TACE（TAE）を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ENBD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PTCD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "メラサキュームの管理");
		regItem("", "UC_HYOKA", $_unit_id, "ポリペクトミーを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部ESDを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "下部ESDを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "EIS・EVLを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "EUS‐FNAを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "エコー下肝腫瘍生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "エコー下肝生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "パテンシーカプセルを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "カプセル内視鏡を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "モニラック浣腸");
		regItem("", "UC_HYOKA", $_unit_id, "膵液細胞診の採取方法");
		regItem("", "UC_HYOKA", $_unit_id, "手術を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔鏡下肝生検を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CARTを受ける患者の看護");
	}
	if ($nm=="★B棟19・20階病棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ストマ造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "低圧持続吸引施行中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ドレーン類挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "術前オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "術直後のケア（ショックスコア含む）");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔穿刺の介助、ケア");
		regItem("", "UC_HYOKA", $_unit_id, "放射線療法患者のケア");
		regItem("", "UC_HYOKA", $_unit_id, "人間ドック入院患者のケア");
		regItem("", "UC_HYOKA", $_unit_id, "接遇、ﾏﾅｰについて");
	}

	if ($nm=="★救急PC") {
		regItem("", "UC_HYOKA", $_unit_id, "膀胱灌流（持続膀胱洗浄）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "間歇的自己導尿指導");
		regItem("", "UC_HYOKA", $_unit_id, "除毛");
		regItem("", "UC_HYOKA", $_unit_id, "低圧持続吸引：胸腔ドレーンの挿入後の看護");
		regItem("", "UC_HYOKA", $_unit_id, "イレウス管");
		regItem("", "UC_HYOKA", $_unit_id, "薬剤アレルギー発症時の対応");
		regItem("", "UC_HYOKA", $_unit_id, "外用薬与薬：軟膏・貼付薬");
		regItem("", "UC_HYOKA", $_unit_id, "経鼻的胃管挿入");
		regItem("", "UC_HYOKA", $_unit_id, "検体の採取方法：胃液培養");
		regItem("", "UC_HYOKA", $_unit_id, "肺血栓塞栓症・深部静脈血栓症の予防");
		regItem("", "UC_HYOKA", $_unit_id, "手術前オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺");
		regItem("", "UC_HYOKA", $_unit_id, "食道・胃・十二指腸内視鏡");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡");
		regItem("", "UC_HYOKA", $_unit_id, "放射線の曝露防止");
		regItem("", "UC_HYOKA", $_unit_id, "心臓血管造影");
		regItem("", "UC_HYOKA", $_unit_id, "下部消化管造影検査（注腸造影）");
		regItem("", "UC_HYOKA", $_unit_id, "上部消化管造影検査（胃透視）");
		regItem("", "UC_HYOKA", $_unit_id, "CT");
		regItem("", "UC_HYOKA", $_unit_id, "MRI");
		regItem("", "UC_HYOKA", $_unit_id, "心臓超音波検査");
		regItem("", "UC_HYOKA", $_unit_id, "入院時の対応");
		regItem("", "UC_HYOKA", $_unit_id, "退院・転院時の対応");
		regItem("", "UC_HYOKA", $_unit_id, "患者・家族の苦情への対応");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠中の検査：超音波検査");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠中の検査：腹囲・子宮底測定");
		regItem("", "UC_HYOKA", $_unit_id, "新生児の観察：全身観察");
		regItem("", "UC_HYOKA", $_unit_id, "静脈採血（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "静脈穿刺（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "輸液管理（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "プレパレーション");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：眼");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：直腸・肛門");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：生殖器");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：一般状態");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：基本手技（視診・触診・打診・聴診）");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：精神状態");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：頭頸部");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：耳・鼻・口腔・咽頭・喉頭");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：胸部：肺");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：胸部：乳房");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：心臓");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：循環");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：腹部");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：筋骨格");
		regItem("", "UC_HYOKA", $_unit_id, "フィジカルアセスメント：神経");
		regItem("", "UC_HYOKA", $_unit_id, "血液透析：内シャント");
		regItem("", "UC_HYOKA", $_unit_id, "血液透析：バスキュラーアクセスカテーテル");
		regItem("", "UC_HYOKA", $_unit_id, "心電図の読み方：ST波形の観察");
		regItem("", "UC_HYOKA", $_unit_id, "腹腔穿刺");
		regItem("", "UC_HYOKA", $_unit_id, "創傷浸出液の管理：パウチ・ドレーン");
		regItem("", "UC_HYOKA", $_unit_id, "熱傷のケア");
		regItem("", "UC_HYOKA", $_unit_id, "一時ペーシング：経静脈的・心外膜的（介助）");
		regItem("", "UC_HYOKA", $_unit_id, "直達・介達牽引時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開における患者の管理");
		regItem("", "UC_HYOKA", $_unit_id, "スパイナルドレナージ");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔穿刺");
		regItem("", "UC_HYOKA", $_unit_id, "気管切開：介助");
		regItem("", "UC_HYOKA", $_unit_id, "統合失調症患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "精神科における情報収集とアセスメント");
		regItem("", "UC_HYOKA", $_unit_id, "自殺のリスクがある患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "隔離・身体拘束に伴う援助");
		regItem("", "UC_HYOKA", $_unit_id, "廃用症候群の予防");
		regItem("", "UC_HYOKA", $_unit_id, "義歯の取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "誤嚥予防");
		regItem("", "UC_HYOKA", $_unit_id, "災害時への備え（防災マニュアル）");
		regItem("", "UC_HYOKA", $_unit_id, "災害時の対応・トリアージ　(院内における");
		regItem("", "UC_HYOKA", $_unit_id, "外国人患者への対応：入院時の対応（英語）");
		regItem("", "UC_HYOKA", $_unit_id, "外国人患者への対応：日常のケア（英語）");
		regItem("", "UC_HYOKA", $_unit_id, "外国人患者への対応：退院時の対応（英語）");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的ペーシング");
		regItem("", "UC_HYOKA", $_unit_id, "外傷創部の洗浄");
		regItem("", "UC_HYOKA", $_unit_id, "有害物質除去時の胃洗浄");
		regItem("", "UC_HYOKA", $_unit_id, "輪状甲状靭帯切開（介助）");
		regItem("", "UC_HYOKA", $_unit_id, "NPPV（非侵襲的陽圧換気療法）");
	}
	if ($nm=="★１A外来") {
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（喀痰）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（呼気テスト）");
		regItem("", "UC_HYOKA", $_unit_id, "髄注介助");
		regItem("", "UC_HYOKA", $_unit_id, "瀉血実施時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺の看護");
		regItem("", "UC_HYOKA", $_unit_id, "貯血の看護");
	}
	if ($nm=="★ハートセンター") {
		regItem("", "UC_HYOKA", $_unit_id, "トロップチェック、ラピチェック");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養の介助");
		regItem("", "UC_HYOKA", $_unit_id, "硬化療法の介助");
		regItem("", "UC_HYOKA", $_unit_id, "OGTT検査");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（30分臥床安静採血）");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（鼻腔培養）");
		regItem("", "UC_HYOKA", $_unit_id, "外来DCの介助");
	}
	if ($nm=="★呼吸器内科") {
		regItem("", "UC_HYOKA", $_unit_id, "吸入指導");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "胃液培養の方法と検体取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "外注採血の方法と検体取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "鼻腔・咽頭培養の方法と検体の取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "ベナンバックス吸入の方法");
		regItem("", "UC_HYOKA", $_unit_id, "結核（疑い）患者の対応");
	}
	if ($nm=="★膠原・糖・内分泌") {
		regItem("", "UC_HYOKA", $_unit_id, "OGTT（75ｇ糖負荷試験）の準備と実施ができる");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "ガムテストの準備と実施ができる");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養検査の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "関節内注射の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "自己血糖測定の患者（家族）指導ができる");
		regItem("", "UC_HYOKA", $_unit_id, "インスリン自己注射の患者（家族）指導ができる");
		regItem("", "UC_HYOKA", $_unit_id, "生物学的製剤自己注射の患者（家族）指導ができる");
		regItem("", "UC_HYOKA", $_unit_id, "３０分安静臥床採血の準備と実施ができる");
	}
	if ($nm=="★消化器内科") {
		regItem("", "UC_HYOKA", $_unit_id, "PTCD挿入している患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "ヒュミラの自己注射指導");
		regItem("", "UC_HYOKA", $_unit_id, "直腸 診を受ける患者の介助");
		regItem("", "UC_HYOKA", $_unit_id, "腹部超音波検査の介助");
		regItem("", "UC_HYOKA", $_unit_id, "尿素呼気テストを受ける患者への説明・実施");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CTC検査を受ける患者への説明");
		regItem("", "UC_HYOKA", $_unit_id, "ポート増設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "在宅IVH中の患者の看護");
	}
	if ($nm=="★消化器外科・呼吸器外科") {
		regItem("", "UC_HYOKA", $_unit_id, "胃カメラを受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "注腸検査を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "胃透視を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "超音波を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "DIC-CTを受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "気管支鏡を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "PET-CTを受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "CT（造影含む）を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "MRI（造影含む）を受ける患者の看護（検査説明・終了後の観察項目など）");
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（便培・便潜血）");
		regItem("", "UC_HYOKA", $_unit_id, "血液培養採取時の介助");
		regItem("", "UC_HYOKA", $_unit_id, "採血の介助（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "呼気テストの検体採取");
		regItem("", "UC_HYOKA", $_unit_id, "胸水穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "腹水穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "直腸診の介助");
		regItem("", "UC_HYOKA", $_unit_id, "永久気管孔の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "在宅IVH中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔ドレーン挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "在宅酸素療法中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ストマ造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ポート造設中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PTCD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ENBD挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膵管挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腸ろう挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ﾍﾟﾝﾛｰｽﾞﾄﾞﾚｰﾝ（開放ﾄﾞﾚｰﾝ）挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "食道がん放射線・化学療法を受ける患者の看護");
	}
	if ($nm=="★乳腺センター") {
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "CV挿入中のポートフラッシュ");
		regItem("", "UC_HYOKA", $_unit_id, "胸腔穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "セローマ穿刺の介助");
		regItem("", "UC_HYOKA", $_unit_id, "診察介助（触診）");
		regItem("", "UC_HYOKA", $_unit_id, "診察介助（包帯交換）");
		regItem("", "UC_HYOKA", $_unit_id, "エキスパンダーの介助");
		regItem("", "UC_HYOKA", $_unit_id, "縫合処置の介助");
		regItem("", "UC_HYOKA", $_unit_id, "細菌培養");
		regItem("", "UC_HYOKA", $_unit_id, "便潜血検査");
		regItem("", "UC_HYOKA", $_unit_id, "細胞診・組織診");
		regItem("", "UC_HYOKA", $_unit_id, "血液ガス採取の介助");
		regItem("", "UC_HYOKA", $_unit_id, "退院後リンパ浮腫指導");
		regItem("", "UC_HYOKA", $_unit_id, "リンパ浮腫指導");
		regItem("", "UC_HYOKA", $_unit_id, "化学療法室を利用する患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "局所処置の必要な患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "在宅酸素導入");
		regItem("", "UC_HYOKA", $_unit_id, "他院借用プレパラートの取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "当院貸出プレパラートの取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "FISH検査");
	}
	if ($nm=="★脳神経内科外科") {
		regItem("", "UC_HYOKA", $_unit_id, "小児患者の身体計測：身長・体重・頭囲");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "小児患者の与薬");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺（タップテスト）介助");
		regItem("", "UC_HYOKA", $_unit_id, "貯血オリエンテーション");
		regItem("", "UC_HYOKA", $_unit_id, "シャント圧調節介助");
		regItem("", "UC_HYOKA", $_unit_id, "OGTT");
		regItem("", "UC_HYOKA", $_unit_id, "テンシロンテスト介助");
		regItem("", "UC_HYOKA", $_unit_id, "バクロフェン注入介助");
		regItem("", "UC_HYOKA", $_unit_id, "迷走神経刺激外来介助");
		regItem("", "UC_HYOKA", $_unit_id, "インターフェロン自己注射指導");
		regItem("", "UC_HYOKA", $_unit_id, "アポカイン自己注射指導");
	}
	if ($nm=="★皮膚科・形成外科") {
		regItem("", "UC_HYOKA", $_unit_id, "検体採取（蛍光抗体採血）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "外来手術介助");
		regItem("", "UC_HYOKA", $_unit_id, "緊急処置（生検、縫合）介助");
		regItem("", "UC_HYOKA", $_unit_id, "鼻骨整復術介助");
		regItem("", "UC_HYOKA", $_unit_id, "バイポーラ・エルマン使用方法");
		regItem("", "UC_HYOKA", $_unit_id, "弾性ストッキング測定方法");
		regItem("", "UC_HYOKA", $_unit_id, "リンデロン局注止血介助");
		regItem("", "UC_HYOKA", $_unit_id, "ケナコルト局注準備方法");
		regItem("", "UC_HYOKA", $_unit_id, "プロプラノール投与開始時、増量時の準備、説明方法");
		regItem("", "UC_HYOKA", $_unit_id, "ボトックス請求、準備、使用後の処理方法");
		regItem("", "UC_HYOKA", $_unit_id, "SPP測定");
	}
	if ($nm=="★整形外科") {
		regItem("", "UC_HYOKA", $_unit_id, "患部固定術介助：シーネ");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "患部固定術介助：オルソグラス（ギプス）");
		regItem("", "UC_HYOKA", $_unit_id, "患部固定術介助：石膏ギプス");
		regItem("", "UC_HYOKA", $_unit_id, "ギプスカット");
		regItem("", "UC_HYOKA", $_unit_id, "松葉杖調整");
		regItem("", "UC_HYOKA", $_unit_id, "患部クーリング");
		regItem("", "UC_HYOKA", $_unit_id, "三角巾");
		regItem("", "UC_HYOKA", $_unit_id, "テゾー固定法");
		regItem("", "UC_HYOKA", $_unit_id, "抜糸・抜鉤");
		regItem("", "UC_HYOKA", $_unit_id, "関節内注射介助");
		regItem("", "UC_HYOKA", $_unit_id, "トリガーポイント注射介助");
		regItem("", "UC_HYOKA", $_unit_id, "整復術介助");
		regItem("", "UC_HYOKA", $_unit_id, "小児採血介助・実施");
		regItem("", "UC_HYOKA", $_unit_id, "松葉杖使用方法指導");
		regItem("", "UC_HYOKA", $_unit_id, "自己注射指導");
		regItem("", "UC_HYOKA", $_unit_id, "小児セデーションの看護");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎バンド装着、指導");
		regItem("", "UC_HYOKA", $_unit_id, "針生検介助");
		regItem("", "UC_HYOKA", $_unit_id, "ブロック注射介助");
		regItem("", "UC_HYOKA", $_unit_id, "頚椎カラー装着、指導");
		regItem("", "UC_HYOKA", $_unit_id, "クラビクルバンド装着介助、指導");
	}
	if ($nm=="★泌尿器科・腎臓内科") {
		regItem("", "UC_HYOKA", $_unit_id, "膀胱瘻交換介助");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "陰嚢穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱鏡検査介助（軟性鏡）");
		regItem("", "UC_HYOKA", $_unit_id, "軟性鏡洗浄方法");
		regItem("", "UC_HYOKA", $_unit_id, "尿管ステント抜去介助");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱洗浄介助");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱留置カテーテル交換介助");
		regItem("", "UC_HYOKA", $_unit_id, "前立腺生検介助");
		regItem("", "UC_HYOKA", $_unit_id, "自己導尿指導");
		regItem("", "UC_HYOKA", $_unit_id, "超音波検査介助（腎・膀胱・陰嚢・精巣・前立腺）");
		regItem("", "UC_HYOKA", $_unit_id, "ウロフロメトリー");
		regItem("", "UC_HYOKA", $_unit_id, "ウロダイナミクス介助");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱留置カテーテル抜去トライ");
		regItem("", "UC_HYOKA", $_unit_id, "ＢＣＧ膀胱内注入療法");
		regItem("", "UC_HYOKA", $_unit_id, "残尿測定");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱潅流介助と看護");
	}
	if ($nm=="★眼科") {
		regItem("", "UC_HYOKA", $_unit_id, "眼科診療機器・器具の用途・使い方がわかる。");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "眼鏡処方箋の取り扱いができる");
		regItem("", "UC_HYOKA", $_unit_id, "EKC患者の対応ができる");
		regItem("", "UC_HYOKA", $_unit_id, "FAG検査の指示受け、説明、準備、介助、観察ができる");
		regItem("", "UC_HYOKA", $_unit_id, "FA/IA検査の指示受け、説明、準備、介助、観察ができる。");
		regItem("", "UC_HYOKA", $_unit_id, "外来手術のオリエンテーションができる");
		regItem("", "UC_HYOKA", $_unit_id, "白内障手術のオリエンテーションができる。");
		regItem("", "UC_HYOKA", $_unit_id, "硝子体手術のオリエンテーションができる");
		regItem("", "UC_HYOKA", $_unit_id, "硝子体手術の準備ができる。");
		regItem("", "UC_HYOKA", $_unit_id, "硝子体手術の介助・術後観察ができる。");
		regItem("", "UC_HYOKA", $_unit_id, "洗眼の準備、介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "結膜下注射の準備、介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "涙道洗浄の準備、介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "シェッツ眼圧測定の準備、介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "シルマー試験が行える");
		regItem("", "UC_HYOKA", $_unit_id, "霰粒腫切開術の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "涙道内視鏡の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "涙嚢鼻腔吻合術後の処置介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "血清点眼作成の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "涙点プラグ挿入の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "ケナコルトテノン嚢下注射の準備と介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "医療用コンタクトレンズの挿入ができる");
		regItem("", "UC_HYOKA", $_unit_id, "ツベルクリン反応検査の介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "PTKのオリエンテーションができる");
		regItem("", "UC_HYOKA", $_unit_id, "PTKの術前、術中、術後ケアができる");
		regItem("", "UC_HYOKA", $_unit_id, "LASIKのオリエンテーションができる");
		regItem("", "UC_HYOKA", $_unit_id, "LASIKの術前、術中、術後ケアができる");
		regItem("", "UC_HYOKA", $_unit_id, "角膜抜糸の介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "小児眼科診察介助ができる");
		regItem("", "UC_HYOKA", $_unit_id, "小児の採血ができる");
		regItem("", "UC_HYOKA", $_unit_id, "小児の術前オリエンテーションができる");
		regItem("", "UC_HYOKA", $_unit_id, "白内障外来手術当日の術前処置と手術室への案内ができる。");
	}
	if ($nm=="★ペインクリニック") {
		regItem("", "UC_HYOKA", $_unit_id, "神経ブロックの準備・作成");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "神経ブロックの介助");
		regItem("", "UC_HYOKA", $_unit_id, "神経ブロック後の安静解除基準の理解");
		regItem("", "UC_HYOKA", $_unit_id, "神経ブロック後の患者指導");
		regItem("", "UC_HYOKA", $_unit_id, "透視下ブロックの準備・作成");
		regItem("", "UC_HYOKA", $_unit_id, "透視下ブロックの介助");
		regItem("", "UC_HYOKA", $_unit_id, "レーザー治療の介助");
		regItem("", "UC_HYOKA", $_unit_id, "鎮静前評価の依頼");
		regItem("", "UC_HYOKA", $_unit_id, "タイムアウト");
		regItem("", "UC_HYOKA", $_unit_id, "鎮静処置介助");
		regItem("", "UC_HYOKA", $_unit_id, "鎮静解除基準の理解");
		regItem("", "UC_HYOKA", $_unit_id, "ＩＲＳＢの準備");
		regItem("", "UC_HYOKA", $_unit_id, "ＩＲＳＢの準備の介助");
		regItem("", "UC_HYOKA", $_unit_id, "ボトックスの取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "ドラッグチャレンジテストの準備");
		regItem("", "UC_HYOKA", $_unit_id, "ドラッグチャレンジテストの介助");
		regItem("", "UC_HYOKA", $_unit_id, "エピドラスコピーの説明・指導");
		regItem("", "UC_HYOKA", $_unit_id, "SCSトライアルの準備");
		regItem("", "UC_HYOKA", $_unit_id, "SCS挿入患者への説明・指導");
		regItem("", "UC_HYOKA", $_unit_id, "慢性疼痛に対する麻薬処方の理解");
		regItem("", "UC_HYOKA", $_unit_id, "三角巾の巻き方");
	}
	if ($nm=="★産科・婦人科") {
		regItem("", "UC_HYOKA", $_unit_id, "褥婦の保健指導");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "集団指導（母親学級、両親学級）");
		regItem("", "UC_HYOKA", $_unit_id, "採卵、胚移植の介助");
		regItem("", "UC_HYOKA", $_unit_id, "リプロ外来患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腫瘍外来患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ラパロ外来患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "子宮鏡検査の介助");
		regItem("", "UC_HYOKA", $_unit_id, "子宮頸癌、体癌検査の介助");
		regItem("", "UC_HYOKA", $_unit_id, "コルポスコピー検査の介助");
		regItem("", "UC_HYOKA", $_unit_id, "人工授精の介助");
		regItem("", "UC_HYOKA", $_unit_id, "OGTT　GCT検査の実施");
		regItem("", "UC_HYOKA", $_unit_id, "創部処置の介助");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠初期の看護（分娩予約）");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠中期の看護");
		regItem("", "UC_HYOKA", $_unit_id, "妊娠後期の看護");
		regItem("", "UC_HYOKA", $_unit_id, "緊急時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "子宮脱の看護（ペッサリー装着）");
		regItem("", "UC_HYOKA", $_unit_id, "性器出血の看護（リープ）");
		regItem("", "UC_HYOKA", $_unit_id, "子宮内膣内操作の介助");
	}
	if ($nm=="★耳鼻咽喉科") {
		regItem("", "UC_HYOKA", $_unit_id, "検体採取(咽頭・耳孔・鼻粘膜培養)と検体の取り扱い");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "耳浴・耳洗を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "鼓膜麻酔を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ファイバー下生検・細胞診を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "リップバイオプシーを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "穿刺吸引細胞診を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "鼻骨骨折整復を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "上顎洞穿刺・洗浄を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "鼻洗浄を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "減感作・舌下免疫療法を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "声帯コラーゲン注入を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "アルゴンプラズマレーザー治療を受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小児の診察介助");
	}
	if ($nm=="★小児科・小児外科") {
		regItem("", "UC_HYOKA", $_unit_id, "身体の計測：身長・体重・頭囲・胸囲・腹囲（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "ﾊﾞｲﾀﾙｻｲﾝの測定：腋窩検温（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "ﾊﾞｲﾀﾙｻｲﾝの測定：脈拍・心拍（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "ﾊﾞｲﾀﾙｻｲﾝの測定：血圧　(小児）");
		regItem("", "UC_HYOKA", $_unit_id, "バイタルサイン測定：経皮的動脈血酸素飽和度（SPO2)　(小児）");
		regItem("", "UC_HYOKA", $_unit_id, "静脈採血（小児）・採血の介助");
		regItem("", "UC_HYOKA", $_unit_id, "静脈穿刺の介助（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "輸液管理（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "経管栄養：経鼻胃管の介助(小児）");
		regItem("", "UC_HYOKA", $_unit_id, "経鼻的胃管挿入の介助（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "排泄介助：腹部マッサージ・肛門刺激（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "転倒・転落の予防（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "プレパレーション");
		regItem("", "UC_HYOKA", $_unit_id, "経口与薬（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "直腸内与薬（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "浣腸（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "起立性調節障害診断テスト（ODテスト）");
		regItem("", "UC_HYOKA", $_unit_id, "呼気テスト(小児）");
		regItem("", "UC_HYOKA", $_unit_id, "骨髄穿刺（小児）");
		regItem("", "UC_HYOKA", $_unit_id, "腰椎穿刺（小児）");
	}
	if ($nm=="★メンタルクリニック") {
		regItem("", "UC_HYOKA", $_unit_id, "任意入院時の患者、家族への説明と対応");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
	}
	if ($nm=="★透析療法室") {
		regItem("", "UC_HYOKA", $_unit_id, "体重測定（体重計）");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "透析回路のテープ固定");
		regItem("", "UC_HYOKA", $_unit_id, "カテーテルからの透析開始・終了介助");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの血液ガス測定");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの血糖測定");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの採血");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの点滴（輸液ポンプ）");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの点滴（シリンジポンプ）");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの輸血(RBC)");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの輸血(PC)");
		regItem("", "UC_HYOKA", $_unit_id, "回路からの輸血（FFP）");
		regItem("", "UC_HYOKA", $_unit_id, "透析用穿刺針の抜針");
		regItem("", "UC_HYOKA", $_unit_id, "透析中のトイレ離脱");
		regItem("", "UC_HYOKA", $_unit_id, "透析中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "血漿交換中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "外来CAPD患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PTAを受ける患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ブラッドアクセスの管理");
		regItem("", "UC_HYOKA", $_unit_id, "日本腹膜透析医学会認定　CAPD認定指導看護師");
		regItem("", "UC_HYOKA", $_unit_id, "日本腎不全看護学会認定　透析療法指導看護師");
		regItem("", "UC_HYOKA", $_unit_id, "日本看護協会認定　透析看護認定看護師");
	}
	if ($nm=="★放射線1号館、B棟") {
		regItem("", "UC_HYOKA", $_unit_id, "ＣＴ検査の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "血管外漏出時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "造影剤副作用時の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＴコロノグラフィー検査の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＴコロナリー検査の看護");
		regItem("", "UC_HYOKA", $_unit_id, "放射線治療中患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "放射線治療計画ＣＴの看護");
		regItem("", "UC_HYOKA", $_unit_id, "内診介助");
		regItem("", "UC_HYOKA", $_unit_id, "全身照射の看護");
		regItem("", "UC_HYOKA", $_unit_id, "定位手術的照射の看護");
		regItem("", "UC_HYOKA", $_unit_id, "眼瞼照射の看護");
		regItem("", "UC_HYOKA", $_unit_id, "試作水晶体遮断用コンタクト型ブロックの運用");
		regItem("", "UC_HYOKA", $_unit_id, "ケロイド照射の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＫＯＲＴＵＣ　�気隆埜�");
		regItem("", "UC_HYOKA", $_unit_id, "ＫＯＲＴＵＣ　�兇隆埜�");
		regItem("", "UC_HYOKA", $_unit_id, "頭頸部癌ＩＭＲＴ患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "前立腺癌ＩＭＲＴ患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "前立腺癌強度変調放射線位置照合システムの治療開始までの流れ");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的放射線治療用金属マーカー留置術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "Ｄ棟放射線治療室看護業務の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＲＡＬＳの看護");
		regItem("", "UC_HYOKA", $_unit_id, "軟性内視鏡の取扱い");
		regItem("", "UC_HYOKA", $_unit_id, "ＩＶＲ医師診察介助");
		regItem("", "UC_HYOKA", $_unit_id, "血管造影標準看護");
		regItem("", "UC_HYOKA", $_unit_id, "血管造影資材管理");
		regItem("", "UC_HYOKA", $_unit_id, "血管造影資材コスト入力");
		regItem("", "UC_HYOKA", $_unit_id, "抗癌剤の取扱い");
		regItem("", "UC_HYOKA", $_unit_id, "動脈化学塞栓術（TACE）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "動静脈奇形（ＡＶＭ）塞栓術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎血管筋脂肪腫（ＡＭＬ）塞栓術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "子宮動脈塞栓術（ＵＡＥ）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "内腸骨動脈バルーン閉鎖術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎動脈瘤塞栓術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的血管形成術（ＰＴＡ）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＶポート造設術　の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＶポート抜去術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＩＶＨ挿入術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "副腎静脈サンプリングの看護");
		regItem("", "UC_HYOKA", $_unit_id, "膵サンプリングの看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＴガイド下生検術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＣＴガイド下ドレナージ術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "骨盤動注の看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管支動脈塞栓術（ＢＡＥ）の看護");
		regItem("", "UC_HYOKA", $_unit_id, "経静脈的肝生検の看護");
		regItem("", "UC_HYOKA", $_unit_id, "耳鼻科動注セルジンガー法の看護");
		regItem("", "UC_HYOKA", $_unit_id, "耳鼻科動注ＳＴＡ法の看護");
		regItem("", "UC_HYOKA", $_unit_id, "全身麻酔の手術看護");
		regItem("", "UC_HYOKA", $_unit_id, "Aライン挿入の介助");
		regItem("", "UC_HYOKA", $_unit_id, "脳神経外科アンギオ診断看護");
		regItem("", "UC_HYOKA", $_unit_id, "脳動脈瘤塞栓術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "頚動脈ステント留置術（CAS)の看護");
		regItem("", "UC_HYOKA", $_unit_id, "BOTの看護");
		regItem("", "UC_HYOKA", $_unit_id, "和田テストの看護");
		regItem("", "UC_HYOKA", $_unit_id, "手術使用特殊物品");
		regItem("", "UC_HYOKA", $_unit_id, "スパイラルドレナージ挿入の看護");
		regItem("", "UC_HYOKA", $_unit_id, "硬膜動静脈奇形塞栓術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "術前・術後訪問");
		regItem("", "UC_HYOKA", $_unit_id, "透視室標準看護");
		regItem("", "UC_HYOKA", $_unit_id, "経静脈性腎盂造影（IP)の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胃透視（MDL)の看護");
		regItem("", "UC_HYOKA", $_unit_id, "マンモトームの看護");
		regItem("", "UC_HYOKA", $_unit_id, "ヒステログラフィの看護");
		regItem("", "UC_HYOKA", $_unit_id, "逆行性腎盂造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "尿管ステント交換の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎盂バルーンカテーテル交換の看護");
		regItem("", "UC_HYOKA", $_unit_id, "点滴静脈性腎盂造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膀胱造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "チェーンCGの看護");
		regItem("", "UC_HYOKA", $_unit_id, "尿道造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "スプリント造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "ボリュームスタディの看護");
		regItem("", "UC_HYOKA", $_unit_id, "関節造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "脊髄造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "神経根ブロックの看護");
		regItem("", "UC_HYOKA", $_unit_id, "椎間板造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "椎体形成術の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小児膀胱造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小児食道造影・胃透視の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小児注腸の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腸重積の修正の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小児経静脈性腎盂造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "EDチューブ挿入の看護");
		regItem("", "UC_HYOKA", $_unit_id, "PHモニター挿入の看護");
		regItem("", "UC_HYOKA", $_unit_id, "小腸造影の看護");
		regItem("", "UC_HYOKA", $_unit_id, "腎ろう造影の看護");
	}
	if ($nm=="★内視鏡室") {
		regItem("", "UC_HYOKA", $_unit_id, "上部検査準備・片付け");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡後の安静室看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（クリップ法）");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（ＨＳＥ局注法）");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（純エタノール局注法）");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（高周波凝固法）");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（アルゴンプラズマ凝固法：ＡＰＣ）");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査介助・看護（アルト法）");
		regItem("", "UC_HYOKA", $_unit_id, "食道ブジー介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "食道アカラシアブジー介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "食道ステント介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡的食道胃静脈瘤結紮術（ＥＶＬ）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡的硬化療法（ＥＩＳ）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡的逆行性胆道膵管撮影法（ＥＲＣＰ）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡的経鼻胆道ﾄﾞﾚﾅｰｼﾞ（ＥＮＢＤ）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡的十二指腸乳頭括約筋切開術（ＥＳＴ）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "超音波内視鏡検査（ＥＵＳ/消内）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "超音波内視鏡検査（ＥＵＳ/食道胃）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "超音波内視鏡検査（ＥＵＳ/肝胆膵）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "超音波内視鏡検査（ＥＵＳーＦＮＡ/肝胆膵）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "超音波内視鏡検査（ＥＵＳ/大腸）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部内視鏡検査（精査/食道胃）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部ＥＳＤ（食道胃）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部ＥＳＤ（消内）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "上部ＥＭＲ（消内）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "経鼻内視鏡検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "経皮的内視鏡的胃瘻増設術（ＰＥＧ/食道胃）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "気管支検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "ＥＵＳ−ＴＢＮＡ検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡検査介助・看護（止血術）");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡検査介助・看護（マーキング）");
		regItem("", "UC_HYOKA", $_unit_id, "大腸内視鏡検査介助・看護（ポリペクトミー・ＥＭＲ）");
		regItem("", "UC_HYOKA", $_unit_id, "下部ＥＳＤ（消内）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "下部ＥＳＤ（大腸）介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "大腸ステント介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "大腸イレウス管介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "小腸内視鏡検査介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "下部緊急内視鏡介助・看護");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡洗浄・取扱い（洗浄・消毒・滅菌について）");
	}
	if ($nm=="★健康スポーツ室") {
		regItem("", "UC_HYOKA", $_unit_id, "吸入指導");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "関節注射準備と介助");
		regItem("", "UC_HYOKA", $_unit_id, "ボトックス注射介助");
		regItem("", "UC_HYOKA", $_unit_id, "各種ワクチン接種の取り扱いと介助");
		regItem("", "UC_HYOKA", $_unit_id, "トレッドミル検査介助");
		regItem("", "UC_HYOKA", $_unit_id, "心肺運動負荷試験の補助");
		regItem("", "UC_HYOKA", $_unit_id, "心臓リハビリテーションの看護");
		regItem("", "UC_HYOKA", $_unit_id, "和温療法の看護");
		regItem("", "UC_HYOKA", $_unit_id, "人間ドック受診者の対応と検査の確認");
		regItem("", "UC_HYOKA", $_unit_id, "内視鏡後の患者の看護");
	}
	if ($nm=="★患者・看護相談室") {
		regItem("", "UC_HYOKA", $_unit_id, "患者・家族との療養面談");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "退院支援関連のコスト算定");
		regItem("", "UC_HYOKA", $_unit_id, "退院前カンファレンスの運営");
		regItem("", "UC_HYOKA", $_unit_id, "介護保険制度の説明");
		regItem("", "UC_HYOKA", $_unit_id, "在宅療養に関わる多職種との連携");
		regItem("", "UC_HYOKA", $_unit_id, "ケアマネージャーの手配と連携");
		regItem("", "UC_HYOKA", $_unit_id, "訪問看護師の手配と連携");
		regItem("", "UC_HYOKA", $_unit_id, "在宅往診医の手配と連携");
		regItem("", "UC_HYOKA", $_unit_id, "院内教育活動");
		regItem("", "UC_HYOKA", $_unit_id, "コンサルテーション");
	}
	if ($nm=="★化学療法室") {
		regItem("", "UC_HYOKA", $_unit_id, "ホルモン製剤筋肉注射");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "抗悪性腫瘍製剤皮下注射の介助");
		regItem("", "UC_HYOKA", $_unit_id, "抗悪性腫瘍製剤静脈内点滴注射の刺入介助");
		regItem("", "UC_HYOKA", $_unit_id, "抗悪性腫瘍製剤静脈内点滴注射（iv）の介助");
		regItem("", "UC_HYOKA", $_unit_id, "採血の介助");
		regItem("", "UC_HYOKA", $_unit_id, "IVHポートの穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "IVHポートの穿刺");
		regItem("", "UC_HYOKA", $_unit_id, "動注ポートの穿刺介助");
		regItem("", "UC_HYOKA", $_unit_id, "血管外漏出時の対応");
		regItem("", "UC_HYOKA", $_unit_id, "薬剤によるアレルギー症状出現時の対応");
		regItem("", "UC_HYOKA", $_unit_id, "シュアフューザーポンプの交換");
	}
	if ($nm=="★がん治療センター") {
		regItem("", "UC_HYOKA", $_unit_id, "終末期患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "がん患者の緩和ケア病棟への転院調整");
		regItem("", "UC_HYOKA", $_unit_id, "がん患者の在宅療養サービスの調整");
		regItem("", "UC_HYOKA", $_unit_id, "放射線治療中のがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "倦怠感のあるがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "不眠のあるがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "精神症状（不安、怒り、抑うつ）のあるがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "せん妄のあるがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "呼吸器症状（呼吸困難感、咳嗽など）のある、がん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "消化器症状（吐き気、嘔吐、便秘、下痢など）のある,がん患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "浮腫のあるがん患者への看護");
		regItem("", "UC_HYOKA", $_unit_id, "がん患者の家族へのケア");
		regItem("", "UC_HYOKA", $_unit_id, "日本看護協会認定緩和ケア認定看護師または,がん性疼痛看護認定看護師、または,がん看護専門看護師");
		regItem("", "UC_HYOKA", $_unit_id, "国立がんセンター主催相談員研修（1）〜（3）受講");
		regItem("", "UC_HYOKA", $_unit_id, "一般財団法人ライフプランニングセンター主催がんリハビリテーション研修");
		regItem("", "UC_HYOKA", $_unit_id, "静脈注射アドバンス（CVポート）");
		regItem("", "UC_HYOKA", $_unit_id, "静脈注射アドバンス（オピオイド早送り）");
	}
	if ($nm=="★RFA治療室") {
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療中の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療時のポジショニング");
		regItem("", "UC_HYOKA", $_unit_id, "対極板の装着部位の選択と装着");
		regItem("", "UC_HYOKA", $_unit_id, "RFAラジエーターのセットアップ");
		regItem("", "UC_HYOKA", $_unit_id, "RFAラジエーターの操作");
		regItem("", "UC_HYOKA", $_unit_id, "人工胸水・腹水留置をする患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "造影剤の作成・準備");
		regItem("", "UC_HYOKA", $_unit_id, "肝生検を行う患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "胸水・腹水穿刺、排液を行う患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "膿瘍穿刺を行う看護");
		regItem("", "UC_HYOKA", $_unit_id, "陽子線マーカー挿入時の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "CV、ポート挿入中の患者の看");
		regItem("", "UC_HYOKA", $_unit_id, "RFA治療を受けるペースメーカー挿入中の患者の看護");
		regItem("", "UC_HYOKA", $_unit_id, "RFAによる熱傷発生時の観察・看護・報告");
		regItem("", "UC_HYOKA", $_unit_id, "滅菌物の展開が出来る");
		regItem("", "UC_HYOKA", $_unit_id, "滅菌物の受け渡しが出来る");
	}
	if ($nm=="★看護部") { // なし
	}
	if ($nm=="★手術部：手術室") { // なし
	}
	if ($nm=="★手術部：滅菌室") { // なし
	}
	if ($nm=="★総合診療内科・血液内科・リハビリテーション科") { // なし
	}



}


c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 当院での医療の提供を希望するすべての患者のニーズを把握し、その患者・家族にとって最善の医療が提供できるようそれぞれのニーズに対応する</div><div>2. 患者にとって最善である医療が患者の意思で決定されるよう支援する</div><div>3. 各部署からの報告への対応、および各部門との調整をする。</div><div>4. 看護職員を育成する。</div><div>5. ６病院看護部および看護学部との連携をはかる。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★看護部');");

c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 小児外科の手術を受ける目的で入院した患者、また術後の患者へのケアを提供する。</div><div>2. 小児がんなどの疾患により、化学療法や放射線療法を目的として入院する患者へのケアを提供する。</div><div>3. 周産期母子メディカルセンターとして関連部署で連携・協働し、医療的ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館10階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 小児科で診療を行う疾患全般で、検査・治療目的で入院する患者に適切なケアを提供する。</div><div>2. 整形外科・眼科・小児外科・心臓血管外科・脳外科・形成外科・耳鼻科の手術を受ける目的で入院した小児に適切な術前術後のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館10階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 多職種からなる病棟スタッフが互いに協力し、患者・ご家族が満足できる周産期・小児医療を提供する。</div><div>2. 長期入院患者とご家族が在宅での生活を受け止め、適切な時期に退院できるよう、医療チームが支援する </div><div>3. 地域医療機関からハイリスク新生児の受け入れを積極的に行う。</div><div>4. 在胎34週未満の早産児や、在胎34週以降の集中治療は要さないが、呼吸循環管理や薬剤投与、手術、精査等が必要な児に適切な周産期・小児医療を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館10階CN病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1.  切迫流早産、前期破水、前置胎盤、妊娠高血圧症、常位胎盤早期剥離、多胎妊娠など妊娠に伴う疾患で入院した妊産褥婦に、各期で適切なケアを実施する。</div><div>2. 血栓症、てんかん、精神疾患、心疾患、脳血管疾患、膠原病、腎疾患、呼吸器疾患など母体合併症がある妊産褥婦に対して、適切なケアを実施する。</div><div>3. 合併疾患の診療科と協働し専門的治療を実施する。</div><div>4. 分娩が目的で入院した妊産婦に助産ケアを提供する。</div><div>5. 新生児の胎外生活適応への看護と家族の育児支援を実施する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館11階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 婦人科領域の手術（子宮癌・子宮筋腫・卵巣腫瘍・子宮外妊娠・子宮ポリープなど）を受ける目的で入院した患者、また術後患者にケアを提供する。</div><div>2. 婦人科疾患（子宮癌・卵巣癌）の化学療法や放射線療法を受ける患者のケアを提供する。</div><div>3. 婦人科疾患（子宮癌・卵巣癌など）の終末期患者のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館11階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 泌尿器科皮膚科の手術を受ける目的で入院した患者、または術後患者のケアを提供する。</div><div>2. 化学療法、放射線科療法目的で入院した患者のケアを提供する。</div><div>3. ペインコントロール目的で入院した患者のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館12階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 整形外科の手術を受ける目的で入院した患者、また手術後の患者のケアを提供する。</div><div>2. 化学療法、放射線治療、疼痛コントロール目的で入院するケースには、がん治療センターや、ペインクリニックと協働しケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館12階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 眼科手術を受ける目的で入院した患者、および術後患者のケアを提供する。</div><div>2. 手術以外（点眼や点滴など）、検査目的（眼圧日内変動など）で入院する患者に適切なケアを提供する。</div><div>3. 退院支援が必要な場合は看護相談室・医療福祉相談室と連携し退院支援を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館13階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 耳鼻咽喉頭頚科の手術を受ける目的で入院した患者に対し、術前から術後、退院に向けてのケアを提供する。</div><div>2. 頭頚部癌に関する化学療法・放射線治療目的での入院患者のケア、睡眠時無呼吸症候群でのPSGなどの検査入院のケアを提供する。</div><div>3. 頭頸部癌終末期患者に対する緩和医療を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館13階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 急性期・慢性期を問わず、個室への入院希望、及び緊急で入院が必要な全診療科の患者へのケアを医師・薬剤師やＰＴ・ＯＴなどと連携を図り、検査や治療・リハビリテーション時のケアを提供する。</div><div>2. 内視鏡や内視鏡治療を受ける目的で入院した患者に対して、検査や治療前準備・検査や治療後のケアを提供する。</div><div>3. 化学療法を受ける目的で入院した患者に対して、薬剤師や化学療法室などと連携し、化学療法のケアを提供する。</div><div>4. 外科系の手術を受ける目的で入院した患者に対して、術前準備・術後のケアを提供する。</div><div>5. 緊急の入院を有する患者に対して、関連部署（当該科・当該病棟）と連携し、検査・治療時のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館14階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 術直後の全身管理が必要な患者、急性発症から重症な症状に変化した患者、慢性期・終末期で状態が悪化し集中ケアを必要とする患者に、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館7階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 循環器疾患における集中治療を脱した患者のケアを提供する。</div><div>2. 心臓血管外科における周手術期（術前・手術室から帰室・手術室から集中治療室を経由して帰室する患者）ケアを提供する。</div><div>3. 心臓カテーテル検査を受ける患者や経皮的血管内治療を受ける患者のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館7階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 血液疾患患者に対して安全で質の高い医療や看護を提供する。</div><div>2. 感染予防のための対策を徹底する。</div><div>3. マニュアルに基づき正しい知識、技術で医療を実践する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館8階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 糖尿病内分泌内科の糖尿病患者に対し、医師、看護師、栄養士、による教育的指導をする。</div><div>2. 膠原病内科のステロイドパルス療法、薬剤コントロールを行う患者に対し、副作用が最小限となるよう副作用の予防、入院期間が長期化するためストレスの軽減に努めケアを提供する。</div><div>3. 血液内科の化学療法を受ける患者に対し、正確な化学療法投与の実施、化学療法副作用による苦痛が最小限になるよう予防とケアを提供する。</div><div>4. 循環器内科の患者に対し、治療や検査が安全に、また不安を最小限に受けられるようケアを提供する。慢性疾患の患者には教育的指導をする。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館8階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 精神障害者の医療及び保護を提供する。</div><div>2. 安全で質の高い精神医療を提供する。</div><div>3. 患者の人権を擁護し、行動制限の最小化をはかる。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館8階C病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 呼吸器外科の手術後、合併症など症状が出現した際の適切な対応をする。</div><div>2. 呼吸器系の悪性疾患において化学療法や放射線療法など適切な対応をする。</div><div>3. 気管支鏡検査、CTガイド下肺生検など呼吸器疾患に関する検査において、検査前後のケアを提供する。</div><div>4. 慢性呼吸不全、肺線維症など呼吸器疾患の急性期から慢性期、終末期に至るケースまで適切なケアを提供する。</div><div>5. 呼吸器疾患の終末期において対症療法や退院支援など、緩和チームや患者・看護相談室、医療福祉相談室と連携し適切なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館9階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. HIV感染症、AIDS患者に対する専門的ケアを提供する。</div><div>2. 循環器内科のカテーテル手術やカテーテル検査を行う患者の看護ケアを提供する。</div><div>3. 膠原病・呼吸器疾患・感染症などの慢性疾患患者に対する支援を行う。</div><div>4. がん患者に対し、緩和ケアチームやがん治療センターと協働し、支援する。</div><div>5. 放射線科における前立腺癌高精度放射線治療のための経皮的金属マーカ留置術を受ける患者に対し、適切なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★1号館9階B病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 乳腺科の主に乳がん周術期におけるケア、終末期における疼痛コントロール、在宅療養への移行支援をする。</div><div>2. 糖尿病治療開始支援、セルフケアコントロールに向けて生活指導をする。</div><div>3. 全身性エリテマトーデス、関節リウマチ、皮膚筋炎など急性期の薬物療法、慢性期の薬物セルフケアコントロールや生活指導をする。</div><div>4. 急性期観察が必要な患者の全身管理を実施する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟10階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 腎生検などの検査入院、慢性腎不全患などの教育入院、ステロイドパルス療法などの薬物療法目的の患者へのケアを提供する。</div><div>2. 腎臓内科の手術を受ける目的で入院した患者、また術後患者のケアを提供する。</div><div>3. 透析導入目的で入院した患者にケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟11階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 脳神経外科の手術を受ける目的で入院した患者、また術後患者のケアを提供する。</div><div>2. ADLの低下がある、もしくは予測される場合は、早期にリハビリ依頼をする。</div><div>3. 退院支援が必要な場合は、患者・看護相談室へ連絡し支援依頼し、協働する。</div><div>4. 転院が必要な場合や、社会資源の利用に関する相談は、医療福祉相談室へ連絡し協働する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟12階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 脳神経内科は、脳梗塞や脳出血などの脳血管疾患、パーキンソン病や筋萎縮性側索硬化症など神経難病の患者にケアを提供する。</div><div>2. 集中治療室で治療を終了した患者は当病棟でケアを提供し、医師やMSW、リハビリテーションセラピスト、薬剤師と協働し、自宅退院を図る。自宅退院が困難な患者は、回復期リハビリテーション病院や療養施設への転院を円滑に進める。</div><div>3. 退院後も継続したケアが提供されるよう地域や他院、当院各科外来と連携する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟13階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 大腸肛門外科・形成外科の手術を受ける目的で入院した患者に、術前術後の看護ケアを提供する。</div><div>2. 脳神経内科の薬剤コントロール中の患者に、適切な看護ケアを提供する。</div><div>3. 大腸肛門外科の化学療法・放射線療法を受ける患者、また終末期の患者に適切な看護ケアを提供する。</div><div>4. 大腸肛門外科患者の下部消化管内視鏡検査、ESDなどの治療を受ける患者に、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟14階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 肝胆膵外科食道胃外科の患者（肝移植も含む）に、周術期看護、放射線療法、化学療法、検査や終末期看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟15階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 食道胃外科の手術を受ける目的で入院した患者、また術後患者のケアを提供する。</div><div>2. 内視鏡・ESDなどの検査入院、化学療法や放射線療法目的で入院するケースにも、適切なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟16階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 経皮的ラジオ波焼灼術、内視鏡検査、治療（ESD・EMRなど）、手術前検査、化学療法や放射線療法を受ける目的で入院する患者に適切なケアを提供する。</div><div>2. 緩和ケア、終末期ケアが必要な患者に、適切なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟17階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 経皮的ラジオ波焼灼術、内視鏡検査、治療（ESD・EMRなど）、手術前検査、化学療法や放射線療法を受ける目的で入院する患者に適切なケアを提供する。</div><div>2. 緩和ケア、終末期ケアが必要な患者に、適切なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟18階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 消化器内科など内視鏡や内視鏡治療を受ける目的で入院した患者に対して、検査や治療前準備・検査や治療後のケアを提供する。</div><div>2. 脳神経内科など薬物療法・リハビリテーション目的で入院した患者に対して、薬剤師やＰＴ・ＯＴと連携を図り、検査や治療・リハビリテーション時のケアを提供する。</div><div>3. 血液内科など化学療法を受ける目的で入院した患者に対して、薬剤師や化学療法室などと連携し、化学療法のケアを提供する。</div><div>4. 消化器外科・整形外科など外科系の手術を受ける目的で入院した患者に対して、術前準備・術後のケアを提供する。</div><div>5. 緊急の入院治療を要する患者に対して、関連部署（当該科・当該病棟）と連携する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟19・20階病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 術直後の全身管理が必要な患者、急性発症から重症な症状に変化した患者、慢性期・終末期で状態が悪化し集中ケアを必要とする患者に、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★B棟6階A病棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 経皮的ラジオ波焼灼術、手術前検査を受ける目的で入院する患者に適切なケアを提供する。</div><div>2. 経皮的ラジオ波焼灼術後の合併症発生後の処置が必要な患者に、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★RFA治療室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 緩和ケアチーム介入患者（入院・外来）における身体的、精神的、心理社会的、霊的苦痛の緩和</div><div>2.  緩和ケアチーム介入患者の家族へのケア</div><div>3.  患者相談室利用患者・家族等に対するがんに関する相談への対応</div><div>4.  緩和ケア病棟入院や地域医療福祉サービス導入のための地域連携・院外部門との調整</div><div>5. 患者支援プログラム（がん茶論・ミニレクチャー・市民公開講座等）でのファシリテーター、司会進行など利用者への対応</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★がん治療センター');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 循環器内科および心臓血管外科の診断、治療を受ける目的で来院した患者に適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★ハートセンター');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 急性・慢性・がん・非がんに関わらず、ペインクリニックの診断や治療を受ける目的で来院する患者に適切な看護ケアを提供する。</div><div>2. 薬物療法、麻酔科的信州治療、理学療法、認知行動療法を受ける目的で来院する患者に、適切な看護ケアを提供する。</div><div>3. 薬物療法、理学療法、手術療法が必要な患者に他部署と連携を図り、適切な看護を提供する。</div><div>4. 臨床心理士による心理面談の調整を行う。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★ペインクリニック');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 精神障害者に対し、質の高い精神看護を提供し、社会復帰の促進及びその自立と社会経済活動への参加の促進のための必要なケアを提供する。</div><div>2. 認知症疾患医療センターと連携し、地域で事例化した対応困難な認知症症例を受け入れる。</div><div>3. 身体疾患治療中に生じた譫妄やステロイド精神病など、精神症状を呈した患者を各診療科と連携するコンサルテーション・リエゾンを展開する。</div><div>4. 他職種と連携し、適切なケアを提供する</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★メンタルクリニック');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 乳腺科の診断・治療を受ける目的で来院した患者に適切な看護ケアを提供する。</div><div>2. 化学療法が必要な患者に対し、がん治療センターの協力のもとに適切な看護ケアを提供する。</div><div>3. 手術後の治療（経過観察・ホルモン療法・放射線治療）に対する患者に、他部署との協力のもと、適切な看護ケアを提供する。</div><div>4. リンパ浮腫予防・リンパ浮腫発症時の患者のケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★乳腺センター');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 健康スポーツ室へ受診する患者や家族に適切なケアを提供する。</div><div>2. 健康スポーツクリニックで心臓リハビリテーションを目的に来院する患者に、医師・健康運動指導士・栄養士と協働し、生活指導や運動指導を提供する。</div><div>3. 人間ドッグ健診者に対し、適切で分かりやすい指導や説明を提供する。</div><div>4. 健康スポーツ室の予約診察室を受診する患者や家族へ、品位ある態度でサービスを提供し、診療の予約時間を厳守する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★健康スポーツ室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 消化器内科、食道胃外科、大腸肛門外科、肝胆膵外科、呼吸器内科、呼吸器外科の診断や治療を受ける目的で来院する患者に適切な看護ケアを提供する。</div><div>2. 緊急内視鏡検査・治療が入っても対応できるように、緊急内視鏡対応部屋を決め、必要品の配置を行っている。</div><div>3. 病棟、他の外来と連携・共同することで、調整出来る範囲で待ち時間なく調整され、円滑かつ安全な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★内視鏡室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 外来通院中のがん患者に対する抗がん剤・分子標的薬投与の管理</div><div>2. 外来通院中の関節リウマチ、クローン病、ベーチェット病、強直性脊椎炎、潰瘍性大腸炎、尋常性乾癬、関節症性乾癬、膿疱性乾癬、乾癬性紅皮症等の患者に対する生物学的製剤投与の管理</div><div>3. 乳腺科・泌尿器科等のがん患者に対するホルモン注射の投与管理</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★化学療法室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 肺癌などの悪性疾患やCOPD、間質性肺炎、喘息などの慢性疾患の患者に、適切な看護ケアを提供する。</div><div>2. 医師の指示がある場合、COPDや喘息では、吸入薬の使用方法を適切に指導する。</div><div>3. 専門外来においては、睡眠時無呼吸症候群、サルコイドーシス、禁煙について適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★呼吸器内科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 呼吸器・消化器領域において手術前、手術後の患者に適切な看護ケアを提供する。</div><div>2. 消化器外科領域で、化学療法や放射線療法を受ける患者に、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★消化器外科・呼吸器外科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 一般的な小児科疾患から専門医療を必要とする疾患、急性疾患から慢性疾患まであらゆる疾患に対し、適切な看護ケアを提供する。</div><div>2. 小児看護専門看護師、糖尿病看護認定看護師、皮膚・排泄ケア看護認定看護師と連携し、患者ケアを提供する。</div><div>3. マルトリートメントチーム（医師・看護師・小児看護専門看護師・医療福祉相談室）で情報共有し、地域・関連機関と連携し、患者サポートを行う。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★小児科・小児外科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 院内の専門チーム（理学療法士、MSW、褥瘡対策チーム、NSTチーム、緩和ケアチーム）と連携し、在宅支援・退院支援におけるケアを提供する。</div><div>2. 患者が安心・安全に在宅療養ができるように地域と連携する。</div><div>3. 在宅療養中の患者・家族に対しての療養指導を提供する。</div><div>4. 看護師に対して教育的支援をする。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★患者・看護相談室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 全身麻酔・局所麻酔下で手術（検査含む）を受ける患者へのケアを提供する。</div><div>2. 手術実施にあたり、麻酔科・各診療部門・看護部門・臨床検査部門・臨床工学部門・放射線部門が協働し専門的技術を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★手術部：手術室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 各診療部門・看護部門・臨床検査部門・臨床工学部門・放射線部門・検査部門・薬剤部門で使用された再生可能な医療器材すべての洗浄・消毒・滅菌処理を実施し、安全に患者へ提供する。</div><div>2. 洗浄・消毒・滅菌工程の保証管理，滅菌物の管理，払い出しおよび回収する。</div><div>3. 手術部、医療安全推進部感染対策室と連携を密にし、滅菌業務の質の向上と滅菌の質の確保を図る。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★手術部：滅菌室');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 放射線検査と放射線治療を目的に来院した患者に、適切な看護ケアを提供する。</div><div>2. 放射線検査と放射線治療の特徴を充分に理解・把握し、患者が主体的に治療に向き合えるよう精神的支援と、セルフケア能力を高める生活指導を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★放射線1号館、B棟');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 当院での救急医療の提供を希望する全ての患者のニーズを把握し、速やかにトリアージを実施する。</div><div>2. 高次の救急対応が出来るよう、スタッフの知識や技術の向上を図る。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★救急PC');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 整形外科領域における慢性疾患、急性疾患、その他先天性疾患の患者に対し、適切なケアを提供する。</div><div>2. 整形外科手術が必要な患者の入院前ケア、化学療法、放射線療法目的で入院する患者に必要なケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★整形外科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 消化器内科の診療を受ける目的で外来に来院した患者に、快適な療養生活のための看護ケアを提供する。</div><div>2. 一般各科外来や検査部（放射線部・内視鏡室など）、救急プライマリセンター、入院する病棟と連携し、適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★消化器内科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 婦人科疾患の治療が必要な、青年期から老年期のすべての女性患者に適切な看護ケアを提供する。</div><div>2. 不妊症に悩む女性、妊孕性温存を目的とした男女に対応する。</div><div>3. 産科外来では周産期センターとして、正常妊娠および胎児異常やハイリスク妊娠に対応する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★産婦人科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 皮膚科・形成外科領域の診断と治療を受ける目的で来院した患者に、適切な看護ケアを提供する。</div><div>2. 金属パッチテストなどの検査入院、化学療法や光線治療、ステロイドパルス療法、手術目的などの患者に対し、他部署と連携し適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★皮膚科・形成外科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 眼科疾患に対する検査、診断、治療を受ける目的で来院した患者に、適切な看護ケアを提供する。</div><div>2. 緊急での処置や手術が必要な場合は、即時入院・緊急手術に対応する。</div><div>3. 視力障害が高度で、日常生活に支障をきたす場合は、ロービジョン外来を受診し、医療福祉相談室とも連携し、利用可能な社会資源に関する情報提供や支援をする。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★眼科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 総合診療科・血液内科・腫瘍内科の診断、治療を受ける目的で来院した患者に適切な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★総合診療内科・血液内科・リハビリテーション科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 耳鼻咽喉頭頚科外来に来院する患者に、適切な看護ケアを提供する。</div><div>2. 手術、化学療法、放射線治療後や治療中の患者に、他部署と連携し、適切なケアを提供する。</div><div>3. 耳鼻咽喉頭頚科疾患に関連した画像検査や聴覚検査や味覚検査などは、他部門（放射線部・臨床検査室）と連携し看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★耳鼻咽喉科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 脳神経領域の診断・治療を受ける目的で来院した患者に適切な看護を提供する。</div><div>2. 生命維持に影響する患者に対し、放射線部・救急PC・ICUなどと連携し、迅速で安全な看護ケアを提供する。</div><div>3. 在宅でのケアが必要な患者に対し、看護相談室や医療福祉相談室等と連携し、必要な看護ケアを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★脳神経内科外科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 急性期から慢性期までのあらゆる泌尿器科・腎臓内科疾患に対し、その疾患に適した看護ケアを提供する。</div><div>2. 必要に応じ、他部署（透析療法室・化学療法室・患者看護相談室など）と連携し、適切な看護を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★泌尿器科・腎臓内科');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 膠原病・リウマチ内科領域、糖尿病・内分泌内科領域の診断、治療を受ける目的で来院した患者に、適切な看護ケアを提供する。 </div><div>2. 在宅療養指導（主にインスリン自己注射、自己血糖測定）、透析予防外来、フットケアなどを提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★膠原病・リウマチ内科・糖尿病・内分泌内科外来');");
c2dbExec("update spfm_hyoka_han set job_description_html = '<div>1. 外来・入院を問わず、血漿交換を必要と診断され、治療を受ける目的で来院する全ての患者に対し、適切な看護ケアを提供する。</div><div>2. 血液透析を導入予定の患者、すでに維持血液透析を行っている全ての患者に、安全で適切な看護を提供する。</div>' where sheet_id in (select sheet_id from spfm_hyoka_sheet where sheet_nm = '★透析療法室');");




//----------------------------------------------------------
// 基礎看護技術
//----------------------------------------------------------

regItem("9", "UC_HYOKA", $unit_id_common2, "環境調整技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.温度、湿度、換気、採光、臭気、騒音、病室整備の療養生活環境調整");
regItem("", "UC_HYOKA", $unit_id_common2, "2.ベッドメーキング");
regItem("9", "UC_HYOKA", $unit_id_common2, "食事援助技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.食生活支援");
regItem("", "UC_HYOKA", $unit_id_common2, "2.食事介助");
regItem("", "UC_HYOKA", $unit_id_common2, "3.経管栄養法");
regItem("9", "UC_HYOKA", $unit_id_common2, "排泄援助技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.自然排尿・排便援助（尿器・便器介助、可能な限りおむつを用いない援助を含む。）");
regItem("", "UC_HYOKA", $unit_id_common2, "2.導尿");
regItem("", "UC_HYOKA", $unit_id_common2, "3.膀胱内留置カテーテルの挿入と管理");
regItem("", "UC_HYOKA", $unit_id_common2, "4.浣腸");
regItem("", "UC_HYOKA", $unit_id_common2, "5.摘便");
regItem("9", "UC_HYOKA", $unit_id_common2, "活動・休息援助技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.歩行介助・移動の介助・移送");
regItem("", "UC_HYOKA", $unit_id_common2, "2.体位変換");
regItem("", "UC_HYOKA", $unit_id_common2, "3.関節可動域訓練・廃用性症候群予防");
regItem("", "UC_HYOKA", $unit_id_common2, "4.入眠・睡眠への援助");
regItem("", "UC_HYOKA", $unit_id_common2, "5.体動、移動に注意が必要な患者への援助");
regItem("9", "UC_HYOKA", $unit_id_common2, "清潔・衣生活援助技術（例：�，�ら�Δ砲弔い董∩寛霆�を要する患者、ドレーン挿入、点滴を行っている患者等への実施）");
regItem("", "UC_HYOKA", $unit_id_common2, "1.清拭");
regItem("", "UC_HYOKA", $unit_id_common2, "2.洗髪");
regItem("", "UC_HYOKA", $unit_id_common2, "3.口腔ケア");
regItem("", "UC_HYOKA", $unit_id_common2, "4.入浴介助");
regItem("", "UC_HYOKA", $unit_id_common2, "5.部分浴・陰部ケア・おむつ交換");
regItem("", "UC_HYOKA", $unit_id_common2, "6.寝衣交換等の衣生活支援、整容");
regItem("9", "UC_HYOKA", $unit_id_common2, "呼吸・循環を整える技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.酸素吸入療法");
regItem("", "UC_HYOKA", $unit_id_common2, "2.吸引（気管内、口腔内、鼻腔内）");
regItem("", "UC_HYOKA", $unit_id_common2, "3.ネブライザーの実施");
regItem("", "UC_HYOKA", $unit_id_common2, "4.体温調整");
regItem("", "UC_HYOKA", $unit_id_common2, "5.体位ドレナージ");
regItem("", "UC_HYOKA", $unit_id_common2, "6.人工呼吸器の管理");
regItem("9", "UC_HYOKA", $unit_id_common2, "創傷管理技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.創傷処置");
regItem("", "UC_HYOKA", $unit_id_common2, "2.褥瘡の予防");
regItem("", "UC_HYOKA", $unit_id_common2, "3.包帯法");
regItem("9", "UC_HYOKA", $unit_id_common2, "与薬の技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.経口薬の与薬、外用薬の与薬、直腸内与薬");
regItem("", "UC_HYOKA", $unit_id_common2, "2.皮下注射、筋肉内注射、皮内注射");
regItem("", "UC_HYOKA", $unit_id_common2, "3.静脈内注射、点滴静脈内注射");
regItem("", "UC_HYOKA", $unit_id_common2, "4.中心静脈内注射の準備・介助・管理");
regItem("", "UC_HYOKA", $unit_id_common2, "5.輸液ポンプの準備と管理");
regItem("", "UC_HYOKA", $unit_id_common2, "6.輸血の準備、輸血中と輸血後の観察");
regItem("", "UC_HYOKA", $unit_id_common2, "7.抗生物質の用法と副作用の観察");
regItem("", "UC_HYOKA", $unit_id_common2, "8.インシュリン製剤の種類・用法・副作用の観察");
regItem("", "UC_HYOKA", $unit_id_common2, "9.麻薬の主作用・副作用の観察");
regItem("", "UC_HYOKA", $unit_id_common2, "10.薬剤等の管理（毒薬・劇薬・麻薬、血液製剤を含む）");
regItem("9", "UC_HYOKA", $unit_id_common2, "救命救急処置技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.意識レベルの把握");
regItem("", "UC_HYOKA", $unit_id_common2, "2.気道確保");
regItem("", "UC_HYOKA", $unit_id_common2, "3.人工呼吸");
regItem("", "UC_HYOKA", $unit_id_common2, "4.閉鎖式心臓マッサージ");
regItem("", "UC_HYOKA", $unit_id_common2, "5.気管挿管の準備と介助");
regItem("", "UC_HYOKA", $unit_id_common2, "6.外傷性の止血");
regItem("", "UC_HYOKA", $unit_id_common2, "7.チームメンバーへの応援要請");
regItem("9", "UC_HYOKA", $unit_id_common2, "症状・生体機能管理技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.バイタルサイン（呼吸・脈拍・体温・血圧）の観察と解釈");
regItem("", "UC_HYOKA", $unit_id_common2, "2.身体計測");
regItem("", "UC_HYOKA", $unit_id_common2, "3.静脈血採血と検体の取扱い");
regItem("", "UC_HYOKA", $unit_id_common2, "4.動脈血採血の準備と検体の取り扱い");
regItem("", "UC_HYOKA", $unit_id_common2, "5.採尿・尿検査の方法と検体の取り扱い");
regItem("", "UC_HYOKA", $unit_id_common2, "6.血糖値測定と検体の取扱い");
regItem("", "UC_HYOKA", $unit_id_common2, "7.心電図モニター・12誘導心電図の装着、管理");
regItem("", "UC_HYOKA", $unit_id_common2, "8.パルスオキシメーターによる測定");
regItem("9", "UC_HYOKA", $unit_id_common2, "苦痛の緩和安楽確保の技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.安楽な体位の保持");
regItem("", "UC_HYOKA", $unit_id_common2, "2.罨法等身体安楽促進ケア");
regItem("", "UC_HYOKA", $unit_id_common2, "3.リラクゼーション技法（例：呼吸法・自立訓練法など）");
regItem("", "UC_HYOKA", $unit_id_common2, "4.精神的安寧を保つための看護ケア（例：患者の嗜好や習慣等を取り入れたケアを行う等）");
regItem("9", "UC_HYOKA", $unit_id_common2, "感染予防技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "1.スタンダードプリコーション（標準予防策）の実施");
regItem("", "UC_HYOKA", $unit_id_common2, "2.必要な防護用具（手袋、ゴーグル、ガウン等）の選択");
regItem("", "UC_HYOKA", $unit_id_common2, "3.無菌操作の実施");
regItem("", "UC_HYOKA", $unit_id_common2, "4.医療廃棄物規定に沿った適切な取扱い");
regItem("", "UC_HYOKA", $unit_id_common2, "5.針刺し切創、粘膜曝露等による職業感染防止対策と事故後の対応");
regItem("", "UC_HYOKA", $unit_id_common2, "6.洗浄・消毒・滅菌の適切な選択");
regItem("9", "UC_HYOKA", $unit_id_common2, "死亡時のケアに関する技術"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "死後のケア");
regItem("9", "UC_HYOKA", $unit_id_common2, "その他"); // ★★★
regItem("", "UC_HYOKA", $unit_id_common2, "輸液ポンプアドバンス");





//----------------------------------------------------------
// 行動項目（Privilege）（外来＋病棟）
//----------------------------------------------------------
regItem("", "UC_PRIVILEGE", $unit_id_common3, "礼儀・接遇・マナー", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common3, "地球環境保全（医療資材の適切利用・節約・環境整備）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common3, "スタッフとの信頼関係・職種間連携", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common3, "自己啓発・向上心(教育力、国際対応力・研究力)", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common3, "迅速な対応", array("is_kouza_obligate"=>1));



//----------------------------------------------------------
// 看護共通項目（Privilege）（外来＋病棟）
//----------------------------------------------------------
regItem("", "UC_PRIVILEGE", $unit_id_common4, "患者アセスメント・再アセスメントの実施", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "輸血業務の実施", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "臓器/移植プログラムの実施", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "鎮静・麻酔のモニタリング", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "麻薬・毒薬・向精神薬の管理・投与", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "ハイアラート薬の管理・投与", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "リスクマネジメントの実施（医療安全・感染対策を含む）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "一次救命の実施（BLS）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "二次蘇生処置の実施（ACLS／ICLS／PALS）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "看護学または関連領域の学士号を取得", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "看護学または関連領域の修士号を取得", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "救急患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "昏睡状態患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "生命維持装置を装着している患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "伝染病を患った患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "免疫不全患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "透析を受けている患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "身体抑制・身体拘束を受けている患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "化学療法を受けている患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "高齢患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "小児患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "虐待を受けている患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "転倒転落リスクの高い患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "疼痛の強い患者のケア", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVナース（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVアドバンス：CVポート（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVアドバンス：麻薬静脈注射（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVアドバンス：インスリン療法（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVアドバンス：造影剤投与（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "IVアドバンス：HD（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：集中ケア（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：糖尿病看護（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：院内急変（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：救急初療（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：摂食・嚥下（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：褥瘡ケア（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：リンパ浮腫（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：新生児救急（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：メンタルケア（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "学内認定：がん化学療法（学内認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：看護管理（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：救急看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：集中ケア（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：皮膚・排泄ケア（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：緩和ケア（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：がん性疼痛看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：がん化学療法（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：感染管理（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：不妊症看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：糖尿病看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：新生児集中ケア（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：透析看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：乳がん看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：手術看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：小児救急（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：摂食・嚥下障害看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：認知症看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：脳卒中リハビリテーション看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：がん放射線看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：慢性呼吸器疾患看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：慢性心不全看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "認定看護師：訪問看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：がん看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：精神看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：地域看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：老人看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：小児看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：母性看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：慢性疾患看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：急性・重症患者看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：感染症看護（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "専門看護師：家族支援（看護協会認定）", array("is_kouza_obligate"=>1));
regItem("", "UC_PRIVILEGE", $unit_id_common4, "身だしなみベスト10（部内認定）");






function regItem($section_division, $composit, $unit_id, $text, $opts=array()) {
	$hyoka_order = 1 + c2dbGetOne("select max(hyoka_order) from spfm_hyoka_items where unit_id = ".$unit_id);
	$sql =
	" insert into spfm_hyoka_items (".
	"     huid, unit_composit, unit_id, hyoka_naiyou, suisyou_kaisu, use_jisseki_kaisu, kouza_relation, hyoka_order".
	"    ,is_used_item, section_division, disp_hyoka_number, is_kouza_obligate, baritess_contents_id, nshikaku_id, gakkai_id".
	"    ,emp_id, privilege_detail, user_emp_id, use_jiko_hyoka, use_hyoka_flow, rev_id, kensyu_id".
	" ) values (".
	" nextval('spfm_hyoka_items_huid_seq')". // huid
	",'".$composit."'". // unit_composit
	",".$unit_id. // unit_id
	",".c2dbStr($text). // hyoka_naiyou
	",''". // suisyou_kaisu
	",''". // use_jisseki_kaisu
	",''". // kouza_relation
	",".$hyoka_order. // hyoka_order
	",''". // is_used_item
	",'".$section_division."'". // section_division
	",''". // disp_hyoka_number
	",'".$opts["is_kouza_obligate"]."'". // is_kouza_obligate
	",null". // baritess_contents_id
	",null". // nshikaku_id
	",null". // gakkai_id
	",''". // emp_id
	",''". // privilege_detail
	",''". // user_emp_id
	",''". // use_jiko_hyoka
	",'1'". // use_hyoka_flow
	",0". // rev_id
	",null". // kensyu_id
	" )";
	c2dbExec($sql);
}


function regApproval($unit_id) {
	$st_ids = array(37, 39, 40, 41, 42, 43, 45, 46);
	$hier_seq = c2dbGetOne("select max(appr_hier_seq) from spfm_approval_hier");
	foreach ($st_ids as $st_id) {
		$hier_seq ++;
		$sql =
		" insert into spfm_approval_hier (".
		"     appr_hier_seq, wf_type, appr_kbn, fr_last_cadr_hier, fr_st_id, target_id, hier_info, hier_count, hier_names, fr_last_cadr_id".
		" ) values (".
		"     ".$hier_seq.", 'HYOKA', 'YAKUSYOKU', '', ".$st_id.", ".$unit_id.", ".
		c2dbStr(
			'a:2:{i:0;a:3:{s:13:"subhier_count";s:1:"1";s:12:"subhier_type";s:2:"OR";s:14:"subhier_check1";N;}'.
			'i:1;a:3:{s:13:"subhier_count";s:1:"1";s:12:"subhier_type";s:2:"OR";s:14:"subhier_check1";N;}}'
		).", 2, '一次評価\t二次評価', NULL);";
		c2dbExec($sql);
	}

	$patterns = array(
		array("#20#25",     37,1,"K_CLASS", 20, 25),
		array("#20#24",     37,2,"K_CLASS", 20, 24),
		array("#20-136#37", 39,1,"K_ATRB",  136,37),
		array("#20#25",     39,2,"K_CLASS", 20, 25),
		array("#20-136#39", 40,1,"K_ATRB",  136,39),
		array("#20-136#37", 40,2,"K_ATRB",  136,37),
		array("#20-136#40", 41,1,"K_ATRB",  136,40),
		array("#20-136#37", 41,2,"K_ATRB",  136,37),
		array("#20-136#40", 42,1,"K_ATRB",  136,40),
		array("#20-136#37", 42,2,"K_ATRB",  136,37),
		array("#20-136#41", 43,1,"K_ATRB",  136,41),
		array("#20-136#41", 43,2,"K_ATRB",  136,41),
		array("#20-136#40", 43,2,"K_ATRB",  136,40),
		array("#self_st#43",45,1,"",        0,  0),
		array("#20-136#41", 45,2,"K_ATRB",  136,41),
		array("#self_st#45",46,1,"",        0,  0),
		array("#self_st#43",46,2,"",        0,  0)
	);
	$emp_seq = c2dbGetOne("select max(appr_emp_seq) from spfm_approval_emp");
	foreach ($patterns as $pp) {
		$emp_seq ++;
		$sql =
		" insert into spfm_approval_emp (".
		"     appr_emp_seq, wf_type, appr_kbn, emp_id_or_syozoku, fr_st_id, fr_last_cadr_id, fr_last_cadr_hier, target_id, appr_hier".
		"    ,appr_subhier, to_last_cadr_hier, to_last_cadr_id, to_st_id, self_st_id".
		" ) values (".
		"     ".$emp_seq.", 'HYOKA', 'YAKUSYOKU', '".$pp[0]."', ".$pp[1].", NULL, '', ".$unit_id.", ".$pp[2].
		"    ,1, '".$pp[3]."', ".$pp[4].", ".$pp[5].", NULL".
		" );";
		c2dbExec($sql);
	}
}

c2dbExec("select refresh_spft_hyoka_concurrent_mst_func(0, 0, '');");



echo "end.";
