<?
require_once("../common.php"); // 読込みと同時にログイン判別

if ($_REQUEST["drop"]) {
	c2dbExec("drop table _doctor");
}
if ($_REQUEST["create"]) {
	$sql = array(
	"CREATE TABLE _doctor",
	"(",
	"seq bigint NOT NULL,",
	"line_no integer,",
	"menkyo_syurui text,",
	"emp_id character varying(12) NOT NULL,",
	"emp_personal_id character varying(12) NOT NULL,",
	"errors text,",
	"emp_nm text,",
	"menkyo_num text,",
	"menkyo_ymd text,",
	"menkyo_koufu text,",
	"jokin text,",
	"hijokin_genkinmu text,",
	"hijokin_daytime text,",
	"hijokin_total text,",
	"emp_join text,",
	"bikou text,",
	"hokeni_no text,",
	"hokeni_ymd text,",
	"nintei_gakkai text,",
	"nintei_nm text,",
	"nintei_no text,",
	"nintei_ymd text,",
	"nintei_to_ymd text,",
	"senmon_gakkai text,",
	"senmon_nm text,",
	"senmon_no text,",
	"senmon_ymd text,",
	"senmon_to_ymd text,",
	"mental_no text,",
	"mental_ymd text,",
	"mental_to_ymd text,",
	"sinsyo_kamoku text,",
	"sinsyo_no text,",
	"sinsyo_ymd text,",
	"ikusei_tantoui text,",
	"ikusei_syurui text,",
	"ikusei_ymd text,",
	"nanbyo_no text,",
	"nanbyo_ymd text,",
	"nanbyo_to_ymd text,",
	"syoni_no text,",
	"syoni_ymd text,",
	"syoni_to_ymd text,",
	"is_symbol character varying(1),",
	"  CONSTRAINT _doctor_pkey PRIMARY KEY (seq)",
	");"
	);
	c2dbExec(implode("\n", $sql));
}


$menkyo_syurui = "";
$tsv = $_REQUEST["ta_tsv"];
$try_real_save = $_REQUEST["try_real_save"];
$try_upload = $_REQUEST["try_upload"];
$try_del = $_REQUEST["try_del"];

function to_ymd($vv, &$errors, $err, $is_hissu, $is_symbol_row="") {
	if (!$is_hissu && !strlen($vv)) return "";
	$_err = "";
	if ($is_hissu && !strlen($vv)) { if ($is_symbol_row) $_err = $err."空値"; }
	else if (!preg_match('/[0-9]+\/[0-9]+\/[0-9]+/', $vv)) { if ($is_symbol_row) $_err = $err.'不正('.$vv.')'; }
	else if (!c2Checkdate(eliminateYmdSlash($vv))) { if ($is_symbol_row) $_err = $err.'不正('.$vv.')'; }
	if ($_err) $errors[]= $_err;
	else $vv = eliminateYmdSlash($vv);
	return $vv;
}
if ($try_del) {
	$luid = $_REQUEST["luid"];
	c2dbExec("delete from spft_profile where luid = ".(int)$luid);
	header("Location:junten_doctor.php");
	die;
}
if ($try_upload) {
	$lines = c2GetCsv($tsv);

	$menkyo_syurui = trim($lines[1][6]);

	// 職員IDマッチング
	$_rows = c2dbGetRows("select emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from empmst");
	$empmst = array();
	$empNames = array();
	foreach ($_rows as $row) {
		$empmst[$row["emp_personal_id"]] = $row["emp_id"];
		$empNames[$row["emp_personal_id"]] = $row["emp_lt_nm"].$row["emp_ft_nm"];
	}

	c2dbExec("delete from _doctor");
	$total_cnt = 0;
	$success_cnt = 0;
	$error_cnt = 0;
	$prev = array();
	foreach ($lines as $line_no => $line) {
		foreach ($line as $idx => $vv) $line[$idx] = trim(str_replace("\v", "", str_replace("\n", "", str_replace("\r", "", str_replace("　", " ", $vv)))));
		if ($line_no <= 2) continue;

		$total_cnt++;
		$errors = array();
		$is_symbol_row = 1;
		$empnm = trim(str_replace(" ", "", str_replace("　", "", $line[2])));
		if (!$line[1]) {
			$is_symbol_row = 0;
			$line[1] = $prev[1];
			$line[13] = $prev[13];
			$line[7] = $prev[7];
			$line[9] = $prev[9];
		}

		$emp_id = $empmst[$line[1]];
		if ($is_symbol_row) {
			if (!$emp_id) $errors[]= '[列1]職員不明';
			else {
//				if ($empnm!=$empNames[$line[1]]) $errors[]= '[列1]職員名ミスマッチ'."\t".$empNames[$line[1]];
			}
		}

		$menkyo_ymd = to_ymd($line[7], $errors, '[列7]免許登録年月日', 1, $is_symbol_row);

		if ($is_symbol_row) if ($line[9]!="常勤" && $line[9]!="非常勤") $errors[]= '[列9]常勤非常勤不正（'.$line[9].'）';

		$emp_join   = $line[13];
		if ($emp_join) {
			$emp_join = to_ymd($line[13], $errors, '[列13]就任年月日', 1, $is_symbol_row);
		}

		// 保険医
		if (!$is_symbol_row) {
			$line[15]="";
			$line[16]="";
		}
		$hissu = ($line[15] || $line[16] ? 1 : "");
		$hoken_no = $line[15];
		if ($hissu && !$line[15]) $errors[]= "[列15]保険医:登録番号空欄";
		$hokeni_ymd   = to_ymd($line[16], $errors,    '[列16]保険医:登録年月日', $hissu, $is_symbol_row);

		// 認定医
		$hissu = ($line[17] || $line[18] || $line[19] || $line[20] || $line[21] ? 1 : "");

		if ($hissu && !$line[17]) $errors[]= "[列17]認定医:学会名空欄";
		if ($hissu && !$line[18]) $errors[]= "[列18]認定医:認定医名空欄";
		if ($hissu && !$line[19]) $errors[]= "[列19]認定医:認定医番号空欄";
		$nintei_ymd   = to_ymd($line[20], $errors,    '[列20]認定医:認定年月日', $hissu, $is_symbol_row);
		$nintei_to_ymd   = to_ymd($line[21], $errors, '[列21]認定医:有効期限', $hissu, $is_symbol_row);

		// 専門医
		$hissu = ($line[22] || $line[23] || $line[24] || $line[25] || $line[26] ? 1 : "");
		if ($hissu && !$line[22]) $errors[]= "[列22]専門医:学会名空欄";
		if ($hissu && !$line[23]) $errors[]= "[列23]専門医:専門医名空欄";
		if ($hissu && !$line[24]) $errors[]= "[列24]専門医:専門医番号空欄";
		$senmon_ymd   = to_ymd($line[25], $errors,    '[列25]専門医:認定年月日', $hissu, $is_symbol_row);
		$senmon_to_ymd   = to_ymd($line[26], $errors, '[列26]専門医:有効期限', $hissu, $is_symbol_row);

		// 精神保健指定医
		if (!$is_symbol_row) {
			$line[27]="";
			$line[28]="";
			$line[29]="";
		}
		$hissu = ($line[27] || $line[28] || $line[29] ? 1 : "");
		if ($hissu && !$line[27]) $errors[]= "[列27]精神保健指定医:指定医番号空欄";
		$mental_ymd   = to_ymd($line[28], $errors,    '[列28]精神保健指定医:交付日', $hissu, $is_symbol_row);
		$mental_to_ymd   = to_ymd($line[29], $errors, '[列29]精神保健指定医:有効期限', $hissu, $is_symbol_row);

		// 身体障害者指定医
		$hissu = ($line[30] || $line[31] || $line[32] ? 1 : "");
		if ($hissu && !$line[30]) $errors[]= "[列30]身体障害者指定医:担当科目空欄";
		if ($hissu && !$line[31]) $errors[]= "[列31]身体障害者指定医:登録番号空欄";
		$sinsyo_ymd   = to_ymd($line[32], $errors,    '[列32]身体障害者指定医:指定年月日', $hissu, $is_symbol_row);

		// 育成更正医療担当医
		$hissu = ($line[33] || $line[34] || $line[35] ? 1 : "");
		if ($hissu && !$line[33]) $errors[]= "[列33]育成更正医療担当医:育成更生医療担当医空欄";
		if ($hissu && !$line[34]) $errors[]= "[列34]育成更正医療担当医:担当する医療の種類空欄";
		$ikusei_ymd   = to_ymd($line[35], $errors,    '[列35]育成更正医療担当医:指定更新期限', $hissu, $is_symbol_row);

		// 難病指定医
		if (!$is_symbol_row) {
			$line[36]="";
			$line[37]="";
			$line[38]="";
		}
		$hissu = ($line[36] || $line[37] || $line[38] ? 1 : "");
		if ($hissu && !$line[36]) $errors[]= "[列36]難病指定医:指定医番号空欄";
		$nanbyo_ymd   = to_ymd($line[37], $errors,    '[列37]難病指定医:指定年月日', $hissu, $is_symbol_row);
		$nanbyo_to_ymd   = to_ymd($line[38], $errors, '[列38]難病指定医:有効期限', $hissu, $is_symbol_row);

		// 小児慢性特定疾病指定医
		if (!$is_symbol_row) {
			$line[39]="";
			$line[40]="";
			$line[41]="";
		}
		$hissu = ($line[39] || $line[40] || $line[41] ? 1 : "");
		if ($hissu && !$line[39]) $errors[]= "[列39]小児慢性特定疾病指定医:指定医番号空欄";
		$syoni_ymd   = to_ymd($line[40], $errors,     '[列40]小児慢性特定疾病指定医:指定年月日', $hissu, $is_symbol_row);
		$syoni_to_ymd   = to_ymd($line[41], $errors,  '[列41]小児慢性特定疾病指定医:有効期限', $hissu, $is_symbol_row);


		if (count($errors)) $error_cnt++;
		else $success_cnt++;

		$sql =
		" insert into _doctor (".
		" seq".
		",menkyo_syurui".
		",line_no".
		",emp_id".
		",emp_personal_id".
		",errors".
		",emp_nm".
		",menkyo_num".
		",menkyo_ymd".
		",menkyo_koufu".
		",jokin".
		",hijokin_genkinmu".
		",hijokin_daytime".
		",hijokin_total".
		",emp_join".
		",bikou".
		",hokeni_no".
		",hokeni_ymd".
		",nintei_gakkai".
		",nintei_nm".
		",nintei_no".
		",nintei_ymd".
		",nintei_to_ymd".
		",senmon_gakkai".
		",senmon_nm".
		",senmon_no".
		",senmon_ymd".
		",senmon_to_ymd".
		",mental_no".
		",mental_ymd".
		",mental_to_ymd".
		",sinsyo_kamoku".
		",sinsyo_no".
		",sinsyo_ymd".
		",ikusei_tantoui".
		",ikusei_syurui".
		",ikusei_ymd".
		",nanbyo_no".
		",nanbyo_ymd".
		",nanbyo_to_ymd".
		",syoni_no".
		",syoni_ymd".
		",syoni_to_ymd".
		",is_symbol".
		" ) values (".
		" nextval('_doctor_seq')".
		",".c2dbStr($menkyo_syurui). //menkyo_syurui
		",".$line_no.
		",".c2dbStr($emp_id). //emp_id
		",".c2dbStr($line[1]). //emp_personal_id
		",".c2dbStr(implode("／", $errors)). //errors
		",".c2dbStr($line[3]). //emp_nm
		",".c2dbStr($line[6]). //menkyo_num
		",".c2dbStr($menkyo_ymd). //menkyo_ymd
		",".c2dbStr($line[8]). //menkyo_koufu
		",".c2dbStr($line[9]). //jokin
		",".c2dbStr($line[10]). //hijokin_genkinmu
		",".c2dbStr($line[11]). //hijokin_daytime
		",".c2dbStr($line[12]). //hijokin_total
		",".c2dbStr($emp_join). //emp_join
		",".c2dbStr($line[14]). //bikou
		",".c2dbStr($line[15]). //hokeni_no         [No.9]
		",".c2dbStr($hokeni_ymd). //hokeni_ymd      [No.9]
		",".c2dbStr($line[17]). //nintei_gakkai   [No.13]
		",".c2dbStr($line[18]). //nintei_nm       [No.13]
		",".c2dbStr($line[19]). //nintei_no       [No.13]
		",".c2dbStr($nintei_ymd). //nintei_ymd          [No.13]
		",".c2dbStr($nintei_to_ymd). //nintei_to_ymd    [No.13]
		",".c2dbStr($line[22]). //senmon_gakkai     [No.18]
		",".c2dbStr($line[23]). //senmon_nm         [No.18]
		",".c2dbStr($line[24]). //senmon_no         [No.18]
		",".c2dbStr($senmon_ymd). //senmon_ymd            [No.18]
		",".c2dbStr($senmon_to_ymd). //senmon_to_ymd      [No.18]
		",".c2dbStr($line[27]). //mental_no       [No.21]
		",".c2dbStr($mental_ymd). //mental_ymd          [No.21]
		",".c2dbStr($mental_to_ymd). //mental_to_ymd    [No.21]
		",".c2dbStr($line[30]). //sinsyo_kamoku     [No.16]
		",".c2dbStr($line[31]). //sinsyo_no         [No.16]
		",".c2dbStr($sinsyo_ymd). //sinsyo_ymd            [No.16]
		",".c2dbStr($line[33]). //ikusei_tantoui  [No.17]
		",".c2dbStr($line[34]). //ikusei_syurui   [No.17]
		",".c2dbStr($ikusei_ymd). //ikusei_ymd          [No.17]
		",".c2dbStr($line[36]). //nanbyo_no         [No.11]
		",".c2dbStr($nanbyo_ymd). //nanbyo_ymd            [No.11]
		",".c2dbStr($nanbyo_to_ymd). //nanbyo_to_ymd      [No.11]
		",".c2dbStr($line[39]). //syoni_no        [No.12]
		",".c2dbStr($syoni_ymd). //syoni_ymd            [No.12]
		",".c2dbStr($syoni_to_ymd). //syoni_to_ymd      [No.12]
		",'".((int)$is_symbol_row)."'".
		")";
		c2dbExec($sql);

		$prev = $line;
	}

	$sql =
	" update _doctor set errors = errors || '／職員行重複' from (".
	" select cnt, emp_personal_id from (select count(emp_personal_id) as cnt, emp_personal_id from _doctor where is_symbol = '1' group by emp_personal_id) d where cnt > 1 order by cnt desc".
	" ) d where d.emp_personal_id = _doctor.emp_personal_id";
	c2dbExec($sql);


	$rows = c2dbGetRows("select cnt, hokeni_no from (select count(hokeni_no) as cnt, hokeni_no from _doctor where hokeni_no <> '' group by hokeni_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／保険医:登録重複=".$row["hokeni_no"]."' where hokeni_no = ".c2dbStr($row["hokeni_no"]));
	}


	$rows = c2dbGetRows("select cnt, nintei_gakkai, nintei_nm, nintei_no from (select count(*) as cnt, nintei_gakkai, nintei_nm, nintei_no from _doctor where nintei_gakkai <> '' or nintei_nm <> '' or nintei_no <> '' group by nintei_gakkai, nintei_nm, nintei_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／認定医:登録重複=".$row["nintei_gakkai"].":".$row["nintei_nm"].":".$row["nintei_no"]."' where nintei_gakkai =  ".c2dbStr($row["nintei_gakkai"]) . " and nintei_nm =  ".c2dbStr($row["nintei_nm"]) . " and nintei_no =  ".c2dbStr($row["nintei_no"]));
	}


	$rows = c2dbGetRows("select cnt, senmon_gakkai, senmon_nm, senmon_no from (select count(*) as cnt, senmon_gakkai, senmon_nm, senmon_no from _doctor where senmon_gakkai <> '' or senmon_nm <> '' or senmon_no <> '' group by senmon_gakkai, senmon_nm, senmon_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／専門医:登録重複=".$row["senmon_gakkai"].":".$row["senmon_nm"].":".$row["senmon_no"]."' where senmon_gakkai =  ".c2dbStr($row["senmon_gakkai"]) . " and senmon_nm =  ".c2dbStr($row["senmon_nm"]) . " and senmon_no =  ".c2dbStr($row["senmon_no"]));
	}


	$rows = c2dbGetRows("select cnt, mental_no from (select count(mental_no) as cnt, mental_no from _doctor where mental_no <> '' group by mental_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／精神保健指定医:登録重複=".$row["mental_no"]."' where mental_no = ".c2dbStr($row["mental_no"]));
	}


	$rows = c2dbGetRows("select cnt, sinsyo_no, sinsyo_kamoku from (select count(*) as cnt, sinsyo_no, sinsyo_kamoku from _doctor where sinsyo_no <> '' or sinsyo_kamoku <> '' group by sinsyo_no, sinsyo_kamoku ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／身体障害者指定医:登録重複=".$row["sinsyo_no"].":".$row["sinsyo_kamoku"]."' where sinsyo_no =  ".c2dbStr($row["sinsyo_no"]) . " and sinsyo_kamoku =  ".c2dbStr($row["sinsyo_kamoku"]));
	}


	$rows = c2dbGetRows("select cnt, nanbyo_no from (select count(nanbyo_no) as cnt, nanbyo_no from _doctor where nanbyo_no <> '' group by nanbyo_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／難病指定医:登録重複=".$row["nanbyo_no"]."' where nanbyo_no = ".c2dbStr($row["nanbyo_no"]));
	}


	$rows = c2dbGetRows("select cnt, syoni_no from (select count(*) as cnt, syoni_no from _doctor where syoni_no <> '' group by syoni_no ) d where cnt > 1");
	foreach ($rows as $row) {
		c2dbExec("update _doctor set errors = errors || '／小児慢性特定疾病指定医:登録重複=".$row["syoni_no"]."' where syoni_no =  ".c2dbStr($row["syoni_no"]));
	}

	$error_cnt = c2dbGetOne("select count(*) from _doctor where errors <> ''");
	$success_cnt = $total_cnt - $error_cnt;
}



if ($try_real_save) {
	$rows = c2dbGetRows("select emp_id from spft_empmst");
	$empmst = array();
	foreach ($rows as $row) {
		$empmst[$row["emp_id"]] = 1;
	}

//	c2dbExec("delete from spft_profile where luid = 22 and shikaku_nm = ".c2dbStr($menkyo_syurui));
	$now_ymdhms = date("YmdHis");

	// 免許
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_nm, shikaku_bangou, ymd_fr, shikaku_ninteikikan, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 22, 208, '0', emp_id, menkyo_syurui, menkyo_num, menkyo_ymd, menkyo_koufu, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and is_symbol = '1'";
	c2dbExec($sql);

	// 保険医 No.9
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_bangou, ymd_fr, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 9, 212, '0', emp_id, hokeni_no, hokeni_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (hokeni_no <> '' or hokeni_ymd <> '')";
	c2dbExec($sql);

	// 難病指定医 No.11
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_bangou, ymd_fr, ymd_to, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 11, 212, '0', emp_id, nanbyo_no, nanbyo_ymd, nanbyo_to_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (nanbyo_no <> '' or nanbyo_ymd <> '' or nanbyo_to_ymd <> '')";
	c2dbExec($sql);

	// 小児慢性特定疾病指定医 No.12
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_bangou, ymd_fr, ymd_to, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 12, 212, '0', emp_id, syoni_no, syoni_ymd, syoni_to_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (syoni_no <> '' or syoni_ymd <> '' or syoni_to_ymd <> '')";
	c2dbExec($sql);

	// 学会（認定医）No.13
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_nm, kensyu_theme, shikaku_bangou, ymd_fr, ymd_to, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 13, 216, '0', emp_id, nintei_gakkai, nintei_nm, nintei_no, nintei_ymd, nintei_to_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (nintei_gakkai <> '' or nintei_nm <> '' or nintei_no <> '' or nintei_ymd <> '' or nintei_to_ymd <> '')";
	c2dbExec($sql);

	// 身体障害者指定医 No.16
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, bikou2, shikaku_bangou, ymd_fr, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 16, 212, '0', emp_id, sinsyo_kamoku, sinsyo_no, sinsyo_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (sinsyo_kamoku <> '' or sinsyo_no <> '' or sinsyo_ymd <> '')";
	c2dbExec($sql);

	// 育成更生医療担当医 No.17
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, bikou3, bikou2, ymd_fr, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 17, 212, '0', emp_id, ikusei_tantoui, ikusei_syurui, ikusei_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (ikusei_tantoui <> '' or ikusei_syurui <> '' or ikusei_ymd <> '')";
	c2dbExec($sql);

	// 学会（専門医）No.18
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_nm, kensyu_theme, shikaku_bangou, ymd_fr, ymd_to, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 18, 216, '0', emp_id, senmon_gakkai, senmon_nm, senmon_no, senmon_ymd, senmon_to_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (senmon_gakkai <> '' or senmon_nm <> '' or senmon_no <> '' or senmon_ymd <> '' or senmon_to_ymd <> '')";
	c2dbExec($sql);

	// 精神保健指定医（メンタルクリニックのみ） No.21
	$sql =
	" insert into spft_profile (".
	"     puid, luid, muid, del_flg, emp_id, shikaku_bangou, ymd_fr, ymd_to, update_ymdhms".
	" ) select".
	"     nextval('spft_profile_puid_seq'), 21, 212, '0', emp_id, mental_no, mental_ymd, mental_to_ymd, '".$now_ymdhms."'".
	" from _doctor".
	" where errors = '' and (mental_no <> '' or mental_ymd <> '' or mental_to_ymd <> '')";
	c2dbExec($sql);

	$sql =
	" update empmst set emp_join = _doctor.emp_join from _doctor".
	" where _doctor.emp_id = empmst.emp_id and _doctor.emp_join <> ''";
	c2dbExec($sql);

	c2dbExec("insert into spft_empmst (emp_id) select emp_id from empmst where emp_id not in (select emp_id from spft_empmst)");

	$sql =
	" update spft_empmst set kinmubi_kinmujikan = _doctor.hijokin_daytime from _doctor".
	" where _doctor.emp_id = spft_empmst.emp_id".
	" and _doctor.errors = ''".
	" and _doctor.hijokin_daytime <> ''";
	c2dbExec($sql);

	$sql =
	" update spft_empmst set empmst_ext1 = _doctor.hijokin_total from _doctor".
	" where _doctor.emp_id = spft_empmst.emp_id".
	" and _doctor.errors = ''".
	" and _doctor.hijokin_total <> ''";
	c2dbExec($sql);

	$sql =
	" update spft_empmst set profile_comment = _doctor.bikou from _doctor".
	" where _doctor.emp_id = spft_empmst.emp_id".
	" and _doctor.errors = ''".
	" and _doctor.bikou <> ''";
	c2dbExec($sql);

	c2dbExec("insert into empcond (emp_id, wage) select emp_id, '0' from empmst where emp_id not in (select emp_id from empcond)");
	c2dbExec("update empcond set duty_form = 1 where emp_id in ( select emp_id from _doctor where jokin = '常勤' )");
	c2dbExec("update empcond set duty_form = 2 where emp_id in ( select emp_id from _doctor where jokin = '非常勤' )");

	header("Location:junten_doctor.php");
	die;
}




//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="../../js/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.11.2.min.js"></script>
<script type="text/javascript" src="../js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript" src="../js/wdconv.js"></script>
</head>
<body spellcheck="false">

<div style="padding:10px">
現況
<table cellspacing="0" cellpadding="0" class="list_table">
<?
$rows = c2dbGetRows("select ls.luid, ls.field_label, count(sp.puid) as cnt from spfm_list_lineup ls left outer join spft_profile sp on (sp.luid = ls.luid) where ls.luid in (9,11,12,13,16,17,18,21) group by ls.luid, ls.field_label");
$idx = 0;
foreach ($rows as $row) {
	$idx++;
	echo '<tr><th class="left">'.hh($row["field_label"]).'</th><td>'.$row["cnt"].'</td>';
	echo '<td><form name="frm'.$idx.'" action="junten_doctor.php?try_del=1" method="post"><input type="hidden" name="luid" value="'.hh($row["luid"]).'" /><button onclick="document.frm'.$idx.'.submit()">削除</button></form></td></tr>';
}
?>
</table>
</div>


<? if ($try_upload) { ?>
<div style="padding:10px">
	<div>
		<table cellspacing="0" cellpadding="0"><tr>
			<td style="padding:5px" class="red">
				<b><?=hh($menkyo_syurui)?></b>
			</td>
			<td style="padding:5px">
			⇒
			検出<?=count($lines)?>行／対象<?=$total_cnt?>行／成功<?=$success_cnt?>行／エラー<?=$error_cnt?>行

			</td>
			<? if ($error_cnt) { ?>
			<td style="padding:5px">
				<span class="red">⇒ 取込できません</span>
			</td>
			<? } ?>
			<td style="padding:5px">
				<form name="frm2" action="junten_doctor.php">
					⇒
					<input type="hidden" name="try_real_save" value="1" />
					<input type="submit" value="正登録" />
				</form>
			</td>
		</tr></table>
	</div>
</div>
<? } ?>



<div style="overflow:scroll; width:100%; height:200px; padding:10px">
<table cellspacing="0" cellpadding="0" class="list_table">

<?
	$rows = c2dbGetRows("select * from _doctor where errors <> '' order by seq");
	if (count($rows)) {
		echo '<tr><th>行番号</th><th>職員番号</th><th>エラー内容</th></tr>';
	}
	$cnt = 0;
	foreach ($rows as $row) {
		$cnt++;
		echo '<tr>';
		echo '<td>'.($row["line_no"]+1)."</td>";
		echo '<td>'.$row["emp_personal_id"]."</td>";
		echo '<td>'.hh($row["errors"]).'</td>';
		echo '</tr>';
	}
?>
</table>
</div>

<div style="padding:10px">
<form name="frm" action="junten_doctor.php" method="post">
	<input type="hidden" name="try_upload" value="1" />
	<input type="submit" value="アップロード">
	<textarea name="ta_tsv" style="width:100%; height:200px"><?=hh($tsv)?></textarea>
</form>
</div>


</body>
</html>