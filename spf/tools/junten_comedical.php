<?
require_once("../common.php"); // 読込みと同時にログイン判別

if ($_REQUEST["drop"]) {
	c2dbExec("drop table _comedical");
}
if ($_REQUEST["create"]) {
	$sql = array(
	"CREATE TABLE _comedical",
	"(",
	"  seq bigint NOT NULL,",
	"  line_no integer,",
	"  menkyo_syurui text,",
	"  emp_id character varying(12) NOT NULL,",
	"  emp_personal_id character varying(12) NOT NULL,",
	"  errors text,",
	"  emp_nm text,",
	"  menkyo_num text,",
	"  menkyo_ymd text,",
	"  menkyo_koufu text,",
	"  jokin text,",
	"  hijokin_genkinmu text,",
	"  hijokin_daytime text,",
	"  hijokin_total text,",
	"  emp_join text,",
	"  bikou text,",
	"  CONSTRAINT _comedical_pkey PRIMARY KEY (seq)",
	");"
	);
	c2dbExec(implode("\n", $sql));
}


$menkyo_syurui = "";
$tsv = $_REQUEST["ta_tsv"];
$try_real_save = $_REQUEST["try_real_save"];
$try_upload = $_REQUEST["try_upload"];
$try_del = $_REQUEST["try_del"];
$data_start_idx = $_REQUEST["data_start_idx"];
if (!$data_start_row) $data_start_row = 5;
$title_row = $_REQUEST["title_row"];
if (!$title_row) $title_row = 3;
$no_menkyo = $_REQUEST["no_menkyo"];

if ($try_del) {
	$shikaku_nm = $_REQUEST["shikaku_nm"];
	c2dbExec("delete from spft_profile where luid = 22 and shikaku_nm = ".c2dbStr($shikaku_nm));
	header("Location:junten_comedical.php");
	die;
}
if ($try_upload) {
	$lines = c2GetCsv($tsv);

	$menkyo_syurui = trim($lines[$title_row-1][6]);

	// 職員IDマッチング
	$_rows = c2dbGetRows("select emp_id, emp_personal_id, emp_lt_nm, emp_ft_nm from empmst");
	$empmst = array();
	$empNames = array();
	foreach ($_rows as $row) {
		$empmst[$row["emp_personal_id"]] = $row["emp_id"];
		$empNames[$row["emp_personal_id"]] = $row["emp_lt_nm"].$row["emp_ft_nm"];
	}

	c2dbExec("delete from _comedical");
	$total_cnt = 0;
	$success_cnt = 0;
	$error_cnt = 0;
	$warn = array();
	foreach ($lines as $line_no => $line) {
		foreach ($line as $idx => $vv) $line[$idx] = trim(str_replace("\v", "", str_replace("\n", "", str_replace("\r", "", str_replace("　", " ", $vv)))));
		if ($line_no <= $data_start_row-2) continue;
		if (!$line[1]) continue;
		$total_cnt++;

		$menkyo_ymd = $line[7];
		$emp_join = $line[13];
		$errors = array();

		$emp_id = $empmst[$line[1]];
		if (!$emp_id) {
			$errors[]= '[列1]職員不明';
		} else {
			$empnm = trim(str_replace(" ", "", str_replace("　", "", $line[2])));
			if ($empnm!=$empNames[$line[1]]) $errors[]= '[列1]職員名ミスマッチ'."\t".$empNames[$line[1]];
		}

		if ($no_menkyo) {
			$menkyo_ymd = "";
			$line[6] = "";
			$line[8] = "";
		} else {
			if (!preg_match('/[0-9]+\/[0-9]+\/[0-9]+/', $menkyo_ymd)) $errors[]= '[列7]免許登録年月日不正('.$menkyo_ymd.')';
			else if (!c2Checkdate(eliminateYmdSlash($menkyo_ymd))) $errors[]= '[列7]免許登録年月日不正('.$menkyo_ymd.')';
			else $menkyo_ymd = eliminateYmdSlash($menkyo_ymd);
		}

		if ($line[9]!="常勤" && $line[9]!="非常勤") $errors[]= '[列9]常勤非常勤不正（'.$line[9].'）';

		if (!preg_match('/[0-9]+\/[0-9]+\/[0-9]+/', $emp_join)) $errors[]= '[列13]採用年月日不正('.$emp_join.')';
		else if (!c2Checkdate(eliminateYmdSlash($emp_join))) $errors[]= '[列13]採用年月日不正('.$emp_join.')';
		else $emp_join = eliminateYmdSlash($emp_join);

		$sql =
		" insert into _comedical (".
		" seq".
		",menkyo_syurui".
		",line_no".
		",emp_id".
		",emp_personal_id".
		",errors".
		",emp_nm".
		",menkyo_num".
		",menkyo_ymd".
		",menkyo_koufu".
		",jokin".
		",hijokin_genkinmu".
		",hijokin_daytime".
		",hijokin_total".
		",emp_join".
		",bikou".
		" ) values (".
		" nextval('_comedical_seq')".
		",".c2dbStr($menkyo_syurui). //menkyo_syurui
		",".$line_no.
		",".c2dbStr($emp_id). //emp_id
		",".c2dbStr($line[1]). //emp_personal_id
		",".c2dbStr(implode("／", $errors)). //errors
		",".c2dbStr($line[3]). //emp_nm
		",".c2dbStr($line[6]). //menkyo_num
		",".c2dbStr($menkyo_ymd). //menkyo_ymd
		",".c2dbStr($line[8]). //menkyo_koufu
		",".c2dbStr($line[9]). //jokin
		",".c2dbStr($line[10]). //hijokin_genkinmu
		",".c2dbStr($line[11]). //hijokin_daytime
		",".c2dbStr($line[12]). //hijokin_total
		",".c2dbStr($emp_join). //emp_join
		",".c2dbStr($line[14]). //bikou
		")";
		c2dbExec($sql);
	}
	$sql =
	" update _comedical set errors = errors || '／職員行重複' from (".
	" select cnt, emp_personal_id from (select count(emp_personal_id) as cnt, emp_personal_id from _comedical group by emp_personal_id) d where cnt > 1 order by cnt desc".
	" ) d where d.emp_personal_id = _comedical.emp_personal_id";
	c2dbExec($sql);

	$error_cnt = c2dbGetOne("select count(*) from _comedical where errors <> ''");
	$success_cnt = $total_cnt - $error_cnt;
}

if ($try_real_save) {
	$rows = c2dbGetRows("select emp_id from spft_empmst");
	$empmst = array();
	foreach ($rows as $row) {
		$empmst[$row["emp_id"]] = 1;
	}

//	c2dbExec("delete from spft_profile where luid = 22 and shikaku_nm = ".c2dbStr($menkyo_syurui));
	$now_ymdhms = date("YmdHis");

	if (!$no_menkyo) {
		$sql =
		" insert into spft_profile (".
		"     puid, luid, muid, del_flg, emp_id, shikaku_nm, shikaku_bangou, ymd_fr, shikaku_ninteikikan, update_ymdhms".
		" ) select".
		"     nextval('spft_profile_puid_seq'), 22, 208, '0', emp_id, menkyo_syurui, menkyo_num, menkyo_ymd, menkyo_koufu, '".$now_ymdhms."'".
		" from _comedical".
		" where errors = ''";
		c2dbExec($sql);
	}

	$sql =
	" update empmst set emp_join = _comedical.emp_join from _comedical".
	" where _comedical.emp_id = empmst.emp_id";
	c2dbExec($sql);

	c2dbExec("insert into spft_empmst (emp_id) select emp_id from empmst where emp_id not in (select emp_id from spft_empmst)");

	$sql =
	" update spft_empmst set kinmubi_kinmujikan = _comedical.hijokin_daytime from _comedical".
	" where _comedical.emp_id = spft_empmst.emp_id".
	" and _comedical.errors = ''".
	" and _comedical.hijokin_daytime <> ''";
	c2dbExec($sql);

	$sql =
	" update spft_empmst set empmst_ext1 = _comedical.hijokin_total from _comedical".
	" where _comedical.emp_id = spft_empmst.emp_id".
	" and _comedical.errors = ''".
	" and _comedical.hijokin_total <> ''";
	c2dbExec($sql);

	$sql =
	" update spft_empmst set profile_comment = _comedical.bikou from _comedical".
	" where _comedical.emp_id = spft_empmst.emp_id".
	" and _comedical.errors = ''".
	" and _comedical.bikou <> ''";
	c2dbExec($sql);

	c2dbExec("insert into empcond (emp_id, wage) select emp_id, '0' from empmst where emp_id not in (select emp_id from empcond)");
	c2dbExec("update empcond set duty_form = 1 where emp_id in ( select emp_id from _comedical where jokin = '常勤' )");
	c2dbExec("update empcond set duty_form = 2 where emp_id in ( select emp_id from _comedical where jokin = '非常勤' )");


	header("Location:junten_comedical.php");
	die;
}




//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="../css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="../css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.11.2.min.js"></script>
<script type="text/javascript" src="../js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript" src="../js/wdconv.js"></script>
</head>
<body spellcheck="false">

<div style="padding:10px">
現況
<table cellspacing="0" cellpadding="0" class="list_table">
<?
$rows = c2dbGetRows("select shikaku_nm, count(shikaku_nm) as cnt from spft_profile where luid = 22 group by shikaku_nm");
$idx = 0;
foreach ($rows as $row) {
	$idx++;
	echo '<tr><th>'.hh($row["shikaku_nm"]).'</th><td>'.$row["cnt"].'</td>';
	echo '<td><form name="frm'.$idx.'" action="junten_comedical.php?try_del=1" method="post"><input type="hidden" name="shikaku_nm" value="'.hh($row["shikaku_nm"]).'" /><button onclick="document.frm'.$idx.'.submit()">削除</button></form></td></tr>';
}
?>
</table>
</div>


<? if ($try_upload) { ?>
<div style="padding:10px">
	<div>
		<table cellspacing="0" cellpadding="0"><tr>
			<td style="padding:5px" class="red">
				<b>[免許種類]<?=hh($menkyo_syurui)?></b>
			</td>
			<td style="padding:5px">
			⇒
			検出<?=count($lines)?>行／対象<?=$total_cnt?>行／成功<?=$success_cnt?>行／エラー<?=$error_cnt?>行

			</td>
			<? if ($error_cnt) { ?>
			<td style="padding:5px">
				<span class="red">⇒ 取込できません</span>
			</td>
			<? } ?>
			<td style="padding:5px">
				<form name="frm2" action="junten_comedical.php">
					⇒
					<input type="hidden" name="try_real_save" value="1" />
					
					<? if ($no_menkyo) { ?>
					<input type="hidden" name="no_menkyo" value="1" />
					<input type="submit" value="免許なし正登録" />
					<? } else { ?>
					<input type="submit" value="正登録" />
					<? } ?>
				</form>
			</td>
		</tr></table>
	</div>
</div>
<? } ?>



<div style="overflow:scroll; width:100%; height:200px; padding:10px">
<table cellspacing="0" cellpadding="0" class="list_table">

<?
	$rows = c2dbGetRows("select * from _comedical where errors <> '' order by seq");
	if (count($rows)) {
		echo '<tr><th>行番号</th><th>職員番号</th><th>エラー内容</th></tr>';
	}
	$cnt = 0;
	foreach ($rows as $row) {
		$cnt++;
		echo '<tr>';
		echo '<td>'.($row["line_no"]+1)."</td>";
		echo '<td>'.$row["emp_personal_id"]."</td>";
		echo '<td>'.hh($row["errors"]).'</td>';
		echo '</tr>';
	}
?>
</table>
<? if (count($warn)) { ?>
<div><b>警告あり</b></div>
<pre>
<?=print_r($warn)?>
</pre>
<? } ?>
</div>


<div style="padding:10px">
<form name="frm" action="junten_comedical.php" method="post">
	<input type="hidden" name="try_upload" value="1" />
	<div>
		<label><input type="checkbox" name="no_menkyo"<?=($_REQUEST["no_menkyo"]?" checked" :"")?>>免許なし</label>
		<input type="submit" value="アップロード">
	</div>
	<textarea name="ta_tsv" style="width:100%; height:200px"><?=hh($tsv)?></textarea>
	
</form>
</div>


</body>
</html>