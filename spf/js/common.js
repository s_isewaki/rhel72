/* Copyright medi-system solutions co,.Ltd. All rights reserved. */

var currentContent = ""; // for admin

var NOT_SCROLL = true;
var DO_SCROLL = false;
var NOT_BLINK = true;
var DO_BLINK = false;

var open_sheet_ids = {}; // &admin
var hyoka_selected_composits = ""; // &admin
var hyoka_sheet_finder = {"cadr":"", "st":"", "job":""};
var hyoka_left_scroll_top = 0; // &admin
var hyoka_left_show_id = ""; // &admin
var hyoka_left_open_all = ""; // &admin
var hyoka_left_find_sheet_name = ""; // &admin


$dialogs = {};

$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;

if ($.ui) {
	$.extend($.ui.dialog.prototype.options, {
	    modal: true,
	    resizable: false
	});
}

var gLocks = {};
var tryLock = function(key) { if (gLocks[key]) return 0; gLocks[key]=1; return 1; }
var endLock = function(key, timeout) { if(!timeout) timeout=300; setTimeout(function(){ delete gLocks[key]; }, int(timeout)); }

var str = function(s) { if (!s && s!=0) return ""; return ""+s; }
var substr2=function(s,p,l){s=str(s);if(!s||s.length==0)return "";if(s.length<=p)return "";if(!l || s.length<=p+l) return s.substring(p);return s.substring(p,p+l);}
var stopEvent = function(evt) { if(!evt) evt=window.event; try{evt.stopPropagation();}catch(ex){} try{evt.preventDefault();}catch(ex){} try{evt.cancelBubble=true;}catch(ex){} return false; }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var eee = function(id) { if(!document.getElementById(id)) console.log(id); return document.getElementById(id); }
var ee = function(id) { return document.getElementById(id); }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var tryFocus = function(id) { if (!id) return; var e=ee(id); return tryFocusByElem(e); }
var tryFocusByElem = function(e) { if (!e) return; if(!e || e.disabled || e.readOnly) return; e.focus(); try{e.select();}catch(ex){ return; } return 1; }
var isEnterKey=function(elem,evt){if(!elem.disabled && !elem.readOnly) { var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==13; }}
var isShiftKey=function(elem,evt){if(!elem.disabled && !elem.readOnly) { var o=window.event; return (o?!!o.shiftKey:!!evt.shiftKey); }}
var isKeyboradKey=function(elem, evt, k){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==k; }
var getComboValue = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].value; }
var getComboText = function(obj) { if (obj.selectedIndex<0) return ""; return obj.options[obj.selectedIndex].text; }
var zeroSuppress = function(s) { return s.replace(/^0/, ""); }
var isOneOf = function(s, a) { for(var i=0; i<a.length; i++) if(s==a[i]) return true; return false; }
var getDaysInMonth = function(y,m) { y=int(y);m=int(m); if(y<1||m<1||m>12) return 0; if(m==4||m==6||m==9||m==11) return 30; if(m!=2) return 31; if(!(y%400)) return 29; if(!(y%100)) return 28; return ((y%4)?28:29); }
var getWindowScrollTop = function(win) { if ('pageYOffset' in window) return win.pageYOffset; return Math.round (win.document.documentElement.scrollTop); }
var htmlEscape = function(str) {
    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toEntity(\"$1\",$2)+"')+'"');
}
var getFormElementByName = function(form, name) { return $(form).find("[name="+name+"]").get(0); }
var getWindowAvailSize = function() {
	var wh = window.screen.height;
	var ww = window.screen.width;
	var wah = window.screen.availHeight;
	var waw = window.screen.availWidth;
	if (wah && wah < wh ) wh = wah;
	if (waw && waw < ww ) ww = waw;
	return {height:wh, width:ww};
}
var getElapseYear = function(fy,fm,fd,ty,tm,td) {
    if (int(fy)<1 || int(fm)<1 || int(fd)<1 || int(ty)<1 || int(tm)<1 || int(td)<1) return "";
    var yy=int(ty)-int(fy);
    var fmd = int(fm)*100+int(fd);
    var tmd = int(tm)*100+int(td);
    if (yy >= 0) return yy - (fmd > tmd ? 1 : 0);
    return yy - (fmd > tmd ? 1 : 0);
}
var htmlBinary = function(str) {
    var ret = trim(str).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/ /g,"&nbsp;").replace(/\n/g, '\\n');
    return eval('"'+ret.replace(/\&amp\;\#([x]?)([0-9A-Za-z]+)\;/g, '"+toBinary(\"$1\",$2)+"')+'"');
}
var toEntity = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return "&#0x"+n+";";
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return "&#"+n+";";
}
var toBinary = function(x, n) {
    if (x) {
        var hex = eval("0x"+n);
        if (hex<0xFF) return '&amp;#'+x+n+";";
        return cFromCharCode(eval("0x"+n));
    }
    var ord = eval(n);
    if (ord < 128) return '&amp;#'+n+";";
    return cFromCharCode(eval(n));
}
var cFromCharCode = function(){
    var buf = '';
    for ( var i = 0, l = arguments.length; i < l; i++ ) {
        var n = arguments[i];
        if ( n < 0x10000 ) {
            buf += String.fromCharCode(n);
            continue;
        }
        n -= 0x10000;
        buf += String.fromCharCode(
            0xD800 + ( n >> 10 ),
            0xDC00 + ( n & 0x3FF )
        );
    }
    return buf;
};

var setComboValue = function(obj,v, t){
    var o=obj.options;
    for(var i=0; i<o.length; i++) {
        if(o[i].value==v) {
            obj.selectedIndex=i;
            return;
        }
    }
    if (v && !t) {
        o[o.length] = new Option(v);
        obj.selectedIndex = o.length-1;
        return;
    }
    if (v && t) {
        o[o.length] = new Option(t, v);
        obj.selectedIndex = o.length-1;
        return;
    }
    obj.selectedIndex=0;
}
function getClientWidth(){
    if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    return 0;
}
function ymdToJpWeek(ymd) {
    var aYmd = ymdToArray(ymd);
    var dt = new Date(aYmd.yy, aYmd.zmm-1, aYmd.zdd);
    var ary = ["日","月","火","水","木","金","土"];
    return ary[dt.getDay()];
}
function ymdDateAdd(ymd, inc) {
    var yy = substr2(ymd, 0, 4);
    var mm = substr2(ymd, 4, 2);
    var dd = substr2(ymd, 6, 2);
    var dt = new Date(yy, int(mm)-1, int(dd)).getTime();
    var ndt = new Date(dt + (60*60*24*1000) * inc);
    return ndt.getFullYear() + ("00"+(ndt.getMonth()+1)).slice(-2) + ("00"+(ndt.getDate())).slice(-2);
}
function ymdMonthAdd(ymd, inc) {
    inc = int(inc)*1;
    var yy = int(substr2(ymd, 0, 4));
    var mm = int(substr2(ymd, 4, 2));
    var dd = int(substr2(ymd, 6, 2));
    if (inc>0) {
        yy += int((mm + inc - 1) / 12);
        mm = (mm + inc - 1) % 12 + 1;
    }
    if (inc<0) {
        inc = Math.abs(inc);
        if (inc>=mm) yy += int((mm - inc - 12) / 12);
        if (inc<mm) mm -= inc;
        if (inc>=mm) mm = 12 + ((mm - inc) % 12);
    }
    var ndt = new Date(yy, int(mm)-1, int(dd));
    return yy + ("00"+mm).slice(-2) + ("00"+(ndt.getDate())).slice(-2);
}
function getTodayYmd() {
    var dt = new Date();
    return dt.getFullYear() + ("00"+(dt.getMonth()+1)).slice(-2) + ("00"+(dt.getDate())).slice(-2);
}
function getYmd(str, minYmd, maxYmd) {
    var ret = getYmdOrErr(str, minYmd, maxYmd);
    if (ret.length==8) return ret;
    return "";
}
function getYmdOrErr(str, minYmd, maxYmd, minInc, maxInc) {
	if (!window.SysYmd) alert("エラー：SysYmdがありません");
    str = ""+str;
    var baseErrMsg =
    "\n日付はスラッシュ・ハイフン・ピリオドいずれかで区切って指定するか、"+
    "半角数値で「年年年年月月日日」の８文字形式で指定してください。";
    if (!minYmd) { minInc = ""; minYmd = SysYmd.MinYmd; }
    if (!maxYmd) { maxInc = ""; maxYmd = SysYmd.MaxYmd; }
    var minYmdAllow = SysYmd.MinYmdAllow;
    str = str.replace(/\./g, '/'); // ドットは年月セパレータとみなす
    str = str.replace(/\-/g, '/'); // ハイフンも年月セパレータとみなす
    var ary = [];
    if (str.indexOf("/")>=0) {
        if (!str.match(/^[0-9]+\/[0-9]+\/[0-9]+$/)) return "年月日書式が正しくありません。"+baseErrMsg;
        var ary = str.split("/");
        ary[0]*=1; ary[1]*=1; ary[2]*=1; // 数値にする
    } else if (!str.match(/^[0-9]{8}$/)) return "年月日書式が正しくありません。"+baseErrMsg; // 区切り文字無しで、８文字でない場合
    else {
        ary = [str.substring(0,4)*1, str.substring(4,6)*1, str.substring(6,8)*1]; // 数値として格納する
    }
    var preErrMsg = ary[0]+"年"+ary[1]+"月"+ary[2]+"日は、";
    if (!ary[1] || ary[1]>12) return preErrMsg+"存在しない年月日です。"; // 月エラー
    if (!ary[2] || ary[2]>getDaysInMonth(ary[0], ary[1])) return preErrMsg+"存在しない年月日です。"; // 日の範囲エラー
    var nYmd = ary[0]*10000+ary[1]*100+ary[2];
    if (nYmd < minYmd*1) {
		if (!SysYmd.MinYmdAllow || nYmd!=int(SysYmd.MinYmd)) {
			minInc +="";
			var minJp = (minInc.length ? (minInc=="0" ? "本日 − " : (minInc.replace("-",""))+"日"+(minInc.indexOf("-")==0?"前":"後")+" − ") : "");
			return preErrMsg+"指定可能な年月日の範囲を下回っています。（最小："+minJp+ymdToArray(minYmd, "slash_ymd")+"）";
		}
	}
    if (nYmd > maxYmd*1) {
		if (!SysYmd.MaxYmdAllow || nYmd!=int(SysYmd.MaxYmd)) {
			maxInc +="";
			var maxJp = (maxInc.length ? (maxInc=="0" ? "本日 − " : (maxInc.replace("-",""))+"日"+(maxInc.indexOf("-")==0?"前":"後")+" − ") : "");
			return preErrMsg+"指定可能な年月日の範囲を超えています。（最大："+maxJp+ymdToArray(maxYmd, "slash_ymd")+"）";
		}
	}
    return ary[0] + ("00"+ary[1]).slice(-2) + ("00"+ary[2]).slice(-2); // OK。8文字数値で返す
}
function ymdToArray(ymdhm, format) {
    ymdhm += "";
    if (ymdhm.indexOf("/")>=0) {
        var aYmd = ymdhm.split("/");
        if (aYmd.length!=3) ymdhm = "";
        else if (aYmd[0].length!=4) ymdhm = "";
        else if (aYmd[1].length!=1 && aYmd[1].length!=2) ymdhm = "";
        else if (aYmd[2].length!=1 && aYmd[2].length!=2) ymdhm = "";
        else ymdhm = ("0000"+aYmd[0]).slice(-4) + ("00"+aYmd[1]).slice(-2) + ("00"+aYmd[2]).slice(-2);
    }
    else {
        if (ymdhm.length < 8) {
            if (ymdhm.length!=4 && ymdhm.length!=6) ymdhm = "";
        }
    }
    var yy = substr2(ymdhm, 0, 4);
    var mm = substr2(ymdhm, 4, 2);
    var dd = substr2(ymdhm, 6, 2);
    var hh = substr2(ymdhm, 8, 2);
    var mi = substr2(ymdhm, 10, 2);
    var ymd = yy + mm + dd;
    var hm = hh + mi;
    var zmm = zeroSuppress(mm);
    var zdd = zeroSuppress(dd);
    var zhh = zeroSuppress(hh);
    var dt = new Date(yy, zmm-1, zdd);
    var ary = ["","月","火","水","木","金","土","日"];
    var weekNumber = dt.getDay();
    if (weekNumber==0) weekNumber = 7;// 月(1)〜日(7)
    var weekJp = ary[weekNumber];
    var ret = {
        yy:yy, mm:mm, dd:dd, hh:hh, mi:mi, ymd:ymd,
        zmm:zmm, zdd:zdd, zhh:zhh,
        weekNumber:weekNumber,
        md_jp : (mm ? mm+"月"+dd+"日" : ""),
        mdw_jp : (mm ? zmm+"月"+dd+"日("+ weekJp+")": ""),
        slash_mdw: (mm ? zmm+"/"+zdd+"("+ weekJp+")": ""),
        slash_ymd: (yy ? yy+"/"+mm+"/"+dd : ""),
        slash_zymd: (yy ? yy+"/"+zmm+"/"+zdd : ""),
        zmd_jp:(zmm ? zmm+"月"+zdd+"日" : ""),
        slash_md_hm:(yy ? yy+"/"+mm+"/"+dd+" "+hh+":"+mi : "")
    };
    if (format && typeof ret[format] == undefined) alert("日付フォーマット指定が不正："+format);
    if (format) return ret[format];
    return ret;
}


function encodeToUnicodeEntity(str) {
    var surrogate_1st = 0;
    var out = [];
    for (var i = 0; i < str.length; ++i) {
        var utf16_code = str.charCodeAt(i);
        if (surrogate_1st != 0) {
            if (utf16_code >= 0xDC00 && utf16_code <= 0xDFFF) {
                var surrogate_2nd = utf16_code;
                var unicode_code = (surrogate_1st - 0xD800) * (1 << 10) + (1 << 16) + (surrogate_2nd - 0xDC00);
                out.push("&#"+unicode_code.toString(10).toUpperCase() + ";");
            } else {
                // Malformed surrogate pair ignored.
            }
            surrogate_1st = 0;
        } else if (utf16_code >= 0xD800 && utf16_code <= 0xDBFF) {
            surrogate_1st = utf16_code;
        } else {
            out.push("&#"+utf16_code.toString(10).toUpperCase() + ";");
        }
    }
    return encodeURIComponent(out.join(""));
}

function c2ShowLoginPage() {
    function c2GetMainWin(win) {
        if (win.name=='comedix_main') return win;
        if (win.opener && win.opener.location.hostname==win.location.hostname) return c2GetMainWin(win.opener);
        if (win.top && win.top.location.hostname==win.location.hostname && win.top!=win) return c2GetMainWin(win.top);
        return win;
    }
    var contentsRoot = "";
    var pos = location.href.indexOf('/', 8);
    if (pos) pos = location.href.indexOf('/', pos+1);
    if (pos) contentsRoot = location.href.substring(0,pos+1);
    var mainWin = c2GetMainWin(window);
    mainWin.location.href = contentsRoot+"login.php";
    if (window!=mainWin && !window.closed) window.close();
}

var AJAX_ERROR = "AJAX_ERROR";
var TYPE_JSON = "TYPE_JSON";
function getCustomTrueAjaxResult(msg, type_json, isCheckOnly) {
    var errMsg = "";
    if (msg=="SESSION_NOT_FOUND") errMsg = "画面呼び出しが不正です。（SESSION_NOT_FOUND）";
    else if (msg=="DATABASE_CONNECTION_ERROR") errMsg = "データベース接続に失敗しました。";
    else if (msg=="DATABASE_SELECT_ERROR") errMsg = "データベース検索に失敗しました。";
    else if (msg=="DATABASE_UPDATE_ERROR") errMsg = "データベース更新に失敗しました。";
    else if (msg=="SYSTEM_ERROR") errMsg = "システムエラーが発生しました。";
    else if (msg=="NO_AUTH") errMsg = "機能操作権限がありません。";
    else if (msg=="SESSION_EXPIRED") errMsg = "・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。";
    if (errMsg) {
        alert(errMsg);
        c2ShowLoginPage();
        return AJAX_ERROR;
    }
    if (substr2(msg,0,2)=="ng") {
        alert(msg.substring(2));
        return AJAX_ERROR;
    }
    if (isCheckOnly) return "";

    if (substr2(msg,0,2)!="ok") {
//        if (!ee("errdump")) {
//            $("body").append('<div id="errdump" style="position:absolute;left:0;top:0; background-color:#444444; color:#ffffff"></div>');
//        }
//        ee("errdump").innerHTML = '<pre>'+msg;
        alert(msg);
        return AJAX_ERROR;
    }
    if (type_json!=TYPE_JSON) return msg.substring(2);
    var ret = "";
    try { eval("var ret = "+msg.substring(2)+";"); } catch(ex) {
        if (ee("dump")) ee("dump").innerHTML = htmlEscape(msg);
        alert("通信エラーが発生しました。(1)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    if (!ret) {
        alert("通信エラーが発生しました。(2)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    return ret;
}

function isParentEnabling(formId, elem) {
    if (!elem) return true;
    if (elem.disabled) return false;
    if (elem.style.display=="none") return false;
    if (elem.id==formId) return true;
    return isParentEnabling(formId, elem.parentNode);
}

function serializeUnicodeEntity(wrapId) {
    var data = getFormData(wrapId);
    var serials = [];
    for (var idx in data) {
        var obj = data[idx];
        serials.push("uni_entity__"+obj.name+"="+encodeToUnicodeEntity(obj.value));
    }
    return serials.join("&");
}

function getFormData(wrapId) {
    var data = [];
    var pushData = function(elem) {
        var obj = {};
        obj["name"] = elem.name;
        obj["value"] = elem.value;
        data.push(obj);
    }
    var wrapElem = ee(wrapId);
    $(wrapElem).find("input[type=text]").each(    function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $(wrapElem).find("input[type=password]").each(function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $(wrapElem).find("input[type=hidden]").each(  function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $(wrapElem).find("input[type=checkbox]").each(function(){ if(isParentEnabling(wrapId, this) && this.checked) pushData(this); });
    $(wrapElem).find("input[type=radio]").each(   function(){ if(isParentEnabling(wrapId, this) && this.checked) pushData(this); });
    $(wrapElem).find("select").each(              function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    $(wrapElem).find("textarea").each(            function(){ if(isParentEnabling(wrapId, this)) pushData(this); });
    return data;
}

var activateJavascriptSeq = 0;
function activateJavascript(innerHTMLId) {
    var removeTags = [];
    $("#"+innerHTMLId+" textarea.script").each(function() {
        activateJavascriptSeq++;
        removeTags.push(this);
        var html = this.value;
        var script = document.createElement('script');
        script.setAttribute("type", "text/javascript");
        script.setAttribute("script_id", activateJavascriptSeq);
        $(this.parentNode).append(script);
        script.text = html;
    });
    for (var idx=0; idx<removeTags.length; idx++) {
        $(removeTags[idx]).remove();
    }
}



//==================================================================================================
// （汎用）年月日計算
//==================================================================================================
function calcYmd(id, notSet) {
    var eYmd = ee("ymd#"+id);
    var eHH =  ee("hh#"+id);
    var eMI =  ee("mi#"+id);

    var ymd = eYmd.value;
    ymd = ymd.replace(/\./g, '/'); // ドットは年月セパレータとみなす
    if (ymd.indexOf("/")>=0) {
        var aYmd = ymd.split("/");
        if (aYmd.length!=3) { ee(id).value = ""; return ""; }
        ymd = ("0000"+aYmd[0]).slice(-4) + ("00"+aYmd[1]).slice(-2) + ("00"+aYmd[2]).slice(-2);
    }

    var aYmd = ymdToArray(ymd);
    var out = aYmd["ymd"];
    // 年月日値と時フィールド値有りなら時分を前方ゼロ埋めで足す
    if (out.length==8 && eHH && eHH.value.length>0 && eMI) {
        out += ("00"+eHH.value).slice(-2) + ("00"+eMI.value).slice(-2);
    }
    if (!notSet) ee(id).value = out; // 保存用の隠しフィールドにYYYYMMDDを格納する
    return out; // 戻り値としても返す
}
function calcYymm(id, notSet) {
    var eYY = ee("yyyy#"+id);
    var eMM = ee("mm#"+id);
    var ym1st = getYmd(eYY.value+"/"+eMM.value+"/01"); // 1月1日で確認
    if (!notSet) ee(id).value = ym1st; // 保存用の隠しフィールドにYYYYMM01（ついたち）を格納する
    return ym1st; // 戻り値としても返す
}




//==================================================================================================
// ハイライタ
//==================================================================================================
function Hilighter($targetElements, resultElemId, scrollPaneId, scrollFunc, colorProc, matchedCallback) {
	this.$targetElements = $targetElements;
	this.resultElemId = resultElemId;
	this.scrollPaneId = scrollPaneId;
	this.scrollFunc = scrollFunc;
	this.colorProc = colorProc;
	this.matchedCallback = matchedCallback;
    this.lastInfo = {key:"", scrollTop:-1, focusIdx:-1};
    this.lastMatchedKeyword = "";
}
Hilighter.prototype = {
	startHilight : function(key) {
        key = key.replace(/　/g, " ", key);
        var tmp = key.split(" ");
        var keys = [];
        for(var idx=0; idx<tmp.length; idx++) {
            if (tmp[idx]!="") if (keys.length < 3) keys.push(tmp[idx]);
        }
        var tryCount = 0;
        var matches = [];
        var self = this;
        this.$targetElements.each(function() {
            tryCount++;
            var bool = self.isMatch(this, keys);
            self.setHilightColor(this, (bool ? "on" : ""), "");
            if (bool) matches.push(this);
        });
        if (this.resultElemId) ee(this.resultElemId).innerHTML = (tryCount && keys.length ? "<nobr>該当"+(matches.length)+"箇所</nobr>" : "");
		if (this.matchedCallback) this.matchedCallback(key, matches);

		// 所定位置へスクロール
		var scTop = ee(this.scrollPaneId).scrollTop;
		if (this.lastInfo.key==key && matches.length) {
			this.lastInfo.focusIdx++;
			if (this.lastInfo.focusIdx > matches.length-1) this.lastInfo.focusIdx = 0;
			var focusElem = matches[this.lastInfo.focusIdx];
			var scTop = this.scrollFunc(focusElem);
			$(ee(this.scrollPaneId)).stop().animate({scrollTop:scTop});
			this.lastInfo.scrollTop = ee(this.scrollPaneId).scrollTop;
			self.setHilightColor(focusElem, "on", "yes");
		}
		this.lastInfo.key = key;
		this.lastMatchedKeyword = (matches.length ? key : "");
	},
	isMatch : function(elem, keys) {
        if (!keys.length) return "";
        var str = "";
        if (elem.tagName=="INPUT") str = elem.value;
        else str = elem.innerHTML;
        for (var idx in keys) {
            var key = keys[idx];
            if (str.indexOf(key)<0) return "";
        }
        return "on";
	},
    setHilightColor : function (elem, matched, isFocus) {
		if (this.colorProc) return this.colorProc(elem, matched, isFocus);
        elem.style.backgroundColor = (matched ? "#eeee88" : "");
        var clr = "#ffffff";
        if (matched) clr = "#eeee88";
        if (isFocus) clr = "#ff9944";
        elem.style.border = "2px solid "+clr;
    }
};





//==================================================================================================
// カレンダ
//==================================================================================================
var CAL = {
    curYmd: {},
    tmpYmd: {},
    nowYmd : {},
    startYear : 0,
    calDialog : 0,
    targetid : "",
    minYear : "",
    maxYear : "",
    warekiMode : "1",
    showCalendar: function(targetId) {
		if (!window.SysYmd) alert("SysYmdがありません(CAL)");
        this.targetId = targetId;
        var self = this;
        var ids1 = targetId.split("#");
        if (!ee(ids1[1])) {
			alert("カレンダ呼出しが不正です。（ID="+ids1[1]+"）");
			return;
		}
        this.calDialog = $("#calendar_dialog_container").dialog({
            title:"日付選択", width:385, height:365, dialogClass:'', modal:true,
            open:function(){
            var ymd = ee(ids1[1]).value;
            ymd = getYmd(ymd, SysYmd.MinYmd, SysYmd.MaxYmd);
            self.curYmd = ymdToArray(ymd);
            var today = new Date();
            self.nowYmd = {yy:today.getFullYear(), mm:today.getMonth()+1, dd:today.getDate()};
            var todayYmd = self.nowYmd.yy+"/"+("00"+self.nowYmd.mm).slice(-2)+"/"+("00"+self.nowYmd.dd).slice(-2);
            var refYmd = (ymd ? self.curYmd : self.nowYmd);
            self.tmpYmd.yy = refYmd.yy; self.tmpYmd.mm = refYmd.mm; self.tmpYmd.dd = refYmd.dd;
            self.startYear = self.tmpYmd.yy - ((self.tmpYmd.yy-1)%10) - 10; // 10で割り切れる年の翌年から数えて、その10年前から描画
            var htY = [];
            htY.push('<a href="javascript:void(0)" onclick="CAL.scrollY(1)"');
            htY.push(' style="float:left; height:20px; width:55px; font-size:11px; color:#999999; line-height:20px">過去</a>');
            htY.push('<a href="javascript:void(0)" onclick="CAL.scrollY(0)"');
            htY.push(' style="float:left; height:20px; width:55px; font-size:11px; color:#999999; line-height:20px">未来</a>');
            htY.push('<div style="overflow:hidden; height:250px; width:110px; border-top:7px solid #ffffff; border-bottom:5px solid #ffffff"');
            htY.push(' id="calendar_year_scroll">');
            var tagTitle = "";
            if (self.warekiMode=="1") tagTitle = ' title="対象年の12月31日時点の元号です"';
            htY.push('<table id="calendar_year_container" style="width:330px; height:250px"'+tagTitle+' cellspacing="0" cellpadding="0"><tr>');
            for (var idx1=0; idx1<=2; idx1++) {
				htY.push('<td>');
                for (var idx2=1; idx2<=10; idx2++) {
                    htY.push('<a id="calendar_year'+(idx1*10+idx2)+'" href="javascript:void(0)" onclick="CAL.clickY(this)"></a>');
                }
				htY.push('</td>');
            }
            htY.push('</tr></table>');

            htY.push('</div>');

            var htM = [];
            htM.push('<table cellspacing="0" cellpadding="0">');
            htM.push('<tr>');
            for (var idx=1; idx<=6; idx++) {
                htM.push('<td><a style="width:40px;" id="calendar_month'+(idx)+'"');
                htM.push(' href="javascript:void(0)" onclick="CAL.clickM('+(idx)+')">');
                htM.push((idx)+'<span class="font11" style="padding-left:2px">月</span></a></td>');
            }
            htM.push('</tr>');
            htM.push('<tr>');
            for (var idx=7; idx<=12; idx++) {
                htM.push('<td><a style="width:40px;" id="calendar_month'+(idx)+'"');
                htM.push(' href="javascript:void(0)"  onclick="CAL.clickM('+(idx)+')">');
                htM.push(idx+'<span class="font11" style="padding-left:2px">月</span></a></td>');
            }
            htM.push('</tr>');
            htM.push('</table>');

            var ret = [];
			var sSysMinYmd = ymdToArray(SysYmd.MinYmd, "slash_ymd");
			var sSysMaxYmd = ymdToArray(SysYmd.MaxYmd, "slash_ymd");
            ret.push('<table cellspacing="0" cellpadding="0"><tr style="padding-bottom:5px">');
            ret.push('<th><a href="javascript:void(0)" onclick="CAL.clickD(\'\')" style="width:80px">クリア</a></th>');
            ret.push('<th style="padding-left:5px"><a href="javascript:void(0)"');
            ret.push(' onclick="CAL.clickD(\''+todayYmd+'\')" style="width:80px">本日</a></th>');
            ret.push('<th style="padding-left:5px"><a href="javascript:void(0)"');
            ret.push(' onclick="CAL.clickD(\''+sSysMinYmd+'\')" style="width:98px">'+sSysMinYmd+'</a></th>');
            ret.push('<th style="padding-left:5px"><a href="javascript:void(0)"');
            ret.push(' onclick="CAL.clickD(\''+sSysMaxYmd+'\')" style="width:98px">'+sSysMaxYmd+'</a></th>');
            ret.push('</tr></table>');

            ret.push('<table cellspacing="0" cellpadding="0" style="width:385px"><tr>');
            ret.push('<th style="width:120px; vertical-align:top; padding-top:10px">');

            ret.push('<table cellspacing="0" cellpadding="0" style="width:100%"><tr>');
            ret.push('<th style="width:110px;" id="td_calendar_year">' + htY.join("") + '</th>');
            ret.push('</tr></table>');

            ret.push('</th>');
            ret.push('<th>');

            ret.push('<table cellspacing="0" cellpadding="0" style="width:100%">');
            ret.push('<tr><th style="width:80px; padding:10px 0 0 10px" id="td_calendar_month">' + htM.join("") + '</th></tr>');
            ret.push('</table>');

            ret.push('<table cellspacing="0" cellpadding="0">');
            ret.push('<tr><th style="width:260px; padding-left:10px" id="td_calendar_date"></th></tr>');
            ret.push('</table>');

            ret.push('</th></tr></table>');
            ee("calendar_dialog_content").innerHTML = ret.join("");
            self.drawY();
            self.drawM();
            self.drawD();
            tryFocus("calendar_close");
        }});
    },
    scrollY : function(isUp){
        var scLeft = (isUp ? 0 : 220);
        var self = this;
        self.startYear += (isUp ? -10 : 10);
        $("#calendar_year_scroll").animate({scrollLeft:scLeft}, { duration:150, done:function() {
            self.drawY();
        }});
    },
    drawY : function() {
        for (var idx=1; idx<=30; idx++) {
            var yy = this.startYear-1+idx;
            var wareki = "";
            var tagTitle = "";
            if (this.warekiMode=="1") {
                wareki = WDCONV.seirekiToWareki(yy+"1231", 4, "GY");
                if (wareki) wareki = "／" + wareki.replace("年", "");
            }
            ee("calendar_year"+idx).innerHTML = yy + '<span class="font11" style="padding-left:2px">年</span>'+wareki;
            var cls = "";
            if (yy==this.tmpYmd.yy) cls = "cal_selected";
            else if (yy==this.nowYmd.yy) cls = "cal_today";
            ee("calendar_year"+idx).className = cls;
            ee("calendar_year"+idx).setAttribute("year_value", yy);
        }
        ee("calendar_year_container").parentNode.scrollLeft = 110; // 先頭から5行分先へスクロール
    },
    drawM : function() {
        for (var idx=1; idx<=12; idx++) {
            var cls = "";
            if (idx==this.tmpYmd.mm) cls = "cal_selected";
            else if (idx==this.nowYmd.mm && this.tmpYmd.yy==this.nowYmd.yy) cls = "cal_today";
            ee("calendar_month"+idx).className = cls;
        }
    },
    drawD : function() {
        var yy = this.tmpYmd.yy;
        var mm = this.tmpYmd.mm;
        var dd = this.tmpYmd.dd;
        var htD = [];
        htD.push('<div>日</div><div>月</div><div>火</div><div>水</div><div>木</div><div>金</div><div>土</div>');

        var oneDay = (60*60*24*1000);
        var getumatubi = new Date(yy, mm, 0);
        var ippi = new Date(yy, mm-1, 1);
        var ippiYoubi = ippi.getDay(); // 日曜がゼロ、土曜が６
        var tDate = new Date(ippi - oneDay * (1 + ippiYoubi)); // １日を含む週よりひとつ前の週の土曜
        for (;;) {
            tDate = new Date(tDate.getTime() + oneDay*1);
            tYoubi = tDate.getDay();
            var tYY = tDate.getFullYear();
            var tMM = tDate.getMonth()+1;
            var tDD = tDate.getDate();
            var tYmd = tYY + tMM + tDD;

            var cls = '';
            if (tMM!=mm) { cls += ' calendar_other_month'; }
            if (tYY==this.nowYmd.yy && tMM==this.nowYmd.mm && tDD==this.nowYmd.dd) cls += ' cal_today';
            if (tYY==this.curYmd.yy && tMM==this.curYmd.mm && tDD==this.curYmd.dd) cls += ' cal_selected';
            htD.push('<a class="'+cls+'" onclick="CAL.clickD(\''+tYY+"/"+("00"+(tMM)).slice(-2)+"/"+("00"+(tDD)).slice(-2)+'\')">'+int(tDD)+'</a>');
            if (tYoubi==6 && tDate >=getumatubi) break;
        }
        ee("td_calendar_date").innerHTML = htD.join("");
    },
    clickY : function(elem) {
        this.tmpYmd.yy = elem.getAttribute("year_value");
        this.drawY();
        this.drawM();
        this.drawD();
    },
    clickM : function(mm) {
        this.tmpYmd.mm = mm;
        this.drawY();
        this.drawM();
        this.drawD();
    },
    clickD : function(ymd) {
        ee(this.targetId).value = ymd;
        $(ee(this.targetId)).change();
        this.calDialog.dialog("close");
        tryFocus(this.targetId);
    }
}



//**************************************************************************************************
// DIFFチェック
//**************************************************************************************************
var watchDiffStatus = {};
var watchInputError = {};
function watchDiff(elem, muid, isKeyup) {
	_watchDiff(elem, muid, isKeyup);
	syozokuScanAndChange(elem, muid, isKeyup);
}


function _watchDiff(elem, muid, isKeyup) {
    if (!muid) return;
    var isDiff = 0;
    var tagName = elem.tagName.toLowerCase();
    var defval = elem.getAttribute("defval");
    if (tagName=="textarea") defval = ee("defval#"+elem.id).value;
    val = "";
    if ((tagName=="input") || tagName=="textarea") val = elem.value;
    if (tagName=="select") val = elem.options[elem.selectedIndex].value;
    if (val != defval) isDiff = 1;

    var errmsg = "";
    var regexp = elem.getAttribute("regexp");
    var is_hissu = (elem.className.indexOf("is_hissu")>=0 ? 1 : 0);
    if (is_hissu && val=="") errmsg = "必須入力です。";
    else if (val!="") {
        if (regexp) {
            if (regexp=="ymd") {
                var ymdTerm = (elem.getAttribute("ymd_term")+",,,").split(",");
                var minInc = (ymdTerm[0]=="today+" ? int(ymdTerm[1]) : (ymdTerm[0]=="today-" ? int(ymdTerm[1])*-1 : "no-setting"));
                var minYmd = (minInc=="no-setting" ? "" : ymdDateAdd(getTodayYmd(), minInc));
                var maxInc = (ymdTerm[2]=="today+" ? int(ymdTerm[3]) : (ymdTerm[2]=="today-" ? int(ymdTerm[3])*-1 : "no-setting"));
                var maxYmd = (maxInc=="no-setting" ? "" : ymdDateAdd(getTodayYmd(), maxInc));
                if (val!=defval && !getYmd(val, minYmd, maxYmd)) errmsg = getYmdOrErr(val, minYmd, maxYmd, minInc+"", maxInc+"");
            } else if (regexp=="yyyy" || regexp=="mm") {
                var ymTerm = (elem.getAttribute("ym_term")+",,,").split(",");
                var minInc = (ymTerm[0]=="today+" ? int(ymTerm[1]) : (ymTerm[0]=="today-" ? int(ymTerm[1])*-1 : "no-setting"));
                var aMinYmd = (minInc=="no-setting" ? "" : ymdToArray(ymdMonthAdd(getTodayYmd(), minInc)));
                var maxInc = (ymTerm[2]=="today+" ? int(ymTerm[3]) : (ymTerm[2]=="today-" ? int(ymTerm[3])*-1 : "no-setting"));
                var aMaxYmd = (maxInc=="no-setting" ? "" : ymdToArray(ymdMonthAdd(getTodayYmd(), maxInc)));
                if (regexp=="yyyy") {
                    if (!val.match("^[0-9]+$")) errmsg = "西暦年を半角数値４桁で指定してください。";
                    else if (aMinYmd && val!=defval) {
                        if (val<aMinYmd.yy) errmsg = "指定可能な年の値を下回っています。（最小："+aMinYmd.yy+"/"+aMinYmd.mm+"）";
                        else if (val>aMaxYmd.yy) errmsg = "指定可能な年の値を超えています。（最大："+aMaxYmd.yy+"/"+aMaxYmd.mm+"）";
                    }
                } else {
                    if (!val.match("^[0-9]+$")) errmsg = "月を半角数値で指定してください。";
                    else if (int(val)<1 || int(val)>12) errmsg = "月の値が不正です。";; // 月の確認。1から12
                    if (aMinYmd && !errmsg && val!=defval) {
                        var elemY = ee(elem.id.replace("yyyy#","mm"));
                        if (elemY && elemY.value && elemY.value.length==4) {
                            if (elemY.value==aMinYmd.yy && int(val) < aMinYmd.mm) {
                                errmsg = "指定可能な月の値を下回っています。（最小："+aMinYmd.yy+"/"+aMinYmd.mm+"）";
                            } else if (elemY.value==aMaxYmd.yy && int(val) > aMaxYmd.mm) {
                                errmsg = "指定可能な月の値を超えています。（最大："+aMaxYmd.yy+"/"+aMaxYmd.mm+"）";
                            }
                        }
                    }
                }
            } else if (regexp=="hh") {
                if (!val.match("^[0-9]+$")) errmsg = "時刻を半角数値で指定してください。";
                else if (val!="0" && val!="00" && !int(val)>=1 && int(val)<=23) errmsg = getYmdOrErr(val); // 時の確認。0から23
            } else if (regexp=="mi") {
                if (!val.match("^[0-9]+$")) errmsg = "時刻を半角数値で指定してください。";
                else if (val!="0" && val!="00" && !int(val)>=1 && int(val)<=59) errmsg = getYmdOrErr(val); // 分の確認。0から59
            } else {
                if (!val.match(regexp)) errmsg = "入力が不正です。";
            }
        }
    }
    if (!errmsg) {
        var elemIds = elem.id.split("@");
        var uJP = "";
        if (elemIds[0]=="empmst" && elemIds[1]=="emp_personal_id") uJP = "職員ID";
        if (elemIds[0]=="empmst" && elemIds[1]=="emp_idm") uJP = "ICカードIDm";
        if (elemIds[0]=="login" && elemIds[1]=="emp_login_id") uJP = "ログインID";
        if (elemIds[0]=="login" && elemIds[1]=="emp_login_mail") uJP = "ログインメールID";
        var checkEmpId = currentEmpId;
        if (elemIds.length > 5 && int(elemIds[5])==int(NEW_EMP_MUID)) checkEmpId = ""; // 新規登録時はカラにする
        if (uJP) {
            var isUnique = 0;
            var async = (isKeyup ? true : false);
            $.ajax({
                url:"common.php?ajax_action=check_id_unique&field_id="+elemIds[1]+"&emp_id="+checkEmpId+"&"+elemIds[1]+"="+encodeURIComponent(val),
                async:async, success:function(msg) {
                var ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
                errmsg = str(ret.error);
                if (!errmsg && !ret["is_unique"]) errmsg = uJP+"の一意性が確認できませんでした。";
                _watchDiff2(elem, muid, isDiff, errmsg);
            }});
            return;
        }
    }
    _watchDiff2(elem, muid, isDiff, errmsg);
    if (regexp=="ymd") elem.setAttribute("wareki_navi", WDCONV.seirekiToWareki(val, 4, "GYMD"));
    if (regexp=="yyyy") elem.setAttribute("wareki_navi", WDCONV.seirekiToWareki(val, 4, "GY"));
}



function _watchDiff2(elem, muid, isDiff, errmsg) {
    elem.setAttribute("navi", errmsg);
    if (errmsg) $(elem).addClass("value_error");
    else $(elem).removeClass("value_error");
    if (isDiff) $(elem).addClass("value_diff");
    else $(elem).removeClass("value_diff");

    if (errmsg) {
        if (!watchInputError[muid]) watchInputError[muid] = {};
        watchInputError[muid][elem.id] = errmsg;
    } else {
        if (watchInputError[muid]) {
            delete watchInputError[muid][elem.id];
            var ok = 0;
            for (var obj in watchInputError[muid]) ok = 1;
            if (!ok) delete watchInputError[muid];
        }
    }

    if (isDiff) {
        if (!watchDiffStatus[muid]) watchDiffStatus[muid] = {};
        watchDiffStatus[muid][elem.id] = 1;
    } else {
        if (watchDiffStatus[muid]) {
            delete watchDiffStatus[muid][elem.id];
            var ok = 0;
            for (var obj in watchDiffStatus[muid]) ok = 1;
            if (!ok) delete watchDiffStatus[muid];
        }
    }
    changeTabColor(muid);
}



//**************************************************************************************************
// ナビ
//**************************************************************************************************
var gNavi = {
    globalMouseX : 0,
    globalMouseY : 0,
    $tblNavi : 0,
    iframeNavi : 0,
    tdNavi : 0,
    thNaviTitle : 0,
    activeMode : 1,
    initialize : function() {
        var self = this;
        var tag =
        '<table cellspacing="0" cellpadding="0" id="tblNavi" onmouseover="gNavi.hide(this)"><tr>' +
        '    <th id="thNaviTitle"></th><td id="tdNavi"></td>' +
        '</tr></table>';
        $("body").append(tag);
        this.$tblNavi = $("#tblNavi");
        this.iframeNavi = ee("iframeNavi");
        this.tdNavi = ee("tdNavi");
        this.thNaviTitle = ee("thNaviTitle");
        $("html").mousemove(function(evt){
            self.globalMouseX = evt.pageX;
            self.globalMouseY = evt.pageY;
        });
        $(document).on('mouseenter', '[navi],[navititle]', function(){ self.show(this); });
        $(document).on('mouseleave', '[navi],[navititle]', function(){ self.hide(); });
    },
    show : function(target){
        if (!gNavi.activeMode) return;
        var $target = $(target);
        var gmsg = trim($target.attr("navi"));
        var gwareki = trim($target.attr("wareki_navi"));
        var gtitle = trim($target.attr("navititle"));
        var bGmsg = (gmsg!="" || gwareki!="");
        var bGtitle = (gtitle!="");
        if (!bGmsg && !bGtitle) return;

        var win = window;
        if (parent && parent.floatingpage) win = parent.floatingpage;
        var scTop = $(win).scrollTop();
        var wh = win.innerHeight;
        if (document.all || win.opera) wh = win.document.documentElement.clientHeight;
        if (!wh) wh = win.document.getElementsByTagName("body")[0].clientHeight;
        if (!wh) wh = document.body.clientHeight;

        if (wh<1) return;

        var ww = win.innerWidth;
        if (!ww && document.documentElement) ww = document.documentElement.clientWidth;
        if (!ww) ww = document.body.clientWidth;
        if (!ww) return;

        this.tdNavi.innerHTML = gwareki + (gwareki ? '<br />' : '') + gmsg.replace(/(\n)/g, "<br />");
        this.tdNavi.style.display = (bGmsg ? "" : "none");
        this.thNaviTitle.innerHTML = trim(gtitle);
        this.thNaviTitle.style.display = (bGtitle ? "" : "none");

        this.$tblNavi.css({top:-1000, width:"auto"});
        this.$tblNavi.show();

        var tblh = this.$tblNavi.get(0).offsetHeight;
        var tblw = this.$tblNavi.get(0).offsetWidth;
        if (tblw > ww - 20) { tblw = ww - 20; this.$tblNavi.css({"width":tblw}); }
        this.$tblNavi.hide();

        var wy = (wh - tblh - 20);
        if (wy <= this.globalMouseY-scTop) wy = this.globalMouseY - scTop - tblh - 30;

        this.$tblNavi.css({top:wy, left:18});

        this.$tblNavi.fadeIn(300);
    },
    hide : function() {
        if (this.$tblNavi) this.$tblNavi.stop(true, true).hide();
    }
};



//==================================================================================================
// ページャ関連
//==================================================================================================
function checkPageNum(new_page_num) {
    if (!new_page_num.match( /^[1-9][0-9]*$/g)) { alert("数字を指定してください。"); return 0; }
    return new_page_num;
}



//==================================================================================================
// Admin/User共通
//==================================================================================================
function tryAdminReg(regAreaTagId, ajax_action, option, noDialog, noScroll, finishFunc) { // for admin
    if (!option) option = "";
    var postData = (regAreaTagId ? serializeUnicodeEntity(regAreaTagId) : "");
    var url = "common.php?ajax_action="+ajax_action+option;
    var ret = 0;
    $.ajax({ url:url, data:postData, success:function(msg) {
        if (finishFunc) finishFunc();
        var ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
        if (!noDialog && !ret.returnMsg) alert("保存しました。");
        if (ret.returnMsg) alert(ret.returnMsg);
        if (!ret.forwardSection && !ret.forwardUrl && !ret.forwardCommand) { alert("画面リロードに失敗しました。"+msg); return; }
        closeDialog("all");
        if (ret.forwardCommand) { ret.forwardCommand(); return; }
        if (ret.forwardUrl) { window.location.href = ret.forwardUrl; return; }
        if (ret.notReload) return;
        var scTop = ee("div_content").scrollTop;
        var scLeft = ee("div_content").scrollLeft;
        showAdminSect(ret.forwardSection, ret.forwardOption, ret.postAreaTagId);
        if (noScroll) ee("div_content").scrollTop = scTop;
        if (noScroll) ee("div_content").scrollLeft = scLeft;
        ret = 1;
    }});
    return ret;
}



function showAdminSect(section, option, postAreaTagId) { // for admin
    if (!option) option = "";
    var cont = ee("div_section_header_container");
    var postData = (postAreaTagId ? "&"+serializeUnicodeEntity(postAreaTagId) : "");
    cont.innerHTML = '<table class="section_title"><tr><td></td></tr></table>';
    ee("div_content").innerHTML = "";
    if (currentContent && ee("menu_"+currentContent)) ee("menu_"+currentContent).className = "";
    currentContent = section;
    if (ee("menu_"+section)) {
        //alert("メニューがありません"+currentContent);
        ee("menu_"+section).className = "menu_selected"; // 開閉状態を開くにする
    }
    $.ajax({ url:"common.php?ajax_action=html_"+section+option, data:postData, success:function(msg) {
        var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
        ee("div_content").style.height = "200px";
        ee("div_content").innerHTML = msg;
        var sectionHeader = ee("div_section_header");
        cont.innerHTML = '';
        if (sectionHeader) cont.appendChild(sectionHeader);
        activateJavascript("div_content");
        resizeIframeEx();
    }});
}

function closeDialog(id) {
    if (id=="all") {
        for (var did in $dialogs) if (did!="all") closeDialog(did);
    }
    $dlg = $dialogs[id];
    if (!$dlg) return;
    $dlg.dialog("close").remove();
    delete $dialogs[id];
}


var currentHid = "";
var currentATag1 = "";
var currentATag2 = "";
function popupBaritessDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("研修講義選択（バリテス）", "dialog_baritess_selector", "&callback_func=baritessSelectorCallback", "", "baritess_dialog");
}
function popupKensyuDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("研修", "dialog_kensyu_selector", "&callback_func=kensyuSelectorCallback", "", "kensyu_dialog");
}
function popupHyokaSyugiDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("研修", "dialog_hyoka_syugi_selector", "&callback_func=hyokaSyugiSelectorCallback", "", "hyoka_syugi_dialog");
}
function popupNShikakuDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("認定資格", "dialog_nshikaku_selector", "&callback_func=nshikakuSelectorCallback", "", "nshikaku_dialog");
}
function popupKousyuDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("講習資格", "dialog_kousyu_selector", "&callback_func=kousyuSelectorCallback", "", "kousyu_dialog");
}
function popupGakkaiDialog(huid, aTag1, aTag2) {
    currentHid = huid;
    currentATag1 = aTag1;
    currentATag2 = aTag2;
    popupAdmDlg("学会参加", "dialog_gakkai_selector", "&callback_func=gakkaiSelectorCallback", "", "gakkai_dialog");
}
function popupSqe(composit, emp_id, han_id) {
    var opt = ',left=0, top=0, location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes';
	var param = "&emp_id="+encodeURIComponent(emp_id)+"&composits="+composit+"&han_id="+han_id;
	var size = getWindowAvailSize();
	window.open("common.php?screen_action=get_user_hyoka"+param, "", 'width='+size.width+',height='+size.height+opt);
}

function baritessSelectorCallback(contentsId, contentsTitle) {
    ee("baritess_contents_id@"+currentHid).value = contentsId;
    if (window.gSqeScreen) watchDiff(ee("baritess_contents_id@"+currentHid), gSqeScreen);
    closeDialog("baritess_dialog");
    var disp = (contentsId ? contentsId+":"+contentsTitle : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!contentsId) return;

    var url = "common.php?ajax_action=get_baritess_status&emp_id="+currentEmpId+"&contents_id="+contentsId;
    $.ajax({ url:url, success:function(msg) {
        var ret = getCustomTrueAjaxResult(msg, ""); if (ret==AJAX_ERROR) return;
        currentATag2.style.display = "block";
        currentATag2.setAttribute("contents_id", contentsId);
        currentATag2.innerHTML = ret;
    }});
}
function kensyuSelectorCallback(kensyuId, kensyuNm) {
    ee("kensyu_id@"+currentHid).value = kensyuId;
    if (window.gSqeScreen) watchDiff(ee("kensyu_id@"+currentHid), gSqeScreen);
    closeDialog("kensyu_dialog");
    var disp = (kensyuId ? kensyuId+":"+kensyuNm : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!kensyuId) return;

    currentATag2.style.display = "block";
    currentATag2.setAttribute("kensyu_id", kensyuId);
    currentATag2.innerHTML = '<img class="profile_link" alt="" src="./img/spacer.gif" height="16" width="20"><div>'+kensyuNm+'</div>';
}
function hyokaSyugiSelectorCallback(hyokaSyugiId, hyokaSyugiNm) {
    ee("hyoka_syugi_id@"+currentHid).value = hyokaSyugiId;
    if (window.gSqeScreen) watchDiff(ee("hyoka_syugi_id@"+currentHid), gSqeScreen);
    closeDialog("hyoka_syugi_dialog");
    var disp = (hyokaSyugiId ? hyokaSyugiId+":"+hyokaSyugiNm : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!hyokaSyugiId) return;

    currentATag2.style.display = "block";
    currentATag2.setAttribute("hyoka_syugi_id", hyokaSyugiId);
    currentATag2.innerHTML = '<img class="profile_link" alt="" src="./img/spacer.gif" height="16" width="20"><div>'+hyokaSyugiNm+'</div>';
}
function nshikakuSelectorCallback(nshikakuId, nshikakuNm) {
    ee("nshikaku_id@"+currentHid).value = nshikakuId;
    if (window.gSqeScreen) watchDiff(ee("nshikaku_id@"+currentHid), gSqeScreen);
    closeDialog("nshikaku_dialog");
    var disp = (nshikakuId ? nshikakuId+":"+nshikakuNm : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!nshikakuId) return;

    currentATag2.style.display = "block";
    currentATag2.setAttribute("nshikaku_id", nshikakuId);
    currentATag2.innerHTML = '<img class="profile_link" alt="" src="./img/spacer.gif" height="16" width="20"><div>'+nshikakuNm+'</div>';
}
function kousyuSelectorCallback(kousyuId, kousyuNm) {
    ee("kousyu_id@"+currentHid).value = kousyuId;
    if (window.gSqeScreen) watchDiff(ee("kousyu_id@"+currentHid), gSqeScreen);
    closeDialog("kousyu_dialog");
    var disp = (kousyuId ? kousyuId+":"+kousyuNm : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!kousyuId) return;

    currentATag2.style.display = "block";
    currentATag2.setAttribute("kousyu_id", kousyuId);
    currentATag2.innerHTML = '<img class="profile_link" alt="" src="./img/spacer.gif" height="16" width="20"><div>'+kousyuNm+'</div>';
}
function gakkaiSelectorCallback(gakkaiId, gakkaiNm) {
    ee("gakkai_id@"+currentHid).value = gakkaiId;
    if (window.gSqeScreen) watchDiff(ee("gakkai_id@"+currentHid), gSqeScreen);
    closeDialog("gakkai_dialog");
    var disp = (gakkaiId ? gakkaiId+":"+gakkaiNm : "(指定なし)");
    currentATag1.innerHTML = '<img class="edit_link" width="20" height="16" alt="" src="./img/spacer.gif" /><div>'+disp+'</div>';

    if (!currentATag2) return;
    currentATag2.style.display = "none";
    if (!gakkaiId) return;

    currentATag2.style.display = "block";
    currentATag2.setAttribute("gakkai_id", gakkaiId);
    currentATag2.innerHTML = '<img class="profile_link" alt="" src="./img/spacer.gif" height="16" width="20"><div>'+gakkaiNm+'</div>';
}
function openAdminHelp(az) {
    window.open("help/index_admin.html#adminmenu_"+az, "spf_help", "width:900,height:600,scrollbars=yes,resizable=yes,menubar=no,toolbar=yes,location=no,status=no");
}

var currentContent = "";
function _popupComDlg(title, html, dialogName, closeEvent, dlgOption) {
    if (!dialogName) dialogName = "GeneralDialog";
    var innerId = "dialogInner"+dialogName;
    var dlg = ee("dialog_workspace");
    var inner = ee(innerId);
    if (inner) { inner.innerHTML = ""; inner.parentNode.removeChild(inner); }
    dlg.innerHTML = '<div id="'+innerId+'">'+html+'</div>';
    //activateJavascript(innerId);
    var dw = dlg.offsetWidth + 15;
    var dh = dlg.offsetHeight + 45;
    if (dh > 600) {
		$("#"+innerId).css({overflowY:"scroll"});
		dw += 20;
		dh = 600;
	}
    if (!dlgOption) dlgOption = {};
    if (dlgOption["WidthScrollerMargin"]) dw += 20;
    $dialogs[dialogName] = $("#"+innerId).dialog({title:title, width:dw, height:dh, modal:true, close:function(){
        if (closeEvent) closeEvent();
        closeDialog(dialogName);
    }, open:function(){
	    activateJavascript(innerId);
       $(ee(innerId).parentNode).find("div.ui-dialog-titlebar").get(0).className = "ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix "+innerId;
    }});
    return $dialogs[dialogName];
}
function popupAdmDlg(title, ajax_action, params, postAreaTagId, dialogName) {
	if (!params) params = "";
    var postData = (postAreaTagId ? "&"+serializeUnicodeEntity(postAreaTagId) : "");
    var $dlg = 0;
    $.ajax({ url:"common.php?ajax_action="+ajax_action+params, data:postData, success:function(msg) {
        var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
        $dig = _popupComDlg(title, msg, dialogName, "");
    }});
    return $dlg;
}

//------------------------------------------------------------------------------
// 選択肢ダイアログ（休職理由の「候補」など）
//------------------------------------------------------------------------------
function popupAssistDialog(elemId, selectionCsv) {
	_popupAssistDialog(elemId, selectionCsv.split(","));
}
function popupAssistRelMasterDialog(elemId, relMasterId) {
    $.ajax({ url:"common.php?ajax_action=get_master_selection_by_json&rel_master_id="+relMasterId, success:function(msg) {
        var ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
		_popupAssistDialog(elemId, ret);
    }});
}

var assistLastMatchKeyword = {};
function _popupAssistDialog(elemId, objects) {
	var _colorCallback = function(elem, matched, isFocus) {
        var clr = "";
        if (matched) clr = "#e3e3d3";
        if (isFocus) clr = "#eeee88";
        elem.style.backgroundColor = clr;
		ee("hdn_assist_last_text").value = elem.innerHTML;
	}
    var _focusClosure = function(focusElem) {
		return focusElem.offsetTop - 36;
	};
	var _matchedCallback = function(key, matches) {
		assistLastMatchKeyword[elemId] = (matches.length ? key : "");
	}

    htmlList = [];
    htmlList.push('<div id="assist_dialog_content">');
    var itemCount = 0;
    var uniqueList = {};
    for (id in objects) {
		var vv = trim(objects[id]);
		if (vv=="") continue;
		if (uniqueList[vv]) continue;

		itemCount++;
        htmlList.push('<a class="block black_link relative" style="padding:3px; border-bottom:1px solid #ffffff" href="javascript:void(0)"');
        htmlList.push(' onclick="var elem=ee(\''+elemId+'\'); elem.value=this.innerHTML; $(elem).change(); closeDialog(\'AssistDialog\')">'+vv+'</a>');
		uniqueList[vv] = 1;
    }
    htmlList.push('</div>');


    var html = [];
    html.push('<div class="absolute" style="background-color:#ffffff; padding-top:4px; height:30px; z-index:1" id="assist_dialog_fixed_header">');


    html.push('<table cellspacing="0" cellpadding="0" style="width:360px"><tr>');
    if (itemCount>15) { // 10個以下ならハイライターを出さない
	    html.push('<td><input type="text" class="text" onchange="assistHilighter.startHilight(this.value);" id="assist_dialog_text"');
		html.push(' navi="検索文字を入力し、エンターキーを押すと、ハイライト表示します。エンターキーを押すたびに該当行へスクロールします。"');
	    html.push(' onkeydown="if (isEnterKey(this,event)) { if (!isShiftKey(this,event)) { assistHilighter.startHilight(this.value); }}"');
	    html.push(' onkeyup="if (isEnterKey(this,event)) { if (isShiftKey(this,event)) { stopEvent(event); var elem=ee(\''+elemId+'\'); elem.value=ee(\'hdn_assist_last_text\').value; closeDialog(\'AssistDialog\'); $(elem).change(); }} ');
		html.push('"/></td>');
	    html.push('<td style="padding-left:10px" class="middle gray"><div id="div_assist_find_matchcount" style="width:110px"></div></td>');
	}
    if (itemCount>0) { // 1個もなければ空値指定ボタンも出さない
	    html.push('<td class="right middle" style="padding-left:10px;"><nobr><button class="w80" onclick="var elem=ee(\''+elemId+'\');');
	    html.push(' elem.value=\'\'; $(elem).change(); closeDialog(\'AssistDialog\')">空値指定</button></nobr></td>');
	}
    html.push('</tr></table>');
    html.push('<input type="hidden" id="hdn_assist_last_text" />');

    html.push('</div>');

	if (!itemCount) {
	    html.push('<div class="center gray" style="padding:30px 0 0 0;">候補はありません。</div>');
	}

    html.push('<div style="background-color:#ffffff; height:36px; width:364px">&nbsp;');
    html.push('</div>');


    _popupComDlg("候補", html.join("\n")+htmlList.join("\n"), "AssistDialog");
    window.assistHilighter = new Hilighter($("div#assist_dialog_content a"), "div_assist_find_matchcount", "dialogInnerAssistDialog", _focusClosure, _colorCallback, _matchedCallback);
    ee("hdn_assist_last_text").value = "";
    var adfh = ee("assist_dialog_fixed_header");
    adfh.style.width = (ee("dialogInnerAssistDialog").offsetWidth-26)+"px";
    adfh.parentNode.parentNode.appendChild(adfh);
    adfh.style.top = "32px";
    adfh.style.left = "6px";
    var lastKey = str(assistLastMatchKeyword[elemId]);
    if (lastKey) {
		ee("assist_dialog_text").value = lastKey;
		assistHilighter.startHilight(lastKey);
	}
    tryFocus("assist_dialog_text");
}

function popupWindowByPost(location, option, postAreaTagId) {
    if (!option) option = "";
    var postData = (postAreaTagId ? getFormData(postAreaTagId) : {});
    var formHtml = [];
    var frm = ee("tmpForm");
    if (frm) { frm.parentNode.removeChild(frm); delete frm; }
    var formTag = '<form id="tmpForm" name="tmpForm" action="'+location+option+'" target="_blank" method="post"></form>';
    for (var idx in postData) {
        var obj = postData[idx];
        formHtml.push('<input type="hidden" name="'+obj.name+'" value="'+obj.value+'" />');
    }
    $(document.body).append(formTag);
    $("#tmpForm").append(formHtml);
    document.tmpForm.submit();
}
