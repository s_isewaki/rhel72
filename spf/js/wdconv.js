/* Copyright © medi-system solutions co,.Ltd. All rights reserved. */
/* miyazaki 自家製LIBです。SPFと関係ない機能も多く含んでいます。 */

var pad = function(s, len) { return ((new Array(len + 1).join("0")) + s+"").slice(len*-1); };
var getMonthLastDay = function(y,m) { y=int(y);m=int(m);if(!y||y<1||!m||m<1||12<m) return 0; var a=[31,28,31,30,31,30,31,31,30,31,30,31]; var o=a[m-1]; if(m!=2 && (y%4==0 || y%400==0)) return 29; return o; }

//=======================================================================================
//
// 西暦・和暦変換オブジェクト
//
//=======================================================================================
var WDCONV = {
	getTodayYmd : function() {
		var dt = new Date();
		return dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
	},

	getEto : function(ymd,type) {
		var ary = this.somethingToSeireki(ymd).split("/");
		if (!ary[0]) return "";
		var eary = [
			"子,ね,ねずみ","丑,うし,うし","寅,とら,とら","卯,う,うさぎ","辰,たつ,たつ","巳,み,へび",
			"午,うま,うま","未,ひつじ,ひつじ","申,さる,さる","酉,とり,とり","戌,いぬ,いぬ","亥,い,いのしし"];
		var p = eary[((9*1+1*ary[0]) % 12) - 1].split(",");
		if (!type) return p;
		if (type==1) return p[0];
		if (type==2) return p[1];
		if (type==3) return p[2];
	},

	getElapseDays : function(fr, to) {
		var sFr = this.somethingToSeireki(fr);
		var sTo = this.somethingToSeireki(to);
		if (!sFr || !sTo) return "";
		var aFr = sFr.split("/");
		var aTo = sTo.split("/");
		var yy = aTo[0] - aFr[0];
		var mm = aTo[1] - aFr[1];
		var dd = aTo[2] - aFr[2];

		var dFr = new Date(aFr[0], aFr[1]-1, aFr[2]);
		var dTo = new Date(aTo[0], aTo[1]-1, aTo[2]);
		var nDt = Math.floor((dTo - dFr) / (24 * 60 * 60 * 1000));

		return dt.getFullYear() + "/" + (dt.getMonth()+1) + "/" + dt.getDate();
	},

	getElapseYmd : function(fr, to) {
		var sFr = this.somethingToSeireki(fr);
		var sTo = this.somethingToSeireki(to);
		if (!sFr || !sTo) return "";
		var aFr = (sFr<sTo?sFr:sTo).split("/");
		var aTo = (sFr<sTo?sTo:sFr).split("/");
		var tTo = new Date(aTo[0], aTo[1]-1, aTo[2]);
		var yy=aTo[0]-aFr[0], mm=aTo[1]-aFr[1], dd=aTo[2]-aFr[2];
		if (dd < 0) { mm--; aTo[1]--; }
		if (mm < 0) { yy--; mm += 12; }
		if (aTo[1] < 1) { aTo[0]--; aTo[1] += 12; }
		if (dd<0) {
			var tFr = new Date(aTo[0], aTo[1]-1, aTo[2]);
			var dd = Math.floor((tTo - tFr) / (24 * 60 * 60 * 1000));
		}
		if (!yy && !mm && !dd) return "0日";
		return (yy ? yy+"年":"")+(mm?mm+"ヵ月":"")+(dd?dd+"日":"");
	},

	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 西暦から和暦へ
	//
	// ・元号へ変換できなければカラ返却
	// ・月が取得できなければ元号と年まで返却
	// ・日が取得できなければ年月部分まで返却
	// 例）1900/9/11  => 明治33年09月11日
	// 例）千四年元日 => 長保06年01月01日
	//――――――――――――――――――――――――――――――――――――――――――――――――
	seirekiToWareki : function(ymd, ntk, realm) {
		if (!realm) realm="GYMD";
		if (realm.replace(/G/,"").replace(/Y/,"").replace(/M/,"").replace(/D/,"")!="") return;
		var ary = this.extractFormatYmdPart(ymd);
		var yyyy = ary.y;
		if (!yyyy || yyyy<645 || 2100<yyyy) return ""; // 年が645〜2100でなければ、カラを返す
		var yyyymmdd = pad(yyyy,4)+pad(ary.m,2)+pad(ary.d,2);
		var gengo="";
		var syyyy="";
		for (var idx=this.GENGO_DEFINITION.length-1; idx>=0; idx--){
			var ag= this.GENGO_DEFINITION[idx];
			if (ag[0] <= yyyymmdd) { syyyy=ag[0].substring(0,4); gengo=ag[1]; break; }
		}
		if (!gengo) return "";   // 元号部分が取れなければカラを返す
		yyyy = yyyy - syyyy*1+int(1);
		if (yyyy>99) return "";
		if (ntk==4 || ntk==5) {
			return realm.replace(/G/,gengo).
				replace(/Y/,(ntk==4?yyyy:pad(yyyy, 4))+"年").
				replace(/M/,(ntk==4?ary.m:pad(ary.m,2))+"月").
				replace(/D/,(ntk==4?ary.d:pad(ary.d,2))+"日");
		}
		return realm.replace(/G/,gengo).
			replace(/Y/,this.numToKanji(pad(yyyy, 4),ntk)+"年").
			replace(/M/,(ary.m ? this.numToKanji(pad(ary.m,2),ntk)+"月" : "")).
			replace(/D/,(ary.d ? this.numToKanji(pad(ary.d,2),ntk)+"日" : ""));
	},




	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 和暦から西暦へ
	//
	// ・元号文字あるいは和暦年が取得できなければカラ返却（元号は必ず先頭であること）
	// ・月が取得できなければ年のみ返却
	// ・日が取得できなければ年月部分まで返却
	// 例）大正元年壱月廿五 ⇒ 1912/01/25
	// 例）平成１０年睦月末 ⇒ 1998/01/31
	// 例）平成 三年 20日　８月 ⇒ 1991/08/20
	//――――――――――――――――――――――――――――――――――――――――――――――――
	warekiToSeireki : function(gymd){
		return this.somethingToSeireki(gymd, 1);
	},
	somethingToSeireki : function(gymd, isWareki) {
		gymd=trim(gymd);
		var ymd = 0;
		var gannen = "";
		var tc = gymd.substring(0,1);
		if (tc=="M" || tc=="Ｍ") { gymd = "明治"+gymd.substring(1); isWareki=1; }
		if (tc=="T" || tc=="Ｔ") { gymd = "大正"+gymd.substring(1); isWareki=1; }
		if (tc=="S" || tc=="Ｓ") { gymd = "昭和"+gymd.substring(1); isWareki=1; }
		if (tc=="H" || tc=="Ｈ") { gymd = "平成"+gymd.substring(1); isWareki=1; }

		for (var idx in this.GENGO_DEFINITION){
			if (idx==0) continue;
			var ag = this.GENGO_DEFINITION[idx];
			if (gymd.substring(0,ag[1].length)!=ag[1]) continue;
			isWareki=1;
			gannen = ag[0].substring(0,4);
			ymd = trim(gymd.substring(2));
			break;
		}
		if (!ymd && !isWareki) ymd = gymd;
		if (!ymd && isWareki) return "";
		if (!gannen) gannen = "1";
		var ary = this.extractFormatYmdPart(ymd);
		return (ary.y?pad(gannen-1+int(ary.y),4):"")+(ary.m?"/"+pad(ary.m,2):"")+(ary.d?"/"+pad(ary.d,2):"");
	},





	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 漢数字/ひらがななどでの日本語の読みを「整数」へ変換する。変換できるところまで行う。
	//
	// 例）いっちょうごせんいっぴゃくじゅうさんおくさんぜんにじゅうまんはっぴゃくろくじゅう ⇒ 1511330200860
	// 例）壱萬弐千五十参 ⇒ 12053
	//――――――――――――――――――――――――――――――――――――――――――――――――
	japaneseToNum : function(s) {
		s = this.toHira(trim(s));
		s = s.replace(/[ 　\t\r\n\f]/g, "");         // 改行やスペースを全部取り除く
		s = this.toKansuuji(s);
		return this.kansuujiToNum(s);
	},




	csum : function (ary) { return (ary[3] ? ary[3]*1000 : 0) + (ary[2] ? ary[2]*100 : 0) + (ary[1] ? ary[1]*10 : 0); },
	kansuujiToNum : function(s) {
		var bigs = 0; // 兆/億/万系の計算済みフラグ
		var smls = 0; // 千/百/十の計算済みフラグ
		var a_bigs = []; // 兆/億/万系の計算結果キャッシュ
		var a_smls = [,,,]; // 千/百/十の計算結果キャッシュ
		var cache = "";
		var jpl = ["万","億","兆","京","垓","穣","溝","澗","正","載","極","恒河沙","阿僧祇","那由他","不可思議","無量大数"];
		var idx = -1;
		for (;;) {
			idx++;
			if (idx>=s.length) break;

			var pos = -1;
			var c = s[idx];
			for (var jn in jpl) {
				var jps=jpl[jn];
				var jl=jps.length;
				if (jpl>s.length-idx) break;
				if (s.substring(idx, idx+jpl)==jps) { pos=jn; idx+=jpl-1; break; }
			}
			if      (c=="〇" && cache!="") cache += c;
			else if (pos>0) { if (bigs && bigs<=pos) return ""; bigs=pos; a_bigs[pos]=this.csum(a_smls)*1+int(cache); cache=""; a_smls=[,,,]; smls=0; }
			else if (c=="千") {
				if (smls==1 || smls==2 || smls==3) return "";
				if (bigs && cache.length>1) return "";
				smls=3; a_smls[3]=(cache?cache:1); cache="";
			}
			else if (c=="百") {
				if (smls==1 || smls==2) return "";
				if (bigs && cache.length>2) return "";
				if (smls==3 && cache.length>1) return "";
				smls=2; a_smls[2]=(cache?cache:1); cache="";
			}
			else if (c=="十") {
				if (smls == 1) return "";
				if (bigs && cache.length>3) return "";
				if (smls==3 && cache.length>2) return "";
				if (smls==2 && cache.length>1) return "";
				smls=1; a_smls[1]=(cache?cache:1); cache="";
			}
			else {
				var n = this.TO_SUUJI_LIST[c];
				if (!n) break;
				if (bigs && cache.length>4) return "";
				if (smls==3 && cache.length>3) return "";
				if (smls==2 && cache.length>2) return "";
				if (smls==1 && cache.length>1) return "";
				cache += n;
			}
		}
		var nums=[];
		for (var jn=jpl.length; jn>0; jn--) {
			if (!a_bigs[jn]) continue;
			nums.push(pad(a_bigs[jn-1]),4);
		}
		var num = nums.join("")+pad(this.csum(a_smls)*1+int(cache),4);
		for (;;) {
			if (!num.length || num[0]!=="0") break;
			num = num.substring(1);
		}
		return num;
	},


	// 1: 〇三五〇    〇一〇二
	// 2:   三五〇      一〇二
	// 3: 三百五十        百二
	numToKanji : function(num, ntk){
		if (!ntk) return num;
		var out=[];
		var nz=(ntk==1?1:0);
		for (var idx=0; idx<num.length; idx++) {
				var nn=this.TO_KANSUUJI_LIST[num[idx]];
				if (!nz && nn=="〇") continue;
				nz=1;
				if (ntk==1 || ntk==2) {
					out.push(nn);
					continue;
				}
				if (ntk==3) {
					if (nn!="〇" && (nn!="一" || idx==num.length-1)) out.push(nn);
					if (idx==num.length-2) out.push("十");
					if (idx==num.length-3) out.push("百");
					if (idx==num.length-4) out.push("千");
				}
		}
		return out.join("");
	},

	extractFormatYmdPart : function(ymd){
		if (!ymd) return {"y":"","m":"","d":""};
		ymd = trim(ymd).replace(/[・／\. ]/g, "\/").replace(/せいれき/g, "").replace(/西暦/g, "").replace(/A.D./g, "");
		ymd = this.toHira(ymd);  // かなを全角ひらがなに、数値は半角に、全角スペースは半角に

		var ary = this.extractYmdPart(ymd);
		// 年を求める。
		var yy = this.kansuujiToNum(ary.y);
		if (yy>2999) yy = "";

		// 月を求める。
		var mm = 0;
		if (yy) {
			if (ary.m=="末") mm = 12;
			else mm = this.kansuujiToNum(ary.m);
			if (mm<1 || 12<mm) mm = 0; // 月は1〜12でなければ、無効とする
		}

		// 日を求める。
		var dd = 0;
		if (mm) {
			var maxdd = getMonthLastDay(yy,mm);
			if (ary.d=="末") dd = maxdd;
			else dd = this.kansuujiToNum(ary.d);
			if (dd<1 || maxdd<dd) dd = 0; // 日は1〜月末日でなければ、無効とする。月が判明しないと、同様に無効とする。
		}
		return {"y":yy, "m":mm, "d":dd};
	},

	extractYmdPart : function(ymd){
		if (ymd.match(/^[0-9]{8}$/)) ymd = ymd.substring(0,4)+"/"+ymd.substring(4,6)+"/"+ymd.substring(6);
		if (ymd.match(/^[3-9][0-9]{6}$/)) ymd = ymd.substring(0,3)+"/"+ymd.substring(3,5)+"/"+ymd.substring(5);
		ymd = trim(ymd).replace(/[／．・\/\.\- 　\t\r\n\f]/g, "/"); // トリムのち、左記はすべてセパレータとみなす
		ymd = this.toKansuuji(ymd);
		ymd = ymd.replace(/\/+/g, '/');   // スラは一個にする
		ymd = ymd.replace(/年\/+/g, '年'); // 年表現を優先
		ymd = ymd.replace(/月\/+/g, '月'); // 月表現を優先
		ymd = ymd.replace(/日\/+/g, '日'); // 日表現を優先
		var cache="", yy="", mm="", dd="", ok={}, slashes=[];
		for (idx=0; idx<ymd.length; idx++){
			var c = ymd.substring(idx, idx+1);
			if      (c=="年") { if (ok["y"]) return []; yy = cache; cache = ""; ok["y"] = 1; }
			else if (c=="月") { if (ok["m"]) return []; mm = cache; cache = ""; ok["m"] = 1; }
			else if (c=="日") { if (ok["d"]) return []; dd = cache; cache = ""; ok["d"] = 1; }
			else if (c=="/")  { slashes.push(cache); cache = ""; }
			else cache += c;
		}
		if (cache) slashes.push(cache);
		for ( var idx=0; idx<slashes.length; idx++) {
			var s = slashes[idx];
			if (!ok["y"]) { yy=s; ok["y"]=1; continue; }
			if (!ok["m"]) { mm=s; ok["m"]=1; continue; }
			if (!ok["d"]) { dd=s; ok["d"]=1; continue; }
			return {};
		}
//		if (yy.length>4) yy="";
		return {"y":yy, "m":mm, "d":dd};
	},


	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 全半角カナはひらがなに、数値、全角スペースは半角に
	//――――――――――――――――――――――――――――――――――――――――――――――――
	TO_HIRAGANA_LIST : [
		{"from":"｢｣･｡､ｧｨｩｪｫｬｭｮｯｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｰ",
		"to":"「」・。、ぁぃぅぇぉゃゅょっあいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをんー"},
		{"from":"ｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾊﾋﾌﾍﾎ", "to":"がぎぐげござじずぜぞだぢづでどばびぶべぼ"},
		{"from":"ﾊﾋﾌﾍﾎ","to":"ぱぴぷぺぽ"}
	],
	toHira : function(str) {
		var out=[];
		var idx=str.length;
		while (idx>0) {
			idx--;
			var ch = str.charCodeAt(idx);
			if (ch==0x30F0) { out.unshift("い"); continue; } // ヰ⇒い
			if (ch==0x30F1) { out.unshift("え"); continue; } // ヱ⇒え
			if (0x30A1 <= ch && ch <= 0x30F6) { out.unshift(String.fromCharCode(ch-0x0060)); continue; } // カタ⇒ひら
			if (0xFF10 <= ch && ch <= 0xFF19) { out.unshift(String.fromCharCode(ch-0xFEE0)); continue; } // 半角数値へ
			if (ch==0x3000) { out.unshift(" "); continue; } // スペース
			if (0xFF66 <= ch && ch <= 0xFF9F) { // 半角
				var dy=0; if (ch==0xFF9E) dy=1; if(ch==0xFF9F) dy=2; // 半角濁点
				if (dy) { if (idx==0) break; idx--; ch=str.charCodeAt(idx); }
				var pos = str[idx].indexOf(this.TO_HIRAGANA_LIST[dy].from);
				if (pos>=0) { out.unshift(this.TO_HIRAGANA_LIST[dy].to); }
				continue;
			}
			out.unshift(String.fromCharCode(ch));
		}
		return out.join("");
	},

	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 日付用 ひらがな・旧式漢数字統一表   「〇一二三四五六七八九十百千万億兆」および「年月日」に置き換える。
	//――――――――――――――――――――――――――――――――――――――――――――――――
	toKansuuji : function(str) {
		var out=[];
		var idx=-1;
		for(;;) {
			idx++;
			if (idx>=str.length) break;
			var b = 0;
			for (var nn=0; nn<this.KANJI_NUM_DATE_INTEGRATE_LIST.length; nn++) {
				var ss=this.KANJI_NUM_DATE_INTEGRATE_LIST[nn];
				var slen=ss[0].length;
				if (slen>str.length-idx) continue;
				if (str.substring(idx, idx+slen)==ss[0]) { b=1; out.push(ss[1]); idx+=slen-1; break; }
			}
			if (!b) out.push(str[idx]);
		}
		return out.join("");
	},
	KANJI_NUM_DATE_INTEGRATE_LIST : [
		["元年","一年"], ["元旦","一月一日"], ["元日","一月一日"],
		["節分","二月三日"], ["大晦日","十二月三十一日"],
		["睦月","一月"],   ["如月","二月"],   ["弥生","三月"],   ["卯月","四月"],   ["皐月","五月"],   ["水無月","六月"],
		["文月","七月"],   ["葉月","八月"],   ["長月","九月"],   ["神無月","十月"], ["霜月","十一月"], ["師走","十二月"],

		["零","〇"],
		["壱","一"],   ["弌","一"],   ["壹","一"],   ["弐","二"],   ["貳","二"],   ["参","三"],
		["肆","四"],   ["伍","五"],   ["陸","六"],   ["質","七"],   ["捌","八"],   ["玖","九"],
		["拾","十"],   ["佰","百"],   ["陌","百"],
		["仟","千"],   ["阡","千"],   ["萬","万"],
		["廿","二十"], ["卅","三十"], ["丗","三十"], ["初日","一日"],

		["ちょう","兆"],   ["おく","億"],   ["まん","万"],   ["せん","千"],   ["ぜん","千"],
		["ひゃく","百"],   ["ぴゃく","百"],   ["びゃく","百"],   ["じゅう","十"],   ["きゅう","九"],
		["ついたち","一日"], ["はつか","二十日"], ["とんで","〇"],

		["ここの","九"], ["とお","十"],   ["とう","十"],   /*"じゅっ","十"],*/
		["なな","七"],   ["さん","三"],   ["よん","四"],   ["よっ","四"],   ["ろく","六"],   ["ろっ","六"],
		["ぜろ","〇"],   ["れい","〇"],   ["いち","一"],   ["ひと","一"],   ["いっ","一"],
		["みっ","三"],   ["はっ","八"],   ["いつ","五"],   ["よう","八"],   ["よお","八"],   ["はち","八"],
		["むい","六"],   ["なの","七"],   ["はっ","八"],   ["しち","七"],   ["ひち","七"],
		["にい","二"],   ["ふつ","二"],   ["ふた","二"],
		["ねん","年"],   ["がつ","月"],   ["にち","日"],

		["ご","五"],   ["に","二"],   ["よ","四"],   ["く","九"],   ["し","四"],
		["か","日"],   ["ぴ","日"]
	],

	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 数値変換表
	//――――――――――――――――――――――――――――――――――――――――――――――――
	TO_KANSUUJI_LIST : {
		"1" :"一",   "2" :"二",   "3" :"三",   "4" :"四",   "5" :"五",   "6" :"六",   "7" :"七",   "8" :"八",   "9" :"九",   "0" :"〇",
		"一":"一",   "二":"二",   "三":"三",   "四":"四",   "五":"五",   "六":"六",   "七":"七",   "八":"八",   "九":"九",   "〇":"〇",
		"１":"一",   "２":"二",   "３":"三",   "４":"四",   "５":"五",   "６":"六",   "７":"七",   "８":"八",   "９":"九",   "０":"〇"
	},
	TO_SUUJI_LIST : {
		"1" :"1",   "2" :"2",   "3" :"3",   "4" :"4",   "5" :"5",   "6" :"6",   "7" :"7",   "8" :"8",   "9" :"9",   "0" :"0",
		"一":"1",   "二":"2",   "三":"3",   "四":"4",   "五":"5",   "六":"6",   "七":"7",   "八":"8",   "九":"9",   "〇":"0",
		"１":"1",   "２":"2",   "３":"3",   "４":"4",   "５":"5",   "６":"6",   "７":"7",   "８":"8",   "９":"9",   "０":"0"
	},

	//――――――――――――――――――――――――――――――――――――――――――――――――
	// 元号デファイン
	//――――――――――――――――――――――――――――――――――――――――――――――――
	GENGO_DEFINITION : [
		["00000000",""],
		["06450619","大化"],        ["06500215","白雉"],        ["06860720","朱鳥"],        ["07010321","大宝"],        ["07040510","慶雲"],
		["07080111","和銅"],        ["07150902","霊亀"],        ["07171117","養老"],        ["07240204","神亀"],        ["07290805","天平"],
		["07490414","天平感宝"],    ["07490702","天平勝宝"],    ["07570818","天平宝字"],    ["07650107","天平神護"],    ["07670816","神護景雲"],
		["07701001","宝亀"],        ["07810101","天応"],        ["07820819","延暦"],        ["08060518","大同"],        ["08100917","弘仁"],
		["08240103","天長"],        ["08340103","承和"],        ["08480613","嘉祥"],        ["08510428","仁寿"],        ["08541130","斉衡"],
		["08570221","天安"],        ["08590416","貞観"],        ["08770416","元慶"],        ["08850221","仁和"],        ["08890427","寛平"],
		["08980426","昌泰"],        ["09010715","延喜"],        ["09230411","延長"],        ["09310426","承平"],        ["09380522","天慶"],
		["09470422","天暦"],        ["09471027","天徳"],        ["09610216","応和"],        ["09640710","康保"],        ["09680813","安和"],
		["09700325","天禄"],        ["09731220","天延"],        ["09760713","貞元"],        ["09781129","天元"],        ["09830415","永観"],
		["09850427","寛和"],        ["09870405","永延"],        ["09890808","永祚"],        ["09901107","正暦"],        ["09950222","長徳"],
		["09990113","長保"],        ["10040720","寛弘"],        ["10121225","長和"],        ["10170423","寛仁"],        ["10210202","治安"],
		["10240713","万寿"],        ["10280725","長元"],        ["10370421","長暦"],        ["10401110","長久"],        ["10441124","寛徳"],
		["10460414","永承"],        ["10530111","天喜"],        ["10580829","康平"],        ["10650802","治暦"],        ["10690413","延久"],
		["10740823","承保"],        ["10771117","承暦"],        ["10810210","永保"],        ["10840207","応徳"],        ["10870407","寛治"],
		["10941215","嘉保"],        ["10961216","永長"],        ["10971121","承徳"],        ["10990828","康和"],        ["11040210","長治"],
		["11060409","嘉承"],        ["11080803","天仁"],        ["11100713","天永"],        ["11130713","永久"],        ["11180403","元永"],
		["11200410","保安"],        ["11240403","天治"],        ["11260122","大治"],        ["11310129","天承"],        ["11320811","長承"],
		["11350427","保延"],        ["11410710","永治"],        ["11420428","康冶"],        ["11440223","天養"],        ["11450722","久安"],
		["11510126","仁平"],        ["11541028","久寿"],        ["11560427","保元"],        ["11590420","平治"],        ["11600110","永暦"],
		["11610904","応保"],        ["11630329","長寛"],        ["11650605","永万"],        ["11660827","仁安"],        ["11690408","嘉応"],
		["11710421","承安"],        ["11750728","安元"],        ["11770804","治承"],        ["11810714","養和"],        ["11820527","寿永"],
		["11840416","元暦"],        ["11850814","文治"],        ["11900411","建久"],        ["11990427","正治"],        ["12010213","建仁"],
		["12040220","元久"],        ["12060427","建永"],        ["12071025","承元"],        ["12110309","建暦"],        ["12131206","建保"],
		["12190412","承久"],        ["12220413","貞応"],        ["12241120","元仁"],        ["12250420","嘉禄"],        ["12271210","安貞"],
		["12290305","寛喜"],        ["12320402","貞永"],        ["12330415","天福"],        ["12341105","文暦"],        ["12350919","嘉禎"],
		["12381123","暦仁"],        ["12390207","延応"],        ["12400716","仁治"],        ["12430226","寛元"],        ["12470228","宝治"],
		["12490318","建長"],        ["12561005","康元"],        ["12570314","正嘉"],        ["12590326","正元"],        ["12600413","文応"],
		["12610220","弘長"],        ["12640228","文永"],        ["12750425","建治"],        ["12780229","弘安"],        ["12880428","正応"],
		["12930805","永仁"],        ["12990425","正安"],        ["13021121","乾元"],        ["13030805","嘉元"],        ["13061214","徳治"],
		["13081009","延慶"],        ["13110428","応長"],        ["13120320","正和"],        ["13170203","文保"],        ["13190428","元応"],
		["13210223","元亨"],        ["13241209","正中"],        ["13260426","嘉暦"],        ["13290829","元徳"],        ["13310809","元弘"],/*南朝*/
		["13320428","正慶"],        ["13340129","建武"],        ["13360229","延元"],/*南朝*/["13380828","暦応"],        ["13400428","興国"],/*南朝*/
		["13420427","康永"],        ["13451021","貞和"],        ["13461208","正平"],/*南朝*/["13500227","観応"],        ["13520927","文和"],
		["13560328","延文"],        ["13610329","康安"],        ["13620923","貞治"],        ["13680218","応安"],        ["13700724","建徳"],/*南朝*/
		["13720407","文中"],/*南朝*/["13750227","永和"],        ["13750527","天授"],/*南朝*/["13790322","康暦"],        ["13810210","弘和"],/*南朝*/
		["13810224","永徳"],        ["13840227","至徳"],        ["13840428","元中"],        ["13870823","嘉慶"],        ["13890209","康応"],
		["13900326","明徳"],        ["13940705","応永"],        ["14280427","正長"],        ["14290905","永享"],        ["14410217","嘉吉"],
		["14440205","文安"],        ["14490728","宝徳"],        ["14520725","享徳"],        ["14550725","康正"],        ["14570928","長禄"],
		["14601221","寛正"],        ["14660228","文正"],        ["14670305","応仁"],        ["14690428","文明"],        ["14870720","長享"],
		["14890821","延徳"],        ["14920719","明応"],        ["15010229","文亀"],        ["15040230","永正"],        ["15210823","大栄"],
		["15280820","享禄"],        ["15320729","天文"],        ["15551023","弘治"],        ["15580228","永禄"],        ["15700423","元亀"],
		["15730728","天正"],        ["15921208","文禄"],        ["15961027","慶長"],        ["16150713","元和"],        ["16240230","寛永"],
		["16441216","正保"],        ["16480215","慶安"],        ["16520918","承応"],        ["16550413","明暦"],        ["16580723","万治"],
		["16610425","寛文"],        ["16730921","延宝"],        ["16810929","天和"],        ["16840221","貞享"],        ["16880930","元禄"],
		["17040313","宝永"],        ["17110425","正徳"],        ["17160622","享保"],        ["17360428","元文"],        ["17410227","寛保"],
		["17440221","延享"],        ["17480712","寛延"],        ["17511027","宝暦"],        ["17640602","明和"],        ["17721116","安永"],
		["17810402","天明"],        ["17890125","寛政"],        ["18010205","享和"],        ["18040211","文化"],        ["18180422","文政"],
		["18301210","天保"],        ["18441202","弘化"],        ["18480228","嘉永"],        ["18541127","安政"],        ["18600318","万延"],
		["18610219","文久"],        ["18640220","元治"],        ["18650408","慶応"],        ["18680908","明治"],        ["19120730","大正"],
		["19261225","昭和"],        ["19890108","平成"]
	]
};
