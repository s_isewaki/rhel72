<?

//****************************************************************************************************************
// 施設基準管理画面
//****************************************************************************************************************
function html_mst_sisetu() {

    $do_export = $_REQUEST["do_export"];

    //==========================================================================
    // 一覧
    //==========================================================================
    $sql =
    " select ss.*, ssel.emp_id, ssel.emp_type".
    ",em.emp_lt_nm, em.emp_ft_nm, em.emp_personal_id".
    ",cm.class_nm, am.atrb_nm, dm.dept_nm, cr.room_nm".
    ",em.emp_class, em.emp_attribute, em.emp_dept, em.emp_room, ssel.last_class_id, ssel.last_atrb_id, ssel.last_dept_id, ssel.last_room_id".
    " from spfm_sisetu ss".
    " left outer join spfm_sisetu_emp_list ssel on (ssel.sisetu_id = ss.sisetu_id)".
    " left outer join empmst em on (em.emp_id = ssel.emp_id)".
    " left outer join classmst cm on (cm.class_id = em.emp_class)".
    " left outer join atrbmst am on (am.atrb_id = em.emp_attribute)".
    " left outer join deptmst dm on (dm.dept_id = em.emp_dept)".
    " left outer join classroom cr on (cr.room_id = em.emp_room)".
    " order by ss.sisetu_cd, ssel.emp_type, em.emp_lt_nm, em.emp_ft_nm";
    $_sisetuRows = c2dbGetRows($sql);

    //――――――――――――――――――――――――――――――――――――――――――――
    // CSV出力
    //――――――――――――――――――――――――――――――――――――――――――――
    if ($do_export) {
        c2OutputCsvHeader("spf_sisetu_" . date("Ymd_His") . ".csv");
        echo pack('CCC', 239, 187, 191); // UTF8 BOM出力
        $head = '"施設基準管理ID","施設基準コード","施設基準名","施設基準条件等","役割","職員ID","職員名","部署"';
        echo mb_convert_encoding($head, "utf8", "eucJP-win")."\r\n";
        foreach ($_sisetuRows as $row) {
            $emp_type_jp = "書類発行資格者";
            if ($row["emp_type"]=="SENJU") $emp_type_jp = "専従者";
            if ($row["emp_type"]=="SENNIN") $emp_type_jp = "専任者";
            $line = array();
            $line[]= '"'.str_replace('"', '""', $row["sisetu_id"]).'"';
            $line[]= '"'.str_replace('"', '""', $row["sisetu_cd"]).'"';
            $line[]= '"'.str_replace('"', '""', $row["sisetu_nm"]).'"';
            $line[]= '"'.str_replace('"', '""', $row["kijun_memo"]).'"';
            $line[]= '"'.str_replace('"', '""', $emp_type_jp).'"';
            $line[]= '"'.str_replace('"', '""', $row["emp_personal_id"]).'"';
            $line[]= '"'.str_replace('"', '""', $row["emp_lt_nm"]." ".$row["emp_ft_nm"]).'"';
            $line[]= '"'.str_replace('"', '""', jointSyozokuDisp($row["class_nm"], $row["atrb_nm"], $row["dept_nm"], $row["room_nm"])).'"';
            echo mb_convert_encoding(implode(",", $line), "utf8", "eucJP-win")."\r\n";
        }
        die;
    }

    $sisetuRows = array();
    $sisetuEmpRows = array();
    $empSenjuDup = array();
    $empSenjuList = array();
    $empHissuList = array();
    $exist_idou = 0;
    $exist_blank = 0;
    foreach ($_sisetuRows as $row) {
        $sisetu_id = $row["sisetu_id"];
        $emp_type = $row["emp_type"];
        //----------------------------------------
        // 専従の数を数える。２つ以上あれば基本的にはエラー
        //----------------------------------------
        if ($emp_type=="SENJU") {
            $emp_id = $row["emp_id"];
            if (in_array($emp_id, $empSenjuList)) $empSenjuDup[$emp_id] = (int)$empSenjuDup[$emp_id] + 1;
            $empSenjuList[]= $emp_id;
        }
        //----------------------------------------
        // 異動ありならフラグを立てる
        //----------------------------------------
        $idou = 0;
        if ($row["emp_class"] != $row["last_class_id"]) $idou = 1;
        else if ($row["emp_attribute"] != $row["last_atrb_id"]) $idou = 1;
        else if ($row["emp_dept"] != $row["last_dept_id"]) $idou = 1;
        else if ($row["emp_room"] != $row["last_room_id"]) $idou = 1;
        if ($idou) { $row["idou"] = 1; $exist_idou = 1; }


        if (!isset($sisetuRows[$sisetu_id])) {
            $sisetuRows[$sisetu_id] = $row;
            $empHissuList[$sisetu_id."__SENJU"]  = (int)$row["senju_hissu_ninzu"];
            $empHissuList[$sisetu_id."__SENNIN"] = (int)$row["sennin_hissu_ninzu"];
        }
        $sisetuEmpRows[$sisetu_id][$emp_type][] = $row;

        //----------------------------------------
        // 人数が足りてないか、登録されていない（その１）
        //----------------------------------------
        if ($emp_type=="SENJU" || $emp_type=="SENNIN") {
            $seid = $sisetu_id."__".$emp_type;
            $empHissuList[$seid]--;
        }
    }
    //----------------------------------------
    // 人数が足りてないか、登録されていない（その２）
    //----------------------------------------
    $keys = array_keys($empHissuList);
    foreach ($keys as $seid) {
        if ($empHissuList[$seid] <= 0) unset($empHissuList[$seid]);
    }
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
        <table cellspacing="0" cellpadding="0" class="section_title"><tr>
            <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">L</th><? } ?>
            <th class="section_title_title"><nobr>施設基準管理</nobr></th>
            <th><button type="button" onclick="popupSisetuEditor()">施設基準新規登録</button></th>
            <th class="button"><nobr><button type="button" onclick="popupWindowByPost('common.php?ajax_action=html_mst_sisetu&do_export=1', '', 'div_section_header')">CSV出力</button></nobr></th>
        </tr></table>

        <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
        <? // 検索条件                                                                                ?>
        <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
        <div style="padding-bottom:4px">

        <? if ($exist_idou) { ?><div style="padding:0 2px" class="red">◆本機能での登録後に部署変更のあった職員が存在します。</div><? } ?>
        <? if (count($empSenjuDup)) { ?><div style="padding:0 2px" class="red">◆複数の施設基準に専従者登録されている職員が存在します。</div><? } ?>
        <? if (count($empHissuList)) { ?><div style="padding:0 2px" class="red">◆必要人数未満の施設基準があります。</div><? } ?>

        </div>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        function popupSisetuEditor(sisetu_id) {
            if (!sisetu_id) sisetu_id = "";
            popupAdmDlg('施設基準登録', 'dialog_mst_sisetu', '&sisetu_id='+sisetu_id);
        }
        function saveSisetuEditor() {
            closeDialog("sisetu_editor_dialog");
            import_field_csv_ary[currentCol-1] = tfid;
            ee("import_field_csv").value = import_field_csv_ary.join(",");
            showAdminSect('mst_import', '', 'div_section_header');
        }
        var currentATag = "";
        var currentParam = "";
        function showEmpSelector(aTag) {
            currentATag = aTag;
            var ids = currentATag.id.split("__");
            currentParam = "&sisetu_id="+ids[1]+"&emp_type="+ids[2];
            popupAdmDlg('職員選択','dlg_emp_selector', "&callback_func=mstSisetuEmpSelectorCallback", currentATag.id);
        }
        function mstSisetuEmpSelectorCallback(empList) {
            if (!confirm("保存します。よろしいですか？")) return;
            var empIds = [];
            for (var idx=0; idx<empList.length; idx++) {
                empIds.push(empList[idx].emp_id);
            }
            ee("emp_ids").value = empIds.join("\t");
            tryAdminReg('sisetu_emp_list_forwarder', 'reg_mst_sisetu_emp_list', currentParam, 1);
        }
        function delSisetu(sisetu_id) {
            if (!confirm("削除します。よろしいですか？")) return;
            tryAdminReg('', 'reg_mst_sisetu_del', "&sisetu_id="+sisetu_id, 1);
        }
    </textarea>

    <div>
    <table cellspacing="0" cellpadding="0" class="nowrap list_table">
    <tr class="pale">
        <th>施設基準コード</th>
        <th>施設基準名</th>
        <th>施設基準条件等</th>
        <th>専従者</th>
        <th>専任者</th>
        <th>書類発行資格者</th>
        <th></th>
    </th>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域：一覧データ                                                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <tbody id="list_tbody">
    <? $emp_type_code_list = array("SENJU","SENNIN","SYORUI_HAKKOU"); ?>
    <? foreach ($sisetuRows as $sisetu_id => $sisetuRow) { ?>
    <?     $sisetu_nm = $sisetuRow["sisetu_nm"]; ?>
    <?     if ($sisetu_nm=="") $sisetu_nm = "(施設基準名未設定)"; ?>
    <tr>
        <td><?=hh($sisetuRow["sisetu_cd"])?></td>
        <td><a class="block" href="javascript:void(0)" onclick="popupSisetuEditor('<?=$sisetuRow["sisetu_id"]?>')"><?=hh($sisetu_nm)?></a></td>
        <td><?=hh($sisetuRow["kijun_memo"])?></td>
        <?
            foreach ($emp_type_code_list as $emp_type) {
                echo '<td>';
                echo '<a id="td__'.$sisetuRow["sisetu_id"].'__'.$emp_type.'" class="block" href="javascript:void(0)" onclick="showEmpSelector(this)">';
                echo getEmpHtmlBox($sisetuRow, $sisetuEmpRows[$sisetuRow["sisetu_id"]][$emp_type], $emp_type, $empSenjuDup);
                echo '</a></td>';
            }
        ?>
        <td><button type="button" onclick="delSisetu(<?=$sisetuRow["sisetu_id"]?>)">削除</button></td>
    </tr>
    <? } ?>
    </tbody>
    </table>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // 施設基準登録ダイアログ                                                                      ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="sisetu_editor_container" style="display:none">
        <div id="sisetu_editor"></div>
    </div>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // 職員登録のためのフォワーダ                                                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div style="display:none">
        <form id="sisetu_emp_list_forwarder">
            <input type="hidden" name="emp_ids" id="emp_ids" />
        </form>
    </div>
<?
}


function getEmpHtmlBox(&$sisetuRow, &$empRows, $emp_type, $empSenjuDup) {
    $ninzu = (is_array($empRows) ? count($empRows) : 0);
    $hissu_msg = "";
    $navi = ' navi="現在の指定人数"';
    if ($emp_type=="SENJU" || $emp_type=="SENNIN") {
        $hissu = (int)$sisetuRow[strtolower($emp_type)."_hissu_ninzu"];
        $hissu_msg = "／".$hissu."名";
        if ($ninzu < $hissu) $hissu_msg .= '&nbsp;<span style="color:#ffffff; background-color:#dd4444; line-height:1.5; padding:0 2px">未満</span>';
        $navi = ' navi="現在の指定人数／必要人数"';
    }

    echo '<div'.$navi.' class="center">'.$ninzu."名".$hissu_msg.'</div>';
    $emp_ids = array();
    if (is_array($empRows)) {
        foreach ($empRows as $row) {
            $dup = (int)$empSenjuDup[$row["emp_id"]];
            $emp_ids[]= $row["emp_id"];
            $emp_nm = hh($row["emp_lt_nm"]." ".$row["emp_ft_nm"]);
            echo '<div navititle="'.$emp_nm.'"';
            if ($dup > 0 || $row["idou"]) echo ' class="red"';
            echo ' navi="'.hh(jointSyozokuDisp($row["class_nm"], $row["atrb_nm"], $row["dept_nm"], $row["room_nm"]));

            if ($row["idou"]) echo '<br>以前の部署：'.getSyozokuDisp($row["last_class_id"], $row["last_atrb_id"], $row["last_dept_id"], $row["last_room_id"]);

            echo '"';
            echo ' onmouseover="style.textDecoration=\'underline\'" onmouseout="style.textDecoration=\'none\'"';
            echo '>'.$emp_nm;
            if ($dup > 0) echo '&nbsp;<span style="color:#ffffff; background-color:#dd4444; line-height:1.5; padding:0 2px">専従'.($dup+1).'重複</span>';
            if ($row["idou"]) echo '&nbsp;<span style="color:#ffffff; background-color:#dd4444; line-height:1.5; padding:0 2px">部署変更</span>';
            echo '</div>';
        }
    }
    echo '<input type="hidden" name="emp_ids" value="'.implode("\t",$emp_ids).'" />';
}


//****************************************************************************************************************
// 施設基準情報ダイアログ表示
//****************************************************************************************************************
function dialog_mst_sisetu() {
    $sisetu_id = (int)$_REQUEST["sisetu_id"];
    $row = c2dbGetTopRow("select * from spfm_sisetu where sisetu_id = ".$sisetu_id);
?>
        <div style="padding:5px">
            <input type="hidden" name="sisetu_id" value="<?=hh($row["sisetu_id"])?>" />
            <table cellspacing="0" cellpadding="0" class="dialog_input_table">
            <tr>
                <th>施設基準コード</th>
                <td><input type="text" class="text w100" maxlength="30" name="sisetu_cd" value="<?=hh($row["sisetu_cd"])?>" /></td>
            </tr>
            <tr>
                <th>施設基準名</th>
                <td><input type="text" class="text w300" maxlength="100" name="sisetu_nm" value="<?=hh($row["sisetu_nm"])?>" /></td>
            </tr>
            <tr>
                <th><nobr>施設基準条件等</nobr></th>
                <td><textarea class="w300" cols="60" rows="4" name="kijun_memo"><?=hh($row["kijun_memo"])?></textarea></td>
            </tr>
            <tr>
                <th>専従必要人数</th>
                <td><input type="text" class="text w50 imeoff" maxlength="3" name="senju_hissu_ninzu" value="<?=(int)$row["senju_hissu_ninzu"]?>" /></td>
            </tr>
            <tr>
                <th>専任必要人数</th>
                <td><input type="text" class="text w50 imeoff" maxlength="3" name="sennin_hissu_ninzu" value="<?=(int)$row["sennin_hissu_ninzu"]?>" /></td>
            </tr>
            </table>
        </div>
        <div class="center"><button type-"button" onclick="tryAdminReg('dialogInnerGeneralDialog', 'reg_mst_sisetu')">保存</button></div>
<?
    die;
}
//****************************************************************************************************************
// 施設基準情報保存
//****************************************************************************************************************
function reg_mst_sisetu() {
    $sisetu_id = (int)$_REQUEST["sisetu_id"];
    if (!$sisetu_id) {
        $sisetu_id = c2dbGetOne("select nextval('spfm_sisetu_id_seq')");
        $sql =
        " insert into spfm_sisetu ( sisetu_id, sisetu_cd, sisetu_nm, kijun_memo, senju_hissu_ninzu, sennin_hissu_ninzu ) values (".
        " ".$sisetu_id.
        ",".c2dbStr($_REQUEST["sisetu_cd"]).
        ",".c2dbStr($_REQUEST["sisetu_nm"]).
        ",".c2dbStr($_REQUEST["kijun_memo"]).
        ",".(int)$_REQUEST["senju_hissu_ninzu"].
        ",".(int)$_REQUEST["sennin_hissu_ninzu"].
        ")";
        c2dbExec($sql);
    }
    else {
        $sql =
        " update spfm_sisetu set".
        " sisetu_cd = ".c2dbStr($_REQUEST["sisetu_cd"]).
        ",sisetu_nm = ".c2dbStr($_REQUEST["sisetu_nm"]).
        ",kijun_memo = ".c2dbStr($_REQUEST["kijun_memo"]).
        ",senju_hissu_ninzu = ".(int)$_REQUEST["senju_hissu_ninzu"].
        ",sennin_hissu_ninzu = ".(int)$_REQUEST["sennin_hissu_ninzu"].
        " where sisetu_id = ".$sisetu_id;
        c2dbExec($sql);
    }
    echo "ok".'{forwardSection:"mst_sisetu"}';
    die;
}


//****************************************************************************************************************
// 施設基準への職員登録
//****************************************************************************************************************
function reg_mst_sisetu_emp_list() {
    $sisetu_id = (int)$_REQUEST["sisetu_id"];
    $emp_type = $_REQUEST["emp_type"];
    $emp_ids = explode("\t", $_REQUEST["emp_ids"]);
    if ($sisetu_id<1) { echo "不正なアクセスです。(sisetu_id)"; die; }
    if ($emp_type!="SENJU" && $emp_type!="SENNIN" && $emp_type!="SYORUI_HAKKOU") { echo "不正なアクセスです。(emp_type)"; die; }

    c2dbExec("delete from spfm_sisetu_emp_list where sisetu_id = ".$sisetu_id." and emp_type = '".$emp_type."'");
    foreach ($emp_ids as $emp_id) {
        if (!$emp_id) continue;
        c2dbExec("insert into spfm_sisetu_emp_list ( sisetu_id, emp_type, emp_id ) values (".$sisetu_id.",'".$emp_type."', ".c2dbStr($emp_id).")");
    }

    $sql =
    " update spfm_sisetu_emp_list set".
    " last_class_id = empmst.emp_class".
    ",last_atrb_id = empmst.emp_attribute".
    ",last_dept_id = empmst.emp_dept".
    ",last_room_id = empmst.emp_room".
    " from empmst".
    " where empmst.emp_id = spfm_sisetu_emp_list.emp_id".
    " and spfm_sisetu_emp_list.sisetu_id = ".$sisetu_id.
    " and spfm_sisetu_emp_list.emp_type = '".$emp_type."'";
    c2dbExec($sql);

    echo "ok".'{forwardSection:"mst_sisetu"}';
    die;
}


//****************************************************************************************************************
// 施設基準の削除
//****************************************************************************************************************
function reg_mst_sisetu_del() {
    $sisetu_id = (int)$_REQUEST["sisetu_id"];
    if ($sisetu_id<1) { echo "不正なアクセスです。(sisetu_id)"; die; }

    c2dbExec("delete from spfm_sisetu where sisetu_id = ".$sisetu_id);
    c2dbExec("delete from spfm_sisetu_emp_list where sisetu_id = ".$sisetu_id);
    echo "ok".'{forwardSection:"mst_sisetu"}';
    die;
}

