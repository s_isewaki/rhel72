<?php
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// CORE 2 Login And LoginCheck
//
// 関数２個
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

require_once(dirname(__FILE__)."/C2Env.php"); // CoMedixシステムならではの汎用関数群
c2env::adjustCurrentDir(); // ★★★必要ならカレントディレクトリ変更
require_once(dirname(__FILE__)."/C2DB.php"); // DBアクセッサ
require_once("Cmx.php"); // DB接続定義、conf.inf等の読込みのために必要。遠い将来は読込み廃止したい

class c2login
{

    //**********************************************************************************************************************
    // ログイン処理。main_menu.phpのみでコール。
    // ランチャー呼出しなら、ここには来ない。
    //**********************************************************************************************************************
    function tryLogin($try_login_id, $try_password, $try_systemid)
    {
        // 「systemid」なるものを指定してきた場合は、そこからパスワードを求める
        if ($try_login_id!="" && $try_systemid!="") {
            $prf_org_cd = c2dbGetOne("select prf_org_cd from profile");
            if (strcmp($try_systemid, md5($prf_org_cd)) == 0) {
                $sql = "select emp_login_pass from login where emp_login_id = ".c2dbStr($id)." and emp_id in (select emp_id from authmst where not a.emp_del_flg)";
                $try_password = c2dbGetOne($sql);
            }
        }

        // 端末認証キーチェックがonの場合
        $client_auth = c2dbGetSystemConfig('security.client_auth'); // 環境設定⇒その他
        if ($client_auth == 1) {
            // 環境設定権限も端末認証権限もなければ端末認証キーをチェックする
            $sql = "select a.emp_config_flg, a.emp_cauth_flg from login l inner join authmst a on l.emp_id = a.emp_id where l.emp_login_id = ".c2dbStr($try_login_id);
            $row = c2dbGetTopRow($sql);
            if ($row["emp_config_flg"]!="t" && $row["emp_cauth_flg"]!="t") {
                $auth_key = $_COOKIE["caid"];
                if ($auth_key == "") {
                    c2ShowAlertAndGotoLoginPageAndDie('本端末でのログインは許可されていません。');
                }
                $sql = "select count(*) from client_auth where auth_key = ".c2dbStr($auth_key)." and (not disabled)";
                $cnt = (int)c2dbGetOne($sql);
                if ($cnt == 0) {
                    c2ShowAlertAndGotoLoginPageAndDie('本端末でのログインは許可されていません。');
                }
            }
        }
        if ($try_login_id == "" || $try_password == "") {
            c2ShowAlertAndGotoLoginPageAndDie('IDとパスワードを入力してください。');
        }
        // try_login_idとtry_passwordがある場合にはDBと照合
        $sql =
        " select l.emp_id from login l inner join authmst a on l.emp_id = a.emp_id".
        " where l.emp_login_id = ".c2dbStr($try_login_id)." and l.emp_login_pass = ".c2dbStr($try_password)." and (not a.emp_del_flg)";
        $rows = c2dbGetRows($sql);
        // レコード数が1以外ならログイン画面へ戻す
        if (count($rows)!=1) {
            c2ShowAlertAndGotoLoginPageAndDie('IDとPasswordが間違っています');
        }
        $emp_id = $rows[0]["emp_id"];

        // 同時ログインを許可しない場合(ゲストでない場合）
        if ($try_login_id!="guest" && $try_login_id!="guest2") {
            // 同時ログイン警告フラグを取得
            $multi_login_alert = c2dbGetOne("select multi_login_alert from config");
            // 無視してログインしてよければ
            if ($multi_login_alert=="f" || $_REQUEST["del_session"]=="1") {
                c2dbExec("delete from session where emp_id = ".c2dbStr($emp_id)); //sessionとユーザーIDが同じなら削除する
            // 無視してログインしたらだめな場合
            } else {
                // 同じユーザがログイン中かどうかチェック
                $sql = "select count(session_id) from session where emp_id = ".c2dbStr($emp_id);
                global $SESSIONEXPIRE;
                if ($SESSIONEXPIRE > 0) {
                    $current = time();
                    $expire = $current - $SESSIONEXPIRE;
                    $sql .= " and session_made between '$expire' and '$current'";
                }
                $already_login_count = c2dbGetOne($sql);
                // 同時ログインの場合は、「del_session削除指示」を追加したフォームを作成⇒再送してもらうように準備する。
                if ($already_login_count) {
                    $errmsg = '同じ職員IDで他の画面でログイン中です。\nログインすると、他の画面は、強制的にログアウトされ、編集中のデータは破棄されます。';
                    echo '<body>';
                    echo '<form name="login" action="main_menu.php" method="'.$_SERVER['REQUEST_METHOD'].'">';
                    foreach ($_REQUEST as $key => $val) {
                        echo '<input type="hidden" name="'.htmlspecialchars($key, ENT_COMPAT, 'EUC-JP').'"';
                        echo '" value="'.htmlspecialchars($val, ENT_COMPAT, 'EUC-JP').'">';
                    }
                    echo '<input type="hidden" name="del_session" value="1">';
                    echo '</form>';
                    echo '<script type="text/javascript">';
                    echo ' if (confirm(\''.$errmsg.'\')) document.login.submit();';
                    echo ' else location.href = "login.php"';
                    echo '</script></body>';
                    die;
                }
            }
        }
        $purge_on_login = c2dbGetSystemConfig('session.purge_on_login');
        if ($purge_on_login) {
            c2dbExec("delete from session where emp_id = ".c2dbStr($emp_id)." and session_made <= '".strtotime("-3 days")."'");
        }

        $new_session = md5(uniqid(rand(), 1));
        $hashlen = strlen($new_session);
        if (!$hashlen) {
            c2LogErrorAndDie("", c2GetLogTypicalMsg("301"), c2env::getBacktrace());
        }
        $sessiondate = time();
        $sql = " insert into session (session_id,emp_id,session_made) values(".c2dbStr($new_session).",".c2dbStr($emp_id).",".c2dbStr($sessiondate).")";
        c2dbExec($sql);

        $postfix = (defined("C2_DEV_MODE") ? "(1)": "");
        if (!$new_session) {
            if (array_key_exists("ajax_action", $_REQUEST)) {
                echo "SESSION_EXPIRED";
                die;
            } else {
                c2ShowAlertAndGotoLoginPageAndDie("・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。");
            }
        }

        return $new_session;
    }

    //**********************************************************************************************************************
    // セッションチェックを行う。
    // 現在有効なログインかを、DBへ確認する。
    // 成功時は、ログインユーザの基本情報を返す。
    // 失敗したらログイン画面へ送還しアプリを終了する
    //**********************************************************************************************************************
    function validateSession()
    {
        require_once(dirname(__FILE__) . '/../../../about_session.php');

        // 画面に渡ってきたsessionを取得する。
        // 序所に$_REQUEST["session"]は廃止中 ⇒ $_COOKIE["CMX_AUTH_SESSION"]へ切替中
        // ただし、セッションIDは、$_REQUEST優先でチェック。ランチャーがあるため。
        // $_REQUESTに値がなければ$_COOKIEからsessionを取得する。
        // ※ $_COOKIEへのsession値格納は、main_menu.phpで行っている。
        $_sess = $_REQUEST["session"];
        if (!$_sess) {
            $_sess = $_COOKIE["CMX_AUTH_SESSION"];
        }
        if ($_sess=="") {
            if (array_key_exists("ajax_action", $_REQUEST)) {
                echo "SESSION_EXPIRED";
                die;
            } else {
                c2ShowAlertAndGotoLoginPageAndDie("・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。");
            }
        }

        // DBに登録済の有効な自己セッションを取得するためのSQL
        $current = time();
        $sql = "select session_id from session where session_id = ".c2dbStr($_sess);

        $SESSIONEXPIRE = get_session_expire();
        if ($SESSIONEXPIRE > 0) {
            $expire = $current - $SESSIONEXPIRE;
            $sql .= " and session_made between '$expire' and '$current'"; //現在時刻とセッション有効期限の間なら有効
        }

        // $_REQUEST["session"]や「$session」で判定しているアプリ用に展開
        global $session;
        $session = c2dbGetOne($sql); // DBからsessionを検索し、値を格納
        $_REQUEST["session"] = $session;

        // session取れなければエラー
        $postfix = (defined("C2_DEV_MODE") ? "(3)".$_sess: "");
        if ($session=="") {
            if (array_key_exists("ajax_action", $_REQUEST)) {
                echo "SESSION_EXPIRED";
                die;
            } else {
                c2ShowAlertAndGotoLoginPageAndDie("・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。");
            }
        }

        // session持続時間を更新
        $sql = "update session set session_made = '$current' where session_id = ".c2dbStr($session);
        c2dbExec($sql);

        // C2アプリのために、DEFINEにも展開
        define("C2_SESSION_ID", $session);

        // よく利用する職員ID、職員名はDEFINEに保持する。
        // それ以外は戻り値として行を返す。
        $emp_id = c2dbGetOne("select emp_id from session where session_id = ".c2dbStr($session));
        if (!$emp_id) {
            c2ShowAlertAndGotoLoginPageAndDie("・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。");
        }
        $row = c2dbGetTopRow("select * from empmst where emp_id = ".c2dbStr($emp_id));
        if (!$row) {
            c2ShowAlertAndGotoLoginPageAndDie("・長時間利用されていない\n・2重にログインした\n・Comedixからログアウトした\nいずれかの理由により利用できません。\n\n再度ログインしてください。");
        }
        define("C2_LOGIN_EMP_ID", trim(@$row["emp_id"]));
        define("C2_LOGIN_EMP_LT_NM", trim(@$row["emp_lt_nm"]));
        define("C2_LOGIN_EMP_FT_NM", trim(@$row["emp_ft_nm"]));
        return $row;
    }
}
