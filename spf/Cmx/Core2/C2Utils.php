<?
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf.inf");
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf_define.inf");

define("C2_EUCJP_ENCODE_STRING", (phpversion() < '5.2.1' ? "eucJP-win" : "CP51932"));

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// CORE 2 Utils
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――


// 経過年取得 誕生日や在籍年数算出で利用
function c2GetElapseYear($fr_ymd, $to_ymd="", $postfix="") {
    if (strlen($to_ymd)!=8) $to_ymd = date("Ymd");
    if ($fr_ymd > $to_ymd) return "";
    if (strlen($fr_ymd)!=8) return "";
    return substr($to_ymd,0,4) - substr($fr_ymd,0,4) - (substr($fr_ymd,4) > substr($to_ymd,4) ? 1 : 0).$postfix;
}


// CoMedixでは通常、北海道がゼロとなることに注意
function c2GetPrefList($insert_blank=false) {
    $ary = array(
        "", // インデックスゼロは空番
        "北海道",
        "青森県","岩手県","宮城県","秋田県","山形県","福島県",
        "茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県",
        "新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県","静岡県","愛知県",
        "三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県",
        "鳥取県","島根県","岡山県","広島県","山口県",
        "徳島県","香川県","愛媛県","高知県",
        "福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県",
        "沖縄県"
    );
    if (!$insert_blank) array_shift($ary); // ブランク無しなら、先頭の空白は除去する
    return $ary;
}



//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// HTMLエスケープ
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――


//--------------------------------------------------------------------
// eucシステムのうちは、どちらかというと、h() を利用せず、以下のhh()を利用することを推奨。
//
// about_comedixのh()と同じく、文字列をHTMLエスケープするためにある。
// しかしそのうち数値参照文字については、EUCだと物理的に完全エスケープは不可能。hh()は、いささか小細工を含んでいる。
//
// たとえば非EUC「はしご�癲廚鬟罅璽兇�入力したとき、ブラウザは、「&#39641;」として扱い、実際に「数値参照文字」のままPOST送信もする。
// しかし、ユーザが「はしご�癲廚鯑�力したのか、実は「&#39641;」のとおりに入力したのか、それを判断することはできない。
//
// よって、数値参照文字のまま、エスケープしないようにする、なのだが、それはそれで問題があって、
// 1) 「&#39641;」 は、はしご�發任△襦�「&#39641;」このまま入力した、とは「考えにくい」。よって、「&」→「&amp;」にエスケープしないでおく。
// 2)「&#9;」は、タブ文字である。ブラウザに渡せばタブ文字として認識されるが、普通にタブ文字でなく、わざわざこの表現で入力されることは、基本無い。
// 3)「&#0;」は、文字ではない。ブラウザに渡せば問題が発生する可能性もある。普通に「&」→「&amp;」にすべき。
// 上記から、
// ※ASCII内の文字で無い制御文字コードは出力しないようにする。
// ※ASCII以上なら、もし非文字であっても、それは単に「フォントが無い」という解釈が可能となる。
// ※１文字コードずつ判断するのは面倒なので、ASCII以下の文字なら、それは数値参照文字でなく、ただの文字列なのさ、ということにする。
//
// hh()によりPHP4であっても大幅にUTF-8文字が扱えるようになるのだが、いろいろと完全には解消できていない（物理的にできないものもある）。以下注意
// ・Unicodeサロゲートペアや合成文字(macは合成文字をよく使うが、合成文字でしかあり得ない絵文字などは特に注意)
// ・csvやexcel・pdfなどへの出力
// ・php4とphp5のmb_convert_encoding。php4にはCP51932が無い。
// ・見た目同じなのに文字コードが異なるもの。重複登録されているもの。いつの間にかコードが変わる可能性もある。（コードが変わること自体には問題はないが）
//--------------------------------------------------------------------
if (!function_exists("hh")) {
function hh($var) {
    if (is_array($var)) {
        return "htmlspecialcharsへの引数が不正：".c2GetBacktrace();
    }
    // 文字列をh()でエスケープするのだが、
    // 「&#NNNN;」または「&#xNNNN;」形式の部分については、そこだけ抜き取って、
    // c2uToEntity()関数にその部分だけエスケープを任せる。
    return preg_replace_callback("/\&amp\;\#([xX]?)([0-9A-Fa-f]+);/","_c2toEntity", str_replace(" ", "&nbsp;", htmlspecialchars($var)));
}
}

//******************************************************************************
// 垂直タブ除去 vertical tab elimination
//******************************************************************************
function ve($s) {
    return preg_replace('/[\x0b]/', '', $s);
}
//******************************************************************************
//
//******************************************************************************
function c2NumberFormat($v) {
    if (!$v) return ""; // ゼロはカラで返す
    return number_format($v);
}



//--------------------------------------------------------------------
// 数値文字参照形式を整形する。
// 先頭に「&#」をつけて、末尾に";"をつけて返したい。
// $matchesは要素２の配列。第３要素は数値。第２要素は、エックスか、空値。
// 数値参照文字であっても、非文字（バイナリデータか制御文字）をブラウザに渡すのは危険なため、
// ただの文字列として返す。
//--------------------------------------------------------------------
function _c2toEntity($matches) {
    $x = $matches[1]; // 第２要素：エックスか、空値
    $n = $matches[2]; // 第３要素：数値
    if ($x) { // エックスがある。つまり16進数である。
        if ($n<0xFF) return '&amp;#'.$x.$n.";"; // 数値部分が0xFF以下。つまりASCIIか、非文字。これはエスケープして普通の文字列として返す。
        return "&#x".$n.";"; // ASCII以上。数値参照文字形式（&#x...;）にして返す。
    }
    // エックスが無い場合
    if ($n < 128) return '&amp;#'.$n.";"; // 数値部分が0xFF以下。つまりASCIIか、非文字。これはエスケープして普通の文字列として返す。
    return "&#".$n.";"; // ASCII以上。数値参照文字形式（&#x...;）にして返す。
}
//--------------------------------------------------------------------
// ワードブレーク関数
// 半角文字が英単語のように改行されないのを防ぐため、
// 文字列を1文字ずつ分断して、<wbr>でつなぐ。
// しかし昨今では、 <wbr>では頑固に改行しないままのブラウザもある。よって「&#8203;」でつなぐようにする。
// （しかし「&#8203;」では、余分な文字間スペースが発生するレガシーブラウザもある。）
//--------------------------------------------------------------------
function c2wbr($str, $s2=""){
    $str = trim($str);
    $s2 = trim($s2);
    if (!mb_strlen($str) && !mb_strlen($s2)) return "";
    $out = array();
    $matches = array();
    $dmy = array("","","");
    while (strlen($str)) {
        if ($str[0]=="&" && preg_match("/^\&\#[xX]?[0-9A-Fa-f]+;/", $str, $matches)==1) {
            $len = strlen($matches[0]);
            $str = substr($str, $len);
            $mat = substr($matches[0], 2, strlen($matches[0])-3);
            $dmy[1] = ($mat[0]=="x" || $mat[0]=="X" ? "x" : "");
            if ($dmy[1]) $mat = substr($mat, 1);
            $dmy[2] = $mat;
            $out[] = c2uToEntity($dmy);
        } else {
            $out[] = hh(mb_substr($str, 0, 1));
            $str = mb_substr($str, 1);
        }
    }
    return implode("&#8203;", $out).$s2;
}
//--------------------------------------------------------------------
// 配列をワードブレークマークアップでつなぐ。
// 「&#8203;」と「<wbr>」のダブルパンチにする。
// IE6などでは、文字スペースが空いてしまうかもしれないため注意が必要。IE6はもうじき対応外になる。IE6非対応アプリを中心に利用のこと。
//--------------------------------------------------------------------
function c2ary_wbr($ary) {
    return implode("&#8203;<wbr>",$ary);
}
//--------------------------------------------------------------------
// nl2brの別バージョン。
// nl2brだと、「\n」は「\n」＋「<br />」に変換される。\nを無くすための関数。
//--------------------------------------------------------------------
function c2nl2br($v) {
    return preg_replace("/\n/","<br />", $v);
}

//--------------------------------------------------------------------
// boolean判定を行う。
// DBのbooleanは"f"であり、"f"なのか、nullなのか、空文字が取得されてしまったのか、など、一気に判別できないため。
// またフラグ系はbooleanでなくint=1やvarchar=1の場合もある。これらは一律、この関数が利用可能。
// なお、結果、この関数でfalseとなる場合は、
// 1) カラ文字
// 2) null
// 3) 数値でゼロ
// 4) 文字のゼロ
// 5) 小文字の"f"
//--------------------------------------------------------------------
function c2bool($b) {
    if ($b==="f") return false;
    if ($b==="false") return false;
    if ($b==="FALSE") return false;
    return $b ? true : false;
}

//--------------------------------------------------------------------
// JavaScript構文としてエスケープ
// ダブルクォート、改行文字、バックスラッシュの３つでＯＫ
//--------------------------------------------------------------------
function c2js($s) {
    return mb_ereg_replace('"', '\"', mb_ereg_replace("\r", '', mb_ereg_replace("\n", "\\n", mb_ereg_replace("\\\\", '\\\\', $s))));
}
//--------------------------------------------------------------------
// JavaScript構文としてエスケープ
// ダブルクォート、改行文字、バックスラッシュ、シングルクォートの４つに加えてシングルクォートも
//--------------------------------------------------------------------
function c2jsS($s) {
    return mb_ereg_replace("'", "\'", c2js($s));
}


//--------------------------------------------------------------------
// 連想配列をJSONへ変換
// 外部LIBは細かい変換仕様が不明瞭かつ口にあわないし、
// バージョンUPによって挙動が変わる可能性や懸念は排除したい。あまり利用したくない。
// やりたいことは些細なことなので、自作定義。
//--------------------------------------------------------------------
function c2ToJson($var, $is_recursive=0) {
    if (gettype($var) == 'array') {
        // 連想配列の場合
        if (is_array($var) && count($var) && (array_keys($var) !== range(0, sizeof($var) - 1))) {
            $properties = array();
            foreach ($var as $k => $v) {
                $quot = 0;
                if ($k=="") $quot = 1;
                else if (preg_match("/[0-9]/", $k[0])) $quot = 1; // 先頭が数値ならクォート
                else if (strpos($k, "@")!==FALSE) $quot = 1;
                $properties[] = ($quot?'"'.$k.'"':$k).":".c2ToJson($v, 1);
            }
            return '{' . join(',', $properties) . '}';
        }
        // 通常の配列の場合
        $properties = array();
        foreach ($var as $v) $properties[] = c2ToJson($v, 1);

        return '[' . join(',', $properties) . ']';
    }
    // それ以外なら、文字列として処理
    if (!$is_recursive && $var=="") return "{}";
    return '"' . c2js($var) . '"';
}








//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 日付ユーティリティ
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

//--------------------------------------------------------------------
// 指定「年月日時分」の別表現をいろいろ作る
// 「利用側都合」の関数なので、クセが有る場合は適宜定義を「追加」するように。原則、既存の定義を修正してはいけない。
// 年月日のみで時分がなくとも利用可能
// 引数の日付妥当性は確認しない。9999/99/99 などがあり得る。
//--------------------------------------------------------------------
function c2ConvYmd($ymdhms, $format="") {
    if (strlen($ymdhms)<6) { // 日がなくとも年月(yyyymm)までは判定可能
        if ($format!="") return "";
        return array();
    }
//    if (!preg_match("/^[0-9\/]+$/", $sect)) return $ymdhms;

    $yy = substr($ymdhms, 0, 4);
    $mm = substr($ymdhms, 4, 2);
    $dd = substr($ymdhms, 6, 2);
    $hh = substr($ymdhms, 8, 2);
    $mi = substr($ymdhms, 10, 2);
    $ss = substr($ymdhms, 12, 2);
    $ymd = $yy.$mm.$dd;
    $hm = $hh.$mi;
    $zmm = ltrim($mm, "0");
    $zdd = ltrim($dd, "0");
    $zhh = ltrim($hh, "0"); if ($zhh=="") $zhh = "0";
    $weeks = array("日", "月", "火", "水", "木", "金", "土");
    $week_jp = "";
    if (strlen($ymd)==8) {
        $week = date("w", strtotime($ymd));
        $week_jp = $weeks[$week];
    }
    $out = array(
        "yy"=>$yy, "mm"=>$mm, "dd"=>$dd, "hh"=>$hh, "mi"=>$mi, "ss"=>$ss, "ymd"=>$ymd,
        "zmm"=>$zmm, "zdd"=>$zdd, "zhh"=>$zhh,
        "slash_ymd"    =>($dd!="" ? $yy."/".$mm."/".$dd                    : ""),
        "slash_ym"     =>($mm!="" ? $yy."/".$mm                            : ""),
        "slash_zymd"   =>($dd!="" ? $yy."/".$zmm."/".$zdd                  : ""),
        "slash_ymdhm"  =>($mi!="" ? $yy."/".$mm."/".$dd." ".$hh.":".$mi    : ""),
        "slash_ymdhms" =>($mi!="" ? $yy."/".$mm."/".$dd." ".$hh.":".$mi.":".$ss : ""),
        "slash_mdhm"   =>($mi!="" ? $mm."/".$dd." ".$hh.":".$mi            : ""),
        "slash_zymdhm" =>($mi!="" ? $yy."/".$zmm."/".$zdd." ".$zhh.":".$mi : ""),
        "slash_zymdhms"=>($mi!="" ? $yy."/".$zmm."/".$zdd." ".$zhh.":".$mi.":".$ss : ""),
        "slash_zmd"    =>($dd!="" ? $zmm."/".$zdd                          : ""),
        "slash_zymdw"  =>($dd!="" ? $yy."/".$zmm."/".$zdd."(".$week_jp.")" : ""),
        "slash_zmdw"   =>($dd!="" ? $zmm."/".$zdd."(".$week_jp.")"         : ""),
        "hhmi"   =>($mi!="" ? $hh.$mi      : ""),
        "hh_mi"  =>($mi!="" ? $hh.":".$mi  : ""),
        "zhh_mi" =>($mi!="" ? $zhh.":".$mi : "")
    );
    if ($format!="") {
        if (!array_key_exists($format, $out)) return "c2ConvYmd()第二引数のパターンは存在しません(".$format.")：\n".c2GetBacktrace();
        return $out[$format];
    }
    return $out;
}

function c2Checkdate($ymd) {
    $aYmd = c2ConvYmd($ymd);
    return checkdate((int)$aYmd["mm"], (int)$aYmd["dd"], (int)$aYmd["yy"]);
}






//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// プログラムトレース
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

function c2ShutdownFunction() {
    echo c2GetTrace("hs");
}

global $_c2TraceArray;
$_c2TraceArray = array();

global $_c2TraceMicrotime;
$_c2TraceMicrotime = -1;

global $_c2TraceMicrotimeTtl;
$_c2TraceMicrotimeTtl = -1;

function c2Trace($div1, $msg) {
    global $_c2TraceArray;
    global $_c2TraceMicrotime;
    global $_c2TraceMicrotimeTtl;
    if (!$_COOKIE["C2TRACE"] && !count($_c2TraceArray)) {
        $_c2TraceArray[] = array(
            "ymdhms"=>"", "elapse"=>"", "mem"=>"", "ttl"=>"", "div1"=>$div1, "msg"=>"トレース開始指定がありません。", "backtrace"=>c2GetBacktrace()
        );
        return;
    }

    $mc = substr(microtime(true), 5);
    $mc = $mc * 10000;
    $elapse = "-";
    $ttl = "-";
    if ($_c2TraceMicrotime >= 0) {
        $elapse = ((int)($mc - $_c2TraceMicrotime)) / 10000;
        $ttl    = ((int)($mc - $_c2TraceMicrotimeTtl)) / 10000;
    }

    $_c2TraceArray[] = array(
        "ymdhms"=>date("H:i:s"),
        "elapse"=>$elapse,
        "mem"=>memory_get_usage(),
        "ttl"=>$ttl,
        "div1"=>$div1,
        "msg"=>$msg,
        "backtrace"=>c2GetBacktrace()
    );

    $_c2TraceMicrotime = $mc;
    if ($_c2TraceMicrotimeTtl < 0) $_c2TraceMicrotimeTtl = $mc;
}
function c2GetTrace($fmt) {
    if (!$_COOKIE["C2TRACE"]) return;
    c2Trace("[C2Utils][END]", "終了時最終トレース");
    global $_c2TraceArray;

    $br = (strpos($fmt, "b")!==FALSE ? "<br>" : "\n"); // $fmtに「b」文字が含まれていれば、区切り文字は<br>とする
    $is_tag = (strpos($fmt, "t")!==FALSE ? "1" : ""); // $fmtに「t」文字が含まれていれば、HTMLタグをつける
    $is_tag = 1;

    $header = "";
    if (strpos($fmt, "h")!==FALSE) {// $fmtに「h」文字が含まれていれば、ヘッダをつける
        if ($is_tag) {
            $header = '<a id="c2TraceHeader" href="javascript:void(0)" onclick="document.getElementById(\'c2TraceResult\').style.display=\'none\'">【trace:'.date("Y/m/d").'】</a>';
        } else {
            $header = $br."【trace:".date("Y/m/d")."】".$br;
        }
    }

    if (strpos($fmt, "s")!==FALSE) { // $fmtに「s」文字が含まれていれば、文字列として返す
        if ($is_tag) {
            $out = array();
            if ($_COOKIE["C2TRACE"]=="direct") {
                $out[]='<div style="background-color:#ffff00; position:absolute; left:0; top:0; z-index:50001; font-size:12px;">';
                $out[]= '<a href="javascript:void(0)" onclick="var o=document.getElementById(\'c2TraceResult\'); o.style.display=(o.style.display==\'none\'?\'\':\'none\')">';
                $out[]= '<b>trace</b></a></div>';
            }
            $out[]= '<pre id="c2TraceResult" style="line-height:1.4; font-family:\'Osaka・等幅\',\'ＭＳ ゴシック\',monospace; font-size:12px; position:absolute; z-index:50000; top:0; left:0; height:100%; overflow:scroll; background-color:#ffffff; border:1px solid #9999ff; display:none">'.$header;
            $logIdx = 0;
            $rowIdx = 0;
            foreach ($_c2TraceArray as $rr) {
                $rowIdx++;
                $memb = $rr["mem"] % 1024; // バイト端数
                $_memk = ($rr["mem"] - $memb) / 1024; // kb総量
                $memk = $_memk % 1024; // kb端数
                $_memm = $_memk - $memk; // mb総量
                $memm = $_memm % 1024; // mb端数
                $tmsg =
                "[".$rr["ymdhms"]."]".
                str_pad($rowIdx, 5," ").
                "[経過]".str_pad($rr["elapse"],8," ").
                "[合計]".str_pad($rr["ttl"],8," ").
                "[MEM]".str_pad($memm,3," ",STR_PAD_LEFT)."m+".str_pad($memk,4," ",STR_PAD_LEFT)."k+".str_pad($memb,4," ",STR_PAD_LEFT)."b".
                " ".$rr["div1"];
                if (!is_array($rr["msg"])) {
                    $tmsg .= " ".$rr["msg"];
                    $rr["msg"] = array();
                }
                $rr["msg"]["backtrace"] = $rr["backtrace"];
                $out[]= '<div class="c2TraceItem" style="border-bottom:1px solid #bbbbbb;">'.$tmsg;
                if (is_array($rr["msg"])) {
                    $out1 = array();
                    $out2 = array();
                    foreach ($rr["msg"] as $k => $v) {
                        $clr = ($k=="error" ? "ff0000":"00aa00");
                        if ($k=="backtrace") $clr = "0099cc";
                        $logIdx++;
                        $out1[] = '&nbsp;<a href="javascript:void(0)" rid="c2us'.$logIdx.'" onclick="return c2uAlt(this)">'.$k."</a>";
                        $out2[] = '<span id="c2us'.$logIdx.'" style="display:none; color:#'.$clr.'"><br>'.$v.'</span>';
                    }
                }
                $out[]= implode("", $out1).implode("", $out2).'</div>';
            }
            $out[]= '</pre>';
            $out[]= '<script type="text/javascript">var c2uAlt=function(o){var id=o.getAttribute(\'rid\'); var d=document.getElementById(id).style.display; document.getElementById(id).style.display=(d=="none"?"":"none"); o.style.color=(d?"#ff0000":""); return false; }</script>';
            return implode("", $out);
        } else {
            $out = array();
            foreach ($_c2TraceArray as $rr) {
                $memb = $rr["mem"] % 1024; // バイト端数
                $_memk = ($rr["mem"] - $memb) / 1024; // kb総量
                $memk = $_memk % 1024; // kb端数
                $_memm = $_memk - $memk; // mb総量
                $memm = $_memm % 1024; // mb端数
                $tmsg =
                "[".$rr["ymdhms"]."]".
                "[経過]".str_pad($rr["elapse"],8," ").
                "[合計]".str_pad($rr["ttl"],8," ").
                "[MEM]".str_pad($memm,3," ",STR_PAD_LEFT)."m+".str_pad($memk,4," ",STR_PAD_LEFT)."k+".str_pad($memb,4," ",STR_PAD_LEFT)."b".
                " ".$rr["div1"];
                if (is_array($rr["msg"])) {
                    foreach ($msg as $k => $v) {
                        $tmsg .= " [".$k."]".$v;
                    }
                } else {
                    $tmsg .= " ".$rr["msg"];
                }
                $out[]=$tmsg;
            }
            return $header.implode($br, $out);
        }
    }
    return $_c2TraceArray;
}

function c2GetBacktrace($separator = "\n") {
    if (!function_exists("debug_backtrace")) return "";
    $ary = debug_backtrace();
    $out = array();
    $prev = "";
    for ($idx = count($ary)-1; $idx>=0; $idx--) {
        if ($ary[$idx]["function"]=="c2GetBacktrace") break;
        if ($ary[$idx]["function"]=="c2dbMakeSqlErrorHtml") break;
        $out[]=
        "【".basename(dirname($ary[$idx]["file"]))."/".basename($ary[$idx]["file"])."】 --- "
        .$prev.$ary[$idx]["line"]."行目で ".$ary[$idx]["function"]."() を実行";
        $prev = "  ".$ary[$idx]["function"]."() 関数";
        if ($ary[$idx]["function"]=="c2dbExec") break;
    }
    return (count($out) ? "最初に" : "").implode($separator."次に", $out);
}

function _c2TraceSetFromRequest() {
    $c2t = @$_REQUEST["c2trace"];
    if ($c2t=="download" || $c2t=="tool" || $c2t=="direct") {
        setcookie("C2TRACE", $c2t);
    }
    if ($c2t=="off" || $c2t=="none" || $c2t=="end" || $c2t=="exit" || $c2t==="0") {
        setcookie("C2TRACE", "", time()-3600);
    }
    if (@$_COOKIE["C2TRACE"]) {
        register_shutdown_function('c2ShutdownFunction');
    }
}
_c2TraceSetFromRequest();



// /comedix/ や、/redhat6/などの文字を返す。前後スラつき
function c2GetContentsRootPath(){
    $document_root = $_SERVER["DOCUMENT_ROOT"]; // apache ドキュメントルート。/var/www/htmlあたりとなる
    $contents_root = dirname(dirname(dirname(dirname(__FILE__)))); // コンテンツルートはこのUtilsの３階層上である。ファイル名入れて４回dirname()実行。
    return substr($contents_root, strlen($document_root))."/";
}
// /comedix/ までのパスを返す。/var/www/html/comedix/のようになる。前後スラつき。
function c2GetContentsAbsPath(){
    return dirname(dirname(dirname(dirname(__FILE__)))) . "/";
}
function c2AddIncludePath($dir) {
    set_include_path($dir . PATH_SEPARATOR . get_include_path());
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// エラー処理関連
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// システムエラーの場合。ほぼ100%、SQLエラーの場合。
function c2EchoErrorOrGotoErrorPageAndDie($errmsg, $err_place) {
    // AJAXモードでのエラーの場合
    if (defined("C2_AJAX_ACTION_NAME")) {
        if ($_REQUEST[C2_AJAX_ACTION_NAME]) {
            echo "【致命的エラー発生】\n";
            echo $err_place."\n".$errmsg;
            die; //アプリ終了
        }
    }

    $ecode = "";
    if ($GLOBALS["C2_ERROR_DB_CONNECTION"]) $ecode = 1; // C2のDBコネクションエラー
    if ($GLOBALS["ABOUT_POSTGRES_ERROR_DB_CONNECTION"]) $ecode = 2; // about_postgresのコネクションエラー

    // 開発デバッグモードでのエラーの場合、および、DB接続エラーでない場合は、画面にエラー即出し終了。エラー画面には遷移しない。
    if (defined("C2_DEV_MODE") && !$ecode) {
        $strong = '<span style="color:red; font-weight:bold">';
        $sqle = '[Native message: ERROR:';
        $sqle_pos = strpos($errmsg, $sqle);
        if ($sqle_pos > 0) {
            $errmsg = str_replace($sqle, $sqle.'</span>'.$strong, $errmsg);
        }
        $errmsg = str_replace("【", '【'.$strong, $errmsg);
        $errmsg = str_replace("】", '</span>】', $errmsg);
        $errmsg = str_replace("行目で", '行目で'.$strong, $errmsg);
        $errmsg = str_replace("を実行", '</span>を実行', $errmsg);

        echo "<br><br>【致命的エラー発生】(C2_DEV_MODE=1)<br>";
        echo $err_place."<br><span>".str_replace("\n", "</span><br><span>",$errmsg)."</span>";
        die; //アプリ終了
    }
    // JavaScript1でエラー画面へ。showpage.jsをロードせずに、必要中身をここに展開記載している。
    // トップ画面をエラー画面に遷移させつつ、現在の画面がトップ画面でなければ閉じるようにする。
    ?>
    <script type="text/javascript">
    function c2TmpGetMainWin(win) {
        if (win.name=='comedix_main') return win;
        if (win.opener && win.opener.location.hostname==win.location.hostname) return c2TmpGetMainWin(win.opener);
        if (win.top && win.top.location.hostname==win.location.hostname && win.top!=win) return c2TmpGetMainWin(win.top);
        return win;
    }
    var mainWin = c2TmpGetMainWin(window);
//    if (mainWin.forwardPage) mainWin.forwardPage("error.php<?=($ecode ? '?ecode='.$ecode : "")?>");
    mainWin.location = "<?=c2GetContentsRootPath()?>error.php<?=($ecode ? '?ecode='.$ecode : "")?>";
    if (window!=mainWin && !window.closed) window.close();
    </script>
    <?
    die; //アプリ終了
}



// ログインへ。基本的にはセッションエラーの場合。
function c2ShowAlertAndGotoLoginPageAndDie($errmsg) {
    if ($_REQUEST["ajax_action"]) {
        echo "SESSION_EXPIRED";
        die;
    }
    // 開発デバッグモードでのエラーの場合
/*
    if (defined("C2_DEV_MODE")) {
        echo str_replace("\n", "<br>", c2GetBacktrace());
        echo "<br><br>【ログインページへ】(C2_DEV_MODE=1)<br>";
        echo $err_place."<br>".str_replace("\n", "<br>",$errmsg);
        die; //アプリ終了
    }
*/
    if ($errmsg!="") echo("<script type='text/javascript'>alert('".$errmsg."');</script>\n");
    // JavaScript1でエラー画面へ。showpage.jsをロードせずに、必要中身をここに展開記載している。
    // トップ画面をエラー画面に遷移させつつ、現在の画面がトップ画面でなければ閉じるようにする。
    ?>
    <script type="text/javascript">
    function c2TmpGetMainWin(win) {
        if (win.name=='comedix_main') return win;
        if (win.opener && win.opener.location.hostname==win.location.hostname) return c2TmpGetMainWin(win.opener);
        if (win.top && win.top.location.hostname==win.location.hostname && win.top!=win) return c2TmpGetMainWin(win.top);
        return win;
    }
    var mainWin = c2TmpGetMainWin(window);
    mainWin.location.href = "<?=c2GetContentsRootPath()?>login.php";
    if (window!=mainWin && !window.closed) window.close();
    </script>
    <?
    die; //アプリ終了
}

function c2GetLogTypicalMsg($n) {
    require_once("conf/error.inf"); //エラーメッセージ一覧ファイルを読み込む
    if ($n=="001") return $ERROR001; // cannot find conf.inf
    if ($n=="002") return $ERROR002; // fail to write into a sql log
    if ($n=="101") return $ERROR101; // cannot connect to db
    if ($n=="102") return $ERROR102; // fail to execute select sql
    if ($n=="103") return $ERROR103; // fail to execute insert sql
    if ($n=="104") return $ERROR104; // fail to execute update sql
    if ($n=="105") return $ERROR105; // fail to execute delete sql
    if ($n=="106") return $ERROR106; // fail to check the record count
    if ($n=="201") return $ERROR201; // fail to open a file
    if ($n=="202") return $ERROR202; // fail to close a file
    if ($n=="203") return $ERROR203; // fail to read a whole file
    if ($n=="204") return $ERROR204; // fail to read a part of a file
    if ($n=="205") return $ERROR205; // fail to write into a file
    if ($n=="206") return $ERROR206; // a file does not exist
    if ($n=="207") return $ERROR207; // fail to read a csv file
    if ($n=="301") return $ERROR301; // fail to use hash
    if ($n=="12")  return $ERROR12;  // fail to write into a file
    return "";
}

function c2LogErrorAndDie($fname, $errmsg, $backtrace=""){
    if (!$fname) $fname = c2GetCallerFileName("");
    $errorDate = date("D M j G:i:s T Y");  // 例  Sat Mar 10 15:16:08 MST 2001
    if (is_file("./log/atmodule_err.log")){
        if (filesize("./log/atmodule_err.log") < 1024*1024) { // 1Mbまで
            @error_log($errorDate ."--> ".$fname.":". $errmsg . "\n",3,"./log/atmodule_err.log");
        }
    }
    error_log($errorDate ."--> ".$fname.":".$errmsg.($backtrace?"\n":"").$backtrace. "\n",3,"./log/error.log");
    if (defined("C2_DEV_MODE")) { echo "invalid session"; die; }
    c2ShowAlertAndGotoLoginPageAndDie("");
}

function c2LogSqlErrorAndDie($fname, $errmsg, $sql, $err_place){
    if (!$fname) $fname = c2GetCallerFileName("");
    $errorDate = date("D M j G:i:s T Y");  // 例  Sat Mar 10 15:16:08 MST 2001
    if (is_file("./log/atmodule_err.log")){
        if (filesize("./log/atmodule_err.log") < 1024*1024) { // 1Mbまで
            @error_log($errorDate ."--> ".$fname.":". $errmsg . "\n",3,"./log/atmodule_err.log");
        }
    }
    error_log($errorDate ."--> ".$fname.":".$errmsg.($sql?"\n":"").$sql. "\n",3,"./log/error.log");
    c2EchoErrorOrGotoErrorPageAndDie($errmsg, $err_place);
}

// ２個前の実行ファイルのファイル名を返す
function c2GetCallerFileName($fname) {
    if (function_exists("debug_backtrace")) {
        $ary = debug_backtrace();
        $ret = basename($ary[1]["file"]);
        if ($ret) return $ret;
    }
    if ($fname) return $fname;
    return $fname; // TODO
}



function c2CheckExistRequestParams($request, $config) {
    if (!$config["RequestStrict"]) return array();
    $errors = array();
    $keys = array_keys($request);
    foreach ($keys as $key) {
        if (in_array($key, $config["SystemRequestParams"])) continue; // システム必須パラメータ。該当すればOKスルー
        if (in_array($key, $config["ScreenRequestParams"])) continue; // 画面に送信され得るパラメータ。該当すればOKスルー
        $errors[]= "画面へ不正なパラメータが送信されました。".$key;
    }
    foreach ($config["RequireRequestParams"] as $key) {
        if (in_array($key, $keys)) continue; // 送信されないといけないパラメータ。存在すればOKスルー
        $errors[]= "画面への必須パラメータが存在しません。".$key;
    }
    return $errors;
}


function c2OutputCsvHeader($fileName, $content_length = 0) {
    header("Content-Disposition: attachment; filename=".$fileName);
    header("Content-Type: application/octet-stream; name=".$fileName);
    if ($content_length) header("Content-Length: " . $content_length);
}

// 文字列をCSV取得
// 行区切りはShift_JISにつき復帰改行CRLFなのだが、
// 改行LFだけで判定し、分割された文字列から復帰CRをトリムするほうが安全だろう。
function c2GetCsv($cont, $delim = "\n", $is_line = false) {
    // 先頭にUTF8-BOMがあれば除去する
    if (!$is_line) {
        if (substr($cont, 0, 3)==pack('C*',0xEF,0xBB,0xBF)) $cont = substr($cont, 3);
    }
    $out = array();
    $ttl_len = strlen($cont);
    $offset = 0; // 文字列シーク位置カーソル
    $mod = 0; // Wクォート偶数奇数判定用
    $len = 0; // 文字列長
    $dlen = strlen($delim);
    $line_offset = 0;
    $sepa = "";
    for (;;) {
        $pos = strpos($cont, $delim, $line_offset+$offset); // デリミタの位置。文字列先頭がデリミタならゼロ
        if ($pos===FALSE && strlen($cont) - $line_offset - $offset > 0) $pos = strlen($cont); // デリミタ無しなら、終端の「次の文字」にデリミタがある、と仮定
        $slen = $pos - $line_offset - $offset; // 区切り文字を含んでいるかもしれない区間文字列長。
        if ($pos>0) {
            $part = substr($cont, $line_offset + $offset, $slen); // 区間文字列。区切り文字を含んでいるかもしれない。
            $quot = $mod + substr_count($part, '"'); // 区間文字列内のWクオートの数＋$mod。前回ループで奇数なら$modは1である。
            $mod = $quot % 2;// Wクォート数が偶数か奇数か。奇数なら改行コードは区切り文字でなくてデータである。
        }
        // 末尾未到達で、かつWクォート奇数ならオフセットを進める
        if ($mod==1 && ($line_offset+$len+$slen+$dlen < $ttl_len)) { $offset += $slen+$dlen; $len += $slen+$dlen; continue; }
        if ($is_line) {
            $out[]= _c2GetCsvEliminateWQuort(substr($cont, $line_offset+$offset, $len+$slen), $delim); // 列取得モードなら出力にそのまま追加
        } else {
            $line = rtrim(rtrim(substr($cont, $line_offset, $len+$slen+$dlen), "\r"), $delim); // 行データ
            // 項目区切り文字が決定していない場合（初回探索）
            if (!strlen($sepa)) {
                if (strpos($line, "\t")!==FALSE) $sepa = "\t"; // 本システムはCSVデータにタブを含まないので、もしタブがあればタブ区切りとみなす。
                else $sepa = ","; // そうでなければ、カンマ区切りであるとする。
            }
            $out[]= c2GetCsv($line, $sepa, true); // 行取得モードなら列を取得
        }
        $line_offset += $len + $slen+$dlen; // 次のデリミタ以降から開始するように
        if ($line_offset >= $ttl_len) break; // 最後に達したので終了
        $len = 0;
        $offset = 0;
        $mod = 0;
    }
    return $out;
}

function _c2GetCsvEliminateWQuort($str, $delim) {
    if (substr($str, -1)=="\r") $str = substr($str, 0, strlen($str)-1);
    $is_quorted = 0;
    if (substr($str, 0, 1)=='"') { $is_quorted++; $str = substr($str, 1); }
    if (substr($str, -1)=='"') { $is_quorted++; $str = substr($str, 0, strlen($str)-1); }
    // タブ区切りTSVである場合。文字列がダブルクオートされていない場合は、その中に含まれるWクォートはそのまま文字である。
    if ($delim=="\t" && $is_quorted!=2) return $str;
    // 通常はこちら。CSVか、TSVでもダブルクォートで囲まれている場合。その中に含まれるWクォートは、２個セットで１個である。
    return str_replace('""', '"', $str);
}

function c2MakeCmxCrypt($str) {
    if (!$str) { echo "[C2Utils::c2MakeCmxCrypt] error: argument empty."; die; }
    return md5("cmxc".$str);
    //$binpath = dirname(__FILE__)."/bin/";
    //$ipath = $binpath."cmx_crypt_ignore";
    //$module="cmx_crypt";
    //$cpath = $binpath.$module;
    //if (file_exists($cpath."_ignore")) return md5("cmxc".$str);
    //if (!file_exists($cpath)) {
	//	$err = c2BuildCmxBin("cmx_crypt");
	//	if ($err) { echo $err; die; }
	//}
    //$crypt = exec($cpath." ".$str." stdout");
    //$cmdret = exec("echo $?");
    //$err = "";
    //if ($cmdret) $err = "[C2Utils::c2MakeCmxCrypt] error: failure (".$cmdret.")";
    //else if (strlen($crypt)!=32) $err = "[C2Utils::c2MakeCmxCrypt] error: bytesize illegal: ".$crypt;
    //else return $crypt;
    //$fp = @fopen($ipath, "a");
    //@fwrite($fp, $err);
    //@fclose($fp);
    //return "";
}

//function c2BuildCmxBin($module) {
//	$err = "[c2BuildCmxBin] 未定義のエラーです。".$module;
//    $binpath = dirname(__FILE__)."/bin/";
//    $cpath = $binpath.$module;
//	for (;;) {
//	    $cwd = getcwd();
//		chdir ($binpath);
//		$cwd2 = getcwd();
//		if ($binpath != $cwd2."/") { $err = "[c2BuildCmxBin] ディレクトリ権限が正しくありません。"; break; }
//		@unlink ($cpath);
//		if (file_exists($cpath)) { $err = "[c2BuildCmxBin] ファイル削除権限がありません。".$module; break; }
//		@unlink ($cpath.".c");
//		if (file_exists($cpath.".c")) { $err = "[c2BuildCmxBin] ファイル削除権限がありません。".$module.".c"; break; }
//		if (!file_exists($cpath.".src"))  { $err = "[c2BuildCmxBin] 作成できませんでした。srcがありません。".$module; break; }
//		if (file_exists($binpath."cmx_unpack")) {
//			exec("cmx_unpack ".$module.".src");
//		} else {
//			exec("unzip ".$module.".src");
//		}
//		if (!file_exists($cpath.".c")) { $err = "[c2BuildCmxBin] ファイルが作成できません。".$module.".c"; break; }
//	    exec("gcc -o ".$module." ".$module.".c");
//	    $isok = exec("echo $?");
//	    chdir ($cwd);
//		if ($isok) { $err = "[c2BuildCmxBin] 生成エラーが発生しました。"; break; }
//		if (!file_exists($cpath)) { $err = "[c2BuildCmxBin] ファイルが作成できません。".$module; break; }
//		$err = "";
//		break;
//	}
//	if (!$err) @unlink ($cpath.".src");
//	@unlink ($cpath.".c");
//	return $err;
//}
