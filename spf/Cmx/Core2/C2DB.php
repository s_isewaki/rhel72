<?
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf.inf");
require(dirname(dirname(dirname(dirname(__FILE__))))."/conf/conf_define.inf");
require_once(dirname(dirname(dirname(dirname(__FILE__))))."/Cmx.php");
require_once(dirname(__FILE__)."/C2Utils.php"); // 汎用関数群
c2Trace("[C2DB]","ロード:".__FILE__);
c2Trace("[C2DB]","getcwd=".getcwd());


//require_once("../../../Cmx.php"); // DB接続定義、conf.inf等の読込みのために必要。遠い将来は読込み廃止したい
require_once('libs/MDB2.php'); // DB接続定義。PEAR利用。遠い将来は廃止されるだろう

c2Trace("[C2DB]","(DEFINED) DB_BASE_DIR=".CMX_BASE_DIR);
c2Trace("[C2DB]","(DEFINED) CONF_FILE_OK=".defined("CONF_FILE"?CONF_FILE:""));

if (!defined("CONF_FILE")) {
    echo "【C2DBプログラムエラー】C2DB.php：conf/conf.infが読込まれていません<br>";
    echo str_replace("\n", "<br>", c2GetBacktrace());
    die;
}



//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// CORE 2 DB Access
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

// DB関連グローバル、スタティック扱い
class _c2db {
    var $spTypes = array();
    var $spParams = array();
    var $isRemainParams = 0;
    var $con = null;
    var $c2Con = null;
    var $autoExecLogCategoryName = ""; // 好きに適当な値を指定すると、c2_exec_sql_logテーブルに更新系SQLの実行ログを作成します。
    var $lastErrorMsg = "";
    var $errorReturn = false;
}
global $c2db;
// グローバルに存在する$c2dbを返す。呼び元でglobal $c2dbとしなくてよいように。
$c2db = new _c2db();
function c2GetC2DB() {
    global $c2db;
    return $c2db;
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// 基礎処理と主インクルード
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

//======================================
// 開発モード
// SQLエラーでエラー画面に強制送還せずに停止させたい場合に、活性化すること。デバッガつきIDE利用者には不要かも
//======================================
define("C2_DEV_MODE", 1);




//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// DBユーティリティ関連 (MDB2利用)
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

//******************************************************************************
// DBへは短く簡潔にアクセスしたい。
// DB接続などの事前準備などを意識することなく、いつでも、どこからでも、安全にアクセスしたい。
// いろんな箇所で、$mdb2 = new StudyMdbWrapper()ということをしたくない。
// $mdb2を関数引数やメンバ変数保持などを行って、管理しなければいけない苦労を撤廃したい。
// $mdb2->find(xxxx, yyyy, zzzz) というように、毎々、冗長な不要引数をつけたくない。
//
// およびCoMedixの既存基本思想に沿い、以下とする。
//
// このPHPWebサイトは、マルチスレッドで動作していないため、
// 各所各所でクラスをnew()でインスタンス化などしなくとも、安全にスレッドセーフであるし、
// DBコネクションもひとつだけしか接続しない。
// つまり例えばDBセレクトカーソルでループしながら、別のコネクションでUPDATE、などということは行わない。
// DBコネクションはシングルトンひとつで十分。というか、よっぽどのことがなければ、複数DB接続は行わないように。
//
// セレクト結果はFETCHMODE_ASSOC１本で。FETCHMODE_ORDEREDは論外。FETCHMODE_OBJECTでやりたい場合は拡張改造してください。
// DB関連のエラーはすべてシステム停止。
//
// よって以下としてみる
//******************************************************************************
function _c2dbGetConnection($sql) {
	global $c2db;
	if ($c2db->c2Con) return $c2db->c2Con;
    c2Trace("[C2DB]","Connection取得・・・");
    $_c2Con =& MDB2::singleton(PG_DB_DSN);
    if (PEAR::isError($_c2Con)) {
        $GLOBALS["C2_ERROR_DB_CONNECTION"]=1;
        $emsg = c2dbMakeSqlErrorHtml($sql)."DB接続エラー：".c2dbDebugInfo($_c2Con).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    c2Trace("[C2DB]","Connection取得完了");
    $c2db->c2Con = $_c2Con;
    return $_c2Con;
}
function c2dbExecLogStart($autoExecLogCategoryName) {
    global $c2db;
    $c2db->autoExecLogCategoryName = $autoExecLogCategoryName;
}
function c2dbExecLogStop() {
    global $c2db;
    $c2db->autoExecLogCategoryName = "";
}
function c2dbDebugInfo($obj) {
    global $c2db;
    $err = $obj->getDebugInfo();
    $err2 = $err;
    $pos = strpos($err2, "Native message: ERROR:");
    if ($pos >=0) $err2 = substr($err2, $pos+22);
    $pos = strpos($err2, "\nLINE ");
    if ($pos >=0) $err2 = substr($err2, 0, $pos);
    $c2db->lastErrorMsg = $err2;
    return $err;
}
function c2dbRemainParams() {
    global $c2db;
    $c2db->isRemainParams = 1;
}
function c2dbGetLastErrorMsg() {
    global $c2db;
    return $c2db->lastErrorMsg;
}
function c2dbResetLastErrorMsg() {
    global $c2db;
    $c2db->lastErrorMsg = "";
}
function c2dbSetErrorReturn($bool) {
    global $c2db;
    $c2db->errorReturn = ($bool ? true : false);
}
function c2dbClearParams() {
    global $c2db;
    $c2db->spTypes = array();
    $c2db->spParams = array();
    $c2db->isRemainParams = 0;
}
function c2dbGetTableScheme($table_name) {
    $sql =
    " select pg_attribute.attname, pg_type.typname".
    " from pg_attribute, pg_type".
    " where pg_attribute.atttypid = pg_type.oid".
    " and ( pg_attribute.atttypid < 26 or pg_attribute.atttypid > 29)".
    " and attrelid in (".
    "     select pg_class.oid from pg_class, pg_namespace".
    "     where relname='".$table_name."' and pg_class.relnamespace=pg_namespace.oid".
    " )";
    $rows = c2dbGetRows($sql);
    $ret = array();
    foreach ($rows as $row) {
        $ret[$row["attname"]] = $row["typname"];
    }
    return $ret;
}

function c2dbStr($fieldValue, $fieldType = "text") {
    global $c2db;
    $fieldName = "SP".(count($c2db->spTypes) + 1);
    array_push($c2db->spTypes, $fieldType);
    if ($fieldValue=="") {
        if ($fieldType=="text" || $fieldType=="varchar") return "''";
    }
    $c2db->spParams[$fieldName] = $fieldValue;
    return ":".$fieldName;
}
function c2dbInt($fieldValue) {
    if ($fieldValue!="") $fieldValue = (int)@$fieldValue;
    return c2dbStr($fieldValue, "integer");
}
function c2dbBool($boolValue) {
    if ($boolValue=="f") return "false"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
    if ($boolValue) return "true"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
    return "false"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
}
function c2dbTime($fieldValue) {
    return c2dbStr($fieldValue, "timestamp");
}
function c2dbFloat($fieldValue) {
    return c2dbStr($fieldValue, "float");
}
function c2dbGetRows($sql, $getType="GetRows") {
    global $c2db;
    $rsql = c2dbSqlReverse($sql, array_keys($c2db->spParams), array_values($c2db->spParams), $c2db->spTypes);
    c2Trace("[C2DB]", array($getType."-->c2dbGetRows() SQL"=>$rsql));
    if ($sql=="") {
        $emsg = $getType."-->c2dbGetRows()に失敗しました。SQLが指定されていません： ".c2GetTrace("hs");
        print_r(c2GetTrace("hs"));
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    c2dbQueryDebug($sql, array_keys($c2db->spParams), array_values($c2db->spParams), $c2db->spTypes);
    // 接続
    $c2Con = _c2dbGetConnection($sql);
    // Prepare
    $statement = $c2Con->prepare($sql, $c2db->spTypes);
    $c2db->c2Con = $c2Con;
    if (PEAR::isError($statement)) {
        $emsg = c2dbMakeSqlErrorHtml($sql).$getType."-->c2dbGetRows()/mdb->perpare()に失敗しました： ".c2dbDebugInfo($statement).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    // Execute
    $query_result = $statement->execute($c2db->spParams);
    if (PEAR::isError($query_result)) {
        $emsg = c2dbMakeSqlErrorHtml($sql).$getType."-->c2dbGetRows()/mdb->execute()に失敗しました： ".c2dbDebugInfo($query_result).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    if ($getType=="GetOne") $fetch_result = $query_result->fetchOne(); // 先頭フィールド取得
    if ($getType=="GetTopRow") $fetch_result = $query_result->fetchRow(MDB2_FETCHMODE_ASSOC); // 先頭行取得
    if ($getType=="GetRows") $fetch_result = $query_result->fetchAll(MDB2_FETCHMODE_ASSOC); // 全行取得
    if ($getType=="GetRowsOrdered") $fetch_result = $query_result->fetchAll(MDB2_FETCHMODE_ORDERED); // 全行取得項目名なし
    if ($getType!="GetResult") $query_result->free();
    $statement->free();

    if (!$c2db->isRemainParams) {
        $c2db->spTypes = array();
        $c2db->spParams = array();
    }
    if ($getType!="GetResult") {
        return $fetch_result;
    }
    return $query_result;
}
function c2dbGetOne($sql) {
    return c2dbGetRows($sql, "GetOne");
}
function c2dbGetTopRow($sql) {
    return c2dbGetRows($sql, "GetTopRow");
}
function c2dbGetResult($sql) {
    return c2dbGetRows($sql, "GetResult");
}
function c2dbGetNext($cursor) {
    $row = $cursor->fetchRow(MDB2_FETCHMODE_ASSOC);
    if (PEAR::isError($row)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbGetNext()/mdb->fetchRow()に失敗しました： ".c2dbDebugInfo($row).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    if (!$row) {
        if ($cursor) $cursor->free();
    }
    return $row;
}
function c2dbExec($sql) {
    c2Trace("[C2DB]", "c2dbExec()開始");
    global $c2db;
    if ($sql=="") {
        $emsg = "c2dbExec()に失敗しました。SQLが指定されていません： ".c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }

    c2dbQueryDebug($sql, array_keys($c2db->spParams), array_values($c2db->spParams), $c2db->spTypes);

    $c2Con = _c2dbGetConnection($sql); // 接続
    // Prepare
    $statement = $c2Con->prepare($sql, $c2db->spTypes);
    $c2db->c2Con = $c2Con;
    if (PEAR::isError($statement)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbExec()/mdb->prepare(sql, types)に失敗しました： ".c2dbDebugInfo($statement).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }

    // Execute
    $query_result = $statement->execute($c2db->spParams);
    if (PEAR::isError($query_result)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbExec()/mdb->execute(params)に失敗しました： ".c2dbDebugInfo($query_result).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
    }
    $statement->free();
    if ($c2db->autoExecLogCategoryName) {
        $_sql = c2dbSql($sql);
        $_backtrace = c2GetBacktrace();

        $_statement = $c2Con->prepare("select nextval('c2_exec_sql_log_seq')", array());
	    $c2db->c2Con = $c2Con;
        if (PEAR::isError($_statement)) {
            $emsg = "c2dbExec()/mdb->perpare(sql)に失敗しました：c2_exec_sql_log_seqシーケンスが取得できません".c2dbDebugInfo($_statement).c2GetTrace("hs");
            if ($c2db->errorReturn) return $emsg;
            c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
        }
        // Execute
        $query_result = $_statement->execute(array());
        if (PEAR::isError($query_result)) {
            $emsg = "c2dbExec()/mdb->execute(params)に失敗しました：c2_exec_sql_log_seqシーケンスが取得できません".c2dbDebugInfo($query_result).c2GetTrace("hs");
            if ($c2db->errorReturn) return $emsg;
            c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
        }
        $nextval = $query_result->fetchOne();
        $s_nextval = "".$nextval;
        // １万行おきに、１００万行以前を消す
        if (strlen($s_nextval)>=7 && substr($s_nextval, -4)=="0000") {
            $del_seq_max = substr($s_nextval, 0, strlen($s_nextval)-4);
            $del_seq_max = ((int)$del_seq_max - 100) . "0000";
            $_statement = $c2Con->prepare("delete from c2_exec_sql_log where seq < ".$del_seq_max, array());
		    $c2db->c2Con = $c2Con;
            $query_result = $_statement->execute(array());
        }
        $log_sql =
        " insert into c2_exec_sql_log (".
        "     seq, category_name, sql, backtrace, create_emp_id, create_emp_nm, create_datetime".
        " ) values (".
        "     ".$nextval.", :category_name, :sql, :backtrace, :create_emp_id, :create_emp_nm, current_timestamp".
        " )";
        $_statement = $c2Con->prepare($log_sql, array("text", "text", "text", "text", "text"));
        if (PEAR::isError($_statement)) {
            // コーディングエラー。メッセージは簡単でよい。
            $emsg = "c2dbExec()/mdb->prepare(sql, types)に失敗しました（ 自動SQLログ保存エラー）".c2dbDebugInfo($_statement).c2GetTrace("hs");
            if ($c2db->errorReturn) return $emsg;
            c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $log_sql, __FILE__." ".__LINE__);
        }
        $query_result = $_statement->execute(
            array(
                "category_name"=>$c2db->autoExecLogCategoryName,
                "sql"=>ltrim($_sql),
                "backtrace"=>$_backtrace,
                "create_emp_id"=>C2_LOGIN_EMP_ID,
                "create_emp_nm"=>C2_LOGIN_EMP_LT_NM." ".C2_LOGIN_EMP_FT_NM
            )
        );
        if (PEAR::isError($query_result)) {
            $emsg =
                c2GetBacktrace()."\n\n" . "【SQL】\n".$sql."\n\n".
                "c2dbExec()/mdb->execute(params)に失敗しました：自動SQLログ保存に失敗しました： ".
                c2dbDebugInfo($query_result).c2GetTrace("hs");
            if ($c2db->errorReturn) return $emsg;
            c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, $sql, __FILE__." ".__LINE__);
        }
        $_statement->free();
    }
    if (!$c2db->isRemainParams) {
        $c2db->spTypes = array();
        $c2db->spParams = array();
    }
    return true;
}
function c2dbSql($sql){
    global $c2db;
    $keys = array();
    foreach ($c2db->spParams as $k=>$v) $keys[] = $k;
    for ($idx = count($keys)-1; $idx>=0; $idx--) {
        $k = $keys[$idx];
        $type = $c2db->spTypes[$idx];
        if ($type=="int" || $type=="integer") {
            $sql = str_replace(":".$k, (strlen($c2db->spParams[$k]) ? $c2db->spParams[$k] : "null"), $sql);
        }
        else {
            $sql = str_replace(":".$k, "'".$c2db->spParams[$k]."'", $sql);
        }
    }
    return $sql;
}
// 最新のシーケンスを取得する.
function c2dbGetNextSequence($sequence_name) {
    $c2Con = _c2dbGetConnection($sql); // 接続
    $seq = $c2Con->nextID($sequence_name);
    $c2db->c2Con = $c2Con;
    if (PEAR::isError($seq)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbGetNextSequence()/mdb->nextID(sequence_name)に失敗しました： ".c2dbDebugInfo($seq).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, "", __FILE__." ".__LINE__);
    }
    return $seq;
}

function c2dbBeginTrans() {
    global $c2db;
    c2Trace("[C2DB]", "トランザクション開始");
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->beginTransaction();
    $c2db->c2Con = $c2Con;
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクション開始エラー： ".c2dbDebugInfo($tran).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, "", __FILE__." ".__LINE__);
    }
}
function c2dbCommit() {
    c2Trace("[C2DB]", "コミット開始・・・");
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->commit();
    $c2db->c2Con = $c2Con;
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクションコミットエラー： ".c2dbDebugInfo($tran).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, "", __FILE__." ".__LINE__);
    }
    c2Trace("[C2DB]", "コミット");
}
function c2dbRollback() {
    c2Trace("[C2DB]", "ロールバック開始・・・");
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->rollback();
    $c2db->c2Con = $c2Con;
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクションロールバックエラー： ".c2dbDebugInfo($tran).c2GetTrace("hs");
        if ($c2db->errorReturn) return $emsg;
        c2LogSqlErrorAndDie(c2GetCallerFileName(""), $emsg, "", __FILE__." ".__LINE__);
    }
    c2Trace("[C2DB]", "ロールバック");
}
function c2dbSqlReverse($sql, $columns = NULL, $values, $spTypes) {
    for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
        $sql = str_replace(":".$columns[$i], "".var_export($values[$i], TRUE)."", $sql);
    }
    return $sql;
}

function c2dbQueryDebug($sql, $columns = NULL, $values, $spTypes) {
    global $SQLLOG;
    global $SQLLOGFILE;
    $dmode = (defined("C2_DEV_MODE") ? C2_DEV_MODE : "");
    if ($SQLLOG!="on" && !$dmode!="2") return;
    $sql2 = $sql;
    $sql3 = array();
    for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
        $sql3[] = $columns[$i]."|".var_export($values[$i], TRUE)."|".$spTypes[$i];
        $sql2 = str_replace(":".$columns[$i], "".var_export($values[$i], TRUE)."", $sql2);
    }
    if (!$fname) $fname = c2GetCallerFileName("");

    $msg = c2dbMakeSqlErrorHtml($sql)."\nPREPARE QUERY:".$sql."\nEXECUTE QUERY:".$sql2."\nPARAMS:".implode("", $sql3).c2GetTrace("hs");

    $fpath = "./log/atmodule_sql.log";
    if (is_file($fpath) && filesize($fpath) < 1024*1024) { // 1Mbまで
        @error_log("[".$fname."][".date("Y/m/d H:i:s")."] ".$msg."\n", 3, "./log/atmodule_sql.log");
    }
    if(($SQLLOG=="on" || $dmode=="2") && $SQLLOGFILE){
        $ret = error_log("[".$fname."][".date("Y/m/d H:i:s")."] ".$msg."\n\n", 3, $SQLLOGFILE);
    }
}


function c2dbMakeSqlErrorHtml($sql) {
    return c2GetBacktrace()."\n\n" . ($sql ? "【SQL】\n".c2dbSql($sql)."\n\n" : "");
}
function c2dbGetSystemConfig($key) {
    return c2dbGetOne("select value from system_config where key = ".c2dbStr($key));
}
function c2dbSetSystemConfig($key, $v, $no_trans=0) {
    if (!$no_trans) c2dbBeginTrans();
    c2dbExec("delete from system_config where key = ".c2dbStr($key));
    c2dbExec("insert into system_config (key, value) values (".c2dbStr($key).", ".c2dbStr($v).")");
    if (!$no_trans) c2dbCommit();
}


