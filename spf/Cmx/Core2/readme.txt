コア2の複製


リリースタイミングの都合上、
本来はclass/Cmx/Core2に存在するファイルを、
ここに複製して、spfの開発を行いました。

いずれまた、class/Cmx/Core2への一本化も可能ですが、
ずっと分離状態のままでよいと思います。


SPFは、他フォルダへの依存が極めて少ないアプリです。
/comedix/jsも、/comedix/cssも、/comedix/imgも利用していません。

spfフォルダ以外では、
conf/conf.inf
conf/conf_define.inf

および、SQEでtinymceを利用
tinymce/jscripts/tiny_mce/tiny_mce.js


のみになっています。
このため、ソース的に独立して利用することが容易、というメリットがあります。


binフォルダは、
C言語プログラム「cmx_crypt」等で利用予定でしたが、最後の最後で廃止としました。
「C2Utils.php」の「c2MakeCmxCrypt()」です。
binフォルダ自体、現状で完全削除可能となっていますが、残しています。



