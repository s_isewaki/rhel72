<?
//****************************************************************************************************************
// 職員選択ダイアログ
//****************************************************************************************************************
function html_dlg_emp_selector() {
    $callback_func = $_REQUEST["callback_func"];
    $is_workflow = $_REQUEST["is_workflow"];
    $use_buttons = explode("_", $_REQUEST["use_buttons"]);
    $emp_ids = explode("\t", $_REQUEST["emp_ids"]);
    if (!$_REQUEST["emp_ids"]) $emp_ids = array();

    if (!$callback_func) { echo "不正なアクセスです。(1g)"; die; }

    $sql = "select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_nm from empmst";
    if (count($emp_ids) < 500) {
        $sql .= " where emp_id in ('".implode("','", $emp_ids)."')";
    }
    $sql .= " order by emp_lt_nm, emp_ft_nm";
    $rows = c2dbGetRows($sql);
    foreach ($rows as $row) {
        $empRows[$row["emp_id"]] = $row["emp_nm"];
    }

    $empmstRows = array();
    foreach ($emp_ids as $emp_id_or_syozoku) {
        $empmstRows[] = array("emp_id_or_syozoku" => $emp_id_or_syozoku, "emp_nm"=>$empRows[$emp_id_or_syozoku]);
    }
    $iinkaiRows = getMasterHash("spfm_iinkai", 1);
    $stmstRows = getMasterHash("stmst", 1);
    $jobmstRows = getMasterHash("jobmst", 1);
    $classmstRows = getMasterHash("classmst", 1);
//    $atrbmstRows = getMasterHash("atrbmst", 1);
//    $deptmstRows = getMasterHash("deptmst", 1);
//    $classroomRows = getMasterHash("classroom", 1);
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        var gEmpList = [];
        function tryDialogSearch(elem, next_page_num) {
            if (elem.id=="edlg_class_id") changeDlgSyozoku("atrb_id", getComboValue(elem), ["edlg_dept_id","edlg_room_id"]);
            if (elem.id=="edlg_atrb_id") changeDlgSyozoku("dept_id", getComboValue(elem), ["edlg_room_id"]);
            if (elem.id=="edlg_dept_id") changeDlgSyozoku("room_id", getComboValue(elem));
            var focusId = elem.id;
            var page_size = int(ee("edlg_page_size").value);
            if (page_size<5)    { page_size = 5; ee("edlg_page_size").value = page_size; }
            if (page_size>1000) { page_size = 1000; ee("edlg_page_size").value = page_size; }
            var show_emp_personal_id = (ee("edlg_show_emp_personal_id").checked ? "1" : "");
            var show_emp_retire = (ee("edlg_show_emp_retire").checked ? "1" : "");
            var show_emp_del_flg = (ee("edlg_show_emp_del_flg").checked ? "1" : "");
            var show_kenmu = (ee("edlg_show_kenmu").checked ? "1" : "");
            var params =
            "&page_num="+next_page_num+"&page_size="+page_size+"&show_emp_personal_id="+show_emp_personal_id+
            "&show_emp_retire="+show_emp_retire+"&show_emp_del_flg="+show_emp_del_flg+"&show_kenmu="+show_kenmu;
            $.ajax({ url:"common.php?ajax_action=dlg_emp_selector_search"+params,
                data:serializeUnicodeEntity("edlg_search_joken"), success:function(msg) {
                var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
                ee("edlg_search_result").innerHTML = msg;
                activateJavascript("edlg_search_result");
				adjustEmpSelectorSize();
                tryFocus(focusId);
                setRowColor();
            }});
        }
        function selEmp(aTag) {
            var emp_id = aTag.getAttribute("emp_id");
            var emp_nm = aTag.innerHTML;
            for (var idx=0; idx<gEmpList.length; idx++) if (gEmpList[idx].emp_id==emp_id) return;
            $("#edlg_div_scroll").append('<a href="javascript:void(0)" onclick="delEmp(this)" emp_id="'+emp_id+'" class="block">'+emp_nm+'</a>');

            $(ee("div_"+emp_id)).append('<img class="absolute" src="./img/petit6r.gif" name="petit_'+emp_id+'" style="left:-8px; top:4px" alt="" />');
            createEmpList();
        }
        function selHonnin() {
            for (var idx=0; idx<gEmpList.length; idx++) if (gEmpList[idx].emp_id=="#self") return;
            $("#edlg_div_scroll").append('<a href="javascript:void(0)" onclick="delEmp(this)" emp_id="#self" class="block"><span class="id_syozoku">本人</span></a>');
            createEmpList();
        }
        function selHonninJoseki() {
            var cmbSt = ee("edlg_st_id");

            var id = getComboValue(cmbSt);
            if (!id) return alert("役職を指定してください。");
            var nm = cmbSt.options[cmbSt.selectedIndex].text;
            var ids = "#self_st#"+getComboValue(cmbSt);
            for (var idx=0; idx<gEmpList.length; idx++) if (gEmpList[idx].emp_id==ids) return;
            $("#edlg_div_scroll").append('<a href="javascript:void(0)" onclick="delEmp(this)" emp_id="'+ids+'" class="block"><span class="id_syozoku">本人所属の'+nm+'</span></a>');
            createEmpList();
        }
        function selSyozokuYakusyoku() {
            var cmbClass = ee("edlg_class_id");
            var cmbAtrb = ee("edlg_atrb_id");
            var cmbDept = ee("edlg_dept_id");
            var cmbRoom = ee("edlg_room_id");
            var cmbSt = ee("edlg_st_id");

            var id = getComboValue(cmbClass);
            if (!id) return alert("部署を指定してください。");
            var nm = cmbClass.options[cmbClass.selectedIndex].text;
            if (getComboValue(cmbAtrb)) nm += "＞" + cmbAtrb.options[cmbAtrb.selectedIndex].text;
            if (getComboValue(cmbDept)) nm += "＞" + cmbDept.options[cmbDept.selectedIndex].text;
            if (getComboValue(cmbRoom)) nm += "＞" + cmbRoom.options[cmbRoom.selectedIndex].text;
            if (getComboValue(cmbSt))   nm += "の" +   cmbSt.options[cmbSt.selectedIndex].text;
            var cid = getComboValue(cmbClass);
            var aid = getComboValue(cmbAtrb);
            var did = getComboValue(cmbDept);
            var rid = getComboValue(cmbRoom);
            var st  = getComboValue(cmbSt);
            var cadr_st = "#"+cid;
            if (aid) { cadr_st += "-" + aid; if (did) { cadr_st += "-" + did; if (rid) { cadr_st += "-" + rid; }}}
            cadr_st += "#"+st;

			$("div.item_div").each(function(){
				if (cid && cid!=this.getAttribute("cid")) return;
				if (aid && aid!=this.getAttribute("aid")) return;
				if (did && did!=this.getAttribute("did")) return;
				if (rid && rid!=this.getAttribute("rid")) return;
				if (st  && st!=this.getAttribute("st")) return;
				$(this).append('<img class="absolute" src="./img/petit6g.gif" style="left:-8px; top:11px" name="petit_'+cadr_st+'" alt="" />');
			});

            for (var idx=0; idx<gEmpList.length; idx++) if (gEmpList[idx].emp_id==cadr_st) return;
            $("#edlg_div_scroll").append('<a href="javascript:void(0)" onclick="delEmp(this)" emp_id="'+cadr_st+'" class="block"><span class="id_syozoku">'+nm+'</span></a>');
            createEmpList();
        }
        function delEmp(aTag) {
			if (!confirm("この職員をリストから除外します。")) return;
			var empIdOrSyozoku = aTag.getAttribute("emp_id");
			$("#emp_selector_scroller img").each(function(){
				if (this.getAttribute("name") == "petit_"+empIdOrSyozoku) this.parentNode.removeChild(this);
			});
            aTag.parentNode.removeChild(aTag);
            createEmpList();
        }
        function createEmpList() {
            gEmpList = [];
            var emp_ids = [];
            $("#edlg_div_scroll a").each(function(){
            	if (!this.innerHTML) return;
            	emp_ids.push(this.getAttribute("emp_id"));
                gEmpList.push({emp_id:this.getAttribute("emp_id"), emp_nm:this.innerHTML});
            });
            ee("emp_ids").value = emp_ids.join("\t");
        }
        function settle() {
            <?=$callback_func?>(gEmpList, <?=c2ToJson($_REQUEST)?>);
        }
        function dialogOpenEvent() {
            tryFocus("edlg_emp_nm");
        }
        function changeDlgSyozoku(destId, parentValue, clearIds) {
            if (clearIds) {
                for (var idx=0; idx<clearIds.length; idx++) {
                    var cmb = ee(clearIds[idx]);
                    if (cmb.options) cmb.options.length = 1;
                }
            }
            var cmb = ee("edlg_"+destId);
            cmb.options.length = 1;
            var list = syozoku_data[destId][parentValue];
            if (!list) return;
            cmb.options.length = list.length + 1;
            for (var idx=1; idx<=list.length; idx++) {
                cmb.options[idx] = new Option(list[idx-1]["ssnm"], list[idx-1]["ssid"]);
            }
        }
        function adjustEmpSelectorSize() {
			var hh= ee("edlg_search_joken").offsetHeight;
			ee("emp_selector_scroller").style.height = (490 - 40 - hh) + "px";

		}
		function setRowColor() {
		}
		adjustEmpSelectorSize();
        createEmpList();
        tryFocus("edlg_emp_nm");
        setRowColor();
    </textarea>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div class="relative" style="height:490px; width:930px; overflow:hidden; padding-top:5px">
        <div class="absolute" style="width:170px; height:100%">
            <table cellspacing="0" cellpadding="0" style="width:170px"><tr>
            <td class="border0 bottom" style="padding-bottom:3px"><b>■ 選択中職員</b></td>
            <td class="border0 right" style="width:74px; padding-bottom:3px">
                <div style="border:2px solid #dd4444"><button type="button" onclick="settle()" class="width100">確定</button></div></td>
            </tr></table>
            <table cellspacing="0" cellpadding="0" style="width:168px"><tr><td style="padding4px; border:1px solid #777777">
            <div style="width:160px; height:450px; overflow-y:scroll; background-color:#ffffff; padding:4px" id="edlg_div_scroll"
            navi="クリックすると職員を除外します。"
            >
            <? foreach ($empmstRows as $row) { ?>
                <?
                    if (!in_array($row["emp_id_or_syozoku"], $emp_ids)) continue;
                    $nm = hh($row["emp_nm"]);
                    if ($row["emp_id_or_syozoku"]=="#self") $nm = '<span class="id_syozoku">本人</span>';
                    else if (substr($row["emp_id_or_syozoku"],0,8)=="#self_st") {
						$nm = hh(getAuthYakusyokuNm($row["emp_id_or_syozoku"]));
						if ($nm) $nm = '<span class="id_syozoku">'.$nm.'</span>';
					}
                    else if (!$nm) {
						$nm = hh(getSyozokuYakusyokuNm($row["emp_id_or_syozoku"]));
						if ($nm) $nm = '<span class="id_syozoku">'.$nm."</span>";
					}
                ?>
                <a href="javascript:void(0)" onclick="delEmp(this)" emp_id="<?=hh($row["emp_id_or_syozoku"])?>" class="block"><?=$nm?></a>
            <? } ?>
            </div>
            </td></tr></table>
        </div>
        <div class="absolute" style="width:740px; left:190px">
                <div id="edlg_search_joken" class="finder_box" style="width:740px">
                	<input type="hidden" name="emp_ids" id="emp_ids" value="<?=hh($_REQUEST["emp_ids"])?>" />
            <table cellspacing="0" cellpadding="0" class="width100"><tr><td class="border0 gray">
                    <div navi="職員名または職員IDを部分一致検索します。"><nobr>
                    	職員
                    	<input type="text" class="text w100" id="edlg_emp_nm" name="edlg_emp_nm"
                    		onkeydown="if(isEnterKey(this,event)) tryDialogSearch(this,1)" onchange="tryDialogSearch(this,1)" />
                    </nobr></div>
                    <!--div><nobr>委員会&nbsp;<?=make_select_tag("edlg_iinkai_id", "", $iinkaiRows, "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    </nobr></div -->
                    <div><nobr>役職&nbsp;<?=make_select_tag("edlg_st_id", "150", $stmstRows, "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    </nobr></div>
                    <div><nobr>職種&nbsp;<?=make_select_tag("edlg_job_id", "150", $jobmstRows, "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    </nobr></div>
                    <div><nobr>所属部署
                    <?=make_select_tag("edlg_class_id", "120", $classmstRows, "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    <?=make_select_tag("edlg_atrb_id", "120", array(), "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    <?=make_select_tag("edlg_dept_id", "120", array(), "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    <span class="area4">
                    <?=make_select_tag("edlg_room_id", "120", array(), "", 1, 'onchange="tryDialogSearch(this,1)"', "", "", "", 1); ?>
                    </span>
                    </nobr></div>
                    <div class="clearfix"></div>
            </td>
            <td class="border0 gray" style="width:100px; padding-bottom:3px">
                    <? if ($is_workflow || in_array("self", $use_buttons)) { ?>
                    <div class="right" style="padding:0 3px 0 0; height:22px">
                    	<button type="button" onclick="selHonnin()" class="w130">本人追加</button>
                    </div>
                    <? } ?>
                    <? if ($is_workflow || in_array("branch", $use_buttons)) { ?>
                    <div class="right" style="padding:0 3px 0 0; height:22px">
                    	<button type="button" onclick="selSyozokuYakusyoku()" class="w130">部署/役職として追加</button>
                    </div>
                    <? } ?>
                    <? if ($is_workflow || in_array("supervisor", $use_buttons)) { ?>
                    <div class="right" style="padding:0 3px 0 0; height:22px" navititle="本人上席を追加"
                    	navi="本人の所属階層と同一の職員のうち、指定した役職者を追加できます。<br>本人の所属階層を遡って確認しません。">
                    	<button type="button" onclick="selHonninJoseki()" class="w130">本人上席を追加</button>
                    </div>
                    <? } ?>
                    <div class="right font12" style="padding:2px 3px 0 0; height:22px"><nobr>
			            <input type="text" class="text w30 imeoff" value="15" id="edlg_page_size" maxlength="4"
			                onkeydown="if(isEnterKey(this,event)) tryDialogSearch(this,1)" /> 件ずつ
                    	<button type="button" onclick="tryDialogSearch(this,1)" id="edlg_search_btn" class="w50">検索</button>
                    </nobr></div>
                    <div class="clearfix"></div>
            </td></tr>
            <tr><td colspan="2">
            </td></tr>
            </table>
                </div>
                <div id="edlg_search_result"><?=dlg_emp_selector_search()?></div>
        </div>
    </div>
<?
}
//================================================================================================================
// 職員選択ダイアログ 検索処理
//================================================================================================================
function dlg_emp_selector_search() {
    $req = $_REQUEST;

    $_tmp = explode("\t", $_REQUEST["emp_ids"]);
    $mm_emp_ids = array(); // マッチさせたいemp_idリスト
    $mm_cadr_st = array(); // マッチさせたい class-atrb-dept-roomコードリスト
    foreach ($_tmp as $es) {
		if (!$es) continue; // カラ配列。無視。
		if (substr($es,0,5)=="#self") continue; // 本人または本人の上席。この画面ではそれが誰かはわからない。飛ばす」
		if (substr($es,0,1)!="#") { $mm_emp_ids[$es]=1; continue; } // 先頭が#では無い ⇒ emp_idである
		// 以降、先頭が#である⇒cadr#stコード。（先頭の#を除去して、さらに#で分割すると、cadrとstに分かれる）
		$mm_cadr_st[]= explode("#", substr($es,1));
	}

    $main_table = "empmst em";
    $alias = "em";
    $cc_field = "";
    $kenmu_join = "";
	if ($req["show_kenmu"]) {
		$main_table =" concurrent_view2 em inner join empmst em2 on ( em2.emp_id = em.emp_id )";
		$alias = "em2";
		$cc_field = ",em.is_concurrent";
//	    $order_by = $alias.".emp_lt_nm, ".$alias.".emp_ft_nm, em.is_concurrent, em.emp_class, em.emp_attribute, em.emp_dept, em.emp_room";
	}
    $order_by = "em.emp_class, em.emp_attribute, em.emp_dept, em.emp_room, ".$alias.".emp_lt_nm, ".$alias.".emp_ft_nm";

    $count_sql = "select count(em.emp_id) from ".$main_table." left outer join authmst au on (em.emp_id = au.emp_id) where 1 = 1";

    $page_sql =
    " select".
    " em.emp_id, ".$alias.".emp_personal_id, ".$alias.".emp_ft_nm, ".$alias.".emp_lt_nm".
    ",em.emp_class, em.emp_attribute, em.emp_dept, em.emp_room, em.emp_st, em.emp_job".
    ",cm.class_nm".
    ",am.atrb_nm".
    ",dm.dept_nm".
    ",cr.room_nm".
    ",stmst.st_nm".
    ",jobmst.job_nm".
    $cc_field.
    ",".$alias.".emp_retire".
    ",case when au.emp_del_flg = 'f' then '' else '1' end as emp_del_flg".
    " from ".$main_table.
    " left outer join classmst cm on (em.emp_class = cm.class_id AND cm.class_del_flg = false)".
    " left outer join atrbmst am on (em.emp_attribute = am.atrb_id AND am.atrb_del_flg = false)".
    " left outer join deptmst dm on (em.emp_dept = dm.dept_id AND dm.dept_del_flg = false)".
    " left outer join classroom cr on (em.emp_room = cr.room_id AND cr.room_del_flg = false)".
    " left outer join stmst on (em.emp_st = stmst.st_id AND stmst.st_del_flg = false)".
    " left outer join jobmst on (em.emp_job = jobmst.job_id AND jobmst.job_del_flg = false)".
    " left outer join authmst au on (au.emp_id = em.emp_id)".
    " where 1 = 1";

    //--------------------
    // 画面指定された検索条件
    //--------------------
    $where = "";
    if (!$req["show_emp_del_flg"]) $where .= " and au.emp_del_flg = 'f'";
    if (!$req["show_emp_retire"]) $where .= " and ( ".$alias.".emp_retire is null or ".$alias.".emp_retire = '' or ".$alias.".emp_retire >= '".date("Ymd")."')";
    if ($req["edlg_iinkai_id"]) {
		$where .= " and em.emp_id in (select emp_id from spfm_iinkai_emp_list where iinkai_id = ".(int)$req["edlg_iinkai_id"].")";
	}

	if ($req["show_kenmu"]) {
	    if ($req["edlg_class_id"]) $where .= " and em.emp_class = ".(int)$req["edlg_class_id"];
	    if ($req["edlg_atrb_id"])  $where .= " and em.emp_attribute = ".(int)$req["edlg_atrb_id"];
	    if ($req["edlg_dept_id"])  $where .= " and em.emp_dept = ".(int)$req["edlg_dept_id"];
	    if ($req["edlg_room_id"])  $where .= " and em.emp_room = ".(int)$req["edlg_room_id"];
	    if ($req["edlg_st_id"])    $where .= " and em.emp_st = ".(int)$req["edlg_st_id"];
	    if ($req["edlg_job_id"])   $where .= " and em.emp_job = ".(int)$req["edlg_job_id"];
	}
	else {
		$_sel = "select emp_id from concurrent_view2 cv";
	    if ($req["edlg_class_id"]) $where .= " and em.emp_id in ( ".$_sel." where cv.emp_class = ".(int)$req["edlg_class_id"].")";
	    if ($req["edlg_atrb_id"])  $where .= " and em.emp_id in ( ".$_sel." where cv.emp_attribute = ".(int)$req["edlg_atrb_id"].")";
	    if ($req["edlg_dept_id"])  $where .= " and em.emp_id in ( ".$_sel." where cv.emp_dept = ".(int)$req["edlg_dept_id"].")";
	    if ($req["edlg_room_id"])  $where .= " and em.emp_id in ( ".$_sel." where cv.emp_room = ".(int)$req["edlg_room_id"].")";
	    if ($req["edlg_st_id"])    $where .= " and em.emp_id in ( ".$_sel." where cv.emp_st = ".(int)$req["edlg_st_id"].")";
	    if ($req["edlg_job_id"])   $where .= " and em.emp_id in ( ".$_sel." where cv.emp_job = ".(int)$req["edlg_job_id"].")";
	}

    if ($req["edlg_emp_nm"]) {
        $names = explode(" ", str_replace("　", " ", $req["edlg_emp_nm"]));
        $tmp = array();
        foreach ($names as $nm) {
            if (!strlen($nm)) continue;
            $tmp[]=
            "(".
            " ".$alias.".emp_ft_nm ilike ".c2dbStr("%".str_replace("%","'",$nm)."%").
            " or ".$alias.".emp_lt_nm ilike ".c2dbStr("%".str_replace("%","'",$nm)."%").
            " or ".$alias.".emp_kn_ft_nm ilike ".c2dbStr("%".str_replace("%","'",$nm)."%").
            " or ".$alias.".emp_kn_lt_nm ilike ".c2dbStr("%".str_replace("%","'",$nm)."%").
            " or ".$alias.".emp_personal_id ilike ".c2dbStr("%".str_replace("%","'",$nm)."%").
            ")";
        }
        if (count($tmp)) $where .= " and ".implode(" and ", $tmp);
    }

    // 該当件数取得SQL
    c2dbRemainParams();
    $ttl_row_count = c2dbGetOne($count_sql.$where);

    // ページング
    $page_size = (int)$_REQUEST["page_size"];
    $page_num = (int)$_REQUEST["page_num"];
    if (!$page_size) $page_size = 15;

    $last_page_num = ceil($ttl_row_count / $page_size); // 全ページ数
    if ($page_num > $last_page_num) $page_num = $last_page_num; // 指定ページ数が全ページ数より大きい場合は歳後のページにする
    if ($page_num < 1) $page_num = 1; // ページは１ページ以上とする
    $offset_idx = $page_size * ($page_num - 1); // 指定ページの最初のレコード番号

    // ページデータ取得SQL
    $page_sql = $page_sql.$where." order by ".$order_by." offset ".$offset_idx." limit ".$page_size;

    $empmstRows = c2dbGetRows($page_sql);
    c2dbClearParams();
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // ページャ                                                                                ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div style="padding:5px 0 3px 0">
        <table cellspacing="0" cellpadding="0" class="width100"><tr>
            <td class="left rec_count"><nobr>該当<span><?=$ttl_row_count?></span>件</nobr></td>
            <td class="middle" style="color:#444444">
            	<label navititle="職員ID" navi="チェックすると、職員IDを表示するようにします。"><input type="checkbox" id="edlg_show_emp_personal_id" onclick="tryDialogSearch(this,1)"
            	<?=($req["show_emp_personal_id"]?" checked":"")?> />職員ID</label>
            	<label navititle="退職" navi="退職した職員も検索に含めます。チェックを行わないと昨日までに退職した職員は含みません。<br>一覧表示では、退職日が登録されている職員に「退職」が表示されます。退職日が未来であっても「退職」が表示されます。<br>「退職」表示にマウスを近づけると、退職日が表示されます。"><input type="checkbox" id="edlg_show_emp_retire" onclick="tryDialogSearch(this,1)"
            	<?=($req["show_emp_retire"]?" checked":"")?> />退職</label>
            	<label navititle="利用停止" navi="利用停止が行われている職員も検索に含めます。チェックを行わないと利用停止職員は含みません。<br>一覧表示では、利用停止に設定されている職員に「停止」が表示されます。"><input type="checkbox" id="edlg_show_emp_del_flg" onclick="tryDialogSearch(this,1)"
            	<?=($req["show_emp_del_flg"]?" checked":"")?> />利用停止</label>
            	<label navititle="兼務" navi="兼務を表示します。<br>兼務は独立した行として表示されます。"><input type="checkbox" id="edlg_show_kenmu" onclick="tryDialogSearch(this,1)"
            	<?=($req["show_kenmu"]?" checked":"")?> />兼務</label>
            </td>
            <td class="right">
                <? outputPagerPartHtml("EmpSelectorDialog", "", $page_num, $last_page_num); ?>
            </td>
        </tr></table>
    </div>
    <div style="overflow-y:scroll; padding:1px 0 0 1px" id="emp_selector_scroller">
<?
    if (count($empmstRows)) {
        echo '<table cellspacing="0" cellpadding="0" class="hover_colord list_table width100"><tr>';
        echo '<tr class="pale">';
        echo '<th>No.</th><th width="auto"><nobr>職員</nobr></th>';
        echo '<th width="auto"><nobr>所属部署</nobr></th><th>役職</th><th><nobr>職種</nobr></th>';
        if ($req["show_emp_retire"]) {
            echo '<th><nobr>退職</nobr></th>';
        }
        if ($req["show_emp_del_flg"]) {
            echo '<th><nobr>停止</nobr></th>';
        }
        echo '</tr>';

		$rownum = $offset_idx;
        foreach ($empmstRows as $row) {
			$rownum++;

			$cadr = $row["emp_class"]."-".$row["emp_attribute"]."-".$row["emp_dept"]."-".$row["emp_room"]."-"; // ハイフン多目に

            echo '<tr>';
            echo '<td style="padding:0 2px 0 8px" title="職員管理ID='.hh($row["emp_id"]).'">';

            echo '<div class="relative item_div" style="padding-top:1px;" id="div_'.$row["emp_id"].'"';
            if ($row["emp_class"]) echo ' cid="'.$row["emp_class"].'"';
            if ($row["emp_attribute"]) echo ' aid="'.$row["emp_attribute"].'"';
            if ($row["emp_dept"]) echo ' did="'.$row["emp_dept"].'"';
            if ($row["emp_room"]) echo ' rid="'.$row["emp_room"].'"';
            if ($row["emp_st"]) echo ' st="'.$row["emp_st"].'"';
            echo '>';
            echo '<div class="relative">'.$rownum.'</div>';

			$match_emp_id = "";
			$match_syozoku = "";
			if (count($mm_emp_ids) && $mm_emp_ids[$row["emp_id"]]) {
	            echo '<img class="absolute" src="./img/petit6r.gif" name="petit_'.$row["emp_id"].'" style="left:-8px; top:4px" alt="" />';
			}
			if (count($mm_cadr_st)) {
				$cadr = $row["emp_class"]."-".$row["emp_attribute"]."-".$row["emp_dept"]."-".$row["emp_room"]."-"; // ハイフン多目に
				foreach ($mm_cadr_st as $cadr_st) {
					if (!$cadr_st[1] || $cadr_st[1]==$row["emp_st"]) { // 役職定義が無いか、マッチ
						if (strpos($cadr, $cadr_st[0]."-")===0) {
							// さらに所属部署マッチ⇒マーク追加
				            echo '<img class="absolute" src="./img/petit6g.gif" style="left:-8px; top:11px" name="petit_#'.$cadr_st[0]."#".$cadr_st[1].'" alt="" />';
				        }
					}
				}
			}
            echo '</div></td>';
            echo '<td>';
            if ($req["show_emp_personal_id"]) {
            echo '<span class="gray font11">'.hh($row["emp_personal_id"]).'</span><br>';
            }
            echo '<a href="javascript:void(0)" onclick="selEmp(this)" class="block" navi="クリックすると「選択中職員」一覧に追加します。"';
            echo ' emp_id="'.hh($row["emp_id"]).'">';
            if ($row["is_concurrent"]) {
				echo '<span style="padding-right:2px"><span style="background-color:#cc6600; color:#ffffff; padding:0 1px">兼</span></span>';
			}
            echo '<nobr>'.hh($row["emp_lt_nm"])."</nobr>&nbsp;<nobr>".hh($row["emp_ft_nm"]).'</nobr></a></nobr>';
            echo '</td>';
            echo '<td>'.str_replace("\t", " ＞ ", rtrim($row["class_nm"]."\t".$row["atrb_nm"]."\t".$row["dept_nm"]."\t".$row["room_nm"])).'</td>';
            echo '<td class="center">'.hh($row["st_nm"]).'</td>';
            echo '<td class="center">'.hh($row["job_nm"]).'</td>';
            if ($req["show_emp_retire"]) {
				if ($row["emp_retire"]) {
		            echo '<td class="center red" navi="退職日：'.c2ConvYmd($row["emp_retire"],"slash_ymd").'">退職</td>';
				} else {
		            echo '<td class="center"></td>';
				}
            }
	        if ($req["show_emp_del_flg"]) {
				if ($row["emp_del_flg"]) {
		            echo '<td class="center red" navi="利用停止中職員">停止</td>';
				} else {
		            echo '<td class="center"></td>';
				}
	        }
            echo '</tr>';
        }
        echo '</table></div>';
    }
    echo '</div>';
    die;
}