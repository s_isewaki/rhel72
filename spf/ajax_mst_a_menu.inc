<?

//****************************************************************************************************************
// メニューマスタ
//****************************************************************************************************************
function html_mst_menu() {
	global $preserved_muids;
    $menuRows = c2dbGetRows("select * from spfm_menu order by menu_order");
    $_rows = c2dbGetRows("select muid, count(muid) as cnt from spfm_sect_layout group by muid");
    $sectItems = array();
    foreach ($_rows as $row) $sectItems[$row["muid"]] = $row["cnt"];
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        $("#mst_menu_container").sortable({revert:true, update:function(event, ui){
            setOrder();
         }});
        function setOrder() {
            var idOrder = [];
            $("#mst_menu_container div.menu_box").each(function(){
                idOrder.push(this.getAttribute("menu_key"));
            });
            ee("menu_order_list").value = idOrder.join(",");
        }
        function activeChanged(checked, muid, prefix) {
			ee(prefix+'_blank').style.display = (checked? 'none' : '');
			ee(prefix+'_using').style.display = (checked? '' : 'none');
			ee("sect_msg_on"+muid).style.display = (checked? '' : 'none');
			ee("sect_msg_off"+muid).style.display = (checked? 'none' : '');
		}
        setOrder();
    </textarea>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
    <table cellspacing="0" cellpadding="0" class="section_title"><tr>
        <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">A</th><? } ?>
        <th class="section_title_title" style="width:200px"><nobr>メニュー構成</nobr></th>
        <th class="left font12" style="letter-spacing:0">
        	<? $behavior = c2dbGetSystemConfig("spf.DaiMenuBehavior"); ?>
        	メニューフォルダ制御
        	<select name="spfDaiMenuBehavior">
        		<option value="">ユーザ画面では最初は閉じた状態</option>
        		<option value="expanded"<?=($behavior=="expanded" ? ' selected' : '')?>>ユーザ画面では最初は開いた状態</option>
        		<option value="hidden"<?=($behavior=="hidden" ? ' selected' : '')?>>ユーザ画面では隠す</option>
        	</select>
        </th>
        <th class="button"><nobr><button type="button" onclick="tryAdminReg('right_area', 'reg_mst_menu')" class="w70">保存</button></nobr></th>
        <!-- td class="helplink"><nobr><a href="javascript:void(0)" onclick="openAdminHelp('A');">ﾍﾙﾌﾟ</a></nobr></td -->
    </tr></table>
    <div style="padding-bottom:4px">
        <table cellspacing="0" cellpadding="0"><tr>
            <td style="width:340px"></td>
            <td class="w50 font12 gray center">本人</td>
            <td class="w50 font12 gray center">監督者</td>
            <td class="w50 font12 gray center">承認者</td>
            <td class="w60 font12 gray center">人事</td>
        </tr></table>
    </div>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="mst_menu_container">
        <input type="hidden" name="menu_order_list" id="menu_order_list" />
    <?
        foreach ($menuRows as $row) {
            $prefix = "menu@".$row["menu_hier"]."@".$row["muid"];
            $dcs = "メニューフォルダ";
            $bgclr = "4a7bac";
            $preserved_nm = @$preserved_muids[$row["muid"]]["caption"];
            if ($row["menu_hier"]=="chu") {
				$dcs = "一般機能"; $bgclr = "888888";
				if ($preserved_nm) { $dcs = "固定機能"; $bgclr = "5f9d31"; }
			}
            if ($row["menu_hier"]=="syo") {
				$dcs = "特殊機能"; $bgclr = "888888";
				if ($preserved_nm) { $dcs = "特殊固定機能"; $bgclr = "5f9d31"; }
			}
			if ($preserved_nm) $preserved_nm = "このメニューはシステム固定で「".$preserved_nm."」として利用されます。";
    ?>
        <div class="menu_box menu_box_<?=$row["menu_hier"]?>" menu_key="<?=$prefix?>" id="<?=$prefix?>"
            style="margin-top:4px; width:870px">
	        <table cellspacing="0" cellpadding="0" class="width100"><tr>
	        <td style="padding:3px; cursor:move; background:url(img/bk_line.gif) repeat-x">
		        <table cellspacing="0" cellpadding="0" class="width100"><tr>
		            <td width="26" class="middle" style="padding:0 5px"><input type="checkbox" name="<?=$prefix?>@is_active" value="1"<?=($row["is_active"]?" checked":"")?>
		                onclick="activeChanged(this.checked, '<?=$row["muid"]?>', '<?=$prefix?>');"
		                navi="利用する場合はチェックしてください。"
		             /></td>
		            <td style="background-color:#<?=$bgclr?>; color:#ffffff; padding:3px 5px; width:120px"
		            	navi="muid=<?=$row["muid"]?>" navi="<?=$preserved_nm?>"><nobr><?=$dcs?></nobr></td>
		            <td width="50" navi="メニュー名" style="padding-left:5px"><input type="text" class="text" name="<?=$prefix?>@menu_label" value="<?=hh($row["menu_label"])?>" /></td>
		            <td width="50"><select name="<?=$prefix?>@is_usr_display" navi="本人表示">
		                <option value=""></option>
		                <option value="1"<?=($row["is_usr_display"]=="1"?" selected":"")?>>表示</option>
		            </select></td>
		            <td width="50"><select name="<?=$prefix?>@is_kan_display" navi="監督者表示">
		                <option value=""></option>
		                <option value="1"<?=($row["is_kan_display"]=="1"?" selected":"")?>>表示</option>
		            </select></td>
		            <td width="50"><select name="<?=$prefix?>@is_syo_display" navi="承認者（評価者/確認者）表示">
		                <option value=""></option>
		                <option value="1"<?=($row["is_syo_display"]=="1"?" selected":"")?>>表示</option>
		            </select></td>
		            <td width="50"><select name="<?=$prefix?>@is_jin_display" navi="人事（人事部）表示">
		                <option value=""></option>
		                <option value="1"<?=($row["is_jin_display"]=="1"?" selected":"")?>>表示</option>
		            </select></td>

		            <td style="line-height:20px; width:auto"><nobr>
		                <span id="<?=$prefix?>_blank" style="padding-left:4px; color:red;<?=($row["is_active"]?"display:none":"")?>">(空き)</span>
		                <span id="<?=$prefix?>_using" style="padding-left:4px; color:#00aa00;<?=($row["is_active"]?"":"display:none")?>">(利用中)</span>
		            </nobr></td>
		            <td style="line-height:1; font-size:10px; width:40px; text-align:right; color:#aaaaaa; padding-right:10px">
		                <div sytle="line-height:1; padding:0; text-align:right">▲</div>
		                <div sytle="line-height:1; padding:0; text-align:right">▼</div>
		            </td>
		        </tr></table>
		    </td>
            <td style="background-color:#ffffff; border:0; padding:0; width:180px; padding-left:5px" navi="メニューへの項目の搭載は「セクション構成」で行います。">
            <?
            	if ($sectItems[$row["muid"]]) {
					echo '<div id="sect_msg_on'.$row["muid"].'" class="gray">'.$sectItems[$row["muid"]].'アイテムがレイアウト済</div>';
					echo '<div id="sect_msg_off'.$row["muid"].'" class="red" style="display:none">！'.$sectItems[$row["muid"]].'アイテムが削除されます</div>';
				}
            ?>
		    </td>
	        </tr></table>
        </div>
        <? } ?>
    </div>
<?
}






//================================================================================================================
// メニューマスタの保存
//================================================================================================================
function reg_mst_menu() {
    $order_list = explode(",", $_REQUEST["menu_order_list"]);
    c2dbBeginTrans();

    //------------------------------------------------------
    // 保存する。
    // 大中小それぞれのメニューの数は決められているので、メニューは追加削除は無い。アクティブにするかどうかだけ。
    //------------------------------------------------------
    $menu_order = 0;
    foreach ($order_list as $ids) {
        list($dummy, $menu_hier, $muid) = explode("@", $ids);
        $menu_order++;

        $prefix = "menu@".$menu_hier."@".$muid."@";

        $sql =
        " update spfm_menu set".
        " menu_label = ".c2dbStr($_REQUEST[$prefix."menu_label"]).
        ",is_active = ".c2dbStr($_REQUEST[$prefix."is_active"]).
        ",is_usr_display = ".c2dbStr($_REQUEST[$prefix."is_usr_display"]).
        ",is_kan_display = ".c2dbStr($_REQUEST[$prefix."is_kan_display"]).
        ",is_syo_display = ".c2dbStr($_REQUEST[$prefix."is_syo_display"]).
        ",is_jin_display = ".c2dbStr($_REQUEST[$prefix."is_jin_display"]).
        ",menu_order = ".$menu_order.
        ",menu_children = ''".
        " where muid = ".$muid;
        c2dbExec($sql);
    }

    //------------------------------------------------------
    // menu_children サマリーの作成
    // （親メニュー行に、子メニューIDを示したCSVをセット）
    //------------------------------------------------------
    $rows = c2dbGetRows("select * from spfm_menu where is_active = '1' order by menu_order");
    $children = array();
    $dai_muid = "";
    $chu_muid = "";
    foreach ($rows as $row) {
        if ($row["menu_hier"]=="dai") {
            $dai_muid = $row["muid"];
            $chu_muid = "";
        }
        if ($row["menu_hier"]=="chu") {
            $chu_muid = $row["muid"];
            if (!$dai_muid) { echo "一般機能の前にメニューフォルダが指定されていません。"; die; }
            $children[$dai_muid][] = $row["muid"];
        }
    }
    foreach ($children as $muid => $ary) {
        $sql =
        " update spfm_menu set".
        " menu_children = ".c2dbStr(implode(",", $ary)).
        " where muid = ".$muid;
        c2dbExec($sql);
    }

    //------------------------------------------------------
    // 利用チェックを外されたメニューがレイアウトされている場合は、
    // レイアウト紐つけから抹消してしまう
    //------------------------------------------------------
    c2dbExec("delete from spfm_sect_layout where muid in (select muid from spfm_menu where is_active <> '1' or is_active is null)");
    c2dbCommit();

	c2dbSetSystemConfig("spf.DaiMenuBehavior", $_REQUEST["spfDaiMenuBehavior"]);

    echo "ok".'{forwardSection:"mst_menu", forwardOption:""}';
    die;
}

