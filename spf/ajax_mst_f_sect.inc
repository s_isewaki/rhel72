<?

//****************************************************************************************************************
// 注釈HTMLダイアログの中身を取得
//****************************************************************************************************************
function dialog_inst() {
	$lane = (int)$_REQUEST["lane"];
    $iid = (int)$_REQUEST["inst_id"];
    $cont = c2dbGetOne("select inst_html from spfm_inst where inst_id = ".$iid);
    echo '<div style="padding:5px" id="dialog_inst_area">';
    echo '<textarea name="inst_html" id="inst_html" cols="50" rows="10" style="width:700px; height:215px">'.hh($cont).'</textarea>';
    echo '<div style="padding:5px" class="center"><button type="button" class="w70" onclick="regInstructionDialog('.$iid.','.$lane.')">ＯＫ</button></div>';
    echo '</div>';
    die;
}

//****************************************************************************************************************
// 注釈HTMLの保存更新（メニューに組み込むかは別）
//****************************************************************************************************************
function reg_inst() {
    $iid = (int)$_REQUEST["inst_id"];
    $cont = $_REQUEST["inst_html"];
    if ($iid) {
        c2dbExec("update spfm_inst set inst_html = ".c2dbStr($cont)." where inst_id = ".$iid);
    } else {
        $iid = c2dbGetOne("select nextval('spfm_inst_id_seq')");
        c2dbExec("insert into spfm_inst (inst_id, inst_html) values (".$iid.", ".c2dbStr($cont).")");
    }
    echo 'ok{"iid":'.$iid.'}';
    die;
}






//****************************************************************************************************************
// セクション構成マスタ画面
//****************************************************************************************************************
function html_mst_sect_layout() {
	global $preserved_muids;
    $muid = (int)$_REQUEST["muid"];
    $lane_count = (int)$_REQUEST["lane_count"];
    $all_fields = array();
    $field_prefix = array();
    $field_labels = array();

    $cur_menu_row = array();
    $menu_labels = array();
    $dai_menu_label = "";
    $chu_menu_label = "";
    // 分類一覧取得、プルダウン準備
    $menuRows = c2dbGetRows("select * from spfm_menu where is_active = '1' order by menu_order");
    foreach ($menuRows as $row) {
        if (!$row["menu_label"]) $row["menu_label"] = "【名称未設定】";
        if ($row["menu_hier"]=="dai") {
            $dai_menu_label = $row["menu_label"];
            continue;
        }
        if ($row["menu_hier"]=="chu") {
            $chu_menu_label = $row["menu_label"];
        }
        if ($row["muid"]==$muid) $cur_menu_row = $row;
        $menu_labels[$row["muid"]] = $dai_menu_label." ≫ ".$row["menu_label"];
        if ($row["menu_hier"]=="syo") {
        $menu_labels[$row["muid"]] = $dai_menu_label." ≫ ".$chu_menu_label." ≫ ".$row["menu_label"];
        }
    }

    // 全入力項目を取得
    $fieldStructs = c2dbGetRows("select * from spfm_field_struct order by table_id, field_id");
    foreach ($fieldStructs as $row) {
        // ラベル項目を作成
        $table_field_type_id = $row["table_id"]."@".$row["field_id"]."@label";
        $all_fields[]= $table_field_type_id;
        $field_prefix[$table_field_type_id] = '<th style="background-color:#aaaaaa">ﾗﾍﾞﾙ</th>';
        $field_labels[$table_field_type_id] = '<td>'.hh($row["field_label"]).'</td>';
        // インプット項目を作成
        $table_field_type_id = $row["table_id"]."@".$row["field_id"]."@field";
        $all_fields[]= $table_field_type_id;
        $field_prefix[$table_field_type_id] = '<th style="background-color:#4a7bac">入力項目</th>';
        $field_labels[$table_field_type_id] = '<td>'.hh($row["field_label"]).'</td>';
    }

    // 全リスト項目を取得、これも全総合項目リストに追加
    $rows = c2dbGetRows("select * from spfm_list_lineup where is_active = '1' order by luid");
    foreach ($rows as $row) {
        // ラベル項目を作成
        $table_field_type_id = "spft_profile@".$row["luid"]."@label";
        $all_fields[]= $table_field_type_id;
        $field_prefix[$table_field_type_id] = '<th style="background-color:#aaaaaa">ﾘｽﾄﾗﾍﾞﾙ</th>';
        $field_labels[$table_field_type_id] = '<td>'.hh($row["field_label"]).'</td>';
        // インプット項目を作成
        $table_field_type_id = "spft_profile@".$row["luid"]."@list";
        $all_fields[]= $table_field_type_id;
        $field_prefix[$table_field_type_id] = '<th style="background-color:#6a9c32">ﾘｽﾄ項目</th>';
        $field_labels[$table_field_type_id] = '<td>'.hh($row["field_label"]).'</td>';
    }

    // レイアウト済項目一覧
    $sectLayouts = c2dbGetRows("select * from spfm_sect_layout order by field_order");
    $layoutedRows = array();
    $otherLayouted = array();
    $anyLayouted = array();
    $layoutedFields = array();
    $max_lane_count = 1;
    foreach ($sectLayouts as $row) {
        $tftid = $row["table_field_type_id"];
        $flg = 0;
		$lane = max(1, (int)$row["field_lane"]);
        if ($row["muid"]==$muid) {
			$flg = 1;
			if ($lane_count && $row["field_lane"] > $lane_count) $flg = 0;
			$max_lane_count = max($max_lane_count, $row["field_lane"]);
		}

		if ($flg) {
            $layoutedRows[$lane][] = $row; // 割当済み項目中、現在開いている分類へ割当済のもの
            $layoutedFields[] = $tftid; // 割当済み項目IDリストに追加
            continue;
        }
        else $otherLayouted[$tftid] .= "\n「".$row["muid"]." ".$menu_labels[$row["muid"]]."」へ割当中"; // 現在開いている分類「以外」で割当済みの項目
    }
    if (!$lane_count) $lane_count = $max_lane_count;
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        <? // height調整はindex_admin.php参照のこと ?>
        $("div#menu_entry_box_scroll1, div#menu_entry_box_scroll2, div#menu_no_assign").sortable({revert:true, helper:"original",
            forceHelperSize: true, connectWith:"div#menu_no_assign, div#menu_entry_box_scroll1, div#menu_entry_box_scroll2", scroll:false,
            appendTo: 'body',
            tolerance: 'pointer',
            revert: 'invalid',update:function(event, ui){
            setFieldLayoutOrder();
        }});
        function getElementLane(elem) {
			for (var idx=0; idx<10; idx++) {
				var lane = elem.getAttribute("lane");
				if (lane) return lane;
				elem = elem.parentNode;
			}
			alert("lane missing!");
		}
        function insertBr(lane) {
            $("#menu_entry_box_scroll"+lane).append(
                '<div class="m_f_box is_br" table_field_type_id="br">'+
                '   <table cellspacing="0" cellpadding="0" class="width100"><tr>'+
                '       <td>（改行）</td>'+
                '       <td class="right"><a href="javascript:void(0)" onclick="deleteATag(this)">削除</a></td>'+
                '   </tr></table>'+
                '</div>'
            );
            setFieldLayoutOrder();
            alert("改行パーツを末尾に追加しました。");
        }
        function adjustInnerHeight(ttlHeight, headerHeight) {
		    if (ee("menu_entry_box_scroll1")) ee("menu_entry_box_scroll1").style.height = (ttlHeight - headerHeight - 110) + "px";
		    if (ee("menu_entry_box_scroll2")) ee("menu_entry_box_scroll2").style.height = (ttlHeight - headerHeight - 110) + "px";
		    if (ee("menu_no_assign_scroll")) ee("menu_no_assign_scroll").style.height = (ttlHeight - headerHeight - 110) + "px";
		}
        // 注釈HTMLダイアログの保存
        function regInstructionDialog(instId, lane) {
            // 注釈HTMLダイアログの中身を保存
            var url = "common.php?ajax_action=reg_inst&inst_id="+instId;
            var postData = serializeUnicodeEntity("dialog_inst_area");
            $.ajax({ url:url, data:postData, success:function(msg) {
                var ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
                closeDialog("all");
                var fieldWidthId = 'field@inst@'+ret.iid+'@field_width';
                var fieldWidth = (ee(fieldWidthId) ? ee(fieldWidthId).value : "");
                // 保存が完了したらドラッグドロップエリアを更新
                alert("注釈の「HTML記述内容」は保存されました。\n\n注釈の並び順／注釈の追加行為は、「保存」ボタンを押すまで完了しません。");
                var html =
                '<table cellspacing="0" cellpadding="0" class="width100"><tr>'+
                '    <th style="background-color:#d170b9">注釈HTML</th>'+
                '    <td><a class="block" href="javascript:void(0)" onclick="showInstructionDialog('+ret.iid+', '+lane+')">注釈HTMLの入力</a></td>'+
                '    <td class="right"><input type="text" class="text w40 imeoff" name="'+fieldWidthId+'" id="'+fieldWidthId+'"'+
                '    navititle="表示幅の指定" value="'+fieldWidth+'" />'+
                '    <td class="right w20"><a href="javascript:void(0)" onclick="deleteATag(this)"><nobr>削除</nobr></a></td>'+
                '</tr></table>';
                if (instId) {
                    ee("inst@"+instId).innerHTML = html;
                } else {
                    $("#menu_entry_box_scroll"+lane).append(
                        '<div class="m_f_box is_inst" table_field_type_id="inst@'+ret.iid+'" id="inst@'+ret.iid+'">'+html+'</div>'
                    );
                }
                setFieldLayoutOrder();
            }});
        }
        function showInstructionDialog(instId, lane) {
            if(!instId) instId = "";
            var url = "common.php?ajax_action=dialog_inst&inst_id="+instId+"&lane="+lane;
            $.ajax({ url:url, success:function(msg) {
                var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
                $dialogs["inst_dialog"] = $(msg).dialog({
                    title:"注釈HTML", width:720, height:300, modal:true,
                    close:function(){
                        closeDialog("all");
                    }
                });
            }});
        }
        function deleteATag(aTag) {
            var div = aTag.parentNode.parentNode.parentNode.parentNode.parentNode;
            div.parentNode.removeChild(div);
            delete div;
            var lane = 1;
            var lane = getElementLane(aTag);
            setFieldLayoutOrder();
        }
        function setFieldLayoutOrder() {
			for (var lane=1; lane<=2; lane++) {
				if (!ee("menu_entry_box_scroll"+lane)) continue;
	            var idOrder = [];
	            $("div#menu_entry_box_scroll"+lane+" div.m_f_box").each(function() {
	                var table_field_type_id = str(this.getAttribute("table_field_type_id"));
	                var pnode = this.parentNode;
	                var ids = pnode.id.split("@");
	                var muid = ids[1];
	                idOrder.push(table_field_type_id);
	            });
	            ee("layout_order_list"+lane).value = idOrder.join(",");
            }
        }
        function eliminateKoumoku() {
            var isShowLabel = ee("chk_mst_sect_layout_label").checked;
            var is_ShowMiwariate = ee("chk_mst_sect_layout_miwariate").checked;
            $("div#menu_no_assign div.m_f_box").each(function() {
                var disp = "";
                if (!isShowLabel && this.className.indexOf("is_label")>=0) disp = "none";
                if (!is_ShowMiwariate && this.className.indexOf("other_layouted")>=0) disp = "none";
                this.style.display = disp;
            });
        }
        setFieldLayoutOrder();
    </textarea>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
        <table cellspacing="0" cellpadding="0" class="section_title"><tr>
            <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">F</th><? } ?>
            <th class="section_title_title" style="width:200px"><nobr>セクション構成</nobr></th>
            <th style="font-size:12px; letter-spacing:0">
                メニュー
                <select id="menu_muid" onchange="showAdminSect('mst_sect_layout', '&muid='+this.value)">
                <? foreach ($menuRows as $row) {
                    if ($row["menu_hier"]=="dai") continue; // 大分類はスルー
                    if ($row["muid"]==$muid) $cur_menu_row = $row;
                    $menu_label = $menu_labels[$row["muid"]];
                ?>
                <option value="<?=$row["muid"]?>"<?=($row["muid"]==$muid?" selected":"")?>><?=hh($menu_label)?></option>
                <? } ?>
                </select>
                <span style="padding-left:10px; padding-right:0">レーン</span>
                <select id="menu_lane_count" class="font12"
                	onchange="showAdminSect('mst_sect_layout', '&lane_count='+this.value+'&muid='+ee('menu_muid').value)">
                	<option value="1">単レーン</option>
                	<option value="2"<?=($lane_count==2?" selected":"")?>>２レーン</option>
                </select>
            </th>
            <? if ($preserved_muids[$muid]["section_layoutable"]!="none") { ?>
            <th class="button"><nobr><button type="button" onclick="tryAdminReg('reg_area', 'reg_mst_sect_layout')" class="w70">保存</button></nobr></th>
            <? } ?>
        </tr></table>
    </div>

	<?
		if ($preserved_muids[$muid]["section_layoutable"]=="none") {
			echo '<div style="padding-top:20px">このメニューはシステム固定です。セクションレイアウトは設定できません。</div>';
			return;
		}
    ?>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <table cellspacing="0" cellpadding="0" id="reg_area"><tr>

    <?
    	if (!$lane_count) $lane_count = 1;
    	for ($lane = 1; $lane<=$lane_count; $lane++) {
			$rows = $layoutedRows[$lane];
			if (!is_array($rows)) $rows = array();

    ?>
    <td style="padding-right:10px; width:340px" class="reg_td" lane="<?=$lane?>">

        <div style="padding-bottom:3px">
            <b><?=$menu_labels[$muid]?></b>
        </div>
        <div style="height:28px;">
            <button type="button" onclick="insertBr(<?=$lane?>)">改行を追加</button>
            <button type="button" onclick="showInstructionDialog('', <?=$lane?>)">注釈HTMLを追加</button>
        </div>

        <div style="padding-bottom:10px; background-color:#dddddd;">
        <div style="padding:10px; overflow-y:scroll" id="menu_entry_box_scroll<?=$lane?>">
        <?
        $prefix = "menu@chu@".$cur_menu_row["muid"]."@";
        foreach ($rows as $row) {
            $table_field_type_id = $row["table_field_type_id"];
            $attrs = explode("@",$table_field_type_id);
            $input_type = $attrs[2];
            $other_layouted = ($otherLayouted[$table_field_type_id] ? " other_layouted" : "");
            if ($table_field_type_id=="br") {
            ?>
                <div class="m_f_box is_br assigned" table_field_type_id="<?=$table_field_type_id?>">
                    <table cellspacing="0" cellpadding="0" class="width100"><tr>
                        <td>（改行）</td>
                        <td class="right"><a href="javascript:void(0)" onclick="deleteATag(this)">削除</a></td>
                    </tr></table>
                </div>
            <?
            } else if ($attrs[0]=="inst") {
            ?>
                <div class="m_f_box is_inst assigned" table_field_type_id="<?=$table_field_type_id?>" id="<?=$table_field_type_id?>">
                    <table cellspacing="0" cellpadding="0" class="width100"><tr>
                        <th style="background-color:#d170b9">注釈HTML</th>
                        <td><a class="block" href="javascript:void(0)" onclick="showInstructionDialog(<?=$row["inst_id"]?>,<?=$lane?>)">注釈HTMLの入力</a></td>
                        <td class="right"><input type="text" class="text w40 imeoff"
                            name="field@<?=$table_field_type_id?>@field_width" id="field@<?=$table_field_type_id?>@field_width"
                            navititle="表示幅の指定"
                            value="<?=hh($row["field_width"])?>" />
                        <td class="right w40"><a href="javascript:void(0)" onclick="deleteATag(this)">削除</a></td>
                    </tr></table>
                </div>
            <?
            } else {
            ?>
                <div class="m_f_box is_<?=$input_type?> <?=$other_layouted?>" table_field_type_id="<?=$table_field_type_id?>">
                    <table cellspacing="0" cellpadding="0" class="width100"><tr>
                        <?
                        	$navi = ' navi="項目ID：'.$table_field_type_id.'"';
                        	echo str_replace('<th', '<th'.$navi, $field_prefix[$row["table_field_type_id"]]);
                        	$navi = ' navi="'.$otherLayouted[$table_field_type_id].'"';
                        	echo str_replace('<td', '<td'.$navi, $field_labels[$row["table_field_type_id"]]);
                        ?>
                        <? if ($input_type!="list") { ?>
                        <td class="right"><input type="text" class="text w40 imeoff"
                            name="field@<?=$table_field_type_id?>@field_width" id="field@<?=$table_field_type_id?>@field_width"
                            navititle="表示幅の指定"
                            value="<?=hh($row["field_width"])?>" />
                        <? } ?>
                    </tr></table>
                </div>
            <?
            }
        }
        ?>
        </div>
        </div>
        <input type="hidden" name="layout_order_list<?=$lane?>" id="layout_order_list<?=$lane?>" />
    </td>
    <? } // lane loop ?>
    <td style="padding-left:10px">
        <div style="padding-bottom:3px">
            <b><span style="padding-right:2px">≪</span>候補項目</b>
        </div>
        <div style="height:28px">
            <label><input type="checkbox" onclick="eliminateKoumoku()" id="chk_mst_sect_layout_label">ﾗﾍﾞﾙ表示</label>
            <label><input type="checkbox" onclick="eliminateKoumoku()" id="chk_mst_sect_layout_miwariate">割当済表示</label>
        </div>
        <div style="padding-bottom:10px; background-color:#dddddd;">
        <div style="padding:10px; overflow-y:scroll" id="menu_no_assign_scroll">
            <div class="menu_box" id="menu_no_assign">
                <?
                    foreach ($all_fields as $table_field_type_id) {
                        if (in_array($table_field_type_id, $layoutedFields)) continue; // 左側へ割当済みならスルー
                        $attrs = explode("@", $table_field_type_id);
                        if ($attrs[0]=="inst") $attrs[2] = "inst";
                        $input_type = $attrs[2];
                        $other_layouted = ($otherLayouted[$table_field_type_id] ? " other_layouted" : "");
                        $is_label = ($input_type=="label" ? " is_label":"");
                ?>
                    <div class="m_f_box is_<?=$attrs[2]?> <?=$other_layouted?><?=$is_label?>" table_field_type_id="<?=$table_field_type_id?>"
                        style="<?=($is_label || $other_layouted?"display:none":"")?>">
                        <table cellspacing="0" cellpadding="0" class="width100"
                            navi="項目ID：<?=$table_field_type_id?> <?=$otherLayouted[$table_field_type_id]?>"><tr>
                            <?=$field_prefix[$table_field_type_id]?>
                            <?=$field_labels[$table_field_type_id]?>
                            <? if ($input_type!="list") { ?>
                            <td class="right"><input type="text" class="text w40 imeoff"
                                id="field@<?=$table_field_type_id?>@field_width" name="field@<?=$table_field_type_id?>@field_width" value="" />
                            <? } ?>
                        </tr></table>
                    </div>
                <? } ?>
            </div>
        </div>
        </div>
	    <input type="hidden" name="muid" value="<?=hh($muid)?>" />
    </td>
    </tr>
    </table>
<?
}
//================================================================================================================
// セクション構成 保存
//================================================================================================================
function reg_mst_sect_layout() {
    $muid = (int)$_REQUEST["muid"];
    $order_lists = array("1"=>explode(",", $_REQUEST["layout_order_list1"]), "2"=>explode(",", $_REQUEST["layout_order_list2"]));
    $field_order = 0;

    c2dbBeginTrans();
    c2dbExec("delete from spfm_sect_layout where muid = ".$muid);
    foreach ($order_lists as $lane => $order_list) {
	    foreach ($order_list as $table_field_type_id) {
	        if (!$table_field_type_id) continue;
	        $tftid = explode("@", $table_field_type_id);
	        $luid = "null";
	        $inst_id = "null";
	        if ($tftid[0]=="inst") $inst_id = (int)$tftid[1];
	        if ($tftid[2]=="list") $luid = $tftid[1];
	        $field_order++;
	        $field_width = (int)$_REQUEST["field@".$table_field_type_id."@field_width"];
	        $sql =
	        " insert into spfm_sect_layout (".
	        "     field_order, table_field_type_id, field_width, muid, luid, inst_id, field_lane".
	        " ) values (".
	        " ".c2dbInt($field_order).
	        ",".c2dbStr($table_field_type_id).
	        ",".c2dbStr($field_width).
	        ",".$muid.
	        ",".$luid.
	        ",".$inst_id.
	        ",".$lane.
	        " )";
	        c2dbExec($sql);
	    }
	}
    c2dbCommit();
    echo "ok".'{forwardSection:"mst_sect_layout", forwardOption:"&muid='.$muid.'"}';
    die;
}

