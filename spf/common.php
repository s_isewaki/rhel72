<?
define("SELF_APPID",   "ccusr1");
define("SELF_USER_AUTH_FLG", "emp_ccusr1_flg");
define("SELF_ADMIN_AUTH_FLG", "emp_ccadm1_flg");

define("KS_SEPARATOR", chr(0x1c)); // キーバリューセパレータ
define("FS_SEPARATOR", chr(0x1d)); // フィールドセパレータ

define("CSVENC_UTF8", "utf-8");
define("CSVENC_SJIS", "shift_jis");
define("CSVENC_EUCJP", "euc-jp");


define("SPF_VERSION", "1.0");
$GOD_CRYPT = array(
    "1.0" => "f880bca1aa1667cec9f281e1ad6913fa"
);

define("UB3", "___"); // アンダーバー３個

if ($_REQUEST["c2trace"]=="direct" || $_REQUEST["c2trace"]=="intelligent") {
    $_COOKIE["C2TRACE"] = $_REQUEST["c2trace"];
}
if ($_REQUEST["c2trace"]=="off" || $_REQUEST["c2trace"]=="exit") {
    $_COOKIE["C2TRACE"] = "";
}


define("SQE_SCREEN", "sqe_screen");
define("HYOKA_MUID", "501");
define("HYOKA_LUID", "301");
define("NEW_EMP_MUID", "502");
define("NEW_EMP_ID", "xxxxxxxxxxxxxxx"); // 新規作成の場合の仮emp_id。12文字以上とする
$preserved_muids = array(
	HYOKA_MUID=> array("caption"=>"SQE評価", "section_layoutable"=>"none"),
	NEW_EMP_MUID=> array("caption"=>"職員新規登録", "section_layoutable"=>"yes", "is_new_emp_section"=>"1")
);

//define("C2_SESSION_GROUP_NAME", "spf_ccusr1");
define("DO_PRINT", ($_REQUEST["do_print"] ? "1" : ""));

$pref_selection = array(
    "北海道", // CoMedixは、北海道=0である、とほほ
    "青森県","岩手県","宮城県","秋田県","山形県","福島県",
    "茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県",
    "新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県","静岡県","愛知県",
    "三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県",
    "鳥取県","島根県","岡山県","広島県","山口県",
    "徳島県","香川県","愛媛県","高知県",
    "福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県",
    "沖縄県"
);

$finder_result_formats = array(
	"RF_DEFAULT" => "",
	"RF_LIST" => "リストタイプ",
	"RF_BOX_2" => "BOXタイプ (2列)",
	"RF_BOX_3" => "BOXタイプ (3列)",
	"RF_BOX_4" => "BOXタイプ (4列)",
	"RF_BOX_5" => "BOXタイプ (5列)",
	"RF_BOX_6" => "BOXタイプ (6列)",
	"RF_BOX_7" => "BOXタイプ (7列)",
	"RF_BOX_8" => "BOXタイプ (8列)",
	"RF_MEISI_2" => "名刺タイプ (2列)",
	"RF_MEISI_3" => "名刺タイプ (3列)",
	"RF_MEISI_4" => "名刺タイプ (4列)"
);

$sinsei_status_list = array(
    "send" => array(
        //コマンド名         一覧表記        プルダウン表記         カラー
        "SS_NONE"   => array("jp"=>"編集中", "act"=>"編集中に戻す", "clr"=>"#aaaaaa"),
        "SS_APPLY"  => array("jp"=>"申請済", "act"=>"申請する",     "clr"=>"#000000")
    ),
    "recv" => array(
        "SS_NONE"   => array("jp"=>"",       "act"=>"未確認に戻す", "clr"=>"#aaaaaa"),
        "SS_CHECK"  => array("jp"=>"確認済", "act"=>"確認済にする", "clr"=>"#000000"),
        "SS_RETURN" => array("jp"=>"差戻し", "act"=>"差戻し",       "clr"=>"#000000"),
        "SS_REJECT" => array("jp"=>"却下",   "act"=>"却下する",     "clr"=>"#000000")
    )
);

$hyoka_status_list = array(
    "send" => array(
        //コマンド名         一覧表記        プルダウン表記         カラー
        "SS_NONE"   => array("jp"=>"編集中", "act"=>"編集中に戻す", "clr"=>"#aaaaaa"),
        "SS_APPLY"  => array("jp"=>"申請済", "act"=>"申請する",     "clr"=>"#000000")
    ),
    "recv" => array(
        "SS_NONE"   => array("jp"=>"",       "act"=>"未確認に戻す", "clr"=>"#aaaaaa"),
        "SS_CHECK"  => array("jp"=>"確認済", "act"=>"確認済にする", "clr"=>"#000000")
    )
);


$fixed_master_conv = array(
    "study_test_status@assembly_actual_status" => array("0"=>"未", "1"=>"欠席", "2"=>"出席"),
    "study_test_status@test_status"            => array("0"=>"未", "1"=>"受講中", "1"=>"合格"),
    "study_test_status@total_status"           => array("0"=>"未修", "1"=>"受講済")
);

function getSinseiStatusJp($sendOrRecv, $sinsei_status) {
    global $sinsei_status_list;
    $ret = $sinsei_status_list[$sendOrRecv][$sinsei_status]["jp"];
    return $ret;
}

function makeSinseiSelections($sendOrRecv, $sinsei_status) {
    global $sinsei_status_list;
    if (!$sinsei_status_list[$sendOrRecv][$sinsei_status]) $sinsei_status = "SS_NONE";
    $selections = array();
    foreach ($sinsei_status_list[$sendOrRecv] as $k => $row) {
        if ($k==$sinsei_status) $selections[$k] = $row["jp"];
        else $selections[$k] = $row["act"];
    }
    return $selections;
}
function makeHyokaSelections($sendOrRecv, $hyoka_status) {
    global $hyoka_status_list;
    if (!$hyoka_status_list[$sendOrRecv][$hyoka_status]) $hyoka_status = "SS_NONE";
    $selections = array();
    foreach ($hyoka_status_list[$sendOrRecv] as $k => $row) {
        if ($k==$hyoka_status) $selections[$k] = $row["jp"];
        else $selections[$k] = $row["act"];
    }
    return $selections;
}

function getProgressJp($progress, $hier){
    if ($progress=="") return "";
    $progress = (int)$progress;
    if ($progress<1) return "";

    // 所定階層が指定されていない場合は、全体進捗表現で返す
    if ($hier===0) {
        if ($progress == -10) return "(なし)";
        if ($progress == 3) return "編集中";
        if ($progress == 7) return "申請中";
        $mod = $progress % 10;
        $hier = (int)($progress / 10);
        if ($mod == 1) return "階層".$hier."から差戻し";
        if ($mod == 3) return "階層".$hier."確認中";
        if ($mod == 5) return "階層".$hier."で棄却";
        if ($mod == 7) return "階層".($hier+1)."申請中";
        if ($mod == 9) return "全稟議終了";
        return "(不明;".$progress.")";
    }

    // 所定階層があれば、所定階層における進捗表現で返す
    $hier = (int)$hier;
    if ($hier<1) return "";
    $minProgress = ($hier-1)*10+7;
    if ($progress < $minProgress) return "稟議未到達";
    if ($minProgress == $progress) return "稟議開始";
    if ($progress == $hier*10+1) return "差戻し";
    if ($progress == $hier*10+3) return "稟議中";
    if ($progress == $hier*10+5) return "棄却";
    if ($progress == $hier*10+7) return "稟議終了";
    if ($progress == $hier*10+9) return "全稟議終了";
    return "稟議終了";
}


//--------------------------------------------------------------------
// 指定日の翌日のYYYYMMDDを返す。第二引数にマイナス記号を指定すると、指定日の前日YYYYMMDDを返す。
//--------------------------------------------------------------------
function get_next_ymd($ymd, $plus_or_minus = "+") {
    return date("Ymd", strtotime(c2ConvYmd($ymd, "slash_ymd")." ".$plus_or_minus."1 day"));
}

function getSysMinMaxYmd() {
    $MinYmd = c2dbGetSystemConfig("spf.SysMinYmd");
    $MaxYmd = c2dbGetSystemConfig("spf.SysMaxYmd");
    $MinYmdAllow = (c2dbGetSystemConfig("spf.SysMinYmdAllow") ? "1": "");
    $MaxYmdAllow = (c2dbGetSystemConfig("spf.SysMaxYmdAllow") ? "1": "");
    if (strlen($MinYmd)!=8) $MinYmd = "19000101";
    if (strlen($MaxYmd)!=8) $MaxYmd = "20991231";
    return array("MinYmd"=>$MinYmd, "MaxYmd"=>$MaxYmd, "MinYmdAllow"=>$MinYmdAllow, "MaxYmdAllow"=>$MaxYmdAllow);
}

function eliminateYmdSlash($ymd) {
    if (strlen($ymd)==8 && strpos($ymd, "/")===FALSE && strpos($ymd, "-")===FALSE && strpos($ymd, ".")===FALSE) return $ymd;
    list($yy, $mm, $dd) = explode("/", str_replace('.', "/", str_replace("-", "/", $ymd)));
    if (strlen($yy)==0 || strlen($mm)==0 || strlen($dd)==0) return "";
    if (strlen($yy)>4 || strlen($mm)>2 || strlen($dd)>2) return "";
    return sprintf("%04d", $yy).sprintf("%02d", $mm).sprintf("%02d", $dd);
}
// ★★★非利用の方向で。
function eliminateYmSlash($yymm) {
    if (strlen($yymm)==6 && strpos($ymd, "/")===FALSE && strpos($ymd, "-")===FALSE && strpos($ymd, ".")===FALSE) return $yymm;
    list($yy, $mm) = explode("/", str_replace(".", "/", str_replace("-", "/", $yymm)));
    if (strlen($yy)==0 || strlen($mm)==0) return "";
    if (strlen($yy)>4 || strlen($mm)>2) return "";
    return sprintf("%04d", $yy).sprintf("%02d", $mm);
}

function convYmdWrap($yyyymmdd) {
    if ($yyyymmdd=="00000000") return "";
    if ($yyyymmdd=="99999999") return "無期限";
    return c2ConvYmd(str_replace("-", "",str_replace("/", "",substr($yyyymmdd, 0, 10))), "slash_ymd");
}

global $LIST_FIELD_DEF_SYS_FIELDS; // リストフィールド(spfm_list_field_def）から除外できないフィールド
$LIST_FIELD_DEF_SYS_FIELDS = array(
//  "spft_profile@data_stream"
);

global $HISSU_FIELDS; // ユーザ入力が必須のフィールド
$HISSU_FIELDS = array("empmst@emp_lt_nm", "empmst@emp_ft_nm", "empmst@emp_kn_lt_nm", "empmst@emp_kn_ft_nm", "empmst@emp_sex");

global $MASTER_TABLES;
$MASTER_TABLES = array(
    "prefs"=>"都道府県マスタ", "genders"=>"性別マスタ",
    "classmst"=>"施設マスタ","atrbmst"=>"課マスタ","deptmst"=>"科マスタ","classroom"=>"エリアマスタ",
    "jobmst"=>"職種マスタ","stmst"=>"役職マスタ","jobmst_senmon"=>"専門職種マスタ",
    "spfm_ninteisya"=>"認定者マスタ","spfm_shikaku"=>"資格マスタ","spfm_iinkai"=>"委員会マスタ",
    "spfm_nshikaku"=>"認定資格マスタ","spfm_kousyu"=>"講習資格マスタ","spfm_kensyu"=>"研修マスタ","spfm_gakkai"=>"学会マスタ",
    "spfm_hyoka_syugi"=>"評価手技マスタ",
    "spfm_hyoka_sheet"=>"評価シートマスタ","spfm_hyoka_han"=>"評価エディションマスタ","spfm_hyoka_term"=>"評価エディション年度",
    "authgroup"=>"権限グループマスタ", "dispgroup"=>"表示グループマスタ", "menugroup"=>"メニューグループマスタ"
);


function getEmpProfileDetail($emp_id, $field_id) {
    // さらに拡張：shikaku_nmを出力する場合は、職員の資格登録数も取得
    $sql =
    " select * from spft_profile".
    " where emp_id = ".c2dbStr($emp_id).
    " and ".$field_id." is not null and ".$field_id." <> ''".
    " and del_flg = 0".
    " order by ymd_fr";
    $rows = c2dbGetRows($sql);
    $out = array();
    foreach ($rows as $row) {
        $out[$row[$field_id]][] = $row["cnt"];
    }
    return $out;
}

global $masters;
$masters = array();
function getMasterHash($table_id, $is_hash, $parent_id="") {
    $table_id = trim($table_id);
    global $masters;
    $postfix = "";
    if ($is_hash) $postfix = "_hash";
    if (!$masters[$table_id]) {
        if ($table_id=="prefs") {
            global $pref_selection;
            $masters["prefs"] = $pref_selection;
            $masters["prefs_hash"] = $pref_selection;
            return $masters["prefs".$postfix];
        }
        if ($table_id=="genders") {
            $masters["genders"] = array("","男","女","未設定");
            $masters["genders_hash"] = array("1"=>"男", "2"=>"女", "3"=>"未設定");
            return $masters["genders".$postfix];
        }

        $sql = "";
        if ($table_id == "spfm_ninteisya") $sql = "select ninteisya_id, ninteisya_nm from spfm_ninteisya where ninteisya_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_shikaku") $sql = "select shikaku_id, shikaku_nm from spfm_shikaku where shikaku_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_nshikaku")$sql = "select nshikaku_id, nshikaku_nm from spfm_nshikaku where nshikaku_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_kousyu")  $sql = "select kousyu_id, kousyu_nm from spfm_kousyu where kousyu_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_kensyu")  $sql = "select kensyu_id, kensyu_nm from spfm_kensyu where kensyu_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_hyoka_syugi")  $sql = "select hyoka_syugi_id, hyoka_syugi_nm from spfm_hyoka_syugi where hyoka_syugi_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_gakkai")  $sql = "select gakkai_id, gakkai_nm from spfm_gakkai where gakkai_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_iinkai")  $sql = "select iinkai_id, iinkai_nm from spfm_iinkai where iinkai_disp_flg = '1' order by order_no";
        if ($table_id == "classmst")     $sql = "select class_id, class_nm from classmst where class_del_flg = false order by order_no";
        if ($table_id == "classroom")    $sql = "select dept_id, room_id, room_nm from classroom where room_del_flg = false order by order_no";
        if ($table_id == "jobmst")       $sql = "select job_id, job_nm from jobmst where job_del_flg = 'f' order by order_no";
        if ($table_id == "stmst")        $sql = "select st_id, st_nm from stmst where st_del_flg = 'f' order by order_no";
        if ($table_id == "jobmst_senmon") $sql = "select job_senmon_id, job_id, job_senmon_nm from jobmst_senmon where job_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_hyoka_sheet") $sql = "select sheet_id, sheet_nm from spfm_hyoka_sheet order by sheet_order";
        if ($table_id == "spfm_hyoka_han")   $sql = "select han_id, han_nm from spfm_hyoka_han order by han_id";
        if ($table_id == "spfm_hyoka_term")  $sql = "select han_term from spfm_hyoka_han group by han_term order by han_term";
        if ($table_id == "authgroup")   $sql = "select group_id, group_nm from authgroup order by group_id";
        if ($table_id == "dispgroup")   $sql = "select group_id, group_nm from dispgroup order by group_id";
        if ($table_id == "menugroup")   $sql = "select group_id, group_nm from menugroup order by group_id";

        if ($table_id == "atrbmst") {
            $sql = "select class_id, atrb_id, atrb_nm from atrbmst where atrb_del_flg = false order by order_no";
            if (USE_SPFM_ATRB=="1") {
                $sql =
                " select spf_dept_id as dept_id, spf_atrb_id as atrb_id, spf_atrb_nm as atrb_nm".
                " from spfm_atrbmst where spf_atrb_del_flg = 0 order by order_no";
            }
        }
        if ($table_id == "deptmst") {
            $sql = "select atrb_id, dept_id, dept_nm from deptmst where dept_del_flg = false order by order_no";
            if (USE_SPFM_DEPT=="1") {
                $sql =
                " select spf_dept_id as dept_id, spf_atrb_id as atrb_id, spf_dept_nm as dept_nm".
                " from spfm_deptmst where spf_dept_del_flg = 0 order by order_no";
            }
        }
        if (!$sql) { echo "getMasterHash() テーブル指定が不正です。".$table_id; die; }

        //----------------------------------------
        // その１）プレーンリスト
        //----------------------------------------
        $rows = c2dbGetRows($sql);

        //----------------------------------------
        // その２）キー&バリュー形式（_hash）
        //----------------------------------------
        $hash = array();
        foreach ($rows as $row) {
            if      ($table_id == "classmst")         $hash[$row["class_id"]] =    $row["class_nm"];
            else if ($table_id == "atrbmst")          $hash[$row["atrb_id"]]  =    $row["atrb_nm"];
            else if ($table_id == "deptmst")          $hash[$row["dept_id"]]  =    $row["dept_nm"];
            else if ($table_id == "classroom")        $hash[$row["room_id"]]  =    $row["room_nm"];
            else if ($table_id == "jobmst")           $hash[$row["job_id"]]   =    $row["job_nm"];
            else if ($table_id == "jobmst_senmon")    $hash[$row["job_senmon_id"]]=$row["job_senmon_nm"];
            else if ($table_id == "stmst")            $hash[$row["st_id"]]    =    $row["st_nm"];
            else if ($table_id == "spfm_iinkai")      $hash[$row["iinkai_id"]]=    $row["iinkai_nm"];
            else if ($table_id == "spfm_shikaku")     $hash[$row["shikaku_id"]]=   $row["shikaku_nm"];
            else if ($table_id == "spfm_nshikaku")    $hash[$row["nshikaku_id"]]=  $row["nshikaku_nm"];
            else if ($table_id == "spfm_kousyu")      $hash[$row["kousyu_id"]]=    $row["kousyu_nm"];
            else if ($table_id == "spfm_kensyu")      $hash[$row["kensyu_id"]]=    $row["kensyu_nm"];
            else if ($table_id == "spfm_hyoka_syugi") $hash[$row["hyoka_syugi_id"]]= $row["hyoka_syugi_nm"];
            else if ($table_id == "spfm_gakkai")      $hash[$row["gakkai_id"]]=    $row["gakkai_nm"];
            else if ($table_id == "spfm_ninteisya")   $hash[$row["ninteisya_id"]]= $row["ninteisya_nm"];
            else if ($table_id == "spfm_hyoka_sheet") $hash[$row["sheet_id"]]=     $row["sheet_nm"];
            else if ($table_id == "spfm_hyoka_han")   $hash[$row["han_id"]]=       $row["han_nm"];
            else if ($table_id == "authgroup")        $hash[$row["group_id"]]=     $row["group_nm"];
            else if ($table_id == "dispgroup")        $hash[$row["group_id"]]=     $row["group_nm"];
            else if ($table_id == "menugroup")        $hash[$row["group_id"]]=     $row["group_nm"];
            else if ($table_id == "spfm_hyoka_term") {
				$frto = explode("-", $row["han_term"]);
				$hash[$row["han_term"]]= c2ConvYmd($frto[0],"slash_ymd")."〜".c2ConvYmd($frto[1], "slash_ymd");
			}
        }
        $masters[$table_id] = $rows;
        $masters[$table_id."_hash"] = $hash;
    }
    return $masters[$table_id.$postfix];
}

global $masterr;
$masterr = array();
function getMasterHashReverse($table_id) {
    $table_id = trim($table_id);
    global $masterr;
    if (!$masterr[$table_id]) {
        if ($table_id=="prefs") {
            global $pref_selection;
            $rev = array();
            foreach ($pref_selection as $idx=>$pref) $rev[$pref] = $idx;
            $masterr["prefs"] = $rev;
            return $masterr["prefs"];
        }
        if ($table_id=="genders") {
            $masterr["genders_hash"] = array("男"=>"1", "女"=>"2", "未設定"=>"3");
            return $masterr["genders".$postfix];
        }

        $sql = "";
        if ($table_id == "spfm_ninteisya") $sql = "select ninteisya_id, ninteisya_nm from spfm_ninteisya where ninteisya_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_shikaku") $sql = "select shikaku_id, shikaku_nm from spfm_shikaku where shikaku_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_nshikaku")$sql = "select nshikaku_id, nshikaku_nm from spfm_nshikaku where nshikaku_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_kousyu")$sql = "select kousyu_id, kousyu_nm from spfm_kousyu where kousyu_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_kensyu")  $sql = "select kensyu_id, kensyu_nm from spfm_kensyu where kensyu_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_hyoka_syugi")  $sql = "select hyoka_syugi_id, hyoka_syugi_nm from spfm_hyoka_syugi where hyoka_syugi_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_gakkai")  $sql = "select gakkai_id, gakkai_nm from spfm_gakkai where gakkai_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_iinkai")  $sql = "select iinkai_id, iinkai_nm from spfm_iinkai where iinkai_disp_flg = '1' order by order_no";
        if ($table_id == "classmst")     $sql = "select class_id, class_nm from classmst where class_del_flg = false order by order_no";
        if ($table_id == "classroom")    $sql = "select dept_id, room_id, room_nm from classroom where room_del_flg = false order by order_no";
        if ($table_id == "jobmst")       $sql = "select job_id, job_nm from jobmst where job_del_flg = 'f' order by order_no";
        if ($table_id == "stmst")        $sql = "select st_id, st_nm from stmst where st_del_flg = 'f' order by order_no";
        if ($table_id == "jobmst_senmon") $sql = "select job_senmon_id, job_id, job_senmon_nm from jobmst_senmon where job_disp_flg = '1' order by order_no";
        if ($table_id == "spfm_hyoka_sheet") $sql = "select sheet_id, sheet_nm from spfm_hyoka_sheet order by sheet_order";
        if ($table_id == "spfm_hyoka_han")   $sql = "select han_id, han_nm from spfm_hyoka_han order by han_id";
        if ($table_id == "spfm_hyoka_term")  $sql = "select han_term from spfm_hyoka_han group by han_term order by han_term";
        if ($table_id == "authgroup")   $sql = "select group_id, group_nm from authgroup order by group_id";
        if ($table_id == "dispgroup")   $sql = "select group_id, group_nm from dispgroup order by group_id";
        if ($table_id == "menugroup")   $sql = "select group_id, group_nm from menugroup order by group_id";

        if ($table_id == "atrbmst") {
            $sql = "select class_id, atrb_id, atrb_nm from atrbmst where atrb_del_flg = false order by order_no";
            if (USE_SPFM_ATRB=="1") {
                $sql =
                " select spf_dept_id as dept_id, spf_atrb_id as atrb_id, spf_atrb_nm as atrb_nm".
                " from spfm_atrbmst where spf_atrb_del_flg = 0 order by order_no";
            }
        }
        if ($table_id == "deptmst") {
            $sql = "select atrb_id, dept_id, dept_nm from deptmst where dept_del_flg = false order by order_no";
            if (USE_SPFM_DEPT=="1") {
                $sql =
                " select spf_dept_id as dept_id, spf_atrb_id as atrb_id, spf_dept_nm as dept_nm".
                " from spfm_deptmst where spf_dept_del_flg = 0 order by order_no";
            }
        }
        if (!$sql) { echo "getMasterHash() テーブル指定が不正です。".$table_id; die; }

        //----------------------------------------
        // その１）プレーンリスト
        //----------------------------------------
        $rows = c2dbGetRows($sql);

        //----------------------------------------
        // その２）キー&バリュー形式（_hash）
        //----------------------------------------
        $hash = array();
        foreach ($rows as $row) {
            if      ($table_id == "classmst")         $hash[$row["class_nm"]] =    $row["class_id"];
            else if ($table_id == "atrbmst")          $hash[$row["atrb_nm"]]  =    $row["atrb_id"];
            else if ($table_id == "deptmst")          $hash[$row["dept_nm"]]  =    $row["dept_id"];
            else if ($table_id == "classroom")        $hash[$row["room_nm"]]  =    $row["room_id"];
            else if ($table_id == "jobmst")           $hash[$row["job_nm"]]   =    $row["job_id"];
            else if ($table_id == "stmst")            $hash[$row["st_nm"]]    =    $row["st_id"];
            else if ($table_id == "spfm_iinkai")      $hash[$row["iinkai_nm"]]=    $row["iinkai_id"];
            else if ($table_id == "spfm_shikaku")     $hash[$row["shikaku_nm"]]=   $row["shikaku_id"];
            else if ($table_id == "spfm_nshikaku")    $hash[$row["nshikaku_nm"]]=  $row["nshikaku_id"];
            else if ($table_id == "spfm_kousyu")      $hash[$row["kousyu_nm"]]=    $row["kousyu_id"];
            else if ($table_id == "spfm_kensyu")      $hash[$row["kensyu_nm"]]=    $row["kensyu_id"];
            else if ($table_id == "spfm_hyoka_syugi") $hash[$row["hyoka_syugi_nm"]]=$row["hyoka_syugi_id"];
            else if ($table_id == "spfm_gakkai")      $hash[$row["gakkai_nm"]]=    $row["gakkai_id"];
            else if ($table_id == "spfm_ninteisya")   $hash[$row["ninteisya_nm"]]= $row["ninteisya_id"];
            else if ($table_id == "spfm_hyoka_sheet") $hash[$row["sheet_nm"]]=     $row["sheet_id"];
            else if ($table_id == "spfm_hyoka_han")   $hash[$row["han_nm"]]=       $row["han_id"];
            else if ($table_id == "authgroup")        $hash[$row["group_nm"]]=     $row["group_id"];
            else if ($table_id == "dispgroup")        $hash[$row["group_nm"]]=     $row["group_id"];
            else if ($table_id == "menugroup")        $hash[$row["group_nm"]]=     $row["group_id"];
        }
        $masterr[$table_id] = $rows;
    }
    return $masterr[$table_id];
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
function writeSyozokuMasterData() {
    echo 'var syozoku_data = {};'."\n";
    $rows = getMasterHash("classmst", 0);
    $jsary = array();
    foreach ($rows as $row) $jsary[""][] = array("ssid"=>$row["class_id"], "ssnm"=>$row["class_nm"]);
    echo 'syozoku_data["class_id"] = ' .c2ToJson($jsary).";\n";
    echo 'syozoku_data["emp_class"] = syozoku_data["class_id"];'."\n";

    $rows = getMasterHash("atrbmst", 0);
    $jsary = array();
    foreach ($rows as $row) $jsary[$row["class_id"]][] = array("ssid"=>$row["atrb_id"], "ssnm"=>$row["atrb_nm"]);
    echo 'syozoku_data["atrb_id"] = ' .c2ToJson($jsary).";\n";
    echo 'syozoku_data["emp_attribute"] = syozoku_data["atrb_id"];'."\n";

    $rows = getMasterHash("deptmst", 0);
    $jsary = array();
    foreach ($rows as $row) $jsary[$row["atrb_id"]][] = array("ssid"=>$row["dept_id"], "ssnm"=>$row["dept_nm"]);
    echo 'syozoku_data["dept_id"] = ' .c2ToJson($jsary).";\n";
    echo 'syozoku_data["emp_dept"] = syozoku_data["dept_id"];'."\n";

    $rows = getMasterHash("classroom", 0);
    $jsary = array();
    foreach ($rows as $row) $jsary[$row["dept_id"]][] = array("ssid"=>$row["room_id"], "ssnm"=>$row["room_nm"]);
    echo 'syozoku_data["room_id"] = ' .c2ToJson($jsary).";\n";
    echo 'syozoku_data["emp_room"] = syozoku_data["room_id"];'."\n";
}

function getSyozokuDisp($class_id, $atrb_id, $dept_id, $room_id) {
    $class_rows = getMasterHash("classmst", 1);
    $atrb_rows = getMasterHash("atrbmst", 1);
    $dept_rows = getMasterHash("deptmst", 1);
    $room_rows = getMasterHash("classroom", 1);
    $class_nm = $class_rows[$class_id];
    $atrb_nm  = $atrb_rows[$atrb_id];
    $dept_nm  = $dept_rows[$dept_id];
    $room_nm  = $room_rows[$room_id];
    return jointSyozokuDisp($class_nm, $atrb_nm, $dept_nm, $room_nm);
}

function jointSyozokuDisp($class_nm, $atrb_nm, $dept_nm, $room_nm) {
    if (SYOZOKU_STYLE=="SINRYOKA_TO_BUSYO") {
        return str_replace("\t", " ＞ ", rtrim($class_nm."\t".$dept_nm."\t".$atrb_nm."\t".$room_nm)); // わざわざアールトリムしていることに注意。
    } else if (SYOZOKU_STYLE=="SINRYOKA_BUSYO_EACH") {
        return str_replace("\t", " ｜ ", rtrim($class_nm."\t".$atrb_nm."\t".$dept_nm."\t".$room_nm)); // わざわざアールトリムしていることに注意。
    } else {
        return str_replace("\t", " ＞ ", rtrim($class_nm."\t".$atrb_nm."\t".$dept_nm."\t".$room_nm)); // わざわざアールトリムしていることに注意。
    }
}


// このファイルのひとつ上の階層に上ってから、Core2呼び出し
require_once(dirname(dirname(__FILE__))."/spf/Cmx/Core2/C2FW.php");

function set_pg_version(){
	$_ary1 = explode(" ", c2dbGetOne("select version()"));
	$vgn = explode(".", $_ary1[1]);
	define("PG_VERSION",  $_ary1[1]);
	define("PG_MAJOR_VERSION",  $vgn[0]);
	define("PG_MINOR1_VERSION", $vgn[1]);
	define("PG_MINOR2_VERSION", $vgn[2]);
	define("PG_OVER_AVAILABLE", $vgn[0]>=8 && $vgn[1]>=4 ? 1 : "");
}

// アプリ名
$c2app = c2GetC2App();
//define("SELF_APPNAME",   $c2app->getApplicationName("ccusr1"));
define("SELF_APPNAME",   "スタッフ・ポートフォリオ");

$loginEmpmstRow = c2login::validateSession(); // クッキーのセッション値からログイン者判別

$EAL = $c2app->getEmpAuthority(C2_LOGIN_EMP_ID); // ログイン者の機能権限有無リスト
$spfNendoStartMMDD = c2dbGetSystemConfig("spf.NendoStartMMDD");
$spf_exec_log_start = c2dbGetSystemConfig("spf.ExecLogStart");

function _define_system_auth($eal) {
    $ng = (!defined("C2_SESSION_ID") || !C2_SESSION_ID);
    if (!$ng) {
        $_god = c2dbGetSystemConfig("spf.GodSessonId");
        $_dev = c2dbGetSystemConfig("spf.DevSessonId");
        $_opm = c2dbGetSystemConfig("spf.OpmSessonId");
    }
    define("IS_ADMIN_AUTH", $eal[SELF_ADMIN_AUTH_FLG] ? "1" : "");
    define("IS_SYSTEM_AUTH", (!$ng && ($_god==C2_SESSION_ID || $_dev==C2_SESSION_ID || $_opm==C2_SESSION_ID) ? "1" : ""));
    define("IS_GOD_AUTH", (!$ng && $_god==C2_SESSION_ID ? "1" : ""));
    define("IS_DEV_AUTH", (!$ng && $_dev==C2_SESSION_ID ? "1" : ""));
    define("IS_OPM_AUTH", (!$ng && $_opm==C2_SESSION_ID ? "1" : ""));
}
_define_system_auth($EAL);



if ($spf_exec_log_start) c2dbExecLogStart("spf"); // DBにspfという名義で更新系SQLログを保存させるように指定

define("IS_JIN_AUTH", (c2dbGetOne("select is_jin_auth from spft_empmst where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID)) ? "1" : ""));

define("SCRIPT_VERSION", (float)c2dbGetSystemConfig("spf.ScriptVersion"));

define("SHOW_TFID", (c2dbGetSystemConfig("spf.DevShowTfid") ? "1" : ""));



function isDispOrEditable($d_or_e, $empmstRow, $structRow) {
    $auth = getLoginUserAuth($empmstRow["emp_id"]);
    $ret = array();
    // ログインユーザ本人である。かつ、このフィールドに本人へのアクセスが許容されている。
    if ($auth["is_usr"] && $structRow["is_usr_".$d_or_e]) $ret["usr_auth"]=1;
    // ログインユーザは人事である。かつ、このフィールドには人事者へのアクセスが許容されている。
    if ($auth["is_jin"] && $structRow["is_jin_".$d_or_e]) $ret["jin_auth"]=1;
    // ログイン者は対象者から見て監督者である。かつ、このフィールドには監督者へのアクセスが許容されている。
    if ($auth["is_kan"] && $structRow["is_kan_".$d_or_e]) $ret["kan_auth"]=1;
    // ログイン者は対象者から見て承認者/評価者である。かつ、このフィールドには承認者/評価者へのアクセスが許容されている。
    if ($auth["is_syo"] && $structRow["is_syo_".$d_or_e]) $ret["syo_auth"]=1;
    return $ret;
}

// 選択対象者が現在、誰かの監督者か
function isKantokusya($emp_id) {
    $sql =
    " select sk.joint_id from concurrent_view cv".
    " inner join spfm_kantoku sk on (".
    "     sk.emp_id_or_syozoku = cv.emp_id".
    "     or (".
    "         ( sk.to_st_id = 0 or sk.to_st_id = cv.emp_st )".
    "         and (".
    "             sk.to_last_cadr_hier = 'K_CLASS' and to_last_cadr_id = cv.emp_class".
    "             or sk.to_last_cadr_hier = 'K_ATRB' and to_last_cadr_id = cv.emp_attribute".
    "             or sk.to_last_cadr_hier = 'K_DEPT' and to_last_cadr_id = cv.emp_dept".
    "             or sk.to_last_cadr_hier = 'K_ROOM' and to_last_cadr_id = cv.emp_room".
    "         )".
    "     )".
    " )".
    " where cv.emp_id = ".c2dbStr($emp_id)." limit 1";
    return (c2dbGetOne($sql) ? 1 : 0);
}
// 選択対象者が現在、誰かの承認者か
function isSyoninsya($emp_id) {
    $sql = " select emp_id from spft_notify where to_emp_id = ".c2dbStr($emp_id)." limit 1";
    return (c2dbGetOne($sql) ? 1 : 0);
}

global $loginUserAuthRows;
$loginUserAuthRows = array();
function getLoginUserAuth($emp_id) {

    global $loginUserAuthRows;
    if (!$loginUserAuthRows[$emp_id]) {
        $ret = array();
        if ($emp_id==C2_LOGIN_EMP_ID) $ret["is_usr"]=1; // ログインユーザ本人である
        if (IS_JIN_AUTH=="1") $ret["is_jin"]=1; // ログインユーザは人事である

        //============================================
        // ログイン者は、対象者の監督者か
        //============================================
        $sql =
        " select sk.joint_id from spfm_kantoku sk".
        // ログイン者が監督者か
        " where exists (".
        "     select emp_id from concurrent_view2 cv".
        "     where cv.emp_id = ".c2dbStr(C2_LOGIN_EMP_ID).
        "     and (".
        "         sk.emp_id_or_syozoku = cv.emp_id". // 監督者として直接指定されている
        "         or".
        "         (".
        "             ( sk.to_st_id = 0 or sk.to_st_id = cv.emp_st )". // ログイン者の所属が指定されている
        "             and (". // ログイン者の所属が指定されている
        "                    ( sk.to_last_cadr_hier = 'K_CLASS' and to_last_cadr_id = cv.emp_class )".
        "                 or ( sk.to_last_cadr_hier = 'K_ATRB' and to_last_cadr_id = cv.emp_attribute )".
        "                 or ( sk.to_last_cadr_hier = 'K_DEPT' and to_last_cadr_id = cv.emp_dept )".
        "                 or ( sk.to_last_cadr_hier = 'K_ROOM' and to_last_cadr_id = cv.emp_room )".
        "             )".
        "         )".
        "     )".
        " )".
        // 対象者の所属部署を監督するかどうか
        " and exists (".
        "     select emp_id from concurrent_view2 cv".
        "     where cv.emp_id = ".c2dbStr($emp_id).
        "     and sk.joint_id in ( cv.c_id, cv.ca_id, cv.cad_id, cv.cadr_id )".
        " )".
        " limit 1";
        $ret["is_kan"] = (c2dbGetOne($sql) ? 1 : 0);
        //============================================
        // ログイン者は、対象者の承認者か
        //============================================
	    $sql = " select emp_id from spft_notify where emp_id = ".c2dbStr($emp_id)." and to_emp_id = ".c2dbStr(C2_LOGIN_EMP_ID)." limit 1";
        $ret["is_syo"] = (c2dbGetOne($sql) ? 1 : 0);
        $loginUserAuthRows[$emp_id] = $ret;
    }
    return $loginUserAuthRows[$emp_id];
}

//================================================================================================================
// プルダウンタグを作成
//================================================================================================================
function make_select_tag($fid, $field_width, $selections, $def, $is_editable, $other, $ownerId, $is_hissu, $field_label, $allow_empty) {
    $html = array();
    if (!$is_editable) {
        $html[]= '<input type="hidden" id="'.$fid.'" name="'.$fid.'" value="'.hh($def).'" field_label="'.$field_label.'" />';
        $html[]= '<div class="label_box" navititle="読取専用" navi="編集権限がありません">'.hh($selections[$def]).'</div>';
        return implode("", $html);
    }
    if ($is_hissu) $html[]= '<div style="position:relative">';
    $html[]= '<select id="'.$fid.'" name="'.$fid.'" style="'.($field_width && $field_width!="auto"? 'width:'.$field_width.'px':"").'"'.$other;
    if ($is_hissu) $html[]= ' class="is_hissu"';
    $html[]= ' field_label="'.$field_label.'"';
    $html[]= ' defval="'.hh($def).'" onchange="watchDiff(this, \''.$ownerId.'\')">';
    if (($def=="" || !isset($selections[""])) && $allow_empty) {
	    $html[]= '<option value=""'.($def==""?' class="select_defval"':"").'>　</option>';
	}
    foreach ($selections as $k => $v) {
        $html[]= '<option value="'.hh($k).'"'.($k==$def && strlen($k)==strlen($def) ?' selected class="select_defval"':"").'>'.hh($v).'</option>';
    }
    $html[]= '</select>';
    if ($is_hissu) $html[]= '<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" /></div>';
    return implode("", $html);
}



//================================================================================================================
// メッセージ通知のメッセージを取得
//================================================================================================================
function getMessagesHtml($disp_place, $emp_id) {
    $ret = getMessages($disp_place, $emp_id);

    $out = array();
    foreach ($ret[$disp_place] as $key => $row) {
        $out[]= $row["msg"];
    }
    return implode("\n", $out);
}
function getMessages($disp_place_comma_list, $emp_id) {
    $ary = explode(",", $disp_place_comma_list);
    $dps = "'". implode("', '", $ary) . "'";
    $out = array();
    foreach ($ary as $dp) $out[$dp] = array(); // 結果数ゼロ件で初期化。
    $rows = c2dbGetRows("select * from spfm_messages where is_active = '1' and disp_place in (".$dps.") order by sort_order, message_id desc");
    c2dbSetErrorReturn(true); // 以降、画面に登録されたSQLを実行してゆくが、エラーでdie停止させないようにしておく。
    foreach ($rows as $row) {
        $message_id = $row["message_id"];
        $sql = trim($row["sql_formula"]);
        if ($sql=="") continue;
        if (strtolower(substr($sql,0,6))!="select") {
            $out[$row["disp_place"]][$message_id] = array("msg"=>"", "warn"=>"判定構文が不正につき結果を表示できません。(1:".$message_id.")");
            continue;
        }
        $sql = getSwapMessage($sql, 1, $emp_id);
        c2dbResetLastErrorMsg();
        $topRow = c2dbGetTopRow("select * from (".$sql.") d limit 1");
        $err = c2dbGetLastErrorMsg();
        if ($err) {
            $out[$row["disp_place"]][$message_id] = array("msg"=>"", "warn"=>"判定構文が不正につき結果を表示できません。(2:".$message_id.")");
            continue;
        }

        $message_formula = unserialize($row["message_formula"]);
        $msgObj = getOneMessageHtml(unserialize($row["message_formula"]), $topRow, $emp_id);

        if ($msgObj["html"]!="") {
            $muid = $msgObj["formula"]["muid"];
            $menu_code = $msgObj["formula"]["menu_code"];

            $html = $msgObj["html"];
            if ($muid) {
                $html =
                '<a class="with_icon" href="javascript:void(0)" onclick="showBSect('.$muid.',\'\');">'.
                '<img src="./img/spacer.gif" width="20" height="16" alt="" class="profile_link" />'.
                '<div><nobr>'.$html.'</nobr></div></a>';
            } else if ($menu_code) {
                $ids = explode("@", $menu_code);
                $option = "&message_id=".$message_id."&pattern_idx=".$msgObj["pattern_idx"];
                $cls_link = ($ids[1] ? "find_link" : "sinsei_link");
                $html =
                '<a class="with_icon" href="javascript:void(0)" onclick="showSummarySect(\''.$ids[0].$ids[1].'\',\''.$option.'\');">'.
                '<img src="./img/spacer.gif" width="20" height="16" alt="" class="'.$cls_link.'" />'.
                '<div><nobr>'.$html.'</nobr></div></a>';
            }

            $ary = array("msg"=>$msgObj["html"], "warn"=>"", "muid"=>$row["muid"], "html"=>$html);
            $out[$row["disp_place"]][$message_id] = $ary;
        }
    }
    c2dbSetErrorReturn(false); // エラーでdie停止させるように戻す
    return $out;
}

function getOneMessageHtml($message_formula, $row, $emp_id) {
    if (!is_array($message_formula)) return "";
    $pattern_idx = 0;
    foreach ($message_formula as $formula) {
        $pattern_idx++;
        $alias = $formula["field_alias"];
        $joken = $formula["field_joken"];
        if ($joken=="") $joken = "OP_EMPTY";
        $html = $formula["field_html"];
        if (!is_array($row)) $row= array();
        if (!in_array($alias, array_keys($row))) continue;
        $v = $row[$alias];
        if ($v===0) $v = "0";
        $isMatch = checkJoken($v, $joken, $formula["field_value"]);
        if (!$isMatch) continue;
        $html = getSwapMessage($html, "", $emp_id);

        foreach ($row as $f => $v) {
            $html = str_replace('{{'.$f.'}}', hh($v), $html);
        }
        return array("html"=> $html, "formula" =>$formula, "pattern_idx"=>$pattern_idx);
    }
    return array();
}

//------------------------------------
// メッセージ条件、検索時条件の判定。
// $vが、$jokenにマッチする場合は1、そうでなければカラ文字を返す。
// $vはデータ。$compはマスタ登録されている比較検証文字
//------------------------------------
function checkJoken($v, $joken, $comp) {
    $MATCH = 1;
    $NO_MATCH = "";
    if ($v===0) $v = "0";
    if ($joken=="OP_EMPTY" && $v!="") return $NO_MATCH; // カラであるか？
    if ($joken=="OP_EMPTY_OR_ZERO" && $v!="" && $v!="0") return $NO_MATCH; // カラかゼロか？
    if ($joken=="OP_EXIST" && $v=="") return $NO_MATCH; // 値ありか？
    if ($joken=="OP_EQUAL" && $v!==$comp) return $NO_MATCH; // 値が等しいか？
    if ($joken=="OP_NOTEQUAL" && $v===$comp) return $NO_MATCH; // 値が等しくないか？

    if ($comp!="") { // 比較検証文字がマスタ指定されている？
        if ($v!="") { // データがあるか？
            if ($joken=="OP_FRONT"   && strpos($v, $comp)!==0) return $NO_MATCH; // データの先頭が、比較検証文字か？（前方一致）
            if ($joken=="OP_INCLUDE" && strpos($comp, $v)<0) return $NO_MATCH; // 比較検証文字のどこかに、データ文字を含んでいるか？
            if ($joken=="OP_INDEXOF" && strpos($v, $comp)<0) return $NO_MATCH; // データのどこかに、比較検証文字を含んでいるか？

            if (is_numeric($v) && is_numeric($comp)) {
                if ($joken=="OP_OVER"    && $v <= $comp) return $NO_MATCH; // 数値として超か？
                if ($joken=="OP_BIGGER"  && $v <  $comp) return $NO_MATCH; // 数値として以上か？
                if ($joken=="OP_SMALLER" && $v >  $comp) return $NO_MATCH; // 数値として以下か？
                if ($joken=="OP_UNDER"   && $v >= $comp) return $NO_MATCH; // 数値として未満か？
            } else {
                if ($joken=="OP_OVER"    && strcmp($v, $comp) <= 0) return $NO_MATCH; // 文字列として超か？
                if ($joken=="OP_BIGGER"  && strcmp($v, $comp) <  0) return $NO_MATCH; // 文字列として以上か？
                if ($joken=="OP_SMALLER" && strcmp($v, $comp) >  0) return $NO_MATCH; // 文字列として以下か？
                if ($joken=="OP_UNDER"   && strcmp($v, $comp) >= 0) return $NO_MATCH; // 文字列として未満か？
            }
        }
    }
    return $MATCH;
}

// 検索、メッセージ
function getJokenPulldownOptions($joken) {
    $ret = array();
    $ret[]= '<option value="OP_EMPTY"'.($joken=="OP_EMPTY"?" selected":"").'>空値の場合</option>';
    $ret[]= '<option value="OP_EMPTY_OR_ZERO"'.($joken=="OP_EMPTY_OR_ZERO"?" selected":"").'>空値またはゼロの場合</option>';
    $ret[]= '<option value="OP_EXIST"'.($joken=="OP_EXIST"?" selected":"").'>空値でない場合</option>';
    $ret[]= '<option value="OP_EQUAL" useval="1"'.($joken=="OP_EQUAL"?" selected":"").'>所定値と等しい場合</option>';
    $ret[]= '<option value="OP_FRONT" useval="1"'.($joken=="OP_FRONT"?" selected":"").'>所定値と前方一致する場合</option>';
    $ret[]= '<option value="OP_INCLUDE" useval="1"'.($joken=="OP_INCLUDE"?" selected":"").'>所定値の一部に含まれる場合</option>';
    $ret[]= '<option value="OP_INDEXOF" useval="1"'.($joken=="OP_INDEXOF"?" selected":"").'>所定値を一部に含んでいる場合</option>';
    $ret[]= '<option value="OP_NOTEQUAL" useval="1"'.($joken=="OP_NOTEQUAL"?" selected":"").'>所定値と等しくない場合</option>';
    $ret[]= '<option value="OP_OVER" useval="1"'.($joken=="OP_OVER"?" selected":"").'>所定値超の場合</option>';
    $ret[]= '<option value="OP_BIGGER" useval="1"'.($joken=="OP_BIGGER"?" selected":"").'>所定値以上の場合</option>';
    $ret[]= '<option value="OP_SMALLER" useval="1"'.($joken=="OP_SMALLER"?" selected":"").'>所定値以下の場合</option>';
    $ret[]= '<option value="OP_UNDER" useval="1"'.($joken=="OP_UNDER"?" selected":"").'>所定値未満の場合</option>';
    $ret[]= '<option value="OP_MATCH" useval="1"'.($joken=="OP_MATCH"?" selected":"").'>所定値正規表現にマッチした場合</option>';
    $ret[]= '<option value="OP_UNMATCH" useval="1"'.($joken=="OP_UNMATCH"?" selected":"").'>所定値正規表現にマッチしない場合</option>';
    $ret[]= '<option value="OP_SQL" useval="1"'.($joken=="OP_SQL"?" selected":"").'>所定値に構文を指定（SQL）</option>';
    return implode("\n", $ret);
}

function makeWhereLike($field, $vv) {
	$where = "(";
	$where .= $field." ilike ".c2dbStr("%".$vv."%");
	if ($vv[0]=="@" && strlen($vv)>1) { // 先頭が半角「@」なら、完全一致を含める
		$where .= " or ".$field." = ".c2dbStr(substr($vv,1));
	}
	if (mb_substr($vv,0,1)=="＠" && strlen($vv)>2) { // 先頭が全角「＠」なら、完全一致を含める
		$where .= " or ".$field." = ".c2dbStr(substr($vv,2));
	}
	return $where .")";
}

//------------------------------------
// 検索時条件の作成。SQL版。
// $vはデータ。$compはマスタ登録されている比較検証文字
// nullとか面倒
//------------------------------------
function makeJokenSql($f, $joken, $comp, $intOrCharOrBool, $isReverse, $isIncludeEmpty) {

    $iComp = strlen($comp ? (int)$comp : "null");

    // 反転でない場合
    if (!$isReverse) {
        if ($joken=="OP_SQL") { // 構文を指定
            return "(".getSwapMessage($comp, 1, ""). ")";
        }
        if ($joken=="OP_EMPTY") { // カラである
        	if ($intOrCharOrBool=="bool") return "(".$f." is null or ".$f." = 'f')";
            return "(".$f." is null or ".$f." = ". ($intOrCharOrBool=="char" ? "''" : "0") . ")";
        }
        if ($joken=="OP_EMPTY_OR_ZERO") { // カラかゼロ
        	if ($intOrCharOrBool=="bool") return "(".$f." is null or ".$f." = 'f')";
            return "(".$f." is null or ".$f." = ". ($intOrCharOrBool=="char" ? "'0' or ".$f." = ''" : "0") . ")";
        }
        if ($joken=="OP_EXIST" && $v=="") { // 値あり
        	if ($intOrCharOrBool=="bool") return "(".$f." = 't')";
            return $f." is not null and not ".$f." = ". ($intOrCharOrBool=="char" ? "''" : "0");
        }
        if ($joken=="OP_EQUAL" && $v!==$comp) { // 値が等しい
        	if ($intOrCharOrBool=="bool") return "(".$f." = '".(c2Bool($comp)?"t":"f")."')";
            return $f." ".($intOrCharOrBool=="char" ? "=".c2dbStr($comp) : (strlen($comp)?"=".(int)$comp:"is null") );
        }
        if ($joken=="OP_NOTEQUAL" && $v===$comp) { // 値が等しくない
        	if ($intOrCharOrBool=="bool") return "(".$f." = '".(c2Bool($comp)?"f":"t")."')";
            if (strlen($comp)) {
                return $f." = ".($intOrCharOrBool=="char" ? c2dbStr($comp) : (int)$comp);
            } else {
                return $f." is not null and not ".$f."=". ($intOrCharOrBool=="char" ? "''" : "0"); // 「カラと等しくない」は、「値あり」と同義
            }
        }
        if (!strlen($comp)) return "1 <> 1"; // 比較検証文字がマスタ指定されていない。以降は設定不備である。falseになるよう返す。

        // 反転でなく、カラも含める特例の比較条件
        if ($intOrCharOrBool!="bool") {
	        if ($isIncludeEmpty) {
	            if ($intOrCharOrBool=="char") {
	                if ($joken=="OP_FRONT")   return "(".$f." is null or ".$f." ilike ".c2dbStr($comp."%") . ")"; // データの先頭が、比較検証文字である（前方一致）
	                if ($joken=="OP_INCLUDE") return "(".$f." is null or ".c2dbStr($comp)." ilike '%' || ".$f." || '%')"; // 比較検証文字のどこかに、データ文字を含んでいる
	                if ($joken=="OP_INDEXOF") return "(".$f." is null or ".$f." ilike '%".c2dbStr($comp)."%')"; // データのどこかに、比較検証文字を含んでいる
	            }

	            if ($joken=="OP_OVER")    return "(".$f." is null or ".$f." > " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 超である
	            if ($joken=="OP_BIGGER")  return "(".$f." is null or ".$f." >= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 以上である
	            if ($joken=="OP_SMALLER") return "(".$f." is null or ".$f." <= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 以下である
	            if ($joken=="OP_UNDER")   return "(".$f." is null or ".$f." < " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 未満である
	        }
	        // 反転でなく、カラは含めない通常の比較条件
	        else {
	            if ($intOrCharOrBool=="char") {
	                if ($joken=="OP_FRONT")   return $f." ilike ".c2dbStr($comp."%"); // データの先頭が、比較検証文字である（前方一致）
	                if ($joken=="OP_INCLUDE") return c2dbStr($comp)." ilike '%' || ".$f." || '%'"; // 比較検証文字のどこかに、データ文字を含んでいる
	                if ($joken=="OP_INDEXOF") return $f." ilike '%".c2dbStr($comp)."%'"; // データのどこかに、比較検証文字を含んでいる
	            }

	            if ($joken=="OP_OVER")    return $f." > " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 超である
	            if ($joken=="OP_BIGGER")  return $f." >= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 以上である
	            if ($joken=="OP_SMALLER") return $f." <= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 以下である
	            if ($joken=="OP_UNDER")   return $f." < " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 未満である
	        }
	    }
    }
    // 反転の場合
    else {
        if ($joken=="OP_SQL") { // 構文を指定
            return " not (".getSwapMessage($comp, 1, ""). ")";
        }
        if ($joken=="OP_EMPTY") { // カラである⇒値あり（文字列ゼロは含む）
        	if ($intOrCharOrBool=="bool") return "(".$f." = 't')";
            return $f." is not null and not ".$f." = ". ($intOrCharOrBool=="char" ? "''" : "0");
        }
        if ($joken=="OP_EMPTY_OR_ZERO") { // カラかゼロ⇒ゼロ以外
        	if ($intOrCharOrBool=="bool") return "(".$f." = 't')";
            return $f." is not null and not ".$f." = ". ($intOrCharOrBool=="char" ? "'' and not ".$f." = '0'" : "0");
        }
        if ($joken=="OP_EXIST" && $v=="") { // 値あり⇒カラかゼロ
        	if ($intOrCharOrBool=="bool") return "(".$f." is null or ".$f." = 'f')";
            return "(".$f." is null or ".$f." = ". ($intOrCharOrBool=="char" ? "'0' or ".$f." = ''" : "0") . ")";
        }
        if ($joken=="OP_EQUAL" && $v!==$comp) { // 値が等しい⇒値が等しくない
        	if ($intOrCharOrBool=="bool") return "(".$f." = '".(c2Bool($comp)?"f":"t")."')";
            if (strlen($comp)) {
                return $f." = ".($intOrCharOrBool=="char" ? c2dbStr($comp) : (int)$comp);
            } else {
                return $f." is not null and not ".$f."=". ($intOrCharOrBool=="char" ? "''" : "0"); // 「カラと等しくない」は、「値あり」と同義
            }
        }
        if ($joken=="OP_NOTEQUAL" && $v===$comp) { // 値が等しくない⇒値が等しい
        	if ($intOrCharOrBool=="bool") return "(".$f." = '".(c2Bool($comp)?"t":"f")."')";
            return $f." ".($intOrCharOrBool=="char" ? "=".c2dbStr($comp) : (strlen($comp)?"=".(int)$comp:"is null") );
        }
        if (!strlen($comp)) return "1 <> 1"; // 比較検証文字がマスタ指定されていない。以降は設定不備である。falseになるよう返す。

        // 反転で、カラを含める特例の逆 ⇒ つまり反転だがカラを含めない
        if ($intOrCharOrBool!="bool") {
	        if ($isIncludeEmpty) {
	            if ($intOrCharOrBool=="char") {
	                if ($joken=="OP_FRONT")   return $f." not ilike ".c2dbStr($comp."%"); // 反転。
	                if ($joken=="OP_INCLUDE") return c2dbStr($comp)." ilike '%' || ".$f." || '%'"; // 反転。
	                if ($joken=="OP_INDEXOF") return $f." ilike '%".c2dbStr($comp)."%'"; // 反転。
	            }

	            if ($joken=="OP_OVER")    return $f." < " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 超の反転
	            if ($joken=="OP_BIGGER")  return $f." <= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 以上の反転
	            if ($joken=="OP_SMALLER") return $f." >= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 以下の反転
	            if ($joken=="OP_UNDER")   return $f." > " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp); // 未満の反転
	        }
	        // 反転で、カラを含めない ⇒ つまりカラを含めることになる
	        else {
	            if ($intOrCharOrBool=="char") {
	                if ($joken=="OP_FRONT")   return "(".$f." is null or ".$f." not ilike ".c2dbStr($comp."%") . ")"; // 反転。
	                if ($joken=="OP_INCLUDE") return "(".$f." is null or ".c2dbStr($comp)." ilike '%' || ".$f." || '%')"; // 反転。
	                if ($joken=="OP_INDEXOF") return "(".$f." is null or ".$f." ilike '%".c2dbStr($comp)."%')"; // 反転。
	            }

	            if ($joken=="OP_OVER")    return "(".$f." is null or ".$f." < " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 超の反転
	            if ($joken=="OP_BIGGER")  return "(".$f." is null or ".$f." <= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 以上の反転
	            if ($joken=="OP_SMALLER") return "(".$f." is null or ".$f." >= ".($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 以下の反転
	            if ($joken=="OP_UNDER")   return "(".$f." is null or ".$f." > " .($intOrCharOrBool=="char"? c2dbStr($comp) : (int)$comp).")"; // 未満の反転

	        }
	    }
    }
    return "";
}


//------------------------------------
// 評価または確認の場合は、この職員に対して評価/確認を行う職員を取得する。
// 最初に、luid(リスト項目)の判定。ゼロか値のあるものの、大きいほうが優先
// 次に、対象者が役職別に存在すれば役職別を優先利用する。
// 最後に、st_idやatrb_dept_idなど、ゼロか値のあるもののうち、値のあるものが優先。ただし、下位層への考慮が必要。
//
// $empmstRow 評価対象者のempmst
// $listLineupRow ＝ $listLineup[$luid] ＝ 免許、行動評価、役職評価など、リスト単位の定義(spfm_list_lineup)
// $luid 上記のキー
//------------------------------------
function generateWorkflow($emp_id, $target_id, $_need_syozoku, $_need_yakusyoku, $wf_type) { // target_idは、uid(unit_id)またはluid(list_id)
    if (!$_need_syozoku && !$_need_yakusyoku) {
        return array("appr_hier_row" => array(), "appr_emp_rows" => array());
    }

    $whereOr = array();
    $whereAnd = array();
    if ($_need_syozoku) {
        $whereOr[]= " (".
        " appr_kbn='SYOZOKU'".
        " and exists (".
        "     select cv.emp_id from concurrent_view2 cv".
		"     inner join authmst au on (au.emp_id = cv.emp_id and au.emp_del_flg = 'f')".
        "     where cv.emp_id = ".c2dbStr($emp_id).
        "     and (".
        "         fr_last_cadr_hier = '0'".
        "         or ( fr_last_cadr_hier = 'S_CLASS' and fr_last_cadr_id = cv.emp_class) ".
        "         or ( fr_last_cadr_hier = 'S_ATRB' and fr_last_cadr_id = cv.emp_attribute )".
        "         or ( fr_last_cadr_hier = 'S_DEPT' and fr_last_cadr_id = cv.emp_dept )".
        "         or ( fr_last_cadr_hier = 'S_ROOM' and fr_last_cadr_id = cv.emp_room )".
        "     )".
        " ))";
    } else {
        $whereAnd[]= " appr_kbn <> 'SYOZOKU'"; // 「所属別評価を実施する」がオフなら無視
    }
    if ($_need_yakusyoku) {
        $whereOr[]= " (appr_kbn='YAKUSYOKU' and (fr_st_id = 0 or fr_st_id in ( select emp_st from concurrent_view2 where emp_id = ".c2dbStr($emp_id).")))";
    } else {
        $whereAnd[]= " appr_kbn <> 'YAKUSYOKU'"; // 「役職別評価を実施する」がオフなら無視
    }
    //------------------------------------
    // 評価ワークフロー有りかどうかを確認。
    //------------------------------------
    $sql =
    " select d.* from (".
    " select *".
    ",case when appr_kbn = 'SYOZOKU' then fr_last_cadr_hier || fr_last_cadr_id else cast(fr_st_id as varchar) end as code".
    ",case when hier_count = -1 then 1 else 0 end as is_deny_workflow".
    " from spfm_approval_hier".
    " where (target_id = 0 or target_id = ".(int)$target_id.")".
    " and wf_type = '".$wf_type."'".
    " and hier_count is not null".
    " ".(count($whereOr) ? " and (".implode(" or ", $whereOr).")" : "").
    " ".(count($whereAnd) ? " and (".implode(" and ", $whereAnd).")" : "").
    " ) d".
    // まずは全体共通基本設定より、個々のリスト/評価設定を優先。(target_idゼロは基本設定)
    // 次に所属より役職優先(appr_kbnがSよりYが先)
    // その次は[ワークフロー非利用]判定（hier_countがマイナス1は、ワークフロー非利用である）
    // その次は全部署・全役職に準拠より、直接部署や役職を指定しているものを優先（codeに値がないものは、「準拠」である。）
    " order by target_id desc, appr_kbn desc, is_deny_workflow desc, code desc";
    $ahRows = c2dbGetRows($sql);
    $appr_hier_row = array(); // 確認評価タスクがある場合は、これに確認タスクとなる該当行が代入される
    $deny_kbn = array(); // ワークフロー無しに指定されている場合は、そこで終了したい。
    foreach ($ahRows as $ahRow) {
        if ($ahRow["is_deny_workflow"]) {
			return array("appr_hier_row"=>array(), "appr_emp_rows"=>array(), "is_deny_workflow"=>1); // ワークフロー無しなら終わり
			$appr_hier_row = array();
		}
        $appr_hier_row = $ahRow;
        if (!$ahRow["hier_count"]) continue; // 設定省略：準拠の場合
        break;
    }
    if ($appr_hier_row["hier_info"]) $appr_hier_row["hier_info"] = unserialize($appr_hier_row["hier_info"]);

    //------------------------------------
    // この職員に対して評価/確認を行う職員を取得する。
    // 最初に、target_id(シート番号)の判定。ゼロか値のあるものの、大きいほうが優先
    // 次に、対象者が役職別に存在すれば役職別を優先利用する。
    // 最後に、st_idやatrb_dept_idなど、ゼロか値のあるもののうち、値のあるものが優先。ただし、下位層への考慮が必要。
    //------------------------------------
    $appr_emp_rows = getHyokaKakuninsyaList($appr_hier_row["wf_type"], $appr_hier_row, $_need_syozoku, $_need_yakusyoku, $emp_id);
    return array(
        "appr_hier_row" => $appr_hier_row, // (spfm_approval_hier)確認評価があるならその確認を行う役職または所属と、階層稟議形式情報
        "appr_emp_rows" => $appr_emp_rows, // (spfm_approval_emp) 確認評価があるなら確認者一覧（回答は含まない）
        "is_deny_workflow"=>(count($appr_hier_row) ? "" : 1) // 階層定義が無い場合は承認評価なしとする。階層はあるが職員が居ない場合は承認評価ありとする。
    );
}



//------------------------------------
// 対象職員の、対象リストの評価/確認を実際に行う職員一覧を取得
//
// 評価または確認の場合は、この職員に対して評価/確認を行う職員を取得する。
// 最初に、luid(リスト項目)の判定。ゼロか値のあるものの、大きいほうが優先
// 次に、対象者が役職別に存在すれば役職別を優先利用する。
// 最後に、st_idやatrb_dept_idなど、ゼロか値のあるもののうち、値のあるものが優先。ただし、下位層への考慮が必要。
//------------------------------------
function getHyokaKakuninsyaList($wf_type, $appr_hier_hyoka_row, $_need_syozoku, $_need_yakusyoku, $emp_id) {
    if (!$_need_syozoku && !$_need_yakusyoku) return;

    if ($appr_hier_hyoka_row["appr_kbn"]=="SYOZOKU") {
        $code = $appr_hier_hyoka_row["fr_last_cadr_hier"].$appr_hier_hyoka_row["fr_last_hier_id"];
    } else {
        $code = $appr_hier_hyoka_row["fr_st_id"];
    }

	// この「対象」を承認・評価する「対象」リスト
    if ($code=="") $code = "0";
    $sql =
    " select ae.*, em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_nm from spfm_approval_emp ae".
    " left outer join empmst em on (em.emp_id = ae.emp_id_or_syozoku)".
    " where ae.wf_type = '".$wf_type."'".
    " and ae.target_id = ".(int)$appr_hier_hyoka_row["target_id"].
    " and ae.appr_kbn = '".$appr_hier_hyoka_row["appr_kbn"]."'".
    " and (em.emp_id is not null or ae.emp_id_or_syozoku ilike '#%')";
    if ($appr_hier_hyoka_row["appr_kbn"]=="SYOZOKU") {
        $sql .= " and ae.fr_last_cadr_id = ".((int)$appr_hier_hyoka_row["fr_last_cadr_id"])." and ae.fr_last_cadr_hier = ".c2dbStr($appr_hier_hyoka_row["fr_last_cadr_hier"]);
    } else {
        $sql .= " and ae.fr_st_id = ".(int)$appr_hier_hyoka_row["fr_st_id"];
    }
    $rows = c2dbGetRows($sql);
    $appr_emp_hyoka_rows = array();
    $dummy_idx = 0;
    $empRow = c2dbGetTopRow("select * from empmst where emp_id = ".c2dbStr($emp_id)); // 本人

    foreach ($rows as $row) {
        if ($row["appr_kbn"]=="SYOZOKU" && !$_need_syozoku) continue;
        if ($row["appr_kbn"]=="YAKUSYOKU" && !$_need_yakusyoku) continue;

		// 本人が承認する場合
        if ($row["emp_id_or_syozoku"]=="#self") {
            $appr_emp_hyoka_rows[$row["appr_hier"]][$emp_id]= array(
                "syozoku"=>"#self", "syozoku_nm"=>"本人", "is_honnin"=>"1",
                "subhier"=>$row["appr_subhier"], "emp_nm"=>$empRow["emp_lt_nm"]." ".$empRow["emp_ft_nm"]
            );
            continue;
        }
        // 本人所属の、指定役職者が承認する場合
        if (substr($row["emp_id_or_syozoku"],0,9)=="#self_st#") {
            $emp_st = substr($row["emp_id_or_syozoku"],9);
            $class_id = (int)$empRow["emp_class"];
            $ca_id = $class_id."-".(int)$empRow["emp_attribute"];
            $cad_id = $ca_id."-".(int)$empRow["emp_dept"];
            $cadr_id = $cad_id."-".(int)$empRow["emp_room"];
            $code = $row["emp_id_or_syozoku"];

            $sql2 =
            " select em.emp_id, em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_nm".
            " from empmst em".
            " inner join authmst am on (am.emp_id = em.emp_id and am.emp_del_flg = 'f')".
            " inner join concurrent_view2 cv on (cv.emp_id = em.emp_id)".
            " where cv.cadr_id = '".$cadr_id."'".
            " and cv.emp_st = ".$emp_st;
            $rows2 = c2dbGetRows($sql2);
            $syozoku = str_replace(" ", "", getSyozokuDisp($empRow["emp_class"],$empRow["emp_attribute"], $empRow["emp_dept"], $empRow["emp_room"]));
            $stmstRows = getMasterHash("stmst", 1);
            $yakusyoku = $stmstRows[$emp_st];
            foreach ($rows2 as $row2) {
                $appr_emp_hyoka_rows[$row["appr_hier"]][$row2["emp_id"]] = array(
                    "syozoku"=>$code,"syozoku_nm"=>"本人所属（".$syozoku."）の".$yakusyoku,"subhier"=>$row["appr_subhier"], "emp_nm"=>$row2["emp_nm"],
                    "is_honnin_kantoku"=>"1"
                );
            }
            if (!count($rows2)) {
                $dummy_idx++;
                $appr_emp_hyoka_rows[$row["appr_hier"]]["UNKNOWN_EMP_ID".$dummy_idx] = array(
                    "syozoku"=>$code,"syozoku_nm"=>"本人所属（".$syozoku."）の".$yakusyoku,"subhier"=>$row["appr_subhier"], "emp_nm"=>"（該当者なし）",
                    "is_honnin_kantoku"=>"1", "eliminated"=>"1"
                );
            }
            continue;
        }

        // 職員IDでなく、所属役職が指定されている場合。展開して持ってくる
        if (substr($row["emp_id_or_syozoku"],0,1)=="#") {
            $syozoku_yakusyoku = explode("#", substr($row["emp_id_or_syozoku"],1));
            $syozoku = explode("-", $syozoku_yakusyoku[0]);

            $sql2 =
            " select cv.emp_id, em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_nm from empmst em".
            " inner join authmst am on (am.emp_id = em.emp_id and am.emp_del_flg = 'f')".
            " inner join concurrent_view2 cv on (cv.emp_id = em.emp_id)".
            " where 1 = 1".
            " ".($syozoku[0] ? " and cv.emp_class = ".(int)$syozoku[0] : "").
            " ".($syozoku[1] ? " and cv.emp_attribute = ".(int)$syozoku[1] : "").
            " ".($syozoku[2] ? " and cv.emp_dept = ".(int)$syozoku[2] : "").
            " ".($syozoku[3] ? " and cv.emp_room = ".(int)$syozoku[3] : "").
            " ".($syozoku_yakusyoku[1] ? " and cv.emp_st = ".(int)$syozoku_yakusyoku[1] : "");
            $rows2 = c2dbGetRows($sql2);
            foreach ($rows2 as $row2) {
                $code = $row["emp_id_or_syozoku"];
                $appr_emp_hyoka_rows[$row["appr_hier"]][$row2["emp_id"]] = array(
                    "syozoku"=>$code,"syozoku_nm"=>getSyozokuYakusyokuNm($code),"subhier"=>$row["appr_subhier"], "emp_nm"=>$row2["emp_nm"]
                );
            }
            continue;
        }
        $appr_emp_hyoka_rows[$row["appr_hier"]][$row["emp_id_or_syozoku"]] = array("subhier"=>$row["appr_subhier"], "emp_nm"=>$row["emp_nm"]);
    }
    return $appr_emp_hyoka_rows;
}



//--------------------------------------------------------------------------------------------------
// ワークフローの進捗度合いを求める
// spft_profileのkakunin_progressまたはhyoka_progressの値となる。
//--------------------------------------------------------------------------------------------------
// appr_hier_row 階層構造(spfm_approval_hier)
// appr_emp_rows 階層ごとのemp_id(spfm_approval_emp)
// hier_stream_data 実際に入力されたデータ、これまでのデータがマージ済みのもの
// wf_type KAKUNIN or HYOKA
// ilstLineupRow
function getRingiKairanProgress($appr_hier_row, $appr_emp_rows, $hier_stream_data, $wf_type, $sinsei_status, $is_need_sinsei) {
    global $sinsei_status_list;

    // 確認評価が無い。「-10=確認評価なし」で終了
    if (!$appr_hier_row["hier_count"]) return -10;

    // 申請機能がオンであるが、未申請。「1」で終わり
    if ($is_need_sinsei) {
        if ($sinsei_status!="SS_APPLY") return 3; // 申請でない（編集中）。
    }

    $progress = 7; // 申請者は申請済みである場合。あるいは申請操作機能がオフ（自動申請モード）につき、申請する。「7」にする。

    $hier_infos = $appr_hier_row["hier_info"]; // 階層構造
    $hier_count = (int)$appr_hier_row["hier_count"];

    for ($_hier=1; $_hier<=$hier_count; $_hier++) { // 階層ループ
        $is_hier_ok = ""; // この階層がOKになれば1となる
        $is_hier_input = ""; // この階層で誰かが入力を開始していれば1となる
        $hier_emps = $appr_emp_rows[$_hier]; // この階層の職員
        if (!is_array($hier_emps)) return $progress; // この階層には受信者が存在しない。
        $subhier_emps = array();
        foreach ($hier_emps as $emp_id => $_subhier_info) {
            if ($_subhier_info["eliminated"]) { continue; } // この職員は除外操作が行われているのでスルー
            $subhier_emps[$_subhier_info["subhier"]][] = $emp_id; // 確認評価すべき職員 サブ階層ごとに組みなおし
        }
        $tmp = $hier_stream_data[$_hier]; // 入力データ
        if (!is_array($tmp)) return $progress;

        $subhier_stream_emps = array();
        foreach ($tmp as $_emp_id => $eInfo) {
            $subhier_stream_emps[$eInfo["subhier"]][] = array("emp_id"=>$_emp_id, "result"=>$eInfo["result"], "ymdhm"=>$eInfo["ymdhm"]); // サブ階層ごとの入力
        }

        $hier_info = $hier_infos[$_hier-1]; // この階層の構造
        $subhier_type = $hier_info["subhier_type"]; // このhier配下のsubhierグループの、完了形式(連結形式)(AND/OR)

        $emp_ok_count = 0; // このサブ階層での終了者数
        $emp_in_count = 0; // このサブ階層での入力者数
        $subhier_ok_count = 0;
        $subhier_start = 0; // 開始したなら１
        $subhier_reject = 0; // 棄却者がいれば１
        $subhier_return = 0; // 差戻し者がいれば１
        for ($_subhier=1; $_subhier<=(int)$hier_info["subhier_count"]; $_subhier++) {
            //$subhier_type = $hier_info["subhier_type".$_subhier]; // このsubhierが全員合意(AND)またはひとり以上合意(OR)

            $stream_emps = $subhier_stream_emps[$_subhier]; // 確認評価した職員
            $subhier_check = $hier_info["subhier_check".$_subhier]; // このsubhier職員の全会一致なら1、そうでない場合は１人以上でＯＫ
            if (!is_array($stream_emps)) continue;

            foreach ($stream_emps as $seInfo) {
                if ($seInfo["result"]=="") continue; // 未入力ならノーカン
                //if ($seInfo["result"]=="SS_NONE") { continue; } // 未入力ならノーカン
                if ($seInfo["result"]=="SS_CHECK" && $wf_type=="HYOKA") { $emp_ok_count++; break; } // 評価なら何か値があればOK
                if ($seInfo["result"]=="SS_REJECT") { $subhier_reject = 1; break; } // 棄却が存在した。もう他を見る必要は無い。ブレーク
                if ($seInfo["result"]=="SS_RETURN") $subhier_return = 1; // 差し戻しが存在した場合。他に棄却が存在するかもしれないのでこのままループ続行
                //	if ($seInfo["result"]!="SS_APPLY" && $seInfo["result"]!="SS_CHECK") continue; // 送信者の送信済みか受信者の受信済みでなければノーカン
                if (in_array($seInfo["emp_id"], $subhier_emps[$_subhier])) {
					if ($seInfo["result"]=="SS_APPLY" && $seInfo["result"]=="SS_CHECK") $emp_ok_count++;
					if ($seInfo["result"]=="SS_NONE") $emp_in_count++;
				}
            }
            if ($subhier_reject) break; // 棄却がひとりでもみつかったらとにかくアウト
            if ($emp_ok_count) $is_hier_input = 1; // 誰かひとり以上は入力済みである
             // subhierがOR条件。で、ubhier内職員ひとり以上OKにつき、subhierはOK。
            if ($emp_ok_count && !$subhier_check) {

                $subhier_ok_count++;
                if ($subhier_type=="OR") { // hierもor条件。hier自体がOKとなる。
                    $is_hier_ok = 1;
                    break;
                }
            }
            if ($emp_ok_count==count($subhier_emps[$_subhier])) { $is_hier_ok = 1; break; } // subhier内職員が全部OKなら、AND/OR問わずＯＫ。
        }
        if ($subhier_ok_count==(int)$hier_info["subhier_count"]) $is_hier_ok = 1;

        if ($subhier_reject) return $_hier * 10 + 5; // 棄却は一番優先。棄却が本階層内にある複数サブグループのどこかで見つかったのなら終了
        if ($subhier_return) return $_hier * 10 + 1; // 差戻しあり。１の位を１にして終了
        if ($is_hier_ok) {
            if ($_hier==$hier_count) return $_hier * 10 + 9; // 最終階層まですべてＯＫ。1の位を9にして終了。
            $progress = $_hier * 10 + 7; // 階層ごとＯＫ。十以上の位は階層数。プラス、1の位を完了7に。
            continue;
        }
        if ($is_hier_input) {
            return $_hier * 10 + 3; // 誰か入力済みだが不完全。十の位は階層数。場合によっては百の位も。それにプラス、1の位を編集中3にしてここまでで終了。
        }
        if ($emp_in_count) {
            return $_hier * 10 + 3; // 編集開始のみなら＋３。
		}
        return $progress; // 誰も入力していない場合。ここまでの進捗を返す。
    }
    echo "システムエラーが発生しました。（getRingiKairanProgress）";
}


// 対象の職員の階層は、いま時点でステータスを変更できる場合にtrueを返す
// [自分の前階層の７] 〜 [自分の次階層の２] までで操作可能。
// 書式的には以下となる
// (職員hier - 1) x 10 + 7   ≦  progress値  ≦  (職員hier + 1) x 10 + 2

// $progress：現在の進捗
// $hier ：確認したい階層
function isChangeableRingiKairan($progress, $hier) {
    if (getRingiKairanMessage($progress, $hier)) return ""; // 何かエラーがあれば登録操作不可
    return "1";
}

// 申請あるいは承認入力が「出来ない」理由を返す。
// $progress：現在の進捗
// $hier ：確認したい階層
function getRingiKairanMessage($progress, $hier) {
    if ($progress < 0) return "申請対象外"; // ゼロ以下(マイナス10)。申請機能が無い。終了
    if ($progress=="" && $hier==0) return ""; // 新規の場合。自己評価時階層ゼロのとき、progressはカラである。
    if ($progress < ($hier-1) * 10 + 7 && $hier==1) return "未申請"; // 前階層の１の位が7以下なら操作できない。(理由メッセージのみ変えたい)
    if ($progress < ($hier-1) * 10 + 7) return "未到達"; // 前階層の１の位が7以下なら操作できない。(理由メッセージのみ変えたい)
    if ($progress > ($hier+1) * 10 + 2) return "終了"; // 次階層の１の位が2以上なら操作できない。
    return "";
}

// 対象の職員に、ステータス変更通知が必要な場合にtrueを返す
//
// 以下のとき通知したい
// ・前階層完了／前階層の１の位が７以上 　 ：　(職員hier - 1) x 10 + 7  ＝  progress値
// ・同階層編集中／同階層の１の位が５　　　 ：　(職員hier) x 10 + ５  ＝  progress値
// ・次階層で差し戻し／次階層の１の位が1　 ：　(職員hier + 1) x 10 + 1  ＝  progress値
// ただし、サブグループが「ひとり以上」で、ひとり以上が承認済の場合、サブグループ的にＯＫな場合は通知しない。
//
// $progress：現在の進捗
// $hier ：確認したい階層
function isNotifyRingiKairan($progress, $hier, $subhier, $emp_id, $appr_hier_row, $data_stream_this_hier, $eliminated) {
    if ($eliminated) return ""; // 最初から除外済みがわかっている。終了
    if ($progress < 0) return ""; // ゼロ以下。申請機能が無い。終了
    $is_kouho = 0;
    if      ( ($hier-1) * 10 + 7 == $progress) $is_kouho = 1;
    else if ( ($hier)   * 10 + 3 == $progress) $is_kouho = 1;
    else if ( ($hier)   * 10 + 5 == $progress) $is_kouho = 1;
    else if ( ($hier+1) * 10 + 1 == $progress) $is_kouho = 1;
    if (!$is_kouho) return "";

    // サブグループ確認
    // この階層が完了していないことはわかっているので
    // 他のサブグループ状況は確認する必要が無い。
    // この職員が未入力であることもわかっている。
    // 同一職員は同一階層の複数サブグループにまたがれない仕様でもある。
    // 他の職員が差し戻したり棄却したりしていないことはわかっているので、
    // とにかくサブグループが「ひとり以上」でＯＫな設定なら、
    // 完了していないことが問題にならないので通知は不要と判定できる。
    $hier_infos = $appr_hier_row["hier_info"]; // 階層構造
    $hier_info = $hier_infos[$hier-1]; // この階層の構造

    if (!is_array($data_stream_this_hier)) return ""; // （恐らく）この階層には職員が存在しない。文句なしに通知「不要」

    // この階層で引数職員は応答済。通知は「不要」となる
    $emp_result = $data_stream_this_hier[$emp_id]["result"];
    if ($emp_result=="SS_CHECK" || $emp_result=="SS_RETURN" || $emp_result=="SS_REJECT") return "";

    // 誰かが差戻していたり、却下していたら、全会一致でなくとも通知は不要とする。
    foreach ($data_stream_this_hier as $_emp_id => $eInfo) {
        if ($eInfo["result"]=="SS_RETURN") return ""; // subhier関係なく、誰かによる差戻しありにつき、通知なし確定
        if ($eInfo["result"]=="SS_REJECT") return ""; // subhier関係なく、誰かによる却下ありにつき、通知なし確定
    }

    // 以降、引数職員は応答していない状態。かつ、誰も差戻したり却下していない状態。
    // その上で、このsubhier職員の全会一致設定なら、通知は「必須」となる。
    if ($hier_info["subhier_check".$subhier]) return "1";

    // 引数職員は応答していない状態。かつ、誰も差戻したり却下していない状態。
    // その上、全会一致でないので、このsubhier職員の誰かが入力済なら、通知は「不要」となる。
    foreach ($data_stream_this_hier as $_emp_id => $eInfo) {
        if ($eInfo["subhier"]==$subhier && $eInfo["result"]=="SS_CHECK") return ""; // このsubhierで、承認した職員が居た。文句なしに通知「不要」
    }

    return "1";
}

function getSyozokuYakusyokuNm($emp_id_or_syozoku) {
    if (!substr($emp_id_or_syozoku,0,1)=="#") return "";
    $cadr_st = explode("#", substr($emp_id_or_syozoku,1));
    if ($cadr_st[0]=="self_st") return "";
    $cadr = explode("-", $cadr_st[0]);
    $syozoku = str_replace(" ", "", getSyozokuDisp($cadr[0], $cadr[1], $cadr[2], $cadr[3]));
    if ($cadr_st[1]) {
        $stmstRows = getMasterHash("stmst", 1);
        $syozoku .= "の".$stmstRows[$cadr_st[1]];
    }
    return $syozoku;
}

function getAuthYakusyokuNm($emp_id_or_syozoku) {
    if (substr($emp_id_or_syozoku,0,9)=="#self_st#") {
        $code = explode("#", substr($emp_id_or_syozoku,1));
        if ($code[1]) {
            $stmstRows = getMasterHash("stmst", 1);
            return "本人所属の".$stmstRows[$code[1]];
        }
    }
    return "";
}

function getWfTypeJp($wf_type) {
    if ($wf_type=="KAKUNIN" || $wf_type=="kakunin") return "確認";
    if ($wf_type=="HYOKA"  || $wf_type=="hyoka")  return "評価";
    return "";
}



define("USE_SPFM_ATRB", c2dbGetSystemConfig("spf.use_spfm_atrb"));
define("USE_SPFM_DEPT", c2dbGetSystemConfig("spf.use_spfm_dept"));
define("USE_SPFM_STMST", c2dbGetSystemConfig("spf.use_spfm_stmst"));
define("SYOZOKU_STYLE",    c2dbGetSystemConfig("spf.syozoku_style"));


// メッセージ通知や検索構成で埋め込まれるプレースホルダを置換する
function getSwapMessage($str, $is_sql, $emp_id) {
    global $spfNendoStartMMDD;
    $quot = ($is_sql ? "'" : "");
    $str = str_replace("{{PROFILE_EMP_ID}}", $quot.$emp_id.$quot, $str);
    $str = str_replace("{{LOGIN_EMP_ID}}", $quot.C2_LOGIN_EMP_ID.$quot, $str);
    $str = str_replace("{{NENDO_START_MMDD}}", $quot.$spfNendoStartMMDD.$quot, $str);
    $str = str_replace("{{TODAY_YYYYMMDD}}", $quot.date("Ymd").$quot, $str);
    $str = str_replace("{{TODAY_YYYYMMDD_-1_YEAR}}", $quot.date("Ymd", strtotime("-1 year")).$quot, $str);
    $str = str_replace("{{TODAY_YYYYMMDD_+1_YEAR}}", $quot.date("Ymd", strtotime("+1 year")).$quot, $str);
    return $str;
}

function forwardAction() {
    global $EAL;
    $aa = $_REQUEST["ajax_action"];
    $sa = $_REQUEST["screen_action"];
    if (!$aa && !$sa) return;
    if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie(""); // ﾛｸﾞｲﾝ者にspf権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

    if ($sa=="get_work_opm")          { require("ajax_mst_k_env.inc");   get_work_opm();        die; }
    if ($sa=="upload_work_opm")       { require("ajax_mst_k_env.inc");   upload_work_opm();     die; }
    if ($sa=="upload_opm")            { require("ajax_mst_k_env.inc");   upload_opm();          die; }
    if ($sa=="create_opm")            { require("ajax_mst_k_env.inc");   create_opm();          die; }
    if ($sa=="db_download")           { require("ajax_mst_k_env.inc");   db_download();         die; }

    if ($aa=="get_master_selection_by_json")  { get_master_selection_by_json($_REQUEST["rel_master_id"]); die; }
    if ($aa=="get_job_senmon_list_by_json")   { get_job_senmon_list_by_json((int)$_REQUEST["job_id"]);    die; }
    if ($aa=="get_dialog_html")        { require("ajax_user_profile.inc");     get_dialog_html();         die; }
    if ($aa=="get_input_html")         { require("ajax_user_profile.inc");     get_input_html();          die; }
    if ($aa=="get_single_list_html")   { require("ajax_user_profile.inc");     get_single_list_html();    die; }
    if ($aa=="reg_profile")            { require("ajax_user_profile.inc");     reg_profile();             die; }
    if ($aa=="reg_profile_appr_emp")   { require("ajax_user_profile.inc");     reg_profile_appr_emp();    die; }
    if ($aa=="get_hyoka_workflow")     { require("ajax_user_profile.inc");     get_hyoka_workflow();      die; }
    if ($aa=="check_id_unique")        { require("ajax_user_profile.inc");     check_id_unique();         die; }
    if ($aa=="reg_emp_config")         { require("ajax_user_profile.inc");     reg_emp_config();          die; }
    if ($aa=="get_baritess_status")    { require("ajax_user_profile.inc");     get_baritess_status();     die; }



    if ($aa=="get_summary_html")       { require("ajax_user_finder.inc");      get_summary_html();        die; }
    if ($aa=="dlg_emp_selector")       { require("ajax_dlg_emp_selector.inc"); html_dlg_emp_selector();   die; }
    if ($aa=="dlg_emp_selector_search"){ require("ajax_dlg_emp_selector.inc"); dlg_emp_selector_search(); die; }
    if ($aa=="dialog_image_html")      { require("image_uploader.php");        dialog_image_html();       die; }

    if ($sa=="get_user_hyoka")         { require("ajax_mst_d_sqe.inc");        get_user_hyoka();          die; }
    if ($aa=="html_get_user_hyoka")    { require("ajax_mst_d_sqe.inc");        html_get_user_hyoka();     die; }
    if ($aa=="html_mst_hyoka1")         { require("ajax_mst_d_sqe.inc");   html_mst_hyoka1();         die; }
    if ($aa=="load_left_sheet_list")    { require("ajax_mst_d_sqe.inc");   load_left_sheet_list();    die; }
    if ($aa=="load_left_emp_list")      { require("ajax_mst_d_sqe.inc");   load_left_emp_list();      die; }
    if ($aa=="load_left_part")          { require("ajax_mst_d_sqe.inc");   load_left_part();          die; }
    if ($aa=="load_sheet")              { require("ajax_mst_d_sqe.inc");   load_sheet();              die; }
    if ($aa=="load_han")                { require("ajax_mst_d_sqe.inc");   load_han();                die; }
    if ($aa=="load_unit")               { require("ajax_mst_d_sqe.inc");   load_unit();               die; }
    if ($aa=="reg_mst_hyoka_sheet")     { require("ajax_mst_d_sqe.inc");   reg_mst_hyoka_sheet();     die; }
    if ($aa=="reg_mst_hyoka_han")       { require("ajax_mst_d_sqe.inc");   reg_mst_hyoka_han();       die; }
    if ($aa=="reg_mst_copy_term")       { require("ajax_mst_d_sqe.inc");   reg_mst_copy_term();       die; }
    if ($aa=="load_emp_han")            { require("ajax_mst_d_sqe.inc");   load_emp_han();            die; }
    if ($aa=="clear_hyoka_profile")     { require("ajax_mst_d_sqe.inc");   clear_hyoka_profile();     die; }

    if ($aa=="html_mst_hyoka2")         { require("ajax_mst_d_sqe.inc");   html_mst_hyoka2();         die; }
    if ($aa=="load_user_han")           { require("ajax_mst_d_sqe.inc");   load_user_han();           die; }
    if ($aa=="reg_sqe_profile")         { require("ajax_mst_d_sqe.inc");   reg_sqe_profile();         die; }
    if ($aa=="dialog_unit_selector")    { require("ajax_mst_d_sqe.inc");   dialog_unit_selector();    die; }
    if ($aa=="dialog_unit_inner")       { require("ajax_mst_d_sqe.inc");   dialog_unit_inner();       die; }
    if ($aa=="dialog_baritess_selector"){ require("ajax_mst_d_sqe.inc");   dialog_baritess_selector();die; }
    if ($aa=="dialog_baritess_inner")   { require("ajax_mst_d_sqe.inc");   dialog_baritess_inner();   die; }
    if ($aa=="dialog_nshikaku_selector"){ require("ajax_mst_d_sqe.inc");   dialog_nshikaku_selector();die; }
    if ($aa=="dialog_nshikaku_inner")   { require("ajax_mst_d_sqe.inc");   dialog_nshikaku_inner();   die; }
    if ($aa=="dialog_kousyu_selector")  { require("ajax_mst_d_sqe.inc");   dialog_kousyu_selector();  die; }
    if ($aa=="dialog_kousyu_inner")     { require("ajax_mst_d_sqe.inc");   dialog_kousyu_inner();     die; }
    if ($aa=="dialog_kensyu_selector")  { require("ajax_mst_d_sqe.inc");   dialog_kensyu_selector();  die; }
    if ($aa=="dialog_kensyu_inner")     { require("ajax_mst_d_sqe.inc");   dialog_kensyu_inner();     die; }
    if ($aa=="dialog_hyoka_syugi_selector")  { require("ajax_mst_d_sqe.inc");   dialog_hyoka_syugi_selector();  die; }
    if ($aa=="dialog_hyoka_syugi_inner")     { require("ajax_mst_d_sqe.inc");   dialog_hyoka_syugi_inner();     die; }
    if ($aa=="dialog_gakkai_selector")  { require("ajax_mst_d_sqe.inc");   dialog_gakkai_selector();  die; }
    if ($aa=="dialog_gakkai_inner")     { require("ajax_mst_d_sqe.inc");   dialog_gakkai_inner();     die; }
    if ($aa=="dialog_sqe_env")          { require("ajax_mst_d_sqe.inc");   dialog_sqe_env();          die; }
    if ($aa=="reg_sqe_env")             { require("ajax_mst_d_sqe.inc");   reg_sqe_env();             die; }

    //-----------------------------------------------
    // ここから管理者権限・人事権限でないと利用できないアクセス
    //-----------------------------------------------
    if(!IS_ADMIN_AUTH && !IS_JIN_AUTH) {
        echo $aa;
        die;
        c2ShowAlertAndGotoLoginPageAndDie(""); // ﾛｸﾞｲﾝ者にspf管理者権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行
    }

    if ($aa=="html_mst_menu")           { require("ajax_mst_a_menu.inc");     html_mst_menu();           die; }
    if ($aa=="reg_mst_menu" )           { require("ajax_mst_a_menu.inc");     reg_mst_menu();            die; }
    if ($aa=="html_mst_hanyou")         { require("ajax_mst_b_hanyou.inc");   html_mst_hanyou();         die; }
    if ($aa=="reg_mst_hanyou")          { require("ajax_mst_b_hanyou.inc");   reg_mst_hanyou();          die; }
    if ($aa=="dialog_nayose_html")      { require("ajax_mst_b_hanyou.inc");   dialog_nayose_html();      die; }
    if ($aa=="dialog_nayose_inner")     { require("ajax_mst_b_hanyou.inc");   dialog_nayose_inner();     die; }
    if ($aa=="reg_nayose")              { require("ajax_mst_b_hanyou.inc");   reg_nayose();              die; }
    if ($aa=="reg_to_master")           { require("ajax_mst_b_hanyou.inc");   reg_to_master();           die; }
    if ($aa=="nayose_shindan")          { require("ajax_mst_b_hanyou.inc");   nayose_shindan();          die; }
    if ($aa=="html_mst_field_struct")   { require("ajax_mst_c_field.inc");    html_mst_field_struct();   die; }
    if ($aa=="reg_mst_field_struct")    { require("ajax_mst_c_field.inc");    reg_mst_field_struct();    die; }
    if ($aa=="reg_mst_field_add")       { require("ajax_mst_c_field.inc");    reg_mst_field_add();       die; }


    if ($aa=="html_mst_list_struct")    { require("ajax_mst_e_list.inc");     html_mst_list_struct();    die; }
    if ($aa=="reg_mst_list_struct")     { require("ajax_mst_e_list.inc");     reg_mst_list_struct();     die; }
    if ($aa=="html_mst_sect_layout")    { require("ajax_mst_f_sect.inc");     html_mst_sect_layout();    die; }
    if ($aa=="reg_mst_sect_layout")     { require("ajax_mst_f_sect.inc");     reg_mst_sect_layout();     die; }
    if ($aa=="dialog_inst")             { require("ajax_mst_f_sect.inc");     dialog_inst();             die; }
    if ($aa=="reg_inst")                { require("ajax_mst_f_sect.inc");     reg_inst();                die; }
    if ($aa=="html_mst_finder")         { require("ajax_mst_g_finder.inc");   html_mst_finder();         die; }
    if ($aa=="reg_mst_finder_joken")    { require("ajax_mst_g_finder.inc");   reg_mst_finder_joken();    die; }
    if ($aa=="del_mst_finder_joken")    { require("ajax_mst_g_finder.inc");   del_mst_finder_joken();    die; }
    if ($aa=="dialog_mst_finder_kihon") { require("ajax_mst_g_finder.inc");   dialog_mst_finder_kihon(); die; }
    if ($aa=="reg_mst_finder_kihon")    { require("ajax_mst_g_finder.inc");   reg_mst_finder_kihon();    die; }
    if ($aa=="html_mst_approval")       { require("ajax_mst_h_approval.inc"); html_mst_approval();       die; }
    if ($aa=="reg_mst_approval")        { require("ajax_mst_h_approval.inc"); reg_mst_approval();        die; }
    if ($aa=="dialog_approval_use")     { require("ajax_mst_h_approval.inc"); dialog_approval_use();     die; }
    if ($aa=="reg_dialog_approval_use") { require("ajax_mst_h_approval.inc"); reg_dialog_approval_use(); die; }
    if ($aa=="dialog_approval_kbn")     { require("ajax_mst_h_approval.inc"); dialog_approval_kbn();     die; }
    if ($aa=="reg_dialog_approval_kbn") { require("ajax_mst_h_approval.inc"); reg_dialog_approval_kbn(); die; }
    //if ($aa=="reg_dialog_approval_jiko"){ require("ajax_mst_h_approval.inc"); reg_dialog_approval_jiko();die; }
    if ($aa=="dialog_approval_hier")    { require("ajax_mst_h_approval.inc"); dialog_approval_hier();    die; }
    if ($aa=="reg_dialog_approval_hier"){ require("ajax_mst_h_approval.inc"); reg_dialog_approval_hier();die; }
    if ($aa=="reg_dialog_approval_emp") { require("ajax_mst_h_approval.inc"); reg_dialog_approval_emp(); die; }
    if ($aa=="html_mst_message")        { require("ajax_mst_i_message.inc");  html_mst_message();        die; }
    if ($aa=="reg_mst_message")         { require("ajax_mst_i_message.inc");  reg_mst_message();         die; }
    if ($aa=="reg_mst_message_addrow")  { require("ajax_mst_i_message.inc");  reg_mst_message_addrow();  die; }
    if ($aa=="reg_mst_message_delrow")  { require("ajax_mst_i_message.inc");  reg_mst_message_delrow();  die; }
    if ($aa=="html_mst_message_test")   { require("ajax_mst_i_message.inc");  html_mst_message_test();   die; }
    if ($aa=="html_mst_import")         { require("ajax_mst_j_import.inc");   html_mst_import();         die; }
    if ($aa=="html_mst_env")            { require("ajax_mst_k_env.inc");      html_mst_env();            die; }
    if ($aa=="reg_env_showmenu")        { require("ajax_mst_k_env.inc");      reg_env_showmenu();        die; }
    if ($aa=="reg_env_enterprize1")     { require("ajax_mst_k_env.inc");      reg_env_enterprize1();     die; }
    if ($aa=="reg_env_enterprize2")     { require("ajax_mst_k_env.inc");      reg_env_enterprize2();     die; }
    if ($aa=="reg_env_enterprize3")     { require("ajax_mst_k_env.inc");      reg_env_enterprize3();     die; }
    if ($aa=="reg_env_captions")        { require("ajax_mst_k_env.inc");      reg_env_captions();        die; }
    if ($aa=="reg_env_dev_assist")      { require("ajax_mst_k_env.inc");      reg_env_dev_assist();      die; }
    if ($aa=="reg_env_sqe")             { require("ajax_mst_k_env.inc");      reg_env_sqe();             die; }
    if ($aa=="reg_sys_config_entry")    { require("ajax_mst_k_env.inc");      reg_sys_config_entry();    die; }
    if ($aa=="reg_sys_config_exit")     { require("ajax_mst_k_env.inc");      reg_sys_config_exit();     die; }
    if ($aa=="get_spf_backup")          { require("ajax_mst_k_env.inc");      get_spf_backup();          die; }
    if ($aa=="html_mst_sisetu")         { require("ajax_mst_l_sisetu.inc");   html_mst_sisetu();         die; }
    if ($aa=="dialog_mst_sisetu")       { require("ajax_mst_l_sisetu.inc");   dialog_mst_sisetu();       die; }
    if ($aa=="reg_mst_sisetu")          { require("ajax_mst_l_sisetu.inc");   reg_mst_sisetu();          die; }
    if ($aa=="reg_mst_sisetu_emp_list") { require("ajax_mst_l_sisetu.inc");   reg_mst_sisetu_emp_list(); die; }
    if ($aa=="reg_mst_sisetu_del")      { require("ajax_mst_l_sisetu.inc");   reg_mst_sisetu_del();      die; }
    if ($aa=="html_mst_list_def")       { require("ajax_mst_m_list_def.inc"); html_mst_list_def();       die; }
    if ($aa=="reg_mst_list_def")        { require("ajax_mst_m_list_def.inc"); reg_mst_list_def();        die; }

    if ($aa=="html_mst_kantoku")        { require("ajax_mst_n_kantoku.inc");  html_mst_kantoku();        die; }
    if ($aa=="reg_mst_kantoku")         { require("ajax_mst_n_kantoku.inc");  reg_mst_kantoku();         die; }

    else echo "不正なリクエストです。ajax_action=".$aa; // ajax_action該当なし。プログラム実装エラーである
    die;


}
forwardAction();

function getSelectionCaption($pulldown_selections, $vv) {
    $values = explode(",", $pulldown_selections);
    foreach ($values as $val) {
        $kv = explode("=", $val);
        if ($kv[0]!=$vv) continue;
        if ($kv[1]!="") return $kv[1];
        return $vv;
    }
    return $vv;
}
function getSelectionCaptionReverse($pulldown_selections, $vv) {
    $values = explode(",", $pulldown_selections);
    foreach ($values as $val) {
        $kv = explode("=", $val);
        if ($kv[1]!=$vv) continue;
        if ($kv[0]!="") return $kv[0];
        return $vv;
    }
    return $vv;
}


function get_master_selection_by_json($rel_master_id) {
	$hash = getMasterHash($rel_master_id, 1);
	$ary = array();
	foreach ($hash as $id => $v) {
		$ary[]= $v;
	}
    echo "ok".c2ToJson($ary);
}

function get_job_senmon_list_by_json($job_id) {
    echo "ok".c2ToJson(get_job_senmon_list($job_id));
}

function get_job_senmon_list($job_id) {
    $sql =
    " select job_senmon_id, job_senmon_nm from jobmst_senmon".
    " where job_disp_flg = '1'".
    " and job_id = ".(int)$job_id.
    " order by order_no";
    return c2dbGetRows($sql);
}


// 職員マスタ取得
function getEmpmstRow($emp_id) {
    $sql =
    " select *, empmst.emp_id as emp_id from empmst".
    " left outer join spft_empmst on (spft_empmst.emp_id = empmst.emp_id)".
    " where empmst.emp_id = ".c2dbStr($emp_id);
    return c2dbGetTopRow($sql);
}









//****************************************************************************************************************
// リスト行の表示用、セル内データ単体をフォーマットして作成
//****************************************************************************************************************
function getDispHtml(&$profileRow, &$listStructRow, $makeLink="MAKE_FULL", $empProfileDetail=array()) { // MAKE_FULL or NOT_MAKE or MAKE_EXCEPT_IMAGE
    global $fixed_master_conv;
    $table_id = $listStructRow["table_id"];
    $field_id = $listStructRow["field_id"];
    $field_type = $listStructRow["field_type"];
    $fid = $table_id."@".$field_id;
    $cls = $listStructRow["list_align"];
    $val = $profileRow[$table_id."__".$field_id];

    if ($table_id=="other" && $field_id =="rownum") return $val;

    if (($table_id=="spft_profile" && $field_id=="syozoku") || ($table_id=="class_history_view" && $field_id=="syozoku")) {
        return getSyozokuDisp($profileRow[$table_id."__class_id"], $profileRow[$table_id."__atrb_id"], $profileRow[$table_id."__dept_id"], $profileRow[$table_id."__room_id"]);
    }
    if ($table_id=="empmst" && $field_id=="syozoku") {
        return getSyozokuDisp($profileRow[$table_id."__emp_class"], $profileRow[$table_id."__emp_attribute"], $profileRow[$table_id."__emp_dept"], $profileRow[$table_id."__emp_room"]);
    }
    if ($field_id=="baritess_contents_id") {
        if (!$val) return "";
        $total_status = $profileRow["study_test_status__total_status"];
        if (!strlen($total_status)) return "";
        if ($total_status) $stat = "受講済";
        if ($total_status === "0") $stat = "(未修)";
        if ($makeLink=="NOT_MAKE") return $stat;
        return '<a href="javascript:void(0)" class="baritess" onclick="popupBaritessContentsView('.$val.')"><nobr>'.$stat.'</nobr></a>';
    }
    if ($field_id=="is_kouza_obligate") {
        $stat = "";
        if ($val=="0") $stat = "−";
        if ($val=="1") $stat = "必須要件";
        return $stat;
    }
    if ($field_type=="image") {
        $name_field = $table_id."__original_file_name";
        if ($field_id=="image_id1" || $field_id=="image_id2" || $field_id=="image_id3" || $field_id=="image_id4") {
            $name_field = "spft_image".substr($field_id,8)."__original_file_name";
        }
        $original_file_name = $profileRow[$name_field];
        if ($val && !$original_file_name) { // 画像IDがあるのに画像名が無い場合（ダイアログでの保存時はこの状態）
            $original_file_name = c2dbGetOne("select original_file_name from spft_image where image_id = ".(int)$val);
        }
        if ($val && $makeLink=="MAKE_FULL") {
            $dlInfo = genDownloadTicket($val, $original_file_name);
            return ''.
            '<a class="inline_icon" target="_blank" href="./'.$dlInfo["rel_url"].'" style="line-height:16px; padding:2px 0">'.
            '<img src="./img/spacer.gif" width="20" height="16" alt="" class="download_link" />'.
            c2wbr($original_file_name).'</a>';
        } else {
            return hh($original_file_name);
        }
    }
    if ($field_type=="pulldown") {
        if ($listStructRow["rel_master_id"]) {
            if (!strlen($val)) return "";
            $hash = getMasterHash($listStructRow["rel_master_id"], 1);
            return $hash[$val];
        }
        if ($listStructRow["pulldown_selections"]) {
            $val = getSelectionCaption($listStructRow["pulldown_selections"], $val);
            return hh($val);
        }
        return '(pulldown-value no format)'.$fid;
    }
    if ($fixed_master_conv[$fid]) {
        if ($val!="") {
            $new_val = $fixed_master_conv[$fid][$val];
            $val = ($new_val!="" ? $new_val : "(".$val.")");
        }
        return $val;
    }
    if ($listStructRow["field_type"]=="yymm") {
        return c2ConvYmd($val, "slash_ym");
    }
    if ($listStructRow["field_type"]=="ymdhm") {
        return c2ConvYmd($val, "slash_ymd") . " " . c2ConvYmd($val, "zhh_mi");
    }
    if ($listStructRow["field_type"]=="ymd") {
        return convYmdWrap($val);
    }
    return hh($val); // 残りは text or label or textarea
}



//****************************************************************************************************************
// DBデータ用に整形する
//****************************************************************************************************************
function getRawValue($table_id, $field_id, $val, $structRow) {
    global $fixed_master_conv;
    $field_type = $structRow["field_type"];
    $fid = $table_id."@".$field_id;
    $cls = $structRow["list_align"];

    if ($field_type=="text") {
        return $val;
    }
    if ($field_type=="pulldown") {
        if ($structRow["rel_master_id"]) {
            if (!strlen($val)) return "";
            $hash = getMasterHashReverse($structRow["rel_master_id"]);
            return $hash[$val];
        }
        if ($structRow["pulldown_selections"]) {
            $val = getSelectionCaption($structRow["pulldown_selections"], $val);
            return hh($val);
        }
        return $val;
    }
    if ($structRow["field_type"]=="yymm") {
        return eliminateYmSlash($val);
    }
    if ($structRow["field_type"]=="ymdhm") {
        $obj = c2ConvYmd($val);
        return $obj["ymd"].$obj["hh"].$obj["mi"].$obj["ss"];
    }
    if ($structRow["field_type"]=="ymd") {
        return eliminateYmdSlash($val);
    }
    return $val;
}









//****************************************************************************************************************
// 入力パーツ単体を作成
// 非表示かどうかは、この関数の呼び元で制御されるものとする。
//****************************************************************************************************************
function _getInputFieldHtml($empmstRow, $luid, $structRow, $dbdata, $table_id, $field_id, $field_type, $puid, $other_key, $fid, $ownerId) {

    $emp_id = $empmstRow["emp_id"];
    $val = $dbdata[$table_id."__".$field_id];
    global $fixed_master_conv;
    $is_hissu = ($structRow["is_hissu"] ? " is_hissu":"");
    $rel_master_id = $structRow["rel_master_id"];

    $field_width = $structRow["field_width"];
    //$fid = $table_id."@".$field_id."@".$luid."@".$puid."@".$other_key;

    if (!$field_width) $field_width = "100";

    $mst_field_type = $structRow["field_type"];
    $field_label = str_replace("<br>","",$structRow["field_label"]);
	$tag_title = (SHOW_TFID ? ' title="tfid='.$fid.'"' : "");
    if ($field_type=="label") {
        return '<th'.$tag_title.' class="label_box" style="width:'.$field_width.'px"><div class="'.$structRow["label_align"].'"><nobr>'.$field_label.'</nobr></div></th>'."\n";
    }
    if ($field_id=="emp_photo") return "";
    if ($field_type!="field") return ""; // label or list なら終了

    $html = array();
    $html[]= '<td'.$tag_title.' style="width:'.$field_width.'px;">'; //  title="'.$fid.'"

    // 編集可能かどうか
    // 編集不可の場合は、<HIDDEN>+ラベル、とする。
    $is_display  = (count(isDispOrEditable("display", $empmstRow, $structRow)) ? 1 : "");

    if (!$is_display) {
        $html[]= '<div class="label_box hide_box" navititle="秘匿項目" navi="閲覧権限がありません"></div></td>';
        return implode("", $html);
    }

    $is_editable = (count(isDispOrEditable("editable", $empmstRow, $structRow)) ? 1 : "");

    $ime_style = "";
    if ($structRow["ime_off"]) $ime_style .= "; ime-mode:disabled";
    else if ($mst_field_type=="ymd") $ime_style .= "; ime-mode:disabled";
    else if ($mst_field_type=="ymdhm") $ime_style .= "; ime-mode:disabled";

    //==================================
    // 画像入力項目（アップローダへのリンク）
    //==================================
    if ($mst_field_type=="image") {
        $file_name = $dbdata[$table_id."__original_file_name"];
        if ($field_id=="image_id1" || $field_id=="image_id2" || $field_id=="image_id3" || $field_id=="image_id4") {
            $file_name = $dbdata["spft_image".substr($field_id,8)."__original_file_name"];
        }
        $field_width = 160;
        $html[]= '<table cellspacing="0" cellpadding="0"><tr><td style="padding:0">';
         // 画像コンテンツのコンテナ。画像変更を行うと、このDIVタグのinnerHTMLが書き換えられる。(callbackImageSelected関数)
        $html[]= '<div class="label_box" style="overflow:hidden; width:'.$field_width.'px" id="div'.$fid.'">';
        if ($val && $file_name) {
            $dlInfo = genDownloadTicket($val, $file_name);
            $html[]= '<a class="with_icon" target="_blank" href="./'.$dlInfo["rel_url"].'">';
            $html[]= '<img src="./img/spacer.gif" width="20" height="16" alt="" class="download_link" />';
            $html[]= '<div><nobr>'.hh($file_name).'</nobr></div></a>';
        }
        $html[]= '</div></td>';

        if ($is_editable) {
            $html[]= '<td style="padding:0">';
            $html[]= '<input type="hidden" name="'.$fid.'" id="'.$fid.'" value="'.$val.'" field_label="'.$field_label.'" />';
            $html[]= '<button type="button" onclick="popupImageDialog(\''.$val.'\', \''.$fid.'\', ee(\'div'.$fid.'\'))">…</button></td>';
        }
        $html[]= "</tr></table></td>\n";
        return implode("", $html);
    }
    //==================================
    // プルダウン入力項目
    //==================================
    if ($mst_field_type=="pulldown") {

        $selections = array();

        if ($field_id=="confirm_status") {
            global $sinsei_status_list;
            if (!$sinsei_status_list[$val]) $val = "SS_NONE";
            $send_or_resv = ($emp_id==C2_LOGIN_EMP_ID ? "send": "recv");
            $rows = explode(",", $sinsei_status_list[$val][$send_or_resv]);
            $selections[$val] = $sinsei_status_list[$val]["jp"];
            foreach ($rows as $k) {
                if (!$k) continue;
                $selections[$k] = $sinsei_status_list[$k]["act"];
            }
            if ($is_editable) {
                if ($is_hissu) $html[]= '<div style="position:relative">';
                $html[]= '<select id="'.$fid.'" name="'.$fid.'" class="'.$is_hissu.'" style="'.$field_width.'"';
                $html[]= ' defval="'.$val.'" onchange="watchDiff(this, \''.$ownerId.'\')">';
                foreach ($selections as $k => $v) {
                    $html[]= '<option value="'.hh($k).'"'.($k==$val?" selected":"").'>'.hh($v).'</option>';
                }
                $html[]= '</select>';
                if ($is_hissu) $html[]= '<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" /></div>';
                $html[]= '</td>';
            } else {
                foreach ($selections as $k => $v) {
                    if ($k!=$val) continue;
                    $html[]= '<div class="label_box" navititle="読取専用" navi="編集権限がありません"><nobr>'.hh($v)."</nobr></div></td>";
                    break;
                }
            }
            return implode("", $html);
        }
        if ($field_id=="job_senmon_id") {
            $ids = explode("@", $fid);
            $rows = get_job_senmon_list($dbdata[$ids[0]."__job_id"]);
            if ($is_editable) {
                if ($is_hissu) $html[]= '<div style="position:relative">';
                $html[]= '<select id="'.$fid.'" name="'.$fid.'" style="'.$field_width.'" class="'.$is_hissu.'"';
                $html[]= ' defval="'.$val.'" onchange="watchDiff(this, \''.$ownerId.'\')">';
                $html[]= '<option value=""></option>';
                foreach ($rows as $row) {
                    $k = $row["job_senmon_id"];
                    $nm = $row["job_senmon_nm"];
                    $html[]= '<option value="'.hh($k).'"'.($k==$val?" selected":"").'>'.hh($nm).'</option>';
                }
                $html[]= '</select>';
                if ($is_hissu) $html[]= '<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" /></div>';
                $html[]= '</td>';
            } else {
                foreach ($rows as $row) {
                    $k = $row["hyoka_han_id"];
                    if ($k!=$val) continue;
                    $html[]= '<div class="label_box" navititle="読取専用" navi="編集権限がありません"><nobr>'.hh($row["job_senmon_nm"])."</nobr></div></td>";
                    break;
                }
            }
            return implode("", $html);
        }
        else if ($field_id=="") {
            $srow = $sinsei_status_list[$val];
            $html[]= '<td><span style="color:'.$srow["clr"].'">'.$srow["jp"].'</span></td>';
            return implode("", $html);
        }
        if ($rel_master_id) {
            $onchange = "";
            $allow_empty = 1;
            if (substr($rel_master_id, -2)=="--") {
				$rel_master_id = substr($rel_master_id, 0, strlen($rel_master_id)-2);
				$allow_empty = "";
			}
			$selection = getMasterHash($rel_master_id, 1);
            if ($rel_master_id == "jobmst") $onchange = 'onchange="watchDiff(this, \''.$ownerId.'\');changeJobId(this)"';
            $html[]= make_select_tag($fid, $field_width, $selection, $val, $is_editable, $onchange, $ownerId, $is_hissu, $field_label, $allow_empty)."</td>";
            return implode("", $html);
        }
        $renzoku_combo = ($field_id=="syozoku");
        $syozoku_pattern = array("class_id", "atrb_id", "dept_id", "room_id");
        if ($table_id=="empmst") $syozoku_pattern = array("emp_class", "emp_attribute", "emp_dept", "emp_room");
        if ($renzoku_combo) $field_id = ($table_id=="empmst" ? "emp_class": "class_id");

        $v_class = $dbdata[$table_id."__".$syozoku_pattern[0]];
        $v_atrb  = $dbdata[$table_id."__".$syozoku_pattern[1]];
        $v_dept  = $dbdata[$table_id."__".$syozoku_pattern[2]];
        $v_room  = $dbdata[$table_id."__".$syozoku_pattern[3]];
        $class_id = str_replace($field_id, $syozoku_pattern[0], $fid);

        $atrb_id = str_replace($field_id, $syozoku_pattern[1], $fid);
        $dept_id = str_replace($field_id, $syozoku_pattern[2], $fid);
        $room_id = str_replace($field_id, $syozoku_pattern[3], $fid);
        if ($field_id=="class_id" || $field_id=="emp_class") {
            $html[]= make_select_tag($class_id, $field_width, getMasterHash("classmst", 1), $v_class, $is_editable, 'onchange="watchDiff(this, \''.$ownerId.'\');"', $ownerId, $is_hissu, $field_label, 1);
            if (!$renzoku_combo) { $html[]= "</td>"; return implode("", $html); }
        }
        for ($idx=1; $idx<=2; $idx++) {
            if ( c2bool(SYOZOKU_STYLE!="SINRYOKA_TO_BUSYO") == c2bool($idx==1)) {
                if ($renzoku_combo) $field_id = ($table_id=="empmst" ? "emp_attribute": "atrb_id");
                if ($field_id=="atrb_id" || $field_id=="emp_attribute") {
                    $html[]= make_select_tag($atrb_id, $field_width, getMasterHash("atrbmst", 1, $v_class), $v_atrb, $is_editable, 'onchange="watchDiff(this, \''.$ownerId.'\');"', $ownerId, $is_hissu, $field_label, 1);
                    if (!$renzoku_combo) { $html[]= "</td>"; return implode("", $html); }
                }
            } else {
                if ($renzoku_combo) $field_id = ($table_id=="empmst" ? "emp_dept": "dept_id");
                if ($field_id=="dept_id" || $field_id=="emp_dept") {
                    $html[]= make_select_tag($dept_id, $field_width, getMasterHash("deptmst", 1, $v_atrb), $v_dept, $is_editable, ' onchange="watchDiff(this, \''.$ownerId.'\');"', $ownerId, $is_hissu, $field_label, 1);
                    if (!$renzoku_combo) { $html[]= "</td>"; return implode("", $html); }
                }
            }
        }
        if ($renzoku_combo) $field_id = ($table_id=="empmst" ? "emp_room": "room_id");
        if ($field_id=="room_id" || $field_id=="emp_room") {
            $html[]= make_select_tag($room_id, $field_width, getMasterHash("classroom", 1, $v_room), $v_room, $is_editable, "", $ownerId, $is_hissu, $field_label, 1)."</td>";
            return implode("", $html);
        }

        $ary = explode(",", $structRow["pulldown_selections"]);
        foreach ($ary as $opt) {
            if ($opt=="") continue;
            $keyval = explode("=",$opt);
            if (count($keyval)==1) $keyval[1] = $keyval[0];
            $selections[$keyval[0]] = $keyval[1];
        }
        if (!count($selections) && $structRow["selections_by_sql"]) {
            $ary = c2dbGetRows($structRow["selections_by_sql"], "GetRowsOrdered");
            foreach ($ary as $opt) {
                $selections[$opt[0]] = $opt[0];
            }
        }
        if (count($selections)) {
            $html[]= make_select_tag($fid, $field_width, $selections, $val, $is_editable, "", $ownerId, $is_hissu, $field_label, 1)."</td>";
            return implode("", $html);
        }
        $html[]= '(選択肢はありません)'.$field_id."</td>";
        return implode("", $html);
    }

    //==================================
    // 要加工の特殊入力or表示項目
    //==================================
    $tfid = $table_id."@".$field_id;
    if ($field_id=="zaisyoku_nensu") {
        $html[]='<div class="label_box" navititle="読取専用/自動算出" id="'.$fid.'">'.c2GetElapseYear($dbdata["empmst__emp_join"], date("Ymd"), "年")."</div></td>";
        return implode("", $html);
    }
    else if ($field_id=="emp_age") {
        $html[]='<div class="label_box" navititle="読取専用/自動算出" id="'.$fid.'">'.c2GetElapseYear($dbdata["empmst__emp_birth"], date("Ymd"), "歳")."</div></td>";
        return implode("", $html);
    }
    else if ($fixed_master_conv[$tfid]) {
        if ($val!="") {
            $new_val = $fixed_master_conv[$tfid][$val];
            $val = ($new_val!="" ? $new_val : "(".$val.")");
        }
    }
    //==================================
    // 「年月日」または「年月日時分」入力項目。カレンダつき。時分あわせてテキストボックス３個
    //==================================
    if ($mst_field_type=="ymd" || $mst_field_type=="ymdhm") {
        if (!$is_editable) {
            $slash_val = "";
            if ($mst_field_type=="ymd") $slash_val = convYmdWrap($val);
            else if ($mst_field_type=="ymdhm") $slash_val = c2ConvYmd($val, "slash_ymdhm");
            $html[]='<div class="label_box '.$mst_field_type.'_navi" navititle="読取専用" navi="編集権限がありません" >'.hh($slash_val).'</div></td>';
        } else {
            $slash_val = "";
            $ymd_ary = c2ConvYmd($val);
            if ($mst_field_type=="ymd" || $mst_field_type=="ymdhm") $slash_val = $ymd_ary["slash_ymd"];
            else $slash_val = $ymd_ary["slash_ym"];

            $setEllapseJs = "";
            if ($field_id=="emp_birth") {
                $fids = explode("@", $fid);
                $fids[0]="other";
                $fids[1]="emp_age";
                $dest_fid = implode("@", $fids);
                $setEllapseJs = "setElapseYear(this, '', '".$dest_fid."', '歳');";
            }
            if ($field_id=="emp_join") {
                $fids = explode("@", $fid);
                $fids[1]="emp_retire";
                $to_ymd = implode("@", $fids);
                $fids[0]="other";
                $fids[1]="zaisyoku_nensu";
                $dest_fid = implode("@", $fids);
                $setEllapseJs = "setElapseYear(this, ee('ymd#".$to_ymd."'), '".$dest_fid."', '年');";
            }
            if ($field_id=="emp_retire") {
                $fids = explode("@", $fid);
                $fids[1]="emp_join";
                $to_ymd = implode("@", $fids);
                $fids[0]="other";
                $fids[1]="zaisyoku_nensu";
                $dest_fid = implode("@", $fids);
                $setEllapseJs = "setElapseYear(ee('ymd#".$to_ymd."'), this, '".$dest_fid."', '年');";
            }

            $html[]='<div style="position:relative"><nobr>';
            $html[]='<input type="hidden" name="'.$fid.'" id="'.$fid.'" value="'.$val.'" field_label="'.$field_label.'" />';
            $html[]='<input type="text" class="text imeoff'.$is_hissu.'" id="ymd#'.$fid.'" style="width:'.($field_width-30).'px;'.$ime_style.'"';
            $html[]=' field_label="'.$field_label.'"';
            $html[]=' regexp="ymd" ymd_term="'.$structRow["format_regexp"].'" maxlength="10"';
            $html[]=' onchange="watchDiff(this, \''.$ownerId.'\'); calcYmd(\''.$fid.'\');'.$setEllapseJs.'"';
            $html[]=' onkeyup="watchDiff(this, \''.$ownerId.'\', 1);'.$setEllapseJs.'"';
            $html[]=' value="'.$slash_val.'" defval="'.$slash_val.'" maxlength="'.$structRow["max_length"].'" />';
            if ($is_hissu) $html[]='<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" />';
            $html[]='<button type="button" class="calendar_btn" onclick="CAL.showCalendar(\'ymd#'.$fid.'\')"></button>';
            if ($mst_field_type=="ymdhm") {
                $html[]='&nbsp;<input type="text" class="text imeoff'.$is_hissu.'" id="hh#'.$fid.'" style="width:30px;'.$ime_style.'"';
                $html[]=' value="'.$ymd_ary["hh"].'" defval="'.$ymd_ary["hh"].'" maxlength="2"';
                $html[]=' regexp="hh" field_label="'.$field_label.'"';
                $html[]=' onchange="watchDiff(this, \''.$ownerId.'\');  calcYmd(\''.$fid.'\');'.$setEllapseJs.'"';
                $html[]=' onkeyup="watchDiff(this, \''.$ownerId.'\', 1);'.$setEllapseJs.'" />';
                $html[]='：';
                $html[]='<input type="text" class="text imeoff'.$is_hissu.'" id="mi#'.$fid.'" style="width:30px;'.$ime_style.'"';
                $html[]=' regexp="mi" field_label="'.$field_label.'"';
                $html[]=' value="'.$ymd_ary["mi"].'" defval="'.$ymd_ary["mi"].'" maxlength="2"';
                $html[]=' onchange="watchDiff(this, \''.$ownerId.'\'); calcYmd(\''.$fid.'\');'.$setEllapseJs.'"';
                $html[]=' onkeyup="watchDiff(this, \''.$ownerId.'\', 1);'.$setEllapseJs.'" />';
            }
            $html[]='</nobr></div></td>';
        }
        return implode("", $html);
    }
    //==================================
    // 「年月」入力項目。テキストボックス２個
    //==================================
    if ($mst_field_type=="yymm") {
        if (!$is_editable) {
            $html[]='<div class="label_box yymm_navi" navititle="読取専用" navi="編集権限がありません">'.hh(c2ConvYmd($val, "slash_ym")).'</div></td>';
        } else {
            $ymd_ary = c2ConvYmd($val);

            $html[]='<input type="hidden" name="'.$fid.'" id="'.$fid.'" value="'.$val.'" field_label="'.$field_label.'" />';
            $html[]='<table cellspacing="" cellpading="" class="table_ym_input"><tr>';
            $html[]='<td><div style="position:relative"><nobr>';
            $html[]='<input type="text" class="text imeoff'.$is_hissu.'" id="yyyy#'.$fid.'" style="width:50px;'.$ime_style.'"';
            $html[]=' value="'.$ymd_ary["yy"].'" defval="'.$ymd_ary["yy"].'" maxlength="4" field_label="'.$field_label.'"';
            $html[]=' regexp="yyyy" ym_term="'.$structRow["format_regexp"].'"';
            $html[]=' onchange="watchDiff(this, \''.$ownerId.'\');  calcYymm(\''.$fid.'\');"';
            $html[]=' onkeyup="watchDiff(this, \''.$ownerId.'\', 1);" />';
            if ($is_hissu) $html[]='<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" />';
            $html[]='</nobr></div></td><th>年</th><td><div style="position:relative"><nobr>';
            $html[]='<input type="text" class="text imeoff'.$is_hissu.'" id="mm#'.$fid.'" style="width:30px;'.$ime_style.'"';
            $html[]=' regexp="mm" ym_term="'.$structRow["format_regexp"].'" field_label="'.$field_label.'"';
            $html[]=' value="'.$ymd_ary["mm"].'" defval="'.$ymd_ary["mm"].'" maxlength="2"';
            $html[]=' onchange="watchDiff(this, \''.$ownerId.'\'); calcYymm(\''.$fid.'\');"';
            $html[]=' onkeyup="watchDiff(this, \''.$ownerId.'\', 1);" />';
            if ($is_hissu) $html[]='<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" />';
            $html[]='</nobr></div></td><th>月</th></tr></table>';
            $html[]='</td>';
        }
        return implode("", $html);
    }
    //==================================
    // テキスト入力項目
    //==================================
    if ($mst_field_type=="text") {
        if (!$is_editable) {
            $html[]='<div class="label_box" navititle="読取専用" navi="編集権限がありません">'.hh($val).'</div></td>';
        } else {
            $text_width = $field_width-8;
            $html[]='<div style="position:relative"><nobr>';
            if ($structRow["pulldown_selections"]) $text_width -= 40;
            $html[]='<input type="text" class="text'.$is_hissu.'" name="'.$fid.'" id="'.$fid.'" style="width:'.$text_width."px;".$ime_style.'"';
            $html[]=' regexp="'.$structRow["format_regexp"].'" field_label="'.$field_label.'"';
            $html[]=' value="'.hh($val).'" maxlength="'.$structRow["max_length"].'" defval="'.hh($val).'"';
            $html[]=' onchange="watchDiff(this, \''.$ownerId.'\')" onkeyup="watchDiff(this, \''.$ownerId.'\', 1)" />';

            if ($structRow["rel_master_id"]) {
                $html[]='<button type="button" class="kouho_btn" navi="候補" onclick="popupAssistRelMasterDialog(\''.$fid.'\', \''.
                hh(c2js($structRow["rel_master_id"])).'\')"></button>';
            }
            else if ($structRow["pulldown_selections"]) {
                $html[]='<button type="button" class="kouho_btn" navi="候補" onclick="popupAssistDialog(\''.$fid.'\', \''.
                hh(c2js($structRow["pulldown_selections"])).'\')"></button>';
            }
            else if ($structRow["selections_by_sql"]) {
                $_rows = c2dbGetRows($structRow["selections_by_sql"], "GetRowsOrdered");
                $ary = array();
                foreach ($_rows as $_row) $ary[]= $_row[0];
                $html[]='<button type="button" class="kouho_btn" navi="候補" onclick="popupAssistDialog(\''.$fid.'\', \''.
                hh(c2js(implode(",", $ary))).'\')"></button>';
            }
            if ($is_hissu) $html[]='<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" />';
            $html[]='</nobr></div></td>';
        }
        return implode("", $html);
    }
    //==================================
    // テキストエリア入力項目
    //==================================
    if ($mst_field_type=="textarea") {
        if (!$is_editable) {
			if (!strlen($val)) $val = " ";
            $html[]='<div class="label_box" style="height:auto; padding:3px" navititle="読取専用" navi="編集権限がありません">'.hh($val).'</div></td>';
        } else {
            $html[]='<div style="position:relative">';
            $html[]='<textarea id="defval#'.$fid.'" field_label="'.$field_label.'" style="display:none"';
            $html[]=' maxlength="'.$structRow["max_length"].'">'.hh($val).'</textarea>';
            $html[]='<textarea name="'.$fid.'" id="'.$fid.'" style="width:'.($field_width-8)."px;".$ime_style.'" field_label="'.$field_label.'"';
            $html[]='onchange="watchDiff(this, \''.$ownerId.'\')" onkeyup="watchDiff(this, \''.$ownerId.'\', 1)"';
            $html[]=' maxlength="'.$structRow["max_length"].'" class="'.$is_hissu.'">'.hh($val).'</textarea>';
            if ($is_hissu) $html[]='<img src="img/hissu.gif" alt="" class="is_hissu" navi="必須入力です" />';
            $html[]='</div></td>';
            return implode("", $html);
        }
    }
    //==================================
    // ラベル項目
    //==================================
    if ($mst_field_type=="label") {
        $html[]='<div class="'.$structRow["text_align"].' label_box">'.hh($val).'</div></td>'; return implode("", $html);
    }
    $html[]='</td>'."\n";
    return implode("", $html);
}




//****************************************************************************************************************
// セレクト文を作成（プロファイル用）
// 検索や申請受付系はここでは扱わない
//****************************************************************************************************************
function _createSelectSql($listLineupRows, $listStructRows, $empmstRow, $luid, $puid, $other_keys, $table_preference) {
    $listLineup = $listLineupRows[$luid];
    $emp_id = $empmstRow["emp_id"];
    $tables = array();
    $select = array();
    $from = array();
    $where = array();
    $fixed_where = array();
    // テーブルの簡易優先順（その１）
    // ユーザ画面の基本構成である。どちらかというと以下のパターン
    // ・各種主軸となり得るマスタがあればそれを一覧表示
    // ・そこにプロファイル登録データがあれば紐つけ
    // ・それに職員を紐つける
    $table_order = array(
        "spfm_hyoka_items", "study_test_status",
        "spft_profile", "class_history_view", "st_history_view", "study_contents_status_view",
        "jobmst_senmon", "spft_image1", "spft_image2", "spft_image3", "spft_image4",
        "empmst", "spft_empmst", "login", "authmst", "authgroup", "menugroup", "dispgroup",
        "spft_workflow", "other"
    );
    // テーブルの簡易優先順（その２）
    // 職員ありきのデータ一覧を取得するパターン
    if ($table_preference=="preference_empmst") {
        $table_order = array(
            "empmst", "spft_empmst", "spft_profile", "spfm_hyoka_items", "study_test_status",
            "class_history_view", "st_history_view", "study_contents_status_view",
            "jobmst_senmon", "spft_image1", "spft_image2", "spft_image3", "spft_image4",
            "spft_workflow", "other", "login", "authmst", "authgroup", "menugroup", "dispgroup"
        );
    }

    // セレクト句を作成、利用テーブルも知りたい。
    $use_rownum = 0;
    foreach ($listStructRows as $row) {
        if (!$row) {
            echo 'プログラムエラー／SELECTテーブル定義が空値です。';
            die;
        }
        //if ($row["table_id"]=="other" && $row["field_id"]=="rownum") {
        //    $use_rownum = 1;
        //    continue;
        //}
        if (!in_array($row["table_id"], $table_order)) {
            echo 'データ設計エラー／未対応のテーブルが指定されました。セレクト文が作成できません。<br>';
            echo 'このテーブルを一覧表示に加えたい場合は、JOIN順に注意しながら$table_order変数へテーブルIDを追加してください。';
            echo '(非対応テーブルID：'.$row["table_id"].")";
            die;
        }
        if ($row["field_id"]=="jisseki_kaisu") {
            $select[]= "spfm_hyoka_items.use_jisseki_kaisu";
            $tables["spfm_hyoka_items"] = 1;
        }
        if ($row["field_id"]=="baritess_contents_id") {
            $select[]= "study_test_status.total_status";
            $tables["study_test_status"] = 1;
        }
        if ($row["field_id"]=="syozoku") {
            if ($row["table_id"]=="spft_profile" || $row["table_id"]=="class_history_view") {
                $select[]= $row["table_id"].".class_id";
                $select[]= $row["table_id"].".atrb_id";
                $select[]= $row["table_id"].".dept_id";
                $select[]= $row["table_id"].".room_id";
            }
            if ($row["table_id"]=="empmst") {
                $select[]= $row["table_id"].".emp_class";
                $select[]= $row["table_id"].".emp_attribute";
                $select[]= $row["table_id"].".emp_dept";
                $select[]= $row["table_id"].".emp_room";
            }
        } else {
            $select[]= $row["table_id"].".".$row["field_id"];
        }
        if ($row["field_id"]=="image_id1") {
            $select[]= "spft_image1.original_file_name";
            $tables["spft_image1"] = 1;
        } else if ($row["field_id"]=="image_id2") {
            $select[]= "spft_image2.original_file_name";
            $tables["spft_image2"] = 1;
        } else if ($row["field_id"]=="image_id3") {
            $select[]= "spft_image3.original_file_name";
            $tables["spft_image3"] = 1;
        } else if ($row["field_id"]=="image_id4") {
            $select[]= "spft_image4.original_file_name";
            $tables["spft_image4"] = 1;
        }
        $tables[$row["table_id"]] = 1;
    }
    if ($tables["spfm_hyoka_items"]) {
        $select[]= "spfm_hyoka_items.is_progress_row";
        $select[]= "spfm_hyoka_items.section_division";
        $select[]= "spfm_hyoka_items.hyoka_unit_id";
        $select[]= "spfm_hyoka_items.hyoka_han_id";
        $select[]= "spfm_hyoka_items.huid";
    }
    if ($tables["spft_profile"]) {
        $select[]= "spft_profile.puid";
        $select[]= "spft_profile.huid";
        $select[]= "spft_profile.workflow_scheme";
        $select[]= "spft_profile.progress";
        $select[]= "spft_profile.data_stream";
        $select[]= "spft_profile.sinsei_status";
        $select[]= "spft_profile.sinsei_ymdhms";
        $select[]= "spft_profile.update_ymdhms";
        $select[]= "spft_profile.wf_rowtype";
    }

    array_unique($select);

    $foreigns = array();
    if (in_array("class_history_view.emp_id", $select))    $foreigns["emp_id"] = "class_history_view__emp_id";
    //if (in_array("class_history_view.start_ymd", $select)) $foreigns["start_ymd"] = "class_history_view__start_ymd";
    if (in_array("class_history_view.unique_id", $select)) $foreigns["unique_id"] = "class_history_view__unique_id";
    if (in_array("empmst.emp_id", $select))    $foreigns["emp_id"] = "empmst__emp_id";
    if (in_array("empmst.emp_job", $select))   $foreigns["emp_job"] = "empmst__emp_job";
    if (in_array("jobmst_senmon.job_senmon_id", $select))    $foreigns["job_senmon_id"] = "jobmst_senmon__job_senmon_id";
    if (in_array("jobmst_senmon.job_senmon_nm", $select))    $foreigns["job_senmon_id"] = "jobmst_senmon__job_senmon_nm";

    if (in_array("spfm_hyoka_items.disp_hyoka_number", $select))    $foreigns["disp_hyoka_number"] = "spfm_hyoka_items__disp_hyoka_number";
    if (in_array("spfm_hyoka_items.huid", $select))    $foreigns["huid"] = "spfm_hyoka_items__huid";
    if (in_array("spfm_hyoka_items.hyoka_han_id", $select))    $foreigns["hyoka_han_id"] = "spfm_hyoka_items__hyoka_han_id";
    if (in_array("spfm_hyoka_items.hyoka_unit_id", $select))    $foreigns["hyoka_unit_id"] = "spfm_hyoka_items__hyoka_unit_id";
    if (in_array("spfm_hyoka_items.section_division", $select))    $foreigns["section_division"] = "spfm_hyoka_items__section_division";
    if (in_array("spft_empmst.emp_id", $select))   $foreigns["emp_id"] = "empmst__emp_id";
    if (in_array("spft_empmst.job_senmon_id", $select))   $foreigns["job_senmon_id"] = "empmst__job_senmon_id";
    if (in_array("spfm_hyoka_target.hyoka_han_id", $select))    $foreigns["hyoka_han_id"] = "spfm_hyoka_target__hyoka_han_id";
    if (in_array("spfm_hyoka_target.hyoka_unit_id", $select))    $foreigns["hyoka_unit_id"] = "spfm_hyoka_target__hyoka_unit_id";
    if (in_array("spft_profile.huid", $select))   $foreigns["huid"] = "spft_profile__huid";
    if (in_array("spft_profile.hyoka_han_id", $select))   $foreigns["hyoka_han_id"] = "spft_profile__hyoka_han_id";
    if (in_array("spft_profile.hyoka_unit_id", $select))   $foreigns["hyoka_unit_id"] = "spft_profile__hyoka_unit_id";
    if (in_array("spft_profile.puid", $select))   $foreigns["puid"] = "spft_profile__puid";
    if (in_array("st_history_view.emp_id", $select))   $foreigns["emp_id"] = "st_history_view__emp_id";
    if (in_array("st_history_view.unique_id", $select))   $foreigns["unique_id"] = "st_history_view__unique_id";

    // テーブルを検索優先順に並べ替えつつ、from句とwhere句を作成
    $join_err_msg = "データ設計エラー／未対応の検索組合せが指定されました。JOIN条件を実装する必要があります。";
    $appended_table_ids = array();
    foreach ($table_order as $table_id) {
        if (!$tables[$table_id]) continue;
        // １個目のテーブル
        if (!count($from)) {
            $from[] = $table_id;
            if ($table_id=="spfm_hyoka_items") {
                $where[]= " and spfm_hyoka_items.del_flg = 0";
                if ($listLineup["hyoka_unit_id"]) $where[]= " and spfm_hyoka_items.hyoka_unit_id = ".(int)$listLineup["hyoka_unit_id"];
                if ($other_keys["huid"]!="") $where[]= " and spfm_hyoka_items.huid = ".(int)$other_keys["huid"];
            } else if ($table_id=="spft_profile") {
                $where[]= " and spft_profile.del_flg = 0";
                $where[]= " and spft_profile.luid = ".(int)$luid;
                if ($emp_id) $where[]= " and spft_profile.emp_id = ".c2dbStr($emp_id);
                if ((int)$puid) $where[]= " and spft_profile.puid = ".(int)$puid;
                if ($puid=="new") $where[]= " and spft_profile.puid is null";
                if ($other_keys["puid_parent"] && $other_keys["puid_parent"]=="new") $where[]= " and spft_profile.puid_parent is null";
                if ($other_keys["puid_parent"] && $other_keys["puid_parent"]!="new") $where[]= " and spft_profile.puid_parent = ".(int)$other_keys["puid_parent"];
                if ($other_keys["luid_parent"]) $where[]= " and spft_profile.luid_parent = ".(int)$other_keys["luid_parent"];

            } else if ($table_id=="class_history_view") {
                if ($emp_id) $where[]= " and class_history_view.emp_id = ".c2dbStr($emp_id);
                if ($other_keys["unique_id"]!="") $where[]= " and class_history_view.unique_id = ".c2dbStr($other_keys["unique_id"]);

            } else if ($table_id=="st_history_view") {
                if ($emp_id) $where[]= " and st_history_view.emp_id = ".c2dbStr($emp_id);
                if ($other_keys["unique_id"]!="") $where[]= " and st_history_view.unique_id = ".c2dbStr($other_keys["unique_id"]);

            } else if ($table_id=="study_contents_status_view") {
                if ($emp_id) $where[]= " and study_contents_status_view.emp_id = ".c2dbStr($emp_id);

            } else if ($table_id=="empmst") {
                if ($emp_id) $where[]= " and empmst.emp_id = ".c2dbStr($emp_id);

            } else {
                echo "データ設計エラー／規定外の検索組合せが指定されました。(基本from-".$table_id.")";
                die;
            }
            if ($fixed_where[$table_id]) $where = array_merge($where, $fixed_where[$table_id]);
        }
        // ２個目以降、JOINされるテーブル
        else {
            if ($table_id=="spft_profile") {
                $from[]= " left outer join spft_profile on (";
                if (in_array("spfm_hyoka_items", $appended_table_ids)) {
                    $from[]=
                    " spft_profile.del_flg = 0".
                    " and spft_profile.huid = spfm_hyoka_items.huid".
                    " and spft_profile.luid = ".(int)$luid;
                    if ($emp_id) $from[]= " and spft_profile.emp_id = ".c2dbStr($emp_id);
                } else if (in_array("empmst", $appended_table_ids)) {
                    $from[]= " spft_profile.emp_id = empmst.emp_id";
                    if ($emp_id) $from[]= " and spft_profile.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                if ($fixed_where["spft_profile"]) $from = array_merge($from, $fixed_where["spft_profile"]);
                if ($other_keys["puid_parent"] && $other_keys["puid_parent"]=="new") $from[]= " and spft_profile.puid_parent is null";
                if ($other_keys["puid_parent"] && $other_keys["puid_parent"]!="new") $from[]= " and spft_profile.puid_parent = ".(int)$other_keys["puid_parent"];
                if ($other_keys["luid_parent"]) $from[]= " and spft_profile.luid_parent = ".(int)$other_keys["luid_parent"];
                if ((int)$puid) $from[]= " and spft_profile.puid = ".(int)$puid;
                $from[]= " )";

            } else if ($table_id=="study_contents_status_view") {
                $from[]= " left outer join study_contents_status_view on (";
                if (in_array("empmst", $appended_table_ids)) {
                    $from[]= " study_contents_status_view.emp_id = empmst.emp_id";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                $from[]= " )";
            } else if ($table_id=="class_history_view") {
                $from[]= " left outer join class_history_view on (";
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " spft_profile.emp_id = class_history_view.emp_id";
                    if ($emp_id) $from[]= " and class_history_view.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                $from[]= " )";
            } else if ($table_id=="st_history_view") {
                $from[]= " left outer join st_history_view on (";
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " spft_profile.emp_id = st_history_view.emp_id";
                    if ($emp_id) $from[]= " and st_history_view.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                $from[]= " )";
            } else if (preg_match("/^spft_profile[0-9]+$/", $table_id)) {
                $_luid = substr($table_id, 12);
                $from[]= " left outer join spft_profile ".$table_id." on (";
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]=
                    " ".$table_id.".del_flg = 0".
                    " and ".$table_id.".luid  = ".$_luid.
                    " and ".$table_id.".wf_rowtype = 'PROGRESS'".
                    " and ".$table_id.".puid_parent = spft_profile.puid";
                    if ($emp_id) $from[]= " and ".$table_id.".emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                $from[]= " )";
            } else if ($table_id=="study_test_status") {
                $from[]= " left outer join study_test_status on (";
                if (in_array("spfm_hyoka_items", $appended_table_ids)) {
                    $from[]= " study_test_status.test_id = spfm_hyoka_items.baritess_contents_id";
                    if ($emp_id) $from[]= " and study_test_status.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                if ($fixed_where["study_test_status"]) $from = array_merge($from, $fixed_where["study_test_status"]);
                $from[]= " )";
            } else if ($table_id=="spft_empmst") {
                $from[]= " left outer join spft_empmst on (";
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " spft_empmst.emp_id = spft_profile.emp_id";
                    if ($emp_id) $from[]= " and spft_empmst.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                if ($fixed_where["spft_empmst"]) $from = array_merge($from, $fixed_where["spft_empmst"]);
                $from[]= " )";
            } else if ($table_id=="jobmst_senmon") {
                $from[]= " left outer join jobmst_senmon on (";
                if (in_array("spft_empmst", $appended_table_ids)) {
                    $from[]= " jobmst_senmon.job_senmon_id = spft_empmst.job_senmon_id";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                if ($fixed_where["study_test_status"]) $from = array_merge($from, $fixed_where["study_test_status"]);
                $from[]= " )";
            } else if ($table_id=="empmst") {
                $from[]= " left outer join empmst on (";
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " empmst.emp_id = spft_profile.emp_id";
                    if ($emp_id) $from[]= " and empmst.emp_id = ".c2dbStr($emp_id);
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
                if ($fixed_where["empmst"]) $from = array_merge($from, $fixed_where["empmst"]);
                $from[]= " )";
            } else if ($table_id=="spft_image1") {
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " left outer join spft_image spft_image1 on (";
                    $from[]= "     spft_image1.image_id = spft_profile.image_id1";
                    $from[]= " )";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
            } else if ($table_id=="spft_image2") {
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " left outer join spft_image spft_image2 on (";
                    $from[]= "     spft_image2.image_id = spft_profile.image_id2";
                    $from[]= " )";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
            } else if ($table_id=="spft_image3") {
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " left outer join spft_image spft_image3 on (";
                    $from[]= "     spft_image3.image_id = spft_profile.image_id3";
                    $from[]= " )";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
            } else if ($table_id=="spft_image4") {
                if (in_array("spft_profile", $appended_table_ids)) {
                    $from[]= " left outer join spft_image spft_image4 on (";
                    $from[]= "     spft_image4.image_id = spft_profile.image_id4";
                    $from[]= " )";
                } else {
                    echo $join_err_msg."(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                    die;
                }
            } else {
                echo $join_err_msg."(2)(JOINテーブル：".$table_id.")(既出テーブル：".implode(",", $appended_table_ids).")";
                die;
            }
        }
        $appended_table_ids[] = $table_id; // 出現したテーブルを格納
    }

    // オーダーバイを作成
    $order_by1 = array(); // テーブルエイリアスつき
    $order_by2 = array(); // テーブルエイリアス無し
    if ($tables["spfm_hyoka_items"]) {
        $order_by1[]= "spfm_hyoka_items.hyoka_order";
        $order_by2[]= "hyoka_order";
    }
    for ($idx=1; $idx<=3; $idx++) {
        foreach ($listStructRows as $row) {
            if ((int)substr($row["order_by"],0,1)==$idx) {
                $order_by1[] = $row["table_id"].".".$row["field_id"]." ".substr($row["order_by"],2);
                $order_by2[] = $row["field_id"]." ".substr($row["order_by"],2);
            }
        }
    }
    $select_as = array();
    foreach ($select as $tfid) {
        list($table_id, $field_id) = explode(".", $tfid);
        $select_as[] = $tfid." as ".$table_id."__".$field_id;
    }
    if (!count($order_by)) {
        if ($tables["spft_profile"]) {
            $order_by1[]= "spft_profile.puid desc";
            $order_by2[]= "puid desc";
        }
    }
    array_unique($select_as);

    $base_sql =
    " select\n".implode(",\n", $select_as)."\n".
    " from ".implode("\n", $from)."\n".
    " where 1 = 1\n".implode("\n", $where)."\n";

    $count_sql = "select count(*) from (".$base_sql.") d";

    $full_sql =
    " select\n".implode(",\n", $select_as)."\n".
    " from ".implode("\n", $from)."\n".
    " where 1 = 1\n".implode("\n", $where)."\n".
    (count($order_by1) ? " order by ".implode(", ", $order_by1) : "");

    return array(
        "sql"=>$full_sql,
        "count_sql"=>$count_sql,
        "select_field"=>$select,
        "foreigns" => $foreigns,
    );
}










function isInputInvalid($table_id, $row, $import_sousa, $msg_prefix) {
    $errors = array();
    if ($table_id == "spft_profile") {
        //if (!$row["luid"]) $errors[]= $msg_prefix.'リストIDが不明です。(システムエラー)';
        if (!$row["emp_id"]) $errors[]= $msg_prefix.'職員管理IDが不明です。';
    }
    if ($table_id == "empmst") {
        $wherekey = "emp_personal_id";
        $whereval = $row["emp_personal_id"];
        if ($row["emp_id"]) {
            $wherekey = "emp_id";
            $whereval = $row["emp_id"];
        }
        $row["emp_kn_ft_nm"] = mb_convert_kana($row["emp_kn_lt_nm"], "HVc");
        $row["emp_kn_ft_nm"] = mb_convert_kana($row["emp_kn_ft_nm"], "HVc");
        if ($row["emp_lt_nm"]=="")                  $errors[]= $msg_prefix.'苗字（漢字）を入力してください。';
        else if (strlen($row["emp_lt_nm"]) > 20)    $errors[]= $msg_prefix.'苗字（漢字）が長すぎます。';
        if ($row["emp_ft_nm"]=="")                  $errors[]= $msg_prefix.'名前（漢字）を入力してください。';
        else if (strlen($row["emp_ft_nm"]) > 20)    $errors[]= $msg_prefix.'名前（漢字）が長すぎます。';
        if ($row["emp_kn_lt_nm"]=="")               $errors[]= $msg_prefix.'苗字（ひらがな）を入力してください。';
        else if (strlen($row["emp_kn_lt_nm"]) > 20) $errors[]= $msg_prefix.'苗字（ひらがな）が長すぎます。';
        if ($row["emp_kn_ft_nm"]=="")               $errors[]= $msg_prefix.'名前（ひらがな）を入力してください。';
        else if (strlen($row["emp_kn_ft_nm"]) > 20) $errors[]= $msg_prefix.'名前（ひらがな）が長すぎます。';
        if ($row["emp_birth"] && !c2Checkdate($row["emp_birth"]))   $errors[]= $msg_prefix.'生年月日が不正です。';
        if ($row["emp_join"] && !c2Checkdate($row["emp_join"]))     $errors[]= $msg_prefix.'入職日が不正です。';
        if ($row["emp_retire"] && !c2Checkdate($row["emp_retire"])) $errors[]= $msg_prefix.'退職日が不正です。';
        if (strlen($row["emp_idm"]) > 32) $errors[]= $msg_prefix.'ICカードIDmが長すぎます。';
        else if ($row["emp_idm"] != "") {
            if ($import_sousa=="imp_mod") {
                $sql =
                " select count(*) from empmst where emp_idm = ".c2dbStr($row["emp_idm"]).
                " and ".$wherekey." <> ".c2dbStr($whereval).
                " and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')";
                if ((int)c2dbGetOne($sql) > 0) $errors[]= $msg_prefix.'入力されたICカードIDmは、すでに使われています。';
            } else {
                $sql =
                " select count(*) from empmst where emp_idm = ".c2dbStr($row["emp_idm"]).
                " and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')";
                if ((int)c2dbGetOne($sql) > 0) $errors[]= $msg_prefix.'入力されたICカードIDmは、すでに使われています。';
            }
        }

        if ($row["emp_personal_id"]!="") {
            if ($is_update) {
                $sql = "select count(*) from empmst where emp_personal_id = ".c2dbStr($row["emp_personal_id"])." and emp_id <> ".c2dbStr($row["emp_id"]);
            } else {
                $sql = "select count(*) from empmst where emp_personal_id = ".c2dbStr($row["emp_personal_id"]);
            }
            if ((int)c2dbGetOne($sql) > 0) $errors[]= $msg_prefix.'指定された職員IDは既に使用されています。';
        }
    }
    return $errors;
}



// emp_login_id emp_login_pass emp_login_mail
function validEmpmst($field_id, $vv, $jp, $ins_or_upd, $emp_id, $pass_length) {
    $vv = trim($vv);
    if (!$jp) $jp = $field_id; // 名前がない。フィールドIDを名前として使用

	// smallint型。不正ならエラーにせず、null登録
	$comedix_int_or_null = array("emp_class", "emp_attribute", "emp_dept", "emp_room", "emp_job", "emp_st");
	if (in_array($field_id, $comedix_int_or_null)) {
		if ((int)$vv < 1 || (int)$vv > 32767) $vv = "NULL";
		else $vv = (int)$vv;
		return array("value"=>$vv);
	}
	// 性別は1or2or3。不正はエラーにせず、3とする
    if ($field_id=="emp_sex") {
		if ($vv!="1" && $vv!="2") $vv = "3";
		$vv = "'".$vv."'";
		return array("value"=>$vv);
	}
	// 真偽値。不正はエラーにせず、falseとする
	$comedix_boolean = array("emp_pic_flg", "emp_login_flg");
	if (in_array($field_id, $comedix_boolean)) {
		$vv = (c2Bool($vv) ? "'t'" :"'f'");
		return array("value"=>$vv);
	}

    if ($field_id=="emp_kn_ft_nm") $vv = mb_convert_kana($vv, "HVc");
    if ($field_id=="emp_kn_lt_nm") $vv = mb_convert_kana($vv, "HVc");

	// 空欄不可
	$comedix_hissu = array(
		"emp_id", "emp_personal_id",
		"emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm",
		"emp_login_id", "emp_login_pass", "emp_login_email"
	);
	if (in_array($field_id, $comedix_hissu) && !strlen($vv)) return array("error"=>$jp.'を入力してください。');

	// 文字列長エラー
	$comedix_lengths = array(
		"emp_id" => 12, "emp_personal_id"=>12, "emp_ft_nm"=>20, "emp_lt_nm"=>20, "emp_kn_lt_nm"=>20, "emp_kn_ft_nm"=>20,
		"emp_zip1"=>3, "emp_zip2"=>4, "emp_prv"=>2, "emp_addr1"=>100, "emp_addr2"=>100, "emp_ext"=>10,
		"emp_tel1"=>6, "emp_tel2"=>6, "emp_tel3"=>6, "emp_mobile1"=>6, "emp_mobile2"=>6, "emp_mobile3"=>6,
		"emp_email"=>120, "emp_m_email"=>120, "emp_sex"=>1, "emp_birth"=>8, "emp_join"=>8, "emp_retire"=>8,
		"emp_keywd"=>40, "emp_email2"=>120, "emp_phs"=>6, "emp_idm"=>32,
		"emp_login_id"=>20, "emp_login_mail"=>20
	);
	if ($field_id=="emp_login_pass") {
		if ($len>0) $comedix_lengths[$field_id] = $len;
	}
	$max_len = (int)$comedix_length[$field_id];
	if ($max_len > mb_strlen($vv)) return array("error"=>$jp."が長すぎます。");

	// 年月日エラー
	$comedix_yyyymmdd = array("emp_birth", "emp_join", "emp_retire", "pass_change_date");
    if (strlen($vv) && in_array($field_id, $comedix_yyyymmdd) && !c2Checkdate($vv)) return array("error"=>$jp.'が不正です。');

    if ($field_id=="emp_idm" && strlen($vv)) {
        $sql =
        " select count(*) from empmst where emp_idm = ".c2dbStr($vv).
        ($ins_or_upd=="update" ? " and emp_id <> ".c2dbStr($emp_id) : "").
        " and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')";
        if ((int)c2dbGetOne($sql) > 0) return array("error"=>$jp.'は、既に使用されています。');
    }

    if ($field_id=="emp_personal_id") {
        $sql = "select count(*) from empmst where emp_personal_id = ".c2dbStr($vv).
        ($ins_or_upd=="update" ? " and emp_id <> ".c2dbStr($emp_id) : "");
        if ((int)c2dbGetOne($sql) > 0) return array("error"=>$jp.'は、既に使用されています。');
    }
    if ($field_id=="emp_login_id") {
        $sql = "select count(*) from login where emp_login_id = ".c2dbStr($vv).
        ($ins_or_upd=="update" ? " and emp_id <> ".c2dbStr($emp_id) : "");
        if ((int)c2dbGetOne($sql) > 0) return array("error"=>$jp.'は、既に使用されています。');
    }
	if (!strlen($vv)) return array("value"=>"''");
	return array("value"=>$vv, "is_param"=>1);
}
function validLoginTable($field_id, $ret, $emp_id, $site_id, $use_cyrus) {
    if ($field_id=="emp_login_id") {
		if ($site_id=="" && preg_match("/[^0-9a-z_-]/", $ret) > 0) return 'ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。';
		if ($site_id!="" && preg_match("/[^0-9a-zA-Z_-]/", $ret) > 0) return 'ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。';
		if (substr($ret, 0, 1) == "-") return 'ログインIDの先頭に「-」は使えません。';
		if ($site_id=="" && ($ret=="cyrus" || $ret=="postmaster" || $ret=="root")) return '「'.$ret.'」はログインIDとして使用できません。';
	}
	if ($field_id=="emp_login_pass") {
		if (preg_match("/[^0-9a-zA-Z_-]/", $ret) > 0) return 'パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。';
		if (substr($ret, 0, 1) == "-") return 'パスワードの先頭に「-」は使えません。';
	}
	if ($field_id=="emp_login_mail" && $site_id != "") {
        if ($use_cyrus) {
            if (preg_match("/[^0-9a-z_-]/", $ret) > 0) return 'メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。';
        } else {
            if (preg_match("/[^0-9a-z_.-]/", $ret) > 0) return 'メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。';
        }
        if (substr($ret, 0, 1) == "-") return 'メールIDの先頭に「-」は使えません。';
    }

	// メールID重複チェック
	if ($site_id!="") {
		$sql =
		" select count(*) from login inner join authmst on login.emp_id = authmst.emp_id".
		" where login.emp_login_mail = '$mail_id' and authmst.emp_del_flg = 'f'";
		if ((int)c2dbGetOne($sql) > 0) return '指定されたメールIDは既に使用されています。';
	}
}

function regEmpMst() {
    $c2App = c2GetC2App();
    $iconCount = $c2App->getHeaderIconCount();

    //----------名前情報----------
    $keywd = get_keyword($row["lt_kana_nm"]);
    //----------Transaction begin----------
    pg_query($con, "begin");
/*
    //----------最初に新しいIDとパスワードを取得する----------
    $cond = "";
    $sel = select_from_table($con,$SQL23,$cond,$fname);         //max値を取得
    $max = pg_result($sel,0,"max"); //max値を取得する
    $val = substr($max,4,12)+1;     //max値からID番号の部分を取り出しインクリメント
    $yrs = date("y");               //職員IDの頭二桁
    $mth = date("m");               //職員IDの次の二桁
    $yymm ="$yrs"."$mth";           //職員IDの頭４桁を作成

    for($i=8;$i>strlen($val);$i--) $zero=$zero."0";

    $new_emp_id = "$yymm$zero$val"; //新しい職員ID
*/
    //----------login----------
//  $sql =
//  " insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg, emp_login_mail) values (".
//  " ".c2dbStr($new_emp_id).", ".c2dbStr($row["emp_login_id"]).", ".$row["emp_login_pass"].", TRUE, ".c2dbStr($row["mail_id"]).")";
//  c2dbExec($sql);
    //----------authmst----------
    c2dbExec("insert into authmst (emp_id, emp_pass_flg, emp_aprv_auth) values(".c2dbStr($new_emp_id).", TRUE, FALSE)");
    //---------- option ----------
    c2dbExec("insert into option(emp_id, schedule1_default, schedule2_default ) values(".c2dbStr($new_emp_id).", 2, 2)");
    //---------- mygroupmst ----------
    c2dbExec("insert into mygroupmst (owner_id, mygroup_id, mygroup_nm) values (".c2dbStr($new_emp_id).",1, 'マイグループ')");
/*
    //----------empmstテーブルに挿入する----------
    $sql =
    " insert into empmst(".
    " emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept".
    ",emp_room, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm".
    ",emp_sex, emp_birth, emp_join, emp_keywd, emp_idm, emp_profile ) values ( ";
    $content_emp = array($new_emp_id, $personal_id, $cls, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $keywd, $row["idm"], $profile) ;
    $in_emp = insert_into_table($con, $sql, $content_emp, $fname);

*/
    // 勤務シフト作成のリアルタイム同期対応
    $sync_flag = c2Bool(c2dbGetOne("select sync_flag from duty_shift_option"));
    if ($sync_flag) {
        $sql =
        " select group_id from duty_shift_sync_link".
        " where class_id = $cls and atrb_id = $atrb and dept_id = $dept and room_id".($room=="" ? "is null" : " = ".$room);
        $group_id = c2dbGetOne($sql);
        if ($group_id) {
            $cnt = 1 + (int)c2dbGetOne("select count(*) from duty_shift_staff where group_id = ".c2dbStr($group_id));
            $sql =
            " insert into duty_shift_staff (group_id, emp_id, no) values (".
            " ".c2dbStr($group_id).
            ",".c2dbStr($new_emp_id).
            ",".$cnt.")";
            c2dbExec($sql);
        }
    }

    // XOOPSアカウントの作成
    require_once("webmail/config/config.php");

    // 権限グループの更新
//    $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);

//    require_once("class/Cmx/Core2/C2App.php");
    c2dbBeginTrans();
    // 権限グループの更新
    $xoops_err = auth_change_group_of_employee($con, $new_emp_id, $auth_group_id);
    if ($xoops_err) {
        c2dbRollback();
        pg_query("rollback");
        pg_close($con);
    }

    // 権限整合性・ライセンスのチェック
    $c2app = c2GetC2App();
    $err = $c2app->authCheckConsistency(C2_LOGIN_EMP_ID);
    if ($err) {
        c2dbRollback();
        pg_query("rollback");
        pg_close($con);
        return $err;
    }

    // コミット処理
    pg_query($con, "commit");
    pg_close($con);

//  if ($use_cyrus == "t") {
//      // メールアカウントの追加（ログインIDが数字のみでない場合）
//      $account = ($site_id == "") ? $row["emp_login_id"] : "{$mail_id}_{$site_id}";
//      if (preg_match("/[a-z]/", $account) > 0) {
//          $dir = getcwd();
//          if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
//              exec("/usr/bin/perl ".$dir."/mboxadm/addmailbox ".$account." ".$row["emp_login_pass"]." 2>&1 1> /dev/null", $output, $exit_status);
//              if ($exit_status !== 0) {
//                  exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account ".$row["emp_login_pass"]);
//              }
//          } else {
//              exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account ".$row["emp_login_pass"]." $imapServerAddress $dir");
//          }
//      }
//  }
//  webmail_quota_change_to_default($account);

    // 写真関連処理
    if ($updatephoto == "t" && $_FILES["photofile"]["error"] == 0) {
        if (!is_dir("profile")) {
            mkdir("profile", 0755);
        }
        copy($_FILES["photofile"]["tmp_name"], "profile/{$new_emp_id}.jpg");
    }
}

function outputPagerPartHtml($sect, $postAreaTagId, $page_num, $last_page_num) {
    $is_profile = (preg_match("/^[0-9]+$/", $sect) ? true : false);
    $uSect = "pager_".$sect."_";
    $common_attr = 'href="javascript:void(0)" onfocus="this.className=\'focussing\'" onblur="this.className=\'\'"';


    $common_js = "showSummarySect('".$sect."', '&page_num=[PAGE]&default_focus='+this.id, 1, '".$postAreaTagId."', '', {isPagerCommand:1});";
    if ($is_profile) $common_js = "showBSect('".$sect."', '&page_num=[PAGE]&default_focus='+this.id, '', '', '".$postAreaTagId."');";
    if ($sect=="EmpSelectorDialog") $common_js = 'tryDialogSearch(this, [PAGE]);';
    if ($sect=="mst_import") $common_js = "refreshView([PAGE]);";
    if ($sect=="mst_hyoka") $common_js = "loadEmpList('', [PAGE]);";


    echo '<table cellspacing="0" cellpadding="0" class="pager_btn" style="text-align:right; margin:0 0 0 auto"><tr>';
    if ($page_num>1) {
        echo '<td class="pager_top"><a '.$common_attr.' onclick="'.str_replace("[PAGE]","1",$common_js).'" id="'.$uSect.'top"></a></td>';
        echo '<td class="pager_prev"><a '.$common_attr.' onclick="'.str_replace("[PAGE]",$page_num-1,$common_js).'" id="'.$uSect.'prev"></a></td>';
    } else {
        echo '<td class="pager_top_off"><a></a></td>';
        echo '<td class="pager_prev_off"><a></a></td>';
    }
    if ($last_page_num>1) {
        $box_size = 30;
        if ($last_page_num>=1000) $box_size = 40;
        if ($last_page_num>=10000) $box_size = 50;
        echo '<td><input type="text" class="text w'.$box_size.' imeoff" id="'.$uSect.'move" maxlength="5" ignore_diff="1"';
        echo ' onkeydown="if(!isEnterKey(this,event)) return; if (!checkPageNum(this.value)) return;';

        if ($sect=="EmpSelectorDialog") echo "tryDialogSearch(this, this.value);\"";
        else if ($sect=="mst_import") echo "refreshView(this.value);\"";
        else if ($sect=="mst_hyoka") echo "loadEmpList('', this.value);\"";
        else if ($is_profile) echo " showBSect('".$sect."', '&page_num='+this.value+'&default_focus='+this.id, '', '', '".$postAreaTagId."');\"";
        else echo " showSummarySect('".$sect."', '&page_num='+this.value+'&default_focus='+this.id, 1, '".$postAreaTagId."', '', {isPagerCommand:1});\"";

        echo ' onfocus="this.select()"';
        echo ' value="'.$page_num.'" /></td>';
        echo '<td class="pager_move"><a '.$common_attr.' onclick="var mElem=ee(\''.$uSect.'move\'); if (!checkPageNum(mElem.value)) return false;';

        if ($sect=="EmpSelectorDialog") echo "tryDialogSearch(mElem, mElem.value);\"";
        else if ($sect=="mst_import") echo "refreshView(mElem.value);\"";
        else if ($sect=="mst_hyoka") echo "showAdminSect('".$sect."', '&page_num='+mElem.value, '".$postAreaTagId."');\"";
        else if ($is_profile) echo " showBSect('".$sect."', '&page_num='+mElem.value+'&default_focus='+mElem.id, '', '', '".$postAreaTagId."');\"";
        else echo " showSummarySect('".$sect."', '&page_num='+mElem.value+'&default_focus='+mElem.id, 1, '".$postAreaTagId."', '', {isPagerCommand:1});\"";

        echo ' style="font-size:11px"></a></td>';
    } else {
        echo '<td><input type="text" class="text w30 disabled" id="'.$uSect.'move" value="'.$page_num.'" disabled /></td>';
        echo '<td class="pager_move_off"><a></a></td>';
    }
    if ($page_num<$last_page_num) {
        echo '<td class="pager_next"><a '.$common_attr.' onclick="'.str_replace("[PAGE]",$page_num+1,$common_js).'" id="'.$uSect.'next"></a></td>';
        echo '<td class="pager_last"><a '.$common_attr.' onclick="'.str_replace("[PAGE]",$last_page_num,$common_js).'" id="'.$uSect.'last"></a></td>';
    } else {
        echo '<td class="pager_next_off"><a></a></td>';
        echo '<td class="pager_last_off"><a></a></td>';
    }
    echo '</tr></table>';
}

function divideTableAlias($alias) {
    if (preg_match("/^spft_profile[0-9]+$/", $alias)) return array("spft_profile", substr($alias, 12));
    if (preg_match("/^spfm_hyoka_han[0-9]+$/", $alias)) return array("spfm_hyoka_han", substr($alias, 14));
    if (preg_match("/^spfm_hyoka_unit[0-9]+$/", $alias)) return array("spfm_hyoka_unit", substr($alias, 15));
    if (preg_match("/^spfm_hyoka_sheet[0-9]+$/", $alias)) return array("spfm_hyoka_sheet", substr($alias, 16));
    return array($alias, "");
}

//****************************************************************************************************************
// ダウンロードファイルのURLを作成する。(.htaccess参照のこと）
//****************************************************************************************************************
global $spfDownloadLimitMinutes;
$spfDownloadLimitMinutes = 0;
function genDownloadTicket($image_id, $file_name_html) {
    global $spfDownloadLimitMinutes;
    if (!$spfDownloadLimitMinutes) {
        $spfDownloadLimitMinutes = c2dbGetSystemConfig("spf.DownloadLimitMinutes");
        if (!$spfDownloadLimitMinutes) $spfDownloadLimitMinutes = 15;
    }
    $limit = date("YmdHis", strtotime("+".$spfDownloadLimitMinutes." minute"));
    $ticket = c2MakeCmxCrypt($image_id."@".$limit.C2_SESSION_ID);

    // HREFに仕込まれるため、原則的にはクォートも何もせず、手を加えてはいけない。右クリックでDLしようとするとおかしくなる。
    // ファイル名にシングル/ダブルクォートが含まれるとHTMLが壊れるが、
    // アップロードされたファイル名そのものなので前提として含まれない、とする。
    $file_name_html = str_replace('"','',str_replace("'","",$file_name_html));

    $rel_url = 'download/'.$image_id.'/'.$limit."/".$ticket."/".$file_name_html;
    return array("limit"=>$limit, "ticket"=>$ticket, "rel_url"=>$rel_url);
}

//****************************************************************************************************************
// ダウンロードURLが指定された場合。ダウンロード可能なら画像バイナリを標準出力(genDownloadTicket関数、.htaccessも参照のこと）
//****************************************************************************************************************
if ($_REQUEST["get_image_id"]) {
    $err = "";
    for (;;) {
        $ticket = $_REQUEST["ticket"];
        $limit = $_REQUEST["limit_ymdhms"];
        $image_id = (int)$_REQUEST["get_image_id"];
        if (!$ticket) { $err = "不正なアクセスです。(ticket)"; break; }
        if (!$limit) { $err = "不正なアクセスです。(limit)"; break; }
        if ($limit < date("YmdHis")) { $err = "ダウンロード可能な時間が超過しました。呼出元画面を再表示する必要があります。"; break; }
        $chk = c2MakeCmxCrypt($image_id."@".$limit.C2_SESSION_ID);
        if ($chk != $ticket) { $err = "ダウンロード権限が無いか、不正なアクセスです。"; break; }
        $imageRow = c2dbGetTopRow("select image_base64, image_mime from spft_image where image_id = ".$image_id);
        header("Content-Type: ".$imageRow["image_mime"]);
        echo base64_decode($imageRow["image_base64"]);
        break;
    }
    if ($err) {
        c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML");
        c2env::echoStaticHtmlTag("HTML5_HEAD_META");

        $port = "http://";
        if ($_SERVER["PORT"]=="443") $port = "https://";
        $server = $_SERVER["HTTP_HOST"];
    ?>
        <base href="<?=$port.$server.c2GetContentsRootPath()?>spf/" target="<?=$port.$server.c2GetContentsRootPath()?>spf/">
        <title>CoMedix ダウンロードエラー <?=SELF_APPNAME?></title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        </head>
        <body>
            <div class="window_title" id="window_title" style="background-color:#ffffff; height:32px; padding:5px 0 0 5px; background-position:5px 5px">
                <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
                    <th style="background:url(img/logo.gif) no-repeat 7px 0; width:220px"></th>
                    <th style="width:200px; height:27px; letter-spacing:3px; color:#dd4444; font-size:18px; font-weight:normal; border:0; padding:2px 0 0 2px">ダウンロードエラー</th>
                    <td width="auto"></td>
                </tr></table>
            </div>
            <div style="padding:20px 10px">
                <?=$err?>
                <div style="padding-top:10px">
                    <span class="subject1" style="letter-spacing:2px">url</span>
                    <?=hh($_SERVER["REQUEST_URI"])?>
                </div>
            </div>
            </body></html>
        <?
    }
    die;
}

//------------------------------------------------------------------------------
// 職員プロファイルの画像を取得する。
// 表示枠は114x152という妙に中途なサイズに決まっており、
// かつ、ブラウザ側で強制的にリサイズするようになっておったが、あまりにも酷なので、
// この縦横比にあわせて画像の拡縮を決めたい。
// 既存の律儀な顧客が仮に114x152で作成しているとすれば、そのままにするが、
// そうでなければ横120基準で拡縮させる。120だと比率的に、縦は160である。
// なお、画像の格納pathは、comedix/profile/".$emp_id.".jpgである。
//------------------------------------------------------------------------------
function getPhotoHtml($emp_id, $base_w, $left_padding) { // base_w=デフォルト幅120
    $path = "./profile/".$emp_id.".jpg";
    if ($base_w<1) $base_w = 120;
    $base_h = $base_w / 3 * 4; // デフォルト高160;

    $rw = 0; // リアル画像の幅
    $rh = 0; // リアル画像の高さ
    if (file_exists($path)) {
        list($rw, $rh) = @(array)getimagesize($path);
        if ($rw && $rh) {
            // 職員登録機能のレガシーサイズで確定
            if ($rw==114 && $r==152) {
            }
            else {
                $base_rate = $base_w / $base_h; // 表示枠の、縦に対する横の大きさ。横が大きいほど数値が大きい。
                $real_rate = $rw / $rh; // 実画像の、縦に対する横の大きさ。横が大きいほど数値が大きい。

                if ($real_rate > $base_rate) {
                    $rh = (int)($rh * ($base_w / $rw)); // 実画像は表示枠より横が大きめ⇒横を$base_wに縮小した場合の縦サイズを求める
                    $rw = $base_w;
                } else {
                    $rw = (int)($rw * ($base_h / $rh)); // 実画像は表示枠より縦が大きめか同じ⇒縦を$base_hに縮小した場合の横サイズを求める
                    $rh = $base_h;
                }
            }
        }
    }

    $path = ".".$path; // プラウザから見たディレクトリLは、PHPカレントディレクトリと同じではない。/spf/以下であるからディレクトリを一つ上に
    if (!$rw || !$rh) {
        $rw = $base_w;
        $rh = $base_h;
        $path = "img/avator.gif"; // 画像がないのでアバター(120x$base_h)を表示
        $imgsize = 'width="'.$base_w.'" height="'.$base_h.'"'; // 新アバターサイズ
    }
    $imgsize = 'width="'.$rw.'" height="'.$rh.'"'; // <img>タグに指定するサイズ

	$html = array();
	// 横幅24px以下ならシャドウ、白フチ無し
	if ($base_w <= 24) {
	    $html[]= '<td class="right border0" style="width:'.($rw).'px">';
	    $html[]= '<img src="'.$path.'" alt="" height="'.$rh.'" width="'.$rw.'" style="margin:0" />';
	    $html[]= '</div></td>';
	}
	// 横幅36px以下なら白フチ無し
	else if ($base_w <= 60) {
	    $html[]= '<td class="right border0" style="width:'.($rw+16).'px">';
	    $html[]= '<div style="margin:0 0 0 auto; background:url(shadow.php?iw='.$rw.'&ih='.$rh.'&mgn=0) no-repeat">';
	    $html[]= '<img src="'.$path.'" alt="" height="'.$rh.'" width="'.$rw.'" style="padding:9px; margin:0" />';
	    $html[]= '</div></td>';
	}
    // 120x160画像のとき、シャドウは余白が8px四方、影が9px四方である。結果、実画像より34px大きくなる。
	else {
	    $html[]= '<td class="right border0" style="'.($left_padding ? "padding-left:".$left_padding."px;":"").'width:'.($rw+34).'px">';
	    $html[]= '<div style="margin:0 0 0 auto; background:url(shadow.php?iw='.$rw.'&ih='.$rh.') no-repeat">';
	    $html[]= '<img src="'.$path.'" alt="" height="'.$rh.'" width="'.$rw.'" style="padding:17px; margin:0" />';
	    $html[]= '</div></td>';
	}
    return implode($html);
}
/*
function get_emp_concurrent_sql() {
	$sql = array(
		"select",
		"empmst.emp_id, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, empmst.emp_job",
		"from empmst",
		"union",
		"select",
		"concurrent.emp_id, concurrent.emp_class, concurrent.emp_attribute, concurrent.emp_dept, concurrent.emp_room, concurrent.emp_st, empmst.emp_job",
		"from concurrent",
		"inner join empmst on ( empmst.emp_id = concurrent.emp_id )"
	);
	return implode("\n", $sql);
}
*/
