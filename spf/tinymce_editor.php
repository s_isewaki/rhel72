<?
//****************************************************************************************************************
// tinymceエディタ
// メインコンテンツが汚染されないために、iframeとして独立させています。
//
// やむなくSQEで利用していますが、そこだけです。
// よっぽどのことが無い限りは、tinymceは、使うべきではありません。
//****************************************************************************************************************
    $tinymce_version = $_REQUEST["tinymce_version"];
    $getter_func = $_REQUEST["getter_func"];
    $setter_func = $_REQUEST["setter_func"];

    if (!$tinymce_version) $tinymce_version = "2.1";

    require_once("common.php"); // 読込みと同時にログイン判別
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?> - 文章作成</title>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<? if ($tinymce_version=="2.1") { ?>
<script type="text/javascript" src="../tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<? } ?>
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/JavaScript">
$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;

var tinymce_version = "";
var getter_func = null;
var setter_func = null;
var errCode = "";

function applyHtml() {
    if (errCode) return;
    setter_func(tinyMCE.activeEditor.getContent(), tinymce_version);
}

function loadInit() {
    if (errCode) alert("不正な画面呼出です。（"+errCode+"）");
	tinymce_version = "<?=str_replace('"', '', $tinymce_version)?>";
	getter_func = eval("<?=str_replace('"', '', $getter_func)?>");
	setter_func = eval("<?=str_replace('"', '', $setter_func)?>");
	if (!tinymce_version) errCode = "01";
	else if (!getter_func) errCode = "02";
	else if (!setter_func) errCode = "03";
	else if (typeof getter_func != "function") errCode = "04";
	else if (typeof setter_func != "function") errCode = "05";
	ee("tinymce_content").innerHTML = '<textarea name="tm_textarea" id="tm_textarea" cols="50" rows="20">' + getter_func() + '</textarea>';
	<? if ($tinymce_version=="2.1") { ?>
	    tinyMCE.init({
	        mode : "textareas",
	        theme : "advanced",
	        plugins : "preview,table,emotions,fullscreen,layer",
	        //language : "ja_euc-jp",
	        language : "ja",
	        width : "100%",
	        height : "400",
	        theme_advanced_toolbar_location : "top",
	        theme_advanced_toolbar_align : "left",
	        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
	        theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,undo,redo",
	        theme_advanced_buttons3 : "tablecontrols,|,visualaid,|,code",
	        content_css : "./css/style.css",
	        extended_valid_elements : "iframe[align<bottom?left?middle?right?top|class|frameborder|height|id|longdesc|marginheight|marginwidth|name|scrolling<auto?no?yes|src|style|title|width]",
	        theme_advanced_statusbar_location : "none",
	        force_br_newlines : true,
	        forced_root_block : '',
	        force_p_newlines : false
	    });
	<? } ?>
}
</script>
</head>
<body onload="loadInit()" spellcheck="false"><div id="CLIENT_FRAME_CONTENT" style="overflow:hidden">

<div style="padding:5px 0; position:absolute; z-index:100; text-align:right; width:100%">
    <span class="gray font12">他から同一文章を複製したい場合は、「HTML」ボタンを押して、「HTMLソースエディタ」に元の文章を貼りつけてください。</span>
    <button type="button" onclick="applyHtml()">編集完了</button>
</div>

<div style="width:100%; height:36px">&nbsp;</div>

<div class="tinymce_content" id="tinymce_content">
</div>




</div><!-- //CLIENT_FRAME_CONTENT -->
</body>
</html>