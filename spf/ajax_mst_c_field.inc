<?
/*
c2dbExec("insert into spfm_field_struct(table_id, field_id) values ('login', 'emp_login_id');");
c2dbExec("insert into spfm_field_struct(table_id, field_id) values ('login', 'emp_login_pass');");
c2dbExec("insert into spfm_field_struct(table_id, field_id) values ('login', 'emp_login_flg');");
c2dbExec("insert into spfm_field_struct(table_id, field_id) values ('login', 'emp_login_mail');");
        (
            [table_id] => authmst
            [field_id] => emp_del_flg
            [field_label] => 利用停止
            [field_type] => pulldown
            [pulldown_selections] => t=利用停止
            [max_length] =>
            [ime_off] =>
            [format_regexp] =>
            [field_navi] =>
            [label_align] => right
            [text_align] => left
            [is_usr_display] => 1
            [is_kan_display] => 1
            [is_syo_display] => 1
            [is_jin_display] => 1
            [is_usr_editable] =>
            [is_kan_editable] =>
            [is_syo_editable] =>
            [is_jin_editable] =>
            [is_hissu] => 1
            [available_field_type] => pulldown
            [rel_master_id] =>
*/
//c2dbExec("update spfm_field_struct set available_field_type = 'pulldown' where table_id = 'login' and field_id = 'emp_login_flg'");
//****************************************************************************************************************
// 拡張項目マスタ
//****************************************************************************************************************
function html_mst_field_struct() {
    global $MASTER_TABLES;
    global $HISSU_FIELDS;
    $fieldStructs = c2dbGetRows("select * from spfm_field_struct order by table_id, field_id");

    $scheme = c2dbGetTableScheme("spft_empmst");
    $yobiMaxIdx = 0;
    foreach ($scheme as $field_id => $dummy) {
    	if (substr($field_id,0,9)=="yobi_text") $yobiMaxIdx = substr($field_id,9);
	}
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
    <table cellspacing="0" cellpadding="0" class="section_title"><tr>
        <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">C</th><? } ?>
        <th class="section_title_title"><nobr>拡張項目</nobr></th>
        <th><button type="button" class="w70" onclick="confirmAddField()">項目追加</button></th>
        <th class="button"><nobr><button type="button" onclick="tryAdminReg('form_mst_field_struct', 'reg_mst_field_struct')" class="w70">保存</button></nobr></th>
    </tr></table>
    </div>

	<textarea class="script">
		function confirmAddField() {
			var msg =
			"新しい空欄自由入力項目「yobi_text<?=$yobiMaxIdx+1?>」を追加します。\n\n"+
			"この操作は元に戻すことはできません。\n\n"+
			"追加してよろしいですか？";
			if (!confirm(msg)) return;

		    $.ajax({ url:"common.php?ajax_action=reg_mst_field_add", success:function(msg) {
		        ret = getCustomTrueAjaxResult(msg, TYPE_JSON); if (ret==AJAX_ERROR) return;
		        alert("保存しました。"+ret.newFieldId+"が追加されました。");
		        showAdminSect("mst_field_struct");
		    }});
		    return;
		}
	</textarea>

    <div class="gray" style="padding-bottom:10px">
        ◆ 職員と「１対１」になる属性項目情報の設定を行います。<br>
        ◆ ここでは利用可能な全項目が表示されています。実際にどのメニュー・セクションで、どの項目を利用するかは、「セクション構成」機能で行います。
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="form_mst_field_struct">
    <table cellspacing="0" cellpadding="0" class="hover_colord list_table">
    <tr class="pale">
        <th>No.</th>
        <th>テーブルID<br>フィールドID</th>
        <th><nobr>本人・監督・承認・人事</nobr></th>
        <th>ラベル</th>
        <th>入力タイプ</th>
        <th>入力必須<br>日本語IME</th>
        <th>候補（DBマスタ連動）<br>または入力選択肢</th>
        <th>入力時書式<br>最大文字数</th>
    </tr>
    <?
    	$rowNumber = 0;
    	foreach ($fieldStructs as $row) {
			$rowNumber++;
			$fid = $row["table_id"]."@".$row["field_id"];
			$ftype = $row["field_type"];
			$types = explode(",", $row["available_field_type"]);
			$aftypes = array();
            foreach ($types as $t) if ($t) $aftypes[$t] = 1;
            if (!$listStructs[$fid]) $listStructs[$fid] = $row;
	?>
    <tr>
        <td style="line-height:24px"><?=$rowNumber?></td>
        <? // ----------------------------------------------------- ?>
        <? // テーブルID・フィールドID                              ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <div class="gray" style="line-height:24px"><?=$row["table_id"]?></div>
            <div class="right" style="line-height:24px"><?=$row["field_id"]?></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 権限                                                  ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <div class="gray" style="line-height:24px"><nobr>
            表示：
            <input type="checkbox" value="1" name="<?=$fid?>@is_usr_display"<?=($row["is_usr_display"]?" checked":"")?> navi="本人表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_kan_display"<?=($row["is_kan_display"]?" checked":"")?> navi="監督者表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_syo_display"<?=($row["is_syo_display"]?" checked":"")?> navi="承認者/評価者表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_jin_display"<?=($row["is_jin_display"]?" checked":"")?> navi="人事表示権限" />
            </nobr></div>
            <div class="gray" style="line-height:24px"><nobr>
            編集：
            <input type="checkbox" value="1" name="<?=$fid?>@is_usr_editable"<?=($row["is_usr_editable"]?" checked":"")?> navi="本人編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_kan_editable"<?=($row["is_kan_editable"]?" checked":"")?> navi="監督者編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_syo_editable"<?=($row["is_syo_editable"]?" checked":"")?> navi="承認者/評価者編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_jin_editable"<?=($row["is_jin_editable"]?" checked":"")?> navi="人事編集権限" />
            </nobr></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // ラベル                                                ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <input type="text" class="text w120" value="<?=hh($row["field_label"])?>" name="<?=$fid?>@field_label" />
            <div style="padding-top:2px" class="gray">
            ｱﾗｲﾝ
            <select name="<?=$fid?>@label_align" navititle="ラベルのアライン" navi="右：右寄せ表示、左：左寄せ表示、中：中央表示">
                <option value="right">右</option>
                <option value="left"<?=($row["label_align"]=="left"?" selected":"")?>>左</option>
                <option value="center"<?=($row["label_align"]=="center"?" selected":"")?>>中</option>
            </select>
            </div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 入力タイプ                                            ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <select name="<?=$fid?>@field_type" navititle="入力タイプ" style="color:#dd4444; font-weight:bold">
                <? $fsel = array($ftype=>" selected"); ?>
                <option value="text"<?=$fsel["text"]?><?=($aftypes["text"]?'':' disabled')?>>テキスト</option>
                <option value="textarea"<?=$fsel["textarea"]?><?=($aftypes["textarea"]?'':' disabled')?>>複数行テキスト</option>
                <option value="pulldown"<?=$fsel["pulldown"]?><?=($aftypes["pulldown"]?'':' disabled')?>>プルダウン</option>
                <option value="yymm"<?=$fsel["yymm"]?><?=($aftypes["yymm"]?'':' disabled')?>>年月</option>
                <option value="ymd"<?=$fsel["ymd"]?><?=($aftypes["ymd"]?'':' disabled')?>>年月日</option>
                <option value="ymdhm"<?=$fsel["ymdhm"]?><?=($aftypes["ymdhm"]?'':' disabled')?>>年月日時分</option>
                <option value="image"<?=$fsel["image"]?><?=($aftypes["image"]?'':' disabled')?>>画像</option>
                <option value="label"<?=$fsel["label"]?><?=($aftypes["label"]?'':' disabled')?>>表示のみ</option>
            </select>
            <div style="padding-top:2px" class="gray">
            ｱﾗｲﾝ
            <select name="<?=$fid?>@text_align" navititle="入力時アライン" navi="入力ボックス内やプルダウン内において、右：右寄せ表示、左：左寄せ表示、中：中央表示">
                <option value="left">左</option>
                <option value="right"<?=($row["text_align"]=="right"?" selected":"")?>>右</option>
            </select>
            </div>
        </nobr></td>
        <? // ----------------------------------------------------- ?>
        <? // 入力必須・日本語IME                                   ?>
        <? // ----------------------------------------------------- ?>
        <td><nobr>
            <select name="<?=$fid?>@is_hissu" navititle="入力必須" navi="「入力必須」を指定すると、空値のままの保存が行えないようになります。<br>赤い文字で示されている場合は、必須を外すことはできません。"
                style="<?=(in_array($fid, $HISSU_FIELDS) ? 'color:#dd4444' : '')?>">
                <? if (!in_array($fid, $HISSU_FIELDS)) { ?>
                    <option value=""></option>
                <? } ?>
                <option value="1"<?=($row["is_hissu"]=="1" ?" selected":"")?>>入力必須</option>
            </select>
            </nobr><br>
            <? if ($ftype=="text") { ?>
            <div style="padding-top:2px" class="right"><nobr>
                <select name="<?=$fid?>@ime_off" navititle="日本語IME" navi="「IMEオフ」にすると、入力ボックス上ではキーボード全角禁止モードとなりますが、コピー＆ペーストは許容されます。">
                    <option value=""></option>
                    <option value="1"<?=($row["ime_off"]=="1"?" selected":"")?>>IMEオフ</option>
                </select>
            </nobr></div>
            <? } ?>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // DBマスタ連動・入力選択肢                              ?>
        <? // ----------------------------------------------------- ?>
        <td>
        <? if ($ftype=="ymd" || $ftype=="ym" || $ftype=="label" || $ftype=="textarea") { ?>
        <? } else { ?>
            <select name="<?=$fid?>@rel_master_id" onchange="ee('divps_<?=$fid?>').style.display=(this.value?'none':'');" style="width:180px"
            navititle="DBマスタ連動" navi="あらかじめ用意されたマスターリストが利用できます。
            入力タイプが「プルダウン」「テキスト」「複数行テキスト」のときに利用することができます。
            「プルダウン」のときは、DBには規定の項目マスタコードが保存されます。
            「テキスト」「複数行テキスト」の場合は、選択肢は「候補」ポップアップで提供され、DBには選択文字列そのものが保存されます。
            「テキスト」「複数行テキスト」に対して、「候補」以外の自由入力を許容するには、「+」つき項目を指定してください。"
            >
                <option value=""></option>
                <? foreach ($MASTER_TABLES as $mtid => $mtnm) { ?>
                <option value="<?=$mtid?>"<?=($mtid==$row["rel_master_id"]?" selected":"")?>><?=$mtid?> <?=$mtnm?></option>
                <? if ($ftype=="text" || $ftype=="textarea") { ?>
                <option value="<?=$mtid?>+"<?=(($mtid."+")==$row["rel_master_id"]?" selected":"")?>><?=$mtid?>+ <?=$mtnm?>（＋自由入力）</option>
                <? } ?>
                <? } ?>
            </select>
            <div id="divps_<?=$fid?>" style="padding-top:2px;<?=($row["rel_master_id"]?"display:none":"")?>">
                <input type="text" class="text" value="<?=hh($row["pulldown_selections"])?>" name="<?=$fid?>@pulldown_selections"
                navititle="入力選択肢" navi="「A=1,B=2,C=3, ... key=value」形式で指定すると、画面にはvalue、データベースにはkeyが保存されます。
                「A,B,C ... 」形式で指定すると、画面およびデータベースともに同じ値で保存されます。" />
            </div>
        <? } ?>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 入力時書式・最大文字数                                ?>
        <? // ----------------------------------------------------- ?>
        <td class="gray"><nobr>
        <? if ($ftype=="ymd" || $ftype=="ym") { ?>
            <? $regexp = explode(",", $row["format_regexp"]); ?>
            最小
            <select name="<?=$fid?>@format_regexp1" onchange="ee('<?=$fid?>@format_regexp2').style.display=(this.value==''?'none':'');">
                <option value=""<?=$regexp[0]==""?"selected":""?>>なし</option>
                <option value="today-"<?=$regexp[0]=="today-"?"selected":""?>>入力日前</option>
                <option value="today+"<?=$regexp[0]=="today+"?"selected":""?>>入力日後</option>
            </select>
            <span id="<?=$fid?>@format_regexp2" style="<?=$regexp[0]==""?"display:none":""?>">
            <input type="text" class="text imeoff w40" name="<?=$fid?>@format_regexp2" value="<?=hh($regexp[1])?>" maxlength="5"
            navi="おおよそ1年なら365、100年なら36500というように指定してください。<br>空欄またはゼロを指定すると入力日当日と判断されます。" />
            <?=($ftype=="ym"?"月":"日")?>
            </span>
            <div style="padding-top:2px">
            最大
            <select name="<?=$fid?>@format_regexp3" onchange="ee('<?=$fid?>@format_regexp4').style.display=(this.value==''?'none':'');">
                <option value=""<?=$regexp[2]==""?"selected":""?>>なし</option>
                <option value="today-"<?=$regexp[2]=="today-"?"selected":""?>>入力日前</option>
                <option value="today+"<?=$regexp[2]=="today+"?"selected":""?>>入力日後</option>
            </select>
            <span id="<?=$fid?>@format_regexp4" style="<?=$regexp[0]==""?"display:none":""?>">
            <input type="text" class="text imeoff w40" name="<?=$fid?>@format_regexp4" value="<?=hh($regexp[3])?>" maxlength="5"
            navi="おおよそ1年なら365、100年なら36500というように指定してください。<br>空欄またはゼロを指定すると入力日当日と判断されます。"
            />
            <?=($ftype=="ym"?"月":"日")?>
            </span>
            </div>
        <? } else if ($ftype=="text" || $ftype=="textarea") { ?>
            <input type="text" class="text" value="<?=hh($row["format_regexp"])?>" name="<?=$fid?>@format_regexp"
            navititle="入力時書式" navi="正規表現を使って記述してください。JavaScript/PHP双方で利用されます。
            例）数字1文字以上：&nbsp;&nbsp;^[0-9]+$
            例）数字ちょうど3文字：&nbsp;&nbsp;^[0-9]{3}$
            例）数字またはアルファベット：&nbsp;&nbsp;^[0-9a-zA-Z]+$
            例）数字またはアルファベットだが、必ずアルファベットで開始：&nbsp;&nbsp;^[a-zA-X]+[0-9a-zA-Z]*$
            例）アルファベットで開始していればよい：&nbsp;&nbsp;^[a-zA-X]
            例）アルファベットで終了していればよい：&nbsp;&nbsp;[a-zA-X]$"
             />
            <div style="padding-top:2px" class="gray">
                最大文字数
                <input type="text" class="text imeoff w40" value="<?=hh($row["max_length"])?>" name="<?=$fid?>@max_length"
                navititle="最大文字数" navi="HTMLタグの「maxlength」の値です。入力ボックスに入力可能な文字の数です。
                複数行テキストへ指定した場合、最新ブラウザでなければ無効な場合があります。
                システム内に規定の上限がある場合があり、それを超えることはできません。"
                 />
            </div>
        <? } ?>
       </nobr></td>
    </tr>
    <? } ?>
    </table>
    </div>
<?
}

//================================================================================================================
// 項目マスタの保存
//================================================================================================================
function reg_mst_field_add() {
    $scheme = c2dbGetTableScheme("spft_empmst");
    $yobiMaxIdx = 0;
    foreach ($scheme as $field_id => $dummy) {
    	if (substr($field_id,0,9)=="yobi_text") $yobiMaxIdx = substr($field_id,9) + 1;
	}
    if ($yobiMaxIdx < 50) { echo 'ng追加に失敗しました。('.$yobiMaxIdx.')'; die; }
    if ($yobiMaxIdx > 200) { echo 'ng最大追加可能数（200）に達しています。新しい項目を作成できません。'; die; }

    c2dbBeginTrans();
	$new_yobi_text = "yobi_text".sprintf("%02d", $yobiMaxIdx);
	c2dbExec("alter table spft_empmst add column ".$new_yobi_text." text;");
	c2dbExec("alter table spft_empmst alter column ".$new_yobi_text." set storage extended;");
	c2dbExec("insert into spfm_field_struct ( table_id, field_id ) values ('spft_empmst', '".$new_yobi_text."')");
	c2dbExec(
		" update spfm_field_struct set".
		" field_label = '予備".$yobiMaxIdx."'".
		",field_type = 'text', label_align = 'right', text_align = 'left', is_usr_display = '1'".
		",is_kan_display = '1', is_syo_display = '1', is_jin_display = '1', is_jin_editable = '1'".
		",available_field_type = 'yymm,ymd,ymdhm,text,textarea,pulldown'".
		" where field_id = '".$new_yobi_text."'"
	);
	c2dbCommit();
	echo 'ok{newFieldId:"'.$new_yobi_text.'"}';
}





//================================================================================================================
// 項目マスタの保存
//================================================================================================================
function reg_mst_field_struct() {
    $fieldStructs = c2dbGetRows("select * from spfm_field_struct order by table_id, field_id");
    c2dbBeginTrans();
    $notReload = "1";
    $returnMsg = "保存対象はありません。";
    foreach ($fieldStructs as $row) {
        $where = " where table_id = ".c2dbStr($row["table_id"])." and field_id = ".c2dbStr($row["field_id"]);
        $fid = $row["table_id"]."@".$row["field_id"]."@";
        $format_regexp = $_REQUEST[$fid."format_regexp"];
        if ($_REQUEST[$fid."field_type"]=="ym" || $_REQUEST[$fid."field_type"]=="ymd") {
            $format_regexp =
            $_REQUEST[$fid."format_regexp1"].",".
            (int)str_replace(",","",str_replace("-","",$_REQUEST[$fid."format_regexp2"])).",".
            $_REQUEST[$fid."format_regexp3"].",".
            (int)str_replace(",","",str_replace("-","",$_REQUEST[$fid."format_regexp4"]));
        }
        $upd = array();
        if ($row["field_label"]!=$_REQUEST[$fid."field_label"]) $upd[]= "field_label = " . c2dbStr($_REQUEST[$fid."field_label"]);
        if ($row["field_type"]!=$_REQUEST[$fid."field_type"]) $upd[]= "field_type = " . c2dbStr($_REQUEST[$fid."field_type"]);
        if ($row["pulldown_selections"]!=$_REQUEST[$fid."pulldown_selections"]) $upd[]= "pulldown_selections = " . c2dbStr($_REQUEST[$fid."pulldown_selections"]);
        if ($row["rel_master_id"]!=$_REQUEST[$fid."rel_master_id"]) $upd[]= "rel_master_id = " . c2dbStr($_REQUEST[$fid."rel_master_id"]);
        if ($row["max_length"]!=$_REQUEST[$fid."max_length"]) $upd[]= "max_length = " . c2dbStr($_REQUEST[$fid."max_length"]);
        if ($row["ime_off"]!=$_REQUEST[$fid."ime_off"]) $upd[]= "ime_off = " . c2dbStr($_REQUEST[$fid."ime_off"]);
        if ($row["format_regexp"]!=$format_regexp) $upd[]= "format_regexp = " . c2dbStr($format_regexp);
        if ($row["label_align"]!=$_REQUEST[$fid."label_align"]) $upd[]= "label_align = " . c2dbStr($_REQUEST[$fid."label_align"]);
        if ($row["text_align"]!=$_REQUEST[$fid."text_align"]) $upd[]= "text_align = " . c2dbStr($_REQUEST[$fid."text_align"]);
        if ($row["is_usr_display"]!=$_REQUEST[$fid."is_usr_display"]) $upd[]= "is_usr_display = " . c2dbStr($_REQUEST[$fid."is_usr_display"]);
        if ($row["is_kan_display"]!=$_REQUEST[$fid."is_kan_display"]) $upd[]= "is_kan_display = " . c2dbStr($_REQUEST[$fid."is_kan_display"]);
        if ($row["is_syo_display"]!=$_REQUEST[$fid."is_syo_display"]) $upd[]= "is_syo_display = " . c2dbStr($_REQUEST[$fid."is_syo_display"]);
        if ($row["is_jin_display"]!=$_REQUEST[$fid."is_jin_display"]) $upd[]= "is_jin_display = " . c2dbStr($_REQUEST[$fid."is_jin_display"]);
        if ($row["is_usr_editable"]!=$_REQUEST[$fid."is_usr_editable"]) $upd[]= "is_usr_editable = " . c2dbStr($_REQUEST[$fid."is_usr_editable"]);
        if ($row["is_kan_editable"]!=$_REQUEST[$fid."is_kan_editable"]) $upd[]= "is_kan_editable = " . c2dbStr($_REQUEST[$fid."is_kan_editable"]);
        if ($row["is_syo_editable"]!=$_REQUEST[$fid."is_syo_editable"]) $upd[]= "is_syo_editable = " . c2dbStr($_REQUEST[$fid."is_syo_editable"]);
        if ($row["is_jin_editable"]!=$_REQUEST[$fid."is_jin_editable"]) $upd[]= "is_jin_editable = " . c2dbStr($_REQUEST[$fid."is_jin_editable"]);
        if ($row["is_hissu"]!=$_REQUEST[$fid."is_hissu"]) $upd[]= "is_hissu = " . c2dbStr($_REQUEST[$fid."is_hissu"]);
        if (count($upd)) {
            $notReload = "";
            $returnMsg = "";
            c2dbExec("update spfm_field_struct set ".implode(",", $upd). $where);
        }
    }
    c2dbCommit();
    echo "ok".'{forwardSection:"mst_field_struct", forwardOption:"", returnMsg:"'.$returnMsg.'", notReload:"'.$notReload.'"}';
    die;
}
