<?
//****************************************************************************************************************
// ユーザ画面
//
// 一度開いたら、管理画面に移動するか、画面を閉じるか、ロゴをクリックしてリロードするまで、開きっぱなしである。
//
// ajaxによって、メニューも含め、内部を書き換えている。このため、「戻る」等が存在しない。
// コンテンツはiframeではない。
// いったん管理画面を開くと、JavaScript変数も、関数も、セクション変更、職員変更を行なったところで、えんえん有効なままであり、それは狙いである。
// ダイアログを開いて保存や削除を行うと、おおよそ必要なセクションの再読込リクエストをサーバに投げている。
//****************************************************************************************************************
require_once("common.php"); // 読込みと同時にログイン判別

/*
set_time_limit (3600);

c2dbExec("drop table if exists _check");
c2dbExec("drop table if exists _targets");
c2dbExec("create table _check ( emp_id character varying(12), cnt integer )");
c2dbExec("create table _targets ( seq integer, emp_id character varying(12), han_id integer, unit_id integer, puid integer, hier integer, unknowns integer, exists integer )");
c2dbExec("ALTER TABLE ONLY _check ADD CONSTRAINT _check_pkey PRIMARY KEY (emp_id);");
c2dbExec("ALTER TABLE ONLY _targets ADD CONSTRAINT _targets_pkey PRIMARY KEY (seq);");

$rows = c2dbGetRows("select emp_id, hyoka_han_id, hyoka_unit_id, puid, workflow_scheme from spft_profile where muid = 501 and hyoka_han_id > 0 order by puid offset 0 limit 2000");
echo '<pre>';
$emps = array();
$unknown = array();
$exists = array();
$unknown_count = 0;
$seq = 0;
foreach ($rows as $row) {
	$obj = unserialize($row["workflow_scheme"]);
	unset ($row["workflow_scheme"]);
	$a = $obj["appr_emp_rows"];
	foreach ($a as $hier => $emp) {
		$seq++;
        c2dbExec("insert into _targets ( seq, puid, han_id, unit_id, emp_id, hier, unknowns, exists ) values (".$seq.", ".$row["puid"].", ".$row["hyoka_han_id"].", ".$row["hyoka_unit_id"].", '".$row["emp_id"]."', ".$hier.", 0, 0);");
		foreach ($emp as $eid => $o) {
			if (substr($eid,0,7)=="UNKNOWN") {
				c2dbExec("update _targets set unknowns = unknowns+1 where seq = ".$seq);
			} else {
				$emps[$eid]++;
				c2dbExec("update _targets set exists = exists+1 where seq = ".$seq);
			}
		}
	}
}
foreach ($emps as $eid => $cnt) {
	c2dbExec("insert into _check ( emp_id, cnt ) values ('".$eid."', ".$cnt.");");
}
die;

*/
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にspf権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

$physically_eliminate = c2dbGetSystemConfig("spf.AuthEmpPhysicallyEliminate");
$capEmpProfile = c2dbGetSystemConfig("spf.CaptionEmpProfile");
if (!$capEmpProfile) $capEmpProfile = "職員プロファイル";


$rows = c2dbGetRows("select * from spfm_menu where is_active = '1' order by menu_order");
$menuRows = array();
$menuList = array();
$menuLabels = array();
$dai_muid = "";
$chu_muid = "";
foreach ($rows as $row) {
    $menuRows[$row["muid"]] = $row;
    $bid = $row["muid"];
    if ($row["menu_hier"]== "dai") {
        $dai_muid = $bid;
        $menuList[$dai_muid] = array();
    }
    if ($row["menu_hier"]== "chu") {
        $chu_muid = $bid;
        $menuList[$dai_muid][$chu_muid] = array();
    }
}
$loginUserAuth = getLoginUserAuth(C2_LOGIN_EMP_ID);

// ログイン者が現在、誰かの監督者か
$is_kan = isKantokusya(C2_LOGIN_EMP_ID);
$is_syo = isSyoninsya(C2_LOGIN_EMP_ID);

$sql =
" select * from spfm_finder where finder_title is not null".
"  and ( is_all_display = '1'".
" ". ($EAL[SELF_ADMIN_AUTH_FLG] ? " or is_adm_display = '1'" : "").
" ". (IS_JIN_AUTH ? " or is_jin_display = '1'" : "").
" ". ($is_kan ? " or is_kan_display = '1'" : "").
" ". ($is_syo ? " or is_syo_display = '1'" : "").
" )".
" order by finder_title";
$_finderRows = c2dbGetRows($sql);
$finderRows = array();
foreach ($_finderRows as $frow) {
    $finderRows[$frow["finder_id"]] = $frow["finder_title"];
}

$empConfigRow = c2dbGetTopRow("select * from spft_emp_config where emp_id = ".c2dbStr(C2_LOGIN_EMP_ID)); // ログイン者の個別設定
if (!$empConfigRow) c2dbExec("insert into spft_emp_config (emp_id) values (".c2dbStr(C2_LOGIN_EMP_ID).")"); // なければログイン者の個別設定を作成しておく


// 本来main_menu.phpで。Core2導入されていないのでここで実行
setcookie('CMX_CONTENTS_ROOT', c2GetContentsRootPath());


//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript" src="js/wdconv.js"></script>
<?require_once(dirname(__FILE__)."/js/dynamic.inc");?>
<script type="text/JavaScript">

var spfActiveSendStatMenu = "<?=c2dbGetSystemConfig("spf.ActiveSendStatMenu")?>";
var spfActiveRecvStatMenu = "<?=c2dbGetSystemConfig("spf.ActiveRecvStatMenu")?>";

function resizeIframeEx() {
    var win = window.parent;
    var screenHeight = window.document.body.clientHeight;
    var contentTop = 0;
    if (win.floatingpage) {
        var screenHeight = win.document.body.clientHeight;
        var pBody = win.document.getElementById("contents_body");
        if (!pBody) {
            var pTd = win.document.getElementById("centerblock");
            if (pTd) pBody = pTd.parentNode.parentNode.parentNode;
        }
        contentTop = pBody.offsetTop;
    } else {
        ee("window_title").style.padding = "5px 0 0 5px";
        ee("window_title").style.backgroundPosition = "5px 5px";
    }
    var mainTable = ee("main_table");
    var bottomMargin = 20;

    var ttlHeight = (screenHeight - contentTop - mainTable.offsetTop - bottomMargin);
    ee("div_menu_wrapper").style.height = ttlHeight + "px";
    ee("div_content").style.height = ttlHeight + "px";
    ee("cont_margin").style.height = (ttlHeight - 100) + "px";
}
function loadInit() {
    showSummarySect("EmpInfo", "&emp_id=<?=C2_LOGIN_EMP_ID?>", NOT_SCROLL);
    showSummarySect("Dashboard", "", NOT_SCROLL, "", NOT_BLINK);
    if (parent && parent.hideSideTopMenuDelayed) parent.hideSideTopMenuDelayed();
    resizeIframeEx();
    gNavi.initialize();
}
</script>
</head>
<body onload="loadInit()" onresize="resizeIframeEx()"<?=(DO_PRINT?' class="print"':'')?> spellcheck="false"><div id="CLIENT_FRAME_CONTENT" pageBottomMargin="0" parentBodyOverflowY="hidden" thisBodyOverflowY="hidden" thisBodyOverflowX="hidden">



<? //============================================================================================ ?>
<? // タイトルバーとタブ                                                                          ?>
<? //============================================================================================ ?>
<div class="window_title print-none" id="window_title">
    <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
        <th style="background:url(img/logo.gif) no-repeat 7px 0; width:200px"><nobr><a class="pankuzu" href="index_user.php" title="<?=SELF_APPNAME?>" style="width:210px; display:block; text-decoration:none; height:26px">&nbsp;</a></nobr></th>
        <td style="width:auto; padding:0 20px; vertical-align:top; text-align:left">
        <div style="height:26px; overflow:hidden; padding-top:6px" id="tab_container">
            <table cellspacing="0" cellpadding="0"><tr>
            <?
            $fixed_menu = array(""=>"");
            if (c2dbGetSystemConfig("spf.ActiveDashboardMenu")) $fixed_menu["Dashboard"]= "ダッシュボード";
            if (c2dbGetSystemConfig("spf.ActiveSendStatMenu")) $fixed_menu["SendStat"]= "申請一覧";
            if (c2dbGetSystemConfig("spf.ActiveRecvStatMenu")) $fixed_menu["RecvStat"]= "受付一覧";

            foreach ($fixed_menu as $bid => $label) {
            ?>
                <td id="tdtab_<?=$bid?>" style="display:none" navi="セクション位置へスクロール（非表示状態なら最新表示）">
                    <a href="javascript:void(0)" class="no_underline" onclick="moveSummarySect('<?=$bid?>')"
                    ><table cellspacing="0" cellpadding="0"><tr><td><div><nobr><?=$label?></nobr></div></td></tr></table></a>
                    <span><?=$label?></span>
                </td>
            <?
            }
            foreach ($finderRows as $finder_id => $finder_title) {
            ?>
                <td id="tdtab_Finder<?=$finder_id?>" style="display:none" navi="セクション位置へスクロール（非表示状態なら最新表示）">
                    <a href="javascript:void(0)" class="no_underline" onclick="moveSummarySect('Finder<?=$finder_id?>')"
                    ><table cellspacing="0" cellpadding="0"><tr><td><div><nobr><?=$finder_title?></nobr></div></td></tr></table></a>
                    <span><?=$finder_title?></span>
                </td>
                <!-- td id="tdtab_Finder<?=$finder_id?>Result" style="display:none" navi="セクション位置へスクロール（非表示状態なら最新表示）">
                    <a href="javascript:void(0)" class="no_underline" onclick="moveSummarySect('Finder<?=$finder_id?>Result')"
                    ><table cellspacing="0" cellpadding="0"><tr><td><div><nobr><?=$finder_title?></nobr></div></td></tr></table></a>
                    <span><?=$finder_title?></span>
                </td -->
            <? } ?>

            <? foreach ($menuRows as $bid => $row) {
                if ($row["menu_hier"]== "dai") continue;
            ?>
                <td id="tdtab_Menu<?=$bid?>" style="display:none">
                    <a href="javascript:void(0)" class="no_underline" onclick="moveBSect('<?=$bid?>')" navi="セクション位置へスクロール（非表示状態なら最新表示）"
                    ><table cellspacing="0" cellpadding="0"><tr><td><div><nobr><?=$row["menu_label"]?></nobr></div></td></tr></table></a>
                    <span><?=$row["menu_label"]?></span>
                </td>
            <? } ?>
            </tr></table>
        </div>
        </td>


        <td width="60" style="padding-right:30px; padding-top:3px"><!--button type="button" oncilck="contentPrint()" class="w70">印刷</button--></td>
        <?
            $jin_menu = "";
            if (!IS_ADMIN_AUTH && IS_JIN_AUTH) $jin_menu = trim(c2dbGetSystemConfig("spf.JinjiComponents"));
            if (IS_ADMIN_AUTH || $jin_menu) {
        ?>
        <td width="100" class="right"><a href="index_admin.php" class="to_admin block font12" style="line-height:16px; padding:5px 8px">→管理画面へ</a></td>
        <? } ?>
        <? if (c2dbGetSystemConfig("spf.AppCloseBtnEnabled")) { ?>
        <td width="30" class="app_closebtn"><a href="javascript:window.close()" title="スタッフ・ポートフォリオ画面を閉じる"></a></td>
        <? } ?>
    </tr></table>
</div>




<? //============================================================================================ ?>
<? // 左側：メニュー                                                                              ?>
<? //============================================================================================ ?>
<table cellspacing="0" cellpadding="0" style="width:100%" id="main_table"><tr>
<td style="width:180px" class="print-none">
<div style="overflow-y:scroll; border-top:1px solid #b3c5d9" id="div_menu_wrapper">
<div style="padding-bottom:300px">

    <? if (c2dbGetSystemConfig("spf.ActiveDashboardMenu") || c2dbGetSystemConfig("spf.ActiveSendStatMenu") || c2dbGetSystemConfig("spf.ActiveRecvStatMenu"))  { ?>
    <div style="padding:0 5px 7px 0;" class="div_menu">
        <div style="padding:3px; background:url(img/bk_sect_bar.gif) repeat-x 0 -16px; color:#ffffff; border-top:1px solid #dddddd; font-size:12px; letter-spacing:2px; height:24px"><div style="background:url(img/iset_menutitle.gif) no-repeat 0 0; padding-left:20px">申請・受付</div></div>
        <div class="menu_container" id="menu_Apply_container" style="padding-left:5px">
            <? if (c2dbGetSystemConfig("spf.ActiveDashboardMenu")) { ?>
            <table cellspacing="0" cellpadding="0"><tr id="menu_Dashboard">
                <td class="refresh" onclick="menuShowSummarySect('Dashboard')" navi="セクションの最新表示 ⇒ セクション位置へスクロール"></td>
                <td class="move" onclick="moveSummarySect('Dashboard')" navi="セクション位置へスクロール（非表示状態なら最新表示）"><nobr>ダッシュボード</nobr></td>
                <td class="hide" onclick="closeSection('Dashboard')" navi="セクションを閉じる"></td>
            </table>
            <? } ?>

            <? if (c2dbGetSystemConfig("spf.ActiveSendStatMenu")) { ?>
            <table cellspacing="0" cellpadding="0"><tr id="menu_SendStat">
                <td class="refresh" onclick="menuShowSummarySect('SendStat')" navi="セクションの最新表示 ⇒ セクション位置へスクロール"></td>
                <td class="move" onclick="moveSummarySect('SendStat')" navi="セクション位置へスクロール（非表示状態なら最新表示）"><nobr>申請一覧</nobr></td>
                <td class="hide" onclick="closeSection('SendStat')" navi="セクションを閉じる"></td>
            </table>
            <? } ?>

            <? if (c2dbGetSystemConfig("spf.ActiveRecvStatMenu")) { ?>
            <table cellspacing="0" cellpadding="0"><tr id="menu_RecvStat">
                <td class="refresh" onclick="menuShowSummarySect('RecvStat')" navi="セクションの最新表示 ⇒ セクション位置スクロール"></td>
                <td class="move" onclick="moveSummarySect('RecvStat')" navi="セクション位置へスクロール（非表示状態なら最新表示）"><nobr>受付一覧</nobr></td>
                <td class="hide" onclick="closeSection('RecvStat')" navi="セクションを閉じる"></td>
            </table>
            <? } ?>
        </div>
    </div>
    <? } ?>


    <? if (count($finderRows)) { ?>
    <div style="padding:0px 5px 2px 0;" class="div_menu">
        <div style="padding:3px; background:url(img/bk_sect_bar.gif) repeat-x 0 -16px; color:#ffffff; border-top:1px solid #dddddd; font-size:12px; letter-spacing:2px; height:24px; position:relative">
            <? if (c2dbGetSystemConfig("spf.AllowFinderSelector")) { ?>
            <a id="popup_finder_selector" onclick="popupFinderIdSelector()"></a>
            <? } ?>
            <div style="background:url(img/iset_menutitle.gif) no-repeat 0 -16px; padding-left:20px">検索</div>
        </div>
        <? foreach ($finderRows as $finder_id => $finder_title) { ?>
        <?      $fid = "Finder".$finder_id; ?>
        <?      $disp = $fid = "Finder".$finder_id; ?>
        <?      $disp_style = (strpos($empConfigRow["hide_finder_ids"], '['.$finder_id.']')!==FALSE ? " display:none" : ""); ?>
        <div class="menu_container" id="divmenu_<?=$fid?>"
            style="padding-left:5px;<?=$disp_style?>">
            <table cellspacing="0" cellpadding="0"><tr id="menu_<?=$fid?>">
                <td class="refresh" onclick="menuShowSummarySect('<?=$fid?>',lastSummaryCallOptions['<?=$fid?>'].option)" navi="セクションの最新表示 ⇒ セクション位置へスクロール"></td>
                <td class="move" onclick="moveSummarySect('<?=$fid?>')" navi="セクション位置へスクロール（非表示状態なら最新表示）">
                <div style="width:115px; overflow:hidden"><nobr><?=$finder_title?></nobr></div></td>
                <td class="hide" onclick="closeSection('<?=$fid?>')" navi="セクションを閉じる"></td>
            </table>

            <div style="display:none">
                <table cellspacing="0" cellpadding="0"><tr id="menu_<?=$fid?>Result" style="display:none">
                    <td class="refresh" onclick="menuShowSummarySect('<?=$fid?>Result')" navi="セクションの最新表示 ⇒ セクション位置へスクロール"></td>
                    <td class="move" onclick="moveSummarySect('<?=$fid?>Result')">⇒検索結果</td>
                    <td class="hide" onclick="closeSection('<?=$fid?>Result')" navi="セクションを閉じる"></td>
                </table>
            </div>
        </div>
        <? } ?>
    </div>
    <? } ?>


    <?
        //--------------------------------------------------
        // 対象者の個人情報
        //--------------------------------------------------
    ?>
    <div style="padding:5px 5px 0 0;">
        <div style="padding:3px; background:url(img/bk_sect_bar.gif) repeat-x 0 -16px; color:#ffffff; border-top:1px solid #dddddd; font-size:12px; letter-spacing:2px; height:24px"><div style="background:url(img/iset_menutitle.gif) no-repeat 0 -32px; padding-left:20px"><?=$capEmpProfile?></div></div>
        <table cellspacing="0" cellpadding="0"><tr>
        <td style="padding:0 0 3px 5px" id="emp_selector_container">
        </td>
        <td style="padding:2px 0 0 2px"><a href="javascript:void(0)" id="emp_info_link" class="emp_info_link" onclick="showEmpInfoSect()" style="padding:0"></a></td>
        </tr></table>
        <? // ここに個人情報 ?>
        <div id="sect_EmpInfo" class="menu_title" style="display:none"></div>
    </div>


    <div style="padding:5px 5px 5px 0; border-left:5px solid #ffffff" class="div_menu div_profile_menu" id="div_profile_menu_outer">
    <?
        //--------------------------------------------------
        // 個別プロファイルメニュー
        //--------------------------------------------------
        $behavior = c2dbGetSystemConfig("spf.DaiMenuBehavior");
        foreach ($menuList as $dai_muid => $chu_menu_rows) {
            $menuRow = $menuRows[$dai_muid];
            //if (!count(isDispOrEditable("display", $loginEmpmstRow, $menuRow))) continue;
            echo '<div id="menu_DaiMenu'.$dai_muid.'_outer">';
            echo '<a href="javascript:void(0)" onclick="toggleNode(this.id)" id="menu_DaiMenu'.$dai_muid.'"';
            echo ' style="position:relative; display:block; padding:0; height:18px'.($behavior=="hidden" ?'; display:none;' : '').'"';
            echo ($behavior=="hidden" || $behavior=="expanded" ? ' class="menu_selected"' : '');
            echo '>';
            echo '<div class="menu_dai_icon"></div>';
            echo '<div class="menu_dai_caption"><nobr>'.hh($menuRow["menu_label"])."</nobr></div></a>\n";
            echo '<div class="menu_container" id="menu_DaiMenu'.$dai_muid.'_container"';
            echo ($behavior=="hidden" || $behavior=="expanded" ? '' : ' style="display:none"');
            echo '>'."\n";
            foreach ($chu_menu_rows as $chu_muid => $syo_menu_rows) {
                $menuRow = $menuRows[$chu_muid];
                //if (!count(isDispOrEditable("display", $loginEmpmstRow, $menuRow))) continue;

                echo '<table cellspacing="0" cellpadding="0" id="table_menu'.$chu_muid.'"><tr id="menu_Menu'.$chu_muid.'" muid="'.$chu_muid.'">';
                echo '  <td class="refresh" onclick="showBSect(\''.$chu_muid.'\')" navi="セクションの最新表示 ⇒ セクション位置へスクロール"></td>';
                echo '  <td class="move" onclick="moveBSect(\''.$chu_muid.'\')" navi="セクション位置へスクロール（非表示状態なら最新表示）">';
                echo '<div><nobr>'.hh($menuRow["menu_label"]).'</nobr></div></td>';
                echo '  <td class="hide" onclick="closeSection(\'Menu'.$chu_muid.'\')" navi="セクションを閉じる"></td>';
                echo '</table>'."\n";
            }
            echo '</div>';
            echo '</div>';
        }
    ?>
    </div>

    <div style="padding:10px 5px 5px 5px;" class="div_menu div_profile_menu">
        <div style="border-top:2px solid #ddaa44; padding-top:5px">
            <div navititle="こかナビ表示切替" navi="こかナビとは、この表示です。<br>チェックをオフにすると表示しません。"><label><input type="checkbox" value="1" checked onclick="gNavi.activeMode=(!gNavi.activeMode ? 1 : 0);" />こかナビ&nbsp;オン</label></div>
        </div>
    </div>

</div>
</td>
<? // 左側おわり ?>



<? //============================================================================================ ?>
<? // 右側：セクションコンテンツ                                                                  ?>
<? //============================================================================================ ?>
<td>
<div id="div_content" style="padding:0 5px 0 0; position:relative; overflow-y:scroll; border-top:1px solid #b3c5d9;">
    <div id="div_profile_receive_stocker" style="display:none; border:2px solid #dd0000"></div>
    <div id="input_dai_chu_container" style="position:relative; top:0px">
    <div class="input_section" id="sect_Dashboard" style="display:none;"></div>
    <div class="input_section" id="sect_SendStat"  style="display:none;"></div>
    <div class="input_section" id="sect_RecvStat"  style="display:none;"></div>

    <? foreach ($finderRows as $finder_id => $finder_title) { ?>
    <?  $fid = "Finder".$finder_id; ?>
    	<div>
		    <div class="input_section" id="sect_<?=$fid?>"    style="display:none; margin-bottom:0"></div>
		    <div class="input_section finder_result_section" id="sect_<?=$fid?>Result" style="display:none"></div>
	    </div>
    <?
    }
    //--------------------------------------------------
    // 個人プロファイル群の中分類コンテンツコンテナ
    //--------------------------------------------------
    foreach ($menuList as $dai_muid => $chu_menu_rows) {
        foreach ($chu_menu_rows as $chu_muid => $syo_menu_rows) {
            $menuRow = $menuRows[$chu_muid];
            echo '<div class="div_content input_section" id="sect_Menu'.$chu_muid.'" style="display:none;"></div>'."\n";
        }
    }
    echo '</div>';
    echo '<div id="input_syo_container" style="position:relative; top:-40px">';

    //--------------------------------------------------
    // 個人プロファイル群の小分類コンテンツコンテナ
    //--------------------------------------------------
    foreach ($menuList as $dai_muid => $chu_menu_rows) {
        foreach ($chu_menu_rows as $chu_muid => $syo_menu_rows) {
            $menuRow = $menuRows[$chu_muid];
            echo '<div class="input_children" id="sect_Menu'.$chu_muid.'_children">';
            foreach ($syo_menu_rows as $syo_muid) {
                echo '<div class="div_content" id="sect_Menu'.$syo_muid.'" menu_hier="syo" style="position:relative; top:0; display:none;"></div>'."\n";
            }
            echo '</div>';
        }
    }
    ?>
    </div>
    <div id="cont_margin">&nbsp;</div>
</div>

</td></tr></table>
<? // 右側おわり ?>

</div><!-- //CLIENT_FRAME_CONTENT -->

<div style="position:absolute"><div id="dialog_workspace"></div></div>


<div id="calendar_dialog_container" style="display:none"><div id="calendar_dialog_content" style="padding:5px 0 0 0"></div></div>

<div id="div_download_form"></div>

<div id="finder_id_selector_dialog" style="display:none; padding-top:10px">
    <div class="gray" style="padding-bottom:5px">常時表示する検索条件を指定できます。<br>この設定は個人ごとに保存されます。</div>
    <div id="finder_id_selector_dialog_inner" style="width:245px; height:350px; border:1px solid #999999; overflow-y:scroll; padding:5px;"></div>
</div>

</body>
<script type="text/javascript">
    resizeIframeEx();
</script>
</html>