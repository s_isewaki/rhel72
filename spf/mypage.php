<?
// マイページへの埋め込み用コンテンツ。ただし、何かを通知したりする予定がないので、マイページにはハコだけ表示する。
function show_spf_mypage_notice($con, $emp_id, $session, $fname, $font_size) {
?>

<table width="100%" cellpadding="0" cellspacing="0" style="border:0; border-collapse:collapse" class="j12">
<tr>
<td style="border:1px solid #5279a5; background:url(img/icon/s54.gif) no-repeat #bdd1e7 3px 2px; padding:3px">
    <b><a href="spf/index.php?session=<?=$session?>" style="padding-left:26px">スタッフ・ポートフォリオ</a></b>
</td>
</tr>
<tr>
<td style="border:1px solid #5279a5; padding:3px">
    現在、表示コンテンツは設定されていません。
</td>
</tr>
</table>
<? } ?>
