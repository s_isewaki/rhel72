<?


//****************************************************************************************************************
// リスト構成マスタ
//****************************************************************************************************************
function html_mst_list_struct() {
    global $MASTER_TABLES;
    $luid = (int)$_REQUEST["luid"];
    $show_all = (int)$_REQUEST["show_all"];
    $listFieldDef = c2dbGetRows("select * from spfm_list_field_def order by table_id, field_id");
    $lineupRows = c2dbGetRows("select * from spfm_list_lineup order by luid");
    $rows = c2dbGetRows("select * from spfm_list_struct where luid = ".$luid." order by field_order");
    $listStructs = array();
    foreach ($rows as $row) {
        $listStructs[$row["table_id"]."@".$row["field_id"]."@".$luid] = $row;
    }
    $pRow = c2dbGetTopRow("select * from spfm_list_lineup where luid = ".$luid);
    $field_label = $pRow["field_label"];
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        $("#mst_list_struct_sortable").sortable({revert:true, update:function(event, ui) {
            setOrder();
         }});
         function setOrder() {
            var idOrder = [];
            $("#mst_list_struct_sortable tr").each(function(){
                idOrder.push(this.getAttribute("field_id"));
            });
            ee("mst_list_struct_order").value = idOrder.join(",");
        }
        function changeView() {
			showAdminSect('mst_list_struct', '', 'div_section_header');
		}
		function toggleShowAll(bool) {
			if (bool) {
				$("#mst_list_struct_stocker tr.is_def").each(function(){
					ee("mst_list_struct_sortable").appendChild(this);
				});
			} else {
				$("#mst_list_struct_sortable tr.is_def").each(function(){
					ee("mst_list_struct_stocker").appendChild(this);
				});
			}
			setOrder();
		}
		function chageIsActive(bool, fid) {
			ee("trid_"+fid).className = (bool ? "" : "is_def");
		}
        setOrder();
    </textarea>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <input type="hidden" name="luid" value="<?=hh($luid)?>" />
    <div id="div_section_header">
        <table cellspacing="0" cellpadding="0" class="section_title"><tr>
            <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">E</th><? } ?>
            <th class="section_title_title" style="width:200px"><nobr>リスト構成</nobr></th>
            <th style="font-size:12px; letter-spacing:0"><nobr>
                リスト
                <select onchange="changeView()" name="luid">
                <? foreach ($lineupRows as $row) {
                    $field_label = $row["field_label"];
                    if (!$field_label && $row["is_active"]) $field_label = "【名称未設定】";
                    if (!$row["is_active"]) $field_label = "（空き/非利用）".$field_label;
                ?>
                <option value="<?=$row["luid"]?>"<?=($row["luid"]==$luid?" selected":"")?>><?=$row["luid"]?>&nbsp;<?=hh($field_label)?></option>
                <? } ?>
                </select>
                <label class="white_label"><input type="checkbox" name="show_all" value="1" onclick="toggleShowAll(this.checked)">全候補を表示</label>
            </nobr></th>
            <th class="button"><nobr><button type="button" onclick="tryAdminReg('right_area', 'reg_mst_list_struct')" class="w70">保存</button></nobr></th>
        </tr></table>

        <div class="finder_box" style="padding:5px">
        <table cellspacing="0" cellpadding="0">
        <tr>
            <td class="middle">
            	<label><input type="checkbox" name="spfm_list_lineup@is_active" value="1" <?=($pRow["is_active"]?" checked":"")?> />利用する</label>
            </td>
            <td class="middle" style="padding:0px 4px 0 20px">リスト名</td>
            <td><input type="text" class="text" name="spfm_list_lineup@field_label" value="<?=hh($pRow["field_label"])?>" /></td>
        </tr>
        </table>
        </div>

        <div style="padding:5px 0 2px 0"><b>■リスト列設定</b></div>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div class="smallsize">
    <table cellspacing="0" cellpadding="0" class="list_table">
    <tr class="pale">
        <th>No.<br><nobr>利用</nobr></th>
        <th>テーブルID<br>フィールドID</th>
        <th><nobr>本人・監督・承認・人事</nobr></th>
        <th>ラベル</th>
        <th>入力タイプ</th>
        <th>入力必須<br>日本語IME</th>
        <th>候補（DBマスタ連動）<br>入力選択肢</th>
        <th><nobr>入力時書式<br>入力最大文字数</nobr></th>
        <th><nobr>内容<br>欄</nobr></th>
        <th><nobr>ナビ</nobr></th>
        <th><nobr>ﾘｽﾄ<br>ｱﾗｲﾝ</nobr></th>
        <th><nobr>ｵｰﾀﾞｰ<br>ﾊﾞｲ</nobr></th>
        <th><nobr>一覧<br>非表示</nobr></th>
        <th><nobr>一覧<br>入力</nobr></th>
    </tr>
    <tbody id="mst_list_struct_sortable" class="hover_colord">
    <?
        $listFieldDefIdList = array();
        $defTypeList = array();
        $selectedCount = count($listStructs);
        foreach ($listFieldDef as $row) {
            $row["is_def"] = "is_def";
            $field_id = $row["field_id"];
            $fid = $row["table_id"]."@".$field_id."@".$luid;
            $listFieldDefIdList[$fid] = 1;
            $types = explode(",", $row["available_field_type"]);
            foreach ($types as $t) if ($t) $defTypeList[$fid][$t] = 1;
            if (!$listStructs[$fid]) $listStructs[$fid] = $row;
        }
        $rowNumber = 0;
        foreach ($listStructs as $fid => $row) {
            if (!$listFieldDefIdList[$fid]) continue;
            $ftype = $row["field_type"];
            $rowNumber++;
            $aftypes = $defTypeList[$fid];
    ?>
    <? if ($rowNumber==$selectedCount+1) { ?>
    </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" style="display:none">
    <tbody id="mst_list_struct_stocker">
    <? } ?>

    <tr navi="<?=hh($row["field_label"])?>" id="trid_<?=$fid?>" field_id="<?=$fid?>" class="<?=$row["is_def"]?>">
        <td style="line-height:24px">
        	<?=$rowNumber?><br>
        	<input type="checkbox" name="<?=$fid?>@is_active"<?=(!$row["is_def"]?" checked":"")?> onclick="chageIsActive(this.checked, '<?=$fid?>')" />
        </td>

        <? // ----------------------------------------------------- ?>
        <? // テーブルID・フィールドID                              ?>
        <? // ----------------------------------------------------- ?>
        <td style="cursor:move">
            <div class="gray" style="line-height:24px"><?=$row["table_id"]?></div>
            <div class="right" style="line-height:24px"><?=$row["field_id"]?></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 権限                                                  ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <div class="gray" style="line-height:24px"><nobr>
            表示：
            <input type="checkbox" value="1" name="<?=$fid?>@is_usr_display"<?=($row["is_usr_display"]?" checked":"")?> navi="本人表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_kan_display"<?=($row["is_kan_display"]?" checked":"")?> navi="監督者表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_syo_display"<?=($row["is_syo_display"]?" checked":"")?> navi="承認者/評価者表示権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_jin_display"<?=($row["is_jin_display"]?" checked":"")?> navi="人事表示権限" />
            </nobr></div>
            <div class="gray" style="line-height:24px"><nobr>
            編集：
            <input type="checkbox" value="1" name="<?=$fid?>@is_usr_editable"<?=($row["is_usr_editable"]?" checked":"")?> navi="本人編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_kan_editable"<?=($row["is_kan_editable"]?" checked":"")?> navi="監督者編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_syo_editable"<?=($row["is_syo_editable"]?" checked":"")?> navi="承認者/評価者編集権限" />
            <input type="checkbox" value="1" name="<?=$fid?>@is_jin_editable"<?=($row["is_jin_editable"]?" checked":"")?> navi="人事編集権限" />
            </nobr></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // ラベル                                                ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <input type="text" class="text w120" value="<?=hh($row["field_label"])?>" name="<?=$fid?>@field_label" />
            <div style="padding-top:2px" class="gray">
            ｱﾗｲﾝ
            <select name="<?=$fid?>@label_align" navititle="ラベルのアライン" navi="右：右寄せ表示、左：左寄せ表示、中：中央表示">
                <option value="right">右</option>
                <option value="left"<?=($row["label_align"]=="left"?" selected":"")?>>左</option>
                <option value="center"<?=($row["label_align"]=="center"?" selected":"")?>>中</option>
            </select>
            </div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 入力タイプ                                            ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <select name="<?=$fid?>@field_type" navititle="入力タイプ" style="color:#dd4444; font-weight:bold">
                <? $fsel = array($ftype=>" selected"); ?>
                <option value="text"<?=$fsel["text"]?><?=($aftypes["text"]?'':' disabled')?>>テキスト</option>
                <option value="textarea"<?=$fsel["textarea"]?><?=($aftypes["textarea"]?'':' disabled')?>>複数行テキスト</option>
                <option value="pulldown"<?=$fsel["pulldown"]?><?=($aftypes["pulldown"]?'':' disabled')?>>プルダウン</option>
                <option value="yymm"<?=$fsel["yymm"]?><?=($aftypes["yymm"]?'':' disabled')?>>年月</option>
                <option value="ymd"<?=$fsel["ymd"]?><?=($aftypes["ymd"]?'':' disabled')?>>年月日</option>
                <option value="ymdhm"<?=$fsel["ymdhm"]?><?=($aftypes["ymdhm"]?'':' disabled')?>>年月日時分</option>
                <option value="image"<?=$fsel["image"]?><?=($aftypes["image"]?'':' disabled')?>>画像</option>
                <option value="label"<?=$fsel["label"]?><?=($aftypes["label"]?'':' disabled')?>>表示のみ</option>
            </select>
            <div style="padding-top:2px" class="gray"><nobr>
            ｱﾗｲﾝ
            <select name="<?=$fid?>@text_align" navititle="入力時アライン" navi="入力ボックス内やプルダウン内において、右：右寄せ表示、左：左寄せ表示、中：中央表示">
                <option value="left">左</option>
                <option value="right"<?=($row["text_align"]=="right"?" selected":"")?>>右</option>
            </select>
            幅
            <input type="text" class="text imeoff w30" name="<?=$fid?>@field_width" value="<?=hh($row["field_width"])?>"
            navititle="幅" navi="入力ボックスの幅であり、リスト表示される場合は基準の列幅となります。" />
            </nobr></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 入力必須・日本語IME                                   ?>
        <? // ----------------------------------------------------- ?>
        <td><nobr>
            <select name="<?=$fid?>@is_hissu" navititle="入力必須" navi="「入力必須」を指定すると、空値のまま保存操作を行えないようになります。">
                <option value=""></option>
                <option value="1"<?=($row["is_hissu"]=="1"?" selected":"")?>>入力必須</option>
            </select>
            </nobr><br>
            <div style="padding-top:2px" class="right"><nobr>
                <select name="<?=$fid?>@ime_off" navititle="日本語IME" navi="「IMEオフ」にすると、入力ボックス上ではキーボード全角禁止モードとなりますが、コピー＆ペーストは許容されます。">
                    <option value=""></option>
                    <option value="1"<?=($row["ime_off"]=="1"?" selected":"")?>>IMEオフ</option>
                </select>
            </nobr></div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // DBマスタ連動・入力選択肢                              ?>
        <? // ----------------------------------------------------- ?>
        <td>
            <select name="<?=$fid?>@rel_master_id" onchange="ee('divps_<?=$fid?>').style.display=(this.value?'none':'');" style="width:180px"
            navititle="DBマスタ連動" navi="あらかじめ用意されたマスターリストが利用できます。
            入力タイプが「プルダウン」「テキスト」「複数行テキスト」のときに利用することができます。
            「プルダウン」のときは、DBには規定の項目マスタコードが保存されます。
            「テキスト」「複数行テキスト」の場合は、選択肢は「候補」ポップアップで提供され、DBには選択文字列そのものが保存されます。
            「テキスト」「複数行テキスト」に対して、「候補」以外の自由入力を許容するには、「+」つき項目を指定してください。"
            >
                <option value=""></option>
                <? foreach ($MASTER_TABLES as $mtid => $mtnm) { ?>
                <option value="<?=$mtid?>"<?=($mtid==$row["rel_master_id"]?" selected":"")?>><?=$mtid?> <?=$mtnm?></option>
                <? if ($ftype=="text" || $ftype=="textarea") { ?>
                <option value="<?=$mtid?>+"<?=(($mtid."+")==$row["rel_master_id"]?" selected":"")?>><?=$mtid?>+ <?=$mtnm?>（＋自由入力）</option>
                <? } ?>
                <? } ?>
            </select>
            <div id="divps_<?=$fid?>" style="padding-top:2px;<?=($row["rel_master_id"]?"display:none":"")?>">
                <input type="text" class="text" value="<?=hh($row["pulldown_selections"])?>" name="<?=$fid?>@pulldown_selections"
                navititle="入力選択肢" navi="「A=1,B=2,C=3, ... key=value」形式で指定すると、画面にはvalue、データベースにはkeyが保存されます。
                「A,B,C ... 」形式で指定すると、画面およびデータベースともに同じ値で保存されます。" />
            </div>
        </td>
        <? // ----------------------------------------------------- ?>
        <? // 入力時書式・最大文字数                                ?>
        <? // ----------------------------------------------------- ?>
        <td class="gray"><nobr>
        <? if ($ftype=="ymd" || $ftype=="yymm") { ?>
            <? $regexp = explode(",", $row["format_regexp"]); ?>
            最小
            <select name="<?=$fid?>@format_regexp1" onchange="ee('<?=$fid?>@format_regexp2').style.display=(this.value==''?'none':'');">
                <option value=""<?=$regexp[0]==""?"selected":""?>>なし</option>
                <option value="today-"<?=$regexp[0]=="today-"?"selected":""?>>入力日前</option>
                <option value="today+"<?=$regexp[0]=="today+"?"selected":""?>>入力日後</option>
            </select>
            <span id="<?=$fid?>@format_regexp2" style="<?=$regexp[0]==""?"display:none":""?>">
            <input type="text" class="text imeoff w40" name="<?=$fid?>@format_regexp2" value="<?=hh($regexp[1])?>" maxlength="5"
            navi="おおよそ1年なら365、100年なら36500というように指定してください。<br>空欄またはゼロを指定すると入力日当日と判断されます。" />
            <?=($ftype=="yymm"?"月":"日")?>
            </span>
            <div style="padding-top:2px">
            最大
            <select name="<?=$fid?>@format_regexp3" onchange="ee('<?=$fid?>@format_regexp4').style.display=(this.value==''?'none':'');">
                <option value=""<?=$regexp[2]==""?"selected":""?>>なし</option>
                <option value="today-"<?=$regexp[2]=="today-"?"selected":""?>>入力日前</option>
                <option value="today+"<?=$regexp[2]=="today+"?"selected":""?>>入力日後</option>
            </select>
            <span id="<?=$fid?>@format_regexp4" style="<?=$regexp[0]==""?"display:none":""?>">
            <input type="text" class="text imeoff w40" name="<?=$fid?>@format_regexp4" value="<?=hh($regexp[3])?>" maxlength="5"
            navi="おおよそ1年なら365、100年なら36500というように指定してください。<br>空欄またはゼロを指定すると入力日当日と判断されます。"
            />
            <?=($ftype=="yymm"?"月":"日")?>
            </span>
            </div>
        <? } else if ($ftype=="text" || $ftype=="textarea") { ?>
            <input type="text" class="text" value="<?=hh($row["format_regexp"])?>" name="<?=$fid?>@format_regexp"
            navititle="入力時書式" navi="正規表現を使って記述してください。JavaScript/PHP双方で利用されます。
            例）数字1文字以上：&nbsp;&nbsp;^[0-9]+$
            例）数字ちょうど3文字：&nbsp;&nbsp;^[0-9]{3}$
            例）数字またはアルファベット：&nbsp;&nbsp;^[0-9a-zA-Z]+$
            例）数字またはアルファベットだが、必ずアルファベットで開始：&nbsp;&nbsp;^[a-zA-X]+[0-9a-zA-Z]*$
            例）アルファベットで開始していればよい：&nbsp;&nbsp;^[a-zA-X]
            例）アルファベットで終了していればよい：&nbsp;&nbsp;[a-zA-X]$
            例）アルファベットで終了していればよい：&nbsp;&nbsp;[a-zA-X]$"
             />
            <div style="padding-top:2px" class="gray">
                最大文字数
                <input type="text" class="text imeoff w40" value="<?=hh($row["max_length"])?>" name="<?=$fid?>@max_length"
                navititle="最大文字数" navi="HTMLタグの「maxlength」の値です。入力ボックスに入力可能な文字の数です。
                複数行テキストへ指定した場合、最新ブラウザでなければ無効な場合があります。
                システム内に規定の上限がある場合があり、それを超えることはできません。"
                 />
            </div>
        <? } ?>
        </nobr></td>
        <? // ----------------------------------------------------- ?>
        <? //                                 ?>
        <? // ----------------------------------------------------- ?>
        <td><input type="checkbox" value="1" name="<?=$fid?>@is_subject"<?=($row["is_subject"]?" checked":"")?> /></td>
        <td><input type="text" class="text" name="<?=$fid?>@field_navi" value="<?=hh($row["field_navi"])?>" /></td>
        <td>
            <select name="<?=$fid?>@list_align">
                <option value="left">左</option>
                <option value="center"<?=($row["list_align"]=="center"?" selected":"")?>>中</option>
                <option value="right"<?=($row["list_align"]=="right"?" selected":"")?>>右</option>
            </select>
        </td>
        <td>
            <select name="<?=$fid?>@order_by">
                <option value=""></option>
                <option value="1_asc"<?=($row["order_by"]=="1_asc"?" selected":"")?>>1：昇順</option>
                <option value="1_desc"<?=($row["order_by"]=="1_desc"?" selected":"")?>>1：降順</option>
                <option value="2_asc"<?=($row["order_by"]=="2_asc"?" selected":"")?>>2：昇順</option>
                <option value="2_desc"<?=($row["order_by"]=="2_desc"?" selected":"")?>>2：降順</option>
                <option value="3_asc"<?=($row["order_by"]=="3_asc"?" selected":"")?>>3：昇順</option>
                <option value="3_desc"<?=($row["order_by"]=="3_desc"?" selected":"")?>>3：降順</option>
            </select>
        </td>
        <td>
            <select name="<?=$fid?>@is_hide_from_list">
                <option value=""></option>
                <option value="1"<?=($row["is_hide_from_list"]=="1"?" selected":"")?>>○</option>
            </select>
        </td>
        <td>
            <select name="<?=$fid?>@is_list_input">
                <option value=""></option>
                <option value="1"<?=($row["is_list_input"]=="1"?" selected":"")?>>○</option>
            </select>
        </td>
    </tr>
        <? } ?>
    </tbody>
    </table>
    <input type="hidden" name="mst_list_struct_order" id="mst_list_struct_order" />
    </div>
<?
}
//================================================================================================================
// リスト構成マスタの保存
//================================================================================================================
function reg_mst_list_struct() {
    $luid = (int)$_REQUEST["luid"];
    if (!$luid) { echo "リクエストが不正です。保存対象のリストが不明です。"; die; }
    if ($_REQUEST["spfm_list_lineup@is_active"]) {
	    if (!$_REQUEST["mst_list_struct_order"]) { echo "リクエストが不正です。画面のリスト構成が取得できませんでした。"; die; }
	}

    c2dbBeginTrans();
    $sql =
    " update spfm_list_lineup set".
    " is_active = ".c2dbStr($_REQUEST["spfm_list_lineup@is_active"]).
    ",field_label = ".c2dbStr($_REQUEST["spfm_list_lineup@field_label"]).
    " where luid = ".$luid;
    c2dbExec($sql);

    c2dbBeginTrans();
    $sql =
    " update spfm_list_lineup set is_need_approval_syozoku = '', is_need_approval_yakusyoku = '', is_need_sinsei = ''".
    " where not is_active = '1'";
    c2dbExec($sql);

    $field_order = 0;
    c2dbExec("delete from spfm_list_struct where luid = ".$luid);
    $order_list = explode(",", $_REQUEST["mst_list_struct_order"]);

    foreach ($order_list as $prefix) {
        list($table_id, $field_id, $_luid) = explode("@", $prefix);
        if (!$_REQUEST[$prefix."@is_active"]) continue;
        $format_regexp = $_REQUEST[$prefix."@format_regexp"];
        if ($_REQUEST[$prefix."@field_type"]=="yymm" || $_REQUEST[$prefix."@field_type"]=="ymd") {
            $format_regexp =
            $_REQUEST[$prefix."@format_regexp1"].",".
            (int)str_replace(",","",str_replace("-","",$_REQUEST[$prefix."@format_regexp2"])).",".
            $_REQUEST[$prefix."@format_regexp3"].",".
            (int)str_replace(",","",str_replace("-","",$_REQUEST[$prefix."@format_regexp4"]));
        }

        $field_order++;
        $sql =
        " insert into spfm_list_struct (".
        "     table_id, luid, field_id, field_order, field_label, field_navi, field_type, field_width".
        "   , ime_off, is_hissu, format_regexp, label_align, text_align, list_align, pulldown_selections, is_hide_from_list, is_list_input".
        "   , max_length, order_by, rel_master_id".
        "   , is_usr_display, is_kan_display, is_syo_display, is_jin_display".
        "   , is_usr_editable, is_kan_editable, is_syo_editable, is_jin_editable, is_subject".
        ") values (".
        "     ".c2dbStr($table_id).
        "    ,".c2dbStr($_luid).
        "    ,".c2dbStr($field_id).
        "    ,".c2dbStr(sprintf("%02d", $field_order)).
        "    ,".c2dbStr($_REQUEST[$prefix."@field_label"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@field_navi"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@field_type"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@field_width"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@ime_off"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_hissu"]).
        "    ,".c2dbStr($format_regexp).
        "    ,".c2dbStr($_REQUEST[$prefix."@label_align"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@text_align"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@list_align"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@pulldown_selections"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_hide_from_list"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_list_input"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@max_length"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@order_by"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@rel_master_id"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_usr_display"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_kan_display"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_syo_display"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_jin_display"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_usr_editable"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_kan_editable"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_syo_editable"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_jin_editable"]).
        "    ,".c2dbStr($_REQUEST[$prefix."@is_subject"]).
        ")";
        c2dbExec($sql);
    }
    c2dbCommit();
    echo "ok".'{forwardSection:"mst_list_struct", forwardOption:"&luid='.$luid.'"}';
    die;
}
