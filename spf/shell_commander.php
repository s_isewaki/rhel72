<?
//****************************************************************************************************************
// シェルコマンダー
//
// 事前に、「製品管理」ログインで、最も特別なログイン「God Login」していないと利用できません。
//
//
// メンテナンス用に、テキストボックスに入力されたlinuxコマンドを実行するだけのものです。
// 取扱いは注意ください。
//****************************************************************************************************************
require_once("common.php"); // 読込みと同時にログイン判別
if (!defined("IS_GOD_AUTH")) { echo "権限がありません。"; die; }
if (!IS_GOD_AUTH) { echo "権限がありません。"; die; }

if ($_REQUEST["ta_cmd"]) {
	if (!$_REQUEST["token"] || $_REQUEST["token"]!=c2dbGetSystemConfig("spf.ShellCommanderToken")) {
		echo 'トークン不正です。画面をリロードするか、いったん閉じてやりなおしてください。マルチ画面は利用できません。';
		die;
	}
	exec($_REQUEST["ta_cmd"], $ret);
	echo '<div><b>応答'.count($ret).'行</b></div>';
	echo '<pre style="font-family:\'ＭＳ ゴシック\',monospace">';
	echo hh(implode("\n", $ret));
	echo '</pre>';
	die;
}























$new_token = md5(date("YmdHis"));
c2dbSetSystemConfig("spf.ShellCommanderToken", $new_token);


$cwd = getcwd();

$presets = c2dbGetRows("select * from spfm_command_preset where type = 'shell' order by seq");

//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?></title>
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript">
var presets = <?=c2ToJson($presets)?>;
function resizeResult() {
	var hh = ee("ta_cmd").offsetHeight;
	ee("div_result").style.paddingTop = (hh + 54) + "px";
	ee("div_preset").style.paddingTop = (hh + 54) + "px";
	setTimeout("resizeResult()", 300);
}
function spfExecCommand() {
	if (!confirm("本当に実行してよろしいですか？\n\n実行内容を再確認してください。")) return;
    ee("div_result").innerHTML = "";
    $.ajax({
        url:"shell_commander.php", data:serializeUnicodeEntity("div_send"), success:function(msg) {
        var ret = getCustomTrueAjaxResult(msg, "", 1); if (ret==AJAX_ERROR) return;
        ee("div_result").innerHTML = msg;
        window.scrollTo(0,0);
    }});
}
function setPreset(idx){
	ee("ta_cmd").innerHTML = presets[idx].command;
	ee("ta_cmd").value = presets[idx].command;
	$('#div_preset').toggle();
}
</script>
</head>
<body spellcheck="false" onload="resizeResult()">

<div style="padding:10px; position:fixed; width:100%; background-color:#eeeeee; border-bottom:1px solid #cccccc; z-index:100">
	<div id="div_send">
		<textarea name="ta_cmd" id="ta_cmd" style="width:98%; height:60px; font-size:12px"></textarea>
		<input type="hidden" name="token" value="<?=$new_token?>" />
	</div>
	<div style="padding-top:3px">
		<button type="button" class="w70" onclick="spfExecCommand()">実行</button>
		<span style="padding-left:10px; color:#338833">
			<b>【簡易シェルコマンダー】</b>※実行には十分注意ください。※topは使えませんがcdは使えます。※[Ctrl+Z][Ctrl+Y]が利用できます。
			<button type="button" onclick="$('#div_preset').toggle(); window.scrollTo(0,0);">プリセット</button>
		</span>
	</div>
</div>

<div style="padding:70px 15px 30px 15px; position:absolute" id="div_result"></div>

<div style="padding:70px 15px 10px 15px; display:none; background-color:#dddddd; border-bottom:1px solid #bbbbbb; font-size:12px; position:relative" id="div_preset">
	<div class="gray" style="padding-bottom:5px">現在のディレクトリ：<?=$cwd?></div>
	<? for ($idx=0; $idx<count($presets); $idx++) { ?>
	<a class="block" style="margin-top:3px" href="javascript:void(0)" onclick="setPreset(<?=$idx?>)">◆ <?=hh($presets[$idx]["information"])?></a>
	<? } ?>
</div>

</body>
</html>