<?
//****************************************************************************************************************
// 管理画面のガワ画面
//
// 管理画面呼出しは、すべてAJAXによって内部書き換えを行っている。このため、「戻る」等が存在しない。
// 右側の管理コンテンツは、iframeではない。
// いったん管理画面を開くと、JavaScript変数も、関数も、、管理メニューを変更したところで、えんえん有効なままであり、それは狙いである。
//
// たまに、そんな変数をリセットするために、リロードを発生させる管理機能もある。（「製品管理」機能など）
//****************************************************************************************************************
require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_ADMIN_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にspf権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

$init_action = $_REQUEST["init_action"];
if (!$init_action) $init_action = "mst_menu";
$init_option = $_REQUEST["init_option"];


$hidemenu = ($_REQUEST["hidemenu"] ? "1" : "");
$hideheader = ($_REQUEST["hideheader"] ? "1" : "");


//■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
?>
<? c2env::echoStaticHtmlTag("HTML5_DOCTYPE_HTML"); ?>
<? c2env::echoStaticHtmlTag("HTML5_HEAD_META"); ?>
<title>CoMedix <?=SELF_APPNAME?> - 管理画面</title>
<link rel="stylesheet" type="text/css" href="css/start/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=SCRIPT_VERSION?>">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript" src="js/wdconv.js"></script>
<script type="text/JavaScript">
var SysYmd = <?=c2ToJson(getSysMinMaxYmd())?>;
var currentEmpId = "";
<? writeSyozokuMasterData() ?>

function trySCExit() {
	alert("製品管理モードを終了します。");
    $.ajax({ url:"common.php?ajax_action=reg_sys_config_exit", success:function(msg) {
    	if (msg=="ok") { location.href = 'index_admin.php?init_action=mst_env'; return; }
    	alert(msg);
    }});
}
function resizeIframeEx() {
    var win = window.parent;
    var screenHeight = window.document.body.clientHeight;
    var screenWidth = window.document.body.clientWidth;
    var contentTop = 0;
    var parentPadding = 0;
    if (win.floatingpage) {
        var screenHeight = win.document.body.clientHeight;
        var pBody = win.document.getElementById("contents_body");
        if (!pBody) {
            var pTd = win.document.getElementById("centerblock");
            if (pTd) pBody = pTd.parentNode.parentNode.parentNode;
        }
        contentTop = pBody.offsetTop;
        ee("window_title").style.padding = "";
        ee("window_title").style.backgroundPosition = "0 0";
    } else {
        parentPadding = 5;
    }

    var mainTable = ee("main_table");
    var bottomMargin = 10;
    var ttlHeight = (screenHeight - contentTop - mainTable.offsetTop - bottomMargin - parentPadding);
    var headerHeight = ee("div_section_header_container").offsetHeight + 10 - 20;
    ee("div_admin_menu_wrapper").style.height = ttlHeight + "px";

	<? if ($hideheader) { ?>
	var cont = "";
    if (parent && parent.document) {
		var ifm = parent.document.getElementById("iframe_inner_content");
		var div = parent.document.getElementById("div_content");
		if (ifm && div) {
			cont = ee("div_content");
			if (cont) {
				cont.style.overflow = "auto";
				cont.style.height = "auto";
				cont.style.width = "auto";
				ifm.style.height = Math.max(div.offsetHeight-20, cont.offsetHeight)+"px";
				return;
			}
		}
	}
	<? } ?>
    ee("div_content").style.height = (ttlHeight - headerHeight) + "px";
    var ttlWidth = screenWidth - ee("div_admin_menu_wrapper").offsetWidth - 10;
    ee("div_content").style.width = (ttlWidth) + "px";

    if (typeof adjustInnerHeight == "function") {
        adjustInnerHeight(ttlHeight, headerHeight);
    }
}
function loadInit() {
    var init_action = "<?=$init_action?>";
    if (!ee("menu_<?=$init_action?>")) {
        init_action = "mst_env";
        init_option = "";
    }
    if (ee("menu_"+init_action)) {
        showAdminSect(init_action, "<?=$init_option?>");
    }
    if (parent && parent.hideSideTopMenuDelayed) parent.hideSideTopMenuDelayed();
    gNavi.initialize();
    setTimeout(function(){ resizeIframeEx(); }, 100);
}
</script>
</head>
<body onload="loadInit()" onresize="resizeIframeEx()" spellcheck="false"><div id="CLIENT_FRAME_CONTENT" pageBottomMargin="0" parentBodyOverflowY="hidden" thisBodyOverflowY="hidden" thisBodyOverflowX="hidden" style="overflow:hidden">



<div class="window_title" id="window_title"
	style="background-color:#ffffff; height:32px; padding:5px 0 0 5px; background-position:5px 5px;<?=($hidemenu?'display:none':'')?>">
    <table cellspacing="0" cellpadding="0" style="width:100%"><tr>
        <th style="background:url(img/logo.gif) no-repeat 7px 0; width:220px"><a class="pankuzu" href="index_user.php" title="<?=SELF_APPNAME?>" style="width:210px; display:block; text-decoration:none; height:26px">&nbsp;</a></th>
        <th><a class="pankuzu pankuzu2 block" style="width:120px; height:27px; letter-spacing:3px; text-align:center; color:#dd4444; font-size:18px; font-weight:normal; border:0; padding:2px 0 0 2px" href="index_admin.php">管理画面</a></th>
        <td width="auto"></td>
        <td width="120" class="right"><a href="index_user.php" class="font12 to_user block" style="line-height:16px; padding:5px 8px">→ユーザ画面へ</a></td>
        <? if (c2dbGetSystemConfig("spf.AppCloseBtnEnabled")) { ?>
        <td width="30" class="app_closebtn"><a href="javascript:window.close()" title="スタッフ・ポートフォリオ画面を閉じる"></a></td>
        <? } ?>
    </tr></table>
</div>




<? //============================================================================================ ?>
<? // 左側：メニュー                                                                              ?>
<? //============================================================================================ ?>
<table cellspacing="0" cellpadding="0" style="width:100%" id="main_table"><tr><td style="width:140px;<?=($hidemenu?'display:none':'')?>">
<div id="div_admin_menu_wrapper" style="padding-top:1px">
    <div class="menu_title" style="background:url(img/bk_sect_bar.gif) repeat-x 0 -15px #1f5289; height:28px; color:#ffffff; width:140px; letter-spacing:1px"><div style="padding:3px 0 0 5px">管理項目</div></div>
    <div class="div_admin_menu">
        <?
            $ary = explode(",", c2dbGetSystemConfig("spf.AdminComponents"));
            $components = array("K"=>1);
            foreach ($ary as $c) $components[strtoupper($c)] = 1;
            function _drawAdminLink($components, $k, $js, $id, $jp) {
                if ($components[$k] || IS_SYSTEM_AUTH) {
                    $jp = (IS_SYSTEM_AUTH ? $k.":" : "") . $jp;
                    echo '<a href="javascript:void(0)" onclick="showAdminSect(\''.$js.'\')" id="'.$id.'"><nobr>'.$jp.'</nobr></a>';
                }
                echo "\n";
            }
            _drawAdminLink($components, "A",  "mst_menu",                             'menu_mst_menu',         'メニュー構成');
            _drawAdminLink($components, "B",  "mst_hanyou','&master_id=jobmst_senmon",'menu_mst_hanyou',       '汎用マスタ');
            _drawAdminLink($components, "C",  "mst_field_struct",                     'menu_mst_field_struct', '拡張項目');
            _drawAdminLink($components, "D1", "mst_hyoka1",                           'menu_mst_hyoka1',       'SQE設定');
            _drawAdminLink($components, "D2", "mst_hyoka2",                           'menu_mst_hyoka2',       'SQE個人別設定');
            _drawAdminLink($components, "E",  "mst_list_struct','&luid=01",           'menu_mst_list_struct',  'リスト構成');
            _drawAdminLink($components, "F",  "mst_sect_layout','&muid=201",          'menu_mst_sect_layout',  'セクション構成');
            _drawAdminLink($components, "G",  "mst_finder",                           'menu_mst_finder',       '検索構成');
            _drawAdminLink($components, "H",  "mst_approval', '&wf_type=KAKUNIN",     'menu_mst_approval',     '承認フロー構成');
            _drawAdminLink($components, "I",  "mst_message",                          'menu_mst_message',      'メッセージ通知');
            _drawAdminLink($components, "J",  "mst_import",                           'menu_mst_import',       'データインポート');
            _drawAdminLink($components, "K",  "mst_env",                              'menu_mst_env',          '製品管理');
            _drawAdminLink($components, "L",  "mst_sisetu",                           'menu_mst_sisetu',       '施設基準管理');
          //_drawAdminLink($components, "M",  "mst_list_def",                         'menu_mst_list_def',     'リスト項目初期設定');
            _drawAdminLink($components, "N",  "mst_kantoku",                          'menu_mst_kantoku',      '監督者設定');
        ?>
    </div>
    <? if (IS_SYSTEM_AUTH) {?>
    	<div style="padding-top:10px" class="font12"><a href="javascript:void(0)" onclick="trySCExit()">◆製品管理モード終了</a></div>
    <? } ?>
</div>
</td>




<? //============================================================================================ ?>
<? // 右側：メインコンテンツ                                                                      ?>
<? //============================================================================================ ?>
<td id="right_area" style="padding-left:5px">
    <div id="div_section_header_container" style="border-top:1px solid #dddddd;<?=($hideheader?'display:none':'')?>"></div>
    <div id="div_content" style="padding:0 5px 0 0; overflow:scroll; height:100px"></div>
</td></tr></table>
<? // 右側おわり ?>





</div><!-- //CLIENT_FRAME_CONTENT -->

<div style="position:absolute"><div id="dialog_workspace"></div></div>

<div id="calendar_dialog_container" style="display:none"><div id="calendar_dialog_content" style="padding:5px 0 0 0"></div></div>

</body>
</html>