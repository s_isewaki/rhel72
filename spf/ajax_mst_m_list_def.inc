<?


//****************************************************************************************************************
// リスト項目初期設定マスタ
//****************************************************************************************************************
function html_mst_list_def() {
    global $LIST_FIELD_DEF_SYS_FIELDS;
    $text_fields = array("varchar", "text", "bpchar");

    $ddl_table_id = $_REQUEST["ddl_table_id"];
    $txt_table_id = $_REQUEST["txt_table_id"];

    $table_id = $ddl_table_id;
    if ($txt_table_id) $table_id = $txt_table_id;
    $table_id_disp = ($table_id ? $table_id : "未指定");

    $table_list = array();
    $rows = c2dbGetRows("select table_id from spfm_list_field_def order by table_id");
    foreach ($rows as $row) {
        $table_list[$row["table_id"]] = 1;
    }

    $scheme = c2dbGetTableScheme($table_id);
    ksort($scheme);
    $table_error = "";
    if ($table_id && !count($scheme)) $table_error = "指定したテーブルIDは存在しません。";

    $sql =
    " select lfd.*, ls.tfid from spfm_list_field_def lfd".
    " left outer join (".
    "     select table_id, field_id, table_id || field_id as tfid from spfm_list_struct".
    "     group by table_id, field_id".
    " ) ls on ( ls.table_id = lfd.table_id and ls.field_id = lfd.field_id )".
    " where lfd.table_id = ".c2dbStr($table_id).
    " order by lfd.field_id";
    $rows = c2dbGetRows($sql);
    $listFieldDef = array();
    foreach ($rows as $row) {
        $listFieldDef[$row["table_id"]][$row["field_id"]] = $row;
    }
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
        <table cellspacing="0" cellpadding="0" class="section_title"><tr>
            <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">M</th><? } ?>
            <th class="section_title_title"><nobr>リスト項目初期設定</nobr></th>
            <th class="font13" style="letter-spacing:0">
                登録済みテーブルID
                <select name="ddl_table_id" onchange="showAdminSect('mst_list_def','', 'div_section_header')">
                    <option value=""></option>
                    <? foreach ($table_list as $tid => $dummy) { ?>
                    <? if ($tid=="other") continue; ?>
                    <option value="<?=$tid?>"<?=$ddl_table_id==$tid?" selected":""?>><?=$tid?></option>
                    <? } ?>
                </select>
                または未登録のテーブル
                <input type="text" class="text imeoff" name="txt_table_id" value="<?=$txt_table_id?>"
                    onchange="showAdminSect('mst_list_def','', 'div_section_header')"
                    onkeydown="if (isEnterKey(this,event)) showAdminSect('mst_list_def','', 'div_section_header')" />
            </th>
            <th class="button"><nobr><button type="button" onclick="tryAdminReg('right_area', 'reg_mst_list_def')">保存</button></nobr></th>
        </tr></table>
    </div>


    <? if (!$table_id) { ?>
        <div style="padding:5px 0" class="gray">
            ◆「E:リスト項目」で利用するためのテーブル＋フィールドをシステムに組み込みます。ユーザ画面上は「リスト」として表示や登録が行われるものです。<br>
            ◆タイトル部でテーブルIDを指定すると、ここに実際のテーブル情報が表示されます。「組込み」チェックを行って、
                システムに組み込むフィールドを指定してください。<br>
            ◆テーブルやフィールドの組み込みは無作為に行うことができません。データ構造への理解が必要です。<br>
            ◆初期入力タイプは「E:リスト項目」の「タイプ」列の選択可能範囲を制限します。その他の入力項目は、「E:リスト項目」で上書再設定が可能です。<br>
            ◆組み込み済フィールドを「E:リスト項目」で利用すると「<span style="color:#aa0000">構成済</span>」となり、削除できません。<br>
            ◆システムが内部で必須とするフィールドは「<span style="color:#aa0000">ｼｽﾃﾑ管理</span>」となり、削除できません。
        </div>
    <? } ?>


    <? if ($table_id) { ?>
        <div>
            テーブルID： <b><?=$table_id_disp?></b>
        </div>
    <? } ?>


    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div class="smallsize">
    <span style="color:red"><?=$table_error?></span>
    <input type="hidden" name="table_id" value="<?=$table_id?>" />

    <? if ($table_id && !$table_error) { ?>
        <table cellspacing="0" cellpadding="0" class="list_table">
        <tr class="pale">
            <th><nobr>組込み</nobr></th>
            <th>フィールドID</th>
            <th>ＤＢタイプ</th>
            <th>初期入力タイプ</th>
            <th>初期ラベル</th>
            <th>初期入力幅</th>
            <th><nobr>初期<br>日本語IME</nobr></th>
        </tr>
        <tbody id="mst_list_struct_sortable" class="hover_colord">
        <?
            foreach ($scheme as $field_id => $field_type) {
                $row = $listFieldDef[$table_id][$field_id];
                $is_active = 1;
                if (!$row) { $is_active = 0; $row = array("field_id"=>$field_id); }
                $is_sys_field = (in_array($table_id."@".$field_id ,$LIST_FIELD_DEF_SYS_FIELDS) ? 1 : 0);
        ?>
        <tr <?=($is_active?"":' class="not_active_row"')?>>
            <? if ($row["tfid"] || $is_sys_field){ ?>
                <td class="center" style="color:#aa0000">
                    <? if ($row["tfid"]){ ?><div>構成済</div><? } ?>
                    <? if ($is_sys_field){ ?><div>ｼｽﾃﾑ管理</div><? } ?>
                    <input type="hidden" value="1" name="<?=$field_id?>@is_active" />
                </td>
            <? } else { ?>
                <td><input type="checkbox" value="1" name="<?=$field_id?>@is_active"<?=($is_active?" checked":"")?> /></td>
            <? } ?>
            <td><?=$field_id?></td>
            <td><?=$field_type?></td>

            <td>
                <select name="<?=$field_id?>@field_type">
                    <? $fsel = array($row["field_type"]=>" selected"); ?>
                    <option value="text"<?=$fsel["text"]?>>テキスト</option>
                    <option value="textarea"<?=$fsel["textarea"]?>>複数行テキスト</option>
                    <option value="pulldown"<?=$fsel["pulldown"]?>>プルダウン</option>
                    <option value="yymm"<?=$fsel["yymm"]?>>年月</option>
                    <option value="ymd"<?=$fsel["ymd"]?>>年月日</option>
                    <option value="ymd"<?=$fsel["ymdhm"]?>>年月日時分</option>
                    <option value="image"<?=$fsel["image"]?>>画像</option>
                    <option value="label"<?=$fsel["label"]?>>表示のみ</option>
                </select>
            </td>
            <td><input type="text" class="text" name="<?=$field_id?>@field_label" value="<?=hh($row["field_label"])?>" /></td>
            <td><input type="text" class="text imeoff w40" name="<?=$field_id?>@field_width" value="<?=hh($row["field_width"])?>" /></td>
            <td>
                <? if (!in_array($field_type, $text_fields)) { ?>
                    <input type="hidden" name="<?=$field_id?>@ime_off" value="1" />
                    オフ
                <? } else { ?>
                    <select name="<?=$field_id?>@ime_off">
                        <option value=""></option>
                        <option value="1"<?=($row["ime_off"]?" selected":"")?>>オフ</option>
                    </select>
                <? } ?>
            </td>
        </tr>
        <? } ?>
        </tbody>
        </table>
        <div class="gray" style="padding-top:4px">※ 「組込み」非チェックで保存すると、行のすべての設定値がクリア／リセットされます。</div>
    <? } ?>

    </div>
<?
}
//================================================================================================================
// リスト項目マスタの保存
//================================================================================================================
function reg_mst_list_def() {
    $table_id = $_REQUEST["table_id"];
    $ddl_table_id = $_REQUEST["ddl_table_id"];
    $txt_table_id = $_REQUEST["txt_table_id"];
    if (!$table_id) { echo "不正なアクセスです。(table_id)"; die; }
    $scheme = c2dbGetTableScheme($table_id);
    if (!count($scheme)) { echo "不明なテーブルIDです。(".$table_id.")"; die; }

    ksort($scheme);
    c2dbBeginTrans();

    c2dbExec("delete from spfm_list_field_def where table_id = ".c2dbStr($table_id));

    foreach ($scheme as $field_id => $field_type) {
        if (!$_REQUEST[$field_id."@is_active"]) continue;
        $sql =
        " insert into spfm_list_field_def (".
        "     table_id, field_id, ime_off, field_width, field_label, field_type".
        ") values (".
        "     ".c2dbStr($table_id).
        "    ,".c2dbStr($field_id).
        "    ,".c2dbStr($_REQUEST[$field_id."@ime_off"]).
        "    ,".c2dbStr((int)$_REQUEST[$field_id."@field_width"]).
        "    ,".c2dbStr($_REQUEST[$field_id."@field_label"]).
        "    ,".c2dbStr($_REQUEST[$field_id."@field_type"]).
        ")";
        c2dbExec($sql);
    }
    c2dbCommit();
    echo "ok".'{forwardSection:"mst_list_def", forwardOption:"&ddl_table_id='.$ddl_table_id.'&txt_table_id='.$txt_table_id.'"}';
    die;
}
