<?

function _write_kantoku_row($joint_id, $kantoku_data, $caption, $indent) {
	$emp_ids = $kantoku_data["emp_ids"];
	$emp_ids = implode("\t", (array)$emp_ids);
	echo  '<tr class="relative" id="tr_'.$joint_id.'"><td class="w300"><div><nobr>';
	echo '<span style="letter-spacing:8px" class="silver">'.$indent.'</span>';
	echo '<span id="span_'.$joint_id.'" style="border:2px solid #ffffff" class="branch" joint_id="'.$joint_id.'">';
	echo hh($caption).'</span></nobr></div></td>'."\n";
	echo '<td class="w40">'."\n";
	echo '<button type="button" onclick="showEmpSelector(\''.$joint_id.'\')">設定</button>'."\n";
	echo '</td>'."\n";
	echo '<td id="td_'.$joint_id.'" style="line-height:22px">'."\n";
	echo _make_emp_td_inner($joint_id, $emp_ids, $kantoku_data["disp_html"]);
	echo '</td></tr>'."\n";
}
function _make_emp_td_inner($joint_id, $emp_ids_tsv, $disp_html) {
	if (is_array($disp_html)) {
		$disp_html =
		'<span style="padding:0 2px">'.
		'<span style="padding:0 4px; border:2px solid #ffffff" class="supervisor" joint_id="'.$joint_id.'"><nobr>'.
		implode('</nobr></span></span><span style="padding:0 2px"><span style="padding:0 4px; border:2px solid #ffffff" class="supervisor" joint_id="'.$joint_id.'"><nobr>', $disp_html). '</nobr></span></span>';
	}
	return "".
	'<input type="hidden" id="hdn_'.$joint_id.'" value="'.$emp_ids_tsv.'" />'."\n".
	'<div id="div_'.$joint_id.'">'.$disp_html.'</div>';
}
function get_kantoku_caption($kantoku_id=0) {
	$ret = array();
    $sql =
    " select jo.joint_id, jo.emp_id_or_syozoku, em.emp_lt_nm || ' ' || em.emp_ft_nm as emp_nm from spfm_kantoku jo".
    " left outer join empmst em on (em.emp_id = jo.emp_id_or_syozoku )".
    " ".($kantoku_id ? " and jo.joint_id =  ".c2dbStr($kantoku_id) : "").
    " order by em.emp_lt_nm, em.emp_ft_nm, jo.joint_id, jo.emp_id_or_syozoku";
	$rows = c2dbGetRows($sql);
	foreach ($rows as $row) {
		$joint_id = $row["joint_id"];
        $emp_nm = hh($row["emp_nm"]);
        if ($row["emp_id_or_syozoku"]=="#self") $emp_nm = '<span class="id_syozoku">本人</span>';
        else if (substr($row["emp_id_or_syozoku"],0,9)=="#self_st#") $emp_nm = '<span class="id_syozoku">'.hh(getAuthYakusyokuNm($row["emp_id_or_syozoku"]))."</span>";
        else if (!$emp_nm) $emp_nm = '<span class="id_syozoku">'.hh(getSyozokuYakusyokuNm($row["emp_id_or_syozoku"]))."</span>";
        $ret[$joint_id]["disp_html"][]= $emp_nm;

        $code = $row["emp_id_or_syozoku"];
        $appr_emp_hyoka_rows[$row["appr_hier"]][$row2["emp_id"]] = array(
            "syozoku"=>$code,"syozoku_nm"=>getSyozokuYakusyokuNm($row["emp_id_or_syozoku"]),"subhier"=>$row["appr_subhier"], "emp_nm"=>$row2["emp_nm"]
        );
		$ret[$joint_id]["emp_ids"][]= hh($row["emp_id_or_syozoku"]);
    }
    return $ret;
}
//****************************************************************************************************************
// 監督者設定
//****************************************************************************************************************
function html_mst_kantoku() {
    $menuRows = c2dbGetRows("select * from spfm_menu order by menu_order");
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        var hilightFocusClosure = function(focusElem) {
			var jointId = focusElem.getAttribute("joint_id");
			var tr = ee("tr_"+jointId);
			return tr.offsetTop;
		};
        var hilighterB = new Hilighter($("#kantoku_main_table span.branch"),     "div_result_branch",     "div_content", hilightFocusClosure, 0);
        var hilighterS = new Hilighter($("#kantoku_main_table span.supervisor"), "div_result_supervisor", "div_content", hilightFocusClosure, 0);

        var currentJointId = "";
        function showEmpSelector(kantokuId) {
            currentJointId = kantokuId;
            popupAdmDlg('職員選択','dlg_emp_selector', "&use_buttons=branch&callback_func=mstKantokuEmpSelectorCallback&emp_ids="+encodeURIComponent(ee("hdn_"+kantokuId).value));
        }
        function mstKantokuEmpSelectorCallback(empList) {
            if (!confirm("保存します。よろしいですか？")) return;
            var empIds = [];
            for (var idx=0; idx<empList.length; idx++) {
                empIds.push(empList[idx].emp_id);
            }
            ee("hdn_emp_ids").value = empIds.join("\t");
            ee("hdn_joint_id").value = currentJointId;
            var postData = serializeUnicodeEntity("kantoku_reg_area");
            closeDialog("all");
	        $.ajax({ url:"common.php?ajax_action=reg_mst_kantoku", data:postData, success:function(msg) {
	            var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
	            ee("td_"+currentJointId).innerHTML = msg;
	        }});
        }
    </textarea>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
    <table cellspacing="0" cellpadding="0" class="section_title"><tr>
        <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">N</th><? } ?>
        <th class="section_title_title" style="width:200px"><nobr>監督者設定</nobr></th>
        <td style="width:auto"></td>
    </tr></table>
    <div style="padding:0 5px 5px 0">
	    <div class="finder_box">
	    	<div>
	    		検索キー(所属部署)
	    		<input type="text" class="text w200"
	    		navi="検索文字を入力し、エンターキーを押すと、ハイライト表示します。エンターキーを押すたびに該当行へスクロールします。"
	    		onchange="hilighterB.startHilight(this.value);"
	    		onkeydown="if(isEnterKey(this,event)) hilighterB.startHilight(this.value);"
	    		/>
	    	</div>
	    	<div id="div_result_branch" style="height:24px; line-height:22px; width:100px"></div>
	    	<div>
	    		検索キー(監督者)
	    		<input type="text" class="text w200"
	    		navi="検索文字を入力し、エンターキーを押すと、ハイライト表示します。エンターキーを押すたびに該当行へスクロールします。"
	    		onchange="hilighterS.startHilight(this.value);"
	    		onkeydown="if(isEnterKey(this,event)) hilighterS.startHilight(this.value);"
	    		/>
	    	</div>
	    	<div id="div_result_supervisor" style="height:24px; line-height:22px"></div>
		    <div class="clearfix"></div>
	    </div>
    </div>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="mst_menu_container">
	<?
		$classRows = getMasterHash("classmst", 0);
		$atrbRows = getMasterHash("atrbmst", 0);
		$deptRows = getMasterHash("deptmst", 0);
		$roomRows = getMasterHash("classroom", 0);

		$kantoku = get_kantoku_caption(0);
	?>
	<div id="kantoku_reg_area">
		<input type="hidden" id="hdn_emp_ids" name="hdn_emp_ids" />
		<input type="hidden" id="hdn_joint_id" name="hdn_joint_id" />
	</div>
    <table cellspacing="0" cellpadding="0" class="list_table relative width100" id="kantoku_main_table">
    <tr class="relative">
    	<th>所属部署</th>
    	<th></th>
    	<th>監督者</th>
    </tr>
	<?
		foreach ($classRows as $classRow) {
			$_class_id = $classRow["class_id"];
			if (!$_class_id) continue;
			$joint_id1 = (int)$_class_id;
			_write_kantoku_row($joint_id1, $kantoku[$joint_id1], $classRow["class_nm"], "");
			foreach ($atrbRows as $atrbRow) {
				if ($atrbRow["class_id"]!=$_class_id) continue;
				$_atrb_id = (int)$atrbRow["atrb_id"];
				$joint_id2 = $joint_id1."-".$_atrb_id;
				_write_kantoku_row($joint_id2, $kantoku[$joint_id2], $atrbRow["atrb_nm"], "＞");
				foreach ($deptRows as $deptRow) {
					if ($deptRow["atrb_id"]!=$_atrb_id) continue;
					$_dept_id = (int)$deptRow["dept_id"];
					$joint_id3 = $joint_id2."-".$_dept_id;
					_write_kantoku_row($joint_id3, $kantoku[$joint_id3], $deptRow["dept_nm"], "＞＞");
					foreach ($roomRows as $roomRow) {
						if ($roomRow["dept_id"]!=$_dept_id) continue;
						$_room_id = (int)$roomRow["room_id"];
						$joint_id4 = $joint_id3."-".$_room_id;
						_write_kantoku_row($joint_id4, $kantoku[$joint_id4], $roomRow["dept_nm"], "＞＞＞");
					}
				}
			}
		}
   	?>
    </table>
    </div>
<?
}

//================================================================================================================
// 監督者設定の保存
//================================================================================================================
function reg_mst_kantoku() {
	$joint_id = $_REQUEST["hdn_joint_id"];
    $emp_ids = $_REQUEST["hdn_emp_ids"];
    $emp_ids_ary = explode("\t", $emp_ids);
    if (!$joint_id) { echo "ng呼出しが不正です。(joint_id)"; die; }

    c2dbBeginTrans();
	c2dbExec("delete from spfm_kantoku where joint_id = ".c2dbStr($joint_id));
	foreach ($emp_ids_ary as $emp_id) {
		if (!$emp_id) continue;
		$to_last_cadr_id = "0";
		$to_last_cadr_hier = "";
		$to_st_id = "0";
		$ary = explode("#", $emp_id);
		if (count($ary)>1) {
			if ($ary[1]!="self" && $ary[1]!="self_st") {
				$to_st_id = (int)$ary[2];
				$cadr = explode("-", $ary[1]);
				$cadr_list = array("", "K_CLASS", "K_ATRB", "K_DEPT", "K_ROOM");
				$to_last_cadr_hier = $cadr_list[count($cadr)];
				$to_last_cadr_id = (int)$cadr[count($cadr)-1];
			}
		}
		$sql =
		" insert into spfm_kantoku ( joint_id, emp_id_or_syozoku, to_last_cadr_hier, to_last_cadr_id, to_st_id ) values ("
		.c2dbStr($joint_id).",".c2dbStr($emp_id).",'".$to_last_cadr_hier."', ".$to_last_cadr_id.", ".$to_st_id.")";
		c2dbExec($sql);
	}
    c2dbCommit();


    $kantoku = get_kantoku_caption($joint_id);
    echo _make_emp_td_inner($joint_id, implode("\t", (array)$kantoku[$joint_id]["emp_ids"]), $kantoku[$joint_id]["disp_html"]);
    die;
}

