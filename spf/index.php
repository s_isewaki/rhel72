<?
//****************************************************************************************************************
// SPFエントリURL
// CoMedixメニューから呼ばれる。
// オンロードで別ウィンドウを開く。
// 実際のSPF画面は、index_user.php または、index_admin.phpである。
// この画面はただのキッカーである。
//
// 「proxy_action」は、spf自体が利用するパラメータである。利用していないように見えても削除してはいけない。
// 「proxy_action」は、管理側「承認フロー設定」「SQE設定」あたりで利用している。
//  iframeの「無限ネスト」っぽく見える書式において、ブラウザの過剰反応を防ぐためのものである。
//  例えば、common.phpに、「<iframe src="common.php?page=1"></iframe>」と記述されていれば、無限ネストである。
//  これをブラウザが検知し、未然にページ表示をアボートする。
//  ただ、「pageパラメータが2なら、<iframe>は生成されない」なら、無限ネストに見えても、実はそうでない。
//  このため、common.phpを呼ばずに、「proxy_action」としてページを呼んで、ブラウザを騙しているのである。
//****************************************************************************************************************
if ($_REQUEST["proxy_action"]) {
	$pa = $_REQUEST["proxy_action"];
	unset($_REQUEST["proxy_action"]);
	require_once($pa);
	die;
}

require_once("common.php"); // 読込みと同時にログイン判別
if(!$EAL[SELF_USER_AUTH_FLG]) c2ShowAlertAndGotoLoginPageAndDie("操作権限がありません。"); // ﾛｸﾞｲﾝ者にspf権限が無ければﾛｸﾞｲﾝﾍﾟｰｼﾞへ強制送還するJS発行

?>
<title>CoMedix | スタッフ・ポートフォリオ</title>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/common.js?v=<?=SCRIPT_VERSION?>"></script>
<script type="text/javascript">
function openStaffPortfolio() {
	var size = getWindowAvailSize();
    var param = ',left=0, top=0, location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes';
    window.open('index_user.php', '', 'width='+size.width+',height='+size.height+param);
}
</script>
</head>
<body onload="openStaffPortfolio()" spellcheck="false">
    <button type="button" onclick="openStaffPortfolio();" style="margin-top: 30px;">スタッフ・ポートフォリオを開く</button>
</body>
</html>
