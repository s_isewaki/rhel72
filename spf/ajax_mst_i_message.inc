<?

//****************************************************************************************************************
// 選択肢マスタ
//****************************************************************************************************************
function html_mst_message() {
    $sql_crlf = $_REQUEST["sql_crlf"];
    $sql_crlf_opt = ($sql_crlf ? "&sql_crlf=1":"");

    $listLineupRows = c2dbGetRows("select * from spfm_list_lineup where is_active = '1' order by luid");
    $menuRows = c2dbGetRows("select * from spfm_menu where is_active = '1' and menu_hier in ('chu','syo') order by menu_order");
    $menuNames = array();
    foreach ($menuRows as $row) {
        $menuNames[$row["muid"]] = $row["menu_label"];
    }

    $fieldStructRows = c2dbGetRows("select * from spfm_field_struct where table_id <> 'option' order by table_id, field_id");

    $_menuRows = c2dbGetRows("select * from spfm_menu");
    $menuRows = array();
    foreach ($_menuRows as $row) {
        $menuRows[$row["muid"]] = $row["menu_label"];
    }

    $messageRows = c2dbGetRows("select * from spfm_messages order by sort_order, message_id desc");

    $finderRows = c2dbGetRows("select * from spfm_finder where finder_title is not null and finder_title <> '' order by finder_title");
    $finderTitles = array();
    foreach ($finderRows as $row) {
        $finderTitles[$row["finder_id"]] = $row["finder_title"];
    }
?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // セクションヘッダ。コンテンツロード直後にこの位置から消滅⇒非スクロール領域へ移動        ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div id="div_section_header">
        <table cellspacing="0" cellpadding="0" class="section_title"><tr>
            <? if (IS_SYSTEM_AUTH) { ?><th class="section_title_abc">I</th><? } ?>
            <th class="section_title_title"><nobr>メッセージ通知</nobr></th>
        </tr></table>
    </div>

    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // JavaScript。コンテンツ領域ロード「直後」に「実行」される。                              ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <textarea class="script">
        var currentMessageId = "";
        var defaultIsKojin = "";
        var defaultPatternCount = "";
        var defaultSql = "";
        function popupMessageEditor(message_id) {
            currentMessageId = message_id;
            var dialogHtml = ee("message_editor_container").innerHTML;

            $dialogs["message_dialog"] = $("#message_editor").dialog({
                title:"メッセージ通知設定", width:980, height:630, modal:true,
                close:function(){
                    closeDialog("all");
                    ee("message_editor_container").innerHTML = dialogHtml;
                }
            });
            ee("message_id").value = message_id;
            ee("message_title").value = ee("div_message_title"+message_id).innerHTML;
            ee("message_id_disp").innerHTML = message_id;
            setComboValue(ee("is_active"), ee("is_active"+message_id).value);
            setComboValue(ee("disp_place"), ee("disp_place"+message_id).value);
            setComboValue(ee("is_kojin"), ee("is_kojin"+message_id).value);
            ee("sort_order").value = ee("sort_order"+message_id).value;
            setComboValue(ee("pattern_count"), ee("pattern_count"+message_id).value);

            var html = ee("sql_formula"+message_id).innerHTML;
            if (html == "（なし）") html = "";
            ee("sql_formula_wrapper").innerHTML =
                '&lt;textarea name="sql_formula" id="sql_formula" cols="50" rows="8" style="width:800px; height:160px" onkeyup="setSqlCheckVihavior();">' + html + '&lt;/textarea>';
            defaultSql = ee("sql_formula").value;
            defaultIsKojin = ee("is_kojin"+message_id).value;
            defaultSortOrder = ee("sort_order"+message_id).value;
            defaultPatternCount = ee("pattern_count"+message_id).value;
            tryTest(1);
        }
        function setSqlCheckVihavior(){
            var isChanged = (defaultSql != ee("sql_formula").value);
            ee("sql_formula").style.border = (isChanged ? "1px solid #ff0000" : ""); // 未チェックだったら色をつける
            if (defaultIsKojin != getComboValue(ee("is_kojin"))) isChanged = 1;
            if (defaultPatternCount != getComboValue(ee("pattern_count"))) isChanged = 1;
            ee("span_test_msg").style.display = (isChanged ? "" : "none");
            if (ee("btn_save")) ee("btn_save").disabled = (isChanged ? true : false);
        }
        function tryTest(is_init) {
            ee("response_patterns").style.visibility = "hidden";
            var postData = serializeUnicodeEntity("message_editor");
            var url = "common.php?ajax_action=html_mst_message_test<?=$sql_crlf_opt?>"+(is_init ? "&is_init=1":"");
            $.ajax({ url:url, data:postData, success:function(msg) {
                var check = getCustomTrueAjaxResult(msg, "", 1); if (check==AJAX_ERROR) return;
                ee("response_patterns").style.visibility = "visible";
                ee("response_patterns").innerHTML = msg;
                activateJavascript("div_response_list");
                textExecuted();
                setSqlCheckVihavior();
            }});
        }
    </textarea>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <? // コンテンツ領域                                                                          ?>
    <? //―――――――――――――――――――――――――――――――――――――――――――― ?>
    <div style="padding-bottom:2px">
        <button type="button" onclick="alert('空欄レコードが追加登録されます。');tryAdminReg('', 'reg_mst_message_addrow','<?=$sql_crlf_opt?>',1,1)" class="w70">追加</button>
        <span style="padding-left:50px">
            <label><input type="checkbox" name="sql_crlf" onclick="showAdminSect('mst_message', '&sql_crlf='+(this.checked?1:''))" <?=($sql_crlf?" checked":"")?> />SQL改行つき</label>
        </span>
    </div>
    <table cellspacing="0" cellpadding="0" class="list_table">
    <tr class="pale">
        <th><nobr>No.</nobr></th>
        <th><nobr>表示<br>順</nobr></th>
        <th><nobr>保留</nobr></th>
        <th><nobr>適用</nobr></th>
        <th><nobr>表示位置</nobr></th>
        <th><nobr>タイトル／判定SQL書式／応答メッセージHTML</nobr></th>
        <th><nobr>応答パターン数</nobr></th>
        <th><nobr>削除</nobr></th>
    </tr>
    <?
        foreach ($messageRows as $row)  {
    ?>
        <tr>
            <td class="center"><?=$row["message_id"]?></td>
            <td class="center"><?=$row["sort_order"]?></td>
            <? if ($row["is_active"]) { ?>
	            <td></td>
            <? } else { ?>
	            <td class="center" style="background-color:#dd4444; color:#ffffff"><nobr>保留</nobr></td>
            <? } ?>
            <td class="center"><nobr><?=($row["is_kojin"]?"個人":"全体")?></nobr></td>
            <td class="center"><nobr>
            <?
                $dp = strtoupper($row["disp_place"]);
                if ($dp=="DASHBD@SOGO") echo "ダッシュボード<br>総合状況";
                else if ($dp=="DASHBD@SEND") echo "ダッシュボード<br>申請状況";
                else if ($dp=="DASHBD@RECV") echo "ダッシュボード<br>受付状況";
                else if ($dp=="MENU@DASHBOARD") echo "ダッシュボード";
                else if ($dp=="MENU@SENDSTAT") echo "申請一覧";
                else if ($dp=="MENU@RECVSTAT") echo "受付一覧";
                else if (substr($dp,0,11)=="MENU@FINDER") echo "[検索]".$finderTitles[substr($dp,11)];
                else if (substr($dp,0,5)=="MENU@") echo "".$menuNames[substr($dp,5)];
            ?>
            </td>
            <td>
                <a class="block" href="javascript:void(0)" onclick="popupMessageEditor(<?=$row["message_id"]?>)">
                <div style="color:#d6309f">■<span id="div_message_title<?=$row["message_id"]?>"><?=($row["message_title"]==""?"（タイトルなし）" :hh($row["message_title"]))?></span></div>
                <? if ($sql_crlf) {?>
                <pre style="display:inline"><?=($row["sql_formula"] ? hh($row["sql_formula"]):"（なし）")?></pre>
                <? } else { ?>
                <?=($row["sql_formula"] ? hh(preg_replace("/\s+/","\n",$row["sql_formula"])):"（なし）")?>
                <? } ?>
                <span style="display:none" id="sql_formula<?=$row["message_id"]?>"><?=$row["sql_formula"]?></span>
                <input type="hidden" id="disp_place<?=$row["message_id"]?>" value="<?=$row["disp_place"]?>" />
                <input type="hidden" id="is_active<?=$row["message_id"]?>" value="<?=$row["is_active"]?>" />
                <input type="hidden" id="is_kojin<?=$row["message_id"]?>" value="<?=$row["is_kojin"]?>" />
                <input type="hidden" id="sort_order<?=$row["message_id"]?>" value="<?=$row["sort_order"]?>" />
                <input type="hidden" id="table_id<?=$row["message_id"]?>" value="<?=$row["table_id"]?>" />
                <input type="hidden" id="field_id<?=$row["message_id"]?>" value="<?=$row["field_id"]?>" />
                <input type="hidden" id="pattern_count<?=$row["message_id"]?>" value="<?=$row["pattern_count"]?>" />
            </td>
            <td><?=$row["pattern_count"]?></td>
            <td><button type="button" onclick="if(confirm('削除します。よろしいですか？'))tryAdminReg('', 'reg_mst_message_delrow', '&message_id=<?=$row["message_id"].$sql_crlf_opt?>',1,1);">削除</button></td>
        </tr>
    <? } ?>
    </table>








    <div id="message_editor_container" style="display:none">
        <div id="message_editor">
            <div style="padding:5px 0">
                <input type="hidden" name="message_id" id="message_id" />
                <div style="padding-bottom:3px" class="font12">
                <nobr>
                    <b>No.<span id="message_id_disp"></span></b>
                    <span style="padding-left:10px" navititle="保留設定" navi="この通知メッセージ設定を実際にユーザ画面で稼働させるには、「利用」にしてください。">
                        <select name="is_active" id="is_active">
                            <option value="">保留</option>
                            <option value="1">利用</option>
                        </select>
                    </span>
                    <span style="padding-left:10px" navititle="並び順" navi="">
                        並び順
                        <input type="text" name="sort_order" id="sort_order" class="text w50" maxlength="50" />
                    </span>
                    <span style="padding-left:10px" navititle="応答パターン数" navi="画面下部「応答」の、入力できる数が変化します。<br>判定SQLの結果次第で通知メッセージを変更したい場合に、<br>その通知メッセージのパターン数を指定してください。<br>変更した後は、応答パターンの再構成が必要です。SQLテストを再実行してください。">
                        応答パターン数
                        <select name="pattern_count" id="pattern_count" onchange="setSqlCheckVihavior()">
                            <?for ($idx=1; $idx<=30; $idx++) { ?>
                                <option value="<?=$idx?>"><?=$idx?>パターン</option>
                            <? } ?>
                        </select>
                    </span>
                    <span style="padding-left:10px" navititle="全体/個人向け"
                        navi="クリック遷移先を制限します。<br>全体向けメッセージを作成する場合は、個人のプロファイル関連のセクションには遷移を誘導できません。<br>変更した後は、応答パターンの再構成が必要です。SQLテストを再実行してください。">
                        <select name="is_kojin" id="is_kojin" onchange="setSqlCheckVihavior()">
                            <option value="">全体向け</option>
                            <option value="1">個人向け</option>
                        </select>
                    </span>
                    <span style="padding-left:10px" navititle="実行場所/表示位置" navi="この通知メッセージは、指定した場所を開くときに検索実行されます。<br>その結果、通知メッセージ内容があれば、指定場所のタイトル直下に表示されます。">
                        実行場所/表示位置
                        <select name="disp_place" id="disp_place" style="width:330px">
                            <option value="DASHBD@SOGO">ダッシュボード総合状況（確認者・評価者向け設定）</option>
                            <option value="DASHBD@RECV">ダッシュボード受付状況（確認者・評価者向け設定）</option>
                            <option value="DASHBD@SEND">ダッシュボード申請状況（ログイン職員個人向け設定）</option>
                            <option value="MENU@DASHBOARD">ダッシュボード</option>
                            <option value="MENU@SENDSTAT">申請一覧</option>
                            <option value="MENU@RECVSTAT">受付一覧</option>
			                <? foreach ($finderRows as $row) { ?>
			                   <option value="MENU@FINDER<?=$row["finder_id"]?>">[検索]<?=$row["finder_title"]?></option>
			                <? } ?>
                            <? foreach ($menuNames as $_muid => $_menu_label) { ?>
                                <option value="MENU@<?=$_muid?>"><?=$_menu_label?></option>
                            <? } ?>
                        </select>
                    </span>
                </nobr>
                </div>


                <table cellspacing="0" cellpadding="0" style="width:880px">
                    <tr navititle="タイトル" navi="このメッセージ登録をわかりやすくする為につけてください。ユーザ画面への表示はされません。"><th style="width:70px"><nobr>タイトル</nobr></th><td>
                        <input type="text" class="text w500" name="message_title" id="message_title" />
                    </td></tr>
                    <tr><th style="width:70px"><nobr>判定SQL</nobr></th><td id="sql_formula_wrapper" navititle="判定SQL" navi="「select」で開始しなければ、登録できません。">
                    </td></tr>
                </table>

            </div>
            <div class="center">
                <span id="span_test_msg" style="display:none" class="red">SQLのテストを行ってください。</span>
                <button type="button" onclick="tryTest()">SQLのテスト</button>
            </div>

            <div class="center" style="padding-top:10px;" id="response_patterns">
                <div style="width:880px; height:200px; border:1px solid #bbbbbb; background-color:#dddddd"></div>
            </div>

        </div>
    </div>
<?
}
//================================================================================================================
//
//================================================================================================================
function reg_mst_message_addrow() {
    $opt = ($_REQUEST["sql_crlf"] ? "&sql_crlf=1" :"");
    $max_id = 1 + c2dbGetOne("select max(message_id) from spfm_messages");
    c2dbExec("insert into spfm_messages (message_id, disp_place) values (".$max_id.", 'DASHBD@SOGO')");
    echo "ok".'{forwardSection:"mst_message", forwardOption:"'.$opt.'"}';
    die;
}
function reg_mst_message_delrow() {
    $opt = ($_REQUEST["sql_crlf"] ? "&sql_crlf=1" :"");
    $message_id = (int)$_REQUEST["message_id"];
    c2dbExec("delete from spfm_messages where message_id = ".$message_id);
    echo "ok".'{forwardSection:"mst_message", forwardOption:"'.$opt.'"}';
    die;
}


//======================================================================================================================
// テストボタン押下
//======================================================================================================================
function html_mst_message_test() {
    $is_kojin = $_REQUEST["is_kojin"];
    $pattern_count = max(1, (int)$_REQUEST["pattern_count"]);
    $message_id = (int)$_REQUEST["message_id"];
    $is_init = $_REQUEST["is_init"]; // 「メッセージ通知設定」ダイアログを開いた直後にもテスト実行される。そのときis_initに値あり。
    $sql_crlf = $_REQUEST["sql_crlf"];
    $sql_crlf_opt = ($sql_crlf ? "&sql_crlf=1":"");

    $message_formula = get_message_formula($message_id, $is_init, $pattern_count); // テスト実行した瞬間の応答設定各種

    $sql = trim($_REQUEST["sql_formula"]);
    $toperr = "";
    $topRow = 0;
    do {
        if (strtolower(substr($sql,0,6))!="select") {
            $toperr = "判定SQLは「select」で開始してください。";
            break;
        }
        $sql = getSwapMessage($sql, 1, "");
        c2dbSetErrorReturn(true);
        $topRow = c2dbGetTopRow("select * from (".$sql.") d limit 1");
        $err = c2dbGetLastErrorMsg();
        if ($err) {
            $toperr = "実行エラーが発生しました。<br><br>".hh(trim($err));
            break;
        }
    } while (0);
    if (!$toperr) {
		// 行が取得できなかった。フィールド名がわからないので再取得
	    if (!is_array($topRow)) {
			$topRow = array();
		    $c2Con = _c2dbGetConnection($sql);
		    $statement = $c2Con->prepare($sql, array());
		    $query_result = $statement->execute($c2db->spParams);
		    $field_count = pg_num_fields($query_result->result);
		    for ($idx = 0; $idx < $field_count; $idx++) {
				$field_nm = pg_field_name($query_result->result, $idx);
				$topRow[$field_nm] = "";
			}
		}
        $fieldNames = array_keys($topRow);
        if (in_array("?column?", $fieldNames)) {
            $toperr = "検索結果に不明なフィールド名があります。セレクトする項目に「as xxx」エイリアスを指定し、取得フィールド名を決めてください。";
        }
        for ($idx=0; $idx<count($fieldNames); $idx++) {
            if ($fieldNames[$idx]=="?column?") $fieldNames[$idx] = "（フィールド名不明）";
        }
    }
    $listLineupRows = c2dbGetRows("select * from spfm_list_lineup where is_active = '1' order by luid");


    if ($toperr) echo '<div style="padding-bottom:5px" class="red">'.$toperr.'</div>';
?>

    <div style="width:880px; height:190px; overflow-y:scroll; border:1px solid #bbbbbb; background-color:#eeeeee" id="div_response_list">

        <textarea class="script">
            function usevalCheck(idx) {
                var cmb = ee("field_joken"+idx);
                ee('span_field_value'+idx).style.display=(cmb.options[cmb.selectedIndex].getAttribute('useval')?'':'none');
            }
            defaultSql = ee("sql_formula").value;
            defaultIsKojin = getComboValue(ee("is_kojin"));
            defaultPatternCount = getComboValue(ee("pattern_count"));
        </textarea>

        <table cellspacing="0" cellpadding="0" style="background-color:#ffffff">
        <? for ($puidx=1; $puidx<=$pattern_count; $puidx++) { ?>
            <? $mfobj = $message_formula[$puidx]; ?>
            <tr>
            <th style="width:70px"><nobr>応答<?=$puidx?></nobr></th>
            <td class="top left" style="padding:5px">
            <div style="padding-bottom:4px">
                <select name="field_alias<?=$puidx?>" id="field_alias<?=$puidx?>" onchange="ee('div_ta<?=$puidx?>').setAttribute('navi', '取得値は半角２重中カッコを利用し、{{'+this.value+'}}で表示に組み込むことができます。');">
                <? foreach( $fieldNames as $f){ ?>
                <option value="<?=$f?>"<?=($f==""?" selected":"")?>><?=$f?></option>
                <? } ?>
                </select>
                の値が
                <select name="field_joken<?=$puidx?>" id="field_joken<?=$puidx?>" onchange="usevalCheck(<?=$puidx?>)">
                    <?=getJokenPulldownOptions("")?>
                </select>
                <span id="span_field_value<?=$puidx?>">
                （所定値：<input type="text" class="text w200" name="field_value<?=$puidx?>" value="<?=hh($mfobj["field_value"])?>" />）
                </span>
            </div>
            <div id="div_ta<?=$puidx?>" navi="取得値は半角２重中カッコを利用し、{{<?=hh($fieldNames[0])?>}}で表示に組み込むことができます。">
                上記条件に合致した場合の通知メッセージ（HTML）<br>
                <textarea name="field_html<?=$puidx?>" cols="50" rows="3" style="width:770px; height70px:"><?=hh($mfobj["field_html"])?></textarea>
            </div>
            <div style="padding:2px 0">
            <? if ($is_kojin) { ?>
                <? $menuRows = c2dbGetRows("select * from spfm_menu where is_active = '1' and menu_hier in ('chu','syo') order by menu_order"); ?>
                通知メッセージをクリックしたときの遷移先を指定(個人向け)
                <input type="hidden" name="menu_code" value="Menu" />
                <select name="muid<?=$puidx?>" id="msg_muid<?=$puidx?>">
                    <option value=""></option>
                <? foreach ($menuRows as $row) { ?>
                    <option value="<?=$row["muid"]?>"><?=$row["muid"]?>:　<?=$row["menu_label"]?></option>
                <? } ?>
                </select>
            <? } else { ?>
                メッセージクリックによる遷移先を指定(全体向け)
                <? $finderRows = c2dbGetRows("select * from spfm_finder where finder_title is not null and finder_title <> '' order by finder_title"); ?>
                <select name="menu_code<?=$puidx?>" id="msg_menu_code<?=$puidx?>" onchange="showFinderFields(<?=$puidx?>)">
                    <option value=""></option>
                    <option value="Dashboard">ダッシュボード</option>
                    <option value="SendStat">申請一覧</option>
                    <option value="RecvStat">受付一覧</option>
                <? foreach ($finderRows as $row) { ?>
                    <option value="Finder@<?=$row["finder_id"]?>">検索<?=$row["finder_id"]?>:　<?=hh($row["finder_title"])?></option>
                <? } ?>
                </select>
                <fieldset id="finder_fields_title<?=$puidx?>">
                <legend>検索画面を開いたときの検索条件初期値</legend>
                <?
                    foreach ($finderRows as $row) {
                        $finder_id = $row["finder_id"];
                        if ($row["finder_title"]=="") continue;
                        for ($fidx=1; $fidx<=15; $fidx++) {
                            $joken = unserialize($row["table_field_joken".$fidx]);
                            if (!$joken) continue;
                            if (!$joken["field_label"]) continue;
                            echo '<div finder_id="'.$finder_id.'" pattern_idx="'.$puidx.'" style="display:none; padding-bottom:2px">';
                            $key = 'default_value'.$puidx.'_'.$finder_id.'_'.$fidx.'@'.$joken["long_field_id"];

                            if ($joken["field_type"]=="checkbox") {
                                echo $joken["field_label"]."【チェック形式】";
                                echo '<select name="'.$key.'">';
                                echo '<option value=""></option>';
                                echo '<option value="1"'.($mfobj[$key]?" selected":"").'>オン</option>';
                                echo '</select>';
                            }
                            else {
                                $type_jp = "";
                                if ($joken["field_type"]=="text") $type_jp="テキスト形式";
                                if ($joken["field_type"]=="pulldown") $type_jp="プルダウン形式";
                                if ($joken["field_type"]=="ymd") $type_jp="年月日形式";
                                if ($joken["field_type"]=="ym") $type_jp="年月形式";
                                if ($joken["field_type"]=="checkbox") $type_jp="チェック形式";
                                echo $joken["field_label"]."【".$type_jp."】";
                                echo '<input type="text" class="text" value="'.$mfobj[$key].'"';
                                echo ' name="'.$key.'" />';
                            }
                            echo '</div>';
                        }
                    }
                ?>
                </fieldset>
            <? } ?>
            </div>
            </td></tr>
        <? } // end of for ?>
        </table>
        <textarea class="script">
            function textExecuted() {
                <? for ($puidx=1; $puidx<=$pattern_count; $puidx++) { ?>
                <? $mfobj = $message_formula[$puidx]; ?>
                    setComboValue(ee("field_alias<?=$puidx?>"), "<?=$mfobj["field_alias"]?>");
                    setComboValue(ee("field_joken<?=$puidx?>"), "<?=$mfobj["field_joken"]?>");
                    <? if ($is_kojin) { ?>
                        //setComboValue(ee("msg_luid<?=$puidx?>"), "<?=$mfobj["luid"]?>");
                        setComboValue(ee("msg_muid<?=$puidx?>"), "<?=$mfobj["muid"]?>");
                    <? } else { ?>
                        setComboValue(ee("msg_menu_code<?=$puidx?>"), "<?=$mfobj["menu_code"]?>");
                    <? } ?>
                    usevalCheck(<?=$puidx?>);
                    showFinderFields(<?=$puidx?>);
                <? } ?>
            }
            function showFinderFields(pattern_idx) {
                var finder_id = "";
                <? if ($is_kojin) { ?>
                    //setComboValue(ee("msg_luid<?=$puidx?>"), "<?=$mfobj["luid"]?>");
                    var combo = ee("msg_muid"+pattern_idx);
                <? } else { ?>
                    var combo = ee("msg_menu_code"+pattern_idx);
                <? } ?>
                var comboValue = getComboValue(combo).split("@");
                if (comboValue[0]=="Finder") {
                    finder_id = comboValue[1];
                }
                <? if (!$is_kojin) { ?>
                ee("finder_fields_title"+pattern_idx).style.display = (finder_id=="" ? "none":"");
                $("#finder_fields_title"+pattern_idx+" div").each(function(){
                    var _pattern_idx = this.getAttribute("pattern_idx");
                    if (_pattern_idx!=pattern_idx) return;

                    var _finder_id = this.getAttribute("finder_id");
                    this.style.display = (_finder_id == finder_id ? "" : "none");
                });
                <? } ?>
            }
        </textarea>
    </div>


    <?
    if (!$toperr) {
        $msgObj = getOneMessageHtml($message_formula, $topRow, "");
        if ($msgObj["html"]==="") {
            echo '<div style="padding-top:5px">';
            echo '<span style="color:#00aa00">現在の実行結果では、表示されるメッセージはありません。</span>';
            echo '</div>';
        } else {
            echo '<div style="padding:5px; background-color:#ffffcc">';
            echo '<table cellspacing="0" cellpadding="0"><tr><td>上記判定結果で表示されるメッセージ：</td><td>';
            echo '<textarea cols="50" rows="1" style="width:600px" readOnly>'.hh($msgObj["html"]).'</textarea></td></tr></table>';
            echo '</div>';
        }
    }
    ?>

    <div class="center" style="padding-top:10px; position:relative">
        <? if (!$toperr) { ?>
            <button type="button" id="btn_save" onclick="tryAdminReg('message_editor', 'reg_mst_message', '<?=$sql_crlf_opt?>', '', 1)" class="w70">保存</button>
        <? } ?>
    </div>


<?
    die;
}

function get_message_formula($message_id, $is_init, $pattern_count) {
    if ($message_id && $is_init) {
        return unserialize(c2dbGetOne("select message_formula from spfm_messages where message_id = ".(int)$message_id));
    } else {
        $message_formula = array();
        for ($puidx=1; $puidx<=$pattern_count; $puidx++) {
            $ary = array(
                "field_alias" => $_REQUEST["field_alias".$puidx],
                "field_joken" => $_REQUEST["field_joken".$puidx],
                "field_value" => $_REQUEST["field_value".$puidx],
                "field_html" => $_REQUEST["field_html".$puidx],
                "luid" => $_REQUEST["luid".$puidx],
                "muid" => $_REQUEST["muid".$puidx],
                "menu_code" => $_REQUEST["menu_code".$puidx]
            );
            foreach ($_REQUEST as $k => $v) {
                if (substr($k,0,13)=="default_value") $ary[$k] = $v;
            }
            $message_formula[$puidx] = $ary;
        }
    }
    return $message_formula;
}


function reg_mst_message() {
    $opt = ($_REQUEST["sql_crlf"] ? "&sql_crlf=1" :"");
    $message_id = (int)$_REQUEST["message_id"];
    if (!$message_id) { echo "不正なパラメータです。(message_id)"; die; }
    $pattern_count = max(1, (int)$_REQUEST["pattern_count"]);
    $message_formula = get_message_formula(0, 0, $pattern_count);
    $sql =
    " update spfm_messages set".
    " message_title = ".c2dbStr($_REQUEST["message_title"]).
    ",message_formula = ".c2dbStr(serialize($message_formula)).
    ",is_active = '".($_REQUEST["is_active"] ? "1":"")."'".
    ",sql_formula = ".c2dbStr($_REQUEST["sql_formula"]).
    ",pattern_count = ".$pattern_count.
    ",disp_place = ".c2dbStr($_REQUEST["disp_place"]).
    ",is_kojin = ".c2dbStr($_REQUEST["is_kojin"]).
    ",sort_order = ".c2dbStr($_REQUEST["sort_order"]).
    " where message_id = ".$message_id;
    c2dbExec($sql);
    echo "ok".'{forwardSection:"mst_message", forwardOption:"&message_id='.$message_id.$opt.'"}';
    die;
}
