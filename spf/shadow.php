<?php
//****************************************************************************************************************
// プロファイル画像の「フチどり＋シャドウ画像」を作成します。
// ふちどりシャドウ画像は、プロファイル画像表示の際、常に動的に作成している、ということです。
//
// 例えば、<img src="shadow.php?mgn=xx&rate=xx&iw=xx&ih=xx" />
// 例えば、<div style="background:url(shadow.php?mgn=xx&rate=xx&iw=xx&ih=xx)"></div>
// というように利用します。「mgn」「rate」省略時はデフォルト値があります。
//
// 「mgn」「rate」「iw」「ih」の４つのパラメータからシャドウを作成します。
//****************************************************************************************************************
header("Content-Type: image/png");

$mgn = trim($_REQUEST["mgn"]); // マージン、写真ぽい白背景部分に該当するもの
if ($mgn==="") $mgn = 8;

$depth = 9;

$rate = trim($_REQUEST["rate"]); // シャドウの濃さに関連
if ($rate==="") $rate = 0.4;


$ow = $_REQUEST["iw"] + $mgn + $mgn; // iw = 画像幅 image width
$oh = $_REQUEST["ih"] + $mgn + $mgn; // ih = 画像高 image height


$iw = $ow + $depth + $depth;
$ih = $oh + $depth + $depth;

$img = imagecreatetruecolor($iw, $ih);
imagesavealpha($img, true);
imagealphablending($img, false);
$s1  = imagecolorallocatealpha($img, 0, 0, 0, 127); // 黒のクリア色を作成
imagefilledrectangle($img, 0, 0, $iw-1, $ih-1, $s1);

$deep = array(87,92,98,105,112,117,121,124,126);

$corner = array(
    array(127,127,127,127,127,126,126,126,126),
    array(127,127,126,126,126,126,125,125,124),
    array(127,126,126,126,125,124,123,123,122),
    array(127,126,126,125,124,123,121,120,118),
    array(127,126,125,124,122,120,118,116,114),
    array(126,125,124,122,120,118,115,112,109),
    array(126,125,123,121,118,115,111,107,103),
    array(126,125,123,120,116,112,107,101,97),
    array(126,124,122,119,115,110,103,95,93)
);

for ($dp = $depth; $dp > 0; $dp--) {
    $s1  = imagecolorallocatealpha($img, 0, 0, 0, $deep[$dp-1]-((127-$deep[$dp-1])*$rate)); // 透明度を持つ黒色を作成 127 / 16 * 15
    imagefilledrectangle($img, $depth-$dp+2, $depth-$dp+4, $depth+$ow+$dp-1-2, $depth+$oh+$dp-1, $s1);
}

foreach ($corner as $col => $cols){
    foreach ($cols as $row => $alpha){
        $a = $alpha - ((127-$alpha)*($rate+0.1));
        imagesetpixel($img, $col+2, $row+4, imagecolorallocatealpha($img, 0, 0, 0, $a));
        imagesetpixel($img, $iw-$col-1-2, $row+4, imagecolorallocatealpha($img, 0, 0, 0, $a));
        imagesetpixel($img, $col+2, $ih-$row-1, imagecolorallocatealpha($img, 0, 0, 0, $a));
        imagesetpixel($img, $iw-$col-1-2, $ih-$row-1, imagecolorallocatealpha($img, 0, 0, 0, $a));
    }
}

$s1  = imagecolorallocatealpha($img, 255, 255, 255, 0); // 真ん中を白で
//$s1  = imagecolorallocatealpha($img, 245, 245, 245, 0);
//$s1  = imagecolorallocatealpha($img, 0, 0, 0, 0);
//$s1  = imagecolorallocatealpha($img, 210, 210, 210, 0);
imagefilledrectangle($img, $depth, $depth, $iw-$depth-1, $ih-$depth-1, $s1);

imagepng($img); // ファイル出力
imagedestroy($img); // 開放
?>