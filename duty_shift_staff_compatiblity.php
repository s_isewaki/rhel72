<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜相性設定</title>
<!-- 相性設定 -->
<!-- duty_shift_staff_compatiblity.php -->
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
html {height:100%;}
td.w180{ width:180;}
td.w30 { width:40; }
td.w80 { width:80; }
td.w135{ width:135;}
td.w136{ width:136;}
td.w138{ width:138;}
td.w44 { width:44.5; }
td.w45 { width:45; }
td.w46 { width:46; }
td.h100{ width:40;height:120;}
body{
	margin-bottom:0px;
	margin-bottom:0px;
	height:100%;
}
#NameH {
	margin-left:10px;
	overflow-y:hidden;
	overflow-x:hidden;
	background-color:#FFFFFF;
}
#NameV {
	margin-top:-1px;
	margin-left:10px;
	margin-bottom:0px;
	padding-bottom:0px;
	height:540px;
	overflow-y:scroll;
	background-color:#FFFFFF;
}
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
<?
// フロートウィンドウの処理はduty_shift_menu.phpから流用
?>
<!-- ************************************************************************** -->
<!-- JavaScript（メッセージ子画面表示処理）-->
<!-- ************************************************************************** -->
<script language="JavaScript">
    function showFloatName(data1, data2) {
        try {
            //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
//            var data1 = "氏名";
//            var data2 = memo;

            if (data1 != "") {
//                var msg1  = '<table bgcolor="#dddddd" border="1">\n';
//                    msg1 += '<tr><td colspan="2" bgcolor="#ccddFF" align=\"center\"><b>'+data1+'</b></td></tr>\n';
//                    msg1 += '<tr><td>'+data2+'</td></tr>\n';
//                    msg1 += '</table>\n';
//                showMsg(msg1);
                var msg1  = '<table bgcolor="#dddddd" border="1">\n';
                    msg1 += '<tr><td>縦:'+data1+'</td></tr>\n';
                    msg1 += '<tr><td>横:'+data2+'</td></tr>\n';
                    msg1 += '</table>\n';
                showMsg(msg1);
            }
        }
        catch(err){
        }
    }
    ///-----------------------------------------------------------------------------

    //(注1)

    //--HTML出力
    function outputLAYER(layName,html){
    if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
      document.getElementById(layName).innerHTML=html;
    } else if(document.all){            //e4用
      document.all(layName).innerHTML=html;
    } else if(document.layers) {        //n4用
       with(document.layers[layName].document){
         open();
         write(html);
         close();
      }
    }
    }

    //--マウス追跡
    /*==================================================================
    followingLAYER()

    Syntax :
     追跡レイヤー名 = new followingLAYER('レイヤー名'
                                ,右方向位置,下方向位置,動作間隔,html)

     レイヤー名 マウスを追跡させるレイヤー名
     右方向位置 マウスから右方向へ何ピクセル離すか
     下方向位置 マウスから下方向へ何ピクセル離すか
     動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
     html       マウスを追跡するHTML

    ------------------------------------------------------------------*/
    /*--/////////////ここから下は触らなくても動きます/////////////--*/

    //--追跡オブジェクト
    //e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
    function followingLAYER(layName,ofx,ofy,delay,html){
    this.layName = layName;   //マウスを追跡させるレイヤー名
    this.ofx     = ofx;       //マウスから右方向へ何ピクセル離すか
    this.ofy     = ofy;       //マウスから下方向へ何ピクセル離すか
    this.delay   = delay;     //マウスを追跡するタイミング
    if(document.layers)
      this.div='<layer name="'+layName+'" left="-100" top="-100">\n'              + html + '</layer>\n';
    else
      this.div='<div id="'+layName+'"\n'              +'style="position:absolute;left:-100px;top:-100px">\n'              + html + '</div>\n';
    document.write(this.div);
    }

    //--メソッドmoveLAYER()を追加する
    followingLAYER.prototype.moveLAYER = moveLAYER; //メソッドを追加する
    function moveLAYER(layName,x,y){
    if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
        document.getElementById(layName).style.left = x;
        document.getElementById(layName).style.top  = y;
    } else if(document.all){            //e4用
        document.all(layName).style.pixelLeft = x;
        document.all(layName).style.pixelTop  = y;
    } else if(document.layers)          //n4用
        document.layers[layName].moveTo(x,y);
    }

    //--Eventをセットする(マウスを動かすとdofollow()を実行します)
    document.onmousemove = dofollow;
    //--n4マウスムーブイベント走査開始
    if(document.layers)document.captureEvents(Event.MOUSEMOVE);
    //--oの全画面のEventを拾えないことへの対策
    if(window.opera){
    op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'             +'                     left:100%;top:100%"></div> ';
    document.write(op_dmydoc);
    }

    //--イベント発生時にマウス追跡実行
    function dofollow(e){
    for(var i=0 ; i < a.length ; i++ )
      setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"          +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"       ,a[i].delay);
    }

    //--マウスX座標get
    function getMouseX(e){
    if(navigator.userAgent.search(
             "Opera(\ |\/)6") != -1 )   //o6用
        return e.clientX;
    else if(document.all)               //e4,e5,e6用
        return document.body.scrollLeft+event.clientX;
    else if(document.layers ||
            document.getElementById)    //n4,n6,n7,m1,o7,s1用
        return e.pageX;
    }

    //--マウスY座標get
    function getMouseY(e){
    if(navigator.userAgent.search(
             "Opera(\ |\/)6") != -1 )   //o6用
        return e.clientY;
    else if(document.all)               //e4,e5,e6用
        return document.body.scrollTop+event.clientY;
    else if(document.layers ||
            document.getElementById)    //n4,n6,n7,m1,o7,s1用
        return e.pageY;
    }

    /*////////////////////////////// マウスを追跡するレイヤーここまで */

    //テンプレートを作成しoutputLAYERへ渡す
    function showMsg(msg1){
        outputLAYER('test0',msg1);
    }

    //レイヤーの中身を消す
    function hideMsg(){
        var msg1 ='';
        outputLAYER('test0',msg1);
    }

    //レイヤーの数だけa[i]=…部分を増減して使ってください
    var a = new Array();
    a[0]  = new followingLAYER('test0',20,10,100,'');
</script>
</head>

<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("show_class_name.ini");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	// 勤務パターン情報を取得
	///-----------------------------------------------------------------------------
	$wktmgrp_array = $obj->get_wktmgrp_array();
	if (count($wktmgrp_array) <= 0) {
		$err_msg_1 = "勤務パターン情報(wktmgrp)が未設定です。管理者に連絡してください";
	}

	?>

<link rel="stylesheet" type="text/css" href="css/main.css">

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?
	// 画面遷移
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	show_user_title($session, $section_admin_auth);		//duty_shift_common.ini
	echo("</table>\n");

	// タブ
	// 相性設定条件を追加する（パラメータ追加）201401 START -->
	//	$arr_option = "&group_id=" . $group_id ."&staff_id=" . $staff_id;
	$arr_option = "&group_id=" . $group_id . "&pattern_id=" . $pattern_id;
	// 相性設定条件を追加する（パラメータ追加）END
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	show_user_tab_menuitem($session, $fname, $arr_option);	// duty_shift_user_tab_common.ini
	echo("</table>\n");

	// 下線
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
<?
	if ($err_msg_1 != "") {
		echo($err_msg_1);
		echo("<br>\n");
	}
?>

<!-- 相性設定条件を追加する（パラメータ追加）201401 START -->
<!-- <FORM name="mainform" action="duty_shift_staff_com_update_exe.php?session=<?=$session?>" method="post">  -->
<FORM name="mainform" action="duty_shift_staff_com_update_exe.php?session=<?=$session?>&group_id=<?=$group_id?>&pattern_id=<?=$pattern_id?>" method="post">
<!-- 相性設定条件を追加する END -->

&nbsp&nbsp相性設定&nbsp&nbsp&nbsp&nbsp&nbsp<input type="submit" value="更新">
<DIV ID="NameH">
<TABLE BORDER="0" cellspacing="0" cellpadding="0" class="list" style="table-layout: fixed; ">
<?
	$name_array = array();

	// 前画面から持ってきた職員IDで $name_arrayを作る

	if( $staff_count == "" || $staff_count <=0 ){
		// 更新プログラムから戻ってきたとき
		// パラメータ不一致：再取得する 201401
//		$a_data = explode(",",$arg_data);
//		for($i=1;$i<count($a_data);$i++){ // 1番目はダミーなので捨てる
//			$name_array[$i-1]['id'] = $a_data[$i];
//		}
		$data_st = $obj->get_stmst_array();
		$data_job = $obj->get_jobmst_array();
		$data_emp = $obj->get_empmst_array("");
		$name_array = array();
		$name_array = $obj->get_duty_shift_staff_array($group_id, $data_st, $data_job, $data_emp);
		$staff_count = count($name_array);

	}else{
		// 職員設定画面から呼ばれたとき
		for($i=0; $i<$staff_count; $i++){
			$emp_id = "staff_id_".$i;
			$emp_id = $$emp_id;
			$name_array[$i]['id'] = $emp_id;
		}
	}

	// 日本語を持ってくるのは危険なので職員IDよりＤＢから氏名を得る
	$sql = "SELECT emp_lt_nm,emp_ft_nm FROM empmst ";
	for($i=0; $i<$staff_count; $i++){
		$emp_id = $name_array[$i]['id'];
		$cond = "WHERE emp_id = '$emp_id' ";
		$sel = select_from_table($con, $sql, $cond, $fname);
		$n = pg_numrows($sel);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$lt_nm = pg_result($sel, $c, "emp_lt_nm");
		$ft_nm = pg_result($sel, $c, "emp_ft_nm");
		$name_array[$i]['name'] = $lt_nm.$ft_nm;
	}

	// $name_arrayから$comp_array を作る。相性テーブルemp_pair_setから相性データを得る
	$comp_array = array();
	$ptn_array = array();								// 相性設定条件を追加する

	$stt = 1;
	$sql = "select emp_id, emp_id_2, compatibility, atdptn_id from emp_pair_set ";
	for( $r = 0 ; $r < $staff_count ; $r++ ){
		$rid = $name_array[$r]['id'];
		for( $c=$stt ; $c < $staff_count ; $c++ ){
			$cid = $name_array[$c]['id'];
			$comp_array[$rid][$cid] = "";	// initialize
			$ptn_array[$rid][$cid] = "";	// initialize

			$cond = "where (emp_id = '$rid' and emp_id_2 = '$cid') or (emp_id = '$cid' and emp_id_2 = '$rid') ";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$kekka = '';
			$kekka2 = 0;
			if (pg_numrows($sel) != 0){
				$kekka =  pg_result($sel, 0, "compatibility"); // 1件しか出てこないはず。
				$kekka2 = pg_result($sel, 0, "atdptn_id");
			}
			$comp_array[$rid][$cid] =  $kekka;
			$ptn_array[$rid][$cid] = ($kekka2 > 0) ? $kekka2 : '' ;
		}
		$stt++;
	}
	//------------------------------------------------------------------------
	// 相性設定条件を追加する（シフト）201401 START
	//------------------------------------------------------------------------
	// シフトパターンデータの取得
	$sql = "select * from atdptn ap join duty_shift_pattern dp on ap.atdptn_id=dp.atdptn_ptn_id and ap.group_id=dp.pattern_id ";
	$sql .="left join atdbk_reason_mst rm on dp.reason=rm.reason_id ";
	$sql .="where ap.group_id='$pattern_id' and (rm.display_flag is null or rm.display_flag='t') ";
	$sql .= "and ap.atdptn_id <> '10' ";			// 休暇を除外するか ※休暇のIDは10で固定的な扱い
	$sql .= "order by order_no, atdptn_id";

	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$pattern = array();
	while ($row = pg_fetch_assoc($sel)) {
		// 相性設定改修時：休暇詳細は扱わない
//		$id = sprintf("%02d%02d", $row["atdptn_ptn_id"], $row["reason"]);
		$id = $row["atdptn_ptn_id"];
		$name = $row["font_name"];
		$font_color = $row["font_color_id"];
		$back_color = $row["back_color_id"];

		// 相性設定改修時：休暇詳細は扱わない
//		if($row["reason"] >= "44" and $row["reason"] <= "47") {
//			$name = $hol_pattern[ $row["reason"] ];
//		}
//		elseif(!empty($hol_pattern[ $row["reason"] ])) {
//			$name .= "(". $hol_pattern[ $row["reason"] ]. ")";
//		}
		$pattern[] = array(
			"id" => $id,
			"name" => $name,
			"font_color" => $font_color,
			"back_color" => $back_color
		);
	}

	$pattern_list = "<option value=\"0\"></option>\n";		// 先頭の空白
   	for( $i=0; $i < count($pattern); $i++) {
   		$pattern_list .= "<option style=\"background-color:".$pattern[$i]["back_color"]."; color:".$pattern[$i]["font_color"]."\" value=\"".$pattern[$i]["id"]."\">".$pattern[$i]["name"]."</option>\n";
	}
	// 相性設定条件を追加する END


	pg_close($con); // DB閉じる


	// 横見出し氏名
	// 相性設定条件を追加する＋列幅を調整 START

	//	echo "<TR height=\"30\" align=\"center\" valign=\"center\" >\n";
	echo "<TR height=\"35\" align=\"center\" valign=\"center\" >\n";
	echo "<TD CLASS=\"w80\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp</TD>";
	for( $c=0 ; $c < $staff_count ; $c++ ){
		$name = $name_array[$c]['name'];
		$ln_name=mb_substr($name,0,1);
		for($lc=1; $lc<strlen($name); $lc++){
			$wk = mb_substr($name,$lc,1);
			$ln_name = $ln_name."<BR>".$wk;
		}
//		$disp = "<TD CLASS=\"h100\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ln_name</TD>";
		$disp = "<td height=\"100\" width=\"50\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$ln_name</TD>";
		echo $disp;
		$cid = $name_array[$c]['id'];
		$name = $name_array[$c]['name'];
	        echo("<input type=\"hidden\" name=\"cid_".$c."\" value=\"$cid\">\n");
	        echo("<input type=\"hidden\" name=\"name_".$c."\" value=\"$name\">\n");
	}
	$disp = "<TD CLASS=\"w80\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">&nbsp</TD>";
	echo $disp;
	echo "</TR>\n";
?>
</TABLE>
</DIV>
<DIV ID="NameV">
<TABLE BORDER="0" cellspacing="0" cellpadding="0" class="list" style="table-layout: fixed; ">
<?

	// 相性設定条件を追加する＋列幅を調整 START
	for( $r=0 ; $r < $staff_count ; $r++ ){
//		echo "<TR height=\"30\" align=\"center\" valign=\"center\" >\n";	// 相性設定追加：高さ拡張
		echo "<TR height=\"35\" align=\"center\" valign=\"center\" >\n";
		$hname = $name_array[$r]['name'];
		$rid = $name_array[$r]['id'];
		echo "<TD CLASS=\"w80\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$hname</TD>";
		// FORM SELECT
		for( $c=0 ; $c < $staff_count ; $c++ ){
			$mask = " BGCOLOR=\"#FFFFFF\"";
			$vname = $name_array[$c]['name'];
			$name_opt = "onmouseover=\"showFloatName('$vname','$hname');\" onmouseout=\"hideMsg();\"";
			$cid = $name_array[$c]['id'];
			if ( $c <= $r ){
				// 左下側半分・表示のみ
				$mask = " BGCOLOR=\"#e0e0e0\"";
				if( $c == $r ){
					$set_value = '−';
				}else{
					$set_value = $comp_array[$cid][$rid];
					if ($set_value != "") { 													// 相性設定がある時
						$temp_value = search_ptn_item($pattern, $ptn_array[$cid][$rid]);
						if($temp_value >= 0) {
							$set_value .= "<br>" . $pattern[$temp_value]["name"];				// 相性設定（シフトパターン）追加
						}																		// シフト設定はなくても良い
					}
				}
//				echo " <TD CLASS=\"w30\" $mask align=\"center\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">"; // 相性設定追加：幅拡張
				echo " <td width=\"50\" $mask align=\"center\"><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
				echo $set_value;
			}else{
				// 右上側半分・入力欄
				$set_value = "";
				$set_value = $comp_array[$rid][$cid];
//				echo " <TD CLASS=\"w30\" $mask $name_opt ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">"; // 相性設定追加：幅拡張
				echo " <td width=\"50\" $mask $name_opt ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\">";
				$disp = "<select name=\"pair_set_".$c."_".$r."\" style=\"width:50px;\"><option>$set_value</option>";
				$disp = $disp."	<option value=\"○\">○</option>";
				$disp = $disp."	<option value=\"×\">×</option>";
				$disp = $disp."	<option value=\"\">解</option>";
				$disp = $disp."</select>\n";
				echo $disp;

				// 相性設定条件を追加する START
				$disp = "<select name=\"shift_set_".$c."_".$r."\" style=\"width:50px;\">";
				// 設定に該当する値を検索したら初期表示に含める
				$temp_value = search_ptn_item($pattern, $ptn_array[$rid][$cid]);
				if($temp_value == -1) {
					//見つからなかった（未設定箇所）
					$disp .= "<option></option>";
				} else {
					$disp .= "<option value=\"".$pattern[$temp_value]["id"]."\">".$pattern[$temp_value]["name"]."</option>\n";
				}
				$disp = $disp . $pattern_list;
				$disp = $disp . "</select>\n";
				echo $disp;
				// 相性設定条件を追加する END
			}
			echo "</TD>\n";
		}
		$rid = $name_array[$r]['id'];
        echo("<input type=\"hidden\" name=\"rid_".$r."\" value=\"$rid\">\n"); // 職員ID。この画面へ戻ってくるときに必要

		echo "<TD CLASS=\"w80\" ><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$hname</TD>";
		echo "</TR>\n";
	}
	// 相性設定条件を追加する＋列幅を調整 END
?>
</TABLE>
</DIV>
</td></tr></table>
        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
<?
	echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
	echo("<input type=\"hidden\" name=\"staff_count\" value=\"$staff_count\">\n");
	echo("<input type=\"hidden\" name=\"group_id\" value=\"$group_id\">\n");
	echo("<input type=\"hidden\" name=\"pattern_id\" value=\"$pattern_id\">\n");	// 相性設定追加：パラメータ追加
	?>
</FORM>
</body>
</html>
<?
// 設定に該当する値を検索したら初期表示に含める
function search_ptn_item($pattern, $search_ptn) {

	$ret = -1;

	for( $i = 0; $i < count($pattern); $i++) {
		if ( $pattern[$i]["id"] == $search_ptn ) {
			$ret = $i;
			break;
		}
	}

	return $ret;
}
/*
debug情報

$name_arrayの構成

上位モジュール、またはＤＢ更新モジュールから呼ばれるときに職員IDを持ってきて$name_arrayを構成する

$iは最大で$staff_count
$name_array[$i]['id'] = 職員id
$name_array[$i]['name'] = 職員IDをキーとしてemp_mstテーブルから氏名を得る

$name_arrayから$comp_arrayを作る

$comp_arrayを画面表示と相性DB更新に用いる

$comp_arrayの構成。この構成でridをemp_idと、cidをemp_id_2の２つを条件としてＤＢから相性を得る

$comp_arrayの例('値'をDBから得る)
rid  cid  値
id01 id02 ○
id01 id03 ×
id01 id04
id02 id03 ×
id02 id04
id03 id04

id04が新規登録の職員の場合DBの検索結果が戻らないのでvalueを'(空白)'とする
相性設定が無い場合もDBの検索結果が戻らないのでvalueを'(空白)'とする

画面表示例(左下側半分は表示のみ)

      0      1     2     3  ← $c
      id01 id02  id03  id04...
0:id01 − [○▼][×▼][  ▼]
1:id02 ○    − [×▼][  ▼]
2:id03 ×    ×   −  [  ▼]
3:id04                  −
↑
$r

*/
?>
