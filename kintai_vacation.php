<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 休暇管理</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("atdbk_menu_common.ini");

require_once './common_log_class.ini';
require_once './kintai/common/mdb_wrapper.php'; // MDB本体をWrap
require_once './kintai/common/mdb.php'; 		// DBアクセスユーティリティ
require_once './kintai/common/user_info.php';	// ユーザ情報
require_once './kintai/common/master_util.php';	// マスタ
require_once './kintai/common/exception.php';
require_once './kintai/common/date_util.php';
require_once './kintai/common/code_util.php';
require_once 'settings_common.php'; // 年次有給休暇簿 20131126


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// DBコネクション作成（既存用）
$con = connect2db($fname);

if($umode == "usr"){
    // 権限のチェック
    $checkauth = check_authority($session, 5, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
}else if($umode == "adm"){
    // 勤務管理権限チェック
    $checkauth = check_authority($session, 42, $fname);
    if ($checkauth == "0") {
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
}
elseif ($umode == "duty") {
    //シフト
    require_once("duty_shift_common_class.php");
    $obj = new duty_shift_common_class($con, $fname);
    //ユーザ画面用
    $chk_flg = $obj->check_authority_user($session, $fname);
    if ($chk_flg == "") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showLoginPage(window);</script>");
        exit;
    }
    //管理画面用
    $section_admin_auth = $obj->check_authority_Management($session, $fname);
}
elseif ($umode == "apply") {
    ;
}
else{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$work_admin_auth = check_authority($session, 42, $fname);

if($flg == ""){
    $flg = "1";
}


//フォームデータ取得
$umode 		= $_REQUEST["umode"];
$selEmpID	= $_REQUEST["selEmpID"];
$date_y1	= $_REQUEST["date_y1"];
$date_m1	= $_REQUEST["date_m1"];
$date_d1	= $_REQUEST["date_d1"];
$date_y2	= $_REQUEST["date_y2"];
$date_m2	= $_REQUEST["date_m2"];
$date_d2	= $_REQUEST["date_d2"];

//日付範囲
if($date_y1 == "" || $date_m1 == "" || $date_d1 == ""){
    //初期値（月日）
    require_once './kintai/individual/kintai_vacation_ini.php'; // マスタ
    //開始年計算
    $iniMD = $date_m1.$date_d1;
    $nowMD = date("md");

    if($iniMD <= $nowMD ){
        $date_y1 = date("Y");
    }else{
        $date_y1 = date("Y") - 1;
    }
    //終了年月日計算
    $edYMD = mktime(0, 0, 0, intval($date_m1), intval($date_d1), (intval($date_y1) + 1));   //１年プラス
    $edYMD = mktime(0, 0, 0, date("n", $edYMD), date("j", $edYMD) - 1, date("Y", $edYMD));  //１日マイナス
    $date_y2 = date("Y", $edYMD);
    $date_m2 = date("m", $edYMD);
    $date_d2 = date("d", $edYMD);
}

//データアクセス
try {

    MDBWrapper::init(); // おまじない
    $log = new common_log_class(basename(__FILE__));
    $db = new MDB($log); // ログはnullでも可。指定するとどのPHPからMDBにアクセスした際のエラーかわかる

    $db->beginTransaction(); // トランザクション開始
    
    //部署階層数
    $class_cnt = MasterUtil::get_organization_level_count();
    
    //職員ID・氏名を取得
    $ukey  = "";
    $uType = "";
    if($umode == "usr"){
        $ukey  = $session;
        $uType = "s";
    }else if($umode == "adm" || $umode == "duty" || $umode == "apply"){
        if($selEmpID != ""){
            $ukey  = $selEmpID;
            $uType = "u";
        }
    }
    $user_info  = UserInfo::get($ukey, $uType);
    $base_info  = $user_info->get_base_info();
    $job_info   = $user_info->get_job_info();
    $dept_info  = $user_info->get_dept_info();
    $croom_info = $user_info->get_class_room_info();
    
    //所属
    if($class_cnt == 4 && $croom_info->room_nm != ""){
        $shozoku = $croom_info->room_nm;
    }else{
        $shozoku = $dept_info->dept_nm;
    }
    
    //休暇申請テーブル
    if($base_info->emp_id != ""){
        $cnt = 0;
        $rData = array();
        //atdbkrslt　実績ベース
        $sql  = "";
        $sql .= "SELECT ";
        $sql .= " kintai_hol.apply_id            AS ap_id, ";
        $sql .= " kintai_hol.apply_date          AS ap_date, ";
        $sql .= " kintai_hol.apply_stat          AS ap_status, ";
        $sql .= " kintai_hol.kind_flag           AS hol_kind, ";
        $sql .= " kintai_hol.reason              AS hol_reason, ";
        $sql .= " kintai_hol.start_date          AS st_date, ";
        $sql .= " kintai_hol.end_date            AS ed_date, ";
        $sql .= " kintai_hol.start_time          AS st_time, ";
        $sql .= " kintai_hol.use_hour            AS use_h, ";
        $sql .= " kintai_hol.use_minute          AS use_m, ";
        $sql .= " kintai_hol.remark              AS hol_remark, ";
        $sql .= " atdbk_reason_mst.sequence      AS reason_seq, ";
        $sql .= " COALESCE( ";
        $sql .= "          NULLIF(atdbk_reason_mst.display_name, ''), ";
        $sql .= "          NULLIF(atdbk_reason_mst.default_name, '') ";
        $sql .= "         )                      AS disp_name, ";
        $sql .= " atdbkrslt.reason               AS hol_reason, ";
        $sql .= " atdbk_reason_mst.holiday_count AS hol_count ";
        $sql .= "FROM ";
        $sql .= " atdbkrslt ";
        $sql .= "LEFT JOIN ";
        $sql .= " atdbk_reason_mst ";
        $sql .= "ON ";
        $sql .= " atdbk_reason_mst.reason_id = atdbkrslt.reason ";
        $sql .= "LEFT JOIN ";
        $sql .= " kintai_hol ";
        $sql .= "ON ";
        $sql .= " kintai_hol.emp_id = atdbkrslt.emp_id AND ";
        $sql .= " kintai_hol.start_date <= atdbkrslt.date AND ";
        $sql .= " kintai_hol.end_date   >= atdbkrslt.date AND ";
        $sql .= " (kintai_hol.kind_flag = '5' OR ";
        $sql .= "  kintai_hol.kind_flag = '6' OR ";
        $sql .= "  kintai_hol.kind_flag = '99') ";
        $sql .= "WHERE ";
        $sql .= " atdbkrslt.emp_id = '".$base_info->emp_id."' AND ";
        $sql .= " atdbkrslt.date >= '".$date_y1.$date_m1.$date_d1."' AND ";
        $sql .= " atdbkrslt.date <= '".$date_y2.$date_m2.$date_d2."' AND ";
        $sql .= " NOT (atdbk_reason_mst.kind_flag = '') ";
        $sql .= "ORDER BY ";
        $sql .= " atdbkrslt.date ASC ";
//echo $sql."<br>";		
        $retRec = $db->find($sql);
//echo count($retRec)."<br>";		
        //$rData = array();
        if(count($retRec) > 0){
            //$cnt = 0;
            $totalJ = array();  //事由別累計用
            $totalS = array();  //種類別累計用
            $apply_id = "";
            for($i=0;$i<count($retRec);$i++){
//echo $retRec[$i]["st_date"]."<br>";		
                //日数
                $dateDiff = DateUtil::date_diff($retRec[$i]["st_date"], $retRec[$i]["ed_date"]);
                if($dateDiff > 0){
                    for($j=1;$j<=$dateDiff;$j++){
                        //日付
                        $tmpArr = array();
                        $tmpArr = DateUtil::split_ymd($retRec[$i]["st_date"]);
                        $stDate = date("Ymd", mktime(0, 
                                                     0, 
                                                     0, 
                                                     intval($tmpArr["m"]), 
                                                     intval($tmpArr["d"]) + ($j - 1), 
                                                     intval($tmpArr["y"])
                                                     )
                                      );
                        //範囲外は終了
                        if($stDate < $date_y1.$date_m1.$date_d1 || $stDate > $date_y2.$date_m2.$date_d2){
                            //範囲外！　処理なし
                            
                        }else{
                            if ($rData[$cnt]["ap_id"] == $apply_id) {
                                $apply_id = $rData[$cnt]["ap_id"];
                                continue;
                            }
                        
                            //範囲内！　配列生成              
                            $rData[$cnt]["ymd"] = $stDate;  //Ymd
                            $tmpArr = DateUtil::split_ymd($stDate);
                            $rData[$cnt]["date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
                            //曜日
                            $rData[$cnt]["wday"] = DateUtil::day_of_week($stDate, true);
                            //承認
                            $rData[$cnt]["sts"] = CodeUtil::get_apply_status($retRec[$i]["ap_status"]);
                            //事由
                            $rData[$cnt]["hol_reason"] = $retRec[$i]["disp_name"];
                            //詳細
                            $rData[$cnt]["hol_remark"] = $retRec[$i]["hol_remark"];
                            //日数
                            $rData[$cnt]["daycnt"] = $retRec[$i]["hol_count"];
                            //時間数
                            $timeM = 0;
                            if($retRec[$i]["st_time"] != ""){
                                $rData[$cnt]["time"] = intval($retRec[$i]["use_h"]).":".$retRec[$i]["use_m"];
                                $timeM = intval($retRec[$i]["use_h"]) * 60 + intval($retRec[$i]["use_m"]);
                                //日数クリア
                                $rData[$cnt]["daycnt"] = "";
                            }
                            //申請日
                            $tmpArr = DateUtil::split_ymd($retRec[$i]["ap_date"]);
                            $rData[$cnt]["ap_date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
                            //特別・病気
                            $rData[$cnt]["kind"] = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"]);
                            
                            //累計
                            //事由
                            if($retRec[$i]["ap_status"] == 1){
                                $totalJ[$retRec[$i]["hol_reason"]]["seq"]   = $retRec[$i]["reason_seq"];
                                $totalJ[$retRec[$i]["hol_reason"]]["name"]  = $retRec[$i]["disp_name"];
                                $totalJ[$retRec[$i]["hol_reason"]]["day"]   = floatval($totalJ[$retRec[$i]["hol_reason"]]["day"]) + floatval($rData[$cnt]["daycnt"]);
                                $totalJ[$retRec[$i]["hol_reason"]]["timeM"] = intval($totalJ[$retRec[$i]["hol_reason"]]["timeM"]) + $timeM;
                                //種類
                                $totalS[$retRec[$i]["hol_kind"]]["name"]  = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"], true);
                                $totalS[$retRec[$i]["hol_kind"]]["day"]   = intval($totalS[$retRec[$i]["hol_kind"]]["day"]) + intval($rData[$cnt]["daycnt"]);
                                $totalS[$retRec[$i]["hol_kind"]]["timeM"] = intval($totalS[$retRec[$i]["hol_kind"]]["timeM"]) + $timeM;
                            }
                            
                            $cnt++;
                        }
                    }
                }
            }
        }
        //kintai_hol　申請ベース
        $sql  = "";
        $sql .= "SELECT ";
        $sql .= " kintai_hol.apply_date          AS ap_date, ";
        $sql .= " kintai_hol.apply_stat          AS ap_status, ";
        $sql .= " kintai_hol.kind_flag           AS hol_kind, ";
        $sql .= " kintai_hol.reason              AS hol_reason, ";
        $sql .= " kintai_hol.start_date          AS st_date, ";
        $sql .= " kintai_hol.end_date            AS ed_date, ";
        $sql .= " kintai_hol.start_time          AS st_time, ";
        $sql .= " kintai_hol.use_hour            AS use_h, ";
        $sql .= " kintai_hol.use_minute          AS use_m, ";
        $sql .= " kintai_hol.remark              AS hol_remark, ";
        $sql .= " atdbk_reason_mst.sequence      AS reason_seq, ";
        $sql .= " COALESCE( ";
        $sql .= "          NULLIF(atdbk_reason_mst.display_name, ''), ";
        $sql .= "          NULLIF(atdbk_reason_mst.default_name, '') ";
        $sql .= "         )                      AS disp_name, ";
        $sql .= " atdbk_reason_mst.holiday_count AS hol_count ";
        $sql .= "FROM ";
        $sql .= " kintai_hol ";
        $sql .= "LEFT JOIN ";
        $sql .= " atdbk_reason_mst ";
        $sql .= "ON ";
        $sql .= " atdbk_reason_mst.reason_id = kintai_hol.reason ";
        $sql .= "WHERE ";
        $sql .= " kintai_hol.emp_id = '".$base_info->emp_id."' AND ";
        $sql .= " (kintai_hol.apply_stat = '0' OR ";
        $sql .= "  kintai_hol.apply_stat = '1') AND ";
        $sql .= " (kintai_hol.kind_flag = '5' OR ";
        $sql .= "  kintai_hol.kind_flag = '6' OR ";
        $sql .= "  kintai_hol.kind_flag = '99') AND ";
        $sql .= " ((kintai_hol.start_date >= '".$date_y1.$date_m1.$date_d1."' AND kintai_hol.start_date <= '".$date_y2.$date_m2.$date_d2."') OR ";
        $sql .= "  (kintai_hol.end_date   >= '".$date_y1.$date_m1.$date_d1."' AND kintai_hol.end_date   <= '".$date_y2.$date_m2.$date_d2."')) ";
        $sql .= "ORDER BY ";
        $sql .= " kintai_hol.start_date ASC ";
//echo $sql."<br>";		
        $retRec = $db->find($sql);
//echo count($retRec)."<br>";		
        if(count($retRec) > 0){
            $totalJ = array();  //事由別累計用
            $totalS = array();  //種類別累計用
            for($i=0;$i<count($retRec);$i++){
//echo $retRec[$i]["st_date"]."<br>";		
                //日数
                $dateDiff = DateUtil::date_diff($retRec[$i]["st_date"], $retRec[$i]["ed_date"]);
                if($dateDiff > 0){
                    for($j=1;$j<=$dateDiff;$j++){
                        //日付
                        $tmpArr = array();
                        $tmpArr = DateUtil::split_ymd($retRec[$i]["st_date"]);
                        $stDate = date("Ymd", mktime(0, 
                                                     0, 
                                                     0, 
                                                     intval($tmpArr["m"]), 
                                                     intval($tmpArr["d"]) + ($j - 1), 
                                                     intval($tmpArr["y"])
                                                     )
                                      );
                        //範囲外は終了
                        if($stDate < $date_y1.$date_m1.$date_d1 || $stDate > $date_y2.$date_m2.$date_d2){
                            //範囲外！　処理なし
                            
                        }else{
                            //存在する日付はスルー
                            /*
                            $chkFlg = 0;
                            for($chkI=0;$chkI<count($rData);$chkI++){
                                if($rData[$chkI]["ymd"] == $stDate){
                                    $chkFlg = 1;
                                    break;
                                }
                            }
                            if($chkFlg == 1){
//echo $stDate.":スルー<br>";
                                break;
                            }
                            */
                            //範囲内！　配列生成              
                            $rData[$cnt]["ymd"] = $stDate;  //Ymd
                            $tmpArr = DateUtil::split_ymd($stDate);
                            $rData[$cnt]["date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
                            //曜日
                            $rData[$cnt]["wday"] = DateUtil::day_of_week($stDate, true);
                            //承認
                            $rData[$cnt]["sts"] = CodeUtil::get_apply_status($retRec[$i]["ap_status"]);
                            //事由
                            $rData[$cnt]["hol_reason"] = $retRec[$i]["disp_name"];
                            //詳細
                            $rData[$cnt]["hol_remark"] = $retRec[$i]["hol_remark"];
                            //日数
                            $rData[$cnt]["daycnt"] = $retRec[$i]["hol_count"];
                            //時間数
                            $timeM = 0;
                            if($retRec[$i]["st_time"] != ""){
                                $rData[$cnt]["time"] = intval($retRec[$i]["use_h"]).":".$retRec[$i]["use_m"];
                                $timeM = intval($retRec[$i]["use_h"]) * 60 + intval($retRec[$i]["use_m"]);
                                //日数クリア
                                $rData[$cnt]["daycnt"] = "";
                            }
                            //申請日
                            $tmpArr = DateUtil::split_ymd($retRec[$i]["ap_date"]);
                            $rData[$cnt]["ap_date"] = intval($tmpArr["m"])."/".intval($tmpArr["d"]);
                            //特別・病気
                            $rData[$cnt]["kind"] = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"]);
                            
                            //累計
                            if($retRec[$i]["ap_status"] == 1){
                                //事由
                                $totalJ[$retRec[$i]["hol_reason"]]["seq"]   = $retRec[$i]["reason_seq"];
                                $totalJ[$retRec[$i]["hol_reason"]]["name"]  = $retRec[$i]["disp_name"];
                                $totalJ[$retRec[$i]["hol_reason"]]["day"]   = floatval($totalJ[$retRec[$i]["hol_reason"]]["day"]) + floatval($rData[$cnt]["daycnt"]);
                                $totalJ[$retRec[$i]["hol_reason"]]["timeM"] = intval($totalJ[$retRec[$i]["hol_reason"]]["timeM"]) + $timeM;
                                //種類
                                $totalS[$retRec[$i]["hol_kind"]]["name"]  = CodeUtil::get_vacation_kind($retRec[$i]["hol_kind"], true);
                                $totalS[$retRec[$i]["hol_kind"]]["day"]   = intval($totalS[$retRec[$i]["hol_kind"]]["day"]) + intval($rData[$cnt]["daycnt"]);
                                $totalS[$retRec[$i]["hol_kind"]]["timeM"] = intval($totalS[$retRec[$i]["hol_kind"]]["timeM"]) + $timeM;
                            }
                            
                            $cnt++;
                        }
                    }
                }
            }
        }
        
        //ソート
        foreach ($rData as $key => $rA){
          $rS[$key] = $rA["ymd"];
        }
        array_multisort($rS,SORT_ASC, $rData);

        
    }

    $db->endTransaction(); // トランザクション終了
} catch (BusinessException $bex) {
    $message = $bex->getMessage();
    echo("<script type='text/javascript'>alert('".$message."');</script>");
    echo("<script language='javascript'>history.back();</script>");
    exit;
} catch (FatalException $fex) { 
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}

//事由別累計配列操作
//ソート
foreach ($totalJ as $key => $arr){
  $seq[$key] = $arr["seq"];
}
array_multisort($seq,SORT_ASC, $totalJ);
//出力用配列へ格納
$arrJiyu = array();
$cnt = 0;
foreach($totalJ as $key => $arr){
    $arrJiyu[$cnt] = $arr["name"];
    if(intval($arr["day"]) > 0){
        $arrJiyu[$cnt] .= " ".$arr["day"];
    }
    if(intval($arr["timeM"]) > 0){
        $arrJiyu[$cnt] .= " (".floor($arr["timeM"] / 60).":".sprintf("%02d",($arr["timeM"] % 60)).")";
    }
    $cnt++;
}
//種類別累計配列操作
//出力用配列へ格納
$arrShurui = array();
$cnt = 0;
if($totalS[5]["day"] > 0 || $totalS[5]["timeM"] > 0){
    $arrShurui[$cnt] = $totalS[5]["name"];
    if(intval($totalS[5]["day"]) > 0){
        $arrShurui[$cnt] .= " ".$totalS[5]["day"];
    }
    if(intval($totalS[5]["timeM"]) > 0){
        $arrShurui[$cnt] .= " (".floor($totalS[5]["timeM"] / 60).":".sprintf("%02d",($totalS[5]["timeM"] % 60)).")";
    }
    $cnt++;
}
if($totalS[6]["day"] > 0 || $totalS[6]["timeM"] > 0){
    $arrShurui[$cnt] = $totalS[6]["name"];
    if(intval($totalS[6]["day"]) > 0){
        $arrShurui[$cnt] .= " ".$totalS[6]["day"];
    }
    if(intval($totalS[6]["timeM"]) > 0){
        $arrShurui[$cnt] .= " (".floor($totalS[6]["timeM"] / 60).":".sprintf("%02d",($totalS[6]["timeM"] % 60)).")";
    }
    $cnt++;
}
if($totalS[99]["day"] > 0 || $totalS[99]["timeM"] > 0){
    $arrShurui[$cnt] = $totalS[99]["name"];
    if(intval($totalS[99]["day"]) > 0){
        $arrShurui[$cnt] .= " ".$totalS[99]["day"];
    }
    if(intval($totalS[99]["timeM"]) > 0){
        $arrShurui[$cnt] .= " (".floor($totalS[99]["timeM"] / 60).":".sprintf("%02d",($totalS[99]["timeM"] % 60)).")";
    }
    $cnt++;
}


?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="./kintai/js/common_date.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
<?php
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();
// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>
<script type="text/javascript">
var childwin;
function openEmployeeList(item_id) {
    dx = screen.width;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=' + document.frmKintai.session.value;
    url += '&emp_id=';
    url += '&item_id='+item_id;
<?
if ($umode == "duty") {
?>
	url += '&dept_only_flag=1';
<? } ?>    
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
    childwin.focus();
}
function add_target_list(item_id, select_emp_id, name) {
    var list = select_emp_id.split(",");
    
    if (list.length > 1) {
        alert("複数の職員が選択されています。");
        return false;
    }
    childwin.close();
    document.frmKintai.selEmpID.value = select_emp_id;
    document.frmKintai.action = "./kintai_vacation.php";
    document.frmKintai.method="POST";
    document.frmKintai.submit();

}

function pageReload(){

    sYMD = document.frmKintai.date_y1.value + document.frmKintai.date_m1.value + document.frmKintai.date_d1.value;
    sY = eval(document.frmKintai.date_y1.value);
    sM = eval(document.frmKintai.date_m1.value);
    sD = eval(document.frmKintai.date_d1.value);
    eYMD = document.frmKintai.date_y2.value + document.frmKintai.date_m2.value + document.frmKintai.date_d2.value;
    eY = eval(document.frmKintai.date_y2.value);
    eM = eval(document.frmKintai.date_m2.value);
    eD = eval(document.frmKintai.date_d2.value);
    //日付妥当性
    if(!(validate_date(sY, sM, sD))){
        alert("開始日付が不正です。");
        return false;
    }
    if(!(validate_date(eY, eM, eD))){
        alert("終了日付が不正です。");
        return false;
    }
    //範囲正当性
    if(sYMD > eYMD){
        alert("指定範囲が不正です。");
        return false;
    }
    
    document.frmKintai.action = "./kintai_vacation.php";
    document.frmKintai.method="POST";
    document.frmKintai.submit();
}
function print_xls(){

    document.frmKintai.action = "./kintai_vacation_xls.php";
    document.frmKintai.method="POST";
    document.frmKintai.submit();

}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initcal();">
<form name="frmKintai" method="post">
<?php
if ($umode == "duty") {
    require_once("duty_shift_common.ini");
    require_once("duty_shift_user_tab_common.ini");
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "&group_id=" . $group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
<?
}
//■■■■ユーザ画面ヘッダ■■■■■■■■■■■■
elseif($umode == "usr"){
    require_once("atdbk_menu_common.ini");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<?php
    if ($work_admin_auth == "1") {
?>
                    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<?php
    }
?>
                </tr>
            </table>
<?php
    // メニュー表示
    show_atdbk_menuitem($session, $fname, "");
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                </tr>
            </table>
            <table width="500" border="0" cellspacing="0" cellpadding="1">
                <tr>
                    <td height="22" colspan="2">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;&nbsp;
<?php
    $wk_title = "有給（年次）休暇";
    $wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=1\">{$wk_title}</a>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");

    $wk_title = "休暇取得状況";
    $wk_url = "<a href=\"atdbk_vacation.php?session={$session}&year={$year}&flg=2\">{$wk_title}</a>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");

    //$wk_title = "年次休暇簿";
    $wk_title = get_settings_value("kintai_vacation_paid_hol_title", "年次休暇簿"); // 年次有給休暇簿 20131210
    $wk_url = "<a href=\"kintai_vacation_paid_holiday.php?session={$session}&umode=usr\">{$wk_title}</a>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");

    //休暇簿　2012.04
    $wk_title = "休暇簿";
    $wk_url = "<b>{$wk_title}</b>";
    echo($wk_url);
    echo("&nbsp;&nbsp;");
?>
                        </font>
                    </td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
</table>
<?php
//■■■■管理者画面ヘッダ■■■■■■■■■■■■
}else if($umode == "adm"){
    require_once("work_admin_menu_common.ini");
    require_once("referer_common.ini");
    // 遷移元の設定
    if ($work != "") {
        set_referer($con, $session, "work", $work, $fname);
        $referer = $work;
    } else {
        $referer = get_referer($con, $session, "work", $fname);
    }
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
<?php
    if ($referer == "2") { 
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?php echo($session); ?>"><b>勤務管理</b></a></font></td>
                </tr>
            </table>
<?php 
    } else { 
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
                    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
                </tr>
            </table>
<?php
    }
    // メニュータブ
    show_work_admin_menuitem($session, $fname, "");
?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                </tr>
            </table>
            <img src="img/spacer.gif" alt="" width="1" height="5"><br>
    <?
    //サブメニュー表示
    show_work_admin_submenu($session,$fname,$arr_option);
?>
        </td>
    </tr>
</table>
<?php
}
?>

<table>
    <tr>
        <td>
            <select id="date_y1" name="date_y1">
                <?php show_select_years_kintai(10, $date_y1, true); ?>
            </select>/
            <select id="date_m1" name="date_m1">
                <?php show_select_months($date_m1, true); ?>
            </select>/
            <select id="date_d1" name="date_d1">
                <?php show_select_days($date_d1, true); ?>
            </select>
        </td>
        <td>
            <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
            <font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
                <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
            </font>
        </td>
        <td>
            &nbsp;〜&nbsp;
        </td>
        <td>
            <select id="date_y2" name="date_y2">
                <?php show_select_years_kintai(10, $date_y2, true, true); ?>
            </select>/
            <select id="date_m2" name="date_m2">
                <?php show_select_months($date_m2, true); ?>
            </select>/
            <select id="date_d2" name="date_d2">
                <?php show_select_days($date_d2, true); ?>
            </select>
        </td>
        <td>
            <img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
            <font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
                <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
            </font>
        </td>
        <td>
            &nbsp;&nbsp;
            <input type="button" value="表示" onclick="return pageReload();">&nbsp;&nbsp;
<?php
if($umode == "adm" || $umode == "duty"){
?>			
            <input type="button" value="職員選択" onclick="openEmployeeList(1);">&nbsp;&nbsp;
<?php
}
?>
            <input type="button" value="印刷" onclick="print_xls();">
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID：</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $base_info->emp_personal_id; ?></font></td>
        <td>&nbsp;&nbsp;</td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属：</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $shozoku; ?></font></td>
        <td>&nbsp;&nbsp;</td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種：</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $job_info->job_nm; ?></font></td>
        <td>&nbsp;&nbsp;</td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名：</font></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $base_info->emp_lt_nm."&nbsp;".$base_info->emp_ft_nm; ?></font></td>
    </tr>
</table>
<!--りすと-->
<table border="0" cellspacing="0" cellpadding="2" class="list">
<?php
if(MasterUtil::get_kintai_ptn() == 0){

?>
    <tr>
        <td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
        <td align="center" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
        <td align="center" width="120"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
        <td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
    </tr>
<?php
}else{
?>
    <tr>
        <td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
        <td align="center" width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
        <td align="center" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認</font></td>
        <td align="center" width="120"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
        <td align="center" width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
        <td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
        <td align="center" width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間数</font></td>
        <td align="center" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font></td>
        <td align="center" width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font></td>
    </tr>
<?php
}

for($i=0;$i<count($rData);$i++){
    if(MasterUtil::get_kintai_ptn() == 0){
?>
    <tr>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["date"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["wday"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["hol_reason"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["daycnt"]; ?></font></td>
    </tr>
<?php
    }else{
?>
    <tr>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["date"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["wday"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["sts"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["hol_reason"]; ?></font></td>
        <td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["hol_remark"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["daycnt"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["time"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["ap_date"]; ?></font></td>
        <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo $rData[$i]["kind"]; ?></font></td>
    </tr>
<?php
    }
}
?>
</table>

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">[事由別累計]</font><br>
<?php
for($i=0;$i<count($arrJiyu);$i++){
    echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>　".$arrJiyu[$i]."</font>";
}

if(MasterUtil::get_kintai_ptn() != 0){
?>
<br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">[種類別累計]</font><br>
<?php
    if(count($arrShurui) > 0){
        for($i=0;$i<count($arrShurui);$i++){
            echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>　".$arrShurui[$i]."</font>";
        }
    }
}
?>
<br>

<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="year" value="<?php echo($year); ?>">

<input type="hidden" name="umode" value="<?php echo($umode); ?>">
<input type="hidden" name="selEmpID" value="<?php echo($selEmpID); ?>">

<input type="hidden" name="p_emp_id" value="<?php echo($base_info->emp_id); ?>">

</form>
</body>
</html>
<?php
function show_select_years_kintai($num, $fix, $blank = false, $include_next = false) {
    if ($blank) {
        echo("<option value=\"-\">\n");
    }

    $now = date("Y");

    if ($include_next) {
        $num++;
        $now++;
    }

    if ($fix > 0) {
        $num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
    }

    for ($i = 0; $i < $num; $i++) {
        $yr = $now - $i;
        echo("<option value=\"$yr\"");
        if ($fix == $yr) {
            echo(" selected");
        }
        echo(">$yr</option>\n");
    }
}

?>