<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("show_nurse_list.ini");
require("referer_common.ini");
require("label_by_profile_type.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//診療科権限チェック
$dept = check_authority($session,28,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// メドレポート管理権限を取得
$summary_admin_auth = check_authority($session, 59, $fname);

// 事業所IDの設定
$enti_id = 1;

//DBコネクションの作成
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "entity", $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 病床管理/居室管理
$bed_manage_title = $_label_by_profile["BED_MANAGE"][$profile_type];

// 患者管理/利用者管理
$patient_manage_title = $_label_by_profile["PATIENT_MANAGE"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

// 担当看護師/担当者２
$nurse_title = $_label_by_profile["NURSE"][$profile_type];

// 担当看護師名/担当者２
$nurse_name_title = $_label_by_profile["NURSE_NAME"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);
?>
<title>CoMedix マスターメンテナンス | <?=$nurse_title?>一覧</title>
<script type="text/javascript">
function controlNurseList(img, trID) {
	var trDisplay;
	if (img.className == 'close') {
		img.className = 'open';
		trDisplay = '';
	} else {
		img.className = 'close';
		trDisplay = 'none';
	}

	var trs = document.getElementsByTagName('tr');
	for (var i = 0, j = trs.length; i < j; i++) {
		if (trs[i].id == trID) {
			trs[i].style.display = trDisplay;
		}
	}
}

function deleteNurse() {
	if (document.nu.elements['del_nurse[]'] == undefined) {
		alert('削除可能な<?=$nurse_title?>が存在しません');
		return;
	}

	if (document.nu.elements['del_nurse[]'].length == undefined) {
		if (!document.nu.elements['del_nurse[]'].checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	} else {
		var checked = false;
		for (var i = 0, j = document.nu.elements['del_nurse[]'].length; i < j; i++) {
			if (document.nu.elements['del_nurse[]'][i].checked) {
				checked = true;
				break;
			}
		}
		if (!checked) {
			alert('チェックボックスをオンにしてください。');
			return;
		}
	}

	if (confirm('削除してよろしいですか？')) {
		document.nu.submit();
	}
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
img.close {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/minus.gif");
	vertical-align:middle;
}
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "1") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="<? echo ($bed_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b><? echo ($bed_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($patient_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($patient_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="patient_info_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "3") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="summary_menu.php?session=<? echo($session); ?>"><img src="img/icon/b57.gif" width="32" height="32" border="0" alt="<? echo($med_report_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="summary_menu.php?session=<? echo($session); ?>"><b><? echo($med_report_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="summary_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>"><? echo ($section_title); ?></a></font></td>
</tr>
<? if ($ward_admin_auth == "1" && $referer == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>"><?=$ward_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
<? if ($patient_admin_auth == "1" && $referer == "2") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_admin_menu.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>
<? } ?>
<? if ($summary_admin_auth == "1" && $referer == "3") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_kubun.php?session=<? echo($session); ?>"><?=$summary_kubun_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="sum_ordsc_med_list.php?session=<? echo($session); ?>">処方・注射オーダ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_tmpl.php?session=<? echo($session); ?>">テンプレート</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_mst.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>

<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_access_log_daily.php?session=<? echo($session); ?>">アクセスログ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="summary_kanri_option.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>

<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#bdd1e7"><a href="entity_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo ($section_title); ?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="nurse_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$nurse_title?>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="section_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$section_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doctor_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="nurse_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$nurse_title?>登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteNurse();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form name="nu" action="nurse_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="15%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$section_title?></font></td>
<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$nurse_name_title?></font></td>
</tr>
<? show_nurse_list($con, $enti_id, @$st_id, $session, $fname); ?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="enti_id" value="<? echo($enti_id); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
