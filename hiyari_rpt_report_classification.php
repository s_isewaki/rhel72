<?php
// ◇コメントが一部文字化けしています
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_auth_models.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("hiyari_report_classification.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ( $summary == "0" ) {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ( $con == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//権限ユーザーフラグ
$is_inci_auth_emp_flg = is_inci_auth_emp($session,$fname,$con);

//更新権限(範囲無視)
$is_report_db_update_able = is_report_db_update_able($session,$fname,$con);

if($search_emp_class) $search_emp_class = explode(",", $search_emp_class);
if($search_emp_attribute) $search_emp_attribute = explode(",", $search_emp_attribute);
if($search_emp_dept) $search_emp_dept = explode(",", $search_emp_dept);
if($search_emp_room) $search_emp_room = explode(",", $search_emp_room);

//==============================
//特殊な画面呼び出しの場合の処理
//==============================

//トップ画面の評価予定日件数からの呼び出しの場合
if ( $called_from_other_page == "hiyari_menu.php" ) {
    //評価予定日検索をパラメータ「search_evaluation_date」で検索した状態で表示する。
    //報告部署検索をパラメータ「search_emp_class」等で検索した状態で表示するケースもあり。

    //検索欄はオープンしておく。
    $search_toggle_div = "open";

    //検索を実行しているため、日付検索エリアはなし。(全件対象：トップ画面の件数と一致させるため。)
    $search_date_mode = "non";

    //自動分類フラグをオフ
    $auto_group_flg = "0";

    //報告日の期間、3ヶ月以内を条件に追加
    if ( $search_torey_date_in_search_area_start == "" ) {
        // 評価期限が過ぎたものを検索する場合、期間のスタートを’2004/01/01’とする
        if ($search_evaluation_date == 'passed') { 
            // 報告月が過ぎたものを表示する場合は以下の値を入力する。
            $search_torey_date_in_search_area_start = '2004/01/01';
        }
        else {
            //3ヶ月前から当日を期間へ設定する
            $search_torey_date_in_search_area_start = date("Y/m/d", strtotime("-3 month"));
        }
        
        $search_torey_date_in_search_area_end = date("Y/m/d");
    }
}


//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
    $search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
    $search_date_month = date("m");
}

//検索時報告日デフォルト用
$search_default_date_start = date("Y/m/d", strtotime("-1 year"));
$search_default_date_end = date("Y/m/d");

//自動分類フラグ
if ($auto_group_flg == "") {
    $auto_group_flg = "0";
}

//==============================
//ユーザーID取得
//==============================
$sql = "SELECT * FROM empmst JOIN session USING (emp_id) WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp = pg_fetch_assoc($sel);
$emp_id = $emp["emp_id"];


//==============================
//トランザクション開始
//==============================
pg_query($con,"begin transaction");

//==============================
//処理モード共通情報
//==============================
//ログインユーザーの報告書の所属制限
$report_db_read_div = get_report_db_read_div($session,$fname,$con);


//割付利用権限
$priority_auth = get_emp_priority_auth($session,$fname,$con);
$is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
$is_report_classification_usable = $report_db_read_div == "0";
if($report_db_read_div >= 1 && $report_db_read_div <= 4 && $is_emp_auth_system_all)
{
    $is_report_classification_usable = true;
}

// 参照権限のみか、更新権限ありか
if (is_inci_auth_emp($session,$fname,$con) && $is_report_db_update_able) {
     //更新権限あり
    $is_update = true;
}
else{
    $is_update = false;
}


$rep_obj = new hiyari_report_class($con, $fname);

//==============================
//処理モードごとの処理
//==============================
if($is_postback == "true")
{

    switch ($mode)
    {
        case "folder_delete":

            //==============================
            //分類フォルダの削除
            //==============================
            if(!empty($folder_select))
            {
                $select_count=count($folder_select);
                for($i=0;$i<$select_count;$i++)
                {
                    if($folder_select[$i]!="" && $folder_select[$i]!="delete")
                    {
                        //フォルダID
                        $del_folder_id = $folder_select[$i];

                        //フォルダの物理削除
                        delete_classification_folder($con,$fname,$del_folder_id);

                        //選択中の分類フォルダを削除した場合は選択フォルダを「全て」にする。
                        if($del_folder_id == $folder_id)
                        {
                            $page = 1;
                            $folder_id = "";
                        }
                    }
                }

            }

            break;
        case "change_report_classification":

            //==============================
            //レポート分類変更
            //==============================
            $move_report_id = $change_report_classification_report_id;
            $from_folder_id = $folder_id;
            $to_folder_id = $change_report_classification_folder_id;

            //ゴミ箱フォルダが関与しない場合
            if($from_folder_id != "delete" && $to_folder_id != "delete")
            {
                //「全て」→「分類」：分類追加
                if($from_folder_id == "" && $to_folder_id != "")
                {
                    regist_report_classification($con,$fname,$to_folder_id,$move_report_id,"0");
                }
                //「分類」→「全て」：分類解除
                elseif($from_folder_id != "" && $to_folder_id == "")
                {
                    regist_report_classification($con,$fname,$from_folder_id,$move_report_id,"1");
                }
                //「分類」→「分類」：分類移動
                elseif($from_folder_id != "" && $to_folder_id != "")
                {
                    regist_report_classification($con,$fname,$from_folder_id,$move_report_id,"1");
                    regist_report_classification($con,$fname,$to_folder_id,$move_report_id,"0");


                }
                //「全て」→「全て」：ありえない
                else{}
            }
            //ゴミ箱フォルダが関与する場合
            else
            {
                //「全て」→「ゴミ箱」：論理削除
                if($from_folder_id == "" && $to_folder_id == "delete")
                {
                    if(!is_report_db_update_able_report($session,$fname,$con,$move_report_id))
                    {
                        ?>
                        <script type="text/javascript">
                        alert("更新権限が無いため削除できません。");
                        </script>
                        <?php
                    }
                    else
                    {
                        $rep_obj->del_report($move_report_id);
                    }
                }
                //「ゴミ箱」→「全て」：論理削除解除
                elseif($from_folder_id == "delete" && $to_folder_id == "")
                {
                    if(!is_report_db_update_able_report($session,$fname,$con,$move_report_id))
                    {
                        ?>
                        <script type="text/javascript">
                        alert("更新権限が無いため削除取消できません。");
                        </script>
                        <?php
                    }
                    else
                    {
                        $rep_obj->del_report_cancel($move_report_id);
                    }
                }
                //その他：ありえない
                else{}
            }

            break;
        case "delete":

            //==============================
            //レポート論理削除
            //==============================
            if(!empty($list_select))
            {
                $select_count=count($list_select);

                //チェック
                $is_ok = true;
                for($i=0;$i<$select_count;$i++)
                {
                    if(!$list_select[$i]=="")
                    {
                        //レポートID
                        $report_id = $list_select[$i];

                        if(!is_report_db_update_able_report($session,$fname,$con,$report_id))
                        {
                            $is_ok = false;
                            $ng_message = "更新権限が無いため削除できません。";
                            break;
                        }

                        $sql = "select * from inci_report_link where report_id = $report_id";
                        $sel = select_from_table($con,$sql,"",$fname);
                        if($sel == 0){
                            pg_query($con,"rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        //割付がある場合
                        if(pg_num_rows($sel) >0)
                        {
                            $is_ok = false;
                            $ng_message = "事案番号関連付けを行っているレポートは削除できません。";
                            break;
                        }

                    }
                }

                if($is_ok)
                {
                    //実行
                    for($i=0;$i<$select_count;$i++)
                    {
                        if(!$list_select[$i]=="")
                        {
                            //レポートID
                            $report_id = $list_select[$i];

                            //論理削除
                            $rep_obj->del_report($report_id);
                        }
                    }
                }
                else
                {
                    ?>
                    <script type="text/javascript">
                    alert("<?=$ng_message?>");
                    </script>
                    <?php
                }
            }
            break;
        case "delete_cancel":

            //==============================
            //レポート論理削除取消
            //==============================
            if(!empty($list_select))
            {
                $select_count=count($list_select);

                //チェック
                $is_ok = true;
                for($i=0;$i<$select_count;$i++)
                {
                    if(!$list_select[$i]=="")
                    {
                        //レポートID
                        $report_id = $list_select[$i];

                        if(!is_report_db_update_able_report($session,$fname,$con,$report_id))
                        {
                            $is_ok = false;
                            $ng_message = "更新権限が無いため削除取消できません。";
                            break;
                        }
                    }
                }

                if($is_ok)
                {
                    //実行
                    for($i=0;$i<$select_count;$i++)
                    {
                        if(!$list_select[$i]=="")
                        {
                            //レポートID
                            $report_id = $list_select[$i];

                            //論理削除取消
                            $rep_obj->del_report_cancel($report_id);
                        }
                    }
                }
                else
                {
                    ?>
                    <script type="text/javascript">
                    alert("<?=$ng_message?>");
                    </script>
                    <?php
                }
            }
            break;
        case "kill":
            $rep_obj->kill_report_all();

            break;
        default:
            break;
    }

    if($_POST['search_emp_all'] == 'true') {
        //担当範囲でシステム全体に対する担当者ではない場合
        $report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
        $priority_auth = get_emp_priority_auth($session,$fname,$con);
        $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
        if(!$report_db_read_auth_flg || !$is_emp_auth_system_all)
        {
            //検索初期桙ﾌ部署の条件をプルダウンの先頭と同じとする 20090317
            //権限w定なし／一般ユーザー：ユーザー所属の部署一覧
            if($priority_auth == "" || $priority_auth == "GU")
            {
                $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
            }
            //権限w定あり：担当者となる部署一覧
            else
            {
                //w定ユーザーが、w定権限で担当者となる部署の一覧を取得し、先頭の部署をg用
                //担当範囲
                if($report_db_read_auth_flg)
                {
                    $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
                }
                //所属範囲
                else
                {
                    $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
                }
            }

            foreach($emp_class_post as $key => $value) {
                $emp_class[$key] = $value["emp_class"];
                $emp_attribute[$key] = $value["emp_attribute"];
                $emp_dept[$key] = $value["emp_dept"];
                $emp_room[$key] = $value["emp_room"];
            }

            //==============================
            //報告ファイルの所属参照制限がある場合の所属検索条件
            //==============================

            //所属検索初期条件を設定
            if($report_db_read_div > 0) {
                $search_emp_class = $emp_class;
            }
            if($report_db_read_div > 1) {
                $search_emp_attribute = $emp_attribute;
            }
            if($report_db_read_div > 2) {
                $search_emp_dept = $emp_dept;
            }
            if($report_db_read_div > 3) {
                $search_emp_room = $emp_room;
            }
        }
    }

}
//==============================
//初回アクセス時の処理
//==============================
else
{
    $search_emp_all = true;

    //起動パラメータに指定されていない場合
    if($search_emp_class == "")
    {
        //担当範囲でシステム全体に対する担当者ではない場合
        $report_db_read_auth_flg = get_report_db_read_auth_flg($session,$fname,$con);
        $priority_auth = get_emp_priority_auth($session,$fname,$con);
        $is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);
        if(!$report_db_read_auth_flg || !$is_emp_auth_system_all)
        {
            //検索初期時の部署の条件をプルダウンの先頭と同じとする 20090317
            //権限指定なし／一般ユーザー：ユーザー所属の部署一覧
            if($priority_auth == "" || $priority_auth == "GU")
            {
                $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
            }
            //権限指定あり：担当者となる部署一覧
            else
            {
                //指定ユーザーが、指定権限で担当者となる部署の一覧を取得し、先頭の部署を使用
                //担当範囲
                if($report_db_read_auth_flg)
                {
                    $emp_class_post = get_emp_auth_class_post($emp_id, $priority_auth, $con, $fname);
                }
                //所属範囲
                else
                {
                    $emp_class_post = get_emp_class_post($emp_id, $con, $fname);
                }
            }

            foreach($emp_class_post as $key => $value) {
                $emp_class[$key] = $value["emp_class"];
                $emp_attribute[$key] = $value["emp_attribute"];
                $emp_dept[$key] = $value["emp_dept"];
                $emp_room[$key] = $value["emp_room"];
            }

            //==============================
            //報告ファイルの所属参照制限がある場合の所属検索条件
            //==============================
            //所属検索初期条件を設定
            if($report_db_read_div > 0) {
                $search_emp_class = $emp_class;
            }
            if($report_db_read_div > 1) {
                $search_emp_attribute = $emp_attribute;
            }
            if($report_db_read_div > 2) {
                $search_emp_dept = $emp_dept;
            }
            if($report_db_read_div > 3) {
                $search_emp_room = $emp_room;
            }
        }
    }
}


if ($search_date_target == "") {
    $search_date_target = "report_date";
}

if($_POST['search_evaluation_date_post'] !=''){
    $search_evaluation_date =  $_POST['search_evaluation_date_post'];
}
    
    
$search_info = array();
$search_info["search_date_target"] = $search_date_target;
$search_info["search_date_mode"] = $search_date_mode;
$search_info["search_date_year"] = $search_date_year;
$search_info["search_date_month"] = $search_date_month;
$search_info["search_date_ymd_st"] = $search_date_ymd_st;
$search_info["search_date_ymd_ed"] = $search_date_ymd_ed;
$search_info["search_evaluation_date"] = $search_evaluation_date;
$search_info["search_torey_date_in_search_area_start"] = $search_torey_date_in_search_area_start;
$search_info["search_torey_date_in_search_area_end"] = $search_torey_date_in_search_area_end;
$search_info["search_incident_date_start"] = $search_incident_date_start;
$search_info["search_incident_date_end"] = $search_incident_date_end;

$search_info["search_emp_class"] = $search_emp_class;
$search_info["search_emp_attribute"] = $search_emp_attribute;
$search_info["search_emp_dept"] = $search_emp_dept;
$search_info["search_emp_room"] = $search_emp_room;

$search_info["search_emp_all"] = $search_emp_all;

$search_info["search_emp_job"] = $search_emp_job;
$search_info["search_emp_name"] = $search_emp_name;
$search_info["search_patient_id"] = $search_patient_id;
$search_info["search_patient_name"] = $search_patient_name;
$search_info["search_patient_year_from"] = $search_patient_year_from;
$search_info["search_patient_year_to"] = $search_patient_year_to;
$search_info["search_toggle_div"] = $search_toggle_div;
$search_info["search_report_no"] = $search_report_no;

$search_info["search_title"] = $search_title;
$search_info["search_incident"] = $search_incident;
$search_info["title_and_or_div"] = $title_and_or_div;
$search_info["incident_and_or_div"] = $incident_and_or_div;

if ($called_from_other_page == "hiyari_menu.php" || $_POST['search_evaluation_date_post'] !=''){
    if ($priority_auth != 'GU'){
        $search_info["eval_auth"] = $priority_auth;
    }
}

//==============================
// 自動分類処理
//==============================
//
// ソート時、改ページ時は自動分類処理を行わない
// 自動分類フラグを条件追加
if ($mode != "sort" && $mode != "page_change") {
    // ユーザーが利用可能な分類条件設定されているフォルダの情報を取得
    $arr_auto_classification_folder = get_auto_classification_folder_info($con, $fname,$emp_id, "");

    //==================================================
    // ユーザーが利用可能なフォルダに自動追加されているレポートを全て分類削除します。
    //==================================================

    delete_all_folder_report($con, $fname, $arr_auto_classification_folder);
}


//==============================
//トランザクション終了
//==============================
pg_query($con, "commit");

if ($sort_div == "0") {
    $sort = "asc";
}
else {
    $sort = "desc";
}



$order = "order by main_report_id desc, report_link_status, report_id desc";
if ( $search_date_mode == "non" ) {
    if ( $sort_item == "INCIDENT_DATE" ) {
        $sort_item = "REPORT_NO";
    }
    else if ( $sort_item == "EVALUATION_DATE" && $search_evaluation_date == "" ) {
        $sort_item = "REPORT_NO";
    }
}

//件数取得
$cnt = get_report_count($con, $fname, "", $search_info);
$all_cnt = $cnt;

//権限ユーザーのみ
if ( $is_inci_auth_emp_flg && $is_report_db_update_able ) {
    $delete_cnt = get_report_count($con, $fname, "delete", $search_info);
}
if ( $folder_id != "" && $folder_id != "delete" ) {
    $folder_cnt = get_report_count($con, $fname, $folder_id, $search_info);
}

//page省略時は先頭ページを対象とする。
if ( $page == "" ) {
    $page = 1;
}
//一画面内の最大表示件数
$disp_count_max_in_page = 20;

// 報告書一覧取得
$list_data_array = get_report_data($con, $fname, $folder_id, $search_info, $page, $sort, $sort_item, $disp_count_max_in_page);

//==============================
//Excelリスト出力用フォルダー内レポート一覧情報
//==============================

$excel_target_report_id_list = "";
$excel_list_data_array = get_excel_data($con, $fname, $folder_id, $search_info,  $sort, $sort_item);
if ( $excel_list_data_array ) {
    foreach ( $excel_list_data_array as $list_data ) {
        $tmp_report_id =  $list_data["report_id"];
        if ( $excel_target_report_id_list != "" ) {
            $excel_target_report_id_list .= ",";
        }
        $excel_target_report_id_list .= $tmp_report_id;
    }
}

//==============================
//ページングに関する情報
//==============================

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
if ( $folder_id == "" ) {
    $rec_count = $all_cnt;
}
else if ($folder_id == "delete") {
    $rec_count = $delete_cnt;
}
else {
    $rec_count = $folder_cnt;
}
if ( $rec_count == 1 && $list_data_array[0] == "" ) {
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
    $page_max  = 1;
}
else
{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//インシレベルの表示
//==============================
$is_inci_level_disp = is_inci_level_disp($session,$fname,$con);

//==============================
//インシデント権限マスタ取得
//==============================
$arr_inci = get_inci_mst($con, $fname);

//==============================
//表示
//==============================
$page_title = "報告ファイル";

//レポートDB権限
$is_report_db_update_able = is_report_db_update_able($session,$fname,$con);

//------------------------------
//実行アイコン
//------------------------------
if($folder_id != "delete"){
    if($is_inci_auth_emp_flg && $is_report_db_update_able){
        $is_delete_usable = true;
    }
    else{
        $is_delete_usable = false;
    }
    $is_delete_cansel_usable = false;
    $is_kill_usable = false;
}
elseif( $folder_id == "delete"){
    $is_delete_usable = false;
    $is_delete_cansel_usable = true;
    //セーフティマネージャのみ、ゴミ箱を空にできる
    if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM")) {
        $is_kill_usable = true;
    } else {
        $is_kill_usable = false;
    }
}

$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$icon_disp = array();
$icon_disp['create'] = $arr_btn_flg["create_btn_show_flg"] == "t";
$icon_disp['return'] = false;
$icon_disp['forward'] = false;
$icon_disp['delete1'] = false;
$icon_disp['read_flg'] = false;
if ($folder_id=="delete"){
    $icon_disp['edit'] = false;
    $icon_disp['progress'] = false;
    $icon_disp['analysis'] = false;
    $icon_disp['analysis_register'] = false;
    $icon_disp['excel'] = false;
}
else{
    $icon_disp['edit'] = true;
    $icon_disp['progress'] = is_progres_editable($session,$fname,$con);
    $icon_disp['analysis'] = is_report_analysis_usable($session,$fname,$con);
    $icon_disp['analysis_register'] = (is_analysis_available($con, $fname) && is_analysis_update_usable($session,$fname,$con));
    $icon_disp['excel'] = $arr_btn_flg["excel_btn_show_flg"] == "t";
}
$icon_disp['delete2'] = $is_delete_usable;
$icon_disp['undelete'] = $is_delete_cansel_usable;
$icon_disp['kill'] = $is_kill_usable;
$icon_disp['search'] = TRUE;

//------------------------------
//分類フォルダ
//------------------------------
$folders = array();

//「全て」フォルダ
$is_common_folder = true;
$is_selected_this_folder = ($folder_id == "");
$folders[0]['td_bgcolor'] = get_folder_td_bgcolor($is_selected_this_folder);
$folders[0]['has_checkbox'] = false;
$folders[0]['id'] = "";
$folders[0]['name'] = "全て";
$folders[0]['count'] = $all_cnt;
$folders[0]['is_common'] = $is_common_folder;
$folders[0]['is_selected'] = $is_selected_this_folder;
$folders[0]['style'] = get_folder_style($is_selected_this_folder);
$folders[0]['image'] = get_folder_image($is_selected_this_folder,$is_common_folder);

//「ゴミ箱」フォルダ（権限ユーザーのみ）
if($is_inci_auth_emp_flg && $is_report_db_update_able){
    $is_common_folder = true;
    $is_selected_this_folder = ($folder_id == "delete");
    $folders[1]['td_bgcolor'] = get_folder_td_bgcolor($is_selected_this_folder);
    $folders[1]['has_checkbox'] = false;
    $folders[1]['id'] = "delete";
    $folders[1]['name'] = "ゴミ箱";
    $folders[1]['count'] = $delete_cnt;
    $folders[1]['is_common'] = $is_common_folder;
    $folders[1]['is_selected'] = $is_selected_this_folder;
    $folders[1]['style'] = get_folder_style($is_selected_this_folder);
    $folders[1]['image'] = 'img/icon/hiyari_lt_delete.gif';
}

//各フォルダ
$all_folder_info = get_all_classification_folder_info($con,$fname,$emp_id);
$i=2;
foreach($all_folder_info as $all_info){
    $is_common_folder = $all_info["common_flg"]=="t";
    $is_selected_this_folder = ($folder_id == $all_info["folder_id"]);

    // フォルダ内のレポート件数算出
    $this_folder_report_count = 0;
    if ($is_selected_this_folder) {
        $this_folder_report_count = $folder_cnt;
    } else {
        if ($auto_group_flg == "1") {
            $this_folder_report_count = get_report_count($con, $fname, $all_info["folder_id"], $search_info);
        }
    }

    $folders[$i]['td_bgcolor'] = get_folder_td_bgcolor($is_selected_this_folder);
    $folders[$i]['has_checkbox'] = true;
    $folders[$i]['id'] = $all_info["folder_id"];
    $folders[$i]['name'] = h($all_info["folder_name"]);
    $folders[$i]['count'] = $this_folder_report_count;
    $folders[$i]['is_common'] = $is_common_folder;
    $folders[$i]['is_selected'] = $is_selected_this_folder;
    $folders[$i]['style'] = get_folder_style($is_selected_this_folder);
    $folders[$i]['image'] = get_folder_image($is_selected_this_folder,$is_common_folder);
    $i++;
}

//------------------------------
//一覧タイトル
//------------------------------
if ($search_date_mode != "non"){
    if ($search_date_target == "incident_date"){
        $date_type = "INCIDENT_DATE";
        $date_name = "発生日";
    }
    elseif ($search_date_target == "evaluation_date"){
        $date_type = "EVALUATION_DATE";
        $date_name = "評価予定日";
    }
    else{
        $date_type = "REGIST_DATE";
        $date_name = "報告日";
    }
}
else{
    if ($search_evaluation_date == ""){
        $date_type = "REGIST_DATE";
        $date_name = "報告日";
    }
    else{
        $date_type = "EVALUATION_DATE";
        $date_name = "評価予定日";
    }
}

//------------------------------
//進捗表示
//------------------------------
$auth_list = array();
$progress_use = array();
$evaluation_use = array();  //担当者毎の評価機能利用フラグ
$is_evaluation_use = false; //評価機能を利用する担当者がいるかどうかのフラグ
if ($design_mode == 1){
    foreach($arr_inci as $arr_key => $arr){
        $progress_use[$arr_key] = is_usable_auth($arr_key,$fname,$con);
    }
}
else{
    $arr_inci_progress = search_inci_report_progress_dflt_flg($con, $fname);
    $num = pg_numrows($arr_inci_progress);
    for($i=0; $i<$num; $i++) {
        $auth = pg_result($arr_inci_progress,$i,"auth");
        $progress_use[$auth] = (pg_result($arr_inci_progress,$i,"use_flg") == 't');
        $evaluation_use[$auth] = ($sysConf->get('fantol.evaluation.use.' . strtolower($auth) . '.flg') == 't');
        if ($progress_use[$auth]){
            $auth_list[$auth]['auth_name'] = h(pg_result($arr_inci_progress,$i,"auth_short_name"));
            $auth_list[$auth]['eval_use'] = $evaluation_use[$auth];
        }
        if ($evaluation_use[$auth]){
            $is_evaluation_use = true;
        }
    }
}

//------------------------------
//ステータス
//------------------------------
$report_ids = '';
foreach($list_data_array as $i => $list_data){
    if ($i > 0){
        $report_ids.= ",";
    }
    $report_ids.= "'" . $list_data["report_id"] . "'";
}

if ($report_ids != '') {
    //評価ステータス
    $emp_auth = get_emp_priority_auth($session,$fname,$con);
    if ($design_mode == 2){
        if ($evaluation_use[$emp_auth]){
            $sql = "SELECT report_id, auth, evaluation_flg FROM inci_report_progress WHERE report_id IN ($report_ids)";
            $sel = select_from_table($con, $sql, "", $fname);
            $rows = pg_fetch_all($sel);
            foreach ($rows as $r){
                $evaluation_status[$r['report_id']][$r['auth']] = $r['evaluation_flg'];
            }
        }
    }

    //分析ステータス
    $sql = "SELECT analysis_problem_id FROM inci_analysis_regist WHERE available AND analysis_problem_id IN ($report_ids)";
    $sel = select_from_table($con, $sql, "", $fname);
    $rows = pg_fetch_all($sel);
    foreach ($rows as $r){
        $analysis_status[] = $r['analysis_problem_id'];
    }
}

//------------------------------
//一覧データ
//------------------------------
$profile_use = get_inci_profile($fname, $con);

//患者影響レベル略称情報を取得
$level_disp_list = get_inci_level_short_name($con,$fname);

//匿名表示
if ($design_mode == 1){
    $anonymous_name = h(get_anonymous_name($con,$fname));
}
else{
    $anonymous_name = '匿名';
}
$anonymous_setting = get_anonymous_setting($session, $con, $fname);
$mouseover_popup_base_flg = false;
if(is_inci_auth_emp_of_auth($session,$fname,$con,"SM") and !empty($anonymous_setting)) {
    // セーフティマネージャで一部匿名の場合 → 実名が見える
    $mouseover_popup_base_flg = true;
}

$list = array();
foreach($list_data_array as $i => $list_data){
    $report_id =  $list_data["report_id"];
    $list[$i]['report_id'] =  $report_id;
    $list[$i]['list_id'] = $report_id;
    $list[$i]['mail_id'] = $list_data["mail_id"];

    $list[$i]['checked'] =  false;
    if ($mode = 'change_report_classification'){
        if (in_array($report_id, $list_select)){
            $list[$i]['checked'] =  true;
        }
    }

    $list[$i]['report_link_id'] = $list_data["report_link_id"];
    $list[$i]['report_link_status'] = $list_data["report_link_status"];

    if ($design_mode == 1){
        if($folder_id == ""){
            $report_link_status = $list_data["report_link_status"];
            $little_report_img = get_little_report_img($report_link_status);
        }
        else{
            $report_link_status = 0;//全て単独レポートとして表示
            $little_report_img = get_little_report_img($report_link_status);
        }
    }
    
    //ステータス
    if ($design_mode == 2){
    $status[3]['flag'] = $rep_obj->get_sm_lock($report_id);
    $status[3]['name'] = 'ロック';

        if ($is_evaluation_use){
        if ($evaluation_use[$emp_auth]){
            $status[4]['flag'] = $evaluation_status[$report_id][$emp_auth] == 't';
        }
            else{
                $status[4]['flag'] = false;
            }
            $status[4]['name'] = '評価';
    }
    
        $status[5]['flag'] = in_array($report_id, $analysis_status);
    $status[5]['name'] = '分析';

    $list[$i]['status'] = $status;
    }
    
    //事案番号
    $list[$i]['report_no'] = $list_data["report_no"];
    $report_no_align = "left";
    if($report_link_status == "2"){
        $report_no_align = "right";
    }
    $list[$i]['report_no_align'] =  $report_no_align;
    $list[$i]['little_report_img'] =  $little_report_img;

    //表題
    $list[$i]['title'] = h($list_data["report_title"]);

    //送信時報告者職種
    $list[$i]['send_job_id'] = h($list_data["send_job_id"]);
    
    //送信時様式
    $list[$i]['send_eis_id'] = h($list_data["send_eis_id"]);
    
    //送信時様式No
    $list[$i]['eis_no'] = h($list_data["eis_no"]);


    //報告部署
    $class_nm = "";
    if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != ""){
        $class_nm .= $list_data["class_nm"];
    }
    if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != ""){
        $class_nm .= $list_data["atrb_nm"];
    }
    if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != ""){
        $class_nm .= $list_data["dept_nm"];
    }
    if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != ""){
        $class_nm .= $list_data["room_nm"];
    }
    $list[$i]['class_nm'] = $class_nm;
    
    
    ////報告者_id
    $list[$i]['registrant_id'] = h($list_data["registrant_id"]);
    
    //報告者
    $real_name = h($list_data["registrant_name"]);
    $list[$i]['real_name'] = $real_name;

    $anonymous_flg = $list_data["anonymous_report_flg"] == "t";
    $list[$i]['anonymous_flg'] = $anonymous_flg;

    if($anonymous_flg){
        $list[$i]['disp_emp_name'] = $anonymous_name;
    }
    else{
        $list[$i]['disp_emp_name'] = $real_name;
    }

    $mouseover_popup_flg = false;
    if($mouseover_popup_base_flg) {
        $mouseover_popup_flg = true;
    }
    else {
        foreach ($anonymous_setting as $auth => $data) {
            if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 0) {
                $mouseover_popup_flg = true;
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 1) {
                if ($list_data["registrant_class"] == $emp["emp_class"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 2) {
                if ($list_data["send_emp_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"]) {
                    $mouseover_popup_flg = true;
                }
            }
            else if ($data["report_db_use_flg"] == 't' and $data["report_db_read_div"] == 3) {
                if ($list_data["send_emp_class"] == $emp["emp_class"] and $list_data["registrant_attribute"] == $emp["emp_attribute"] and $list_data["registrant_dept"] == $emp["emp_dept"]) {
                    $mouseover_popup_flg = true;
                }
            }
            if ($data["report_db_read_auth_flg"] == 't') {
                if ($list_data["registrant_room"]) {
                    if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ][ $list_data["registrant_room"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
                else {
                    if ($data["rm"][ $list_data["registrant_class"] ][ $list_data["registrant_attribute"] ][ $list_data["registrant_dept"] ]) {
                        $mouseover_popup_flg = true;
                    }
                }
            }
        }
    }
    $list[$i]['mouseover_popup_flg'] = $mouseover_popup_flg;

    //日付
    $incident_date = $list_data["incident_date"];    //発生日
    $evaluation_date = $list_data["evaluation_date"]; //評価予定日
    $create_date =  $list_data["registration_date"];    //報告日
    if($search_date_mode != "non"){
        if($search_date_target == "incident_date"){
           $report_date = $incident_date;
        }
        elseif($search_date_target == "evaluation_date"){
            $report_date = $evaluation_date;
        }
        else{
            $report_date = $create_date;
        }
    }
    else{
        if($search_evaluation_date == ""){
            $report_date = $create_date;
        }
        else{
            $report_date = $evaluation_date;
        }
    }
    $list[$i]['report_date'] = $report_date;

    //影響
    $level = $list_data["level"];
    if($level != ""){
        $level = $level_disp_list[$level];
    }
    $list[$i]['level'] = $level;

    //進捗
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);

    $auth_info = array();
    
    foreach($arr_inci as $arr_key => $arr_val) {
        $progress_array = array();
        if ($progress_use[$arr_key]){
        
            $sql = "select * from inci_report_progress_detail";
            $cond = "where report_id = '$report_id' and auth = '$arr_key' and use_flg = 't'";
            //$cond = "where mail_id =". $mail_id. " and auth = '$arr_key' and use_flg = 't'";
            $result = select_from_table($con,$sql,$cond,$fname);
            $progress_array = pg_fetch_all($result);
            
            //$start_flg ='f';
            //$end_flg ='f';
            //$eval_flg = 'f';
            
            $auth_ = strtolower($arr_key) .'_start_flg';
            $start_flg = $list_data["$auth_"] ;
            
            $auth_ = strtolower($arr_key) .'_end_flg';
            $end_flg = $list_data["$auth_"] ;
                    
            $auth_ = strtolower($arr_key) .'_evaluation_flg';
            $eval_flg = $list_data["$auth_"] ;       
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    if($value['start_flg']=='t'){
                        $start_flg = 't';
                    }else{
                        $start_flg ='f';
                        break;
                    }
                }
            }
            
            $start_flg_individual = 'f';
            if($start_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    $sql = "select start_flg from inci_report_progress_detail";
                    $cond = " where report_id = '$report_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $start_flg_individual_tmp=pg_fetch_result($results, 0, "start_flg");
 
                    if($start_flg_individual_tmp=='t'){
                        $start_flg_individual = 't';
                    }else {
                        $start_flg_individual = 'f';
                    }
                }
            }       
            
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    if($value['end_flg']=='t'){
                        $end_flg = 't';
                    }else{
                        $end_flg ='f';
                        break;
                    }
                }
            }
            
            $end_flg_individual = 'f';
            if($end_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    $sql = "select end_flg from inci_report_progress_detail";
                    $cond = " where report_id = '$report_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $end_flg_individual_tmp=pg_fetch_result($results, 0, "end_flg");
                    
                    if($end_flg_individual_tmp=='t'){
                        $end_flg_individual = 't';
                    }else {
                        $end_flg_individual = 'f';
                    }
                }
            }
            
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    if($value['evaluation_flg']=='t'){
                        $eval_flg = 't';
                    }else{
                        $eval_flg ='f';
                        break;
                    }
                }
            }
            
            $eval_flg_individual = 'f';
            if($eval_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    $sql = "select evaluation_flg from inci_report_progress_detail";
                    $cond = " where report_id = '$report_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $eval_flg_individual_tmp=pg_fetch_result($results, 0, "evaluation_flg");
                    
                    if($eval_flg_individual_tmp=='t'){
                        $eval_flg_individual = 't';
                    }else {
                        $eval_flg_individual = 'f';
                    }
                }
            }
                   
             
            if($start_flg  == "t"){
                $accepted  ="1";
            }else if($start_flg_individual == "t"){
               $accepted  ="2";
            }else{
                $accepted  ="0";
            }
            
            if($end_flg  == "t"){
                $confirmed  ="1";
            }else if($end_flg_individual == "t"){
               $confirmed  ="2";
            }else{
                $confirmed  ="0";
            }
            
            if($eval_flg  == "t"){
                $evaluated  ="1";
            }else if($eval_flg_individual == "t"){
               $evaluated  ="2";
            }else{
                $evaluated  ="0";
            }
            
                      
            //$accepted  = ($start_flg  == "t");
            //$confirmed = ($end_flg   == "t");
            //$evaluated = ($eval_flg   == "t");
            
            switch ($arr_key){
                case "SM":
                    $use_flag  = ($list_data["sm_use_flg"]        == "t");
                    //$accepted  = ($list_data["sm_start_flg"]      == "t");
                    //$confirmed = ($list_data["sm_end_flg"]        == "t");
                    //$evaluated = ($list_data["sm_evaluation_flg"] == "t");
                    break;
                case "RM":
                    $use_flag  = ($list_data["rm_use_flg"]        == "t");
                    //$accepted  = ($list_data["rm_start_flg"]      == "t");
                    //$confirmed = ($list_data["rm_end_flg"]        == "t");
                    //$evaluated = ($list_data["rm_evaluation_flg"] == "t");
                    break;
                case "RA":
                    $use_flag  = ($list_data["ra_use_flg"]        == "t");
                    //$accepted  = ($list_data["ra_start_flg"]      == "t");
                    //$confirmed = ($list_data["ra_end_flg"]        == "t");
                    //$evaluated = ($list_data["ra_evaluation_flg"] == "t");
                    break;
                case "SD":
                    $use_flag  = ($list_data["sd_use_flg"]        == "t");
                    //$accepted  = ($list_data["sd_start_flg"]      == "t");
                    //$confirmed = ($list_data["sd_end_flg"]        == "t");
                    //$evaluated = ($list_data["sd_evaluation_flg"] == "t");
                    break;
                case "MD":
                    $use_flag  = ($list_data["md_use_flg"]        == "t");
                    //$accepted  = ($list_data["md_start_flg"]      == "t");
                    //$confirmed = ($list_data["md_end_flg"]        == "t");
                    //$evaluated = ($list_data["md_evaluation_flg"] == "t");
                    break;
                case "HD":
                    $use_flag  = ($list_data["hd_use_flg"]        == "t");
                    //$accepted  = ($list_data["hd_start_flg"]      == "t");
                    //$confirmed = ($list_data["hd_end_flg"]        == "t");
                    //$evaluated = ($list_data["hd_evaluation_flg"] == "t");
                    break;
                default:
            }
            $auth_info[$arr_key]['use'] = $use_flag;
            $auth_info[$arr_key]['accepted'] = $accepted;
            $auth_info[$arr_key]['confirmed'] = $confirmed;
            if ($design_mode == 2){
                if ($evaluation_use[$arr_key]){
                    $auth_info[$arr_key]['evaluated'] = $evaluated;
                }
            }
        }
    }
    $list[$i]["auth_info"] = $auth_info;

    //更新権限を取得
    $report_folder_update_flg = getReportFolderUpdateAuth($con, $session, $fname, $report_id);
    if ($report_folder_update_flg) {
        $list[$i]['report_torey_update_flg_value'] = '1';
    }
    else {
        $list[$i]['report_torey_update_flg_value'] = '0';
    }
}

include("hiyari_rpt_display.php");

//==============================
//DBコネクション終了
//==============================
pg_close($con);

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * 分類フォルダを削除します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $folder_id 削除するフォルダID
 */
function delete_classification_folder($con,$fname,$folder_id)
{
    //フォルダ削除
    $sql  = "delete from inci_classification_folder";
    $cond = "where folder_id = $folder_id";
    $result = delete_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //発生場面・内容の条件情報も削除
    $sql  = "delete from inci_classification_folder_400_condition";
    $cond = "where folder_id = $folder_id";
    $result = delete_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //レポートの所属情報削除
    $sql  = "delete from inci_report_classification";
    $cond = "where folder_id = $folder_id";
    $result = delete_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/**
 * ユーザーが利用可能なフォルダに自動追加されているレポートを全て分類削除します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param array $arr_auto_classification_folder フォルダ情報
 */
function delete_all_folder_report($con, $fname,$arr_auto_classification_folder)
{
    // フォルダID
    $folder_ids = "";
    foreach($arr_auto_classification_folder as $arr_folder_info)
    {
        if ($folder_ids != "") {
            $folder_ids .= ",";
        }
        $folder_ids .= $arr_folder_info["folder_id"];
    }
    // レポート削除処理 add_del_div(自動追加：2)
    $sql   = "delete from inci_report_classification";
    $cond  = "where inci_report_classification.add_del_div = '2' ";
    if ($folder_ids != "") {
        $cond  .= " and folder_id in ($folder_ids)";
    }

    $result = delete_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

//条件にあう報告書の件数を取得
function get_report_count($con, $fname, $folder_id, $search_info) {

    $cond_del_flg = ($folder_id != "delete") ? "not a.del_flag" : "a.del_flag";

    list ($cond_kikan, $arr_join_field) = get_kikan_cond($search_info);

    $sql   = "select count(*) as cnt ";
    $sql   .= " from inci_report a ";
    //職員
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";

    //進捗
    if ($search_info['eval_auth'] != ''){
        $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='" . $search_info['eval_auth'] . "') prg ON prg.report_id = a.report_id ";
    }
    
    //自動分類条件
    list ($cond_folder, $folder_join_field) = get_folder_cond($con, $fname, $folder_id);
    //join
    $arr_join_field = array_merge($arr_join_field, $folder_join_field);

    //検索条件
    $cond_search = "";
    if ($search_info["search_date_mode"] == "non") {
        list ($cond_search, $search_join_field) = get_search_cond($con,$fname,$search_info);

        $arr_join_field = array_merge($arr_join_field, $search_join_field);
    }

    $arr_join_field = array_unique($arr_join_field);

//echo("cnt=".count($arr_join_field));
//print_r($arr_join_field);

    $join_str = "";
//  foreach ($i=0; $i<count($arr_join_field); $i++) {
    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

//echo("join_str=$join_str");

    //期間条件
    if ($cond_kikan != "") {
        $cond_kikan = $cond_kikan." and ";
    }

    $cond  = " where $cond_kikan not a.shitagaki_flg and $cond_del_flg and not a.kill_flg ";
    $cond .= $cond_search;

    if ($folder_id != "" && $folder_id != "delete" && $cond_folder != "") {
        $cond .= $cond_folder;
//echo("join_str=$join_str");
//exit;
    }

    //評価済みを除く
    if ($search_info['eval_auth'] != ''){
        $cond .= " and not prg.evaluation_flg";
    }
    
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return pg_fetch_result($sel,0,"cnt");

}


//条件にあう報告書のデータを取得
function get_report_data($con, $fname, $folder_id, $search_info, $page, $sort, $sort_item, $disp_count_max_in_page) {

    $cond_del_flg = ($folder_id != "delete") ? "not a.del_flag" : "a.del_flag";

    list ($cond_kikan, $arr_join_field) = get_kikan_cond($search_info);

    //自動分類条件
    list ($cond_folder, $folder_join_field) = get_folder_cond($con, $fname, $folder_id);

    $arr_join_field = array_merge($arr_join_field, $folder_join_field);
    //検索条件
    $cond_search = "";
    if ($search_info["search_date_mode"] == "non") {
        list ($cond_search, $search_join_field) = get_search_cond($con,$fname,$search_info);
        $arr_join_field = array_merge($arr_join_field, $search_join_field);
    }

    $arr_join_field = array_unique($arr_join_field);

    $sql   = "select ";
    $sql   .= " a.report_id ";
    $sql   .= ",a.report_title ";
    $sql   .= ",a.registrant_id ";
    $sql   .= ",a.registrant_name ";
    $sql   .= ",a.registration_date ";
    $sql   .= ",a.del_flag ";
    $sql   .= ",a.report_no ";
    $sql   .= ",a.shitagaki_flg ";
    $sql   .= ",a.anonymous_report_flg ";
    $sql   .= ",a.registrant_class ";
    $sql   .= ",a.registrant_attribute ";
    $sql   .= ",a.registrant_dept ";
    $sql   .= ",a.registrant_room ";
    $sql   .= ",a.kill_flg ";
    $sql   .= ",a.incident_date ";
    $sql   .= ",a.evaluation_date ";
    $sql   .= ",e.emp_lt_nm || e.emp_ft_nm as registrant_name1 ";
    $sql   .= ",e.emp_kn_lt_nm || e.emp_kn_ft_nm as registrant_name2 ";
    $sql   .= ",l.report_link_id ";
    $sql   .= ",l.report_link_status ";
    $sql   .= ",l.main_report_id ";
//    $sql   .= ",l.main_report_id "; // ★★★20150617 重複
    $sql   .= ",classmst.class_nm ";
    $sql   .= ",atrbmst.atrb_nm ";
    $sql   .= ",deptmst.dept_nm ";
    $sql   .= ",classroom.room_nm ";
    $sql   .= ",a.inci_level AS level ";
    $sql   .= ",m.input_item_num_90_10 AS level_num ";
    $sql   .= ",p_SM.start_flg AS SM_start_flg ";
    $sql   .= ",p_SM.end_flg AS SM_end_flg ";
    $sql   .= ",p_SM.use_flg AS SM_use_flg ";
    $sql   .= ",p_SM.evaluation_flg AS SM_evaluation_flg ";
    $sql   .= ",p_RM.start_flg AS RM_start_flg ";
    $sql   .= ",p_RM.end_flg AS RM_end_flg ";
    $sql   .= ",p_RM.use_flg AS RM_use_flg ";
    $sql   .= ",p_RM.evaluation_flg AS RM_evaluation_flg ";
    $sql   .= ",p_RA.start_flg AS RA_start_flg ";
    $sql   .= ",p_RA.end_flg AS RA_end_flg ";
    $sql   .= ",p_RA.use_flg AS RA_use_flg ";
    $sql   .= ",p_RA.evaluation_flg AS RA_evaluation_flg ";
    $sql   .= ",p_SD.start_flg AS SD_start_flg ";
    $sql   .= ",p_SD.end_flg AS SD_end_flg ";
    $sql   .= ",p_SD.use_flg AS SD_use_flg ";
    $sql   .= ",p_SD.evaluation_flg AS SD_evaluation_flg ";
    $sql   .= ",p_MD.start_flg AS MD_start_flg ";
    $sql   .= ",p_MD.end_flg AS MD_end_flg ";
    $sql   .= ",p_MD.use_flg AS MD_use_flg ";
    $sql   .= ",p_MD.evaluation_flg AS MD_evaluation_flg ";
    $sql   .= ",p_HD.start_flg AS HD_start_flg ";
    $sql   .= ",p_HD.end_flg AS HD_end_flg ";
    $sql   .= ",p_HD.use_flg AS HD_use_flg ";
    $sql   .= ",p_HD.evaluation_flg AS HD_evaluation_flg ";
//    $sql   .= ",inci_mail_send_info.eis_id AS send_eis_id "; // ★★★20150617
//    $sql   .= ",inci_mail_send_info.job_id AS send_job_id "; // ★★★20150617
//    $sql   .= ",inci_mail_send_info.mail_id "; // ★★★20150617
//    $sql   .= ",set.eis_no AS eis_no "; // ★★★20150617


    $sql   .= " from inci_report a ";
    //職員
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";
    //関連付
    $sql .= " left join inci_report_link_view l on a.report_id = l.report_id ";
    //部署
    $sql .= " left join classmst on classmst.class_id = a.registrant_class ";
    $sql .= " left join atrbmst on atrbmst.atrb_id = a.registrant_attribute ";
    $sql .= " left join deptmst on deptmst.dept_id = a.registrant_dept ";
    $sql .= " left join classroom on classroom.room_id = a.registrant_room ";
    $join_str = "";

    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

    //影響レベル
    $sql .= " LEFT JOIN(";
    $sql .= "   SELECT easy_code AS input_item_90_10, easy_num AS input_item_num_90_10";
    $sql .= "   FROM inci_report_materials";
    $sql .= "   WHERE grp_code = 90 AND easy_item_code = 10";
    $sql .= " ) m ON a.inci_level = m.input_item_90_10";

    //進捗
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='SM') p_SM ON p_SM.report_id = a.report_id";
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='RM') p_RM ON p_RM.report_id = a.report_id";
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='RA') p_RA ON p_RA.report_id = a.report_id";
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='SD') p_SD ON p_SD.report_id = a.report_id";
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='MD') p_MD ON p_MD.report_id = a.report_id";
    $sql .= " LEFT JOIN (SELECT * FROM inci_report_progress WHERE auth='HD') p_HD ON p_HD.report_id = a.report_id";

    // 最新の様式 20150507
//     $sql .= " LEFT JOIN (SELECT inci_mail_send_info.report_id, eis_id, job_id from inci_mail_send_info inner join (select report_id, max(mail_id) from inci_mail_send_info group by report_id) b on inci_mail_send_info.mail_id = b.max) send_mail on send_mail.report_id = a.report_id ";

    // 最新の様式 ★★★20150617
//    $sql .= " LEFT JOIN inci_mail_send_info on inci_mail_send_info.mail_id = (select max(mail_id) from inci_mail_send_info where report_id = a.report_id group by report_id) ";
    
//    $sql .= " LEFT JOIN inci_easyinput_set set on inci_mail_send_info.eis_id = set.eis_id ";

    //期間条件
    if ($cond_kikan != "") {
        $cond_kikan = $cond_kikan." and ";
    }

    $cond  = " where $cond_kikan not a.shitagaki_flg and $cond_del_flg and not a.kill_flg ";
    $cond .= $cond_search;

    if ($folder_id != "" && $folder_id != "delete" && $cond_folder != "") {
        $cond .= $cond_folder;
    }

    //評価済みを除く
    if ($search_info['eval_auth'] != ''){
        $cond .= " and not p_" . $search_info['eval_auth'] . ".evaluation_flg";
    }

    $order = "";
    switch($sort_item)
    {
        case "REPORT_NO":
            $order = "order by a.report_no $sort";
            break;
        case "REPORT_TITLE":
            $order = "order by a.report_title $sort, a.report_id desc";
            break;
        case "CLASS":
            $order = "order by a.registrant_class $sort, a.registrant_attribute $sort , a.registrant_dept $sort , a.registrant_room, a.report_id desc";
            break;
        case "REGIST_NAME":
            $order = "order by a.anonymous_report_flg $sort, e.emp_kn_lt_nm || e.emp_kn_ft_nm $sort, a.report_id desc";
            break;
        case "REGIST_DATE":
            $order = "order by a.registration_date $sort, a.report_id desc";
            break;
        case "EVALUATION_DATE":
            $order = "order by a.evaluation_date $sort, a.report_id desc";
            break;
        case "INCIDENT_DATE":
            $order = "order by a.incident_date $sort, a.report_id desc";
            break;
        case "LEVEL":
            $order = "order by level_num $sort ,report_id desc";
            break;
        case "SM_START":
            $order = "order by sm_start_flg $sort, report_id desc";
            break;
        case "SM_END":
            $order = "order by sm_end_flg $sort, report_id desc";
            break;
        case "SM_EVAL":
            $order = "order by sm_evaluation_flg $sort, report_id desc";
            break;
        case "RM_START":
            $order = "order by rm_start_flg $sort, report_id desc";
            break;
        case "RM_END":
            $order = "order by rm_end_flg $sort, report_id desc";
            break;
        case "RM_EVAL":
            $order = "order by rm_evaluation_flg $sort, report_id desc";
            break;
        case "RA_START":
            $order = "order by ra_start_flg $sort, report_id desc";
            break;
        case "RA_END":
            $order = "order by ra_end_flg $sort, report_id desc";
            break;
        case "RA_EVAL":
            $order = "order by ra_evaluation_flg $sort, report_id desc";
            break;
        case "SD_START":
            $order = "order by sd_start_flg $sort, report_id desc";
            break;
        case "SD_END":
            $order = "order by sd_end_flg $sort, report_id desc";
            break;
        case "SD_EVAL":
            $order = "order by sd_evaluation_flg $sort, report_id desc";
            break;
        case "MD_START":
            $order = "order by md_start_flg $sort, report_id desc";
            break;
        case "MD_END":
            $order = "order by md_end_flg $sort, report_id desc";
            break;
        case "MD_EVAL":
            $order = "order by md_evaluation_flg $sort, report_id desc";
            break;
        case "HD_START":
            $order = "order by hd_start_flg $sort, report_id desc";
            break;
        case "HD_END":
            $order = "order by hd_end_flg $sort, report_id desc";
            break;
        case "HD_EVAL":
            $order = "order by hd_evaluation_flg $sort, report_id desc";
            break;
        default:
            //何もしない。
            $order = "order by l.main_report_id desc, l.report_link_status, a.report_id desc";
//          $order = "order by a.report_id desc";
            break;
    }

    // offset limit
    $offset = ($page - 1)*$disp_count_max_in_page;
    $cond .= " $order offset $offset limit $disp_count_max_in_page ";

    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    //return pg_fetch_all($sel);★★★20150617コメントアウト
    // ★★★20150617 上SQLでジョインせずに、ループ内で実施。orderbyやwhereが無いので、ここで問題ない。
    // ページあたり20件が初期だが、100件まではこちらのほうが早いはず。
    // これでも遅ければ、where report_id in ()に切替を推奨。
    $rows = pg_fetch_all($sel);
    foreach ($rows as $idx => $row) {
        $sql =
        " select".
        " si.eis_id as send_eis_id".
        ",si.job_id as send_job_id".
        ",si.mail_id".
        ",es.eis_no".
        " from inci_mail_send_info si".
        " inner join (".
        "     select max(mail_id) as max_mail_id from inci_mail_send_info where report_id = ".(int)$row["report_id"].
        " ) si2 on ( si2.max_mail_id = si.mail_id )".
        " left outer join inci_easyinput_set es on si.eis_id = es.eis_id";
        $sel2 = select_from_table($con,$sql,"",$fname);
        if($sel2 == 0){
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $row2 = pg_fetch_assoc($sel2);
        $rows[$idx]["send_eis_id"] = $row2["send_eis_id"];
        $rows[$idx]["send_job_id"] = $row2["send_job_id"];
        $rows[$idx]["mail_id"] = $row2["mail_id"];
        $rows[$idx]["eis_no"] = $row2["eis_no"];
    }
    return $rows;
}

//期間の条件を取得
//権限で見れる部署も条件に追加する
function get_kikan_cond($search_info) {

    $search_date_target = $search_info["search_date_target"];
    $search_date_mode = $search_info["search_date_mode"];
    $search_date_year = $search_info["search_date_year"];
    $search_date_month = $search_info["search_date_month"];
    $search_date_ymd_st = $search_info["search_date_ymd_st"];
    $search_date_ymd_ed = $search_info["search_date_ymd_ed"];
    $search_evaluation_date = $search_info["search_evaluation_date"];
    $search_torey_date_in_search_area_start = $search_info["search_torey_date_in_search_area_start"];
    $search_torey_date_in_search_area_end = $search_info["search_torey_date_in_search_area_end"];
    $search_incident_date_start = $search_info["search_incident_date_start"];
    $search_incident_date_end = $search_info["search_incident_date_end"];

    $search_emp_class = $search_info["search_emp_class"];
    $search_emp_attribute = $search_info["search_emp_attribute"];
    $search_emp_dept = $search_info["search_emp_dept"];
    $search_emp_room = $search_info["search_emp_room"];

    $search_title = $search_info["search_title"];
    $search_incident = $search_info["search_incident"];
    $title_and_or_div = $search_info["title_and_or_div"];
    $incident_and_or_div = $search_info["incident_and_or_div"];



    $cond = "";
    $join_field = array();

    //日付による絞込み(フィールド特定)
    if($search_date_target == "incident_date")
    {
        $search_date_field = "a.incident_date";
    }
    elseif($search_date_target == "evaluation_date")
    {
        $search_date_field = "a.evaluation_date";
    }
    else
    {
        $search_date_field = "a.registration_date";
    }

    //日付による絞込み
    if($search_date_mode == "month")
    {
        $search_date_1 = $search_date_year."/".$search_date_month;
        $cond .= " $search_date_field like '$search_date_1%' ";
    }
    elseif($search_date_mode == "ymd_st_ed")
    {
        if($search_date_ymd_st != "")
        {
            $cond .= " $search_date_field >= '$search_date_ymd_st' ";
        }
        if($search_date_ymd_ed != "")
        {
            if($search_date_ymd_st != "")
            {
                $cond .= " and ";
            }
            $cond .= " $search_date_field <= '$search_date_ymd_ed' ";
        }
    }

    //報告部署
    if($search_emp_class[0] != "" && $cond != "") {
        $cond .= " and ( ";
        $flag = false;
    } else if($search_emp_class[0] != ""){
        $cond .= " ( ";
    }
    foreach($search_emp_class as $key => $value) {
        if($value != "")
        {
            if ($cond != "" && $flag == false) {
                $cond .= " ( ";
                $flag = true;
            } else if($cond != ""){
                $cond .= " or ( ";
            } else {
                $cond .= " ( ";
            }
            $cond .= " a.registrant_class = $value ";
        }
        
        /* 北野病院用
        if($search_emp_room[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_room = $search_emp_room[$key] ";
        }
        
        else if($search_emp_dept[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_dept = $search_emp_dept[$key] ";
        }
        
        
        else if($search_emp_attribute[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_attribute = $search_emp_attribute[$key] ";
        }

*/
        
        if($search_emp_attribute[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_attribute = $search_emp_attribute[$key] ";
        }
        if($search_emp_dept[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_dept = $search_emp_dept[$key] ";
        }
        if($search_emp_room[$key] != "")
        {
            if ($cond != "") {
                $cond .= " and ";
            }
            $cond .= " a.registrant_room = $search_emp_room[$key] ";
        }
        if($search_emp_class[0] != "" && $cond != "") {
            $cond .= " ) ";
        }
    }
    if($search_emp_class[0] != "" && $cond != "") {
        $cond .= " ) ";
    }

    return array($cond, $join_field);
}

//検索条件を取得
function get_search_cond($con,$fname,$search_info) {

    $where = "";
    $join_field = array();

    $search_emp_class = $search_info["search_emp_class"];
    $search_emp_attribute = $search_info["search_emp_attribute"];
    $search_emp_dept = $search_info["search_emp_dept"];
    $search_emp_room = $search_info["search_emp_room"];
    $search_emp_job = $search_info["search_emp_job"];
    $search_emp_name = $search_info["search_emp_name"];
    $search_patient_id = $search_info["search_patient_id"];
    $search_patient_name = $search_info["search_patient_name"];
    $search_patient_year_from = $search_info["search_patient_year_from"];
    $search_patient_year_to = $search_info["search_patient_year_to"];
    $search_toggle_div = $search_info["search_toggle_div"];
    $search_report_no = $search_info["search_report_no"];
    $search_evaluation_date = $search_info["search_evaluation_date"];
    $search_torey_date_in_search_area_start = $search_info["search_torey_date_in_search_area_start"];
    $search_torey_date_in_search_area_end = $search_info["search_torey_date_in_search_area_end"];
    $search_incident_date_start = $search_info["search_incident_date_start"];
    $search_incident_date_end = $search_info["search_incident_date_end"];

    $search_title = $search_info["search_title"];
    $search_incident = $search_info["search_incident"];
    $title_and_or_div = $search_info["title_and_or_div"];
    $incident_and_or_div = $search_info["incident_and_or_div"];

    //報告部署
    foreach($search_emp_class as $key => $value) {
        if($key == 0 && $search_emp_class[0] != "") {
            $where .= " and ( ";
        } else if($search_emp_class[$key] != "") {
            $where .= " or ";
        }
        
        //北野病院
        if($search_emp_room[$key] != "")
        {
            $where .= " ( a.registrant_room = $search_emp_room[$key] ";
        }else if($search_emp_dept[$key] != "")
        {
            $where .= " ( a.registrant_dept = $search_emp_dept[$key] ";
        }
        else if($search_emp_attribute[$key] != "")
        {
            $where .= " ( a.registrant_attribute = $search_emp_attribute[$key] ";
        }
        else if($search_emp_class[$key] != "")
        {
            $where .= " ( a.registrant_class = $search_emp_class[$key] ";
        }
        
        
        
      /*  if($search_emp_class[$key] != "")
        {
            $where .= " ( a.registrant_class = $search_emp_class[$key] ";
        }
        if($search_emp_attribute[$key] != "")
        {
            $where .= " and a.registrant_attribute = $search_emp_attribute[$key] ";
        }
        if($search_emp_dept[$key] != "")
        {
            $where .= " and a.registrant_dept = $search_emp_dept[$key] ";
        }
        if($search_emp_room[$key] != "")
        {
            $where .= " and a.registrant_room = $search_emp_room[$key] ";
        }*/
        
        if($search_emp_class[0] != "") {
            $where .= " ) ";
        }
    }
    if($search_emp_class[0] != "") {
        $where .= " ) ";
    }
    if($search_emp_job != "")
    {
        $where .= " and e.emp_job = $search_emp_job ";
    }
    if($search_emp_name != "")
    {
        $search_emp_name2 = mb_ereg_replace(" ", "", pg_escape_string($search_emp_name));
        $search_emp_name2 = mb_ereg_replace("　", "", $search_emp_name2);

        $where .= " and ";
        $where .= " ( ";
        $where .= "   ( ";
        $where .= "     a.anonymous_report_flg ";
        $where .= "     and ";
        $where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
        $where .= "   ) ";
        $where .= "   or ";
        $where .= "   ( ";
        $where .= "     not a.anonymous_report_flg ";
        $where .= "     and  ";
        $where .= "       ( ";
        $where .= "            e.emp_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_kn_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or e.emp_kn_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or (e.emp_lt_nm || e.emp_ft_nm) like '%$search_emp_name2%' ";
        $where .= "         or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$search_emp_name2%' ";
        $where .= "       ) ";
        $where .= "   ) ";
        $where .= " ) ";
    }

    if($search_patient_id != "")
    {
        $search_patient_id2 = pg_escape_string($search_patient_id);
        $where .= " and a_210_10.input_item = '$search_patient_id2' ";
        $join_field[] = "210_10";
    }

    if($search_patient_name != "")
    {
        $search_patient_name2 =  pg_escape_string($search_patient_name);
        $where .= " and a_210_20.input_item like '%$search_patient_name2%' ";
        $join_field[] = "210_20";
    }

    if($search_patient_year_from == "900")
    {
        $where .= " and a_210_40.input_item = '$search_patient_year_from' ";
        $join_field[] = "210_40";
    }
    else
    {
        if($search_patient_year_from != "" && $search_patient_year_to != "")
        {
            $where .= " and a_210_40.input_item >= '$search_patient_year_from' ";
            $where .= " and a_210_40.input_item <= '$search_patient_year_to' ";
            $join_field[] = "210_40";
        }
        else if($search_patient_year_from != "" && $search_patient_year_to == "")
        {
            $where .= " and a_210_40.input_item >= '$search_patient_year_from' ";
            $where .= " and a_210_40.input_item < '900' ";
            $join_field[] = "210_40";
        }
        else if($search_patient_year_from == "" && $search_patient_year_to != "")
        {
            $where .= " and a_210_40.input_item <= '$search_patient_year_to' ";
            $join_field[] = "210_40";
        }
    }

    if($search_report_no != "")
    {
        $search_report_no2 = pg_escape_string($search_report_no);
        $where .= " and a.report_no like '{$search_report_no2}%' ";
    }

    //評価予定日
    if($search_evaluation_date != "")
    {
 
        switch ( $search_evaluation_date ) {
        
            // 評価日が過ぎた報告書を検索
            case 'passed':
                $date_max = date('Y/m/d', strtotime('-1 day'));
                $date_min = '2004/01/01';
                break;
            
            // 評価日が当日の報告書を検索
            case 'today':
                $date_max = date('Y/m/d');
                $date_min = $date_max;
                break;
            
            // 評価日が明日の報告書を検索
            case 'tomorrow':
                $date_max = date('Y/m/d', strtotime('+1 day'));
                $date_min = $date_max;
                break;
            
            // 評価日が一週間の報告書を検索
            case '1week':
                $date_max = date('Y/m/d', strtotime('+7 day'));
                $date_min = date('Y/m/d');
                break;
            
            // 評価日が二週間の報告書を検索
            case '2week':
                $date_max = date('Y/m/d', strtotime('+14 day'));
                $date_min = date('Y/m/d');
                break;
        }
        
        
        $where .= " and (a.evaluation_date >= '$date_min' and a.evaluation_date <= '$date_max')";
    }

    if($search_torey_date_in_search_area_start != "")
    {
        $where .= " and a.registration_date >= '$search_torey_date_in_search_area_start' ";
    }
    if($search_torey_date_in_search_area_end != "")
    {
        $where .= " and a.registration_date <= '$search_torey_date_in_search_area_end' ";
    }


    if($search_incident_date_start != "")
    {
        $where .= " and a.incident_date >= '$search_incident_date_start'";
    }
    if($search_incident_date_end != "")
    {
        $where .= " and a.incident_date <= '$search_incident_date_end'";
    }

    if($search_title != "") {
        $title_keyword = mb_ereg_replace("　"," ",$search_title); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($title_and_or_div == 1 || $title_and_or_div == "") {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " report_title like '%{$title_keyword_list[$i]}%' ";
        }
        $where .= " and ($wk_str) ";
    }

    if($search_incident != "") {
        $title_keyword = mb_ereg_replace("　"," ",$search_incident); //全角スペース除去
        $title_keyword = trim($title_keyword); //前後スペース除去
        $title_keyword_list = mb_split(" ", $title_keyword); //スペース区切り

        $wk_str = "";
        //マルチワードについては常にOR結合
        for ($i=0; $i<count($title_keyword_list); $i++) {
            if ($i > 0) {
                if($incident_and_or_div == 1 || $incident_and_or_div == "") {
                    $wk_str .= " and ";
                } else {
                    $wk_str .= " or ";
                }
            }
            $wk_str .= " a_520_30.input_item like '%{$title_keyword_list[$i]}%' ";
        }
        $where .= " and ($wk_str) ";
        $join_field[] = "520_30";
    }

    return array($where, $join_field);
}

//エクセル用に報告書IDを取得
function get_excel_data($con, $fname, $folder_id, $search_info, $sort, $sort_item) {

    $cond_del_flg = ($folder_id != "delete") ? "not a.del_flag" : "a.del_flag";

    list ($cond_kikan, $arr_join_field) = get_kikan_cond($search_info);

    //自動分類条件
    list ($cond_folder, $folder_join_field) = get_folder_cond($con, $fname, $folder_id);

    $arr_join_field = array_merge($arr_join_field, $folder_join_field);
    //検索条件
    $cond_search = "";
    if ($search_info["search_date_mode"] == "non") {
        list ($cond_search, $search_join_field) = get_search_cond($con,$fname,$search_info);
        $arr_join_field = array_merge($arr_join_field, $search_join_field);
    }

    $arr_join_field = array_unique($arr_join_field);

    $sql   = "select ";
    $sql   .= " a.report_id ";

    $sql   .= " from inci_report a ";
    //職員
    $sql .= " left join empmst e on e.emp_id = a.registrant_id ";
    //関連付
    $sql .= " left join inci_report_link_view l on a.report_id = l.report_id ";

    $join_str = "";
    foreach ($arr_join_field as $wk_i => $wk_join_field) {
        list ($gi_g, $gi_i) = split("_", $wk_join_field);
        $join_str .= "left join inci_easyinput_data a_{$gi_g}_{$gi_i} on a.eid_id = a_{$gi_g}_{$gi_i}.eid_id and a_{$gi_g}_{$gi_i}.grp_code = {$gi_g} and a_{$gi_g}_{$gi_i}.easy_item_code = {$gi_i} ";
    }
    $sql .= $join_str;

    //期間条件
    if ($cond_kikan != "") {
        $cond_kikan = $cond_kikan." and ";
    }

    $cond  = " where $cond_kikan not a.shitagaki_flg and $cond_del_flg and not a.kill_flg ";
    $cond .= $cond_search;

    if ($folder_id != "" && $folder_id != "delete" && $cond_folder != "") {
        $cond .= $cond_folder;
    }

    $order = "";
    switch($sort_item)
    {
        case "REPORT_NO":
            $order = "order by a.report_no $sort";
            break;
        case "REPORT_TITLE":
            $order = "order by a.report_title $sort, a.report_id desc";
            break;
        case "CLASS":
            $order = "order by a.registrant_class $sort, a.registrant_attribute $sort , a.registrant_dept $sort , a.registrant_room, a.report_id desc";
            break;
        case "REGIST_NAME":
            $order = "order by a.anonymous_report_flg $sort, e.emp_kn_lt_nm || e.emp_kn_ft_nm $sort, a.report_id desc";
            break;
        case "REGIST_DATE":
            $order = "order by a.registration_date $sort, a.report_id desc";
            break;
        case "EVALUATION_DATE":
            $order = "order by a.evaluation_date $sort, a.report_id desc";
            break;
        case "INCIDENT_DATE":
            $order = "order by a.incident_date $sort, a.report_id desc";
            break;
        default:
            $order = "order by l.main_report_id desc, l.report_link_status, a.report_id desc";
            break;
    }

    $cond .= " $order ";

    $sel = select_from_table($con,$sql,$cond,$fname);
    if ($sel == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    return pg_fetch_all($sel);
}

function get_folder_style($is_selected) {
    if($is_selected) {
        return 'style="color:#000000;font-weight:bold"';
    }
    else {
        return '';
    }
}
function get_folder_image($is_selected,$is_common_folder) {
    if($is_common_folder) {
        if($is_selected) {
            return 'img/icon/hiyari_lt_folder_open.gif';
        }
        else {
            return 'img/icon/hiyari_lt_folder.gif';
        }
    }
    else {
        if($is_selected) {
            return 'img/icon/hiyari_lt_folder2_open.gif';
        }
        else {
            return 'img/icon/hiyari_lt_folder2.gif';
        }
    }
}
function get_folder_td_bgcolor($is_selected) {
    if($is_selected) {
        return 'bgcolor="#FFDDFD"';
    }
    else {
        return '';
    }
}
