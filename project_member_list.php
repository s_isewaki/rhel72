<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 委員会詳細</title>
<?
require("about_authority.php");
require("about_session.php");
require("show_select_values.ini");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);
if ($pjt_admin_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);


// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

if ($schd_emp_id == "") {
	$schd_emp_id = $emp_id;
}

$arr_class_name = get_class_name_array($con, $fname);


$str_bgcolor_head = "#defafa"; //背景色(ヘッダ行)

$str_bgcolor1 = "#fadede"; //背景色1 委員会ＷGごとに交互に表示する
$str_bgcolor2 = "#fefcdf"; //背景色2 委員会ＷGごとに交互に表示する

// 組織1階層目の一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$classes = array();
while ($row = pg_fetch_array($sel)) {
	$classes[$row["class_id"]] = $row["class_nm"];
}

if($class_id !=null)
{
	// 組織2階層目の一覧を取得
	$sql = "select atrb_id, atrb_nm from atrbmst";
	$cond = "where class_id = $class_id and atrb_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$attributes = array();
	while ($row = pg_fetch_array($sel)) {
		$attributes[$row["atrb_id"]] = $row["atrb_nm"];
	}
}


if ($_SERVER["REQUEST_METHOD"]=="GET")
{ //GETで飛ばされてきたら以下を処理→画面遷移で呼ばれた場合
	
	//画面遷移で呼ばれた場合はデフォルトでチェックボックスをつける
	$ch_end_date = "1";
	
}

if($search_flg == "t")
{
	
	//出力用構造体
	$disp_data = array();
	$disp_cnt = -1;
	$color_number = 1;
	
	if($r_level == "5")
	{
		//責任者の検索の場合は別ＳＱＬにしました
		
		//検索条件にメンバー名が指定されている場合
		$query_name = " and   (rspemp.emp_lt_nm || rspemp.emp_ft_nm like '%".$search_rsp_user_name."%' or 
				rspemp.emp_kn_lt_nm || rspemp.emp_kn_ft_nm like '%".$search_rsp_user_name."%') ";
		
		if ($ch_end_date == "1") {
			$pjt_end_date = " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null) ";
		}
		else
		{
			$pjt_end_date = " ";
		}
		
		// 委員会,ワーキング情報を取得
		$sql = "select pjt_id as id, pjt_name as community, 
				CASE WHEN pjt_parent_id is null THEN  0 ELSE  1 END as working_flg, a.wg_sort_id,
				class_nm as struc1, atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm, pp.com_name
				from
				(select project.pjt_id, project.pjt_name, project.pjt_delete_flag, project.pjt_parent_id, project.wg_sort_id,
				project.class_id , project.atrb_id, project.pjt_response,  project.pjt_reg_emp_id,
				rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm
				from project 
				inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
				where pjt_delete_flag = 'f' "
			.$pjt_end_date.$query_name."  order by project.pjt_sort_id ,project.wg_sort_id ,project.class_id ,project.atrb_id ) a
				left join classmst on a.class_id = classmst.class_id
				left join atrbmst on a.atrb_id = atrbmst.atrb_id
				left join (select pjt_id as wid ,pjt_name as com_name, pjt_sort_id as sid from project) pp on a.pjt_parent_id = pp.wid
				";
		$cond = " where pjt_delete_flag = 'f' order by pp.sid,a.wg_sort_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		while ($row = pg_fetch_array($sel)) 
		{
			//委員会のメンバー情報を取得し、表示用配列に代入する
			
			$disp_cnt++;
			
			$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
			$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
			$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
			$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
			
			if($row["working_flg"] == "0")
			{
				//委員会名
				$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
				$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
			}
			else
			{
				//ワーキング名
				$disp_data[$disp_cnt]["community"] = $row["com_name"];//ワーキングが所属する委員会名
				$disp_data[$disp_cnt]["working"] = $row["community"];//ワーキング名
			}
			
			if($color_number % 2 ==0)
			{
				$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
			}
			else
			{
				$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
			}
			$color_number++;
			
		}
	}
	else
	{
		
		////////////////////////////////////////
		////////条件検索に設定するSQLを作成する
		////////////////////////////////////////
		
		if($ch_user_name == "1")
		{
			//検索条件にメンバー名が指定されている場合
			$query_name = " and   (empmst.emp_lt_nm || empmst.emp_ft_nm like '%".$search_user_name."%' or 
					empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like '%".$search_user_name."%') ";
		}
		else
		{
			//検索条件にメンバー名が指定されていない場合
			$query_name = " ";
		}
		
		if ($ch_end_date == "1") {
			$pjt_end_date = " and (project.pjt_real_end_date = '' or project.pjt_real_end_date is null) ";
		}
		else
		{
			$pjt_end_date = " ";
		}
		
		switch ($r_level) 
		{
			case "1"://条件指定なし
				$set_class1 = " ";
				$set_class2 = " ";
				$set_community = " ";
				$set_working = " ";
				break;
			
			case "2"://組織指定
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";	}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = " ";
				$set_working = " ";
				
				break;
			
			case "3":
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";	}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = "and project.pjt_name like '%".$search_project_name."%' ";
				$set_working = " ";
				
				break;
			
			case "4":
				if($class_id =="")//第一階層が指定されていない場合はSQLにis nullを記述
					{$set_class1 = "and project.class_id is null ";}
				else
					{$set_class1 = "and project.class_id='".$class_id."' ";}
				
				if($atrb_id =="")//第二階層が指定されていない場合はSQLにis nullを記述
					{$set_class2 = "and project.atrb_id is null ";}
				else
					{$set_class2 = "and project.atrb_id='".$atrb_id."' ";}
				
				$set_community = " ";
				$set_working = "and project.pjt_name like '%".$search_working_name."%' ";
				
				break;
			
			default:
				$set_class1 = " ";
				$set_class2 = " ";
				$set_community = " ";
				$set_working = " ";
				
				break;
		}
		
		////////////////////////////////////////
		////////作成したSQLを実行する
		////////////////////////////////////////
		
		// 委員会情報を取得

		$sql = "select a.pjt_id as id, pjt_name as community, pjt_parent_id as parent_id ,
				classmst.class_nm as struc1, atrbmst.atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm 
				from
				(select project.pjt_id, project.pjt_name, project.pjt_parent_id, project.pjt_delete_flag, 
				project.class_id , project.atrb_id, project.pjt_response, 
				rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm
				from project 
				inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
				where pjt_parent_id is null and pjt_delete_flag = 'f' "
			.$pjt_end_date.$set_class1.$set_class2.$set_community."  order by project.pjt_sort_id, project.class_id ,project.atrb_id ) a
				left join classmst on a.class_id = classmst.class_id
				left join atrbmst on a.atrb_id = atrbmst.atrb_id 
				";
				
		$cond = " where pjt_parent_id is null and pjt_delete_flag = 'f' order by struc1 desc, struc2 desc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == "0") {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		
		$www = pg_num_rows($sel);
		
		while ($row = pg_fetch_array($sel)) 
		{
			
			//委員会の議事録承認者を取得
			$sql = "select pjt_id, empmst.emp_lt_nm as ap_lt_nm, empmst.emp_ft_nm as ap_ft_nm 
                    from prcdaprv_default 
					left join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id
					where prcdaprv_default.pjt_id = ".$row["id"]."";
			$cond = "order by prcdaprv_order, prcdaprv_no";
			
			$sel_ap = select_from_table($con, $sql, $cond, $fname);
			if ($sel_ap == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			// メンバー情報を取得〜配列に格納(委員会)
			$sql = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, a.member_kind , 
					case a.member_kind when '1' then '事務局' else 'メンバー' end 
					from (select promember.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.member_kind
					from promember inner join empmst on promember.emp_id = empmst.emp_id 
					".$query_name."			
					where promember.pjt_id = '".$row["id"]."' ) a ";
			$cond = "";

			$sel2 = select_from_table($con, $sql, $cond, $fname);
			if ($sel2 == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			if($r_level < 4)//もし検索条件にＷＧ名が指定されていたら委員会情報は表示しない
			{
				if(pg_num_rows($sel2) == 0)//メンバーが一人も存在しなかった場合は委員会情報のみを表示する
				{
					
					if($ch_user_name != "1")//メンバーが存在していなくて、検索条件でメンバー名を指定していたら表示しない
					{
						
						$disp_cnt++;
						
						$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
						$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
						$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
						$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
						$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
						$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
						
						if($color_number % 2 ==0)
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
						}
						else
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
						}
						
						$ap_cnt = 0;
						while ($row_sel_ap = pg_fetch_array($sel_ap)) 
						{
							//承認者エリアに担当者名を設定
								
							if($ap_cnt == 0)
							{
								$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							else
							{
								$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							$ap_cnt++;
						}
						$color_number++;
					}
				}			
				else
				{
					//委員会のメンバー情報を取得し、表示用配列に代入する
					
					$disp_cnt++;
					
					$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
					$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
					$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
					$disp_data[$disp_cnt]["community"] = $row["community"];	//委員会名
					$disp_data[$disp_cnt]["working"] = "";	//ワーキング名
					$disp_data[$disp_cnt]["rsp_name"] = $row["rsp_lt_nm"].$row["rsp_ft_nm"];		//責任者
					
					//事務局メンバー数
					$secretariat_cnt = 0;

					//一般メンバー数
					$gen_member_cnt = 0;
					
					while ($row2 = pg_fetch_array($sel2)) 
					{
						//委員会のメンバー情報を取得し、表示用配列に代入する
						
						
						if($row2["case"] == "事務局")
						{
							//事務局エリアに担当者名を設定
							
							if($secretariat_cnt == 0)
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $disp_data[$disp_cnt]["sec_mem_name"].",".$row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							
							$secretariat_cnt++;
						}
						else
						{
							//メンバーエリアに担当者名を設定
							
							if($gen_member_cnt == 0)
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $disp_data[$disp_cnt]["gen_mem_name"].",".$row2["emp_lt_nm"].$row2["emp_ft_nm"];		//担当者
							}
							$gen_member_cnt++;
						}
					}
					
					$ap_cnt = 0;
					while ($row_sel_ap = pg_fetch_array($sel_ap)) 
					{
						//承認者エリアに担当者名を設定
						
						if($ap_cnt == 0)
						{
							$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						else
						{
							$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						
						$ap_cnt++;
					}
					
					if($color_number % 2 ==0)
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
					}
					else
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
					}
					
				}
				
				if(pg_num_rows($sel2) > 0)
				{
					$color_number++;
				}
			}
			
			// 委員会に紐づいたWG情報を取得
			$sql = "select pjt_id as id, pjt_name as community, pjt_parent_id as parent_id ,
					classmst.class_nm as struc1, atrbmst.atrb_nm as struc2, pjt_response as res_id, rsp_lt_nm, rsp_ft_nm 
					from
					(select project.pjt_id, project.pjt_name, project.pjt_parent_id, project.pjt_delete_flag,  
					project.class_id , project.atrb_id, project.pjt_response,
					rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm, project.wg_sort_id
					from project 
					inner join empmst rspemp on project.pjt_response = rspemp.emp_id 
					where pjt_parent_id = '".$row["id"]."' and pjt_delete_flag = 'f' "
				.$pjt_end_date.$set_working." order by project.wg_sort_id, project.class_id ,project.atrb_id) a
					left join classmst on a.class_id = classmst.class_id
					left join atrbmst on a.atrb_id = atrbmst.atrb_id
					";
			$cond = " where pjt_delete_flag = 'f'";
			$sel3 = select_from_table($con, $sql, $cond, $fname);
			if ($sel3 == "0") {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			
			while ($row3 = pg_fetch_array($sel3)) 
			{

				//ワーキングの議事録承認者を取得
				$sql = "select pjt_id, empmst.emp_lt_nm as ap_lt_nm, empmst.emp_ft_nm as ap_ft_nm 
						from prcdaprv_default 
						left join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id
						where prcdaprv_default.pjt_id = ".$row3["id"]."";
				$cond = "order by prcdaprv_order, prcdaprv_no";
				
				$sel_ap = select_from_table($con, $sql, $cond, $fname);
				if ($sel_ap == "0") {
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
				

				//WK情報を取得し、表示用配列に代入する
				
				// メンバー情報を取得〜配列に格納(ＷＧ)
				$sql = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, a.member_kind , 
						case a.member_kind when '1' then '事務局' else 'メンバー' end 
						from (select promember.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.member_kind
						from promember inner join empmst on promember.emp_id = empmst.emp_id 
						".$query_name."			
						where promember.pjt_id = '".$row3["id"]."' ) a ";
				
				$cond = "";
				
				
				$sel4 = select_from_table($con, $sql, $cond, $fname);
				if ($sel4 == "0") {
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
				
				if(pg_num_rows($sel4) == 0)//メンバーが一人も存在しなかった場合は委員会情報のみを表示する
				{
					
					if($ch_user_name != "1")//メンバーが存在していなくて、検索条件でメンバー名を指定していたら表示しない
					{
						
						$disp_cnt++;
						
						$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
						$disp_data[$disp_cnt]["wgid"] = $row3["id"];		//ワーキングＩＤ
						$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
						$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
						$disp_data[$disp_cnt]["community"] =  $row["community"];	//委員会名
						$disp_data[$disp_cnt]["working"] = $row3["community"];	//ワーキング名
						$disp_data[$disp_cnt]["rsp_name"] = $row3["rsp_lt_nm"].$row3["rsp_ft_nm"];		//責任者
						
						if($color_number % 2 ==0)
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
						}
						else
						{
							$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
						}
						
						$ap_cnt = 0;
						while ($row_sel_ap = pg_fetch_array($sel_ap)) 
						{
							//承認者エリアに担当者名を設定
							
							if($ap_cnt == 0)
							{
								$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							else
							{
								$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
							}
							
							$ap_cnt++;
						}
						
						$color_number++;
					}
				}
				else
				{
					
					//委員会のメンバー情報を取得し、表示用配列に代入する
					
					$disp_cnt++;
					
					$disp_data[$disp_cnt]["comid"] = $row["id"];		//委員会ＩＤ
					$disp_data[$disp_cnt]["wgid"] = $row3["id"];		//ワーキングＩＤ
					$disp_data[$disp_cnt]["struc1"] = $row["struc1"];		//組織１
					$disp_data[$disp_cnt]["struc2"] = $row["struc2"];		//組織２
					$disp_data[$disp_cnt]["community"] =  $row["community"];	//委員会名
					$disp_data[$disp_cnt]["working"] = $row3["community"];	//ワーキング名
					$disp_data[$disp_cnt]["rsp_name"] = $row3["rsp_lt_nm"].$row3["rsp_ft_nm"];		//責任者

					//事務局メンバー数
					$secretariat_cnt = 0;
					
					//一般メンバー数
					$gen_member_cnt = 0;

					while ($row4 = pg_fetch_array($sel4)) 
					{
						
						//委員会のメンバー情報を取得し、表示用配列に代入する
						if($row4["case"] == "事務局")
						{
							//事務局エリアに担当者名を設定
							
							if($secretariat_cnt == 0)
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["sec_mem_name"] = $disp_data[$disp_cnt]["sec_mem_name"].",".$row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							
							$secretariat_cnt++;
						}
						else
						{
							//メンバーエリアに担当者名を設定
							
							if($gen_member_cnt == 0)
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							else
							{
								$disp_data[$disp_cnt]["gen_mem_name"] = $disp_data[$disp_cnt]["gen_mem_name"].",".$row4["emp_lt_nm"].$row4["emp_ft_nm"];		//担当者
							}
							
							$gen_member_cnt++;
						}
					}

					$ap_cnt = 0;
					while ($row_sel_ap = pg_fetch_array($sel_ap)) 
					{
						//承認者エリアに担当者名を設定
						
						if($ap_cnt == 0)
						{
							$disp_data[$disp_cnt]["approver"] = $row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						else
						{
							$disp_data[$disp_cnt]["approver"] = $disp_data[$disp_cnt]["approver"].",".$row_sel_ap["ap_lt_nm"].$row_sel_ap["ap_ft_nm"];	//承認者
						}
						
						$ap_cnt++;
					}
					
					if($color_number % 2 ==0)
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor1; //背景色
					}
					else
					{
						$disp_data[$disp_cnt]["bgcolor"] = $str_bgcolor2; //背景色
					}
				}
				
				if(pg_num_rows($sel4) > 0)
				{
					$color_number++;
				}
			}
		}
	}
}


?>
<?
require("project_emplist_caller_javascript.php");
insert_javascript();
?>
<script language="javascript">
<!--
function changeSelectClass() 
{
	document.pjt.submit();
}

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, "");
}

function startSearch() {

	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "r_level")
		{
		
			if(document.pjt.elements[i].checked)
			{
				switch (document.pjt.elements[i].value)
				{
					 case "3":
					
						if(document.pjt.search_project_name.value.trim() == "")
						{
							alert("委員会名を設定してください");
							return false;
						}

						break;

					 case "4":

						if(document.pjt.search_project_name.value.trim() == "")
						{
							alert("委員会名を設定してください");
							return false;
						}
						
						if(document.pjt.search_working_name.value.trim() == "")
						{
							alert("WG名を設定してください");
							return false;
						}
						
						break;

					 case "5":

						if(document.pjt.search_rsp_user_name.value.trim() == "")
						{
							alert("責任者を設定してください");
							return false;
						}
						
						break;

				}
			}
		}
		
		if(document.pjt.elements[i].name == "ch_user_name")
		{
			if(document.pjt.elements[i].checked)
			{
				if(document.pjt.search_user_name.value.trim() == "")
				{
					alert("事務局・メンバー名を設定してください")
					return false;
				}
			}
		}
	}

    pjtForm = document.getElementById("pjt");

    pjtForm.action = 'project_member_list.php';
    pjtForm.method = 'post';

	document.pjt.search_flg.value = 't';
	document.pjt.submit();
}

function fc_change_user_name()
{
	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "ch_user_name")
		{
			if(document.pjt.elements[i].checked)
			{
				//条件指定がチェックであれば有効にする

				document.pjt.search_user_name.disabled = false;
				document.pjt.search_user_name.value = "";
			}
			else
			{
				document.pjt.search_user_name.disabled = true;
				document.pjt.search_user_name.value = "";
			}
			break;
		}
	}
}


function fc_change_rsp_user_name()
{
	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "ch_rsp_user_name")
		{
			if(document.pjt.elements[i].checked)
			{
				//条件指定がチェックであれば有効にする

				document.pjt.search_rsp_user_name.disabled = false;
				document.pjt.search_rsp_user_name.value = "";
			}
			else
			{
				document.pjt.search_rsp_user_name.disabled = true;
				document.pjt.search_rsp_user_name.value = "";
			}
			break;
		}
	}
}


function fc_change_r_level()
{

	for(i = 0; i < document.pjt.elements.length; i++)
	{
		if(document.pjt.elements[i].name == "r_level")
		{
		
			if(document.pjt.elements[i].checked)
			{
				switch (document.pjt.elements[i].value)
				{
					 case "1":
						
						document.pjt.class_id.disabled = true;
						document.pjt.atrb_id.disabled = true;
						
						document.pjt.search_project_name.disabled = true;
						document.pjt.search_project_name.value = "";

						document.pjt.search_working_name.disabled = true;
						document.pjt.search_working_name.value = "";

						document.pjt.search_rsp_user_name.disabled = true;
						document.pjt.search_rsp_user_name.value = "";

						document.pjt.ch_user_name.disabled = false;
						document.pjt.search_user_name.disabled = true;
						document.pjt.search_user_name.value = "";
						
						break;

					 case "2":
						
						document.pjt.class_id.disabled = false;
						document.pjt.atrb_id.disabled = false;

						document.pjt.ch_user_name.disabled = false;
						document.pjt.search_user_name.disabled = true;
						document.pjt.search_user_name.value = "";
						
						document.pjt.search_project_name.disabled = true;
						document.pjt.search_project_name.value = "";

						document.pjt.search_working_name.disabled = true;
						document.pjt.search_working_name.value = "";
						
						document.pjt.search_rsp_user_name.disabled = true;
						document.pjt.search_rsp_user_name.value = "";

						break;


					 case "3":
						
						document.pjt.class_id.disabled = false;
						document.pjt.atrb_id.disabled = false;
						document.pjt.search_project_name.disabled = false;

						document.pjt.ch_user_name.disabled = false;
						document.pjt.search_user_name.disabled = true;
						document.pjt.search_user_name.value = "";

						document.pjt.search_working_name.disabled = true;
						document.pjt.search_working_name.value = "";
						
						document.pjt.search_rsp_user_name.disabled = true;
						document.pjt.search_rsp_user_name.value = "";

						break;

					 case "4":
						
						document.pjt.search_rsp_user_name.disabled = true;
						document.pjt.search_rsp_user_name.value = "";

						document.pjt.ch_user_name.disabled = false;
						document.pjt.search_user_name.disabled = true;
						document.pjt.search_user_name.value = "";

						document.pjt.class_id.disabled = false;
						document.pjt.atrb_id.disabled = false;
						document.pjt.search_project_name.disabled = false;

						document.pjt.search_working_name.disabled = false;
						
						break;

					 case "5":
						
						document.pjt.class_id.disabled = true;
						document.pjt.atrb_id.disabled = true;
						
						document.pjt.search_project_name.disabled = true;
						document.pjt.search_project_name.value = "";

						document.pjt.search_working_name.disabled = true;
						document.pjt.search_working_name.value = "";

						document.pjt.ch_user_name.disabled = true;
						document.pjt.search_user_name.value = "";

						for(i = 0; i < document.pjt.elements.length; i++)
						{
							if(document.pjt.elements[i].name == "ch_user_name")
							{
								document.pjt.elements[i].checked = false;
								break;
							}
						}
						document.pjt.search_user_name.disabled = true;
						document.pjt.search_user_name.value = "";

						document.pjt.search_rsp_user_name.disabled = false;
						document.pjt.search_rsp_user_name.value = "";

						break;
				}
			}
		}
	}
}


function clickDownLoad() {
    pjtForm = document.getElementById("pjt");

    pjtForm.action = 'project_member_list_excel.php';
    pjtForm.method = 'post';
    pjtForm.submit();
}


//-->
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.schdreg {border-collapse:collapse;}
.schdreg td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#5279a5 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<form name="pjt" id="pjt" action="project_member_list.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>
<? if ($pjt_admin_auth == "1") { ?>
<td width="85%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
<? }else{ ?>
<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_wg_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_member_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>委員会・WG名簿</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_bulk_set.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="120" height="22" align="center">&nbsp;</td></tr>
</table>

<table cellspacing="0" cellpadding="0" class="list1">
	<tr>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">条件指定</font></td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">条件内容</font></td>
	</tr>

	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">条件指定なし：</font>
			<input type="radio" name="r_level" id="r_level" value="1" 
			onclick="fc_change_r_level()" <? if(($r_level == "1")||($r_level == null)){echo("checked");} ?>/>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
	</tr>

	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<span><? echo($arr_class_name[0]); ?>：</span>
			<input type="radio" name="r_level" id="r_level" value="2" 
			onclick="fc_change_r_level()" <? if($r_level == "2"){echo("checked");} ?>/>
		</font>
		</td>
		<td align="left" bgcolor="#fefcdf">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="class_id" onchange="changeSelectClass();" <? if(($r_level < 2)||($r_level == 5)){echo("disabled");} ?> >
				<option value="">　　　　　</option>
					<?
					foreach ($classes as $tmp_class_id => $tmp_class_nm) {
						echo("<option value=\"$tmp_class_id\"");
						if ($tmp_class_id == $class_id) {
							echo(" selected");
						}
						echo(">$tmp_class_nm\n");
					}
					?>
			</select>
			<select name="atrb_id" <? if(($r_level < 2)||($r_level == 5)){echo("disabled");} ?>>
				<option value="">　　　　　</option>
					<?
					foreach ($attributes as $tmp_atrb_id => $tmp_atrb_nm) {
						echo("<option value=\"$tmp_atrb_id\"");
						if ($tmp_atrb_id == $atrb_id) {
							echo(" selected");
						}
						echo(">$tmp_atrb_nm\n");
					}
					?>
			</select>
		</font>
		</td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会名：</font>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="r_level" id="r_level" value="3" 
				onclick="fc_change_r_level()" <? if($r_level == "3"){echo("checked");} ?> />
			</font>
		</td>
		<td width="480" bgcolor="#fefcdf">
			<input name="search_project_name" type="text" size="120" maxlength="80" 
			value="<? echo($search_project_name); ?>" style="ime-mode: active;"  <? if(($r_level < 3)||($r_level == 5)){echo("disabled");} ?>>
		</td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG名：</font>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="r_level" id="r_level" value="4" 
				onclick="fc_change_r_level()" <? if($r_level == "4"){echo("checked");} ?> />
			</font>
		</td>
		<td width="480" bgcolor="#fefcdf">
			<input name="search_working_name" type="text" size="120" maxlength="80" 
			value="<? echo($search_working_name); ?>" style="ime-mode: active;" <? if(($r_level < 4)||($r_level == 5)){echo("disabled");} ?> >
		</td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">責任者名：</font>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="r_level" id="r_level" value="5" 
				onclick="fc_change_r_level()" <? if($r_level == "5"){echo("checked");} ?> />
			</font>
		</td>
		<td width="480" bgcolor="#fefcdf">
			<input name="search_rsp_user_name" type="text" size="60" maxlength="40" 
			value="<? echo($search_rsp_user_name); ?>" style="ime-mode: active;"  <? if($r_level != 5){echo("disabled");} ?>>
		</td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事務局・メンバー名：</font>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="checkbox" name="ch_user_name" id="ch_user_name" value="1" 
				onclick="fc_change_user_name()" <? if($ch_user_name == "1"){echo("checked");} ?>  <? if($r_level == 5){echo("disabled");} ?>  />
			</font>
		</td>
		<td width="480" bgcolor="#fefcdf">
			<input name="search_user_name" type="text" size="60" maxlength="40" 
			value="<? echo($search_user_name); ?>" style="ime-mode: active;"  <? if($ch_user_name != "1"){echo("disabled");} ?>>
		</td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了済みを表示しない：</font>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="checkbox" name="ch_end_date" id="ch_end_date" value="1" <? if($ch_end_date == "1"){echo("checked");} ?> />
			</font>
		</td>
		<td width="480" bgcolor="<?=$str_bgcolor_head?>"></td>
	</tr>
	<tr>
		<td align="right" bgcolor="<?=$str_bgcolor_head?>" colspan="2">
			<input type="button" name="search_btn" value="検索" style="margin-left:2em;width=5.5em;" onclick="startSearch();">
		</td>
	</tr>

	<tr>
<?
if(($search_flg == "t") && ($disp_cnt > -1))
{
?>
		<td colspan="2" align="right" bgcolor="<?=$str_bgcolor_head?>">

			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="submit" name="excel_btn" value="EXCEL出力" onclick="clickDownLoad()" >
			</font>
		</td>
	<?
}
?>
	</tr>


</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="120" height="22" align="center">&nbsp;</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list1">

<?
if($search_flg == "t")
{
	
	if($disp_cnt < 0)
	{
?>
	<tr>
		<td align="center" bgcolor="#C8FFF5">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索結果がありません</font>
		</td>
	<tr>
<?
	}
	else
	{
?>
	<tr>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織１</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織２</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ワーキング</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">責任者</font>
		</td>
<?	
		if($r_level != "5")//責任者検索の場合は以下の表示は必要なし 
		{
?>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事務局</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メンバー</font>
		</td>
		<td align="center" bgcolor="<?=$str_bgcolor_head?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議事録承認者</font>
		</td>



<?
		}
?>
	</tr>
<?
		for ($i = 0; $i <= $disp_cnt; $i++) 
		{
?>
	<tr>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["struc1"]?></font>
		</td>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["struc2"]?></font>
		</td>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<a href="project_update.php?session=<?=$session?>&adm_flg=t&pjt_id=<?=$disp_data[$i]["comid"]?>#P1">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["community"]?></font>
			</a>
		</td>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<a href="project_wg_update.php?session=<?=$session?>&adm_flg=t&pjt_id=<?=$disp_data[$i]["wgid"]?>#P1">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["working"]?></font>
			</a>
		</td>
		<td align="center" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["rsp_name"]?></font>
		</td>
<?	
			if($r_level != "5")//責任者検索の場合は以下の表示は必要なし 
			{
?>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["sec_mem_name"]?></font>
		</td>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["gen_mem_name"]?></font>
		</td>
		<td align="left" bgcolor="<?=$disp_data[$i]["bgcolor"]?>">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$disp_data[$i]["approver"]?></font>
		</td>


<?
			}
?>
	</tr>
<?
		}
	}
}
?>
</table>


<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" id="search_flg" name="search_flg" value="">
</form>

</body>
<? pg_close($con); ?>
</html>
