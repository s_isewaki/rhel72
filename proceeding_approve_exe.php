<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth_pjt = check_authority($session, 31, $fname);
$checkauth_aprv = check_authority($session, 7, $fname);
if ($checkauth_pjt == "1" || $checkauth_aprv == "1") {
	$checkauth = "1";
}
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($prcd_subject == "") {
	echo("<script type=\"text/javascript\">alert('議題を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($prcd_subject) > 200) {
	echo("<script type=\"text/javascript\">alert('議題が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($prcd_place_detail) > 100) {
	echo("<script type=\"text/javascript\">alert('場所（詳細）が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (!checkdate($prcd_month, $prcd_day, $prcd_year)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ("$prcd_start_hour$prcd_start_min" >= "$prcd_end_hour$prcd_end_min") {
	echo("<script type=\"text/javascript\">alert('終了時刻は開始時刻より後にしてください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

if($status_flg == "approve")
{
	//承認処理


	//権限並列（代理承認）の場合は対象の階層の承認者に承認日と代理フラグを立てる
	// 自分の承認階層と承認種別を取得する
	$sql = "select a.prcdaprv_emp_id, a.pjt_schd_id, prcdaprv_mng.next_notice_div, a.prcdaprv_order from prcdaprv_mng ,
			(select prcdaprv.pjt_schd_id, prcdaprv.prcdaprv_emp_id, prcdaprv.prcdaprv_order from prcdaprv
			where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id ='$emp_id') a ";
			
			
	$cond = "where prcdaprv_mng.prcdaprv_order = a.prcdaprv_order  and  prcdaprv_mng.pjt_schd_id = '$pjt_schd_id' ";
	$notice_sel = select_from_table($con, $sql, $cond, $fname);
	if ($notice_sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_order = pg_fetch_result($notice_sel, 0, "prcdaprv_order");
	$emp_notice = pg_fetch_result($notice_sel, 0, "next_notice_div");
	
	if($emp_notice == "2")
	{
		//権限並列（代理承認）の場合→対象の階層の承認者に承認日と代理フラグを立てる
		
		// 承認日をセット(自分用)
		$setvalue = array(date("Ymd"));
		$sql = "update prcdaprv set";
		$set = array("prcdaprv_date");
		$cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id = '$emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		

		// 承認日をセット(代理用)
		$setvalue = array(date("Ymd"),"t");
		$sql = "update prcdaprv set";
		$set = array("prcdaprv_date","substitute_flg");
		$cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id != '$emp_id' and prcdaprv_order='$emp_order'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}
	else
	{
		//権限同期（単独承認）の場合

		// 承認日をセット
		$setvalue = array(date("Ymd"));
		$sql = "update prcdaprv set";
		$set = array("prcdaprv_date");
		$cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id = '$emp_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	}
	
}
else
{
	//差戻し処理
	// 承認日に"00000000"をセット（差戻しとして判断する）
	$setvalue = array("00000000");
	$sql = "update prcdaprv set";
	$set = array("prcdaprv_date");
	$cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}


/**********承認/差戻コメント処理追加******************/

if(trim($prcd_aprv_comment) == "")
{
	$prcd_aprv_comment = "";
}
else
{
	$prcd_aprv_comment = nl2br($prcd_aprv_comment);

	// 承認者名を取得
	$sql = "select emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel_name = select_from_table($con, $sql, $cond, $fname);
	if ($sel_name == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_name = pg_fetch_result($sel_name, 0, "emp_lt_nm").pg_fetch_result($sel_name, 0, "emp_ft_nm");
	
	$prcd_aprv_comment = $prcd_aprv_comment."<br>"."<".date("Y/m/d")." : ".$emp_name.">";
}

//前回までの履歴があれば追加
if(trim($prcd_aprv_comment_display) != "")
{
	$prcd_aprv_comment = $prcd_aprv_comment_display."<br><br>".$prcd_aprv_comment;
}


/**********承認/差戻コメント処理追加******************/


// 承認情報を更新
$sql = "update proceeding set";
$set = array("prcd_subject", "prcd_place_id", "prcd_place_detail", "prcd_date", "prcd_start_time", "prcd_end_time", "prcd_person", "prcd_absentee", "prcd_summary", "prcd_aprv_comment");
$setvalue = array($prcd_subject, $prcd_place_id, $prcd_place_detail, "$prcd_year$prcd_month$prcd_day", "$prcd_start_hour$prcd_start_min", "$prcd_end_hour$prcd_end_min", $prcd_person, $prcd_absentee, p($prcd_summary), $prcd_aprv_comment);
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


// 承認者がすべて承認済みの場合、申請ステータスを「承認済」に変更
$sql = "update proceeding set";
$set = array("prcd_status");

if($status_flg == "approve")
{
	//承認処理
	$setvalue = array("2");
	$cond = "where pjt_schd_id = '$pjt_schd_id' and not exists (select * from prcdaprv where prcdaprv.pjt_schd_id = proceeding.pjt_schd_id and prcdaprv.prcdaprv_date is null)";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
}
else
{
	//差戻し処理
	$setvalue = array("3");
	$cond = "where pjt_schd_id = '$pjt_schd_id' ";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");	
	echo("<script language=\"javascript\">window.close();</script>\n");
}
