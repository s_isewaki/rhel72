<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>設備予約 | テンプレート一覧</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("show_facility.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteFcltmpl() {
	if (document.tmpl.elements['del_lib[]'] == undefined) {
		alert('削除可能なデータが存在しません。');
		return;
	}

	var checked = false;

	if (document.tmpl.elements['del_lib[]'] != undefined) {
		if (document.tmpl.elements['del_lib[]'].length == undefined) {
			if (document.tmpl.elements['del_lib[]'].checked) {
				checked = true;
			}
		} else {
			for (var i = 0, j = document.tmpl.elements['del_lib[]'].length; i < j; i++) {
				if (document.tmpl.elements['del_lib[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.tmpl.submit();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">

<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="facility_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>テンプレート一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteFcltmpl();"></td>

</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="tmpl" action="facility_tmpl_delete.php" method="post">
<? show_fcltmpl_list($con,$session,$fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_fcltmpl_list($con, $session, $fname) {

	// テンプレート一覧の取得
	$sql_lib = "select fcltmpl_id, fcltmpl_name from fcltmpl";
	$cond_lib = "where fcltmpl_del_flg = 'f' order by fcltmpl_id";
	$sel_lib = select_from_table($con, $sql_lib, $cond_lib, $fname);
	if ($sel_lib == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// 0件であれば何もせず復帰
	if (pg_num_rows($sel_lib) == 0) {
		return;
	}

	// テーブル出力開始
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" class=\"list\">\n");

	// 見出しを出力
	echo("<tr>\n");
	echo("<td width=\"6%\" align=\"center\">\n");
	echo("<br>\n");
	echo("</td>\n");
	echo("<td width=\"94%\">\n");

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"block_in\">\n");
	echo("<tr>\n");

	echo("<td>\n");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">テンプレート名</font>\n");
	echo("</td>\n");

	echo("</tr>\n");
	echo("</table>\n");

	echo("</td>\n");
	echo("</tr>\n");

	// 一覧をループ
	while ($row_lib = pg_fetch_array($sel_lib)) {

		// 行を出力
		$fcltmpl_id = $row_lib["fcltmpl_id"];
		$fcltmpl_name = $row_lib["fcltmpl_name"];

		echo("<tr>\n");
		echo("<td width=\"6%\" align=\"center\"><input type=\"checkbox\" name=\"del_lib[]\" value=\"$fcltmpl_id\"></td><td width=\"94%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"./facility_tmpl_update.php?session=$session&fcltmpl_id=$fcltmpl_id\">$fcltmpl_name</a></font></td>");
		echo("</tr>\n");

	}

	// テーブル出力終了
	echo("</table>\n");
}
?>
