<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | エリア登録</title>
<?php
require("about_authority.php");
require("about_session.php");
require("maintenance_menu_list.ini");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// エリア一覧を取得
$sql = "select area_id, area_name, order_no from area";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$areas = (pg_num_rows($sel) > 0) ? pg_fetch_all($sel) : array();
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
    .list {border-collapse:collapse;}
    .list td {border:#5279a5 solid 1px;}
</style>
</head>



<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr bgcolor="#f6f9ff">
            <td width="32" height="32" class="spacing">
                <a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a>
            </td>
            <td width="100%">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">
                    &nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>
                    &nbsp;&gt;&nbsp;<a href="class_menu.php?session=<? echo($session); ?>"><b>組織</b></a>
                </font>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <!-- left -->
        <td width="160" valign="top">
            <? write_menu_list($session, $fname) ?>
        </td>
        <!-- left -->

        <!-- center -->
        <td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
        <!-- center -->


        <!-- right -->
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr height="22">
                    <td width="60" align="center" bgcolor="#bdd1e7"><a href="class_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?>登録</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="attribute_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?>登録</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="department_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?>登録</font></a></td>
                    <? if ($arr_class_name["class_cnt"] == 4) { ?>
                        <td width="5">&nbsp;</td>
                        <td width="90" align="center" bgcolor="#bdd1e7"><a href="class_room_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?>登録</font></a></td>
                    <? } ?>
                    <td width="5">&nbsp;</td>
                    <td width="60" align="center" bgcolor="#bdd1e7"><a href="class_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織名</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="80" align="center" bgcolor="#5279a5"><a href="area_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>エリア登録</b></font></a></td>
                    <td>&nbsp;</td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                </tr>
            </table>

            <form name="regform" action="area_insert.php" method="post">
                <table width="550" border="0" cellspacing="0" cellpadding="2">
                    <tr height="22">
                        <td valign="bottom">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>登録</b></font>
                        </td>
                    </tr>
                </table>
                <table width="550" border="0" cellspacing="0" cellpadding="2" class="list">
                    <tr height="22">
                        <td width="60" bgcolor="#f6f9ff" align="right">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア名</font>
                        </td>
                        <td width="350">
                            <input type="text" name="area_name" value="" size="50" maxlength="30" style="ime-mode:active;">
                        </td>
                        <td bgcolor="#f6f9ff" align="right">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font>
                        </td>
                        <td>
                            <input type="text" name="order_no" value="" size="3" maxlength="2" style="ime-mode:disabled;">
                        </td>
                    </tr>
                </table>
                <table width="550" border="0" cellspacing="0" cellpadding="2">
                    <tr height="22">
                        <td align="right">
                            <input type="submit" value="登録">
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="session" value="<? echo($session); ?>">
            </form>


            <form name="delform" action="area_update.php" method="post">
                <table width="550" border="0" cellspacing="0" cellpadding="2">
                    <tr height="22">
                        <td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一覧</b></font></td>
                    </tr>
                </table>
                <? if (count($areas) == 0) { ?>
                    <table width="550" border="0" cellspacing="0" cellpadding="2">
                        <tr height="22">
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録されていません。</font></td>
                        </tr>
                    </table>
                <? } else { ?>
                    <table width="550" border="0" cellspacing="0" cellpadding="2" class="list">
                        <tr height="22" bgcolor="#f6f9ff" align="center">
                            <td width="60"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
                            <td width="350"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア名</font></td>
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font></td>
                        </tr>
                    <? foreach ($areas as $tmp_area) { ?>
                        <tr height="22">
                            <td align="center"><input type="checkbox" name="del_area_id_list[]" value="<? echo($tmp_area["area_id"]); ?>"><input type="hidden" name="area_id_list[]" value="<? echo($tmp_area["area_id"]); ?>"></td>
                            <td><input type="text" name="area_name_list[]" value="<? echo($tmp_area["area_name"]); ?>" size="50" maxlength="30" style="ime-mode:active;"></td>
                            <td align="center"><input type="text" name="order_no_list[]" value="<? echo($tmp_area["order_no"]); ?>" size="3" maxlength="2" style="ime-mode:disabled;"></td>
                        </tr>
                    <? } ?>
                </table>
                <table width="550" border="0" cellspacing="0" cellpadding="2">
                    <tr height="22">
                        <td align="right"><input type="submit" value="更新"></td>
                    </tr>
                </table>
                <input type="hidden" name="session" value="<? echo($session); ?>">
            </form>
            <? } ?>
        </td>
        <!-- right -->
    </tr>
</table>
</body>
<? pg_close($con); ?>
</html>
