<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("workflow_common.ini");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

$conf = new Cmx_SystemConfig();
$imprint_tab = $conf->get('apply.tabs.imprint');
$approve_field = $conf->get('apply.view.approve_field');
$title_label = $conf->get('apply.view.title_label');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 表示設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt; 
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; 
	<a href="workflow_notice_setting.php?session=<? echo($session); ?>"><b>オプション</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<? show_wkfw_menuitem($session, $fname, ""); ?>

<form action="workflow_screen_setting_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="65%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決済・申請のユーザ画面に印影登録タブを表示する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="imprint_tab" value="t"<? if ($imprint_tab != "f") {echo(" checked");} ?>>する
<input type="radio" name="imprint_tab" value="f"<? if ($imprint_tab == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="65%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認欄を上部に表示する</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="approve_field" value="T"<? if ($approve_field == "T") {echo(" checked");} ?>>する
<input type="radio" name="approve_field" value="B"<? if ($approve_field != "T") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="65%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題文言変更</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="title_label" value="1"<? if ($title_label != "2") {echo(" checked");} ?>>表題
<input type="radio" name="title_label" value="2"<? if ($title_label == "2") {echo(" checked");} ?>>標題
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
