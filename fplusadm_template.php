<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | ワークフロー更新</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("fplusadm_common.ini");
require_once("./conf/sql.inf");
require_once("fplus_common_class.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 部門一覧を取得
$sel_class = $obj->get_classmst();
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	$classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sel_atrb = $obj->get_atrbmst();

// 科一覧を取得
$sel_dept = $obj->get_deptmst();
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_dept_nm = $row["dept_nm"];
	$dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sel_st = $obj->get_stmst();

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);


// 職員ID取得
$emp_id = get_emp_id($con,$session,$fname);

// ログインユーザの職員情報を取得
$sql = "select emp_class, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = '$emp_id'";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_name = pg_fetch_result($sel_emp, 0, "emp_name");

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

// プレビュー押下時
if ($preview_flg == "1") {
// wkfw_content 保存
	if (strlen($wkfw_content) > 0) {
		$ext = ".php";
		$savefilename = "fplus/workflow/tmp/{$session}_t{$ext}";

		// 内容書き込み
		if (!$fp = fopen($savefilename, "w")) {
			echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
		if (!fwrite($fp, $wkfw_content, 2000000)) {
			echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		fclose($fp);

		$wkfw_title = str_replace("'","\'", $wkfw_title);
	}
}


if ($back != "t") {

	if($mode == "ALIAS")
	{
		$sql = "select * from fpluswkfwmst";
	}
	else
	{
		$sql = "select * from fpluswkfwmst_real";
	}

	$cond = "where wkfw_id = $wkfw_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$wkfw_type = pg_fetch_result($sel, 0, "wkfw_type");
	$wkfw_title = pg_fetch_result($sel, 0, "wkfw_title");
	$wkfw_content = pg_fetch_result($sel, 0, "wkfw_content");
	$start_date = pg_fetch_result($sel, 0, "wkfw_start_date");
	$end_date = pg_fetch_result($sel, 0, "wkfw_end_date");
	$wkfw_appr = pg_fetch_result($sel, 0, "wkfw_appr");
	$wkfw_content_type = pg_fetch_result($sel, 0, "wkfw_content_type");
	$wkfw_folder_id = pg_fetch_result($sel, 0, "wkfw_folder_id");

	$start_year = substr($start_date, 0, 4);
	$start_month = substr($start_date, 4, 2);
	$start_day = substr($start_date, 6, 2);
	$end_year = substr($end_date, 0, 4);
	$end_month = substr($end_date, 4, 2);
	$end_day = substr($end_date, 6, 2);


	$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
	$ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
	$ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");

	$short_wkfw_name = pg_fetch_result($sel, 0, "short_wkfw_name");
	$apply_title_disp_flg = pg_fetch_result($sel, 0, "apply_title_disp_flg");

	$send_mail_flg = pg_fetch_result($sel, 0, "send_mail_flg");
	$lib_reg_flg = pg_fetch_result($sel, 0, "lib_reg_flg");
	$lib_keyword = pg_fetch_result($sel, 0, "lib_keyword");
	$lib_no = pg_fetch_result($sel, 0, "lib_no");
	$lib_summary = pg_fetch_result($sel, 0, "lib_summary");
	$lib_archive = pg_fetch_result($sel, 0, "lib_archive");
	$lib_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");
	$lib_folder_id = pg_fetch_result($sel, 0, "lib_folder_id");
	$lib_show_login_flg = pg_fetch_result($sel, 0, "lib_show_login_flg");
	$lib_show_login_begin = pg_fetch_result($sel, 0, "lib_show_login_begin");
	$lib_show_login_end = pg_fetch_result($sel, 0, "lib_show_login_end");
	$lib_private_flg = pg_fetch_result($sel, 0, "lib_private_flg");
	$lib_ref_dept_st_flg = pg_fetch_result($sel, 0, "lib_ref_dept_st_flg");
	$lib_ref_dept_flg = pg_fetch_result($sel, 0, "lib_ref_dept_flg");
	$lib_ref_st_flg = pg_fetch_result($sel, 0, "lib_ref_st_flg");
	$lib_upd_dept_st_flg = pg_fetch_result($sel, 0, "lib_upd_dept_st_flg");
	$lib_upd_dept_flg = pg_fetch_result($sel, 0, "lib_upd_dept_flg");
	$lib_upd_st_flg = pg_fetch_result($sel, 0, "lib_upd_st_flg");
	$approve_label = pg_fetch_result($sel, 0, "approve_label");

	$lib_show_login_begin1 = substr($lib_show_login_begin, 0, 4);
	$lib_show_login_begin2 = substr($lib_show_login_begin, 4, 2);
	$lib_show_login_begin3 = substr($lib_show_login_begin, 6, 2);
	$lib_show_login_end1 = substr($lib_show_login_end, 0, 4);
	$lib_show_login_end2 = substr($lib_show_login_end, 4, 2);
	$lib_show_login_end3 = substr($lib_show_login_end, 6, 2);

	switch ($lib_archive) {
	case "1":  // 個人ファイル
	case "2":  // 全体共有
		$lib_private_flg1 = "";
		$lib_private_flg2 = "";
		break;
	case "3":  // 部署共有
		$lib_private_flg1 = $lib_private_flg;
		$lib_private_flg2 = "";
		break;
	case "4":  // 委員会・WG共有
		$lib_private_flg1 = "";
		$lib_private_flg2 = $lib_private_flg;
		break;
	}

	$wkfw_mode = ($mode == "REAL") ? 1 : 2;
	$sql = "select f.class_id, f.atrb_id, f.dept_id from fpluswkfwlibrefdept f left join classmst c on f.class_id = c.class_id left join atrbmst a on f.atrb_id = a.atrb_id left join deptmst d on f.dept_id = d.dept_id";
	$cond = "where f.wkfw_mode = $wkfw_mode and f.wkfw_id = $wkfw_id order by c.order_no, a.order_no, d.order_no";
	$sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
	if ($sel_ref_dept == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel_ref_dept)) {
		$tmp_class_id = $row["class_id"];
		$tmp_atrb_id = $row["atrb_id"];
		$tmp_dept_id = $row["dept_id"];
		$lib_ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
	}

	$sql = "select st_id from fpluswkfwlibrefst";
	$cond = "where wkfw_mode = $wkfw_mode and wkfw_id = $wkfw_id order by st_id";
	$sel_ref_st = select_from_table($con, $sql, $cond, $fname);
	if ($sel_ref_st == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel_ref_st)) {
		$tmp_st_id = $row["st_id"];
		$lib_ref_st[] = $tmp_st_id;
	}

	$sql = "select emp_id from fpluswkfwlibrefemp";
	$cond = "where wkfw_mode = $wkfw_mode and wkfw_id = $wkfw_id order by emp_id";
	$sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_ref_emp == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$target_id_list2 = "";
	$is_first_flg = true;
	while ($row = pg_fetch_array($sel_ref_emp)) {
		$tmp_emp_id = $row["emp_id"];
		if ($is_first_flg) {
			$is_first_flg = false;
		} else {
			$target_id_list2 .= ",";
		}
		$target_id_list2 .= $tmp_emp_id;
	}

	$sql = "select f.class_id, f.atrb_id, f.dept_id from fpluswkfwlibupddept f left join classmst c on f.class_id = c.class_id left join atrbmst a on f.atrb_id = a.atrb_id left join deptmst d on f.dept_id = d.dept_id";
	$cond = "where f.wkfw_mode = $wkfw_mode and f.wkfw_id = $wkfw_id order by c.order_no, a.order_no, d.order_no";
	$sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
	if ($sel_upd_dept == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel_upd_dept)) {
		$tmp_class_id = $row["class_id"];
		$tmp_atrb_id = $row["atrb_id"];
		$tmp_dept_id = $row["dept_id"];
		$lib_upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
	}

	$sql = "select st_id from fpluswkfwlibupdst";
	$cond = "where wkfw_mode = $wkfw_mode and wkfw_id = $wkfw_id order by st_id";
	$sel_upd_st = select_from_table($con, $sql, $cond, $fname);
	if ($sel_upd_st == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel_upd_st)) {
		$tmp_st_id = $row["st_id"];
		$lib_upd_st[] = $tmp_st_id;
	}

	$sql = "select emp_id from fpluswkfwlibupdemp";
	$cond = "where wkfw_mode = $wkfw_mode and wkfw_id = $wkfw_id order by emp_id";
	$sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_upd_emp == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$target_id_list3 = "";
	$is_first_flg = true;
	while ($row = pg_fetch_array($sel_upd_emp)) {
		$tmp_emp_id = $row["emp_id"];
		if ($is_first_flg) {
			$is_first_flg = false;
		} else {
			$target_id_list3 .= ",";
		}
		$target_id_list3 .= $tmp_emp_id;
	}

	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	// 利用権限
	// 部署
	if($ref_dept_flg == "2")
	{
		$arr_refdept = $obj->get_wkfw_refdept($wkfw_id, $mode);
		$ref_dept = array();
		foreach($arr_refdept as $dept)
		{
			$class_id = $dept["class_id"];
			$atrb_id  = $dept["atrb_id"];
			$dept_id  = $dept["dept_id"];

			$ref_dept[] = "$class_id-$atrb_id-$dept_id";
		}
	}

	// 役職
	if($ref_st_flg == "2")
	{
		$arr_refst = $obj->get_wkfw_refst($wkfw_id, $mode);
		$ref_st = array();
		foreach($arr_refst as $refst)
		{
			$ref_st[] = $refst["st_id"];
		}
	}

	// 職員
	$arr_refemp = $obj->get_wkfw_refemp($wkfw_id, $mode);
	$target_id_list1 = "";
	foreach($arr_refemp as $refemp)
	{
		if($target_id_list1 != "")
		{
			$target_id_list1 .= ",";
		}
		$target_id_list1 .= $refemp["emp_id"];
	}

	// 承認階層情報取得
	if($mode == "ALIAS")
	{
		$sql = "select * from fplusapvmng";
	}
	else
	{
		$sql = "select * from fplusapvmng_real";
	}
	$cond = "where wkfw_id = $wkfw_id order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "apv_div0_flg$i";
		$apv_div1_flg = "apv_div1_flg$i";
		$apv_div2_flg = "apv_div2_flg$i";
		$apv_div3_flg = "apv_div3_flg$i";
		$apv_div4_flg = "apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div2_flg .= pg_fetch_result($sel, $i - 1, "apv_div2_flg");
		$$apv_div3_flg .= pg_fetch_result($sel, $i - 1, "apv_div3_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		$apv_num = pg_fetch_result($sel, $i - 1, "apv_num");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// 委員会・ＷＧ
		if(pg_fetch_result($sel, $i - 1, "apv_div3_flg") == "t")
		{
			search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, $mode, $ret_arr);
		}

		// その他
		if(pg_fetch_result($sel, $i - 1, "apv_div2_flg") == "t")
		{
			set_aply_apv($apv_num, $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 委員会ＷＧ親ＩＤ
			$var_name = "pjt_parent_id$i";
			$$var_name .= $row["pjt_parent_id"];

			// 委員会ＷＧ親名称
			$var_name = "pjt_parent_nm$i";
			$$var_name .= $row["pjt_parent_nm"];

			// 委員会ＷＧ子ＩＤ
			$var_name = "pjt_child_id$i";
			$$var_name .= $row["pjt_child_id"];

			// 委員会ＷＧ子名称
			$var_name = "pjt_child_nm$i";
			$$var_name .= $row["pjt_child_nm"];

			// 部門ＩＤ
			$var_name = "class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

		$var_name = "apv_num$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "apv_num"));

	}

	// 結果通知管理情報取得
	$notice = "";
	$arr_wkfwnoticemng = $obj->get_wkfwnoticemng($wkfw_id, $mode);
	$rslt_ntc_div0_flg = $arr_wkfwnoticemng["rslt_ntc_div0_flg"];
	$rslt_ntc_div1_flg = $arr_wkfwnoticemng["rslt_ntc_div1_flg"];
	$rslt_ntc_div2_flg = $arr_wkfwnoticemng["rslt_ntc_div2_flg"];
	$rslt_ntc_div3_flg = $arr_wkfwnoticemng["rslt_ntc_div3_flg"];
	$rslt_ntc_div4_flg = $arr_wkfwnoticemng["rslt_ntc_div4_flg"];

	$notice_target_class_div = $arr_wkfwnoticemng["notice_target_class_div"];

	// 部署役職指定(結果通知)
	if($rslt_ntc_div0_flg == "t")
	{
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "0", $mode);
		if($notice_target_class_div != 0)
		{
			$arr_classname = $obj->get_classname();
			if($notice_target_class_div == "1")
			{
				$notice_target_class_name = $arr_classname[0]["class_nm"];
			}
			else if($notice_target_class_div == "2")
			{
				$notice_target_class_name = $arr_classname[0]["atrb_nm"];
			}
			else if($notice_target_class_div == "3")
			{
				$notice_target_class_name = $arr_classname[0]["dept_nm"];
			}
			else if($notice_target_class_div == "4")
			{
				$notice_target_class_name = $arr_classname[0]["room_nm"];
			}

			$notice .= "報告者の所属する【";
	 		$notice .=	$notice_target_class_name;
 			$notice .=	"】の";
		}
		else
		{
			if(count($arr_wkfwnoticestdtl) > 0)
			{
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$notice .=	"【".$in_hospital2_title."】の";
				}
			}
		}

		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_id != "")
			{
				$notice_st_id .= ",";
				$notice .= ", ";
			}
			$notice_st_id .= $wkfwnoticestdtl["st_id"];
			$notice .= $wkfwnoticestdtl["st_nm"];
		}
	}

	// 部署役職指定(結果通知)(部署指定)
	if($rslt_ntc_div4_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		// 部署
		$arr_wkfwnoticesectdtl = $obj->get_wkfwnoticesectdtl($wkfw_id, $mode);
		$notice_class_sect_id  = $arr_wkfwnoticesectdtl["class_id"];
		$notice_atrb_sect_id   = $arr_wkfwnoticesectdtl["atrb_id"];
		$notice_dept_sect_id   = $arr_wkfwnoticesectdtl["dept_id"];
		$notice_room_sect_id   = $arr_wkfwnoticesectdtl["room_id"];

		$notice_class_sect_nm = $arr_wkfwnoticesectdtl["class_nm"];
		$notice_atrb_sect_nm  = $arr_wkfwnoticesectdtl["atrb_nm"];
		$notice_dept_sect_nm  = $arr_wkfwnoticesectdtl["dept_nm"];
		$notice_room_sect_nm  = $arr_wkfwnoticesectdtl["room_nm"];

		$notice_all_class_nm = $notice_class_sect_nm;
		if($notice_all_class_nm != "")
		{
			if($notice_atrb_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_atrb_sect_nm;
			}

			if($notice_dept_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_dept_sect_nm;
			}

			if($notice_room_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_room_sect_nm;
			}
		}

		// 役職
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "4", $mode);

		$notice_st_sect_id = "";
		$notice_st_sect_nm = "";
		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_sect_id != "")
			{
				$notice_st_sect_id .= ",";
				$notice_st_sect_nm .= ", ";
			}
			$notice_st_sect_id .= $wkfwnoticestdtl["st_id"];
			$notice_st_sect_nm .= $wkfwnoticestdtl["st_nm"];
		}

		if($notice_all_class_nm != "" && $notice_st_sect_nm != "")
		{
			$notice .= $notice_all_class_nm."の".$notice_st_sect_nm;
		}
	}

	// 職員指定(結果通知)
	if($rslt_ntc_div1_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticedtl = $obj->get_wkfwnoticedtl($wkfw_id, $mode);
		foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
		{
			if($notice_emp_id != "")
			{
				$notice_emp_id .= ",";
				$notice_emp_nm .= ", ";
				$notice .= ", ";
			}
			$notice_emp_id .= $wkfwnoticedtl["emp_id"];
			$notice_emp_lt_nm = $wkfwnoticedtl["emp_lt_nm"];
			$notice_emp_ft_nm = $wkfwnoticedtl["emp_ft_nm"];

			$notice_emp_nm .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
			$notice .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
		}
	}

	// 委員会・ＷＧ指定(結果通知)
	if($rslt_ntc_div3_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticepjtdtl = $obj->get_wkfwnoticepjtdtl($wkfw_id, $mode);
		$notice_pjt_parent_id = $arr_wkfwnoticepjtdtl[0]["pjt_parent_id"];
		$notice_pjt_parent_nm = $arr_wkfwnoticepjtdtl[0]["pjt_parent_nm"];
		$notice_pjt_child_id  = $arr_wkfwnoticepjtdtl[0]["pjt_child_id"];
		$notice_pjt_child_nm  = $arr_wkfwnoticepjtdtl[0]["pjt_child_nm"];

		$notice .= $notice_pjt_parent_nm;
		if($notice_pjt_child_nm != "")
		{
			$notice .= " > ";
			$notice .= $notice_pjt_child_nm;
		}
	}

	// その他
	if($rslt_ntc_div2_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}
		$notice .= "報告時に結果通知者を指定する";

	}

	// 添付ファイル情報を取得
	if($mode == "ALIAS")
	{
		$sql = "select wkfwfile_no, wkfwfile_name from fpluswkfwfile";
	}
	else
	{
		$sql = "select wkfwfile_no, wkfwfile_name from fpluswkfwfile_real";
	}
	$cond = "where wkfw_id = $wkfw_id order by wkfwfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$file_id = array();
	$filename = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_file_no = $row["wkfwfile_no"];
		$tmp_filename = $row["wkfwfile_name"];

		array_push($file_id, $tmp_file_no);
		array_push($filename, $tmp_filename);

		// 一時フォルダにコピー
		$ext = strrchr($tmp_filename, ".");

		if($mode == "ALIAS")
		{
			copy("fplus/workflow/{$wkfw_id}_{$tmp_file_no}{$ext}", "fplus/workflow/tmp/{$session}_{$tmp_file_no}{$ext}");
		}
		else
		{
			copy("fplus/workflow/real/{$wkfw_id}_{$tmp_file_no}{$ext}", "fplus/workflow/tmp/{$session}_{$tmp_file_no}{$ext}");
		}
	}

	// 前提とする申請書取得
	$arr_wkfwfprecond = $obj->get_wkfwfprecond($wkfw_id, $mode);
	$precond_wkfw_id = array();
	$precond_wkfw_title = array();
	foreach($arr_wkfwfprecond as $wkfwfprecond)
	{
		array_push($precond_wkfw_id, $wkfwfprecond["precond_wkfw_id"]);
		array_push($precond_wkfw_title, $wkfwfprecond["wkfw_title"]);
	}

	// デフォルト値の設定
	$ref_dept_flg = ($ref_dept_flg == "") ? "1" : $ref_dept_flg;
	$ref_st_flg = ($ref_st_flg == "") ? "1" : $ref_st_flg;


//	if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
//	$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";

}

// メンバー情報を配列に格納
for ($list_idx=1; $list_idx<=3; $list_idx++) {
	$arr_target["$list_idx"] = array();
	$target_id_list_name = "target_id_list$list_idx";
	$target_id_list = $$target_id_list_name;
	if ($target_id_list != "") {
		$arr_target_id = split(",", $target_id_list);
		for ($i = 0; $i < count($arr_target_id); $i++) {
			$tmp_emp_id = $arr_target_id[$i];
			$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
			$cond = "where emp_id = '$tmp_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
			}
			$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
			array_push($arr_target["$list_idx"], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
		}
	}
}

// 30分以上前に保存されたファイルを削除
if (!is_dir("fplus")) {
	mkdir("fplus", 0755);
}
if (!is_dir("fplus/workflow")) {
	mkdir("fplus/workflow", 0755);
}
if (!is_dir("fplus/workflow/tmp")) {
	mkdir("fplus/workflow/tmp", 0755);
}
foreach (glob("fplus/workflow/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

if($mode == "ALIAS")
{
	// カテゴリ名
	if($wkfw_type != "")
	{
		$wkfl_mst = $obj->get_wkfwcate_mst($wkfw_type);
		$folder_path = $wkfl_mst["wkfw_nm"];
	}
	// フォルダ名
	if($wkfw_folder_id != "")
	{
		$folder_list = $obj->get_folder_path($wkfw_folder_id);
		foreach($folder_list as $folder)
		{
			if($folder_path != "")
			{
				$folder_path .= " > ";
			}
			$folder_path .= $folder["name"];
		}
	}

	if($folder_path == "")
	{
		$folder_path = "選択してください";
	}

	$folder_path = h($folder_path, ENT_QUOTES);
	$folder_path = str_replace("&#039;", "\'", $folder_path);
	$folder_path = str_replace("&quot;", "\"", $folder_path);
}

if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";

// 文書保存先書庫名を取得
$lib_archive_nm = lib_get_archive_name($lib_archive);

// 文書保存先カテゴリ名を取得
$lib_cate_nm = lib_get_category_name($con, $lib_cate_id, $fname);

// 文書保存先フォルダのパスを取得
$lib_folder_path = lib_get_folder_path($con, $lib_folder_id, $fname);

if ($lib_ref_dept_flg != "1" && $lib_ref_dept_flg != "2") {$lib_ref_dept_flg = "1";}
if ($lib_ref_st_flg != "1" && $lib_ref_st_flg != "2") {$lib_ref_st_flg = "1";}
if ($lib_upd_dept_flg != "1" && $lib_upd_dept_flg != "2") {$lib_upd_dept_flg = "1";}
if ($lib_upd_st_flg != "1" && $lib_upd_st_flg != "2") {$lib_upd_st_flg = "1";}

if ($lib_ref_toggle_mode == "") {$lib_ref_toggle_mode = "▼";}
$lib_ref_toggle_display = ($lib_ref_toggle_mode == "▼") ? "none" : "";
if ($lib_upd_toggle_mode == "") {$lib_upd_toggle_mode = "▼";}
$lib_upd_toggle_display = ($lib_upd_toggle_mode == "▼") ? "none" : "";
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
<?
require_once("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 700;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=16';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function initPage()
{
	<?
	if($mode == "ALIAS"){
	?>
    document.getElementById('folder_path').innerHTML = '<?=$folder_path?>';
	<?
	}
	?>

    setRefClassSrcOptions(true, '<? echo($ref_class_src); ?>', '<? echo($ref_atrb_src); ?>');
    setDisabled();
    update_target_html("1");

	update_target_html("2");
	update_target_html("3");
	onChangeLibraryArchive(true, '<? echo($lib_ref_class_src); ?>', '<? echo($lib_ref_atrb_src); ?>',  '<? echo($lib_upd_class_src); ?>', '<? echo($lib_upd_atrb_src); ?>');
}

function checkApprove(){

	var ref_dept_box = document.wkfw.ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}
	document.wkfw.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
	document.wkfw.action = "fplusadm_template.php";
	document.wkfw.submit();
}

function attachFile() {
	window.open('fplusadm_workflow_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function submitForm() {
	var circulation = document.wkfw.wkfw_appr[1].checked;
	if (circulation) {
		for (var i = 0, j = document.wkfw.elements.length; i < j; i++) {
			if (document.wkfw.elements[i].type == 'checkbox') {
				document.wkfw.elements[i].disabled = false;
			}
		}
	}

	var ref_dept_box = document.wkfw.ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}

	document.wkfw.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;
	document.wkfw.lib_ref_toggle_mode.value = document.getElementById('lib_ref_toggle').innerHTML;
	document.wkfw.lib_upd_toggle_mode.value = document.getElementById('lib_upd_toggle').innerHTML;

	var ref_dept_box = document.wkfw.lib_ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'lib_hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}

	var upd_dept_box = document.wkfw.lib_upd_dept;
	if (!upd_dept_box.disabled) {
		for (var i = 0, j = upd_dept_box.length; i < j; i++) {
			addHiddenElement(document.wkfw, 'lib_hid_upd_dept[]', upd_dept_box.options[i].value);
		}
	}

	document.wkfw.lib_ref_toggle_mode.value = document.getElementById('lib_ref_toggle').innerHTML;
	document.wkfw.lib_upd_toggle_mode.value = document.getElementById('lib_upd_toggle').innerHTML;

	closeEmployeeList();

	document.wkfw.submit();
}

function referTemplate() {
	window.open('fplusadm_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	document.wkfw.action = "fplusadm_template.php";
	document.wkfw.preview_flg.value = "1";
	document.wkfw.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=650,height=700";
	window.open(url, 'preview_window',option);
}

function openFolderSetting()
{
    var cate_id   = document.getElementById('wkfw_type').value;
    var folder_id = document.getElementById('wkfw_folder_id').value;
    window.open('fplusadm_folder_setting.php?session=<?=$session?>&screen_mode=workflow&selected_cate=' + cate_id + '&selected_folder=' + folder_id, 'newwin', 'width=640,height=700,scrollbars=yes');
}

function setCategory(folder_name, cate_id, folder_id)
{
    document.getElementById('folder_path').innerHTML = folder_name;
    document.getElementById('wkfw_type').value = cate_id;
    document.getElementById('wkfw_folder_id').value = folder_id;
}

// 前提とする申請書追加
function addPrecond()
{
	window.open('fplusadm_workflow_setting.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=600,scrollbars=yes');
}

// 前提とする申請書削除
function delPrecond(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_precond_', '');

	var p = document.getElementById('p_precond_' + id);
	document.getElementById('precond_wkfw').removeChild(p);

	sel_precond_wkfw_id = document.getElementById('sel_precond_wkfw_id').value;
	arr_sel_precond_wkfw_id = sel_precond_wkfw_id.split(",");
	tmp_precond_wkfw_id = "";
	for(i=0; i<arr_sel_precond_wkfw_id.length;i++)
	{
		if(arr_sel_precond_wkfw_id[i] != id)
		{
			if(tmp_precond_wkfw_id != "")
			{
				tmp_precond_wkfw_id += ",";
			}
			tmp_precond_wkfw_id += arr_sel_precond_wkfw_id[i];
		}
	}
	document.getElementById('sel_precond_wkfw_id').value = tmp_precond_wkfw_id;
}



var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("atrbs[$tmp_class_id] = [];\n");
	}

	echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

	$pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];
	$tmp_dept_nm = $row["dept_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("depts[$tmp_class_id] = {};\n");
	}

	if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
		echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
	}

	echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id, name: '$tmp_dept_nm'});\n");

	$pre_class_id = $tmp_class_id;
	$pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>




function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {
	var ref_class_src = document.getElementById('ref_class_src');

	if (!init_flg) {
		deleteAllOptions(document.getElementById('ref_dept'));
	}
	deleteAllOptions(ref_class_src);

	addOption(ref_class_src, '-', '----------', default_ref_class_src);
	for (var i = 0, len = classes.length; i < len; i++) {
		addOption(ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
	}

	setRefAtrbSrcOptions(default_ref_atrb_src);
}

function setRefAtrbSrcOptions(default_ref_atrb_src) {
	var ref_atrb_src = document.getElementById('ref_atrb_src');

	deleteAllOptions(document.getElementById('ref_atrb_src'));
	addOption(ref_atrb_src, '-', '----------', default_ref_atrb_src);

	var class_id = document.getElementById('ref_class_src').value;
	if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
		}
	}

	setRefDeptSrcOptions();
}

function setRefDeptSrcOptions() {
	var ref_dept_src = document.getElementById('ref_dept_src');
	deleteAllOptions(ref_dept_src);

	var class_id = document.getElementById('ref_class_src').value;
	var atrb_id = document.getElementById('ref_atrb_src').value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(ref_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(ref_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function deleteAllOptions(box)
{
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}


function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function setDisabled()
{
	var disabled;
	disabled = !(!document.getElementById('ref_dept_st_flg').disabled && !document.getElementById('ref_dept_st_flg').checked);

	var ref_dept_flg = document.getElementsByName('ref_dept_flg');
	ref_dept_flg[0].disabled = disabled;
	ref_dept_flg[1].disabled = disabled;

	disabled = !(!ref_dept_flg[1].disabled && ref_dept_flg[1].checked);

	document.getElementById('ref_class_src').disabled = disabled;
	document.getElementById('ref_atrb_src').disabled = disabled;
	document.getElementById('ref_dept_src').disabled = disabled;
	document.getElementById('ref_dept').disabled = disabled;
	document.getElementById('add_ref_dept').disabled = disabled;
	document.getElementById('delete_ref_dept').disabled = disabled;
	document.getElementById('delete_all_ref_dept').disabled = disabled;
	document.getElementById('select_all_ref_dept').disabled = disabled;

	disabled = (document.getElementById('ref_dept_st_flg').disabled || document.getElementById('ref_dept_st_flg').checked);

	var ref_st_flg = document.getElementsByName('ref_st_flg');
	ref_st_flg[0].disabled = disabled;
	ref_st_flg[1].disabled = disabled;

	disabled = !(!ref_st_flg[1].disabled && ref_st_flg[1].checked);
	document.getElementById('ref_st[]').disabled = disabled;
}

function addSelectedOptions(dest_box, src_box) {
	var options = new Array();
	for (var i = 0, j = dest_box.length; i < j; i++) {
		options[dest_box.options[i].value] = dest_box.options[i].text;
	}
	deleteAllOptions(dest_box);
	for (var i = 0, j = src_box.length; i < j; i++) {
		if (src_box.options[i].selected) {
			options[src_box.options[i].value] = src_box.options[i].text;
		}
	}

	for (var i in options) {
		addOption(dest_box, i, options[i]);
	}
}

function deleteSelectedOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		if (box.options[i].selected) {
			box.options[i] = null;
		}
	}
}

function selectAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = true;
	}
}

function toggle(button)
{
	var display;
	if (button.innerHTML == '▼') {
		button.innerHTML = '▲';
		display = '';
	} else {
		button.innerHTML = '▼';
		display = 'none';
	}
	document.getElementById('ref_toggle1').style.display = display;
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}

function clear_notice()
{
	document.getElementById('notice_content').innerHTML = '';
	document.getElementById('notice').value = '';

	document.getElementById('rslt_ntc_div0_flg').value = '';
	document.getElementById('rslt_ntc_div1_flg').value = '';
	document.getElementById('rslt_ntc_div2_flg').value = '';
	document.getElementById('rslt_ntc_div3_flg').value = '';
	document.getElementById('rslt_ntc_div4_flg').value = '';

	document.getElementById('notice_target_class_div').value = '';
	document.getElementById('notice_st_id').value = '';
	document.getElementById('notice_emp_id').value = '';
	document.getElementById('notice_emp_nm').value = '';
	document.getElementById('notice_pjt_parent_id').value = '';
	document.getElementById('notice_pjt_child_id').value = '';
	document.getElementById('notice_pjt_parent_nm').value = '';
	document.getElementById('notice_pjt_child_nm').value = '';

	document.getElementById('notice_class_sect_id').value = '';
	document.getElementById('notice_atrb_sect_id').value = '';
	document.getElementById('notice_dept_sect_id').value = '';
	document.getElementById('notice_room_sect_id').value = '';
	document.getElementById('notice_st_sect_id').value = '';
}

function addHiddenElement(frm, name, value) {
	var input = document.createElement('input');
	input.type = 'hidden';
	input.name = name;
	input.value = value;
	frm.appendChild(input);
}


//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
	for ($i=1; $i<=3; $i++) {
		$script = "m_target_list['$i'] = new Array(";
		$is_first = true;
		foreach($arr_target["$i"] as $row)
		{
			if($is_first)
			{
				$is_first = false;
			}
			else
			{
				$script .= ",";
			}
			$tmp_emp_id = $row["id"];
			$tmp_emp_name = $row["name"];
			$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
		}
		$script .= ");\n";
		print $script;
	}
?>

function setLibDetailDisplay(display) {
	document.getElementById('lib_detail').style.display = display;
}

function selectLibFolder() {
	var a = document.wkfw.lib_archive.value;
	var c = document.wkfw.lib_cate_id.value;
	var f = document.wkfw.lib_folder_id.value;
	window.open('library_folder_select.php?session=<? echo($session); ?>&a=' + a + '&c=' + c + '&f=' + f + '&path=W', 'newwin', 'width=640,height=700,scrollbars=yes');
}

function onChangeLibraryArchive(
	init_flg,
	default_ref_class_src,
	default_ref_atrb_src,
	default_upd_class_src,
	default_upd_atrb_src
) {
	setLibraryRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src);
	setLibraryUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src);
	setLibraryDisplay();
	setLibraryDisabled();
}

var LIB_ARCHIVE_ALL = '2';
var LIB_ARCHIVE_SECTION = '3';
var LIB_ARCHIVE_PROJECT = '4';
var LIB_ARCHIVE_PRIVATE = '1';

function setLibraryRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {
	if (!init_flg) {
		deleteAllOptions(document.wkfw.lib_ref_dept);
	}

	deleteAllOptions(document.wkfw.lib_ref_class_src);

	var archive = document.wkfw.lib_archive.value;
	if (archive == LIB_ARCHIVE_SECTION && document.wkfw.lib_private_flg1.checked) {
		var value = '<? echo($emp_class); ?>';
		for (var i = 0, len = classes.length; i < len; i++) {
			if (classes[i].id != value) continue;
			addOption(document.wkfw.lib_ref_class_src, value, classes[i].name);
		}
	} else {
		addOption(document.wkfw.lib_ref_class_src, '-', '----------', default_ref_class_src);
		for (var i = 0, len = classes.length; i < len; i++) {
			addOption(document.wkfw.lib_ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
		}
	}

	setLibraryRefAtrbSrcOptions(default_ref_atrb_src);
}

function setLibraryRefAtrbSrcOptions(default_ref_atrb_src) {
	deleteAllOptions(document.wkfw.lib_ref_atrb_src);

	addOption(document.wkfw.lib_ref_atrb_src, '-', '----------', default_ref_atrb_src);

	var class_id = document.wkfw.lib_ref_class_src.value;
	if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(document.wkfw.lib_ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
		}
	}

	setLibraryRefDeptSrcOptions();
}

function setLibraryRefDeptSrcOptions() {
	deleteAllOptions(document.wkfw.lib_ref_dept_src);

	var class_id = document.wkfw.lib_ref_class_src.value;
	var atrb_id = document.wkfw.lib_ref_atrb_src.value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(document.wkfw.lib_ref_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(document.wkfw.lib_ref_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function setLibraryUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src) {
	if (!init_flg) {
		deleteAllOptions(document.wkfw.lib_upd_dept);
	}

	deleteAllOptions(document.wkfw.lib_upd_class_src);

	var archive = document.wkfw.lib_archive.value;
	if (archive == LIB_ARCHIVE_SECTION) {
		var value = '<? echo($emp_class); ?>';
		for (var i = 0, len = classes.length; i < len; i++) {
			if (classes[i].id != value) continue;
			addOption(document.wkfw.lib_upd_class_src, value, classes[i].name);
		}
	} else {
		addOption(document.wkfw.lib_upd_class_src, '-', '----------', default_upd_class_src);
		for (var i = 0, len = classes.length; i < len; i++) {
			addOption(document.wkfw.lib_upd_class_src, classes[i].id, classes[i].name, default_upd_class_src);
		}
	}

	setLibraryUpdAtrbSrcOptions(default_upd_atrb_src);
}

function setLibraryUpdAtrbSrcOptions(default_upd_atrb_src) {
	deleteAllOptions(document.wkfw.lib_upd_atrb_src);

	addOption(document.wkfw.lib_upd_atrb_src, '-', '----------', default_upd_atrb_src);

	var class_id = document.wkfw.lib_upd_class_src.value;
	if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(document.wkfw.lib_upd_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_upd_atrb_src);
		}
	}

	setLibraryUpdDeptSrcOptions();
}

function setLibraryUpdDeptSrcOptions() {
	deleteAllOptions(document.wkfw.lib_upd_dept_src);

	var class_id = document.wkfw.lib_upd_class_src.value;
	var atrb_id = document.wkfw.lib_upd_atrb_src.value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(document.wkfw.lib_upd_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(document.wkfw.lib_upd_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function setLibraryDisplay() {
	var archive = document.wkfw.lib_archive.value;
	switch (archive) {
	case LIB_ARCHIVE_ALL:
	case LIB_ARCHIVE_PRIVATE:
		document.getElementById('lib_limit_section').style.display = 'none';
		document.getElementById('lib_limit_project').style.display = 'none';
		break;
	case LIB_ARCHIVE_SECTION:
		document.getElementById('lib_limit_section').style.display = '';
		document.getElementById('lib_limit_project').style.display = 'none';
		break;
	case LIB_ARCHIVE_PROJECT:
		document.getElementById('lib_limit_section').style.display = 'none';
		document.getElementById('lib_limit_project').style.display = '';
		break;
	}
}

function setLibraryDisabled() {
	var disabled;
	var archive = document.wkfw.lib_archive.value;

	disabled = !(archive == LIB_ARCHIVE_ALL);
	document.wkfw.lib_show_login_flg.disabled = disabled;

	disabled = !(archive == LIB_ARCHIVE_ALL && document.wkfw.lib_show_login_flg.checked);
	document.wkfw.lib_show_login_begin1.disabled = disabled;
	document.wkfw.lib_show_login_begin2.disabled = disabled;
	document.wkfw.lib_show_login_begin3.disabled = disabled;
	document.wkfw.lib_show_login_end1.disabled = disabled;
	document.wkfw.lib_show_login_end2.disabled = disabled;
	document.wkfw.lib_show_login_end3.disabled = disabled;

	disabled = ((archive == LIB_ARCHIVE_PROJECT && document.wkfw.lib_private_flg2.checked) || archive == LIB_ARCHIVE_PRIVATE);
	document.wkfw.lib_ref_dept_st_flg.disabled = disabled;

	disabled = (document.wkfw.lib_private_flg2.checked);
	document.wkfw.lib_emplist1_project.disabled = disabled;

	disabled = !(!document.wkfw.lib_ref_dept_st_flg.disabled && !document.wkfw.lib_ref_dept_st_flg.checked);
	document.wkfw.lib_ref_dept_flg[0].disabled = disabled;
	document.wkfw.lib_ref_dept_flg[1].disabled = disabled;

	disabled = !(!document.wkfw.lib_ref_dept_flg[1].disabled && document.wkfw.lib_ref_dept_flg[1].checked);
//	document.wkfw.lib_ref_dept_all.disabled = disabled;
	document.wkfw.lib_ref_class_src.disabled = disabled;
	document.wkfw.lib_ref_atrb_src.disabled = disabled;
	document.wkfw.lib_ref_dept_src.disabled = disabled;
	document.wkfw.lib_ref_dept.disabled = disabled;
	document.wkfw.add_lib_ref_dept.disabled = disabled;
	document.wkfw.delete_lib_ref_dept.disabled = disabled;
	document.wkfw.delete_lib_all_ref_dept.disabled = disabled;
	document.wkfw.select_lib_all_ref_dept.disabled = disabled;

	disabled = (document.wkfw.lib_ref_dept_st_flg.disabled || document.wkfw.lib_ref_dept_st_flg.checked);
	document.wkfw.lib_ref_st_flg[0].disabled = disabled;
	document.wkfw.lib_ref_st_flg[1].disabled = disabled;

	disabled = !(!document.wkfw.lib_ref_st_flg[1].disabled && document.wkfw.lib_ref_st_flg[1].checked);
	document.wkfw.elements['lib_ref_st[]'].disabled = disabled;

	disabled = ((archive == LIB_ARCHIVE_SECTION && document.wkfw.lib_private_flg1.checked) || (archive == LIB_ARCHIVE_PROJECT && document.wkfw.lib_private_flg2.checked) || archive == LIB_ARCHIVE_PRIVATE);
	document.wkfw.lib_emplist1.disabled = disabled;
	document.wkfw.lib_emplist1_clear.disabled = disabled;
	if (disabled) {
		clearLibraryAllTargets('2');
	}

	disabled = (archive == LIB_ARCHIVE_PRIVATE);
	document.wkfw.lib_emplist2.disabled = disabled;
	document.wkfw.lib_emplist2_clear.disabled = disabled;
	if (disabled) {
		clearLibraryAllTargets('3');
	}

	disabled = (archive == LIB_ARCHIVE_PROJECT || archive == LIB_ARCHIVE_PRIVATE);
	document.wkfw.lib_upd_dept_st_flg.disabled = disabled;

	disabled = !(!document.wkfw.lib_upd_dept_st_flg.disabled && !document.wkfw.lib_upd_dept_st_flg.checked);
	document.wkfw.lib_upd_dept_flg[0].disabled = disabled;
	document.wkfw.lib_upd_dept_flg[1].disabled = disabled;

	disabled = !(!document.wkfw.lib_upd_dept_flg[1].disabled && document.wkfw.lib_upd_dept_flg[1].checked);
//	document.wkfw.lib_upd_dept_all.disabled = disabled;
	document.wkfw.lib_upd_class_src.disabled = disabled;
	document.wkfw.lib_upd_atrb_src.disabled = disabled;
	document.wkfw.lib_upd_dept_src.disabled = disabled;
	document.wkfw.lib_upd_dept.disabled = disabled;
	document.wkfw.add_lib_upd_dept.disabled = disabled;
	document.wkfw.delete_lib_upd_dept.disabled = disabled;
	document.wkfw.delete_lib_all_upd_dept.disabled = disabled;
	document.wkfw.select_lib_all_upd_dept.disabled = disabled;

	disabled = (document.wkfw.lib_upd_dept_st_flg.disabled || document.wkfw.lib_upd_dept_st_flg.checked);
	document.wkfw.lib_upd_st_flg[0].disabled = disabled;
	document.wkfw.lib_upd_st_flg[1].disabled = disabled;

	disabled = !(!document.wkfw.lib_upd_st_flg[1].disabled && document.wkfw.lib_upd_st_flg[1].checked);
	document.wkfw.elements['lib_upd_st[]'].disabled = disabled;
}

function toggleLibrary(button) {
	var display;
	if (button.innerHTML == '▼') {
		button.innerHTML = '▲';
		display = '';
	} else {
		button.innerHTML = '▼';
		display = 'none';
	}

	for (var i = 1; i <= 3; i++) {
		document.getElementById(button.id.concat(i)).style.display = display;
	}
}

function clearLibraryAllTargets(idx) {
	document.getElementById('target_disp_area'.concat(idx)).innerHTML = '';
	document.wkfw.elements['target_id_list'.concat(idx)].value = '';
	document.wkfw.elements['target_name_list'.concat(idx)].value = '';
}

/*
function showLibraryDeptAll(div) {
	var emp_class = '';
	if (document.wkfw.lib_archive.value == LIB_ARCHIVE_SECTION) {
		if (div == 'upd' || document.wkfw.lib_private_flg1.checked) {
			emp_class = '<? echo($emp_class); ?>';
		}
	}

	dept_win = window.open('select_dept_all.php?session=<? echo($session); ?>&module=library&emp_class='.concat(emp_class).concat('&div=').concat(div), 'deptall', 'width=800,height=500,scrollbars=yes');
}
*/

function setLibraryProjectMembers() {
	var url = 'library_project_member_xml.php?session=<? echo($session); ?>&category='.concat(document.wkfw.lib_cate_id.value);
	var callback = {
		success: function (o) {
			var ids = o.responseXML.getElementsByTagName('ID');
			var names = o.responseXML.getElementsByTagName('Name');
			for (var i = 0, j = ids.length; i < j; i++) {
				add_target_list('2', ids[i].firstChild.nodeValue, names[i].firstChild.nodeValue);
			}
		},
		failure: function () {alert('データの取得に失敗しました。');}
	};
	YAHOO.util.Connect.asyncRequest('GET', url, callback);
}

function openLibraryEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = 'emplist_popup.php';
	url += '?session=<? echo($session); ?>';
	url += '&emp_id=<? echo($emp_id); ?>';
	url += '&mode=' + (item_id - 1);
	url += '&item_id=' + item_id;
	childwin = window.open(url, 'emplistpopup', 'left=' + (dx - wx) + ',top=' + base + ',width=' + wx + ',height=' + wy + ',scrollbars=yes,resizable=yes');
	childwin.focus();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#E6B3D4 solid 1px;}
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">



<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>


<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">


	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr height="22"><? show_wkfw_menuitem($session, $fname, array("wkfw_id" => $wkfw_id, "mode" => $mode)); ?></tr>
	</table>




	<form name="wkfw" action="fplusadm_workflow_update.php" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" style="margin-top:6px">
		<!-- 承認タイプ -->
		<tr height="22">
			<td width="22%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告ルート</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<input type="radio" name="wkfw_appr" value="1"<? if ($wkfw_appr == "1") {echo(" checked");} ?>>同報
			<input type="radio" name="wkfw_appr" value="2"<? if ($wkfw_appr == "2") {echo(" checked");} ?>>稟議（回覧）
			</font></td>
		</tr>

		<!-- 管理ＣＤ -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理ＣＤ</font></td>
			<? if($mode == "REAL") { ?>
			<td colspan="2"><input name="short_wkfw_name" type="text" size="5" maxlength="4" value="<?=h($short_wkfw_name);?>" style="ime-mode:inactive;"></td>
			<? } else { ?>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($short_wkfw_name);?></font></td>
			<input type="hidden" name="short_wkfw_name" value="<?=h($short_wkfw_name);?>">
			<? } ?>
		</tr>

		<!-- 表題の表示・非表示 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表題の表示・非表示</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<input type="radio" name="apply_title_disp_flg" value="t"<? if ($apply_title_disp_flg == "t") {echo(" checked");} ?>>表示
			<input type="radio" name="apply_title_disp_flg" value="f"<? if ($apply_title_disp_flg == "f") {echo(" checked");} ?>>非表示
			</font></td>
		</tr>

		<!-- 承認階層 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付階層数</font></td>
			<td colspan="2"><select name="approve_num" onChange="checkApprove();"><option value="0">選択してください</option><?
			for ($i = 1; $i <= 20; $i++) {
				echo("<option value=\"$i\"");
				if ($approve_num == $i) { echo(" selected"); }
				echo(">$i</option>\n");
			}
			?>
			</select></td>
		</tr>


<?
$approve = array($approve1, $approve2, $approve3, $approve4, $approve5, $approve6, $approve7, $approve8, $approve9, $approve10, $approve11, $approve12, $approve13, $approve14, $approve15, $approve16, $approve17, $approve18, $approve19, $approve20);

$apv_div0_flg = array($apv_div0_flg1, $apv_div0_flg2, $apv_div0_flg3, $apv_div0_flg4, $apv_div0_flg5, $apv_div0_flg6, $apv_div0_flg7, $apv_div0_flg8, $apv_div0_flg9, $apv_div0_flg10, $apv_div0_flg11, $apv_div0_flg12, $apv_div0_flg13, $apv_div0_flg14, $apv_div0_flg15, $apv_div0_flg16, $apv_div0_flg17, $apv_div0_flg18, $apv_div0_flg19, $apv_div0_flg20);
$apv_div1_flg = array($apv_div1_flg1, $apv_div1_flg2, $apv_div1_flg3, $apv_div1_flg4, $apv_div1_flg5, $apv_div1_flg6, $apv_div1_flg7, $apv_div1_flg8, $apv_div1_flg9, $apv_div1_flg10, $apv_div1_flg11, $apv_div1_flg12, $apv_div1_flg13, $apv_div1_flg14, $apv_div1_flg15, $apv_div1_flg16, $apv_div1_flg17, $apv_div1_flg18, $apv_div1_flg19, $apv_div1_flg20);
$apv_div2_flg = array($apv_div2_flg1, $apv_div2_flg2, $apv_div2_flg3, $apv_div2_flg4, $apv_div2_flg5, $apv_div2_flg6, $apv_div2_flg7, $apv_div2_flg8, $apv_div2_flg9, $apv_div2_flg10, $apv_div2_flg11, $apv_div2_flg12, $apv_div2_flg13, $apv_div2_flg14, $apv_div2_flg15, $apv_div2_flg16, $apv_div2_flg17, $apv_div2_flg18, $apv_div2_flg19, $apv_div2_flg20);
$apv_div3_flg = array($apv_div3_flg1, $apv_div3_flg2, $apv_div3_flg3, $apv_div3_flg4, $apv_div3_flg5, $apv_div3_flg6, $apv_div3_flg7, $apv_div3_flg8, $apv_div3_flg9, $apv_div3_flg10, $apv_div3_flg11, $apv_div3_flg12, $apv_div3_flg13, $apv_div3_flg14, $apv_div3_flg15, $apv_div3_flg16, $apv_div3_flg17, $apv_div3_flg18, $apv_div3_flg19, $apv_div3_flg20);
$apv_div4_flg = array($apv_div4_flg1, $apv_div4_flg2, $apv_div4_flg3, $apv_div4_flg4, $apv_div4_flg5, $apv_div4_flg6, $apv_div4_flg7, $apv_div4_flg8, $apv_div4_flg9, $apv_div4_flg10, $apv_div4_flg11, $apv_div4_flg12, $apv_div4_flg13, $apv_div4_flg14, $apv_div4_flg15, $apv_div4_flg16, $apv_div4_flg17, $apv_div4_flg18, $apv_div4_flg19, $apv_div4_flg20);


$target_class_div = array($target_class_div1, $target_class_div2, $target_class_div3, $target_class_div4, $target_class_div5, $target_class_div6, $target_class_div7, $target_class_div8, $target_class_div9, $target_class_div10, $target_class_div11, $target_class_div12, $target_class_div13, $target_class_div14, $target_class_div15, $target_class_div16, $target_class_div17, $target_class_div18, $target_class_div19, $target_class_div20);
$st_id = array($st_id1, $st_id2, $st_id3, $st_id4, $st_id5, $st_id6, $st_id7, $st_id8, $st_id9, $st_id10, $st_id11, $st_id12, $st_id13, $st_id14, $st_id15, $st_id16, $st_id17, $st_id18, $st_id19, $st_id20);
$emp_ids = array($emp_id1, $emp_id2, $emp_id3, $emp_id4, $emp_id5, $emp_id6, $emp_id7, $emp_id8, $emp_id9, $emp_id10, $emp_id11, $emp_id12, $emp_id13, $emp_id14, $emp_id15, $emp_id16, $emp_id17, $emp_id18, $emp_id19, $emp_id20);
$emp_nm = array($emp_nm1, $emp_nm2, $emp_nm3, $emp_nm4, $emp_nm5, $emp_nm6, $emp_nm7, $emp_nm8, $emp_nm9, $emp_nm10, $emp_nm11, $emp_nm12, $emp_nm13, $emp_nm14, $emp_nm15, $emp_nm16, $emp_nm17, $emp_nm18, $emp_nm19, $emp_nm20);
$emp_del_flg = array($emp_del_flg1, $emp_del_flg2, $emp_del_flg3, $emp_del_flg4, $emp_del_flg5, $emp_del_flg6, $emp_del_flg7, $emp_del_flg8, $emp_del_flg9, $emp_del_flg10, $emp_del_flg11, $emp_del_flg12, $emp_del_flg13, $emp_del_flg14, $emp_del_flg15, $emp_del_flg16, $emp_del_flg17, $emp_del_flg18, $emp_del_flg19, $emp_del_flg20);
$pjt_parent_id = array($pjt_parent_id1, $pjt_parent_id2, $pjt_parent_id3, $pjt_parent_id4, $pjt_parent_id5, $pjt_parent_id6, $pjt_parent_id7, $pjt_parent_id8, $pjt_parent_id9, $pjt_parent_id10, $pjt_parent_id11, $pjt_parent_id12, $pjt_parent_id13, $pjt_parent_id14, $pjt_parent_id15, $pjt_parent_id16, $pjt_parent_id17, $pjt_parent_id18, $pjt_parent_id19, $pjt_parent_id20);
$pjt_child_id = array($pjt_child_id1, $pjt_child_id2, $pjt_child_id3, $pjt_child_id4, $pjt_child_id5, $pjt_child_id6, $pjt_child_id7, $pjt_child_id8, $pjt_child_id9, $pjt_child_id10, $pjt_child_id11, $pjt_child_id12, $pjt_child_id13, $pjt_child_id14, $pjt_child_id15, $pjt_child_id16, $pjt_child_id17, $pjt_child_id18, $pjt_child_id19, $pjt_child_id20);
$pjt_parent_nm = array($pjt_parent_nm1, $pjt_parent_nm2, $pjt_parent_nm3, $pjt_parent_nm4, $pjt_parent_nm5, $pjt_parent_nm6, $pjt_parent_nm7, $pjt_parent_nm8, $pjt_parent_nm9, $pjt_parent_nm10, $pjt_parent_nm11, $pjt_parent_nm12, $pjt_parent_nm13, $pjt_parent_nm14, $pjt_parent_nm15, $pjt_parent_nm16, $pjt_parent_nm17, $pjt_parent_nm18, $pjt_parent_nm19, $pjt_parent_nm20);
$pjt_child_nm = array($pjt_child_nm1, $pjt_child_nm2, $pjt_child_nm3, $pjt_child_nm4, $pjt_child_nm5, $pjt_child_nm6, $pjt_child_nm7, $pjt_child_nm8, $pjt_child_nm9, $pjt_child_nm10, $pjt_child_nm11, $pjt_child_nm12, $pjt_child_nm13, $pjt_child_nm14, $pjt_child_nm15, $pjt_child_nm16, $pjt_child_nm17, $pjt_child_nm18, $pjt_child_nm19, $pjt_child_nm20);

$class_sect_id = array($class_sect_id1, $class_sect_id2, $class_sect_id3, $class_sect_id4, $class_sect_id5, $class_sect_id6, $class_sect_id7, $class_sect_id8, $class_sect_id9, $class_sect_id10, $class_sect_id11, $class_sect_id12, $class_sect_id13, $class_sect_id14, $class_sect_id15, $class_sect_id16, $class_sect_id17, $class_sect_id18, $class_sect_id19, $class_sect_id20);
$atrb_sect_id = array($atrb_sect_id1, $atrb_sect_id2, $atrb_sect_id3, $atrb_sect_id4, $atrb_sect_id5, $atrb_sect_id6, $atrb_sect_id7, $atrb_sect_id8, $atrb_sect_id9, $atrb_sect_id10, $atrb_sect_id11, $atrb_sect_id12, $atrb_sect_id13, $atrb_sect_id14, $atrb_sect_id15, $atrb_sect_id16, $atrb_sect_id17, $atrb_sect_id18, $atrb_sect_id19, $atrb_sect_id20);
$dept_sect_id = array($dept_sect_id1, $dept_sect_id2, $dept_sect_id3, $dept_sect_id4, $dept_sect_id5, $dept_sect_id6, $dept_sect_id7, $dept_sect_id8, $dept_sect_id9, $dept_sect_id10, $dept_sect_id11, $dept_sect_id12, $dept_sect_id13, $dept_sect_id14, $dept_sect_id15, $dept_sect_id16, $dept_sect_id17, $dept_sect_id18, $dept_sect_id19, $dept_sect_id20);
$room_sect_id = array($room_sect_id1, $room_sect_id2, $room_sect_id3, $room_sect_id4, $room_sect_id5, $room_sect_id6, $room_sect_id7, $room_sect_id8, $room_sect_id9, $room_sect_id10, $room_sect_id11, $room_sect_id12, $room_sect_id13, $room_sect_id14, $room_sect_id15, $room_sect_id16, $room_sect_id17, $room_sect_id18, $room_sect_id19, $room_sect_id20);

$st_sect_id = array($st_sect_id1, $st_sect_id2, $st_sect_id3, $st_sect_id4, $st_sect_id5, $st_sect_id6, $st_sect_id7, $st_sect_id8, $st_sect_id9, $st_sect_id10, $st_sect_id11, $st_sect_id12, $st_sect_id13, $st_sect_id14, $st_sect_id15, $st_sect_id16, $st_sect_id17, $st_sect_id18, $st_sect_id19, $st_sect_id20);

$multi_apv_flg = array($multi_apv_flg1, $multi_apv_flg2, $multi_apv_flg3, $multi_apv_flg4, $multi_apv_flg5, $multi_apv_flg6, $multi_apv_flg7, $multi_apv_flg8, $multi_apv_flg9, $multi_apv_flg10, $multi_apv_flg11, $multi_apv_flg12, $multi_apv_flg13, $multi_apv_flg14, $multi_apv_flg15, $multi_apv_flg16, $multi_apv_flg17, $multi_apv_flg18, $multi_apv_flg19, $multi_apv_flg20);
$next_notice_div = array($next_notice_div1, $next_notice_div2, $next_notice_div3, $next_notice_div4, $next_notice_div5, $next_notice_div6, $next_notice_div7, $next_notice_div8, $next_notice_div9, $next_notice_div10, $next_notice_div11, $next_notice_div12, $next_notice_div13, $next_notice_div14, $next_notice_div15, $next_notice_div16, $next_notice_div17, $next_notice_div18, $next_notice_div19, $next_notice_div20);

$apv_num = array($apv_num1, $apv_num2, $apv_num3, $apv_num4, $apv_num5, $apv_num6, $apv_num7, $apv_num8, $apv_num9, $apv_num10, $apv_num11, $apv_num12, $apv_num13, $apv_num14, $apv_num15, $apv_num16, $apv_num17, $apv_num18, $apv_num19, $apv_num20);

//$deci_flg = array($deci_flg1, $deci_flg2, $deci_flg3, $deci_flg4, $deci_flg5, $deci_flg6, $deci_flg7, $deci_flg8, $deci_flg9, $deci_flg10, $deci_flg11, $deci_flg12, $deci_flg13, $deci_flg14, $deci_flg15, $deci_flg16, $deci_flg17, $deci_flg18, $deci_flg19, $deci_flg20);

for ($i = 0; $i < $approve_num; $i++) {
	$j = $i + 1;

//	if($emp_del_flg[$i] == 't') {
//		$text_color = '#FF0000';
//	} else {
		$text_color = '#000000';
//	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#feeae9\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"window.open('fplusadm_approve_list.php?session=$session&approve=$j', 'newwin2', 'width=640,height=700,scrollbars=yes')\">受付階層" . $j . "</a></font></td>\n");
	echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"green\"><span id=\"approve_content". $j ."\"></span></font>");

//	echo("<td colspan=\"2\"><input name=\"approve_content" . $j . "\" type=\"text\" size=\"50\" maxlength=\"50\" value=\"".$approve[$i]."\" style=\"color:".$text_color."\" readonly>\n");
//	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">決定権</font><input type=\"checkbox\" name=\"deci_flg" . $j . "\"");
//	if ($deci_flg[$i] == "on") {
//		echo(" checked");
//	}
//	echo(">");

	$target_class_div[$i] = ($target_class_div[$i] == "") ? "1" : $target_class_div[$i];
	$multi_apv_flg[$i] = ($multi_apv_flg[$i] == "") ? "f" : $multi_apv_flg[$i];
	$next_notice_div[$i] = ($next_notice_div[$i] == "") ? "2" : $next_notice_div[$i];
	$apv_num[$i] = ($apv_num[$i] == "") ? "1" : $apv_num[$i];

	echo("<input type=\"hidden\" name=\"approve" . $j . "\" value=\"" . $approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_div0_flg" . $j . "\" value=\"" . $apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div1_flg" . $j . "\" value=\"" . $apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div2_flg" . $j . "\" value=\"" . $apv_div2_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div3_flg" . $j . "\" value=\"" . $apv_div3_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div4_flg" . $j . "\" value=\"" . $apv_div4_flg[$i] . "\">\n");


	echo("<input type=\"hidden\" name=\"target_class_div" . $j . "\" value=\"" . $target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"st_id" . $j . "\" value=\"" . $st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_id" . $j . "\" value=\"" . $emp_ids[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_nm" . $j . "\" value=\"" . $emp_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_id" . $j . "\" value=\"" . $pjt_parent_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_id" . $j . "\" value=\"" . $pjt_child_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_nm" . $j . "\" value=\"" . $pjt_parent_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_nm" . $j . "\" value=\"" . $pjt_child_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"class_sect_id" . $j . "\" value=\"" . $class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"atrb_sect_id" . $j . "\" value=\"" . $atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"dept_sect_id" . $j . "\" value=\"" . $dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"room_sect_id" . $j . "\" value=\"" . $room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"st_sect_id" . $j . "\" value=\"" . $st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"multi_apv_flg" . $j . "\" value=\"" . $multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"next_notice_div" . $j . "\" value=\"" . $next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_num" . $j . "\" value=\"" . $apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>
<?
if($mode == "ALIAS")
{
?>
		<!-- フォルダ -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ</font></td>
			<td colspan="2">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					<span id="folder_path" style="padding-right:2px;"></span>
					<input type="button" value="選択" onclick="openFolderSetting();">
				</font>
			</td>
		</tr>
<?
}
?>
		<!-- ワークフロー名 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ワークフロー名</font></td>
			<td colspan="2"><input name="wkfw_title" type="text" size="50" maxlength="50" value="<?=h($wkfw_title);?>" style="ime-mode: active;"></td>
		</tr>


		<!-- 本文形式 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文形式</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="wkfw_content_type" value="1"<? if ($wkfw_content_type == "1") {echo(" checked");} ?> onclick="document.wkfw.referbtn.disabled = true;
				<?
				if ($can_regist_flg == false && $wkfw_content_type == "1") {
					echo("document.wkfw.update.disabled = false;");
				}
				?>
				">テキスト
				<input type="radio" name="wkfw_content_type" value="2"<? if ($wkfw_content_type == "2") {echo(" checked");} ?> onclick="document.wkfw.referbtn.disabled = false;
				<?
				if ($can_regist_flg == false) {
					echo("document.wkfw.update.disabled = true;");
				}
				?>
				">テンプレート（ＰＨＰ）
				</font>
				<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<textarea name="wkfw_content" rows="15" cols="80">
				<?
				$wkfw_content_tmp = eregi_replace("/(textarea)", "_\\1", $wkfw_content);
				echo($wkfw_content_tmp);
				?>
			</textarea></td>
		</tr>

		<!-- 申請者以外への結果通知 -->
		<tr height="100">
			<td align="right" bgcolor="#feeae9">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">報告者以外への結果通知</font><br>
				<input type="button" value="設定" style="margin-left:2em;width=4.0em;" onclick="window.open('fplusadm_notice_list.php?session=<?=$session?>', 'newwin2', 'width=640,height=700,scrollbars=yes');"><br>
				<input type="button" value="クリア" style="margin-left:2em;width=4.0em;" onclick="clear_notice();">
			</td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="green"><span id="notice_content"></span></font></td>
		</tr>

		<input type="hidden" name="notice" id="notice" value="<?=$notice?>">

		<input type="hidden" name="rslt_ntc_div0_flg" id="rslt_ntc_div0_flg" value="<?=$rslt_ntc_div0_flg?>">
		<input type="hidden" name="rslt_ntc_div1_flg" id="rslt_ntc_div1_flg" value="<?=$rslt_ntc_div1_flg?>">
		<input type="hidden" name="rslt_ntc_div2_flg" id="rslt_ntc_div2_flg" value="<?=$rslt_ntc_div2_flg?>">
		<input type="hidden" name="rslt_ntc_div3_flg" id="rslt_ntc_div3_flg" value="<?=$rslt_ntc_div3_flg?>">
		<input type="hidden" name="rslt_ntc_div4_flg" id="rslt_ntc_div4_flg" value="<?=$rslt_ntc_div4_flg?>">

		<input type="hidden" name="notice_target_class_div" value="<?=$notice_target_class_div?>">
		<input type="hidden" name="notice_st_id" value="<?=$notice_st_id?>">
		<input type="hidden" name="notice_emp_id" value="<?=$notice_emp_id?>">
		<input type="hidden" name="notice_emp_nm" value="<?=$notice_emp_nm?>">
		<input type="hidden" name="notice_pjt_parent_id" value="<?=$notice_pjt_parent_id?>">
		<input type="hidden" name="notice_pjt_child_id" value="<?=$notice_pjt_child_id?>">
		<input type="hidden" name="notice_pjt_parent_nm" value="<?=$notice_pjt_parent_nm?>">
		<input type="hidden" name="notice_pjt_child_nm" value="<?=$notice_pjt_child_nm?>">

		<input type="hidden" name="notice_class_sect_id" id="notice_class_sect_id" value="<?=$notice_class_sect_id?>">
		<input type="hidden" name="notice_atrb_sect_id" id="notice_atrb_sect_id" value="<?=$notice_atrb_sect_id?>">
		<input type="hidden" name="notice_dept_sect_id" id="notice_dept_sect_id" value="<?=$notice_dept_sect_id?>">
		<input type="hidden" name="notice_room_sect_id" id="notice_room_sect_id" value="<?=$notice_room_sect_id?>">
		<input type="hidden" name="notice_st_sect_id" id="notice_st_sect_id" value="<?=$notice_st_sect_id?>">

		<!-- 前提とする申請書 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前提とする報告書</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<span id="precond_wkfw">
				<?
				for ($i = 0; $i < count($precond_wkfw_id); $i++) {
					$tmp_wkfw_id = $precond_wkfw_id[$i];
					$tmp_wkfw_title = $precond_wkfw_title[$i];

					echo("<p id=\"p_precond_{$tmp_wkfw_id}\">\n");
					echo("{$tmp_wkfw_title}\n");
					echo("<input type=\"button\" id=\"btn_precond_{$tmp_wkfw_id}\" name=\"btn_precond_{$tmp_wkfw_id}\" value=\"削除\" onclick=\"delPrecond(event);\">\n");
					echo("<input type=\"hidden\" name=\"precond_wkfw_title[]\" value=\"{$tmp_wkfw_title}\">\n");
					echo("<input type=\"hidden\" name=\"precond_wkfw_id[]\" value=\"{$tmp_wkfw_id}\">\n");
					echo("</p>\n");
				}
				?>
				</span>
				</font>
				<input type="button" value="追加" onclick="addPrecond();">
			</td>
		</tr>

		<input type="hidden" name="sel_precond_wkfw_id" id="sel_precond_wkfw_id" value="">


		<!-- フォーマットファイル -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォーマットファイル</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<div id="attach">
				<?
				// ダウンロード文書名の設定を取得（文書管理で設定されている内容を使用）
				$filename_flg = lib_get_filename_flg();
				if ($filename_flg == 0) {
					$target = "target=\"_blank\"";
				} else {
					$target = "";
				}

				for ($i = 0; $i < count($filename); $i++) {
					$tmp_file_id = $file_id[$i];
					$tmp_filename = $filename[$i];
					$ext = strrchr($tmp_filename, ".");

					echo("<p id=\"p_{$tmp_file_id}\">\n");
				//	echo("<a href=\"workflow/tmp/{$session}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
					echo("<a href=\"fplusadm_workflow_attach_refer.php?session=$session&file_flg=2&wkfw_id=$wkfw_id&file_id=$tmp_file_id&filename=" . urlencode($tmp_filename) . "\"  $target>$tmp_filename</a>\n");

					echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
					echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
					echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
					echo("</p>\n");
				}
				?>
				</div>
				<input type="button" value="追加" onclick="attachFile();">
			</font></td>
		</tr>


		<!-- 利用期間 -->
		<tr height="22">
			<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用期間</font></td>
			<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<select name="start_year"><? show_update_years($start_year, 1, true); ?></select>/<select name="start_month"><? show_select_months($start_month, true); ?></select>/<select name="start_day"><? show_select_days($start_day, true); ?></select> 〜 <select name="end_year"><? show_update_years($end_year, 1, true); ?></select>/<select name="end_month"><? show_select_months($end_month, true); ?></select>/<select name="end_day"><? show_select_days($end_day, true); ?></select></font><br>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">※常に利用する場合は設定不要です</font>
			</td>
		</tr>


		<!-- 利用可能範囲の設定 -->
		<tr height="22">
			<td bgcolor="#feeae9" colspan="3" class="spacing">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<span style="cursor:pointer;" onclick="toggle(document.getElementById('ref_toggle'));">利用可能範囲の設定&nbsp;</span>
				<span id="ref_toggle" style="cursor:pointer;" onclick="toggle(this);"><? echo($ref_toggle_mode); ?></span>
				</font>
			</td>
		</tr>
	</table>







	<div id="ref_toggle1" style="display:<? echo($ref_toggle_display); ?>;">
		<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" style="position:relative;top:-1px;">
			<tr height="22">
				<td width="18%" bgcolor="#feeae9" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="ref_dept_st_flg" id="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
				<td bgcolor="#feeae9" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
				<td bgcolor="#feeae9" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
			</tr>

			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="1"<? if ($ref_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
								<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
								</font>
							</td>
							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
										<td></td>
										<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
											<select name="ref_class_src" id="ref_class_src" onchange="setRefAtrbSrcOptions();">
											</select><? echo($arr_class_name[0]); ?><br>
											<select name="ref_atrb_src" id="ref_atrb_src" onchange="setRefDeptSrcOptions();">
											</select><? echo($arr_class_name[1]); ?>
											</font>
										</td>
									</tr>
									<tr>
										<td>
											<select name="ref_dept" id="ref_dept" size="6" multiple style="width:120px;">
												<?
												foreach ($ref_dept as $tmp_dept_id) {
													echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
												}
												?>
											</select>
										</td>

										<td align="center"><input type="button" name="add_ref_dept" id="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" id="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);"></td>
										<td><select name="ref_dept_src" id="ref_dept_src" size="6" multiple style="width:120px;"></select></td>
									</tr>

									<tr>
										<td><input type="button" name="delete_all_ref_dept" id="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
										<td></td>
										<td><input type="button" name="select_all_ref_dept" id="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>


				<td>
					<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="radio" name="ref_st_flg" id="ref_st_flg" value="1"<? if ($ref_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
								<input type="radio" name="ref_st_flg" id="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
							</font></td>

							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td>
											<select name="ref_st[]" id="ref_st[]" size="10" multiple>
											<?
											while ($row = pg_fetch_array($sel_st)) {
												$tmp_st_id = $row["st_id"];
												$tmp_st_nm = $row["st_nm"];
												echo("<option value=\"$tmp_st_id\"");
												if (in_array($tmp_st_id, $ref_st)) {
													echo(" selected");
												}
												echo(">$tmp_st_nm");
											}
											pg_result_seek($sel_st, 0);
											?>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>

			</tr>


			<tr height="22">
				<td bgcolor="#feeae9" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
					<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
					<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
				<td colspan="2">
					<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
						<tr>
							<td width="350" height="60" style="border:#E6B3D4 solid 1px;">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
									<span id="target_disp_area1"></span>
								</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>

		</table>

	</div>




	<img src="img/spacer.gif" alt="" width="1" height="4"><br>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td width="34%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付依頼受信時にEメールを送信</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="send_mail_flg" value="t"<? if ($send_mail_flg == "t") {echo(" checked");} ?>>する
				<input type="radio" name="send_mail_flg" value="f"<? if ($send_mail_flg != "t") {echo(" checked");} ?>>しない
				</font></td>
		</tr>
	</table>




	<img src="img/spacer.gif" alt="" width="1" height="4"><br>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td width="34%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受付後に添付ファイルを文書管理へ登録</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="radio" name="lib_reg_flg" value="t"<? if ($lib_reg_flg == "t") {echo(" checked");} ?> onclick="setLibDetailDisplay('block');">する
				<input type="radio" name="lib_reg_flg" value="f"<? if ($lib_reg_flg != "t") {echo(" checked");} ?> onclick="setLibDetailDisplay('none');">しない
			</font></td>
		</tr>
	</table>


	<div id="lib_detail" style="display:<? echo(($lib_reg_flg == "t") ? "block" : "none"); ?>">
		<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" style="position:relative;top:-1px;">

			<tr height="22">
				<td width="15%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キーワード</font></td>
				<td><input name="lib_keyword" type="text" size="50" maxlength="50" value="<? echo($lib_keyword); ?>" style="ime-mode:active;"></td>
				<td width="14%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書番号</font></td>
				<td width="17%"><input type="text" name="lib_no" value="<? echo($lib_no); ?>" style="ime-mode:inactive;"></td>
			</tr>

			<tr height="22">
				<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">説明</font></td>
				<td colspan="3"><textarea name="lib_summary" rows="5" cols="40" style="ime-mode:active;"><? echo($lib_summary); ?></textarea></td>
			</tr>

			<tr height="22">
				<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
				<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<span id="lib_folder_path" style="padding-right:2px;">
				<?
				if ($lib_cate_nm != "") {
					echo("$lib_archive_nm &gt; $lib_cate_nm");
					lib_write_folder_path($lib_folder_path, true);
				}
				?>
				</span>
				<input type="button" value="選択" onclick="selectLibFolder();">
				<input type="hidden" name="lib_archive" value="<? echo($lib_archive); ?>">
				<input type="hidden" name="lib_cate_id" value="<? echo($lib_cate_id); ?>">
				<input type="hidden" name="lib_folder_id" value="<? echo($lib_folder_id); ?>">
				</font></td>
			</tr>

			<tr height="22">
				<td align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
				<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="checkbox" name="lib_show_login_flg" value="t"<? if ($lib_show_login_flg == "t") {echo(" checked");} ?> onclick="setLibraryDisabled();">ログインページに表示する
				<img src="img/spacer.gif" alt="" width="10" height="1">
				表示期間：
				<select name="lib_show_login_begin1"><? show_update_years($lib_show_login_begin1, 1, true); ?></select>/<select name="lib_show_login_begin2"><? show_select_months($lib_show_login_begin2, true); ?></select>/<select name="lib_show_login_begin3"><? show_select_days($lib_show_login_begin3, true); ?></select> 〜 <select name="lib_show_login_end1"><? show_update_years($lib_show_login_end1, 1, true); ?></select>/<select name="lib_show_login_end2"><? show_select_months($lib_show_login_end2, true); ?></select>/<select name="lib_show_login_end3"><? show_select_days($lib_show_login_end3, true); ?></select>
				</font></td>
			</tr>

			<!-- 文書権限 start -->
			<tr height="22">
				<td bgcolor="#feeae9" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span style="cursor:pointer;" onclick="toggleLibrary(document.getElementById('lib_ref_toggle'));">参照可能範囲の指定&nbsp;</span><span id="lib_ref_toggle" style="cursor:pointer;" onclick="toggleLibrary(this);"><? echo($lib_ref_toggle_mode); ?></span><span id="lib_limit_section" style="display:none;padding-left:15px;"><input type="checkbox" name="lib_private_flg1" value="t"<? if ($lib_private_flg1 == "t") {echo(" checked");} ?> onclick="setLibraryRefClassSrcOptions(false);setLibraryDisabled();">所属<? echo($arr_class_name[0]); ?>内のみ参照可能とする</span><span id="lib_limit_project" style="display:none;padding-left:15px;"><input type="checkbox" name="lib_private_flg2" value="t"<? if ($lib_private_flg2 == "t") {echo(" checked");} ?> onclick="setLibraryDisabled();">委員会メンバーのみ参照可能とする<input type="button" name="lib_emplist1_project" value="職員に委員会メンバーを追加" onclick="setLibraryProjectMembers();" style="margin-left:10px;"></span></font></td>
			</tr>

			<tr height="22" id="lib_ref_toggle1" style="display:<? echo($lib_ref_toggle_display); ?>;">
				<td bgcolor="#feeae9" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="lib_ref_dept_st_flg" value="t"<? if ($lib_ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setLibraryDisabled();">許可しない</font></td>
				<td bgcolor="#feeae9" class="spacing">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
							<!--
							<td align="right"><input type="button" name="lib_ref_dept_all" value="全画面" onclick="showLibraryDeptAll('ref');"></td>
							-->
						</tr>
					</table>
				</td>
				<td bgcolor="#feeae9" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
			</tr>
			<tr id="lib_ref_toggle2" style="display:<? echo($lib_ref_toggle_display); ?>;">
				<td>
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
							<input type="radio" name="lib_ref_dept_flg" value="1"<? if ($lib_ref_dept_flg == "1") {echo(" checked");} ?> onclick="setLibraryDisabled();">すべて<br>
							<input type="radio" name="lib_ref_dept_flg" value="2"<? if ($lib_ref_dept_flg == "2") {echo(" checked");} ?> onclick="setLibraryDisabled();">指定する
							</font></td>
							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
										<td></td>
										<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
											<select name="lib_ref_class_src" onchange="setLibraryRefAtrbSrcOptions();">
											</select><? echo($arr_class_name[0]); ?><br>
											<select name="lib_ref_atrb_src" onchange="setLibraryRefDeptSrcOptions();">
											</select><? echo($arr_class_name[1]); ?>
											</font></td>
									</tr>

									<tr>
										<td>
											<select name="lib_ref_dept" size="6" multiple style="width:120px;">
											<?
											foreach ($lib_ref_dept as $tmp_lib_dept_id) {
												echo("<option value=\"$tmp_lib_dept_id\">{$dept_names[$tmp_lib_dept_id]}");
											}
											?>
											</select>
										</td>

										<td align="center"><input type="button" name="add_lib_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.lib_ref_dept, this.form.lib_ref_dept_src);"><br><br><input type="button" name="delete_lib_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.lib_ref_dept);"></td>
										<td><select name="lib_ref_dept_src" size="6" multiple style="width:120px;"></select></td>
									</tr>

									<tr>
										<td><input type="button" name="delete_lib_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.lib_ref_dept);"></td>
										<td></td>
										<td><input type="button" name="select_lib_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.lib_ref_dept_src);"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>


				<td colspan="2">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="radio" name="lib_ref_st_flg" value="1"<? if ($lib_ref_st_flg == "1") {echo(" checked");} ?> onclick="setLibraryDisabled();">すべて<br>
								<input type="radio" name="lib_ref_st_flg" value="2"<? if ($lib_ref_st_flg == "2") {echo(" checked");} ?> onclick="setLibraryDisabled();">指定する
								</font>
							</td>
							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td>
											<select name="lib_ref_st[]" size="10" multiple>
											<?
											while ($row = pg_fetch_array($sel_st)) {
												$tmp_st_id = $row["st_id"];
												$tmp_st_nm = $row["st_nm"];
												echo("<option value=\"$tmp_st_id\"");
												if (in_array($tmp_st_id, $lib_ref_st)) {
													echo(" selected");
												}
												echo(">$tmp_st_nm");
											}
											pg_result_seek($sel_st, 0);
											?>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>


			</tr>


			<tr height="22" id="lib_ref_toggle3" style="display:<? echo($lib_ref_toggle_display); ?>;">
				<td bgcolor="#feeae9" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
					<input type="button" name="lib_emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openLibraryEmployeeList('2');"><br>
					<input type="button" name="lib_emplist1_clear" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('2', '<? echo($emp_id); ?>','<? echo($emp_name); ?>');"><br></td>
				<td colspan="3">
					<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
						<tr>
							<td width="350" height="60" style="border:#E6B3D4 solid 1px;">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
								<span id="target_disp_area2"></span>
								</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>


			<tr height="22">
				<td bgcolor="#feeae9" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span style="cursor:pointer;" onclick="toggleLibrary(document.getElementById('lib_upd_toggle'));">更新可能範囲の指定&nbsp;</span><span id="lib_upd_toggle" style="cursor:pointer;" onclick="toggleLibrary(this);"><? echo($lib_upd_toggle_mode); ?></span></font></td>
			</tr>

			<tr height="22" id="lib_upd_toggle1" style="display:<? echo($lib_upd_toggle_display); ?>;">
				<td bgcolor="#feeae9" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="lib_upd_dept_st_flg" value="t"<? if ($lib_upd_dept_st_flg == "t") {echo(" checked");} ?> onclick="setLibraryDisabled();">許可しない</font></td>
				<td bgcolor="#feeae9" class="spacing">

					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
							<!--
							<td align="right"><input type="button" name="lib_upd_dept_all" value="全画面" onclick="showLibraryDeptAll('lib_upd');"></td>
							-->
						</tr>
					</table>

				</td>

				<td bgcolor="#feeae9" colspan="2" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
			</tr>

			<tr id="lib_upd_toggle2" style="display:<? echo($lib_upd_toggle_display); ?>;">
				<td>
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="radio" name="lib_upd_dept_flg" value="1"<? if ($lib_upd_dept_flg == "1") {echo(" checked");} ?> onclick="setLibraryDisabled();">すべて<br>
								<input type="radio" name="lib_upd_dept_flg" value="2"<? if ($lib_upd_dept_flg == "2") {echo(" checked");} ?> onclick="setLibraryDisabled();">指定する
								</font></td>
							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
										<td></td>
										<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
											<select name="lib_upd_class_src" onchange="setLibraryUpdAtrbSrcOptions();">
											</select><? echo($arr_class_name[0]); ?><br>
											<select name="lib_upd_atrb_src" onchange="setLibraryUpdDeptSrcOptions();">
											</select><? echo($arr_class_name[1]); ?>
											</font></td>
									</tr>
									<tr>
										<td>
											<select name="lib_upd_dept" size="6" multiple style="width:120px;">
											<?
											foreach ($lib_upd_dept as $tmp_lib_dept_id) {
												echo("<option value=\"$tmp_lib_dept_id\">{$dept_names[$tmp_lib_dept_id]}");
											}
											?>
											</select>
										</td>
										<td align="center"><input type="button" name="add_lib_upd_dept" value=" &lt; " onclick="addSelectedOptions(this.form.lib_upd_dept, this.form.lib_upd_dept_src);"><br><br><input type="button" name="delete_lib_upd_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.lib_upd_dept);"></td>
										<td><select name="lib_upd_dept_src" size="6" multiple style="width:120px;"></select></td>
									</tr>
									<tr>
										<td><input type="button" name="delete_lib_all_upd_dept" value="全て消去" onclick="deleteAllOptions(this.form.lib_upd_dept);"></td>
										<td></td>
										<td><input type="button" name="select_lib_all_upd_dept" value="全て選択" onclick="selectAllOptions(this.form.lib_upd_dept_src);"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>


				<td colspan="2">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<input type="radio" name="lib_upd_st_flg" value="1"<? if ($lib_upd_st_flg == "1") {echo(" checked");} ?> onclick="setLibraryDisabled();">すべて<br>
								<input type="radio" name="lib_upd_st_flg" value="2"<? if ($lib_upd_st_flg == "2") {echo(" checked");} ?> onclick="setLibraryDisabled();">指定する
								</font></td>
							<td>
								<table cellspacing="2" cellpadding="0" border="0">
									<tr>
										<td>
											<select name="lib_upd_st[]" size="10" multiple>
											<?
											while ($row = pg_fetch_array($sel_st)) {
												$tmp_st_id = $row["st_id"];
												$tmp_st_nm = $row["st_nm"];
												echo("<option value=\"$tmp_st_id\"");
												if (in_array($tmp_st_id, $lib_upd_st)) {
													echo(" selected");
												}
												echo(">$tmp_st_nm");
											}
											pg_result_seek($sel_st, 0);
											?>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>

			</tr>


			<tr height="22" id="lib_upd_toggle3" style="display:<? echo($lib_upd_toggle_display); ?>;">
				<td bgcolor="#feeae9" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
					<input type="button" name="lib_emplist2" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openLibraryEmployeeList('3');"><br>
					<input type="button" name="lib_emplist2_clear" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('3', '<? echo($emp_id); ?>', '<? echo($emp_name); ?>');"><br></td>
				<td colspan="3">
					<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
						<tr>
							<td width="350" height="60" style="border:#E6B3D4 solid 1px;">
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
								<span id="target_disp_area3"></span>
								</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- 文書権限 end -->


		</table>
	</div>



	<!--
	<img src="img/spacer.gif" alt="" width="1" height="4"><br>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
	<td width="34%" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文言</font></td>
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<input type="radio" name="approve_label" value="1"<? if ($approve_label == "1") {echo(" checked");} ?>>「承認」の文言を使用
	<input type="radio" name="approve_label" value="2"<? if ($approve_label == "2") {echo(" checked");} ?>>「確認」の文言を使用
	</font></td>
	</tr>
	</table>
	-->

	<input type="hidden" name="approve_label" value="1">

	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr height="22">
			<td align="right">
				<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">
				<input type="button" name="update" value="更新" onclick="submitForm();"
				<?
				if ($can_regist_flg == false && $wkfw_content_type == "2") {
					echo(" disabled");
				}
				?>
				></td>
		</tr>
	</table>


	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="wkfw_id" id="wkfw_id" value="<? echo($wkfw_id); ?>">
	<input type="hidden" name="back" value="t">
	<input type="hidden" name="preview_flg" value="">
	<input type="hidden" name="wkfw_type" id="wkfw_type" value="<?=$wkfw_type?>">
	<input type="hidden" name="wkfw_folder_id" id="wkfw_folder_id" value="<?=$wkfw_folder_id?>">
	<input type="hidden" name="ref_toggle_mode" value="">
	<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
	<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
	<input type="hidden" name="mode" value="<?=$mode?>">
	<input type="hidden" name="lib_ref_toggle_mode" value="">
	<input type="hidden" name="lib_upd_toggle_mode" value="">
	<input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
	<input type="hidden" id="target_name_list2" name="target_name_list2" value="">
	<input type="hidden" id="target_id_list3"   name="target_id_list3" value="">
	<input type="hidden" id="target_name_list3" name="target_name_list3" value="">

</form>


</td></tr></table>

</td></tr></table>





<script type="text/javascript">
<?
// プレビュー押下時
if ($preview_flg == "1") {


// wkfw_content 保存
$ext = ".php";
$savefilename = "fplus/workflow/tmp/{$session}_d{$ext}";

// 内容書き込み
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);

// 承認者とフォーマットファイル情報をファイルで引き渡す
$format_cnt = count($filename);
$id_data = "approve_num=$approve_num";
$id_data .= "&format_cnt=$format_cnt";
// 承認者
for ($i=0; $i<$approve_num; $i++) {

	$tmp_apv_div0_flg = "apv_div0_flg".($i+1);
	$tmp_apv_div1_flg = "apv_div1_flg".($i+1);
	$tmp_apv_div2_flg = "apv_div2_flg".($i+1);
	$tmp_apv_div3_flg = "apv_div3_flg".($i+1);
	$tmp_apv_div4_flg = "apv_div4_flg".($i+1);

	$tmp_target_class_div = "target_class_div".($i+1);
	$tmp_st_id = "st_id".($i+1);
	$tmp_emp_id = "emp_id".($i+1);
	$tmp_pjt_parent_id = "pjt_parent_id".($i+1);
	$tmp_pjt_child_id = "pjt_child_id".($i+1);
	$tmp_class_sect_id = "class_sect_id".($i+1);
	$tmp_atrb_sect_id = "atrb_sect_id".($i+1);
	$tmp_dept_sect_id = "dept_sect_id".($i+1);
	$tmp_room_sect_id = "room_sect_id".($i+1);
	$tmp_st_sect_id = "st_sect_id".($i+1);
	$tmp_multi_apv_flg = "multi_apv_flg".($i+1);
	$tmp_apv_num = "apv_num".($i+1);

	$id_data .= "&$tmp_apv_div0_flg=$apv_div0_flg[$i]";
	$id_data .= "&$tmp_apv_div1_flg=$apv_div1_flg[$i]";
	$id_data .= "&$tmp_apv_div2_flg=$apv_div2_flg[$i]";
	$id_data .= "&$tmp_apv_div3_flg=$apv_div3_flg[$i]";
	$id_data .= "&$tmp_apv_div4_flg=$apv_div4_flg[$i]";

	$id_data .= "&$tmp_target_class_div=$target_class_div[$i]";
	$id_data .= "&$tmp_st_id=$st_id[$i]";
	$id_data .= "&$tmp_emp_id=$emp_ids[$i]";
	$id_data .= "&$tmp_pjt_parent_id=$pjt_parent_id[$i]";
	$id_data .= "&$tmp_pjt_child_id=$pjt_child_id[$i]";
	$id_data .= "&$tmp_class_sect_id=$class_sect_id[$i]";
	$id_data .= "&$tmp_atrb_sect_id=$atrb_sect_id[$i]";
	$id_data .= "&$tmp_dept_sect_id=$dept_sect_id[$i]";
	$id_data .= "&$tmp_room_sect_id=$room_sect_id[$i]";
	$id_data .= "&$tmp_st_sect_id=$st_sect_id[$i]";
	$id_data .= "&$tmp_multi_apv_flg=$multi_apv_flg[$i]";
	$id_data .= "&$tmp_apv_num=$apv_num[$i]";
}
$id_data .= "&notice=$notice";

$id_data .= "&rslt_ntc_div0_flg=$rslt_ntc_div0_flg";
$id_data .= "&rslt_ntc_div1_flg=$rslt_ntc_div1_flg";
$id_data .= "&rslt_ntc_div2_flg=$rslt_ntc_div2_flg";
$id_data .= "&rslt_ntc_div3_flg=$rslt_ntc_div3_flg";
$id_data .= "&rslt_ntc_div4_flg=$rslt_ntc_div4_flg";

$id_data .= "&notice_target_class_div=$notice_target_class_div";
$id_data .= "&notice_st_id=$notice_st_id";
$id_data .= "&notice_emp_id=$notice_emp_id";
$id_data .= "&notice_pjt_parent_id=$notice_pjt_parent_id";
$id_data .= "&notice_pjt_child_id=$notice_pjt_child_id";

$id_data .= "&notice_class_sect_id=$notice_class_sect_id";
$id_data .= "&notice_atrb_sect_id=$notice_atrb_sect_id";
$id_data .= "&notice_dept_sect_id=$notice_dept_sect_id";
$id_data .= "&notice_room_sect_id=$notice_room_sect_id";
$id_data .= "&notice_st_sect_id=$notice_st_sect_id";


// フォーマットファイル
for ($i=0; $i<$format_cnt; $i++) {
	$tmp_file_id = "file_id".($i+1);
	$tmp_filename = "filename".($i+1);
	$id_data .= "&$tmp_file_id=$file_id[$i]";
	$id_data .= "&$tmp_filename=$filename[$i]";
}

// 前提とする申請書
$precond_cnt = count($precond_wkfw_id);
$id_data .= "&precond_cnt=$precond_cnt";
for ($i=0; $i<$precond_cnt; $i++)
{
	$tmp_precond_wkfw_id = "precond_wkfw_id".($i+1);
	$tmp_precond_wkfw_title = "precond_wkfw_title".($i+1);
	$id_data .= "&$tmp_precond_wkfw_id=$precond_wkfw_id[$i]";
	$id_data .= "&$tmp_precond_wkfw_title=$precond_wkfw_title[$i]";
}


$idfilename = "fplus/workflow/tmp/{$session}_id{$ext}";
// 内容書き込み
$fp = fopen($idfilename, "w");
fwrite($fp, $id_data, 2000000);

fclose($fp);


$wkfw_title = urlencode($wkfw_title);
?>
	wkfw_type      = document.getElementById('wkfw_type').value;
	wkfw_folder_id = document.getElementById('wkfw_folder_id').value;
//	approve_label  = (document.wkfw.approve_label[0].checked) ? '1' : '2';
	approve_label  = '1';
	var url = 'fplusadm_workflow_register_preview.php'
		+ '?session=<? echo($session); ?>'
		+ '&wkfw_id=<? echo($wkfw_id); ?>'
		+ '&wkfw_appr=<? echo($wkfw_appr); ?>'
		+ '&wkfw_type='+ wkfw_type
		+ '&wkfw_folder_id='+ wkfw_folder_id
		+ '&wkfw_content_type=<? echo($wkfw_content_type); ?>'
		+ '&wkfw_title=<? echo($wkfw_title); ?>'
		+ '&short_wkfw_name=<? echo($short_wkfw_name); ?>'
		+ '&apply_title_disp_flg=<? echo($apply_title_disp_flg); ?>'
		+ '&approve_label='+ approve_label
		;
	show_preview_window(url);
<?
}

// テキスト時はボタンを無効化
if ($wkfw_content_type == "1") {
?>
document.wkfw.referbtn.disabled = true;
<?
}
?>

// 承認階層
<?
for ($i = 0; $i < $approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('approve_content<?=$j?>').innerHTML = '<?=$approve[$i]?>';
<?
}
?>

// 申請者以外の結果通知
document.getElementById('notice_content').innerHTML = '<?=$notice?>';


// 前提となる申請書
<?
$sel_precond_wkfw_id = "";
foreach($precond_wkfw_id as $tmp_precond_wkfw_id)
{
	if($sel_precond_wkfw_id != "")
	{
		$sel_precond_wkfw_id .= ",";
	}
	$sel_precond_wkfw_id .= $tmp_precond_wkfw_id;
}
?>
document.getElementById('sel_precond_wkfw_id').value = '<?=$sel_precond_wkfw_id?>';

</script>
</body>
</html>

<?
// ワークロー・承認者詳細情報取得
function search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {

	$sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg ";
	if($mode == "ALIAS")
	{
		$sql .= "from fplusapvdtl a ";
	}
	else
	{
		$sql .= "from fplusapvdtl_real a ";
	}
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "inner join authmst c on a.emp_id = c.emp_id ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order order by a.apv_sub_order asc";
    $cond = "";


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$idx = 0;
	while($row = pg_fetch_array($sel))
	{

		if($idx > 0)
		{
			$emp_id .= ",";
			$approve .= ", ";
			$emp_del_flg .= ",";
		}
		$emp_id .= $row["emp_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$emp_del_flg .= $row["emp_del_flg"];

		$emp_full_nm = $emp_lt_nm ." ".$emp_ft_nm;
		$approve .= $emp_full_nm;
		$idx++;
	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => $emp_id,
							    "emp_nm" => $approve,
							    "emp_del_flg" => $emp_del_flg,
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
   							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));

}

// ワークフロー・部署役職情報取得
function search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	global $_label_by_profile;
	global $profile_type;

	$sql  = "select ";
	$sql .= "a.target_class_div, ";
	$sql .= "case a.target_class_div ";
	$sql .= "when '1' then c.class_nm ";
	$sql .= "when '2' then c.atrb_nm ";
	$sql .= "when '3' then c.dept_nm ";
	$sql .= "when '4' then c.room_nm ";
	$sql .= "else '' ";
	$sql .= "end as target_class_name, ";
	$sql .= "b.st_id,";
	$sql .= "b.st_nm ";
	if($mode == "ALIAS")
	{
		$sql .= "from fplusapvmng a left outer join ";
	}
	else
	{
		$sql .= "from fplusapvmng_real a left outer join ";
	}
	$sql .= "(select a.wkfw_id, a.apv_order, a.st_id, b.st_nm ";

	if($mode == "ALIAS")
	{
		$sql .= "from fplusapvpstdtl a inner join stmst b on a.st_id = b.st_id where st_div = 0) b on ";
	}
	else
	{
		$sql .= "from fplusapvpstdtl_real a inner join stmst b on a.st_id = b.st_id where st_div = 0) b on ";
	}
	$sql .= "a.wkfw_id = b.wkfw_id and a.apv_order = b.apv_order, ";
	$sql .= "(select * from classname) c ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";

    $cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);

	$approve = "";
	$target_class_div = "";
	$st_id = "";
	$st_nm = "";


	for ($i=0; $i<$num; $i++) {

		if($i == 0) {
		  	$target_class_div = pg_fetch_result($sel, $i, "target_class_div");
		  	$target_class_name = pg_fetch_result($sel, $i, "target_class_name");
     		$st_id = pg_fetch_result($sel, $i, "st_id");
			$st_nm = pg_fetch_result($sel, $i, "st_nm");

			if($target_class_div != 0) {
				$approve .= "報告者の所属する【";
    	 		$approve .=	$target_class_name;
     			$approve .=	"】の";
			} else {
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$approve .=	"【".$in_hospital2_title."】の";
				}
			}

			$approve .=	$st_nm;

		} else {
			$st_id .=",";
     		$st_id .= pg_fetch_result($sel, $i, "st_id");

			$st_nm = pg_fetch_result($sel, $i, "st_nm");
			$approve .=	", ";
			$approve .=	$st_nm;
		}


	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => $target_class_div,
								"st_id" => $st_id,
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 委員会ＷＧ情報取得
function search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr)
{

	$sql  = "select pjt_id, pjt_name from project ";
	if($mode == "ALIAS")
	{
		$sql .= "where pjt_id in (select parent_pjt_id from fpluswkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order) ";
	}
	else
	{
		$sql .= "where pjt_id in (select parent_pjt_id from fpluswkfwpjtdtl_real where wkfw_id = $wkfw_id and apv_order = $apv_order) ";
	}

    $sql .= "union all ";
    $sql .= "select pjt_id, pjt_name from project ";

	if($mode == "ALIAS")
	{
	    $sql .= "where pjt_id in (select child_pjt_id from fpluswkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order)";
	}
	else
	{
	    $sql .= "where pjt_id in (select child_pjt_id from fpluswkfwpjtdtl_real where wkfw_id = $wkfw_id and apv_order = $apv_order)";
	}
	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if(pg_numrows($sel) == 1)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

		$approve = $pjt_parent_nm;
	}
	else if(pg_numrows($sel) == 2)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
		$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
		$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

		$approve  = $pjt_parent_nm;
		$approve .= " > ";
		$approve .= $pjt_child_nm;
	}
	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => $pjt_parent_id,
							    "pjt_parent_nm" => $pjt_parent_nm,
							    "pjt_child_id" => $pjt_child_id,
							    "pjt_child_nm" => $pjt_child_nm,
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 部署役職(部署所属)取得
function search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr)
{

	// 部署取得
	$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
	if($mode == "ALIAS")
	{
		$sql .= "from fplusapvsectdtl a ";
	}
	else
	{
		$sql .= "from fplusapvsectdtl_real a ";
	}
	$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
	$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
	$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
	$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
	$sql .= "where a.wkfw_id = $wkfw_id ";
	$sql .= "and a.apv_order = $apv_order ";

	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class_sect_id = pg_fetch_result($sel, 0, "class_id");
	$atrb_sect_id = pg_fetch_result($sel, 0, "atrb_id");
	$dept_sect_id = pg_fetch_result($sel, 0, "dept_id");
	$room_sect_id = pg_fetch_result($sel, 0, "room_id");

	$class_nm = pg_fetch_result($sel, 0, "class_nm");
	$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
	$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
	$room_nm = pg_fetch_result($sel, 0, "room_nm");

	if($class_nm != "")
	{
		$class_sect_nm .= $class_nm;

		if($atrb_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $atrb_nm;
		}

		if($dept_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $dept_nm;
		}

		if($room_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $room_nm;
		}
	}

	// 役職
	if($class_sect_nm != "")
	{
		$sql  = "select a.*, b.st_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from fplusapvpstdtl a ";
		}
		else
		{
			$sql .= "from fplusapvpstdtl_real a ";
		}
		$sql .= "left join stmst b on a.st_id = b.st_id and not st_del_flg ";
		$sql .= "where a.wkfw_id = $wkfw_id ";
		$sql .= "and a.apv_order = $apv_order ";
		$sql .= "and a.st_div = 4 ";

		$sel  = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$all_st_sect_id = "";
		$all_st_sect_nm = "";
		while($row = pg_fetch_array($sel))
		{
			if($all_st_sect_id != "")
			{
				$all_st_sect_id .= ",";
				$all_st_sect_nm .= ", ";
			}

			$all_st_sect_id .= $row["st_id"];
			$all_st_sect_nm .= $row["st_nm"];
		}

	}
	$approve = "";
	if($class_sect_nm != "" && $all_st_sect_nm != "")
	{
		$approve = $class_sect_nm."の".$all_st_sect_nm;
	}

	array_push($ret_arr, array("approve" => $approve,
	 						    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => $class_sect_id,
							    "atrb_sect_id" => $atrb_sect_id,
							    "dept_sect_id" => $dept_sect_id,
							    "room_sect_id" => $room_sect_id,
							    "st_sect_id" => $all_st_sect_id));
}

function set_aply_apv($apv_num, &$ret_arr) {

	$approve = "報告時に受付者を指定する(".$apv_num."名)";
	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
   							    "emp_del_flg" => "f",
							    "pjt_parent_id" => "",
							    "pjt_parent_nm" => "",
							    "pjt_child_id" => "",
							    "pjt_child_nm" => "",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));

}
?>
