<?
require("about_session.php");
require("about_authority.php");
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($group_nm == "") {
	echo("<script type=\"text/javascript\">alert('権限グループ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($group_nm) > 30) {
	echo("<script type=\"text/javascript\">alert('権限グループ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// トランザクションを開始
pg_query($con, "begin");
//XX mysql_query("start transaction", $con_mysql);

// 現在の権限グループ情報を取得
$sql = "select * from authgroup";
$cond = "where group_id = $group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$current_group_auth = pg_fetch_array($sel, 0, PGSQL_ASSOC);

// 新しい権限グループ情報を作成
$new_group_auth = $current_group_auth;
foreach ($new_group_auth as $tmp_column => $tmp_value) {
	if (preg_match("/^(.*_flg)$/", $tmp_column) && $tmp_value == "t") {
		$new_group_auth[$tmp_column] = "f";
	}
}
auth_set_overwrite($webml,      "webml_flg",     $new_group_auth);
auth_set_overwrite($bbs,        "bbs_flg",       $new_group_auth);
auth_set_overwrite($attditem,   "attditem_flg",  $new_group_auth);
auth_set_overwrite($schd,       "schd_flg",      $new_group_auth);
auth_set_overwrite($wkflw,      "wkflw_flg",     $new_group_auth);
auth_set_overwrite($phone,      "phone_flg",     $new_group_auth);
auth_set_overwrite($attd,       "attdcd_flg",    $new_group_auth);
auth_set_overwrite($qa,         "qa_flg",        $new_group_auth);
auth_set_overwrite($aprv_use,   "aprv_flg",      $new_group_auth);
auth_set_overwrite($pass_flg,   "pass_flg",      $new_group_auth);
auth_set_overwrite($adbk,       "adbk_flg",      $new_group_auth);
auth_set_overwrite($empif,      "empif_flg",     $new_group_auth);
auth_set_overwrite($reg,        "reg_flg",       $new_group_auth);
auth_set_overwrite($conf,       "conf_flg",      $new_group_auth);
auth_set_overwrite($resv,       "resv_flg",      $new_group_auth);
auth_set_overwrite($tmgd,       "tmgd_flg",      $new_group_auth);
auth_set_overwrite($schdplc,    "schdplc_flg",   $new_group_auth);
auth_set_overwrite($ward,       "ward_flg",      $new_group_auth);
auth_set_overwrite($ward_reg,   "ward_reg_flg",  $new_group_auth);
auth_set_overwrite($ptreg,      "ptreg_flg",     $new_group_auth);
auth_set_overwrite($ptif,       "ptif_flg",      $new_group_auth);
auth_set_overwrite($dept_reg,   "dept_flg",      $new_group_auth);
auth_set_overwrite($news,       "news_flg",      $new_group_auth);
auth_set_overwrite($master,     "master_flg",    $new_group_auth);
auth_set_overwrite($job_flg,    "job_flg",       $new_group_auth);
auth_set_overwrite($status_flg, "status_flg",    $new_group_auth);
auth_set_overwrite($entity_flg, "entity_flg",    $new_group_auth);
auth_set_overwrite($work,       "work_flg",      $new_group_auth);
auth_set_overwrite($pjt,        "pjt_flg",       $new_group_auth);
auth_set_overwrite($lib,        "lib_flg",       $new_group_auth);
auth_set_overwrite($config,     "config_flg",    $new_group_auth);
auth_set_overwrite($biz,        "biz_flg",       $new_group_auth);
auth_set_overwrite($inout,      "inout_flg",     $new_group_auth);
auth_set_overwrite($dishist,    "dishist_flg",   $new_group_auth);
auth_set_overwrite($outreg,     "outreg_flg",    $new_group_auth);
auth_set_overwrite($disreg,     "disreg_flg",    $new_group_auth);
auth_set_overwrite($hspprf,     "hspprf_flg",    $new_group_auth);
auth_set_overwrite($lcs,        "lcs_flg",       $new_group_auth);
auth_set_overwrite($fcl,        "fcl_flg",       $new_group_auth);
auth_set_overwrite($wkadm,      "wkadm_flg",     $new_group_auth);
auth_set_overwrite($cmt,        "cmt_flg",       $new_group_auth);
auth_set_overwrite($cmtadm,     "cmtadm_flg",    $new_group_auth);
auth_set_overwrite($memo,       "memo_flg",      $new_group_auth);
auth_set_overwrite($newsuser,   "newsuser_flg",  $new_group_auth);
auth_set_overwrite($inci,       "inci_flg",      $new_group_auth);
auth_set_overwrite($rm,         "rm_flg",        $new_group_auth);
auth_set_overwrite($fplus,      "fplus_flg",     $new_group_auth);
auth_set_overwrite($fplusadm,   "fplusadm_flg",  $new_group_auth);
auth_set_overwrite($ext,        "ext_flg",       $new_group_auth);
auth_set_overwrite($extadm,     "extadm_flg",    $new_group_auth);
auth_set_overwrite($intram,     "intram_flg",    $new_group_auth);
auth_set_overwrite($stat,       "stat_flg",      $new_group_auth);
auth_set_overwrite($allot,      "allot_flg",     $new_group_auth);
auth_set_overwrite($life,       "life_flg",      $new_group_auth);
auth_set_overwrite($intra,      "intra_flg",     $new_group_auth);
auth_set_overwrite($ymstat,     "ymstat_flg",    $new_group_auth);
auth_set_overwrite($med,        "med_flg",       $new_group_auth);
auth_set_overwrite($qaadm,      "qaadm_flg",     $new_group_auth);
auth_set_overwrite($medadm,     "medadm_flg",    $new_group_auth);
auth_set_overwrite($link,       "link_flg",      $new_group_auth);
auth_set_overwrite($linkadm,    "linkadm_flg",   $new_group_auth);
auth_set_overwrite($bbsadm,     "bbsadm_flg",    $new_group_auth);
auth_set_overwrite($libadm,     "libadm_flg",    $new_group_auth);
auth_set_overwrite($adbkadm,    "adbkadm_flg",   $new_group_auth);
auth_set_overwrite($search,     "search_flg",    $new_group_auth);
auth_set_overwrite($webmladm,   "webmladm_flg",  $new_group_auth);
auth_set_overwrite($shift,      "shift_flg",     $new_group_auth);
auth_set_overwrite($shiftadm,   "shiftadm_flg",  $new_group_auth);
auth_set_overwrite($cas,        "cas_flg",       $new_group_auth);
auth_set_overwrite($casadm,     "casadm_flg",    $new_group_auth);
auth_set_overwrite($jnl,        "jnl_flg",       $new_group_auth);
auth_set_overwrite($jnladm,     "jnladm_flg",    $new_group_auth);
auth_set_overwrite($nlcs,       "nlcs_flg",      $new_group_auth);
auth_set_overwrite($nlcsadm,    "nlcsadm_flg",   $new_group_auth);
auth_set_overwrite($kview,      "kview_flg",     $new_group_auth);
auth_set_overwrite($kviewadm,   "kviewadm_flg",  $new_group_auth);
auth_set_overwrite($manabu,     "manabu_flg",    $new_group_auth);
auth_set_overwrite($manabuadm,  "manabuadm_flg", $new_group_auth);
auth_set_overwrite($ptadm,      "ptadm_flg",     $new_group_auth);
auth_set_overwrite($pjtadm,     "pjtadm_flg",    $new_group_auth);
auth_set_overwrite($amb,        "amb_flg",       $new_group_auth);
auth_set_overwrite($ambadm,     "ambadm_flg",    $new_group_auth);
auth_set_overwrite($jinji,      "jinji_flg",     $new_group_auth);
auth_set_overwrite($jinjiadm,   "jinjiadm_flg",  $new_group_auth);
auth_set_overwrite($ladder,     "ladder_flg",    $new_group_auth);
auth_set_overwrite($ladderadm,  "ladderadm_flg", $new_group_auth);
auth_set_overwrite($shift2,     "shift2_flg",    $new_group_auth);
auth_set_overwrite($shift2adm,  "shift2adm_flg", $new_group_auth);
auth_set_overwrite($cauth,      "cauth_flg",     $new_group_auth);
auth_set_overwrite($accesslog,  "accesslog_flg", $new_group_auth);
auth_set_overwrite($career,     "career_flg",    $new_group_auth);
auth_set_overwrite($careeradm,  "careeradm_flg", $new_group_auth);
auth_set_overwrite($ccusr1,     "ccusr1_flg",    $new_group_auth);
auth_set_overwrite($ccadm1,     "ccadm1_flg",    $new_group_auth);


// 管理権限とその他権限の整合性を取る
auth_fix_relations($new_group_auth, true);

if ($new_group_auth["libadm_flg"] == "t" && $new_group_auth["lib_flg"] != "t" && $new_group_auth["intra_flg"] != "t") {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\">alert('以下の組み合わせでは登録できません。\\n文書管理（管理者）権限あり、文書管理（基本）・イントラネット権限なし');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($new_group_auth["extadm_flg"] == "t" && $new_group_auth["ext_flg"] != "t" && $new_group_auth["intra_flg"] != "t") {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\">alert('以下の組み合わせでは登録できません。\\n内線電話帳管理権限あり、内線電話帳・イントラネット権限なし');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($new_group_auth["entity_flg"] == "t" && $new_group_auth["ptif_flg"] != "t" && $new_group_auth["inout_flg"] != "t" && $new_group_auth["dishist_flg"] != "t" && $new_group_auth["ward_flg"] != "t" && $new_group_auth["med_flg"] != "t") {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\">alert('以下の組み合わせでは登録できません。\\n診療科権限あり、患者基本情報・入院来院履歴・プロブレム・病床管理・メドレポート権限なし');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($new_group_auth["ptadm_flg"] == "t" && $new_group_auth["ptif_flg"] != "t" && $new_group_auth["inout_flg"] != "t" && $new_group_auth["dishist_flg"] != "t") {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\">alert('以下の組み合わせでは登録できません。\\n患者管理管理者権限あり、患者基本情報・入院来院履歴・プロブレム権限なし');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 当該権限グループが設定されている職員一覧を取得する
$sql = "select emp_id from authmst";
$cond = "where group_id = $group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 該当職員の権限情報を更新
while ($row = pg_fetch_array($sel)) {
//XX 	auth_update_employee($con, $con_mysql, $row["emp_id"], $current_group_auth, $new_group_auth, $fname);
	auth_update_employee($con, $row["emp_id"], $current_group_auth, $new_group_auth, $fname);
}

// 権限グループ情報を更新
$new_group_auth["group_nm"] = $group_nm;
$set      = array_keys($new_group_auth);
$setvalue = array_values($new_group_auth);

// 権限グループ情報を更新
$sql = "update authgroup set";
$cond = "where group_id = $group_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query("rollback");
	pg_close($con);
//XX	mysql_query("rollback", $con_mysql);
//XX	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 整合性のチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname);
auth_check_consistency($con, $session, $fname);

// トランザクションをコミット
pg_query($con, "commit");
//XX mysql_query("commit", $con_mysql);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'employee_auth_group.php?session=$session&group_id=$group_id';</script>");

//XX function auth_update_employee($con, $con_mysql, $emp_id, $current_group_auth, $new_group_auth, $fname) {
function auth_update_employee($con, $emp_id, $current_group_auth, $new_group_auth, $fname) {

	// 職員の権限情報を取得
	$sql = "select * from authmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
//XX 		mysql_query("rollback", $con_mysql);
//XX 		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_auth = pg_fetch_array($sel, 0, PGSQL_ASSOC);

	// 職員の権限情報から現在の権限グループ情報を外す（追加権限のみ残る）
	foreach ($current_group_auth as $tmp_column => $tmp_value) {
		if (preg_match("/^(.*_flg)$/", $tmp_column) && $tmp_value == "t") {
			$emp_auth["emp_$tmp_column"] = "f";
		}
	}

	// 職員の権限情報に新しい権限グループ情報を設定する
	foreach ($new_group_auth as $tmp_column => $tmp_value) {
		if (preg_match("/^(.*_flg)$/", $tmp_column) && $tmp_value == "t") {
			$emp_auth["emp_$tmp_column"] = "t";
		}
	}

	// 管理権限とその他権限の整合性を取る
	auth_fix_relations($emp_auth);

	// 職員の権限情報を更新
	$sql = "update authmst set";
	$set = array_keys($emp_auth);
	$setvalue = array_values($emp_auth);
	$cond = "where emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query("rollback");
		pg_close($con);
//XX 		mysql_query("rollback", $con_mysql);
//XX 		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// XOOPSデータを更新
//XX 	auth_update_xoops($con, $con_mysql, $emp_id, $fname);
}
