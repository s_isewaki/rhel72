<?php
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("show_class_name.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

//====================================
//����̾
//====================================
$fname = $PHP_SELF;

//====================================
//���å����Υ����å�
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// ���¥����å�
// �����󥷥ǥ�ȥ�ݡ���(47)
//====================================
$inci_auth_chk = check_authority($session, 47, $fname);
if ($inci_auth_chk == '0') {
    showLoginPage();
    exit;
}

//==================================================
//�ǥ������1=��ǥ�����2=���ǥ������
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//DB���ͥ���������
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//====================================
// ����̾����
//====================================
$sel_auth_name = sel_auth_name($con, $fname, $auth);


// ���򿦡۷�ۼԾ������
$decide_use_flg = "t";
$decide_target_class = "1";
$decide_rm_flg = "f";
$decide_ra_flg = "f";
$decide_md_flg = "f";
$decide_arr_post = array();
if($auth == 'RM' || $auth == 'RA' || $auth == 'SD'  || $auth == 'MD') {
    $sel_post = search_inci_auth_emp_post($con, $fname, $auth, '��ۼ�');
    $cnt=0;
    while ($row_post = pg_fetch_array($sel_post)) {

        if($cnt == 0) {
            $decide_use_flg = $row_post["use_flg"];
            $decide_target_class = $row_post["target_class"];
            $decide_rm_flg = $row_post["rm_flg"];
            $decide_ra_flg = $row_post["ra_flg"];
            $decide_md_flg = $row_post["md_flg"];
        }
        if($row_post["st_nm"] != "") {
            array_push($decide_arr_post, $row_post["st_nm"]);
        }
        $cnt++;
    }
}

// ���򿦡ۿ��ļԾ������
$discuss_use_flg = "t";
$discuss_target_class = "1";
$discuss_rm_flg = "f";
$discuss_ra_flg = "f";
$discuss_arr_post = array();
if($auth == 'RM' || $auth == 'RA' || $auth == 'SD') {
    $sel_post = search_inci_auth_emp_post($con, $fname, $auth, '���ļ�');
    $cnt=0;
    while ($row_post = pg_fetch_array($sel_post)) {

        if($cnt == 0) {
            $discuss_use_flg = $row_post["use_flg"];
            $discuss_target_class = $row_post["target_class"];
            $discuss_rm_flg = $row_post["rm_flg"];
            $discuss_ra_flg = $row_post["ra_flg"];
        }
        if($row_post["st_nm"] != "") {
            array_push($discuss_arr_post, $row_post["st_nm"]);
        }
        $cnt++;
    }
}

// ��������������
if($auth != 'SD') {
    // �ڿ����۷�ۼԾ������
    $arr_decide_target = search_inci_auth_emp($con, $fname, $auth, '��ۼ�');
}
if($auth == 'SM' || $auth == 'RM' || $auth == 'RA') {
    // �ڿ����ۿ��ļԾ������
    $arr_discuss_target = search_inci_auth_emp($con, $fname, $auth, '���ļ�');
}



//====================================
// ���������̾(���硦�ݡ���)����
//====================================
$arr_class_name = get_class_name_array($con, $fname);

//====================================
//ɽ��
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);

$smarty->assign('sel_auth_name', h($sel_auth_name));
$smarty->assign('auth', $auth);

//------------------------------------
//��ۼ�
//------------------------------------
//������
$smarty->assign('decide_use_flg', $decide_use_flg);
if ($decide_use_flg){
    //������̾
    $smarty->assign('decide_class', $arr_class_name[$decide_target_class - 1]);

    //��̾
    if(($auth == 'RM' && $decide_rm_flg == 't') || ($auth == 'RA' && $decide_ra_flg == 't') || ($auth == 'MD' && $decide_md_flg == 't')) {
        array_push($decide_arr_post, h($sel_auth_name));
    }
    $smarty->assign('decide_posts', $decide_arr_post);
}

//����
$smarty->assign('decide_target', implode(',', $arr_decide_target));

//------------------------------------
//���ļ�
//------------------------------------
//������
$smarty->assign('discuss_use_flg', $discuss_use_flg);
if ($discuss_use_flg){
    //������̾
    $smarty->assign('discuss_class', $arr_class_name[$discuss_target_class - 1]);

    //��̾
    if(($auth == 'RM' && $discuss_rm_flg == 't') || ($auth == 'RA' && $discuss_ra_flg == 't')) {
        array_push($discuss_arr_post, h($sel_auth_name));
    }
    $smarty->assign('discuss_posts', $discuss_arr_post);
}

//����
$smarty->assign('discuss_target', implode(',', $arr_discuss_target));

if ($design_mode == 1){
    $smarty->display("hiyari_charge_disp1.tpl");
}
else{
    $smarty->display("hiyari_charge_disp2.tpl");
}

pg_close($con);

//==============================================================================
// �����ؿ�
//==============================================================================
//====================================
// ����̾�μ���
//====================================
function sel_auth_name($con, $fname, $auth) {

    $sql = "select auth_name from inci_auth_mst ";
    $cond = "where auth = '$auth'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $tmp_auth_name = pg_result($sel,0,"auth_name");
    return $tmp_auth_name;
}

//====================================
// ô���Ծ������
//====================================
function search_inci_auth_emp($con, $fname, $auth_div, $auth_part) {

    $arr_target = array();

    $sql = "select " .
                "a.emp_id, " .
                "b.emp_name " .
            "from " .
                "inci_auth_emp a, " .
                "(select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_name from empmst) b ";
    $cond = "where a.emp_id = b.emp_id and auth = '$auth_div' and a.auth_part = '$auth_part'";
    $sel_target = select_from_table($con, $sql, $cond, $fname);
    if($sel_target == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    while ($row_target = pg_fetch_array($sel_target)) {

        $tmp_target_id = $row_target["emp_id"];
        $tmp_target_nm = $row_target["emp_name"];

        $arr_target[$tmp_target_id] = $tmp_target_nm;
    }
    return $arr_target;

}

//====================================
// ô�����������ơ��֥�������
//====================================
function search_inci_auth_emp_post($con, $fname, $auth_div, $auth_part) {

    $sql = "select ".
                "a.*, ".
                "b.emp_st, ".
                "c.st_nm ".
            "from ".
                "inci_auth_emp_post a ".
                "left outer join inci_auth_emp_st b ".
                "on a.auth = b.auth ".
                "and  a.auth_part = b.auth_part ".
                "left outer join stmst c ".
                "on b.emp_st = c.st_id and c.st_del_flg = 'f' ";
    $cond = "where a.auth = '$auth_div' and a.auth_part = '$auth_part'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    return $sel;

}

?>