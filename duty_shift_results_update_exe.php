<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜勤務実績入力「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_time_common_class.php");
require_once("timecard_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_time = new duty_shift_time_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
//日編集
///-----------------------------------------------------------------------------
$start_date = $duty_start_date;
$end_date = $duty_end_date;
$calendar_array = $obj->get_calendar_array($start_date, $end_date);
for ($i=0; $i<count($calendar_array); $i++) {
	$duty_date[$i + 1] = $calendar_array[$i]["date"];
	$duty_type[$i + 1] = $calendar_array[$i]["type"];
}
//カレンダーの未設定は警告を表示する
$calendar_chk_flg = false;
for($k=1;$k<=$day_cnt;$k++) {
	if ($duty_type[$k] == "") {
		$calendar_chk_flg = true;
		break;
	}
}
if ($calendar_chk_flg) {
	pg_query($con, "rollback");
	pg_close($con);
	$wk_date_str = substr($duty_date[$k], 0, 4) ."年";
	$wk_date_str .= intval(substr($duty_date[$k], 4, 2))."月";
	$wk_date_str .= intval(substr($duty_date[$k], 6, 2))."日";
	echo("<script type=\"text/javascript\">alert('{$wk_date_str}のカレンダーの属性が設定されていません。管理メニュー＞マスターメンテナンス＞カレンダー画面で設定してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

///-----------------------------------------------------------------------------
//ＤＢ(wktmgrp)より勤務グループ情報を取得
///-----------------------------------------------------------------------------
$data_wktmgrp = $obj->get_wktmgrp_array();
// 勤務シフトグループ情報を取得 start 20120305
$group_array = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
//勤務管理連動、デフォルトはしない
$timecard_auto_flg = $group_array[0]["timecard_auto_flg"];
if ($timecard_auto_flg == "") {
	$timecard_auto_flg = "f";
}
// 勤務シフトグループ情報を取得 end 20120305

///-----------------------------------------------------------------------------
//出勤表の勤務時間帯情報を取得
///-----------------------------------------------------------------------------
$arr_duty_shift_pattern_cnt = $obj->get_arr_duty_shift_pattern_cnt();

for ($i=0; $i<count($data_wktmgrp); $i++) {
    if ($arr_duty_shift_pattern_cnt[$data_wktmgrp[$i]["id"]] != "") {
        $officehours_array[$i] = $obj_time->get_officehours_array($data_wktmgrp[$i]["id"]);
        $officehours_array[$i]["pattern_id"] = $data_wktmgrp[$i]["id"];
    }
}

$wk_data = array();
for($i=0;$i<$data_cnt;$i++) {
	//応援追加情報
	$assist_str = "assist_group_$i";
	$arr_assist_group = split(",", $$assist_str);
	for($k=1;$k<=$day_cnt;$k++) {
		$wk_data[$i]["assist_group_$k"] = $arr_assist_group[$k - 1];
	}

}
//個人別所定時間を優先するための対応 20140318
$wk_emp_id = $staff_id[0]; //先頭の職員ID 
$timecard_common_class = new timecard_common_class($con, $fname, $wk_emp_id, $start_date, "");

$arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
$arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($start_date, $end_date);

/******************************************************************************/
// atdbkrsltへのデータ反映
/******************************************************************************/
///-----------------------------------------------------------------------------
// 勤務実績ＤＢ(atdbkrslt）へのデータ反映
///-----------------------------------------------------------------------------
for($i=0;$i<$data_cnt;$i++) {

//※更新がない状態で時刻設定のための登録があり得るので以下の判定を除く 20090803
//画面上で更新があった場合のみ以下の処理を行う。
//if ($rslt_upd_flg[$i] != "1") {
//	continue;
//}
    //個人別勤務時間 20140318
    $emp_id = $staff_id[$i];
    $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($emp_id, $start_date);
    
for($k=1;$k<=$day_cnt;$k++) {
	///-----------------------------------------------------------------------------
	//項目の配列編集
	///-----------------------------------------------------------------------------
	$wk_date = $duty_date[$k];
//	$emp_id = $staff_id[$i];
//	$g_id = "rslt_group_id_$i" . "_" . $k; //未使用の変数
	$ptn_id = "rslt_pattern_id_$i" . "_" . $k;

	$atdptn_ptn_id = "";
	$reason = "";
	$wk = "rslt_id_$i" . "_" . $k;
	if ($$wk != "") {
		if ((int)substr($$wk,0,2) > 0) {
			$atdptn_ptn_id = (int)substr($$wk,0,2);
		}
		if ((int)substr($$wk,2,2) > 0) {
			$reason = (int)substr($$wk,2,2);
		}
	}

	$reason_2 = "rslt_reason_2_$i" . "_" . $k;
	if ($$reason_2 != "") {
		$reason = $$reason_2;
	}
	//-------------------------------------------------------------------
	// データが存在するかチェック
	//-------------------------------------------------------------------
	$sql = "select * from atdbkrslt";
	$cond = "where emp_id = '$emp_id' and date = '$wk_date' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	if ($num > 0) {
		///-----------------------------------------------------------------------------
		// 勤務開始／終了時刻設定
		///-----------------------------------------------------------------------------
		$wk_start_time = pg_result($sel,0,"start_time");
		$wk_end_time = pg_result($sel,0,"end_time");
		$wk_reg_prg_flg = pg_result($sel,0,"reg_prg_flg");
		$wk1 = pg_result($sel,0,"pattern");
		$wk2 = pg_result($sel,0,"tmcd_group_id");
		$wk3 = pg_result($sel,0,"reason");
		$wk_meeting_start_time = pg_result($sel,0,"meeting_start_time");
		$wk_meeting_end_time = pg_result($sel,0,"meeting_end_time");
		//タイムカードから打刻された場合は更新しない 20090706
		// 不具合対応 20090803
		// 勤務パターンが設定されていて、時刻が未設定の場合
		//		if ($wk_reg_prg_flg != "1" || //出勤表側、タイムカードから打刻された場合に関わらず更新可能とする 20100412
			// (&& $wk_start_time == "" && $wk_end_time == "") //時刻の条件を廃止 20100412 20100722 一度間違えたので再修正
		if ($wk1 != "" || $atdptn_ptn_id != "") {
			//休日以外の場合に設定
			if ($atdptn_ptn_id != "10") {
				$start_time = $wk_start_time;
				$end_time = $wk_end_time;
				//// パターンが変更された時に時刻を更新する 20100810
				//if ($wk1 != $atdptn_ptn_id) {
				// パターンが変更された時に時刻を更新する 20100810 //DBのパターンと同じでも時刻が未設定なら更新する 20120228
				//勤務管理連動していない場合に、時刻を置き換える 20120305
				if ($timecard_auto_flg == "f" && ($wk1 != $atdptn_ptn_id ||
                        ($atdptn_ptn_id != "" && $wk1 == $atdptn_ptn_id &&
						 ($wk_start_time == "" && $wk_end_time == "") ))) {

                        //個人別所定時間を優先する場合 20140318
                        if ($arr_empcond_officehours_ptn[$$ptn_id][$atdptn_ptn_id] == 1) {
                            $wk_weekday = $arr_weekday_flg[$wk_date];
                            $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $wk_date);
                            if ($arr_empcond_officehours["officehours2_start"] != "") {
                                $start_time = str_replace(":", "", $arr_empcond_officehours["officehours2_start"]);
                                $end_time = str_replace(":", "", $arr_empcond_officehours["officehours2_end"]);
                            }
                            //未設定時は時間帯画面の時刻を使用 20140609
                            if ($start_time == "") {
                                for ($p=0; $p<count($officehours_array); $p++) {
                                    if ($officehours_array[$p]["pattern_id"] == $$ptn_id) {
                                        $wk_hhmm_1 = "start_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                        $start_time = $officehours_array[$p][$wk_hhmm_1];
                                        $wk_hhmm_2 = "end_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                        $end_time = $officehours_array[$p][$wk_hhmm_2];
                                        break;
                                    }
                                }
                            }
                        }
                        //時間帯画面の時刻を使用 20140609
                        else {
                            for ($p=0; $p<count($officehours_array); $p++) {
                                if ($officehours_array[$p]["pattern_id"] == $$ptn_id) {
                                    $wk_hhmm_1 = "start_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                    $start_time = $officehours_array[$p][$wk_hhmm_1];
                                    $wk_hhmm_2 = "end_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                    $end_time = $officehours_array[$p][$wk_hhmm_2];
                                    break;
                                }
                            }
                        }
				}
			} else {
                    //休日の場合の対応 20120510
                    //勤務管理連動していない場合は、空白にする
                    if ($timecard_auto_flg == "f") {
                        $start_time = "";
                        $end_time = "";
                    }
                    //勤務管理連動している場合は、設定済はそのまま使用する
                    else {
                        $start_time = $wk_start_time;
                        $end_time = $wk_end_time;
                    }
            }
			///-----------------------------------------------------------------------------
			// 値が同じ場合、更新しない
			///-----------------------------------------------------------------------------
	//		if ($pattern_id == $$ptn_id) {
			if ($wk_data[$i]["assist_group_$k"] == $group_id) {
				///-----------------------------------------------------------------------------
				// 存在する場合、ＵＰＤＡＴＥ
				///-----------------------------------------------------------------------------
				if ($atdptn_ptn_id != $wk1) {
					//出勤グループＩＤ／出勤パターンＩＤ
					$sql = "update atdbkrslt set";
					$set = array("pattern", "tmcd_group_id", "reg_prg_flg");
					$setvalue = array($atdptn_ptn_id, $$ptn_id, "2");
					$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
				if (($wk_start_time != $start_time) ||
					($wk_end_time != $end_time)) {
					//翌日フラグの更新 20100623
						$next_day_flag = "0";
                        //前日フラグ 20120509
                        $previous_day_flag = "0";
                        if ($atdptn_ptn_id != "") {
							$arr_atdptn_info = $obj->get_atdptn_info($$ptn_id, $atdptn_ptn_id);
							//(前日フラグがない && 出勤時刻＞退勤時刻) || 24時間以上
							if (($arr_atdptn_info["previous_day_flag"] != "1" &&
									$start_time > $end_time) || 
									$arr_atdptn_info["over_24hour_flag"] == "1") {
								$next_day_flag = "1";
							}
                            if ($arr_atdptn_info["previous_day_flag"] == "1") {
                                $previous_day_flag = "1";
                            }
						}
						
					//勤務開始／終了時間
					$sql = "update atdbkrslt set";
                    $set = array("start_time", "end_time", "reg_prg_flg", "next_day_flag", "previous_day_flag");
                    $setvalue = array($start_time, $end_time, "2", $next_day_flag, $previous_day_flag);
					$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
				if ($reason != $wk3) {
					// 空白の場合はnullを設定する
					if ($reason == "") {
						$reason = null;
					}
					// 事由
					$sql = "update atdbkrslt set";
					$set = array("reason", "reg_prg_flg");
					$setvalue = array($reason, "2");
					$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
			}
			}
//			if ($wk_reg_prg_flg != "1" && $atdptn_ptn_id != "") {
			// 出勤表側、タイムカード打刻に関わらず更新可能とする
			// パターンが変更された時に時刻を更新する 20110801
			if ($atdptn_ptn_id != $wk1) {
				//変更後パターンの時間帯の時刻が未設定の場合は、更新しない
				$wk_upd_flg = true;
				//会議・研修時刻設定追加 20091015
				if ($atdptn_ptn_id != "") {
					$arr_atdptn_info = $obj->get_atdptn_info($$ptn_id, $atdptn_ptn_id);
					if ($arr_atdptn_info["meeting_start_time"] == "" && 
						$arr_atdptn_info["meeting_end_time"] == "") {
						$wk_upd_flg = false;
					}
				} else {
					$arr_atdptn_info = array();
				}

				if ($wk_upd_flg) {				
					//会議・研修時刻
					$sql = "update atdbkrslt set";
					$set = array("meeting_start_time", "meeting_end_time");
					$setvalue = array($arr_atdptn_info["meeting_start_time"], $arr_atdptn_info["meeting_end_time"]);
					$cond = "where emp_id = '$emp_id' and date = '$wk_date'";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
					if ($upd == 0) {
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
			}
	} else {
		if ($atdptn_ptn_id != "") {
			///-----------------------------------------------------------------------------
			// 存在しない場合、ＩＮＳＥＲＴ
			///-----------------------------------------------------------------------------
			// 勤務開始／終了時刻設定を追加 2008/5/30
			$start_time = "";
			$end_time = "";
				//勤務管理連動していない場合に、時刻を置き換える 20120305
				if ($timecard_auto_flg == "f") {
                    //個人別所定時間を優先する場合 20140318
                    if ($arr_empcond_officehours_ptn[$$ptn_id][$atdptn_ptn_id] == 1) {
                        $wk_weekday = $arr_weekday_flg[$wk_date];
                        $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $wk_date);
                        if ($arr_empcond_officehours["officehours2_start"] != "") {
                            $start_time = str_replace(":", "", $arr_empcond_officehours["officehours2_start"]);
                            $end_time = str_replace(":", "", $arr_empcond_officehours["officehours2_end"]);
                        }
                    }
                    //未設定時は時間帯画面の時刻を使用
                    if ($start_time == "") {
                        for ($p=0; $p<count($officehours_array); $p++) {
                            if ($officehours_array[$p]["pattern_id"] == $$ptn_id) {
                                $wk_hhmm_1 = "start_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                $wk_hhmm_2 = "end_hhmm_" . $atdptn_ptn_id . "_" . $duty_type[$k] . "_2";
                                $start_time = $officehours_array[$p][$wk_hhmm_1];
                                $end_time = $officehours_array[$p][$wk_hhmm_2];
                                break;
                            }
                        }
                    }
				}
			//会議・研修時刻設定追加 20091015
			$arr_atdptn_info = $obj->get_atdptn_info($$ptn_id, $atdptn_ptn_id);
			
				//翌日フラグの更新 20100623
				$next_day_flag = "0";
				//(前日フラグがない && 出勤時刻＞退勤時刻) || 24時間以上
				if (($arr_atdptn_info["previous_day_flag"] != "1" &&
						$start_time > $end_time) || 
						$arr_atdptn_info["over_24hour_flag"] == "1") {
					$next_day_flag = "1";
				}
				$sql = "insert into atdbkrslt (emp_id, date, pattern, reason, tmcd_group_id, start_time, end_time, meeting_start_time, meeting_end_time, reg_prg_flg, next_day_flag";
				$sql .= ") values (";
				$content = array($emp_id, $wk_date, $atdptn_ptn_id, $reason, $pattern_id, $start_time, $end_time, $arr_atdptn_info["meeting_start_time"], $arr_atdptn_info["meeting_end_time"], "2", $next_day_flag);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}
}

//応援情報更新
$cond_add = "";
for ($i=0;$i<$data_cnt;$i++) {
	$wk_id = $staff_id[$i];
	if ($cond_add == "") {
		$cond_add .= " and (a.emp_id = '$wk_id' ";
	} else {
		$cond_add .= "or a.emp_id = '$wk_id' ";
	}
}
if ($cond_add != "") {
	$cond_add .= ") ";
}
//職員の所属するグループID
$belong_group_array = array();

$sql = "select a.emp_id, a.group_id as belong_group_id, b.main_group_id from duty_shift_staff a ";
$sql .= " left join duty_shift_plan_staff b on ";
$sql .= " b.emp_id = a.emp_id and  ";
$sql .= " b.group_id = '$group_id' ";
$sql .= " and (b.duty_yyyy = $duty_yyyy and b.duty_mm = $duty_mm) ";
$cond = " where true $cond_add ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$num = pg_numrows($sel);
for ($i=0;$i<$num;$i++) {
	$wk_staff_id = pg_result($sel,$i,"emp_id");
	$wk_group_id = pg_result($sel,$i,"belong_group_id");
	$wk_main_group_id = pg_result($sel,$i,"main_group_id");
	if ($wk_main_group_id != "") {
		$belong_group_array["$wk_staff_id"] = $wk_main_group_id; //月毎の情報から設定
	} else {
		$belong_group_array["$wk_staff_id"] = $wk_group_id; //職員設定から設定
	}
}

for($i=0;$i<$data_cnt;$i++) {

//画面上で更新があった場合のみ以下の処理を行う。
//if ($rslt_upd_flg[$i] != "1") {
//	continue;
//}

for($k=1;$k<=$day_cnt;$k++) {
	
	$wk_date = $duty_date[$k];
	$emp_id = $staff_id[$i];
	//グループIDが同じ場合
	if ($wk_data[$i]["assist_group_$k"] == $group_id) {
	
		//データ確認
		$sql = "select a.group_id from duty_shift_plan_assist a ";
		$cond = "where a.emp_id = '$staff_id[$i]' and a.duty_date = '$duty_date[$k]'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_numrows($sel);
		//存在している場合更新
		if ($num > 0) {

			$wk_group_id = pg_result($sel,0,"group_id");
			//グループが同じ場合は不要
			if ($wk_group_id != $group_id) {
				$sql = "update duty_shift_plan_assist set";
				$set = array("group_id");
				$setvalue = array($group_id);
				$cond = "where emp_id = '$emp_id' and duty_date = '$wk_date'";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
			
			}
			
		} else {
		//ない場合、追加
			$wk = "rslt_id_$i" . "_" . $k;
			//ただし、未設定、所属グループと同じ場合は不要
			if ($$wk != "0000" && $belong_group_array["$emp_id"] != $group_id) {
			
				$sql = "insert into duty_shift_plan_assist (emp_id, duty_date, group_id, assist_flg, main_group_id ";
				$sql .= ") values (";
				$content = array($emp_id, $wk_date, $group_id, "1", $belong_group_array["$emp_id"]);
				$ins = insert_into_table($con, $sql, $content, $fname);
				if ($ins == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			
			}
		}
	
	}
}
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_results.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm';</script>");
?>

<!-- ************************************************************************ -->
<!-- デバック用 -->
<!-- ************************************************************************ -->
<HTML>
<?
/**
echo("デバック");
echo("<br>");

echo("group_id=$group_id");
echo("<br>");
echo("duty_yyyy=$duty_yyyy");
echo("<br>");
echo("duty_mm=$duty_mm");
echo("<br>");
echo("pattern_id=$pattern_id");
echo("<br>");

for($i=0;$i<$data_cnt;$i++) {
	for($k=1;$k<=$day_cnt;$k++) {
		$wk3 = "atdptn_ptn_id_$i" . "_" . $k;
		echo($wk3);
		echo("=");
		echo($$wk3);
		echo("<br>");
	}
	echo("<br>");
}
**/
?>
</HTML>



