<?
ob_start();
require_once("about_comedix.php");
ob_end_clean();

$fname = $PHP_SELF;

$con = connect2db($fname);
pg_query($con, "begin");

$error = "";
foreach ($_POST as $key => $disease) {
	$id = substr($key, 3);  // 「dis10」のようにPOSTされるので「10」のみにする

	$start_ymd = date_split($disease["start_ymd"]);
	if ($start_ymd === false) {
		$error = "{$disease["line_no"]}行目の開始年月日を正しく入力してください。";
		break;
	}

	$start_ymd_selected = 0;
	$start_ymd_selected += ($start_ymd["y"] > 0) ? 1 : 0;
	$start_ymd_selected += ($start_ymd["m"] > 0) ? 1 : 0;
	$start_ymd_selected += ($start_ymd["d"] > 0) ? 1 : 0;
	if (($start_ymd_selected >= 1 && $start_ymd_selected <= 2) || ($start_ymd_selected === 3 && !checkdate($start_ymd["m"], $start_ymd["d"], $start_ymd["y"]))) {
		$error = "{$disease["line_no"]}行目の開始年月日を正しく入力してください。";
		break;
	}
	$start_ymd = ($start_ymd_selected === 3) ? sprintf("%04d%02d%02d", $start_ymd["y"], $start_ymd["m"], $start_ymd["d"]) : null;

	$end_ymd = date_split($disease["end_ymd"]);
	if ($end_ymd === false) {
		$error = "{$disease["line_no"]}行目の終了年月日を正しく入力してください。";
		break;
	}

	$end_ymd_selected = 0;
	$end_ymd_selected += ($end_ymd["y"] > 0) ? 1 : 0;
	$end_ymd_selected += ($end_ymd["m"] > 0) ? 1 : 0;
	$end_ymd_selected += ($end_ymd["d"] > 0) ? 1 : 0;
	if (($end_ymd_selected >= 1 && $end_ymd_selected <= 2) || ($end_ymd_selected === 3 && !checkdate($end_ymd["m"], $end_ymd["d"], $end_ymd["y"]))) {
		$error = "{$disease["line_no"]}行目の終了年月日を正しく入力してください。";
		break;
	}
	$end_ymd = ($end_ymd_selected === 3) ? sprintf("%04d%02d%02d", $end_ymd["y"], $end_ymd["m"], $end_ymd["d"]) : null;

	if (!is_null($start_ymd) && !is_null($end_ymd) && $start_ymd > $end_ymd) {
		$error = "{$disease["line_no"]}行目の終了年月日は開始年月日より後にしてください。";
		break;
	}

	$sql = "update sum_pt_disease set";
	$set = array("start_ymd", "is_primary", "is_specified", "end_ymd", "outcome");
	$setvalue = array($start_ymd, $disease["is_primary"], $disease["is_specified"], $end_ymd, $disease["outcome"]);
	$cond = q("where id = %d", $id);
	$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
	if ($upd == 0) {
		$error = "予期せぬエラーが発生したため、更新できませんでした。";
		break;
	}
}

if ($error === "") {
	pg_query($con, "commit");
	echo "更新しました。";
} else {
	pg_query($con, "rollback");
	echo $error;
}

pg_close($con);
exit;

function date_split($ymd) {

	// 未入力の場合
	if (strlen($ymd) === 0) {
		$date_array = array("y" => "", "m" => "", "d" => "");

	// 数値8桁の場合
	} else if (preg_match("|\A\d{8}\z|", $ymd)) {
		$date_array["y"] = substr($ymd, 0, 4);
		$date_array["m"] = substr($ymd, 4, 2);
		$date_array["d"] = substr($ymd, 6, 2);

	// 「/」区切りの場合
	} else if (preg_match("|\A\d{4}/\d{1,2}/\d{1,2}\z|", $ymd)) {
		list($date_array["y"], $date_array["m"], $date_array["d"]) = explode("/", $ymd);

	// 「-」区切りの場合
	} else if (preg_match("|\A\d{4}-\d{1,2}-\d{1,2}\z|", $ymd)) {
		list($date_array["y"], $date_array["m"], $date_array["d"]) = explode("-", $ymd);

	} else {
		$date_array = false;
	}

	return $date_array;
}
