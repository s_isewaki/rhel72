<?
ob_start();

require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("hiyari_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 47, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ファイル名を取得
$sql = "select newsfile_name from inci_newsfile";
$cond = "where news_id = $news_id and newsfile_no = $newsfile_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$newsfile_name = pg_fetch_result($sel, 0, "newsfile_name");
$ext = strrchr($newsfile_name, ".");

// データベース接続を閉じる
pg_close($con);

ob_end_clean();

$newsfile_folder_name = get_newsfile_folder_name();
$lib_url = "{$newsfile_folder_name}/{$news_id}_{$newsfile_no}{$ext}";
$file_path = $lib_url;
// 運用ディレクトリ名を取得
$dir = str_replace($_SERVER["DOCUMENT_ROOT"], "", $_SERVER["SCRIPT_FILENAME"]);
$dir = str_replace("inci_news_file_refer.php", "", $dir);
$lib_url = "http://" . $_SERVER['HTTP_HOST'] . $dir . $lib_url;

// file download
$filename_flg = get_newsfile_filename_flg();
if ($filename_flg > 0) {
	if (is_file($file_path)) {
		ob_clean();
		if ($filename_flg == 1) { // MSIE
			$newsfile_name = mb_convert_encoding($newsfile_name, "SJIS", "EUC-JP");
		} else {
			$newsfile_name = mb_convert_encoding($newsfile_name, "UTF-8", "EUC-JP");
		}

		header("Content-Disposition: attachment; filename=$newsfile_name");
		header("Content-Type: application/octet-stream; name=$newsfile_name");
		header("Content-Length: " . filesize($file_path));
		readfile($file_path);
		ob_end_flush();
		exit;
	} else {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
} else {
	header("Location: $lib_url");
}
?>
