<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 勤務表印刷 | 勤務予定表</title>

<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("atdbk_menu_common.ini");
require_once("duty_shift_common_class.php");
require_once("atdbk_duty_shift_list_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 勤務管理権限の取得
///-----------------------------------------------------------------------------
$work_admin_auth = check_authority($session, 42, $fname);
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_prt = new atdbk_duty_shift_list_common_class($con, $fname);

$sql = "select emp_id, emp_lt_nm, emp_ft_nm, emp_class from empmst";
// 管理画面から出勤表印刷可能とするため$emp_idがある場合は指定職員、$emp_idがない場合は、ログイン職員とする
if ($emp_id != "") {
	$cond = "where emp_id = '$emp_id'";
} else {
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
}

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_class_id = pg_fetch_result($sel, 0, "emp_class");
///-----------------------------------------------------------------------------
// 部門マスタより名称を取得
///-----------------------------------------------------------------------------
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' ";
$cond .= "and class_id = $emp_class_id ";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_class_name = pg_fetch_result($sel_class, 0, "class_nm");
///-----------------------------------------------------------------------------
// ログインユーザの出勤パターンIDを取得
//ＤＢ(atdptn)より出勤パターン情報を取得
///-----------------------------------------------------------------------------
$pattern_id = get_timecard_group_id($con, $emp_id, $fname);
$atdptn_array_all = $obj->get_atdptn_array("");

///-----------------------------------------------------------------------------
//開始日／終了日
///-----------------------------------------------------------------------------
$day_cnt = array();
$year = array();
$month = array();
if ( $print_mm == "4") {
	for($i=1;$i<7;$i++){
		$year[$i] = $print_yyyy;
		$month[$i] = 3 + $i;
	}
} else {
	$month[1] = "10";
	$month[2] = "11";
	$month[3] = "12";
	$month[4] = "1";
	$month[5] = "2";
	$month[6] = "3";
	$year[1] = $print_yyyy;
	$year[2] = $print_yyyy;
	$year[3] = $print_yyyy;
	$year[4] = $print_yyyy + 1;
	$year[5] = $print_yyyy + 1;
	$year[6] = $print_yyyy + 1;
}
for ($i=1;$i<7;$i++) {
	$day_cnt[$i] = $obj->days_in_month($year[$i], $month[$i]);
	///-----------------------------------------------------------------------------
	//勤務シフト記号情報（履歴）を取得
	///-----------------------------------------------------------------------------
//	$pattern_array_all[$i] = $obj->get_duty_shift_pattern_history_array("", $year[$i], $month[$i], $atdptn_array_all);
}
//年月を使用していないので修正
$pattern_array_all = $obj->get_duty_shift_pattern_array("", $atdptn_array_all);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$calendar_array = array();
for ($i=1;$i<7;$i++) {
	$duty_yyyy = $year[$i];
	$duty_mm = $month[$i];

	$wk_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);

	$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, "01");
	$end_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $wk_cnt);
	$calendar_array[$i] = $obj->get_calendar_array($start_date, $end_date);

	for ($k=1; $k<=$wk_cnt; $k++) {
		$tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
		$week_array[$i][$k]["name"] = $obj->get_weekday($tmp_date);
	}
}
///-----------------------------------------------------------------------------
//データ取得
///-----------------------------------------------------------------------------
$data = array();
for($i=1; $i<7; $i++){
	//勤務予定取得
	$atdbk_array = $obj_prt->get_atdbk_atdbkrslt_array("atdbk", $pattern_id, $emp_id, $year[$i], $month[$i], $pattern_array_all);
	//データ設定
	for($k=1; $k<=31; $k++) {
		//初期値設定
		$data[$i][$k]["font_name"] = "　";
		$data[$i][$k]["count"] = "　";
		//対象日のデータが存在する場合、設定
		for($m=0; $m<count($atdbk_array); $m++) {
			if ($k == $atdbk_array[$m]["day"]){
				if ($atdbk_array[$m]["font_name"] != "") {
					$data[$i][$k]["font_name"] = $atdbk_array[$m]["font_name"];
					$data[$i][$k]["font_color_id"] = $atdbk_array[$m]["font_color_id"];
					$data[$i][$k]["back_color_id"] = $atdbk_array[$m]["back_color_id"];
					$data[$i][$k]["count"] = $atdbk_array[$m]["count"];
				}
				break;
			}
		}
	}
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="self.print();self.close();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<!-- ------------------------------------------------------------------------ -->
<!-- タイトル -->
<!-- ------------------------------------------------------------------------ -->
<table align="center">
<tr>
	<td width="300">
		<table align="left">
		<tr heigth="22" align="center">
			<td width="140">★勤務予定表★</td>
		</tr>
		<tr heigth="22" align="center">
			<td width="140"><? echo($month[1]) ?>月〜<? echo($month[6]) ?>月</td>
		</tr>
		</table>
	</td>
	<td>
		<table border=0 cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list" align="center">
		<tr heigth="22" align="center">
			<td width="200"><font size="4"><? echo($emp_class_name) ?></font>
			</td>
			<td width="200"><? echo($emp_name) ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<!-- ------------------------------------------------------------------------ -->
<!-- 表６ヶ月分 -->
<!-- ------------------------------------------------------------------------ -->
<table border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
	<? for($i=1;$i<7;$i++){ ?>
	<td>
	<table align="left"><tr><td width-"100">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表　見出し -->
		<!-- ------------------------------------------------------------------------ -->
		<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list" align="center">
			<caption align="top">☆<? echo($month[$i]) ?>月☆</caption>
			<tr heigth="22" align="center">
				<td width="20">　</td>
				<td width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
				<td width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休</font></td>
				<td width="20"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">数</font></td>
			</tr>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表　データ部分 -->
		<!-- ------------------------------------------------------------------------ -->
		<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list" align="center">
			<? for($k=1;$k<=31;$k++) { ?>
				<tr heigth="22" align="center">
				<? if ($day_cnt[$i] >= $k) { ?>
					<!--no -->
					<td width="20"><? echo($k); ?></td>
					<!--名前 -->
					<td width="20">
					<? 
						//土、日、祝日の文字色変更
						$wk_color = "";
						if ($week_array[$i][$k]["name"] == "土"){
							$wk_color = "#0000ff";
						} else if ($week_array[$i][$k]["name"] == "日") {
							$wk_color = "#ff0000";
						} else {
							if (($calendar_array[$i][$k-1]["type"] == "4") ||
								($calendar_array[$i][$k-1]["type"] == "5") ||
								($calendar_array[$i][$k-1]["type"] == "6")) {
								$wk_color = "#ff0000";
							}
						}
						$wk = $week_array[$i][$k]["name"];
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\" >$wk</font>");
					?>
					</td>
					<!--休日 -->
					<? 
						$wk_font_color = "";
						$wk_back_color = "";
//						$wk_font_color = $data[$i][$k]["font_color_id"];
//						$wk_back_color = $data[$i][$k]["back_color_id"];
						$wk_font_name = $data[$i][$k]["font_name"];
						echo("<td width=\"40\" bgcolor=\"$wk_back_color\">");
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_font_color\">$wk_font_name</font>");
						echo("</td>");
					?>
					<!--数 -->
					<td width="20">
					<? 
						$wk = $data[$i][$k]["count"];
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font>");
					?>
					</td>
				<? } else { ?>
					<!--no -->
					<td width="20">　</td>
					<!--名前 -->
					<td width="20">　</td>
					<!--休日 -->
					<td width="40">　</td>
					<!--数 -->
					<td width="20">　</td>
				<? } ?>
				</tr>
			<? } ?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 表　備考欄 -->
		<!-- ------------------------------------------------------------------------ -->
		<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list" align="center">
		<tr heigth="22" align="left" valign="top">
			<td width="100" height="50"></td>
		</tr>
		</table>
	</td></tr></table>
	</td>
	<? } ?>
</tr>
</table>
<!-- ************************************************************************ -->
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
