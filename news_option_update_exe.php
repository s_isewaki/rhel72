<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

if ($default_category == "") {
    $default_category = "1";
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ（1:「お知らせ登録画面」を変更）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con, "1");

// トランザクションを開始
pg_query($con, "begin");

// オプション情報をDELETE〜INSERT
$sql = "delete from newsconfig";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
//オプションの設定を更新するためにsql文を変更(setting_henshin, setting_aggregateを追加)
$sql = "insert into newsconfig (default_record_flg, everyone, rows_per_page, default_category, default_term, mypage_read_flg, default_read_status, emp_id_chk, emp_class_chk, emp_job_chk, refer_rows_per_page, emp_st_chk, emplist_class, emplist_job, emp_comment_chk) values (";
$content = array(
    $record_flg,
    $everyone_flg,
    $rows_per_page,
    $default_category,
    $default_term,
    $mypage_read_flg,
    $default_read_status,
    $emp_id_chk == 't' ? $emp_id_chk : "f",
    $emp_class_chk == 't' ? $emp_class_chk : "f",
    $emp_job_chk == 't' ? $emp_job_chk : "f",
    $refer_rows_per_page,
    $emp_st_chk == 't' ? $emp_st_chk : "f",
    $setting_emplist_class == 't' ? $setting_emplist_class : "f",
    $setting_emplist_job == 't' ? $setting_emplist_job : "f",
    $emp_comment_chk == 't' ? $emp_comment_chk : "f"
);

$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$conf = new Cmx_SystemConfig();

$setting_henshin = ($setting_henshin == 't') ? $setting_henshin : "f";
$conf->set('setting_henshin', $setting_henshin);
$setting_aggregate = ($setting_aggregate == 't') ? $setting_aggregate : "f";
$conf->set('setting_aggregate', $setting_aggregate);
$conf->set('news.notice_popup', $_POST['notice_popup']);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'news_option.php?session=$session';</script>");
