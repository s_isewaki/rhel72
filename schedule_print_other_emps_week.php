<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | スケジュール印刷（一覧）</title>
<?
$fname = $PHP_SELF;

require_once("about_comedix.php");
require("time_check2.ini");
require("project_check2.ini");
require("show_date_navigation_week.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("show_other_schedule_chart.ini");
require("get_values.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$e_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
	time_update($con, $e_id, $timegd, $fname);
} else if ($timegd == "") {
	$timegd = time_check($con, $e_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
	project_update($con, $e_id, $pjtgd, $fname);
} else if ($pjtgd == "") {
	$pjtgd = project_check($con, $emp_id, $e_id, $fname);
}

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $e_id, $fname);

// 表示対象週のスタート日のタイムスタンプを取得
$start_date = get_start_date_timestamp($con, $date, $e_id, $fname);

// 表示対象週の7日分の日付を配列で取得
$dates = get_dates_in_week($start_date);
$start_ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $dates[0]);
$end_ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $dates[count($dates) - 1]);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22" bgcolor="f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("{$start_ymd}〜{$end_ymd}"); ?></font></td>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="2">
<?
$tmp_start_ymd = date("Ymd", $start_date);
$tmp_end_ymd = date("Ymd", strtotime("+6 days", $start_date));
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $tmp_start_ymd, $tmp_end_ymd);

// 日付行を表示
show_dates($dates, $arr_calendar_memo);

// 行事行を表示
if ($timegd == "on") {
	show_events($con, $dates, $session, $fname);
}

// 委員会・WG一覧を表示
if ($pjtgd == "on") {
	show_projects($con, $e_id, $dates, $session, $fname);
}

// 職員一覧を表示
show_employees($con, $emp_id, $dates, $session, $fname);
?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示対象週のスタート日のタイムスタンプを取得
function get_start_date_timestamp($con, $date, $e_id, $fname) {
	$sql = "select calendar_start1 from option";
	$cond = "where emp_id = '$e_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

	$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

	return get_start_day($date, $start_wd);
}

// 表示対象週の7日分の日付を配列で取得
function get_dates_in_week($start_date) {
	$ret = array();
	for ($i = 0; $i < 7; $i++) {
		$ret[] = date("Ymd", strtotime("+$i days", $start_date));
	}
	return $ret;
}

// 日付行を表示
function show_dates($dates, $arr_calendar_memo) {
	$today = date("Ymd");

	echo("<tr height=\"22\" valign=\"top\">\n");
	echo("<td width=\"9%\" bgcolor=\"#f6f9ff\">&nbsp;</td>\n");
	foreach ($dates as $date) {
		$timestamp = to_timestamp($date);
		$month = date("m", $timestamp);
		$day = date("d", $timestamp);
		$weekday_info = get_weekday_info($timestamp);
		$holiday = ktHolidayName($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} elseif ($holiday != "") {
			$bgcolor = "#fadede";
		} else if ($arr_calendar_memo["{$date}_type"] == "5") {
			$bgcolor = "#defafa";
		} else {
			$bgcolor = $weekday_info["color"];
		}

		echo("<td width=\"13%\" align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b>{$month}/{$day}（{$weekday_info["label"]}）</b><br>");
		// カレンダーのメモがある場合は設定する
		if ($arr_calendar_memo["$date"] != "") {
			if ($holiday == "") {
				$holiday = $arr_calendar_memo["$date"];
			} else {
				$holiday .= "<br>".$arr_calendar_memo["$date"];
			}
		}
		if ($holiday != "") {
			echo("<font color=\"red\" class=\"j12\">$holiday</font><br>");
		}
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// 行事行を表示
function show_events($con, $dates, $session, $fname) {
	$today = date("Ymd");

	echo("<tr height=\"40\" valign=\"top\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">行事</font></td>\n");
	foreach ($dates as $date) {
		$timestamp = to_timestamp($date);
		$weekday_info = get_weekday_info($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} else {
			$bgcolor = $weekday_info["color"];
		}

		echo("<td bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		show_events_on_day($con, $date, $session, $fname);
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// 行事を表示
function show_events_on_day($con, $date, $session, $fname) {
	$ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
	$event_exists = false;

	// 時間指定なし行事を表示
	$sql = "select timegd.*, timegdfcl.name as fcl_name from timegd left join timegdfcl on timegdfcl.fcl_id = timegd.fcl_id";
	$cond = "where to_char(timegd.event_date, 'YYYYMMDD') = '$date' and timegd.time_flg = 'f' order by timegd.event_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$fcl_name = $row["fcl_name"];
		if ($fcl_name == "") {
			$fcl_name = "全体";
		}
		$detail = preg_replace("/\r?\n/", "<br>", $row["event_content"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;");
		echo($row["event_name"]);
		echo("<br>");
		echo("</div>");
		$event_exists = true;
	}

	// 時間指定あり行事を表示
	$sql = "select timegd.*, timegdfcl.name as fcl_name from timegd left join timegdfcl on timegdfcl.fcl_id = timegd.fcl_id";
	$cond = "where to_char(timegd.event_date, 'YYYYMMDD') = '$date' and timegd.time_flg = 't' order by timegd.event_time";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$formatted_start_time = substr($row["event_time"], 0, 5);
		$formatted_end_time = substr($row["event_dur"], 0, 5);
		$fcl_name = $row["fcl_name"];
		if ($fcl_name == "") {
			$fcl_name = "全体";
		}
		$detail = preg_replace("/\r?\n/", "<br>", $row["event_content"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("{$formatted_start_time}-{$formatted_end_time}<br>");
		echo($row["event_name"]);
		echo("<br>");
		echo("</div>");
		$event_exists = true;
	}

	if (!$event_exists) {
		echo("&nbsp;");
	}
}

// 委員会・WG一覧を表示
function show_projects($con, $e_id, $dates, $session, $fname) {

	// 表示設定中の委員会・WG一覧を取得
	$sql = "select pjt_id, pjt_name from project";
	$cond = "where pjt_id in (select pjt_id from schdproject where emp_id = '$e_id' and ownother = '1') and (pjt_public_flag = 't' or pjt_response = '$e_id' or exists (select * from promember where promember.pjt_id = pjt_id and promember.emp_id = '$e_id')) and pjt_delete_flag = 'f' order by pjt_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 委員会・WGスケジュール行を表示
	while ($row = pg_fetch_array($sel)) {
		show_project_schedules($con, $row["pjt_id"], $row["pjt_name"], $dates, $session, $fname);
	}
}

// 委員会・WGスケジュール行を表示
function show_project_schedules($con, $pjt_id, $pjt_name, $dates, $session, $fname) {
	$today = date("Ymd");

	echo("<tr height=\"40\" valign=\"top\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">$pjt_name</font></td>\n");
	foreach ($dates as $date) {
		$timestamp = to_timestamp($date);
		$weekday_info = get_weekday_info($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} else {
			$bgcolor = $weekday_info["color"];
		}

		echo("<td bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		show_project_schedules_on_day($con, $pjt_id, $date, $session, $fname);
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// 委員会・WGスケジュールを表示
function show_project_schedules_on_day($con, $pjt_id, $date, $session, $fname) {
	$ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
	$link_date = to_timestamp($date);
	$schedule_exists = false;

	// 時間指定なしスケジュールを表示
		$sql = "select proschd2.*, proschdtype.type_name, scheduleplace.place_name from (proschd2 inner join proschdtype on proschdtype.type_id = proschd2.pjt_schd_type) left join scheduleplace on scheduleplace.place_id = proschd2.pjt_schd_place_id";
		$cond = "where proschd2.pjt_id = $pjt_id and proschd2.pjt_schd_start_date = '$date' order by proschd2.pjt_schd_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$place_name = $row["place_name"];
		if ($row["pjt_schd_place_id"] == "0") {
			$place_name = "その他";
		}
		if ($row["pjt_schd_plc"] != "") {
			$place_name .= " {$row["pjt_schd_plc"]}";
		}
		$detail = preg_replace("/\r?\n/", "<br>", $row["pjt_schd_detail"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;");
		echo($row["pjt_schd_title"]);
		echo("<br>");
		echo("</div>");
		$schedule_exists = true;
	}

	// 時間指定ありスケジュールを表示
		$sql = "select proschd.*, proschdtype.type_name, scheduleplace.place_name from (proschd inner join proschdtype on proschdtype.type_id = proschd.pjt_schd_type) left join scheduleplace on scheduleplace.place_id = proschd.pjt_schd_place_id";
		$cond = "where proschd.pjt_id = $pjt_id and proschd.pjt_schd_start_date = '$date' order by proschd.pjt_schd_start_time_v";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$formatted_start_time = substr($row["pjt_schd_start_time_v"], 0, 2).":".substr($row["pjt_schd_start_time_v"], 2, 2);
		$formatted_end_time = substr($row["pjt_schd_dur_v"], 0, 2).":".substr($row["pjt_schd_dur_v"], 2, 2);
		$place_name = $row["place_name"];
		if ($row["pjt_schd_place_id"] == "0") {
			$place_name = "その他";
		}
		if ($row["pjt_schd_plc"] != "") {
			$place_name .= " {$row["pjt_schd_plc"]}";
		}
		$detail = preg_replace("/\r?\n/", "<br>", $row["pjt_schd_detail"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("{$formatted_start_time}-{$formatted_end_time}<br>");
		echo($row["pjt_schd_title"]);
		echo("<br>");
		echo("</div>");
		$schedule_exists = true;
	}

	if (!$schedule_exists) {
		echo("&nbsp;");
	}
}

// 職員一覧を表示
function show_employees($con, $emp_id, $dates, $session, $fname) {
	foreach ($emp_id as $tmp_emp_id) {
		$tmp_emp_nm = get_emp_kanji_name($con, $tmp_emp_id, $fname);
		show_personal_schedules($con, $tmp_emp_id, $tmp_emp_nm, $dates, $session, $fname);
	}
}

// 職員スケジュール行を表示
function show_personal_schedules($con, $emp_id, $emp_nm, $dates, $session, $fname) {
	$today = date("Ymd");

	echo("<tr height=\"40\" valign=\"top\">\n");
	echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_nm</font></td>\n");
	foreach ($dates as $date) {
		$timestamp = to_timestamp($date);
		$weekday_info = get_weekday_info($timestamp);
		if ($date == $today) {
			$bgcolor = "#ccffcc";
		} else {
			$bgcolor = $weekday_info["color"];
		}

		echo("<td bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		show_personal_schedules_on_day($con, $emp_id, $date, $session, $fname);
		echo("</font></td>\n");
	}
	echo("</tr>\n");
}

// 職員スケジュールを表示
function show_personal_schedules_on_day($con, $emp_id, $date, $session, $fname) {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$login_emp_id = pg_fetch_result($sel, 0, "emp_id");

	$ymd = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $date);
	$link_date = to_timestamp($date);
	$schedule_exists = false;

	// 時間指定なし職員スケジュールを表示
	$sql = "select s.*, t.type_name, p.place_name from schdmst2 s inner join schdtype t on t.type_id = s.schd_type left join scheduleplace p on p.place_id = s.schd_place_id";
	$cond = get_other_schedule_sql_base($emp_id, $login_emp_id) . " and to_char(s.schd_start_date, 'YYYYMMDD') = '$date' order by s.schd_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$style = get_style_by_marker($row["marker"]);
		$place_name = $row["place_name"];
		if ($row["schd_place_id"] == "0") {
			$place_name = "その他";
		}
		if ($row["schd_plc"] != "") {
			$place_name .= " {$row["schd_plc"]}";
		}
		$status = ($row["schd_status"] == "2") ? "[未承認]" : "";
		$detail = preg_replace("/\r?\n/", "<br>", $row["schd_detail"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;");
		if ($row["schd_imprt"] == "3") {
			echo("<span$style>予定あり</span>");
		} else {
			echo("<span$style>");
			eh("{$row["schd_title"]} $place_name$status");
			echo("</span>");
		}
		echo("</div>");
		$schedule_exists = true;
	}

	// 時間指定あり職員スケジュールを表示
	$sql = "select s.*, t.type_name, p.place_name from schdmst s inner join schdtype t on t.type_id = s.schd_type left join scheduleplace p on p.place_id = s.schd_place_id";
	$cond = get_other_schedule_sql_base($emp_id, $login_emp_id) . " and to_char(s.schd_start_date, 'YYYYMMDD') = '$date' order by s.schd_start_time_v";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$style = get_style_by_marker($row["marker"]);
		$formatted_start_time = substr($row["schd_start_time_v"], 0, 2).":".substr($row["schd_start_time_v"], 2, 2);
		$formatted_end_time = substr($row["schd_dur_v"], 0, 2).":".substr($row["schd_dur_v"], 2, 2);
		$place_name = $row["place_name"];
		if ($row["schd_place_id"] == "0") {
			$place_name = "その他";
		}
		if ($row["schd_plc"] != "") {
			$place_name .= " {$row["schd_plc"]}";
		}
		$status = ($row["schd_status"] == "2") ? "[未承認]" : "";
		$detail = preg_replace("/\r?\n/", "<br>", $row["schd_detail"]);
		echo("<div style=\"margin-bottom:5px;\">");
		echo("{$formatted_start_time}-{$formatted_end_time}<br>");
		if ($row["schd_imprt"] == "3") {
			echo("<span$style>予定あり</span>");
		} else {
			echo("<span$style>");
			eh("{$row["schd_title"]} $place_name$status");
			echo("</span>");
		}
		echo("</div>\n");
		$schedule_exists = true;
	}

	if (!$schedule_exists) {
		echo("&nbsp;");
	}
}

// タイムスタンプから曜日情報を取得
function get_weekday_info($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return array("label" => "日", "color" => "#fadede");
		break;
	case "1":
		return array("label" => "月", "color" => "#fefcdf");
		break;
	case "2":
		return array("label" => "火", "color" => "#fefcdf");
		break;
	case "3":
		return array("label" => "水", "color" => "#fefcdf");
		break;
	case "4":
		return array("label" => "木", "color" => "#fefcdf");
		break;
	case "5":
		return array("label" => "金", "color" => "#fefcdf");
		break;
	case "6":
		return array("label" => "土", "color" => "#defafa");
		break;
	}
}
?>
