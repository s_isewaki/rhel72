<?
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("get_ward_div.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_KANRI, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;

$con = connect2db($fname);

// ポストバック時の処理
if ($is_postback == "true") {
	pg_query($con, "begin");

	// 除外診療科を更新
	set_out_sect_id_list($con, $fname, $out_sect_id_list);

	// 病棟区分を更新
	$sql = "update wddvmst set";
	$set = array("general_flg");
	if (!is_array($wddv_ids)) $wddv_ids = array();
	if (count($wddv_ids) > 0) {
		$setvalue = array("t");
		$cond = "where wddv_id in (" . implode(", ", $wddv_ids) . ")";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$setvalue = array("f");
		$cond = "where wddv_id not in (" . implode(", ", $wddv_ids) . ")";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		$setvalue = array("f");
		$cond = "";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	pg_query($con, "commit");
}

// 診療科一覧取得
$sql = "select sect_id,sect_nm from sectmst";
$cond = "where not sect_del_flg order by sect_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sect_list = pg_fetch_all($sel);

// 除外診療科一覧取得
$out_sect_id_list = get_out_sect_id_list($con, $fname);

// 病棟区分一覧取得
$ward_divs = get_ward_div($con, $fname, false, false, false, true);

// 画面名
$PAGE_TITLE = "看護必要度対象設定";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($KANGO_TITLE); ?> | <? echo($PAGE_TITLE); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_kango_header($session, $fname, true); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<form name="mainform" action="<?=$fname?>" method="post">

<p style="margin-bottom:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">■<?=$KANGO_TITLE?>で取り扱わない入院患者の診療科をチェックしてください。</font></p>


<table width="400" border="0" cellspacing="0" cellpadding="1" class="list" style="margin-top:2px;margin-bottom:0;">
<tr>
<td bgcolor="#bdd1e7" width="30"></td>
<td bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
</tr>
<?
foreach ($sect_list as $sect_list1) {
	$sect_id = $sect_list1["sect_id"];
	$sect_nm = $sect_list1["sect_nm"];
?>
<tr>
<td align="center"><input type="checkbox" name="out_sect_id_list[]" value="<? echo($sect_id); ?>"<? if (in_array($sect_id, $out_sect_id_list)) {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($sect_nm)); ?></font></td>
</tr>
<?
}
?>
</table>


<p style="margin-top:15px;margin-bottom:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">■一般病床の区分にチェックしてください。</font></p>


<table width="400" border="0" cellspacing="0" cellpadding="1" class="list" style="margin-top:2px;margin-bottom:0;">
<tr>
<td bgcolor="#bdd1e7" width="30"></td>
<td bgcolor="#bdd1e7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟区分</font></td>
</tr>
<? foreach ($ward_divs as $tmp_wddv_id => $tmp_wddv) { ?>
<tr>
<td align="center"><input type="checkbox" name="wddv_ids[]" value="<? echo($tmp_wddv_id); ?>"<? if ($tmp_wddv["general_flg"]) {echo(" checked");} ?>></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($tmp_wddv["name"])); ?></font></td>
</tr>
<? } ?>
</table>


<table width="400" border="0" cellspacing="0" cellpadding="0" style="margin-top:2px;margin-bottom:0;">
<tr>
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>


<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
</form>

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
