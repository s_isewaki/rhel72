<?php
/*************************************************************************
リンクライブラリ管理
  リンク名表示設定・アイコン登録・登録
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$checkauth = check_authority($session, 61, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// リンクIDチェック
if (empty($link_id)) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

$filename = $_FILES["file"]["name"];
if ($filename == "") {
	echo("<script type=\"text/javascript\">alert('ファイルを選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

switch ($_FILES["file"]["error"]) {
	case UPLOAD_ERR_INI_SIZE:
	case UPLOAD_ERR_FORM_SIZE:
		echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	case UPLOAD_ERR_PARTIAL:
	case UPLOAD_ERR_NO_FILE:
		echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
}

list($width, $height, $type, $attr) = getimagesize($_FILES["file"]["tmp_name"]); 
if ($width==0 or $height==0) {
	echo("<script type=\"text/javascript\">alert('画像ではないファイルです。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($width>32 or $height>32) {
	echo("<script type=\"text/javascript\">alert('アイコンの大きさが32x32以上です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// ファイル保存用ディレクトリがなければ作成
if (!is_dir("link")) {
	mkdir("link");
	chmod("link", 0777);
}
if (!is_dir("link/icon")) {
	mkdir("link/icon");
	chmod("link/icon", 0777);
}

// アップロードされたファイルを保存
$ext = strrchr($filename, ".");
if ( move_uploaded_file($_FILES["file"]["tmp_name"], "link/icon/{$link_id}{$ext}") ) {
	// chmod
	chmod("link/icon/{$link_id}{$ext}", 0666);
	
	// DBに登録
	$sql = "update link set";
	$set = array("icon");
	$setvalue = array("{$link_id}{$ext}");
	$cond = "where link_id=$link_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
else {
	echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
window.opener.location.reload();
window.close();
</script>