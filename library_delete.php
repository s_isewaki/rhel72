<?php
ini_set("max_execution_time", 0);

require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");
require("library_common.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 文書管理権限チェック
$auth_id = ($path == "3") ? 63 : 32;
$check_auth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// 文書管理管理者権限を取得
$lib_admin_auth = check_authority($session, 63, $fname);

// 移動ファイル情報
$move_files = array();

// データベースに接続
$con = connect2db($fname);

// アクセスログ（フォルダ削除）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,"1");

// アクセスログ（文書削除）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,"2");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) libcom_goto_error_page_and_die();
$empmstRow = pg_fetch_assoc($sel_emp);
$emp_id = $empmstRow["emp_id"];


// トランザクションの開始
pg_query($con, "begin");

// カテゴリが選択された場合
if (is_array($cid)) {

    // 選択されたカテゴリをループ
    foreach ($cid as $tmp_cid) {

        // カテゴリ直下にフォルダが存在した場合、エラーとする
        $sql = "select count(*) from libfolder";
        $cond = "where lib_cate_id = $tmp_cid and not exists (select * from libtree where child_id = libfolder.folder_id)";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            $sql = "select lib_cate_nm from libcate";
            $cond = "where lib_cate_id = $tmp_cid";
            $sel = select_from_table($con, $sql, $cond, $fname);
	        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
            $cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$cate_nm}」は、フォルダが存在するため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // カテゴリ直下に最新版の文書が存在した場合、エラーとする
        $sql = "select count(*) from libinfo";
        $cond = "where lib_cate_id = $tmp_cid and folder_id is null and exists (select * from libedition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from libedition group by base_lib_id) latest_edition where latest_edition.base_lib_id = libedition.base_lib_id and latest_edition.edition_no = libedition.edition_no) and libedition.lib_id = libinfo.lib_id)";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            $sql = "select lib_cate_nm from libcate";
            $cond = "where lib_cate_id = $tmp_cid";
            $sel = select_from_table($con, $sql, $cond, $fname);
	        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
            $cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$cate_nm}」は、文書が存在するため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // ワークフローの保存先として使われていたら削除不可とする
        $sql = "select count(*) from wkfwmst";
        $cond = "where lib_archive = '$archive' and lib_cate_id = $tmp_cid and wkfw_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            $sql = "select lib_cate_nm from libcate";
            $cond = "where lib_cate_id = $tmp_cid";
            $sel = select_from_table($con, $sql, $cond, $fname);
	        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
            $cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$cate_nm}」はワークフローの文書保存先として指定されているため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // カテゴリ直下の文書一覧を取得（古い版のみのはず）
        $sql = "select lib_id, lib_archive, lib_cate_id, lib_extension from libinfo";
        $cond = "where lib_cate_id = $tmp_cid and folder_id is null";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 文書一覧をループ
        while ($row = pg_fetch_array($sel)) {
            $tmp_old_lib_id = $row["lib_id"];
            $tmp_old_archive = $row["lib_archive"];
            $tmp_old_cate_id = $row["lib_cate_id"];
            $tmp_old_extension = $row["lib_extension"];

            // 最新版の保存先情報を取得
            $sql2 = "select i.lib_archive, i.lib_cate_id, i.folder_id from (select e1.lib_id, (select max(e2.lib_id) from libedition e2 where e2.base_lib_id = (select e3.base_lib_id from libedition e3 where e3.lib_id = $tmp_old_lib_id)) as current_lib_id from libedition e1) e inner join libinfo i on i.lib_id = e.current_lib_id";
            $cond2 = "";
            $sel2 = select_from_table($con, $sql2, $cond2, $fname);
	        if ($sel2 == 0) libcom_goto_error_page_and_die($sql2." ".$cond2, $con);

            $tmp_new_archive = pg_fetch_result($sel2, 0, "lib_archive");
            $tmp_new_cate_id = pg_fetch_result($sel2, 0, "lib_cate_id");
            $tmp_new_folder_id = pg_fetch_result($sel2, 0, "folder_id");

            // 古い版の保存先を最新版に合わせる
            if ($tmp_new_folder_id == "") {$tmp_new_folder_id = null;}
            $sql2 = "update libinfo set";
            $set2 = array("lib_archive", "lib_cate_id", "folder_id");
            $setvalue2 = array($tmp_new_archive, $tmp_new_cate_id, $tmp_new_folder_id);
            $cond2 = "where lib_id = $tmp_old_lib_id";
            $upd2 = update_set_table($con, $sql2, $set2, $setvalue2, $cond2, $fname);
	        if ($upd2 == 0) libcom_goto_error_page_and_die($sql2." ".$cond2, $con);

            // 移動ファイル情報を追加
            $move_files[$tmp_old_lib_id] = array(
                "old_archive" => $tmp_old_archive,
                "old_cate_id" => $tmp_old_cate_id,
                "lib_extension" => $tmp_old_extension,
                "new_archive" => $tmp_new_archive,
                "new_cate_id" => $tmp_new_cate_id
            );
        }

        // カテゴリデータを削除
        $sql = "delete from libcate";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 権限情報削除
        $sql = "delete from libcaterefdept";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libcaterefst";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libcaterefemp";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libcateupddept";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libcateupdst";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libcateupdemp";
        $cond = "where lib_cate_id = $tmp_cid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    }
}

// フォルダが選択された場合
if (is_array($fid)) {

    // 親フォルダ。カテゴリ直下なら親フォルダはカラである
    $parent_folder_id = lib_get_one($con, $fname, "select parent_id from libtree where child_id = ".(int)$fid[0]);

    // カテゴリ
    $parent_cate_id = lib_get_one($con, $fname, "select lib_cate_id from libfolder where folder_id = ".(int)$fid[0]);

    // 選択されたフォルダをループ
    foreach ($fid as $tmp_fid) {

        // フォルダ情報を取得
        $sql = "select * from libfolder";
        $cond = "where folder_id = $tmp_fid";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        $tmp_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");
        $folder_name = pg_fetch_result($sel, 0, "folder_name");
        $link_id = pg_fetch_result($sel, 0, "link_id");

        // 管理者以外でリンクフォルダを削除しようとした場合、エラーとする
        if (!empty($link_id) and $lib_admin_auth == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、管理者以外には削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // フォルダ直下にフォルダが存在した場合、エラーとする
        $sql = "select count(*) from libtree";
        $cond = "where parent_id = $tmp_fid";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、フォルダが存在するため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // フォルダがリンクされていた場合、エラーとする
        $sql = "select * from libfolder";
        $cond = "where link_id = $tmp_fid";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (pg_num_rows($sel) > 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、リンクフォルダが存在するため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // フォルダに最新版の文書が存在した場合、エラーとする
        $sql = "select count(*) from libinfo";
        $cond = "where folder_id = $tmp_fid and exists (select * from libedition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from libedition group by base_lib_id) latest_edition where latest_edition.base_lib_id = libedition.base_lib_id and latest_edition.edition_no = libedition.edition_no) and libedition.lib_id = libinfo.lib_id)";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、文書が存在するため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // カテゴリ直下の文書一覧を取得（古い版のみのはず）
        $sql = "select lib_id, lib_archive, lib_cate_id, lib_extension from libinfo";
        $cond = "where folder_id = $tmp_fid";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 文書一覧をループ
        while ($row = pg_fetch_array($sel)) {
            $tmp_old_lib_id = $row["lib_id"];
            $tmp_old_archive = $row["lib_archive"];
            $tmp_old_cate_id = $row["lib_cate_id"];
            $tmp_old_extension = $row["lib_extension"];

            // 最新版の保存先情報を取得
            $sql2 = "select i.lib_archive, i.lib_cate_id, i.folder_id from (select e1.lib_id, (select max(e2.lib_id) from libedition e2 where e2.base_lib_id = (select e3.base_lib_id from libedition e3 where e3.lib_id = $tmp_old_lib_id)) as current_lib_id from libedition e1) e inner join libinfo i on i.lib_id = e.current_lib_id";
            $cond2 = "";
            $sel2 = select_from_table($con, $sql2, $cond2, $fname);
	        if ($sel2 == 0) libcom_goto_error_page_and_die($sql2." ".$cond2, $con);

            $tmp_new_archive = pg_fetch_result($sel2, 0, "lib_archive");
            $tmp_new_cate_id = pg_fetch_result($sel2, 0, "lib_cate_id");
            $tmp_new_folder_id = pg_fetch_result($sel2, 0, "folder_id");

            // 古い版の保存先を最新版に合わせる
            if ($tmp_new_folder_id == "") {$tmp_new_folder_id = null;}
            $sql2 = "update libinfo set";
            $set2 = array("lib_archive", "lib_cate_id", "folder_id");
            $setvalue2 = array($tmp_new_archive, $tmp_new_cate_id, $tmp_new_folder_id);
            $cond2 = "where lib_id = $tmp_old_lib_id";
            $upd2 = update_set_table($con, $sql2, $set2, $setvalue2, $cond2, $fname);
	        if ($upd2 == 0) libcom_goto_error_page_and_die($sql2." ".$cond2, $con);

            // 移動ファイル情報を追加
            $move_files[$tmp_old_lib_id] = array(
                "old_archive" => $tmp_old_archive,
                "old_cate_id" => $tmp_old_cate_id,
                "lib_extension" => $tmp_old_extension,
                "new_archive" => $tmp_new_archive,
                "new_cate_id" => $tmp_new_cate_id
            );
        }
    }

    // フォルダデータを削除
    reset($fid);
    foreach ($fid as $tmp_fid) {

        // ワークフローの保存先として使われていたら削除不可とする
        $sql = "select count(*) from wkfwmst";
        $cond = "where lib_folder_id = $tmp_fid and wkfw_del_flg = 'f'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
        if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
            $sql = "select folder_name from libfolder";
            $cond = "where folder_id = $tmp_fid";
            $sel = select_from_table($con, $sql, $cond, $fname);
	        if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
            $folder_name = pg_fetch_result($sel, 0, "folder_name");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$folder_name}」はワークフローの文書保存先として指定されているため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        $sql = "delete from libtree";
        $cond = "where parent_id = $tmp_fid or child_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolder";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 権限情報削除
        $sql = "delete from libfolderrefdept";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolderrefst";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolderrefemp";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolderupddept";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolderupdst";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        $sql = "delete from libfolderupdemp";
        $cond = "where folder_id = $tmp_fid";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    }
}

// 削除対象の文書が存在する場合
if (is_array($did) && count($did) > 0) {
    $del_docs = array();
    // ログインユーザの情報を取得
    $sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
    $cond = "where emp_id = (select emp_id from session where session_id = '$session')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    $emp_id = pg_fetch_result($sel, 0, "emp_id");
    $emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

    // 親フォルダ。カテゴリ直下なら親フォルダはカラである
    $parent_folder_id = lib_get_one($con, $fname, "select folder_id from libinfo where lib_id = ".(int)$did[0]);
    // カテゴリ
    $parent_cate_id = lib_get_one($con, $fname, "select lib_cate_id from libinfo where lib_id = ".(int)$did[0]);

    // 削除対象の文書情報を配列に保存
    $did_list = implode(", ", $did);
    $sql = "select i.lib_id, i.lib_nm, i.lib_archive, i.lib_cate_id, c.lib_cate_nm, i.folder_id, f.folder_name from libinfo i inner join libcate c on c.lib_cate_id = i.lib_cate_id left join libfolder f on f.folder_id = i.folder_id";
    $cond = "where i.lib_delete_flag = 'f' and i.lib_id in (select e1.lib_id from libedition e1 where e1.base_lib_id in (select e2.base_lib_id from libedition e2 where e2.lib_id in ($did_list)))";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    while ($row = pg_fetch_array($sel)) {
        $del_docs[] = array(
            "id" => $row["lib_id"],
            "name" => $row["lib_nm"],
            "archive" => $row["lib_archive"],
            "category" => $row["lib_cate_id"],
            "category_name" => $row["lib_cate_nm"],
            "folder" => $row["folder_id"],
            "folder_name" => $row["folder_name"]
        );
    }

    // 文書データを削除
    foreach ($del_docs as $tmp_doc) {

        // 文書削除ログを追加
        $tmp_detail = "文書ID：{$tmp_doc["id"]},文書名：{$tmp_doc["name"]},書庫：{$tmp_doc["archive"]},カテゴリID：{$tmp_doc["category"]},カテゴリ名：{$tmp_doc["category_name"]},フォルダID：{$tmp_doc["folder"]},フォルダ名：{$tmp_doc["folder_name"]}";
        $sql = "insert into oplog (op_time, op_emp_id, op_emp_nm, optype_id, op_detail) values (";
        $content = array(date("YmdHis"), $emp_id, $emp_nm, 1, pg_escape_string($tmp_detail));
        $ins = insert_into_table($con, $sql, $content, $fname);
	    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);

        // 文書参照ログを削除
        $sql = "delete from libreflog";
        $cond = "where lib_id = {$tmp_doc["id"]}";
        $del = delete_from_table($con, $sql, $cond, $fname);
	    if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 版情報を削除
        $sql = "delete from libedition";
        $cond = "where lib_id = {$tmp_doc["id"]}";
        $del = delete_from_table($con, $sql, $cond, $fname);
	    if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

        // 文書情報を削除
        $sql = "delete from libinfo";
        $cond = "where lib_id = {$tmp_doc["id"]}";
        $del = delete_from_table($con, $sql, $cond, $fname);
	    if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

    }
}


// フォルダ更新日時を更新
lib_update_folder_modified($con, $parent_cate_id, $parent_folder_id, $fname, $emp_id, $archive);


// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 古い版のファイルを移動
foreach ($move_files as $tmp_lib_id => $tmp_move_file) {
    $tmp_old_file_path = get_document_path($tmp_move_file["old_archive"], $tmp_move_file["old_cate_id"], $tmp_lib_id, $tmp_move_file["lib_extension"]);
    $tmp_new_file_path = get_document_path($tmp_move_file["new_archive"], $tmp_move_file["new_cate_id"], $tmp_lib_id, $tmp_move_file["lib_extension"]);
    rename($tmp_old_file_path, $tmp_new_file_path);
}

// 物理ファイルを削除
$cwd = getcwd();
if (is_array($del_docs)) {
    reset($del_docs);
    foreach ($del_docs as $tmp_doc) {
        switch ($tmp_doc["archive"]) {
        case "2":  // 全体共有
            $dirname = "all";
            break;
        case "3":  // 部署共有
            $dirname = "section";
            break;
        case "4":  // 委員会・WG共有
            $dirname = "project";
            break;
        case "1":  // 個人ファイル
            $dirname = "private";
            break;
        }
        exec("rm -f {$cwd}/docArchive/{$dirname}/cate{$tmp_doc["category"]}/document{$tmp_doc["id"]}.*");
    }
}
if (is_array($cid)) {
    switch ($archive) {
    case "2":  // 全体共有
        $dirname = "all";
        break;
    case "3":  // 部署共有
        $dirname = "section";
        break;
    case "4":  // 委員会・WG共有
        $dirname = "project";
        break;
    case "1":  // 個人ファイル
        $dirname = "private";
        break;
    }
    reset($cid);
    foreach ($cid as $tmp_cid) {
        exec("rm -fr {$cwd}/docArchive/$dirname/cate{$tmp_cid}");
    }
}
// 画面遷移
if ($path == "2") {
    echo("<script type=\"text/javascript\">location.href = 'library_admin_refer_log.php?session=$session&sort=$sort&page=$page';</script>");
} else if ($path == "3") {
    echo("<script type=\"text/javascript\">location.href = 'library_admin_menu.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
} else if ($ret_url != "") {
    echo("<script type=\"text/javascript\">location.href = '$ret_url';</script>");
} else {
    echo("<script type=\"text/javascript\">location.href = 'library_list_all.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
}
?>