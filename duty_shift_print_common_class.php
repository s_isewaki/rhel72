<!-- ************************************************************************ -->
<!-- 勤務シフト作成　印刷共通部品ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");

class duty_shift_print_common_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;		// 勤務シフト共通クラス
	var $obj_duty;	// 当直共通クラス

	var $default_width = "24";
	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_print_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
		$this->obj_duty = new duty_shift_duty_common_class($con, $fname);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（印刷）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	//
	// 見出し（日段）
	//
	/*************************************************************************/
	function showTitleWeek(
						$duty_yyyy,			//勤務年
						$duty_mm,			//勤務月
						$day_cnt,			//日数
						$plan_array,		//勤務シフト情報
						$week_array,		//週情報
						$title_gyo_array,	//行合計タイトル情報
						$calendar_array,	//カレンダー情報
						$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
						$show_data_flg		//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直）
	){
		$reason_2_array = $this->obj->get_reason_2();

		//-------------------------------------------------------------------------------
		//日段
		//-------------------------------------------------------------------------------
		echo("<tr height=\"22\"> \n");
		echo("<td width=\"20\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">番号</font></td> \n");
		echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">チーム</font></td> \n");
		echo("<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">氏名</font></td> \n");
		echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td> \n");
		echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">役職</font></td> \n");
		echo("<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">雇用・勤務形態</font></td> \n"); //20130219
		echo("<td width=\"32\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td> \n");
		//勤務状況（日）
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk_date = $calendar_array[$k-1]["date"];
			$wk_dd = (int)substr($wk_date,6,2);
			//$wk_width = 20;
			$wk_width = $this->default_width;
			echo("<td width=\"$wk_width\" align=\"center\" colspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_dd</font></td>\n");
		}
		//集計行列の印刷をする場合
		if ($total_print_flg == "1") {
			//個人日数
			for ($k=0; $k<count($title_gyo_array); $k++) {
				$wk = $title_gyo_array[$k]["name"];
				if ($title_gyo_array[$k]["id"] == "10") {
					//公休、有給表示
					echo("<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n");
					echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">公休</font></td> \n");
					echo("<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n");
					echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">年休<br>有休</font></td> \n");
				}
				echo("<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n");
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n");
			}
		}
		echo("</tr> \n");
		//-------------------------------------------------------------------------------
		//曜日段
		//-------------------------------------------------------------------------------
		echo("<tr height=\"22\"> \n");
		//勤務状況（曜日）
		for ($k=1; $k<=$day_cnt; $k++) {
			//土、日、祝日の文字色変更
			$wk_color = "";
			if ($week_array[$k]["name"] == "土"){
				$wk_color = "#0000ff";
			} else if ($week_array[$k]["name"] == "日") {
				$wk_color = "#ff0000";
			} else {
				if (($calendar_array[$k-1]["type"] == "4") ||
					($calendar_array[$k-1]["type"] == "5") ||
					($calendar_array[$k-1]["type"] == "6")) {
					$wk_color = "#ff0000";
				}
			}
			//表示
			$wk_name = $week_array[$k]["name"];
			//$wk_width = 20;
			$wk_width = $this->default_width;
			//			for ($i=0; $i<count($plan_array); $i++) {
//			for ($m=0; $m<count($reason_2_array); $m++) {
//				if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"]) {
//					$wk_width = 30;
//				}
//				if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"]) {
//					$wk_width = 30;
//				}
//			}
//			}
			echo("<td width=\"$wk_width\" align=\"center\" colspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=$wk_color>$wk_name</font></td> \n");
		}
		echo("</tr> \n");
	}
	/*************************************************************************/
	//
	// 表データ
	//
	/*************************************************************************/
	function showList(
					$pattern_id,			//出勤パターンＩＤ
					$plan_array,			//勤務シフト情報
					$title_gyo_array,		//行合計タイトル情報
					$gyo_array,				//行合計情報
					$start_cnt,				//行表示開始位置
					$end_cnt,				//行表示終了位置
					$day_cnt,				//日数
					$max_font_cnt,			//最大文字数
					$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
					$show_data_flg="",			//２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直）
					$group_id,
					$title_hol_array,		//休暇タイトル情報
					$hol_cnt_array,			//休暇日数
					$calendar_array			//カレンダー情報
	){
		// 当日
		$today = Date("Ymd");

		$wk_height = 22 * (int)$max_font_cnt;
		$reason_2_array = $this->obj->get_reason_2();

		//当直パターン取得
		$duty_pattern =	$this->obj_duty->get_duty_pattern_array();

		// 役職・職種・雇用勤務形態の履歴情報 20130219
		$wk_group = array();
		$wk_group[0] = array("job_disp_flg"=>"t", "st_disp_flg"=>"t", "es_disp_flg"=>"t");
		$history_info = $this->obj->get_history_info($calendar_array, $wk_group);

		for ($i=$start_cnt; $i<$end_cnt; $i++) {
			//*******************************************************************************
			// 予定データ
			//*******************************************************************************
//			echo("<tr height=\"$wk_height\"> \n");
			echo("<tr> \n");
			if ($show_data_flg != "") {
				$span = "2";
			} else {
				$span = "1";
			}
			//-------------------------------------------------------------------------------
			// 表示順
			//-------------------------------------------------------------------------------
			$wk = $plan_array[$i]["no"];
			echo("<td width=\"20\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// チーム
			//-------------------------------------------------------------------------------
			// チームの背景色
			$wk_color = "";
			if ($plan_array[$i]["other_staff_flg"] == "1") {
				//他病棟スタッフ
				$wk_color = "#dce8bb";
			}

			$wk = $plan_array[$i]["team_name"];
			echo("<td width=\"40\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>");
			//-------------------------------------------------------------------------------
			// 氏名
			//-------------------------------------------------------------------------------
			// 氏名の背景色
			$wk_color = "";
			if ($plan_array[$i]["other_staff_flg"] == "1") {
				$wk_color = "#dce8bb"; //他病棟スタッフ
			}
			// 氏名
			$wk = $plan_array[$i]["staff_name"];
			$color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
			echo("<td id=\"$wk_td_id\" width=\"100\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 職種
			//-------------------------------------------------------------------------------
			$staff_id = $plan_array[$i]["staff_id"];

			//職種履歴を考慮する 20130219
			$arr_name_color = $this->obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
			$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];
			$es_color = ($arr_name_color["color"] !="") ? "#" . $arr_name_color["color"] : "black";

			echo("<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$es_color\" >$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 役職
			//-------------------------------------------------------------------------------
			//役職履歴を考慮する 20130219
			$arr_name_color = $this->obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
			$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];
			$es_color = ($arr_name_color["color"] !="") ? "#" . $arr_name_color["color"] : "black";
			echo("<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$es_color\" >$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 雇用・勤務形態 20130219
			//-------------------------------------------------------------------------------
			$arr_wk = array("1"=>"常勤","2"=>"非常勤","3"=>"短時間正職員");
			$buf_wk = $plan_array[$i]["duty_form"];
			$arr_name_color = $this->obj->get_show_name($history_info["duty_form_info"][$staff_id], $arr_wk, "duty_form", $history_info["start_date"], $history_info["end_date"]);
			$wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $arr_wk[$buf_wk];
			$es_color = ($arr_name_color["color"] !="") ? "#" . $arr_name_color["color"] : "black";
			echo("<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$es_color\" >$wk</font></td>\n");

			echo("<td width=\"32\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">予定</font></td>\n");

			//-------------------------------------------------------------------------------
			// 勤務状況
			//-------------------------------------------------------------------------------
			for ($k=1; $k<=$day_cnt; $k++) {
				//勤務シフト予定（表示）
				if ($plan_array[$i]["pattern_id_$k"] == $pattern_id &&
					 ($plan_array[$i]["assist_group_$k"] == "" ||
					  $plan_array[$i]["assist_group_$k"] == $group_id)) {
					$wk_font_color = $plan_array[$i]["font_color_$k"];
					$wk_back_color = $plan_array[$i]["back_color_$k"];
				} else {
					$wk_font_color = "#C0C0C0";		//灰色
					$wk_back_color = "";
				}
				//名称
				$wk = $plan_array[$i]["font_name_$k"];
				$fsize = (strlen($wk) > 2) ? "j10" : "j12";
				//$wk_width = 20;
				$wk_width = $this->default_width;
				echo("<td width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fsize\" color=\"$wk_font_color\" >$wk</font></td>\n");
			}
			//集計行列の印刷をする場合
			if ($total_print_flg == "1") {
				if ($show_data_flg == "3") {
					$span = "2";
				} else {
					$span = "1";
				}
				//公休・年休表示
				$wk_koukyu_cnt = 0;
				$wk_nenkyu_cnt = 0;
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休表示
					if ($title_hol_array[$k]["reason"] == "1") {
						$wk_nenkyu_cnt = $hol_cnt_array[$i][$k];
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						$wk_koukyu_cnt = $hol_cnt_array[$i][$k];
					}
				}
				//-------------------------------------------------------------------------------
				//個人日数
				//-------------------------------------------------------------------------------
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = $gyo_array[$i][$k];
					if ($title_gyo_array[$k]["id"] == "10") {
						//公休、有休表示
						echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td> \n");
						echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td> \n");
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n");
				}
			}
			echo("</tr> \n");
			//*******************************************************************************
			// 実績データ
			//*******************************************************************************
			if ($show_data_flg != "") {
				echo("<tr> \n");
				switch ($show_data_flg) {
				case "1":
					$tmp_title = "実績";
					break;
				case "2":
					$tmp_title = "希望";
					break;
				case "3":
					$tmp_title = $this->obj->duty_or_oncall_str;
					break;
				case "4":
					$tmp_title = "ｺﾒﾝﾄ";
					break;
				default:
					$tmp_title = "";
				}
				echo("<td width=\"32\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_title</font></td>\n");
				//勤務状況（実績）
				for ($k=1; $k<=$day_cnt; $k++) {

					if($show_data_flg == "4"){
						$wk = h($plan_array[$i]["comment_$k"]); //コメント
                        //文字、背景色クリア 20130701
                        $wk_font_color = "";
                        $wk_back_color = "";
                    } else if ($show_data_flg != "3") {
						//文字、背景色
						if (/*$plan_array[$i]["rslt_pattern_id_$k"] == $pattern_id &&*/
							 ($plan_array[$i]["assist_group_$k"] == "" ||
							  $plan_array[$i]["assist_group_$k"] == $group_id)) {
							$wk_font_color = $plan_array[$i]["rslt_font_color_$k"];
							$wk_back_color = $plan_array[$i]["rslt_back_color_$k"];
						} else {
							$wk_font_color = "#C0C0C0";		//灰色
							$wk_back_color = "";
						}
						//勤務実績
						$wk = $plan_array[$i]["rslt_name_$k"];
						//退勤がない場合の表示20100820
						if (($plan_array[$i]["rslt_start_time_$k"] != "" &&
							$plan_array[$i]["rslt_end_time_$k"] == "") ||
							($plan_array[$i]["rslt_start_time_$k"] == "" && //出勤がない場合 20110124
							$plan_array[$i]["rslt_end_time_$k"] != "")) {
							$wk .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" color=\"red\" >・</font>";
						}
					} else {
						$wk = "&nbsp;"; // "_" -> " "
						$wk_font_color = "";
						$wk_back_color = "";
						for ($m=0; $m<count($duty_pattern); $m++) {
							if ($plan_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
								$wk = $duty_pattern[$m]["font_name"];	//表示文字名
								$wk_font_color = $duty_pattern[$m]["font_color"];
								$wk_back_color = $duty_pattern[$m]["back_color"];
							}
						}
					}
					$fsize = (strlen($wk) > 2) ? "j10" : "j12";
					//$wk_width = 20;
					$wk_width = $this->default_width;
					echo("<td width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$fsize\" color=\"$wk_font_color\" >$wk</font></td>\n");
				}
				//２段目の集計
				if ($total_print_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
					$gyo_array_cnt = count($title_gyo_array);
					$hol_array_cnt = count($title_hol_array);

					//公休・年休表示
					$wk_koukyu_cnt = 0;
					$wk_nenkyu_cnt = 0;
					for ($k=0; $k<count($title_hol_array); $k++) {
						//公休・年休表示
						if ($title_hol_array[$k]["reason"] == "1") {
							$wk_nenkyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
						}
						if ($title_hol_array[$k]["reason"] == "24") {
							$wk_koukyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
						}
					}
					for ($k=0; $k<count($title_gyo_array); $k++) {
						$wk = $gyo_array[$i][$gyo_array_cnt + $k];
						if ($title_gyo_array[$k]["id"] == "10") {
							//公休、年休表示
							echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td> \n");
							echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td> \n");
							$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
						}
						echo("<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n");
					}
				}
				echo("</tr> \n");
			}
		}
	}
	/*************************************************************************/
	//
	// 列計
	//
	/*************************************************************************/
	function showTitleRetu(
						$day_cnt,				//日数
						$plan_array,			//勤務シフト情報
						$title_gyo_array,		//行合計タイトル情報
						$gyo_array,				//行合計情報
						$title_retu_array,		//列合計タイトル情報
						$retu_array,			//列合計情報
						$total_gyo_flg="1",		// 2:2段 2以外:1段
						$title_hol_array,		//休暇タイトル情報
						$hol_cnt_array			//休暇日数
	){
		$reason_2_array = $this->obj->get_reason_2();

		for ($i=0; $i<count($title_retu_array); $i++) {
			echo("<tr height=\"22\"> \n");
			echo("<td width=\"20\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n");	// 番号（プランク）
			echo("<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n");
			//-------------------------------------------------------------------------------
			// 名称
			//-------------------------------------------------------------------------------
			$wk = $title_retu_array[$i]["name"];
			echo("<td width=\"100\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			//-------------------------------------------------------------------------------
			// 総合計
			//-------------------------------------------------------------------------------
			$wk_cnt = 0;
			for ($k=1; $k<=$day_cnt; $k++) {
				$wk_cnt += $retu_array[$k][$i];
			}
			echo("<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n");
			echo("<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n");
			echo("<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">  </font></td>\n"); //雇用勤怠形態 20130219
			echo("<td width=\"32\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_cnt</font></td>\n");
			//-------------------------------------------------------------------------------
			// 日ごとの合計
			//-------------------------------------------------------------------------------
			for ($k=1; $k<=$day_cnt; $k++) {
				$wk = $retu_array[$k][$i];
				//$wk_width = 20;
				$wk_width = $this->default_width;
				for ($m=0; $m<count($reason_2_array); $m++) {
					if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
						$wk_width = 30;
						break;
					}
					if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
						$wk_width = 30;
						break;
					}
				}
				echo("<td width=\"$wk_width\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
			}
			//-------------------------------------------------------------------------------
			// 個人日数
			//-------------------------------------------------------------------------------
			if (($total_gyo_flg != "2" && $i == 0) || ($total_gyo_flg == "2" && $i <= 1)) {
				$wk_nenkyu_cnt = 0;
				$wk_koukyu_cnt = 0;
				//集計
				for ($k=0; $k<count($title_hol_array); $k++) {
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_hol_array) + $k;
					}

					if ($title_hol_array[$k]["reason"] == "1") {
						for ($m=0; $m<count($plan_array); $m++) {
							$wk_nenkyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						for ($m=0; $m<count($plan_array); $m++) {
							$wk_koukyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
				}
				// 個人日数の列合計
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = 0;
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_gyo_array) + $k;
					}
					for ($m=0; $m<count($gyo_array); $m++) {
						$wk = $wk + $gyo_array[$m][$k2];
					}
					//公休、有休表示
					if ($title_gyo_array[$k]["id"] == "10") {
						echo("<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td>\n");
						echo("<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td>\n");
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					echo("<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n");
				}
			} else {
				// ブランク（個人日数用）
				for ($k=0; $k<count($title_gyo_array); $k++) {
					echo("<td width=\"30\" align=\"center\" style=\"border:none;\"></td>\n");
				}
			}
			echo("</tr>\n");
		}
	}

}
?>



