<?php
/* 勤務シフト作成　管理帳票共通部品ＣＬＡＳＳ */
require_once("about_comedix.php");

class duty_shift_mprint_common_class
{
    var $file_name;	// 呼び出し元ファイル名
    var $_db_con;	// DBコネクション
    
    /*************************************************************************/
    // コンストラクタ
    // @param object $con DBコネクション
    // @param string $fname 画面名
    /*************************************************************************/
    function duty_shift_mprint_common_class($con, $fname)
    {
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
    }


    /**
     * 勤務シフト管理帳票職員
     *
     * @return array 職員情報の配列、no,職員ID,職員名
     *
     */
    function get_duty_shift_staff_array() {
        //-------------------------------------------------------------------
        //勤務シフト管理帳票職員より情報取得
        //-------------------------------------------------------------------
        $sql = "select a.no, a.emp_id, b.emp_lt_nm || ' ' || b.emp_ft_nm as name from duty_shift_mprint_staff a "
        ." left join empmst b on b.emp_id = a.emp_id";
        $cond = "order by no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        $i = 0;
        while ($row = pg_fetch_array($sel)) {
            $data[$i]["no"] = $row["no"];
            $data[$i]["id"] = $row["emp_id"];				//職員ＩＤ
            $data[$i]["name"] = $row["name"];				//職員名
            $i++;
        }
        return $data;
    }


    /**
     * 指定職員が全て集計可能か確認
     *
     * @param string $emp_id 職員ID
     * @return boolean true:可能、false:不可
     *
     */
    function is_duty_shift_mprint_staff($emp_id) {
        //-------------------------------------------------------------------
        //勤務シフト管理帳票職員より情報取得
        //-------------------------------------------------------------------
        $sql = "select count(*) as cnt from duty_shift_mprint_staff ";
        $cond = "where emp_id = '$emp_id' ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $cnt = pg_fetch_result($sel, 0, "cnt");
        
        return ($cnt > 0);
    }
    
    /**
     * 勤務シフト管理帳票職員名取得
     *
     * @param array $set_array 職員情報の配列
     * @return array 職員IDをキーに職員名を値とする配列を返す
     *
     */
    function get_duty_shift_staff_name($set_array) {
        
        $data = array();
        if (count($set_array) == 0) {
            return $data;
        }
        
        $emp_id_str = "";
        for ($i=0; $i<count($set_array); $i++) {
            if ($i != 0 ) {
                $emp_id_str .= ",";
            }
            $emp_id_str .= "'".$set_array[$i]["id"]."'";
        }
        //-------------------------------------------------------------------
        //職員マスタより情報取得
        //-------------------------------------------------------------------
        $sql = "select a.emp_id, a.emp_lt_nm || ' ' || a.emp_ft_nm as name from empmst a ";
        $cond = "where emp_id in ($emp_id_str) ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        while ($row = pg_fetch_array($sel)) {
            $wk_emp_id = $row["emp_id"];
            $data["$wk_emp_id"] = $row["name"];					//職員名
        }
        return $data;
    }
    
    /**
     * 勤務シフト管理帳票情報取得
     *
     * @return array 管理帳票情報の配列 no,ID,名称
     *
     */
    function get_duty_shift_layout_mst() {
        //-------------------------------------------------------------------
        //勤務シフト管理帳票名称より情報取得
        //-------------------------------------------------------------------
        $sql = "select a.* from duty_shift_mprint_layout_mst a ";
        $cond = "order by no";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        $data = array();
        $i = 0;
        while ($row = pg_fetch_array($sel)) {
            $data[$i]["no"] = $row["no"];
            $data[$i]["layout_id"] = $row["layout_id"];
            $data[$i]["layout_name"] = $row["layout_name"];	
            $i++;
        }
        return $data;
    }
    
    /**
     * 指定IDの勤務シフト管理帳票情報取得
     *
     * @param string $layout_id レイアウトID
     * @return array 管理帳票情報の配列 no,ID,名称
     *
     */
    function get_duty_shift_layout_mst_one($layout_id) {
        
        $data = array();
        if ($layout_id == "") {
            return $data;
        }        
        //-------------------------------------------------------------------
        //勤務シフト管理帳票名称より情報取得
        //-------------------------------------------------------------------
        $sql = "select a.* from duty_shift_mprint_layout_mst a ";
        $cond = "where a.layout_id = $layout_id ";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        //-------------------------------------------------------------------
        //情報設定
        //-------------------------------------------------------------------
        while ($row = pg_fetch_array($sel)) {
            $data["no"] = $row["no"];
            $data["layout_id"] = $row["layout_id"];
            $data["layout_name"] = $row["layout_name"];	
        }
        return $data;
    }

}