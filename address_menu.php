<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix アドレス帳 | アドレス帳一覧</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");

?>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("./show_address.ini"); ?>
<?
$fname=$PHP_SELF;

$session=qualify_session($session,$fname);
if($session=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$adbk=check_authority($session,9,$fname);
if($adbk=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// アドレス帳管理権限を取得
$adbk_admin_auth=check_authority($session,64,$fname);

if ($search_submit == "") {
	$search_submit = "検索";
}

$admin_flg = "f";
?>
<script language="JavaScript" type="text/JavaScript">
function checkValue(){
	var res=confirm("削除してよろしいですか？");
	if(res==true){
		document.address.action="./address_delete.php";
		document.address.submit();
	}
}

function sharedFlgOnchange(){
	if (document.search.search_shared_flg.value == '4') {
		document.search.search_item_flg.value = '1';
		document.search.search_item_flg.disabled = true;
	} else {
		document.search.search_item_flg.disabled = false;
	}
}

function downloadCSV() {
	document.csv.action = 'address_menu_csv.php';
	document.csv.submit();
}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="address_menu.php?session=<? echo($session); ?>"><img src="img/icon/b13.gif" width="32" height="32" border="0" alt="アドレス帳"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<? echo($session); ?>"><b>アドレス帳</b></a></font></td>
<? if ($adbk_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="./address_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>アドレス一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規作成</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7">
<?
echo ("<a href=\"address_contact_update.php?session=$session&emp_id=&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg\">");
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本人連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="address_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ作成</font></a></td>
<td width="">&nbsp;</td>
<td width="100" align="center"><input type="button" value="削除" onclick="checkValue()"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="search" action="address_menu.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="search_shared_flg" onchange="sharedFlgOnchange();">
<option value="1"<? if ($search_shared_flg == "1") {echo(" selected");} ?>>全て(個人・共有)</option>
<option value="2"<? if ($search_shared_flg == "2") {echo(" selected");} ?>>個人アドレス帳</option>
<option value="3"<? if ($search_shared_flg == "3") {echo(" selected");} ?>>共有アドレス帳</option>
<option value="4"<? if ($search_shared_flg == "4") {echo(" selected");} ?>>職員連絡先</option>
</select>
&nbsp;
<select name="search_item_flg" <? if ($search_shared_flg == "4") {echo(" disabled");} ?>>
<option value="1"<? if ($search_item_flg == "1") {echo(" selected");} ?>>氏名</option>
<option value="2"<? if ($search_item_flg == "2") {echo(" selected");} ?>>会社名</option>
<option value="3"<? if ($search_item_flg == "3") {echo(" selected");} ?>>部門名</option>
</select>
<input type="text" name="search_name" value="<? echo($search_name); ?>" style="ime-mode: active;" size="40">&nbsp;<input type="submit" name="search_submit" value="検索">&nbsp;&nbsp;&nbsp;
<?
//<input type="submit" name="search_submit" value="全表示">
?>
</font></td>
<td align="right">
<?
// CSV出力
if ($search_shared_flg == "2") {
?>
<input type="button" value="CSV出力" onclick="downloadCSV();">&nbsp;&nbsp;
<? } ?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<?
// 初期表示時は一覧表示しない
 if ($search_shared_flg != "") { ?>
<form name="address" method="post">
<div align="right"><? showNext($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg); ?></div>
<? show_address($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg); ?>
<div align="right"><? showNext($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg); ?></div>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="search_shared_flg" value="<? echo($search_shared_flg); ?>">
<input type="hidden" name="search_name" value="<? echo($search_name); ?>">
<input type="hidden" name="search_submit" value="<? echo($search_submit); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="admin_flg" value="<? echo($admin_flg); ?>">
<input type="hidden" name="search_item_flg" value="<? echo($search_item_flg); ?>">
</form>
<? } ?>
<?
// CSV出力用フォーム
if ($search_shared_flg == "2") {
?>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="search_shared_flg" value="<? echo($search_shared_flg); ?>">
<input type="hidden" name="search_name" value="<? echo($search_name); ?>">
<input type="hidden" name="admin_flg" value="<? echo($admin_flg); ?>">
<input type="hidden" name="search_item_flg" value="<? echo($search_item_flg); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>
</td>
</tr>
</table>
</body>
</html>