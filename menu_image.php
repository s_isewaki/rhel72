<?
require("about_postgres.php");
require("about_session.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// プロフィール画像保存フォルダがなければ作成
if (!is_dir("profile")) {
	mkdir("profile", 0755);
	mkdir("profile/img", 0755);
}

// 職員IDの取得
$con = connect2db($fname);
$emp_id = get_emp_id($con, $session, $fname);
pg_close($con);

// 当該職員の画像が登録済みであれば出力
//// GIF
if (is_file("profile/img/$emp_id.gif")) {
	$fp = fopen("profile/img/$emp_id.gif", "r");
	if ($fp) {
		header("Content-Type: image/gif");
		while (!feof($fp)) {
			echo fgetc($fp);
		}
		fclose($fp);
	} else {
		echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
	}
	exit;
}
//// JPEG
if (is_file("profile/img/$emp_id.jpg")) {
	$fp = fopen("profile/img/$emp_id.jpg", "r");
	if ($fp) {
		header("Content-Type: image/pjepg");
		while (!feof($fp)) {
			echo fgetc($fp);
		}
		fclose($fp);
	} else {
		echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
	}
	exit;
}
//// PNG
if (is_file("profile/img/$emp_id.png")) {
	$fp = fopen("profile/img/$emp_id.png", "r");
	if ($fp) {
		header("Content-Type: image/x-png");
		while (!feof($fp)) {
			echo fgetc($fp);
		}
		fclose($fp);
	} else {
		echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
	}
	exit;
}

/// 未登録のため空白画像を出力する
$fp = fopen("img/spacer.gif", "r");
if ($fp) {
	header("Content-Type: image/gif");
	while (!feof($fp)) {
		echo fgetc($fp);
	}
	fclose($fp);
} else {
	echo("<meta http-equiv='Content-Type' content='text/html; charset=EUC-JP'>");
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
}

function get_emp_id($con, $session, $fname){

require_once("./about_postgres.php");
require("./conf/sql.inf");

	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $SQL1, $cond, $fname);
	if ($sel == 0) {
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	return $emp_id;

}
?>
