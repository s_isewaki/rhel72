<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="Refresh" content="300">
<title>病床管理 | 掲示板 | 投稿一覧</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 表示順のデフォルトを「ツリー表示（新しい順）」とする
if ($sort == "") {$sort = "1";}

// 期間のデフォルトを「1日」とする
if ($term == "") {$term = "1day";}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 表示設定（とりあえず固定）
$newpost_days = 1;
$count_per_page = 15;

// カテゴリIDとテーマIDはとりあえず固定
$category_id = 1;
$theme_id = 1;
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
function changeSortType(sort) {
	location.href = 'bed_menu_status_bbs.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&theme_id=<? echo($theme_id); ?>&sort=' + sort + '&show_content=<? echo($show_content); ?>&term=<? echo($term); ?>';
}

function changeContentView(checked) {
	var show_content = (checked) ? 't' : '';
	location.href = 'bed_menu_status_bbs.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&page=<? echo($page); ?>&term=<? echo($term); ?>&show_content=' + show_content;
}

function postComment() {
<? if ($theme_id == "") { ?>
	alert('テーマが選択されていません。');
<? } else { ?>
	location.href = 'bed_menu_status_bbs_post_register.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>&term=<? echo($term); ?>';
<? } ?>
}

function getCheckedIds(boxes) {
	var checked_ids = new Array();
	if (boxes) {
		if (!boxes.length) {
			if (boxes.checked) {
				checked_ids.push(boxes.value);
			}
		} else {
			for (var i = 0, j = boxes.length; i < j; i++) {
				if (boxes[i].checked) {
					checked_ids.push(boxes[i].value);
				}
			}
		}
	}
	return checked_ids;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>投稿一覧</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
以前の投稿
<span style="margin-left:20px;"><? if ($term != "1day") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=1day\">");} ?>1日<? if ($term != "1day") {echo("</a>");} ?></span>
<span style="margin-left:10px;"><? if ($term != "1week") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=1week\">");} ?>1週間<? if ($term != "1week") {echo("</a>");} ?></span>
<span style="margin-left:10px;"><? if ($term != "2week") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=2week\">");} ?>2週間<? if ($term != "2week") {echo("</a>");} ?></span>
<span style="margin-left:10px;"><? if ($term != "1month") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=1month\">");} ?>1ヶ月<? if ($term != "1month") {echo("</a>");} ?></span>
<span style="margin-left:10px;"><? if ($term != "1year") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=1year\">");} ?>1年<? if ($term != "1year") {echo("</a>");} ?></span>
<span style="margin-left:10px;"><? if ($term != "all") {echo("<a href=\"bed_menu_status_bbs.php?session=$session&category_id=$category_id&theme_id=$theme_id&sort=$sort&show_content=$show_content&term=all\">");} ?>すべて<? if ($term != "all") {echo("</a>");} ?></span>
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0" class="list">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="20%" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font></td>
<td>
<select name="sort" onchange="changeSortType(this.value);">
<option value="1"<? if ($sort == "1") {echo(" selected");} ?>>ツリー表示（新しい順）
<option value="2"<? if ($sort == "2") {echo(" selected");} ?>>ツリー表示（古い順）
<option value="3"<? if ($sort == "3") {echo(" selected");} ?>>投稿順表示（新しい順）
<option value="4"<? if ($sort == "4") {echo(" selected");} ?>>投稿順表示（古い順）
</select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="show_content" value="t"<? if ($show_content == "t") {echo(" checked");} ?> onclick="changeContentView(this.checked);">内容を表示する</font></td>
<td align="right"><input type="button" value="新しい投稿" onclick="postComment();"></td>
</tr>
</table>
</td>
</tr>
<tr height="328">
<td valign="top" style="padding:0 2px;">
<? show_post_list($con, $theme_id, $sort, $newpost_days, $count_per_page, $page, $show_content, $term, $emp_id, $session, $fname); ?>
</td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 投稿一覧を表示
function show_post_list($con, $theme_id, $sort, $newpost_days, $count_per_page, $page, $show_content, $term, $emp_id, $session, $fname) {
	if ($theme_id == "") {
		return;
	}

	// ページのデフォルトは1
	if ($page == 0) {$page = 1;}

	switch ($sort) {
	case "1":  // ツリー表示（新しい順）
		$posts = get_posts_tree($con, $theme_id, true, $term, $fname);
		show_posts_tree($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $count_per_page, $page, $term);
		write_navigation_tree($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname, $posts);
		break;

	case "2":  // ツリー表示（古い順）
		$posts = get_posts_tree($con, $theme_id, false, $term, $fname);
		show_posts_tree($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $count_per_page, $page, $term);
		write_navigation_tree($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname, $posts);
		break;

	case "3":  // 投稿順表示（新しい順）
		$posts = get_posts_flat($con, $theme_id, true, $count_per_page, $page, $term, $fname);
		show_posts_flat($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $term);
		write_navigation_flat($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname);
		break;

	case "4":  // 投稿順表示（古い順）
		$posts = get_posts_flat($con, $theme_id, false, $count_per_page, $page, $term, $fname);
		show_posts_flat($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $term);
		write_navigation_flat($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname);
		break;
	}
}

// ツリー表示用の投稿一覧を取得
function get_posts_tree($con, $theme_id, $id_desc_flg, $term, $fname, $parent_id = 0, $nest = 0) {
	$sql = "select bbs.bbs_id, bbs.bbs_title, bbs.date, deptmst.dept_nm, classroom.room_nm, empmst.emp_lt_nm, empmst.emp_ft_nm, bbs.bbs_del_flg, bbs.del_by_self, bbs.bbs, bbs.emp_id from bedbbs bbs inner join empmst on bbs.emp_id = empmst.emp_id inner join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room";
	$cond = "where bbs.bbsthread_id = $theme_id and bbs.bbs_towhich_id = $parent_id order by bbs.date";
	if ($id_desc_flg) {
		$cond .= " desc";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$posts = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_post_id = $row["bbs_id"];

		$posts[] = array(
			"nest" => $nest,
			"post_id" => $tmp_post_id,
			"title" => $row["bbs_title"],
			"date" => $row["date"],
			"emp_id" => $row["emp_id"],
			"position" => ($row["room_nm"] == "") ? $row["dept_nm"] : $row["room_nm"],
			"emp_nm" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"],
			"deleted" => ($row["bbs_del_flg"] == "t"),
			"deleted_by_self" => ($row["del_by_self"] == "t"),
			"content" => $row["bbs"],
			"files" => get_bbs_files($con, $theme_id, $tmp_post_id, $fname)
		);

		$children = get_posts_tree($con, $theme_id, $id_desc_flg, $term, $fname, $tmp_post_id, $nest + 1);

		$posts = array_merge($posts, $children);
	}

	$undeleted_posts = array();
	foreach ($posts as $i => $tmp_post) {
		if (!$tmp_post["deleted"] || reply_exists($posts, $i)) {
			$undeleted_posts[] = $tmp_post;
		}
	}

	$base_date = get_base_date($term);

	$is_tree_in_terms = false;
	$posts_in_term = array();
	foreach ($undeleted_posts as $tmp_post) {
		if ($tmp_post["nest"] == 0) {
			if ($is_tree_in_terms) {
				$posts_in_term = array_merge($posts_in_term, $thread);
			}
			$thread = array();
			$is_tree_in_terms = false;
		}

		$thread[] = $tmp_post;
		if ($tmp_post["date"] >= $base_date) {
			$is_tree_in_terms = true;
		}
	}
	if ($is_tree_in_terms) {
		$posts_in_term = array_merge($posts_in_term, $thread);
	}

	return $posts_in_term;
}

// 投稿順表示用の投稿一覧を取得
function get_posts_flat($con, $theme_id, $id_desc_flg, $count_per_page, $page, $term, $fname) {
	$sql = "select bbs.bbs_id, bbs.bbs_title, bbs.date, deptmst.dept_nm, classroom.room_nm, empmst.emp_lt_nm, empmst.emp_ft_nm, bbs.bbs, bbs.emp_id from bedbbs bbs inner join empmst on bbs.emp_id = empmst.emp_id inner join deptmst on deptmst.dept_id = empmst.emp_dept left join classroom on classroom.room_id = empmst.emp_room";
	$cond = "where bbs.bbs_del_flg = 'f' and bbs.bbsthread_id = $theme_id and bbs.date >= '" . get_base_date($term) . "' order by bbs.date";
	if ($id_desc_flg) {
		$cond .= " desc";
	}
	if ($count_per_page != 0) {
		$offset = ($page - 1) * $count_per_page;
		$cond .= " offset $offset limit $count_per_page";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$tmp_post_id = $row["bbs_id"];

		$posts[] = array(
			"post_id" => $tmp_post_id,
			"title" => $row["bbs_title"],
			"date" => $row["date"],
			"emp_id" => $row["emp_id"],
			"position" => ($row["room_nm"] == "") ? $row["dept_nm"] : $row["room_nm"],
			"emp_nm" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"],
			"content" => $row["bbs"],
			"files" => get_bbs_files($con, $theme_id, $tmp_post_id, $fname)
		);
	}

	return $posts;
}

// 添付ファイル情報を配列で取得
function get_bbs_files($con, $theme_id, $post_id, $fname) {
	$sql = "select bbsfile_no, bbsfile_name from bedbbsfile";
	$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id order by bbsfile_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$files = array();
	while ($row = pg_fetch_array($sel)) {
		$files[$row["bbsfile_no"]] = $row["bbsfile_name"];
	}
	return $files;
}

// ツリー表示
function show_posts_tree($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $count_per_page, $page, $term) {
	if (count($posts) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">投稿はありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return;
	}

	$comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

	// 読み飛ばすべきルート記事件数
	$offset = ($page - 1) * $count_per_page;

	// 読み飛ばしたルート記事件数
	$skipped_count = 0;

	// 表示済みルート記事件数
	$viewed_count = 0;

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	for ($i = 0, $post_count = count($posts); $i < $post_count; $i++) {
		$tmp_post = $posts[$i];
		$tmp_nest = $tmp_post["nest"];
		$tmp_post_id = $tmp_post["post_id"];
		$tmp_title = $tmp_post["title"];
		$tmp_date = $tmp_post["date"];;
		$tmp_emp_id = $tmp_post["emp_id"];
		$tmp_position = $tmp_post["position"];
		$tmp_emp_nm = $tmp_post["emp_nm"];
		$tmp_deleted = $tmp_post["deleted"];
		$tmp_deleted_by_self = $tmp_post["deleted_by_self"];
		$tmp_content = $tmp_post["content"];
		$tmp_files = $tmp_post["files"];

		if ($count_per_page != 0) {

			// 返信の場合
			if ($tmp_nest > 0) {

				// 読み飛ばし途中であればこの投稿を無視する
				if ($viewed_count == 0 || $skipped_count < $offset) {
					continue;
				}

			// ルート記事の場合
			} else {

				// ページあたりの表示件数ぶんの表示が済んでいればループを抜ける
				if ($viewed_count >= $count_per_page) {
					break;
				}

				// 読み飛ばし件数を増やす
				if ($skipped_count < $offset) {
					$skipped_count++;
					continue;
				}

				$viewed_count++;
			}
		}

		if ($tmp_deleted) {
			if ($tmp_deleted_by_self) {
				$tmp_title = "本人により削除されました";
			} else {
				$tmp_title = "管理者により削除されました";
			}
		} else {

			// 本人の場合は更新可能とする
			if ($tmp_emp_id == $emp_id) {
				$tmp_title = "<a href=\"bed_menu_status_bbs_post_update.php?session=$session&theme_id=$theme_id&post_id=$tmp_post_id&sort=$sort&show_content=$show_content&term=$term\">$tmp_title</a>";
			} else {
				$tmp_title = "<a href=\"bed_menu_status_bbs_detail.php?session=$session&theme_id=$theme_id&post_id=$tmp_post_id&sort=$sort&show_content=$show_content&term=$term\">$tmp_title</a>";
			}
			$new = (substr($tmp_date, 0, 8) >= $comp_date) ? "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">" : "";
		}
		$tmp_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $tmp_date);

		echo("<tr>\n");
		echo("<td>\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr>\n");

			// ルート記事の場合「○」を出力
			if ($tmp_nest == 0) {
				$img_src = (reply_exists($posts, $i)) ? "img/bbs_2.gif" : "img/bbs_1.gif";
				echo("<td width=\"15\"><img src=\"$img_src\" alt=\"\" width=\"15\" height=\"21\"></td>\n");

			// 返信の場合罫線を出力
			} else {
				for ($j = 0; $j <= $tmp_nest; $j++) {
					if ($j == $tmp_nest) {
						$img_src = (reply_exists($posts, $i)) ? "img/bbs_4.gif" : "img/bbs_3.gif";
					} else if ($j == $tmp_nest - 1) {
						$img_src = (same_level_post_exists($posts, $i, $tmp_nest)) ? "img/bbs_6.gif" : "img/bbs_5.gif";
					} else {
						$img_src = (same_level_post_exists($posts, $i, $j + 1)) ? "img/bbs_7.gif" : "img/spacer.gif";
					}
					echo("<td width=\"15\"><img src=\"$img_src\" alt=\"\" width=\"15\" height=\"21\"></td>\n");
				}
			}

			echo("<td width=\"4\">&nbsp;</td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_title}{$new}&nbsp;&nbsp;{$tmp_position}：{$tmp_emp_nm}&nbsp;（{$tmp_date}）</font></td>\n");
			echo("</tr>\n");
			if ($show_content == "t" && !$tmp_deleted) {
				echo("<tr>\n");
				for ($j = 0; $j <= $tmp_nest; $j++) {
					if ($j == $tmp_nest) {
						$img_src = (reply_exists($posts, $i)) ? "img/bbs_7.gif" : "img/spacer.gif";
					} else {
						$img_src = (same_level_post_exists($posts, $i, $j + 1)) ? "img/bbs_7.gif" : "img/spacer.gif";
					}
					echo("<td width=\"15\" style=\"background-image:url('$img_src');\"></td>\n");
				}
				$padding_bottom = (count($tmp_files) > 0) ? "" : " style=\"padding-bottom:3px;\"";
				echo("<td width=\"4\">&nbsp;</td>\n");
				echo("<td{$padding_bottom}><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . str_replace("\n", "<br>", $tmp_content) . "</font></td>\n");
				echo("</tr>\n");

				if (count($tmp_files) > 0) {
					foreach ($tmp_files as $tmp_file_no => $tmp_file_name) {
						$tmp_ext = strrchr($tmp_file_name, ".");

						echo("<tr>\n");
						for ($j = 0; $j <= $tmp_nest; $j++) {
							if ($j == $tmp_nest) {
								$img_src = (reply_exists($posts, $i)) ? "img/bbs_7.gif" : "img/spacer.gif";
							} else {
								$img_src = (same_level_post_exists($posts, $i, $j + 1)) ? "img/bbs_7.gif" : "img/spacer.gif";
							}
							echo("<td width=\"15\" style=\"background-image:url('$img_src');\"></td>\n");
						}
						echo("<td width=\"4\">&nbsp;</td>\n");
						echo("<td style=\"padding-left:15px;\"><img src=\"img/icon/file.gif\" alt=\"16\" width=\"16\" height=\"16\" style=\"position:relative;top:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"bbs/bed/{$theme_id}_{$tmp_post_id}_{$tmp_file_no}{$tmp_ext}\" target=\"_blank\">$tmp_file_name</a></font></td>\n");
						echo("</tr>\n");
					}

					echo("<tr>\n");
					for ($j = 0; $j <= $tmp_nest; $j++) {
						if ($j == $tmp_nest) {
							$img_src = (reply_exists($posts, $i)) ? "img/bbs_7.gif" : "img/spacer.gif";
						} else {
							$img_src = (same_level_post_exists($posts, $i, $j + 1)) ? "img/bbs_7.gif" : "img/spacer.gif";
						}
						echo("<td width=\"15\" style=\"background-image:url('$img_src');\"></td>\n");
					}
					echo("<td width=\"4\"></td>\n");
					echo("<td height=\"5\"></td>\n");
					echo("</tr>\n");
				}
			}
			echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
}

// 投稿順表示
function show_posts_flat($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $term) {
	$comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

	if (count($posts) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">投稿はありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	foreach ($posts as $tmp_post) {
		$tmp_post_id = $tmp_post["post_id"];
		$tmp_title = $tmp_post["title"];
		$tmp_date = $tmp_post["date"];
		$tmp_emp_id = $tmp_post["emp_id"];
		$tmp_position = $tmp_post["position"];
		$tmp_emp_nm = $tmp_post["emp_nm"];
		$tmp_content = $tmp_post["content"];
		$tmp_files = $tmp_post["files"];

		// 本人の場合は更新可能とする
		if ($tmp_emp_id == $emp_id) {
			$tmp_title = "<a href=\"bed_menu_status_bbs_post_update.php?session=$session&theme_id=$theme_id&post_id=$tmp_post_id&sort=$sort&show_content=$show_content&term=$term\">$tmp_title</a>";
		} else {
			$tmp_title = "<a href=\"bed_menu_status_bbs_detail.php?session=$session&theme_id=$theme_id&post_id=$tmp_post_id&sort=$sort&show_content=$show_content&term=$term\">$tmp_title</a>";
		}

		$new = (substr($tmp_date, 0, 8) >= $comp_date) ? "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">" : "";

		$tmp_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $tmp_date);

		echo("<tr height=\"21\">\n");
		echo("<td>\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
			echo("<tr>\n");
			echo("<td width=\"50%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_title}{$new}</font></td>\n");
			echo("<td width=\"20%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_position}：{$tmp_emp_nm}</font></td>\n");
			echo("<td width=\"30%\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_date</font></td>\n");
			echo("</tr>\n");
			if ($show_content == "t") {
				echo("<tr>\n");
				echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"></td>\n");
				echo("</tr>\n");
				if (!$tmp_deleted) {
					echo("<tr>\n");
					echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . str_replace("\n", "<br>", $tmp_content) . "</font></td>\n");
					echo("</tr>\n");

					if (count($tmp_files) > 0) {
						foreach ($tmp_files as $tmp_file_no => $tmp_file_name) {
							$tmp_ext = strrchr($tmp_file_name, ".");

							echo("<tr>\n");
							echo("<td colspan=\"3\" style=\"padding-left:15px;\"><img src=\"img/icon/file.gif\" alt=\"16\" width=\"16\" height=\"16\" style=\"position:relative;top:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"bbs/bed/{$theme_id}_{$tmp_post_id}_{$tmp_file_no}{$tmp_ext}\" target=\"_blank\">$tmp_file_name</a></font></td>\n");
							echo("</tr>\n");
						}
						echo("<tr>\n");
						echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"></td>\n");
						echo("</tr>\n");
					}
					echo("<tr>\n");
					echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
					echo("</tr>\n");
				}
				echo("<tr>\n");
				echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
				echo("</tr>\n");
			}
			echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
}

// ページ切り替えリンクの表示（ツリー表示用）
function write_navigation_tree($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname, $posts) {

	// 全件表示の場合は出力しない
	if ($count_per_page == 0) {
		return;
	}

	// ルート記事件数を算出
	$post_count = 0;
	foreach ($posts as $tmp_post) {
		if ($tmp_post["nest"] == 0) {
			$post_count++;
		}
	}

	write_navigation($theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $post_count);
}

// ページ切り替えリンクの表示（投稿順表示用）
function write_navigation_flat($con, $theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $fname) {

	// 全件表示の場合は出力しない
	if ($count_per_page == 0) {
		return;
	}

	// 投稿件数を取得
	$sql = "select count(*) from bedbbs";
	$cond = "where bbs_del_flg = 'f' and bbsthread_id = $theme_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$post_count = intval(pg_fetch_result($sel, 0, 0));

	write_navigation($theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $post_count);
}

// ページ切り替えリンクの表示（共通部分）
function write_navigation($theme_id, $sort, $count_per_page, $page, $show_content, $term, $session, $post_count) {

	// 1ページで収まる場合は出力しない
	if ($post_count <= $count_per_page) {
		return;
	}

	// ページ数を算出
	$max_page = ceil($post_count / $count_per_page);

	echo("<div style=\"text-align:center;margin-top:10px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
	for ($i = 1; $i <= $max_page; $i++) {
		if ($i == $page) {
			echo("[$i]\n");
		} else {
			echo("<a href=\"bed_menu_status_bbs.php?session=$session&theme_id=$theme_id&sort=$sort&page=$i&show_content=$show_content&term=$term\">$i</a>\n");
		}
	}
	echo("</font></div>\n");

}

// 指定された投稿に返信が存在するかどうか返す
function reply_exists($posts, $index) {
	$nest = $posts[$index]["nest"];

	$exists = false;
	for ($i = $index + 1, $post_count = count($posts); $i < $post_count; $i++) {
		$tmp_nest = $posts[$i]["nest"];
		// 削除されていない返信があることを確認
		if ($tmp_nest > $nest && $posts[$i]["deleted"] == false) {
			$exists = true;
			break;
		} else if ($tmp_nest == $nest) {
			break;
		}
	}

	return $exists;
}

// 指定された投稿より上の階層の投稿前に、指定された階層の投稿が存在するかどうか返す
function same_level_post_exists($posts, $index, $nest) {
	$exists = false;
	for ($i = $index + 1, $post_count = count($posts); $i < $post_count; $i++) {
		$tmp_nest = $posts[$i]["nest"];
		// 削除されていない返信があることを確認
		if ($tmp_nest == $nest && $posts[$i]["deleted"] == false) {
			$exists = true;
			break;
		} else if ($tmp_nest < $nest) {
			break;
		}
	}

	return $exists;
}

function get_message_count($con, $theme_id, $fname) {

	// 削除以外の投稿数を取得
	$sql = "select count(bbsthread_id) as cnt from bedbbs";
	$cond = "where bbsthread_id = $theme_id and  bbs_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$cnt = pg_fetch_result($sel, 0, "cnt");

	return $cnt;
}

function get_base_date($term) {
	$today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
	switch ($term) {
	case "1day":
		return date("Ymd", $today) . "0000";
	case "1week":
		return date("Ymd", strtotime("-6 days", $today)) . "0000";
	case "2week":
		return date("Ymd", strtotime("-13 days", $today)) . "0000";
	case "1month":
		return date("Ymd", strtotime("-30 days", $today)) . "0000";
	case "1year":
		return date("Ymd", strtotime("-364 days", $today)) . "0000";
	case "all":
		return "000000000000";
	}
}
?>
