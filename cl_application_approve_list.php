<?
//ini_set( 'display_errors', 1 );
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_approve_list.ini");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_application_workflow_select_box.ini");

require_once("cl_smarty_setting.ini");
$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = search_wkfwcatemst($con, $fname);
while($row_cate = pg_fetch_array($sel_wkfwcate)) {
	$tmp_wkfw_type = $row_cate["wkfw_type"];
	$tmp_wkfw_nm = $row_cate["wkfw_nm"];

	$arr_wkfwmst_tmp = array();
	$sel_wkfwmst = search_wkfwmst($con, $fname, $tmp_wkfw_type);
	while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
		$tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
		$wkfw_title = $row_wkfwmst["wkfw_title"];

		$arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
	}

	$arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}
// 「CoMedix」というカテゴリ追加
/*
$arr_comedix_tmp["1"] = "議事録公開申請（委員会・WG）";
$arr_comedix_tmp["2"] = "残業申請";
$arr_comedix_tmp["3"] = "勤務時間修正申請";
$arr_comedix_tmp["4"] = "退勤後復帰申請";
$arr_wkfwcatemst["0"] = array("name" => "CoMedix", "wkfw" => $arr_comedix_tmp);
*/

// 申請日セット(本日日付から過去２ヶ月分)
// 申請タブをクリックした場合のみ。

if($apply_date_defalut == "on")
{
	$today = date("Y/m/d", strtotime("today"));

	$arr_today = split("/", $today);
	$date_y2 = $arr_today[0];
	$date_m2 = $arr_today[1];
	$date_d2 = $arr_today[2];

	// ２ヶ月前の日付取得
	$two_months_ago = date("Y/m/d",strtotime("-2 months" ,strtotime($today)));

	$arr_two_months_ago = split("/", $two_months_ago);
	$date_y1 = $arr_two_months_ago[0];
	$date_m1 = $arr_two_months_ago[1];
	$date_d1 = $arr_two_months_ago[2];
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

// h-iwamoto ADD START
/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * カテゴリーオプションHTML取得
 *
 */
$cate_options_str=get_cate_options($arr_wkfwcatemst,$category);

/**
 * セクション名(部門・課・科・室)取得
 */
$pst_slct_bx_strct_str=get_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj);

/**
 * 申請年月日（開始）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y1=cl_get_select_years($date_y1);

// 月オプションHTML取得
$option_date_m1=cl_get_select_months($date_m1);

// 日オプションHTML取得
$option_date_d1=cl_get_select_days($date_d1);

/**
 * 申請年月日（終了）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y2=cl_get_select_years($date_y2);

// 月オプションHTML取得
$option_date_m2=cl_get_select_months($date_m2);

// 日オプションHTML取得
$option_date_d2=cl_get_select_days($date_d2);

/**
 * 受講期間（開始）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y3=cl_get_select_years($date_y3);

// 月オプションHTML取得
$option_date_m3=cl_get_select_months($date_m3);

// 日オプションHTML取得
$option_date_d3=cl_get_select_days($date_d3);

/**
 * 受講期間（終了）オプションHTML取得
 *
 */
// 年オプションHTML取得
$option_date_y4=cl_get_select_years($date_y4);

// 月オプションHTML取得
$option_date_m4=cl_get_select_months($date_m4);

// 日オプションHTML取得
$option_date_d4=cl_get_select_days($date_d4);

/**
 * 申請状況HTML取得
 */
$srch_aprv_lst_str=get_search_approve_list($con, $session, $fname, $category, $workflow, $apply_title, $training_theme, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $apply_emp_nm, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apv_stat, $training_div, $page);


/**
 * 研修区分HTML取得
 */
$training_div_options_str=get_training_div_options($training_div);


/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * 申請状況HTML取得
 */
$apv_stat_options_str=get_apv_stat_options($apv_stat);

// h-iwamoto ADD END

pg_close($con);

// h-iwamoto ADD START
/**
 * テンプレートマッピング
 */
$smarty->assign( 'cl_title'				, $cl_title						);
$smarty->assign( 'session'					, $session							);
$smarty->assign( 'yui_cal_part'				, $yui_cal_part						);
$smarty->assign( 'workflow_auth'			, $workflow_auth					);
$smarty->assign( 'aplyctn_mnitm_str'		, $aplyctn_mnitm_str				);
$smarty->assign( 'cate_options_str'			, $cate_options_str					);
$smarty->assign( 'apply_emp_nm'				, htmlspecialchars($apply_emp_nm)	);
$smarty->assign( 'training_theme'			, htmlspecialchars($training_theme)	);
$smarty->assign( 'pst_slct_bx_strct_str'	, $pst_slct_bx_strct_str			);

$smarty->assign( 'option_date_y1'			, $option_date_y1					);
$smarty->assign( 'option_date_m1'			, $option_date_m1					);
$smarty->assign( 'option_date_d1'			, $option_date_d1					);

$smarty->assign( 'option_date_y2'			, $option_date_y2					);
$smarty->assign( 'option_date_m2'			, $option_date_m2					);
$smarty->assign( 'option_date_d2'			, $option_date_d2					);

$smarty->assign( 'option_date_y3'			, $option_date_y3					);
$smarty->assign( 'option_date_m3'			, $option_date_m3					);
$smarty->assign( 'option_date_d3'			, $option_date_d3					);

$smarty->assign( 'option_date_y4'			, $option_date_y4					);
$smarty->assign( 'option_date_m4'			, $option_date_m4					);
$smarty->assign( 'option_date_d4'			, $option_date_d4					);

$smarty->assign( 'apv_stat_options_str'		, $apv_stat_options_str				);
$smarty->assign( 'srch_aprv_lst_str'		, $srch_aprv_lst_str				);
$smarty->assign( 'js_str'					, $js_str							);

$smarty->assign( 'training_div_options_str'	, $training_div_options_str			); //検索条件:研修区分のHTML

/**
 * テンプレート出力
 */
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

// h-iwamoto ADD END

// h-iwamoto ADD START
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * カテゴリーオプションHTML取得
 */
function get_cate_options($arr_wkfwcatemst,$category)
{

	ob_start();
	show_cate_options($arr_wkfwcatemst, $category);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * セクション名(部門・課・科・室)取得
 */
function get_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj)
{

	ob_start();
	show_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj, "", true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 年オプションHTML取得
 */
function cl_get_select_years($date)
{
	ob_start();
	// 2012/09/27 Yamagawa upd(s)
	//show_select_years(10, $date, true);
	show_select_fiscal_year(10, $date, true, false);
	// 2012/09/27 Yamagawa upd(e)
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{
	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{
	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 申請状況HTML取得
 */
function get_search_approve_list($con, $session, $fname, $category, $workflow, $apply_title, $training_theme, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $apply_emp_nm, $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apv_stat, $training_div, $page)
{
	ob_start();
	search_approve_list($con, $session, $fname, $category, $workflow, pg_escape_string($apply_title), pg_escape_string($training_theme), $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, pg_escape_string($apply_emp_nm), $date_y1, $date_m1, $date_d1, $date_y2, $date_m2, $date_d2, $date_y3, $date_m3, $date_d3, $date_y4, $date_m4, $date_d4, $apv_stat, $training_div, $page);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 申請状況HTML取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}
	return implode('', $str_buff);
}
/**
 * 申請状況HTML取得
 */
function get_apv_stat_options($apply_stat)
{
	ob_start();
	show_apv_stat_options($apply_stat);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 研修区分HTML取得
 */
function get_training_div_options($training_div)
{
	ob_start();
	show_training_div_options($training_div);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{
	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	/**
	 * YUIカレンダー用(画面内)スクリプトを出力します。
	 */
	write_yui_calendar_script2(4);
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// カテゴリオプションを出力
function show_cate_options($arr_cate, $cate) {
	echo("<option value=\"-\">すべて");
	foreach ($arr_cate as $tmp_cate_id => $arr) {
		$tmp_cate_name = $arr["name"];
		echo("<option value=\"$tmp_cate_id\"");
		if($cate != "" && $cate != '-') {
			if ($cate == $tmp_cate_id) {
				echo(" selected");
			}
		}
		echo(">$tmp_cate_name\n");
	}
}

// 承認状況オプションを出力
function show_apv_stat_options($stat) {

	$arr_apv_stat_nm = array('未承認','承認','否認','差戻し');
	$arr_apv_stat_id = array('0','1','2','3');

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_apv_stat_nm);$i++) {

		echo("<option value=\"$arr_apv_stat_id[$i]\"");
		if($stat == $arr_apv_stat_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_apv_stat_nm[$i]\n");
	}
}


// 研修区分オプションを出力
function show_training_div_options($val)
{
	$arr_txt = array("院内研修","院外研修");
	$arr_val = array("1","2");

	echo("<option value=\"-\">すべて");
	for($i=0;$i<count($arr_txt);$i++) {

		echo("<option value=\"$arr_val[$i]\"");
		if($val == $arr_val[$i]) {
			echo(" selected");
		}
		echo(">$arr_txt[$i]\n");
	}
}
