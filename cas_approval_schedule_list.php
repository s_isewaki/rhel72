<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CAS｜認定スケジュール一覧表示</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属：〇〇病棟</font><br>
<table class="list">
	<tr>
		<td rowspan="2">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font>
		</td>
		<td rowspan="2">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日</font>
		</td>
		<td colspan="2">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font>
		</td>
		<td colspan="4">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育/自己学習能力</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font>
		</td>
	</tr>
	<tr>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.1</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.3</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.5</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.6</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価3<br>No.8</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価4<br>No.9</font>
		</td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.10</font>
		</td>
	</tr>	
<?
$a[0] = array('あああ', '20070123', 'c0','d0', 'e0', 'f0','g0', 'h0', 'n0');
$a[1] = array('いいい', '20080123', 'c1','d1', 'e1', 'f1','g1', 'h1', 'n1');
$a[2] = array('ううう', '20090123', 'c2','d2', 'e2', 'f2','g2', 'h2', 'n2');
$a[3] = array('えええ', '20100123', 'c3','d3', 'e3', 'f3','g3', 'h3', 'n3');
$a[4] = array('おおお', '20110123', 'c4','d4', 'e4', 'f4','g4', 'h4', 'n4');


for($i=0; $i<5; $i++){
	$name = $a[$i][0];
	$apply_day = $a[$i][1];
	$eval1_No1 = $a[$i][2];
	$eval2_No3 = $a[$i][3];
	$eval1_No5 = $a[$i][4];
	$eval2_No6 = $a[$i][5];
	$eval3_No8 = $a[$i][6];
	$eval4_No9 = $a[$i][7];
	$eval1_No10 = $a[$i][8];
	$approval_schedule[$i] = "<tr><td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$name</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$apply_day</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval1_No1</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval2_No3</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval1_No5</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval2_No6</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval3_No8</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval4_No9</font>
		</td>
		<td rowspan=\"1\" class=\"apply_list_$i\" onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\" onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\" onclick=\"window.open('cas_approval_schedule_personal.php', 'newwin', 'left=0, top=0, width=700,height=400,scrollbars=yes');\">
			<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$eval1_No10</font>
		</td></tr>";
print $approval_schedule[$i];
}
?>
</table>
</body>
</html>