<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("show_select_values.ini");
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("show_class_name.ini");
require_once("show_emp_next.ini");
require_once("show_emp_list.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 79, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 入院基本料施設基準（一般病床）取得
$sql = "select nursing_standard_id, nursing_standard from fplus_nursing_standard where del_flg = 'f'";
$cond = "order by nursing_standard_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$nursing_standard_list = pg_fetch_all($sel);

// 並び順のデフォルトは職員IDの昇順
$emp_o = "1";

// 表示条件のデフォルトは「利用中の職員を表示」
$view = "1";

// 組織階層情報を取得
$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}


$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

?>
<title>CoMedix <?=$med_report_title?> | 病院登録</title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
	function setAtrbOptions() {

		clearOptions(document.mainform.attribute_id);

		addOption(document.mainform.attribute_id, '', ' ');

		var class_id = document.mainform.class_id.value;

		<? while ($row = pg_fetch_array($sel_atrb)) { ?>
			if (class_id == '<? echo $row["class_id"]; ?>') {
				addOption(document.mainform.attribute_id, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>');
			}
		<? } ?>

		setDeptOptions();
	}

	function setDeptOptions() {

		clearOptions(document.mainform.dept_id);

		addOption(document.mainform.dept_id, '', ' ');

		var atrb_id = document.mainform.attribute_id.value;

		<? while ($row = pg_fetch_array($sel_dept)) { ?>
			if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
				addOption(document.mainform.dept_id, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>');
			}
		<? } ?>
		<? if ($arr_class_name["class_cnt"] == 4) { ?>
			setRoomOptions();
		<? } ?>
	}

	<? if ($arr_class_name["class_cnt"] == 4) { ?>
		function setRoomOptions() {

			clearOptions(document.mainform.room_id);

			addOption(document.mainform.room_id, '', ' ');

			var dept_id = document.mainform.dept_id.value;

			<? while ($row = pg_fetch_array($sel_room)) { ?>
				if (dept_id == '<? echo $row["dept_id"]; ?>') {
					addOption(document.mainform.room_id, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>');
				}
			<? } ?>
		}
	<? } ?>

	function clearOptions(box) {
		for (var i = box.length - 1; i >= 0; i--) {
			box.options[i] = null;
		}
	}

	function addOption(box, value, text, selected) {
		var opt = document.createElement("option");
		opt.value = value;
		opt.text = text;
		if (selected == value) {
			opt.selected = true;
		}
		box.options[box.length] = opt;
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td, .inner td .list td {border:#E6B3D4 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<form name="mainform" action="fplusadm_hospital_mst_insert.php?session=<? echo($session); ?>" method="post">
<table width="560" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#E6B3D4">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>病院登録</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="560" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr>
		<td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="5%" style="border:0;"></td>
					<td width="15%" align="left" style="border:0;"><?=$font?>病院名</font></td>
					<td width="55%" style="border:0;"><?=$font?><input type="text" id="hospital_name" name="hospital_name" style="width:100%;ime-mode: active;" value="<?=$hospital_name?>" /></font></td>
					<td width="25%" style="border:0;"></td>
				</tr>
				<tr height="22">
					<td width="5%" style="border:0;"></td>
					<td width="15%" align="left" style="border:0;"><?=$font?>病院長名</font></td>
					<td width="55%" style="border:0;"><?=$font?><input type="text" id="hospital_directer" name="hospital_directer"  style="width:100%;ime-mode: active;" value="<?=$hospital_directer?>" /></font></td>
					<td width="25%" style="border:0;"></td>
				</tr>
				<tr>
					<td width="5%" style="border:0;"></td>
					<td width="15%" style="border:0;"><?=$font?>病床数</font></td>
					<td width="55%" style="border:0;">
						<?=$font?><input type="text" id="hospital_bed" name="hospital_bed" value="<?=$hospital_bed?>" maxlength="4" size="6" style="ime-mode: disabled;" /></font>
					</td>
					<td width="25%" style="border:0;"></td>
				</tr>
				<tr>
					<td width="5%" style="border:0;"></td>
					<td width="95%" style="border:0;" colspan="3"><?=$font?>入院基本料施設基準（一般病床）</font></td>
				</tr>
				<tr>
					<td width="5%" style="border:0;"></td>
					<td width="95%" style="border:0;" colspan="3">
						<?=$font?>
						<select id="nursing_standard" name="nursing_standard"><option></option>
							<? foreach($nursing_standard_list as $idx => $row){ ?>
								<option value="<?=$row["nursing_standard_id"]?>" ><?=$row["nursing_standard"]?></option>
							<? } ?>
						</select>
						</font>
					</td>
				</tr>
				<tr height="22">
					<td width="100%" colspan="4" style="border:0;">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td width="5%" style="border:0;"></td>
								<td width="95%" style="border:0;"><?=$font?>組織階層</font></td>
							</tr>
							<tr>
								<td width="5%" style="border:0;"></td>
								<td width="95%" style="border:0;">
									<table width="100%" border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td width="5%" style="border:0;"></td>
											<td width="5%" style="border:0;"><?=$font?><? echo($arr_class_name["class_nm"]); ?></font></td>
											<td width="90%" style="border:0;">
												<div style="float:left"><?=$font?>
													<select id="class_id" name="class_id" onchange="setAtrbOptions();">
													<option value="">
													<?
													while ($row = pg_fetch_array($sel_class)) {
														echo("<option value=\"{$row["class_id"]}\"");
														if ($row["class_id"] == $class_id) {echo(" selected");}
														echo(">{$row["class_nm"]}\n");
													}
													?>
													</select>
												</div>
											</td>
										</tr>
										<tr>
											<td width="5%" style="border:0;"></td>
											<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["atrb_nm"]); ?></font></td>
											<td width="85%" style="border:0;">
												<div style="float:left"><?=$font?>
												<select id="attribute_id" name="attribute_id" onchange="setDeptOptions();">
												<option value="">
												<?
												pg_result_seek($sel_atrb, 0);
												while ($row = pg_fetch_array($sel_atrb)) {
													if ($row["class_id"] == $class_id) {
														echo("<option value=\"{$row["atrb_id"]}\"");
														if ($row["atrb_id"] == $attribute_id) {echo(" selected");}
														echo(">{$row["atrb_nm"]}\n");
													}
												}
												?>
												</select>
												</div>
											</td>
										</tr>
										<tr>
											<td width="5%" style="border:0;"></td>
											<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["dept_nm"]); ?></font></td>
											<td width="85%" style="border:0;">
												<div style="float:left"><?=$font?>
												<select id="dept_id" name="dept_id"<? if ($arr_class_name["class_cnt"] == 4) {echo(" onchange=\"setRoomOptions();\"");} ?>>
												<option value="">
												<?
												pg_result_seek($sel_dept, 0);
												while ($row = pg_fetch_array($sel_dept)) {
													if ($row["atrb_id"] == $attribute_id) {
														echo("<option value=\"{$row["dept_id"]}\"");
														if ($row["dept_id"] == $dept_id) {echo(" selected");}
														echo(">{$row["dept_nm"]}\n");
													}
												}
												?>
												</select>
												</div>
											</td>
										</tr>
										<? if ($arr_class_name["class_cnt"] == 4) { ?>
										<tr>
											<td width="5%" style="border:0;"></td>
											<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["room_nm"]); ?></font></td>
											<td width="85%" style="border:0;">
												<div style="float:left"><?=$font?>
													<select id="room_id" name="room_id">
													<option value="">
													<?
													pg_result_seek($sel_room, 0);
													while ($row = pg_fetch_array($sel_room)) {
														if ($row["dept_id"] == $dept_id) {
															echo("<option value=\"{$row["room_id"]}\"");
															if ($row["room_id"] == $room_id) {echo(" selected");}
															echo(">{$row["room_nm"]}\n");
														}
													}
													?>
													</select>
												</div>
											</td>
										</tr>
										<? } ?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="560" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td height="22" align="right"><input type="submit" value="登録"></td>
	</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
