<?
$fname = $_SERVER["PHP_SELF"];
$session = @$_REQUEST["session"];

ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
ob_end_clean();

$ymdhms = Date("YmdHis");
$analysis_id = (int)@$_REQUEST["analysis_id"];
$analysis_no = $_REQUEST["analysis_no"];
$chart_seq = (int)@$_REQUEST["chart_seq"];
$chart_item_count = (int)@$_REQUEST["chart_item_count"];
$cliplist_item_count = (int)@$_REQUEST["cliplist_item_count"];
$chart_data = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["chart_data"])), "EUC-JP", "UTF-8");
$chart_v_data = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["chart_v_data"])), "EUC-JP", "UTF-8");
$cliplist_data = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["cliplist_data"])), "EUC-JP", "UTF-8");
$template_title = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["template_title"])), "EUC-JP", "UTF-8");
$environment_data = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["environment_data"])), "EUC-JP", "UTF-8");
$is_inga_mode = (@$_REQUEST["is_inga_mode"] ? "yes" : "");

$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
$login_emp_id  = get_emp_id($con,$session,$fname);

$error = "";
$warn_msg = "";


for (;;){
	if($session == "0") { $error = "01 / ". $session; break; }
	if($summary_check == "0") { $error = "02"; break; }
	if($con == "0") { $error = "03"; break; }

	// 画面パラメータチェック
	if (!$analysis_no) { $error = "対象がありません：analysis_no"; break; }
	if (!$analysis_id && !$template_title) { $error = "対象がありません：analysis_id"; break; }
	if (!$chart_seq && !$analysis_id) { $error = "対象がありません：chart_seq"; break; }

	// 既存データを検索する。
	if ($chart_seq){
		$sel = select_from_table($con, "select chart_seq, editing_emp_id, delete_flg from inci_analysis_rca_chart where chart_seq = " .$chart_seq, "", $fname);
		if (!$sel) { pg_close($con); $error = "21"; break; }
		$editing_emp_id = pg_fetch_result($sel, 0, 1);
		$delete_flg = pg_fetch_result($sel, 0, 2);
		if (!$editing_emp_id) { $warn_msg = "保存できません。\\n保存するには、編集ロックを先に行う必要があります。"; break; }
		if ($editing_emp_id != $login_emp_id) { $warn_msg = "他者により編集ロックされているため、保存できません。"; break; }
		if ($delete_flg=="t") { $warn_msg = "保存できません。このデータは既に削除されています。"; break; }
	}

	// データがなければ新規登録する。
	if (!$chart_seq){
		$sel = select_from_table($con, "select nextval('inci_analysis_rca_chart_seq')", "", $fname);
		$chart_seq = (int)pg_fetch_result($sel, 0, 0);

		$sql =
			" insert into inci_analysis_rca_chart (".
			" chart_seq, analysis_no, analysis_id, template_title, chart_data, chart_v_data, cliplist_data, chart_item_count, cliplist_item_count, update_ymdhms, is_inga_mode".
			") values (".
			" " . $chart_seq.
			",'" . pg_escape_string($analysis_no). "'".
			"," . ($analysis_id ? $analysis_id : "null").
			",'" . pg_escape_string($template_title). "'".
			",'" . pg_escape_string($chart_data). "'".
			",'" . pg_escape_string($chart_v_data). "'".
			",'" . pg_escape_string($cliplist_data). "'".
			","  . $chart_item_count.
			","  . $cliplist_item_count.
			",'" . $ymdhms. "'".
			",'" . $is_inga_mode. "'".
			")";
		if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "22"; break; }
	}

	// データがあれば更新する
	else {
		$sql =
			" update inci_analysis_rca_chart set" .
			" analysis_no = '". pg_escape_string($analysis_no)."'".
//			",template_title = '".pg_escape_string($template_title)."'".
			",analysis_id = ".($analysis_id ? $analysis_id : "null").
			",chart_item_count = ".$chart_item_count.
			",cliplist_item_count = ".$cliplist_item_count.
			",chart_data = '".pg_escape_string($chart_data)."'".
			",chart_v_data = '".pg_escape_string($chart_v_data)."'".
			",cliplist_data = '".pg_escape_string($cliplist_data)."'".
			",update_ymdhms = '".$ymdhms."'".
			" where chart_seq = " . $chart_seq;
		if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "23"; break; }
	}
	break;
}
if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{"chart_seq":"<?=$chart_seq?>","analysis_id":"<?=$analysis_id?>","error":"<?=$error?>","warn_msg":"<?=$warn_msg?>"}