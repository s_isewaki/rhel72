<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($rec_post1 != "" || $rec_post2 != "") {
	if (preg_match("/^\d{3}$/", $rec_post1) == 0 || preg_match("/^\d{4}$/", $rec_post2) == 0) {
		echo("<script type=\"text/javascript\">alert('請求先郵便番号は半角数字7文字で入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
}
if ($rec_tel1 != "" || $rec_tel2 != "" || $rec_tel3 != "") {
	if (preg_match("/^\d+$/", $rec_tel1) == 0 || preg_match("/^\d+$/", $rec_tel2) == 0 || preg_match("/^\d+$/", $rec_tel3) == 0) {
		echo("<script type=\"text/javascript\">alert('請求先電話番号は半角数字で入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
}
if ($out_left != "") {
	if (preg_match("/^\d+$/", $out_left) == 0) {
		echo("<script type=\"text/javascript\">alert('外来未収金額は半角数字で入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
} else {
	$out_left = null;
}
if ($in_left != "") {
	if (preg_match("/^\d+$/", $in_left) == 0) {
		echo("<script type=\"text/javascript\">alert('入院未収金額は半角数字で入力してください。');</script>\n");
		echo("<script type=\"text/javascript\">history.back();</script>\n");
		exit;
	}
} else {
	$in_left = null;
}

// データベースに接続
$con = connect2db($fname);

// 患者情報を更新
$sql = "update ptifmst set";
$set = array("ptif_req_nm", "ptif_req_zip1", "ptif_req_zip2", "ptif_req_prv", "ptif_req_addr1", "ptif_req_addr2", "ptif_req_tel1", "ptif_req_tel2", "ptif_req_tel3", "ptif_req_email", "ptif_out_unpaid", "ptif_in_unpaid", "ptif_note");
$setvalue = array($rec_name, $rec_post1, $rec_post2, $rec_province, $rec_address1, $rec_address2, $rec_tel1, $rec_tel2, $rec_tel3, $rec_email, $out_left, $in_left, $note2);
$cond = "where ptif_id = '$pt_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
if ($path == "A" || $path == "B") {
	$url_pt_nm = urlencode($pt_nm);
	echo("<script type=\"text/javascript\">location.href = 'bed_menu_inpatient_claim.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'inpatient_patient_claim.php?session=$session&pt_id=$pt_id&key1=$url_key1&key2=$url_key2&path=$path&ward=$ward&ptrm=$ptrm&bed=$bed&bedundec1=$bedundec1&bedundec2=$bedundec2&bedundec3=$bedundec3&in_dt=$in_dt&in_tm=$in_tm&ccl_id=$ccl_id&ptwait_id=$ptwait_id'</script>");
}
?>
