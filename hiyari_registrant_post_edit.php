<?
require("about_session.php");
require("about_authority.php");
require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// セクション名(部門・課・科・室)取得
$arr_class_name = get_class_name_array($con, $fname);





//====================================
// HTMLを出力
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix <?echo $INCIDENT_TITLE ?> | 報告部署変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
//所属選択のJavaScriptを出力
show_post_select_box_script($con, $fname);
?>
<script type="text/javascript">

function load_action()
{
    //プルダウンの設定
    set_post_select_box_obj_key("default","search_emp_class","search_emp_attribute","search_emp_dept","search_emp_room");
    setClassOptions("default","<?=$class_id?>","<?=$attribute_id?>","<?=$dept_id?>","<?=$room_id?>");
}

function update()
{
    var obj = null;

    obj = document.getElementById("search_emp_class");
    var class_id = obj.options[obj.selectedIndex].value;
    var class_name = obj.options[obj.selectedIndex].text;
    if(class_id == "")
    {
        class_name = "";
    }

    obj = document.getElementById("search_emp_attribute");
    var attribute_id = obj.options[obj.selectedIndex].value;
    var attribute_name = obj.options[obj.selectedIndex].text;
    if(attribute_id == "")
    {
        attribute_name = "";
    }

    obj = document.getElementById("search_emp_dept");
    var dept_id = obj.options[obj.selectedIndex].value;
    var dept_name = obj.options[obj.selectedIndex].text;
    if(dept_id == "")
    {
        dept_name = "";
    }

    obj = document.getElementById("search_emp_room");
    if(obj)
    {
        var room_id = obj.options[obj.selectedIndex].value;
        var room_name = obj.options[obj.selectedIndex].text;
        if(room_id == "")
        {
            room_name = "";
        }
    }
    else
    {
        var room_id = "";
        var room_name = "";
    }

    if(window.opener && !window.opener.closed && window.opener.callback_registrant_post_edit){
        window.opener.callback_registrant_post_edit(class_id,attribute_id,dept_id,room_id,class_name,attribute_name,dept_name,room_name);
    }
    window.close();
}

</script>
<style type="text/css">
form {margin:0;}
table.list {border-collapse:collapse;}
table.list td {border:solid #35B341 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action();">
<center>

<? show_hiyari_header_for_sub_window("報告部署変更"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<table width="100%" border='0' cellspacing='0' cellpadding='2' class="list">

<tr>
 <td width="80" align="right" bgcolor="#DFFFDC">
  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($arr_class_name[0])?></font>
 </td>
 <td bgcolor="#FFFFFF">
  <select id="search_emp_class" name="search_emp_class" onchange="setAtrbOptions('default');"></select>
 </td>
</tr>

<tr>
 <td width="80" align="right" bgcolor="#DFFFDC">
  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($arr_class_name[1])?></font>
 </td>
 <td bgcolor="#FFFFFF">
  <select id="search_emp_attribute" name="search_emp_attribute" onchange="setDeptOptions('default');"></select>
 </td>
</tr>

<tr>
 <td width="80" align="right" bgcolor="#DFFFDC">
  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($arr_class_name[2])?></font>
 </td>
 <td bgcolor="#FFFFFF">
  <select id="search_emp_dept" name="search_emp_dept" onchange="setRoomOptions('default');"></select>
 </td>
</tr>

<?if($arr_class_name["class_cnt"] == 4) { ?>
<tr>
 <td width="80" align="right" bgcolor="#DFFFDC">
  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($arr_class_name[3])?></font>
 </td>
 <td bgcolor="#FFFFFF">
  <select id="search_emp_room" name="search_emp_room"></select>
 </td>
</tr>
<?}?>

</table>


<table cellspacing="0" cellpadding="0" border="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
    <td align="right">
        <input type="button" value="変更" onclick="update();">
    </td>
</tr>
</table>




</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>




</center>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
