<?
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("label_by_profile_type.ini");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ファイルが正常にアップロードされたかどうかチェック
$uploaded = false;
if (array_key_exists("csvfile", $_FILES)) {
	if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
		$uploaded = true;
	}
}
if (!$uploaded) {
	echo("<script type=\"text/javascript\">alert('ファイルのアップロードに失敗しました。');</script>\n");
	echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
	exit;
}

// 文字コードの設定
switch ($encoding) {
case "1":
	$file_encoding = "SJIS";
	break;
case "2":
	$file_encoding = "EUC-JP";
	break;
case "3":
	$file_encoding = "UTF-8";
	break;
default:
	exit;
}

// EUC-JPで扱えない文字列は「・」に変換するよう設定
mb_substitute_character(0x30FB);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 組織情報を取得
$arr_class_name = get_class_name_array($con, $fname);


// 役職情報を配列に格納
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$statuses = array();
while ($row = pg_fetch_array($sel)) {
	$statuses[$row["st_id"]] = $row["st_nm"];
}

// 職種情報を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["job_id"]] = $row["job_nm"];
}

// カラム数
$column_count = ($arr_class_name[4] == 4) ? 12 : 11;

// CSVデータを配列に格納
$employees = array();
$employee_no = 1;
$lines = file($_FILES["csvfile"]["tmp_name"]);
foreach ($lines as $line) {

	// 文字コードを変換
	$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

	// EOFを削除
	$line = str_replace(chr(0x1A), "", $line);

	// 空行は無視
	if ($line == "") {
		continue;
	}

	// カラム数チェック
	$employee_data = split(",", $line);
	if (count($employee_data) != $column_count) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('カラム数が不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$employees[$employee_no] = $employee_data;
	$employee_no++;
}

// 職員データを登録
$extra_ids = array();
foreach ($employees as $employee_no => $employee_data) {
	$extra_id = insert_employee_data($con, $employee_no, $employee_data, $arr_class_name[4], $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname);
	if ($extra_id != "") {
		$extra_ids[] = $extra_id;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 存在しない職員IDが指定されていたらalert
if (count($extra_ids) > 0) {
	$tmp_extra_ids = "・" . implode("\\n・", $extra_ids);
	echo("<script type=\"text/javascript\">alert('下記の職員は存在しないため、無視されました。\\n\\n$tmp_extra_ids');</script>\n");
}

// 成功画面を表示
echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=t&encoding=$encoding';</script>");

// 職員データを登録
function insert_employee_data($con, $employee_no, $employee_data, $class_cnt, $class_tree, $arr_class_name, $statuses, $jobs, $session, $fname) {
	global $_label_by_profile;
	global $profile_type;

	$i = 0;
	$personal_id = trim($employee_data[$i++]);
	$emp_lt_nm = $employee_data[$i++];
	$emp_ft_nm = $employee_data[$i++];
	$emp_lt_kana_nm = $employee_data[$i++];
	$emp_ft_kana_nm = $employee_data[$i++];
	$class = trim($employee_data[$i++]);
	$atrb = trim($employee_data[$i++]);
	$dept = trim($employee_data[$i++]);
	if ($arr_class_name[4] == "4") {
		$room = trim($employee_data[$i++]);
	} else {
		$room = "";
	}
	$status = trim($employee_data[$i++]);
	$job = trim($employee_data[$i++]);
	$del_flg = trim($employee_data[$i++]);

	// 当該職員が存在するかチェック
	$sql = "select count(*) from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
		return $personal_id;
	}

	// 部門IDの取得
	$class_id = "";
	$sql = "select class_id from classmst";
	$cond = "where link_key = '$class' and class_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[0]}の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$class_id = pg_fetch_result($sel, 0, "class_id");

	// 課IDの取得
	$atrb_id = "";
	$sql = "select atrb_id from atrbmst";
	$cond = "where class_id = $class_id and link_key = '$atrb' and atrb_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[1]}の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$atrb_id = pg_fetch_result($sel, 0, "atrb_id");

	// 科IDの取得
	$dept_id = "";
	$sql = "select dept_id from deptmst";
	$cond = "where atrb_id = $atrb_id and (link_key1='$dept' OR link_key2='$dept' OR link_key3='$dept') and dept_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[2]}の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	$dept_id = pg_fetch_result($sel, 0, "dept_id");

	// 室IDの取得
	if ($arr_class_name[4] == "4" && $room != "") {
		$sql = "select room_id from classroom";
		$cond = "where dept_id = $dept_id and link_key = '$room' and room_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された{$arr_class_name[3]}の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
		$room_id = pg_fetch_result($sel, 0, "room_id");
	} else {
		$room_id = null;
	}

	// 役職IDを取得
	if ($status != "") {
		$sql = "select st_id from stmst";
		$cond = "where link_key = '$status' and st_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された役職({$status})の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
		$st_id = pg_fetch_result($sel, 0, "st_id");
	} else {
		$st_id = null;
	}

	// 職種IDを取得
	if ($job != "") {
		$sql = "select job_id from jobmst";
		$cond = "where link_key = '$job' and job_del_flg = 'f'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('指定された職種({$job})の外部連携キーが見つかりません。レコード番号：{$employee_no}');</script>\n");
			echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
			exit;
		}
		$job_id = pg_fetch_result($sel, 0, "job_id");
	} else {
		$job_id = null;
	}

	// 入力チェック
	if (strlen($emp_lt_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_ft_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（漢字）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_lt_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('苗字（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if (strlen($emp_ft_kana_nm) > 20) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('名前（かな）が長すぎます。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}
	if ($del_flg != "f" && $del_flg != "t") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('利用停止フラグが不正です。レコード番号：{$employee_no}');</script>\n");
		echo("<script type=\"text/javascript\">location.href = 'employee_attribute_bulk_register3.php?session=$session&result=f&encoding=$encoding';</script>");
		exit;
	}

	$sql = "update empmst set";
	$set = array("emp_lt_nm", "emp_ft_nm", "emp_kn_lt_nm", "emp_kn_ft_nm", "emp_keywd", "emp_class", "emp_attribute", "emp_dept", "emp_room");
	$setvalue = array($emp_lt_nm, $emp_ft_nm, $emp_lt_kana_nm, $emp_ft_kana_nm, get_keyword($emp_lt_kana_nm), $class_id, $atrb_id, $dept_id, $room_id);
	if (!is_null($st_id)) {
		$set[] = "emp_st";
		$setvalue[] = $st_id;
	}
	if (!is_null($job_id)) {
		$set[] = "emp_job";
		$setvalue[] = $job_id;
	}
	$cond = "where emp_personal_id = '$personal_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$sql = "select emp_id from empmst";
	$cond = "where emp_personal_id = '$personal_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	$sql = "update authmst set";
	$set = array("emp_del_flg");
	$setvalue = array($del_flg);
	$cond = "where emp_id = '$emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
