<?
ob_start();
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_values.php");
require_once("get_menu_label.ini");
ob_end_clean();

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $_SERVER["PHP_SELF"];
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

//ログイン職員ＩＤ
$emp_id = get_emp_id($con, $session, $fname);


//=================================================
// 対象日付がない場合は本日を設定
//=================================================
if(@$apply_date_default == "on") list($date_y, $date_m, $date_d) = split("/", date("Y/m/d", strtotime("today")));
$target_yyyymmdd = $date_y. ((int)@$date_m <10 ? "0".(int)@$date_m : $date_m) . ((int)@$date_d <10 ? "0".(int)@$date_d : $date_d);

//=================================================
// 病棟などが指定されていなければデフォルトを設定する
//=================================================
if (@$apply_date_default && @$sel_bldg_cd=="" && @$sel_ward_cd==""){
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$emp_ward = @$c_sot_util->select_from_table($sql);
	$_REQUEST["sel_bldg_cd"] = @pg_fetch_result($emp_ward, 0, "bldg_cd");
	$_REQUEST["sel_ward_cd"] = @pg_fetch_result($emp_ward, 0, "ward_cd");
	$sel_bldg_cd= $_REQUEST["sel_bldg_cd"];
	$sel_ward_cd= $_REQUEST["sel_ward_cd"];
}
$sel_team_id = (int)@$_REQUEST["sel_team_id"];

$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst where ptif_id = '".pg_escape_string(@$_REQUEST["sel_ptif_id"])."'";
$sel = select_from_table($con, $sql, "", $fname);
$ptif_name = trim(@pg_fetch_result($sel, 0, "ptif_lt_kaj_nm") . " " . @pg_fetch_result($sel, 0, "ptif_ft_kaj_nm"));


$THIS_WORKSHEET_ID = "1"; // [sotmst][sot_id]の固定値


ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<title>CoMedix <?=get_report_menu_label($con, $fname)?> | 医師指示一覧</title>
<? write_yui_calendar_use_file_read_0_12_2();// 外部ファイルを読み込む ?>
<? write_yui_calendar_script2(1);// カレンダー作成、関数出力 ?>
<script type="text/javascript">

<?//**************************************************************************?>
<?//受付実施と電卓関連                                                        ?>
<?//**************************************************************************?>
var apply_dialog_calobj = null;<?//受付・実施ダイアログの電卓を押したときの入力先 ?>
var apply_dialog_calc_refreshed = false;<?//受付・実施ダイアログの中の電卓が表示されたときたときtrue、電卓を押したらfalse?>
<?//--------------------------------------------?>
<?// 受付実施ダイアログを隠す                   ?>
<?//--------------------------------------------?>
function hideApplyDialog(){
	document.getElementById('apply_dialog').style.display = "none";
}
<?//--------------------------------------------?>
<?// 時分秒のクリックイベント、電卓を表示する   ?>
<?//--------------------------------------------?>
function adjcalc(hms){
	document.getElementById("apply_dialog_td"+hms).appendChild(document.getElementById('apply_dialog_calculator'));
	var cap="時"; if(hms=="m")cap="分"; if(hms=="s")cap="秒";
	document.getElementById("apply_dialog_caption").innerHTML = cap;
	apply_dialog_calc_refreshed = true;
	apply_dialog_calobj = document.getElementById("apply_dialog_"+hms);
}
function calcf(hms){
	document.getElementById("apply_dialog_calculator").style.display = "";
	adjcalc(hms);
	apply_dialog_calobj.select();
}
<?//--------------------------------------------?>
<?// 電卓を隠す                                 ?>
<?//--------------------------------------------?>
function hidecalc(){document.getElementById('apply_dialog_calculator').style.display='none';}
<?//--------------------------------------------?>
<?// 電卓のマウスオーバー、セル色をつける       ?>
<?//--------------------------------------------?>
function calc1(obj){ obj.style.backgroundColor = "#f90"; obj.style.color = "#fff"; }
<?//--------------------------------------------?>
<?// 電卓のマウスアウト、セル色を戻す           ?>
<?//--------------------------------------------?>
function calc2(obj){ obj.style.backgroundColor = "#fff"; obj.style.color = "#00f"; }
<?//--------------------------------------------?>
<?// 電卓のボタンクリック                       ?>
<?//--------------------------------------------?>
function calc(num){
	var ref = apply_dialog_calc_refreshed;
	var len = apply_dialog_calobj.value.length;
	if (num=="C"){ apply_dialog_calobj.value = ""; return; }
	if (num=="D"){
		if (len == 0) return;
		apply_dialog_calobj.value = apply_dialog_calobj.value.substring(0,len-1);
		apply_dialog_calc_refreshed = false;
		return;
	}
	if (ref){
		apply_dialog_calobj.value = num;
	} else {
		apply_dialog_calobj.value = apply_dialog_calobj.value + "" + num;
		if (len > 0) hidecalc();
	}
	apply_dialog_calc_refreshed = false;
}
<?//--------------------------------------------?>
<?// 受付実施ダイアログを開く                   ?>
<?//--------------------------------------------?>
var apply_dialog_mode = "";
var schedule_various_seq = "";
function getClientWidth(){
    if (document.documentElement && document.documentElement.clientWidth) return document.documentElement.clientWidth;
    if (document.body.clientWidth) return document.body.clientWidth;
    if (window.innerWidth) return window.innerWidth;
    return 0;
}
function showApplyDialog(mode, id){
	apply_dialog_mode = mode;
	schedule_various_seq = id;
	var dialog = document.getElementById("apply_dialog");
	var msg = (mode == "accept" ? "受付" : "実施");
	document.getElementById("apply_dialog_titlebar").innerHTML = msg;
	document.getElementById("apply_dialog_description").innerHTML = "上記を"+msg+"時刻として、"+msg+"を登録します。";
	var date = new Date();
	document.getElementById("apply_dialog_h").value = date.getHours();
	document.getElementById("apply_dialog_m").value = date.getMinutes();
	document.getElementById("apply_dialog_s").value = date.getSeconds();
	dialog.style.left = (getClientWidth() / 2 - 125) + "px";
	dialog.style.top = (140) + "px";
	dialog.style.display = "";
	document.getElementById("apply_dialog_ok").focus();
}
<?//--------------------------------------------?>
<?// 受付実施をサーバに登録する                 ?>
<?//--------------------------------------------?>
function registAcceptOperate(mode){
	inquiryRegist(mode);
}

<?//**************************************************************************?>
<?//AJAXサーバ登録関連（受付実施と評価メモ用                                  ?>
<?//**************************************************************************?>
var ajaxObj = null;
function cnvtime(v, limit){
	if (!v.match(/^\s*[0-9]+\s*$/g)) return "";
	v = parseInt(v);
	if (isNaN(v)) return "";
	if (v > limit) return "";
	if (v < 0) return "";
	return v;
}
<?//--------------------------------------------?>
<?// サーバへ登録送信                           ?>
<?//--------------------------------------------?>
function inquiryRegist(mode){
	var hms = "";
	if (mode=="accept" || mode=="operate"){
		var elemh = document.getElementById("apply_dialog_h");
		var elemm = document.getElementById("apply_dialog_m");
		var elems = document.getElementById("apply_dialog_s");
		elemh.value = cnvtime(elemh.value, 23);
		elemm.value = cnvtime(elemm.value, 59);
		elems.value = cnvtime(elems.value, 59);
		if (elemh.value=="" || elemm.value=="" || elems.value=="") {alert("有効な時刻を指定してください。"); return; }
		hms = elemh.value+":"+elemm.value+":"+elems.value;
		hideApplyDialog();
	}

	var postval =
		"ymd=<?=$target_yyyymmdd?>&schedule_various_seq="+schedule_various_seq+"&mode="+mode+"&hms="+hms+"&session=<?=$session?>";
	if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
	else {
		try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
	}
  ajaxObj.onreadystatechange = returnInquiryRegist;
  ajaxObj.open("POST", "sot_worksheet_apply.php", true);
  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajaxObj.send(postval);
}
<?//--------------------------------------------?>
<?// サーバからの戻りを取得して画面表示を変更   ?>
<?//--------------------------------------------?>
function returnInquiryRegist(){
	if (ajaxObj == null) return;
	if (ajaxObj.readyState != 4) return;
	if (typeof(ajaxObj.status) != "number") return;
	if (ajaxObj.status != 200) return;
	if (!ajaxObj.responseText) return;
	try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
	catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
	if (!ret) return;
	if (!ret.schedule_various_seq) return;
	var elem = null;
	if (ret.is_accepted) {
		elem = document.getElementById("element_accept_"+ret.schedule_various_seq);
		if (elem) { elem.innerHTML = ret.emp_name; }
	}
	if (ret.is_operated) {
		elem = document.getElementById("element_operate_"+ret.schedule_various_seq);
		if (elem) { elem.innerHTML = ret.emp_name; }
	}
	ajaxObj = null;
}
function popupPrintPreview(){
	window.open(
		"sot_worksheet_isisiji.php?session=<?=$session?>&date_y=<?=@$date_y?>&date_m=<?=@$date_m?>&date_d=<?=@$date_d?>&sel_bldg_cd=<?=@$sel_bldg_cd?>&sel_ward_cd=<?=@$sel_ward_cd?>&sel_ptrm_room_no=<?=@$sel_ptrm_room_no?>&sel_team_id=<?=@$sel_team_id?>&print_preview=y",
		"worksheet_isisiji_print",
		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1024,height=700"
	);
}
</script>
<style type="text/css" media="all">
	body { margin:0; color:#000; background-color:#fff; }
	.list {border-collapse:collapse;}
	.list td {border:#5279a5 solid 1px; padding:1px;}
	table.block_in {border-collapse:collapse;}
	table.block_in td {border:#5279a5 solid 0px;}
	img { border: 0; }
	.date_header { background-color:#ffa; }
	.c_red { color:#f05 }
	.apply_header { border: 1px solid #ccc; }
	.timecalcullator table{ border:1px solid #aaa; background-color:#ddd; }
	.timecalcullator td { border:1px solid #aaa; text-align:center; padding:2px 4px; background-color:#fff; cursor:pointer; color:#00f }
	.plane { border:0; border-collapse:collapse; }
	.plane td { border:0; }
<? if (@$_REQUEST["print_preview"]) {?>
	.print_preview_hidden { display:none; }
<? } ?>
</style>
<? if (@$_REQUEST["print_preview"]) {?>
<style type="text/css" media="print">
	html { font-size:12px; }
	.print_color_black { color:#000; }
	.print_target_table th { font-size:100% }
	.print_target_table td { font-size:100% }
	.print_hidden { display:none; }
	.list td {border:#000 solid 1px;}
	.date_header { border: 1px solid #777; }
	.apply_header { border: 1px solid #777; }
</style>
<? } ?>
</head>
<body onload="<? if (@$_REQUEST["print_preview"]) {?>print();close();<?}else{ ?>initcal();<?}?>"<? if (@$_REQUEST["print_preview"]) {?>style="padding:1px"<? } ?>>
<div class="print_preview_hidden">

<? $param = "&date_y=".@$date_y."&date_m=".@$date_m."&date_d=".@$date_d."&sel_bldg_cd=".@$sel_bldg_cd."&sel_ward_cd=".@$sel_ward_cd."&sel_ptrm_room_no=".@$sel_ptrm_room_no."&sel_team_id=".$sel_team_id; ?>

<? summary_common_show_sinryo_top_header($session, $fname, "医師指示一覧", $param); ?>

<? summary_common_show_main_tab_menu($con, $fname, "看護支援", 1); ?>

<script type="text/javascript">
var ward_selections = {};
var room_selections = {};
var combobox = {};
function sel_changed(node, selval){
	combobox[1] = document.getElementById("sel_bldg_cd");
	combobox[2] = document.getElementById("sel_ward_cd");
	combobox[3] = document.getElementById("sel_ptrm_room_no");
	if (node < 3) combobox[3].options.length = 1;
	if (node < 2) combobox[2].options.length = 1;

	<? // 初期化用 自分コンボを再選択?>
	var cmb = combobox[node];
	if (node > 0 && cmb.value != selval) {
		for(var i=0; i<cmb.options.length; i++) if (cmb.options[i].value==selval) cmb.selectedIndex = i;
	}

	var idx = 1;
	if (node == 1){
		for(var i in ward_selections){
			if (ward_selections[i].bldg_cd != selval) continue;
			combobox[2].options.length = combobox[2].options.length + 1;
			combobox[2].options[idx].text  = ward_selections[i].name;
			combobox[2].options[idx].value = ward_selections[i].code;
			idx++;
		}
	}
	if (node == 2){
		for(var i in room_selections){
			if (room_selections[i].bldg_cd != combobox[1].value) continue;
			if (room_selections[i].ward_cd != selval) continue;
			combobox[3].options.length = combobox[3].options.length + 1;
			combobox[3].options[idx].text  = room_selections[i].name;
			combobox[3].options[idx].value = room_selections[i].code;
			idx++;
		}
	}
}

function searchByDay(day_incriment){
	var yy = document.search.date_y;
	var mm = document.search.date_m;
	var dd = document.search.date_d;
	var y = (parseInt(yy.value,10) ? yy.value : yy.options[1].value);
	var m = (parseInt(mm.value,10) ? mm.value : 1)*1;
	var d = (parseInt(dd.value,10) ? dd.value : 1)*1;
	var dt = new Date(y, m-1, d);
	dt.setTime(dt.getTime() + day_incriment * 86400000);

	var idx_y = yy.options[1].value - dt.getFullYear() + 1;
	if (idx_y < 1) return;
	yy.selectedIndex = idx_y;
	mm.selectedIndex = dt.getMonth()+1;
	dd.selectedIndex = dt.getDate();
	document.search.submit();
}

function searchByToday(){
	var dt = new Date();
	document.search.date_y.selectedIndex = document.search.date_y.options[1].value - dt.getFullYear() + 1;
	document.search.date_m.selectedIndex = dt.getMonth()+1;
	document.search.date_d.selectedIndex = dt.getDate();
	document.search.submit();
}

<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索ダイアログのポップアップ     ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function popupKanjaSelect(){
	window.open(
		"sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=1&sel_ward_cd=<?=@$ptif_ward_cd?>&sel_ptif_id=<?=@$sel_ptif_id?>&sel_sort_by_byoto=1",
		"",
		"directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height=480"
	);
}

<? //++++++++++++++++++++++++++++++++++++++?>
<? // 患者検索結果を受けるコールバック関数 ?>
<? //++++++++++++++++++++++++++++++++++++++?>
function callbackKanjaSelect(ptif_id){
	document.getElementById("sel_ptif_id").value = ptif_id;
	document.search.submit();
}

</script>
<?
$EMPTY_OPTION = '<option value="">　　　　</option>';

// チーム一覧
$team_options = array();
$sel = $c_sot_util->select_from_table("select team_id, team_nm from teammst where team_del_flg = 'f'");
while($row = pg_fetch_array($sel)){
	$team_options[] =
	$selected = ($row["team_id"] == $sel_team_id ? " selected" : "");
	$team_options[] = '<option value="'.$row["team_id"].'"'.$selected.'>'.$row["team_nm"].'</option>';
}

// 事業所選択肢(部門一覧)
$bldg_data_options = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) {
	$selected = ($row["bldg_cd"] == @$_REQUEST["sel_bldg_cd"] ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢 JavaScript変数として出力する
$ary = array();
$sel = $c_sot_util->select_from_table("select bldg_cd, ward_cd, ward_name from wdmst where ward_del_flg = 'f' order by ward_cd");
$mst = (array)pg_fetch_all($sel);
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","code":"'.$row["ward_cd"].'","name":"'.$row["ward_name"].'"}';
echo "<script type='text/javascript'>ward_selections = [". implode(",", $ary) . "];\n</script>\n";

// 病室選択肢 JavaScript変数として出力する
$mst = $c_sot_util->getPatientsRoomInfo();
$ary = array();
foreach($mst as $key => $row) $ary[] = '{"bldg_cd":"'.$row["bldg_cd"].'","ward_cd":"'.$row["ward_cd"].'","code":"'.$row["ptrm_room_no"].'","name":"'.$row["ptrm_name"].'"}';
echo "<script type='text/javascript'>room_selections = [". implode(",", $ary) . "];\n</script>\n";
?>

<form name="search" action="sot_worksheet_isisiji.php?session=<?=$session?>" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="excelout" value="">
<table class="list_table" style="margin-top:4px; margin-bottom:8px" cellspacing="1">
	<tr>
		<th>事業所(棟)</th>
		<td><select onchange="sel_changed(1, this.value);" name="sel_bldg_cd" id="sel_bldg_cd"><?=implode("\n", $bldg_data_options);?></select></td>
		<th>病棟</th>
		<td><select onchange="sel_changed(2, this.value);" name="sel_ward_cd" id="sel_ward_cd"><?=$EMPTY_OPTION?></select></td>
		<th>病室</th>
		<td><select name="sel_ptrm_room_no" id="sel_ptrm_room_no"><?=$EMPTY_OPTION?></select></td>
		<!--th>患者氏名</th>
		<td><input type="text" name="sel_ptif_name" id="sel_ptif_name" style="width:200px" value="<?=@$sel_ptif_name?>" /></td-->
		<script type="text/javascript">sel_changed(1, document.getElementById("sel_bldg_cd").value);</script>
		<script type="text/javascript">sel_changed(2, "<?=@$_REQUEST["sel_ward_cd"]?>");</script>
		<script type="text/javascript">sel_changed(3, "<?=@$_REQUEST["sel_ptrm_room_no"]?>");</script>
		<th bgcolor="#fefcdf">チーム</th>
		<td>
			<select name="sel_team_id" id="sel_team_id"><?=$EMPTY_OPTION?><?=implode("\n", $team_options)?></select>
		</td>
	</tr>
	<tr>
		<th>日付</th>
		<td colspan="7">
			<div id="cal1Container" style="margin-top:24px; position:absolute;display:none;z-index:10000;"></div>
			<div style="float:left">
				<div style="float:left">
					<select id="date_y1" name="date_y"><option value="-"><? show_select_years_span(date("Y") - 14, date("Y") + 1, $date_y, false); ?></select>&nbsp;/
					<select id="date_m1" name="date_m"><? show_select_months($date_m, true); ?></select>&nbsp;/
					<select id="date_d1" name="date_d"><? show_select_days($date_d, true); ?></select>
				</div>
				<div style="float:left">
					<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
				</div>
				<div style="float:left; padding-left:4px">
					<input type="button" onclick="searchByDay('-7');" value="≪" title="１週間前"/><!--
				--><input type="button" onclick="searchByDay('-1');" value="<" title="１日前"/><!--
				--><input type="button" onclick="searchByDay('1');" value=">" title="１日後"/><!--
				--><input type="button" onclick="searchByDay('7');" value="≫" title="１週間後"/>
				</div>
				<div style="float:left;">
					<input type="button" onclick="searchByToday();" value="本日"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="float:right">
				<input type="button" onclick="document.search.excelout.value=''; document.search.submit();" value="検索"/>
				<input type="button" onclick="document.search.excelout.value=''; popupPrintPreview();" value="印刷"/>
				<input type="button" onclick="document.search.excelout.value='1'; document.search.submit();" value="Excel出力"/>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>



<? summary_common_show_worksheet_tab_menu("医師指示一覧", $param); ?>

<? //=========================================================================?>
<? // 検索指定領域（患者）                                                    ?>
<? //=========================================================================?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<table class="list_table" cellspacing="1">
	<tr>
		<th width="5%" style="white-space:nowrap">患者ID</th>
		<td width="45%">
			<div style="float:left">
					<input type="text" style="width:100px; ime-mode:disabled" name="sel_ptif_id" id="sel_ptif_id" value="<?=@$_REQUEST["sel_ptif_id"]?>"/><!--
			--><span id="sel_ptif_name" style="padding-left:2px; padding-right:2px"><?=$ptif_name?></span><!--
			--><input type="button" onclick="popupKanjaSelect();" value="患者検索"/><!--
			--><input type="button" onclick="document.search.sel_ptif_id.value=''; document.search.submit();" value="クリア"/>
			</div>
			<div style="clear:both"></div>
		</td>
	</tr>
</table>

</form>


</div><!-- end of [class="print_preview_hidden"] -->


<?
//====================================
// スケジュール中の患者と病室を取得
// ・患者ごとに、どの期間を取得すべきかを求める
//   ・指定日に入院しているなら、入院開始予定日と終了予定日の間のスケジュールをとる
//   ・指定日に入院していないなら、直近の退院日からのスケジュールをとる。
//     ・直近の退院日もない場合は、システム稼動日からが求める期間となる。
//====================================
/*
$sel = select_from_table($con, "select emp_id, emp_lt_nm, emp_ft_nm from empmst", "", $fname);
$empmst = array();
while ($row = pg_fetch_array($sel)){
	$empmst[$row["emp_id"]] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
}
*/
$target_week = "";
if (checkdate((int)@$date_m, (int)@$date_d, (int)@$date_y)) {
	$target_ymd_time = mktime(0, 0, 0, (int)$date_m, (int)$date_d, (int)$date_y);
	$weeks_en = array("sun","mon","tue","wed","thu","fri","sat");
	$target_week = $weeks_en[date("w", $target_ymd_time)];
}

$where1 = "";
$where2 = "";

if ((int)@$_REQUEST["sel_bldg_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where1 .= " and inpt.bldg_cd = " .      (int)@$_REQUEST["sel_bldg_cd"];
}
if ((int)@$_REQUEST["sel_ward_cd"] || $sel_team_id==0) { // チームがなければ必須指定
	$where1 .= " and inpt.ward_cd = " .      (int)@$_REQUEST["sel_ward_cd"];
}

if (@$_REQUEST["sel_ptrm_room_no"]!="") $where1 .= " and inpt.ptrm_room_no = " . (int)$_REQUEST["sel_ptrm_room_no"];

$team_inner_join = "";
if ($sel_team_id) {
	$team_inner_join =
		" inner join sot_ward_team_relation t on (".
		"   t.bldg_cd = inpt.bldg_cd and t.ward_cd = inpt.ward_cd and t.ptrm_room_no = inpt.ptrm_room_no".
		"   and team_id = " . $sel_team_id.
		" )";
}

if (@$_REQUEST["sel_ptif_id"]!="") {
	$where2 .= " and (pt.ptif_id = '".pg_escape_string($_REQUEST["sel_ptif_id"])."')";
}

// 対象日の入院中患者情報
// 通常ありえないが、一日内に複数の入院レコードがあっては困るので、グループバイしておく
$sql_nyuin_kanja_list =
	" select".
	" inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name".
	" from (".
			" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id".
			",max(inpt.inpt_in_dt) as inpt_in_dt".
			",min(inpt.inpt_out_dt) as inpt_out_dt".
			" from (".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id, inpt.inpt_in_dt, '' as inpt_out_dt".
					" from inptmst inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'"." " . $where1.
					" union".
					" select inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id, inpt.inpt_in_dt, inpt.inpt_out_dt".
					" from inpthist inpt".
					$team_inner_join.
					" where inpt.inpt_in_dt <= '" . $target_yyyymmdd . "'".
					" and inpt.inpt_out_dt >= '" . $target_yyyymmdd . "'".
					" " . $where1.
			" ) inpt".
			" group by inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id".
	" ) inpt".
	" left join ptifmst pt on (pt.ptif_id = inpt.ptif_id)".
	" left join bldgmst bldg on (bldg.bldg_cd = inpt.bldg_cd)".
	" left join wdmst wd on (wd.bldg_cd = inpt.bldg_cd and wd.ward_cd = inpt.ward_cd)".
	" left join ptrmmst ptrm on (ptrm.bldg_cd = inpt.bldg_cd and ptrm.ward_cd = inpt.ward_cd and ptrm.ptrm_room_no = inpt.ptrm_room_no)".
	" where 1 = 1".
	" " . $where2.
	" group by".
	" inpt.ptif_id, inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_sect_id".
	",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm, bldg.bldg_name, wd.ward_name, ptrm.ptrm_name";

// スケジュールと受付実施情報を照らしあわせた情報
// およびひとつひとつの指示を照らし合わせた情報
$sql_schedule =
	" select".
	" sss.apply_id".
	",sss.start_ymd".
	",sss.update_seq".
	",sss.schedule_seq".
	",sss.tmpl_chapter_code".
	",sss.tmpl_id".
	",sss.tmpl_file_name".
	",sss.summary_id".
	",sss.summary_seq".
	",sss.emp_id".
	",sss.ptif_id as d_ptif_id".
	",sss.xml_file_name".
	",sss.assign_pattern".
	",sss.start_ymd".
	",sss.end_ymd".
	",sss.times_per_ymd".
	",date_part('month',sss.regist_timestamp)||'/'||date_part('day',sss.regist_timestamp)||' '||".
	" date_part('hour',sss.regist_timestamp)||':'||to_char(sss.regist_timestamp, 'MI') as regist_timestamp".
	",sssv.schedule_various_seq".
	",sssv.string20". //これが本文
	",d.ptif_id".
	",d.bldg_cd".
	",d.ward_cd".
	",d.ptrm_room_no".
	",d.inpt_sect_id".
	",d.ptif_lt_kaj_nm".
	",d.ptif_ft_kaj_nm".
	",d.bldg_name".
	",d.ward_name".
	",d.ptrm_name".
	",sm.cre_date".
	",swa.accepted_emp_id".
	",swa.operated_emp_id".
	" from sot_summary_schedule sss".
	" inner join (".$sql_nyuin_kanja_list.") d on (d.ptif_id = sss.ptif_id)".
	" inner join summary sm on (sm.summary_seq = sss.summary_seq)".
	" inner join sot_summary_schedule_various sssv on ( sssv.schedule_seq = sss.schedule_seq )".
	" left join sot_worksheet_apply swa on (".
	"   swa.schedule_various_seq = sssv.schedule_various_seq".
	"   and swa.worksheet_date = '" . $target_yyyymmdd . "'".
	" )".
	" where sss.delete_flg = false".
	" and sss.start_ymd <= " . $target_yyyymmdd.
	" and sss.end_ymd >= " . $target_yyyymmdd.
	" and sss.times_per_ymd > 0".
	" and sss.temporary_update_flg = 0".
	" and tmpl_file_name in ('tmpl_Siji_YosokuSiji.php','tmpl_Siji_IsiSiji.php')".
	" order by bldg_cd, ward_cd, ptrm_room_no, d.ptif_id, sss.tmpl_chapter_code, sss.schedule_seq";

$sel = $c_sot_util->select_from_table($sql_schedule);
$data = (array)pg_fetch_all($sel);

// ここにて抽出するのは
// 医師指示  MERGE型、検索で抽出されたら、そのまま表示する
// Dr.Call   OVERRIDE型、過去最新と、本日分を再抽出しないといけない。
// 予測指示  指示単体はMERGE型、ただしオーダ単位でOVERRIDEを行う。ややこしい。

// まずは一度ループして、昨日以前の最新のスケジュールはどれか、を取得する。
// あとで本日分とマージする。
$latest_schedule_seq = array(); // オーバーライド判定用
foreach($data as $key => $row){
	if (!$row) continue;
	if ($row["tmpl_file_name"] != "tmpl_Siji_YosokuSiji.php") continue; // 医師指示は関係ない。予測指示とDr.Callのみ。
	// Dr.Callは１オーダ(summary_seq)に対して、ひとつしか存在しない。
	// なので最新のオーダが取得できれば、過去最新のドクターコールがわかる。
	if (trim($row["tmpl_chapter_code"]) == "drcl"){
		if ((int)$row["start_ymd"] < $target_yyyymmdd) { // 本日発効されたものは、関係ない。昨日までに発効されたやつが問題。
			$latest_schedule_seq[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][$row["ptif_id"]] = $row["summary_seq"];
		}
	}

	// 予測指示は１オーダ(summary_seq)に対して、複数のスケジュール(schedule_seq)が存在する。
	// このschedule_seqはMERGE型、つまりmergeされるが、
	// オーダ単位ではオーバーライドさせる必要がある。すなわち過去最新のオーダがわかればよい。

	// ただし予測指示の開始日は、スケジュール個々のものである。オーダの開始日ではない。start_ymdを参照してはならない。
	// オーダ発効日はcre_dateを参照する。
	if (trim($row["tmpl_chapter_code"]) != "drcl"){
		if ((int)$row["cre_date"] < $target_yyyymmdd) { // 本日発効されたものは、関係ない。昨日までに発効されたやつが問題。
			$latest_schedule_seq[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][$row["ptif_id"]] = $row["summary_seq"];
		}
	}
}


$schedule_ptif = array();
$schedule_bunrui = array();
$schedule_detail = array();
$last_summary_seq = 0;
$last_bunrui_key = "";
foreach($data as $key => $row){
	if (!$row) continue;
	// 過去分で、Dr.Callか予測指示のとき、最新のみを取得。
	if ((int)$row["start_ymd"] < $target_yyyymmdd) {
		if ($row["tmpl_file_name"] == "tmpl_Siji_YosokuSiji.php") {
			$latest_seq = @$latest_schedule_seq[$row["bldg_cd"]][$row["ward_cd"]][$row["ptrm_room_no"]][$row["ptif_id"]];
			if ($latest_seq != $row["summary_seq"]) continue;
		}
	}

	$bunrui = "";
	if ($row["tmpl_file_name"] == "tmpl_Siji_IsiSiji.php") $bunrui = "医師指示";
	else if ($row["tmpl_file_name"] == "tmpl_Siji_YosokuSiji.php") {
		$bunrui = (trim($row["tmpl_chapter_code"]) == "drcl" ? "Dr.Call" : "予測指示");
	}

	$row["bunrui"] = $bunrui;
	$ptif_key = $row["bldg_cd"]."@".$row["ward_cd"]."@".$row["ptrm_room_no"]."@".$row["ptif_id"];
	$bunrui_key = $ptif_key."@".$bunrui;

	// 同じ患者、同じ指示種類なのにサマリ連番が変る場合は行の差込を行う。
	$insert_line_count = 0;
	if ($last_bunrui_key == $bunrui_key){
		if ($last_summary_seq != $row["summary_seq"]) $insert_line_count = 1;
	}

	$schedule_ptif[$ptif_key] = $row;
	$schedule_ptif_count[$ptif_key] = @$schedule_ptif_count[$ptif_key] + 1 + $insert_line_count;

	$schedule_bunrui[$ptif_key][$bunrui_key] = $row;
	$schedule_bunrui_count[$ptif_key][$bunrui_key] = @$schedule_bunrui_count[$ptif_key][$bunrui_key] + 1 + $insert_line_count;

	$schedule_detail[$ptif_key][$bunrui_key][] = $row;
	$last_summary_seq = $row["summary_seq"];
	$last_bunrui_key = $bunrui_key;
}
?>

<?
if (@$_REQUEST["excelout"]){
	ob_end_clean();
	xlsOut($schedule_detail);
	die;
}
ob_end_flush();
?>


<? if (!@$_REQUEST["sel_ward_cd"] && !$sel_team_id) { ?>
	<div style="padding:10px; color:#aaa; text-align:center">病棟またはチームを指定してください。</div>
<? } ?>

<? if (count($schedule_ptif)){ ?>
	<table class="listp_table print_target_table">
	<tr>
		<th>病棟</th>
		<th>病室</th>
		<th>患者ID</th>
		<th>患者氏名</th>
		<th>指示種類</th>
		<th>内容</th>
		<th style="text-align:center">指示医</th>
		<th style="text-align:center">受付</th>
		<th style="text-align:center">実施</th>
	</tr>
	<? foreach($schedule_ptif as $ptif_key => $head){ ?>
	<?   $rowspan1 = $schedule_ptif_count[$ptif_key]; ?>
	<?   $ptif_idx = 0; ?>
	<?   foreach($schedule_bunrui[$ptif_key] as $bunrui_key => $detail){ ?>
	<?     $bunrui_idx = 0; ?>
	<?     $rowspan2 = $schedule_bunrui_count[$ptif_key][$bunrui_key]; ?>
	<?     $last_summary_seq = ""; ?>
	<?     foreach($schedule_detail[$ptif_key][$bunrui_key] as $idx => $row){ ?>
	<?       $ptif_idx++; ?>
	<?       $bunrui_idx++; ?>
	<?       if(!$ptif_key) continue; ?>


		<? if ($last_summary_seq != "" && $last_summary_seq != $row["summary_seq"]){ ?>
		<? // 内容１から４：ぶち抜きで日時 ?>
		<tr>
			<td colspan="4" style="padding:2px; background-color:#ffbfff; text-align:center"><?= $row["regist_timestamp"] ?>&nbsp;登録</td>
		</tr>
		<? } ?>


	<tr>
		<? if ($ptif_idx == 1) { ?>
		<td style="width:4%; white-space:nowrap; vertical-align:top" rowspan="<?=$rowspan1?>"><?= h($head["ward_name"])?></td>
		<td style="width:4%; white-space:nowrap; vertical-align:top" rowspan="<?=$rowspan1?>"><?= h($head["ptrm_name"])?></td>
		<td style="width:8%; white-space:nowrap; vertical-align:top" rowspan="<?=$rowspan1?>"><?=h($head["ptif_id"])?></td>
		<td style="width:8%; white-space:nowrap; vertical-align:top" rowspan="<?=$rowspan1?>"><?=h($head["ptif_lt_kaj_nm"]." ".$head["ptif_ft_kaj_nm"])?></td>
		<? } ?>
		<? if ($bunrui_idx == 1) { ?>
		<td style="width:4%; white-space:nowrap; vertical-align:top" rowspan="<?=$rowspan2?>"><?= h($detail["bunrui"])?></td>
		<? } ?>

		<? // 内容１：指示文字列 ?>
		<td style="width:48%;"><?= $c_sot_util->escape_for_textarea($row["string20"]) ?><br/></td>
		<? // 内容２：指示医 ?>
		<td style="width:8%; white-space:nowrap; vertical-align:top; text-align:center"><?= h($c_sot_util->get_emp_name_using_cache($row["emp_id"]))?></td>
		<? // 内容３：受付 ?>
		<td style="width:8%; white-space:nowrap; vertical-align:top; text-align:center" id="element_accept_<?=$row["schedule_various_seq"]?>">
		<?
			$emp_name = $c_sot_util->get_emp_name_using_cache($row["accepted_emp_id"]);
			if ($emp_name){
				echo '<span style="color:#880">'.$emp_name."</span>";
			} else {
				echo '<input type="button" onclick="showApplyDialog(\'accept\',\''.$row["schedule_various_seq"].'\');"';
				echo ' class="print_hidden" value="受付"/>';
			}
		?>
		</td>
		<? // 内容４：実施 ?>
		<td style="width:8%; white-space:nowrap; vertical-align:top; text-align:center" id="element_operate_<?=$row["schedule_various_seq"]?>">
		<?
			$emp_name = h($c_sot_util->get_emp_name_using_cache($row["operated_emp_id"]));
			if ($emp_name){
				echo '<span style="color:#880">'.$emp_name."</span>";
			} else {
				echo '<input type="button" onclick="showApplyDialog(\'operate\',\''.$row["schedule_various_seq"].'\');"';
				echo ' class="print_hidden" value="実施" />';
			}
		?>
		</td>

		<? $last_summary_seq = $row["summary_seq"]; ?>

		</tr>
			<? } ?>
		<? } ?>
	<? } ?>
	</table>
<? } ?>

<?//==========================================================================?>
<?// 承認ダイアログ                                                           ?>
<?//==========================================================================?>
<div id="apply_dialog" style="position:absolute; width:250px; border:1px solid #1484dc; border-top:1px solid #1273c2; background-color:#f4f4f4; font-size:12px; margin-top:4px; text-align:center; display:none;">
	<div style="color:#fff; padding:3px; background-color:#2a9cf4;" id="apply_dialog_titlebar">承認</div>
	<div style="background-color:#f4f4f4; border:1px solid #999">
		<div style="margin:4px">
			<div style="background:url(css/img/bg-b-gura.gif) top repeat-x; border:1px solid #bad9f2">
				<div style="padding:2px">
					<b><?=$date_y?></b><span style="padding:2px">年</span><b><?=(int)$date_m?></b><span style="padding:2px">月</span><b><?=(int)$date_d?></b><span style="padding:2px">日</span>
				</div>
				<div style="padding:4px;">
					<table style="border:0; border-collapse:collapse" class="plane"><tr>
						<td style="width:20%"></td>
						<td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_h" onfocus="adjcalc('h')"></td>
						<td style="width:10%"><a href="" onclick="calcf('h'); return false;" class="always_blue">時</a><div id="apply_dialog_tdh"></div></td>
						<td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_m" onfocus="adjcalc('m')"></td>
						<td style="width:10%"><a href="" onclick="calcf('m'); return false;" class="always_blue">分</a><div id="apply_dialog_tdm"></div></td>
						<td style="width:10%"><input type="text" style="width:30px; ime-mode:disabled" maxlength="2" id="apply_dialog_s" onfocus="adjcalc('s')"></td>
						<td style="width:10%"><a href="" onclick="calcf('s'); return false;" class="always_blue">秒</a><div id="apply_dialog_tds"></div></td>
						<td style="width:20%"></td>
					</tr></table>
				</div>
			</div>
		</div>
		<div style="background-color:#f4f4f4; margin-top:4px">
			<div style="padding:2px" id="apply_dialog_description">上記を受付時刻として、受付を登録します。</div>
			<div>よろしいですか？</div>
			<div style="padding:6px; padding-top:10px">
				<input type="button" value="OK" style="width:80px" onclick="registAcceptOperate(apply_dialog_mode);" id="apply_dialog_ok" />
				<input type="button" value="キャンセル" style="width:80px" onclick="hideApplyDialog();"/>
			</div>
		</div>
	</div>
</div>
<?//==========================================================================?>
<?// 承認ダイアログの電卓                                                     ?>
<?//==========================================================================?>
<div style="position:absolute; border:1px solid #05c; width:100px; display:none" id="apply_dialog_calculator">
	<div style="border:1px solid #aaa; background-color:#f0f0f0;" class="timecalcullator">
		<div style="padding:2px;">
			<div style="float:left; color:#666"><span id="apply_dialog_caption"></span>の選択</div>
			<div style="float:right"><input type="image" src="js/yui_0.12.2/build/calendar/assets/calx.gif" onclick="hidecalc(); return false;"></div>
			<div style="clear:both"></div>
		</div>
		<table style="width:100%">
			<tr>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(1)">1</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(2)">2</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(3)">3</td>
			</tr>
			<tr>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(4)">4</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(5)">5</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(6)">6</td>
			</tr>
			<tr>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(7)">7</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(8)">8</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(9)">9</td>
			</tr>
			<tr>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc('D')">D</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc(0)">0</td>
				<td onmouseover="calc1(this)" onmouseout="calc2(this)" onclick="calc('C')">C</td>
			</tr>
		</table>
	</div>
</div>

</body>
<? pg_close($con); ?>
</html>






<? //-------------------------------------------------------------------------?>
<? // エクセル出力                                                            ?>
<? //-------------------------------------------------------------------------?>
<? function xlsOut($schedule) { ?>
<?   global $c_sot_util; ?>
<?   header("content-type: application/vnd.ms-excel"); ?>
<?   header("Pragma: public"); ?>
<?   header("Expires: 0"); ?>
<?   header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); ?>
<?   header("Content-Disposition: attachment; filename=fplus_aggregate_".Date("Ymd_Hi").".xls;"); ?>
<?   header('Content-Type: application/vnd.ms-excel'); ?>
<?   header("Content-Transfer-Encoding: binary"); ?>
<?   $fontfamily = mb_convert_encoding("ＭＳ Ｐゴシック", "SJIS"); ?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=shift_jis">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="xls.files/filelist.xml">
<link rel=Edit-Time-Data href="xls.files/editdata.mso">
<link rel=OLE-Object-Data href="xls.files/oledata.mso">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Version>11.9999</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.98in .79in .98in .79in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	border:none;
	mso-protection:locked visible;
	mso-style-name:標準;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	border:.5pt solid windowtext;
	background:silver;
	mso-pattern:auto none;}
.xl25
	{mso-style-parent:style0;
	border:.5pt solid windowtext;}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:<?=$fontfamily?>, monospace;
	mso-font-charset:128;
	mso-char-type:katakana;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>Sheet1</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>300</x:HorizontalResolution>
      <x:VerticalResolution>300</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:DoNotDisplayGridlines/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet2</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
   <x:ExcelWorksheet>
    <x:Name>Sheet3</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>4725</x:WindowHeight>
  <x:WindowWidth>8475</x:WindowWidth>
  <x:WindowTopX>480</x:WindowTopX>
  <x:WindowTopY>30</x:WindowTopY>
  <x:AcceptLabelsInFormulas/>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>

<table x:str border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse;table-layout:fixed;'>
 <tr>
  <td class=xl24><ruby><?=mb_convert_encoding("病棟","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("病室","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("患者ID","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("患者氏名","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("指示種類","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("内容","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("指示医","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("受付","SJIS")?></ruby></td>
  <td class=xl24 style='border-left:none'><ruby><?=mb_convert_encoding("実施","SJIS")?></ruby></td>
 </tr>
	<? foreach($schedule as $key => $rows1){ ?>
	<?   foreach($rows1 as $idx1 => $rows2){ ?>
	<?     foreach($rows2 as $idx2 => $row){ ?>
				<tr>
					<td class=xl25 style='border-top:none'><?= mb_convert_encoding(h($row["ward_name"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["ptrm_name"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["ptif_id"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["bunrui"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($row["string20"]),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($c_sot_util->get_emp_name_using_cache($row["emp_id"])),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($c_sot_util->get_emp_name_using_cache($row["accepted_emp_id"])),"SJIS")?></td>
					<td class=xl25 style='border-top:none;border-left:none'><?= mb_convert_encoding(h($c_sot_util->get_emp_name_using_cache($row["operated_emp_id"])),"SJIS")?></td>
				</tr>
	<?     } ?>
	<?   } ?>
	<? } ?>

</table>

</body>

</html>
<? } ?>
