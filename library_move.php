<?php
// AJAXによるファイル移動処理

require_once("about_session.php");
require_once("about_authority.php");
require_once("library_common.php");

$fname = $PHP_SELF;

set_time_limit (1000);

//------------------------------------------------------------------------------
// 必要なリクエスト
//------------------------------------------------------------------------------
// ajax_actionは必ず渡ってくるが、何かの値が仕込まれていれば何でもよい。
// DBエラーの場合の応答内容をajax対応にしたいだけなので、
// 実はここで取得する必要は無い。library_common.phpでエラーが発生したときの応答判定時に取得している。
$ajax_action = $_REQUEST["ajax_action"];
$dest_c = $_REQUEST["c"]; // 移動先カテゴリ
$dest_f = $_REQUEST["f"]; // 移動先フォルダ
$dest_a = $_REQUEST["a"]; // 書庫。移動元と移動先は同じ

$content_type = $_REQUEST["content_type"];
$content_value = $_REQUEST["content_value"];

if ((int)$dest_c<=0 && (int)$dest_f<=0 && $dest_c!="rootfolder") { echo "呼出パラメータが不正です。(cf)"; die; }
if ((int)$dest_a<=0)               { echo "呼出パラメータが不正です。(a)";  die; }
if (!$ajax_action)            { echo "呼出パラメータが不正です。(ajax_action)"; die; }
if ($content_type!="document" && $content_type!="folder" && $content_type!="category") { echo "呼出パラメータが不正です。(content_type)"; die; }


if ($dest_c=="rootfolder") $dest_c = "";

//------------------------------------------------------------------------------
// セッションのチェック
//------------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") { echo "SESSION_EXPIRED"; die; }

// 権限チェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") { echo "SESSION_EXPIRED"; die; }



// データベースに接続
$con = connect2db($fname);

if ($ajax_action) {
	// ログインユーザの職員情報を取得
	$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) libcom_goto_error_page_and_die($sql." ".$cond);
	$empmstRow = pg_fetch_assoc($sel_emp);
	$emp_id = $empmstRow["emp_id"];

	//------------------------------------------------------------------------------
	// 移動処理
	//------------------------------------------------------------------------------
    pg_query($con, "begin");
    $msg = lib_move_content($con, $fname, $emp_id, $content_type, $content_value, $dest_c, $dest_f);
    if ($msg!="ok") {
	    pg_query($con, "rollback");
	    echo $msg;
	    die;
	}
	echo "ok";
    pg_query($con, "commit");
    die;
}
