<?

///*****************************************************************************
// 勤務シフト作成 | シフト作成 PHP5.1.6 ネイティブExcel対応、週間表
// duty_shift_menu_excel_5.phpを流用したため、使わないロジックが残っています
///*****************************************************************************
ob_start();
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");
require_once("duty_shift_menu_excel_5_workshop.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);

///-----------------------------------------------------------------------------
// Excelオブジェクト生成
///-----------------------------------------------------------------------------
$excelObj = new ExcelWorkShop();

///-----------------------------------------------------------------------------
// Excel列名
///-----------------------------------------------------------------------------
$excelColumnName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');

///-----------------------------------------------------------------------------
//処理
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$excel_flg = "1";   //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
$download_data = "";  //ＥＸＣＥＬデータ

///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];

///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") {
    $duty_yyyy = $now_yyyy;
}
if ($duty_mm == "") {
    $duty_mm = $now_mm;
}

///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;

if ($extra_day == "") { $extra_day = 0; }
///-----------------------------------------------------------------------------
// system config
///-----------------------------------------------------------------------------
$conf = new Cmx_SystemConfig();

///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
$individual_flg = "";
$hope_get_flg = "";

if ($data_cnt == "") {
    $data_cnt = 0;
}
//入力形式で表示する日（開始／終了）
$edit_start_day = 0;
$edit_end_day = 0;

$group_id = $cause_group_id;

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();

// 有効なグループの職員を取得
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm); //20140210
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
$data_wktmgrp = $obj->get_wktmgrp_array();

// 追加がある場合、取得
$add_staff_id_all_array = explode(",", $add_staff_id_all);
$add_staff_id_all_cnt = count($add_staff_id_all_array);
if ($add_staff_cnt > 0 || $add_staff_id_all_cnt > 0) {
    $wk_emp_id_array = explode(",", $add_staff_id);
    $wk_array = array_merge($add_staff_id_all_array, $wk_emp_id_array);
    $wk_cnt = count($data_emp);
    $add_staff_id_all = "";
    foreach ($wk_array as $wk_add_staff_id) {
        if ($wk_add_staff_id == "") {
            continue;
        }
        $wk_data_emp = $obj->get_empmst_array($wk_add_staff_id);
        $data_emp[$wk_cnt]["id"] = $wk_data_emp[0]["id"];
        $data_emp[$wk_cnt]["name"] = $wk_data_emp[0]["name"];
        $data_emp[$wk_cnt]["job_id"] = $wk_data_emp[0]["job_id"];
        $data_emp[$wk_cnt]["st_id"] = $wk_data_emp[0]["st_id"];
        $data_emp[$wk_cnt]["join"] = $wk_data_emp[0]["join"];
        $wk_cnt++;

        if ($add_staff_id_all != "") {
            $add_staff_id_all .= ",";
        }
        $add_staff_id_all .= $wk_add_staff_id;
    }
}

///-----------------------------------------------------------------------------
// 指定グループの病棟名を取得
///-----------------------------------------------------------------------------
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
if (count($wk_group) > 0) {
    $group_name = $wk_group[0]["group_name"];
    $pattern_id = $wk_group[0]["pattern_id"];
    $start_month_flg1 = $wk_group[0]["start_month_flg1"];
    $start_day1 = $wk_group[0]["start_day1"];
    $month_flg1 = $wk_group[0]["month_flg1"];
    $end_day1 = $wk_group[0]["end_day1"];
    $start_day2 = $wk_group[0]["start_day2"];
    $month_flg2 = $wk_group[0]["month_flg2"];
    $end_day2 = $wk_group[0]["end_day2"];
    $print_title = $wk_group[0]["print_title"];
    $reason_setting_flg = $wk_group[0]["reason_setting_flg"];
}
if ($start_month_flg1 == "") {
    $start_month_flg1 = "1";
}
if ($start_day1 == "" || $start_day1 == "0") {
    $start_day1 = "1";
}
if ($month_flg1 == "" || $end_day1 == "0") {
    $month_flg1 = "1";
}
if ($end_day1 == "" || $end_day1 == "0") {
    $end_day1 = "99";
}
if ($start_day2 == "" || $start_day2 == "0") {
    $start_day2 = "0";
}
if ($month_flg2 == "" || $end_day2 == "") {
    $month_flg2 = "1";
}
if ($end_day2 == "" || $end_day2 == "0") {
    $end_day2 = "99";
}
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

$start_date = $arr_date[0];
$end_date = date("Ymd", strtotime("+6 day", strtotime($arr_date[1])));

$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$day_cnt = count($calendar_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$tmp_date = $start_date;
for ($k = 1; $k <= $day_cnt; $k++) {
    $tmp_date = strtotime($tmp_date);
    $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
    $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}

// 週表のため、翌月またぎの曜日表記処理
$wk_start = $day_cnt + 1;
$wk_end = $day_cnt + 7; // 一週間分追加
for ($k = $wk_start; $k < $wk_end; $k++) {
    $tmp_date = strtotime($tmp_date);
    $week_array[$k]["name"] = $obj->get_weekday($tmp_date);
    $tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}

///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
//応援追加されている出勤パターングループ確認 
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
}
//応援なしの場合、同じパターングループのデータを設定
else {
    $data_atdptn_all = $data_atdptn;
    $data_pattern_all = $data_pattern;
}
//20140220 end
///-----------------------------------------------------------------------------
//遷移元のデータに変更
///-----------------------------------------------------------------------------
if ($excel_row_count == "2") {
    switch ($excel_row_type) {
        case "0":
            $show_data_flg = "0";
            break; // 空行
        case "1":
            $show_data_flg = "1";
            break; // 勤務実績
        case "2":
            $show_data_flg = "2";
            break; // 勤務希望
        case "3":
            $show_data_flg = "3";
            break; // 当直
        case "4":
            $show_data_flg = "4";
            break; // コメント
        default :
            $show_data_flg = "";
    }
}
else {
    $show_data_flg = "";
}
$set_data = array();
$plan_array = array();
for ($i = 0; $i < $data_cnt; $i++) {
    //応援追加情報
    $assist_str = "assist_group_$i";
    $arr_assist_group = explode(",", $$assist_str);
    //希望分
    if ($show_data_flg == "2") {
        $rslt_assist_str = "rslt_assist_group_$i";
        $arr_rslt_assist_group = explode(",", $$rslt_assist_str);
    }
    ///-----------------------------------------------------------------------------
    //遷移元のデータに変更
    ///-----------------------------------------------------------------------------
    for ($k = 1; $k <= $day_cnt; $k++) {
    	$src_k = $k + $extra_day;

        $set_data[$i]["plan_rslt_flg"] = "1";   //１：実績は再取得
        $wk2 = "pattern_id_$i" . "_" . $src_k;
        $wk3 = "atdptn_ptn_id_$i" . "_" . $src_k;
        $wk5 = "comment_$i" . "_" . $src_k;
        $wk6 = "reason_2_$i" . "_" . $src_k;

        $set_data[$i]["staff_id"] = $staff_id[$i];     //職員ＩＤ

        $set_data[$i]["pattern_id_$k"] = $$wk2;      //出勤グループＩＤ

        if ((int) substr($$wk3, 0, 2) > 0) {
            $set_data[$i]["atdptn_ptn_id_$k"] = (int) substr($$wk3, 0, 2); //出勤パターンＩＤ
        }
        if ((int) substr($$wk3, 2, 2) > 0) {
            $set_data[$i]["reason_$k"] = (int) substr($$wk3, 2, 2); //事由
        }

        $set_data[$i]["reason_2_$k"] = $$wk6;      //事由（午前休暇／午後休暇）
        $set_data[$i]["comment_$k"] = $$wk5;       //コメント
        //応援情報
        $set_data[$i]["assist_group_$k"] = $arr_assist_group[$src_k - 1];
        if ($show_data_flg == "2") {
            $set_data[$i]["rslt_assist_group_$k"] = $arr_rslt_assist_group[$src_k - 1];
        }
        // 勤務シフト希望のデータ
        $wk7 = "rslt_pattern_id_$i" . "_" . $src_k;
        $wk8 = "rslt_id_$i" . "_" . $src_k;
        $wk9 = "rslt_reason_2_$i" . "_" . $src_k;
        if ($show_data_flg == "2" && $plan_results_flg != "3") { //実績入力画面を除く 20160223
            // 勤務シフト希望のデータ
            $set_data[$i]["rslt_pattern_id_$k"] = $$wk7;      //出勤グループＩＤ

            if ((int) substr($$wk8, 0, 2) > 0) {
                $set_data[$i]["rslt_id_$k"] = (int) substr($$wk8, 0, 2); //出勤パターンＩＤ
            }
            if ((int) substr($$wk8, 2, 2) > 0) {
                $set_data[$i]["rslt_reason_$k"] = (int) substr($$wk8, 2, 2);  //事由
            }

            $set_data[$i]["rslt_reason_2_$k"] = $$wk9;      //事由（午前有給／午後有給）
        }
    }
}
$plan_array = $obj->get_duty_shift_plan_array(
    $group_id, $pattern_id, $duty_yyyy, $duty_mm, $day_cnt, $individual_flg, $hope_get_flg,
    $show_data_flg, $set_data, $week_array, $calendar_array, $data_atdptn, $data_pattern_all,
    $data_st, $data_job, $data_emp);
$data_cnt = count($plan_array);

///-----------------------------------------------------------------------------
// 行タイトル（行ごとの集計する勤務パターン）を取得
// 列タイトル（列ごとの集計する勤務パターン）を取得
///-----------------------------------------------------------------------------
$title_gyo_array = $obj->get_total_title_array($pattern_id, "1", $data_pattern);
$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
    $err_msg_2 = "勤務シフトグループ情報が未設定です。管理者に連絡してください。";
}

///-----------------------------------------------------------------------------
//個人日数合計（行）を算出設定
//個人日数合計（列）を算出設定
///-----------------------------------------------------------------------------
//2段目が実績の場合、システム日以降は、行を集計しない。
if ($show_data_flg == "1") {
    $wk_rs_flg = "2";
}
else {
    $wk_rs_flg = "";
}
$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"2", "", $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);

//休暇事由情報
$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);

///-----------------------------------------------------------------------------
//最大文字数算出
///-----------------------------------------------------------------------------
$max_font_cnt = 1;
for ($i = 0; $i < $data_cnt; $i++) {
    for ($k = 1; $k <= $day_cnt; $k++) {
        //予定
        if ($plan_array[$i]["font_name_$k"] != "") {
            if ($max_font_cnt < strlen($plan_array[$i]["font_name_$k"])) {
                if (strlen($plan_array[$i]["font_name_$k"]) <= 2) {
                    $max_font_cnt = 1;
                }
                else if (strlen($plan_array[$i]["font_name_$k"]) <= 4) {
                    $max_font_cnt = 2;
                }
            }
        }
        //実績
        if ($plan_array[$i]["rslt_name_$k"] != "") {
            if ($max_font_cnt < (int) strlen($plan_array[$i]["rslt_name_$k"])) {
                if (strlen($plan_array[$i]["rslt_name_$k"]) <= 2) {
                    $max_font_cnt = 1;
                }
                else if (strlen($plan_array[$i]["rslt_name_$k"]) <= 4) {
                    $max_font_cnt = 2;
                }
            }
        }
    }
}

///-----------------------------------------------------------------------------
//ファイル名
///-----------------------------------------------------------------------------
$filename = 'shift_' . $duty_yyyy . sprintf("%02d", $duty_mm) . '.xls';
if (preg_match('/MSIE/i', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
    $filename, $encoding, mb_internal_encoding());

///-----------------------------------------------------------------------------
// Excel シート名
///----------------------------------------------------------------------------
$excelObj->SetSheetName('shift_' . $duty_yyyy . sprintf("%02d", $duty_mm));

///-----------------------------------------------------------------------------
// 色
///-----------------------------------------------------------------------------
$week_color = $_REQUEST['excel_weekcolor'];
$hope_color = $_REQUEST['excel_hopecolor'];
$match_color = $conf->get('shift.hope.match.color');
if (empty($match_color)) {
    $match_color = '#000000';
}
$match_bgcolor = $conf->get('shift.hope.match.bgcolor');
if (empty($match_bgcolor)) {
    $match_bgcolor = '#00ff00';
}
$unmatch_color = $conf->get('shift.hope.unmatch.color');
if (empty($unmatch_color)) {
    $unmatch_color = '#000000';
}
$unmatch_bgcolor = $conf->get('shift.hope.unmatch.bgcolor');
if (empty($unmatch_bgcolor)) {
    $unmatch_bgcolor = '#ffff00';
}
$plan_individual = $obj->get_plan_individual($group_id, $pattern_id, $start_date, $end_date);

///*****************************************************************************
//ＨＴＭＬ作成
///*****************************************************************************
$start_day = $excel_date;

showData($err_msg_2,
        $group_name,
        $duty_yyyy,
        $duty_mm,
        $print_title,
        $now_yyyy,	//作成年
        $now_mm,    //作成月
        $now_dd,    //作成日
        $day_cnt,
        $st_name_array,
        $excelColumnName,
        $excelObj,
        $show_data_flg,$con,$fname);

$total_print_flg = "1"; // 集計は常に出力する PHP5.1.6対応
$hol_dt_flg = '0'; // 休暇詳細は当面は出さない PHP5.1.6対応

//日・曜日
showTitleWeek($obj, $duty_yyyy, $duty_mm, $day_cnt, $plan_array, $week_array, $title_gyo_array,
              $calendar_array, $total_print_flg, $show_data_flg, $hol_dt_flg, $title_hol_array,
              $excelColumnName, $excelObj, $excel_blancrows, $start_day, $week_color
);

//勤務シフトデータ（職員（列）年月日（行）
showList(
        $obj,
        $obj_duty,
        $pattern_id,
        $plan_array,
        $title_gyo_array,
        $gyo_array,
        0,
        $data_cnt,
        $day_cnt,
        $max_font_cnt,
        $total_print_flg,
        $show_data_flg,
        $group_id,
        $reason_setting_flg,
        $hol_dt_flg,
        $title_hol_array,
        $hol_cnt_array,
        $calendar_array,
        $week_array,
        $excelColumnName,
        $excelObj,
        $excel_blancrows,
        $start_day,
        $week_color,
        $hope_color
);

// Excelカーソルを左上へ置く
$excelObj->SetArea("A1");
$excelObj->SetPosH("LEFT");

///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();

$excelObj->PaperSize($excel_paper_size); // 用紙サイズ
$excelObj->PageSetUp('virtical'); // 向き。

// 倍率と自動或いは利用者指定
$excelObj->PageRatio($excel_setfitpage, $excel_ratio);

// 余白、左右上下頭足
$excelObj->SetMargin($excel_mleft,$excel_mright,$excel_mtop,$excel_mbottom,$excel_mhead,$excel_mfoot);

// ダウンロード
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=' . $filename);
header('Cache-Control: max-age=0');
$excelObj->OutPut();

//設定保存
$arr_key = $obj->get_x5para_conf_key();
foreach ($arr_key as $key => $reqkey) {
    $val = $_REQUEST["$reqkey"];
    $obj->conf_set($group_id, $key, $val);
}

ob_end_flush();

pg_close($con);

/* * ********************************************************************** */
//
// シート初期値、医療機関名称
//
/* * ********************************************************************** */

function showData($err_msg,
	$group_name,
	$duty_yyyy,
	$duty_mm,
	$print_title,
    $create_yyyy, //作成年
    $create_mm, //作成月
    $create_dd, //作成日
	$day_cnt,
	$st_name_array,$excelColumnName,$excelObj,$show_data_flg,$con,$fname){

    // 医療機関名を得る
    $sql = "SELECT prf_name FROM profile ";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $prf_name = pg_fetch_result($sel, 0, "prf_name");
    $excelObj->SetValueJPwith("A2", $prf_name, "");
    $wk_colspan = $day_cnt + 5;
    // 行の高さ
    $excelObj->SetRowDim('1', 16.5);
    $excelObj->SetRowDim('2', 16.5);
    $excelObj->SetRowDim('3', 13.5);
    $excelObj->SetRowDim('4', 16.5);

    // 以後行高さは明細行で指定

    $wk_title = "$group_name  ";
    $wk_title .= $duty_yyyy . "年";
    $wk_title .= $duty_mm . "月　　";

    $excelObj->SetValueJPwith("A1", $wk_title, "BOLD"); //group_name
    $excelObj->SetPosH('LEFT'); // ←左→
    $excelObj->SetPosV('CENTER'); // ↑↓中央

    // 作成者
    global $obj, $group_id;
	$createEmpDispFlg = $obj->conf_get($group_id, "createEmpDispFlg");
	if ($createEmpDispFlg == "t") {
	    $excelCellPosition = $excelColumnName[7] . '2';
		$excelObj->SetArea($excelCellPosition); // セル位置
		$createEmpStat = $obj->conf_get($group_id, "createEmpStat");
		$createEmpName = $obj->conf_get($group_id, "createEmpName");
		$createEmpStr = '作成者：';
		if ($createEmpStat != "") {
			$createEmpStr .= '役職　'.$createEmpStat.'　　';
		}
		$createEmpStr .= '氏名　'.$createEmpName.'　　　印';
		$excelObj->SetValue(mb_convert_encoding($createEmpStr,'UTF-8','EUC-JP'));
	}

    return;
}

// 以下の関数はduty_shift_print_common_class.phpからの流用のため、修正がある場合は同時に行う
// 事由の出力について、印刷とは異なる
/* * ********************************************************************** */
//
// 見出し（日段）
//
/* * ********************************************************************** */

function showTitleWeek(
    $obj,
    $duty_yyyy, //勤務年
    $duty_mm, //勤務月
    $day_cnt, //日数
    $plan_array, // 勤務シフト情報
    $week_array, // 週情報
    $title_gyo_array, // 行合計タイトル情報
    $calendar_array, // カレンダー情報
    $total_print_flg = "1", // 集計行列の印刷フラグ 0しない 1:する
    $show_data_flg, // ２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直、４：コメント）
    $hol_dt_flg, // 休暇日数詳細 1:表示 "":非表示
    $title_hol_array, // 休暇タイトル情報
    $excelColumnName, // エクセル列名
    $excelObj, // ExcelWorkShop
    $excel_blancrows, // 曜日の下に空行をいれるかどうかのスイッチ 2012.8.22
    $start_day,  // 週間表の開始日(月曜日のみ)
    $week_color
) {

    $reason_2_array = $obj->get_reason_2();
    $rowPos = 3;
    $nextrowPos = $rowPos + 1;
    // 列幅は標準値(MS-Pゴシック12p)で設定する。
    // 指定単位はpoint数。1point=1/72インチ。
    // 以後、データ列の幅は日段見出しの処理で設定
    // 行集計の列幅は集計見出しのところで設定

    $excelTitleColPos = 0;

    // 曜日の下に空行をいれるかどうか
    if ($excel_blancrows == 0) {
        $excelMergeBottom = $nextrowPos;
    }
    else {
        $excelMergeBottom = $nextrowPos + 1;
    }

    $excelObj->SetArea($excelColumnName[$excelTitleColPos] . $rowPos);
    $excelObj->SetValue(mb_convert_encoding('職種', 'UTF-8', 'EUC-JP'));
    $excelObj->CellMerge($excelColumnName[$excelTitleColPos] . $rowPos . ":" . $excelColumnName[$excelTitleColPos] . $excelMergeBottom);
    $excelObj->SetColDim($excelColumnName[$excelTitleColPos], 8);
    $excelTitleColPos++;
    $excelObj->CellMerge($excelColumnName[$excelTitleColPos] . $rowPos . ":" . $excelColumnName[$excelTitleColPos] . $excelMergeBottom);
    $excelObj->SetArea($excelColumnName[$excelTitleColPos] . $rowPos . "");
    $excelObj->SetValue(mb_convert_encoding('氏名', 'UTF-8', 'EUC-JP'));
    $excelObj->SetColDim($excelColumnName[$excelTitleColPos], 18); // 3cmにする 18 = 30mm/((25.4/72*12point))*2.54

    //勤務状況（日）
    $wkend_day = $start_day + 7;
    $colmun_counter = 1;
    $nextdd = 1;
    for ($k = $start_day; $k < $wkend_day; $k++) {
        $wk_date = $calendar_array[$k - 1]["date"];
        $wk_dd = (int) substr($wk_date, 6, 2);
        if ($wk_dd == 0) {
            $wk_dd = $nextdd;
            $nextdd++;
        }
        $wk_back_color = "FFFFFF";
        if ($week_color) {
            if ($calendar_array[$k - 1]["type"] == "6") {
                $wk_back_color = "C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" or $calendar_array[$k - 1]["type"] == "5") {
            $wk_back_color = "E0FFFF";
        }
            else if ($week_array[$k]["name"] == "日" or $calendar_array[$k - 1]["type"] == "4") {
            $wk_back_color = "FF99CC";  //Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
        }
        }
        $excelObj->SetColDim($excelColumnName[$colmun_counter + $excelTitleColPos], 30); // 列幅
        $excelObj->SetArea($excelColumnName[$colmun_counter + $excelTitleColPos] . $rowPos);
        $excelObj->SetValue($wk_dd);
        $colmun_counter++;
    }
    $rightPosition = $k + $excelTitleColPos;
    $total_print_flg = "1"; // 集計行列は常に出力する // PHP5.1.6対応

    //-------------------------------------------------------------------------------
    //曜日段
    //-------------------------------------------------------------------------------
    //勤務状況（曜日）
    $wkend_day = $start_day + 7;
    $colmun_counter = 1;
    for ($k = $start_day; $k < $wkend_day; $k++) {
        //土、日、祝日の文字色＆背景色変更
        $wk_color = "";
        $wk_back_color = "FFFFFF";
        if ($week_color) {
            if ($calendar_array[$k - 1]["type"] == "6") {
               $wk_color = "#ff0000";
                $wk_back_color = "#C8FFD5";
            }
            else if ($week_array[$k]["name"] == "土" or $calendar_array[$k - 1]["type"] == "5") {
            $wk_color = "0000ff";
            $wk_back_color = "E0FFFF";
        }
            else if ($week_array[$k]["name"] == "日" or $calendar_array[$k - 1]["type"] == "4") {
            $wk_color = "ff0000";
            $wk_back_color = "FF99CC";  //Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
        }
        }
        //表示
        $wk_name = $week_array[$k]["name"];
        $excelObj->SetArea($excelColumnName[$colmun_counter + $excelTitleColPos] . $nextrowPos);
        $excelObj->SetValueJP($wk_name);
        $excelObj->SetBackColor($wk_back_color);
        if ($excel_blancrows != 0) {
            $wkrow = $nextrowPos + 1;
            $excelObj->SetArea($excelColumnName[$colmun_counter + $excelTitleColPos] . $wkrow);
            $excelObj->SetBackColor($wk_back_color);
        }
        $colmun_counter++;
    }
    $rightPosition = $colmun_counter + $excelTitleColPos - 1;
    $excelObj->SetArea("A" . $rowPos . ":" . $excelColumnName[$rightPosition] . $excelMergeBottom);
    $excelObj->SetBorder("THIN", "000000", 'all');
    $excelObj->SetPosH('CENTER');
    $excelObj->SetPosV('CENTER');
    return;
}

/* * ********************************************************************** */
//
// 表データ
//
/* * ********************************************************************** */

function showList(
    $obj,
    $obj_duty,
    $pattern_id, //出勤パターンＩＤ
    $plan_array, //勤務シフト情報
    $title_gyo_array, //行合計タイトル情報
    $gyo_array, //行合計情報
    $start_cnt, //行表示開始位置
    $end_cnt, //行表示終了位置
    $day_cnt, //日数
    $max_font_cnt, //最大文字数
    $total_print_flg = "1", //集計行列の印刷フラグ 0しない 1:する
    $show_data_flg, //２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直、４：コメント）
    $group_id,
    $reason_setting_flg,
    $hol_dt_flg, //休暇日数詳細 1:表示 "":非表示
    $title_hol_array, //休暇タイトル情報
    $hol_cnt_array, //休暇日数
    $calendar_array,
    $week_array,
    $excelColumnName, //エクセル列名
    $excelObj, //ExcelWorkShop
    $excel_blancrows, // 曜日の下に空行があるときの調整判定用
    $start_day,  // 週間表の開始日(月曜日のみ)
    $week_color,
    $hope_color
) {
    // 当日
    $today = Date("Ymd");

    // 高さ22ピクセルはエクセルの16.5ポイント 2012/1/16
    $wk_height = 16.5;

    $reason_2_array = $obj->get_reason_2();

    //当直パターン取得
    $duty_pattern = $obj_duty->get_duty_pattern_array();

    $data = "";

    // 表内の規定値
    $wk_color = "FFFFFF"; // セル色の規定値は白
    $excelObj->SetBackColorDef($wk_color);

    $excelCellRowPosition = 5;
    if ($excel_blancrows != 0) {
        $excelCellRowPosition++;
    }
    $excelCellRowTop = $excelCellRowPosition;

//行事予定 20150709
	$event_start_row = $excelCellRowPosition;
	global $start_date, $end_date;
	$arr_event_data = $obj->get_event_data($group_id, $start_date, $end_date);

	$event_flg = $_REQUEST['excel_event'];
	if ($event_flg == "1") {
	
		for ($i=1; $i<=4; $i++) {
	        $excelTitleColPos = 0;
	        //*******************************************************************************
	        // 予定データ
	        //*******************************************************************************
	        $excelObj->SetRowDim($excelCellRowPosition, 30); // 高さ22pixcel=16.5point 2012.1.16

	        $excelTitleColPos++; // A列→B列
	        $wkend_day = $start_day + 7;
	        $column_counter = 1;
	        for ($k = $start_day; $k < $wkend_day; $k++) {
				$wk_date = $calendar_array[$k-1]["date"];
            	$excelObj->SetArea($excelColumnName[$column_counter + $excelTitleColPos] . $excelCellRowPosition);
				$wk = $arr_event_data["$wk_date"]["event".$i];
	            $excelObj->SetValueJP($wk);
	            $excelObj->SetShrinkToFit();
	            $column_counter++;
	        }

			$excelCellRowPosition++;
		}
	}

    for ($i = $start_cnt; $i < $end_cnt; $i++) {

        $excelTitleColPos = 0;

        //*******************************************************************************
        // 予定データ
        //*******************************************************************************
        $excelObj->SetRowDim($excelCellRowPosition, 30); // 高さ22pixcel=16.5point 2012.1.16

        if ($show_data_flg != "") {
            $span = "2";
            // セル結合の準備、行指定
            $excelMergeBEGIN = $excelCellRowPosition;
            $excelMergeEND = $excelCellRowPosition + 1;
        }
        else {
            $span = "1";
        }

        //-------------------------------------------------------------------------------
        // 表示順
        //-------------------------------------------------------------------------------
        $excelObj->SetValueJPwith($excelColumnName[$excelTitleColPos] . $excelCellRowPosition,
                                  $plan_array[$i]["job_name"], "CENTER TOP FITSIZE");
        if ($span == "2") {           // 2段表示のとき
            $excelObj->SetArea($excelColumnName[$excelTitleColPos] . $excelMergeBEGIN . ':' . $excelColumnName[$excelTitleColPos] . $excelMergeEND); // 結合範囲指定
            $excelObj->CellMergeReady();         // 結合
        }
        $excelTitleColPos++; // A列→B列

        //-------------------------------------------------------------------------------
        // 氏名
        //-------------------------------------------------------------------------------
        // 氏名の背景色
        $wk_color = "FFFFFF";
        if ($plan_array[$i]["other_staff_flg"] == "1") {
            $wk_color = "dce8bb"; //他病棟スタッフ
        }
        $excelObj->SetBackColorDef($wk_color);

        // 氏名
        $wk = $plan_array[$i]["staff_name"];
        $color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
        $color = str_replace("#", "", $color); // #を取る
        if ($color == "") {
            $color = "FFFFFF";
        }
        $excelObj->SetValueJPwith($excelColumnName[$excelTitleColPos] . $excelCellRowPosition, $plan_array[$i]["staff_name"], "CENTER TOP FITSIZE");
        $excelObj->SetCharColor("000000"); // 性別で色違いをやめる $color -> "000000" 2012.1.20
        $excelObj->SetBackColor($wk_color);
        if ($span == "2") {           // 2段表示のとき
            $excelObj->SetArea($excelColumnName[$excelTitleColPos] . $excelMergeBEGIN . ":" . $excelColumnName[$excelTitleColPos] . $excelMergeEND); // 結合範囲指定
            $excelObj->CellMergeReady();         // 結合
        }

        //-------------------------------------------------------------------------------
        // 勤務状況
        //-------------------------------------------------------------------------------
        // PHP Excel Native改造 色指定を #FFFFFF→FFFFFFへ。#をとる

        $excelObj->SetCharColorDef('000000');
        $wkend_day = $start_day + 7;
        $column_counter = 1;
        for ($k = $start_day; $k < $wkend_day; $k++) {
            //勤務シフト予定（表示）
            if ($plan_array[$i]["pattern_id_$k"] == $pattern_id &&
                ($plan_array[$i]["assist_group_$k"] == "" ||
                $plan_array[$i]["assist_group_$k"] == $group_id)) {
                $wk_font_color = $plan_array[$i]["font_color_$k"];
                $wk_back_color = $plan_array[$i]["back_color_$k"];
            }
            else {
                $wk_font_color = "#C0C0C0";  //灰色
                $wk_back_color = "#ffffff";
            }

            //*曜日で背景色設定
            if ($week_color and ($wk_back_color == "" || strtoupper($wk_back_color) == "#FFFFFF" || strtoupper($wk_back_color) == "FFFFFF")) {
                if ($calendar_array[$k - 1]["type"] == "6") {
                    $wk_back_color = "#C8FFD5";
                }
                else if ($week_array[$k]["name"] == "土" or $calendar_array[$k - 1]["type"] == "5") {
                    $wk_back_color = "#E0FFFF";
                }
                else if ($week_array[$k]["name"] == "日" or $calendar_array[$k - 1]["type"] == "4") {
                    $wk_back_color = "#FF99CC";  //Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
                }
            }

            // 希望色
            if ($hope_color) {
                global $plan_individual, $match_bgcolor, $match_color, $unmatch_bgcolor, $unmatch_color;
                $wk_hope_ptn = $plan_individual["e{$plan_array[$i]["staff_id"]}"]["d{$k}"]["id"];
                $wk_plan_ptn = $plan_array[$i]["atdptn_ptn_id_$k"];
                //両方設定されている場合
                if ($wk_hope_ptn != "" && $wk_plan_ptn != "") {
                    //一致の場合
                    if ($wk_hope_ptn == $wk_plan_ptn) {
                        if ($match_bgcolor !== 'noset') {
                            $wk_back_color = $match_bgcolor;
                        }
                        if ($match_color !== 'noset') {
                            $wk_font_color = $match_color;
                        }
                    }
                    //不一致の場合
                else {
                        if ($unmatch_bgcolor !== 'noset') {
                            $wk_back_color = $unmatch_bgcolor;
                        }
                        if ($unmatch_color !== 'noset') {
                            $wk_font_color = $unmatch_color;
                        }
                    }
                }
            }

            //名称
            $wk = $plan_array[$i]["font_name_$k"];
            //2008/11/27 改修
            if ($plan_array[$i]["reason_2_$k"] != "" &&
                $reason_setting_flg == "t") {
                for ($m = 0; $m < count($reason_2_array); $m++) {
                    if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"]) {
                        $wk_reason_name = $reason_2_array[$m]["font_name"];
                        $wk .= $wk_reason_name;
                        break;
                    }
                }
            }

            $wk_font_color = str_replace("#", "", $wk_font_color); // #を取る
            if ($wk_font_color == "") {
                $wk_font_color = "FFFFFF";
            }
            $wk_back_color = str_replace("#", "", $wk_back_color); // #を取る
            if ($wk_back_color == "") {
                $wk_back_color = "FFFFFF";
            }
            $wk = str_replace("&nbsp;", "", $wk); // HTMLの空白をNull化

            $excelObj->SetArea($excelColumnName[$column_counter + $excelTitleColPos] . $excelCellRowPosition);
            $excelObj->SetValueJP($wk);
            $excelObj->SetBackColor($wk_back_color);
            $excelObj->SetCharColor($wk_font_color);
            $excelObj->SetShrinkToFit();
            $column_counter++;
        }

        $excelClumnNum = $column_counter + $excelTitleColPos;
        $excelDataPosEnd = $column_counter + $excelTitleColPos - 1;
        $excelCellAreaEND = $excelClumnNum - 1;
        $excelClumnNum = $excelTitleColPos;

        //*******************************************************************************
        // 実績データ
        //*******************************************************************************
        if ($show_data_flg != "") {
            $excelCellRowPosition++;
            switch ($show_data_flg) {
                case "0": // 2012.2.3 追加
                    $tmp_title = "";
                    break;
                case "1":
                    $tmp_title = "実績";
                    break;
                case "2":
                    $tmp_title = "希望";
                    break;
                case "3":
                    $tmp_title = "当直";
                    break;
                case "4":
                    $tmp_title = "コメント";
                    break;
                default:
                    $tmp_title = "";
            }
            $excelObj->SetArea($excelColumnName[$excelClumnNum] . $excelCellRowPosition);
            $excelObj->SetValueJP($tmp_title);
            $excelClumnNum++;
            //勤務状況（実績）
            $column_counter = 1;
            $wkend_day = $start_day + 7;
            for ($k = $start_day; $k < $wkend_day; $k++) {

                //コメントの場合
                if ($show_data_flg == "4") { // コメント表示あり
                    $wk = $plan_array[$i]["comment_$k"];
                    $wk_font_color = "#000000";
                    $wk_back_color = "#ffffff";
                }
                else
                if ($show_data_flg != "3" && $show_data_flg != "0") { // 実績表示、勤務希望表示あり（当直表示以外）
                    // 当日以前の場合に表示、希望は無条件に表示
                    $duty_ymd = $calendar_array[$k - 1]["date"]; //前月から当月が作成期間の場合、出力されない不具合対応 20111027
                    if ($duty_ymd <= $today || $show_data_flg == "2") {

                        //文字、背景色
                        if ($plan_array[$i]["rslt_pattern_id_$k"] == $pattern_id) {
                            $wk_font_color = $plan_array[$i]["rslt_font_color_$k"];
                            $wk_back_color = $plan_array[$i]["rslt_back_color_$k"];
                        }
                        else {
                            $wk_font_color = "#C0C0C0";  //灰色
                            $wk_back_color = "";
                        }
                        //勤務実績
                        $wk = $plan_array[$i]["rslt_name_$k"];
                        //2008/11/27 改修
                        if ($plan_array[$i]["rslt_reason_2_$k"] != "" &&
                            $reason_setting_flg == "t") {
                            for ($m = 0; $m < count($reason_2_array); $m++) {
                                if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"]) {
                                    $wk_reason_name = $reason_2_array[$m]["font_name"];
                                    $wk .= $wk_reason_name;
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        $wk_font_color = "";
                        $wk_back_color = "";
                        $wk = ""; // "_" -> " "
                    }
                }
                else {
                    $wk = ""; // "_" -> " "
                    $wk_font_color = "";
                    $wk_back_color = "";
                    for ($m = 0; $m < count($duty_pattern); $m++) {
                        if ($plan_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
                            $wk = $duty_pattern[$m]["font_name"]; //表示文字名
                            $wk_font_color = $duty_pattern[$m]["font_color"];
                            $wk_back_color = $duty_pattern[$m]["back_color"];
                        }
                    }
                }

                //*曜日で背景色設定
                if ($week_color and ($wk_back_color == "" || strtoupper($wk_back_color) == "#FFFFFF")) {
                    if ($calendar_array[$k - 1]["type"] == "6") {
                        $wk_back_color = "#C8FFD5";
                    }
                    else if ($week_array[$k]["name"] == "土" or $calendar_array[$k - 1]["type"] == "5") {
                        $wk_back_color = "#E0FFFF";
                    }
                    else if ($week_array[$k]["name"] == "日" or $calendar_array[$k - 1]["type"] == "4") {
                        $wk_back_color = "#FF99CC";  //Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
                    }
                }

                $wk_width = 20;
                $wk_font_color = str_replace("#", "", $wk_font_color); // #を取る
                if ($wk_font_color == "") {
                    $wk_font_color = "FFFFFF";
                }
                $wk_back_color = str_replace("#", "", $wk_back_color); // #を取る
                if ($wk_back_color == "") {
                    $wk_back_color = "FFFFFF";
                }
                $wk = str_replace("&nbsp;", "", $wk); // 空白の処理
                $excelObj->SetArea($excelColumnName[$excelClumnNum] . $excelCellRowPosition);
                $excelObj->SetValueJP($wk);
                $excelObj->SetBackColor($wk_back_color);
                $excelObj->SetCharColor($wk_font_color);
                $excelClumnNum++;
                $column_counter++;
            } // for
        }

        $excelCellRowPosition++;
    }
    $excelClumnNum--;
    $excelRowEND = $excelCellRowPosition - 1;

    $excelObj->SetArea('B' . $excelCellRowTop . ':' . $excelColumnName[$excelCellAreaEND] . $excelRowEND);
    $excelObj->SetPosH('LEFT'); // ←左→
    $excelObj->SetPosV('TOP'); // ↑↓上
    $excelObj->SetArea('A' . $excelCellRowTop . ':' . $excelColumnName[$excelCellAreaEND] . $excelRowEND);
    $excelObj->SetBorder('THIN', '000000', 'all');
    $excelObj->SetArea('C' . $excelCellRowTop . ':' . $excelColumnName[$excelCellAreaEND] . $excelRowEND); // フォント指定 G列からC列に修正 20140509
    $excelObj->SetFont(mb_convert_encoding('ＭＳ Ｐゴシック', 'UTF-8', 'EUC-JP'), 14);
    return;
}
