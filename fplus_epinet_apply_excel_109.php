<?php

ob_start();


require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("./conf/sql.inf");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック　ファントル君プラス管理者権限
//====================================
$wkfw=check_authority($session,79,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
}
else {
	$encoding = mb_http_output();   // Firefox
}

$stamp = time();

$date = date('YmdHis', $stamp);

if($epi_type == "a")
{
	$fileName = "報告データA.xls";
}
else if($epi_type == "b")
{
	$fileName = "報告データB.xls";
}
else if($epi_type == "ao")
{
	$fileName = "報告データAO.xls";
}
else if($epi_type == "bo")
{
	$fileName = "報告データBO.xls";
}
$fileName = mb_convert_encoding($fileName, $encoding, mb_internal_encoding());

ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$download_data = setData($fname,$con,$epi_type,$sel_d1_y,$sel_d1_m,$sel_d1_d,$sel_d2_y,$sel_d2_m,$sel_d2_d);

echo mb_convert_encoding(
		nl2br($download_data),
		'sjis',
		mb_internal_encoding()
		);

ob_end_flush();

pg_close($con); 


/**
 * /エクセル出力用データセット
 *
 * @param mixed $fname 画面名
 * @param mixed $con 接続情報
 * @param mixed $epi_type 報告書種類
 * @param mixed $sel_d1_y 開始年
 * @param mixed $sel_d1_m 開始月
 * @param mixed $sel_d1_d 開始日
 * @param mixed $sel_d2_y 終了年
 * @param mixed $sel_d2_m 終了月
 * @param mixed $sel_d2_d 終了日
 * @return mixed This is the return value description
 *
 */
function setData
($fname,$con,$epi_type,$sel_d1_y="",$sel_d1_m="",$sel_d1_d="",$sel_d2_y="",$sel_d2_m="",$sel_d2_d="")
{

	//**************************
	// 検索
	//**************************
	$sql =
		" select B.short_wkfw_name, fplusapply.apply_date, fplusapply.apply_no, epinet_data_table.* from fplus_epi_".$epi_type." epinet_data_table inner join fplusapply on (".
		"   fplusapply.apply_id = epinet_data_table.apply_id".
		"   and fplusapply.delete_flg = 'f'".
		"   and fplusapply.apply_stat in ('0','1')".
		"   and fplusapply.draft_flg = 'f'".
		" ) ";
	$sql .= "inner join ";
	$sql .= "(select WM.wkfw_id, WM.wkfw_type, CATE.wkfw_nm, WM.short_wkfw_name ";
	$sql .= "from fpluswkfwmst WM ";
	$sql .= "inner join fpluscatemst CATE on ";
	$sql .= "WM.wkfw_type = CATE.wkfw_type) B on fplusapply.wkfw_id = B.wkfw_id ";
	$sql .= " where (1=1)";
	$sql .= " and data_2_1_arise_date >= '".sprintf("%04d/%02d/%02d",$sel_d1_y,$sel_d1_m,$sel_d1_d)."'"; // 開始日
	$sql .= " and data_2_1_arise_date <= '".sprintf("%04d/%02d/%02d",$sel_d2_y,$sel_d2_m,$sel_d2_d)."'"; // 終了日

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$cond = "";


	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	
	if($epi_type == "a" ||  $epi_type == "ao")
	{
		
		//A.針刺しのデータ出力
		
		$data .= "<table>";
		
		//ヘッダ行表示
		$data .= "<tr align=\"center\">";
		$data .= "<td  ><font size=\"2\">院内報告番号</font></td>";
		$data .= "<td  ><font size=\"2\">集計除外</font></td>";
		$data .= "<td  ><font size=\"2\">入力番号</font></td>";
		$data .= "<td  ><font size=\"2\">番号</font></td>";	
		$data .= "<td  ><font size=\"2\">主キー</font></td>";
		$data .= "<td  ><font size=\"2\">病院コード</font></td>";
		$data .= "<td  ><font size=\"2\">職員番号</font></td>";
		$data .= "<td  ><font size=\"2\">氏名</font></td>";
		$data .= "<td  ><font size=\"2\">ふりがな</font></td>";
		$data .= "<td  ><font size=\"2\">カルテ番号</font></td>";
		$data .= "<td  ><font size=\"2\">性別</font></td>";
		$data .= "<td  ><font size=\"2\">年齢</font></td>";
		$data .= "<td  ><font size=\"2\">経験年数</font></td>";
		$data .= "<td  ><font size=\"2\">所属部門</font></td>";
		$data .= "<td  ><font size=\"2\">所属部門その他</font></td>";
		$data .= "<td  ><font size=\"2\">発生日</font></td>";
		$data .= "<td  ><font size=\"2\">発生時間</font></td>";
		$data .= "<td  ><font size=\"2\">職種</font></td>";
		$data .= "<td  ><font size=\"2\">職種その他</font></td>";
		$data .= "<td  ><font size=\"2\">医師診療科</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所その他</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所詳細</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所詳細その他</font></td>";
		$data .= "<td  ><font size=\"2\">患者の確定</font></td>";
		$data .= "<td  ><font size=\"2\">患者情報有効</font></td>";
		$data .= "<td  ><font size=\"2\">患者カルテ番号</font></td>";
		$data .= "<td  ><font size=\"2\">患者氏名</font></td>";
		$data .= "<td  ><font size=\"2\">入院外来</font></td>";
		$data .= "<td  ><font size=\"2\">HIV</font></td>";
		$data .= "<td  ><font size=\"2\">HCV</font></td>";
		$data .= "<td  ><font size=\"2\">HBs抗原</font></td>";
		$data .= "<td  ><font size=\"2\">HBe抗原</font></td>";
		$data .= "<td  ><font size=\"2\">梅毒</font></td>";
		$data .= "<td  ><font size=\"2\">ATLA(HTLV-1)</font></td>";
		$data .= "<td  ><font size=\"2\">感染その他</font></td>";
		
		$data .= "<td  ><font size=\"2\">最初の使用者</font></td>";
		$data .= "<td  ><font size=\"2\">他者の所持器材</font></td>";
		$data .= "<td  ><font size=\"2\">器材の汚染</font></td>";
		$data .= "<td  ><font size=\"2\">使用目的</font></td>";
		$data .= "<td  ><font size=\"2\">使用目的その他</font></td>";
		$data .= "<td  ><font size=\"2\">使用目的細分</font></td>";
		$data .= "<td  ><font size=\"2\">事例発生状況</font></td>";
		$data .= "<td  ><font size=\"2\">状況その他</font></td>";
		$data .= "<td  ><font size=\"2\">器材名</font></td>";
		$data .= "<td  ><font size=\"2\">器材名その他</font></td>";
		$data .= "<td  ><font size=\"2\">ゲージ数_旧</font></td>";
		$data .= "<td  ><font size=\"2\">ゲージ数</font></td>";
		$data .= "<td  ><font size=\"2\">ゲージ数記載</font></td>";
		$data .= "<td  ><font size=\"2\">安全器材有無</font></td>";
		$data .= "<td  ><font size=\"2\">安全器材詳細有効</font></td>";
		$data .= "<td  ><font size=\"2\">安全装置作動有無</font></td>";
		$data .= "<td  ><font size=\"2\">受傷時期</font></td>";
		$data .= "<td  ><font size=\"2\">安全器材名</font></td>";
		$data .= "<td  ><font size=\"2\">針刺部位1</font></td>";
		$data .= "<td  ><font size=\"2\">針刺部位2</font></td>";
		$data .= "<td  ><font size=\"2\">針刺部位3</font></td>";
		$data .= "<td  ><font size=\"2\">受傷の程度</font></td>";
		$data .= "<td  ><font size=\"2\">手袋着用</font></td>";
		$data .= "<td  ><font size=\"2\">HBs抗体</font></td>";
		$data .= "<td  ><font size=\"2\">緊急処置時受傷</font></td>";
		$data .= "<td  ><font size=\"2\">状況及び背景</font></td>";
		$data .= "<td  ><font size=\"2\">予防策</font></td>";
		$data .= "<td  ><font size=\"2\">防御機能</font></td>";
		$data .= "<td  ><font size=\"2\">防御機能記載</font></td>";
		$data .= "<td  ><font size=\"2\">検査費用_患者</font></td>";
		$data .= "<td  ><font size=\"2\">検査費用_受傷者</font></td>";
		$data .= "<td  ><font size=\"2\">検査費用_計</font></td>";
		$data .= "<td  ><font size=\"2\">業務中断費</font></td>";
		$data .= "<td  ><font size=\"2\">代務採用費</font></td>";
		$data .= "<td  ><font size=\"2\">合計</font></td>";
		$data .= "<td  ><font size=\"2\">感染発症予防費</font></td>";
		$data .= "<td  ><font size=\"2\">発症後治療費用</font></td>";
		$data .= "<td  ><font size=\"2\">総計</font></td>";
		$data .= "<td  ><font size=\"2\">治療内容</font></td>";
		$data .= "<td  ><font size=\"2\">公労災申請</font></td>";
		$data .= "<td  ><font size=\"2\">認定の可否</font></td>";
		$data .= "<td  ><font size=\"2\">認定年月日</font></td>";
		$data .= "<td  ><font size=\"2\">病休日数</font></td>";
		$data .= "<td  ><font size=\"2\">就業制限</font></td>";
		$data .= "<td  ><font size=\"2\">登録年月日</font></td>";
		$data .= "<td  ><font size=\"2\">更新年月日</font></td>";
		$data .= "<td  ><font size=\"2\">削除年月日</font></td>";
		$data .= "<td  ><font size=\"2\">意見</font></td>";
                $data .= "<td  ><font size=\"2\">報告領域</font></td>";
                $data .= "<td  ><font size=\"2\">登録時報告領域</font></td>";
                $data .= "<td  ><font size=\"2\">手術開始時間OR</font></td>";
                $data .= "<td  ><font size=\"2\">手術終了時間OR</font></td>";        
                $data .= "<td  ><font size=\"2\">手術診療科OR</font></td>";
                $data .= "<td  ><font size=\"2\">手術診療科その他OR</font></td>";
                $data .= "<td  ><font size=\"2\">術式OR</font></td>";
                $data .= "<td  ><font size=\"2\">内視鏡手術OR</font></td>";
                $data .= "<td  ><font size=\"2\">職種OR</font></td>";
                $data .= "<td  ><font size=\"2\">職種ORその他</font></td>";        
                $data .= "<td  ><font size=\"2\">医学生科その他OR</font></td>";
                $data .= "<td  ><font size=\"2\">麻酔業務時OR</font></td>";
                $data .= "<td  ><font size=\"2\">発生場所OR</font></td>";
                $data .= "<td  ><font size=\"2\">使用目的OR</font></td>";
                $data .= "<td  ><font size=\"2\">使用目的縫合部OR</font></td>";
                $data .= "<td  ><font size=\"2\">使用目的採血方法OR</font></td>";
                $data .= "<td  ><font size=\"2\">事例発生状況OR</font></td>";
                $data .= "<td  ><font size=\"2\">受傷時所持者OR</font></td>";
                $data .= "<td  ><font size=\"2\">機材の汚染OR</font></td>";
                $data .= "<td  ><font size=\"2\">機材分類OR</font></td>";
                $data .= "<td  ><font size=\"2\">機材名OR</font></td>";
                $data .= "<td  ><font size=\"2\">縫合針種類OR</font></td>";
                $data .= "<td  ><font size=\"2\">縫合針種類その他OR</font></td>";
                $data .= "<td  ><font size=\"2\">縫合針受傷段階OR</font></td>";
                $data .= "<td  ><font size=\"2\">縫合針受傷段階その他OR</font></td>";
                $data .= "<td  ><font size=\"2\">縫合針サイズOR</font></td>";
                $data .= "<td  ><font size=\"2\">機材製造元確認OR</font></td>";
                $data .= "<td  ><font size=\"2\">機材製造元OR</font></td>";
                $data .= "<td  ><font size=\"2\">製品名確認OR</font></td>";
                $data .= "<td  ><font size=\"2\">製品名OR</font></td>";
                $data .= "<td  ><font size=\"2\">安全機材有無OR</font></td>";
                $data .= "<td  ><font size=\"2\">利き手OR</font></td>";
                $data .= "<td  ><font size=\"2\">安全機材だったらOR</font></td>";
                
                
             
		$data .= "</tr>";
		
		while ($row = pg_fetch_array($sel)) 
		{
			
			$data .= "<tr align=\"center\">";

			$year = substr($row["apply_date"], 0, 4);
			$md   = substr($row["apply_date"], 4, 4);
			if($md >= "0101" and $md <= "0331")
			{
				$year = $year - 1;
			}
			$apply_no = $row["short_wkfw_name"]."-".$year."".sprintf("%04d",  $row["apply_no"]);
			$data .= "<td  ><font size=\"2\">".$apply_no."</font></td>";//院内報告番号(暫定)

			$data .= "<td  ><font size=\"2\">FALSE</font></td>";//集計除外(固定)
			$data .= "<td  ><font size=\"2\">".$row["apply_id"]."</font></td>";//入力番号(通番)
			$data .= "<td  ><font size=\"2\"></font></td>";//番号(未使用)	
			$data .= "<td  ><font size=\"2\"></font></td>";//主キー(未使用)
			$data .= "<td  ><font size=\"2\">9999</font></td>";//病院コード(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//職員番号(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//氏名(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//ふりがな(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//カルテ番号(固定)
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_7_sex"])."</font></td>";//性別
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_8_years"])."</font></td>";//年齢
			$data .= "<td  ><font size=\"2\">".$row["data_1_9_exp_year"].".0"."</font></td>";//経験年数
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_2_belong_dept"])."</font></td>";//所属部門
			$data .= "<td  ><font size=\"2\">".$row["data_1_4_dept_etc"]."</font></td>";//所属部門その他
			$data .= "<td  ><font size=\"2\">".$row["data_2_1_arise_date"]."</font></td>";//発生日
			
			//時間表示用にセルの書式を変更
			$data .= "<td  style=\"mso-number-format:\"hh:MM:ss\"\"><font size=\"2\">".$row["data_2_2_arise_hour"].":".$row["data_2_3_arise_minute"].":00</font></td>";//発生時間

			$data .= "<td  ><font size=\"2\">".intval($row["data_3_1_job"])."</font></td>";//職種
			$data .= "<td  ><font size=\"2\">".$row["data_3_4_job_etc"]."</font></td>";//職種その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_3_2_hospital_dept"])."</font></td>";//医師診療科
			$data .= "<td  ><font size=\"2\">".intval($row["data_4_1_arise_area"])."</font></td>";//発生場所
			$data .= "<td  ><font size=\"2\">".$row["data_4_5_arise_etc"]."</font></td>";//発生場所その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_4_3_arise_dept"])."</font></td>";//発生場所詳細
			$data .= "<td  ><font size=\"2\">".$row["data_4_4_arise_dept_etc"]."</font></td>";//発生場所詳細その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_1_patient_decision"])."</font></td>";//患者の確定
			$data .= "<td  ><font size=\"2\">FALSE</font></td>";//患者情報有効(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//患者カルテ番号(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//患者氏名(固定)
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_4_patient_in_out"])."</font></td>";//入院外来
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_5_patient_hiv"])."</font></td>";//HIV
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_6_patient_hcv"])."</font></td>";//HCV
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_7_patient_hbs"])."</font></td>";//HBs抗原
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_8_patient_hbe"])."</font></td>";//HBe抗原
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_9_patient_syphilis"])."</font></td>";//梅毒
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_10_patient_atla"])."</font></td>";//ATLA(HTLV-1)
			$data .= "<td  ><font size=\"2\">".$row["data_5_11_patient_etc"]."</font></td>";//感染その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_6_1_user_select"])."</font></td>";//最初の使用者
			$data .= "<td  ><font size=\"2\">".intval($row["data_6_3_user_owner"])."</font></td>";//他者の所持器材
			$data .= "<td  ><font size=\"2\">".intval($row["data_7_1_pollution"])."</font></td>";//器材の汚染
			$data .= "<td  ><font size=\"2\">".intval($row["data_8_1_use_view"])."</font></td>";//使用目的
			$data .= "<td  ><font size=\"2\">".$row["data_8_4_use_view_etc"]."</font></td>";//使用目的その他;

			if($row["data_8_2_use_view_vein"] != "")
			{$work_set = intval($row["data_8_2_use_view_vein"]);}
			else
			{$work_set = intval($row["data_8_3_use_view_artery"]);}
			$data .= "<td  ><font size=\"2\">".$work_set."</font></td>";//使用目的細分
			$work_set = "";
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_9_1_situation"])."</font></td>";//事例発生状況
			$data .= "<td  ><font size=\"2\">".$row["data_9_2_situation_memo"]."</font></td>";//状況その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_10_1_tools"])."</font></td>";//器材名

			if($row["data_10_3_tools_needle_etc"] != "")
				{$work_set = $row["data_10_3_tools_needle_etc"];}
			elseif($row["data_10_7_tools_ope_memo"] != "")
				{$work_set = $row["data_10_7_tools_ope_memo"];}
			elseif($row["data_10_8_tools_ope_etc"] != "")
				{$work_set = $row["data_10_8_tools_ope_etc"];}
			else
				{$work_set = $row["data_10_10_tools_glass_etc"];}
			$data .= "<td  ><font size=\"2\">".$work_set."</font></td>";//器材名その他
			$work_set = "";
			
			$data .= "<td  ><font size=\"2\"></font></td>";//ゲージ数_旧(未使用)
			

			require_once("fplus_common.ini");
			$work_set = "";
			$part_name = fplus_common_get_episys_master_field_list('data_10_4_tools_needle_gauge');
			foreach($part_name as $epi_key => $epi_val)
			{
				if($epi_val["record_id"] == $row["data_10_4_tools_needle_gauge"])
				{
					$work_set = $epi_val["record_caption"];
				}
			}
			$data .= "<td  ><font size=\"2\">".$work_set."</font></td>";//ゲージ数
			$work_set = "";
			
			
			$data .= "<td  ><font size=\"2\">".$row["data_10_5_tools_needle_gauge_etc"]."</font></td>";//ゲージ数記載
			$data .= "<td  ><font size=\"2\">".intval($row["data_11_1_safe_tls"])."</font></td>";//安全器材有無
			
			if($row["data_11_1_safe_tls"] == "01")
			{$work_set = "TRUE";}
			else
			{$work_set = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$work_set."</font></td>";//安全器材詳細有効
			$work_set = "";
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_11_3_safe_tls_precaution"])."</font></td>";//安全装置作動有無
			$data .= "<td  ><font size=\"2\">".intval($row["data_11_4_safe_period"])."</font></td>";//受傷時期
			$data .= "<td  ><font size=\"2\">".$row["data_11_2_safe_tls_name"]."</font></td>";//安全器材名
			$data .= "<td  ><font size=\"2\">".intval($row["data_12_1_injury_deep"])."</font></td>";//針刺部位1
			$data .= "<td  ><font size=\"2\">".intval($row["data_12_2_injury_middle"])."</font></td>";//針刺部位2
			$data .= "<td  ><font size=\"2\">".intval($row["data_12_3_injury_superficial"])."</font></td>";//針刺部位3
			$data .= "<td  ><font size=\"2\">".intval($row["data_13_1_injury_degree"])."</font></td>";//受傷の程度
			$data .= "<td  ><font size=\"2\">".intval($row["data_14_1_injury_glove"])."</font></td>";//手袋着用
			$data .= "<td  ><font size=\"2\">".intval($row["data_15_1_hbs_positivity"])."</font></td>";//HBs抗体
			$data .= "<td  ><font size=\"2\">".intval($row["data_16_1_emergency_injury"])."</font></td>";//緊急処置時受傷
			
			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_17_1_situation_background"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//状況及び背景
			
			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_18_1_how_prevention"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//予防策

			$data .= "<td  ><font size=\"2\">".intval($row["data_11_5_safe_func"])."</font></td>";//防御機能
			$data .= "<td  ><font size=\"2\"></font></td>";//防御機能記載(未使用)
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_2_patient_exam"])."</font></td>";//検査費用_患者
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_3_injury_exam"])."</font></td>";//検査費用_受傷者
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_4_shokei"])."</font></td>";//検査費用_計
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_5_job_break"])."</font></td>";//業務中断費
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_6_replace_employ"])."</font></td>";//代務採用費
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_7_gokei"])."</font></td>";//合計
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_8_infection_fee"])."</font></td>";//感染発症予防費
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_9_cure_fee"])."</font></td>";//発症後治療費用
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_1_all_total_fee"])."</font></td>";//総計
			
			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_19_10_cure_memo"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//治療内容

			$data .= "<td  ><font size=\"2\">".intval($row["data_19_11_insurance_apply"])."</font></td>";//公労災申請
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_12_apply_certif"])."</font></td>";//認定の可否
			$data .= "<td  ><font size=\"2\">".$row["data_19_13_certif_date"]."</font></td>";//認定年月日
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_14_break_days"])."</font></td>";//病休日数
			$data .= "<td  ><font size=\"2\">".intval($row["data_19_15_restraint_days"])."</font></td>";//就業制限
			$data .= "<td  ><font size=\"2\"></font></td>";//登録年月日(未使用)
			$data .= "<td  ><font size=\"2\"></font></td>";//更新年月日(未使用)
			$data .= "<td  ><font size=\"2\"></font></td>";//削除年月日(未使用)
			
			$set_data = "";
			$set_data = str_replace("\r\n", "",$row["data_11_6_safe_tls_memo"]);
			
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//意見(安全器材)

                        $data .= "<td  ><font size=\"2\"></font></td>";//報告領域
                        $data .= "<td  ><font size=\"2\"></font></td>";//登録時報告領域
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_2_4_ope_start_hour"].":".$row["data_2_5_ope_start_minute"]."</font></td>";//OPE開始時間
                        $data .= "<td  ><font size=\"2\">".$row["data_2_6_ope_end_hour"].":".$row["data_2_7_ope_end_minute"]."</font></td>";//OPE終了時間
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_3_1_hospital_dept"]."</font></td>";//手術診療科
                        $data .= "<td  ><font size=\"2\">".$row["data_3_2_hospital_dept_etc"]."</font></td>";//手術診療科その他
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_4_1_ope"]."</font></td>";//術式
                        $data .= "<td  ><font size=\"2\">".$row["data_4_2_ope_using_mirror"]."</font></td>";//内視鏡手術

                        $data .= "<td  ><font size=\"2\">".$row["data_5_1_job"]."</font></td>";//職種
                        $data .= "<td  ><font size=\"2\">".$row["data_5_2_job_etc"]."</font></td>";//職種その他
                        $data .= "<td  ><font size=\"2\">".$row["data_5_3_student"]."</font></td>";//医学生科その他
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_6_1_when_anesthesia_business"]."</font></td>";//麻酔時
                        $data .= "<td  ><font size=\"2\">".$row["data_6_2_when_anesthesia_business_detail"]."</font></td>";//麻酔業務
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_7_1_arise_area"]."</font></td>";//発生場所

                        $data .= "<td  ><font size=\"2\">".$row["data_7_1_arise_area"]."</font></td>";//使用目的
                        $data .= "<td  ><font size=\"2\"></font></td>";//使用目的縫合部位
                        $data .= "<td  ><font size=\"2\"></font></td>";//使用目的採血方法
                        $data .= "<td  ><font size=\"2\">".$row["data_10_1_situation"]."</font></td>";//事例発生状況                        
                        $data .= "<td  ><font size=\"2\">".$row["data_11_1_user_select"]."</font></td>";//受傷時所持者
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_12_1_pollution"]."</font></td>";//機材の汚染
                        $data .= "<td  ><font size=\"2\">".$row["data_13_2_tools"]."</font></td>";//機材の分類
                        $data .= "<td  ><font size=\"2\"></font></td>";//機材名
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_13_4_tools_type"]."</font></td>";//縫合針種類
                        $data .= "<td  ><font size=\"2\">".$row["data_13_9_tools_needle_type_etc"]."</font></td>";//縫合針種類その他
                        $data .= "<td  ><font size=\"2\">".$row["data_13_5_tools_phese"]."</font></td>";//縫合針受傷段階
                        $data .= "<td  ><font size=\"2\">".$row["data_13_10_tools_needle_phese_etc"]."</font></td>";//縫合針受傷段階その他
                        $data .= "<td  ><font size=\"2\">".$row["data_13_3_tools_size"]."</font></td>";//縫合針サイズ
                        $data .= "<td  ><font size=\"2\"></font></td>";//機材製造元
                        $data .= "<td  ><font size=\"2\"></font></td>";//製品名確認
                        $data .= "<td  ><font size=\"2\"></font></td>";//製品名
                        $data .= "<td  ><font size=\"2\">".$row["data_14_2_safe_tls_precaution"]."</font></td>";//安全機材有無
                        $data .= "<td  ><font size=\"2\">".$row["data_18_1_handedness"]."</font></td>";//利き手
                        $data .= "<td  ><font size=\"2\">".$row["data_21_1_safe_func"]."</font></td>";//安全機材だったら
                        $data .= "</tr>";
			
		}
		
		$data .= "</table>";
		
	}
	else 
	{
		//B.感染のデータ出力
		
		$data .= "<table >";
		
		//ヘッダ行表示
		$data .= "<tr align=\"center\">";
		$data .= "<td  ><font size=\"2\">集計除外</font></td>";
		$data .= "<td  ><font size=\"2\">入力番号</font></td>";
		$data .= "<td  ><font size=\"2\">院内報告番号</font></td>";
		$data .= "<td  ><font size=\"2\">番号</font></td>";	
		$data .= "<td  ><font size=\"2\">主キー</font></td>";
		$data .= "<td  ><font size=\"2\">病院コード</font></td>";
		$data .= "<td  ><font size=\"2\">職員番号</font></td>";
		$data .= "<td  ><font size=\"2\">氏名</font></td>";
		$data .= "<td  ><font size=\"2\">ふりがな</font></td>";
		$data .= "<td  ><font size=\"2\">カルテ番号</font></td>";
		$data .= "<td  ><font size=\"2\">性別</font></td>";
		$data .= "<td  ><font size=\"2\">年齢</font></td>";
		$data .= "<td  ><font size=\"2\">経験年数</font></td>";
		$data .= "<td  ><font size=\"2\">所属部門</font></td>";
		$data .= "<td  ><font size=\"2\">所属部門その他</font></td>";
		$data .= "<td  ><font size=\"2\">発生日</font></td>";
		$data .= "<td  ><font size=\"2\">発生時間</font></td>";
		$data .= "<td  ><font size=\"2\">職種</font></td>";
		$data .= "<td  ><font size=\"2\">職種その他</font></td>";
		$data .= "<td  ><font size=\"2\">医師診療科</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所その他</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所詳細</font></td>";
		$data .= "<td  ><font size=\"2\">発生場所詳細その他</font></td>";
		$data .= "<td  ><font size=\"2\">患者の確定</font></td>";
		$data .= "<td  ><font size=\"2\">患者情報有効</font></td>";
		$data .= "<td  ><font size=\"2\">患者カルテ番号</font></td>";
		$data .= "<td  ><font size=\"2\">患者氏名</font></td>";
		$data .= "<td  ><font size=\"2\">入院外来</font></td>";
		$data .= "<td  ><font size=\"2\">HIV</font></td>";
		$data .= "<td  ><font size=\"2\">HCV</font></td>";
		$data .= "<td  ><font size=\"2\">HBs抗原</font></td>";
		$data .= "<td  ><font size=\"2\">HBe抗原</font></td>";
		$data .= "<td  ><font size=\"2\">梅毒</font></td>";
		$data .= "<td  ><font size=\"2\">ATLA(HTLV-1)</font></td>";
		$data .= "<td  ><font size=\"2\">感染その他</font></td>";

		$data .= "<td  ><font size=\"2\">血液等</font></td>";
		$data .= "<td  ><font size=\"2\">吐物</font></td>";
		$data .= "<td  ><font size=\"2\">痰</font></td>";
		$data .= "<td  ><font size=\"2\">唾液</font></td>";
		$data .= "<td  ><font size=\"2\">脳脊髄液</font></td>";
		$data .= "<td  ><font size=\"2\">腹水</font></td>";
		$data .= "<td  ><font size=\"2\">胸水</font></td>";
		$data .= "<td  ><font size=\"2\">羊水</font></td>";
		$data .= "<td  ><font size=\"2\">尿</font></td>";
		$data .= "<td  ><font size=\"2\">体液その他</font></td>";
		$data .= "<td  ><font size=\"2\">体液その他記載</font></td>";
		$data .= "<td  ><font size=\"2\">無傷な皮膚</font></td>";
		$data .= "<td  ><font size=\"2\">傷のある皮膚</font></td>";
		$data .= "<td  ><font size=\"2\">眼</font></td>";
		$data .= "<td  ><font size=\"2\">鼻</font></td>";
		$data .= "<td  ><font size=\"2\">口</font></td>";
		$data .= "<td  ><font size=\"2\">部位その他</font></td>";
		$data .= "<td  ><font size=\"2\">部位その他記載</font></td>";
		$data .= "<td  ><font size=\"2\">防備していない皮膚</font></td>";
		$data .= "<td  ><font size=\"2\">防衣の隙間等の皮膚</font></td>";
		$data .= "<td  ><font size=\"2\">防衣又は防具を浸透</font></td>";
		$data .= "<td  ><font size=\"2\">白衣などの衣類を浸透</font></td>";
		$data .= "<td  ><font size=\"2\">手袋なし</font></td>";
		$data .= "<td  ><font size=\"2\">一重のゴム手袋</font></td>";
		$data .= "<td  ><font size=\"2\">一重のゴム手袋メーカー</font></td>";
		$data .= "<td  ><font size=\"2\">二重のゴム手袋</font></td>";
		$data .= "<td  ><font size=\"2\">二重のゴム手袋メーカー</font></td>";
		$data .= "<td  ><font size=\"2\">ゴーグル</font></td>";
		$data .= "<td  ><font size=\"2\">眼鏡</font></td>";
		$data .= "<td  ><font size=\"2\">側面保護眼鏡</font></td>";
		$data .= "<td  ><font size=\"2\">フェイスシールド</font></td>";
		$data .= "<td  ><font size=\"2\">外科手術用マスク</font></td>";
		$data .= "<td  ><font size=\"2\">眼保護付外科手術用マスク</font></td>";
		$data .= "<td  ><font size=\"2\">外科手術用ガウン</font></td>";
		$data .= "<td  ><font size=\"2\">プラスティックエプロン</font></td>";
		$data .= "<td  ><font size=\"2\">検査実験衣布地</font></td>";
		$data .= "<td  ><font size=\"2\">検査実験衣その他</font></td>";
		$data .= "<td  ><font size=\"2\">検査実験衣その他記載</font></td>";
		$data .= "<td  ><font size=\"2\">防衣その他</font></td>";
		$data .= "<td  ><font size=\"2\">防衣その他記載</font></td>";
		$data .= "<td  ><font size=\"2\">汚染理由</font></td>";
		$data .= "<td  ><font size=\"2\">栄養チューブその他チューブ名記載</font></td>";
		$data .= "<td  ><font size=\"2\">理由その他記載</font></td>";
		$data .= "<td  ><font size=\"2\">接触時間</font></td>";
		$data .= "<td  ><font size=\"2\">接触量</font></td>";
		$data .= "<td  ><font size=\"2\">部位1</font></td>";
		$data .= "<td  ><font size=\"2\">部位1名</font></td>";
		$data .= "<td  ><font size=\"2\">部位2</font></td>";
		$data .= "<td  ><font size=\"2\">部位2名</font></td>";
		$data .= "<td  ><font size=\"2\">部位3</font></td>";
		$data .= "<td  ><font size=\"2\">部位3名</font></td>";
		$data .= "<td  ><font size=\"2\">HBs抗体</font></td>";
		$data .= "<td  ><font size=\"2\">緊急時汚染</font></td>";
		$data .= "<td  ><font size=\"2\">状況及び背景</font></td>";
		$data .= "<td  ><font size=\"2\">予防策</font></td>";
		
		$data .= "<td  ><font size=\"2\">検査費用_患者</font></td>";
		$data .= "<td  ><font size=\"2\">検査費用_受傷者</font></td>";
		$data .= "<td  ><font size=\"2\">検査費用_計</font></td>";
		$data .= "<td  ><font size=\"2\">業務中断費</font></td>";
		$data .= "<td  ><font size=\"2\">代務採用費</font></td>";
		$data .= "<td  ><font size=\"2\">合計</font></td>";
		$data .= "<td  ><font size=\"2\">感染発症予防費</font></td>";
		$data .= "<td  ><font size=\"2\">発症後治療費用</font></td>";
		$data .= "<td  ><font size=\"2\">総計</font></td>";
		$data .= "<td  ><font size=\"2\">治療内容</font></td>";
		$data .= "<td  ><font size=\"2\">公労災申請</font></td>";
		$data .= "<td  ><font size=\"2\">認定の可否</font></td>";
		$data .= "<td  ><font size=\"2\">認定年月日</font></td>";
		$data .= "<td  ><font size=\"2\">病休日数</font></td>";
		$data .= "<td  ><font size=\"2\">就業制限</font></td>";
		$data .= "<td  ><font size=\"2\">登録年月日</font></td>";
		$data .= "<td  ><font size=\"2\">更新年月日</font></td>";
		$data .= "<td  ><font size=\"2\">削除年月日</font></td>";
                
                
                $data .= "<td  ><font size=\"2\">報告領域</font></td>";
                $data .= "<td  ><font size=\"2\">登録時報告領域</font></td>";
                $data .= "<td  ><font size=\"2\">手術開始時間</font></td>";
                $data .= "<td  ><font size=\"2\">手術終了時間</font></td>";        
                $data .= "<td  ><font size=\"2\">手術診療科</font></td>";
                $data .= "<td  ><font size=\"2\">手術診療科その他</font></td>";
                $data .= "<td  ><font size=\"2\">術式</font></td>";
                $data .= "<td  ><font size=\"2\">内視鏡手術</font></td>";
                $data .= "<td  ><font size=\"2\">職種</font></td>";
                $data .= "<td  ><font size=\"2\">職種その他</font></td>";        
                $data .= "<td  ><font size=\"2\">医学生科</font></td>";
                $data .= "<td  ><font size=\"2\">医学生科その他</font></td>";
                $data .= "<td  ><font size=\"2\">麻酔業務時</font></td>";
                $data .= "<td  ><font size=\"2\">麻酔業務</font></td>";
                $data .= "<td  ><font size=\"2\">発生場所</font></td>";
                $data .= "<td  ><font size=\"2\">血液混入</font></td>";
                $data .= "<td  ><font size=\"2\">手術衣規格</font></td>";
                $data .= "<td  ><font size=\"2\">外科手術用ガウンディスポ</font></td>";
                $data .= "<td  ><font size=\"2\">外科手術用ガウンリユース</font></td>";
                $data .= "<td  ><font size=\"2\">汚染理由</font></td>";
                $data .= "<td  ><font size=\"2\">手袋の裂け目の皮膚</font></td>";
                $data .= "<td  ><font size=\"2\">管理で予防可能</font></td>";
 
		$data .= "</tr>";

		while ($row = pg_fetch_array($sel)) 
		{
			
			$data .= "<tr align=\"center\">";
			
			$data .= "<td  ><font size=\"2\">FALSE</font></td>";//集計除外(固定)
			$data .= "<td  ><font size=\"2\">".$row["apply_id"]."</font></td>";//入力番号(通番)
			
			$year = substr($row["apply_date"], 0, 4);
			$md   = substr($row["apply_date"], 4, 4);
			if($md >= "0101" and $md <= "0331")
			{
				$year = $year - 1;
			}
			$apply_no = $row["short_wkfw_name"]."-".$year."".sprintf("%04d",  $row["apply_no"]);
			$data .= "<td  ><font size=\"2\">".$apply_no."</font></td>";//院内報告番号(暫定)
			
			$data .= "<td  ><font size=\"2\"></font></td>";//番号(未使用)	
			$data .= "<td  ><font size=\"2\"></font></td>";//主キー(未使用)
			
			$data .= "<td  ><font size=\"2\">9999</font></td>";//病院コード(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//職員番号(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//氏名(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//ふりがな(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//カルテ番号(固定)
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_7_sex"])."</font></td>";//性別
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_8_years"])."</font></td>";//年齢
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_9_exp_year"])."</font></td>";//経験年数
			$data .= "<td  ><font size=\"2\">".intval($row["data_1_2_belong_dept"])."</font></td>";//所属部門
			$data .= "<td  ><font size=\"2\">".$row["data_1_4_dept_etc"]."</font></td>";//所属部門その他
			$data .= "<td  ><font size=\"2\">".$row["data_2_1_arise_date"]."</font></td>";//発生日
			
			//時間表示用にセルの書式を変更
			$data .= "<td  style=\"mso-number-format:\"hh:MM:ss\"\"><font size=\"2\">".$row["data_2_2_arise_hour"].":".$row["data_2_3_arise_minute"].":00</font></td>";//発生時間
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_3_1_job"])."</font></td>";//職種
			$data .= "<td  ><font size=\"2\">".$row["data_3_4_job_etc"]."</font></td>";//職種その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_3_2_hospital_dept"])."</font></td>";//医師診療科
			$data .= "<td  ><font size=\"2\">".intval($row["data_4_1_arise_area"])."</font></td>";//発生場所
			$data .= "<td  ><font size=\"2\">".$row["data_4_5_arise_etc"]."</font></td>";//発生場所その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_4_3_arise_dept"])."</font></td>";//発生場所詳細
			$data .= "<td  ><font size=\"2\">".$row["data_4_4_arise_dept_etc"]."</font></td>";//発生場所詳細その他
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_1_patient_decision"])."</font></td>";//患者の確定
			$data .= "<td  ><font size=\"2\">FALSE</font></td>";//患者情報有効(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//患者カルテ番号(固定)
			$data .= "<td  ><font size=\"2\">???</font></td>";//患者氏名(固定)
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_4_patient_in_out"])."</font></td>";//入院外来
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_5_patient_hiv"])."</font></td>";//HIV
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_6_patient_hcv"])."</font></td>";//HCV
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_7_patient_hbs"])."</font></td>";//HBs抗原
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_8_patient_hbe"])."</font></td>";//HBe抗原
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_9_patient_syphilis"])."</font></td>";//梅毒
			$data .= "<td  ><font size=\"2\">".intval($row["data_5_10_patient_atla"])."</font></td>";//ATLA(HTLV-1)
			$data .= "<td  ><font size=\"2\">".$row["data_5_11_patient_etc"]."</font></td>";//感染その他

			$set_data = "";
			if($row["data_6_1_fluid_blood"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//血液等
			
			$set_data = "";
			if($row["data_6_2_fluid_vomit"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//吐物
			
			$set_data = "";
			if($row["data_6_3_fluid_phlegm"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//痰
			
			$set_data = "";
			if($row["data_6_4_fluid_saliva"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//唾液
			
			$set_data = "";
			if($row["data_6_5_fluid_spinal_cord"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//脳脊髄液
			
			$set_data = "";
			if($row["data_6_6_fluid_ascites"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//腹水
			
			$set_data = "";
			if($row["data_6_7_fluid_pleural"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//胸水
			
			$set_data = "";
			if($row["data_6_8_fluid_amniotic"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//羊水
			
			$set_data = "";
			if($row["data_6_9_fluid_urine"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//尿
			
			$set_data = "";
			if($row["data_6_10_fluid_etc"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//体液その他
			
			$data .= "<td  ><font size=\"2\">".$row["data_6_11_fluid_etc_memo"]."</font></td>";//体液その他記載
			
			$set_data = "";
			if($row["data_7_1_part_clean_skin"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//無傷な皮膚
			
			$set_data = "";
			if($row["data_7_2_part_blemished_skin"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//傷のある皮膚
			
			$set_data = "";
			if($row["data_7_3_part_eye"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//眼
			
			$set_data = "";
			if($row["data_7_4_part_nose"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//鼻
			
			$set_data = "";
			if($row["data_7_5_part_mouth"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//口
			
			$set_data = "";
			if($row["data_7_6_part_etc"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//部位その他
			
			$data .= "<td  ><font size=\"2\">".$row["data_7_7_part_etc_memo"]."</font></td>";//部位その他記載
			
			if($row["data_8_1_touch"] == "")
			{
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防備していない皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣の隙間等の皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣又は防具を浸透
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//白衣などの衣類を浸透
			}
			elseif($row["data_8_1_touch"] == "01")
			{
				$data .= "<td  ><font size=\"2\">TRUE</font></td>";//防備していない皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣の隙間等の皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣又は防具を浸透
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//白衣などの衣類を浸透
			}
			elseif($row["data_8_1_touch"] == "02")
			{
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防備していない皮膚
				$data .= "<td  ><font size=\"2\">TRUE</font></td>";//防衣の隙間等の皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣又は防具を浸透
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//白衣などの衣類を浸透
			}
			elseif($row["data_8_1_touch"] == "03")
			{
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防備していない皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣の隙間等の皮膚
				$data .= "<td  ><font size=\"2\">TRUE</font></td>";//防衣又は防具を浸透
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//白衣などの衣類を浸透
			}
			elseif($row["data_8_1_touch"] == "04")
			{
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防備していない皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣の隙間等の皮膚
				$data .= "<td  ><font size=\"2\">FALSE</font></td>";//防衣又は防具を浸透
				$data .= "<td  ><font size=\"2\">TRUE</font></td>";//白衣などの衣類を浸透
			}
			
			$set_data = "";
			if($row["data_9_1_cloth_no_glove"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//手袋なし
			
			$set_data = "";
			if($row["data_9_2_cloth_one_glove"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//一重のゴム手袋
			
			$data .= "<td  ><font size=\"2\">".$row["data_9_3_cloth_one_glove_memo"]."</font></td>";//一重のゴム手袋メーカー
			
			$set_data = "";
			if($row["data_9_4_cloth_double_glove"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//二重のゴム手袋

			$data .= "<td  ><font size=\"2\">".$row["data_9_5_cloth_double_glove_memo"]."</font></td>";//二重のゴム手袋メーカー
			
			$set_data = "";
			if($row["data_9_6_cloth_goggle"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//ゴーグル
			
			$set_data = "";
			if($row["data_9_7_cloth_glass"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//眼鏡
			
			$set_data = "";
			if($row["data_9_8_cloth_glass_aside"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//側面保護眼鏡
			
			$set_data = "";
			if($row["data_9_9_cloth_face_shield"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//フェイスシールド
			
			$set_data = "";
			if($row["data_9_10_cloth_ope_mask"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//外科手術用マスク
			
			$set_data = "";
			if($row["data_9_11_cloth_glass_mask"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//眼保護付外科手術用マスク
			
			$set_data = "";
			if($row["data_9_12_cloth_ope_gown"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//外科手術用ガウン
			
			$set_data = "";
			if($row["data_9_13_cloth_gown"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//プラスティックエプロン
			
			$set_data = "";
			if($row["data_9_14_cloth_experiment"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//検査実験衣布地
			
			$set_data = "";
			if($row["data_9_15_cloth_exp_etc"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//検査実験衣その他
			
			$data .= "<td  ><font size=\"2\">".$row["data_9_16_cloth_exp_etc_memo"]."</font></td>";//検査実験衣その他記載
			
			$set_data = "";
			if($row["data_9_17_cloth_etc"] == "01"){$set_data = "TRUE";}else{$set_data = "FALSE";}
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//防衣その他
			
			$data .= "<td  ><font size=\"2\">".$row["data_9_18_cloth_etc_memo"]."</font></td>";//防衣その他記載
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_10_1_how_arise"])."</font></td>";//汚染理由
			$data .= "<td  ><font size=\"2\">".$row["data_10_2_how_arise_tube"]."</font></td>";//栄養チューブその他チューブ名記載
			$data .= "<td  ><font size=\"2\">".$row["data_10_3_how_arise_etc"]."</font></td>";//理由その他記載
			$data .= "<td  ><font size=\"2\">".intval($row["data_11_1_how_long"])."</font></td>";//接触時間
			$data .= "<td  ><font size=\"2\">".intval($row["data_12_1_how_much"])."</font></td>";//接触量

			$data .= "<td  ><font size=\"2\">".intval($row["data_13_1_injury_deep"])."</font></td>";//部位1
			$data .= "<td  ><font size=\"2\">".$row["data_13_11_injury_deep_name"]."</font></td>";//部位1名
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_13_2_injury_middle"])."</font></td>";//部位2
			$data .= "<td  ><font size=\"2\">".$row["data_13_21_injury_middle_name"]."</font></td>";//部位2名
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_13_3_injury_superficial"])."</font></td>";//部位3
			$data .= "<td  ><font size=\"2\">".$row["data_13_31_injury_superficial_name"]."</font></td>";//部位3名
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_14_1_hbs_positivity"])."</font></td>";//HBs抗体
			$data .= "<td  ><font size=\"2\">".intval($row["data_15_1_emergency_hurt"])."</font></td>";//緊急時汚染


			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_16_1_situation_background"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//状況及び背景
			
			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_17_1_how_prevention"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//予防策
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_2_patient_exam"])."</font></td>";//検査費用_患者
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_3_injury_exam"])."</font></td>";//検査費用_受傷者
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_4_shokei"])."</font></td>";//検査費用_計
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_5_job_break"])."</font></td>";//業務中断費
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_6_replace_employ"])."</font></td>";//代務採用費
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_7_gokei"])."</font></td>";//合計
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_8_infection_fee"])."</font></td>";//感染発症予防費
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_9_cure_fee"])."</font></td>";//発症後治療費用
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_1_all_total_fee"])."</font></td>";//総計
			
			$set_data = "";
			$set_data = ereg_replace("\r\n", "",$row["data_18_10_cure_memo"]);
			$data .= "<td  ><font size=\"2\">".$set_data."</font></td>";//治療内容
			
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_11_insurance_apply"])."</font></td>";//公労災申請
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_12_apply_certif"])."</font></td>";//認定の可否
			$data .= "<td  ><font size=\"2\">".$row["data_18_13_certif_date"]."</font></td>";//認定年月日
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_14_break_days"])."</font></td>";//病休日数
			$data .= "<td  ><font size=\"2\">".intval($row["data_18_15_restraint_days"])."</font></td>";//就業制限
			$data .= "<td  ><font size=\"2\"></font></td>";//登録年月日(未使用)
			$data .= "<td  ><font size=\"2\"></font></td>";//更新年月日(未使用)
			$data .= "<td  ><font size=\"2\"></font></td>";//削除年月日(未使用)
			
                        $data .= "<td  ><font size=\"2\"></font></td>";//治療内容
                        
                        
                        $data .= "<td  ><font size=\"2\"></font></td>";//報告領域
                        $data .= "<td  ><font size=\"2\"></font></td>";//登録時報告領域
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_2_4_ope_start_hour"].":".$row["data_2_5_ope_start_minute"]."</font></td>";//OPE開始時間
                        $data .= "<td  ><font size=\"2\">".$row["data_2_6_ope_end_hour"].":".$row["data_2_7_ope_end_minute"]."</font></td>";//OPE終了時間
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_3_1_hospital_dept"]."</font></td>";//手術診療科
                        $data .= "<td  ><font size=\"2\">".$row["data_3_2_hospital_dept_etc"]."</font></td>";//手術診療科その他
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_4_1_ope"]."</font></td>";//術式
                        $data .= "<td  ><font size=\"2\">".$row["data_4_2_ope_using_mirror"]."</font></td>";//内視鏡手術

                        $data .= "<td  ><font size=\"2\">".$row["data_5_1_job"]."</font></td>";//職種
                        $data .= "<td  ><font size=\"2\">".$row["data_5_2_job_etc"]."</font></td>";//職種その他
                        $data .= "<td  ><font size=\"2\">".$row["data_5_3_student"]."</font></td>";//医学生科その他
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_6_1_when_anesthesia_business"]."</font></td>";//麻酔時
                        $data .= "<td  ><font size=\"2\">".$row["data_6_2_when_anesthesia_business_detail"]."</font></td>";//麻酔業務
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_7_1_arise_area"]."</font></td>";//発生場所
                        
                        $data .= "<td  ><font size=\"2\">".$row["data_9_13_blood_contami"]."</font></td>";//血液混入
                        $data .= "<td  ><font size=\"2\"></font></td>";//手術衣規格
                        
                        $data .= "<td  ><font size=\"2\"></font></td>";//外科手術用ガウンディスポ
                        $data .= "<td  ><font size=\"2\"></font></td>";//外科手術用ガウンリユース
                        $data .= "<td  ><font size=\"2\">".$row["data_13_1_how_arise"]."</font></td>";//汚染理由
                        $data .= "<td  ><font size=\"2\"></font></td>";//手袋の裂け目の皮膚
                        $data .= "<td  ><font size=\"2\">".$row["data_19_1_safe_func"]."</font></td>";//管理で予防可能
                       
                        
			$data .= "</tr>";
			
		}
		
		$data .= "</table>";
		
	}
	return $data;	
	
}

?>
