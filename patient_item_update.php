<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$ptif = check_authority($session, 15, $fname);
if ($ptif == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");;
	exit;
}
$lt_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
?>
<title><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?> | 項目選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	if (opener.document.mainform.ptsubif_tool_flg.value == 't') {
		addOption(document.mainform.elements['tool[]'], '1', 'なし');
	}
	if (opener.document.mainform.ptsubif_tool1.value == 't') {
		addOption(document.mainform.elements['tool[]'], '2', '上肢装具');
	}
	if (opener.document.mainform.ptsubif_tool2.value == 't') {
		addOption(document.mainform.elements['tool[]'], '3', '下肢装具');
	}
	if (opener.document.mainform.ptsubif_tool3.value == 't') {
		addOption(document.mainform.elements['tool[]'], '4', '義歯');
	}
	if (opener.document.mainform.ptsubif_tool4.value == 't') {
		addOption(document.mainform.elements['tool[]'], '5', '義眼');
	}
	if (opener.document.mainform.ptsubif_tool5.value == 't') {
		addOption(document.mainform.elements['tool[]'], '6', '補聴器');
	}
	if (opener.document.mainform.ptsubif_tool6.value == 't') {
		addOption(document.mainform.elements['tool[]'], '7', '車椅子');
	}
	if (opener.document.mainform.ptsubif_tool7.value == 't') {
		addOption(document.mainform.elements['tool[]'], '8', '歩行器');
	}

	if (opener.document.mainform.ptsubif_algy_flg.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '1', 'なし');
	}
	if (opener.document.mainform.ptsubif_allergy1.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '2', '抗生物質');
	}
	if (opener.document.mainform.ptsubif_allergy2.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '3', '解熱鎮静剤');
	}
	if (opener.document.mainform.ptsubif_allergy3.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '4', '局所麻酔薬');
	}
	if (opener.document.mainform.ptsubif_allergy4.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '5', '液製剤');
	}
	if (opener.document.mainform.ptsubif_allergy5.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '6', 'ワクチン');
	}
	if (opener.document.mainform.ptsubif_allergy6.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '7', '造影剤');
	}
	if (opener.document.mainform.ptsubif_allergy7.value == 't') {
		addOption(document.mainform.elements['allergy[]'], '8', 'その他');
	}

	if (opener.document.mainform.ptsubif_infect_flg.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '1', 'なし');
	}
	if (opener.document.mainform.ptsubif_infection1.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '2', '未検査');
	}
	if (opener.document.mainform.ptsubif_infection2.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '3', '梅毒');
	}
	if (opener.document.mainform.ptsubif_infection3.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '4', 'Ｂ型肝炎');
	}
	if (opener.document.mainform.ptsubif_infection4.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '5', 'Ｃ型肝炎');
	}
	if (opener.document.mainform.ptsubif_infection5.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '6', 'ＨＩＶ');
	}
	if (opener.document.mainform.ptsubif_infection6.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '7', 'ＭＲＳＡ');
	}
	if (opener.document.mainform.ptsubif_infection7.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '8', '緑膿菌');
	}
	if (opener.document.mainform.ptsubif_infection8.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '9', '疥癬');
	}
	if (opener.document.mainform.ptsubif_infection9.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '10', 'ＡＴＬ');
	}
	if (opener.document.mainform.ptsubif_infection10.value == 't') {
		addOption(document.mainform.elements['epidemic[]'], '11', 'その他');
	}
}

function addSelectedOption(leftBox, rightBox, singles) {

	// 右側の選択値を保存
	var rightVals = new Array();
	for (var i = 0, j = rightBox.length; i < j; i++) {
		rightVals.push(rightBox.options[i].value);
	}

	// 左側の選択値取得・名称配列生成
	var leftVals = new Array();
	var texts = new Array();
	for (var i = 0, j = leftBox.length; i < j; i++) {
		var opt = leftBox.options[i];
		texts[opt.value] = opt.text;
		if (opt.selected) {
			leftVals.push(opt.value);
		}
	}

	// マージ
	for (var i = 0, j = leftVals.length; i < j; i++) {
		var dup = false;
		for (var m = 0, n = rightVals.length; m < n; m++) {
			if (leftVals[i] == rightVals[m]) {
				dup = true;
				break;
			}
		}
		if (!dup) {
			rightVals.push(leftVals[i]);
		}
	}

	// 単項目チェック
	if (rightVals.length > 1) {
		for (var i = 0, j = singles.length; i < j; i++) {
			for (var m = 0, n = rightVals.length; m < n; m++) {
				if (singles[i] == rightVals[m]) {
					var msg = "「%1」の場合、単独で選択してください";
					msg = msg.replace('%1', texts[rightVals[m]]);
					alert(msg);
					return;
				}
			}
		}
	}

	// 削除
	removeAllOptions(rightBox);

	// 再表示
	for (var i = 0, j = rightVals.length; i < j; i++) {
		var opt = document.createElement("option");
		opt.value = rightVals[i];
		opt.text = texts[rightVals[i]];
		rightBox.options[rightBox.length] = opt;
		try {rightBox.style.fontSize = 'auto';} catch (e) {}
		rightBox.style.overflow = 'auto';
	}

	// 左側を未選択に
	initBox(leftBox, false);
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function removeSelectedOption(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		if (box.options[i].selected) {
			box.options[i] = null;
		}
	}
}

function removeAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function initBox(box, selected) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = selected;
	}
}

function copyItem() {
	opener.document.mainform.ptsubif_tool_flg.value = 'f';
	opener.document.mainform.ptsubif_tool1.value = 'f';
	opener.document.mainform.ptsubif_tool2.value = 'f';
	opener.document.mainform.ptsubif_tool3.value = 'f';
	opener.document.mainform.ptsubif_tool4.value = 'f';
	opener.document.mainform.ptsubif_tool5.value = 'f';
	opener.document.mainform.ptsubif_tool6.value = 'f';
	opener.document.mainform.ptsubif_tool7.value = 'f';
	for (var i = 0, j = document.mainform.elements['tool[]'].length; i < j; i++) {
		switch (document.mainform.elements['tool[]'].options[i].value) {
		case '1':
			opener.document.mainform.ptsubif_tool_flg.value = 't';
			break;
		case '2':
			opener.document.mainform.ptsubif_tool1.value = 't';
			break;
		case '3':
			opener.document.mainform.ptsubif_tool2.value = 't';
			break;
		case '4':
			opener.document.mainform.ptsubif_tool3.value = 't';
			break;
		case '5':
			opener.document.mainform.ptsubif_tool4.value = 't';
			break;
		case '6':
			opener.document.mainform.ptsubif_tool5.value = 't';
			break;
		case '7':
			opener.document.mainform.ptsubif_tool6.value = 't';
			break;
		case '8':
			opener.document.mainform.ptsubif_tool7.value = 't';
			break;
		}
	}

	opener.document.mainform.ptsubif_algy_flg.value = 'f';
	opener.document.mainform.ptsubif_allergy1.value = 'f';
	opener.document.mainform.ptsubif_allergy2.value = 'f';
	opener.document.mainform.ptsubif_allergy3.value = 'f';
	opener.document.mainform.ptsubif_allergy4.value = 'f';
	opener.document.mainform.ptsubif_allergy5.value = 'f';
	opener.document.mainform.ptsubif_allergy6.value = 'f';
	opener.document.mainform.ptsubif_allergy7.value = 'f';
	for (var i = 0, j = document.mainform.elements['allergy[]'].length; i < j; i++) {
		switch (document.mainform.elements['allergy[]'].options[i].value) {
		case '1':
			opener.document.mainform.ptsubif_algy_flg.value = 't';
			break;
		case '2':
			opener.document.mainform.ptsubif_allergy1.value = 't';
			break;
		case '3':
			opener.document.mainform.ptsubif_allergy2.value = 't';
			break;
		case '4':
			opener.document.mainform.ptsubif_allergy3.value = 't';
			break;
		case '5':
			opener.document.mainform.ptsubif_allergy4.value = 't';
			break;
		case '6':
			opener.document.mainform.ptsubif_allergy5.value = 't';
			break;
		case '7':
			opener.document.mainform.ptsubif_allergy6.value = 't';
			break;
		case '8':
			opener.document.mainform.ptsubif_allergy7.value = 't';
			break;
		}
	}

	opener.document.mainform.ptsubif_infect_flg.value = 'f';
	opener.document.mainform.ptsubif_infection1.value = 'f';
	opener.document.mainform.ptsubif_infection2.value = 'f';
	opener.document.mainform.ptsubif_infection3.value = 'f';
	opener.document.mainform.ptsubif_infection4.value = 'f';
	opener.document.mainform.ptsubif_infection5.value = 'f';
	opener.document.mainform.ptsubif_infection6.value = 'f';
	opener.document.mainform.ptsubif_infection7.value = 'f';
	opener.document.mainform.ptsubif_infection8.value = 'f';
	opener.document.mainform.ptsubif_infection9.value = 'f';
	opener.document.mainform.ptsubif_infection10.value = 'f';
	for (var i = 0, j = document.mainform.elements['epidemic[]'].length; i < j; i++) {
		switch (document.mainform.elements['epidemic[]'].options[i].value) {
		case '1':
			opener.document.mainform.ptsubif_infect_flg.value = 't';
			break;
		case '2':
			opener.document.mainform.ptsubif_infection1.value = 't';
			break;
		case '3':
			opener.document.mainform.ptsubif_infection2.value = 't';
			break;
		case '4':
			opener.document.mainform.ptsubif_infection3.value = 't';
			break;
		case '5':
			opener.document.mainform.ptsubif_infection4.value = 't';
			break;
		case '6':
			opener.document.mainform.ptsubif_infection5.value = 't';
			break;
		case '7':
			opener.document.mainform.ptsubif_infection6.value = 't';
			break;
		case '8':
			opener.document.mainform.ptsubif_infection7.value = 't';
			break;
		case '9':
			opener.document.mainform.ptsubif_infection8.value = 't';
			break;
		case '10':
			opener.document.mainform.ptsubif_infection9.value = 't';
			break;
		case '11':
			opener.document.mainform.ptsubif_infection10.value = 't';
			break;
		}
	}

	opener.setItemsLabel();
	self.close();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>項目選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_nm $ft_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<form name="mainform">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="コピー" onclick="copyItem();"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">装具・補助具</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="sel_tool" multiple size="8" style="width:140px;">
<option value="1">なし
<option value="2">上肢装具
<option value="3">下肢装具
<option value="4">義歯
<option value="5">義眼
<option value="6">補聴器
<option value="7">車椅子
<option value="8">歩行器
</select>
</td>
<td align="center" style="padding:0 5px;"><input type="button" value=" &gt; " onclick="addSelectedOption(this.form.elements['sel_tool'], this.form.elements['tool[]'], new Array('1'));"><br><br><input type="button" value=" &lt; " onclick="removeSelectedOption(this.form.elements['tool[]']);"></td>
<td><select name="tool[]" multiple size="8" style="width:140px;"></select></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アレルギー</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="sel_allergy" multiple size="8" style="width:140px;">
<option value="1">なし
<option value="2">抗生物質
<option value="3">解熱鎮静剤
<option value="4">局所麻酔薬
<option value="5">液製剤
<option value="6">ワクチン
<option value="7">造影剤
<option value="8">その他
</select>
</td>
<td align="center" style="padding:0 5px;"><input type="button" value=" &gt; " onclick="addSelectedOption(this.form.elements['sel_allergy'], this.form.elements['allergy[]'], new Array('1'));"><br><br>
<input type="button" value=" &lt; " onclick="removeSelectedOption(this.form.elements['allergy[]']);">
</td>
<td><select name="allergy[]" multiple size="8" style="width:140px;"></select></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">感染症</font></td>
<td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="sel_epidemic" multiple size="11" style="width:140px;">
<option value="1">なし
<option value="2">未検査
<option value="3">梅毒
<option value="4">Ｂ型肝炎
<option value="5">Ｃ型肝炎
<option value="6">ＨＩＶ
<option value="7">ＭＲＳＡ
<option value="8">緑膿菌
<option value="9">疥癬
<option value="10">ＡＴＬ
<option value="11">その他
</select>
</td>
<td align="center" style="padding:0 5px;"><input type="button" value=" &gt; " onclick="addSelectedOption(this.form.elements['sel_epidemic'], this.form.elements['epidemic[]'], new Array('1', '2'));"><br><br>
<input type="button" value=" &lt; " onclick="removeSelectedOption(this.form.elements['epidemic[]']);"></td>
<td><select name="epidemic[]" multiple size="11" style="width:140px;"></select></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="コピー" onclick="copyItem();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
