<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_class_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_application_workflow_common_class.php");
require_once("cl_level_analysis_util.php");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];

/**
 * データベースクローズ
 */
pg_close($con);

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

//====================================
//ファイル名
//====================================
$filename = 'level_analyse.xls';

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}

$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());



$data = get_analysis_data($mdb2, $emp_id);

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

$download_data = get_excle_data($data);

//====================================
//統計分析データEXCEL出力
//====================================
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();

//====================================
//内部関数
//====================================

function get_excle_data($data)
{

	$download_data = "";
	$download_data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";

	$download_data .= "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

	if ($_POST['job'] != '') {
		$download_data .= "<tr>";
		$download_data .= "<td nowrap>";
		$download_data .= "<b><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\">".htmlspecialchars("職種：".$_POST['job_view'])."</font></b>";
		$download_data .= "</td>";
		$download_data .= "</tr>";
	}

	if ($_POST['level'] != '') {
		$download_data .= "<tr>";
		$download_data .= "<td nowrap>";
		$download_data .= "<b><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\">".htmlspecialchars("レベル：".$_POST['level_view'])."</font></b>";
		$download_data .= "</td>";
		$download_data .= "</tr>";
	}


	for ($i = 0; $i < $_POST['affiliation_row_count']; $i++){

		$post_name = '所属'.($i + 1).'：';

		$post_name .= $_POST['class_id_view'][$i];
		$post_name .= " > ";
		$post_name .= $_POST['atrb_id_view'][$i];
		$post_name .= " > ";
		$post_name .= $_POST['dept_id_view'][$i];
		if ($_POST['room_id_view'][$i] != '') {
			$post_name .= " > ";
			$post_name .= $_POST['room_id_view'][$i];
		}

		$download_data .= "<tr>";
		$download_data .= "<td nowrap>";
		$download_data .= "<b><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\">".htmlspecialchars($post_name)."</font></b>";
		$download_data .= "</td>";
		$download_data .= "</tr>";
	}

	$post_name  = "分析項目：";
	$post_name .= $_POST['analysis_category_view'];
	$post_name .= " > ";
	$post_name .= $_POST['analysis_items_view'];

	if ($_POST['year_from'] != '') {
		$post_name .= " 年度：";
		$post_name .= $_POST['year_from_view'];
	}

	if ($_POST['year_to'] != '') {
		$post_name .= " - ";
		$post_name .= $_POST['year_to_view'];
	}

	$download_data .= "<tr>";
	$download_data .= "<td nowrap>";
	$download_data .= "<b><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\">".htmlspecialchars($post_name)."</font></b>";
	$download_data .= "</td>";
	$download_data .= "</tr>";

	$download_data .= "</table>";

	//集計表
	$download_data .= "<table border=\"1\" cellspacing=\"0\" cellpadding=\"2\">";

	//データセル行
	foreach($data as $row)
	{
		$download_data .= "<tr height=\"22\">";
		foreach($row as $cell)
		{
			if ($cell['bolheader']) {
				$download_data .= "<td bgcolor=\"#c8c8c8\"";
			} else {
				$download_data .= "<td";
			}
			if ($cell['rowspan'] != 0 && $cell['rowspan'] != '') {
				$download_data .= " rowspan=\"".$cell['rowspan']."\"";
			}
			if ($cell['colspan'] != 0 && $cell['colspan'] != '') {
				$download_data .= " colspan=\"".$cell['colspan']."\"";
			}
			if ($cell['align'] != '') {
				$download_data .= " align=\"".$cell['align']."\"";
			}
			$download_data .= "><font size=\"2\" face=\"ＭＳ Ｐゴシック, Osaka\">".htmlspecialchars($cell['value'])."</font></td>";
		}

		$download_data .= "</tr>";
	}

	$download_data .= "</table>";


	return $download_data;
}

function get_post_name($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room)
{
	// 組織数取得
	$arr_class_name = get_class_name_array($con, $fname);

	for($i=0; $i<count($search_emp_class); $i++) {
		// 初期化
		$post_name = "";

		if($search_emp_class[$i] != "") {
			$post_name .= get_class_nm($con, $search_emp_class[$i], $fname);

		} else {
			$post_name .= "全て";
		}

		$post_name .= "＞";

		if($search_emp_attribute[$i] != "") {
			$post_name .= get_atrb_nm($con, $search_emp_attribute[$i], $fname);
		} else {
			$post_name .= "全て";
		}

		$post_name .= "＞";

		if($search_emp_dept[$i] != "") {
			$post_name .= get_dept_nm($con, $search_emp_dept[$i], $fname);
		} else {
			$post_name .= "全て";
		}

		if($arr_class_name["class_cnt"] == 4) {
			$post_name .= "＞";
			if($search_emp_room[$i] != "") {
				$post_name .= get_room_nm($con, $search_emp_room[$i], $fname);
			} else {
				$post_name .= "全て";
			}
		}
		$post_name_arr[] = $post_name;
	}

	return implode(" | ", $post_name_arr);
}

?>