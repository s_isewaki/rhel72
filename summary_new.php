<?
/*

画面パラメータ
    $session
        セッションID
    $date
        初期表示する作成日付。
        省略時は現在日付を初期表示する。
    $summary_problem
        {今は未使用}
        プロブレム初期表示値
    $diag
        診療区分初期表示値
    $div_id
        診療区分初期表示値コード
    $priority
        記事優先度初期表示値
    $pt_id
        患者ID
    $xml_file
        テンプレートXMLファイル名
        これが指定された場合に限り、内容欄にテンプレートの内容を表示する。
    $tmpl_id
        テンプレートID
    $problem_id
        プロブレムID
    $summary_naiyo
        内容初期表示値
    $copy_summary_id
        コピーモードの場合にのみ指定する診療記録ID
        これが指定された場合は初期項目のうちいくつかは自動で設定される。
    $sect_id
        診療科ID
    $p_sect_id
        診療科ID(&sectはIEのみURLパラメータとして使用できないためGETアクセスの場合はこちらを使用する)
    $key1
    $key2

当画面への遷移パターン
・通常の空呼び出し
    ・起動時
    ・新規登録完了時
    ・更新画面で更新ボタン押下
    ・その他
・保存時チェックで入力エラー時
・テンプレート画面で入力完了時 (TinyMCEとの関係上、この設計を変えることは困難)
(・プロブレムリストからはJavascript呼び出し。)

*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("./show_select_values.ini");
require_once("./conf/sql.inf");
require_once("label_by_profile_type.ini");
require_once("html_to_text_for_tinymce.php");
require_once("sum_application_workflow_common_class.php");
require_once("sum_application_template.ini");
require_once("sot_util.php");
require_once("sum_exist_workflow_check_exec.ini");

//パラメータ精査
if(@$p_sect_id != "" && @$sect_id == "") $sect_id = $p_sect_id;


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
}


//日付を年月日に分割する。
if(!isset($_REQUEST['date'])){
    $date1 = mktime(0,0,0,date('m'), date('d'), date('Y'));
    $day = date('d', $date1);
    $month = date('m', $date1);
    $year = date('Y', $date1);
}
else
{
    $year = substr($date, 0, 4);
    $month = substr($date, 4, 2);
    $day = substr($date, 6, 2);
}

//利用者IDを取得
$emp_id = get_emp_id($con, $session, $fname);

//コピーモードの場合
$copy_meal_dr_emp_nm = ""; // 「食事開始変更箋」を「コピー」する場合、＜指示医師＞欄を書き換えるための職員名
if(@$copy_summary_id != "") {
    //コピー元の診療記録情報を取得
    $sql =
        " select * from summary".
        " where ptif_id = '$pt_id' and summary_id = '$copy_summary_id'";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        require_once("summary_common.ini");
        summary_common_show_error_page_and_die();
    }
    $copy_db_record = pg_fetch_all($sel);
    if(!$copy_db_record) {
        pg_query($con, "rollback");
        pg_close($con);
        require_once("summary_common.ini");
        summary_common_show_error_page_and_die();
    }

    $copy_db_record = $copy_db_record[0];
    $diag = $copy_db_record["diag_div"];
    $summary_naiyo = $copy_db_record["smry_cmt"];
    // opera対応
    if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") !== FALSE) {
        $summary_naiyo = html_to_text_for_tinymce($summary_naiyo);
    }
    $tmpl_id = $copy_db_record["tmpl_id"];
    $problem_id = $copy_db_record["problem_id"];
    $priority = $copy_db_record["priority"];
    $sect_id = $copy_db_record["sect_id"];
    if($tmpl_id != "") {

        // 2014/12/5
        //「食事開始変更箋」を「コピー」する場合は、＜指示医師＞欄を書き換える。
        // 他者の作成指示をコピーして指示を作成する場合に、指示医師はログイン者に変更したい。
        // 該当職種医師でない職員が「コピー」した場合は、もともとの医師名のままにしておく
        do {
            // これが食事開始変更箋かどうかを取得
            // 最初に食事箋のテンプレートIDを取得する
            $sel = select_from_table($con, "select tmpl_id from tmplmst where tmpl_file = 'tmpl_MealStart.php'", "", $fname);
            $rows2 = pg_fetch_all($sel);
            $tmpl_meal_start_ids = array();
            if ($rows2) {
                foreach ($rows2 as $row) $tmpl_meal_start_ids[] = $row["tmpl_id"];
            }
            if (!count($tmpl_meal_start_ids)) break;

            // 次に、このコピーが食事開始変更箋のコピーを行っているかを判定
            if (!in_array($tmpl_id, $tmpl_meal_start_ids)) break;

            // ログイン者が以下に示す３種の医師かどうか判定。該当医師だったら職員名取得
            $sql =
            " select e.emp_lt_nm, e.emp_ft_nm, j.job_nm from jobmst j".
            " inner join empmst e on ( j.job_id = e.emp_job )".
            " where j.job_nm in ('常勤医師','非常勤医師','歯科医師')".
            " and j.job_del_flg = 'f'".
            " and e.emp_id = '".$emp_id."'";
            $sel = select_from_table($con, $sql, "", $fname);
            while ($row = pg_fetch_array($sel)) {
                $copy_meal_dr_emp_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
                break;
            }
        } while (0);


        //テンプレートXMLをワークXMLとしてコピーする。
        $copy_creater_emp_id = $copy_db_record["emp_id"];

        // 何をしているかよくわからないが、結局、コピー元XMLファイルの名前を取得している。
        // コピー元と同じファイルをワークに登録することで、
        // ポップアップテンプレートが開いたときに、コピー元ファイルを読む、という流れらしい。
        $sql =
            " select xml_file_name, apply_id from (".
            "   select xml_file_name".
            "  ,case apply_id when null then 0 else apply_id end as apply_id".
            "   from sum_xml".
            "   where summary_id = ".(int)$copy_summary_id.
            "   and ptif_id = '".pg_escape_string($pt_id)."'".
            "   and (delete_flg = 0 or delete_flg is null)".
            "   and (is_work_data is null or is_work_data = '0')".
            " ) d".
            " order by apply_id desc limit 1";
        $sel_sum_xml = select_from_table($con, $sql, "", $fname);
        $xml_file = @pg_fetch_result($sel_sum_xml, 0, 0);
        if (!$xml_file) {
            $xml_file = "wk_".$emp_id.".xml";
            $copy_from_xml_file = $copy_creater_emp_id."_".$pt_id."_".$copy_summary_id.".xml";
            $copy_xml_file = $xml_file;

            $del = delete_from_table($con, "delete from sum_xml where xml_file_name = '".$copy_xml_file."' and is_work_data = '1'", "", $fname);
            $sql =
                " insert into sum_xml (emp_id, xml_file_name, smry_xml, is_work_data)".
                " select '".$emp_id."','".$copy_xml_file."', smry_xml, '1'".
                " from sum_xml where (xml_file_name = '".$copy_from_xml_file."'";
            $ins = insert_into_table($con,$sql, array(),$fname);
        }
    }
}

// 診療記録区分IDを取得
if (!@$div_id && @$diag){
    $sql = "select div_id from divmst where diag_div like '".pg_escape_string(@$diag)."'";
    $sel_divmst = select_from_table($con, $sql, "", $fname);
    $div_id = @pg_fetch_result($sel_divmst, 0, 0);
}


//記事優先度の初期値は１
if(@$priority == "")
{
    $priority = "1";
}

// 診療科一覧を取得
$sql =
        " select * from (".
        "   select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
        ") d";
$sql = "select sect_id, sect_nm from (".$sql.") d";
$cond = "where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1) order by sort_order, sect_id";

$sel_sect = select_from_table($con, $sql, $cond, $fname);
if ($sel_sect == 0) {
    pg_close($con);
    require_once("summary_common.ini");
    summary_common_show_error_page_and_die();
}
$sects = pg_fetch_all($sel_sect);

// ログインユーザが担当医の場合、所属している診療科を取得
if (@$sect_id == "") {
    $sql =
        " select dr.sect_id from drmst dr".
        " inner join sectmst se on (se.enti_id = dr.enti_id and se.sect_id = dr.sect_id and se.sect_del_flg = 'f')".
        " where dr.dr_emp_id = '$emp_id' and dr.dr_del_flg = 'f'".
        " limit 1";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        require_once("summary_common.ini");
        summary_common_show_error_page_and_die();
    }
    if (pg_num_rows($sel) > 0) {
        $sect_id = pg_fetch_result($sel, 0, "sect_id");
    }
}

// 診療科がセットされてなければ、患者の診療科をセット
if (@$sect_id == "") {
    $nyuin_info2 = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, ($year*10000)+($month*100)+$day);
    $sect_id = $nyuin_info2["inpt_sect_id"];
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 診療記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];


// ワークフローの取得
$wkfwmst = select_from_table($con, "select * from sum_wkfwmst where tmpl_id = ".(int)@$tmpl_id, "", $fname);
$wkfw_id = @pg_fetch_result($wkfwmst, 0, "wkfw_id");
$wkfw_enabled = @pg_fetch_result($wkfwmst, 0, "wkfw_enabled");

$obj = new application_workflow_common_class($con, $fname);

// 一時保存データの有無　201203追加
$sql_save = " select emp_id, ptif_id from sum_draft";
$cond_save = "where emp_id='".pg_escape_string($emp_id)."' and ptif_id='".pg_escape_string($pt_id)."'";
$sel_save = select_from_table($con, $sql_save, $cond_save, $fname);
$save_array = pg_fetch_all($sel_save);

if (!$save_array) {
    $cnt_save = 0;
}
else {
    $cnt_save = count($save_array);
}

//==============================
//HTML出力
//==============================
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?>｜新規作成</title>
<script type="text/javascript">

function add_target_list(item_id, emp_id, name){ // このコールバック関数は、sum_application_template.iniでも利用されている。かちあう。
    add_target_list_dairi(item_id, emp_id, name);
}

var m_use_tyny_mce = false;
</script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<?

//テンプレートが未使用orOperaの場合だけTinyMCEを使用
if(@$xml_file == ""){
?>
<script type="text/javascript">
if(!tinyMCE.isOpera) {
    m_use_tyny_mce = true;
    tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        plugins : "preview,table,emotions,fullscreen,layer",
        //language : "ja_euc-jp",
        language : "ja",
        width : "100%",
        height : "400",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        content_css : "tinymce/tinymce_content.css",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
        theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
        theme_advanced_buttons3 : "tablecontrols,|,visualaid",
        theme_advanced_statusbar_location : "none",
        force_br_newlines : true,
        forced_root_block : '',
        force_p_newlines : false
    });
}
</script>
<?
}

?>
<script type="text/javascript">

    // この子画面の現在の高さを求める
    function getCurrentHeight(){
        if (!window.parent) return 0;
        var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
        h = Math.max(h, document.body.scrollHeight);
        h = Math.max(h, 300);
        return (h*1+50);
    }
    // この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
    // 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
    function setDefaultHeight(){
        if (!window.parent) return;
        window.parent.setDefaultHeight("migi", 300);
        resetIframeHeight();
        var h = getCurrentHeight();
        if (h) window.parent.setDefaultHeight("migi",h);
    }
    // この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
    // 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
    // 親画面はこの子画面のIFRAMEサイズを変更する。
    // 一旦大きくしたIFRAMEのサイズは通常小さくできない。
    // よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
    function resizeIframeHeight(){
        resetIframeHeight();
        var h = getCurrentHeight();
        if (h) window.parent.resizeIframeHeight("migi", h);
    }
    // この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
    // 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
    function resetIframeHeight(){
        if (!window.parent) return;
        parent.resetIframeHeight("migi");
    }

    //起動時の処理を行います。
    function load_action() {
        set_summary_problem();
        start_auto_session_update();

        if (window.OnloadSub) { OnloadSub(); };
        if (window.refreshApproveOrders) {refreshApproveOrders();}
        setDefaultHeight();
        resizeIframeHeight();
    }

function set_summary_problem() {
    if(document.touroku.problem_id.options.length > 0) {
        var pl_index = document.touroku.problem_id.selectedIndex;
        var pl_value = document.touroku.problem_id.options[pl_index].text;
        document.touroku.summary_problem.value = pl_value;
    }
}

// 診療記録区分ドロップダウンのチェンジイベント
function diag_sel(){
    var diag = document.getElementById("sel_diag");
    if(document.touroku.xml_file.value != ""){
        if (confirm('<? echo($summary_kubun_title); ?>を変更する場合、入力したテンプレートの内容をクリアしますがよろしいですか？')) {
            location.href='./summary_new.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&div_id=' + diag.options[diag.selectedIndex].value;
        }   else {
            for(i=0; i< diag.options.length; i++) {
                if(diag.options[i].text == document.touroku.diag.value) {
                    document.getElementById("sel_diag").selectedIndex = i;
                    break;
                }
            }
        }
    }
}
// テンプレートアイコンのクリックイベント
function showTemplate(diag_div){
    var diag = document.getElementById("sel_diag");
    var idx = diag.selectedIndex;
    var new_idx = idx;
    if (!diag_div) diag_div = diag.options[idx].text;
    for(var i=0; i< diag.options.length; i++) {
        if(diag.options[i].text == diag_div) {
            diag.options.selectedIndex = i;
            new_idx = i;
            break;
        }
    }
    if(idx != new_idx && document.touroku.xml_file.value != ""){
        if(document.touroku.xml_file.value != ""){
            if (!confirm('<? echo($summary_kubun_title); ?>を変更する場合、入力したテンプレートの内容をクリアしますがよろしいですか？')) return;
        }
    }
    showTemplateWhenLoadAfter();
    document.touroku.popupWindowLoaded.value = "";
    setTimeout("reloadTimer(0,"+diag.options[new_idx].value+")", 200);
}
<? // IPADの小手先対応 ?>
<? //  当初location.hrefを変更した直後ポップアップを行っていたが、 ?>
<? //  ポップアップブロックにはじかれないように、先にwindow.openしている。?>
<? //  ところが、summary_tmpl_read.phpがこの画面値を参照しているため、 ?>
<? //  強引にリロードのタイミングをずらすことにした。?>
function reloadTimer(cnt, div_id){
    if (document.touroku.popupWindowLoaded.value != "1"){
        if (cnt > 50) return;
        cnt++;
        setTimeout("reloadTimer("+cnt+","+div_id+")", 200);
        return;
    }
    var seiki_emp_id = document.getElementById("seiki_emp_id").value;
    var dairi_emp_id = document.getElementById("dairi_emp_id").value;
    var url =
        'summary_new.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&div_id=' + div_id+
        '&seiki_emp_id='+seiki_emp_id+'&dairi_emp_id='+dairi_emp_id;
    var problem_id = document.touroku.problem_id.value;
    if (problem_id > 0) {
        url += '&problem_id=' + problem_id;
    }
    location.href = url;
}

function show_tmpl_window(url){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=600,height=700";
    window.open(url, 'tmpl_window',option);
}
function show_copy_window(url){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
    window.open(url, 'tmpl_window',option);
}
//定型文選択画面
function show_fixedform_window(url){
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=10,top=10,width=600,height=700";
    window.open(url, 'fixedform_window',option);
}
//定型文の設定
function call_back_fixedform_select(caller, result) {
    if(!tinyMCE.isOpera) {
        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent() + result.sentence);
    } else {
        document.touroku.summary_naiyo.value += result.sentence;
    }
}

//プロブレムリスト選択画面を表示します。
var problem_list_select_window_open_option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
function showProblemListSelectWindow()
{
    var url = "summary_problem_select_new.php?session=<?=$session?>&pt_id=<?=$pt_id?>";
    window.open(url, 'problem_list_select_window',problem_list_select_window_open_option);
}

function popupStdplanLibraryReference()
{
    var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=975,height=800";
    var problem_id = document.touroku.problem_id.value;
    window.open('summary_stdplan_library_reference_main.php?session=<? echo $session; ?>&ptif_id=<? echo $pt_id; ?>&problem_id=' + problem_id, 'stdPlanLibraryWin', option);
}

//プロブレムリスト選択完了時にコールバックされます。
function problem_list_selected_call_back(problem_id,problem_no,problem)
{
    set_postbackdata();
    document.postback.summary_problem.value = "#" + problem_no + " " + problem;
    document.postback.problem_id.value = problem_id;
    postback();

    //  //プロブレム値を作成
    //  var problem_value = "#" + problem_no + " " + problem;
    //  document.touroku.summary_problem.value = problem_value;
    //  document.touroku.problem_id.value = problem_id;
    //
    //  //プロブレムの制御
    //  seigyo_problem();
}

<?
require_once("summary_ymd_select_util.ini");
write_common_ymd_ctl_script();
?>



</script>
</head>
<body style="background-color:#f4f4f4" onload="load_action()">

<!-- セッションタイムアウト防止 START -->
<script type="text/javascript">
    function start_auto_session_update() {
        setTimeout(auto_sesson_update,60000);//1分(60*1000ms)後に開始
    }
    function auto_sesson_update() {
        document.session_update_form.submit();//セッション更新
        setTimeout(auto_sesson_update,60000);//1分(60*1000ms)後に再呼び出し
    }
</script>
<form name="session_update_form" action="summary_session_update.php" method="post" target="session_update_frame">
    <input type="hidden" name="session" value="<?=$session?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->

<form name="postback" action="summary_new.php" method="post">
<input type="hidden" name="session" value="">
<input type="hidden" name="date" value="">
<input type="hidden" name="summary_problem" value="">
<input type="hidden" name="diag" value="">
<input type="hidden" name="div_id" value="">
<input type="hidden" name="priority" value="">
<input type="hidden" name="pt_id" value="">
<input type="hidden" name="xml_file" value="">
<input type="hidden" name="tmpl_id" value="">
<input type="hidden" name="problem_id" value="">
<input type="hidden" name="summary_naiyo" value="">
<input type="hidden" name="sect_id" value="">
<input type="hidden" name="outreg" value="">
</form>
<script type="text/javascript">
function set_postbackdata() {
    var diag = document.getElementById("sel_diag");
    document.postback.session.value = "<?=$session?>";
    document.postback.date.value =
          document.touroku.sel_yr.options[document.touroku.sel_yr.selectedIndex].value
        + document.touroku.sel_mon.options[document.touroku.sel_mon.selectedIndex].value
        + document.touroku.sel_day.options[document.touroku.sel_day.selectedIndex].value;
    document.postback.summary_problem.value = document.touroku.summary_problem.value;
    document.postback.diag.value = diag.options[diag.selectedIndex].text;
    document.postback.div_id.value = diag.options[diag.selectedIndex].value;
    document.postback.priority.value =
        document.touroku.priority.options[document.touroku.priority.selectedIndex].text;
    document.postback.pt_id.value = "<?=$pt_id?>";
    document.postback.xml_file.value = document.touroku.xml_file.value;
    document.postback.tmpl_id.value = document.touroku.tmpl_id.value;
    document.postback.problem_id.value = document.touroku.problem_id.value;
    document.postback.sect_id.value = document.touroku.sect_id.value;
    document.postback.outreg.value = document.touroku.outreg.value;
    if(m_use_tyny_mce) {
        document.postback.summary_naiyo.value = tinyMCE.activeEditor.getContent();
    } else {
        document.postback.summary_naiyo.value = document.touroku.summary_naiyo.value;
    }
}
function postback() {
    document.postback.submit();
}

function attachFile() {
    window.open('sum_apply_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=250,scrollbars=yes');
}

function detachFile(e) {
    if (e == undefined) {
        e = window.event;
    }

    var btn_id;
    if (e.target) {
        btn_id = e.target.getAttribute('id');
    } else {
        btn_id = e.srcElement.id;
    }
    var id = btn_id.replace('btn_', '');

    var p = document.getElementById('p_' + id);
    document.getElementById('attach').removeChild(p);
    document.getElementById('attach').height = 10;
    resizeParentIframeHeight();
}

// 送信処理
function apply_regist(apv_num, precond_num) {
    // 受信者が全く存在しない場合
  var regist_flg = false;
    for(i=1; i<=apv_num; i++) {
        obj = 'regist_emp_id' + i;
        emp_id = document.touroku.elements[obj].value;
        if(emp_id != "") {
            regist_flg = true;
            break;
        }
    }

    if(!regist_flg) {
        alert('受信者が存在しないため、送信できません。');
        return;
    }

    if (window.InputCheck) {
        if (!InputCheck()) {
            return;
        }
    }

    // 重複チェック
    dpl_flg = false;
    for(i=1; i<=apv_num; i++) {
        obj_src = 'regist_emp_id' + i;
        emp_id_src = document.touroku.elements[obj_src].value;

        if(emp_id_src == "") {
            continue;
        }
        for(j=i+1; j<=apv_num; j++) {
            obj_dist = 'regist_emp_id' + j;
            emp_id_dist = document.touroku.elements[obj_dist].value;
            if(emp_id_src == emp_id_dist) {
                dpl_flg = true;
            }
        }
    }

    if(dpl_flg) {
        if (!confirm('受信者が重複していますが、送信しますか？')) {
            return;
        }
    }
    document.touroku.is_regist_and_send.value = "1";
    document.touroku.summary_naiyo_escaped_amp.value = document.touroku.summary_naiyo.value.split("&").join("&amp;");
    document.touroku.submit();
}

function tourokuSubmit(){
    document.touroku.summary_naiyo_escaped_amp.value = document.touroku.summary_naiyo.value.split("&").join("&amp;");
    document.touroku.submit();
}

// チーム計画
var teamplan_window="";
function standard_team_allocate(){
    var prob_id = document.touroku.problem_id.value;
    if ( prob_id != 0 ){
        var emp_id = "<?=$emp_id?>";
        var yy=document.touroku.sel_yr.value;
        var mm=document.touroku.sel_mon.value;
        var dd=document.touroku.sel_day.value;
        var today_date = yy + "-" + mm + "-" + dd;
        teamplan_window=window.open(    "summary_team_stdplan.php?session=<?=$session?>&pt_id=<?=$pt_id?>&problem_id="+prob_id+"&emp_id="+emp_id+"&create_date="+today_date,
            "",
            "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=950,height=900"
            );
    }else{
        alert("プロブレムを選択してください");
    }
}
</script>

<form name="touroku" action="summary_insert.php" method="post">
    <input type="hidden" name="popupWindowLoaded" value="">


<?
//==================================================
//プロブレム等の表のHTML出力
//==================================================
?>
<? $nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $pt_id, ($year*10000)+($month*100)+$day); ?>

<table class="list_table" cellspacing="1">
    <tr>
        <th width="20%"><a href="summary_rireki_table.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>&mode=section" target="hidari"><?=$_label_by_profile["SECTION"][$profile_type] ?></a></th>
        <td colspan="3">
            <div style="float:left">
                <select name="sect_id">
                    <option value=""></option>
                <? foreach ($sects as $tmp_sect) { ?>
                    <option value="<?=$tmp_sect["sect_id"]?>"<?= ($tmp_sect["sect_id"] == @$sect_id) ? " selected" : ""?>><?=$tmp_sect["sect_nm"]?></option>
                <? } ?>
                </select>
                <label style="vertical-align:bottom"><input type="checkbox" name="outreg" value="t"<?= (@$outreg == "t" && @$nyuin_info["ptif_id"]=="") ? " checked" : ""?>
                    style="padding:2px;"><?=$_label_by_profile["COME"][$profile_type]?>履歴に登録</label>
            </div>
            <? // 患者検索ダイアログを開く。現在の患者の病棟をダイアログに渡す。 ?>
            <div style="float:right">
                <? if ($is_workflow_exist){ ?>
                <input type="button" onclick="parent.location.href='sot_worksheet_ondoban.php?session=<?=$session?>&sel_ptif_id=<?=$pt_id?>';" value="温度板"/>
                <? } ?>
                <input type="button" onclick="popupKanjaSelect();" value="入院一覧"/>
                <script type="text/javascript">
                    function popupKanjaSelect(){
                        var sect = document.touroku.sect_id.value;
                        window.open(
                            "sot_kanja_select.php?session=<?=$session?>&js_callback=callbackKanjaSelect&sel_bldg_cd=<?=@$nyuin_info["bldg_cd"]?>&sel_ward_cd=<?=@$nyuin_info["ward_cd"]?>&sel_sort_by_byoto=1&sel_ptif_id=<?=$pt_id?>",<?//診療科(sel_sect_id)の第一パラメータ(enti_id)はとりあえず1固定にしておく?>
                            "",
                            "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=630,height=480"
                        );
                    }
                    function callbackKanjaSelect(ptif_id){
                        window.parent.location.href='summary_rireki.php?session=<?=$session?>&pt_id='+ptif_id;
                    }
                </script>
            </div>
            <div style="clear:both"></div>
        </td>
    </tr>
    <tr>
        <th>
            <a href="summary_rireki_table.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>&mode=problem" target="hidari">プロブレム</a>
        </th>
        <td colspan='3'>
            <div style="float:left">
                <input type="hidden" id="summary_problem" name="summary_problem" value="<?=@$problem?>">
                <select id='problem_id' name='problem_id' onChange="set_summary_problem();">
                <option value="0" <?if(@$problem_id == 0){?>selected<?}?> >#0 未選択</option>
                <?
                //==============================
                //プロブレムリスト取得
                //==============================
                $sql = "select summary_problem_list.* from summary_problem_list ";
                $cond = "where summary_problem_list.ptif_id = '$pt_id'";
                $cond .= " AND end_date='' ";
                $cond .= " order by summary_problem_list.problem_list_no";
                $sel_plist = select_from_table($con, $sql, $cond, $fname);
                if ($sel_plist == 0) {
                    pg_close($con);
                    require_once("summary_common.ini");
                    summary_common_show_error_page_and_die();
                }
                $num_plist = pg_numrows($sel_plist);
                for($i=0;$i<$num_plist;$i++)
                {
                    $pl_problem_id = pg_result($sel_plist,$i,"problem_id");
                    $pl_problem_list_no = pg_result($sel_plist,$i,"problem_list_no");
                    $pl_problem = pg_result($sel_plist,$i,"problem");
                    $pl_problem_value = "#$pl_problem_list_no $pl_problem";
                    $pl_selected = "";
                    if(@$problem_id == @$pl_problem_id)
                    {
                        $pl_selected = "selected";
                    }
                    ?>
                    <option value="<?=$pl_problem_id?>" <?=$pl_selected?>><?=$pl_problem_value?></option>
                    <?
                }
                ?>
                </select>
            </div>
            <div style="float:right">
                <input type="button" onclick="showProblemListSelectWindow();" value="NEW"/>
            </div>
            <div style="clear:both"></div>
            </td>
    </tr>
    <tr>
        <th><nobr>
            <a href="summary_rireki_table.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>" target="hidari"><?
// 診療記録区分/記録区分
echo ($_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type]); ?></a>
        </nobr></th>
        <? $div_info = summary_common_get_sinryo_kubun_info($con, $fname, $session, $emp_id, @$div_id, "", "yes"); // 編集権限つきリスト ?>
        <td>
            <select id="sel_diag" name="sel_diag" DataMember="診療区分" onChange="diag_sel();">
                <?=$div_info["dropdown_options"]?>
            </select>
            <script type="text/javascript">
                var enc_div_array = {
                    <? foreach($div_info["list"] as $didx => $drow){ ?>
                    "<?=$drow["div_id"]?>":"<?=$drow["enc_diag"]?>",
                    <? } ?>
                    "":""
                };
            </script>
        </td>
        <th><nobr><img src="img/repo-jyuuyou.gif" alt="記事重要度">&nbsp;記事重要度</nobr></th>
        <td width='30'>
            <select id='priority' name='priority'>
            <option value="1" <? if($priority == "1"){echo("selected");} ?>>1</option>
            <option value="2" <? if($priority == "2"){echo("selected");} ?>>2</option>
            <option value="3" <? if($priority == "3"){echo("selected");} ?>>3</option>
            </select>
            </td>
    </tr>
    <tr>
        <th>
            <a href="summary_rireki_list.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>" target="hidari">作成年月日</a>
        </th>
        <td colspan='3'>
            <select id='sel_yr' name='sel_yr'><? show_select_years_span(date("Y") - 14, date("Y") + 1, $year, false); ?></select>年
            <select id='sel_mon' name='sel_mon'><? show_select_months($month); ?></select>月
            <select id='sel_day' name='sel_day'><? show_select_days($day); ?></select>日
            <input type="button" onclick="set_ymd_select_add_day(document.touroku.sel_yr,document.touroku.sel_mon,document.touroku.sel_day,-1);" value="1日前"/>
            <input type="button" onclick="set_ymd_select_add_day(document.touroku.sel_yr,document.touroku.sel_mon,document.touroku.sel_day,1);" value="1日後"/>
        </td>
    </tr>
    <tr>
        <th>
            <a href="#" onclick="popupEmplist2(); return false;" class="always_blue">記載者</a>
            <script type="text/javascript">
                var dairi_emp_selecting = false;
                function popupEmplist2(){
                    dairi_emp_selecting = true;
                    window.open('emplist_popup.php?session=<?=$session?>&item_id=1&mode=16', 'newwin', 'directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=720,height=600');
                }
                function add_target_list_dairi(item_id, emp_id, name){
                    dairi_emp_selecting = false;
                    if (document.getElementById("dairi_emp_id").value==""){
                        document.getElementById("dairi_sousinsya").innerHTML = document.getElementById("sousinsya").innerHTML;
                        document.getElementById("dairi_emp_id").value = document.getElementById("seiki_emp_id").value;
                        document.getElementById("dairi_sousinsya_title").style.display = "";
                    }
                    document.getElementById("sousinsya").innerHTML = name;
                    document.getElementById("seiki_emp_id").value = emp_id;
                }
                function cancelDairi(){
                    if (!confirm("代行者での登録をキャンセルしますか？")) return false;
                    document.getElementById("sousinsya").innerHTML = document.getElementById("dairi_sousinsya").innerHTML;
                    document.getElementById("seiki_emp_id").value = document.getElementById("dairi_emp_id").value;
                    document.getElementById("dairi_sousinsya_title").style.display = "none";
                    document.getElementById("dairi_sousinsya").innerHTML = "";
                    document.getElementById("dairi_emp_id").value = "";
                }
            </script>
        </th>
        <td colspan="4">
            <?
                if (!@$seiki_emp_id) $seiki_emp_id = $emp_id;
                $sql = "select emp_job, emp_lt_nm, emp_ft_nm from empmst where emp_id = '".pg_escape_string($seiki_emp_id)."'";
                $sel2 = select_from_table($con, $sql, "", $fname);
                $emp_info = pg_fetch_array($sel2);
                $sel2 = select_from_table($con, "select job_nm from jobmst where job_id = ".(int)$emp_info["emp_job"], "", $fname);
                $job_info = pg_fetch_array($sel2);
                $job_name = @$job_info["job_nm"];
$job_name_ = $job_name ;
                if ($job_name) $job_name = "（".$job_name."）";
                $dairi_emp_id = @$_REQUEST["dairi_emp_id"];
                $sql = "select emp_job, emp_lt_nm, emp_ft_nm from empmst where emp_id = '".pg_escape_string($dairi_emp_id)."'";
                $sel2 = select_from_table($con, $sql, "", $fname);
                $emp_info_d = pg_fetch_array($sel2);
                $sel2 = select_from_table($con, "select job_nm from jobmst where job_id = ".(int)$emp_info_d["emp_job"], "", $fname);
                $job_info_d = @pg_fetch_array($sel2);
                $job_name_d = @$job_info_d["job_nm"];
                if ($job_name_d) $job_name_d = "（".$job_name_d."）";
            ?>
            <span id="sousinsya" style="padding-right:10px"><?=$emp_info["emp_lt_nm"]." ".$emp_info["emp_ft_nm"]?><?=$job_name?></span>
            <span id="dairi_sousinsya_title" style="padding:2px; background-color:#eff9fd; border:1px solid #9bc8ec;<?=(!$dairi_emp_id)?" display:none;":""?>">
                <a href="#" onclick="cancelDairi()" class="always_blue">代行者</a>
            </span>
            <span id="dairi_sousinsya" style="padding-left:4px">
                <?=@$emp_info_d["emp_lt_nm"]." ".@$emp_info_d["emp_ft_nm"]?><?=$job_name_d?></span>
            <input type="hidden" id="dairi_emp_id" name="dairi_emp_id" value="<?=$dairi_emp_id?>"/>
            <input type="hidden" id="seiki_emp_id" name="seiki_emp_id" value="<?=$seiki_emp_id?>"/>
        </td>
    </tr>
</table>


<?
//==================================================
//内容のタイトルと各種ボタンのHTML出力
//==================================================
?>

<?
if($wkfw_enabled){
    $arr_wkfwapv = $obj->get_wkfwapv_info($wkfw_id, $emp_id, $pt_id);
    $approve_num = count($arr_wkfwapv);   // 受信者数

    // 食事変更開始箋の第1階層自動セット
    if ($_REQUEST["mstt_dr_id"] != "" && $emp_id != $_REQUEST["mstt_dr_id"] && $arr_wkfwapv[0]["apv_order"] == "1" && $arr_wkfwapv[0]["apv_div2_flg"] == "t" && $arr_wkfwapv[0]["emp_infos"][0]["emp_id"] == "") {
        $sql = "select e.emp_lt_nm, e.emp_ft_nm, s.st_nm from empmst e inner join stmst s on e.emp_st = s.st_id";
        $cond = "where e.emp_id = '{$_REQUEST["mstt_dr_id"]}'";
        $sel2 = select_from_table($con, $sql, $cond, $fname);
        if (pg_num_rows($sel2) > 0) {
            $st_nm = pg_fetch_result($sel2, 0, "st_nm");
            $arr_wkfwapv[0]["emp_infos"][0] = array(
                "emp_id" => $_REQUEST["mstt_dr_id"],
                "emp_full_nm" => pg_fetch_result($sel2, 0, "emp_lt_nm") . " " . pg_fetch_result($sel2, 0, "emp_ft_nm"),
                "st_nm" => $st_nm,
                "display_caption" => $st_nm,
                "apv_sub_order" => ""
            );
        }
    }
}
?>

<!--div style="text-align:right; margin-top:4px; padding-bottom:4px; border-bottom:1px solid #fdfdfd;"-->
<table border="0" width="100%"><tr><td align="left">
<!-- チーム計画・看護計画追加 201208 ▼ -->
<input type="button" onclick="standard_team_allocate();" value="チーム計画"/>
<input type="button" onclick="popupStdplanLibraryReference();" value="標準看護計画"/>
<!-- チーム計画・看護計画追加 ここまで ▲ -->
</td>
<td align="right">
<!-- 一時保存機能追加 201203 ▼ -->
    <input type="button" onclick="tmpSaveSubmit();" value="一時保存" <?=$disabled_toroku?> />
    <script type="text/javascript">
        formTmpSave = document.createElement('FORM');
        var b   = document.getElementsByTagName('body');
        b[0].appendChild(formTmpSave);
        formTmpSave.name   = 'frm_tmp_save';
//        formPdf.target = '_blank';
        formTmpSave.method = 'post';
        formTmpSave.action = 'summary_tmp_save.php';
        formTmpSave.innerHTML = '' +
            '<input type="hidden" name="session" value="<?=$session?>" />' +
            '<input type="hidden" name="sect_id" value="" />' +
            '<input type="hidden" name="outreg" value="" />' +
            '<input type="hidden" name="summary_problem" value="" />' +
            '<input type="hidden" name="problem_id" value="" />' +
            '<input type="hidden" name="sel_yr" value="" />' +
            '<input type="hidden" name="sel_mon" value="" />' +
            '<input type="hidden" name="sel_day" value="" />' +
            '<input type="hidden" name="pt_id" value="<?=$pt_id?>" />' +
            '<input type="hidden" name="emp_id" value="<?=$emp_id?>" />' +
            '<input type="hidden" name="seiki_emp_id" value="" />' +
            '<input type="hidden" name="dairi_emp_id" value="" />' +
            '<input type="hidden" name="xml_file" value="<?=$xml_file?>" />' +
            '<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>" />' +
            '<input type="hidden" name="sel_diag" value="" />' +
            '<input type="hidden" name="summary_naiyo" value="" />' +
            '<input type="hidden" name="summary_naiyo_escaped_amp" value="" />' +
            '<input type="hidden" name="copy_summary_id" value="<?=@$copy_summary_id?>" />';

        function tmpSaveSubmit() {
            formTmpSave.sect_id.value             = document.touroku.sect_id.value;
            formTmpSave.outreg.value              = document.touroku.outreg.value;
            formTmpSave.summary_problem.value     = document.touroku.summary_problem.value;
            formTmpSave.problem_id.value          = document.touroku.problem_id.value;
            formTmpSave.sel_diag.value            = document.touroku.sel_diag.value;
            formTmpSave.sel_yr.value              = document.touroku.sel_yr.value;
            formTmpSave.sel_mon.value             = document.touroku.sel_mon.value;
            formTmpSave.sel_day.value             = document.touroku.sel_day.value;
            formTmpSave.seiki_emp_id.value        = document.touroku.seiki_emp_id.value;
            formTmpSave.dairi_emp_id.value        = document.touroku.dairi_emp_id.value;
            formTmpSave.summary_naiyo.value       = document.touroku.summary_naiyo.value;
            formTmpSave.summary_naiyo_escaped_amp = document.touroku.summary_naiyo.value.split("&").join("&amp;");

            formTmpSave.submit();
        }
    </script>

<? if ($sel_save != 0 && $cnt_save > 0) { ?>
    <input type="button" onclick="tmpSaveReadSubmit();" value="一時保存読込み" <?=$disabled_hensyu?> />
<? } ?>
    <script type="text/javascript">
        formTmpSaveRead = document.createElement('FORM');
        var b   = document.getElementsByTagName('body');
        b[0].appendChild(formTmpSaveRead);
        formTmpSaveRead.name   = 'frm_tmp_save_read';
//        formPdf.target = '_blank';
        formTmpSaveRead.method = 'post';
        formTmpSaveRead.action = 'summary_tmp_save_read.php';
        formTmpSaveRead.innerHTML = '' +
            '<input type="hidden" name="session" value="<?=$session?>" />' +
            '<input type="hidden" name="sect_id" value="" />' +
            '<input type="hidden" name="outreg" value="" />' +
            '<input type="hidden" name="summary_problem" value="" />' +
            '<input type="hidden" name="problem_id" value="" />' +
            '<input type="hidden" name="sel_yr" value="" />' +
            '<input type="hidden" name="sel_mon" value="" />' +
            '<input type="hidden" name="sel_day" value="" />' +
            '<input type="hidden" name="pt_id" value="<?=$pt_id?>" />' +
            '<input type="hidden" name="emp_id" value="<?=$emp_id?>" />';

        function tmpSaveReadSubmit() {
            formTmpSaveRead.sect_id.value             = document.touroku.sect_id.value;
            formTmpSaveRead.outreg.value              = document.touroku.outreg.value;
            formTmpSaveRead.summary_problem.value     = document.touroku.summary_problem.value;
            formTmpSaveRead.problem_id.value          = document.touroku.problem_id.value;
            formTmpSaveRead.sel_yr.value              = document.touroku.sel_yr.value;
            formTmpSaveRead.sel_mon.value             = document.touroku.sel_mon.value;
            formTmpSaveRead.sel_day.value             = document.touroku.sel_day.value;

            formTmpSaveRead.submit();
        }
    </script>
<!-- 一時保存機能追加 201203 ▲ -->

    <input type="button" onclick="location.href='./summary_new.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>';" value="クリア"/>
  <? $disabled_hensyu = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
    <input type="button" onclick="startEdit();" value="編集" <?=$disabled_hensyu?> />
    <script type="text/javascript">
        function startEdit(){
            var url =
                    'summary_tmpl_read.php'
                + '?session=<? echo($session); ?>'
                + '&pt_id=<? echo($pt_id); ?>'
                + '&emp_id=<? echo($emp_id); ?>'
                + '&xml_file=' + document.touroku.xml_file.value
                + '&tmpl_id=<? echo(@$tmpl_id); ?>'
                + '&enc_diag=' + enc_div_array[document.getElementById("sel_diag").value]
                + '&div_id=' + document.getElementById("sel_diag").options[document.getElementById("sel_diag").selectedIndex].value
                + '&update_flg=new'
                + '&cre_date='+document.touroku.sel_yr.options[document.touroku.sel_yr.selectedIndex].value
                + document.touroku.sel_mon.options[document.touroku.sel_mon.selectedIndex].value
                + document.touroku.sel_day.options[document.touroku.sel_day.selectedIndex].value
                + '&dairi_emp_id='+document.getElementById("dairi_emp_id").value
                + '&seiki_emp_id='+document.getElementById("seiki_emp_id").value;
            show_tmpl_window(url);
        }
    </script>


<? if($wkfw_enabled){ ?>
  <? $disabled_toroku = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
  <? $disabled_toroku_soshin = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
    <input type="button" onclick="tourokuSubmit();" value="登録" <?=$disabled_toroku?> />
    <input type="button" onclick="apply_regist('<?=$approve_num?>', '<?=count(@$precond_wkfw_id)?>');" value="登録＋送信" <?=$disabled_toroku_soshin?> />
<? } else { ?>
  <? $disabled_toroku = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
    <input type="button" onclick="tourokuSubmit();" value="登録" <?=$disabled_toroku?> />
<? } ?>
</td></tr></table>
<!--/div-->

<div style="border-top:1px solid #d8d8d8; padding-top:4px; padding-bottom:4px">
<div style="float:left">
    <? foreach (@$div_info["icon_list"] as $idx => $row){ ?>
        <div style="float:left; margin-left:2px; margin-bottom:2px; text-align:center; font-size:10px;">
        <a href="" style="text-decoration:none;" class="always_blue" onclick="showTemplate('<? echo $row["diag_div"]; ?>'); return false;"><img src="<? echo $row["file_path"]; ?>" alt="<? echo $row["diag_div"]; ?>" title="<? echo $row["diag_div"]; ?>"/><br/><? echo h($row["diag_div"]); ?></a>
        </div>
    <? } ?>
    <div style="clear:both"></div>
</div>
<div style="float:right; margin-bottom:4px">
    <? $disabled_tmpl = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
    <? $disabled_tmplcopy = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
        <input type="button" onclick="showTemplate('');" value="テンプレート" <?=$disabled_tmpl?> />
        <input type="button" onclick="showTemplateCopy();" value="コピー" <?=$disabled_tmplcopy?> />
        <script type="text/javascript">
            function showTemplateCopy(){
                var diag_selection = document.getElementById("sel_diag").options[document.getElementById("sel_diag").selectedIndex];
                var url =
                        'summary_copy.php'
                    + '?session=<?=$session?>'
                    + '&pt_id=<?=$pt_id?>'
                    + '&emp_id=<?=$emp_id?>'
                    + '&enc_diag=' + enc_div_array[diag_selection.value]
                    + '&div_id=' + diag_selection.value
                    + '&update_flg=new';
                show_copy_window(url);
            }
        </script>
    <? $disabled_teikeibun = (@$disabled_kousin_sousin_flg ? "disabled" : ""); ?>
        <input type="button" onclick="popupTeikeibun();" value="定型文" <?=$disabled_teikeibun?> />
        <script type="text/javascript">
            function popupTeikeibun(){
                var url =
                        'summary_fixedform_select.php'
                    + '?session=<?=$session?>'
                    + '&caller=summary_naiyo'
                    + '&multi_flg=';
                show_fixedform_window(url);
            }
        </script>
</div>
<div style="clear:both"></div>
</div>


<?
//==================================================
//内容入力領域のHTML出力
//==================================================
?>
    <input type="hidden" name="summary_naiyo_escaped_amp" value =""/>
<? // テンプレートである場合 ?>
<? if(@$xml_file != ""){ ?>
    <? require_once("summary_tmpl_util.ini"); ?>
  <?
     $naiyo_html = $_REQUEST["save_html"] ;
     if ($naiyo_html)
     {
       $naiyo_html = html_entity_decode($naiyo_html, ENT_QUOTES) ;
       $naiyo_html = str_replace("<br/>", "\n", $naiyo_html) ;
     }
     else
     {
       $naiyo_html = get_summary_naiyo_html_for_tmpl_textarea_value($con,$fname,$tmpl_id,$xml_file) ;
     }
  ?>
    <?
        $naiyo_textarea_value = @replaceEmpInfo($naiyo_html, $year, $month, $day, $emp_info["emp_lt_nm"], $emp_info["emp_ft_nm"], $job_name_);


        // 2014/12/5 医師名書き換え
        if(@$copy_summary_id != "") {
            $naiyo_textarea_value = replaceEmpInfo_SijiDr($naiyo_textarea_value, $copy_meal_dr_emp_nm);
        }
    ?>

    <textarea id="summary_naiyo" style="width:98%; height:400px; ime-mode:active;" size="54" name="summary_naiyo" <?=(@$xml_file!="")?"readonly":""?>><? echo($naiyo_textarea_value); ?></textarea>
<? // テンプレートではない場合 ?>
<? } else { ?>
    <textarea id="summary_naiyo" style="width:100%; height:400px; ime-mode:active;" size="54" name="summary_naiyo" <?=(@$xml_file!="")?"readonly":""?>><? echo(@$summary_naiyo); ?></textarea>
<? } ?>



<?
if($wkfw_enabled){
    if (@$apply_id == "") {
        show_application_template($con, $session, $fname, $arr_wkfwapv, $wkfw_id, @$apply_title, @$content, @$file_id, @$filename, @$back, @$mode, "");
    } else {
        show_draft_template($con, $session, $fname, $arr_wkfwapv, $apply_id, @$apply_title, @$content, @$file_id, @$filename, @$back, @$mode);
    }
}
?>
<?
//==================================================
//下部のボタンHTML出力
//==================================================
?>
<div style="width:100%; height:24px" align="right" ms_positioning="FlowLayout">
    <div style="margin-top:4px">
        <input type="button" onclick="location.href='./summary_new.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo(@$key1); ?>&key2=<? echo(@$key2); ?>';" value="クリア"/>
        <input type="button" onclick="startEdit();" value="編集" <?=$disabled_hensyu?> />
    <? if($wkfw_enabled){ ?>
        <input type="button" onclick="tourokuSubmit();" value="登録" <?=$disabled_toroku?> />
        <input type="button" onclick="apply_regist('<?=$approve_num?>', '<?=count(@$precond_wkfw_id)?>');" value="登録＋送信" <?=$disabled_toroku_soshin?> />
    <? } else { ?>
        <input type="button" onclick="tourokuSubmit();" value="登録" <?=$disabled_toroku?> />
    <? } ?>
    </div>
</div>


<?
//==================================================
//送信HIDDENパラメータHTML出力
//==================================================
?>
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_appr" value="<?=@pg_fetch_result($wkfwmst, 0, "wkfw_appr") ?>">
<input type="hidden" name="wkfw_content_type" value="<?=@pg_fetch_result($wkfwmst, 0, "wkfw_content_type") ?>">
<input type="hidden" name="is_regist_and_send" value="">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="key1" value="<? echo(@$key1); ?>">
<input type="hidden" name="key2" value="<? echo(@$key2); ?>">
<input type="hidden" name="diag" value="<? echo(@$diag); ?>">
<input type="hidden" name="div_id" value="<? echo(@$div_id); ?>">
<input type="hidden" name="xml_file" value="<? echo(@$xml_file); ?>">
<input type="hidden" name="tmpl_id" value="<? echo(@$tmpl_id); ?>">
<!--
<input type="hidden" name="problem_id" value="<? echo(@$problem_id); ?>">
-->


<?
//==================================================
//起動時テンプレート表示JavaScript出力 (コピーモード)
//==================================================
?>
<?
$url_xml_file_property_name = "xml_file";
if (!@$is_popup_soon){
    if(@$copy_summary_id != "" && @$xml_file != ""){
        if(@$no_tmpl_window != "1") {
            $is_popup_soon = 1;
            $url_xml_file_property_name = "tmpl_copy_xml_file";
        }
    }
}
?>
<script language="javascript">
<? if (@$is_popup_soon){ ?>
    showTemplateWhenLoadAfter();
<? } ?>
    function showTemplateWhenLoadAfter(){
        var diag_selection = document.getElementById("sel_diag").options[document.getElementById("sel_diag").selectedIndex];
        var prob_id = document.touroku.problem_id.value;
        var url = 'summary_tmpl_read.php'
            + '?session=<? echo($session); ?>'
            + '&pt_id=<? echo(@$pt_id); ?>'
            + '&emp_id=<? echo(@$emp_id); ?>'
            + '&<?=$url_xml_file_property_name?>=' + document.touroku.xml_file.value
            + '&tmpl_id=<? echo(@$tmpl_id); ?>'
            + '&enc_diag=' + enc_div_array[diag_selection.value]
            + '&div_id=' + diag_selection.value
            + '&update_flg=new'
            + '&dairi_emp_id='+document.getElementById("dairi_emp_id").value
            + '&seiki_emp_id='+document.getElementById("seiki_emp_id").value
            + '&problem_id='+prob_id;
        show_tmpl_window(url);
    }

  var newUrl ;
  var saveHtml ;
  function showNewPage()
  {
    var form = document.createElement("form");
    document.body.appendChild(form);
    var input = document.createElement("input");
    input.setAttribute("type","hidden");
    input.setAttribute("name","save_html");
    input.setAttribute("value",saveHtml);
    form.appendChild(input);
    form.setAttribute("action",newUrl);
    form.setAttribute("method","post");
    form.submit();
  }

  function testalert()
  {

   alert(newUrl)  ;
  }



</script>


</form>
</body>
</html>

<?
// 指定されたタグ付き文書内の記載者情報を画面上部の掲載値に書き換えます
function replaceEmpInfo($text, $year, $month, $day, $empNmlt, $empNmft, $jobName)
{
  $temp = $text ;

  $idxs = mb_strpos($temp, "＜作成日＞") ;
  if ($idxs > -1)
  {
    $idxn = $idxs + 5 ;
    $idxe = mb_strpos($temp, "＜", $idxn) ;
    if ($idxe > -1)
    {
      $dateStr = $year. "年" . $month . "月" . $day . "日 " . date("H時i分") ;
      $temp = mb_substr($temp, 0, $idxn) . "\n&nbsp;&nbsp;" . $dateStr. "\n" . mb_substr($temp, $idxe)  ;
    }
  }

  $idxs = mb_strpos($temp, "＜記載者＞") ;
  if ($idxs > -1)
  {
    $idxn = $idxs + 5 ;
    $idxe = mb_strpos($temp, "＜", $idxn) ;
    if ($idxe > -1)
    {
      $temp = mb_substr($temp, 0, $idxn) . "\n&nbsp;&nbsp;" . $empNmlt . "&nbsp;" . $empNmft . "\n" . mb_substr($temp, $idxe)  ;
    }
  }

  $idxs = mb_strpos($temp, "＜職種＞") ;
  if ($idxs > -1)
  {
    $idxn = $idxs + 4 ;
    $idxe = mb_strpos($temp, "＜", $idxn) ;
    if ($idxe > -1)
    {
      $temp = mb_substr($temp, 0, $idxn) . "\n&nbsp;&nbsp;" . $jobName . "\n" . mb_substr($temp, $idxe)  ;
    }
  }

  return $temp ;
}
// 2014/12/5 追加
function replaceEmpInfo_SijiDr($text, $dr_emp_nm)
{
  $temp = $text ;

  $idxs = mb_strpos($temp, "＜指示医師＞") ;
  if ($idxs > -1)
  {
    $idxn = $idxs + 6 ;
    $idxe = mb_strpos($temp, "＜", $idxn) ;
    if ($idxe > -1)
    {
      $temp = mb_substr($temp, 0, $idxn) . "\n&nbsp;&nbsp;" . $dr_emp_nm. "\n" . mb_substr($temp, $idxe)  ;
    }
  }
  return $temp ;
}
?>


