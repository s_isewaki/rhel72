<?php
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("Expires: Thu, 01 Jan 1970 00:00:00 GMT");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta name="robots" content="noindex,nofollow">
<title>CoMedix</title>
<?php

require_once("about_comedix.php");
require_once("show_event_from_login.ini");
require_once("show_area_from_login.ini");
require_once("show_allot_from_login.ini");
require_once("show_welfare_from_login.ini");
require_once("show_qa_from_login.ini");
require_once("show_stat_from_login.ini");
require_once("menu_news_from_login.ini");
require_once("show_reservation_from_login.ini");
require_once("show_library_from_login.ini");
require_once("show_calendar_from_login.ini");
require_once("show_wic_from_login.ini");
require_once("show_ext_search_from_login.ini");
require_once("show_link_from_login.ini");
require_once("show_weather_from_login.ini");
require_once("show_extension_from_login.ini");
require_once("show_copyright.ini");

$fname = $PHP_SELF;

// パラメータ
$mode = @$_REQUEST['mode'];

// データベースに接続
$con = connect2db($fname);

// 環境設定値を取得
$sql = "select * from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$interval = pg_fetch_result($sel, 0, "interval");
$login_page_reflesh = ($interval == "t") ? pg_fetch_result($sel, 0, "login_page_reflesh") * 1000 : 0;  // 更新間隔はミリ秒単位に変換
$timecard_use = pg_fetch_result($sel, 0, "timecard_use");
if ($timecard_use == "t") {
    $ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");
    $timecard_btn_width = ($ret_btn_flg == "f") ? "640" : "700";
    $title = "";
} else {
    $ret_btn_flg = "f";
    $title = pg_fetch_result($sel, 0, "title");
}
$login_pos = pg_fetch_result($sel, 0, "login_pos");
$text_show_flg = pg_fetch_result($sel, 0, "text_show_flg");
$free_text = pg_fetch_result($sel, 0, "free_text");
$event_show_flg = pg_fetch_result($sel, 0, "event_show_flg");
$extension_show_flg = pg_fetch_result($sel, 0, "extension_show_flg");
$allot_show_flg = pg_fetch_result($sel, 0, "allot_show_flg");
$welfare_show_flg = pg_fetch_result($sel, 0, "welfare_show_flg");
$qa_show_flg = pg_fetch_result($sel, 0, "qa_show_flg");
$stat_show_flg = pg_fetch_result($sel, 0, "stat_show_flg");
$info_show_flg = pg_fetch_result($sel, 0, "info_show_flg");
$rsv_show_flg = pg_fetch_result($sel, 0, "rsv_show_flg");
$lib_show_flg = pg_fetch_result($sel, 0, "lib_show_flg");
$ext_search_show_flg = pg_fetch_result($sel, 0, "ext_search_show_flg");
$link_show_flg = pg_fetch_result($sel, 0, "link_show_flg");
$login_color = pg_fetch_result($sel, 0, "login_color");
$tmcd_chg_flg = pg_fetch_result($sel, 0, "tmcd_chg_flg");
$cal_show_flg = pg_fetch_result($sel, 0, "cal_show_flg");
$cal_start_wd = pg_fetch_result($sel, 0, "cal_start_wd");
$weather_show_flg = pg_fetch_result($sel, 0, "weather_show_flg");
$weather_area = pg_fetch_result($sel, 0, "weather_area");
$wic_show_flg = pg_fetch_result($sel, 0, "wic_show_flg");
$area_show_flg = pg_fetch_result($sel, 0, "area_show_flg");
$login_autocomplete = pg_fetch_result($sel, 0, "login_autocomplete");
$ic_read_flg = pg_fetch_result($sel, 0, "ic_read_flg");
$login_focus = pg_fetch_result($sel, 0, "login_focus");

// ブロック設定情報を取得
$sql = "select * from loginblock";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$center_blocks = array();
$right_blocks = array();
while ($row = pg_fetch_array($sel)) {
    if (substr($row["block_id"], 0, 1) == "2") {
//      $center_blocks[$row["block_id"]] = $row["order_no"];
        $center_blocks[] = $row["block_id"];
    } else if (substr($row["block_id"], 0, 1) == "3") {
//      $right_blocks[$row["block_id"]] = $row["order_no"];
        $right_blocks[] = $row["block_id"];
    }
}

// ライセンス有効期限情報を取得
$sql = "select lcs_expire_flg, lcs_expire_date from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$expire_flg = pg_fetch_result($sel, 0, "lcs_expire_flg");
$expire_date = pg_fetch_result($sel, 0, "lcs_expire_date");

// 文書管理の並び順は更新日時の降順がデフォルト
if ($doc_o == "") {$doc_o = "4";}

// 文書管理の表示件数は7件がデフォルト
if ($doc_limit == "") {$doc_limit = "7";}

// ログインフォームが中央表示でない場合、タイムカード機能は使用しない
if ($login_pos != "1") {$timecard_use = "f";}

// ログインフォームなしの場合でも、管理者モードであればフォームをヘッダに表示する
if ($login_pos == "3" && $mode === "admin") {
    $login_pos = "2";
}

// mode=nologinなら、ログインフォーム無し
if ($mode === 'nologin') {
    $login_pos = "3";
}

if ($login_color == "1") {
    $block_colors["border"] = "#5279a5";
    $block_colors["header"] = "#bdd1e7";
    $block_colors["column"] = "#f6f9ff";
} else {
    $block_colors["border"] = "#0e9c55";
    $block_colors["header"] = "#ceefc2";
    $block_colors["column"] = "#f7fdf2";
}

// 有効期限のチェック
$expired = false;
$expire_warn = false;
if ($expire_flg == "t") {
    if (date("Ymd") > $expire_date) {
        $expired = true;
        $free_text = "<center><strong>CoMedix利用期限日を過ぎました。継続して利用される場合は、ライセンスの購入手続をお願いします。</strong></center>";
    } else {
        $one_month_after = date("Ymd", strtotime("+1 month"));
        if ($one_month_after >= $expire_date) {
            $expire_warn = true;
            $expire_year = substr($expire_date, 0, 4);
            $expire_month = substr($expire_date, 4, 2);
            $expire_day = substr($expire_date, 6, 2);
            $expire_timestamp = mktime(0, 0, 0, $expire_month, $expire_day, $expire_year);
            $today_timestamp = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
            $until_expire = ($expire_timestamp - $today_timestamp) / (60 * 60 * 24) + 1;
        }
    }
}

//タイムカード設定情報
$sql = "select return_icon_flg from timecard";

$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    //退勤復帰アイコンフラグ 1,"":退勤復帰 2:呼出出勤
    $return_icon_flg = pg_fetch_result($sel, 0, "return_icon_flg");
}
if ($return_icon_flg == "") {$return_icon_flg = "1";}

//アイコン情報
if ($return_icon_flg == "1") {
    $icon_o1_gif = "img/icon-o1.gif";
    $icon_o1_alt = "退復";
    $icon_o2_gif = "img/icon-o2.gif";
    $icon_o2_alt = "復退";
} else {
    $icon_o1_gif = "img/icon-yobishutsu.gif";
    $icon_o1_alt = "呼出";
    $icon_o2_gif = "img/icon-yobitai.gif";
    $icon_o2_alt = "呼退";

}

// include_js
include_js("js/fontsize.js");
include_js("js/popup.js");
include_js("js/prototype/dist/prototype.js");
?>
<script type="text/javascript">
function initPage() {
<?php
    //ログインIDの自動フォーカス
    if ($login_focus == "t") {
        //フォーカスフラグ（画面の状態） "t"or ""初期時はフォーカスする "f"はしない（IDからフォーカスがはずれた、別ボタンが押された場合等）
        if ($focus_flg != "f") {
?>
    if (document.loginform.id) {
        document.loginform.id.focus();
    }
<?php
        }
    }
?>
    if (!window.name) {
        window.name = 'comedix_main';
    }

    if (window.name != 'comedix_main') {
        window.open('login.php', 'comedix_main');
        self.close();
        return;
    }

<?php
if ($ext_search_show_flg == "t") {
    echo "setAtrbOptions();";
}
?>
}

function regTimecard(wherefrom) {
    document.loginform.wherefrom.value = wherefrom;
    document.loginform.action = 'clock_in_insert_from_login.php';
    document.loginform.submit();
}

function regPatternSelectTimecard() {
    document.loginform.action = 'clock_in_insert_from_login.php';
    document.loginform.submit();
}

function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, more, e) {
    var etc = (more > 0) ? '他'.concat(more).concat('件') : null;

    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e, etc
    );
}

function checkEmpty() {
    if (document.loginform.id.value == '') {
        document.loginform.id.focus();
        return false;
    }
    if (document.loginform.pass.value == '') {
        document.loginform.pass.focus();
        return false;
    }
    return true;
}

function readFelica() {
    var felica_anchor = document.getElementById('felica_anchor');
    if (!felica_anchor) {
        alert('プラグインがインストールされていないため利用できません。');
        return;
    }
    felica_anchor.click();
    var idm = document.loginform.idm.value;

    new Ajax.Request(
        'idm_check.php',
        {
            method: 'post',
            parameters: $H({idm: idm}).toQueryString(),
            onComplete: function (req) {
                var result = eval('('  + req.responseText + ')');
                if (result.id != '') {
                    document.loginform.id.value = result.id;
                    document.loginform.pass.value = result.pass;
                } else {
                    alert('職員情報の読み取りに失敗しました。');
                }
            }
        }
    );
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/layout.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
img.close {
    background-image:url("img/icon/plus.gif");
    vertical-align:middle;
}
img.open {
    background-image:url("img/icon/minus.gif");
    vertical-align:middle;
}
.block {border-collapse:collapse; border:<? echo($block_colors["border"]); ?> solid 1px;}
.block .header td {background-color:<? echo($block_colors["header"]); ?>}
.block .column td {background-color:<? echo($block_colors["column"]); ?>}
.block td, .block td .block td {border:<? echo($block_colors["border"]); ?> solid 1px;}
.block td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<form name="loginform" action="main_menu.php" method="post" onsubmit="return checkEmpty();">
<!-- header start -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="200" valign="bottom"><img src="img/main_title.gif" width="195" height="51" border="0"></td>
<td align="right" valign="bottom" style="padding-bottom:3px;">
<table border="0" cellspacing="0" cellpadding="3">
<tr>
<?php
if ($login_pos != "1") {
    show_logo($con, $fname);
    if ($login_pos == "3") {
        echo("<td width=\"200\"></td>\n");
    }
}
?>
<? if ($login_pos == "2") { ?>
<td align="right" valign="bottom" style="padding-left:35px;">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" bgcolor="#f6f9ff" style="padding:1px 15px;border:#5279a5 solid 1px;">
<table border="0" cellspacing="0" cellpadding="1">
<tr>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ログインID：</font></td>
<td><input type="text" name="id" size="14" maxlength="20" style="ime-mode:inactive;"<? if ($login_autocomplete == "f") {echo(" autocomplete=\"off\"");} ?>></td>
<td><? if ($ic_read_flg == "t") { ?>
<input type="button" value="ICカード" onclick="readFelica();">
<span id="FeliCa">
<!--
<a id="felica_anchor" href="felica:">
<input type="hidden" name="idm" />
<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm" />
</a>
-->
</span>
<? } ?></td>
</tr>
<tr>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">パスワード：</font></td>
<td><input type="password" name="pass" size="14" style="ime-mode:inactive;"<? if ($login_autocomplete == "f") {echo(" autocomplete=\"off\"");} ?>></td>
<td><input type="submit" value="ログイン"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<? } ?>
<?php
$date_label = date("Y") . "年" . intval(date("m")) . "月" . intval(date("d")) . "日（" . get_weekday_string() . "）";
echo("<td align=\"right\" valign=\"bottom\" style=\"padding-left:25px;\">\n");
echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
if ($timecard_use != "t" && $tmcd_chg_flg == "t") {
    echo("<tr>\n");
    echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><a href=\"login_timecard.php\">タイムカード</a></font></td>\n");
    echo("</tr>");
}
echo("<tr>\n");
echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$date_label</font></td>\n");
echo("</tr>");
if ($expire_warn == "t") {
    echo("<tr>\n");
    echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\" color=\"red\">利用停止まで{$until_expire}日</font></td>\n");
    echo("</tr>\n");
}
echo("</table>\n");
echo("</td>\n");
?>
</tr>
</table>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td background="img/line_3.gif"><img src="img/spacer.gif" width="1" height="7" alt=""></td>
</tr>
</table>
<!-- header end -->
<? if ($timecard_use == "t") { ?>
<table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="10"></td>
</tr>
<tr>
<td height="240" align="center" valign="middle" id="bg-time" style="padding:2px 15px 15px 15px;">
<iframe name="clock" src="login_timecard_clock.php" height="210" width="600" scrolling="no" frameborder="0">
</iframe>
</td>
</tr>
</table>
<? } else if ($title != "") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" style="padding:50px 15px 40px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" color="#5279a5" style="font-size:80px;"><b><? echo($title); ?></b></font></td>
</tr>
</table>
<? } ?>
<? if ($login_pos == "1") { ?>
<table align="center" width="700" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="5"></td>
</tr>
<tr>
<td height="73" align="center" valign="middle" id="bg-id">
<table width="77%" border="0" cellspacing="0" cellpadding="0">
<tr>
<? if ($ic_read_flg == "t") { ?>
<td>
<input type="button" value="ICカード" onclick="readFelica();">
<span id="FeliCa">
<!--
<a id="felica_anchor" href="felica:">
<input type="hidden" name="idm" />
<polling target="cTafzB6PR9ZGl7MmdfBDm37wfzoBABhLg6mKuzDiXFAH" result="idm" />
</a>
-->
</span>
</td>
<td width="2"></td>
<? } ?>
<td align="right" nowrap><span id="font90w">ID：</span></td>
<td><input type="text" name="id" value="" size="14" maxlength="20" style="font-size:1.3em;ime-mode:inactive;"<? if ($login_autocomplete == "f") {echo(" autocomplete=\"off\"");} ?> onFocus="document.loginform.focus_flg.value='t';" onBlur="document.loginform.focus_flg.value='f';"></td>
<td width="2"></td>
<td align="right" nowrap><span id="font90w">パスワード：</span></td>
<td><input type="password" name="pass" value="" size="14" style="font-size:1.3em;ime-mode:inactive;"<? if ($login_autocomplete == "f") {echo(" autocomplete=\"off\"");} ?>></td>
<td width="2"></td>
<td><input type="image" src="img/login.gif" alt="ログイン" width="45" height="45" onmouseover="this.src = 'img/login-on.gif';" onmouseout="this.src = 'img/login.gif';" onfocus="this.src = 'img/login-on.gif';" onblur="this.src = 'img/login.gif';"></td>
</tr>
</table>
</td>
</tr>
</table>
<? } ?>
<input type="hidden" name="wherefrom" value="">
<input type="hidden" name="doc_o" value="<? echo($doc_o); ?>">
<input type="hidden" name="doc_limit" value="<? echo($doc_limit); ?>">
<input type="hidden" name="select_group_id"  value="">
<input type="hidden" name="select_atdptn_id" value="">
<input type="hidden" name="focus_flg" value="">
<input type="hidden" name="hol_reg_flg" value="">
<input type="hidden" name="conf_ok_flg" value="">
<input type="hidden" name="mode" value="<?=$mode?>">
</form>
<? if ($timecard_use == "t") { ?>
<table align="center" width="<? echo($timecard_btn_width); ?>" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="5"></td>
</tr>
  <tr>
    <td height="99" width="700" align="center" id="bg-icon" nowrap>
    <a href="javascript:regTimecard(1);"><img src="img/icon-b1.gif" alt="出勤" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(4);"><img src="img/icon-b2.gif" alt="退勤" width="95" height="75" border="0"/></a>
<? if ($ret_btn_flg != "t") {
echo("　　　　　");
}
?>
<a href="javascript:regTimecard(2);"><img src="img/icon-g1.gif" alt="外出" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(3);"><img src="img/icon-g2.gif" alt="復帰" width="95" height="75" border="0"/></a>
<? if ($ret_btn_flg == "t") { ?>
　<a href="javascript:regTimecard(5);"><img src="<?=$icon_o1_gif?>" alt="<?=$icon_o1_alt?>" width="95" height="75" border="0"/></a>　<a href="javascript:regTimecard(6);"><img src="<?=$icon_o2_gif?>" alt="<?=$icon_o2_alt?>" width="95" height="75" border="0"/></a>
<?
}
?>
</td>
</tr>
</table>
<? } ?>
<? if ($text_show_flg == "t" || $expired) { ?>
<img src="img/spacer.gif" width="1" height="10"><br>
<? $width = ($ext_search_show_flg == "t" || $link_show_flg == "t" || $extension_show_flg == "t") ? 986 : 700; ?>
<table width="<? echo($width); ?>" align="center" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td style="padding:7px 10px 5px;<? if ($expired) {echo("color:red;");} ?>"><? echo($free_text); ?></td>
</tr>
</table>
<? } ?>
<? if ($event_show_flg == "t") { ?>
<img src="img/spacer.gif" width="1" height="10"><br>
<? $width = ($ext_search_show_flg == "t" || $link_show_flg == "t" || $extension_show_flg == "t") ? 986 : 700; ?>
<table id="event" width="<? echo($width); ?>" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><? show_event_block($con, $_REQUEST["event_date"], $fname, $mode); ?></td>
</tr>
</table>
<? } ?>
<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<?php
foreach ($center_blocks as $block_id) {
    if ($block_id == "201") {
        if ($area_show_flg == "t") {
            show_area_block($con, $fname);
        }
    }
    if ($block_id == "202") {
        if ($allot_show_flg == "t") {
            show_allot_block($con, $fname);
        }
    }
    if ($block_id == "203") {
        if ($welfare_show_flg == "t") {
            show_welfare_block($con, $fname);
        }
    }
    if ($block_id == "204") {
        if ($lib_show_flg == "t") {
            show_library_list($con, $doc_o, $doc_limit, $fname, $mode);
        }
    }
    if ($block_id == "205") {
        if ($qa_show_flg == "t") {
            show_qa_block($con, $qa_category_id, $question_id, $fname, $mode);
        }
    }
    if ($block_id == "206") {
        if ($stat_show_flg == "t") {
            show_stat_block($con, $stat_date, $fname, $mode);
        }
    }
    if ($block_id == "207") {
        if ($info_show_flg == "t") {
            show_menu_news($con, $fname);
        }
    }
    if ($block_id == "208") {
        if ($rsv_show_flg == "t") {
            show_reservation($con, $fname);
        }
    }
    if ($block_id == "209") {
        if ($cal_show_flg == "t" || $weather_show_flg == "t") {
            echo("<table align=\"center\" width=\"700\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
            echo("<tr valign=\"top\">\n");
        }
        if ($cal_show_flg == "t") {
            $width = ($weather_show_flg == "t") ? " width=\"60%\"" : "";
            echo("<td$width>\n");
            show_calendar_block($con, $cal_start_wd, $fname);
            echo("</td>\n");
        }
        if ($cal_show_flg == "t" && $weather_show_flg == "t") {
            echo("<td width=\"10\"></td>\n");
        }
        if ($weather_show_flg == "t") {
            echo("<td>\n");
            show_weather_block($weather_area);
            echo("</td>\n");
        }
        if ($cal_show_flg == "t" || $weather_show_flg == "t") {
            echo("</tr>\n");
            echo("</table>\n");
        }
    }
    if ($block_id == "210") {
        if ($wic_show_flg == "t") {
            show_wic_block();
        }
    }
}
?>
</td>
<? if ($ext_search_show_flg == "t" || $link_show_flg == "t" || $extension_show_flg == "t") { ?>
<td><img src="img/spacer.gif" alt="" width="10" height="1"></td>
<td>
<?php
foreach ($right_blocks as $block_id) {
    if ($block_id == "301") {
        if ($ext_search_show_flg == "t") {
            show_ext_search_block($con, $fname);
        }
    }
    if ($block_id == "302") {
        if ($link_show_flg == "t") {
            show_link_block($con, $fname);
        }
    }
    if ($block_id == "303") {
        if ($extension_show_flg == "t") {
            show_extension_block($con, $fname);
        }
    }
}
?>
</td>
<? } ?>
</tr>
</table>
<!-- footer start -->
<img src="img/spacer.gif" width="1" height="50" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2"></td>
</tr>
<tr>
<td height="18" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_copyright($con, $fname); ?></font></td>
</tr>
</table>
<!-- footer end -->
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?php
function get_weekday_string() {
    $w = date("w");
    switch ($w) {
    case 1:
        return "月";
    case 2:
        return "火";
    case 3:
        return "水";
    case 4:
        return "木";
    case 5:
        return "金";
    case 6:
        return "土";
    case 0:
        return "日";
    }
}

function show_logo($con, $fname) {

    define(LOGO_TYPE_IAMGE, "1");
    define(LOGO_TYPE_TEXT, "2");
    define(LOGO_MAX_WIDTH, 150);
    define(LOGO_MAX_HEIGHT, 50);

    // ロゴ情報を取得
    $sql = "select prf_name, prf_url, prf_logo_type from profile";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        $prf_name = pg_fetch_result($sel, 0, "prf_name");
        $url = pg_fetch_result($sel, 0, "prf_url");
        $logo_type = pg_fetch_result($sel, 0, "prf_logo_type");
    } else {
        $prf_name = "";
        $url = "";
        $logo_type = "";
    }

    switch ($logo_type) {
    case LOGO_TYPE_IAMGE:
        $arr_logo_file = glob("profile/img/logo.*");
        if (count($arr_logo_file) == 0) {
            return;
        }

        $logo_img_src = $arr_logo_file[0];
        $logo_info = getimagesize($logo_img_src);
        $width = $logo_info[0];
        $height = $logo_info[1];
        if ($width > LOGO_MAX_WIDTH) {
            $height = round($height / ($width / LOGO_MAX_WIDTH));
            $width = LOGO_MAX_WIDTH;
        }
        if ($height > LOGO_MAX_HEIGHT) {
            $width = round($width / ($height / LOGO_MAX_HEIGHT));
            $height = LOGO_MAX_HEIGHT;
        }

        $align = "right";
        $logo_html = "<img src=\"$logo_img_src\" alt=\"$prf_name\" width=\"$width\" height=\"$height\" border=\"0\">";
        break;

    case LOGO_TYPE_TEXT:
        if ($prf_name == "") {
            return;
        }

        $align = "right";
        $logo_html = "<font face=\"ＭＳ Ｐゴシック, Osaka\"><b><span style=\"font-size:140%;\">$prf_name</span></b></font>";
        break;

    default:
        return;
    }

    echo("<td width=\"165\" align=\"$align\" valign=\"bottom\" style=\"padding-bottom:3px;\">");
    if ($url != "") {
        echo("<a href=\"$url\" target=\"_blank\">$logo_html</a>");
    } else {
        echo($logo_html);
    }
    echo("</td>");
}
