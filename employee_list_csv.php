<?php
ob_start();

require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 職員一覧をCSV形式で取得
$csv = get_emp_list_csv($con, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $view, $emp_o);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "list.csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 情報をCSV形式で取得
function get_emp_list_csv($con, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $view, $emp_o) {

    // 組織情報を取得
    require_once("show_class_name.ini");
    $arr_class_name = get_class_name_array($con, $fname);
    $class_cnt = $arr_class_name["class_cnt"];

    // 組織タイプを取得
    require_once("label_by_profile_type.ini");
    $profile_type = get_profile_type($con, $fname);

    // ヘッダ行を設定
    $buf = "職員ID,苗字（漢字）,名前（漢字）,苗字（かな）,名前（かな）,";
    $buf .= $arr_class_name["class_nm"] . ",";
    $buf .= $arr_class_name["atrb_nm"] . ",";
    $buf .= $arr_class_name["dept_nm"] . ",";
    if ($class_cnt == 4) {
        $buf .= $arr_class_name["room_nm"] . ",";
    }
    $buf .= "役職,職種,外線番号,内線番号,";
    $buf .= $_label_by_profile["PHS"][$profile_type] . ",";
    $buf .= "電子メール,利用停止フラグ,権限グループ,表示グループ,メニューグループ";
    if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
        $buf .= ",氏名連携不可フラグ,部署連携不可フラグ,役職連携不可フラグ,職種連携不可フラグ";
    }
    $buf .= "\r\n";

    // 職員情報を取得
    $del_flg = ($view == "1") ? "f" : "t";
    $sql = "
    select
        empmst.emp_personal_id,
        empmst.emp_lt_nm, empmst.emp_ft_nm,
        empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm,
        classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm,
        stmst.st_nm, jobmst.job_nm,
        authgroup.group_nm, dispgroup.group_nm as disp_group_nm, menugroup.group_nm as menu_group_nm,
        emp_relation.emp_name_deadlink_flg, emp_relation.emp_class_deadlink_flg, emp_relation.emp_job_deadlink_flg, emp_relation.emp_st_deadlink_flg,
        empmst.emp_mobile1, empmst.emp_mobile2, empmst.emp_mobile3,
        empmst.emp_ext, empmst.emp_phs, empmst.emp_email2
        from empmst
        inner join classmst  on empmst.emp_class     = classmst.class_id
        inner join atrbmst   on empmst.emp_attribute = atrbmst.atrb_id
        inner join deptmst   on empmst.emp_dept      = deptmst.dept_id
        inner join stmst     on empmst.emp_st        = stmst.st_id
        inner join jobmst    on empmst.emp_job       = jobmst.job_id
        left  join classroom on empmst.emp_room      = classroom.room_id
        inner join authmst using (emp_id)
        left  outer join authgroup on authmst.group_id       = authgroup.group_id
        left  join option using(emp_id)
        left  outer join dispgroup on option.disp_group_id   = dispgroup.group_id
        left  outer join menugroup on option.menu_group_id   = menugroup.group_id
        left  outer join emp_relation on emp_relation.emp_id = empmst.emp_id
    ";

    $cond = "where exists (select * from authmst where authmst.emp_del_flg = '$del_flg' and authmst.emp_id = empmst.emp_id)";
    if ($class_cond != 0) {
        $cond .= " and ((emp_class = $class_cond) or (exists (select * from concurrent where concurrent.emp_class = $class_cond and concurrent.emp_id = empmst.emp_id)))";
    }
    if ($atrb_cond != 0) {
        $cond .= " and ((emp_attribute = $atrb_cond) or (exists (select * from concurrent where concurrent.emp_attribute = $atrb_cond and concurrent.emp_id = empmst.emp_id)))";
    }
    if ($dept_cond != 0) {
        $cond .= " and ((emp_dept = $dept_cond) or (exists (select * from concurrent where concurrent.emp_dept = $dept_cond and concurrent.emp_id = empmst.emp_id)))";
    }
    if ($room_cond != 0) {
        $cond .= " and ((emp_room = $room_cond) or (exists (select * from concurrent where concurrent.emp_room = $room_cond and concurrent.emp_id = empmst.emp_id)))";
    }
    if ($job_cond != 0) {
        $cond .= " and emp_job = $job_cond";
    }
    if ($emp_o == "1") {
        $cond .= " order by emp_personal_id";
    } else if ($emp_o == "2") {
        $cond .= " order by emp_personal_id desc";
    } else if ($emp_o == "3") {
        $cond .= " order by emp_keywd";
    } else {
        $cond .= " order by emp_keywd desc";
    }
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ボディ行を設定
    while ($row = pg_fetch_array($sel)) {
        $buf .= $row["emp_personal_id"] . ",";
        $buf .= $row["emp_lt_nm"] . ",";
        $buf .= $row["emp_ft_nm"] . ",";
        $buf .= $row["emp_kn_lt_nm"] . ",";
        $buf .= $row["emp_kn_ft_nm"] . ",";
        $buf .= $row["class_nm"] . ",";
        $buf .= $row["atrb_nm"] . ",";
        $buf .= $row["dept_nm"] . ",";
        if ($class_cnt == 4) {
            $buf .= $row["room_nm"] . ",";
        }
        $buf .= $row["st_nm"] . ",";
        $buf .= $row["job_nm"] . ",";
        if ($row["emp_mobile1"] != "" && $row["emp_mobile2"] != "" && $row["emp_mobile3"] != "") {
            $buf .= $row["emp_mobile1"] . "-" . $row["emp_mobile2"] . "-" . $row["emp_mobile3"] . ",";
        } else {
            $buf .= ",";
        }
        $buf .= $row["emp_ext"] . ",";
        $buf .= $row["emp_phs"] . ",";
        $buf .= $row["emp_email2"] . ",";
        $buf .= $del_flg . ",";
        $buf .= $row["group_nm"] . ",";
        $buf .= $row["disp_group_nm"] . ",";
        $buf .= $row["menu_group_nm"];
        if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
            $buf .= "," . $row["emp_name_deadlink_flg"];
            $buf .= "," . $row["emp_class_deadlink_flg"];
            $buf .= "," . $row["emp_st_deadlink_flg"];
            $buf .= "," . $row["emp_job_deadlink_flg"];
        }
        $buf .= "\r\n";
    }

    // Shift_JISに変換
    return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
