<?php
require_once('about_comedix.php');
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

// CmxSystemConfigインスタンス生成
$sysConf = new Cmx_SystemConfig();

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//====================================
// 登録ボタン押下時DB更新
//====================================
if($postback_mode == "send_change")
{
	// トランザクションの開始
	pg_query($con, "begin transaction");

    //評価機能利用フラグ
    $sysConf->set('fantol.old.mail.already.read.flg', $older_mail_read_flg == 't' ? 't' : 'f');
    
    //報告書を編集、評価時、送信者本人をメール送信の宛先に含めない
    $sysConf->set('fantol.exclude.mail.sender.flg', $exclude_mail_sender_flg == 't' ? 't' : 'f');
    
	// トランザクションをコミット
	pg_query($con, "commit");
}

//====================================
// 初期化処理
//====================================
//評価機能利用フラグ
$older_mail_read_flg = $sysConf->get('fantol.old.mail.already.read.flg') == 't';
//報告書を編集、評価時、送信者本人をメール送信の宛先に含めない 20141008
$exclude_mail_sender_flg = $sysConf->get('fantol.exclude.mail.sender.flg') == 't';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <title>CoMedix <?=$INCIDENT_TITLE?> | その他</title>
    <script type="text/javascript" src="js/fontsize.js"></script>
    <script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <style type="text/css">
        .list {border-collapse:collapse;}
        .list td {border:#35B341 solid 1px;}
        .list_2 {border-collapse:collapse;}
        .list_2 td {border:#999999 solid 1px;}
    </style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <!-- ヘッダー START -->
                <?
                show_hiyari_header($session,$fname,true);
                ?>
                <!-- ヘッダー END -->
                <img src="img/spacer.gif" width="1" height="5" alt=""><br>

                <form name="mainform" action='' mode="post">
                    <input type="hidden" name="postback_mode" value="send_change">
                    <input type="hidden" name="session" value="<?=$session?>">

                    <table border="0" cellspacing="0" cellpadding="0" width="60%" >
                        <tr>
                            <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
                            <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                            <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
                        </tr>
                        <tr>
                            <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                            <td bgcolor="#F5FFE5">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <input type="checkbox" value="t" name="older_mail_read_flg" <?if($older_mail_read_flg){?>checked<?}?>>
                                                メッセージ表示画面を開いた際、古い同一事案番号のメッセージを既読にする。
                                            </font>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <input type="checkbox" value="t" name="exclude_mail_sender_flg" <?if($exclude_mail_sender_flg){?>checked<?}?>>
                                                報告書を編集、評価時、送信者本人をメール送信の宛先に含めない。
                                            </font>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="right">
                                            <br><input type="submit" value="登録">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
                        </tr>
                        <tr>
                            <td><img src="img/r_3.gif" width="10" height="10"></td>
                            <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
                            <td><img src="img/r_4.gif" width="10" height="10"></td>
                        </tr>
                    </table>
                </form>

                <!-- フッター START -->
                <?
                show_hiyari_footer($session,$fname,true);
                ?>
                <!-- フッター END -->

            </td>
        </tr>
    </table>
</body>
</html>
<?
pg_close($con);
