<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require("show_patient_next.ini");
require_once("summary_common.ini");
require_once("get_values.ini");
require_once("sot_util.php");
require("label_by_profile_type.ini");
require_once("sum_exist_workflow_check_exec.ini");

if (!@$search_mode) $search_mode = (!$is_workflow_exist ? "1" : "");

//====================================
// 初期化処理
//====================================
if (!$search_mode){

  $date_y1 = ($_REQUEST["date_y1"] != "") ? $_REQUEST["date_y1"] : "" ;
  $date_m1 = ($_REQUEST["date_m1"] != "") ? $_REQUEST["date_m1"] : "" ;
  $date_d1 = ($_REQUEST["date_d1"] != "") ? $_REQUEST["date_d1"] : "" ;

  // 送信日セット(本日日付から過去２週間分)
  // 初期表示(お知らせから遷移した場合)
  // if ($date_y1 == "" and $date_m1 == "" and $date_d1 == ""){
  if (!$date_y1 and !$date_m1 and !$date_d1){

  // ２週間前の日付取得
  $two_weeks_ago = date("Y/m/d",strtotime("-2 week"));
  $arr_two_weeks_ago = split("/", $two_weeks_ago);
  $date_y1 = $arr_two_weeks_ago[0] ;
  $date_m1 = $arr_two_weeks_ago[1] ;
  $date_d1 = $arr_two_weeks_ago[2] ;

  }

  // 本日の日付
  $today = date("Y/m/d");
  $arr_today = split("/", $today);
  $date_y2 = $arr_today[0];
  $date_m2 = $arr_today[1];
  $date_d2 = $arr_today[2];


}


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

$emp_id = get_emp_id($con, $session, $fname);

summary_common_write_operate_log("access", $con, $fname, $session, $emp_id, "", "");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <? echo ($patient_title); ?>検索</title>
<?

require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");

// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
if (!$search_mode){
  write_yui_calendar_script2(1);
} else {
  write_yui_calendar_script2(2);
}

$initcal = ($search_mode) ? " onload='initcal();'" : " onload='initcal();'" ;
  ?>
</head>
<body<?=$initcal?>>


<? $function_mode = $search_mode ? $patient_title."検索" : "お知らせ"; ?>

<? summary_common_show_sinryo_top_header($session, $fname, $function_mode); ?>

<? summary_common_show_main_tab_menu($con, $fname, $function_mode, $is_workflow_exist); ?>




<? if (!$search_mode){ ?>
<?
//**********************************************************************************************************************
// １of２ お知らせ画面
//**********************************************************************************************************************

/*
//------------------------------------
// データパッチ 2010/4/14仕込み、システムで一回通ればよい。
//------------------------------------
$vlist = array();

$sql = "select count(*) from sum_apply where visibility_apv_order is null";
$sel = select_from_table($con, $sql, "", $fname);

if (pg_fetch_result($sel, 0, 0) > 0) {

	// 同報なら、階層数とする
	$sql =
		" select sum_applyapv.apply_id, max(apv_order) as mo from sum_applyapv".
		" inner join sum_apply on (sum_apply.apply_id = sum_applyapv.apply_id)".
		" inner join sum_wkfwmst on (sum_wkfwmst.wkfw_id = sum_apply.wkfw_id and sum_wkfwmst.wkfw_appr = 1)".
		" group by sum_applyapv.apply_id";
	$sel = select_from_table($con, $sql, "", $fname);
	while($row = pg_fetch_array($sel)){
		$vlist[$row["apply_id"]] = $row["mo"];
		$sqlu = "update sum_apply set visibility_apv_order = ".(int)$row["mo"]." where apply_id = ".(int)$row["apply_id"];
		$ret = update_set_table($con, $sqlu, array(), null, "", $fname);
	}

	$vlist = array();

	// 稟議回覧の最大階層一覧を取得する
	$sql =
		" select * from sum_applyapv".
		" inner join sum_apply on (sum_apply.apply_id = sum_applyapv.apply_id)".
		" inner join sum_wkfwmst on (sum_wkfwmst.wkfw_id = sum_apply.wkfw_id and sum_wkfwmst.wkfw_appr = 2)".
		" order by sum_apply.apply_id, sum_applyapv.apv_order";
	$sel = select_from_table($con, $sql, "", $fname);
	$all = pg_fetch_all($sel);

	$non_accepted_all = array();
	$non_operated_all = array();
	foreach($all as $idx => $row){
		if (!$row["apv_stat_accepted"]) $non_accepted_all[$row["apply_id"]][$row["apv_order"]] = "yes";
		if (!$row["apv_stat_operated"]) $non_operated_all[$row["apply_id"]][$row["apv_order"]] = "yes";
	}

	foreach($all as $idx => $row){
		// 受付で次階層の場合
		if ($row["next_notice_recv_div"]=="1"){
			// 未受付なら、最大値は１
			if ($row["apv_stat_accepted"]!="1"){
				$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], 1);
			}
			// 受付済みなら
			else {
				// 「非同期か並列」の場合は、最大値はapv_orderの次の階層まで
				if ($row["next_notice_div"]=="1" || $row["next_notice_div"]=="3"){
					$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]+1);
				}
				// 「同期」の場合
				else {
					// 同一階層すべて選択されているときは、次の階層まで選択可能
					if (@$non_accepted_all[$row["apply_id"]][$row["apv_order"]]){
						$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]);
					} else {
						$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]+1);
					}
				}
			}
		}
		// 実施で次階層の場合
		if ($row["next_notice_recv_div"]=="2"){
			// 未実施なら、最大値は１
			if ($row["apv_stat_operated"]!="1"){
				$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], 1);
			}
			// 実施済みなら、最大値はapv_order
			else {
				// 「非同期か並列」の場合は、最大値はapv_orderの次の階層まで
				if ($row["next_notice_div"]=="1" || $row["next_notice_div"]=="3"){
					$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]+1);
				}
				// 「同期」の場合
				else {
					// 同一階層すべて選択されているときは、次の階層まで選択可能
					if (@$non_operated_all[$row["apply_id"]][$row["apv_order"]]){
						$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]);
					} else {
						$vlist[$row["apply_id"]] = max(@$vlist[$row["apply_id"]], $row["apv_order"]+1);
					}
				}
			}
		}
	}

	foreach ($vlist as $apply_id => $cnt){
		$sqlu = "update sum_apply set visibility_apv_order = ".(int)$cnt." where apply_id = ".(int)$apply_id;
		$ret = update_set_table($con, $sqlu, array(), null, "", $fname);
	}
}
//------------------------------------
// データパッチ 2010/4/14ここまで
//------------------------------------
*/



$nextday = "0".((int)@$date_d2+1);
$nextday = substr($nextday, strlen($nextday)-2, strlen($nextday));

//==========================================================
// 参考値1of4 否認を行ったオーダと階層を取得
// ただし、他者による否認を含む。
//==========================================================
$sql =
	" select distinct sahs.apply_id, sahs.from_apv_order".
	"   from sum_apply_hinin_sasimodosi sahs".
	"   inner join sum_apply ap on ap.apply_id = sahs.apply_id".
	"   inner join sum_wkfwmst wk on wk.wkfw_id = ap.wkfw_id".
	"   where sahs.hinin_or_sasimodosi = 'hinin'".                           // "hinin"：否認した
	"   and sahs.update_ymdhm is null".
//	"   and sahs.from_emp_id = '".pg_escape_string($emp_id)."'".
	"   and ap.apply_stat = '1'".                                            // 送信状態 1：送信中
	"   and ap.delete_flg = 'f'";                                            // 削除フラグ

if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
  $sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
$sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";

$sql .=
	"   and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	"   and wk.is_hide_approve_list = 0";

$sel = select_from_table($con, $sql, "", $fname);
$hinin_list = array();
while($row = pg_fetch_array($sel)){
	$hinin_list[$row["apply_id"]][$row["from_apv_order"]] = "yes";
}

//==========================================================
// 参考値2of4 差戻しを行って、差戻し対象が修正していないオーダと階層を取得
//==========================================================
$sql =
	" select distinct sast.apply_id, sast.from_apv_order".
	"   from sum_apply_sasimodosi_target sast".
	"   inner join sum_apply ap on ap.apply_id = sast.apply_id".
	"   inner join sum_wkfwmst wk on wk.wkfw_id = ap.wkfw_id".
	"   where sast.update_ymdhm is null".
//	"   and sast.from_emp_id = '".pg_escape_string($emp_id)."'".
	"   and ap.apply_stat = '1'".                                            // 送信状態 1：送信中
	"   and ap.delete_flg = 'f'";                                            // 削除フラグ

if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
  $sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
$sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";

$sql .=
	"   and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	"   and wk.is_hide_approve_list = 0";

$sel = select_from_table($con, $sql, "", $fname);
$sasimodosi_list = array();
while($row = pg_fetch_array($sel)){
	$sasimodosi_list[$row["apply_id"]][$row["from_apv_order"]] = "yes";
}

//==========================================================
// 参考値3of4 差戻しを食らったままのオーダと階層を取得
//==========================================================
$date_from = "$date_y1$date_m1$date_d1";
$date_to = "$date_y2$date_m2$nextday";
$sql = "select count(*) from sum_apply ap inner join sum_applyapv apv on (apv.apply_id = ap.apply_id and apv.emp_id = '$emp_id' and apv.apv_order <= ap.visibility_apv_order) inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_approve_list = 0) inner join summary su on (su.summary_id = ap.summary_id and su.ptif_id = ap.ptif_id and su.emp_id = ap.emp_id) left join (select apply_id, to_apv_order, count(apply_id) as sasimodosare_count from sum_apply_sasimodosi_target where to_emp_id = '$emp_id' and update_ymdhm is null group by apply_id, to_apv_order) sast on (sast.apply_id = apv.apply_id and sast.to_apv_order = apv.apv_order) where ap.apply_stat = '1' and ap.delete_flg = 'f' and ap.apply_date >= '$date_from' and ap.apply_date < '$date_to' and sast.sasimodosare_count > 0;";
$sel = select_from_table($con, $sql, "", $fname);
$approve_sasimodosare_count = (int)@pg_fetch_result($sel, 0, 0);


//==========================================================
// 参考値4of4 差戻しを食らったままのオーダと階層を取得
// ただし、他者に対する差戻しを含む。
// また、代理送信している場合も含める必要がある
//==========================================================
$sql =
	" select count(*) from (".
	" select distinct sast.apply_id, sast.from_apv_order".
	"   from sum_apply_sasimodosi_target sast".
	"   inner join sum_apply ap on ap.apply_id = sast.apply_id".
	"   inner join sum_wkfwmst wk on wk.wkfw_id = ap.wkfw_id".
	"     where sast.update_ymdhm is null".
//	"     and sast.to_emp_id = ap.emp_id".
//	"     and to_apv_order = 0".
	"     and ap.apply_stat = 1".                                              // 送信状態 1：送信中
	"     and ap.delete_flg = 'f'";                                            // 削除フラグ

if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
  $sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
$sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";

$sql .=
	"     and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	"     and (".
	"       (ap.emp_id = '".pg_escape_string($emp_id)."')". // 本人か
	"       or".
	"       (ap.dairi_emp_id = '".pg_escape_string($emp_id)."' and dairi_kakunin_ymdhms is null)". // 代理送信したか
	"     )".
	"   and wk.is_hide_approve_list = 0".
	" ) d";

$sel = select_from_table($con, $sql, "", $fname);
$apply_sasimodosare_count = (int)@pg_fetch_result($sel, 0, 0);


//==========================================================
// 受信操作を行っていないオーダと階層を取得
//==========================================================
$sql =
	" select".
	" apv.apply_id, apv.apv_order, apv.next_notice_recv_div, wk.wkfw_appr".
	",apv.apv_stat_accepted, apv.apv_stat_operated, apv.apv_stat_accepted_by_other, apv.apv_stat_operated_by_other".
	" from sum_applyapv apv".
	" inner join sum_apply ap on (".
	"   ap.apply_id = apv.apply_id".
	"   and ap.apply_stat = 1".
	"   and ap.delete_flg = 'f'".
	"   and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	" )".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_approve_list = 0)".
	" where apv.emp_id = '" . $emp_id ."'".
	" and (".
	"      (apv.next_notice_recv_div = 1 and (apv.apv_stat_accepted is null or apv.apv_stat_accepted = 0))". // 受信していない
	"   or (apv.next_notice_recv_div = 2 and (apv.apv_stat_operated is null or apv.apv_stat_operated = 0))". // 実施していない
	" )".
	" and (".
	"      (apv.next_notice_recv_div = 1 and (apv.apv_stat_accepted_by_other is null or apv.apv_stat_accepted_by_other = 0))". // 他者による受信もしていない
	"   or (apv.next_notice_recv_div = 2 and (apv.apv_stat_operated_by_other is null or apv.apv_stat_operated_by_other = 0))". // 他者による実施もしていない
	" )".
	" and apv.delete_flg = 'f'".
	" and (ap.wkfw_appr = '1' or apv.apv_order = ap.visibility_apv_order)"; // 同報か、あるいは稟議回覧なら受信実施できる階層か

if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
  $sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
$sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";

$selall = select_from_table($con, $sql, "", $fname);

$sql = "select count(*) from sum_apply ap inner join sum_applyapv apv on (apv.apply_id = ap.apply_id and apv.emp_id = '$emp_id' and apv.apv_order <= ap.visibility_apv_order) inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_approve_list = 0) inner join summary su on (su.summary_id = ap.summary_id and su.ptif_id = ap.ptif_id and su.emp_id = ap.emp_id) left join sum_apply_hinin_sasimodosi sahs on (sahs.apply_id = apv.apply_id and sahs.from_apv_order = apv.apv_order and sahs.update_ymdhm is null) where ap.apply_stat = '1' and ap.delete_flg = 'f' and ap.apply_date >= '$date_from' and ap.apply_date < '$date_to' and (apv.apv_stat_accepted is null or apv.apv_stat_accepted = '0') and (apv.apv_stat_accepted_by_other is null or apv.apv_stat_accepted_by_other = '0') and sahs.from_emp_id is null;";
$sel = select_from_table($con, $sql, "", $fname);
$approve_non_accepted_count = (int)@pg_fetch_result($sel, 0, 0);

$approve_non_operated_count = 0;

// 受信実施を行っていないレコードのうち、
// 否認も差戻しもやっていないものの数を取得
// オーダ数でなく、オーダｘ階層数としていることに注意
$order_list = array();
while($row = pg_fetch_array($selall)){
	if (@$hinin_list[$row["apply_id"]][$row["apv_order"]]) continue;
	if (@$sasimodosi_list[$row["apply_id"]][$row["apv_order"]]) continue;
	if (@$order_list[$row["apply_id"]]) continue; // 重複階層は除外
	$order_list[$row["apply_id"]] = 1;

	if ($row["next_notice_recv_div"]=="2" && !$row["apv_stat_operated"] && !$row["apv_stat_operated_by_other"] ) $approve_non_operated_count++;
}



//==========================================================
// 送信したオーダ数のうち、未受付のものを取得。代理送信を含む。
//==========================================================
$apply_non_accepted_count = 0;
$apply_non_operated_count = 0;
$sql =
	" select count(*) from (".
	" select ap.apply_id, count(ap.apply_id) from sum_apply ap".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_apply_list = 0)". // ワークフローにて非通知に設定されていないもの
	" inner join sum_applyapv apv on (apv.apply_id = ap.apply_id)".
	" where ap.apply_stat = '1' and ap.delete_flg = 'f'". // 送信済みで、未削除/履歴でないもの
	" and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	" and (apv.apv_stat_accepted is null or apv.apv_stat_accepted = 0)". // 受付未登録
	" and (apv.apv_stat_accepted_by_other is null or apv.apv_stat_accepted_by_other = 0)". // 他者による受付も未登録
//	" and (wk.wkfw_appr = '1' or apv.apv_order = ap.visibility_apv_order)". // 同報か、あるいは稟議回覧なら受信実施できる階層か
	" and (".
	"   (ap.emp_id = '".pg_escape_string($emp_id)."')". // 自分で送信したか
	"   or".
	"   (ap.dairi_emp_id = '".pg_escape_string($emp_id)."' and dairi_kakunin_ymdhms is null)". // 代理送信したか
    " )";

if (strlen(@$date_y1.@$date_m1.@$date_d1) == 8) { // 送信日(from)
  $sql .= " and ap.apply_date >= '".$date_y1.$date_m1.$date_d1."'";
}
$sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";

$sql .=
	" group by ap.apply_id".
	" ) d";

$sel = select_from_table($con, $sql, "", $fname);
$apply_non_accepted_count = (int)@pg_fetch_result($sel, 0, 0);



//==========================================================
// 送信したオーダ数のうち、未実施のものを取得。代理送信を含む。
//==========================================================
$sql =
	" select count(*) from (".
	" select ap.apply_id, count(ap.apply_id) from sum_apply ap".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_apply_list = 0)". // ワークフローにて非通知に設定されていないもの
	" inner join sum_applyapv apv on (apv.apply_id = ap.apply_id)".
	" where ap.apply_stat = '1' and ap.delete_flg = 'f'". // 送信済みで、未削除/履歴でないもの
	" and (ap.is_approve_complete is null or ap.is_approve_complete = 0)". // 完結していないオーダ
	" and next_notice_recv_div = 2". // 「実施済みで次階層」のもの
	" and (apv.apv_stat_operated is null or apv.apv_stat_operated = 0)". // 実施未登録
	" and (apv.apv_stat_operated_by_other is null or apv.apv_stat_operated_by_other = 0)". // 他者による実施も未登録
//	" and (wk.wkfw_appr = '1' or apv.apv_order = ap.visibility_apv_order)". // 同報か、あるいは稟議回覧なら受信実施できる階層か
	" and (".
	"   (ap.emp_id = '".pg_escape_string($emp_id)."')". // 自分で送信したか
	"   or".
	"   (ap.dairi_emp_id = '".pg_escape_string($emp_id)."' and dairi_kakunin_ymdhms is null)". // 代理送信したか
	" )".
	" group by ap.apply_id".
	" ) d";

$sel = select_from_table($con, $sql, "", $fname);
$apply_non_operated_count = (int)@pg_fetch_result($sel, 0, 0);



$apply_non_complete_checking = 0;
$sql =
	" select count(apply_id) from sum_apply ap".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_apply_list = 0)".
	" where ap.apply_stat = '1' and ap.delete_flg = 'f'".
	" and (ap.is_approve_complete = 1 and approve_complete_check_ymdhms is null)". // 完結していないオーダ
	" and (".
	"   (ap.emp_id = '".pg_escape_string($emp_id)."')". // 自分が送信したか
	"   or".
	"   (ap.dairi_emp_id = '".pg_escape_string($emp_id)."' and dairi_kakunin_ymdhms is null)". // 他者が送信したか
	" )";
$sel = select_from_table($con, $sql, "", $fname);
$apply_non_complete_checking = (int)@pg_fetch_result($sel, 0, 0);


?>
<div style="background-color:#F4F4F4; padding-top:1px; padding-bottom:1px; border-top:1px solid #d8d8d8; text-align:right">
<script type="text/javascript">
function newslist_search() {
  document.newslist.action="summary_menu.php";
  document.newslist.submit();
}
</script>
</div>

<form name="newslist" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="search_mode" value="">
<table class="list_table" cellspacing="1">
<tr>
  <td>
  <table>
		<tr>
	  <th bgcolor="#fefcdf">送信日</th>
	  <td>
		<table>
		<tr>
			<td>
				<select id="date_y1" name="date_y1"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1"><? show_select_days($date_d1, true); ?></select>
			</td>
			<td>
			  <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
			  <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
			</td>
		</tr>
	</table>
	</td>
	<td width="6">
	</td>
	<td>〜
	</td>
	<td width="6">
	</td>
	<td>
    <div class="search_button_div" style="border-bottom:1px solid #fdfdfd">
	  <input type="button" onclick="newslist_search();" value="検索"/>
    </div>
	</td>
</tr>
</table>
</form>
<table width="100%"><tr>
  <td height="6" valign="top" bgcolor="#F4F4F4" style="background-color:#f4f4f4; padding:1px"><table width="100%">
    <tr class="bg-mail" height="157">
      <td align="center" style="border:1px solid #BAD9F2; padding:0px 10px;"><table width="100%">
        <tr>
          <td width="81%" align="left"><span style="font-size:16px; font-weight:bold">&lt;受信一覧&gt;</span><br />
						<? if ($approve_non_accepted_count){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_approve_list.php?session=<?=$session?>&date_y1=<?=$date_y1?>&date_m1=<?=$date_m1?>&date_d1=<?=$date_d1?>&date_y2=<?=$date_y2?>&date_m2=<?=$date_m2?>&date_d2=<?=$date_d2?>&apply_date_default=off&apv_stat_accepted=0" class="always_blue">
								未受付のオーダが<?=$approve_non_accepted_count?>件あります。</a>
							</div>
						<? } ?>
						<? if ($approve_non_operated_count){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_approve_list.php?session=<?=$session?>&apply_date_default=on2&apv_stat_operated=0" class="always_blue">
								未実施のオーダが<?=$approve_non_operated_count?>件あります。</a>
							</div>
						<? } ?>
						<? if ($approve_sasimodosare_count){ ?>
							<div style="padding:5px; background-color:#fffca0; border:2px solid #ff77bb; width:300px; margin-top:10px">
								<a href="sum_application_approve_list.php?session=<?=$session?>&apply_date_default=on2&sel_sasimodosare=1" class="always_blue">
								差し戻し通知オーダが<?=$approve_sasimodosare_count?>件あります。</a>
								</div>
						<? } ?>
						<? if (!$approve_non_accepted_count && !$approve_non_operated_count && !$approve_sasimodosare_count){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_approve_list.php?session=<?=$session?>&apply_date_default=on" class="always_blue">
								未受付・未実施のオーダはありません。</a>
							</div>
						<? } ?>
          </td>
          <td width="19%" align="right"><img src="img/mail-jyushin.gif" alt="" width="137" height="147" /></td>
        </tr>
      </table></td>
      </tr>
    <tr class="bg-mail" height="157">
      <td align="center" style="border:1px solid #BAD9F2; padding:0px 10px;"><table width="100%">
        <tr>
          <td width="81%" align="left"><span style="font-size:16px; font-weight:bold">&lt;送信一覧&gt;</span><br />
						<? if ($apply_non_accepted_count){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_apply_list.php?session=<?=$session?>&date_y1=<?=$date_y1?>&date_m1=<?=$date_m1?>&date_d1=<?=$date_d1?>&date_y2=<?=$date_y2?>&date_m2=<?=$date_m2?>&date_d2=<?=$date_d2?>&apply_date_default=off&apply_stat=non_accepted" class="always_blue">
								未受付のオーダが<?=$apply_non_accepted_count?>件あります。</a>
							</div>
						<? } ?>
						<? if ($apply_non_operated_count){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_apply_list.php?session=<?=$session?>&apply_date_default=on2&apply_stat=non_operated" class="always_blue">
								未実施のオーダが<?=$apply_non_operated_count?>件あります。</a>
							</div>
						<? } ?>
						<? if ($apply_sasimodosare_count){ ?>
							<div style="padding:5px; background-color:#fffca0; border:2px solid #ff77bb; width:300px; margin-top:10px">
								<a href="sum_application_apply_list.php?session=<?=$session?>&apply_date_default=on2&apply_stat=sasimodosi" class="always_blue">
								差し戻し通知オーダが<?=$apply_sasimodosare_count?>件あります。</a>
								</div>
						<? } ?>
						<? if ($apply_non_complete_checking){ ?>
							<div style="padding:5px; background-color:#fffca0; border:2px solid #ff77bb; width:450px; margin-top:10px">
								<a href="sum_application_apply_list.php?session=<?=$session?>&apply_date_default=on2&apply_stat=complete" class="always_blue">
								送受信がすべて完了したオーダが<?=$apply_non_complete_checking?>件あります。確認してください。</a>
								</div>
						<? } ?>
						<? if (!$apply_non_accepted_count && !$apply_non_operated_count && !$apply_sasimodosare_count && !$apply_non_complete_checking){ ?>
							<div style="padding-left:5px">
							<a href="sum_application_apply_list.php?session=<?=$session?>&apply_date_default=on" class="always_blue">
								未受付・未実施の送信済みオーダはありません。</a>
							</div>
						<? } ?>
          </td>
          <td width="19%" align="right"><img src="img/mail-soushin.gif" alt="" width="137" height="147" /></td>
        </tr>
      </table></td>
    </tr>
	</table></td>
	<? if (summary_common_get_summary_options($con,$fname,"is_use_top_information_html")){ ?>
	<?=summary_common_get_summary_options($con,$fname,"top_information_html") ?>
  <? } ?>
</tr></table>




<? } ?>
<? if ($search_mode){ ?>
<?
//**********************************************************************************************************************
// ２of２ 患者検索画面
//**********************************************************************************************************************

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}

$EMPTY_OPTION = '<option value="">　　　　</option>';
$sel_sect_key = trim(@$_REQUEST["sel_sect_key"]);
$sel_dr_key   = trim(@$_REQUEST["sel_dr_key"]);

$sel_ptif_id = trim(@$sel_ptif_id);
$sel_ptif_name = trim(@$sel_ptif_name);
$page = (int)@$page;

if ($search_mode != "2"){
	$sel = select_from_table($con, "select * from drmst where dr_del_flg = 'f' and dr_emp_id = '".pg_escape_string($emp_id)."'", "", $fname);
	$row = @pg_fetch_array($sel);
	$sel_dr_key = @$row["enti_id"]."_".@$row["sect_id"]."_".@$row["dr_id"];
}

$sel_dr_id = "";
list($sel_enti_id, $sel_sect_id) = explode("_", $sel_sect_key."_"); // 診療科をバラす
if ($sel_dr_key) list($sel_enti_id, $sel_sect_id, $sel_dr_id) = explode("_", $sel_dr_key."__"); // もし担当医が指定されていれば、バラす。

$sel_sect_key = $sel_enti_id."_".$sel_sect_id; //診療科コードを再作成




// 主治医選択肢
$sql =
	" select enti_id, sect_id, dr_id, dr_nm from drmst".
	" where dr_del_flg = 'f'".
	" order by enti_id ,sect_id";
$sel = select_from_table($con, $sql, "", $fname);
$dr_js_list = array();
$dr_data_options = array();
$dr_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$dr_js_list[] = 'drlist["'.$row["enti_id"]."_".$row["sect_id"]."_".$row["dr_id"].'"] = "'.$row["dr_nm"].'";'."\n";
	if ($row["enti_id"] != (int)$sel_enti_id || $row["sect_id"] != (int)$sel_sect_id) continue;
	$key = $row["enti_id"]."_".$row["sect_id"]."_".$row["dr_id"];
	$selected = ($key == $sel_dr_key ? " selected" : "");
	$dr_data_options[] = '<option value="'.$key.'"'.$selected.'>'.$row["dr_nm"].'</option>';
}

// 診療科選択肢
$sql =
	" select enti_id, sect_id, sect_nm, sort_order from (".
	"   select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
	" ) d".
	" where sect_del_flg = 'f'".
	" and (sect_hidden_flg is null or sect_hidden_flg <> 1) ".
	" order by sort_order, sect_id";
$sel = select_from_table($con, $sql, "", $fname);
$sect_data_options = array();
$sect_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$key = $row["enti_id"]."_".$row["sect_id"];
	$selected = ($key == $sel_sect_key ? " selected" : "");
	$sect_data_options[] = '<option value="'.$key.'"'.$selected.'>'.$row["sect_nm"].'</option>';
}

  if (array_key_exists("date_y1", $_REQUEST))
  {
    $dt_y1 = (int)@$_REQUEST["date_y1"] ; // 指定なし,"-","00" いずれでも値は0になる(以下同じ)
    $dt_m1 = (int)@$_REQUEST["date_m1"] ;
    $dt_d1 = (int)@$_REQUEST["date_d1"] ;
    $dt_y2 = (int)@$_REQUEST["date_y2"] ;
    $dt_m2 = (int)@$_REQUEST["date_m2"] ;
    $dt_d2 = (int)@$_REQUEST["date_d2"] ;
  }
  else
  {
    $dt_y1 = 0 ; // (int)date("Y") ;
    $dt_m1 = 0 ; // (int)date("m") ;
    $dt_d1 = 0 ; // (int)date("d") ;
    $dt_y2 = $dt_y1 ;
    $dt_m2 = $dt_m1 ;
    $dt_d2 = $dt_d1 ;
  }
?>
<script type="text/javascript">
	var drlist = {};
	<? foreach($dr_js_list as $idx => $row) echo $row; ?>

	function sectChanged(sect_key){
		sect_key += "_";
		var dr = document.getElementById("sel_dr_key");
		var len = sect_key.length;
		var optidx = 0;
		dr.options.length = 1;
		for(opt in drlist){
			if (opt.length <= len) continue;
			if (opt.substring(0, len) != sect_key) continue;
			optidx++;
			dr.options.length = optidx+1;
			dr.options[optidx].value = opt;
			dr.options[optidx].text = drlist[opt];
		}
	}
</script>

<form name="srch" action="summary_menu.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="search_mode" value="2">

<table class="list_table" cellspacing="1">
	<tr>
		<th width="10%" style="text-align:center"><?=$patient_title?>ID</th>
		<td width="10%"><input type="text" name="sel_ptif_id" maxlength="20" value="<?=$sel_ptif_id?>" style="ime-mode: inactive;"></td>
		<th width="10%" style="text-align:center"><?=$patient_title?>氏名</th>
		<td width="10%"><input type="text" name="sel_ptif_name" maxlength="20" value="<?=$sel_ptif_name?>" style="ime-mode: active;"></td>
		<th width="10%" style="text-align:center">診療科</th>
		<td width="10%"><select onchange="sectChanged(this.value);" name="sel_sect_key"><?=implode("\n", $sect_data_options)?></select></td>
		<th width="10%" style="text-align:center">主治医</th>
		<td width="30%"><select name="sel_dr_key" id="sel_dr_key"><?=implode("\n", $dr_data_options)?></select></td>
	</tr>
	<tr>
    <th style="text-align:center">検査日付</th>
		<td colspan="7">
      <table>
      <tr>
        <td>
          <select id="date_y1" name="date_y1"><? show_select_years(10, $dt_y1, true); ?></select>年<select id="date_m1" name="date_m1"><? show_select_months($dt_m1, true); ?></select>月<select id="date_d1" name="date_d1"><? show_select_days($dt_d1, true); ?></select>日
        </td>
        <td>
          <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
            <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
        </td>
        <td>
          &nbsp;〜<select id="date_y2" name="date_y2"><? show_select_years(10, $dt_y2, true); ?></select>年<select id="date_m2" name="date_m2"><? show_select_months($dt_m2, true); ?></select>月<select id="date_d2" name="date_d2"><? show_select_days($dt_d2, true); ?></select>日
        </td>
        <td>
          <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
            <div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
        </td>
      </tr>
      </table>
    </td>
	</tr>
</table>

<div class="search_button_div"><input type="submit" style="display:none" value="dummy"/><input type="button" onclick="document.srch.submit();" value="検索" /></div>
</form>

<form name="hid" method="post">
	<table width="100%" class="list_table" cellspacing="1">
	<tr>
		<th width="20%" style="text-align:center"><?=$patient_title?>ID</th>
		<th width="20%" style="text-align:center"><?=$patient_title?>氏名</th>
		<th width="20%" style="text-align:center">性別</th>
		<th width="20%" style="text-align:center">年齢</th>
		<th width="20%" style="text-align:center">生年月日</th>
	</tr>

<?
  $kensaJoin = "" ;
  $noDrInfo  = false ;
  $date1 = $date_y1 . $date_m1 . $date_d1 ;
  $date2 = $date_y2 . $date_m2 . $date_d2 ;
  if (strlen($date1.$date2) > 6) // 日付欄のすべてが指定されていないときは"------"(長さ6)となっている、このときは検索結果は検索条件に含めない
  {
    $date1 = str_replace("-", "0001", $date_y1) . str_replace("-", "01", $date_m1) . str_replace("-", "01", $date_d1) ;
    $date2 = str_replace("-", "9999", $date_y2) . str_replace("-", "12", $date_m2) . str_replace("-", "31", $date_d2) ;
    $kensaJoin =
      " from ptifmst pt" .
      " inner join (" .
      "              select distinct" .
      "                kn.ptif_id" .
      "              from" .
      "                sum_kensakekka kn" .
      "                @kensaJoin2@" .
      "              where" .
      "                (kn.hdr_uketuke_date >= '" . $date1 . "' and kn.hdr_uketuke_date <= '" . $date2 . "')" .
      "              ) kn2 on (kn2.ptif_id = pt.ptif_id)" ;
    $kensaJoin2 = "" ;
    if ($sel_enti_id && $sel_sect_id && $sel_dr_id)
    {
      $kensaJoin2 =
        " inner join (" .
        "            select" .
        "              dr_nm_kensa" .
        "            from" .
        "              drmst" .
        "            where" .
        "                  enti_id = " . $sel_enti_id .
        "              and sect_id = " . $sel_sect_id .
        "              and dr_id   = " . $sel_dr_id .
        "            ) dc on (dc.dr_nm_kensa = trim(kn.knr_tantou_doc))" ;
    }
    else
    {
      $noDrInfo = true ;
    }
    $kensaJoin = str_replace("@kensaJoin2@", $kensaJoin2, $kensaJoin) ;
  }

$limit = 20;

$sql = "";

if (strlen($kensaJoin) < 1)
{
  if ($sel_sect_id || $sel_dr_id){
	$sql .= " from inptmst inpt";
	$sql .= " left outer join ptifmst pt on (pt.ptif_id = inpt.ptif_id)";
} else {
	$sql .= " from ptifmst pt";
	$sql .= " left outer join inptmst inpt on (inpt.ptif_id = pt.ptif_id)";
}
}

$sql .= $kensaJoin ;
$sql .= " where pt.ptif_del_flg = 'f'";

$where = "";
if ($sel_ptif_id) $where .= " and pt.ptif_id = '".pg_escape_string($sel_ptif_id)."'";

if ($sel_ptif_name != "") {
	$where .= " and ( 1 <> 1";
	$fullnm = str_replace(" ", "", $sel_ptif_name);
	$fullnm = str_replace("　", "", $fullnm);
	$where .=
		" or pt.ptif_ft_kaj_nm like '%".pg_escape_string($sel_ptif_name)."%'".
		" or pt.ptif_lt_kaj_nm like '%".pg_escape_string($sel_ptif_name)."%'".
		" or pt.ptif_ft_kana_nm like '%".pg_escape_string($sel_ptif_name)."%'".
		" or pt.ptif_lt_kana_nm like '%".pg_escape_string($sel_ptif_name)."%'".
		" or (pt.ptif_lt_kaj_nm || pt.ptif_ft_kaj_nm) like '%".pg_escape_string($fullnm)."%'".
		" or (pt.ptif_lt_kana_nm || pt.ptif_ft_kana_nm) like '%".pg_escape_string($fullnm)."%'".
		" )";
}

if (strlen($kensaJoin) < 1)
{
  if ($sel_sect_id) $where .= " and inpt.inpt_sect_id = ".$sel_sect_id;
  if ($sel_dr_id)   {
	// 2015/4/17
	// drmst上で削除済になってしまったdrの患者も取得したい。
	// この主治医と同じ「診療科」で、同姓同名であれば、主治医とみなす。
	// 本来は同一dr_emp_idであれば、という条件にすべきかもしれないが、
	// 主治医メンテはdr_emp_id無しでも登録できてしまうため、名前で持ってこないと取得できない。
	// この画面では、sel_dr_idがあるということは、sel_sect_id存在する。
	$sqldr =
	" select dr_id from drmst".
	" where sect_id = ".(int)$sel_sect_id.
	" and dr_nm in (".
	"     select dr_nm from drmst".
	"     where dr_id = ".$sel_dr_id.
	"     and sect_id = ".(int)$sel_sect_id.
	" ) group by dr_id";
	$seldr = select_from_table($con, $sqldr, "", $fname);
	$dr_ids = array($sel_dr_id);
	while ($rowdr = pg_fetch_assoc($seldr)) $dr_ids[] = $rowdr["dr_id"];
	$where .= " and inpt.dr_id in (".implode(",", array_unique($dr_ids)).")";
  }
}

if (($noDrInfo) || ((strlen($kensaJoin) < 1) && ($where==""))) $where = " and 1 <> 1"; // 検索条件が指定されていなければ、対象がゼロ件になるように、絶対検索されない条件を指定

// 該当レコード数を取得
$sel = select_from_table($con,"select count(*) ".$sql.$where,"",$fname);
$record_count = intval(pg_fetch_result($sel, 0, 0));

if($page == 0){
	$where .= " order by pt.ptif_id limit ".$limit;
}else{
	$where .= " order by pt.ptif_id limit ".$limit." offset ".($page*$limit);
}

$sel = select_from_table($con,"select pt.* ".$sql.$where,"",$fname);

while ($row = pg_fetch_array($sel)){
	$pt_sex = "不明";
	if ($row["ptif_sex"]=="1") $pt_sex = "男性";
	if ($row["ptif_sex"]=="2") $pt_sex = "女性";
	$color = @$color=="#fff" ? "#F1F7FD" : "#fff";
?>
	<tr style="background-color:<?=$color?>">
	<td align="center"><?=$row["ptif_id"]?></td>
	<td><a href="summary_rireki.php?session=<?=$session?>&pt_id=<?=$row["ptif_id"]?>"><?=$row["ptif_lt_kaj_nm"]." ".$row["ptif_ft_kaj_nm"]?></a></td>
	<td align="center"><?=$pt_sex?></td>
	<td align="center"><?=get_age($row["ptif_birth"])?></td>
	<td align="center"><?=$c_sot_util->kanji_ymd($row["ptif_birth"])?></td>
	</tr>
<? } ?>
</table>

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="sel_ptif_id">
<input type="hidden" name="sel_ptif_name">
<input type="hidden" name="search_mode" value="2">
</form>

<? show_page_area($record_count, $limit, $page, $session); ?>
<script type="text/javascript">
function page_change(page) {
	location.href = 'summary_menu.php?session=<? echo($session); ?>&search_mode=2&sel_ptif_id=<? echo(h(urlencode($sel_ptif_id))); ?>&sel_ptif_name=<? echo(h(urlencode($sel_ptif_name))); ?>&sel_sect_key=<? echo(h(urlencode($sel_sect_key))); ?>&sel_dr_key=<? echo(h(urlencode($sel_dr_key))); ?>&page=' + page;
}
</script>

<? } ?>
</body>
<? pg_close($con); ?>
</html>
<?
function show_page_area($record_count, $limit, $page) {
	if ($record_count <= $limit) return;

	$page_max = ceil($record_count / $limit);
?>
<table width="100%" style="margin-top:4px;"><tr><td>
<table class="td_pad2px" style="color:#c0c0c0; white-space:nowrap"><tr>
<td><? if ($page > 0) { ?><a href="javascript:page_change('0');"><? } ?>先頭へ<? if ($page > 0) { ?></a><? } ?></td>
<td><? if ($page > 0) { ?><a href="javascript:page_change('<? echo($page - 1); ?>');"><? } ?>前へ<? if ($page > 0){ ?></a><? } ?></td>
<td>
<? for ($i = 0; $i < $page_max; $i++) { ?>
<? if ($i != $page) { ?><a href="javascript:page_change('<? echo($i); ?>');"><? } ?>[<? echo($i + 1); ?>]<? if ($i != $page) { ?></a><? } ?>
<? } ?>
</td>
<td><? if ($page < $page_max - 1){ ?><a href="javascript:page_change('<? echo($page + 1); ?>');"><? } ?>次へ<? if ($page < $page_max - 1){ ?></a><? } ?></td>
</tr></table>
</td></tr></table>
<?
}
?>
