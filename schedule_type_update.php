<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 種別</title>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 13, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// スケジュールの初期画面を取得
$sql = "select schedule1_default from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
switch ($schedule1_default) {
case "1":
	$default_url = "schedule_menu.php";
	break;
case "2":
	$default_url = "schedule_week_menu.php";
	break;
case "3":
	$default_url = "schedule_month_menu.php";
	break;
}

// 種別名称を取得（スケジュール分）
$sql = "select type_id, type_name from schdtype";
$cond = "order by to_number(type_id, '99')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_name1 = array();
while ($row = pg_fetch_array($sel)) {
	$arr_name1[$row["type_id"]] = $row["type_name"];
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>スケジュール</b></a> &gt; <a href="schedule_place_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>種別</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_comgroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">共通グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_setting_etc.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form action="schedule_type_update_exe.php" method="post">
<table width="300" border="0" cellspacing="0" cellpadding="1">
<tr valign="top">
<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr valign="top"><td>　</td></tr>
	
		<?
		$color_no = 8;
		foreach ($arr_name1 as $tmp_id => $tmp_name) {
		?>
		<tr height="22" valign="bottom">
			<td width="100" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別<? echo($tmp_id); ?>&nbsp;<img src="./img/color_<? echo($color_no); ?>.gif" width="10" height="17" style="border:black solid 1px;">：</font></td>
			<td>
				<? 
				if ($tmp_id == "1") 
					{
						echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font>");
					} 
					else 
					{ 
				?>
					<input type="text" name="schd<? echo($tmp_id); ?>" value="<? echo($tmp_name); ?>" size="20" maxlength="14" style="ime-mode:active;">
				<? } ?>
			</td>
		</tr>
		<?
			$color_no++;
		}
		?>
	</table>
</td>
</tr>
<tr>
<td colspan="2" align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
