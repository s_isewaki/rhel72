<?
require_once("about_comedix.php");
require_once("show_fplus_workflow_detail.ini");
require_once("show_select_values.ini");
require_once("fplus_imprint_common.ini");
require_once("yui_calendar_util.ini");
require_once("get_values.ini");
require_once("fplus_common_class.php");
require_once("get_values_for_template.ini");
require_once("library_common.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new fplus_common_class($con, $fname);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 報告詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];

// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
	if (strpos($apply_content, "<?xml") === false) {
		$wkfw_content_type = "1";
	}
}

if($wkfw_history_no != "")
{
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

$num = 0;
if ($wkfw_content_type == "2") {
	$pos = 0;
	while (1) {
		$pos = strpos($wkfw_content, 'show_cal', $pos);
		if ($pos === false) {
			break;
		} else {
			$num++;
		}
		$pos++;
	}
}


if ($num > 0) {
	// 外部ファイルを読み込む
	write_yui_calendar_use_file_read_0_12_2();
}
// カレンダー作成、関数出力
write_yui_calendar_script2($num);

/*
if ($mode == "apply_printwin") {

//---------テンプレートの場合XML形式のテキスト$contentを作成---------

	if ($wkfw_content_type == "2") {
		// XML変換用コードの保存
		$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
		$savexmlfilename = "workflow/tmp/{$session}_x.php";
		$fp = fopen($savexmlfilename, "w");

		if (!$fp) {
			echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
			echo("<script language='javascript'>history.back();</script>");
		}
		if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
			fclose($fp);
			echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
			echo("<script language='javascript'>history.back();</script>");
		}

		fclose($fp);

		include( $savexmlfilename );

	}
	$savexmlfilename = "workflow/tmp/{$session}_d.php";
	$fp = fopen($savexmlfilename, "w");

	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、印刷してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $content, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、印刷してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);
}
*/
?>
<script type="text/javascript">


function apply_printwin()
{
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_workflow_detail.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = 'printframe';
	document.apply.submit();
	document.apply.target = default_target;
}

function apply_printwin_epinet() {
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_apply_detail_print_epinet.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = '_blank';
	document.apply.submit();
	//document.apply.target = default_target;
}

function apply_printwin_mrsa() {
	document.apply.mode.value = "apply_printwin";
	document.apply.action="fplus_apply_detail_print_mrsa.php?session=<?=$session?>";
	var default_target = document.apply.target;
	document.apply.target = '_blank';
	document.apply.submit();
	//document.apply.target = default_target;
}



// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}

function apply_update(apv_num) {

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	//エピネット用入力チェック処理ここから
	var epi_element = document.getElementById("EPINET_REPORT_PIERCE_CUT_INFECTION");

	if(epi_element != undefined)
	{
	
		if(epi_element.value == "Episys107A")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107A();
			if(ret == false)
			{
				return false;
			}
		}
		else if(epi_element.value == "Episys107B")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107B();
			if(ret == false)
			{
				return false;
			}
		}else if(epi_element.value == "Episys107AO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107AO();
			if(ret == false)
			{
				return false;
			}
		}
                else if(epi_element.value == "Episys107BO")
		{
			//confirm(epi_element.value);
			ret = fc_regist_check_107BO();
			if(ret == false)
			{
				return false;
			}
		}

	}
	//エピネット用入力チェック処理ここまで

	//MRSA報告書入力チェック処理ここから
	var epi_element = document.getElementById("MRSA_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check();
		if(ret == false)
		{
			return false;
		}

	}
	//MRSA報告書入力チェック処理ここまで

	// 医療事故報告用入力チェック処理ここから
	// 緊急報告
	var epi_element = document.getElementById("EPINET_REPORT_IMMEDIATE_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_immediate_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第一報
	var epi_element = document.getElementById("EPINET_REPORT_FIRST_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_first_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 第二報
	var epi_element = document.getElementById("EPINET_REPORT_SECOND_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_second_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション依頼書
	var epi_element = document.getElementById("EPINET_REPORT_CONSULTATION_REQUEST");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_req();
		if(ret == false)
		{
			return false;
		}
	}
	// コンサルテーション報告書
	var epi_element = document.getElementById("EPINET_REPORT_CONSULTATION_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_cons_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療機器不具合報告
	var epi_element = document.getElementById("MEDICAL_APPARATUS_FAULT_REPORT");
	if(epi_element != undefined)
	{
		ret = fc_regist_check_apparatus_fault_repo();
		if(ret == false)
		{
			return false;
		}
	}
	// 医療事故報告用入力チェック処理ここまで

	if (confirm('更新します。よろしいですか？')) {
		document.apply.action="fplus_apply_detail_update.php?session=<?=$session?>&update_mode=mentenance";
		document.apply.submit();
	}
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#E6B3D4 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#E6B3D4 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#E6B3D4 solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initcal();
<? if ($mode == "apply_printwin") { ?>
window.open('fplus_workflow_detail_print.php?session=<?=$session?>&fname=<?=$fname?>&apply_id=<?=$apply_id?>&mode=apply_print', 'apply_print', 'width=640,height=700,scrollbars=yes');
<? } ?>
if (window.OnloadSub) { OnloadSub(); };resizeAllTextArea();if (window.refreshApproveOrders) {refreshApproveOrders();}">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_history_no" value="<? echo($wkfw_history_no); ?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#E6B3D4">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>報告詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
$mode = "";
show_application_workflow_detail($con, $session, $fname, $apply_id, $mode);
?>
</center>
</form>
</body>
<iframe name="printframe" width="0" height="0" frameborder="0"></iframe>
<?
// 申請結果通知画面から呼ばれた場合
if($screen == "NOTICE")
{
// 確認済みフラグ更新
$obj->update_confirmed_flg($apply_id, $session);
?>
<script language='javascript'>
if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
</script>
<?
}

pg_close($con);
?>
</html>
