<?php
require_once("hiyari_db_class.php");
require_once("hiyari_report_xml_class.php");
require_once("hiyari_report_xml_data.php");

class hiyari_report_xml_merge { 
	
	// XML用データ
	var $_data;
	
	// XML作成オブジェクト
	var $_xml;
	
	// エラー
	var $_error = array();
	
	function hiyari_report_xml_merge($data, $xml) { 
		$this->_data = $data;
		$this->_xml  = $xml;
	}
	
	// XMLを作成する(エラー報告付き)
	function create_xml() { 
		// ヒヤリか事故かを取得する
		$this->set_mode();
		$division = $this->get_mode();
		
		// 報告書のバージョンを指定する
		
		// シーケンス番号はreport_idを利用する
		$this->_xml->xml_case_report($division, $this->_data->get_report_id());
		
		// 日付をセットする
		if($date = $this->_data->get_date()) { 
			$this->_xml->xml_date($date);
		} else { 
			$this->set_error(100, 5, 'DATYEAR', '発生年', '必須項目が入力されていません。');
			$this->set_error(100, 5, 'DATMONTH', '発生月', '必須項目が入力されていません。');
			$this->set_error(100, 5, 'DATDAY', '発生曜日', '必須項目が入力されていません。');
		}
		
		// 平日・休日をセットする
		if($this->get_mode() == 1) { // 事故の時
			$holiday = $this->_data->get_holiday();
			
			if($holiday == '01' || $holiday == '02') { 
				$this->_xml->xml_holiday($holiday);
			} else if($holiday == '90') { 
				$this->set_error(100, 5, 'DATHOLIDAY', '曜日区分', 'カレンダーの設定が不明の可能性があります。');
			} else { 
				$this->set_error(100, 5, 'DATHOLIDAY', '曜日区分', '必須項目が入力されていません。');
			}
		}
		
		// 事例が発生した時間帯をセットする
		if($timezone = $this->_data->get_timezone()) { 
			$this->_xml->xml_timezone($timezone, $this->_data->get_timezone_text());
		} else { 
			$this->set_error(100, 40, 'DATTIMEZONE', '発生時間帯', '必須項目が入力されていません。');
		}
		
		// 医療の実施の有無をセットする
		if($execution = $this->_data->get_execution()) { 
			$this->_xml->xml_execution($execution);
		} else { 
			$this->set_error(1100, 10, 'DATEXECUTION', '医療の実施の有無', '必須項目が入力されていません。');
		}
		
		// 治療の程度をセットする
		if($execution == '実施あり') { 
			$level = $this->_data->get_level();
			
			if($this->get_mode() == 2 && $level == '濃厚な治療') { // ヒヤリでは濃厚な治療は選択できない
				$this->set_error(1100, 10, 'DATLEVEL', '治療の程度', 'ヒヤリでは「濃厚な治療」は選択できません。'); // 1100_20
			} else if(empty($level)) { 
				$this->set_error(1100, 10, 'DATLEVEL', '治療の程度', '必須項目が入力されていません。');	// 1100_20
			} else { 
				$this->_xml->xml_level($level, $execution);
			}
		}
		
		// 事故の程度をセットする
		if($this->get_mode() == 1) { 
			if($sequelae = $this->_data->get_sequelae()) { 
				$sequelae_text = $this->_data->get_sequelae_text();
				if($sequelae == '不明' && empty($sequelae_text)) { 
					$this->_set_error(1100, 10, 'DATSEQUELAE', '事故の程度', '不明の場合は事例の程度を1000バイト以内で記入してください。'); // 1100_40
				} else { 
					$this->_xml->xml_sequelae($sequelae, $sequelae_text);
				}
			} else { 
				$this->set_error(1100, 10, 'DATSEQUELAE', '事故の程度', '必須項目が入力されていません。'); // 1100_30
			}
		}
		
		// 影響度をセットする
		if($this->get_mode() == 2 && $execution == '実施なし') { 
			if($influence = $this->_data->get_influence()) { 
				$this->_xml->xml_influence($influence);
			} else { 
				$this->set_error(1100, 10, 'DATINFLUENCE', '影響度', '必須項目が入力されていません。'); // 1100_50
			}
		}
		
		// 発生場所をセットする
		if($site = $this->_data->get_site()) { 
			$site_text = $this->_data->get_site_text();
			
			if($site == 99 && empty($site_text)) { // その他の場合は発生場所を記入
				$this->set_error(110, 70, 'LSTSITE', '発生場所', 'その他の場合は発生場所を1000バイト以内で記入してください。');
			} else { 
				$this->_xml->xml_site($site, $site_text);
			}
		} else { 
			$this->set_error(110, 60, 'LSTSITE', '発生場所', '必須項目が入力されていません。');
		}
		
		// 概要をセットする
		if($summary_data = $this->_data->get_summary()) { 
			$summary_text = $this->_data->get_summary_text();
            
			if($summary_data['code'] == 99 && empty($summary_text)) { // その他の場合は概要を記入
				$this->set_error(900, 20, 'DATSUMMARY', '概要', 'その他の場合は概要を1000バイト以内で記入してください。');
			} else { 
	            $this->_xml->xml_summary($summary_data['export_code'], $summary_text);
			}
		} else {
            
            // 概要が選択されていない場合、エラーを出力 
			$this->set_error(900, 10, 'DATSUMMARY', '概要', '必須項目が入力されていません。');
		}
		
		// 特に報告を求める事例
		if($this->get_mode() == 1) { 
			if($specially = $this->_data->get_specially()) { 
				//$specially_text = $this->_data->get_specially_text();
				//if($specially == '本事例は選択肢には該当しない（その他）' && empty($specially_text)) { 
				//	$this->set_error(1110, 20, 'DATSPECIALLY', '特に報告を求める事例', 'その他の場合は特に報告を求める事例を1000バイト以内で記入してください。');
				//} else { 
					$this->_xml->xml_specially($specially, $specially_text);
				//}
			} else { 
				$this->set_error(1110, 10, 'DATSPECIALLY', '特に報告を求める事例', '必須項目が入力されていません。');
				
			}
		}
		
		// 関連診療科をセットする
		if($this->get_mode() == 1) { 
			if($department = $this->_data->get_department()) { 
				$department_text = $this->_data->get_department_text();
				
				$flg = 0;
				foreach ($department as $value) {
					if (trim($value['def_code']) == "99" && empty($department_text)) { 
						$flg = 1;
						break;
					}
				}
				
				if($flg == 1) { 
					$this->set_error(130, 100, 'DATDEPARTMENT', '関連診療科', 'その他の場合は関連診療科を1000バイト以内で記入してください。');
				} else {
					$this->_xml->xml_department($department, $department_text);
				}
			} else { 
				$this->set_error(130, 90, 'DATDEPARTMENT', '関連診療科', '必須項目が入力されていません。');
			}
		}
		
		// 患者の数をセット
		if($patient_num = $this->_data->get_patient_num()) { 
			$patient_text = $this->_data->get_patient_text();
			
			if(!($patient_num == 2 && empty($patient_text))) { 
				if($patient_year = $this->_data->get_patient_year()) { 
					if($patient_month = $this->_data->get_patient_month()) { 
						if($patient_sex = $this->_data->get_patient_sex()) { 
							if($patient_div = $this->_data->get_patient_div()) { 
								$this->_xml->xml_patient($patient_num, $patient_text, $patient_year, $patient_month, $patient_sex, $patient_div);
							} else { 
								$this->set_error(230, 60, 'GRPPATIENT', '患者区分1', '必須項目が入力されていません。');
							}
						} else { 
							$this->set_error(210, 30, 'GRPPATIENT', '患者の性別', '必須項目が入力されていません。');
						}
					} else { 
						$this->set_error(210, 50, 'GRPPATIENT', '患者の年齢(月数)', '必須項目が入力されていません。');
					}
				} else { 
					$this->set_error(210, 40, 'GRPPATIENT', '患者の年齢(年数)', '必須項目が入力されていません。');
				}
			} else {
				$this->set_error(200, 20, 'GRPPATIENT', '患者の数', '複数人の場合は患者の数を記入してください。');
			}
		} else { 
			$this->set_error(200, 10, 'GRPPATIENT', '患者の数', '必須項目が入力されていません。');
		}
		
		// 疾患名をセット
		if($disease0 = $this->_data->get_disease0()) { 
			$disease1 = $this->_data->get_disease1();
			$disease2 = $this->_data->get_disease2();
			$disease3 = $this->_data->get_disease3();
			
			$this->_xml->xml_disease($disease0, $disease1, $disease2, $disease3);
		} else { 
			$this->set_error(250, 80, 'LSTDISEASE', '疾患名', '必須項目が入力されていません。');
		}
		
		// 直前の患者の状態をセット
		if($patient_state = $this->_data->get_patient_state()) { 
			$patient_state_text = $this->_data->get_patient_state_text();
			
			$patient_state_flg = false;
			foreach($patient_state as $value) { 
				$patient_state_code = sprintf("%02d", $value['def_code']);
				if($patient_state_code == 99) { 
					$patient_state_flg = true;
				}
			}
			
			//if($patient_state_flg == true && empty($patient_state_text)) { 
			//	$this->set_error(290, 130, 'LSTPATIENTSTATE', '直前の患者の状態', 'その他の場合は直前の患者の状態を1000バイト以内で記入してください。');
			//} else { 
				$this->_xml->xml_patient_state($patient_state, $patient_state_text);
			//}
		//} else { 
		//	$this->set_error(290, 120, 'LSTPATIENTSTATE', '直前の患者の状態', '必須項目が入力されていません。');
		}
		
		// 発見者
		if($discoverer = $this->_data->get_discoverer()) { 
			$discoverer_text = $this->_data->get_discoverer_text();
			
			if($discoverer == 90) { 
				$this->set_error(3000, 10, 'DATDISCOVERER', '発見者', '不明は入力できません。');
			} else if($discoverer == 99 && empty($discoverer_text)) { 
				$this->set_error(3000, 20, 'DATDISCOVERER', '発見者', 'その他の場合は発見者を1000バイト以内で記入してください。');
			} else { 
				$this->_xml->xml_discoverer($discoverer, $discoverer_text);
			}
		} else { 
			$this->set_error(3000, 10, 'DATDISCOVERER', '発見者', '必須項目が入力されていません。');
		}
		
		// 当事者
		$concerned_flg = false;
		if($concerned_num = $this->_data->get_concerned_num()) { 
			for($i=0; $i<$this->_data->get_concerned_num(); $i++) { 
				$arr_data[$i] = array('concerned' => $this->_data->get_concerned($i), 
						'concerned_text' => $this->_data->get_concerned_text($i), 
						'qualification' => $this->_data->get_qualification($i), 
						'e_year' => $this->_data->get_experience_year($i), 
						'e_month' => $this->_data->get_experience_month($i),
						'l_year' => $this->_data->get_length_year($i),
						'l_month' => $this->_data->get_length_month($i), 
						'duty' => $this->_data->get_duty($i), 
						'duty_text' => $this->_data->get_duty_text($i), 
						'shift' => $this->_data->get_shift($i), 
						'shift_text' => $this->_data->get_shift_text($i), 
						'hours' => $this->_data->get_hours($i)
						);
				if($this->_data->get_concerned($i) == "" || 
						$this->_data->get_experience_year($i) == "" || 
						$this->_data->get_length_year($i) == "") {
					//$concerned_flg = true;
					break;
				} else if($this->mode == 1 && 
						($this->_data->get_qualification($i) == "" ||
							$this->_data->get_experience_month($i) == "" ||
							$this->_data->get_length_month($i) == "" ||
							$this->_data->get_duty($i) == "" ||
							$this->_data->get_shift($i) == "" || 
							$this->_data->get_hours($I))) { 
					//$concerned_flg = true;
					break;
				}
			}
			
			if($concerned_flg == false) { 
				$this->_xml->xml_concerned($concerned_num, $arr_data, $concerned_num_text); // $concerned_num_textは11人以上になるの事は無いのでNULL
			} else { 
				$this->set_error(3050, 30, 'LSTCONCERNED', '当事者', '必須項目が入力されていません。');
			}
		//} else { 
		//	$this->set_error(3050, 30, 'LSTCONCERNED', '当事者', '必須項目が入力されていません。');
		}
		
		// 当事者以外の関連職種
		if($relcategory = $this->_data->get_relcategory()) { 
			$relcategory_text = $this->_data->get_relcategory_text();
			
			if($relcategory == 99 && empty($relcategory_text)) { 
				$this->set_error(3500, 160, 'LSTRELCATEGORY', '当事者以外の関連職種', 'その他の場合は当事者以外の関連職種を1000バイト以内で記入してください。');
			} else {
				$this->_xml->xml_relcategory($relcategory, $relcategory_text);
			}
		//} else { 
		//	$this->set_error(3500, 150, 'LSTRELCATEGORY', '当事者以外の関連職種', '必須項目が入力されていません。');
		}
		
		$type = $this->_data->get_type();
		$scene = $this->_data->get_scene();
		$detail = $this->_data->get_detail();
		$type_text = $this->_data->get_type_text();
		$scene_text = $this->_data->get_scene_text();
		$detail_text = $this->_data->get_detail_text();
		$medical_supplies = $this->_data->get_medical_supplies();
		$material = $this->_data->get_material();
		$equipment = $this->_data->get_equipment();
        $summary_data = $this->_data->get_summary();
		
        switch ($summary_data['export_code']) {
			case "02" :	//薬剤
				if(empty($type) || empty($scene) || empty($detail) || empty($medical_supplies['name']) || empty($medical_supplies['manufacture'])) { 
					$this->set_error(900, 10, 'GRPMEDICINE', '薬剤に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_medicine($type, $scene, $detail, $type_text, $scene_text, $detail_text, $medical_supplies);
				}
				break;
			case "03" :	//輸血
				if(empty($scene) || empty($detail)) { 
					$this->set_error(900, 10, 'GRPTRANSFUSION', '輸血に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_transfusion($scene, $detail, $scene_text, $detail_text);
				}
				break;
			case "04" :	//治療・処置
				if(empty($type) || empty($scene) || empty($detail) || empty($material['name']) || empty($material['manufacture']) || empty($material['purchase'])) { 
					$this->set_error(900, 10, 'GRPTREATMENT', '治療・処置に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_treatment($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material);
				}
				break;
			case "10" :	//医療機器等
				if(empty($type) || empty($scene) || empty($detail) || //empty($type_text) || empty($scene_text) || empty($detail_text) || 
						empty($material['name']) || empty($material['manufacture']) || empty($material['purchase']) || 
						empty($equipment['name']) || empty($equipment['manufacture']) || empty($equipment['dateofmanufacture']) || empty($equipment['purchase']) || empty($equipment['maintenance'])) { 
					$this->set_error(900, 10, 'GRPAPPARATUS', '医療機器等・医療材料の使用・管理に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_apparatus($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material, $equipment);
				}
				break;
			case "06" :	//ドレーン・チューブ
				if(empty($type) || empty($scene) || empty($detail) || empty($material['name']) || empty($material['manufacture']) || empty($material['purchase'])) { 
					$this->set_error(900, 10, 'GRPDRAINTUBE', 'ドレーン・チューブ類の使用に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_draintube($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material);
				}
				break;
			case "08" :	//検査
				if(empty($type) || empty($scene) || empty($detail) || //empty($type_text) || empty($scene_text) || empty($detail_text) || 
						empty($material['name']) || empty($material['manufacture']) || empty($material['purchase']) || 
						empty($equipment['name']) || empty($equipment['manufacture']) || empty($equipment['dateofmanufacture']) || empty($equipment['purchase']) || empty($equipment['maintenance'])) { 
					$this->set_error(900, 10, 'GRPINSPECTION', '検査に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_inspection($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material, $equipment);
				}
				break;
			case "09" :	//療養上の世話
				if(empty($type) || empty($scene) || empty($detail)) { 
					$this->set_error(900, 10, 'GRPRECUPERATING', '療養上の場面に関する項目', '必須項目が入力されていません。');
				} else { 
					$this->_xml->xml_recuperating($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material);
				}
				break;
			case "99" :	//その他
				break;
		}	
		
		if($this->get_mode() == 1) { 
			if($purpose = $this->_data->get_purpose()) { 
				$this->_xml->xml_purpose($purpose);
			} else { 
				$this->set_error(500, 10, 'DATPURPOSE', '実施した医療行為の目的', '必須項目が入力されていません。');
			}
		}
		
		if($contenttext = $this->_data->get_contenttext()) { 
			$this->_xml->xml_contenttext($contenttext);
		} else { 
			$this->set_error(520, 30, 'DATCONTENTTEXT', '事故(事例)の内容', '必須項目が入力されていません。');
		}
		
		if($factor = $this->_data->get_factor()) { 
			$factor_text = $this->_data->get_factor_text();
			
			if(substr($factor['def_code'], -2) == 99 && empty($factor_text)) { 
				$this->set_error(600, 20, 'LSTFACTOR', '発生要因', 'その他の場合は発生要因を1000バイト以内で記入してください。');
			} else {
				$this->_xml->xml_factor($factor, $factor_text);
			}
		} else { 
			$this->set_error(600, 10, 'LSTFACTOR', '発生要因', '必須項目が入力されていません。');
		}
		
		if($background = $this->_data->get_background()) { 
			$this->_xml->xml_background($background);
		} else { 
			$this->set_error(610, 20, 'DATBACKGROUND', '事故の背景要因の概要', '必須項目が入力されていません。');
		}
		
		if($this->get_mode() == 1) { 
			if($committee = $this->_data->get_committee()) { 
				$committee_text = $this->_data->get_committee_text();
				
				$this->_xml->xml_committee($committee, $committee_text);
			} else { 
				$this->set_error(590, 90, 'DATCOMMITTEE', '事故調査委員会設置の有無', '必須項目が入力されていません。');
			}
		}
		
		if($improvementtext = $this->_data->get_improvementtext()) { 
			$this->_xml->xml_improvementtext($improvementtext);
		} else { 
			$this->set_error(620, 30, 'IMPROVEMENTTEXT', '改善策', '必須項目が入力されていません。');
		}
		
		$this->_xml->xml_close();
	}
	
	function set_mode() { 
		$val = $this->_data->get_mode();
		$this->_xml->set_hiyari_mode($val);
		
		$patient_level = $this->_data->get_patient_level();
		if (empty($patient_level)) {
			$this->set_error(90, 10, 'PATIENTLEVEL', '患者影響レベル', '必須項目が入力されていません。');
		}
		
		/*
		$hiyari_mode = array('LV0', 'LVh', 'LV1', 'LV2', 'LV3', 'L3a', 'LVn');
		$jiko_mode   = array('L3b', 'LV4', 'L4a', 'L4b', 'LV5');
		
		$patient_level = $this->_data->get_patient_level();
		
		if(in_array($patient_level, $hiyari_mode) || empty($patient_level)) { 
			$this->_xml->set_hiyari_mode('hiyari');
		} else if(in_array($patient_level, $jiko_mode)) { 
			$this->_xml->set_hiyari_mode('jiko');
		} else { 
			$this->set_error(90, 10, '必須項目が入力されていません。');
		}
		*/
	}
	
	function get_mode() { 
		if($this->_xml->get_hiyari_mode() == 'hiyari') { 
			return 2;
		} else if($this->_xml->get_hiyari_mode() == 'jiko') { 
			return 1;
		}
	}
	
	function set_error($grp_code, $easy_item_code, $element, $element_name, $message) { 
		$ng['grp_code'] = $grp_code;
		$ng['easy_item_code'] = $easy_item_code;
		$ng['element'] = $element;
		$ng['element_name'] = $element_name;
		$ng['message'] = $message;
		
		$this->_error[] = $ng;
	}
	
	function output_xml_header() { 
		//header("Content-Type: text/xml; charset=Shift_JIS");
		//header('Content-type: application/xhtml+xml; charset=Shift_JIS');
		/*header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"casereport.xml\"");
		*/
		return $this->_xml->xml_header();
		// echo $this->_xml->xml_output();
	}
	
	function output_xml() { 
		return $this->_xml->xml_output();
	}
	
	function output_xml_footer() { 
		return $this->_xml->xml_footer();
	}
	
	function get_error() { 
		return $this->_error;
	}

}

?>