<body>
<form name="items" method="post" action="library_detail.php">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="archive" value="<? echo($archive); ?>">
<input type="hidden" name="category" value="<? echo($category); ?>">
<input type="hidden" name="folder_id" value="<? echo($folder_id); ?>">
<input type="hidden" name="manage_history" value="<? echo($manage_history); ?>">
<input type="hidden" name="document_name" value="<? echo($document_name); ?>">
<input type="hidden" name="document_type" value="<? echo($document_type); ?>">
<input type="hidden" name="keywd" value="<? echo($keywd); ?>">
<input type="hidden" name="lib_no" value="<? echo($lib_no); ?>">
<input type="hidden" name="explain" value="<? echo($explain); ?>">
<input type="hidden" name="show_login_flg" value="<? echo($show_login_flg); ?>">
<input type="hidden" name="show_mypage_flg" value="<? echo ($show_mypage_flg == 'f') ? 'f' : 't'; ?>">
<input type="hidden" name="show_login_begin1" value="<? echo($show_login_begin1); ?>">
<input type="hidden" name="show_login_begin2" value="<? echo($show_login_begin2); ?>">
<input type="hidden" name="show_login_begin3" value="<? echo($show_login_begin3); ?>">
<input type="hidden" name="show_login_end1" value="<? echo($show_login_end1); ?>">
<input type="hidden" name="show_login_end2" value="<? echo($show_login_end2); ?>">
<input type="hidden" name="show_login_end3" value="<? echo($show_login_end3); ?>">
<input type="hidden" name="private_flg1" value="<? echo($private_flg1); ?>">
<input type="hidden" name="private_flg2" value="<? echo($private_flg2); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?php


if (is_array($hid_ref_dept)) {
    foreach ($hid_ref_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?php
if (is_array($ref_st)) {
    foreach ($ref_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<input type="hidden" name="target_id_list2" value="<? echo($target_id_list2); ?>">
<?php
if ($target_id_list1 != "") {
    $hid_ref_emp = explode(",", $target_id_list1);
} else {
    $hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<? echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<? echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<? echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<? echo($upd_atrb_src); ?>">
<?php
if (is_array($hid_upd_dept)) {
    foreach ($hid_upd_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_upd_dept = array();
}
?>
<input type="hidden" name="upd_st_flg" value="<? echo($upd_st_flg); ?>">
<?php
if (is_array($upd_st)) {
    foreach ($upd_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $upd_st = array();
}
?>
<?php
if ($target_id_list2 != "") {
    $hid_upd_emp = explode(",", $target_id_list2);
} else {
    $hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="lib_id" value="<? echo($lib_id); ?>">
<input type="hidden" name="a" value="<? echo($a); ?>">
<input type="hidden" name="c" value="<? echo($c); ?>">
<input type="hidden" name="f" value="<? echo($f); ?>">
<input type="hidden" name="o" value="<? echo($o); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<? echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
</form>
<?php
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("library_common.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

$upload_max_filesize = trim(ini_get('upload_max_filesize'));
preg_match("/([0-9]+)([a-zA-Z])/", $upload_max_filesize, $matches);
$unit = strtolower($matches[2]);
switch($unit) {
    case 'g':
        $matches[1] *= 1024;
    case 'm':
        $matches[1] *= 1024;
    case 'k':
        $matches[1] *= 1024;
}

$upload_max_filesize_bytes = $matches[1];
if ($_SERVER["CONTENT_LENGTH"] > $upload_max_filesize_bytes) {
    echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_FILES);

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);
$filename = $_FILES["upfile"]["name"];
if ($filename != "") {
    switch ($_FILES["upfile"]["error"]) {
    case UPLOAD_ERR_OK:
        break;
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
        echo("<script type=\"text/javascript\">alert('ファイルサイズが大きすぎます。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
        break;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
        echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
        break;
    }
}

// 入力チェック
if ($archive == "") {
    echo("<script type=\"text/javascript\">alert('書庫を選択してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($category == "" || $category == "0") {
    echo("<script type=\"text/javascript\">alert('カテゴリを選択してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($document_name == "") {
    echo("<script type=\"text/javascript\">alert('文書名が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
else{
    if (!is_filename($document_name)) {
        echo("<script type=\"text/javascript\">alert(\"文書名に以下の文字は使えません。\\n\\\ / : * ? \\\" < > | '\");</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
    if (strlen($document_name) > 100) {
        echo("<script type=\"text/javascript\">alert('文書名が長すぎます。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if (strlen($keywd) > 100) {
    echo("<script type=\"text/javascript\">alert('キーワードが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($lib_no) > 50) {
    echo("<script type=\"text/javascript\">alert('文書番号が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($explain) > 200) {
    echo("<script type=\"text/javascript\">alert('説明が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$show_login_begin = "$show_login_begin1$show_login_begin2$show_login_begin3";
$show_login_end = "$show_login_end1$show_login_end2$show_login_end3";
if ($show_login_begin == "---") {$show_login_begin = "";}
if ($show_login_end == "---") {$show_login_end = "";}
if ($show_login_begin != "") {
    if (!checkdate($show_login_begin2, $show_login_begin3, $show_login_begin1)) {
        echo("<script type=\"text/javascript\">alert('表示期間（From）が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if ($show_login_end != "") {
    if (!checkdate($show_login_end2, $show_login_end3, $show_login_end1)) {
        echo("<script type=\"text/javascript\">alert('表示期間（To）が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if ($show_login_begin != "" && $show_login_end != "" && $show_login_begin > $show_login_end) {
    echo("<script type=\"text/javascript\">alert('表示期間が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($archive != "1" && (($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0 && !($archive == "4" && $private_flg2 == "t")) {
    echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if ($archive != "1" && (($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0) {
    echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

// 登録内容の編集
if ($filename != "") {
    $extension = (strpos($filename, ".")) ? preg_replace("/^.*\.([^.]*)$/", "$1", $filename) : "txt";
}
if ($folder_id == "") {$folder_id = null;}
if ($show_login_flg != "t") {$show_login_flg = "f";}
if ($show_mypage_flg != "f") {$show_mypage_flg = "t";}
if ($show_login_begin == "") {$show_login_begin = null;}
if ($show_login_end == "") {$show_login_end = null;}
switch ($archive) {
case "1":  // 個人ファイル
case "2":  // 全体共有
    $private_flg = null;
    break;
case "3":  // 部署共有
    $private_flg = ($private_flg1 == "t") ? "t" : "f";
    break;
case "4":  // 委員会・WG共有
    $private_flg = ($private_flg2 == "t") ? "t" : "f";
    break;
}
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// 文書情報を取得
$sql = "select lib_archive, lib_cate_id, folder_id, lib_extension from libinfo";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
$pre_archive = pg_fetch_result($sel, 0, "lib_archive");
$pre_category = pg_fetch_result($sel, 0, "lib_cate_id");
$pre_folder_id = pg_fetch_result($sel, 0, "folder_id");
$pre_extension = pg_fetch_result($sel, 0, "lib_extension");

// 履歴管理をしない場合
if ($manage_history != "t") {

    // 文書情報を更新
    $sql = "update libinfo set";
    $set = array("lib_archive", "lib_cate_id", "lib_nm", "lib_keyword", "lib_summary", "show_login_flg", "show_login_begin", "show_login_end", "folder_id", "lib_type", "private_flg", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "upd_dept_st_flg", "upd_dept_flg", "upd_st_flg", "lib_up_date", "lib_no", "show_mypage_flg");
    $setvalue = array($archive, $category, $document_name, $keywd, $explain, $show_login_flg, $show_login_begin, $show_login_end, $folder_id, $document_type, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, date("YmdHis"), $lib_no, $show_mypage_flg);
    if ($filename != "") {
        $set[] = "lib_extension";
        $setvalue[] = $extension;
    }
    $cond = "where lib_id = $lib_id";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) libcom_goto_error_page_and_die($sql, $con);

// 履歴管理をする場合
} else {
    $org_lib_id = $lib_id;

    // 現在の版情報を取得
    $sql = "select base_lib_id, edition_no from libedition";
    $cond = "where lib_id = $org_lib_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    $base_lib_id = pg_fetch_result($sel, 0, "base_lib_id");
    $edition_no = intval(pg_fetch_result($sel, 0, "edition_no"));

    // 新しい版が登録されていたらエラーとする（主に「戻る」ボタンからの更新を拒否するため）
    $sql = "select max(edition_no) as max_edition_no from libedition";
    $cond = "where base_lib_id = $base_lib_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);
    $max_edition_no = intval(pg_fetch_result($sel, 0, "max_edition_no"));
    if ($max_edition_no > $edition_no) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('新しい版が登録されています。文書一覧から再表示してください。');</script>");
        if ($path == "3") {
            echo("<script type=\"text/javascript\">location.href = 'library_admin_menu.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
        } else {
            echo("<script type=\"text/javascript\">location.href = 'library_list_all.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
        }
        exit;
    }

    // シーケンスで自動採番に変更
    $sql = "select nextval('library_lib_id_seq') as lib_id";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) libcom_goto_error_page_and_die($sql, $con);
    $lib_id = intval(pg_fetch_result($sel, 0, 0));


    // 文書情報を登録
    $sql = "insert into libinfo (lib_archive, lib_id, lib_cate_id, lib_nm, lib_extension, lib_keyword, lib_summary, emp_id, lib_up_date, lib_delete_flag, show_login_flg, show_login_begin, show_login_end, folder_id, lib_type, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg, lib_no, show_mypage_flg) values (";
    $content = array($archive, $lib_id, $category, $document_name, $extension, $keywd, $explain, $emp_id, date("YmdHis"), "f", $show_login_flg, $show_login_begin, $show_login_end, $folder_id, $document_type, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg, $lib_no, $show_mypage_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);

    // 文書参照ログを削除（念のため）
    $sql = "delete from libreflog";
    $cond = "where lib_id = $lib_id";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

    // 版情報を登録
    $sql = "insert into libedition values (";
    $content = array($base_lib_id, $edition_no + 1, $lib_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 参照可能部署情報をDELETE〜INSERT
$sql = "delete from librefdept";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($hid_ref_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = explode("-", $tmp_val);
    $sql = "insert into librefdept (lib_id, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 参照可能役職情報をDELETE〜INSERT
$sql = "delete from librefst";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($ref_st as $tmp_st_id) {
    $sql = "insert into librefst (lib_id, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 参照可能職員情報をDELETE〜INSERT
$sql = "delete from librefemp";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($hid_ref_emp as $tmp_emp_id) {
    $sql = "insert into librefemp (lib_id, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能部署情報をDELETE〜INSERT
$sql = "delete from libupddept";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($hid_upd_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = explode("-", $tmp_val);
    $sql = "insert into libupddept (lib_id, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能役職情報をDELETE〜INSERT
$sql = "delete from libupdst";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($upd_st as $tmp_st_id) {
    $sql = "insert into libupdst (lib_id, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// 更新可能職員情報をDELETE〜INSERT
$sql = "delete from libupdemp";
$cond = "where lib_id = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) libcom_goto_error_page_and_die($sql." ".$cond, $con);

foreach ($hid_upd_emp as $tmp_emp_id) {
    $sql = "insert into libupdemp (lib_id, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) libcom_goto_error_page_and_die($sql, $con);
}

// フォルダ更新日時を更新
if ($category != $pre_category || $folder_id != $pre_folder_id) {
    lib_update_folder_modified($con, $pre_category, $pre_folder_id, $fname, $emp_id, $archive);
}
lib_update_folder_modified($con, $category, $folder_id, $fname, $emp_id, $archive);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// ファイル保存先ディレクトリがなければ作成
switch ($archive) {
case "1":
    $dir_name = "private";
    break;
case "2":
    $dir_name = "all";
    break;
case "3":
    $dir_name = "section";
    break;
case "4":
    $dir_name = "project";
    break;
}
if (!is_dir("docArchive/{$dir_name}")) {
    mkdir("docArchive/{$dir_name}", 0755);
}
if (!is_dir("docArchive/{$dir_name}/cate{$category}")) {
    mkdir("docArchive/{$dir_name}/cate{$category}", 0755);
}

// 履歴管理をしない場合
if ($manage_history != "t") {

    // ファイルを移動
    switch ($pre_archive) {
    case "1":
        $pre_dir_name = "private";
        break;
    case "2":
        $pre_dir_name = "all";
        break;
    case "3":
        $pre_dir_name = "section";
        break;
    case "4":
        $pre_dir_name = "project";
        break;
    }
    if ($filename == "" && ($archive != $pre_archive || $category != $pre_category)) {
        exec("mv docArchive/{$pre_dir_name}/cate{$pre_category}/document{$lib_id}.* docArchive/{$dir_name}/cate{$category}");
    } else if ($filename != "") {
        exec("rm -f docArchive/{$pre_dir_name}/cate{$pre_category}/document{$lib_id}.*");
        copy($upfile, "docArchive/{$dir_name}/cate{$category}/document{$lib_id}.{$extension}");
    }

// 履歴管理をする場合
} else {

    // ファイルを保存
    copy($upfile, "docArchive/{$dir_name}/cate{$category}/document{$lib_id}.{$extension}");
}

// 文書一覧画面に遷移
if ($path == "3") {
    echo("<script type=\"text/javascript\">location.href = 'library_admin_menu.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
} else {
    echo("<script type=\"text/javascript\">location.href = 'library_list_all.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
}
?>
</body>