<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require_once("about_comedix.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

function mbtrim($string) {
	mb_regex_encoding("UTF-8"); // 本当はmb_trim呼び出し以前に1回実行すれば十分
	$whitespace = '[\0\s]';
	$ret = mb_ereg_replace(sprintf('(^%s+|%s+$)', $whitespace, $whitespace), '', $string);
	return $ret;
}


$seq = (int)@$_REQUEST["seq"];

//==========================================================================
// 新規登録処理 または 更新
//==========================================================================
$errmsg = array();
if (@$_REQUEST["mode"] == "regist" || @$_REQUEST["mode"] == "update"){

	$med_name                = mbtrim(@$_REQUEST["edit_med_name"]);
	$hot13_cd                = mbtrim(@$_REQUEST["edit_hot13_cd"]);
	$hot13_cd                = mbtrim(@$_REQUEST["edit_hot13_cd"]);
	$hot7_cd                 = mbtrim(@$_REQUEST["edit_hot7_cd"]);
	$company_cd              = mbtrim(@$_REQUEST["edit_company_cd"]);
	$cyouzai_cd              = mbtrim(@$_REQUEST["edit_cyouzai_cd"]);
	$buturyu_cd              = mbtrim(@$_REQUEST["edit_buturyu_cd"]);
	$jan_cd                  = mbtrim(@$_REQUEST["edit_jan_cd"]);
	$unitary_cd              = mbtrim(@$_REQUEST["edit_unitary_cd"]);
	$yj_cd                   = mbtrim(@$_REQUEST["edit_yj_cd"]);
	$receipt1_cd             = mbtrim(@$_REQUEST["edit_receipt1_cd"]);
	$receipt2_cd             = mbtrim(@$_REQUEST["edit_receipt2_cd"]);
	$kokuji_name             = mbtrim(@$_REQUEST["edit_kokuji_name"]);
	$hanbai_name             = mbtrim(@$_REQUEST["edit_hanbai_name"]);
	$kikaku_unit_name        = mbtrim(@$_REQUEST["edit_kikaku_unit_name"]);
	$update_kubun            = mbtrim(@$_REQUEST["edit_update_kubun"]);
	$update_ymd              = mbtrim(@$_REQUEST["edit_update_ymd"]);
	$used_counter            = mbtrim(@$_REQUEST["edit_used_counter"]);
	$is_visible_at_selector  = mbtrim(@$_REQUEST["edit_is_visible_at_selector"]);
	$units_separated_tab     = mbtrim(@$_REQUEST["edit_units_separated_tab"]);
	$remark                  = mbtrim(@$_REQUEST["edit_remark"]);
	$zaikei_bunrui_cd        = mbtrim(@$_REQUEST["edit_zaikei_bunrui_cd"]);
	$zaikei_bunrui           = mbtrim(@$_REQUEST["edit_zaikei_bunrui"]);
	$seibun_bunrui_cd        = mbtrim(@$_REQUEST["edit_seibun_bunrui_cd"]);
	$seibun_bunrui           = mbtrim(@$_REQUEST["edit_seibun_bunrui"]);
	$yakkou_bunrui_cd        = mbtrim(@$_REQUEST["edit_yakkou_bunrui_cd"]);


	if ($med_name=="") $errmsg[] = "薬品名を指定してください。";
	if ($hot13_cd=="") $errmsg[] = "基準番号を指定してください。";
	else if (strlen($hot13_cd)!=13) $errmsg[] = "基準番号を数値13桁で指定してください。";

	if (!count($errmsg)){

		//==============================
		// 新規登録
		//==============================
		if (@$_REQUEST["mode"] == "regist"){

			$sql = "select count(*) as cnt from sum_med_saiyou_mst where med_name = '".pg_escape_string($med_name)."'";
			$sel = select_from_table($con, $sql,"",$fname);
			if (pg_fetch_result($sel,0,0)) $errmsg[] = "指定した薬品名は、既に登録されています。";

			$sql = "select count(*) as cnt from sum_medmst where hot13_cd = '".pg_escape_string($hot13_cd)."'";
			$sel = select_from_table($con, $sql,"",$fname);
			if (pg_fetch_result($sel,0,0)) $errmsg[] = "指定した基準番号は、既に登録されています。";

			if (!count($errmsg)){

				//==============================
				// 薬品マスタ
				//==============================
				$sql =
					" insert into sum_medmst (".
					" hot13_cd".
					",hot7_cd".
					",company_cd".
					",cyouzai_cd".
					",buturyu_cd".
					",jan_cd".
					",unitary_cd".
					",yj_cd".
					",receipt1_cd".
					",receipt2_cd".
					",kokuji_name".
					",hanbai_name".
					",receipt_name".
					",kikaku_unit_name".
					",wrap_keitai".
					",wrap_unit_num".
					",wrap_unit_unit".
					",wrap_amount_num".
					",wrap_amount_unit".
					",kubun".
					",maker_name".
					",distributor_name".
					",update_kubun".
					",update_ymd".
					",zaikei_bunrui_cd".
					",zaikei_bunrui".
					",seibun_bunrui_cd".
					",seibun_bunrui".
					",yakkou_bunrui_cd".
					",is_user_data".
					" ) values (".
					" '".pg_escape_string($hot13_cd)."'". //  0 基準番号（ＨＯＴコード）
					",'".pg_escape_string($hot7_cd)."'". //  1 処方用番号（ＨＯＴ７）
					",'".pg_escape_string($company_cd)."'". //  2 会社識別用番号
					",'".pg_escape_string($cyouzai_cd)."'". //  3 調剤用番号
					",'".pg_escape_string($buturyu_cd)."'". //  4 物流用番号
					",'".pg_escape_string($jan_cd)."'". //  5 ＪＡＮコード
					",'".pg_escape_string($unitary_cd)."'". //  6 薬価基準収載医薬品コード
					",'".pg_escape_string($yj_cd)."'". //  7 個別医薬品コード
					",'".pg_escape_string($receipt1_cd)."'". //  8 レセプト電算処理システムコード（１）
					",'".pg_escape_string($receipt2_cd)."'". //  9 レセプト電算処理システムコード（２）
					",'".pg_escape_string($kokuji_name)."'". // 10 告示名称
					",''". // 11 販売名
					",''". // 12 レセプト電算処理システム医薬品名
					",'".pg_escape_string($kikaku_unit_name)."'". // 13 規格単位
					",''". // 14 包装形態
					",''". // 15 包装単位数
					",''". // 16 包装単位単位
					",''". // 17 包装総量数
					",''". // 18 包装総量単位
					",''". // 19 区分
					",''". // 20 製造会社
					",''". // 21 販売会社
					",'".pg_escape_string($update_kubun)."'". // 22 更新区分
					",'".pg_escape_string($update_ymd)."'". // 23 更新年月日
					",'".pg_escape_string($zaikei_bunrui_cd)."'". // zaikei_bunrui_cd".
					",'".pg_escape_string($zaikei_bunrui)."'". // zaikei_bunrui".
					",'".pg_escape_string($seibun_bunrui_cd)."'". // seibun_bunrui_cd".
					",'".pg_escape_string($seibun_bunrui)."'". // seibun_bunrui".
					",'".pg_escape_string($yakkou_bunrui_cd)."'". // yakkou_bunrui_cd".
					",1". // is_user_data
					")";
				$upd = update_set_table($con, $sql, array(), null, "", $fname);

				//==============================
				// 採用薬マスタ
				//==============================
				$sel = select_from_table($con, "select nextval('sum_med_saiyou_mst_seq')", "", $fname);
				$seq = (int)pg_fetch_result($sel, 0, 0);

				$sql =
					" insert into sum_med_saiyou_mst (".
					" seq".
					",med_name".
					",hot13_cd".
					",kokuji_name".
					",seibun_bunrui".
					",yakkou_bunrui_cd".
					",is_visible_at_selector".
					",remark".
					",units_separated_tab".
					",used_counter".
					",sort_order".
					",is_user_data".
					",yj_cd".
					" ) values (".
					" ".$seq.
					",'".pg_escape_string($med_name)."'". //  med_name
					",'".pg_escape_string($hot13_cd)."'". //  hot13_cd
					",'".pg_escape_string($kokuji_name)."'". //  kokuji_name
					",'".pg_escape_string($seibun_bunrui)."'". // seibun_bunrui".
					",'".pg_escape_string($yakkou_bunrui_cd)."'". //  yakkou_bunrui_cd
					",".(int)$is_visible_at_selector. //  is_visible_at_selector
					",'".pg_escape_string($remark)."'". //  remark
					",'".pg_escape_string($units_separated_tab)."'". //  units_separated_tab
					",".(int)$used_counter. //  used_counter
					",0". //  sort_order
					",1". // is_user_data
					",'".pg_escape_string($yj_cd)."'". //  yj_cd
					")";
				$upd = update_set_table($con, $sql, array(), null, "", $fname);

				header("Location: sum_ordsc_med_regist.php?session=".$session."&mode=finished&seq=".$seq);
				die;
			}
		}


		//==============================
		// 更新
		//==============================
		if (@$_REQUEST["mode"] == "update"){

			$sql = "select count(*) as cnt from sum_med_saiyou_mst where seq = '".pg_escape_string($seq)."'";
			$sel = select_from_table($con, $sql,"",$fname);
			if (!pg_fetch_result($sel,0,0)) $errmsg[] = "指定した薬品登録が見つかりませんでした。";

			$sql = "select count(*) as cnt from sum_medmst where hot13_cd = '".pg_escape_string($hot13_cd)."'";
			$sel = select_from_table($con, $sql,"",$fname);
			if (!pg_fetch_result($sel,0,0)) $errmsg[] = "指定した基準番号の薬品が見つかりませんでした。(".$hot13_cd.")";

			if (!count($errmsg)){

				//==============================
				// 薬品マスタ
				//==============================
				$sql =
					" update sum_medmst set".
					" hot7_cd = ".             "'".pg_escape_string($hot7_cd)."'". //  1 処方用番号（ＨＯＴ７）
					",company_cd = ".          "'".pg_escape_string($company_cd)."'". //  2 会社識別用番号
	//				",cyouzai_cd = ".          "'".pg_escape_string($cyouzai_cd)."'". //  3 調剤用番号
	//				",buturyu_cd = ".          "'".pg_escape_string($buturyu_cd)."'". //  4 物流用番号
					",jan_cd = ".              "'".pg_escape_string($jan_cd)."'". //  5 ＪＡＮコード
					",unitary_cd = ".          "'".pg_escape_string($unitary_cd)."'". //  6 薬価基準収載医薬品コード
					",yj_cd = ".               "'".pg_escape_string($yj_cd)."'". //  7 個別医薬品コード
					",receipt1_cd = ".         "'".pg_escape_string($receipt1_cd)."'". //  8 レセプト電算処理システムコード（１）
					",receipt2_cd = ".         "'".pg_escape_string($receipt2_cd)."'". //  9 レセプト電算処理システムコード（２）
					",kikaku_unit_name = ".    "'".pg_escape_string($kikaku_unit_name)."'". // 13 規格単位
	//				",update_kubun = ".        "'".pg_escape_string($update_kubun)."'". // 22 更新区分
	//				",update_ymd = ".          "'".pg_escape_string($update_ymd)."'". // 23 更新年月日
					",zaikei_bunrui_cd = ".    "'".pg_escape_string($zaikei_bunrui_cd)."'". // zaikei_bunrui_cd".
					",zaikei_bunrui = ".       "'".pg_escape_string($zaikei_bunrui)."'". // zaikei_bunrui".
					",seibun_bunrui_cd = ".    "'".pg_escape_string($seibun_bunrui_cd)."'". // seibun_bunrui_cd".
					",seibun_bunrui = ".       "'".pg_escape_string($seibun_bunrui)."'". // seibun_bunrui".
					",yakkou_bunrui_cd = ".    "'".pg_escape_string($yakkou_bunrui_cd)."'". // yakkou_bunrui_cd".
					" where hot13_cd = ".      "'".pg_escape_string($hot13_cd)."'";
				$upd = update_set_table($con, $sql, array(), null, "", $fname);

				//==============================
				// 採用薬マスタ
				//==============================
				$sql =
					" update sum_med_saiyou_mst set".
					" med_name = ".                "'".pg_escape_string($med_name)."'". //  med_name
					",hot13_cd = ".                "'".pg_escape_string($hot13_cd)."'". //  hot13_cd
					",kokuji_name = ".             "'".pg_escape_string($kokuji_name)."'". //  kokuji_name
					",seibun_bunrui = ".           "'".pg_escape_string($seibun_bunrui)."'". // seibun_bunrui".
					",yakkou_bunrui_cd = ".        "'".pg_escape_string($yakkou_bunrui_cd)."'". //  yakkou_bunrui_cd
					",is_visible_at_selector = ".  "".(int)$is_visible_at_selector. //  is_visible_at_selector
					",remark = ".                  "'".pg_escape_string($remark)."'". //  remark
					",units_separated_tab = ".     "'".pg_escape_string($units_separated_tab)."'". //  units_separated_tab
					",used_counter = ".            "".(int)$used_counter. //  used_counter
					",yj_cd = ".                   "'".pg_escape_string($yj_cd)."'". //  yj_cd
					" where seq = " . $seq;
				$upd = update_set_table($con, $sql, array(), null, "", $fname);

				header("Location: sum_ordsc_med_regist.php?session=".$session."&mode=finished&seq=".$seq);
				die;
			}
		}

	}
}



//==========================================================================
// データ取得
//==========================================================================
if (@$_REQUEST["mode"] == "edit" || @$_REQUEST["mode"] == "finished"){
	$sql = "select * from sum_med_saiyou_mst where seq = " . $seq;
	$sel_med_saiyou_mst = select_from_table($con, $sql, "", $fname);
	$rs_med_saiyou_mst = pg_fetch_array($sel_med_saiyou_mst);

	$hot13_cd = @$_REQUEST["new_hot13_cd"];
	if (!$hot13_cd) $hot13_cd = @$rs_med_saiyou_mst["hot13_cd"];
	$sql = "select * from sum_medmst where hot13_cd = '" . pg_escape_string($hot13_cd) . "'";
	$sel_medmst = select_from_table($con, $sql, "", $fname);
	$rs_medmst = pg_fetch_array($sel_medmst);

	foreach($rs_med_saiyou_mst as $field_name => $field_value){
		$_REQUEST["edit_".$field_name] = $rs_med_saiyou_mst[$field_name];
	}
	foreach($rs_medmst as $field_name => $field_value){
		$_REQUEST["edit_".$field_name] = $rs_medmst[$field_name];
	}

	$_REQUEST["edit_zaikei_bunrui_disp"] = "";
	if ($_REQUEST["edit_zaikei_bunrui"] == "S") $_REQUEST["edit_zaikei_bunrui_disp"] = "散剤";
	if ($_REQUEST["edit_zaikei_bunrui"] == "J") $_REQUEST["edit_zaikei_bunrui_disp"] = "錠剤";
	if ($_REQUEST["edit_zaikei_bunrui"] == "C") $_REQUEST["edit_zaikei_bunrui_disp"] = "カプセル";
	if ($_REQUEST["edit_zaikei_bunrui"] == "E") $_REQUEST["edit_zaikei_bunrui_disp"] = "液剤";
	if ($_REQUEST["edit_zaikei_bunrui"] == "Z") $_REQUEST["edit_zaikei_bunrui_disp"] = "その他";

	$_REQUEST["edit_seibun_bunrui_disp"] = "";
	if ($_REQUEST["edit_seibun_bunrui"] == "N") $_REQUEST["edit_seibun_bunrui_disp"] = "内服薬";
	if ($_REQUEST["edit_seibun_bunrui"] == "C") $_REQUEST["edit_seibun_bunrui_disp"] = "注射薬";
	if ($_REQUEST["edit_seibun_bunrui"] == "G") $_REQUEST["edit_seibun_bunrui_disp"] = "外用薬";


			$seibun_bunrui_cd = substr($csv[6],4,3);
			$seibun_bunrui = "";
			if ($seibun_bunrui_cd >= 1   && $seibun_bunrui_cd <= 399) $seibun_bunrui = "N";
			if ($seibun_bunrui_cd >= 400 && $seibun_bunrui_cd <= 699) $seibun_bunrui = "C";
			if ($seibun_bunrui_cd >= 700 && $seibun_bunrui_cd <= 999) $seibun_bunrui = "G";
			$yakkou_bunrui_cd = substr($csv[6],0,3);

}


//==========================================================================
// 新規登録用の呼び出し
//==========================================================================
if (@$_REQUEST["mode"] == ""){
	$_REQUEST["edit_used_counter"] = 1;
	$_REQUEST["edit_is_visible_at_selector"] = 1;
}







$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';



// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list1 {border-collapse:collapse;}
.list1 td { border:#5279a5 solid 1px; text-align:left; white-space:nowrap; background-color:#eee }
.list1 th {  border:#5279a5 solid 1px; text-align:left; font-weight:normal; white-space:nowrap; background-color:#e3ecf4; padding:3px }

.subtitle { font-weight:bold; }

.noime { ime-mode:disabled }
.readonly { border:0; }

.editbox { background-color:#fff; }
+
.info { color:#de1bc6; font-size:12px }

</style>
<script type="text/javascript">
	//==========================
	// 薬品選択画面をポップアップ
	//==========================
	var med_selector_window = null;
	function medSelectorWindowPopup(){
		var enc_str = encodeURIComponent(encodeURIComponent(document.getElementById("edit_med_name").value));
		//var enc_str = encodeURIComponent( document.getElementById("edit_med_name").value );
		if (med_selector_window && !med_selector_window.closed) med_selector_window.focus();
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,width=850,height=650";
		var url = "sum_ordsc_med_selector.php?session=<?=$session?>&js_callback=medSelectorCallback&sel_med_name="+enc_str;
		med_selector_window = window.open(url, 'med_selector_window', option);
		setTimeout("if (med_selector_window && !med_selector_window.closed) med_selector_window.focus();", 100);
	}

	function medSelectorCallback(hot13_cd){
		document.frm.new_hot13_cd.value = hot13_cd;
		document.frm.mode.value = "edit";
		document.frm.submit();
	}
</script>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">


<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//==========================================================================?>
<?// 処方・注射メニュー                                                       ?>
<?//==========================================================================?>
<?= summary_common_show_ordsc_right_menu($session, "薬品")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//==========================================================================?>
<?// サブメニュー                                                             ?>
<?//==========================================================================?>

<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>薬品名一覧</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_med_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>新規登録/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>インポート</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_saiyou_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>採用薬一括登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>




<div><?=$font?>
<script type="text/javascript">
	function mbtrim(s) { return s.replace(/^[ 　]+/,"").replace(/[ 　]+$/,""); }
	function isNumber(s) { return s.match(/[0-9]+/g); }
	function isUnitalyAlpha(s) { return "ABCDEFGHIJKLMNOPQRSTX".indexOf(s,0) >= 0; }
	function addErr(msg) { errmsg[errmsg.length] = msg; }
	function isEmpty(s) { return s=="" ? true: false; }

	function changedUnitaryCd(unitary_cd){
		var fields = ["zaikei_bunrui_cd", "zaikei_bunrui", "zaikei_bunrui_disp", "seibun_bunrui_cd", "seibun_bunrui", "seibun_bunrui_disp", "yakkou_bunrui_cd"];
		for (var i in fields){
			eval("var elem_" + fields[i] + " = document.getElementById('edit_"+fields[i]+"');");
			eval("var isExist = elem_"+fields[i]+" ? true : false;");
			if (!isExist) { alert("アイテムが確認できませんでした。(edit_"+fields[i]+")"); return; }
			eval("elem_" + fields[i] + ".value = ''");
		}

		if (isEmpty(unitary_cd)) return;
		var len = unitary_cd.length;
		if (len >=3){
			elem_yakkou_bunrui_cd.value = unitary_cd.substr(0,3);
		}
		if (len >=7){
			var cd = unitary_cd.substr(4,3);
			elem_seibun_bunrui_cd.value = cd;
			if (cd >= 1   && cd <= 399) { elem_seibun_bunrui.value = "N"; elem_seibun_bunrui_disp.value = "内服薬"; }
			if (cd >= 400 && cd <= 699) { elem_seibun_bunrui.value = "C"; elem_seibun_bunrui_disp.value = "注射薬"; }
			if (cd >= 700 && cd <= 999) { elem_seibun_bunrui.value = "G"; elem_seibun_bunrui_disp.value = "外用薬"; }
		}
		if (len >=8){
			var cd = unitary_cd.substr(7,1);
			elem_zaikei_bunrui_cd.value = cd;
			if      (cd >= 'A' && cd <= 'E') { elem_zaikei_bunrui.value = "S"; elem_zaikei_bunrui_disp.value = "散剤"; }
			else if (cd >= 'F' && cd <= 'L') { elem_zaikei_bunrui.value = "J"; elem_zaikei_bunrui_disp.value = "錠剤"; }
			else if (cd >= 'M' && cd <= 'P') { elem_zaikei_bunrui.value = "C"; elem_zaikei_bunrui_disp.value = "カプセル"; }
			else if (cd >= 'Q' && cd <= 'S') { elem_zaikei_bunrui.value = "E"; elem_zaikei_bunrui_disp.value = "液剤"; }
			else if (cd == 'T' || cd == 'X') { elem_zaikei_bunrui.value = "Z"; elem_zaikei_bunrui_disp.value = "その他"; }
		}
	}

	var errmsg = [];
  function checkAndSubmit(mode){
		errmsg = [];
		var fields = [
			"med_name", "hot13_cd", "hot7_cd", "company_cd", "jan_cd", "unitary_cd",
			"yj_cd", "receipt1_cd", "receipt2_cd", "kikaku_unit_name", "update_kubun", "update_ymd", "used_counter", "is_visible_at_selector",
			"units_separated_tab", "remark", "zaikei_bunrui_cd", "zaikei_bunrui", "seibun_bunrui_cd", "seibun_bunrui", "yakkou_bunrui_cd"
//			, "cyouzai_cd", "buturyu_cd"
		];
		for (var i in fields){
			eval("var elem_" + fields[i] + " = document.getElementById('edit_"+fields[i]+"');");
			eval("var isExist = elem_"+fields[i]+" ? true : false;");
			if (!isExist) { alert("アイテムが確認できませんでした。(edit_"+fields[i]+")"); return; }
			eval("var " + fields[i] + " = mbtrim(elem_" + fields[i] + ".value);");
			eval("elem_" + fields[i] + ".value = " + fields[i]);
		}

		if (isEmpty(med_name)) addErr("薬品名を指定してください。");

		if (isEmpty(hot13_cd)) addErr("基準番号を指定してください。");
		else if (hot13_cd.length!=13) addErr("基準番号(HOT13)は13文字で指定してください。");
		else if (!isNumber(hot13_cd)) addErr("基準番号(HOT13)は数値のみで指定してください。");

		if      (!isEmpty(hot7_cd) && hot7_cd.length!=7)  addErr("処方用番号(HOT7)は7文字で指定してください。");
		else if (!isEmpty(hot7_cd) && !isNumber(hot7_cd)) addErr("処方用番号(HOT7)は数値のみで指定してください。");

		if      (!isEmpty(company_cd) && company_cd.length!=2)  addErr("会社識別番号は2文字で指定してください。");
		else if (!isEmpty(company_cd) && !isNumber(company_cd)) addErr("会社識別番号は数値のみで指定してください。");

//		if      (!isEmpty(cyouzai_cd) && cyouzai_cd.length!=2)  addErr("調剤用番号は2文字で指定してください。");
//		else if (!isEmpty(cyouzai_cd) && !isNumber(cyouzai_cd)) addErr("調剤用番号は数値のみで指定してください。");

//		if      (!isEmpty(buturyu_cd) && buturyu_cd.length!=2)  addErr("物流用番号は2文字で指定してください。");
//		else if (!isEmpty(buturyu_cd) && !isNumber(buturyu_cd)) addErr("物流用番号は数値のみで指定してください。");

		if      (!isEmpty(jan_cd) && jan_cd.length!=13)  addErr("JANコードは13文字で指定してください。");
		else if (!isEmpty(jan_cd) && !isNumber(jan_cd))  addErr("JANコードは数値のみで指定してください。");

		if (isEmpty(unitary_cd)) addErr("薬価基準収載医薬品コードを指定してください。");
		else if (unitary_cd.length!=12) addErr("薬価基準収載医薬品コードは12文字で指定してください。");
		else if (!isNumber(unitary_cd.substr(0,7))) addErr("薬価基準収載医薬品コードの先頭7文字は数値のみで指定してください。");
		else if (!isUnitalyAlpha(unitary_cd.substr(7,1))) addErr("薬価基準収載医薬品コードの先頭8文字目は所定の大文字アルファベットを指定してください。");
		else if (!isNumber(unitary_cd.substr(8))) addErr("薬価基準収載医薬品コードの9文字目以降は数値のみで指定してください。");

		if      (!isEmpty(yj_cd) && yj_cd.length!=12)  addErr("個別医薬品コードは12文字で指定してください。");
		else if (!isEmpty(yj_cd) && !isNumber(yj_cd)) addErr("個別医薬品コードは数値のみで指定してください。");

		if      (!isEmpty(receipt1_cd) && receipt1_cd.length!=9)  addErr("レセプト電算処理システムコード（１）は9文字で指定してください。");
		else if (!isEmpty(receipt1_cd) && !isNumber(receipt1_cd)) addErr("レセプト電算処理システムコード（１）は数値のみで指定してください。");

		if      (!isEmpty(receipt2_cd) && receipt2_cd.length!=9)  addErr("レセプト電算処理システムコード（２）は9文字で指定してください。");
		else if (!isEmpty(receipt2_cd) && !isNumber(receipt2_cd)) addErr("レセプト電算処理システムコード（２）は数値のみで指定してください。");

		if (errmsg.length) {
			alert(errmsg.join("\n"));
			return;
		}

		document.frm.mode.value = mode;
    document.frm.submit();
  }
</script>

<? if ($_REQUEST["mode"]=="finished") { ?>
<div style="margin-top:10px; padding:8px; background-color:#fcffbf">登録しました。</div>
<? } ?>

<? if (count($errmsg)) { ?>
<div style="margin-top:10px">■エラーが発生しました。</div>
<div style="margin-top:4px; padding:8px; background-color:#ffbfd9"><?=implode($errmsg,"<br>")?></div>
<? } ?>

<form action="sum_ordsc_med_regist.php" method="get" name="frm" style="margin-top:10px">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="<?=@$_REQUEST["mode"]?>">
	<input type="hidden" name="seq" value="<?=$seq?>">
	<input type="hidden" name="new_hot13_cd" value="">

	<table style="border-collapse:collapse;"><tr><td style="vertical-align:top">

		<div class="subtitle"><?=$font?>■基本情報</font></div>



		<table class="list1">
			<tr>
				<th><?=$font?>基準番号（ＨＯＴコード）</font></th>
				<td>
					<? if (@$_REQUEST["mode"]==""){ ?>
					<input type="text" name="edit_hot13_cd" id="edit_hot13_cd" value="<?=h(@$_REQUEST["edit_hot13_cd"])?>" class="editbox noime" style="width:150px" maxlength="13" />
					<? } else { ?>
					<input type="text" name="edit_hot13_cd" id="edit_hot13_cd" value="<?=h(@$_REQUEST["edit_hot13_cd"])?>" class="readonly" style="width:150px" readonly />
					<? } ?>
					<input type="button" value="…" onclick="medSelectorWindowPopup()" />
				</td>
			</tr>
		</table>

		<table class="list1" style="margin-top:10px">
			<tr>
				<th><?=$font?>処方用番号(HOT7)</font></th>
				<td><input type="text" name="edit_hot7_cd" id="edit_hot7_cd" value="<?=h(@$_REQUEST["edit_hot7_cd"])?>" class="editbox noime" style="width:150px" maxlength="7" /></td>
			</tr>
			<tr>
				<th><?=$font?>会社識別用番号</font></th>
				<td><input type="text" name="edit_company_cd" id="edit_company_cd" value="<?=h(@$_REQUEST["edit_company_cd"])?>" class="editbox noime" style="width:150px" maxlength="2" /></td>
			</tr>
			<!--tr>
				<th><?=$font?>調剤用番号</font></th>
				<td><input type="text" name="edit_cyouzai_cd" id="edit_cyouzai_cd" value="<?=h(@$_REQUEST["edit_cyouzai_cd"])?>" class="editbox noime" style="width:150px" maxlength="2" /></td>
			</tr-->
			<!--tr>
				<th><?=$font?>物流用番号</font></th>
				<td><input type="text" name="edit_buturyu_cd" id="edit_buturyu_cd" value="<?=h(@$_REQUEST["edit_buturyu_cd"])?>" class="editbox noime" style="width:150px" maxlength="2" /></td>
			</tr-->
			<tr>
				<th><?=$font?>JANコード</font></th>
				<td><input type="text" name="edit_jan_cd" id="edit_jan_cd" value="<?=h(@$_REQUEST["edit_jan_cd"])?>" class="editbox noime" style="width:150px" maxlength="13" /></td>
			</tr>
			<tr>
				<th><?=$font?>薬価基準収載医薬品コード</font></th>
				<td>
				<input type="text" name="edit_unitary_cd" id="edit_unitary_cd" value="<?=h(@$_REQUEST["edit_unitary_cd"])?>" class="editbox noime" style="width:150px" maxlength="12"
				 onchange="changedUnitaryCd(this.value);" />
				 </td>
			</tr>
			<tr>
				<th><?=$font?>個別医薬品コード(YJコード）</font></th>
				<td><input type="text" name="edit_yj_cd" id="edit_yj_cd" value="<?=h(@$_REQUEST["edit_yj_cd"])?>" class="editbox noime" style="width:150px" maxlength="12" /></td>
			</tr>
			<tr>
				<th><?=$font?>レセプト電算処理システムコード（１）</font></th>
				<td><input type="text" name="edit_receipt1_cd" id="edit_receipt1_cd" value="<?=h(@$_REQUEST["edit_receipt1_cd"])?>" class="editbox noime" style="width:150px" maxlength="9" /></td>
			</tr>
			<tr>
				<th><?=$font?>レセプト電算処理システムコード（２）</font></th>
				<td><input type="text" name="edit_receipt2_cd" id="edit_receipt2_cd" value="<?=h(@$_REQUEST["edit_receipt2_cd"])?>" class="editbox noime" style="width:150px" maxlength="9" /></td>
			</tr>
			<!--tr>
				<th><?=$font?>告示名称</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>販売名</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>レセプト電算処理システム医薬品名</font></th>
			</tr-->
			<tr>
				<th><?=$font?>規格単位</font></th>
				<td><input type="text" name="edit_kikaku_unit_name" id="edit_kikaku_unit_name" value="<?=h(@$_REQUEST["edit_kikaku_unit_name"])?>" class="editbox" style="width:200px" maxlength="100" /></td>
			</tr>
			<!--tr>
				<th><?=$font?>包装形態</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>包装単位数</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>包装単位単位</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>包装総量数</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>包装総量単位</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>区分</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>製造会社</font></th>
			</tr-->
			<!--tr>
				<th><?=$font?>販売会社</font></th>
			</tr-->
			<tr>
				<th><?=$font?>更新区分</font></th>
				<td><input type="text" name="edit_update_kubun" id="edit_update_kubun" value="<?=h(@$_REQUEST["edit_update_kubun"])?>" style="width:50px" class="readonly" maxlength="1" readonly /></td>
			</tr>
			<tr>
				<th><?=$font?>更新年月日</font></th>
				<td><input type="text" name="edit_update_ymd" id="edit_update_ymd" value="<?=h(@$_REQUEST["edit_update_ymd"])?>" style="width:150px" class="readonly" maxlength="8" readonly /></td>
			</tr>
			<tr>
				<th><?=$font?>告示名称</font></th>
				<td><input type="text" name="edit_kokuji_name" class="readonly" id="edit_kokuji_name" value="<?=h(@$_REQUEST["edit_kokuji_name"])?>" style="width:200px" readonly /></td>
			</tr>
			<tr>
				<th><?=$font?>販売名</font></th>
				<td><input type="text" name="edit_hanbai_name" class="readonly" id="edit_hanbai_name" value="<?=h(@$_REQUEST["edit_hanbai_name"])?>" style="width:200px" readonly /></td>
			</tr>
		</table>


	</td><td style="padding-left:12px; vertical-align:top">

		<div class="subtitle"><?=$font?>■付加情報</font></div>

		<table class="list1">
			<tr>
				<th><?=$font?>薬品名(販売名)</font></th>
				<td><input type="text" name="edit_med_name" id="edit_med_name" value="<?=h(@$_REQUEST["edit_med_name"])?>" class="editbox" style="width:300px" maxlength="200" /></td>
			</tr>
		</table>

		<table class="list1" style="margin-top:10px">
			<tr>
				<th><?=$font?>採用薬</font></th>
				<td><?=$font?>
					<label><input type="checkbox" name="edit_used_counter" id="edit_used_counter" value="1" <?=@$_REQUEST["edit_used_counter"]?"checked":""?> />採用薬</label><br/>
				</font></td>
			</tr>
			<tr>
				<th><?=$font?>選択可</font></th>
				<td><?=$font?>
					<label><input type="checkbox" name="edit_is_visible_at_selector" id="edit_is_visible_at_selector" value="1" <?=@$_REQUEST["edit_is_visible_at_selector"]?"checked":""?> />選択可</label><br/>
					<span class="info">
						※「選択可」であれば、採用薬でなくても、処方/注射オーダ時に<br/>
						　　利用することができます。<br/>
						※利用した薬品は、自動的に「採用薬」として登録されます。</span>
				</font></td>
			</tr>
			<tr>
				<th><?=$font?>単位</font></th>
				<td>
					<input type="text" name="edit_units_separated_tab" id="edit_units_separated_tab" value="<?=h(@$_REQUEST["edit_units_separated_tab"])?>" class="editbox" style="width:150px" maxlength="200" />
					<span class="info">※カンマ区切り</span>
				</td>
			</tr>
			<tr>
				<th><?=$font?>備考</font></th>
				<td><input type="text" name="edit_remark" id="edit_remark" value="<?=h(@$_REQUEST["edit_remark"])?>" class="editbox" style="width:300px" maxlength="300" /></td>
			</tr>
		</table>


		<div class="subtitle" style="margin-top:12px"><?=$font?>■自動判定（薬価基準収載医薬品コード要指定）</font></div>

		<table class="list1">
			<tr>
				<th><?=$font?>剤形分類コード</font></th>
				<td><input type="text" name="edit_zaikei_bunrui_cd" id="edit_zaikei_bunrui_cd" value="<?=h(@$_REQUEST["edit_zaikei_bunrui_cd"])?>" class="readonly" readonly style="width:100px" /></td>
				<th><?=$font?>剤形分類</font></th>
				<td>
					<input type="text" name="edit_zaikei_bunrui_disp" id="edit_zaikei_bunrui_disp" value="<?=h(@$_REQUEST["edit_zaikei_bunrui_disp"])?>" class="readonly" readonly style="width:100px" />
					<input type="hidden" name="edit_zaikei_bunrui" id="edit_zaikei_bunrui" value="<?=h(@$_REQUEST["edit_zaikei_bunrui"])?>" />
				</td>
			</tr>
			<tr>
				<th><?=$font?>成分分類コード</font></th>
				<td><input type="text" name="edit_seibun_bunrui_cd" id="edit_seibun_bunrui_cd" value="<?=h(@$_REQUEST["edit_seibun_bunrui_cd"])?>" class="readonly" readonly style="width:100px" /></td>
				<th><?=$font?>成分分類</font></th>
				<td>
					<input type="text" name="edit_seibun_bunrui_disp" id="edit_seibun_bunrui_disp" value="<?=h(@$_REQUEST["edit_seibun_bunrui_disp"])?>" class="readonly" readonly style="width:100px" />
					<input type="hidden" name="edit_seibun_bunrui" id="edit_seibun_bunrui" value="<?=h(@$_REQUEST["edit_seibun_bunrui"])?>" />
				</td>
			</tr>
			<tr>
				<th><?=$font?>薬効分類コード</font></th>
				<td colspan="3"><input type="text" name="edit_yakkou_bunrui_cd" id="edit_yakkou_bunrui_cd" value="<?=h(@$_REQUEST["edit_yakkou_bunrui_cd"])?>" class="readonly" readonly style="width:100px" /></td>
			</tr>
		</table>

	</td></tr></table>

	<div style="text-align:center; margin-top:10px">
		<? if (@$_REQUEST["mode"]==""){ ?>
			<input type="button" value="新規登録" onclick="checkAndSubmit('regist');" />
		<? } else { ?>
			<input type="button" value="更新" onclick="checkAndSubmit('update');" />
		<? } ?>
	</div>


</form>
</font></div>



</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
