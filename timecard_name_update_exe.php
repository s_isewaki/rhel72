<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
foreach ($group_names as $i => $group_name) {
	$group_id = $i + 1;

	if ($group_name == "") {
		echo("<script type=\"text/javascript\">alert('グループ名{$group_id}を入力してください。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	if (strlen($group_name) > 30) {
		echo("<script type=\"text/javascript\">alert('グループ名{$group_id}が長すぎます。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// グループ名をDELETE〜INSERT
$sql = "delete from wktmgrp";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
foreach ($group_names as $i => $group_name) {
	$sql = "insert into wktmgrp (group_id, group_name) values (";
	$content = array($i + 1, $group_name);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 出勤パターン名画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_name.php?session=$session';</script>");
?>
