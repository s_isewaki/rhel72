<?
require("about_comedix.php");
require("get_values.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// オプション情報が存在するかどうかチェック
$sql = "select count(*) from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$option_exists = (pg_fetch_result($sel, 0, 0) > 0);

// 登録値の編集
if ($mail_flg != "t") {
    $mail_flg = "f";
}
if ($ext_flg != "t") {
    $ext_flg = "f";
}
if ($inci_flg != "t") {
    $inci_flg = "f";
}
if ($fplus_flg != "t") {
    $fplus_flg = "f";
}
if ($manabu_flg != "t") {
    $manabu_flg = "f";
}
if ($cas_flg != "t") {
    $cas_flg = "f";
}
if ($event_flg != "t") {
    $event_flg = "f";
}
if ($schd_flg != "t") {
    $schd_flg = "f";
}
if ($info_flg != "t") {
    $info_flg = "f";
}
if ($task_flg != "t") {
    $task_flg = "f";
}
if ($whatsnew_flg != "t") {
    $whatsnew_flg = "f";
}
if ($whatsnew_flg == "t") {
    $msg_flg = "t";
    $aprv_flg = "t";
}
else {
    $msg_flg = "f";
    $aprv_flg = "f";
}
if ($schdsrch_flg != "t") {
    $schdsrch_flg = "f";
}
if ($fcl_flg != "t") {
    $fcl_flg = "f";
}
if ($lib_flg != "t") {
    $lib_flg = "f";
}
if ($bbs_flg != "t") {
    $bbs_flg = "f";
}
if ($intra_flg != "t") {
    $intra_flg = "f";
}
if ($bedinfo != "t") {
    $bedinfo = "f";
}
if ($schd_type != "t") {
    $schd_type = "f";
}
if ($wic_flg != "t") {
    $wic_flg = "f";
}
if ($logo_type == "") {
    $logo_type = "1";
}

if ($jnl_flg != "t") {
    $jnl_flg = "f";
}

if ($top_free_text_flg != "t") {
    $top_free_text_flg = "f";
}

if ($ladder_flg != "t") {
    $ladder_flg = "f";
}

if ($top_timecard_error_flg != "t") {
    $top_timecard_error_flg = "f";
}

if ($top_workflow_flg != "t") {
    $top_workflow_flg = "f";
}
if ($top_ccusr1_flg != "t") {
    $top_ccusr1_flg = "f";
}

// オプション情報があれば更新
if ($option_exists) {
    $sql = "update option set";
    $set = array("default_page", "logo_type", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_ladder_flg", "top_timecard_error_flg", "top_workflow_flg", "top_ccusr1_flg");
    $setvalue = array($default_page, $logo_type, $font_size, $mail_flg, $ext_flg, $event_flg, $schd_flg, $info_flg, $task_flg, $msg_flg, $aprv_flg, $schdsrch_flg, $lib_flg, $bbs_flg, $memo_flg, $link_flg, $intra_flg, $bedinfo, $schd_type, $wic_flg, $fcl_flg, $inci_flg, $cas_flg, $fplus_flg, $jnl_flg, $top_free_text_flg, $manabu_flg, $ladder_flg, $top_timecard_error_flg, $top_workflow_flg, $top_ccusr1_flg);
    $cond = "where emp_id = '$emp_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

}
// オプション情報がなければ作成
else {
    $sql = "insert into option (emp_id, schedule1_default, schedule2_default, default_page, logo_type, font_size, top_mail_flg, top_ext_flg, top_event_flg, top_schd_flg, top_info_flg, top_task_flg, top_msg_flg, top_aprv_flg, top_schdsrch_flg, top_lib_flg, top_bbs_flg, top_memo_flg, top_link_flg, top_intra_flg, bed_info, schd_type, top_wic_flg, top_fcl_flg, top_inci_flg, top_cas_flg, top_fplus_flg, top_jnl_flg, top_free_text_flg, top_manabu_flg, top_ladder_flg, top_timecard_error_flg, top_workflow_flg, top_ccusr1_flg) values (";
    $content = array($emp_id, "2", "2", $default_page, $logo_type, $font_size, $mail_flg, $ext_flg, $event_flg, $schd_flg, $info_flg, $task_flg, $msg_flg, $aprv_flg, $schdsrch_flg, $lib_flg, $bbs_flg, $memo_flg, $link_flg, $intra_flg, $bedinfo, $schd_type, $wic_flg, $fcl_flg, $inci_flg, $cas_flg, $fplus_flg, $jnl_flg, $top_free_text_flg, $manabu_flg, $ladder_flg, $top_timecard_error_flg, $top_workflow_flg, $top_ccusr1_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 全画面をリフレッシュ
$f_url = urlencode("option_register.php?session=$session");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");
