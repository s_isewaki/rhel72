<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 勤務時間集計</title>
<?
//ini_set("display_errors", "1");
ini_set("max_execution_time", "0");

require_once("about_comedix.php");
require_once("show_timecard_common.ini");
require_once("referer_common.ini");
require_once("show_attendance_pattern.ini");
require_once("holiday.php");
require_once("work_admin_menu_common.ini");
require_once("timecard_common_class.php");
require_once("date_utils.php");
require_once("atdbk_common_class.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("calendar_name_class.php");
require_once("atdbk_info_common.php");
require_once("ovtm_class.php");
require_once("atdbk_menu_common.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
$url_srch_name = urlencode($srch_name);
$url_emp_ids = "";
foreach ($emp_id as $tmp_id) {
	$url_emp_ids .= "&emp_id[]=$tmp_id";
}

// データベースに接続
$con = connect2db($fname);

//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//残業申請関連クラス
$ovtm_class = new ovtm_class($con, $fname);


// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 締め日を取得
// ************ oose update start *****************
//$sql = "select closing from timecard";
$sql = "select closing, closing_parttime, closing_month_flg from timecard";
// ************ oose update end *****************
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
// ************ oose add start *****************
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}

// 締め日が未登録の場合はエラーとする
if (($closing == "") || ($closing_parttime == "")) {
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}
// ************ oose add end *****************
$closing_month_flg = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_month_flg") : "1";

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		//前月とする 20140925
		$month--;
		if ($month <= 0) {
			$year--;
			$month = 12;
		}
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
// ************ oose add start *****************
	$start_date_fulltime = $start_date;
	$end_date_fulltime = $end_date;
	$start_year_fulltime = substr($start_date_fulltime,0,4);
	$start_month_fulltime = substr($start_date_fulltime,4,2);
	$start_day_fulltime = substr($start_date_fulltime,6,2);
	$end_year_fulltime = substr($end_date_fulltime,0,4);
	$end_month_fulltime = substr($end_date_fulltime,4,2);
	$end_day_fulltime = substr($end_date_fulltime,6,2);
// ************ oose add end *****************
	//月度対応
	//月が未指定の場合
	if (!$month_set_flg) {
		if ($closing_month_flg != "2") {
			$year = $start_year;
			$month = $start_month;
		} else {
			$year = $end_year;
			$month = $end_month;
		}
		$yyyymm = sprintf("%04d%02d", $year, $month);
	}
//前月、翌月リンク用
	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);


	// タイムカード設定情報を取得
	$sql = "select ovtmscr_tm from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	}
	if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}

	// 対象年月度を変数にセット
//	$c_yyyymm = substr($start_date, 0, 6);
	$c_yyyymm = sprintf("%04d%02d", $year, $month);
// ************ oose add start *****************
	$c_yyyymm_fulltime = $c_yyyymm;
// ************ oose add start *****************
}

// ************ oose add start *****************
//非常勤の締め日より開始／終了日算出
if ($closing_parttime != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing_parttime) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			}

		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}

				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;

				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date_parttime = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date_parttime = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
	$start_year_parttime = substr($start_date_parttime,0,4);
	$start_month_parttime = substr($start_date_parttime,4,2);
	$start_day_parttime = substr($start_date_parttime,6,2);
	$end_year_parttime = substr($end_date_parttime,0,4);
	$end_month_parttime = substr($end_date_parttime,4,2);
	$end_day_parttime = substr($end_date_parttime,6,2);

	//月が未指定の場合
	if (!$month_set_flg) {
		if ($closing_month_flg != "2") {
			$year = $start_year;
			$month = $start_month;
		} else {
			$year = $end_year;
			$month = $end_month;
		}
	}
	// 対象年月度を変数にセット
//	$c_yyyymm_parttime = substr($start_date_parttime, 0, 6);
	$c_yyyymm_parttime = sprintf("%04d%02d", $year, $month);
}
// ************ oose add end *****************

//深夜残業対応、以前のデータは深夜勤務
$sql = "select count(*) as cnt from wktotalm";
$cond = "where yyyymm = '$c_yyyymm' and length(time6) > 0 ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$cnt = pg_fetch_result($sel, 0, "cnt");
if ($cnt > 0) {
	$sinnya_flg = "1";
	$sinnya_title = "深夜<br>勤務";
} else {
	$sinnya_flg = "2";
	$sinnya_title = "深夜<br>残業";
}

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function closeBook() {
    if (!document.book.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
	var checked = false;
	if (document.book.elements['c_emp_id[]'] != null) {
		var box_cnt = document.book.elements['c_emp_id[]'].length;
		if (box_cnt == undefined) {
			checked = (document.book.elements['c_emp_id[]'].checked);
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.book.elements['c_emp_id[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (checked) {
		if (confirm('チェックした職員の期間内のタイムカードが修正できなくなります。\n実行してよろしいですか？')) {
			document.book.target = '';
			document.book.action = 'work_admin_total_close.php';
			document.book.submit();
		}
	} else {
		alert('対象者が選択されていません。');
	}
}

function openPrintPage() {

	//var url = 'work_admin_total_print.php';
	//url += '?session=<? echo($session); ?>';
	//url += '<? echo($url_emp_ids); ?>';
	//url += '&yyyymm=<? echo($yyyymm); ?>';

	//window.open(url, 'watp', 'width=800,height=600,scrollbars=yes');
    if (!document.book.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
	window.open('', 'watp', 'width=800,height=600,scrollbars=yes');
	document.book.action = 'work_admin_total_print.php';

	document.book.target = 'watp';
	document.book.submit();


}

function cancelBook() {
    if (!document.book.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
	var checked = false;
	if (document.book.elements['cancel_id[]'] != null) {
		var box_cnt = document.book.elements['cancel_id[]'].length;
		if (box_cnt == undefined) {
			checked = (document.book.elements['cancel_id[]'].checked);
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.book.elements['cancel_id[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}

	if (checked) {
		document.book.target = '';
		document.book.action = 'work_admin_total_cancel.php';
		document.book.submit();
	} else {
		alert('対象者が選択されていません。');
	}
}

function showMonth(flg) {
    if (!document.book.data_exist_flg) {
        alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
        return false;
    }
	if (flg == -1) {
		var yyyymm = '<? echo($last_yyyymm); ?>';
	}
	else {
		var yyyymm = '<? echo($next_yyyymm); ?>';
	}
	document.book.yyyymm.value = yyyymm;

	document.book.target = '';
	document.book.action = 'work_admin_total.php';
	document.book.submit();
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($mmode == "usr") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($checkauth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<?
}
elseif ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
//            "url_emp_ids" => $url_emp_ids,
$option = array(
            "srch_name" => $url_srch_name,
            "cls" => $cls,
            "atrb" => $atrb,
            "dept" => $dept,
            "group_id" => $group_id,
			"page" => $page,
			"shift_group_id" => $shift_group_id,
			"csv_layout_id" => $csv_layout_id,
			"duty_form_jokin" => $duty_form_jokin,
            "duty_form_hijokin" => $duty_form_hijokin,
            "srch_id" => $srch_id,
            "sus_flg" => $sus_flg
                );
if ($mmode != "usr") {
	show_work_admin_menuitem($session, $fname, $option);
}
else {
	show_atdbk_menuitem($session, $fname, $option);
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($closing == "") {  // 締め日未登録の場合 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。出退勤マスターメンテナンスの締め日画面で登録してください。</font></td>
</tr>
</table>
<? } else { ?>
<form name="book" action="work_admin_total_close.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<!-- oose update start -->
<!-- <td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="work_admin_total.php?session=<? echo($session); ?><? echo($url_emp_ids); ?>&yyyymm=<? echo($last_yyyymm); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>">&lt;前月</a>&nbsp;<? echo("{$year}年{$month}月度"); ?>&nbsp;<a href="work_admin_total.php?session=<? echo($session); ?><? echo($url_emp_ids); ?>&yyyymm=<? echo($next_yyyymm); ?>&srch_name=<? echo($url_srch_name); ?>&cls=<? echo($cls); ?>&atrb=<? echo($atrb); ?>&dept=<? echo($dept); ?>&page=<? echo($page); ?>">翌月&gt;</a></font></td> -->
<!-- <td width="340"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td> -->
<td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<a href="javascript:void(0);" onclick="showMonth(-1);">&lt;前月</a>&nbsp;<? echo("{$year}年{$month}月度"); ?>&nbsp;<a href="javascript:void(0);" onclick="showMonth(1);">翌月&gt;</a></font></td>
<td width="340"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
開始日&nbsp;&nbsp;<? echo("{$start_month_fulltime}月{$start_day_fulltime}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;
締め日&nbsp;&nbsp;<? echo("{$end_month_fulltime}月{$end_day_fulltime}日"); ?>&nbsp;-----&nbsp;（常　勤）
<br>
開始日&nbsp;&nbsp;<? echo("{$start_month_parttime}月{$start_day_parttime}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;
締め日&nbsp;&nbsp;<? echo("{$end_month_parttime}月{$end_day_parttime}日"); ?>&nbsp;-----&nbsp;（非常勤）
</font></td>
<!-- oose update end -->
<td align="right">
<input type="button" value="締め解除" onclick="cancelBook();">&nbsp;<input type="button" value="締め" onclick="closeBook();">&nbsp;<input type="button" value="印刷" onclick="openPrintPage();"></td>
</tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#f6f9ff">
<td colspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font></td>
<td colspan="10" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日数</font></td>
<td colspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回数</font></td>
<td colspan="6" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">締め</font></td>
</tr>
<tr height="22">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要勤務<br>日数</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤<br>日数</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休日<br>出勤</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">代出</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">振出</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">代休</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">振休</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年休<br>有休</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">欠勤</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻<br>時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">早退<br>時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基準<br>時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務<br>時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業<br>時間</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sinnya_title); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">締め</font></td>
</tr>
<?
// ************ oose update start *****************
//	show_total_list($con, $emp_id, $start_date, $end_date, $c_yyyymm, $ovtmscr_tm, $fname);
    show_total_list($con, $emp_id,
            $start_date_fulltime, $end_date_fulltime, $c_yyyymm_fulltime,
            $start_date_parttime, $end_date_parttime, $c_yyyymm_parttime,
            $ovtmscr_tm, $fname, $closing, $closing_month_flg, $sinnya_flg, $obj_hol_hour, $ovtm_class);
// ************ oose update end *****************
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<?
foreach ($emp_id as $tmp_id) {
	echo("<input type=\"hidden\" name=\"emp_id[]\" value=\"$tmp_id\">\n");
}
?>
<input type="hidden" name="c_yyyymm" value="<? echo($c_yyyymm); ?>">
<input type="hidden" name="yyyymm" value="<? echo($yyyymm); ?>">
<input type="hidden" name="srch_id" value="<? echo($srch_id); ?>">
<input type="hidden" name="srch_name" value="<? echo($srch_name); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="shift_group_id" value="<? echo($shift_group_id); ?>">
<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
<!-- ************ oose add start ***************** -->
<input type="hidden" name="start_date_fulltime" value="<? echo($start_date_fulltime); ?>">
<input type="hidden" name="end_date_fulltime" value="<? echo($end_date_fulltime); ?>">
<input type="hidden" name="start_date_parttime" value="<? echo($start_date_parttime); ?>">
<input type="hidden" name="end_date_parttime" value="<? echo($end_date_parttime); ?>">
<!-- ************ oose add end ***************** -->
<input type="hidden" name="duty_form_jokin" value="<? echo($duty_form_jokin); ?>">
<input type="hidden" name="duty_form_hijokin" value="<? echo($duty_form_hijokin); ?>">
<input type="hidden" name="sus_flg" value="<? echo($sus_flg); ?>">
<input type="hidden" name="mmode" value="<? echo($mmode); ?>">
<?     //登録時、データがそろっていることの確認用 ?>
<input type="hidden" name="data_exist_flg" value="1">
</form>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 集計表を出力
// ************ oose update start *****************
//function show_total_list($con, $emp_id, $start_date, $end_date, $c_yyyymm, $ovtmscr_tm, $fname) {
function show_total_list($con, $emp_id,
    $start_date_fulltime, $end_date_fulltime, $c_yyyymm_fulltime,
    $start_date_parttime, $end_date_parttime, $c_yyyymm_parttime,
    $ovtmscr_tm, $fname, $closing, $closing_month_flg, $sinnya_flg, $obj_hol_hour, $ovtm_class) {
// ************ oose update end *****************

    // 指定範囲の日付
    if ($start_date_fulltime <= $start_date_parttime) {
        $wk_start_date = $start_date_fulltime;
    } else {
        $wk_start_date = $start_date_parttime;
    }
    if ($end_date_fulltime >= $end_date_parttime) {
        $wk_end_date = $end_date_fulltime;
    } else {
        $wk_end_date = $end_date_parttime;
    }
    //グループ毎のテーブルから勤務時間を取得する
    $timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $wk_start_date, $wk_end_date);

    //出勤表関連共通クラス
	$atdbk_common_class = new atdbk_common_class($con, $fname);

	//ワークフロー関連共通クラス
	$atdbk_workflow_common_class = new atdbk_workflow_common_class($con, $fname);

	//タイムカード情報の取得
	$timecard_bean = new timecard_bean();
	$timecard_bean->select($con, $fname);
    //カレンダー名、所定労働時間履歴用
    $calendar_name_class = new calendar_name_class($con, $fname);

	$atdbk_close_class = new atdbk_close_class($con, $fname);

    // 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));
		//時間給者の休憩時間追加 20100929
		$rest4 = intval(pg_fetch_result($sel, 0, "rest4"));
		$rest5 = intval(pg_fetch_result($sel, 0, "rest5"));
		$rest6 = intval(pg_fetch_result($sel, 0, "rest6"));
		$rest7 = intval(pg_fetch_result($sel, 0, "rest7"));

		$night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
		$delay_overtime_flg = pg_fetch_result($sel, 0, "delay_overtime_flg");
		//$overtime_40_flg = pg_fetch_result($sel, 0, "overtime_40_flg");
		$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;
		//時間給者の休憩時間追加 20100929
		$rest4 = 0;
		$rest5 = 0;
		$rest6 = 0;
		$rest7 = 0;

		$night_workday_count = "";
		$delay_overtime_flg = "1";
		$modify_flg = "t";
	}
	//週40時間超過分は計算しないを固定とする 20100525
	$overtime_40_flg = "2";

	//集計処理の設定を「集計しない」の固定とする 20100910
	$others1 = "1";	//勤務時間内に外出した場合
	$others2 = "1";	//勤務時間外に外出した場合
	$others3 = "1";	//時間給者の休憩の集計
	$others4 = "1";	//遅刻・早退に休憩が含まれた場合
	$others5 = "2";	//早出残業時間の集計

	// 「退勤後復帰」ボタン表示フラグを取得
	$sql = "select ret_btn_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rolllback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

    // 指定範囲の勤務日種別を取得
    $wd = date("w", to_timestamp($wk_start_date));
	while ($wd != 0) {
		$wk_start_date = last_date($wk_start_date);
		$wd = date("w", to_timestamp($wk_start_date));
	}

	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
	// 通常勤務日の労働時間を取得
    //所定労働時間履歴対応 20121129
    $arr_timehist = $calendar_name_class->get_calendarname_time($start_date_fulltime);
    $day1_time_fulltime = $arr_timehist["day1_time"];
    $arr_timehist = $calendar_name_class->get_calendarname_time($start_date_parttime);
    $day1_time_parttime = $arr_timehist["day1_time"];

    //個人別所定時間を優先するための対応 20120309
    $arr_empcond_officehours_ptn = $timecard_common_class->get_empcond_officehours_pattern();
    $arr_weekday_flg = $timecard_common_class->get_arr_weekday_flag($wk_start_date, $wk_end_date);
    // 選択された全職員をループ
	foreach ($emp_id as $tmp_emp_id) {

        $arr_empcond_officehours = $timecard_common_class->get_arr_empcond_officehours($tmp_emp_id, $wk_start_date); //個人別勤務時間帯履歴対応 20130220

        //休日出勤と判定する日設定取得 20091222
		$arr_timecard_holwk_day = get_timecard_holwk_day($con, $fname, $tmp_emp_id);

		// ************ oose add start *****************
		// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
		// 給与支給区分・祝日計算方法、残業管理を取得
        $sql = "select duty_form, wage, hol_minus, no_overtime, specified_time from empcond";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$duty_form = pg_fetch_result($sel, 0, "duty_form");
			$wage = pg_fetch_result($sel, 0, "wage");
			$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
			$no_overtime = pg_fetch_result($sel, 0, "no_overtime");
            $paid_specified_time = pg_fetch_result($sel, 0, "specified_time");
        } else {
			$duty_form = "";
			$wage = "";
			$hol_minus = "";
			$no_overtime = "";
            $paid_specified_time = "";
        }
//		$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
		if ($duty_form == 2) {
			//非常勤
			$c_yyyymm = $c_yyyymm_parttime;
			//$start_date = $start_date_parttime;
			//$end_date = $end_date_parttime;
		} else {
			//常勤
			$c_yyyymm = $c_yyyymm_fulltime;
			//$start_date = $start_date_fulltime;
			//$end_date = $end_date_fulltime;
		}
		$closing = $atdbk_close_class->get_empcond_closing($tmp_emp_id);
		$arr_date = $atdbk_close_class->get_term($c_yyyymm, $closing, $closing_month_flg);
		$start_date = $arr_date["start"];
		$end_date = $arr_date["end"];
		$start_date_part = $start_date;
		$end_date_part = $end_date;
// ************ oose add end *****************
        //所定労働時間履歴対応 20121129
        if ($paid_specified_time != "") {
            $day1_time = $paid_specified_time;
        }
        else {
            if ($duty_form == 2) {
                //非常勤
                $day1_time = $day1_time_parttime;
            }
            else {
                //常勤
                $day1_time = $day1_time_fulltime;
            }
        }
		//一括修正申請情報
		$arr_tmmdapplyall_status = $atdbk_workflow_common_class->get_tmmdapplyall_status($tmp_emp_id, $start_date, $end_date);
		// 職員ID・職員氏名を取得
		$sql = "select emp_personal_id, emp_lt_nm, emp_ft_nm from empmst";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
		if ($tmp_emp_personal_id == "") {
			$tmp_emp_personal_id = "&nbsp;";
		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

		// 変則労働適用情報を配列で取得
		$arr_irrg_info = get_irrg_info_array($con, $tmp_emp_id, $fname);

		// 対象年月の締め状況を取得
		$sql = "select count(*) from atdbkclose";
		$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_closed = (pg_fetch_result($sel, 0, 0) > 0);

		// 集計結果格納用配列を初期化
		$sums = array_fill(0, 19, 0);

		// 締め未済の場合
		if (!$tmp_closed) {

			// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
			$tmp_date = $start_date;
			if ($arr_irrg_info["irrg_type"] == "1") {
				$wd = date("w", to_timestamp($tmp_date));
				while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
					$tmp_date = last_date($tmp_date);
					$wd = date("w", to_timestamp($tmp_date));
				}
			}
			//データ検索用開始日
			$wk_start_date = $tmp_date;

			$counter_for_week = 1;
			$holiday_count = 0;

			// 予定データをまとめて取得
			$arr_atdbk = array();
			$sql =	"SELECT ".
						"atdbk.date, ".
						"atdbk.pattern, ".
						"atdbk.reason, ".
						"atdbk.night_duty, ".
						"atdbk.allow_id, ".
						"atdbk.prov_start_time, ".
						"atdbk.prov_end_time, ".
						"atdbk.tmcd_group_id, ".
						"atdptn.workday_count ".
					"FROM ".
						"atdbk ".
							"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
			$cond = "where emp_id = '$tmp_emp_id' and ";
			$cond .= " date >= '$wk_start_date' and date <= '$end_date'";

			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
				$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
				$arr_atdbk[$date]["pattern"] = $row["pattern"];
				$arr_atdbk[$date]["reason"] = $row["reason"];
				$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
				$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
				$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
				$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
				$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
			}
			// 実績データをまとめて取得
//echo("wk_start_date=$wk_start_date end_date=$end_date");
			$arr_atdbkrslt = array();
			$sql =	"SELECT ".
						"atdbkrslt.*, ".
						"atdptn.workday_count, ".
						"atdptn.base_time, ".
						"atdptn.over_24hour_flag, ".
						"atdptn.out_time_nosubtract_flag, ".
						"atdptn.after_night_duty_flag, ".
                        "atdptn.previous_day_flag as atdptn_previous_day_flag, ".
                        "atdptn.no_count_flag, ".
                        "tmmdapply.apply_id     AS tmmd_apply_id, ".
						"tmmdapply.apply_status AS tmmd_apply_status, ".
						"ovtmapply.apply_id     AS ovtm_apply_id, ".
						"ovtmapply.apply_status AS ovtm_apply_status, ".
						"rtnapply.apply_id      AS rtn_apply_id, ".
						"rtnapply.apply_status  AS rtn_apply_status, ".
						"COALESCE(atdbk_reason_mst.day_count, 0) AS reason_day_count ".
					"FROM ".
						"atdbkrslt ".
							"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
							"LEFT JOIN tmmdapply ON atdbkrslt.emp_id = tmmdapply.emp_id AND CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date AND tmmdapply.delete_flg = false AND tmmdapply.re_apply_id IS NULL ".
							"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
							"LEFT JOIN rtnapply  ON atdbkrslt.emp_id = rtnapply.emp_id  AND CAST(atdbkrslt.date AS varchar) = rtnapply.target_date AND rtnapply.delete_flg = false AND rtnapply.re_apply_id IS NULL ".
							"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
			$cond = "where atdbkrslt.emp_id = '$tmp_emp_id' and ";
			$cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$end_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
                //まとめて設定
                $arr_atdbkrslt[$date] = $row;
                /*
				$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
				$arr_atdbkrslt[$date]["reason"] = $row["reason"];
				$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
				$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
				$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
				$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
				$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
				$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					$end_var = "o_end_time$i";
					$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
					$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
				}
				$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
				$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
				$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
				$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
				$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
				$arr_atdbkrslt[$date]["tmmd_apply_id"] = $row["tmmd_apply_id"];
				$arr_atdbkrslt[$date]["tmmd_apply_status"] = $row["tmmd_apply_status"];
				$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
				$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
				$arr_atdbkrslt[$date]["rtn_apply_id"] = $row["rtn_apply_id"];
				$arr_atdbkrslt[$date]["rtn_apply_status"] = $row["rtn_apply_status"];
				$arr_atdbkrslt[$date]["reason_day_count"] = $row["reason_day_count"];
				$arr_atdbkrslt[$date]["base_time"] = $row["base_time"];
				//残業時刻追加 20100114
				$arr_atdbkrslt[$date]["allow_count"] = $row["allow_count"];
				$arr_atdbkrslt[$date]["over_start_time"] = $row["over_start_time"];
				$arr_atdbkrslt[$date]["over_end_time"] = $row["over_end_time"];
				$arr_atdbkrslt[$date]["over_start_next_day_flag"] = $row["over_start_next_day_flag"];
				$arr_atdbkrslt[$date]["over_end_next_day_flag"] = $row["over_end_next_day_flag"];
				//残業時刻2追加 20110616
				$arr_atdbkrslt[$date]["over_start_time2"] = $row["over_start_time2"];
				$arr_atdbkrslt[$date]["over_end_time2"] = $row["over_end_time2"];
				$arr_atdbkrslt[$date]["over_start_next_day_flag2"] = $row["over_start_next_day_flag2"];
				$arr_atdbkrslt[$date]["over_end_next_day_flag2"] = $row["over_end_next_day_flag2"];
				$arr_atdbkrslt[$date]["over_24hour_flag"] = $row["over_24hour_flag"];
				//早出残業 20100601
				$arr_atdbkrslt[$date]["early_over_time"] = $row["early_over_time"];
				//休憩時刻追加 20100921
				$arr_atdbkrslt[$date]["rest_start_time"] = $row["rest_start_time"];
				$arr_atdbkrslt[$date]["rest_end_time"] = $row["rest_end_time"];
				//外出時間を差し引かないフラグ 20110727
				$arr_atdbkrslt[$date]["out_time_nosubtract_flag"] = $row["out_time_nosubtract_flag"];
				//明け追加 20110819
				$arr_atdbkrslt[$date]["after_night_duty_flag"] = $row["after_night_duty_flag"];
                //時間帯画面の前日フラグ 20121120
                $arr_atdbkrslt[$date]["atdptn_previous_day_flag"] = $row["atdptn_previous_day_flag"];
                //勤務時間を計算しないフラグ 20130225
                $arr_atdbkrslt[$date]["no_count_flag"] = $row["no_count_flag"];
                */
            }
			//時間有休追加 20111207
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				//時間有休データをまとめて取得
				$arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($tmp_emp_id, $wk_start_date, $end_date);

			}

			//支給換算日数 20090908
			$paid_day_count = 0;

			//勤務パターンが1件もない場合の対処 20100108
			$ptn_day_cnt = 0;

			//休日出勤テーブル 2008/10/23
			$arr_hol_work_date = array();
			while ($tmp_date <= $end_date) {

				if ($counter_for_week >= 8) {
					$counter_for_week = 1;
				}
				if ($counter_for_week == 1) {
					$work_time_for_week = 0;
					if ($arr_irrg_info["irrg_type"] == "1") {
						$holiday_count = 0;
					}
					//1日毎の法定外残を週単位に集計 20090708
					$wk_week_hoteigai = 0;
				}
				$work_time = 0;
				$wk_return_time = 0;

				$start_time_info = array();
				$end_time_info = array(); //退勤時刻情報 20100910

				$time3 = 0; //外出時間（所定時間内） 20100910
				$time4 = 0; //外出時間（所定時間外） 20100910
				$tmp_rest2 = 0; //休憩時間 20100910
				$time3_rest = 0; //休憩時間（所定時間内）20100910
				$time4_rest = 0; //休憩時間（所定時間外）20100910
				$arr_effective_time_wk = array(); //休憩を引く前の実働時間 20100915

				$holiday_name = get_holiday_name($tmp_date);
				if ($holiday_name != "") {
					$holiday_count++;
				}

				// 処理日付の勤務日種別を取得
				$type = $arr_type[$tmp_date];

				// 処理日付の勤務予定情報を取得
				$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
				$prov_reason = $arr_atdbk[$tmp_date]["reason"];
				$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];

				$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
				$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
				$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
				$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
				$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

				if ($prov_workday_count == "") {
					$prov_workday_count = 0;
				}

				// 要勤務日数をカウント
//				if ($tmp_date >= $start_date) {

					// カレンダーの属性を元にカウントする 2008/10/21
//					$sums[0] += $timecard_common_class->get_you_kinmu_nissu($type);
/*
					// 予定が未入力でも「休暇」でもなければカウント
					if ($prov_pattern != "" && $prov_pattern != "10") {

						// 当直あり
						if($prov_night_duty == "1")
						{
							//曜日に対応したworkday_countを取得する
							$prov_workday_count = $prov_workday_count + $timecard_common_class->get_prov_night_workday_count($tmp_date);
						}
						$sums[0] = $sums[0] + $prov_workday_count;
					}
*/
//				}

				// 処理日付の勤務実績を取得
				$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
				$reason = $arr_atdbkrslt[$tmp_date]["reason"];
				$night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
				$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
				$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
				$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
				$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
				$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					$end_var = "o_end_time$i";
					$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
					$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

					if ($$start_var != "") {
						$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
					}
					if ($$end_var != "") {
						$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
					}
				}

				$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
				$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

				$workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

				$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
				$next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

				$tmmd_apply_id = $arr_atdbkrslt[$tmp_date]["tmmd_apply_id"];
				$tmmd_apply_status = $arr_atdbkrslt[$tmp_date]["tmmd_apply_status"];
				$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
				$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
				$rtn_apply_id = $arr_atdbkrslt[$tmp_date]["rtn_apply_id"];
				$rtn_apply_status = $arr_atdbkrslt[$tmp_date]["rtn_apply_status"];
				$reason_day_count = $arr_atdbkrslt[$tmp_date]["reason_day_count"];
				$base_time = $arr_atdbkrslt[$tmp_date]["base_time"];

				//hhmmをhh:mmに変換している
				if ($start_time != "") {
					$start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $start_time);
				}
				if ($end_time != "") {
					$end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $end_time);
				}
				if ($out_time != "") {
					$out_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $out_time);
				}
				if ($ret_time != "") {
					$ret_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $ret_time);
				}
				if ($workday_count == "") {
					$workday_count = 0;
				}
				if ($previous_day_flag == "") {
					$previous_day_flag = 0;
				}
				if ($next_day_flag == "") {
					$next_day_flag = 0;
				}
				if ($reason_day_count == "") {
					$reason_day_count = 0;
				}
				//残業時刻追加 20100114
				$over_start_time = $arr_atdbkrslt[$tmp_date]["over_start_time"];
				$over_end_time = $arr_atdbkrslt[$tmp_date]["over_end_time"];
				$over_start_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag"];
				$over_end_next_day_flag = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag"];
				if ($over_start_time != "") {
					$over_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time);
				}
				if ($over_end_time != "") {
					$over_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time);
				}
				//残業時刻2追加 20110616
				$over_start_time2 = $arr_atdbkrslt[$tmp_date]["over_start_time2"];
				$over_end_time2 = $arr_atdbkrslt[$tmp_date]["over_end_time2"];
				$over_start_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_start_next_day_flag2"];
				$over_end_next_day_flag2 = $arr_atdbkrslt[$tmp_date]["over_end_next_day_flag2"];
				if ($over_start_time2 != "") {
					$over_start_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_start_time2);
				}
				if ($over_end_time2 != "") {
					$over_end_time2 = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $over_end_time2);
				}
				$over_24hour_flag = $arr_atdbkrslt[$tmp_date]["over_24hour_flag"];
				//早出残業 20100601
				$early_over_time = $arr_atdbkrslt[$tmp_date]["early_over_time"];
				if ($early_over_time != "") {
					$early_over_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $early_over_time);
				}
				//休憩時刻追加 20100921
				$rest_start_time = $arr_atdbkrslt[$tmp_date]["rest_start_time"];
				$rest_end_time = $arr_atdbkrslt[$tmp_date]["rest_end_time"];
				if ($rest_start_time != "") {
					$rest_start_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_start_time);
				}
				if ($rest_end_time != "") {
					$rest_end_time = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $rest_end_time);
				}
				//外出時間を差し引かないフラグ 20110727
				$out_time_nosubtract_flag = $arr_atdbkrslt[$tmp_date]["out_time_nosubtract_flag"];
				//明け追加 20110819
				$after_night_duty_flag = $arr_atdbkrslt[$tmp_date]["after_night_duty_flag"];
                //時間帯画面の前日フラグ 20121120
                $atdptn_previous_day_flag = $arr_atdbkrslt[$tmp_date]["atdptn_previous_day_flag"];
                //勤務時間を計算しないフラグ 20130225
                $no_count_flag = $arr_atdbkrslt[$tmp_date]["no_count_flag"];

				// 残業表示用に退勤時刻を退避
//				$show_end_time = $end_time;

				//当直の有効判定
				$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

				$rslt_workday_count = "";

				if ($tmp_date >= $start_date) {

					//公休か事由なし休暇で残業時刻がありの場合 20100802
					//事由に関わらず休暇で残業時刻がありの場合へ変更 20100916
					if (($pattern == "10" || $after_night_duty_flag == "1") && //明けを追加 20110819
						$over_start_time != "" && $over_end_time != "") {
						$legal_hol_over_flg = true;
						//残業時刻を設定
						$start_time = $over_start_time;
						$end_time = $over_end_time;

						//法定外残業の基準
						$wk_base_time = 0;
					} else {
						$legal_hol_over_flg = false;
					}

					//勤務パターンのある日数をカウント 20100108
					if ($pattern != "") {
						$ptn_day_cnt++;
					}
					// 出勤日数をカウント（条件：出勤と退勤の打刻あり、または、「明け」の場合）
					if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") {

						//日またがり対応 20101027
						if ($start_time > $end_time &&
								$previous_day_flag == "0") {
							$next_day_flag = "1";
						}

						//休暇時は計算しない 20090806
						if ($pattern != "10") {
							$rslt_workday_count = $workday_count;
						}
						if($night_duty_flag)
						{
							//曜日に対応したworkday_countを取得する
							$rslt_workday_count = $rslt_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
						}
						$sums[1] = $sums[1] + $rslt_workday_count;
						//休日出勤のカウント方法変更 20091222
                        if (check_timecard_holwk(
                                    $arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason, "", $timecard_bean) == true) {
							$sums[2] += 1;
						}
					}

					// 事由ベースの日数計算
					$reason_day_count = $atdbk_common_class->get_reason_day_count($reason);
					//reason_day_countは支給換算日数となる 20090908
					if ($reason != "") {
						$paid_day_count += $reason_day_count;
					}
					//時間有休追加 20111207
					$paid_time_hour = 0;
					if ($obj_hol_hour->paid_hol_hour_flag == "t") {
						$paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
						$paid_time_hour = $paid_hol_min;
						$sums[18] += $paid_hol_min; //時間有休計
					}
					switch ($reason) {
//					case "16":  // 休日出勤
//						$sums[2] += $reason_day_count;
//						break;
					case "14":  // 代替出勤
						$sums[3] += 1;
						break;
					case "15":  // 振替出勤
						$sums[4] += 1;
						break;
					case "65":  // 午前振出
					case "66":  // 午後振出
						$sums[4] += 0.5;
						break;
					case "4":  // 代替休暇
						$sums[5] += 1;
						break;
					case "18":  // 半前代替休
					case "19":  // 半後代替休
						$sums[5] += 0.5;
						break;
					case "17":  // 振替休暇
						$sums[6] += 1;
						break;
					case "20":  // 半前振替休
					case "21":  // 半後振替休
						$sums[6] += 0.5;
						break;
					case "1":  // 有休休暇
					case "37": // 年休
                        if ($pattern == "10") { //20140106 パターンが休暇の場合
                            $sums[7] += 1;
                        }
						break;
					case "2":  // 午前有休
					case "3":  // 午後有休
					case "38": // 午前年休
					case "39": // 午後年休
						$sums[7] += 0.5;
						break;
					case "57":  // 半有半欠
						$sums[7] += 0.5;
						$sums[8] += 0.5;
						break;
					case "6":  // 一般欠勤
					case "7":  // 病傷欠勤
						$sums[8] += 1;
						break;
					case "48": // 午前欠勤
					case "49": // 午後欠勤
					case "60":  // 半特半欠
					case "56":  // 半夏半欠
					case "64":  // 半正半欠
						$sums[8] += 0.5;
						break;
					case "8":  // その他休
						$sums[9] += 1;
						break;
					case "52":  // 午前その他休
					case "53":  // 午後その他休
						$sums[9] += 0.5;
						break;
					case "44":  // 半有半公
					case "58":  // 半特半有
					case "55":  // 半夏半有
					case "62":  // 半正半有
						$sums[7] += 0.5;
						break;
					}
				}

				// 各時間帯の開始時刻・終了時刻を変数に格納
				//type:"2勤務日1,3勤務日2"以外は"1通常"とする。"4法定休日,5所定休日,6祝日,7年末年始休暇"
				$tmp_type = ($type != "2" && $type != "3") ? "1" : $type;
				$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
				$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
				$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
				$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
				$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
				$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

                //個人別所定時間を優先する場合 20120309
                if ($arr_empcond_officehours_ptn[$tmcd_group_id][$pattern] == 1) {
                    $wk_weekday = $arr_weekday_flg[$tmp_date];
                    $arr_empcond_officehours = $timecard_common_class->get_empcond_officehours($wk_weekday, $tmp_date); //個人別勤務時間帯履歴対応 20130220
                    if ($arr_empcond_officehours["officehours2_start"] != "") {
                        $start2 = $arr_empcond_officehours["officehours2_start"];
                        $end2 = $arr_empcond_officehours["officehours2_end"];
                        $start4 = $arr_empcond_officehours["officehours4_start"];
                        $end4 = $arr_empcond_officehours["officehours4_end"];
                    }
                }

                // 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
				if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
					$start2 = $prov_start_time;
					$end2 = $prov_end_time;
				}

				$over_calc_flg = false;	//残業時刻 20100525
				//残業時刻追加 20100114
				//残業開始が予定終了時刻より前の場合に設定
				if ($over_start_time != "") {
					//残業開始日時
					if ($over_start_next_day_flag == "1") {
						$over_start_date = next_date($tmp_date);
					} else {
						$over_start_date = $tmp_date;
					}

					$over_start_date_time = $over_start_date.$over_start_time;
					$over_calc_flg = true;
					//残業終了時刻 20100705
					$over_end_date = ($over_end_next_day_flag == "1") ? next_date($tmp_date) : $tmp_date;
					$over_end_date_time = $over_end_date.$over_end_time;
				}
				// 実働時間をクリア、出勤時刻＜退勤時刻でない場合に前日の残業時間が表示されないようにする。
				$effective_time = 0;

				//一括修正のステータスがある場合は設定
				$tmp_status = $arr_tmmdapplyall_status[$tmp_date]["apply_status"];
				if ($tmmd_apply_status == "" && $tmp_status != "") {
					$tmmd_apply_status = $tmp_status;
					$tmmd_apply_id = $arr_tmmdapplyall_status[$tmp_date]["apply_id"];
				}
				//tmmd_apply_statusからlink_type取得
				$modify_link_type = $timecard_common_class->get_link_type_modify($tmmd_apply_status);
				$modify_apply_id  = $tmmd_apply_id;

				// 残業情報を取得
				//ovtm_apply_statusからlink_type取得
				$night_duty_time_array  = array();
				$duty_work_time         = 0;
				if ($night_duty_flag){
					//当直時間の取得
					$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

					//日数換算の数値ｘ通常勤務日の労働時間の時間
					$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id, $over_start_time, $over_end_time, $early_over_time, $over_24hour_flag, $over_start_time2, $over_end_time2, $over_start_next_day_flag2, $over_end_next_day_flag2);
				$overtime_apply_id  = $ovtm_apply_id;

				//rtn_apply_statusからlink_type取得
				$return_link_type = $timecard_common_class->get_link_type_return($ret_btn_flg, $o_start_time1, $rtn_apply_status);
				$return_apply_id  = $rtn_apply_id;

				// 所定労働・休憩開始終了日時の取得
                // 所定開始時刻が前日の場合 20121120
                if ($atdptn_previous_day_flag == "1") {
                    $tmp_prev_date = last_date($tmp_date);
					$start2_date_time = $tmp_prev_date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_prev_date = $tmp_date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_prev_date, $start2, $end2);
				} else {
					$start2_date_time = $tmp_date.$start2;
					//24時間以上勤務の場合 20100203
					if ($over_24hour_flag == "1") {
						$tmp_end_date = next_date($tmp_date);
					} else {
						$tmp_end_date = $tmp_date;
					}
					$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_end_date, $start2, $end2);
				}

				//休憩時刻追加、入力された場合時間帯の設定の代わりに使用 20100921
				if ($rest_start_time != "" && $rest_end_time != "") {
					$start4 = $rest_start_time;
					$end4 = $rest_end_time;
				}
				$start4_date_time = $tmp_date.$start4;
				$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

				//外出復帰日時の取得
				$out_date_time = $tmp_date.$out_time;
				$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

				// 時間帯データを配列に格納
				$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
				// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
				// 24時間超える場合の翌日の休憩は配列にいれない 20101027
				if ($start2 < $start4) {
					$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, "");  // 休憩
				} else {
					$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩
				}

				//時間帯情報 休憩、深夜、前日当日翌日分の開始終了時刻を保持 20110126
				if ($tmp_date >= $start_date) {
					$work_times_info = $timecard_common_class->get_work_times_info($tmp_date, $start4, $end4, $start5, $end5);
				}
				// 処理当日の所定労働時間を求める
				if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
					$specified_time = 0;
					//		} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算 集計処理の設定を「集計しない」の固定とする 20100910
					//			$specified_time = count($arr_time2);
				} else {
					$specified_time = count(array_diff($arr_time2, $arr_time4));
				}

// 基準時間の計算を変更 2008/10/21
				// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
//				if ($tmp_date >= $start_date && $reason != "16") {
//					if ($arr_irrg_info["irrg_type"] != "2") {
//						$sums[14] += $specified_time;
						//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
//						if ($night_duty_flag){
//							$sums[14] += $duty_work_time;
//						}
//					}
//				}

				//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 start
				if ($over_start_time != "" && $over_end_time != "") {
					if ($start_time == "") {
						$start_time = $start2;
						$previous_day_flag = $atdptn_previous_day_flag; //時間帯設定の前日フラグ
					}
					if ($end_time == "") {
						$end_time = $end2;
						//日またがり対応
						if ($start_time > $end_time &&
								$previous_day_flag == "0") {
							$next_day_flag = "1";
						}
					}
				}
				//出勤退勤時刻がない場合でも残業時刻がある場合は所定時刻で勤務時間を計算する 20141027 end
				// 出退勤とも打刻済みの場合
                //勤務時間を計算しないフラグが"1"以外 20130225
                if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {

					// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
					$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
					$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
					if (empty($start2_date_time) || empty($end2_date_time)) {
						$start2_date_time = $start_date_time;
						$end2_date_time    = $end_date_time;
					}

					$overtime_approve_flg = $timecard_common_class->get_overtime_approve_flg($overtime_link_type, $modify_flg, $modify_link_type);
					// 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
					if ($no_overtime != "t") {
						// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
//						if (!($overtime_link_type == "0" || $overtime_link_type == "3" || $overtime_link_type == "4")) {
						// 残業未承認か確認
						if ($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3")) {
							if (date_utils::to_timestamp_from_ymdhi($start2_date_time) > date_utils::to_timestamp_from_ymdhi($start_date_time)) { //所定より出勤が早い場合には所定にする 20100914 時刻に:があるため遅刻時間の表示されない不具合 20101005
								$start_date_time  = $start2_date_time; //出勤時間、早出残業の場合
							}
							if ($end_date_time > $end2_date_time) {
								$end_date_time = $end2_date_time;
							}
						}
					}

					// 出勤〜退勤までの時間を配列に格納
					$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

					//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
					if (count($arr_tmp_work_time) > 0){
						// 時間帯データを配列に格納
/*
						switch ($reason) {
						case "2":  // 午前有休
						case "38": // 午前年休
							$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
							break;
						case "3":  // 午後有休
						case "39": // 午後年休
							$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
							break;
						default:
*/
							$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
//							break;
//						}
						//休日で残業がある場合 20101028
						if ($pattern == "10" && $over_start_time != "" && $over_end_time != "") {
							//深夜残業の開始終了時間
							$start5 = "2200";
							$end5 = "0500";
						}
						//前日・翌日の深夜帯も追加する
						$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

						// 確定出勤時刻／遅刻回数を取得
						// 勤務パターンで時刻指定がない場合でも、端数処理する
						if ($start2 == "" && $end2 == "" && $fraction1 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
							//基準日時
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_start_time = date_utils::move_time($tmp_office_start_time, $start_date_time, $moving_minutes, $fraction1);
							$start_time_info = array();
						} else {
							$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
							$fixed_start_time = $start_time_info["fixed_time"];
						}

						// 確定退勤時刻／早退回数を取得
						// 勤務パターンで時刻指定がない場合でも、端数処理する
						if ($start2 == "" && $end2 == "" && $fraction2 > "1") {
							//端数処理する分
							$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
							//基準日時
							$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
							$fixed_end_time = date_utils::move_time($tmp_office_start_time, $end_date_time, $moving_minutes, $fraction2);
		//echo("fixed_end_time=$fixed_end_time");
							$end_time_info = array();
						} else {
							//残業時刻が入っている場合は、端数処理しない 20100806
							$wk_over_end_date_time = str_replace(":", "", $over_end_date_time); //20111128
                            if ($over_end_time != "" && $wk_over_end_date_time > $end2_date_time && $end_date_time >= $end2_date_time) { //時間が所定終了よりあとか確認20111128 //早退の場合を除く条件追加 20140902
								$fixed_end_time = $wk_over_end_date_time;
							} else {
								$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
								$fixed_end_time = $end_time_info["fixed_time"];
							}
							//※残業申請未承認の場合と（残業申請不要かつ残業管理をする場合）に退勤時刻を所定にする 20100721
							//残業申請画面を表示しないは除く 20100811
							if ($timecard_bean->over_time_apply_type != "0") {
								if ($overtime_approve_flg == "1" || ($overtime_approve_flg == "0" && $no_overtime != "t")) {
									$fixed_end_time = $end2_date_time;
								}
							}
						}


//						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
//						$fixed_end_time = $end_time_info["fixed_time"];
						//時間有休がある場合は、遅刻早退を計算しない 20111207
						if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_time_hour > 0) {
					        //時間有休と相殺する場合
                            if ($ovtm_class->sousai_sinai_flg != "t") {
                                //遅刻
                                $tikoku_time = 0;
                                $start_time_info["diff_minutes"] = 0;
                                $start_time_info["diff_count"] = 0;
                                //早退
                                $sotai_time = 0;
                                $end_time_info["diff_minutes"] = 0;
                                $end_time_info["diff_count"] = 0;
                            }
						}
						if ($tmp_date >= $start_date) {
							//遅刻
							$sums[10] += $start_time_info["diff_count"];
							$sums[12] += $start_time_info["diff_minutes"];
							//早退
							$sums[11] += $end_time_info["diff_count"];
							$sums[13] += $end_time_info["diff_minutes"];
						}

						// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
						switch ($reason) {
						case "2":  // 午前有休
						case "38": // 午前年休
								// 残業時間が正しく計算されないため、休憩終了時間設定をしない 20100330
								//if (in_array($fixed_start_time, $arr_time2)) {
								$tmp_start_time = $fixed_start_time;
							//} else {
							//	$tmp_start_time = $end4_date_time;
							//}
							$tmp_end_time = $fixed_end_time;
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

							$arr_specified_time = $arr_time2;

							//$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);
								//休憩を引く前の実働時間
								$arr_effective_time_wk = $arr_effective_time;

							break;
						case "3":  // 午後有休
						case "39": // 午後年休
							$tmp_start_time = $fixed_start_time;
								// 残業時間が正しく計算されないため、休憩開始時間設定をしない 20100330
								//if (in_array($fixed_end_time, $arr_time2)) {
								$tmp_end_time = $fixed_end_time;
							//} else {
							//	$tmp_end_time = $start4_date_time;
							//}
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

							$arr_specified_time = $arr_time2;

							//$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);
								//休憩を引く前の実働時間
								$arr_effective_time_wk = $arr_effective_time;

							break;
						default:
							$tmp_start_time = $fixed_start_time;
							$tmp_end_time = $fixed_end_time;
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
								//休憩を引く前の実働時間
								$arr_effective_time_wk = $arr_effective_time;
							if ($wage != "4" || ($start4 != "" && $end4 != "")) { //休憩時刻がある場合を追加 20100925
									$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
							}

							$arr_specified_time = array_diff($arr_time2, $arr_time4);

							//$paid_time = 0;

							break;
						}

						//残業時刻追加対応 20100114
						//予定終了時刻から残業開始時刻の間を除外する
						if ($over_start_time != "") {
							$wk_end_date_time = $end2_date_time;

							$arr_jogai = array();
							/* 20100713
							//基準日時
							$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
							if ($fraction1 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
								$over_start_date_time = date_utils::move_time($tmp_office_start_time, $over_start_date_time, $moving_minutes, $fraction1);
							}
							if ($fraction2 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
								$wk_end_date_time = date_utils::move_time($tmp_office_start_time, $wk_end_date_time, $moving_minutes, $fraction2);
							}
							*/
							//残業開始時刻があとの場合
							if (date_utils::to_timestamp_from_ymdhi($over_start_date_time) > date_utils::to_timestamp_from_ymdhi($wk_end_date_time)) {
								//時間配列を取得する（休憩扱い）
								$arr_jogai = $timecard_common_class->get_minute_time_array($wk_end_date_time, $over_start_date_time);
								//休憩扱いの時間配列を除外する
								$arr_effective_time = array_diff($arr_effective_time, $arr_jogai);
							}

						}

						//休日勤務時間（分）と休日出勤を取得 2008/10/23
						if ($tmp_date >= $start_date) {
							//休暇時は計算しない 20090806
							//if ($pattern != "10") {
							//休日出勤の判定方法変更 20100721
							if (check_timecard_holwk(
										$arr_timecard_holwk_day, $arr_type[$tmp_date], $pattern, $reason) == true) {
								$arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);
								//$sums[31] += $arr_hol_time[3]; //20110126

								// 休日出勤した日付を配列へ設定
								for ($i=4; $i<7; $i++) {
									if ($arr_hol_time[$i] != "") {
										$arr_hol_work_date[$arr_hol_time[$i]] = 1;
									}
								}
							}
						}
						//当直の場合当直時間を計算から除外する
						if ($night_duty_flag){
							//稼働 - (当直 - 所定労働)
							$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
						}

						// 普外時間を算出（分単位）
						// 外出時間を差し引かないフラグ対応 20110727
						if ($out_time_nosubtract_flag != "1") {
							$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
							$time3 = count(array_intersect($arr_out_time, $arr_specified_time));

							// 残外時間を算出（分単位）
							$time4 = count(array_diff($arr_out_time, $arr_time2)); //$arr_specified_timeから変更、休憩も除くため 20100910
						}
						// 実働時間を算出（分単位）
						$effective_time = count($arr_effective_time);
						if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
							//休日残業で残業申請中は休憩を表示しない 20100916
							if (!($legal_hol_over_flg /*&& $overtime_approve_flg == "1"*/)) {
								if ($effective_time > 1200) {  // 20時間を超える場合 20100929
									$effective_time -= $rest7;
									$time3_rest = 0;
									$time4_rest = $rest7;
								} else if ($effective_time > 720) {  // 12時間を超える場合
									$effective_time -= $rest6;
									$time3_rest = 0;
									$time4_rest = $rest6;
								} else if ($effective_time > 540) {  // 9時間を超える場合
									$effective_time -= $rest3;
									$time3_rest = 0;
									$time4_rest = $rest3;
								} else if ($effective_time > 480) {  // 8時間を超える場合
									$effective_time -= $rest2;
									$time3_rest = $rest2;
									$time4_rest = 0;
								} else if ($effective_time > 360) {  // 6時間を超える場合
									$effective_time -= $rest1;
									$time3_rest = $rest1;
									$time4_rest = 0;
								} else if ($effective_time > 240 && $start_time < "12:00") {  // 4時間超え午前開始の場合
									$effective_time -= $rest4;
									$time3_rest = $rest4;
									$time4_rest = 0;
								} else if ($effective_time > 240 && $start_time >= "12:00") {  // 4時間超え午後開始の場合
									$effective_time -= $rest5;
									$time3_rest = $rest5;
									$time4_rest = 0;
								}
							}
							//休日で残業がない場合,出勤退勤がある場合 20100917
							if ($pattern == "10" && $over_start_time == "" && $over_end_time == "") {
								$time3_rest = 0;
								$time4_rest = 0;
							}
						} else {
							// 所定労働内の休憩時間
							$tmp_rest1 = count(array_intersect($arr_time2, $arr_time4));
							// 実働時間内の休憩時間
							$tmp_rest2 = count(array_intersect($arr_effective_time_wk, $arr_time4));
							//休憩時間前後の遅刻早退対応 20100925
							if ($tmp_rest1 > $tmp_rest2) {
								$tmp_rest2 = $tmp_rest1;
							}
						}

						//古い仕様のため廃止 20100330
						/*
								// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
						if ($reason == "14" || $reason == "15") {
							$effective_time -= count($arr_specified_time);
						}
						*/
						// 稼働時間を算出（分単位）
						//$work_time = $effective_time + $paid_time - $time3 - $time4;
						// 半年休の時間をプラスしない 20100330
						$work_time = $effective_time - $time3 - $time4;
						//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
						if ($pattern == "10" && !$legal_hol_over_flg) {
							$work_time = 0;
						}
						// 退勤後復帰回数・時間を算出
						$return_count = 0;
						$return_time = 0;
						$return_late_time = 0; //20100715
                        //申請状態 "0":申請不要 "3":承認済 //勤務時間を計算しないフラグ確認 20130225
                        if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
							for ($i = 1; $i <= 10; $i++) {
								$start_var = "o_start_time$i";
								if ($$start_var == "") {
									break;
								}
								$return_count++;

								$end_var = "o_end_time$i";
								if ($$end_var == "") {
									break;
								}

								//端数処理 20090709
								if ($fraction1 > "1") {
									//端数処理する分
									$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
									//開始日時
									$tmp_start_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$start_var);
									//基準日時
									$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
									$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
									$tmp_ret_start_time = substr($tmp_date_time, 8, 4);
								} else {
									$tmp_ret_start_time = str_replace(":", "", $$start_var);
								}
								if ($fraction2 > "1") {
									//端数処理する分
									$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
									//終了日時
									$tmp_end_date_time = $timecard_common_class->get_schedule_end_date_time(substr($end_date_time,0,8), substr($end_date_time,8,4), $$end_var);
									//基準日時
									$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
									$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

									$tmp_ret_end_time = substr($tmp_date_time, 8, 4);
								} else {
									$tmp_ret_end_time = str_replace(":", "", $$end_var);
								}
								$arr_ret = $timecard_common_class->get_return_array($end_date_time, $tmp_ret_start_time, $tmp_ret_end_time);

								//							$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
								$return_time += count($arr_ret);

								// 月の深夜勤務時間に退勤後復帰の深夜分を追加
								//$return_late_time += count(array_intersect($arr_ret, $arr_time5)); //20100715
								//計算方法変更 20110126
								//深夜残業
								$wk_ret_start = $tmp_date.$tmp_ret_start_time;
								$wk_ret_end = $timecard_common_class->get_schedule_end_date_time($tmp_date, $tmp_ret_start_time, $tmp_ret_end_time);
								$wk_late_night = $timecard_common_class->get_late_night($work_times_info, $wk_ret_start,$wk_ret_end, "");
								$return_late_time += $wk_late_night;
							}
						}
						$wk_return_time = $return_time;
						//残業時刻が入力された場合、重複するため、退勤後復帰時間を加算しない。 20100714 不要 20100910
						//if ($over_calc_flg) {
						//	$wk_return_time = 0; //集計用 $return_time 表示用
						//	$return_late_time = 0; //20100715
						//}

						// 前日までの計 = 週計
						$last_day_total = $work_time_for_week;

						$work_time_for_week += $work_time;
						$work_time_for_week += $wk_return_time;
						if ($tmp_date < $start_date) {
							$tmp_date = next_date($tmp_date);
							$counter_for_week++;
							continue;
						}
						//$sums[15] += $work_time; // 20100910

						//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
						//if ($night_duty_flag){
						//	$sums[15] += $duty_work_time;
						//}

/*
						// 残業時間を算出（分単位）
						if ($arr_irrg_info["irrg_type"] == "") {  // 変則労働適用外の場合

							$tmp_specified_time = $specified_time;
							// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
							if ($delay_overtime_flg == "2") {
								$tmp_specified_time = $specified_time - $start_time_info["diff_minutes"];
							}
							// 日々の表示値は普外・残外の減算前
//							$sums[16] += ($effective_time > 480) ? $effective_time - 480 : 0;
							if ($effective_time > $tmp_specified_time) {
								$sums[16] += $effective_time - $tmp_specified_time;
							} else {
								$sums[16] += 0;
							}
							switch ($others1) {
							case "2":  // 遅刻時間に加算する
								$sums[12] += $time3;
								break;
							case "3":  // 早退時間に加算する
								$sums[13] += $time3;
								break;
							case "4":  // 残業時間から減算する
								$sums[16] -= $time3;
								break;
							}
							switch ($others2) {
							case "2":  // 遅刻時間に加算する
								$sums[12] += $time4;
								break;
							case "3":  // 早退時間に加算する
								$sums[13] += $time4;
								break;
							case "4":  // 残業時間から減算する
								$sums[16] -= $time4;
								break;
							}

						} else if ($arr_irrg_info["irrg_type"] == "1") {  // 変則労働期間・週の場合
							if ($counter_for_week == 7) {
								$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
								if ($hol_minus == "t") {
									$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
								}
								$sums[16] += ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
							}
						}
*/
						// 深夜勤務時間を算出（分単位）
						$time2 = 0;
						//計算方法変更 20110126
						//残業承認時に計算、申請画面を表示しない場合は無条件に計算
						if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
								$timecard_bean->over_time_apply_type == "0") {
							//残業時刻がない場合で、所定終了、退勤を比較後残業計算
							if ($over_start_time == "" || $over_end_time == "") {
                                //if ($end2_date_time < $fixed_end_time) { //退勤時刻を使用しない 20120406
								//	$over_start_date_time = $end2_date_time;
								//	$over_end_date_time = $fixed_end_time;
								//} else {
									$over_start_date_time = ""; //20101109
									$over_end_date_time = "";
								//}
							}
							//深夜残業
							$time2 = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time,$over_end_date_time, "");
                            //休憩対応 20130827
                            if ($time2 > 0 && $start4 != "" && $end4 != "") {
                                $wk_rest_time = $timecard_common_class->get_intersect_times($over_start_date_time, $over_end_date_time, $start4_date_time, $end4_date_time);
                                if ($wk_rest_time > 0) {
                                    $late_night_info = $timecard_common_class->get_late_night_shift_info($tmp_date, $start5, $end5);  // 深夜帯情報
                                    //深夜時間帯か再確認
                                    $wk_s1 = max($over_start_date_time, $start4_date_time);
                                    $wk_e1 = min($over_end_date_time, $end4_date_time);
                                    $wk_rest_time = $timecard_common_class->get_times_info_calc($late_night_info, $wk_s1, $wk_e1);
                                    $time2 -= $wk_rest_time;
                                }
                            }
                            //休憩対応 20140710
                            $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                            $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                            if ($time2 > 0 && $rest_start_time1 != "" && $rest_end_time1 != "") {
                                $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_start_date_time1 = $rest_start_date1.$rest_start_time1;
                                $rest_end_date_time1 = $rest_end_date1.$rest_end_time1;
                                $next_date = next_date($tmp_date);
                                $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time1, $rest_end_date_time1, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean, $ovtm_class);
                                //深夜内の休憩
                                $overtime_rest_night = $arr_resttime_data["late_night"] + $arr_resttime_data["hol_late_night"];
                                $time2 -= $overtime_rest_night;
                                //echo "$tmp_date $rest_start_date_time1 $rest_end_date_time1 $time2 $overtime_rest_night<br>\n";
                            }
                            //残業３−５を追加、２−５の繰り返し処理とする
                            //残業２
                            for ($idx=2; $idx<=5; $idx++) {
                                $s_t = "over_start_time".$idx;
                                $e_t = "over_end_time".$idx;
                                $s_f = "over_start_next_day_flag".$idx;
                                $e_f = "over_end_next_day_flag".$idx;
                                $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                                $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                                $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                                $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                                if ($over_start_time_value != "" && $over_end_time_value != "") {
                                    //開始日時
                                    $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                                    //終了日時
                                    $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;
                                    //深夜時間帯の時間取得
                                    $over_time2_late_night = $timecard_common_class->get_late_night($work_times_info, $over_start_date_time_value, $over_end_date_time_value, "");

                                    //休憩除外
                                    $rs_t = "rest_start_time".$idx;
                                    $re_t = "rest_end_time".$idx;
                                    $rs_f = "rest_start_next_day_flag".$idx;
                                    $re_f = "rest_end_next_day_flag".$idx;
                                    $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                                    $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                                    $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                                    $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];

                                    //残業時間深夜内休憩
                                    $overtime_rest_night = 0;

                                    if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                        $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                        $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                        $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                        $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time_value, $rest_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean);

                                        //深夜内の休憩
                                        $overtime_rest_night =
                                            $arr_resttime_data["late_night"]
                                            + $arr_resttime_data["hol_late_night"];
                                        $over_time2_late_night -= $overtime_rest_night;
                                    }
                                    $time2 += $over_time2_late_night;
                                }
                            }
						}
						$time2 += $return_late_time; //20100715
						$sums[17] += $time2;

						// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
						//$sums[15] += $wk_return_time; //20100714 20100910
						//						$sums[16] += $return_time;
					} else {
					// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
					// 月の開始日前は処理しない
						if ($tmp_date < $start_date) {
							$tmp_date = next_date($tmp_date);
							$counter_for_week++;
							continue;
						}
					}
				} else {

					// 事由が「代替休暇」「振替休暇」の場合、
					// 所定労働時間を稼働時間にプラス
					// 「有給休暇」を除く $reason == "1" || 2008/10/14
					if ($reason == "4" || $reason == "17") {
						if ($tmp_date >= $start_date) {
							$sums[15] += $specified_time;
							$work_time_for_week += $specified_time;
						}
					}
/*
					if ($arr_irrg_info["irrg_type"] == "1" && $counter_for_week == 7) {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
						if ($hol_minus == "t") {
							$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
						}
						$sums[16] += ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					}
*/
					// 退勤後復帰回数を算出
					$return_count = 0;
					// 退勤後復帰時間を算出 20100413
					$return_time = 0;
                    //申請状態 "0":申請不要 "3":承認済 ※未承認時は計算しない //勤務時間を計算しないフラグ確認 20130225
					if (($return_link_type == "0" || $return_link_type == "3") && $no_count_flag != "1") {
						for ($i = 1; $i <= 10; $i++) {
							$start_var = "o_start_time$i";
							if ($$start_var == "") {
								break;
							}
							$return_count++;

							$end_var = "o_end_time$i";
							if ($$end_var == "") {
								break;
							}
							//端数処理
							if ($fraction1 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction1_min);
								//開始日時
								$tmp_start_date_time = $tmp_date.$$start_var;
								//基準日時
								$tmp_base_date_time = substr($tmp_start_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_start_date_time, $moving_minutes, $fraction1);
								$tmp_ret_start_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
							} else {
								$tmp_ret_start_time = str_replace(":", "", $$start_var);
							}
							if ($fraction2 > "1") {
								//端数処理する分
								$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
								//終了日時
								$tmp_end_date_time = $tmp_date.$$end_var;
								//基準日時
								$tmp_base_date_time = substr($tmp_end_date_time,0,10)."00";
								$tmp_date_time = date_utils::move_time($tmp_base_date_time, $tmp_end_date_time, $moving_minutes, $fraction2);

								$tmp_ret_end_time = str_replace(":", "",substr($tmp_date_time, 8, 5));
							} else {
								$tmp_ret_end_time = str_replace(":", "", $$end_var);
							}
							$arr_ret = $timecard_common_class->get_return_array($tmp_date.$tmp_ret_start_time, $tmp_ret_start_time, $tmp_ret_end_time);

							$return_time += count($arr_ret);

							//前日・翌日の深夜帯
							//$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯
							// 月の深夜勤務時間に退勤後復帰の深夜分を追加
							//$sums[17] += count(array_intersect($arr_ret, $arr_time5));
						}
					}
					$wk_return_time = $return_time;
					//分を時分に変換
					$return_time = minute_to_hmm($return_time);

					// 前日までの計 = 週計
					$last_day_total = $work_time_for_week;

					$work_time_for_week += $work_time;
					$work_time_for_week += $wk_return_time;

					if ($tmp_date < $start_date) {
						$tmp_date = next_date($tmp_date);
						$counter_for_week++;
						continue;
					}
				}

				// 残業時間の処理を変更 20090629
				$wk_hoteigai = 0;
				$wk_hoteinai_zan = 0;
				$wk_hoteinai = 0;

				//日数換算
				$total_workday_count = "";
				if (($start_time != "" && $end_time != "") || $after_night_duty_flag == "1") //明けを追加 20110819
				{
					//休暇時は計算しない 20090806
					if ($pattern != "10") {
						if($workday_count != "")
						{
							$total_workday_count = $workday_count;
						}
					}

					if($night_duty_flag)
					{
						//曜日に対応したworkday_countを取得する
						$total_workday_count = $total_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

					}
				}
				else if ($workday_count == 0){
					//日数換算に0が指定された場合のみ0を入れる
					$total_workday_count = $workday_count;
				}
				//法定外残業の基準時間
				if ($base_time == "") {
					$wk_base_time = 8 * 60;
				} else {
					$base_hour = intval(substr($base_time, 0, 2));
					$base_min = intval(substr($base_time, 2, 2));
					$wk_base_time = $base_hour * 60 + $base_min;
				}
				//公休か事由なし休暇で残業時刻がありの場合 20100802
				if ($legal_hol_over_flg) {
					$wk_base_time = 0;
				}
				//残業時間 20100209 変更 20100910
				$wk_zangyo = 0;
				$kinmu_time = 0;
				//出勤時刻、退勤時刻が両方入っている場合に計算する 20101004
                //勤務時間を計算しないフラグが"1"以外 20130225
                if ($start_time != "" && $end_time != "" && $no_count_flag != "1") {
					//残業承認時に計算、申請画面を表示しない場合は無条件に計算
					if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
							$timecard_bean->over_time_apply_type == "0"
						) {

						//残業開始、終了時刻がある場合
						if ($over_start_time != "" && $over_end_time != "") {
							$wk_zangyo = date_utils::get_time_difference($over_end_date_time,$over_start_date_time);
                            //休憩除外
                            $rest_start_time1 = $arr_atdbkrslt[$tmp_date]["rest_start_time1"];
                            $rest_end_time1 = $arr_atdbkrslt[$tmp_date]["rest_end_time1"];
                            if ($rest_start_time1 != "" && $rest_end_time1 != "") {
                                $rest_start_date1 = ($arr_atdbkrslt[$tmp_date]["rest_start_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                $rest_end_date1 = ($arr_atdbkrslt[$tmp_date]["rest_end_next_day_flag1"] == 1) ? next_date($tmp_date) :$tmp_date;
                                $wk_rest_time1 = date_utils::get_time_difference($rest_end_date1.$rest_end_time1,$rest_start_date1.$rest_start_time1);
                                $wk_zangyo -= $wk_rest_time1;
                            }
                            //残業時刻未設定は計算しない 20110825
						//} else {
						//	//残業時刻がない場合で、所定終了、退勤を比較後残業計算 20100913
						//	if ($end2_date_time < $fixed_end_time) {
						//		$wk_zangyo = date_utils::get_time_difference($fixed_end_time,$end2_date_time);
						//	}
						}
                        //残業３−５を追加、２−５の繰り返し処理とする
                        //残業２
                        for ($idx=2; $idx<=5; $idx++) {
                            $s_t = "over_start_time".$idx;
                            $e_t = "over_end_time".$idx;
                            $s_f = "over_start_next_day_flag".$idx;
                            $e_f = "over_end_next_day_flag".$idx;
                            $over_start_time_value = $arr_atdbkrslt[$tmp_date][$s_t];
                            $over_end_time_value = $arr_atdbkrslt[$tmp_date][$e_t];
                            $over_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$s_f];
                            $over_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$e_f];
                            if ($over_start_time_value != "" && $over_end_time_value != "") {
                                    //開始日時
                                    $over_start_date_time_value = ($over_start_next_day_flag_value == 1) ? next_date($tmp_date).$over_start_time_value : $tmp_date.$over_start_time_value;
                                    //終了日時
                                    $over_end_date_time_value = ($over_end_next_day_flag_value == 1) ? next_date($tmp_date).$over_end_time_value : $tmp_date.$over_end_time_value;

                                $wk_zangyo2 = date_utils::get_time_difference($over_end_date_time_value,$over_start_date_time_value);
                                //休憩除外
                                $rs_t = "rest_start_time".$idx;
                                $re_t = "rest_end_time".$idx;
                                $rs_f = "rest_start_next_day_flag".$idx;
                                $re_f = "rest_end_next_day_flag".$idx;
                                $rest_start_time_value = $arr_atdbkrslt[$tmp_date][$rs_t];
                                $rest_end_time_value = $arr_atdbkrslt[$tmp_date][$re_t];
                                $rest_start_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$rs_f];
                                $rest_end_next_day_flag_value = $arr_atdbkrslt[$tmp_date][$re_f];

                                //残業時間深夜内休憩
                                $overtime_rest = 0;

                                if ($rest_start_time_value != "" && $rest_end_time_value != "") {
                                    $rest_start_date_value = ($arr_atdbkrslt[$tmp_date][$rs_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_end_date_value = ($arr_atdbkrslt[$tmp_date][$re_f] == 1) ? next_date($tmp_date) :$tmp_date;
                                    $rest_start_date_time_value = $rest_start_date_value.$rest_start_time_value;
                                    $rest_end_date_time_value = $rest_end_date_value.$rest_end_time_value;
                                    $arr_resttime_data = get_overtime_data($work_times_info, $tmp_date, $pattern, $reason, $arr_timecard_holwk_day, $arr_type, $timecard_common_class, $rest_start_date_time_value, $rest_end_date_time_value, $arr_atdbkrslt[$next_date]["pattern"], $arr_atdbkrslt[$next_date]["reason"], $timecard_bean);

                                    //休憩
                                    $overtime_rest =
                                        $arr_resttime_data["normal_over"]
                                        + $arr_resttime_data["late_night"]
                                        + $arr_resttime_data["hol_over"]
                                        + $arr_resttime_data["hol_late_night"];
                                    //echo "$tmp_date overtime_rest=$overtime_rest";
                                    $wk_zangyo2 -= $overtime_rest;
                                }

                                $wk_zangyo += $wk_zangyo2;
                            }
                        }
						//早出残業
						if ($early_over_time != "") {
							$wk_early_over_time = hmm_to_minute($early_over_time);
							$wk_zangyo += $wk_early_over_time;
						}
					}
					//呼出
					$wk_zangyo += $wk_return_time;

                    //コメント化 20130822
					//if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
					//	//休日残業時は、休憩減算 20100916
					//	if ($legal_hol_over_flg == true) {
					//		if ($wk_zangyo >= ($time3_rest + $time4_rest)) {
					//			$wk_zangyo -= ($time3_rest + $time4_rest);
					//		}
					//	}
					//}
					//休日残業で休憩時刻がある場合 20100921
					if ($legal_hol_over_flg == true && $tmp_rest2 > 0) {
						$wk_zangyo -= $tmp_rest2;
					}
					//出勤時刻と残業開始時刻が一致している場合、所定、遅刻、早退を計算せず、残業、休憩のみ計算する 20100917
					if ($start2 != "" && $start2 == $over_start_time && $over_start_next_day_flag != "1") {//残業開始が翌日でないことを追加 20101027
						$overtime_only_flg = true;
						//遅刻
						$tikoku_time = 0;
						//早退
						$sotai_time = 0;
						//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100922
						if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
								$timecard_bean->over_time_apply_type == "0"
							) {
							$wk_zangyo -= $tmp_rest2;
						} else {
							$tmp_rest2 = 0;
							$time_rest = "";
						}
					} else {
						$overtime_only_flg = false;
                        $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                        //時間有休と相殺しない場合で時間有休ある場合 20140807
                        if ($ovtm_class->sousai_sinai_flg == "t" && $paid_hol_min != "") {
                            //遅刻
                            $tikoku_time = 0;
                            //早退
                            $sotai_time = 0;
                        }
                        else {
                            //遅刻
                            $tikoku_time = $start_time_info["diff_minutes"];
                            //早退
                            $sotai_time = $end_time_info["diff_minutes"];
                        }
					}
					$tikoku_time2 = 0;
					// 遅刻時間を残業時間から減算する場合
					if ($timecard_bean->delay_overtime_flg == "1") {
						if ($wk_zangyo >= $tikoku_time) { //20100913
							$wk_zangyo -= $tikoku_time;
						} else { // 残業時間より多い遅刻時間対応 20100925
							$tikoku_time2 = $tikoku_time - $wk_zangyo;
							$wk_zangyo = 0;
						}
					}

					//勤務時間＝所定労働時間−休憩時間−外出時間−早退−遅刻＋残業時間 20100907
					//　残業時間＝承認済み残業時間＋承認済み呼出し勤務時間
					//　承認済み残業時間＝早出残業時間＋勤務終了後残業時間
					//所定
					if ($pattern == "10" || //休暇は所定を0とする 20100916
							$overtime_only_flg == true ||	//残業のみ計算する 20100917
							$after_night_duty_flag == "1"  //明けの場合 20110819
						) {
						$shotei_time = 0;
					} elseif ($start2 == "" && $end2 == "") {  // 所定時刻指定がない場合、端数処理不具合対応 20101005
						$shotei_time = count($arr_effective_time_wk); //休憩を引く前の実働時間
					} else {
						$shotei_time = count($arr_time2);
					}
                    $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                    if ($obj_hol_hour->paid_hol_hour_flag == "t" && $paid_hol_min != "") {
                        //時間有休と相殺しない場合
                        if ($ovtm_class->sousai_sinai_flg == "t") {
                            //遅刻早退が時間有休より多い場合、その分は勤務時間から減算されるようにする
                            if ($start_time_info["diff_minutes"] != "") {
                                if ($start_time_info["diff_minutes"] >= $paid_hol_min) {
                                    $shotei_time -= $start_time_info["diff_minutes"];
                                }
                                else {
                                    $shotei_time -= $paid_hol_min;
                                }
                            }
                            if ($end_time_info["diff_minutes"] != "") {
                                if ($end_time_info["diff_minutes"] >= $paid_hol_min) {
                                    $shotei_time -= $end_time_info["diff_minutes"];
                                }
                                else {
                                    $shotei_time -= $paid_hol_min;
                                }
                            }
                        }
                        else {
                            $shotei_time -= $paid_hol_min;
                        }
                    }
                    //echo "<br>$tmp_date shotei_time=$shotei_time ";
                    //時間有休追加 20111207
					//if ($obj_hol_hour->paid_hol_hour_flag == "t") {
					//	$shotei_time -= $paid_time_hour;
					//}
					//休憩 $tmp_rest2 $time3_rest $time4_rest
					//外出 $time3 $time4
					$kinmu_time = $shotei_time - $time3 - $time4 + $wk_zangyo;
					// 遅刻時間を残業時間から減算しない場合
					if ($timecard_bean->delay_overtime_flg == "2") {
						$kinmu_time -= $tikoku_time;
					}
					// 遅刻時間を残業時間から減算する場合で、残業未承認の場合で遅刻がある場合は引く 20100914
					else {
						if ($overtime_approve_flg == "1") {
							$kinmu_time -= $tikoku_time;
						} else { //遅刻時間不具合対応 20101005
							// 残業時間より多い遅刻時間対応 20100925
							$kinmu_time -= $tikoku_time2;
						}
					}
                    //echo "<br>$tmp_date kinmu_time=$kinmu_time o_a_flg=$overtime_approve_flg  d_o_flg=".$timecard_bean->delay_overtime_flg;
                    // 早退時間を残業時間から減算しない場合
					if ($timecard_bean->early_leave_time_flg == "2") {
						$kinmu_time -= $sotai_time;
					}
					//時間給者 20100915
					if ($wage == "4" && $start4 == "" && $end4 == "") {  // 時間給かつ休憩時刻指定がない場合 20100925
                        //以下の条件を使用しない 20130822
                        //休日残業時は、休憩を減算しない 20100916
						//if ($legal_hol_over_flg != true) {
							$kinmu_time -= ($time3_rest + $time4_rest);
						//}
					} else {
						if ($overtime_only_flg != true && $legal_hol_over_flg != true) {	//残業のみ計算する場合と休日残業の場合、残業から引くためここでは引かない 20100921
							//月給制日給制、実働時間内の休憩時間
							$kinmu_time -= $tmp_rest2;
						}
					}

					//法定内、法定外残業の計算
					if ($wk_zangyo > 0) {
						//法定内入力有無確認
						if ($legal_in_over_time != "") {
							$wk_hoteinai_zan = hmm_to_minute($legal_in_over_time);
							if ($wk_hoteinai_zan <= $wk_zangyo) {
								$wk_hoteigai = $wk_zangyo - $wk_hoteinai_zan;
							} else {
								$wk_hoteigai = 0;
							}

						} else {
							//法定外残業の基準時間を超える分を法定外残業とする
							if ($kinmu_time > $wk_base_time) {
								$wk_hoteigai = $kinmu_time - $wk_base_time;
							}
							//
							if ($wk_zangyo - $wk_hoteigai > 0) {
								$wk_hoteinai_zan = $wk_zangyo - $wk_hoteigai;
							} else {
								//マイナスになる場合
								$wk_hoteinai_zan = 0;
								$wk_hoteigai = $wk_zangyo;
							}
						}
						//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 start
						if ($ovtm_class->delay_overtime_inout_flg == "t") {
							//時間有休、外出を追加 20141203
							if ($wk_hoteinai_zan > 0 &&
								(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"]) > 0 ||
								 $paid_hol_min > 0 ||
								 $time3 > 0)) {
								$wk_furikae_kouho = max(($start_time_info["diff_minutes"] + $end_time_info["diff_minutes"] + $time3), $paid_hol_min);
								$wk_furikae = min($wk_hoteinai_zan, $wk_furikae_kouho);
								$wk_hoteinai_zan -= $wk_furikae;
								$wk_hoteigai += $wk_furikae;
							}
						}
						//遅刻早退で法定内残業がある場合、法定外残業とする 20141024 end
					}
					//合計
					//法定内
					$wk_hoteinai = $kinmu_time - $wk_zangyo;
					$hoteinai += $wk_hoteinai;
					//法定内残業
					$hoteinai_zan += $wk_hoteinai_zan;
					//法定外残
					$hoteigai += $wk_hoteigai;
					//勤務時間
					$sums[15] += $kinmu_time;
					//残業時間
					$sums[16] += $wk_zangyo;
				}

//comment化
if ($dummy == "1") {
				//残業承認時に計算、申請画面を表示しない場合は無条件に計算 20100608
				if (($timecard_bean->over_time_apply_type != "0" && $overtime_approve_flg == "2") ||
						$timecard_bean->over_time_apply_type == "0"
						|| $wk_return_time > 0 // 退勤後復帰がある場合 20100714
						|| $legal_hol_over_flg) { // 公休か残業時刻ありを条件追加 20100802
					//休暇時は計算しない 20090806
					if ($pattern != "10" ||
						$legal_hol_over_flg) { // 公休で残業時刻ありを条件追加 20100802

						// 計算用の稼働時間
						//			$work_time_calc = $work_time;
						// 遅刻時間を残業時間から減算しない場合、計算用の稼働時間に
						//			if ($delay_overtime_flg == "2") {
						//				$work_time_calc = $work_time + $start_time_info["diff_minutes"];
						//			}

						//法定内勤務、法定内残業、法定外残業
						// 前日までの累計 >= 40時間
						//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
						if ($last_day_total >= 40 * 60 && $overtime_40_flg == "1") {
							//  法定外 += 稼動時間
							//下の処理で合計するためここはコメント化
							//			$hoteigai += $work_time + $wk_return_time;
						} else if ($work_time + $wk_return_time > 0){
							// 上記以外、かつ、 稼動時間がある場合

							//法定内勤務と法定外残業
							//法定内勤務のうち週40時間を超える分から法定外残業へ移動
							//wk法定外
							//残業時刻から計算 20100525
							if ($over_calc_flg) {
								/* 20100713
								// 端数処理する
								if ($fraction2 > "1") {
									//端数処理する分
									$moving_minutes = $timecard_common_class->get_moving_minutes($fraction2_min);
									//基準日時
									if ($end2_date_time != "") {
										$tmp_office_start_time = substr($end2_date_time, 0, 10) . "00";
									} else {
										$tmp_office_start_time = substr($start_date_time, 0, 10) . "00";
									}
									$tmp_over_end_date_time = date_utils::move_time($tmp_office_start_time, $over_end_date_time, $moving_minutes, $fraction2);
								} else {
								*/
									$tmp_over_end_date_time = $over_end_date_time;
								//}
								$arr_over_time = $timecard_common_class->get_minute_time_array($over_start_date_time,$tmp_over_end_date_time);
								//20100712
								$wk_hoteigai = count(array_diff($arr_over_time, $arr_time4));
								$wk_hoteigai_org = $wk_hoteigai;
								if ($wk_hoteigai <= $effective_time) {
									$wk_hoteigai = $effective_time - $wk_base_time;
									if ($wk_hoteigai < 0) {
										$wk_hoteigai = 0;
									}
								}

							}else {
								$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
							}
							if ($wk_hoteigai > 0) {
								//wk法定内 = (稼動時間 - wk法定外)
								$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);

								// 40時間を超えた法定外を除外する
								if ($overtime_40_flg == "1") {
									$wk_jogai = $last_day_total + $work_time + $wk_return_time - (40 * 60);
									if ($wk_jogai > 0) {
										$wk_hoteigai -= $wk_jogai;
									}
								}

								//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
							} else {
								//wk法定内 = (稼動時間)
								$wk_hoteinai = ($work_time + $wk_return_time);

								//マイナスを0とする
								$wk_hoteigai = 0;
							}
							//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug
							//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
							if ($last_day_total + $wk_hoteinai > 40 * 60) {
								if ($overtime_40_flg == "1") {
									$wk_hoteigai = 0;
									//法定内 += (40時間 - 前日までの累計) 20090722 変更
									$hoteinai += (40 * 60) - $last_day_total;
								} else {
									//法定内 += wk法定内 20090722 変更
									$hoteinai += $wk_hoteinai;
								}


								//$arr_week_hoteigai[$counter_for_week] = $wk_hoteigai;
							} else {
								//上記以外
								//法定内 += wk法定内
								$hoteinai += $wk_hoteinai;
								//echo("<br>2 $tmp_date hoteinai=$hoteinai");

							}

							//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
							if ($overtime_40_flg == "1") {
								if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
									$wk_hoteigai += $time3;
								}
								if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
									$wk_hoteigai += $time4;
								}
								switch ($others1) {
									case "2":  // 遅刻時間に加算する
										$sums[12] += $time3;
										break;
									case "3":  // 早退時間に加算する
										$sums[13] += $time3;
										break;
									//						case "4":  // 残業時間から減算する
									//							$sums[16] -= $time3;
									//							break;
								}
								switch ($others2) {
									case "2":  // 遅刻時間に加算する
										$sums[12] += $time4;
										break;
									case "3":  // 早退時間に加算する
										$sums[13] += $time4;
										break;
									//						case "4":  // 残業時間から減算する
									//							$sums[16] -= $time4;
									//							break;
								}
							}

							//echo("$tmp_date w=$wk_week_hoteigai g=$wk_hoteigai "); //debug
							// 法定内残業、所定労働時間から8時間まで間を加算
							// 稼動時間が所定労働時間より大きい場合
							//					$wk_minute = date_utils::hi_to_minute($day1_time);
							$wk_minute = $specified_time;
							// 時間帯の法定外残業の計算を使用する 480 -> $wk_base_time 20091218 20100713
							if (($wk_minute != 0 && $wk_minute < $wk_base_time && ($work_time + $wk_return_time) > $wk_minute) || $over_calc_flg) {

								//稼動時間が法定外残業の計算時間以上
								if (($work_time + $wk_return_time) >= $wk_base_time) {
									//基準法定内残業
									$wk_std_hoteinai_zan = $wk_base_time - $wk_minute;
								} else {
									//8時間未満の場合、稼動時間-所定労働時間
									$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
									//20100713
									if ($over_calc_flg && $wk_hoteigai_org > ($wk_base_time - $specified_time)) {
										$wk_std_hoteinai_zan = $wk_hoteinai;
									}
								}

								//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
								if ($overtime_40_flg == "1") {
									//前日までの累計 + 所定時間 ＜ 40時間 の場合
									if ($last_day_total + $wk_minute < 40 * 60) {
										//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
										$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
										//wk法定内残業 > 基準法定内残業
										if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
											// wk法定内残業 = 基準法定内残業
											$wk_hoteinai_zan = $wk_std_hoteinai_zan;
										}
										//法定内勤務 -= wk法定内残業
										$hoteinai -= $wk_hoteinai_zan;

										//法定内残業 += wk法定内残業
										$hoteinai_zan += $wk_hoteinai_zan;

										$sums[16] += $wk_hoteinai_zan; // 変更 20090722
									}
								} else {
									// wk法定内残業 = 基準法定内残業
									$wk_hoteinai_zan = $wk_std_hoteinai_zan;
									//法定内勤務 -= wk法定内残業
									$hoteinai -= $wk_hoteinai_zan;

									//法定内残業 += wk法定内残業
									$hoteinai_zan += $wk_hoteinai_zan;

									$sums[16] += $wk_hoteinai_zan; // 変更 20090722
								}
							}
							//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
						}

					}

					//1日毎の法定外残を週単位に集計
					$wk_week_hoteigai += $wk_hoteigai;

					//法定外残業で週４０時間超過分 1:残業時間として計算するの場合
					if ($overtime_40_flg == "1") {
						if ($counter_for_week == 7) {
							if ($arr_irrg_info["irrg_type"] == "1") {
								$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
							} else {
								$tmp_irrg_minutes = 40 * 60; // 変則労働期間以外の場合40時間
							}
							if ($hol_minus == "t") {
								$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
							}
							$time1 = ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
							//						$sums[16] += $wk_week_hoteigai;
							$wk_hoteigai = $time1 + $wk_week_hoteigai;
							$hoteigai += $wk_hoteigai;

						} else {
							$wk_hoteigai = "";
						}
						//20090722 前日までの累計+稼働時間 >= 40時間、かつ、半日の場合、所定時間を基準とする
						if ($last_day_total + $work_time + $wk_return_time >= 40 * 60 && $workday_count == 0.5) {
							$wk_base_time = $specified_time;
						}
						$wk_hoteigai_yoteigai = $work_time + $wk_return_time - $wk_base_time;
						if ($wk_hoteigai_yoteigai > 0) {
							if ($others1 != "4") { //勤務時間内に外出した場合が、残業時間から減算するでない場合
								$wk_hoteigai_yoteigai += $time3;
							}
							if ($others2 != "4") { //勤務時間外に外出した場合が、残業時間から減算するでない場合
								$wk_hoteigai_yoteigai += $time4;
							}
							$sums[16] += $wk_hoteigai_yoteigai;
						}
					} else {

						$time1 = 0;
						// 出退勤とも打刻済みの場合
						if ($start_time != "" && $end_time != "") {
							//						$tmp_specified_time = $specified_time;
							//						$tmp_specified_time = 480; //法定内8時間を所定として処理 20090717
							$tmp_specified_time = $wk_base_time; //日数換算対応 20090803
							// 遅刻時間を残業時間から減算しない場合（所定時間から引くことで合わせる）
							if ($delay_overtime_flg == "2") {
								$tmp_specified_time = $tmp_specified_time - $start_time_info["diff_minutes"];
								if ($tmp_specified_time < 0) {
									$tmp_specified_time = 0;
								}
							}
							//休暇時は計算しない 20090806 公休か事由なし休暇で残業時刻が設定済を条件追加 20100802
							if ($pattern != "10" || $legal_hol_over_flg) {
								//残業時刻から計算 20100525
								if ($over_calc_flg) {
									//法定外 20100705
									$time1 = $wk_hoteigai;

								} else {
									if ($effective_time > $tmp_specified_time) {
										$time1 = $effective_time - $tmp_specified_time;
									} else {
										$time1 = $wk_hoteigai; //20100714
									}
								}

								if ($others1 == "4") { //勤務時間内に外出した場合
									if ($time1 > $time3) {
										$time1 -= $time3;
									}
								}
								if ($others2 == "4") { //勤務時間外に外出した場合
									if ($time1 > $time4) {
										$time1 -= $time4;
									}
								}

								switch ($others1) {
									case "2":  // 遅刻時間に加算する
										$sums[12] += $time3;
										break;
									case "3":  // 早退時間に加算する
										$sums[13] += $time3;
										break;
									//							case "4":  // 残業時間から減算する
									//								$sums[16] -= $time3;
									//								break;
								}
								switch ($others2) {
									case "2":  // 遅刻時間に加算する
										$sums[12] += $time4;
										break;
									case "3":  // 早退時間に加算する
										$sums[13] += $time4;
										break;
									//							case "4":  // 残業時間から減算する
									//								$sums[16] -= $time4;
									//								break;
								}
							}
						}
						$wk_hoteigai = $time1; //20100714

						$sums[16] += $wk_hoteigai;
						$hoteigai += $wk_hoteigai;

					}
					//早出残業が入力された場合 20100601
					if ($early_over_time != "") {
						//申請中の場合は計算しない	// 残業未承認か確認
						if (!($overtime_approve_flg == "1" || ($return_link_type != "0" && $return_link_type != "3"))) {

							$wk_early_over_time = hmm_to_minute($early_over_time);
							$work_time += $wk_early_over_time;
							$wk_zangyo += $wk_early_over_time;
							$wk_hoteinai_zan += $wk_early_over_time;
							//合計する
							$sums[15] += $wk_early_over_time;
							$sums[16] += $wk_early_over_time;
							$hoteinai_zan += $wk_early_over_time;
						}

					}
				}
} //comment化
				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
			} // end of while

			// 早退時間を残業時間から減算する設定の場合、残業から減算する 20100713
			if ($timecard_bean->early_leave_time_flg == "1") {
				$sums[16] -= $sums[13];
			}
			// 要勤務日数を取得 20091222
			$wk_year = substr($c_yyyymm, 0, 4);
			$wk_mon = substr($c_yyyymm, 4, 2);
//			$sums[0] = get_you_kinmu_su($con, $fname, $wk_year, $wk_mon, $arr_timecard_holwk_day, $closing);
			//当月日数
			if ($closing_month_flg == "2") { //締め日の月度が終了日の月の場合
				$wk_year2 = substr($start_date, 0, 4);
				$wk_mon2 = substr($start_date, 4, 2);
			} else {
				$wk_year2 = $wk_year;
				$wk_mon2 = $wk_mon;
			}
			$days_in_month = days_in_month($wk_year2, $wk_mon2);
			//対象年月の公休数を取得 20100615
			$legal_hol_cnt = get_legal_hol_cnt_month($con, $fname, $wk_year, $wk_mon, $closing, $closing_month_flg, $tmcd_group_id);
			$sums[0] = $days_in_month - $legal_hol_cnt;

			// 休日出勤 2008/10/23
			// 休日出勤のカウント方法変更 20091127
			//$sums[2] = count($arr_hol_work_date);

			// 変則労働期間が月の場合
			if ($arr_irrg_info["irrg_type"] == "2") {

				// 所定労働時間を基準時間とする
				$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
				if ($hol_minus == "t") {
					$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
				}
				$sums[14] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

				// 稼働時間−基準時間を残業時間とする
				$sums[16] = $sums[15] - $sums[14];
			}
			else {
			// 基準時間の計算を変更 2008/10/21
			// 要勤務日数 × 所定労働時間
				$sums[14] = $sums[0] * date_utils::hi_to_minute($day1_time);
			}

			// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
			for ($i = 15; $i <= 17; $i++) {
				if ($sums[$i] < 0) {
					$sums[$i] = 0;
				}
				$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
			}
			// 残業管理をしない場合、残業時間を０とする
			if ($no_overtime == "t") {
				$sums[16] = 0;
			}
			if ($obj_hol_hour->paid_hol_hour_flag == "t") {
				//１日分の所定労働時間(分)
				//$specified_time_per_day = $obj_hol_hour->get_specified_time($tmp_emp_id, $day1_time);
				//当月分
				//$wk_day = $sums[18] / $specified_time_per_day;
				//$sums[7] += $obj_hol_hour->edit_nenkyu_zan($wk_day);
                //HH:MM形式
                if ($sums[18] > 0) {
                    $sums[7] .= "(".minute_to_hmm($sums[18]).")";
                }
			}
            if ($sums[7] == "") {
                $sums[7] = "0 ";
            }
            // 締め済みの場合
		} else {

			// 月集計値の取得
			$sql = "select days1, days2, days3, days4, days5, days6, days7, days8, days9, days10, count1, count2, time1, time2, time3, time4, time5, time6, time11 from wktotalm";
			$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sums[0] += pg_fetch_result($sel, 0, "days1");
			$sums[1] += pg_fetch_result($sel, 0, "days2");
			$sums[2] += pg_fetch_result($sel, 0, "days3");
			$sums[3] += pg_fetch_result($sel, 0, "days4");
			$sums[4] += pg_fetch_result($sel, 0, "days5");
			$sums[5] += pg_fetch_result($sel, 0, "days6");
			$sums[6] += pg_fetch_result($sel, 0, "days7");
			$sums[7] += pg_fetch_result($sel, 0, "days8");
			$sums[8] += pg_fetch_result($sel, 0, "days9");
			$sums[9] += pg_fetch_result($sel, 0, "days10");
			$sums[10] += pg_fetch_result($sel, 0, "count1");
			$sums[11] += pg_fetch_result($sel, 0, "count2");
			$sums[12] += hmm_to_minute(pg_fetch_result($sel, 0, "time1"));
			$sums[13] += hmm_to_minute(pg_fetch_result($sel, 0, "time2"));
			$sums[14] += hmm_to_minute(pg_fetch_result($sel, 0, "time3"));
			$sums[15] += hmm_to_minute(pg_fetch_result($sel, 0, "time4"));
			$sums[16] += hmm_to_minute(pg_fetch_result($sel, 0, "time5"));
			//深夜勤務 20110126
			if ($sinnya_flg == "1") {
				$sums[17] += hmm_to_minute(pg_fetch_result($sel, 0, "time6"));
			} else {
				//深夜残業
				$sums[17] += hmm_to_minute(pg_fetch_result($sel, 0, "time11"));
			}
            //時間有休追加
            $paid_time_hour = 0;
            if ($obj_hol_hour->paid_hol_hour_flag == "t") {
                //時間有休データをまとめて取得
                $arr_hol_hour_dat = $obj_hol_hour->get_paid_hol_hour_month($tmp_emp_id, $wk_start_date, $end_date);
                $tmp_date = $start_date;
                while ($tmp_date <= $end_date) {
                    $paid_hol_min =  $arr_hol_hour_dat[$tmp_date]["calc_minute"];
                    $sums[18] += $paid_hol_min; //時間有休計
                    $tmp_date = next_date($tmp_date);
                }
                //HH:MM形式
                if ($sums[18] > 0) {
                    //１日分の所定労働時間(分)
                    $specified_time_per_day = $obj_hol_hour->get_specified_time($tmp_emp_id, $day1_time);
                    $wk_day = $sums[18] / $specified_time_per_day;
                    $sums[7] -= $obj_hol_hour->edit_nenkyu_zan($wk_day);
                    $sums[7] .= "(".minute_to_hmm($sums[18]).")";
                }
                if ($sums[7] == "") {
                    $sums[7] = "0 ";
                }

            }
        }

		// 月集計値を表示用に編集
		for ($i = 0; $i <= 9; $i++) {
            if (!($obj_hol_hour->paid_hol_hour_flag == "t" && $i == 7)) { //時間有休を許可する場合の有休を除外
                $sums[$i] .= "日";
            }
		}
		for ($i = 10; $i <= 11; $i++) {
			$sums[$i] .= "回";
		}
		for ($i = 12; $i <= 17; $i++) {
			$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
		}

		// 表示処理
		echo("<tr height=\"22\">\n");
		echo("<td nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_personal_id</font></td>\n");
		echo("<td nowrap><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_name</font></td>\n");
		for ($i = 0; $i < 18; $i++) {
			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . nbsp($sums[$i]) . "</font></td>\n");
		}
		echo("<td align=\"center\" nowrap>\n");
		if ($tmp_closed) {  // 締め済みの場合
			echo("<input type=\"checkbox\" name=\"cancel_id[]\" value=\"$tmp_emp_id\">&nbsp;");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">済</font>");
		} else {  // 締め未済の場合
			//勤務パターンが1件もない場合の対処 20100108
			if ($ptn_day_cnt == 0) {
				echo("<input type=\"checkbox\" name=\"c_emp_id[]\" value=\"$tmp_emp_id\" disabled>");
			} else {
				echo("<input type=\"checkbox\" name=\"c_emp_id[]\" value=\"$tmp_emp_id\" checked>");
			}
		}
		echo("</td>\n");
		echo("</tr>\n");
	}
}
?>
