<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
// 勤務時間の設定
// work_admin_csv_output_target.php
// 2:常勤設定時は、非常勤者の勤務時間を0とし、3:非常勤設定時は、常勤者の勤務時間を0とする
// 上記以外の1,""はそのまま出力
//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
}
$title2 = "勤務時間の設定";
?>
<title>CoMedix <? echo($title1." | ".$title2); ?></title>
<?php
require_once("about_comedix.php");
require_once("work_admin_csv_common.ini");
require_once("duty_shift_mprint_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
//登録時
if ($postback == "1") {
    update_output_target($con, $fname, $output_target, $layout_id, $from_flg);
} else {
	//初期表示時、DBから取得
	
	//出力対象 1,"":全ての職員 2:常勤 3:非常勤を取得
    $arr_csv_config = get_timecard_csv_config($con, $fname, $layout_id, $from_flg);
    $output_target = $arr_csv_config["output_target"];
	if ($output_target == "") {
		$output_target = "1";
	}
}
//レイアウト名取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, $layout_id);
    $layout_name = $arr_layout[$layout_id]["name"];
}
else {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
    $layout_name = $arr_layout_mst["layout_name"];
}

?>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function regist() {

	document.mainform.postback.value = "1";
	document.mainform.action = "work_admin_csv_output_target.php";
	document.mainform.submit();
}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>勤務時間の設定</b></font></td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>

<br>
<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
if ($from_flg != "2") {
    echo("レイアウト".$layout_id);
}
else {
    echo("帳票名称");
}        
echo(" ");
echo($layout_name); ?></font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出力対象</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　<label for="target1"><input type="radio" id="target1" name="output_target" value="1"<? if ($output_target == "1") {echo " checked ";} ?>>全ての職員</label><br>
　<label for="target2"><input type="radio"  id="target2" name="output_target" value="2"<? if ($output_target == "2") {echo " checked ";} ?>>常勤</label><br>
　<label for="target3"><input type="radio"  id="target3" name="output_target" value="3"<? if ($output_target == "3") {echo " checked ";} ?>>非常勤</label></font><br><br>
</td>
</tr>
	<tr height="22">
		<td align="center">
			<input type="button" value="登録" onclick="regist();">&nbsp;&nbsp;
		</td>
	</tr>
</table>
<?
if ($postback == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
登録しました。
</font>
<?	
}
?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- ＨＩＤＤＥＮ -->
	<!-- ------------------------------------------------------------------------ -->
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="postback" value="">
	<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
	<input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
</form>

</body>
<? pg_close($con); ?>

</html>
<?
//設定項目更新
function update_output_target($con, $fname, $output_target, $layout_id, $from_flg) {
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_config";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_config";
        $wk_layout_id = "$layout_id";
    }
    
	///-----------------------------------------------------------------------------
	// トランザクションを開始
	///-----------------------------------------------------------------------------
	pg_query($con, "begin transaction");
	
    $sql = "select output_target from $tablename";
    $cond = "where layout_id = $wk_layout_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		
        $sql = "update $tablename set";
		$set = array("output_target");
		$setvalue = array($output_target);
        $cond = "where layout_id = $wk_layout_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
	} else {		
        $sql = "insert into $tablename (output_target, layout_id";
		$sql .= ") values (";
		$content = array($output_target, $layout_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	///-----------------------------------------------------------------------------
	// トランザクションをコミット
	///-----------------------------------------------------------------------------
	pg_query("commit");
}

?>
