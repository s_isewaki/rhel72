<?
require_once("about_comedix.php");

$summary_naiyo = str_replace("&#160;", " ", $_REQUEST["summary_naiyo_escaped_amp"]);
$summary_naiyo = str_replace("&nbsp;", " ", $summary_naiyo);
$summary_naiyo = str_replace("&amp;", "&", $summary_naiyo);
$summary_naiyo = h($summary_naiyo);

// フリー記載の場合はそのまま
if (!trim(@$xml_file)){
	$summary_naiyo = @$_REQUEST["summary_naiyo"];
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//========================================
//ファイルの読み込み
//========================================
require("about_validation.php");
require("get_values.php");
require("label_by_profile_type.ini");
require_once("summary_common.ini");
require_once("sot_util.php");


//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

//========================================
// 診療区分コードがあれば、コードから診療区分名を取得して、再格納する。
//========================================
$sql = "select diag_div from divmst where div_id = ".(int)$_REQUEST["sel_diag"];
$sel = select_from_table($con,$sql,"",$fname);
$tmp = pg_fetch_result($sel, 0, 0);
if ($tmp) { $_REQUEST["sel_diag"] = $tmp; $sel_diag = $tmp; }

//========================================
// 同一ユーザがレポート登録処理を並列実行するのは不可
//========================================
pg_query($con, "begin");

$login_emp_id = get_emp_id($con, $session, $fname);
$sel_sumxml = $c_sot_util->select_from_table("select oid, ptif_id, smry_xml, smry_html from sum_xml where xml_file_name = 'wk_" . $login_emp_id . ".xml' for update");

$tmp_record_count = pg_num_rows($sel_sumxml);
$has_error = false;

// ユーザの一時レコードが1件もなければ、コピー元データをロックする
if ($tmp_record_count === 0) {
	$sel_copy = $c_sot_util->select_from_table("select * from sum_xml where xml_file_name = '" . pg_escape_string($xml_file) . ".xml' for update");
} else {
	$first_ptif_id = pg_fetch_result($sel_sumxml, 0, "ptif_id");
	$first_smry_xml = pg_fetch_result($sel_sumxml, 0, "smry_xml");
	$first_smry_html = pg_fetch_result($sel_sumxml, 0, "smry_html");

	// 別患者の一時レコードができていたらエラー
	if ($first_ptif_id !== $pt_id) {
		$has_error = true;
	} else {
		for ($i = 1; $i < $tmp_record_count; $i++) {

			// 内容の異なる一時レコードが複数できていたらエラー
			if (pg_fetch_result($sel_sumxml, $i, "ptif_id") !== $first_ptif_id ||
				pg_fetch_result($sel_sumxml, $i, "smry_xml") !== $first_smry_xml ||
				pg_fetch_result($sel_sumxml, $i, "smry_html") !== $first_smry_html
			) {
				$has_error = true;
				break;
			}
		}

		// すべて同じ内容であれば1件のみにする
		if ($tmp_record_count > 1) {
			$sql = "delete from sum_xml";
			$cond = "where xml_file_name = 'wk_".$login_emp_id.".xml'".
					" and oid <> " . pg_fetch_result($sel_sumxml, 0, "oid");
			$del = delete_from_table($con, $sql, $cond, $fname);
			if ($del == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				summary_common_show_error_page_and_die();
			}
		}
	}
}

if ($has_error) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"記録データのエラーが発生しました。レポート画面から再入力してください\");</script>\n");
	echo("<script language=\"javascript\">location.href=\"./summary_new.php?session=".$session."&p_sect_id=".$sect_id."&pt_id=".$pt_id."&diag=".urlencode($sel_diag)."\";</script>\n");
	exit;
}

//========================================
// 添付ファイルの存在確認
//========================================
$filename = (array)@$filename;
if (!is_dir("summary")) mkdir("summary", 0755);
if (!is_dir("summary/tmp")) mkdir("summary/tmp", 0755);
if (!is_dir("summary/attach")) mkdir("summary/attach", 0755);
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");
	$tmp_filename = "summary/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。".$tmp_filename."');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

?>
<body>
<form name="items" action="summary_update.php" method="post">
	<input type="hidden" name="summary_id" value="<?=$summary_id?>">
	<input type="hidden" name="session" value="<?=$session?>">
	<input type="hidden" name="date" value="<?=$sel_yr.$sel_mon.$sel_day?>">
	<input type="hidden" name="summary_problem" value="<?=$summary_problem?>">
	<input type="hidden" name="summary_div" value="<?=$sel_diag?>">
	<input type="hidden" name="priority" value="<?=$priority?>">
	<input type="hidden" name="pt_id" value="<?=$pt_id?>">
	<input type="hidden" name="xml_file" value="<?=$xml_file?>">
	<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
	<input type="hidden" name="problem_id" value="<?=$problem_id?>">
	<input type="hidden" name="summary_naiyo" value="<?=$summary_naiyo?>">
	<input type="hidden" name="sect_id" value="<?=$sect_id?>">
	<input type="hidden" name="no_tmpl_window" value="1">
	<input type="hidden" name="apply_stat" value="<?=$apply_stat?>">
	<input type="hidden" name="is_apply_stat_change_only" value="<?=$is_apply_stat_change_only?>">
	<input type="hidden" name="content" value="<? echo($content); ?>">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
	<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
	<input type="hidden" name="back" value="t">
	<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
	<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
	<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
	<input type="hidden" name="dairi_emp_id" value="<?=$dairi_emp_id?>">
	<input type="hidden" name="dairi_finish" value="<?=$dairi_finish?>">
	<input type="hidden" name="seiki_emp_id" value="<?=$seiki_emp_id?>">
	<?
	foreach ($filename as $tmp_filename) {
		echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
	}
	$file_id = (array)@$file_id;
	foreach ($file_id as $tmp_file_id) {
		echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
	}
	for ($i = 1; $i <= (int)@$approve_num; $i++) {
		echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"".$_REQUEST["regist_emp_id".$i]."\">\n");
	}
	?>
</form>

<?




//========================================
//入力チェック
//========================================
if($sect_id ==""){
	$profile_type = get_profile_type($con, $fname);// 組織タイプを取得
	$section_title = $_label_by_profile["SECTION"][$profile_type];// 診療科/担当部門
	echo("<script language=\"javascript\">alert(\"{$section_title}を入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if($summary_problem ==""){
	echo("<script language=\"javascript\">alert(\"プロブレムを入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if($summary_naiyo ==""){
	echo("<script language=\"javascript\">alert(\"更新内容を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//========================================
//プロブレムリスト存在チェック
//========================================
if($problem_id != "0") {
	$sql = "select count(*) from summary_problem_list where (ptif_id = '$pt_id') and (problem_id = '$problem_id')";
	$sel = $c_sot_util->select_from_table($sql);
	$num=pg_result($sel,0,"count");
	if($num == 0) {
		pg_close($con);
		echo("<script language=\"javascript\">alert(\"指定されたプロブレムは既に削除されています\");</script>\n");
		echo("<script language=\"javascript\">history.back();</script>\n");
		exit;
	}
}

// 未指定の承認者を許可する
if ($wkfw_id && $_REQUEST["apply_stat"] != "2") {
	$tmp_arr_apv = array();
	$tmp_apv_order = 0;
	$tmp_pre_apv_order = 0;
	for ($i = 1; $i <= $approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";
		$tmp_apv_common_group_id_var = "apv_common_group_id$i";

		$tmp_org_apv_order = $$tmp_apv_order_var;
		$tmp_org_pst_approve_num_var = "pst_approve_num$tmp_org_apv_order";
		$tmp_radio_emp_id_var = "radio_emp_id$tmp_org_apv_order";

		if ($$tmp_regist_emp_id_var != "" || ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "")) {
			if ($$tmp_apv_order_var != $tmp_pre_apv_order) {
				$tmp_apv_order++;
				$tmp_apv_sub_order = 1;
				$tmp_pre_apv_order = $$tmp_apv_order_var;
			} else {
				$tmp_apv_sub_order += 1;
			}

			if ($tmp_apv_sub_order == 2) {
				$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
			}

			$tmp_arr_apv[] = array(
				"regist_emp_id" => $$tmp_regist_emp_id_var,
				"st_div" => $$tmp_st_div_var,
				"parent_pjt_id" => $$tmp_parent_pjt_id_var,
				"child_pjt_id" => $$tmp_child_pjt_id_var,
				"apv_order" => $tmp_apv_order,
				"apv_sub_order" => (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
				"multi_apv_flg" => $$tmp_multi_apv_flg_var,
				"next_notice_div" => $$tmp_next_notice_div_var,
				"apv_common_group_id" => $$tmp_apv_common_group_id_var,
				"org_apv_order" => $tmp_org_apv_order
			);
		}

		if ($$tmp_org_pst_approve_num_var != "" && $$tmp_radio_emp_id_var != "" && $tmp_apv_order != $tmp_org_apv_order) {
			$tmp_pst_approve_num_var = "pst_approve_num$tmp_apv_order";
			$$tmp_pst_approve_num_var = $$tmp_org_pst_approve_num_var;

			$base_varnames = array("pst_emp_id", "pst_st_div", "pst_parent_pjt_id", "pst_child_pjt_id");
			for ($j = 1; $j <= $$tmp_pst_approve_num_var; $j++) {
				foreach ($base_varnames as $base_varname) {
					$org_varname = "$base_varname{$tmp_org_apv_order}_{$j}";
					$new_varname = "$base_varname{$tmp_apv_order}_{$j}";
					$$new_varname = $$org_varname;
					unset($$org_varname);
				}
			}

			unset($$tmp_org_pst_approve_num_var);
		}

		unset($$tmp_regist_emp_id_var);
		unset($$tmp_st_div_var);
		unset($$tmp_parent_pjt_id_var);
		unset($$tmp_child_pjt_id_var);
		unset($$tmp_apv_order_var);
		unset($$tmp_apv_sub_order_var);
		unset($$tmp_multi_apv_flg_var);
		unset($$tmp_next_notice_div_var);
		unset($$tmp_apv_common_group_id_var);
	}

	$approve_num = count($tmp_arr_apv);
	$i = 1;
	foreach ($tmp_arr_apv as $tmp_apv) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";
		$tmp_apv_common_group_id_var = "apv_common_group_id$i";
		$tmp_org_apv_order_var = "org_apv_order$i";

		$$tmp_regist_emp_id_var = $tmp_apv["regist_emp_id"];
		$$tmp_st_div_var = $tmp_apv["st_div"];
		$$tmp_parent_pjt_id_var = $tmp_apv["parent_pjt_id"];
		$$tmp_child_pjt_id_var = $tmp_apv["child_pjt_id"];
		$$tmp_apv_order_var = $tmp_apv["apv_order"];
		$$tmp_apv_sub_order_var = $tmp_apv["apv_sub_order"];
		$$tmp_multi_apv_flg_var = $tmp_apv["multi_apv_flg"];
		$$tmp_next_notice_div_var = $tmp_apv["next_notice_div"];
		$$tmp_apv_common_group_id_var = $tmp_apv["apv_common_group_id"];
		$$tmp_org_apv_order_var = $tmp_apv["org_apv_order"];

		$i++;
	}
	unset($tmp_arr_apv);
}

//========================================
// apply_idを採番する。
// apply_noは前回をひきつぐ。
//========================================
$old_apply_id = $apply_id;
$sum_apply = select_from_table($con, "select * from sum_apply where apply_id = ".(int)$apply_id, "", $fname);
$current_apply_stat = (int)@pg_result($sum_apply,0,"apply_stat");
$current_dairi_kakunin_ymdhms = (int)@pg_result($sum_apply,0,"dairi_kakunin_ymdhms");

// 承認連番IDの採番方法変更 (採番用テーブルにて管理)
require_once("sum_applycnt.ini");
$apply_id = get_apply_id($con, $fname);
if($apply_id == 0) {
  pg_query($con, "rollback");
  pg_close($con);
  summary_common_show_error_page_and_die();
}
//$apply_max_sel = select_from_table($con,"select max(apply_id) as max from sum_apply","",$fname);   // 採番方法変更に伴い不要！
//$apply_id = (int)pg_result($apply_max_sel,0,"max") + 1;                                            // 採番方法変更に伴い不要！

$apply_no = @pg_result($sum_apply,0,"apply_no");
if (!(int)$apply_no){
	$apply_no_max_sel = select_from_table($con, "select max(apply_no) as max_apply_no from sum_apply where apply_date like '".date("Ym")."%'", "", $fname);
	$apply_no = ((int)pg_fetch_result($apply_no_max_sel, 0, "max_apply_no")) + 1;
}
if (!$wkfw_id) {
	$apply_id = 0;
}

// 履歴は更新できない
if (@pg_result($sum_apply,0,"re_apply_id")){
	pg_close($con);
	echo("<script language=\"javascript\">alert(\"更新しようとした指示は、過去履歴です。\n他者により更新された可能性があります。再度設定してください。\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}





//========================================
// 現在のサマリ連番取得、sumamry_seqがなければ付与する
//========================================
$summary_seq = $c_sot_util->regist_summary_seq_if_not_exists($summary_id, $pt_id, $con, $fname);


//========================================
// 中止でなければ、メドレポート更新
//========================================
if ($_REQUEST["apply_stat"] != "2"){
	$tmpl_id_sql = ($tmpl_id == "" ? null : $tmpl_id);
	$cre_date = $sel_yr.$sel_mon.$sel_day;
	$summary_naiyo = str_replace("\r\n", "\n", $summary_naiyo);
	$summary_naiyo = str_replace("\n", "<br/>", $summary_naiyo);
	if($problem_id == "") $problem_id = "0";

	$sql = "update summary set";
	$set = array("ptif_id","diag_div", "problem", "smry_cmt", "emp_id", "cre_date", "tmpl_id", "priority", "problem_id","sect_id");
	$setvalue = array($pt_id, pg_escape_string($sel_diag), pg_escape_string($summary_problem), pg_escape_string($summary_naiyo), $seiki_emp_id, $cre_date, $tmpl_id_sql, $priority, $problem_id,$sect_id);
	$cond = "where summary_seq = ".(int)$summary_seq." and emp_id = '".$seiki_emp_id."'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}
}




//----------------------------------
// 既存レコードを論理削除
//----------------------------------
$upd = update_set_table($con, "update sum_apply set delete_flg = 't', re_apply_id = ".$apply_id." where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);
$upd = update_set_table($con, "update sum_applyapv set delete_flg = 't' where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);
$upd = update_set_table($con, "update sum_applyapvemp set delete_flg = 't' where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);
$upd = update_set_table($con, "update sum_applyfile set delete_flg = 't' where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);
$upd = update_set_table($con, "update sum_applynotice set delete_flg = 't' where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);
$upd = update_set_table($con, "update sum_applyasyncrecv set delete_flg = 't' where apply_id = ".(int)$old_apply_id, array(), null, "", $fname);


if ($wkfw_id){

	$visibility_apv_order = 1; // 稟議回覧なら、最初は１階層のみ
	// 同報なら、階層数を取得
	if ($wkfw_appr == "1"){
		for($i=1; $i<=(int)@$approve_num; $i++) {
			$tmp_apv_order_var = "apv_order" . $i;
			$visibility_apv_order = max($visibility_apv_order, $$tmp_apv_order_var);
		}
	}

	//=================
	// 職員情報取得
	//=================
	$rs = select_from_table($con, "select * from empmst where emp_id = '".$seiki_emp_id."'", "", $fname);
	$owner_emp_info = pg_fetch_all($rs);
	$emp_class     = $owner_emp_info[0]["emp_class"];
	$emp_attribute = $owner_emp_info[0]["emp_attribute"];
	$emp_dept      = $owner_emp_info[0]["emp_dept"];
	$emp_room      = $owner_emp_info[0]["emp_room"];

	if (@$dairi_finish) $current_dairi_kakunin_ymdhms = date("YmdHi");
	if ($current_dairi_kakunin_ymdhms) $current_dairi_kakunin_ymdhms = "'".$current_dairi_kakunin_ymdhms."'";
	if (!$current_dairi_kakunin_ymdhms) $current_dairi_kakunin_ymdhms = "null";

	$wkfwmst = select_from_table($con, "select * from sum_wkfwmst where tmpl_id = ".(int)$tmpl_id, "", $fname);
	$sql =
		" insert into sum_apply (".
		" apply_id, wkfw_id, summary_id, apply_content, emp_id".
		",apply_stat, apply_date, delete_flg, apply_title, re_apply_id".
		",apv_fix_show_flg, apv_bak_show_flg, emp_class, emp_attribute, emp_dept".
		",apv_ng_show_flg, emp_room, draft_flg, wkfw_appr, wkfw_content_type".
		",apply_title_disp_flg, apply_no, notice_sel_flg, wkfw_history_no, wkfwfile_history_no, ptif_id".
		",dairi_emp_id, dairi_kakunin_ymdhms, update_emp_id, visibility_apv_order".
		") values (".
		" ".$apply_id.                     // apply_id
		",".$wkfw_id.                      // wkfw_id
		",".$summary_id.                   // summary_id
		", ".qurt_sql(@$content).          // apply_content
		",'".$seiki_emp_id."'".            // emp_id
		",'".($apply_stat == "" ? "0" : $apply_stat)."'". // apply_stat
		",'".date("YmdHi")."'".            // apply_date
		",'f'".                            // delete_flg
		",null".                           // apply_title
		",null".                           // re_apply_id
		",'t'".                            // apv_fix_show_flg
		",'t'".                            // apv_bak_show_flg
		",".qurt_sql($emp_class).          // emp_class
		",".qurt_sql($emp_attribute).      // emp_attribute
		",".qurt_sql($emp_dept).           // emp_dept
		",'t'".                            // apv_ng_show_flg
		",".qurt_sql($emp_room).           // emp_room
		",'f'".                            // draft_flg
		",".qurt_sql(pg_fetch_result($wkfwmst, 0, "wkfw_appr")). // wkfw_appr
		",".qurt_sql(@$wkfw_content_type). // wkfw_content_type
		",null".                           // apply_title_disp_flg
		",'".$apply_no."'".                // apply_no
		",null".                           // notice_sel_flg
		",null".                           // wkfw_history_no
		",null".                           // wkfwfile_history_no
		",".qurt_sql($pt_id).              // ptif_id
		",".qurt_sql($dairi_emp_id).       // dairi_emp_id
		",".$current_dairi_kakunin_ymdhms. // dairi_kakunin_ymdhms
		",".qurt_sql($login_emp_id).       // update_emp_id
		",".$visibility_apv_order.         // visibility_apv_order
		")";
	$ret = update_set_table($con, $sql, array(), null, "", $fname);
	if($ret == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		summary_common_show_error_page_and_die();
	}

	// 受信者情報
	if ($_REQUEST["apply_stat"] != "2"){
		for($i=1; $i<=(int)@$approve_num; $i++) {
			$tmp_regist_emp_id_var = "regist_emp_id" . $i;
			$tmp_apv_order_var = "apv_order" . $i;
			$tmp_org_apv_order_var = "org_apv_order" . $i;
			$tmp_st_div_var = "st_div" . $i;
			$tmp_apv_sub_order_var = "apv_sub_order" . $i;
			$tmp_multi_apv_flg_var = "multi_apv_flg" . $i;
			$tmp_next_notice_div_var = "next_notice_div" . $i;
			$tmp_next_notice_recv_div_var = "next_notice_recv_div" . $i;
			$tmp_parent_pjt_id_var = "parent_pjt_id" . $i;
			$tmp_child_pjt_id_var = "child_pjt_id" . $i;
			$tmp_apv_common_group_id_var = "apv_common_group_id" . $i;

			// 所属、役職も登録する
			$receiver_emp_info = get_empmst($con, $$tmp_regist_emp_id_var, $fname);
			$sql =
				" insert into sum_applyapv (".
				" wkfw_id, apply_id, apv_order, emp_id, apv_stat_accepted".
				",apv_stat_operated, apv_date_accepted, apv_date_operated, delete_flg, apv_comment".
				",st_div, deci_flg, emp_class, emp_attribute, emp_dept".
				",emp_st, apv_fix_show_flg, emp_room, apv_sub_order, multi_apv_flg".
				",next_notice_div, next_notice_recv_div, parent_pjt_id, child_pjt_id, other_apv_flg, apv_common_group_id".
				",apv_stat_accepted_by_other, apv_stat_operated_by_other, wkfw_apv_order".
				" ) values (".
				" ".$wkfw_id.                                       // wkfw_id
				",".$apply_id.                                      // apply_id
				",".qurt_sql($$tmp_apv_order_var).                  // apv_order
				",".qurt_sql($$tmp_regist_emp_id_var).              // emp_id
				",0".                                               // apv_stat_accepted
				",0".                                               // apv_stat_operated
				",null".                                            // apv_date_accepted
				",null".                                            // apv_date_operated
				",'f'".                                             // delete_flg
				",null".                                            // apv_comment
				",".qurt_sql($$tmp_st_div_var).                     // st_div
				",'t'".                                             // deci_flg
				",".qurt_sql($receiver_emp_info[2]).                // emp_class
				",".qurt_sql($receiver_emp_info[3]).                // emp_attribute
				",".qurt_sql($receiver_emp_info[4]).                // emp_dept
				",".qurt_sql($receiver_emp_info[6]).                // emp_st
				",'t'".                                             // apv_fix_show_flg
				",".qurt_sql($receiver_emp_info[33]).               // emp_room
				",".qurt_sql($$tmp_apv_sub_order_var).              // apv_sub_order
				",".qurt_sql($$tmp_multi_apv_flg_var, "'f'").       // multi_apv_flg
				",".qurt_sql($$tmp_next_notice_div_var).            // next_notice_div
				",".qurt_sql($$tmp_next_notice_recv_div_var != "2" ? "1" :"2"). // next_notice_recv_div
				",".qurt_sql($$tmp_parent_pjt_id_var).              // parent_pjt_id
				",".qurt_sql($$tmp_child_pjt_id_var).               // child_pjt_id
				",'f'".                                             // other_apv_flg
				",".qurt_sql($$tmp_apv_common_group_id_var).        // apv_common_group_id
				",0".                                               // apv_stat_accepted_by_other
				",0".                                               // apv_stat_operated_by_other
				",".$$tmp_org_apv_order_var.                        // wkfw_apv_order
				" )";
			$ret = update_set_table($con, $sql, array(), null, "", $fname);
			if($ret == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				summary_common_show_error_page_and_die();
			}
		}
	} else {
		$sql =
			" insert into sum_applyapv (".
			" wkfw_id, apply_id, apv_order, emp_id, apv_stat_accepted".
			",apv_stat_operated, apv_date_accepted, apv_date_operated, delete_flg, apv_comment".
			",st_div, deci_flg, emp_class, emp_attribute, emp_dept".
			",emp_st, apv_fix_show_flg, emp_room, apv_sub_order, multi_apv_flg".
			",next_notice_div, next_notice_recv_div, parent_pjt_id, child_pjt_id, other_apv_flg, apv_common_group_id".
			",apv_stat_accepted_by_other, apv_stat_operated_by_other, wkfw_apv_order".
			" ) select".
			" wkfw_id, ".$apply_id.", apv_order, emp_id, apv_stat_accepted".
			",apv_stat_operated, apv_date_accepted, apv_date_operated, delete_flg, apv_comment".
			",st_div, deci_flg, emp_class, emp_attribute, emp_dept".
			",emp_st, apv_fix_show_flg, emp_room, apv_sub_order, multi_apv_flg".
			",next_notice_div, next_notice_recv_div, parent_pjt_id, child_pjt_id, other_apv_flg, apv_common_group_id".
			",apv_stat_accepted_by_other, apv_stat_operated_by_other, wkfw_apv_order".
			" from sum_applyapv".
			" where apply_id = ".$old_apply_id;
		$ret = update_set_table($con, $sql, array(), null, "", $fname);

		$sql =
			" insert into sum_applynotice (".
			" apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div".
			" ) select".
			" ".$apply_id.", recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div".
			" from sum_applynotice".
			" where apply_id = " . $old_apply_id;
		$ret = update_set_table($con, $sql, array(), null, "", $fname);

		$sql =
			" insert into sum_applyasyncrecv (".
			" apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg".
			" ) select".
			" ".$apply_id.", send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, 'f'".
			" from sum_applyasyncrecv".
			" where apply_id = " . $old_apply_id;
		$ret = update_set_table($con, $sql, array(), null, "", $fname);

		$sql =
			" insert into sum_apply_hinin_sasimodosi (".
			" apply_id, hinin_or_sasimodosi, from_emp_id, from_apply_stat_accepted, from_apply_stat_operated".
			",summary_seq, summary_id, ptif_id, regist_ymdhm, update_ymdhm".
			" ) select".
			" ".$apply_id.", hinin_or_sasimodosi, from_emp_id, from_apply_stat_accepted, from_apply_stat_operated".
			",summary_seq, summary_id, ptif_id, regist_ymdhm, update_ymdhm".
			" from sum_apply_hinin_sasimodosi".
			" where apply_id = " . $old_apply_id;
		$ret = update_set_table($con, $sql, array(), null, "", $fname);

		$sql =
			" insert into sum_apply_sasimodosi_target (".
			" apply_id, from_emp_id, from_apv_order, from_apply_stat_accepted, from_apply_stat_operated".
			",to_emp_id, to_apv_order, to_apply_stat_accepted, to_apply_stat_operated".
			",summary_seq, summary_id, ptif_id, regist_ymdhm, update_ymdhm".
			" ) select".
			" ".$apply_id.", from_emp_id, from_apv_order, from_apply_stat_accepted, from_apply_stat_operated".
			",to_emp_id, to_apv_order, to_apply_stat_accepted, to_apply_stat_operated".
			",summary_seq, summary_id, ptif_id, regist_ymdhm, update_ymdhm".
			" from sum_apply_sasimodosi_target".
			" where apply_id = " . $old_apply_id;
		$ret = update_set_table($con, $sql, array(), null, "", $fname);

	}

	// 添付ファイル登録
	$no = 1;
	if (is_array($filename)){
		foreach ($filename as $tmp_filename) {
			$sql =
				" insert into sum_applyfile (".
				" apply_id, applyfile_no, applyfile_name, delete_flg".
				" ) values (".
				" ".$apply_id.
				",".$no.
				",'".pg_escape_string($tmp_filename)."'".
				",'f'".
				" )";
			$ret = update_set_table($con, $sql, array(), null, "", $fname);
			$no++;
		}
	}
}



//========================================
// 基礎データの血液型を更新
//========================================
if($tmpl_id != ""){
	require_once("summary_tmpl_util.ini");
	update_bloodtype($con,$fname,$xml_file,$pt_id);
}


//========================================
//XMLファイル名を患者固有のファイル名に変更
//========================================
$new_xml_filename = $summary_seq.".xml";// ワークフローなしの場合
if (@$apply_id!="") $new_xml_filename = $summary_seq."_".$apply_id.".xml"; // ワークフローありの場合


//=================
// ゴミデータ削除処理
// apply_idの無い(ワークフローでない)データは履歴を残さない。（履歴を認識できない。）よって物理削除。
// apply_idのある(ワークフローである)アクティブなXML情報があれば論理削除
//=================
$sql =
	" delete from sum_xml".
	" where xml_file_name = '".pg_escape_string($new_xml_filename)."'".
	" and (apply_id is null or apply_id = 0)";
$upd = update_set_table($con, $sql, array(), null, "", $fname);

if (@$apply_id!=""){
	$sql =
		" update sum_xml set".
		" delete_flg = 1".
		" where xml_file_name = '".pg_escape_string($new_xml_filename)."'";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
}



//========================================
// スケジュール関連
//========================================
if($tmpl_id != ""){
	// 既存の本登録スケジュールがあれば論理削除
	$sql =
		" update sot_summary_schedule set delete_flg = true".
		" where summary_seq = ".(int)$summary_seq.
		" and apply_id = " . $old_apply_id;
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

	// テンプレートでコピーボタンを押さなかったら、レコードを複製して正規データを作成。
	// テンプレートでコピーボタンを押したら、新規レコードが作成されているので、
	// そのレコードを正規データに変更する。
	if (pg_num_rows($sel_sumxml) < 1){
		$sql =
			" insert into sum_xml (".
			" xml_file_name, summary_id, smry_xml, smry_html, is_work_data, emp_id".
			",update_timestamp, summary_seq, ptif_id, apply_id, delete_flg)".
			" select".
			"'".pg_escape_string($new_xml_filename)."'".
			",".$summary_id.
			",smry_xml, smry_html".
			",0".
			",emp_id".
			",current_timestamp".
			",".(int)$summary_seq.
			",'$pt_id'".
			",".($apply_id != "" ? (int)$apply_id : "null").
			",0".
			" from sum_xml".
			" where xml_file_name = '".pg_escape_string($xml_file)."'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		$sql = "select nextval('sot_summary_schedule_update_seq_seq')";
		$sel_update_seq = $c_sot_util->select_from_table($sql);
		$update_seq = (int)pg_fetch_result($sel_update_seq, 0, 0);

		$sql =
			" select schedule_seq from sot_summary_schedule".
			" where summary_seq = ".(int)$summary_seq.
			" and apply_id = " . $old_apply_id;
		$sel_schedule_seq_list = $c_sot_util->select_from_table($sql);
		$old_schedule_seq_list = pg_fetch_all($sel_schedule_seq_list);


		foreach((array)@$old_schedule_seq_list as $key => $row){
			if (!$row) break;
			$sel_schedule_seq = $c_sot_util->select_from_table("select nextval('sot_summary_schedule_seq')");
			$new_schedule_seq = pg_fetch_result($sel_schedule_seq, 0, 0);

			$sql =
				" insert into sot_summary_schedule (".
				" schedule_seq, update_seq, wkfw_id".
				",apply_id, tmpl_chapter_code, summary_seq, summary_id".
				",tmpl_id, tmpl_file_name, xml_file_name, emp_id, ptif_id, assign_sun_flg".
				",assign_mon_flg, assign_tue_flg, assign_wed_flg, assign_thu_flg, assign_fri_flg".
				",assign_sat_flg, times_per_ymd, start_ymd, end_ymd, temporary_update_flg".
				",regist_ymd, regist_timestamp, is_hide_worksheet_date, is_hide_worksheet_apply".
				",delete_flg, assign_pattern".
				" ) select".
				" ".$new_schedule_seq. ", ".$update_seq.", ".($wkfw_id != "" ? (int)$wkfw_id : "null").
				",".($apply_id != "" ? (int)$apply_id : "null").", tmpl_chapter_code, ".(int)$summary_seq.", ".$summary_id.
				",tmpl_id, tmpl_file_name, '".pg_escape_string($new_xml_filename)."', emp_id, ptif_id, assign_sun_flg".
				",assign_mon_flg, assign_tue_flg, assign_wed_flg, assign_thu_flg, assign_fri_flg".
				",assign_sat_flg, times_per_ymd, start_ymd, end_ymd, 0".
				",regist_ymd, regist_timestamp, is_hide_worksheet_date, is_hide_worksheet_apply".
				",false, assign_pattern".
				" from sot_summary_schedule".
				" where schedule_seq = ".$row["schedule_seq"];
			$upd = update_set_table($con, $sql, array(), null, "", $fname);

			$sql =
				" insert into sot_summary_schedule_various (".
				" schedule_seq".
				",string1,string2,string3,string4,string5,string6,string7,string8,string9,string10".
				",string11,string12,string13,string14,string15,string16,string17,string18,string19,string20".
				",int1,int2,int3,int4,int5,int6,int7,int8,int9,int10".
				",memo".
				") select".
				" ".$new_schedule_seq.
				",string1,string2,string3,string4,string5,string6,string7,string8,string9,string10".
				",string11,string12,string13,string14,string15,string16,string17,string18,string19,string20".
				",int1,int2,int3,int4,int5,int6,int7,int8,int9,int10".
				",memo".
				" from sot_summary_schedule_various".
				" where schedule_seq = " . $row["schedule_seq"];
			$upd = update_set_table($con, $sql, array(), null, "", $fname);
		}
	} else {
		$sql =
			" update sum_xml set".
			" xml_file_name = '".pg_escape_string($new_xml_filename)."'".
			",is_work_data = '0'".
			",summary_id = ". $summary_id.
			",summary_seq = ". (int)$summary_seq.
			",apply_id = " . ($apply_id != "" ? (int)$apply_id : "null").
			",update_timestamp = current_timestamp".
			" where xml_file_name = 'wk_".$login_emp_id.".xml'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		//==============================================
		// スケジュール仮登録から本登録に変更 (スケジュールがあるテンプレートのみ)
		//==============================================
		$sql =
			" update sot_summary_schedule set".
			" temporary_update_flg = 0".
			",xml_file_name = '".$new_xml_filename."'".
			",apply_id = ". ($apply_id != "" ? (int)$apply_id : "null").
			",wkfw_id = ". ($wkfw_id != "" ? (int)$wkfw_id : "null").
			" where summary_seq = ".(int)$summary_seq.
			" and delete_flg = false".
			" and temporary_update_flg = 1";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);
	}

	$sql =
		" update sot_worksheet_apply set".
		" summary_seq = ".(int)$summary_seq.
		",apply_id = ". ($apply_id != "" ? (int)$apply_id : "null").
		" where summary_seq = ".(int)$summary_seq;
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

}



//========================================
// 添付ファイルを正規の格納場所へ移動
//========================================
if ($wkfw_id){
	foreach (glob("summary/attach/{$apply_id}_*.*") as $tmpfile) unlink($tmpfile);
	for ($i = 0; $i < count($filename); $i++) {
		$ext = strrchr($filename[$i], ".");
		copy("summary/tmp/{$session}_".$file_id[$i].$ext, "summary/attach/{$apply_id}_".($i+1).$ext);
	}
	foreach (glob("summary/tmp/{$session}_*.*") as $tmpfile) unlink($tmpfile);
}


//========================================
// ＤＢコミット
//========================================
pg_query($con, "commit");

//========================================
// ＤＢ登録（ログ）
// スループット向上を期待してトランザクションには入れない
//========================================
summary_common_write_operate_log("modify", $con, $fname, $session, $login_emp_id, "", $pt_id);

pg_close($con);

//========================================
//処理成功時のJavaScript
//========================================
echo("<script language=\"javascript\">parent.hidari.location.reload();</script>\n");
echo("<script language=\"javascript\">location.href=\"./summary_new.php?session=".$session."&p_sect_id=".$sect_id."&pt_id=".$pt_id."&diag=".urlencode($sel_diag)."\";</script>\n");
exit;

function qurt_sql($v, $def = "null"){
	if (trim($v) == "") return $def;
	return "'" . pg_escape_string($v) . "'";
}
?>






