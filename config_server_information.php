<?
require("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require("show_copyright.ini");


$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);



//ログファイルのディレクトリ指定
$dir_name = '/tmp';

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


//dfの実行
$command='df -h';
$output_df=array();
$ret=null;
exec ( $command, $output_df, $ret );


//PHPバージョン情報の実行
$command='php -v';
$output_php_version=array();
$ret=null;
exec ( $command, $output_php_version, $ret );


//Postgresバージョン情報の実行
$command='psql --version';
$output_pstg_verion=array();
$ret=null;
exec ( $command, $output_pstg_verion, $ret );


//httpd情報の実行
$command='httpd -V';
$output_httpd=array();
$ret=null;
exec ( $command, $output_httpd, $ret );

//ディレクトリ・ハンドルをオープン
$res_dir = opendir($dir_name);

$exist_file_flg = 0;
//ディレクトリ内のファイル名を１つずつを取得
while( $file_name = readdir( $res_dir ) )
{
	if($file_name == "server_information_comedix.txt") 
	{
		$exist_file_flg = 1;
		break;
	}
}	
closedir( $res_dir );

$today = date("Ymd");

if($exist_file_flg > 0)
{
	//ファイルが存在しているので中をチェックする
	
	//PHPパッケージ情報の実行
	$command="grep 'comedix_date' /tmp/server_information_comedix.txt";
	$output_info_date=array();
	$ret=null;
	exec ( $command, $output_info_date, $ret );
	$str_date = substr($output_info_date[0],-8,8);

	if($str_date == $today)
	{
		//本日実行したので1日2回は実行しない
		;
	}
	else
	{
		//本日はじめてなので実行する
		
		//PHPパッケージ情報の実行
		$command="echo comedix_date=".$today." >/tmp/server_information_comedix.txt";
		$output_php_pkg=array();
		$ret=null;
		exec ( $command, $output_php_pkg, $ret );
		
		//PHPパッケージ情報の実行
		$command='rpm -qa >>/tmp/server_information_comedix.txt';
		$output_php_pkg=array();
		$ret=null;
		exec ( $command, $output_php_pkg, $ret );
		
	}
}
else
{
	//ファイルが無いので実行する
	
	//PHPパッケージ情報の実行
	$command="echo comedix_date=".$today." >/tmp/server_information_comedix.txt";
	$output_php_pkg=array();
	$ret=null;
	exec ( $command, $output_php_pkg, $ret );
	
	//PHPパッケージ情報の実行
	$command='rpm -qa >>/tmp/server_information_comedix.txt';
	$output_php_pkg=array();
	$ret=null;
	exec ( $command, $output_php_pkg, $ret );
	
}

//PHPパッケージ情報の実行
$command="grep 'php' /tmp/server_information_comedix.txt";
$output_php_pkg=array();
$ret=null;
exec ( $command, $output_php_pkg, $ret );

//Postgresパッケージ情報の実行
$command="grep 'postgresql' /tmp/server_information_comedix.txt";
$output_pstg_pkg=array();
$ret=null;
exec ( $command, $output_pstg_pkg, $ret );


//cyrus情報の実行
$command="grep 'cyrus' /tmp/server_information_comedix.txt";
$output_cyrus=array();
$ret=null;
exec ( $command, $output_cyrus, $ret );

//db4-utils情報の実行
$command="grep 'db4-utils' /tmp/server_information_comedix.txt";
$output_db4=array();
$ret=null;
exec ( $command, $output_db4, $ret );

//libc-client情報の実行
$command="grep 'libc-client' /tmp/server_information_comedix.txt";
$output_libc=array();
$ret=null;
exec ( $command, $output_libc, $ret );


//lm_sensors情報の実行
$command="grep 'lm_sensors' /tmp/server_information_comedix.txt";
$output_lm=array();
$ret=null;
exec ( $command, $output_lm, $ret );


//net-snmp情報の実行
$command="grep 'net-snmp' /tmp/server_information_comedix.txt";
$output_snmp=array();
$ret=null;
exec ( $command, $output_snmp, $ret );


//postfix情報の実行
$command="grep 'postfix' /tmp/server_information_comedix.txt";
$output_postfix=array();
$ret=null;
exec ( $command, $output_postfix, $ret );

$RH_version = @file('/etc/redhat-release');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 環境設定｜その他</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>

<style type="text/css">
pre {
margin-bottom:0px;
}
</style>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="config_register.php?session=<? echo($session); ?>">
							<img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定">
						</a>
					</td>
					<td width="100%">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<? echo($session); ?>"><b>環境設定</b></a>
						</font>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr height="22">
					<td width="110" align="center" bgcolor="#bdd1e7"><a href="config_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面</font></a></td>
					<td width="5">&nbsp;</td>
					<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_sidemenu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></a></td>
					<td width="5">&nbsp;</td>
					<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
					<td width="5">&nbsp;</td>
					<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログ</font></a></td>
					<td width="5">&nbsp;</td>
					<td width="110" align="center" bgcolor="#5279a5"><a href="config_server_information.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"  color="#ffffff"><b>サーバ情報</b></font></a></td>
					<td width="5">&nbsp;</td>
					<td width="120" align="center" bgcolor="#bdd1e7"><a href="config_client_auth.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証履歴</font></a></td>
					<td width="5">&nbsp;</td>
					<td width="90" align="center" bgcolor="#bdd1e7"><a href="config_other.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<form name="mainform" action="config_server_information.php" method="post">
				<img src="img/spacer.gif" width="1" height="20" alt=""><br>

				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CoMedixバージョン</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
									<? 
										$sql = "select created_at from current_package";
										$cond = "";
										$sel = select_from_table($con, $sql, $cond, $fname);
										if ($sel == 0) {
											pg_close($con);
											echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
											echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
											exit;
										}
										$package_date = pg_fetch_result($sel, 0, "created_at");
										echo("CoMedix V" . COMEDIX_VERSION . "-" . $package_date);
									?></label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">データサイズ</font></td>
						<td align="left">
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><label><?$lp_count = 0;?><pre><?foreach($output_df as $line){$lp_count++; echo($line); if ($lp_count < count($output_df)){?><br><?}?><?}?></pre></label></font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">OSバージョン</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
								<?foreach($RH_version as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WEBサーバ</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
								<?foreach($output_httpd as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">PHPバージョン</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
									<?foreach($output_php_version as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">PHPパッケージ</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
									<?foreach($output_php_pkg as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Postgresバージョン</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
									<?foreach($output_pstg_verion as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Postgresパッケージ</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
								<?foreach($output_pstg_pkg as $line){?><?=$line?><br><?}?>								
								</label>
							</font>
						</td>
					</tr>

					<tr height="30">
						<td width="15%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他パッケージ</font></td>
						<td>
							<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
								<label>
									<?foreach($output_cyrus as $line){?><?=$line?><br><?}?>
									<?foreach($output_db4 as $line){?><?=$line?><br><?}?>
									<?foreach($output_libc as $line){?><?=$line?><br><?}?>
									<?foreach($output_lm as $line){?><?=$line?><br><?}?>
									<?foreach($output_snmp as $line){?><?=$line?><br><?}?>
									<?foreach($output_postfix as $line){?><?=$line?><br><?}?>
								</label>
							</font>
						</td>
					</tr>
				</table>

				<input type="hidden" name="session" value="<? echo($session); ?>">
			</form>
		</td>
	</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
