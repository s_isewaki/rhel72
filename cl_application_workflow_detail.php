<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_detail.ini");
require_once("show_select_values.ini");
require_once("cl_application_imprint_common.ini");
require_once("cl_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 申請詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<?

// 申請・ワークフロー情報取得
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];

// 本文形式タイプのデフォルトを「テキスト」とする
if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

// 形式をテキストからテンプレートに変更した場合の古いデータ対応
// wkfw_content_typeは2:テンプレートだが、登録データがXMLでない場合、1:テキストとして処理する
if ($wkfw_content_type == "2") {
	if (strpos($apply_content, "<?xml") === false) {
		$wkfw_content_type = "1";
	}
}

if($wkfw_history_no != "")
{
	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
}

//カレンダー外部ファイルの読み込み
write_yui_calendar_use_file_read_0_12_2();




?>
<script type="text/javascript">
function apply_printwin()
{
	//window.open('cl_application_workflow_detail_print.php?session=<?=$session?>&fname=<?=$fname?>&apply_id=<?=$apply_id?>&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=640,height=700,scrollbars=yes');

	window.open('', 'apply_print', 'width=640,height=700,scrollbars=yes');

	document.apply.action="cl_application_workflow_detail_print.php";
	document.apply.target="apply_print";
	document.apply.mode.value = "apply_print";
	//session,apply_idはhidden初期設定値を使用
	document.apply.submit();

	document.apply.target="_self";




//	document.apply.mode.value = "apply_printwin";
//	document.apply.action="cl_application_workflow_detail.php?session=<?=$session?>";
//	document.apply.submit();
}

function apply_simpleprintwin() {
	document.apply.simple.value = "t";
	apply_printwin();
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];

		//不活性では無い場合はリサイズの対象外
	    if(! t.disabled)
	    {
		    continue;
		}

		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}

p {margin:0;}
</style>

<script type="text/javascript">

//onload()時の処理
function do_onload()
{
	//テンプレート表示コントロール
	Reference_Display('<?=$apply_id?>');
}

//テンプレートの表示が完了した後の処理
function templateDispAfter()
{
	//テキストエリアのリサイズ処理
	resizeAllTextArea();
}

</script>


</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="do_onload()">
<!-- 2012/01/06 Yamagawa upd(s) idを追加 -->
<form id="apply" name="apply" action="#" method="post">
<input type="hidden" id="session" name="session" value="<? echo($session); ?>">
<input type="hidden" id="apply_id" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" id="mode" name="mode" value="">
<input type="hidden" id="simple" name="simple" value="">
<!-- 2012/01/06 Yamagawa upd(e) -->

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>申請詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?
$mode = "";
show_application_workflow_detail($con, $session, $fname, $apply_id, $mode);
?>
</center>
</form>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト START -->
<form id="common_module_call_form" name="common_module_call_form" method="post" action="cl_common_call.php" target="common_module_window"></form>
<script type="text/javascript">
//子画面(共通モジュール)を開きます。
//
//moduleName 子画面のモジュール名を指定します。
//params     子画面へ送信する専用パラメータを設定します。Objectを指定してください。Objectの属性名が送信パラメータ名となり、値がパラメータとなります。
//option     子画面を開くときのオプションを指定します。(JavaScriptのopen関数のoptionと同じです。)
function commonOpen(moduleName,params,option)
{
	//子画面(共通モジュール)送信用フォームタグ
	var f = document.getElementById("common_module_call_form");

	//共通パラメータ用のhiddenを作成、値を設定。
	var html = "";
	html = html + '<input type="hidden" name="common_module" value="' + moduleName + '">';
	html = html + '<input type="hidden" name="session"       value="<?=$session ?>">';

	//専用パラメータ用のhiddenをparamsから作成。
	for(var key in params)
	{
		html = html + '<input type="hidden" name="' + key + '" value="">';
	}
	f.innerHTML = html;

	//専用パラメータ用のhiddenにparamsから値を設定。
	for(var key in params)
	{
		var hidden = eval("f." + key);
		hidden.value = eval("params." + key);
	}

	//子画面を開く
	window.open('', 'common_module_window', option);

	//子画面へ送信
	f.submit();
}
</script>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト END -->
</body>
</html>

<?
// 申請結果通知画面から呼ばれた場合
if($screen == "NOTICE")
{
// 確認済みフラグ更新
$obj->update_confirmed_flg($apply_id, $session);
?>
<script language='javascript'>
if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}
</script>
<?
}

pg_close($con);
?>



