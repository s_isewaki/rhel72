<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜入院登録−患者一覧</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("show_in_register_list.ini");
require_once("inpatient_list_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者登録権限を取得
$pt_reg_auth = check_authority($session, 22, $fname);

// データベースに接続
$con = connect2db($fname);

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 患者検索URLを取得
$sql = "select linkage_url from ptadm";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$linkage_url = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "linkage_url") : "";

// デフォルトの表示モードは「入院予定あり」
if ($view_mode == "") {$view_mode = "1";}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>

function registerIn(ptif_id) {
	location.href = 'inpatient_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&bedundec1=t&bedundec2=t&bedundec3=t&path=3';
}

function reserveIn(ptif_id) {
	location.href = 'inpatient_reserve_register.php?session=<? echo($session); ?>&pt_id=' + ptif_id + '&bedundec1=t&bedundec2=t&bedundec3=t&path=1';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.submenu td td {border-width:0;}
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院" disabled><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table border="0" cellspacing="0" cellpadding="1">
<tr>
<td style="padding-right:25px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($view_mode != "1") {echo("<a href=\"inpatient_register_list.php?session=$session&view_mode=1\">");} ?>入院予定あり<? if ($view_mode != "1") {echo("</a>");} ?></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($view_mode != "2") {echo("<a href=\"inpatient_register_list.php?session=$session&view_mode=2\">");} ?>入院予定なし<? if ($view_mode != "2") {echo("</a>");} ?></font></td>
<? if ($pt_reg_auth == "1") { ?>
<td><input type="button" value="新規登録" onclick="window.open('inpatient_patient_register.php?session=<? echo($session); ?>&path=I', 'ptreg', 'width=640,height=480,scrollbars=yes');"></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td height="2"></td></tr>
<tr bgcolor="#5279a5"><td height="1"></td></tr>
<tr><td height="2"></td></tr>
</table>
<?
// 入院予定ありの場合
if ($view_mode == "1") {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr valign="bottom">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟：<select name="bldgwd" onchange="location.href = 'inpatient_register_list.php?session=<? echo($session); ?>&bldgwd=' + this.value;">
<option value="">すべて
<?
	while ($row = pg_fetch_array($sel_ward)) {
		$tmp_bldgwd = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
		$tmp_ward_name = $row["ward_name"];
		echo("<option value=\"$tmp_bldgwd\"");
		if ($tmp_bldgwd == $bldgwd) {echo(" selected");}
		echo(">$tmp_ward_name\n");
	}
?>
</select></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr><td height="2"></td></tr>
<tr bgcolor="#5279a5"><td height="1"></td></tr>
<tr><td height="2"></td></tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?
	show_next($con, $session, $bldgwd, $page, $fname);
	show_patient_list($con, $session, $bldgwd, $page, $fname);

// 入院予定なしの場合
} else if ($view_mode == "2") {

	// 患者検索URLが未登録の場合
	if ($linkage_url == "") {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form action="inpatient_register_list.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
患者ID<input type="text" name="key1" value="<? echo($key1); ?>">
患者氏名<input type="text" name="key2" value="<? echo($key2); ?>">
<input type="submit" value="検索">
</font></td>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="view_mode" value="2">
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?
		show_next2($con, $session, $key1, $key2, $page, $fname);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<?
		show_patient_list2($con, $session, $key1, $key2, $page, $fname);
?>
</table>
<?
	// 患者検索URLが登録されている場合
	} else {

		// 患者選択時のコールバック処理URLを設定
		$callbacker_url_prefix = ($_SERVER["SERVER_PORT"] == 443) ? "https://" : "http://";
		$collbacker_url_dir = str_replace("inpatient_register_list.php", "", $_SERVER["SCRIPT_NAME"]);
		$callbacker_url = $callbacker_url_prefix . $_SERVER["HTTP_HOST"] . $collbacker_url_dir . "inpatient_sub_menu2_linkage_callbacker.php?session=$session";
?>
<iframe width="100%" height="500" src="<? echo($linkage_url); ?>?callbacker_url=<? echo(urlencode($callbacker_url)); ?>" frameborder="0"></iframe>
<?
	}
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
