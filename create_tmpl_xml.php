<?
require_once("about_comedix.php");
require_once("sot_util.php");
require_once("summary_common.ini");

$fname = $PHP_SELF;
$con = connect2db($fname);

if (!$_REQUEST["xml_file"]) {
	$_REQUEST["xml_file"] = "wk_".$emp_id .".xml";
}

$ptif_id = @$_REQUEST["Patient_ID"];
if ($ptif_id == "") $ptif_id = @$_REQUEST["ptif_id"];
if ($ptif_id == "") $ptif_id = $_REQUEST["pt_id"];
if ($ptif_id == "") {
	echo "患者が指定されていません。";
	die;
}

//============================
// 各テンプレート固有の処理を実行
// 送信パラメータを解析して、$textという変数名でXML形式データを設定する。
//============================
if (@$_REQUEST["xml_creator_use_http"]){
  $text = $c_sot_util->url_get_template_contents_with_post(); // ここではUTF-8で返される
} else {
	require(@$_REQUEST["xml_creator"]);
}

//============================
// 確認のため、いまいちどキャストしてみる
//============================
$xml = new template_xml_class();
$xmldom = $xml->get_xmldom_from_utf8_xml_text($text);
if (!$xmldom) {
	echo $text; // テンプレートで落ちた後、トップ画面に戻ると何が何だかわからなくなるため、とりあえず問題の構文を表示する。
	die;
	//	summary_common_show_error_page_and_die();
}

//$save_xml = mb_convert_encoding($text, "EUC-JP", "UTF-8");
$save_xml = mb_convert_encoding($text, "eucJP-win", "UTF-8") ;

pg_query($con, "begin");

//============================
// 操作者のワークデータを消す
//============================
$xml_file = "wk_".$emp_id .".xml";
$sql = "delete from sum_xml where xml_file_name = '".$xml_file."'";
$del = delete_from_table($con, $sql, "", $fname);

//============================
// XMLを、xml格納テーブルに登録する。
//============================
$sql =
	"insert into sum_xml (emp_id, xml_file_name, smry_xml, is_work_data, ptif_id) values (".
	"'".$emp_id."','".$xml_file."','".pg_escape_string($save_xml)."', '1', '".$ptif_id."'";
$ins = insert_into_table($con,$sql, array(),$fname);

$sql = "select * from sum_xml where xml_file_name = '".$xml_file."' for update";
$sel = select_from_table($con, $sql, "", $fname);

//============================
// XMLから変換したHTMLを、xml格納テーブルに登録する。
//============================
require_once("summary_tmpl_util.ini");
//$save_html = get_tmpl_naiyo_html_default($con, $fname, $xml_file);
// 処理対象のXMLデータは、sum_xmlテーブルから取り出さないで、直接変換する
$save_html = create_tmpl_naiyo_html($con, $fname, $text) ;

$sql = "update sum_xml set smry_html = '".pg_escape_string($save_html)."' where xml_file_name = '" .$xml_file."'";
$upd = update_set_table($con, $sql, array(), null, "", $fname);

pg_query($con, "commit");

//============================
// 温度板のケア・バイタル入力画面から来た場合。親画面にて、新規オーダ発行まで完了させる。
//============================
if (@$_REQUEST["direct_registration"]){
	echo $c_sot_util->javascript("parent.callback_template_registed();");
	die;
}
//============================
// 送受信履歴画面から呼ばれた場合。送信履歴画面・受信履歴画面をサブミットさせる。
//============================
else if(@$command_mode == "update" || @$command_mode == "update_and_resend"){
	echo $c_sot_util->javascript("parent.document.apply.submit();");
	die;
}

//============================
// レポートタブで新規登録した場合
//============================
else if($update_flg == "new"){
	$diag = urlencode($diag);
	$problem = urlencode($summary_problem);
	$href =
		"summary_new.php?session=".$session."&pt_id=".$pt_id."&diag=".$diag."&xml_file=".$xml_file.
		"&tmpl_id=".$tmpl_id."&no_tmpl_window=1&summary_problem=".$problem.
		"&date=".$cre_date."&priority=".$priority."&problem_id=".$problem_id.
		"&p_sect_id=".$sect_id."&outreg=".$outreg."&dairi_emp_id=".@$dairi_emp_id."&seiki_emp_id=".@$seiki_emp_id."&mstt_dr_id=".@$mstt_dr_id;
  $save_html = h($save_html, ENT_QUOTES) ; // ダブルおよびシングルクオートを変換する
  $script = "window.opener.newUrl=\"" . $href . "\";window.opener.saveHtml=\"" . $save_html . "\";window.opener.showNewPage();" ;
//  $script = "window.opener.newUrl=\"xxx\";window.opener.testalert();" ;
  echo $c_sot_util->javascript($script);

//	echo $c_sot_util->javascript("window.opener.location.href = \"".$href."\"");
}

//============================
// レポートタブで更新登録した場合
//============================
else{
	$diag = urlencode($diag);
	$problem = urlencode($summary_problem);
	$href =
		"summary_update.php?session=".$session."&pt_id=".$pt_id."&summary_id=".$summary_id."&summary_div=".$diag.
		"&xml_file=".$xml_file."&tmpl_id=".$tmpl_id."&summary_problem=".$problem.
		"&date=".$cre_date."&priority=".$priority."&problem_id=".$problem_id.
		"&p_sect_id=".$sect_id."&outreg=".$outreg."&no_tmpl_window=1".
		"&dairi_emp_id=".@$dairi_emp_id."&seiki_emp_id=".@$seiki_emp_id;
	echo $c_sot_util->javascript("window.opener.location.href = \"".$href."\"");
}
echo $c_sot_util->javascript("close();");
exit;

?>

<?
// 指定されたテンプレートXMLデータよりデフォルトの変換方法で診療記録内容の表用HTMLを作成します。
// $xml_textはEUC-JPで渡されます。
function create_tmpl_naiyo_html($con, $fname, $xml_text)
{
  $XMLnaiyo = array();
  $dom = domxml_open_mem($xml_text) ;
  $node_array = $dom->get_elements_by_tagname('template');
  foreach ($node_array as $node) {
    $child_node_array = $node->child_nodes();
    $XMLnaiyo = get_child($child_node_array, 0, $XMLnaiyo);
  }

  $naiyo_string = "";
  $pt_id_flg = false;
  $pt_name_flg = false;
  for($i=1; $i<count($XMLnaiyo); $i++) {
    $naiyo_string1 = trim($XMLnaiyo[$i]);
    // 名前の文字化け対策
    if ($pt_id_flg == true) {
      $pt_id = str_replace("&nbsp;","", $naiyo_string1);
      $pt_id_flg = false;
    }
    if (strstr($naiyo_string1,"・ＩＤ")) $pt_id_flg = true;
    if ($pt_name_flg == true) {
      $pt_name = $naiyo_string1;
      if (strstr($pt_name, "?")) $naiyo_string1 = "&nbsp;&nbsp;&nbsp;&nbsp;".get_pt_name($con,$fname,$pt_id);
      $pt_name_flg = false;
    }
    if (strstr($naiyo_string1,"・氏名")) $pt_name_flg = true;
    $naiyo_string1 = str_replace("\r\n", "\n", $naiyo_string1);
    $naiyo_string1 = str_replace("\n", "<br/>", $naiyo_string1);
    $naiyo_string .= $naiyo_string1."<br/>";
  }
  return $naiyo_string;
}
?>