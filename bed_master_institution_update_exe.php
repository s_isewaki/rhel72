<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// 入力チェック
if ($mst_name == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('医療機関・施設名称を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mst_name) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('医療機関・施設名称は全角50文字以内で入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

$sql  = "select count(*) as cnt from institemmst";
$cond = "where mst_name = '$mst_name' and mst_cd != '$mst_cd'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (intval(pg_fetch_result($sel, 0, "cnt")) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('医療機関・施設名称「{$mst_name}」は既に使用されています。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

if (strlen($zip1) > 3) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('郵便番号1は半角3文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($zip2) > 4) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('郵便番号2は半角4文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address1) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('住所1は全角50文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($address2) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('住所2は全角50文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel1) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('電話番号1は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel2) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('電話番号2は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($tel3) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('電話番号3は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($fax1) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('FAX番号1は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($fax2) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('FAX番号2は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($fax3) > 6) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('FAX番号3は半角6文字以内で入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($mst_content) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('備考は全角50文字以内で入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 医療機関情報情報を更新
$sql = "update institemmst set";
$set = array("mst_name", "zip1", "zip2", "type", "province", "address1", "address2", "tel1", "tel2", "tel3", "fax1", "fax2", "fax3", "mst_content", "update_time", "update_emp_id");
$setvalue = array($mst_name, $zip1, $zip2, $type, $province, $address1, $address2, $tel1, $tel2, $tel3, $fax1, $fax2, $fax3, $mst_content, date("YmdHis"), $emp_id);
$cond = "where mst_cd = '$mst_cd'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'bed_master_institution.php?session=$session&mst_cd=$mst_cd';</script>");
?>
