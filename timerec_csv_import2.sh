#!/bin/sh
#=====================================================================
# Time Recorder CSV Auto Import
#=====================================================================

# Dakoku Data Directory (smbmount)
dakoku_dir='/var/kintai'

# CSV Hozon Directory
csv_save_dir='/var/tmp/comedix_timerec'

# CSV File ka-ku-cho-shi
ext='TXT'

# CoMedix Directory
comedix_dir='/var/www/html/comedix'
#---------------------------------------------------------------------

# PHP Program CALL
php -q -c /etc/php.ini -f $comedix_dir/timerec_csv_import.php $dakoku_dir $csv_save_dir $ext 2
#---------------------------------------------------------------------

# CSV File Removed (30 days ago)
find $csv_save_dir -mtime +30 -exec rm -rf {} \;

