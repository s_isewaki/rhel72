<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 文書管理権限チェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session,$auth_id,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// カテゴリ名未入力チェック
if($category_name == ""){
	echo("<script language=\"javascript\">alert(\"カテゴリ名が入力されていません。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// カテゴリ名文字数チェック
if(strlen($category_name) > 20){
	echo("<script language=\"javascript\">alert(\"カテゴリ名が長すぎます。\");</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// カテゴリ名の更新
$sql = "update libcate set";
$set = array("lib_cate_nm");
$setvalue = array($category_name);
$cond = "where lib_cate_id = '$cate_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 文書一覧画面に遷移
if ($path == "3") {
	echo("<script language='javascript'>location.href = 'library_admin_menu.php?session=$session&a=$arch_id&o=$o';</script>");
} else {
	echo("<script language='javascript'>location.href = 'library_list_all.php?session=$session&a=$arch_id&o=$o';</script>");
}
?>
