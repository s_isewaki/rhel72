<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 勤務条件一括設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("atdbk_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$paid_sel = select_from_table($con, $sql, $cond, $fname);
if ($paid_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//配列にデータ格納
$idx = 1;
$arr_paid_hol_info = array();
while($row = pg_fetch_array($paid_sel)){
    $arr_paid_hol_info[$idx] = $row;
    $wk_days = 0;
    for ($i=1; $i<=8; $i++) {
        $varname = "add_day".$i;
        $wk = ($row["$varname"] != "") ? $row["$varname"] : 0;
        $wk_days += $wk;
    }
    $arr_paid_hol_info[$idx]["disp_flg"] = ($wk_days != 0);
    $idx++;
}

// 変則労働適用のデフォルトは「無」
if ($irrg_app == "") {
	$irrg_app = "1";
}

// 期間のデフォルトは週
if ($irrg_type == "") {
	$irrg_type = "1";
}

// 雇用・勤務形態のデフォルトは常勤
if ($duty_form == "") {
	$duty_form = "1";
}

$atdbk_common_class = new atdbk_common_class($con, $fname);
$atdbk_common_class->setJavascript();
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function onChangeIrrgApp() {
	var disabled = (document.employee.irrg_app[0].checked);
	for (var i = 0, j = document.employee.irrg_type.length; i < j; i++) {
		document.employee.irrg_type[i].disabled = disabled;
	}
	document.employee.hol_minus.disabled = disabled;
}

function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}

function setAtrbOptions() {
	clearOptions(document.employee.tgt_atrb);

	var class_id = document.employee.tgt_class.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
	if (class_id == '<? echo $row["class_id"]; ?>') {
		addOption(document.employee.tgt_atrb, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>');
	}
<? } ?>

	setDeptOptions();
}

function setDeptOptions() {
	clearOptions(document.employee.tgt_dept);

	var atrb_id = document.employee.tgt_atrb.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
	if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
		addOption(document.employee.tgt_dept, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>');
	}
<? } ?>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
	setRoomOptions();
<? } ?>
}

<? if ($arr_class_name["class_cnt"] == 4) { ?>
function setRoomOptions() {
	clearOptions(document.employee.tgt_room);

	var dept_id = document.employee.tgt_dept.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
	if (dept_id == '<? echo $row["dept_id"]; ?>') {
		addOption(document.employee.tgt_room, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>');
	}
<? } ?>
}
<? } ?>

function clearOptions(box) {
	for (var i = box.length - 1; i > 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="onChangeIrrgApp();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>">権限</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_display_bulk_setting.php?session=<? echo($session); ?>">表示</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_menu_bulk_setting.php?session=<? echo($session); ?>">メニュー</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件を一括設定しました。</font></td>
<? } else { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※設定内容は登録済みの職員にのみ反映されます。職員の追加登録時には「一覧」→「権限」画面で個別設定するか、再度一括設定して下さい</font></td>
<? } ?>
</tr>
</table>
<form name="employee" action="employee_condition_bulk_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="22%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td width="34%">
<select name="tgt_job[]" size="6" multiple>
<? show_options($sel_job, "job_id", "job_nm", $tgt_job); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_job[]']);">
</td>
<td width="10%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td width="34%">
<select name="tgt_st[]" size="6" multiple>
<? show_options($sel_st, "st_id", "st_nm", $tgt_st); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_st[]']);">
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?></font></td>
<? } ?>
</tr>
<tr>
<td style="padding-right:6px;">
<select name="tgt_class" onchange="setAtrbOptions();">
<option value="">　　　　　　　　</option>
<?
while ($row = pg_fetch_array($sel_class)) {
	echo("<option value=\"{$row["class_id"]}\"");
	if ($row["class_id"] == $tgt_class) {echo(" selected");}
	echo(">{$row["class_nm"]}\n");
}
?>
</select>
</td>
<td style="padding-right:6px;">
<select name="tgt_atrb" onchange="setDeptOptions();">
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_atrb, 0);
while ($row = pg_fetch_array($sel_atrb)) {
	if ($row["class_id"] == $tgt_class) {
		echo("<option value=\"{$row["atrb_id"]}\"");
		if ($row["atrb_id"] == $tgt_atrb) {echo(" selected");}
		echo(">{$row["atrb_nm"]}\n");
	}
}
?>
</select>
</td>
<td style="padding-right:6px;">
<select name="tgt_dept"<? if ($arr_class_name["class_cnt"] == 4) { ?> onchange="setRoomOptions();"<? } ?>>
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_dept, 0);
while ($row = pg_fetch_array($sel_dept)) {
	if ($row["atrb_id"] == $tgt_atrb) {
		echo("<option value=\"{$row["dept_id"]}\"");
		if ($row["dept_id"] == $tgt_dept) {echo(" selected");}
		echo(">{$row["dept_nm"]}\n");
	}
}
?>
</select>
</td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td style="padding-right:6px;">
<select name="tgt_room">
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_room, 0);
while ($row = pg_fetch_array($sel_room)) {
	if ($row["dept_id"] == $tgt_dept) {
		echo("<option value=\"{$row["room_id"]}\"");
		if ($row["room_id"] == $tgt_room) {echo(" selected");}
		echo(">{$row["room_nm"]}\n");
	}
}
?>
</select>
</td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">給与支給区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="wage">
<option value="1"<? if ($wage == "1") {echo(" selected");} ?>>月給制
<option value="3"<? if ($wage == "3") {echo(" selected");} ?>>日給制
<option value="4"<? if ($wage == "4") {echo(" selected");} ?>>時間給制
<option value="5"<? if ($wage == "5") {echo(" selected");} ?>>年俸制
</select>
<label><input type="checkbox" name="wage_omit" value="t"<? if ($wage_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤パターンのグループ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
$selected_group_id = $atdbk_common_class->setGroupOption("tmcd_group_id", "", $tmcd_group_id, "atdptn_id");
?>
<label><input type="checkbox" name="tmcd_group_id_omit" value="t"<? if ($tmcd_group_id_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定未登録時の<br>標準の勤務パターン</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="atdptn_id" id="atdptn_id">
<option value="">
<?
$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($selected_group_id);
foreach ($arr_attendance_pattern as $list_atdptn_id => $list_atdptn_nm) {
	echo("<option value=\"$list_atdptn_id\"");
	if ($list_atdptn_id == $atdptn_id) {
		echo(" selected");
	}
	echo(">$list_atdptn_nm\n");
}
?>
</select>
<label><input type="checkbox" name="atdptn_id_omit" value="t"<? if ($atdptn_id_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変則労働適用</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="irrg_app" value="1"<? if ($irrg_app == "1") {echo(" checked");} ?> onclick="onChangeIrrgApp();">無</label>
<label><input type="radio" name="irrg_app" value="2"<? if ($irrg_app == "2") {echo(" checked");} ?> onclick="onChangeIrrgApp();">有</label>
<label><input type="checkbox" name="irrg_app_omit" value="t"<? if ($irrg_app_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="irrg_type" value="1"<? if ($irrg_type == "1") {echo(" checked");} ?>>週</label>
<label><input type="radio" name="irrg_type" value="2"<? if ($irrg_type == "2") {echo(" checked");} ?>>月</label>
<img src="img/spacer.gif" alt="" height="1" width="20">
<label><input type="checkbox" name="hol_minus" value="t"<? if ($hol_minus == "t") {echo(" checked");} ?>>所定労働時間から祝日分（8時間）を減算する</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="duty_form" value="1"<? if ($duty_form == "1") {echo(" checked");} ?>>常勤</label>
<label><input type="radio" name="duty_form" value="3"<? if ($duty_form == "3") {echo(" checked");} ?>>短時間正職員</label>
<label><input type="radio" name="duty_form" value="2"<? if ($duty_form == "2") {echo(" checked");} ?>>非常勤</label>
<label><input type="checkbox" name="duty_form_omit" value="t"<? if ($duty_form_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業管理</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="checkbox" name="no_overtime" value="t"<? if ($no_overtime == "t") {echo(" checked");} ?>>残業管理をしない</label>
<label><input type="checkbox" name="no_overtime_omit" value="t"<? if ($no_overtime_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有給付与日数表</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="paid_hol_tbl_id">
<?
//while($row = pg_fetch_array($paid_sel)){
for ($i=1; $i<=8; $i++) {
    if ($arr_paid_hol_info[$i]["disp_flg"] == false) {
        continue;
    }
    echo("<option value=\"{$arr_paid_hol_info[$i]["paid_hol_tbl_id"]}\"");
    if ($paid_hol_tbl_id == $arr_paid_hol_info[$i]["paid_hol_tbl_id"]) {
        echo(" selected");
    }
    //名称変更 常勤 週ｎ日
    $wk_tbl_id_name = $atdbk_common_class->get_paid_hol_tbl_id_name($arr_paid_hol_info[$i]["paid_hol_tbl_id"]);
    echo(">{$wk_tbl_id_name}");
    echo("</option>");
}

//選択肢に「なし」を追加
$selected = ($paid_hol_tbl_id == "") ? " selected" : "";
echo("<option value=\"\" $selected>なし</option>");
?>
</select>
<label><input type="checkbox" name="paid_hol_tbl_id_omit" value="t"<? if ($paid_hol_tbl_id_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月集計CSVレイアウト</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="csv_layout_id">
<?
for ($i=1; $i<=20; $i++) {
	$wk_id = sprintf("%02d", $i);
	echo("<option value=\"$wk_id\"");
	if ($csv_layout_id == $wk_id) {
		echo(" selected");
	}
	echo(">レイアウト{$wk_id}");
	echo("</option>");
}
?>
</select>
<label><input type="checkbox" name="csv_layout_id_omit" value="t"<? if ($csv_layout_id_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">締め日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="closing"><option value="--"><option value="1"<? if ($closing == "1") {echo(" selected");} ?>>1<option value="2"<? if ($closing == "2") {echo(" selected");} ?>>5<option value="3"<? if ($closing == "3") {echo(" selected");} ?>>10<option value="4"<? if ($closing == "4") {echo(" selected");} ?>>15<option value="5"<? if ($closing == "5") {echo(" selected");} ?>>20<option value="6"<? if ($closing == "6") {echo(" selected");} ?>>末</select>日 <label><input type="checkbox" name="closing_omit" value="t"<? if ($closing_omit == "t") {echo(" checked");} ?> style="margin-left:20px;">更新しない</label>
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
	while ($row = pg_fetch_array($sel)) {
		$id = $row[$id_col];
		$nm = $row[$nm_col];
		echo("<option value=\"$id\"");
		if (in_array($id, $tgt_ids)) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
}
?>
