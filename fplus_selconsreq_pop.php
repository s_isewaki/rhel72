<?
require_once("about_comedix.php");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$serch = @$_REQUEST["search_mode"];

// 指定された日付
if($arisedate == ""){
	$arisedate = @$_REQUEST["arise_date"];
}

// データベースに接続
$con = connect2db($fname);

//検索ボタン押下後
if($serch == "1"){
	$sql= " SELECT ".
		" 	req.report_no ".
		" 	, req.consultation_request_id ".
		" 	, req.title ".
		" 	, to_char(req.occurrence_date,'YYYY/MM/DD') as occurrence_date ".
		" 	, req.hospital_id ".
		" 	, hosp.hospital_name ".
		" 	, hosp.hospital_directer ".
		" 	, req.first_problem ".
		" 	, req.second_problem ".
		" 	, req.third_problem ".
		" 	, req.fourth_problem ".
		" 	, req.fifth_problem ".
		" 	, req.sixth_problem ".
		" 	, req.seventh_problem ".
		" 	, req.eighth_problem ".
		" 	, req.nineth_problem ".
		" 	, req.tenth_problem ".
		" 	, req.problem_count ".
		" FROM ".
		" 	fplus_consultation_request req ".
		" 	, fplusapply app ".
		" 	, fplus_hospital_master hosp ".
		" WHERE ".
		" 	occurrence_date = '$arisedate' ".
		" 	AND req.apply_id = app.apply_id ".
		" 	AND app.delete_flg = 'f' ".
		" 	AND req.hospital_id = hosp.hospital_id ".
		" 	AND hosp.del_flg = 'f' ";

	$sel = @select_from_table($con, $sql, "", $fname);
	$result = pg_fetch_all($sel);
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 検索 | コンサルテーション依頼書</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
</style>
<script type="text/javascript">

<? //検索結果を返す ?>
function setData(rowidx) {
	opener.document.apply.elements["data_occurrence_date"].value = document.getElementById("hdn_arise_date_" + rowidx).value;
	opener.document.apply.elements["data_title"].value = document.getElementById("hdn_title_" + rowidx).value;
	opener.document.apply.elements["data_first_problem"].value = document.getElementById("hdn_problem1_" + rowidx).value;
	opener.document.apply.elements["data_second_problem"].value = document.getElementById("hdn_problem2_" + rowidx).value;
	opener.document.apply.elements["data_third_problem"].value = document.getElementById("hdn_problem3_" + rowidx).value;
	opener.document.apply.elements["data_fourth_problem"].value = document.getElementById("hdn_problem4_" + rowidx).value;
	opener.document.apply.elements["data_fifth_problem"].value = document.getElementById("hdn_problem5_" + rowidx).value;
	opener.document.apply.elements["data_sixth_problem"].value = document.getElementById("hdn_problem6_" + rowidx).value;
	opener.document.apply.elements["data_seventh_problem"].value = document.getElementById("hdn_problem7_" + rowidx).value;
	opener.document.apply.elements["data_eighth_problem"].value = document.getElementById("hdn_problem8_" + rowidx).value;
	opener.document.apply.elements["data_nineth_problem"].value = document.getElementById("hdn_problem9_" + rowidx).value;
	opener.document.apply.elements["data_tenth_problem"].value = document.getElementById("hdn_problem10_" + rowidx).value;
	opener.document.apply.elements["data_problem_count"].value = document.getElementById("hdn_problem_count_"+rowidx).value;
	opener.document.apply.elements["data_hospital_id"].value = document.getElementById("hdn_hospital_" + rowidx).value;
	opener.document.apply.elements["link_report_no"].value = document.getElementById("hdn_report_no_" + rowidx).value;
	opener.document.apply.elements["hdn_consultation_request_id"].value = document.getElementById("hdn_consultation_request_id_" + rowidx).value;
	opener.document.apply.elements['hospital_directer'].value = document.getElementById("hdn_hospital_directer_" + rowidx).value;
	
	for (var i = 2; i <= 10; i++) {
		if(i<=document.getElementById("hdn_problem_count_"+rowidx).value){
			opener.document.getElementById("prb"+i).style.display = "block";
			opener.document.getElementById("adv"+i).style.display = "block";
		}else{
			opener.document.getElementById("prb"+i).style.display = "none";
			opener.document.getElementById("adv"+i).style.display = "none";
		}
	}

	self.close();
}

//今日ボタン押下処理
function getDateLabel() {

	currentDateObject = new Date();

	wYear = currentDateObject.getYear();

	currentFullYear = (wYear < 2000) ? wYear + 1900 : wYear;
	currentMonth = to2String(currentDateObject.getMonth() + 1);
	currentDate = to2String(currentDateObject.getDate());

	dateString = currentFullYear + "/" + currentMonth + "/" + currentDate;

	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = dateString;
}

function to2String(value) {
	label = "" + value;
	if (label.length < 2) {
		label = "0" + label;
	}
	return label;
}

//カレンダーからの戻り処理を行います。
function call_back_fplus_calendar(caller,ymd)
{
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = ymd;
}

function dateClear() {
	var obj_ymd = document.getElementById("arise_date");
	obj_ymd.value = "";
}

function clickselbutton() {
	if(document.getElementById("arise_date").value == "") {
		alert("検索条件を入力してください。");
		return false;
	}else{
		document.sel.action = 'fplus_selconsreq_pop.php';
		document.sel.search_mode.value = "1";
		document.sel.target = "_self";
		document.sel.submit();
		return false;
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
form {margin:0;}
/*table.list {border-collapse:collapse;}*/
table.list td {border:solid #5279a5 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
	show_fplus_header_for_sub_window("コンサルテーション依頼書検索");
?>
</table>
<form name="sel" action="#" method="post">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>">
	<input type="hidden" name="search_mode" id="search_mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td  bgcolor="#F5FFE5">
<? //検索条件入力フォーム　ここから-------------------- ?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="j12"><span>発生年月日</span></font>
			<font class="j12">
					<input type="text" name="arise_date" id="arise_date" size="15" readonly="readonly" value="<?=$arisedate?>" />
					<input type="button" value="今日" onclick="getDateLabel();" />
					<img id="arise_date_caller" src="img/calendar_link.gif" style="position:relative;top:3px;z-index:1;cursor:pointer; left: 0px;" alt="カレンダー" onclick="window.open('fplus_calendar.php?session=<? echo($session); ?>&caller=arise_date', 'epinet_arise_calendar', 'width=640,height=480,scrollbars=yes');"/>
					<input type="button" value="クリア" onclick="dateClear();" />
					<input id="Button2" type="button" value="検索" style="width:100px;" onclick="clickselbutton();" />
			</font>			
		</td>
	</tr>
</table>
<? //検索条件入力フォーム　ここまで-------------------- ?>
<? //検索結果一覧表示　ここから-------------- ?>
<table border="0" cellpadding="0" cellspacing="0" class="list1" style="background-color:#fff;width:100%;">
	<tr  style="background-color:#ffdfff;">
		<td align="center" style="width:10%;"><font class="j12"><span>依頼書��</span></font></td>
		<td align="center" style="width:10%;"><font class="j12"><span>発生年月日</span></font></td>
		<td align="left" style="width:30%;"><font class="j12"><span>発生病院</span></font></td>
		<td align="left" style="width:50%;"><font class="j12"><span>タイトル</span></font></td>
	</tr>
	<? if($serch=="1"){ ?>
		<? if($result[0]["report_no"]==""){ ?>
		<? //検索結果が0件 ?>
			<tr>
				<td colspan="12" align="center"><font class="j12">データが存在しません</font></td>
			</tr>
		<? }else{ ?>
		<? //検索結果が1件以上 ?>
			<? $count=0 ?>
			<? foreach($result as $idx => $row){ ?>
			<tr>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["report_no"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="center"><font class="j12"><?=$row["occurrence_date"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["hospital_name"]?></font></td>
				<td class=<?="\"apply_list_".$count."\"" ?> onmouseover="highlightCells(this.className);this.style.cursor='pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" onclick="setData(<?=$count?>);" align="left"><font class="j12"><?=$row["title"]?></font></td>
				<? //hiddenにデータ格納 ?>
				<input type="hidden" id="hdn_report_no_<?=$count?>" value="<?=$row["report_no"]?>" />
				<input type="hidden" id="hdn_arise_date_<?=$count?>" value="<?=$row["occurrence_date"]?>" />
				<input type="hidden" id="hdn_hospital_<?=$count?>" value="<?=$row["hospital_id"]?>" />
				<input type="hidden" id="hdn_hospital_directer_<?=$count?>" value="<?=$row["hospital_directer"]?>" />
				<input type="hidden" id="hdn_title_<?=$count?>" value="<?=$row["title"]?>" />
				<input type="hidden" id="hdn_problem1_<?=$count?>" value="<?=$row["first_problem"]?>" />
				<input type="hidden" id="hdn_problem2_<?=$count?>" value="<?=$row["second_problem"]?>" />
				<input type="hidden" id="hdn_problem3_<?=$count?>" value="<?=$row["third_problem"]?>" />
				<input type="hidden" id="hdn_problem4_<?=$count?>" value="<?=$row["fourth_problem"]?>" />
				<input type="hidden" id="hdn_problem5_<?=$count?>" value="<?=$row["fifth_problem"]?>" />
				<input type="hidden" id="hdn_problem6_<?=$count?>" value="<?=$row["sixth_problem"]?>" />
				<input type="hidden" id="hdn_problem7_<?=$count?>" value="<?=$row["seventh_problem"]?>" />
				<input type="hidden" id="hdn_problem8_<?=$count?>" value="<?=$row["eighth_problem"]?>" />
				<input type="hidden" id="hdn_problem9_<?=$count?>" value="<?=$row["nineth_problem"]?>" />
				<input type="hidden" id="hdn_problem10_<?=$count?>" value="<?=$row["tenth_problem"]?>" />
				<input type="hidden" id="hdn_problem_count_<?=$count?>" value="<?=$row["problem_count"]?>" />
				<input type="hidden" id="hdn_consultation_request_id_<?=$count?>" value="<?=$row["consultation_request_id"]?>" />
			</tr>
			<? $count++; ?>
			<?}?>
		<? } ?>
	<? } ?>
</table>
<? //検索結果一覧表示　ここまで-------------- ?>
</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
