<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');


//医療安全
$a = "2";


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
if (check_authority($session, 47, $fname) == "0")
{
    showLoginPage();
    exit;
}



//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');


//==================================================
// ログインユーザID
//==================================================
$emp_id = get_emp_id($con,$session,$fname);



//==================================================
// ツリー情報
//==================================================
$tree = lib_get_category_tree($con, $a, $emp_id, $fname, "private", $doc_counts);



//==================================================
// 一覧情報
//==================================================

//カテゴリが未指定の場合：カテゴリの一覧が表示対象
if ($c == "")
{
    //カテゴリ情報
    $category_list = lib_get_private_category_list($con, $a, $emp_id, $fname);
}

//カテゴリが指定されている場合：フォルダ(カテゴリ)内のフォルダとコンテンツの一覧が表示対象
if ($c != "")
{
    //フォルダ情報
    $sql = "select el_folder.folder_id, el_folder.folder_name, el_folder.reg_emp_id, el_folder.upd_time from el_folder left join el_tree on el_folder.folder_id = el_tree.child_id";
    $parent_id_cond = ($f == "") ? "is null" : " = $f";
    $cond = "where el_folder.lib_archive = '$a' and el_folder.lib_cate_id = '$c' and el_tree.parent_id $parent_id_cond order by el_folder.folder_name";
    $sel_child_folder = select_from_table($con, $sql, $cond, $fname);
    if ($sel_child_folder == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //コンテンツ情報
    $document_list = lib_get_document_list($con, $a, $c, $f, $emp_id, $fname);
}



//==================================================
// フォルダ更新フラグの設定
//==================================================
$folder_modify_flg = false;

//カテゴリが選択されていない場合
if ($c == "")
{
    //書庫直下であるため常に更新可能
    $folder_modify_flg = true;
}
//カテゴリが選択されている場合
else
{
    //ログインユーザーの所属情報を取得
    $sql = "select emp_class, emp_attribute, emp_dept, emp_st from empmst";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_class = pg_fetch_result($sel, 0, "emp_class");
    $emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
    $emp_dept = pg_fetch_result($sel, 0, "emp_dept");
    $emp_st = pg_fetch_result($sel, 0, "emp_st");

    //フォルダの更新権限を取得
    $folder_modify_flg = get_upd_flg($con, $fname, $a, $c, $f, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
}




//==================================================
// ボタンのdisabled状況を設定
//==================================================
$parent_button_disabled      = ($c == "") ? "disabled":"";
$folder_button_disabled      = !$folder_modify_flg ? "disabled":"";
$document_button_disabled    = !$folder_modify_flg || ($c == "") ? "disabled":"";
$folder_edit_button_disabled = !$folder_modify_flg || ($c == "") ? "disabled":"";
$delete_button_disabled      = !$folder_modify_flg ? "disabled":"";
if ($design_mode == 2){
    $parent_button_class      = ($c == "") ? "button_disabled":"button";
    $folder_button_class      = !$folder_modify_flg ? "button_disabled":"button";
    $document_button_class    = !$folder_modify_flg || ($c == "") ? "button_disabled":"button";
    $folder_edit_button_class = !$folder_modify_flg || ($c == "") ? "button_disabled":"button";
    $delete_button_class      = !$folder_modify_flg ? "button_disabled":"button";
}


//==================================================
// デフォルトの並び順を設定
//==================================================
// コンテンツ名の昇順
if ($o == "")
{
    $o = "1";
}



//==================================================
// ドラッグ＆ドロップ使用フラグを取得
//==================================================
$dragdrop_flg = lib_get_dragdrop_flg($con, $fname);

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);
$smarty->assign("fname", $fname);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("PAGE_TITLE", "書庫");
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//e-ラーニング（書庫）データ
$smarty->assign("dragdrop_flg", $dragdrop_flg == "t");
$smarty->assign("cnt_of_tree", count($tree));
$smarty->assign("tree", $tree);
$smarty->assign("a", $a);
$smarty->assign("c", $c);
$smarty->assign("f", $f);
$smarty->assign("o", $o);
$smarty->assign("doc_counts", $doc_counts);
$smarty->assign("parent_folder_id", $parent_folder_id);
$smarty->assign("path", $path);

$smarty->assign("parent_button_disabled", $parent_button_disabled);
$smarty->assign("folder_button_disabled", $folder_button_disabled);
$smarty->assign("document_button_disabled", $document_button_disabled);
$smarty->assign("folder_edit_button_disabled", $folder_edit_button_disabled);
$smarty->assign("delete_button_disabled", $delete_button_disabled);
if ($design_mode == 2){
    $smarty->assign("parent_button_class", $parent_button_class);
    $smarty->assign("folder_button_class", $folder_button_class);
    $smarty->assign("document_button_class", $document_button_class);
    $smarty->assign("folder_edit_button_class", $folder_edit_button_class);
    $smarty->assign("delete_button_class", $delete_button_class);
}
$contents = get_document_list($category_list, $sel_child_folder, $document_list, $a, $c, $f, $o, $folder_modify_flg, $doc_counts, $session, $emp_id, $dragdrop_flg, $con, $fname);
$smarty->assign("contents", $contents);

if ($design_mode == 1){
    $smarty->display("hiyari_el_list_all1.tpl");
}
else{
    $smarty->display("hiyari_el_list_all2.tpl");
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);


//========================================================================================================================================================================================================
//内部関数
//========================================================================================================================================================================================================
// フォルダツリー表示JavaScriptを出力
function lib_write_tree_js($tree, $a, $c, $f, $o, $doc_counts, $session, $fname, &$parent_folder_id, $dragdrop_flg)
{
    $is_safari = (strpos($_SERVER["HTTP_USER_AGENT"], "Safari") !== false);

    //医療安全
    $a = 2;

    foreach ($tree as $tmp_folder)
    {
        $tmp_type = $tmp_folder["type"];
        $tmp_id = $tmp_folder["id"];
        $tmp_nm = h(str_replace("'", "\\'", $tmp_folder["name"]));
        $tmp_writable = $tmp_folder["writable"];

        $lock_icon = ($tmp_writable) ? "" : "<img src=\"img/icon/lock.gif\" alt=\"読み取り専用\" width=\"9\" height=\"12\" border=\"0\" style=\"margin-left:1px;vertical-align:text-bottom;\">";

        if ($tmp_type == "category")
        {
            $tmp_elm = "folder$tmp_id";
            if (is_array($doc_counts))
            {
                $tmp_doc_count = "(" . intval($doc_counts[$tmp_id]["-"]) . ")";
            }
            else
            {
                $tmp_doc_count = "";
            }

            if ($is_safari) {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm$tmp_doc_count</font><span style=\"display:none;\">$tmp_id,</span>$lock_icon', root, false);\n");
            } else {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm<font color=\"#444\">$tmp_doc_count</font></font><span style=\"display:none;\">$tmp_id,</span>$lock_icon', root, false);\n");
            }
            echo("$tmp_elm.onLabelClick = function () {location.href = '$fname?session=$session&a=$a&c=$tmp_id&o=$o'; return false;}\n");
            if ($tmp_writable && $dragdrop_flg == "t")
            {
                echo("new YAHOO.util.DDTarget($tmp_elm.labelElId, 'docs');\n");
            }
            if ($c == $tmp_id && $f == "")
            {
                echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
            }
        }
        else if ($tmp_type == "folder")
        {
            $tmp_cate_id = $tmp_folder["cate_id"];
            $tmp_parent_id = $tmp_folder["parent_id"];
            $tmp_parent_elm = ($tmp_parent_id == "") ? "folder$tmp_cate_id" : "folder{$tmp_cate_id}_{$tmp_parent_id}";
            $tmp_elm = "folder{$tmp_cate_id}_{$tmp_id}";
            if (is_array($doc_counts))
            {
                $tmp_doc_count = "(" . intval($doc_counts[$tmp_cate_id][$tmp_id]) . ")";
            }
            else
            {
                $tmp_doc_count = "";
            }

            if ($is_safari) {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm$tmp_doc_count</font><span style=\"display:none;\">$tmp_cate_id,$tmp_id</span>$lock_icon', $tmp_parent_elm, false);\n");
            } else {
                echo("var $tmp_elm = new YAHOO.widget.TextNode('<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_nm<font color=\"#444\">$tmp_doc_count</font></font><span style=\"display:none;\">$tmp_cate_id,$tmp_id</span>$lock_icon', $tmp_parent_elm, false);\n");
            }
            echo("$tmp_elm.onLabelClick = function () {location.href = '$fname?session=$session&a=$a&c=$tmp_cate_id&f=$tmp_id&o=$o'; return false;}\n");
            if ($tmp_writable && $dragdrop_flg == "t")
            {
                echo("new YAHOO.util.DDTarget($tmp_elm.labelElId, 'docs');\n");
            }
            if ($f == $tmp_id)
            {
                $parent_folder_id = $tmp_parent_id;
                echo("$tmp_elm.labelStyle = 'ygtvlabel selectedLabel';\n");
                echo("expandAncestor($tmp_elm);\n");
            }
        }

        lib_write_tree_js($tmp_folder, $a, $c, $f, $o, $doc_counts, $session, $fname, $parent_folder_id, $dragdrop_flg);
    }
}

// コンテンツ一覧の取得
function get_document_list($category_list, $sel_child_folder, $document_list, $a, $c, $f, $o, $folder_modify_flg, $doc_counts, $session, $emp_id, $dragdrop_flg, $con, $fname)
{
    //医療安全
    $a = 2;

    //ログインユーザーの所属情報取得
    $sql = "select emp_class, emp_attribute, emp_dept, emp_st from empmst";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_class = pg_fetch_result($sel, 0, "emp_class");
    $emp_atrb = pg_fetch_result($sel, 0, "emp_attribute");
    $emp_dept = pg_fetch_result($sel, 0, "emp_dept");
    $emp_st = pg_fetch_result($sel, 0, "emp_st");


    //表示情報
    $docs = array();

    //カテゴリが指定されていない場合：カテゴリの一覧
    if ($c == "")
    {
        foreach ($category_list as $tmp_category)
        {
            $tmp_category_id = $tmp_category["id"];

            // 権限確認
            list($ref_flg, $upd_flg) = get_category_ref_upd_flg($con, $fname, $a, $tmp_category_id, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
            if ($ref_flg == false)
            {
                continue;
            }

            $tmp_doc_count = $doc_counts[$tmp_category_id]["-"];
            $tmp_reg_emp_id = $tmp_category["reg_emp_id"];
            $tmp_folder_modify_flg = ($upd_flg);
            $tmp_folder_delchk_flg = (($tmp_doc_count == 0) && ($upd_flg));
            $tmp_modified = $tmp_category["modified"];

            $docs[] = array(
                "id" => $tmp_category_id,
                "name" => $tmp_category["name"],
                "type" => "category",
                "file_type" => "フォルダ",
                "size" => 0,
                "modify_flg" => $tmp_folder_modify_flg,
                "delchk_flg" => $tmp_folder_delchk_flg,
                "path" => "",
                "modified" => $tmp_modified
            );
        }
    }

    //カテゴリが指定されている場合：フォルダの一覧
    else
    {
        if (is_resource($sel_child_folder))
        {

            while ($row = pg_fetch_array($sel_child_folder))
            {
                $tmp_folder_id = $row["folder_id"];

                // 権限確認
                list($ref_flg, $upd_flg) = get_folder_ref_upd_flg($con, $fname, $a, $tmp_folder_id, $emp_id, $emp_class, $emp_atrb, $emp_dept, $emp_st);
                if ($ref_flg == false)
                {
                    continue;
                }

                $tmp_doc_count = $doc_counts[$c][$tmp_folder_id];
                $tmp_reg_emp_id = $row["reg_emp_id"];
                $tmp_folder_modify_flg = ($upd_flg);
                $tmp_folder_delchk_flg = (($tmp_doc_count == 0) && ($upd_flg));
                $tmp_modified = $row["upd_time"];

                $docs[] = array(
                    "id" => $tmp_folder_id,
                    "name" => $row["folder_name"],
                    "type" => "folder",
                    "file_type" => "フォルダ",
                    "size" => 0,
                    "modify_flg" => $tmp_folder_modify_flg,
                    "delchk_flg" => $tmp_folder_delchk_flg,
                    "path" => "",
                    "modified" => $tmp_modified
                );
            }
        }
        if (is_array($document_list))
        {
            $docs = array_merge($docs, $document_list);
        }
    }



    if (count($docs) == 0)
    {
        return;
    }

    switch ($o)
    {
        case "1":  // 名前の昇順
            usort($docs, "sort_docs_by_name");
            break;
        case "2":  // 名前の降順
            usort($docs, "sort_docs_by_name_desc");
            break;
        case "3":  // 更新日時の昇順
            usort($docs, "sort_docs_by_modified");
            break;
        case "4":  // 更新日時の降順
            usort($docs, "sort_docs_by_modified_desc");
            break;
        case "5":  // サイズの昇順
            usort($docs, "sort_docs_by_size");
            break;
        case "6":  // サイズの降順
            usort($docs, "sort_docs_by_size_desc");
            break;
    }

    $contens = array();
    foreach ($docs as $key => $tmp_doc)
    {
        $tmp_did = $tmp_doc["id"];
        $tmp_dnm = h($tmp_doc["name"]);
        $tmp_type = $tmp_doc["type"];
        $tmp_file_type = $tmp_doc["file_type"];
        $tmp_size = $tmp_doc["size"];

        if ($tmp_type == "category" || $tmp_type == "folder")
        {
            $tmp_modify_flg = ($tmp_doc["modify_flg"]) ? "t" : "f";
        }
        else
        {
            $tmp_modify_flg = ($folder_modify_flg && $tmp_doc["modify_flg"]) ? "t" : "f";
        }
        $tmp_path = $tmp_doc["path"];
        $tmp_modified = $tmp_doc["modified"];

        if ($tmp_type == "category" || $tmp_type == "folder")
        {
            $tmp_delchk_flg = ($tmp_doc["delchk_flg"]) ? "t" : "f";
        }
        else
        {
            $tmp_delchk_flg = $tmp_modify_flg;
        }

        // 削除チェックボックス
        if ($tmp_delchk_flg == "t")
        {
            if ($tmp_type == "category")
            {
                $cbox = "<input type=\"checkbox\" class=\"checkbox\" name=\"cid[]\" value=\"$tmp_did\">";
            }
            else if ($tmp_type == "folder")
            {
                $cbox = "<input type=\"checkbox\" class=\"checkbox\" name=\"fid[]\" value=\"$tmp_did\">";
            }
            else
            {
                $cbox = "<input type=\"checkbox\" class=\"checkbox\" name=\"did[]\" value=\"$tmp_did\">";
            }
        }
        else
        {
            $cbox = "<input type=\"checkbox\" class=\"checkbox\" disabled>";
        }

        // コンテンツのボックスのクラス名
        $tmp_div_class = ($tmp_type == "document" && $tmp_modify_flg == "t") ? "doc" : "";

        // アイコン
        if ($tmp_type == "category" || $tmp_type == "folder")
        {
            $icon = "<img src=\"img/icon/folder.gif\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle;\">";
        }
        else
        {
            $cursor = ($tmp_modify_flg == "t" && $dragdrop_flg == "t") ? "cursor:move;" : "";
            switch ($tmp_file_type)
            {
                case "Word":
                    $img = "word.jpg";
                    break;
                case "Excel":
                    $img = "excel.jpg";
                    break;
                case "PowerPoint":
                    $img = "powerpoint.jpg";
                    break;
                case "PDF":
                    $img = "pdf.jpg";
                    break;
                case "テキスト":
                    $img = "text.jpg";
                    break;
                case "JPEG":
                    $img = "jpeg.jpg";
                    break;
                case "GIF":
                    $img = "gif.jpg";
                    break;
                default:
                    $img = "other.jpg";
                    break;
            }
            $icon = "<img id=\"h_doc$tmp_did\" src=\"img/icon/$img\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle;$cursor\">";
        }

        // 名前のリンク先
        if ($tmp_type == "category")
        {
            $tmp_anchor = "<a href=\"$fname?session=$session&a=$a&c=$tmp_did&o=$o\">$tmp_dnm</a>";
        }
        else if ($tmp_type == "folder")
        {
            $tmp_anchor = "<a href=\"$fname?session=$session&a=$a&c=$c&f=$tmp_did&o=$o\">$tmp_dnm</a>";
        }
        else
        {
            $tmp_lib_url = urlencode($tmp_path);
            if (lib_get_filename_flg() > 0)
            {
                $tmp_anchor = "<a href=\"hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\">$tmp_dnm</a>";
            }
            else
            {
                $tmp_anchor = "<a href=\"hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\" onclick=\"window.open('hiyari_el_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url'); return false;\">$tmp_dnm</a>";
            }
        }

        //感想
        $kansou_url = "hiyari_el_kansou.php?session={$session}&lib_id={$tmp_did}&path=1";
        if($tmp_type == "document")
        {
            $tmp_kansou = "<a href=\"javascript:show_sub_window('$kansou_url')\">感想</a>";
        }
        else
        {
            $tmp_kansou = "&nbsp;";
        }

        // 更新日
        if (strlen($tmp_modified) >= 8)
        {
            $tmp_modified_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(.*)$/", "$1/$2/$3", $tmp_modified);
        }
        else
        {
            $tmp_modified_str = "-";
        }

        // サイズ
        $size = format_size($tmp_size);

        // 権限
        if ($tmp_type == "category")
        {
            $auth_update_url = "hiyari_el_folder_update.php?session=$session&archive=$a&cate_id=$tmp_did&folder_id=&o=$o";
        }
        else if ($tmp_type == "folder")
        {
            $auth_update_url = "hiyari_el_folder_update.php?session=$session&archive=$a&cate_id=$c&folder_id=$tmp_did&o=$o";
        }
        else
        {
            $auth_update_url = "hiyari_el_update.php?session=$session&a=$a&c=$c&f=$f&o=$o&lib_id=$tmp_did";
        }

        $data["can_delete"] = ($tmp_delchk_flg == "t");
        $data["cbox"]=$cbox;
        $data["did"]=$tmp_did;
        $data["div_class"]=$tmp_div_class;
        $data["icon"]=$icon;
        $data["anchor"]=$tmp_anchor;
        $data["kansou"]=$tmp_kansou;
        $data["modified_str"]=$tmp_modified_str;
        $data["size"]=$size;
        $data["auth_update_url"]=$auth_update_url;
        $data["auth"] = ($tmp_modify_flg == "t") ? "更新" : "参照";

        $contens[] = $data;
    }

    return $contens;
}

//※show_document_list()専用関数：サイズをフォーマット
function format_size($size)
{
    if ($size == 0)
    {
        return "-";
    }
    else if ($size <= 1023)
    {
        return "{$size}バイト";
    }
    else if ($size >= 1024 && $size <= 1048576)
    {
        $ret = number_format(($size / 1024), 2) . "KB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    }
    else
    {
        $ret = number_format(($size / 1024 / 1024), 2) . "MB";
        $ret = str_replace(".00", "", $ret);
        return preg_replace("/(.*)\.(\d)0/", "$1.$2", $ret);
    }
}


// コンテンツ一覧を名前の昇順でソート
function sort_docs_by_name($doc1, $doc2)
{
    return strcasecmp($doc1["name"], $doc2["name"]);
}

// コンテンツ一覧を名前の降順でソート
function sort_docs_by_name_desc($doc1, $doc2)
{
    return sort_docs_by_name($doc1, $doc2) * -1;
}

// コンテンツ一覧を更新日時の昇順でソート
function sort_docs_by_modified($doc1, $doc2)
{
    return strcasecmp($doc1["modified"], $doc2["modified"]);
}

// コンテンツ一覧を更新日時の降順でソート
function sort_docs_by_modified_desc($doc1, $doc2)
{
    return sort_docs_by_modified($doc1, $doc2) * -1;
}

// コンテンツ一覧をサイズの昇順でソート
function sort_docs_by_size($doc1, $doc2)
{
    return strnatcasecmp($doc1["size"], $doc2["size"]);
}

// コンテンツ一覧をサイズの降順でソート
function sort_docs_by_size_desc($doc1, $doc2)
{
    return sort_docs_by_size($doc1, $doc2) * -1;
}
