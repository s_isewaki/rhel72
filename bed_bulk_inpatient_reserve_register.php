<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 一括登録 | 入院予定登録</title>
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

$default_url = ($section_admin_auth == "1") ? "entity_menu.php" : "building_list.php";

// 初期表示時
if ($mode == "") {

	// 文字コードのデフォルトはShift_JIS
	if ($encoding == "") {$encoding = "1";}

	// 重複データ処理のデフォルトは上書き
	if ($dup == "") {$dup = "1";}

// 登録ボタン押下時
} else {

	// ファイルが正常にアップロードされたかどうかチェック
	$uploaded = false;
	if (array_key_exists("csvfile", $_FILES)) {
		if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
			$uploaded = true;
		}
	}
	if ($uploaded) {
		$result = import_csv($fname);
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a> &gt; <a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>"><b>一括登録</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<? if ($section_admin_auth == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="entity_menu.php?session=<? echo($session); ?>">診療科</a></font></td>
</tr>
<? } ?>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="building_list.php?session=<? echo($session); ?>">病棟</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>・</b></td>
<td width="90%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="bed_bulk_inpatient_reserve_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入院予定登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<form action="bed_bulk_inpatient_reserve_register.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">重複データの処理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="dup" value="1"<? if ($dup == "1") {echo(" checked");} ?>>上書き処理する
<input type="radio" name="dup" value="2"<? if ($dup == "2") {echo(" checked");} ?>>エラーとして無視する
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="import">
</form>
<? if ($mode == "import") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<? if (!$uploaded) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルのアップロードに失敗しました。</font></td>
</tr>
<? } else if ($uploaded && $result["failed_count"] > 0) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">
<? if ($result["succeeded_count"] > 0) { ?>
以下のエラーが発生しましたが、登録処理が完了しました。（登録件数：<? echo($result["succeeded_count"]); ?>件、エラー件数：<? echo($result["failed_count"]); ?>件）
<? } else { ?>
以下のエラーが発生しました。（登録件数：0件、エラー件数：<? echo($result["failed_count"]); ?>件）
<? } ?>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<ul>
<? foreach ($result["errors"] as $error) { ?>
<li><? echo(h($error)); ?></li>
<? } ?>
</ul>
</font></td>
</tr>
<? } else { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。（登録件数：<? echo($result["succeeded_count"]); ?>件、エラー件数：0件）</font></td>
</tr>
<? } ?>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。</p>
<p style="margin:0;">患者ID、患者氏名（漢字）、患者氏名（かな）、生年月日、性別、診療科、担当医は必須です。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">患者ID……使える文字は半角英数字のみです。</li>
<li style="margin-left:40px;">患者氏名（漢字）……苗字と名前はスペース区切り（スペースは全角半角個数問わず）</li>
<li style="margin-left:40px;">患者氏名（カナ）……同上</li>
<li style="margin-left:40px;">生年月日……YYYYMMDD形式</li>
<li style="margin-left:40px;">性別……半角数字1桁（0：不明、1：男性、2：女性）</li>
<li style="margin-left:40px;">病棟名……未定の場合は空</li>
<li style="margin-left:40px;">病室名……未定の場合は空</li>
<li style="margin-left:40px;">ベッド番号……半角数字1桁（1〜8）、未定の場合は空</li>
<li style="margin-left:40px;">入院予定日時……YYYYMMDDHHII形式（時刻は30分刻みで調整されます）、未定の場合は空（当月中の入院予定として調整されます）</li>
<li style="margin-left:40px;">診療科</li>
<li style="margin-left:40px;">担当医……苗字と名前はスペース区切り（スペースは全角半角個数問わず）</li>
</ul>
<dl style="margin:0;">
<dt>例：</dt>
<dd style="margin-left:20px;font-family:monospace;">0000000001,山田　太郎,ヤマダ　タロウ,19550505,1,A病棟,301号室,2,200903231130,第一内科,田中　一郎</dd>
<dd style="margin-left:20px;font-family:monospace;">0000000002,鈴木　　花子,スズキ　　ハナコ,19770707,2,,,,,脳外科,渡辺　　三郎</dd>
</dl>
</font></td>
</tr>
</table>
</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<?
function import_csv($fname) {
	ini_set("max_execution_time", 0);
	require_once("get_values.ini");

	// 文字コードの設定
	switch ($_POST["encoding"]) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
	}

	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character(0x30FB);

	// 文字変換テーブルの取得
	$character_table = get_character_table($file_encoding);

	// 正しいカラム数
	$column_count = 11;

	// CSVデータを配列に格納
	$patients = array();
	$record_no = 0;
	$lines = file($_FILES["csvfile"]["tmp_name"]);
	foreach ($lines as $line) {

		// 異体字を変換
		$line = convert_encoding($line, $character_table);

		// 文字コードを変換
		$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		$record_no++;
		$patients[$record_no] = split(",", $line);
	}
	unset($lines);

	// データベースに接続
	$con = connect2db($fname);

	// トランザクションを開始
	pg_query($con, "begin");

	// 病棟データを配列に格納
	$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
	$cond = "where ward_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$wards = array();
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["bldg_cd"] . "-" . $row["ward_cd"]] = $row["ward_name"];
	}

	// 病室データを配列に格納
	$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_cur from ptrmmst";
	$cond = "where ptrm_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rooms = array();
	$bed_counts = array();
	while ($row = pg_fetch_array($sel)) {
		$rooms[$row["bldg_cd"] . "-" . $row["ward_cd"] . "-" . $row["ptrm_room_no"]] = $row["ptrm_name"];
		$bed_counts[$row["bldg_cd"] . "-" . $row["ward_cd"] . "-" . $row["ptrm_room_no"]] = $row["ptrm_bed_cur"];
	}

	// 診療科データを配列に格納
	$sql = "select sect_id, sect_nm from sectmst";
	$cond = "where sect_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sects = array();
	while ($row = pg_fetch_array($sel)) {
		$sects[$row["sect_id"]] = $row["sect_nm"];
	}

	// 担当医データを配列に格納
	$sql = "select sect_id, dr_id, dr_nm from drmst";
	$cond = "where dr_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	mb_regex_encoding("EUC-JP");
	$doctors = array();
	while ($row = pg_fetch_array($sel)) {
		$dr_nm = mb_convert_kana($row["dr_nm"], "s");
		$dr_nm = mb_ereg_replace(" " , "", $dr_nm);
		$doctors[$row["sect_id"] . "-" . $row["dr_id"]] = $dr_nm;
	}

	// ログインユーザの職員IDを取得
	$emp_id = get_emp_id($con, $session, $fname);

	// 入院予定データを登録
	$errors = array();
	foreach ($patients as $record_no => $patient_data) {
		$ptif_id = trim($patient_data[0]);
		if ($ptif_id == "") $ptif_id = "（不明）";

		if (count($patient_data) != $column_count) {
			$errors[] = "行番号：{$record_no}、患者ID：{$ptif_id}、理由：カラム数が不正です";
			continue;
		}

		$error = insert_patient_data($con, $patient_data, $wards, $rooms, $bed_counts, $sects, $doctors, $emp_id, $encoding, $session, $fname);
		if ($error != "") $errors[] = "行番号：{$record_no}、患者ID：{$ptif_id}、理由：{$error}";
	}

	// トランザクションをコミット
	pg_query($con, "commit");

	// データベース接続を閉じる
	pg_close($con);

	$failed_count = count($errors);
	return array(
		"succeeded_count" => count($patients) - $failed_count,
		"failed_count" => $failed_count,
		"errors" => $errors
	);
}

// 患者データを登録
function insert_patient_data($con, $patient_data, $wards, $rooms, $bed_counts, $sects, $doctors, $emp_id, $encoding, $session, $fname) {

	// 患者IDの入力チェック
	$ptif_id = trim($patient_data[0]);
	if ($ptif_id == "") {
		return "患者IDは必須です";
	}
	if (preg_match("/[^0-9a-zA-Z]/", $ptif_id) > 0) {
		return "患者IDに使える文字は半角英数字のみです";
	}
	if (strlen($ptif_id) > 12) {
		return "患者IDが長すぎます";
	}

	// 患者氏名（漢字）の入力チェック
	$pt_nm = trim(mb_convert_kana($patient_data[1], "s"));
	$pt_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_nm);
	list($lt_nm, $ft_nm) = explode(" ", $pt_nm);
	if ($lt_nm == "") {
		return "苗字（漢字）を入力してください";
	}
	if (strlen($lt_nm) > 20) {
		return "苗字（漢字）が長すぎます";
	}
	if ($ft_nm == "") {
		$ft_nm = "　";
	}
	if (strlen($ft_nm) > 20) {
		return "名前（漢字）が長すぎます";
	}

	// 患者氏名（かな）の入力チェック
	$pt_kana_nm = trim(mb_convert_kana($patient_data[2], "HVcs"));
	$pt_kana_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_kana_nm);
	list($lt_kana_nm, $ft_kana_nm) = explode(" ", $pt_kana_nm);
	if ($lt_kana_nm == "") {
		return "苗字（カナ）を入力してください";
	}
	if (strlen($lt_kana_nm) > 20) {
		return "苗字（カナ）が長すぎます";
	}
	if ($ft_kana_nm == "") {
		$ft_kana_nm = "　";
	}
	if (strlen($ft_kana_nm) > 20) {
		return "名前（カナ）が長すぎます";
	}

	// 生年月日のチェック
	$birth = trim($patient_data[3]);
	if (preg_match("/^\d{8}$/", $birth) == 0) {
		return "生年月日が不正です";
	}
	$birth_yr = substr($birth, 0, 4);
	$birth_mon = substr($birth, 4, 2);
	$birth_day = substr($birth, 6, 2);
	if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
		return "生年月日が不正です";
	}
	$birth = sprintf("%04d%02d%02d", $birth_yr, $birth_mon, $birth_day);

	// 性別の入力チェック
	$sex = trim($patient_data[4]);
	if ($sex != "0" && $sex != "1" && $sex != "2") {
		return "性別が不正です";
	}

	// 病棟名の入力チェック
	$ward_name = trim($patient_data[5]);
	if ($ward_name != "") {
		$ward_key = array_search($ward_name, $wards);
		if ($ward_key == "") {
			return "病棟名が不正です";
		}
		list($bldg_cd, $ward_cd) = split("-", $ward_key);
	} else {
		$bldg_cd = null;
		$ward_cd = null;
	}

	// 病室名の入力チェック
	$room_name = trim($patient_data[6]);
	if ($room_name != "") {
		$room_keys = array_keys($rooms, $room_name);
		if (count($room_keys) == 0) {
			return "病室名が不正です";
		}

		$room_found = false;
		foreach ($room_keys as $room_key) {
			list($cfm_bldg_cd, $cfm_ward_cd, $room_cd) = split("-", $room_key);
			if ($cfm_bldg_cd == $bldg_cd && $cfm_ward_cd == $ward_cd) {
				$room_found = true;
				break;
			}
		}
		if (!$room_found) {
			return "指定病室が指定病棟に存在しません";
		}
	} else {
		$room_cd = null;
	}

	// ベッド番号の入力チェック
	$bed = trim($patient_data[7]);
	if ($bed != "") {
		$bed = intval($bed);
		if ($bed < 1 || $bed > $bed_counts[$bldg_cd . "-" . $ward_cd . "-" . $room_cd]) {
			return "ベッド番号が不正です";
		}
	} else {
		$bed = null;
	}

	// 入院予定日時の入力チェック
	$in_dttm = trim($patient_data[8]);
	if ($in_dttm != "") {
		if (strlen($in_dttm) == 8) {
			$in_dttm .= "1000";
		}
		if (preg_match("/^\d{12}$/", $in_dttm) == 0) {
			return "入院予定日時が不正です";
		}
		$in_yr = substr($in_dttm, 0, 4);
		$in_mon = substr($in_dttm, 4, 2);
		$in_day = substr($in_dttm, 6, 2);
		if (!checkdate($in_mon, $in_day, $in_yr)) {
			return "入院予定日が不正です";
		}
		$in_dt = sprintf("%04d%02d%02d", $in_yr, $in_mon, $in_day);
		$in_hr = substr($in_dttm, 8, 2);
		if (!$in_hr < 0 || $in_hr > 24) {
			return "入院予定時刻が不正です";
		}
		$in_min = (substr($in_dttm, 10, 2) < 30) ? "00" : "30";
		$in_tm = sprintf("%02d%02d", $in_hr, $in_min);
		$in_dt_flg = "t";
		$in_ym = null;
		$in_td = null;
	} else {
		$in_dt = null;
		$in_tm = null;
		$in_dt_flg = "f";
		$in_ym = date("Ym");
		$in_td = "9";
	}

	// 診療科の入力チェック
	$sect_name = trim($patient_data[9]);
	if ($sect_name == "") {
		return "診療科は必須です";
	}
	$sect_cd = array_search($sect_name, $sects);
	if ($sect_cd == "") {
		return "診療科が不正です";
	}

	// 担当医のチェック
	$doctor_name = trim($patient_data[10]);
	if ($doctor_name == "") {
		$doctor_cd = 0;
	} else {
		$doctor_name = mb_convert_kana($doctor_name, "s");
		$doctor_name = mb_ereg_replace(" " , "", $doctor_name);
		$doctor_keys = array_keys($doctors, $doctor_name);
		if (count($doctor_keys) == 0) {
			return "担当医が不正です";
		}
		$doctor_cd = null;
		foreach ($doctor_keys as $doctor_key) {
			list($cfm_sect_cd, $tmp_doctor_cd) = split("-", $doctor_key);
			if ($cfm_sect_cd == $sect_cd) {
				$doctor_cd = $tmp_doctor_cd;
				break;
			}
		}
		if (is_null($doctor_cd)) {
			return "指定担当医が指定診療科に存在しません";
		}
	}

	// 登録値の編集
	$keyword = get_keyword($lt_kana_nm);

	// 患者未登録の場合は登録
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) == 0) {
		$sql = "insert into ptifmst (ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_keywd, ptif_in_out) values (";
		$content = array($ptif_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, "1");
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 指定患者が入院登録済みの場合は更新せず復帰
	$sql = "select count(*) from inptmst";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		return "入院登録済みのためスキップしました";
	}

	// 指定患者の入院予定情報を取得
	$sql = "select inpt_in_res_dt, inpt_in_res_tm, inpt_in_res_ym, inpt_in_res_td, in_res_updated from inptres";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$is_registered = (intval(pg_fetch_result($sel, 0, 0)) > 0);

	// 登録済みの場合
	if ($is_registered) {

		// 「上書き処理する」の場合
		if ($_POST["dup"] == "1") {
			$org_in_res_dt = trim(pg_fetch_result($sel, 0, "inpt_in_res_dt"));
			$org_in_res_tm = trim(pg_fetch_result($sel, 0, "inpt_in_res_tm"));
			$org_in_res_ym = trim(pg_fetch_result($sel, 0, "inpt_in_res_ym"));
			$org_in_res_td = trim(pg_fetch_result($sel, 0, "inpt_in_res_td"));
			if (strcmp($in_dt, $org_in_res_dt) <> 0 || strcmp($in_tm, $org_in_res_tm) <> 0 || strcmp($in_ym, $org_in_res_ym) <> 0 || strcmp($in_td, $org_in_res_td) <> 0) {
				$in_res_updated = date("YmdHis");
			} else {
				$in_res_updated = pg_fetch_result($sel, 0, "in_res_updated");
			}

			// 入院予定データを更新
			$sql = "update inptres set";
			$set = array("bldg_cd", "ward_cd", "ptrm_room_no", "inpt_bed_no", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_keywd", "inpt_sect_id", "dr_id", "nurse_id", "inpt_in_res_dt_flg", "inpt_in_res_dt", "inpt_in_res_tm", "inpt_in_res_ym", "inpt_in_res_td", "in_res_updated");
			$setvalue = array($bldg_cd, $ward_cd, $room_cd, $bed, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $keyword, $sect_cd, $doctor_cd, "0", $in_dt_flg, $in_dt, $in_tm, $in_ym, $in_td, $in_res_updated);
			$cond = "where ptif_id = '$ptif_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		// 「エラーとして無視する」の場合
		} else {
			return "入院予定登録済みのためスキップしました";
		}

	// 未登録の場合
	} else {

		// 入院予定データを追加
		$sql = "insert into inptres (ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_bed_no, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_lt_kn_nm, inpt_ft_kn_nm, inpt_keywd, inpt_enti_id, inpt_sect_id, dr_id, nurse_id, inpt_in_res_dt_flg, inpt_in_res_dt, inpt_in_res_tm, inpt_in_res_ym, inpt_in_res_td, in_res_updated) values (";
		$content = array($ptif_id, $bldg_cd, $ward_cd, $room_cd, $bed, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $keyword, "1", $sect_cd, $doctor_cd, "0", $in_dt_flg, $in_dt, $in_tm, $in_ym, $in_td, date("YmdHis"));
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	return "";
}

function get_character_table($file_encoding) {
	switch ($file_encoding) {
	case "SJIS":
		$table_file = "character_table_sjis.txt";
		break;
	case "EUC-JP":
		$table_file = "character_table_euc.txt";
		break;
	case "UTF-8":
		$table_file = "character_table_utf8.txt";
		break;
	}

	if (!file_exists($table_file)) return array();

	$lines = file($table_file);

	$table = array();
	for ($i = 0, $j = count($lines); $i < $j; $i++) {
		$tmp_columns = split(',', trim($lines[$i]));
		if (count($tmp_columns) == 2) {
			$table[] = $tmp_columns;
		}
	}
	return $table;
}

function convert_encoding($line, $character_table) {
	foreach ($character_table as $pair) {
		$line = str_replace($pair[0], $pair[1], $line);
	}
	return $line;
}
?>
