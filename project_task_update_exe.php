<?
require("about_authority.php");
require("about_session.php");

if ($wherefrom == "1") {
	$back_url = "to_do_update_project.php";
} else {
	$back_url = "project_task_update.php";
}
?>
<body>
<form name="items" action="<? echo($back_url); ?>" method="post">
<input type="hidden" name="back" value="t">
<input type="hidden" name="work_name" value="<? echo($work_name); ?>">
<input type="hidden" name="status" value="<? echo($status); ?>">
<input type="hidden" name="start_date_y" value="<? echo($start_date_y); ?>">
<input type="hidden" name="start_date_m" value="<? echo($start_date_m); ?>">
<input type="hidden" name="start_date_d" value="<? echo($start_date_d); ?>">
<input type="hidden" name="end_date_y" value="<? echo($end_date_y); ?>">
<input type="hidden" name="end_date_m" value="<? echo($end_date_m); ?>">
<input type="hidden" name="end_date_d" value="<? echo($end_date_d); ?>">
<input type="hidden" name="detail" value="<? echo($detail); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="scale" value="<? echo($scale); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="work_id" value="<? echo($work_id); ?>">
<input type="hidden" name="target_id_list" value="<? echo($target_id_list); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($ward == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($work_name == "") {
	echo("<script type=\"text/javascript\">alert('作業名を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($work_name) > 80) {
	echo("<script type=\"text/javascript\">alert('作業名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($start_date_m, $start_date_d, $start_date_y)) {
	echo("<script type=\"text/javascript\">alert('開始日が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!checkdate($end_date_m, $end_date_d, $end_date_y)) {
	echo("<script type=\"text/javascript\">alert('終了予定日が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
$start_date = "$start_date_y$start_date_m$start_date_d";
$end_date = "$end_date_y$end_date_m$end_date_d";
if ($end_date < $start_date) {
	echo("<script type=\"text/javascript\">alert('終了予定日は開始日以降にしてください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($target_id_list == "") {
	echo("<script type=\"text/javascript\">alert('担当者を選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 進捗が完了の場合
if ($status == "1") {

	// 更新前の進捗を取得
	$sql = "select work_status from prowork";
	$cond = "where work_id = $work_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 進捗が完了でなかった場合
	if (pg_fetch_result($sel, 0, "work_status") != "1") {

		// ログインユーザの職員IDを取得
		$sql = "select emp_id from session";
		$cond = "where session_id = '$session'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$emp_id = pg_fetch_result($sel, 0, "emp_id");

		// ログインユーザがタスク登録者かどうかチェック
		$sql = "select count(*) from prowork";
		$cond = "where work_id = $work_id and emp_id = '$emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$self_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

		// ログインユーザが委員会責任者かどうかチェック
		$sql = "select count(*) from project";
		$cond = "where pjt_id = $pjt_id and pjt_response = '$emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$response_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

		// ログインユーザが事務局メンバーかどうかチェック
		$sql = "select count(*) from promember";
		$cond = "where pjt_id = $pjt_id and member_kind = '1' and emp_id = '$emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$secretariat_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

		if (!$self_flg && !$response_flg && !$secretariat_flg) {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('進捗を完了に変更できるのは、タスク登録者、委員会責任者、事務局メンバーのみです。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
	}
}

// トランザクションの開始
pg_query($con, "begin");

// タスク情報を更新
$sql = "update prowork set";
$set = array("work_name", "work_status", "work_start_date", "work_expire_date", "work_priority", "work_detail", "work_update");
$setvalue = array($work_name, $status, $start_date, $end_date, $priority, $detail, date("Ymd"));
$cond = "where work_id = $work_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// タスク担当者情報を削除
$sql = "delete from proworkmem";
$cond = "where work_id = $work_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// タスク担当者情報を登録
$target_ids = split(",", $target_id_list);
foreach ($target_ids as $target_id) {
	$sql = "insert into proworkmem (work_id, emp_id) values (";
	$content = array($work_id, $target_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// タスク一覧画面を再表示
if ($wherefrom == "1") {
	echo("<script type=\"text/javascript\">location.href = 'work_menu.php?session=$session&date=$date&mode=$mode';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'project_task_menu.php?session=$session&pjt_id=$pjt_id&date=$date&scale=$scale&mode=$mode&adm_flg=$adm_flg';</script>");
}
?>
</body>
