<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require("about_session.php");
require("about_authority.php");
require("label_by_profile_type.ini");
require_once("summary_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 57, $fname);  // メドレポート
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$checkauth = check_authority($session, 33, $fname);  // 入院来院履歴
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者参照権限・病床管理権限・来院登録権限を取得
$out_reg_auth = check_authority($session, 35, $fname);

// データベースに接続
$con = connect2db($fname);

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", $pt_id);


// 患者情報を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 来院履歴情報を取得
//$sql = "select *, (select sect_nm from sectmst where sectmst.enti_id = outhist.enti_id and sectmst.sect_id = outhist.outpt_sect_cd and sectmst.sect_del_flg = 'f') as sect_nm, (select dr_nm from drmst where drmst.enti_id = outhist.enti_id and drmst.sect_id = outhist.outpt_sect_cd and drmst.dr_id = outhist.dr_id and drmst.dr_del_flg = 'f') as dr_nm, (select enti_nm from entimst where entimst.enti_id = outhist.enti_id and entimst.enti_del_flg = 'f') as enti_nm from outhist";
//$cond = "where ptif_id = '$pt_id' order by outpt_out_dt desc";


$sql =
	" select".
	" hist.*".
	",sect.sect_nm".
	",dr.dr_nm".
	",enti.enti_nm".
	",emp.emp_lt_nm".
	",emp.emp_ft_nm".
	",job.job_nm".
	" from outhist hist".
	" left outer join sectmst sect on (".
	"   sect.enti_id = hist.enti_id".
	"   and sect.sect_id = hist.outpt_sect_cd".
	"   and sect.sect_del_flg = 'f'".
	" )".
	" left outer join drmst dr on (".
	"   dr.enti_id = hist.enti_id".
	"   and dr.sect_id = hist.outpt_sect_cd".
	"   and dr.dr_id = hist.dr_id".
	"   and dr.dr_del_flg = 'f'".
	" )".
	" left outer join entimst enti on (".
	"   enti.enti_id = hist.enti_id".
	"   and enti.enti_del_flg = 'f'".
	" )".
	" left outer join empmst emp on (".
	"   trim(emp.emp_id) = hist.outpt_emp_id". // trim()をしないとdemo環境でエラー(データ型が違うため？）
	"   and enti.enti_del_flg = 'f'".
	" )".
	" left outer join jobmst job on (".
	"   job.job_id = emp.emp_job".
	" )".
	" where hist.ptif_id = '$pt_id'".
	" order by hist.outpt_out_dt desc";
$sel_outhist = select_from_table($con, $sql, "", $fname);
if ($sel_outhist == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}

// 入院履歴情報を取得
$sql = "select i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.inpt_disease, (select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, (select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm, (select e.enti_nm from entimst e where e.enti_id = i.inpt_enti_id and e.enti_del_flg = 'f') as enti_nm from inpthist i where i.ptif_id = '$pt_id' union select i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, i.inpt_out_tm, i.inpt_disease, (select s.sect_nm from sectmst s where s.enti_id = i.inpt_enti_id and s.sect_id = i.inpt_sect_id and s.sect_del_flg = 'f') as sect_nm, (select d.dr_nm from drmst d where d.enti_id = i.inpt_enti_id and d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id and d.dr_del_flg = 'f') as dr_nm, (select e.enti_nm from entimst e where e.enti_id = i.inpt_enti_id and e.enti_del_flg = 'f') as enti_nm from inptmst i where i.ptif_id = '$pt_id' and i.inpt_res_flg = 'f'";
$cond = "order by inpt_in_dt desc, inpt_in_tm desc";
$sel_inpthist = select_from_table($con, $sql, $cond, $fname);
if ($sel_inpthist == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}

// 当該患者が現在入院中か否かチェック
$sql = "select count(*) from inptmst";
$cond = "where ptif_id = '$pt_id' and (inpt_in_flg = 't' or inpt_out_res_flg = 't')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$in_bed_flg = (pg_fetch_result($sel, 0, 0) > 0);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者/利用者
$patient_title = $_label_by_profile["PATIENT"][$profile_type];

// 来院/来所
$come_title = $_label_by_profile["COME"][$profile_type];
// 入院/入所
$in_title = $_label_by_profile["IN"][$profile_type];
// 退院/退所
$out_title = $_label_by_profile["OUT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];
?>
<title>CoMedix <?
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($_label_by_profile["MED_REPORT"][$profile_type]); ?> | <?
// 入院来院履歴/入所来所履歴
echo ($_label_by_profile["IN_OUT"][$profile_type]); ?></title>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body>

<? $pager_param_addition = "&page=".@$page."&key1=".@$key1."&key2=".@$key2; ?>

<? summary_common_show_sinryo_top_header($session, $fname, $_label_by_profile["IN_OUT"][$profile_type], "", $pt_id); ?>

<? summary_common_show_report_tab_menu($con, $fname, $_label_by_profile["IN_OUT"][$profile_type], 0, $pt_id, $pager_param_addition); ?>



<table class="prop_table" cellspacing="1">
	<tr height="22">
		<th width="100px"><? echo ($patient_title); ?>ID</th>
		<td width="100px" style="text-align:center"><? echo($pt_id); ?></td>
		<th width="100px"><? echo ($patient_title); ?>氏名</th>
		<td width=""><? echo("$lt_kj_nm $ft_kj_nm"); ?></td>
	</tr>
</table>


<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin-top:16px; margin-bottom:2px; letter-spacing:1px; width:100px"><? echo($come_title); ?>履歴一覧</div>



<table class="listp_table" cellspacing="1">
<tr>
<th width="140"><? echo($come_title); ?>日時</th>
<th width="140"><? echo($section_title); ?></th>
<th width="140">記載者</th>
<th>職種</th>
</tr>

<?
while ($row = pg_fetch_array($sel_outhist)) {
	$out_dt = format_date($row["outpt_out_dt"]);
	$out_tm = format_time($row["outpt_out_tm"]);
	$out_dttm = "$out_dt $out_tm";
	$sect_nm = $row["sect_nm"];
	$dr_nm = $row["dr_nm"];
	$enti_nm = $row["enti_nm"];

	if ($dr_nm==""){
		$dr_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
	}
?>
<tr>
<td><? echo $out_dttm; ?></td>
<td><? echo $sect_nm; ?></td>
<td><? echo $dr_nm; ?></td>
<td><?=@$row["job_nm"]?></td>
</tr>
<? } ?>
</table>


<? if ($out_reg_auth == 1) { ?>
<div class="search_button_div">
	<input type="button" onclick="window.open('outpatient_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');" value="登録"/>
</div>
<? } ?>




<div style="background-color:#f8fdb7; border:1px solid #c5d506; color:#889304; padding:1px; text-align:center; margin-top:16px; margin-bottom:2px; letter-spacing:1px; width:100px"><? echo($in_title); ?>履歴一覧</div>




<table class="listp_table" cellspacing="1">
<tr>
<th width="140"><? echo($in_title); ?>日時</th>
<th width="140"><? echo($out_title); ?>日時</th>
<th width="140"><? echo($section_title); ?></th>
<th width="140">プロブレム</th>
<th><? echo($doctor_title); ?></th>
</tr>

<?
while ($row = pg_fetch_array($sel_inpthist)) {
	$in_dt = format_date($row["inpt_in_dt"]);
	$in_tm = format_time($row["inpt_in_tm"]);
	$in_dttm = "$in_dt $in_tm";
	$out_dt = format_date($row["inpt_out_dt"]);
	$out_tm = format_time($row["inpt_out_tm"]);
	$out_dttm = "$out_dt $out_tm";
	$sect_nm = $row["sect_nm"];
	$disease = $row["inpt_disease"];
	$dr_nm = $row["dr_nm"];
	$enti_nm = $row["enti_nm"];
?>
<tr>
<td><? echo $in_dttm; ?></td>
<td><? echo $out_dttm; ?></td>
<td><? echo $sect_nm; ?></td>
<td><? echo $disease; ?></td>
<td><? echo $dr_nm; ?></td>
</tr>
<?
}
?>
</table>


</body>
<? pg_close($con); ?>
</html>
<?
//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み日付取得
// Name      :  format_date
// Arguments :  dt - 日付（「YYYYMMDD」形式）
// Return    :  日付（「YYYY/MM/DD」形式）
//------------------------------------------------------------------------------
function format_date($dt) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})$/';
	$replacement = '$1/$2/$3';
	return preg_replace($pattern, $replacement, $dt);
}

//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み時刻取得
// Name      :  format_time
// Arguments :  tm - 時刻（「HHMM」形式）
// Return    :  時刻（「HH:MM」形式）
//------------------------------------------------------------------------------
function format_time($tm) {
	$pattern = '/^(\d{2})(\d{2})$/';
	$replacement = '$1:$2';
	return preg_replace($pattern, $replacement, $tm);
}
?>
