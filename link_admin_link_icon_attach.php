<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix リンクライブラリ｜アイコン登録</title>
<?php
/*************************************************************************
リンクライブラリ管理
  リンク名表示設定・アイコン登録画面
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$checkauth = check_authority($session, 61, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="360" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="280" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>アイコン登録</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<form action="link_admin_link_icon_attach_save.php" method="post" enctype="multipart/form-data">
<p><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アイコンファイル：<input type="file" name="file" size="25"></font></p>
<p><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※登録できるファイルの種類は gif, jpg, png のみ。</font></p>
<p><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※アイコン画像の大きさは最大32ピクセルx32ピクセルまで。</font></p>
<p><input type="submit" value="登録"></p>
</center>
<input type="hidden" name="link_id" value="<? eh($link_id); ?>">
<input type="hidden" name="session" value="<? eh($session); ?>">
</form>
</body>
</html>