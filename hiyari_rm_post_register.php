<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
    showLoginPage();
    exit;
}


// データベースに接続
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//======================================
// 組織階層情報取得
//======================================
$arr_class_name = get_class_name_array($con, $fname);

$classes_nm = $arr_class_name["class_nm"]."＞".$arr_class_name["atrb_nm"]."＞".$arr_class_name["dept_nm"];
if($arr_class_name["class_cnt"] == 4) {
    $classes_nm .= "＞".$arr_class_name["room_nm"];
}

//======================================
// 部門情報取得
//======================================
$sql = "select a.class_id, a.class_nm, a.order_no from classmst a";
$cond = "where a.class_del_flg = 'f' order by a.order_no asc";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$classes = array();
while ($row_class = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row_class["class_id"];
    $classes[$tmp_class_id][0][0][0]["name"] = $row_class["class_nm"];
    $classes[$tmp_class_id][0][0][0]["order"] = $row_class["order_no"];
    $classes[$tmp_class_id][0][0][0]["is_open"] = false;

    $sql = "select a.atrb_id, a.atrb_nm, a.order_no from atrbmst a";
    $cond = "where a.class_id = $tmp_class_id and a.atrb_del_flg = 'f' order by a.order_no asc";
    $sel_atrb = select_from_table($con, $sql, $cond, $fname);
    if ($sel_atrb == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row_atrb = pg_fetch_array($sel_atrb)) {
        $tmp_atrb_id = $row_atrb["atrb_id"];
        $classes[$tmp_class_id][$tmp_atrb_id][0][0]["name"] = $row_atrb["atrb_nm"];
        $classes[$tmp_class_id][$tmp_atrb_id][0][0]["order"] = $row_atrb["order_no"];
        $classes[$tmp_class_id][$tmp_atrb_id][0][0]["is_open"] = false;

        $sql = "select a.dept_id, a.dept_nm, a.order_no from deptmst a";
        $cond = "where a.atrb_id = $tmp_atrb_id and a.dept_del_flg = 'f' order by a.order_no asc";
        $sel_dept = select_from_table($con, $sql, $cond, $fname);
        if ($sel_dept == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row_dept = pg_fetch_array($sel_dept)) {
            $tmp_dept_id = $row_dept["dept_id"];
            $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][0]["name"] = $row_dept["dept_nm"];
            $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][0]["order"] = $row_dept["order_no"];
            $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][0]["is_open"] = false;

            if ($arr_class_name["class_cnt"] == 4) {
                $sql = "select a.room_id, a.room_nm, a.order_no from classroom a";
                $cond = "where a.dept_id = $tmp_dept_id and a.room_del_flg = 'f' order by a.order_no asc";
                $sel_room = select_from_table($con, $sql, $cond, $fname);
                if ($sel_room == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                while ($row_room = pg_fetch_array($sel_room)) {
                    $tmp_room_id = $row_room["room_id"];
                    $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][$tmp_room_id]["name"] = $row_room["room_nm"];
                    $classes[$tmp_class_id][$tmp_atrb_id][$tmp_dept_id][$tmp_room_id]["order"] = $row_room["order_no"];
                }
            }
        }
    }
}

//======================================
// CSV取り込みの登録済みデータ削除オプション
//======================================
//デフォルトは、削除しない
if ($del_option == "") {$del_option = "1";}

//======================================
// RM・RA・SD・MD・HD情報取得
//======================================
$post_info = array();
if ($del_option == "1"){
    $sql = "select a.class_id, a.attribute_id, a.dept_id, a.room_id, a.disp_index,a.emp_id, b.emp_lt_nm || ' ' || b.emp_ft_nm as emp_full_nm,".
           "c.order_no as class_order,".
           "case when d.order_no is null then 0 else d.order_no end as atrb_order,".
           "case when e.order_no is null then 0 else e.order_no end as dept_order,".
	       "case when f.order_no is null then 0 else f.order_no end as room_order".
           " from inci_rm a".
           " inner join empmst b on a.emp_id = b.emp_id".
           " left join classmst c on a.class_id = c.class_id".
           " left join atrbmst d on a.attribute_id = d.atrb_id".
           " left join deptmst e on a.dept_id = e.dept_id".
           " left join classroom f on a.room_id = f.room_id";
    $cond = "where a.post_id = $post_id";
    if($arr_class_name["class_cnt"] == 3) {
        $cond .= " and a.room_id is null";
    }
    $cond .= " order by class_order, atrb_order, dept_order, room_order, a.disp_index";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
   
    while ($row = pg_fetch_array($sel)) {
        $class_id = $row["class_id"];
        $attribute_id = ($row["attribute_id"] == null) ? 0 : $row["attribute_id"];
        $dept_id = ($row["dept_id"] == null) ? 0 : $row["dept_id"];
        $room_id = ($row["room_id"] == null) ? 0 : $row["room_id"];
        $disp_index = $row["disp_index"];
        $emp_id = $row["emp_id"];
        $emp_full_nm = $row["emp_full_nm"];

        $temp =  array("class_id" => $class_id,
                        "attribute_id" => $attribute_id,
                        "dept_id" => $dept_id,
                        "room_id" => $room_id,
                        "disp_index" => $disp_index,
                        "emp_id" => $emp_id,
                        "emp_full_nm" => $emp_full_nm);
        
        //部署の表示順をキーにして配列に保存
        $class_order = $row["class_order"];
        $atrb_order = $row["atrb_order"];
        $dept_order = $row["dept_order"];
        $room_order = $row["room_order"];
        $post_info[$class_order][$atrb_order][$dept_order][$room_order][] = $temp;
    }
}

//======================================
// RM・RA・SD・MD・HD権限名称情報取得
//======================================
$arr_cond = array("RM","RA","MD","SD","HD");
$auth_list = get_inci_mst($con, $fname, $arr_cond);

if($post_id == "1")
{
    $auth_name = $auth_list["RM"]["auth_name"];
}
else if($post_id == "2")
{
    $auth_name = $auth_list["RA"]["auth_name"];
}
else if($post_id == "3")
{
    $auth_name = $auth_list["MD"]["auth_name"];
}
else if($post_id == "4")
{
    $auth_name = $auth_list["SD"]["auth_name"];
}
else
{
    $auth_name = $auth_list["HD"]["auth_name"];
}

//======================================
//CSV取り込み
//======================================
// 文字コードのデフォルトは、Shift_JIS
if ($encoding == "") {$encoding = "1";}

$import = array();
if (isset($csvfile)){
    $error_message = '';

    // ファイルが正常にアップロードされたか
    $uploaded = false;
    if (array_key_exists("csvfile", $_FILES)) {
        if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
            $uploaded = true;
        }
    }
    if (!$uploaded) {
        $error_message = $csvfile . "\\nファイルのアップロードに失敗しました。";
    }

    // 文字コード
    switch ($encoding) {
    case "1":
        $file_encoding = "SJIS";
        break;
    case "2":
        $file_encoding = "EUC-JP";
        break;
    case "3":
        $file_encoding = "UTF-8";
        break;
    default:
        exit;
    }
    
    // EUC-JPで扱えない文字列は「・」に変換するよう設定
    mb_substitute_character(0x30FB);
    
    // CSVデータを配列に取り込み
    if ($uploaded) {
        //カラム数（部署数 + 職員ID分の1列）
        $column_count = $arr_class_name["class_cnt"] + 1;

        $lines = file($_FILES["csvfile"]["tmp_name"]);
        foreach ($lines as $index => $line) {
            // 文字コードを変換
            $line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

            // EOFを削除
            $line = str_replace(chr(0x1A), "", $line);

            // 空行は無視
            if ($line == "") {
                continue;
            }

            // カラム数チェック
            $info = split(",", $line);
            if (count($info) != $column_count) {
                $error_message = 'CSVデータのカラム数が不正です。\\n';
                break;
            }

            //レコード番号
            $record_no = $index + 1;

            //取り込みデータ
            $class_name = trim($info[0]);
            $atrb_name = trim($info[1]);
            $dept_name = trim($info[2]);
            if ($arr_class_name["class_cnt"] == 3){
                $room_name = '';
                $emp_personal_id = trim($info[3]);
            }
            else{
                $room_name = trim($info[3]);
                $emp_personal_id = trim($info[4]);
            }

            //部署（class）チェック
            $class_id = 0;
            if ($class_name != ''){
                foreach ($classes as $key => $class){
                    if ($classes[$key][0][0][0]['name'] == $class_name){
                        $class_id = $key;
                        break;
                    }
                }
            }
            if ($class_id === 0){
                $error_message .= 'レコード番号：' . $record_no . '　' . $arr_class_name["class_nm"]. '名が不正です。「' . $class_name . '」\\n';
                continue;
            }

            //部署（atrb）チェック
            $atrb_id = 0;
            if ($atrb_name != ''){
                foreach ($classes[$class_id] as $key => $atrb){
                    if ($key == 0){continue;}
                    if ($classes[$class_id][$key][0][0]['name'] == $atrb_name){
                        $atrb_id = $key;
                        break;
                    }
                }
            }
            if ($atrb_name != '' || $dept_name != '' || $room_name != ''){
                if ($atrb_id === 0){
                    $error_message .= 'レコード番号：' . $record_no . '　' . $arr_class_name["atrb_nm"]. '名が不正です。「' . $atrb_name . '」\\n';
                    continue;
                }
            }

            //部署（dept）チェック
            $dept_id = 0;
            if ($dept_name != ''){
                foreach ($classes[$class_id][$atrb_id] as $key => $dept){
                    if ($key == 0){continue;}
                    if ($classes[$class_id][$atrb_id][$key][0]['name'] == $dept_name){
                        $dept_id = $key;
                        break;
                    }
                }
            }
            if ($dept_name != '' || $room_name != ''){
                if ($dept_id === 0){
                    $error_message .= 'レコード番号：' . $record_no . '　' . $arr_class_name["dept_nm"]. '名が不正です。「' . $dept_name . '」\\n';
                    continue;
                }
            }

            //部署（room）チェック
            $room_id = 0;
            if ($arr_class_name["class_cnt"] == 4) {
                if ($room_name != ''){
                    foreach ($classes[$class_id][$atrb_id][$dept_id] as $key => $room){
                        if ($key == 0){continue;}
                        if ($classes[$class_id][$atrb_id][$dept_id][$key]['name'] == $room_name){
                            $room_id = $key;
                            break;
                        }
                    }
                    if ($room_id === 0){
                        $error_message .= 'レコード番号：' . $record_no . '　' . $arr_class_name["room_nm"]. '名が不正です。「' . $room_name . '」\\n';
                        continue;
                    }
                }
            }

            //職員IDチェック & 職員名取得
            $sql = "select emp_id, emp_lt_nm || ' ' || emp_ft_nm as emp_name from empmst";
            $cond = "where emp_personal_id = '" . pg_escape_string($emp_personal_id) . "'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $emp_info = pg_fetch_array($sel, 0, PGSQL_ASSOC);
            if (!$emp_info){
                $error_message .= 'レコード番号：' . $record_no . '　' . '職員IDが不正です。「' . $emp_personal_id . '」\\n';
                continue;
            }
            $emp_id = $emp_info['emp_id'];
            $emp_full_nm = $emp_info['emp_name'];

            //部署の表示順をキーにして配列に保存
            $temp = array("class_id" => $class_id,
                            "attribute_id" => $atrb_id,
                            "dept_id" => $dept_id,
                            "room_id" => $room_id,
                            "emp_id" => $emp_id,
                            "emp_full_nm" => $emp_full_nm);
            $class_order = $classes[$class_id][0][0][0]['order'];
            $atrb_order = ($atrb_id == 0 ? 0 : $classes[$class_id][$atrb_id][0][0]['order']);
            $dept_order = ($dept_id == 0 ? 0 : $classes[$class_id][$atrb_id][$dept_id][0]['order']);
            $room_order = ($room_id == 0 ? 0 : $classes[$class_id][$atrb_id][$dept_id][$room_id]['order']);
            $post_info[$class_order][$atrb_order][$dept_order][$room_order][] = $temp;
        }
    }
}

// 部署の表示順に保存した担当者情報から表示用配列を作成
$arr_inci_rm = array();
foreach($post_info as $class){
    foreach($class as $atrb){
        foreach($atrb as $dept){
            foreach($dept as $room){
                foreach($room as $info){
                    //表示用配列
                    $arr_inci_rm[] = $info;

                    //下層の表示フラグ（下層に担当者が入れば表示する）
                    $class_id = $info['class_id'];
                    $atrb_id  = $info['attribute_id'];
                    $dept_id  = $info['dept_id'];
                    $room_id  = $info['room_id'];
                    if ($atrb_id > 0){
                        $classes[$class_id][0][0][0]["is_open"] = true;
                    }
                    if ($dept_id > 0){
                        $classes[$class_id][$atrb_id][0][0]["is_open"] = true;
                    }
                    if ($room_id > 0){
                        $classes[$class_id][$atrb_id][$dept_id][0]["is_open"] = true;
                    }
                }
            }
        }
    }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=h($auth_name)?>登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>

<script type="text/javascript">
<!--

$(function(){
    <?php if (isset($error_message) && $error_message != ''){ ?>
        alert('<?php echo $error_message; ?>');
    <?php } ?>

    $('#csv_btn').click(function(){
        if ($('#csv_path').val() == ''){
            alert('ファイルを選択してください。');
        }
        else{
            $('#csvform').submit();
        }
    });
})

function initPage(){

<?
foreach ($opened_ids as $opened_id) {
    echo("change('$opened_id');\n");
}
?>

<?
$class_id_src = "";
$attribute_id_src = "";
$dept_id_src = "";
$room_id_src = "";
$arr_disp_area = array();
foreach($arr_inci_rm as $row) {

    $class_id_dest = $row["class_id"];
    $attribute_id_dest = $row["attribute_id"];
    $dept_id_dest = $row["dept_id"];
    $room_id_dest = $row["room_id"];

    $disp_area = "";
    $disp_area .= $class_id_dest;

    if($class_id_src != $class_id_dest ||
        $attribute_id_src != $attribute_id_dest ||
        $dept_id_src != $dept_id_dest ||
        $room_id_src != $room_id_dest) {

        if($attribute_id_dest != 0) {
            $disp_area .= "-";
            $disp_area .= $attribute_id_dest;
        }

        if($dept_id_dest != 0) {
            $disp_area .= "-";
            $disp_area .= $dept_id_dest;
        }

        if($room_id_dest != 0) {
            $disp_area .= "-";
            $disp_area .= $room_id_dest;
        }
        array_push($arr_disp_area, $disp_area);
    }

    $class_id_src = $class_id_dest;
    $attribute_id_src = $attribute_id_dest;
    $dept_id_src = $dept_id_dest;
    $room_id_src = $room_id_dest;
}
?>

<?
$class_id_src = "";
$attribute_id_src = "";
$dept_id_src = "";
$room_id_src = "";
$disp_area_content = "";
$arr_disp_area_content = array();
$head_hidden_content = "";
$arr_head_hidden_content = array();
$head_nm_hidden_content = "";
$arr_head_nm_hidden_content = array();
$idx = 0;
$max_row = count($arr_inci_rm);
foreach($arr_inci_rm as $row) {

    $class_id_dest = $row["class_id"];
    $attribute_id_dest = $row["attribute_id"];
    $dept_id_dest = $row["dept_id"];
    $room_id_dest = $row["room_id"];

    $element_id = $class_id_dest;
    if($attribute_id_dest != "") {
        $element_id .= "-";
        $element_id .= $attribute_id_dest;
    }
    if($dept_id_dest != "") {
        $element_id .= "-";
        $element_id .= $dept_id_dest;
    }
    if($room_id_dest != "") {
        $element_id .= "-";
        $element_id .= $room_id_dest;
    }


    if($class_id_src == $class_id_dest &&
        $attribute_id_src == $attribute_id_dest &&
        $dept_id_src == $dept_id_dest &&
        $room_id_src == $room_id_dest) {

        $disp_area_content .= ",";
        $head_hidden_content .= ",";
        $head_nm_hidden_content .= ",";
    } else {
        if($idx > 0) {
            array_push($arr_disp_area_content, $disp_area_content);
            array_push($arr_head_hidden_content, $head_hidden_content);
            array_push($arr_head_nm_hidden_content, $head_nm_hidden_content);
            $disp_area_content = "";
            $head_hidden_content = "";
            $head_nm_hidden_content = "";
        }
    }

    $emp_id = $row["emp_id"];
    $emp_full_nm = $row["emp_full_nm"];

    $disp_area_content .= "<a href=\"javascript:delete_rm(\\'{$emp_id}\\', \\'{$element_id}\\', \\'{$emp_full_nm}\\');\">{$emp_full_nm}</a>";
    $head_hidden_content .= $emp_id;
    $head_nm_hidden_content .= $emp_full_nm;

    $idx++;

    if($idx == $max_row) {
        array_push($arr_disp_area_content, $disp_area_content);
        array_push($arr_head_hidden_content, $head_hidden_content);
        array_push($arr_head_nm_hidden_content, $head_nm_hidden_content);
    }

    $class_id_src = $class_id_dest;
    $attribute_id_src = $attribute_id_dest;
    $dept_id_src = $dept_id_dest;
    $room_id_src = $room_id_dest;


}
?>


<?
for($i=0; $i<count($arr_disp_area); $i++) {
?>
if(document.getElementById('disp_area_<?echo($arr_disp_area[$i]);?>'))
{
    document.getElementById('disp_area_<?echo($arr_disp_area[$i]);?>').innerHTML = '<?echo($arr_disp_area_content[$i]);?>';
}
if(document.getElementById('head_<?echo($arr_disp_area[$i]);?>'))
{
    document.getElementById('head_<?echo($arr_disp_area[$i]);?>').value = '<?echo($arr_head_hidden_content[$i]); ?>';
}
if(document.getElementById('head_nm_<?echo($arr_disp_area[$i]);?>'))
{
    document.getElementById('head_nm_<?echo($arr_disp_area[$i]);?>').value = '<?echo($arr_head_nm_hidden_content[$i]);?>';
}
<?
}
?>


}

function changeDisplay(img) {
    img.className = (img.className == 'close') ? 'open' : 'close';
    var row_id = img.id.replace('img', 'tr');
    setChildDisplay(row_id);
}


function change(class_id) {

    var img = document.getElementById('img'.concat(class_id));
    img.className = 'open';

    var row_id = img.id.replace('img', 'tr');
    setChildDisplay(row_id);
}


function is_parent_closed(id)//例）id=1-1-1-1
{

    var ids = id.split("-");
    if(ids.length == 1)
    {
        return false;
    }
    else
    {
        var target_id = "";
        for(var i=0; i<ids.length - 1; i++)
        {
            if(target_id != ""){target_id = target_id + "-";}
            target_id = target_id + ids[i];
            if( is_closed(target_id) )
            {
                return true;
            }
        }
    }
}
function is_closed(id)
{
    var a =  document.getElementById('tr'.concat(id)).style.display == 'none';
    var b =  document.getElementById('img'.concat(id)).className == 'close';
    return a || b;
}




function setChildDisplay(parent_row_id) {
    var img = document.getElementById(parent_row_id.replace('tr', 'img'));
    if (!img) {
        return;
    }

    var rows = document.getElementById('classlist').rows;
    for (var i = 0, j = rows.length; i < j; i++) {

        var class_names = rows[i].className.split(' ');
        if (img.className == 'open') {
            if (class_names[0] == parent_row_id) {
                if(!is_parent_closed(parent_row_id.replace('tr', ''))) {
                rows[i].style.display = '';
                setChildDisplay(rows[i].id);
            }
            }
        } else {
            for (var k = 0, l = class_names.length; k < l; k++) {
                if (class_names[k] == parent_row_id) {
                    rows[i].style.display = 'none';
                    break;
                }
            }
        }
    }
}

function regist() {

    var images = document.getElementsByTagName('img');
    var form = document.getElementById('mainform');
    for (var i = 0, j = images.length; i < j; i++) {
        if (images[i].className == 'open') {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'opened_ids[]';
            input.value = images[i].id.replace('img', '');
            form.appendChild(input);
        }
    }
    document.mainform.action="hiyari_rm_post_insert.php";
    document.mainform.submit();

}

//--------------------------------------------------
//職員名簿画面
//--------------------------------------------------
function call_emp_search(input_div)
{
    var childwin = null;

    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = 'hiyari_emp_list.php';
    url += '?session=<?=$session?>&input_div=' + input_div;
    childwin = window.open(url, 'emplist', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function selectHead(element_id) {
    call_emp_search(element_id);
}

//子画面から呼ばれる関数
function add_emp_list(set_emp_id, set_emp_nm, element_id) {
    set_emp_id = set_emp_id.replace(/, /g, ',');
    set_emp_nm = set_emp_nm.replace(/, /g, ',');

    head = "head_" + element_id;
    head_nm = "head_nm_" + element_id;

    emp_id = document.getElementById(head).value;
    emp_nm = document.getElementById(head_nm).value;

    if(emp_id != "") {
        arr_emp_id = emp_id.split(",");
        for(i=0; i<arr_emp_id.length; i++) {
            if(arr_emp_id[i] == set_emp_id) {
                return;
            }
        }
    }

    if(emp_id != "") {
        emp_id += ",";
    }
    emp_id += set_emp_id;
    document.getElementById(head).value = emp_id;
    if(emp_nm != "") {
        emp_nm += ",";
    }
    emp_nm += set_emp_nm;
    document.getElementById(head_nm).value = emp_nm;

    set_disp_area(emp_id, emp_nm, element_id);
}

function set_disp_area(emp_id, emp_nm, element_id) {

    disp_area = "disp_area_" + element_id;

    arr_emp_id = emp_id.split(",");
    arr_emp_nm = emp_nm.split(",");
    disp_area_content = "";
    for(i=0; i<arr_emp_id.length; i++) {

        if(i > 0) {
            disp_area_content += ',';
        }

        disp_area_content += "<a href=\"javascript:delete_rm('" + arr_emp_id[i] + "', '" + element_id + "', '" + arr_emp_nm[i] + "');\">";
        disp_area_content += arr_emp_nm[i];
        disp_area_content += "</a>";
    }
    document.getElementById(disp_area).innerHTML = disp_area_content;

}


function delete_rm(del_emp_id, element_id, del_emp_nm) {

    if(confirm("「" + del_emp_nm + "」を<?=$auth_name?>から削除します。よろしいですか？")) {

        head = "head_" + element_id;
        head_nm = "head_nm_" + element_id;

        emp_id = document.getElementById(head).value;
        emp_nm = document.getElementById(head_nm).value;

        new_arr_emp_id = "";
        new_arr_emp_nm = "";

        cnt = 0;
        if(emp_id != "") {

            arr_emp_id = emp_id.split(",");
            arr_emp_nm = emp_nm.split(",");

            for(i=0; i<arr_emp_id.length; i++) {
                if(arr_emp_id[i] == del_emp_id) {
                        continue;
                } else {
                    if(cnt > 0) {
                        new_arr_emp_id += ",";
                        new_arr_emp_nm += ",";

                    }
                        new_arr_emp_id += arr_emp_id[i];
                        new_arr_emp_nm += arr_emp_nm[i];
                    cnt++;
                }
            }
        }

        document.getElementById(head).value = new_arr_emp_id;
        document.getElementById(head_nm).value = new_arr_emp_nm;
        set_disp_area(new_arr_emp_id, new_arr_emp_nm, element_id);
    }
}
//-->

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#35B341 solid 1px;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

img.close {
    background-image:url("img/icon/plus.gif");
    vertical-align:middle;
}
img.open {
    background-image:url("img/icon/minus.gif");
    vertical-align:middle;
}

#csvform{
    background-color: #F5FFE5;
    padding-bottom: 1em;
}
#csvform table,#csvform th,#csvform td{
    border:#35B341 solid 1px;
}
#csvform table {
    border-collapse:collapse;
}
#csvform th{
    width:130;
    background-color: #DFFFDC;
    text-align: left;
    font-weight: normal;
}
#csvform td{
    background-color: #FFFFFF;
}
#csv_action{
    padding-top: 3px;
    text-align: right;
}
#csv_path{
    width:300px;
}
#csvform ul{
    margin:0;
}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<!-- ヘッダー START -->
<?
$arr_post_id = array("post_id" => $post_id);
show_hiyari_header($session,$fname,true,$arr_post_id);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>
<form id="csvform" name="csvform" action="#" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <th>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font>
            </th>
            <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="file" id="csv_path" name="csvfile" value="">
                </font>
            </td>
        </tr>
        <tr>
            <th>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録済みの担当者</font>
            </th>
            <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="radio" name="del_option" value="1"<? if ($del_option == "1") {echo(" checked");} ?>>残す
                    <input type="radio" name="del_option" value="2"<? if ($del_option == "2") {echo(" checked");} ?>>削除する
                </font>
            </td>
        </tr>
        <tr>
            <th>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font>
            </th>
            <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
                    <input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
                    <input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
                </font>
            </td>
        </tr>
        <tr>
            <th>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVのフォーマット</font>
            </th>
            <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    下記項目をカンマで区切り、レコードごとに改行します。
                    <ul>
                        <li><? echo $arr_class_name["class_nm"];?></li>
                        <li><? echo $arr_class_name["atrb_nm"];?></li>
                        <li><? echo $arr_class_name["dept_nm"];?></li>
                        <?if($arr_class_name["class_cnt"] == 4) {?>
                            <li><?echo $arr_class_name["room_nm"];?></li>
                        <?}?>
                        <li>職員ID</li>
                    </ul>
                    <?if($arr_class_name["class_cnt"] == 3) {?>
                        例：ファントル病院,看護部,第一病棟,rm1
                    <?}else{?>
                        例：ファントル病院,看護部,第一病棟,Aチーム,id1
                    <?}?>
                </font>
            </td>
        </tr>
    </table>
    <div id="csv_action">
        <input type="button" id="csv_btn" value="取込">
    </div>
    <input type="hidden" name="session" value="<? echo($session); ?>">
    <input type="hidden" name="post_id" value="<? echo($post_id); ?>">
</form>

<form id="mainform" name="mainform" action="#" method="post">
    <table id="classlist" width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr height="26" bgcolor="#DFFFDC">
            <td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属(<?echo($classes_nm);?>)</font></td>
            <td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($auth_name)?></font></td>
        </tr>

        <? foreach ($classes as $tmp_class_id => $tmp_atrbs) { ?>
            <tr height="26" id="tr<? echo($tmp_class_id); ?>">
                <td>
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <img id="img<? echo($tmp_class_id); ?>" src="img/spacer.gif" alt="" width="20" height="20"
                             <?if ($tmp_atrbs[0][0][0]['is_open']){?>
                                class="open"
                             <?}else{?>
                                class="close"
                             <?}?>
                             style="margin-right:3px;" onclick="changeDisplay(this)">
                        <? echo($tmp_atrbs[0][0][0]["name"]); ?>
                    </font>
                </td>
                <td>
                    <table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
                        <tr>
                            <td width="400" style="border:#35B341 solid 1px;">
                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo($tmp_class_id); ?>"></span></font>
                            </td>
                            <td><input type="button" value="設定" onclick="selectHead('<? echo($tmp_class_id); ?>')"></td>
                            <input type="hidden" name="head_<? echo($tmp_class_id); ?>" value="" id="head_<? echo($tmp_class_id); ?>">
                            <input type="hidden" name="head_nm_<? echo($tmp_class_id); ?>" value="" id="head_nm_<? echo($tmp_class_id); ?>">
                        </tr>
                    </table>
                </td>
            </tr>

            <? if (count($tmp_atrbs) == 1) { ?>
                <tr class="tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
                    <td colspan="5" style="padding-left:50px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
                </tr>
            <? } ?>

            <? foreach ($tmp_atrbs as $tmp_atrb_id => $tmp_depts) { ?>
                <? if ($tmp_atrb_id == "0") {continue;} ?>

                <tr class="tr<? echo($tmp_class_id); ?>" 
                    id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>"
                    <?if (!$tmp_atrbs[0][0][0]['is_open']){?>
                        style="display:none;"
                    <?}?>
                    height="26">
                    <td style="padding-left:30px;">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <img id="img<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" src="img/spacer.gif" alt="" width="20" height="20" 
                                <?if ($tmp_depts[0][0]["is_open"]){?>
                                    class="open"
                                <?}else{?>
                                    class="close"
                                <?}?>
                                style="margin-right:3px;" onclick="changeDisplay(this)">
                            <? echo($tmp_depts[0][0]["name"]); ?>
                        </font>
                    </td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
                            <tr>
                                <td width="400" style="border:#35B341 solid 1px;">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>"></span></font>
                                </td>
                                <td><input type="button" value="設定" onclick="selectHead('<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>');"></td>
                                <input type="hidden" name="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" value="" id="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>">
                                <input type="hidden" name="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>" value="" id="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?>">
                            </tr>
                        </table>
                    </td>
                </tr>

                <? if (count($tmp_depts) == 1) { ?>
                    <tr class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
                        <td colspan="5" style="padding-left:80px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
                    </tr>
                <? } ?>

                <? foreach ($tmp_depts as $tmp_dept_id => $tmp_room) { ?>
                    <? if ($tmp_dept_id == "0") {continue;} ?>

                    <tr class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>"
                        id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>"
                        <?if (!$tmp_depts[0][0]["is_open"]){?>
                            style="display:none;"
                        <?}?>
                        height="26">
                        <td style="padding-left:80px;">
                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                <? if ($arr_class_name["class_cnt"] == 4) { ?>
                                    <img id="img<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>" src="img/spacer.gif" alt="" width="20" height="20"
                                        <?if ($tmp_room[0]["is_open"]){?>
                                            class="open"
                                        <?}else{?>
                                            class="close"
                                        <?}?>
                                         style="margin-right:3px;" onclick="changeDisplay(this)">
                                <?}?>
                                <? echo($tmp_room[0]["name"]); ?>
                            </font>
                        </td>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
                            <tr>
                                <td width="400" style="border:#35B341 solid 1px;">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>"></span></font>
                                </td>
                                <td><input type="button" value="設定" onclick="selectHead('<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>');"></td>
                                <input type="hidden" name="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>" value="" id="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>">
                                <input type="hidden" name="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>" value="" id="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?>">
                            </tr>
                            </table>
                        </td>
                    </tr>

                    <?
                    // 4階層指定の場合
                    if ($arr_class_name["class_cnt"] == 4) {
                    ?>
                        <? if (count($tmp_room) == 1) { ?>
                            <tr class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?> tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>" height="26" style="display:none;">
                                <td colspan="5" style="padding-left:110px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">（未登録）</font></td>
                            </tr>
                        <? } ?>

                        <? foreach ($tmp_room as $tmp_room_id => $tmp_data) { ?>
                            <?  if ($tmp_room_id == "0") {continue;} ?>

                            <tr class="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"); ?> tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}"); ?> tr<? echo($tmp_class_id); ?>"
                                id="tr<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>" 
                                <?if (!$tmp_room[0]["is_open"]){?>
                                    style="display:none;"
                                <?}?>
                                height="26">
                                <td style="padding-left:110px;">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <? echo($tmp_data["name"]); ?>
                                    </font>
                                </td>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="2" class="non_in_list">
                                    <tr>
                                        <td width="400" style="border:#35B341 solid 1px;">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="disp_area_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>"></span></font>
                                        </td>
                                        <td><input type="button" value="設定" onclick="selectHead('<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>');"></td>
                                        <input type="hidden" name="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>" value="" id="head_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>">
                                        <input type="hidden" name="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>" value="" id="head_nm_<? echo("{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}"); ?>">
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                <? } ?>
            <? } ?>
        <? } ?>
    </table>

    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr height="26" bgcolor="#F5FFE5">
            <td align="right"><input type="button" value="登録" onclick="regist();"></td>
        </tr>
    </table>
    <input type="hidden" name="session" value="<? echo($session); ?>">
    <input type="hidden" name="post_id" value="<? echo($post_id); ?>">
</form>
</td>

<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
</body>
</html>

<?
pg_close($con);
