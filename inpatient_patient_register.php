<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜患者新規登録</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("show_select_years_wareki.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 初期表示時は和暦
if ($back != "t") {
	$wareki_flg = "t";
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function wareki_flg_onchange() {
	if (document.patient.wareki_flg.checked == true) {
		birth_yr_onchange();
		document.getElementById('year_div').style.display = "none";
		document.getElementById('year_wareki_div').style.display = "";
	} else {
		document.getElementById('year_div').style.display = "";
		document.getElementById('year_wareki_div').style.display = "none";
	}
}

<?
// 改元データを取得
$era_data = get_era_data();
$era_ymd_str = "";
$gengo_str = "";
for ($i = 1; $i < count($era_data); $i++) {
	if ($i > 1) {
		$era_ymd_str .= ",";
		$gengo_str .= ",";
	}
	$split_data = split(",", $era_data[$i]);
	$era_ymd_str .= "'".$split_data[0]."'";
	$gengo_str .= "'".$split_data[1]."'";
}
?>
var ymd_arr = new Array(<?=$era_ymd_str?>);
var gengo_arr = new Array(<?=$gengo_str?>);

function birth_yr_onchange() {
	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		if (year == ymd_arr[i].substring(0,4)) {
			find_idx = i;
			break;
		}
	}

	document.patient.birth_yr2.value = year;
	if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
		document.patient.birth_yr2.selectedIndex += 1;
	}
}

function birth_yr2_onchange() {
	document.patient.birth_yr.value = document.patient.birth_yr2.value;
}

function check_wareki() {

	if (document.patient.wareki_flg.checked == false) {
		return true;
	}

	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		ymp_tmp = ymd_arr[i].substring(0,4);
		if (year == ymp_tmp) {
			find_idx = i;
			break;
		}
	}

	if (find_idx == -1) {
		return true;
	}

	if (ymd < ymd_arr[find_idx]) {
		gengo = gengo_arr[find_idx + 1];
	} else {
		gengo = gengo_arr[find_idx];
	}

	obj = document.getElementById("birth_yr2");
	var wareki_tmp = obj.options[obj.selectedIndex].text;
	select_gengo = wareki_tmp.substring(0,2);
	if (select_gengo != gengo) {
		alert('年月日を正しく指定してください');
		return false;
	}
	return true;
}

function change_age() {
	var birth_year = document.patient.birth_yr.value;
	var birth_month = document.patient.birth_mon.value;
	var birth_day = document.patient.birth_day.value;

	var today = new Date();
	var this_year = today.getFullYear();
	var this_month = today.getMonth() + 1;
	var this_day = today.getDate();

	var age = this_year - birth_year;
	if (this_month < birth_month || (this_month == birth_month && this_day < birth_day)) {
		age--;
	}

	document.getElementById('age_label').innerHTML = age + '歳';
}

function insertPatient(path) {
	document.patient.path.value = path;
	document.patient.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="wareki_flg_onchange();change_age();">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>患者新規登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="patient" action="patient_insert.php" method="post" onsubmit="return check_wareki();">
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td colspan="3"><input name="pt_id" type="text" value="<? echo $pt_id; ?>" maxlength="20" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（漢字）</font></td>
<td width="42%"><input name="lt_nm" type="text" value="<? echo $lt_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（漢字）</font></td>
<td width="26%"><input name="ft_nm" type="text" value="<? echo $ft_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（かな）</font></td>
<td><input name="lt_kana_nm" type="text" value="<? echo $lt_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（かな）</font></td>
<td><input name="ft_kana_nm" type="text" value="<? echo $ft_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td>
<span id="year_div">
<select name="birth_yr" onchange="birth_yr_onchange();change_age();">
<? show_select_years(120, $birth_yr);?>
</select></span><span id="year_wareki_div">
<select name="birth_yr2" onchange="birth_yr2_onchange();change_age();">
<? show_select_years_jp(120, $birth_yr2, "", ""); ?>
</select></span><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="birth_mon" onchange="change_age();"><? show_select_months($birth_mon); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="birth_day" onchange="change_age();"><? show_select_days($birth_day); ?></select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="wareki_flg" value="t" <? if ($wareki_flg == "t") echo(" checked"); ?> onclick="wareki_flg_onchange();">和暦</font>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="age_label"></span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td><select name="sex">
<option value="0"<? if ($sex == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($sex == "1") {echo " selected";} ?>>男性
<option value="2"<? if ($sex == "2") {echo " selected";} ?>>女性
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職業</font></td>
<td><input type="text" name="job" value="<? echo $job; ?>" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="4">
<tr height="22">
<td align="right">
<input type="button" value="入院予定登録" onclick="insertPatient('R');">
<input type="button" value="入院登録" onclick="insertPatient('I');">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="path" value="">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
