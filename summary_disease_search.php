<?
/*
画面パラメータ
$session
	セッションID
$search_input
	検索入力文字列。省略時は無検索。
$mode
	1:病名のみ、2：修飾語のみ、省略時は全て
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("summary_common.ini");
require_once("get_menu_label.ini");
require_once("medis_version.ini");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
	exit;
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
	js_login_exit();
	exit;
}

$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

//==============================
//検索処理
//==============================

// 検索文字配列作成
$search_input = isset($_POST["search_input"]) ? $_POST["search_input"] : "";
if ($search_input !== "") {
	$search_input_sql = mb_convert_kana($search_input, "rn");  // r：全角英字→半角、n：全角数字→半角
	$search_input_sql = strtoupper($search_input_sql);  // 小文字→大文字
	$search_input_sql = mb_ereg_replace("　", " ", $search_input_sql);  // 全角スペース除去
	$search_input_sql = trim($search_input_sql);  // 前後スペース除去
	$search_input_sql = preg_replace("/\s+/", " ", $search_input_sql);  // 連続スペース除去
	$search_input_sql_list = mb_split(" ", $search_input_sql);
} else {
	$search_input_sql_list = array();
}

// MEDIS病名検索実行
$list_cnt = count($search_input_sql_list);
if ($list_cnt > 0) {
	$sql  = " select b.byoumei, '1'::varchar(1) as target_table_class, b.icd10, b.rece_code, null as rece_modirier_code, null as position, null as link_code, b.byoumei_kana ";
	$sql .= " from medis_byoumei b ";
	$sql .= " inner join ( ";
	$sql .=   " select i.link_code ";
	$sql .=   " from medis_byoumei_index_org i ";
	$sql .=   " where ( ";

	for ($i = 0; $i < $list_cnt; $i++) {
		$sql .= " (i.index_str like '%" . qlike($search_input_sql_list[$i]) . "%' ";
		$tmp_kana = mb_convert_kana($search_input_sql_list[$i], "C");  // C：全角ひらがな→全角カタカナ
		if ($tmp_kana !== $search_input_sql_list[$i]) {
			$sql .= " or i.index_str like '%" . qlike($tmp_kana) . "%' ";
		}
		$sql .= " ) ";
		if ($i < $list_cnt - 1) {
			$sql .= " and ";
		}
	}

	$sql .=   " ) ";
	$sql .=   " group by i.link_code ";
	$sql .= " ) i ";
	$sql .= " on b.byoumei_code = i.link_code ";
	$sql .= " where b.update_class <> '1' ";
	if ($mode === "2") {
		$sql .= " and false ";
	}

	$sql .= " union ";

	$sql .= " select m.modifier as byoumei, '2'::varchar(1) as target_table_class, null as icd10, null as rece_code, m.rece_modirier_code, m.position, m.link_code, m.modifier_kana as byoumei_kana ";
	$sql .= " from medis_byoumei_modifier m ";
	$sql .= " inner join ( ";
	$sql .=   " select i.link_code ";
	$sql .=   " from medis_byoumei_index i ";
	$sql .=   " where i.target_table_class = '2' ";
	$sql .=   " and ( ";

	for ($i = 0; $i < $list_cnt; $i++) {
		$sql .= " (i.index_str like '%" . qlike($search_input_sql_list[$i]) . "%' ";
		$tmp_kana = mb_convert_kana($search_input_sql_list[$i], "C");  // C：全角ひらがな→全角カタカナ
		if ($tmp_kana !== $search_input_sql_list[$i]) {
			$sql .= " or i.index_str like '%" . qlike($tmp_kana) . "%' ";
		}
		$sql .= " ) ";
		if ($i < $list_cnt - 1) {
			$sql .= " and ";
		}
	}

	$sql .=   " ) ";
	$sql .=   " group by i.link_code ";
	$sql .= " ) i ";
	$sql .= " on m.link_code = i.link_code ";
	$sql .= " where m.update_class <> '1' ";
	if ($mode === "1") {
		$sql .= " and false ";
	}

	$cond = " order by 8 limit 201 ";

	$sel = select_from_table($con, $sql, $cond, $fname);
	$sel_num = pg_num_rows($sel);
} else {
	$sel = null;
	$sel_num = 0;
}

//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜傷病名検索</title>
<script type="text/javascript">
function focusSearchBox() {
	document.main_form.search_input.focus();
}

function selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code) {
	if (opener && !opener.closed && opener.selectDisease) {
		opener.selectDisease(byoumei, icd10, rece_code, target_table_class, position, link_code);
	}
}

function highlightCells(class_name) {
	changeCellsColor(class_name, '#fefc8f');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}
</script>
</head>
<body onload="focusSearchBox();">

<div style="padding:4px">

<? summary_common_show_dialog_header("傷病名検索"); ?>

<form name="main_form" action="summary_disease_search.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<table style="margin-top:8px">
	<tr>
	<td style="padding:2px" width="100"><a href="summary_disease_rank.php?session=<? echo($session); ?>">頻出順</a></td>
	<td style="padding:2px"><b>病名・修飾語検索</b></td>
	</tr>
</table>

<table class="prop_table" style="margin-top:6px">
	<tr>
	<th width="5%" style="padding:3px"><nobr>病名・修飾語</nobr></th>
	<td width="95%" style="padding:3px"><nobr>
		<input type="text" name="search_input" value="<?=h(@$search_input)?>" style="width:250px;ime-mode: active">
		<select name="mode">
		<option value="">全て</option>
		<option value="1"<? if ($mode === "1") {echo(" selected=\"selected\"");} ?>>病名</option>
		<option value="2"<? if ($mode === "2") {echo(" selected=\"selected\"");} ?>>修飾語</option>
		</select>
		<input type="button" onclick="document.main_form.submit();" value="検索"/>
		<span style="color:#1f97f5">MEDIS標準病名マスターV<?=$MEDIS_VERSION?>対応</nobr>
	</td>
	</tr>
</table>

<table class="list_table" style="margin-top:10px">
	<!-- MEDIS一覧HEADER START -->
	<tr>
	<th style="text-align:center"><nobr>病名/修飾語（接頭語、接尾語）</nobr></th>
	<th style="text-align:center" width="120"><nobr>病名修飾語区分</nobr></th>
	</tr>
	<!-- MEDIS一覧HEADER END -->
	<!-- MEDIS一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num; $i++) {
		if($i >= 200) {
			$is_disp_over = true;
			break;
		}

		$byoumei            = pg_fetch_result($sel, $i, "byoumei");
		$icd10              = pg_fetch_result($sel, $i, "icd10");
		$rece_code          = pg_fetch_result($sel, $i, "rece_code");
		$rece_modirier_code = pg_fetch_result($sel, $i, "rece_modirier_code");
		$position           = pg_fetch_result($sel, $i, "position");
		$link_code          = pg_fetch_result($sel, $i, "link_code");
		if (pg_fetch_result($sel, $i, "target_table_class") == "1") {
			$target_table_class = "病名";
			$js_rece_code = $rece_code;
			$js_target_table_class = "1";
		} else {
			$target_table_class = "修飾語";
			$js_rece_code = $rece_modirier_code;
			$js_target_table_class = "2";
		}

		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
		$line_script .= " onclick=\"selectDisease('" . h_jsparam($byoumei) . "', '" . h_jsparam($icd10) . "', '" . h_jsparam($js_rece_code) . "', '" . h_jsparam($js_target_table_class) . "', '" . h_jsparam($position) . "', '" . h_jsparam($link_code) . "')\"";

		$bgcolor = @$bgcolor=="#fff" ? "#f1f7fd" : "#fff";
	?>
	<tr style="background-color:<?=$bgcolor?>">
	<td <?=$line_script?> align="left"  ><nobr><?=h($byoumei)?></nobr></td>
	<td <?=$line_script?> align="center"><nobr><?=h($target_table_class)?></nobr></td>
	</tr>
	<? } ?>
</table>

<? if ($is_disp_over) { ?>
<div>200件以上データがあります。</div>
<? } ?>
<!-- MEDIS一覧DATA END -->

</form>

</div>
</body>
</html>
<? pg_close($con); ?>
