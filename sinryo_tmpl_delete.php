<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//入力チェック
if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$con=connect2db($fname);
if($con==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//共通SQL部品
$in=" in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";

//使用中チェック ･･･診療区分に割り当てられているテンプレートは削除できない。
$tbl = "select * from tmpl_div_link";
$cond = " where tmpl_div_link.tmpl_id $in";
$sel = select_from_table($con,$tbl,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	pg_close($con);
	exit;
}
$num = pg_numrows($sel);
if($num > 0)
{
	echo("<script language='javascript'>alert(\"診療区分に割り当てられているテンプレートは削除できません。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	pg_close($con);
	exit;
}

//削除処理
$delete_in = "where tmpl_id".$in;
$delete_status="delete from tmplmst $delete_in";
$result_delete=pg_exec($con,$delete_status);
if($result_delete==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}



//共通SQL部品
$del_tables = array(
	"sum_wkfwmst","sum_wkfwapvdtl","sum_wkfwapvmng","sum_wkfwapvpstdtl","sum_wkfwapvsectdtl",
	"sum_wkfwnoticedtl","sum_wkfwnoticemng","sum_wkfwnoticepjtdtl","sum_wkfwnoticesectdtl","sum_wkfwnoticestdtl","sum_wkfwpjtdtl");
for($i=0; $i < count($trashbox); $i++){
	$del_tmpl_id = $trashbox[$i];
	$sql = "select * from sum_wkfwmst where tmpl_id = " .(int)$del_tmpl_id;
	$sel = select_from_table($con,$sql,"",$fname);

  while($row = pg_fetch_array($sel)) {
		for ($tbl=0; $tbl < count($del_tables); $tbl++){
			$sql = "delete from ".$del_tables[$tbl]." where wkfw_id = ". (int)$row["wkfw_id"];
			$result_delete = pg_exec($con, $sql);
		}
	}
}

pg_close($con);

//削除成功時は一覧画面を再表示する。
echo("<script language='javascript'>location.href=\"./sinryo_tmpl_list.php?session=$session\";</script>");

?>