<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("atdbk_common_class.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 他部署兼務
$list_other_post_flg = array(
        "1" => "有り",
        "" => "無し"
        );

// 他部署兼務履歴を取得 20120418
$sql = "SELECT * FROM empcond_other_post_flg_history WHERE emp_id='$emp_id' ORDER BY histdate DESC";
$sel_ophist = select_from_table($con, $sql, "", $fname);
if ($sel_ophist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 他部署兼務履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
/* 他部署兼務 履歴 */
function op_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.ophist.action = 'employee_cond_op_history_delete.php';
        document.ophist.empcond_other_post_flg_history_id.value = id;
        document.ophist.submit();
    }
}
function op_history_edit(id) {
    if (id) {
        document.ophist.empcond_other_post_flg_history_id.value = id;
    }
    document.ophist.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/employee/employee.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>他部署兼務履歴</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<form name="ophist" action="employee_cond_op_history_form.php" method="post">
<button type="button" onclick="op_history_edit()">他部署兼務の履歴を手動で追加する</button>
<? if (pg_num_rows($sel_ophist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">他部署兼務</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_ophist)) {?>
<tr>
    <td class="j12"><?php eh($list_other_post_flg[$r["other_post_flg"]]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="op_history_edit(<?php eh($r["empcond_other_post_flg_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="op_history_delete(<?php eh($r["empcond_other_post_flg_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="empcond_other_post_flg_history_id" value="">
<input type="hidden" name="wherefrom" value="<?php eh($wherefrom); ?>">
</form>

</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>