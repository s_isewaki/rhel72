<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("label_by_profile_type.ini");
require_once("show_sinryo_top_header.ini");
require_once("summary_common.ini");
require_once("about_comedix.php");


//==============================
//ヘッダー処理
//==============================

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療記録使用権限のチェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療記録区分/記録区分
$summary_record_kubun_title = $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];






//==============================
//ポストバック時
//==============================
if(@$is_postback == "true")
{
  if (count($data_select) == 0) {
    echo("<script type=\"text/javascript\">alert('チェックボックスをオンにしてください。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
  }

  //==============================
  //削除処理
  //==============================

  //削除パラメータ作成
  $delete_target_list = "";
  foreach($data_select as $org_id)
  {
    if($delete_target_list != "")
    {
      $delete_target_list .= ",";
    }
    $delete_target_list .= "'".pg_escape_string($org_id)."'";
  }

  // トランザクションの開始
  pg_query($con, "begin");

  //org_byoumeiテーブル削除
  $sql = "delete from org_byoumei where org_id in($delete_target_list)";
  $del = delete_from_table($con, $sql, "", $fname);
  if ($del == 0)
  {
    pg_query($con,"rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
  }

  //org_byoumei_indexテーブル削除
  $sql = "delete from org_byoumei_index where org_id in($delete_target_list)";
  $del = delete_from_table($con, $sql, "", $fname);
  if ($del == 0)
  {
    pg_query($con,"rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
  }

  // トランザクションのコミット
  pg_query($con, "commit");
}


//==============================
//追加病名一覧を取得
//==============================
$sql = "select * from org_byoumei order by (case when byoumei_kana <> '' then byoumei_kana else byoumei end)";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
  pg_close($con);
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
  exit;
}
$data_list = pg_fetch_all($sel);




?>
<title>CoMedix マスターメンテナンス | マスタ管理</title>
<script language="JavaScript" type="text/JavaScript">
function reload_page()
{
  location.href = "summary_org_byoumei_list.php?session=<?=$session?>";
}

function add_byoumei()
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
  window.open('summary_org_byoumei_input.php?session=<?=$session?>&mode=insert', 'newwin', option);
}
function update_byoumei(org_id)
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=640,height=500";
  window.open('summary_org_byoumei_input.php?session=<?=$session?>&mode=update&org_id='+org_id, 'newwin', option);
}
function delete_byoumei()
{
  if (!confirm('選択された病名を削除します。よろしいですか？'))
  {
    return;
  }
  document.mainform.submit();
}


</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_MST"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr height="22">
  <? summary_common_show_mst_tab_menu($session, '病名マスタ追加'); ?>
  <td align="right" nowrap>
  <input type="button" value="追加" onclick="add_byoumei()">
  <input type="button" value="削除" onclick="delete_byoumei()">
  </td>

  </tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<!-- theme list start -->



<form name="mainform" action="summary_org_byoumei_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="is_postback" value="true">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2_in2">
<tr>
<td bgcolor="#f6f9ff" width="30">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp</font>
</td>
<td bgcolor="#f6f9ff" width="*">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font>
</td>
<td bgcolor="#f6f9ff" width="100">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICD10</font>
</td>
<td bgcolor="#f6f9ff" width="100">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変換用コード</font>
</td>
<td bgcolor="#f6f9ff" width="100">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名コード</font>
</td>
<td bgcolor="#f6f9ff" width="100">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理番号</font>
</td>
</tr>
<?
  foreach((array)$data_list as $data_list1) {
		if (!$data_list1) break;
    $org_id = $data_list1['org_id'];
    $byoumei = $data_list1['byoumei'];
    $icd10 = $data_list1['icd10'];
    $byoumei_code = $data_list1['byoumei_code'];
    $rece_code = $data_list1['rece_code'];
    $byoumei_id = $data_list1['byoumei_id'];
?>
<input type="hidden" name="list_item_cd[]" value="<?=$item_cd?>">
<tr>
<td bgcolor="#ffffff" align="center">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <input type="checkbox" name="data_select[]" value="<?=$org_id?>">
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <a href="javascript:update_byoumei('<?=$org_id?>')">
   <?=h($byoumei) ?>
   </a>
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <?=h($icd10) ?>
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <?=h($byoumei_code) ?>
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <?=h($rece_code) ?>
   </font>
</td>
<td bgcolor="#ffffff">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <?=h($byoumei_id) ?>
   </font>
</td>
</tr>
<?
  }
?>
</table>




</td>
</tr>
</table>
</form>
<!-- comment view end -->
</td>
</tr>
</table>




</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
