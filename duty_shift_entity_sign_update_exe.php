<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜管理 |勤務シフト記号「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 指定ＩＤのレコードを全削除
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_pattern";
$cond = "where pattern_id = '$pattern_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 選択された勤務パターングループの勤務シフト記号情報を登録
///-----------------------------------------------------------------------------
for($i=0; $i<$data_cnt;$i++) {
	//値取得
	$wk1 = "atdptn_ptn_id_$i";
	$wk2 = "count_kbn_gyo_$i";
	$wk3 = "count_kbn_retu_$i";
	$wk4 = "font_name_$i";
	$wk5 = "font_color_id_$i";
	$wk6 = "back_color_id_$i";
	$wk7 = "reason_$i";
	$wk8 = "order_no_$i";
	$wk9 = "sfc_code_$i";
	$wk10 = "allotment_order_no_$i";
	$wk11 = "stamp_disp_flg_$i";
	$wk12 = "output_kbn_$i";
	//未設定時処理（null設定）
	if ($$wk2 == "") { $$wk2 = null; }
	if ($$wk3 == "") { $$wk3 = null; }
	if ($$wk10 == "") { $$wk10 = null; }
	//未設定時処理（空白設定）
	if (($$wk7 == "") || ($$wk7 == null)) { $$wk7 = " "; }
	if (empty($$wk11)){
		//チェックがなかったら表示
		$$wk11 = "t";
	}
	else{
		//チェックが有りで非表示
		$$wk11 = "f";
	}
	//ＳＱＬ
	$sql = "insert into duty_shift_pattern (pattern_id, atdptn_ptn_id, reason, count_kbn_gyo, count_kbn_retu, font_name, font_color_id, back_color_id, order_no, sfc_code, allotment_order_no, stamp_disp_flg, output_kbn ) values (";
	$content = array($pattern_id, $$wk1, $$wk7, $$wk2, $$wk3, pg_escape_string($$wk4), $$wk5, $$wk6, $$wk8, $$wk9, $$wk10, $$wk11, $$wk12 );
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_entity_sign.php?session=$session&pattern_id=$pattern_id';</script>");
?>

<!-- ************************************************************************ -->
<!-- デバック用 -->
<!-- ************************************************************************ -->
<HTML>
<?
/***
	for($i=0; $i<$data_cnt;$i++) {
		$wk = "atdptn_ptn_id_$i";
		echo($$wk);
		echo("<br>");
	}
***/
?>
</HTML>

