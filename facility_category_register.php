<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>マスターメンテナンス | 施設・設備 | カテゴリ登録</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_facility.ini");
require_once("show_select_values.ini");
require_once("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// デフォルト値の設定
$base_min = 5;
$pt_flg = "f";
$chart_start_hour = "00";
$chart_start_min = "00";
$chart_end_hour = "24";
$chart_end_min = "00";
$bg_color = "e5effa";
$show_content = "f";
$auth_flg = "f";
$has_admin_flg = "f";
$end_date_flg = "1";
$double_flg = "f";

// 色の選択肢設定
$colors = array(
    "ffe8e8" => "桜色",
    "efe4e4" => "灰色",
    "f7edd3" => "白茶",
    "f5f5e5" => "象牙色",
    "e8f7d8" => "薄い抹茶色",
    "eafaea" => "白緑",
    "e1faf3" => "ベビーブルー",
    "c8f0ff" => "薄い空色",
    "e5effa" => "藍白",
    "e8e0ef" => "薄藤",
    "ffe0ef" => "薄い桃色",
    "f0c7c7" => "灰桜",
    "e8dcbb" => "薄香色",
    "dce8bb" => "利休白茶",
    "c7f0c7" => "浅緑",
    "bbe8dc" => "青磁色",
    "bbdce8" => "水色",
    "c2ceef" => "浅い藍色",
    "c8f0ff" => "藤色",
    "e8bbdc" => "紅藤",
    "fc8181" => "紅梅色",
    "f99570" => "洗朱",
    "e4bf5b" => "芥子色",
    "b8d175" => "抹茶色",
    "84df9c" => "若竹色",
    "75b8d1" => "浅縹",
    "849cdf" => "浅い紺色",
    "aa93c2" => "龍胆色",
    "d175b8" => "モーブ"
);

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 患者管理機能が使用可能かどうか取得
$sql = "select lcs_func1 from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$pt_flg_disabled = (pg_fetch_result($sel, 0, "lcs_func1") != "t") ? " disabled" : "";

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkEndMinute() {
    if (document.mainform.chart_end_hour.value == '24' &&
        document.mainform.chart_end_hour.value != '00') {
        document.mainform.chart_end_min.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>カテゴリ登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="facility_category_insert.php" method="post" name="mainform">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ名</font></td>
<td><input type="text" name="cate_name" size="40" maxlength="100" style="ime-mode: active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間枠</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="base_min">
<option value="5"<? if ($base_min == 5) {echo(" selected");} ?>>5
<option value="10"<? if ($base_min == 10) {echo(" selected");} ?>>10
<option value="15"<? if ($base_min == 15) {echo(" selected");} ?>>15
<option value="20"<? if ($base_min == 20) {echo(" selected");} ?>>20
<option value="30"<? if ($base_min == 30) {echo(" selected");} ?>>30
<option value="60"<? if ($base_min == 60) {echo(" selected");} ?>>60
</select>分
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムチャート表示時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="chart_start_hour">
<? show_select_hrs_0_23($chart_start_hour); ?>
</select>：<select name="chart_start_min">
<? show_minute_options($chart_start_min); ?>
</select>〜<select name="chart_end_hour" onchange="checkEndMinute();">
<? show_select_hrs_by_args($chart_start_hour, $chart_end_hour, $chart_end_hour, false); ?>
</select>：<select name="chart_end_min" onchange="checkEndMinute();">
<? show_minute_options($chart_end_min); ?>
</select>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者管理/利用者管理
echo $_label_by_profile["PATIENT_MANAGE"][$profile_type];
?>機能と連携</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="pt_flg" value="t"<? if ($pt_flg == "t") {echo(" checked");} ?><? echo($pt_flg_disabled); ?>>する
<input type="radio" name="pt_flg" value="f"<? if ($pt_flg == "f") {echo(" checked");} ?><? echo($pt_flg_disabled); ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">色</font></td>
<td>
<select name="bg_color">
<?
foreach ($colors as $tmp_color_code => $tmp_color_name) {
    echo("<option value=\"$tmp_color_code\" style=\"background-color:#$tmp_color_code\"");
    if ($tmp_color_code == $bg_color) {
        echo(" selected");
    }
    echo(">$tmp_color_name\n");
}
?>
</select>
</td>
</tr>
<tr height="22">
<td  align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約可能権限設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="auth_flg" value="t"<? if ($auth_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="auth_flg" value="f"<? if ($auth_flg != "t") {echo(" checked");} ?>>しない
<br><font color="red">※「する」を選択すると、予約可能な職員を指定できるようになります。それ以外の場合は、どの職員でも予約可能となります。</font>
</font></td>
</tr>
<tr height="22">
<td  align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備管理者設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="has_admin_flg" value="t"<? if ($has_admin_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="has_admin_flg" value="f"<? if ($has_admin_flg != "t") {echo(" checked");} ?>>しない
<br><font color="red">※管理者は、どの職員が登録した予約でも更新・削除可能となります。通常は自分の登録した予約しか更新・削除できません。</font>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週表示に予定内容を表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="show_content" value="t"<? if ($show_content == "t") {echo(" checked");} ?>>する
<input type="radio" name="show_content" value="f"<? if ($show_content == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約開始日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="start_date1"><? show_select_years_future(11,$start_date1,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="start_date2"><? show_select_months($start_date2,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="start_date3"><? show_select_days($start_date3,true); ?></select>
</font></td>
</tr>
<td align="right" valign="top" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約期限日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="end_date_flg" value="1"<? if ($end_date_flg == "1") {echo(" checked");} ?>>日付指定
<input type="radio" name="end_date_flg" value="2"<? if ($end_date_flg == "2") {echo(" checked");} ?>>期間指定<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block_in">
<tr>
<td>
<select name="end_date1"><? show_select_years_future(11,$end_date1,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="end_date2"><? show_select_months($end_date2,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="end_date3"><? show_select_days($end_date3,true); ?></select></font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="end_month">
<?
// 1〜24のオプション
echo("<option value=\"-\">\n");

for ($i = 1; $i <= 24; $i++) {
    $val = sprintf("%02d", $i);
    echo("<option value=\"$val\"");
    if ($i == $fix) {
        echo(" selected");
    }
    echo(">$i</option>\n");
}

?>
</select>月後まで
</font>
</td>
</tr>
</table>

</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約時間の重複許可</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="double_flg" value="t"<? if ($double_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="double_flg" value="f"<? if ($double_flg == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// 分オプションを出力
function show_minute_options($minute) {
    $arr_minute = array();
    for ($i = 0; $i < 60; $i += 5) {
        $arr_minute[] = sprintf("%02d", $i);
    }

    foreach ($arr_minute as $tmp_minute) {
        echo("<option value=\"$tmp_minute\"");
        if ($minute == $tmp_minute) {
            echo " selected";
        }
        echo(">$tmp_minute");
    }
}
