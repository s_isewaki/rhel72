<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションをチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限をチェック
$checkauth = check_authority($session, 21, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$row_count = 1;
foreach ($reason_ids as $tmp_reason_id) {
	if (strlen($reasons[$tmp_reason_id]) > 200) {
		echo("<script type=\"text/javascript\">alert('{$row_count}行目の除外理由が長すぎます');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
	$row_count++;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 計算除外理由情報をDELETE〜INSERT
$sql = "delete from bedexprsn";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

while (count($reason_ids) > 0) {
	$tmp_reason_id = array_shift($reason_ids);
	$tmp_display = $displays[$tmp_reason_id];
	$tmp_reason = $reasons[$tmp_reason_id];

	$tmp_display = ($tmp_display == "t") ? "t" : "f";

	$sql = "insert into bedexprsn (reason_id, display, reason) values (";
	$content = array($tmp_reason_id, $tmp_display, $tmp_reason);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'bed_exception_setting.php?session=$session';</script>");
?>
