<?
class html_utils
{

	/**
	 * 最小値・最大値・間隔を指定して数値範囲を表示するコンボボックスを作成する
	 * @param $tag_name    selectの名前
	 * @param $selectValue 選択する値
	 * @param $min         最小値
	 * @param $max         最大値
	 * @param $countup     表示間隔
	 * @param $blank       空項目の有無
	 * @param $blank       空項目のvalue
	 */
	function show_select_options($tag_name, $select_value, $min, $max, $countup, $blank, $blank_value) {
		echo("<select name=\"$tag_name\">");
		html_utils::show_time_options($select_value, $min, $max, $countup, $blank, $blank_value);
		echo("</select>");
	}

	/**
	 * 最小値・最大値・間隔を指定して数値範囲を表示するコンボボックスを作成する
	 * @param $selectValue 選択する値
	 * @param $min         最小値
	 * @param $max         最大値
	 * @param $countup     表示間隔      デフォルト:1
	 * @param $blank       空項目の有無  デフォルト:true
	 * @param $blank       空項目のvalue デフォルト:--
	 */
	function show_time_options($select_value, $min, $max, $countup = 1, $blank = true, $blank_value = "--") {
		if ($blank) {
			echo("<option value=\"$blank_value\">\n");
		}

		for ($i = $min; $i <= $max; $i +=$countup) {
			if (is_numeric($i) && strlen($i) < 2){
				$val = sprintf("%02d", $i);
			}
			else{
				$val = $i;
			}
			echo("<option value=\"$val\"");
			if ($val == $select_value) {
				echo(" selected");
			}
			echo(">$i\n");
		}
	}

	/**
	 * チェックボックスを作成する
	 * @param $tag_name    チェックボックスの名前
	 * @param $check       チェックボックスのvalueと同じ場合チェックがつく
	 * @param $value       チェックボックスのvalue
	 * @param $label_value ラベル（右側に表示）
	 */
	function show_checkbox($tag_name, $check, $value, $label_value = "") {

		$label_name = "";
		if (empty($label_value) == false){
			$label_name = $tag_name."_label";
			echo("<label for=\"".$label_name."\">");
		}

		if (empty($tag_name) == false && empty($value) == false) {
			$id_value = "";
			if (empty($label_name) == false){
				$id_value = "id=\"$label_name\"";
			}
			$checked = "";
			if ($check == $value){
				$checked = " checked";
			}

			$input_type = "checkbox";

			echo("<input type=\"".$input_type."\" name=\"".$tag_name."\" ".$id_value.$checked." value=\"".$value."\">");
		}

		if (empty($label_value) == false){
			echo($label_value."</label>");
		}
		echo("\n");

	}

	/**
	 * チェックボックスを作成する
	 * @param $tag_name    チェックボックスの名前
	 * @param $check       チェックボックスのvalueと同じ場合チェックがつく
	 * @param $value       チェックボックスのvalue
	 * @param $label_value ラベル（右側に表示）
	 * @param $index       ラジオボタンの番号（ラベルに使う、省略,マイナス数値でラベルなし）
	 */
	function show_radio($tag_name, $check, $value, $label_value = "", $index = -1) {
		$label_name = "";
		if (empty($label_value) == false && $index > -1){
			$label_name = $tag_name."_label".$index;
			echo("<label for=\"".$label_name."\">");
		}

		if (empty($tag_name) == false && empty($value) == false) {
			$id_value = "";
			if (empty($label_name) == false){
				$id_value = "id=\"$label_name\"";
			}
			$checked = "";
			if ($check == $value){
				$checked = " checked";
			}
			$input_type = "radio";

			echo("<input type=\"".$input_type."\" name=\"".$tag_name."\" ".$id_value.$checked." value=\"".$value."\">");
		}

		echo($label_value);
		if (empty($label_value) == false && $index > 0){
			echo("</label>");
		}
		echo("\n");

	}

}
?>