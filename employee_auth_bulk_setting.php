<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 権限一括設定</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// ライセンス情報の取得
$sql = "select * from license";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 19; $i++) {
	$var_name = "func$i";
	$$var_name = pg_fetch_result($sel, 0, "lcs_func$i");
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
// 当直・外来分担表
$intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$intra_menu2_4 = mbereg_replace("・外来", "", $intra_menu2_4);
}
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");

// グループ一覧を配列に格納
$sql = "select group_id, group_nm from authgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}

// デフォルトの設定内容を「権限グループの設定」にする
if ($action_mode == "") {
	$action_mode = "1";
}

require_once("get_menu_label.ini");
$report_menu_label = get_report_menu_label($con, $fname);
$shift_menu_label = get_shift_menu_label($con, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var modelWin;

function initPage() {
	setPtRefAuth();
	setPtOutRefAuth();
	setPtDisRefAuth();
	setIntraAuth();
	setQAAuth();
//XX 	setCmtAuth();
	setEmpRefAuth();
	setAmbAuth();
	setWardAuth();
	setInciAuth();
	setFPlusAuth();
	setManabuAuth();
	setMedAuth();
	setKViewAuth();
	setSchdAuth();
	setFclAuth();
	setLinkAuth();
	setBbsAuth();
	setAdbkAuth();
	setWebmlAuth();
	setShiftAuth();
	setShift2Auth();
	setCasAuth();
	setJinjiAuth();
	setLadderAuth();
	setCareerAuth();
	setJnlAuth();
	setNlcsAuth();
	setPjtAuth();
	setCAuthAuth();
	setRowsDisplay();
    <? if ($func31=="t") { ?>setCCUsr1Auth();<? } ?>
}

function setPtRefAuth() {
	setSubBox(document.employee.ptif, document.employee.ptreg.checked, '<? echo($func1); ?>');
}

function setPtOutRefAuth() {
	setSubBox(document.employee.inout, document.employee.outreg.checked, '<? echo($func1); ?>');
}

function setPtDisRefAuth() {
	setSubBox(document.employee.dishist, document.employee.disreg.checked, '<? echo($func1); ?>');
}

function setIntraAuth() {
	var check_flag = (
		document.employee.ymstat.checked ||
		document.employee.intram.checked ||
		document.employee.stat.checked ||
		document.employee.allot.checked ||
		document.employee.life.checked
	);
	setSubBox(document.employee.intra, check_flag, '<? echo($func4); ?>');
}

function setQAAuth() {
	setSubBox(document.employee.qa, document.employee.qaadm.checked, 't');
}

//XX function setCmtAuth() {
//XX 	setSubBox(document.employee.cmt, document.employee.cmtadm.checked, 't');
//XX }

function setEmpRefAuth() {
	setSubBox(document.employee.empif, document.employee.reg.checked, 't');
}

function setAmbAuth() {
	setSubBox(document.employee.amb, document.employee.ambadm.checked, '<? echo($func15); ?>');
}

function setWardAuth() {
	if (document.employee.ward_reg) {
		setSubBox(document.employee.ward, document.employee.ward_reg.checked, '<? echo($func2); ?>');
	}
}

function setInciAuth() {
	setSubBox(document.employee.inci, document.employee.rm.checked, '<? echo($func5); ?>');
}

function setFPlusAuth() {
	setSubBox(document.employee.fplus, document.employee.fplusadm.checked, '<? echo($func13); ?>');
}

function setManabuAuth() {
	setSubBox(document.employee.manabu, document.employee.manabuadm.checked, '<? echo($func12); ?>');
}

function setMedAuth() {
	setSubBox(document.employee.med, document.employee.medadm.checked, '<? echo($func6); ?>');
}

function setKViewAuth() {
	setSubBox(document.employee.kview, document.employee.kviewadm.checked, '<? echo($func11); ?>');
}

function setSchdAuth() {
	setSubBox(document.employee.schd, document.employee.schdplc.checked, 't');
}

function setFclAuth() {
	setSubBox(document.employee.resv, document.employee.fcl.checked, 't');
}

function setLinkAuth() {
	setSubBox(document.employee.link, document.employee.linkadm.checked, 't');
}

function setBbsAuth() {
	setSubBox(document.employee.bbs, document.employee.bbsadm.checked, 't');
}

function setAdbkAuth() {
	setSubBox(document.employee.adbk, document.employee.adbkadm.checked, 't');
}

function setWebmlAuth() {
	setSubBox(document.employee.webml, document.employee.webmladm.checked, 't');
}

function setShiftAuth() {
	setSubBox(document.employee.shift, document.employee.shiftadm.checked, '<? echo($func9); ?>');
}

function setShift2Auth() {
	setSubBox(document.employee.shift2, document.employee.shift2adm.checked, '<? echo($func18); ?>');
}

function setCasAuth() {
	setSubBox(document.employee.cas, document.employee.casadm.checked, '<? echo($func8); ?>');
}

function setJinjiAuth() {
	setSubBox(document.employee.jinji, document.employee.jinjiadm.checked, '<? echo($func16); ?>');
}

function setLadderAuth() {
	setSubBox(document.employee.ladder, document.employee.ladderadm.checked, '<? echo($func17); ?>');
}

function setCareerAuth() {
	setSubBox(document.employee.career, document.employee.careeradm.checked, '<? echo($func19); ?>');
}

function setJnlAuth() {
	setSubBox(document.employee.jnl, document.employee.jnladm.checked, '<? echo($func14); ?>');
}

function setNlcsAuth() {
	setSubBox(document.employee.nlcs, document.employee.nlcsadm.checked, '<? echo($func10); ?>');
}

function setPjtAuth() {
	setSubBox(document.employee.pjt, document.employee.pjtadm.checked, 't');
}

function setCAuthAuth() {
	setSubBox(document.employee.pass_flg, document.employee.cauth.checked, 't');
}
function setCCUsr1Auth() {
    setSubBox(document.employee.ccusr1, document.employee.ccadm1.checked, 't');
}


function setSubBox(sub_box, check_flag, sub_box_checkable) {
	if (check_flag) {
		sub_box.checked = true;
		sub_box.disabled = true;
	} else {
		sub_box.disabled = (sub_box_checkable != 't');
	}
}

function setRowsDisplay() {
	var group_mode = document.employee.action_mode[0].checked;
	document.getElementById('group_row').style.display = (group_mode) ? '' : 'none';
	document.getElementById('copy_row').style.display  = (group_mode) ? 'none' : '';
	document.getElementById('auth_row').style.display  = (group_mode) ? 'none' : '';
	if (group_mode && modelWin) {
		modelWin.close();
		modelWin = null;
	}
}

function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}

function setAtrbOptions() {
	clearOptions(document.employee.tgt_atrb);

	var class_id = document.employee.tgt_class.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
	if (class_id == '<? echo $row["class_id"]; ?>') {
		addOption(document.employee.tgt_atrb, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>');
	}
<? } ?>

	setDeptOptions();
}

function setDeptOptions() {
	clearOptions(document.employee.tgt_dept);

	var atrb_id = document.employee.tgt_atrb.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
	if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
		addOption(document.employee.tgt_dept, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>');
	}
<? } ?>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
	setRoomOptions();
<? } ?>
}

<? if ($arr_class_name["class_cnt"] == 4) { ?>
function setRoomOptions() {
	clearOptions(document.employee.tgt_room);

	var dept_id = document.employee.tgt_dept.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
	if (dept_id == '<? echo $row["dept_id"]; ?>') {
		addOption(document.employee.tgt_room, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>');
	}
<? } ?>
}
<? } ?>

function clearOptions(box) {
	for (var i = box.length - 1; i > 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function openModel() {
	modelWin = window.open('employee_auth_model.php?session=<? echo($session); ?>', 'newwin', 'scrollbars=yes,width=640,height=480');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list td .inner {border-collapse:collapse;}
.list td .inner td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_display_bulk_setting.php?session=<? echo($session); ?>">表示</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_menu_bulk_setting.php?session=<? echo($session); ?>">メニュー</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_condition_bulk_setting.php?session=<? echo($session); ?>">勤務条件</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限を一括設定しました。</font></td>
<? } else { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※設定内容は登録済みの職員にのみ反映されます。職員の追加登録時には「一覧」→「権限」画面で個別設定するか、再度一括設定して下さい</font></td>
<? } ?>
</tr>
</table>
<form name="employee" action="employee_auth_bulk_setting_confirm.php" method="post" onsubmit="this.test_elm">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="18%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td width="34%">
<select name="tgt_job[]" size="6" multiple>
<? show_options($sel_job, "job_id", "job_nm", $tgt_job); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_job[]']);">
</td>
<td width="14%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td width="34%">
<select name="tgt_st[]" size="6" multiple>
<? show_options($sel_st, "st_id", "st_nm", $tgt_st); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_st[]']);">
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?></font></td>
<? } ?>
</tr>
<tr>
<td style="padding-right:6px;">
<select name="tgt_class" onchange="setAtrbOptions();">
<option value="">　　　　　　　　</option>
<?
while ($row = pg_fetch_array($sel_class)) {
	echo("<option value=\"{$row["class_id"]}\"");
	if ($row["class_id"] == $tgt_class) {echo(" selected");}
	echo(">{$row["class_nm"]}\n");
}
?>
</select>
</td>
<td style="padding-right:6px;">
<select name="tgt_atrb" onchange="setDeptOptions();">
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_atrb, 0);
while ($row = pg_fetch_array($sel_atrb)) {
	if ($row["class_id"] == $tgt_class) {
		echo("<option value=\"{$row["atrb_id"]}\"");
		if ($row["atrb_id"] == $tgt_atrb) {echo(" selected");}
		echo(">{$row["atrb_nm"]}\n");
	}
}
?>
</select>
</td>
<td style="padding-right:6px;">
<select name="tgt_dept"<? if ($arr_class_name["class_cnt"] == 4) { ?> onchange="setRoomOptions();"<? } ?>>
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_dept, 0);
while ($row = pg_fetch_array($sel_dept)) {
	if ($row["atrb_id"] == $tgt_atrb) {
		echo("<option value=\"{$row["dept_id"]}\"");
		if ($row["dept_id"] == $tgt_dept) {echo(" selected");}
		echo(">{$row["dept_nm"]}\n");
	}
}
?>
</select>
</td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td style="padding-right:6px;">
<select name="tgt_room">
<option value="">　　　　　　　　</option>
<?
pg_result_seek($sel_room, 0);
while ($row = pg_fetch_array($sel_room)) {
	if ($row["dept_id"] == $tgt_dept) {
		echo("<option value=\"{$row["room_id"]}\"");
		if ($row["room_id"] == $tgt_room) {echo(" selected");}
		echo(">{$row["room_nm"]}\n");
	}
}
?>
</select>
</td>
<? } ?>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定内容</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="action_mode" value="1"<? if ($action_mode == "1") {echo(" checked");} ?> onclick="setRowsDisplay();">権限グループの設定
<input type="radio" name="action_mode" value="2"<? if ($action_mode == "2") {echo(" checked");} ?> onclick="setRowsDisplay();">権限の追加設定
<input type="radio" name="action_mode" value="3"<? if ($action_mode == "3") {echo(" checked");} ?> onclick="setRowsDisplay();">権限の再設定
</font></td>
</tr>
<tr height="22" id="group_row" style="display:none;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></td>
<td colspan="3"><select name="group_id">
<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
?>
</select></td>
</tr>
<tr height="22" id="copy_row" style="display:none;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="openModel();">権限コピー元職員</a></font></td>
<td colspan="3"><input type="text" name="model_emp_name" value="<? echo($hid_model_emp_name); ?>" size="25" disabled> <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※選択した職員の権限がチェックボックスに反映されます</font></td>
</tr>
<tr height="22" id="auth_row" style="display:none;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>基本機能</b></font></td>
</tr>
</table>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール</font></td>
<td align="center"><input name="webml" type="checkbox" value="t"<? if ($webml == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="webmladm" type="checkbox" value="t"<? if ($webmladm == "t") {echo(" checked");} ?> onclick="setWebmlAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
<td align="center"><input name="schd" type="checkbox" value="t"<? if ($schd == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="schdplc" type="checkbox" value="t"<? if ($schdplc == "t") {echo(" checked");} ?> onclick="setSchdAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG</font></td>
<td align="center"><input name="pjt" type="checkbox" value="t"<? if ($pjt == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="pjtadm" type="checkbox" value="t"<? if ($pjtadm == "t") {echo(" checked");} ?> onclick="setPjtAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ・回覧板</font></td>
<td align="center"><input name="newsuser" type="checkbox" value="t"<? if ($newsuser == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="news" type="checkbox" value="t"<? if ($news == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タスク</font></td>
<td align="center"><input name="work" type="checkbox" value="t"<? if ($work == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">伝言メモ</font></td>
<td align="center"><input name="phone" type="checkbox" value="t"<? if ($phone == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設備予約</font></td>
<td align="center"><input name="resv" type="checkbox" value="t"<? if ($resv == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="fcl" type="checkbox" value="t"<? if ($fcl == "t") {echo(" checked");} ?> onclick="setFclAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤表／勤務管理</font></td>
<td align="center"><input name="attd" type="checkbox" value="t"<? if ($attd == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="wkadm" type="checkbox" value="t"<? if ($wkadm == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">決裁・申請／ワークフロー</font></td>
<td align="center"><input name="aprv_use" type="checkbox" value="t"<? if ($aprv_use == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="wkflw" type="checkbox" value="t"<? if ($wkflw == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ネットカンファレンス</font></td>
<td align="center"><input name="conf" type="checkbox" value="t"<? if ($conf == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲示板・電子会議室</font></td>
<td align="center"><input name="bbs" type="checkbox" value="t"<? if ($bbs == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="bbsadm" type="checkbox" value="t"<? if ($bbsadm == "t") {echo(" checked");} ?> onclick="setBbsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Q&amp;A</font></td>
<td align="center"><input name="qa" type="checkbox" value="t"<? if ($qa == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="qaadm" type="checkbox" value="t"<? if ($qaadm == "t") {echo(" checked");} ?> onclick="setQAAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書管理</font></td>
<td align="center"><input name="lib" type="checkbox" value="t"<? if ($lib == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="libadm" type="checkbox" value="t"<? if ($libadm == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス帳</font></td>
<td align="center"><input name="adbk" type="checkbox" value="t"<? if ($adbk == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="adbkadm" type="checkbox" value="t"<? if ($adbkadm == "t") {echo(" checked");} ?> onclick="setAdbkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内線電話帳</font></td>
<td align="center"><input name="ext" type="checkbox" value="t"<? if ($ext == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="extadm" type="checkbox" value="t"<? if ($extadm == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リンクライブラリ</font></td>
<td align="center"><input name="link" type="checkbox" value="t"<? if ($link == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="linkadm" type="checkbox" value="t"<? if ($linkadm == "t") {echo(" checked");} ?> onclick="setLinkAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パスワード変更</font></td>
<td align="center"><input name="pass_flg" type="checkbox" value="t"<? if ($pass_flg == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<!-- tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コミュニティサイト</font></td>
<td align="center"><input name="cmt" type="checkbox" value="t"<? if ($cmt == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="cmtadm" type="checkbox" value="t"<? if ($cmtadm == "t") {echo(" checked");} ?> onclick="setCmtAuth();"></td>
</tr -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備忘録</font></td>
<td align="center"><input name="memo" type="checkbox" value="t"<? if ($memo == "t") {echo(" checked");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証</font></td>
<td align="center"><input name="cauth" type="checkbox" value="t"<? if ($cauth == "t") {echo(" checked");} ?> onclick="setCAuthAuth();"></td>
<td></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>管理機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織プロフィール</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 院内行事登録／法人内行事登録
echo($_label_by_profile["EVENT_REG"][$profile_type]);
?></font></td>
<td></td>
<td align="center"><input name="tmgd" type="checkbox" value="t"<? if ($tmgd == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録</font></td>
<td></td>
<td align="center"><input name="reg" type="checkbox" value="t"<? if ($reg == "t") {echo(" checked");} ?> onclick="setEmpRefAuth();"></td>
<td align="center"><input name="empif" type="checkbox" value="t"<? if ($empif == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マスターメンテナンス</font></td>
<td></td>
<td></td>
<td></td>
<td align="center"><input name="hspprf" type="checkbox" value="t"<? if ($hspprf == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="dept_reg" type="checkbox" value="t"<? if ($dept_reg == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="job_flg" type="checkbox" value="t"<? if ($job_flg == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="status_flg" type="checkbox" value="t"<? if ($status_flg == "t") {echo(" checked");} ?>></td>
<td align="center"><input name="attditem" type="checkbox" value="t"<? if ($attditem == "t") {echo(" checked");} ?>></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">環境設定</font></td>
<td></td>
<td align="center"><input name="config" type="checkbox" value="t"<? if ($config == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス管理</font></td>
<td></td>
<td align="center"><input name="lcs" type="checkbox" value="t"<? if ($lcs == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アクセスログ</font></td>
<td></td>
<td align="center"><input name="aclg" type="checkbox" value="t"<? if ($aclg == "t") {echo(" checked");} ?>></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>

</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務機能</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者登録／利用者登録
echo($_label_by_profile["PATIENT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者基本情報／利用者基本情報
echo($_label_by_profile["PATIENT_INFO"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 来院登録／来所登録
echo($_label_by_profile["OUT_REG"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 入院来院履歴／入所来所履歴
echo($_label_by_profile["IN_OUT"][$profile_type]);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレム</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者権限</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 患者管理／利用者管理
echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><input name="ptreg" type="checkbox" value="t"<? if ($ptreg == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtRefAuth();"></td>
<td align="center"><input name="ptif" type="checkbox" value="t"<? if ($ptif == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="outreg" type="checkbox" value="t"<? if ($outreg == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtOutRefAuth();"></td>
<td align="center"><input name="inout" type="checkbox" value="t"<? if ($inout == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="disreg" type="checkbox" value="t"<? if ($disreg == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?> onclick="setPtDisRefAuth();"></td>
<td align="center"><input name="dishist" type="checkbox" value="t"<? if ($dishist == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ptadm" type="checkbox" value="t"<? if ($ptadm == "t") {echo(" checked");} ?><? if ($func1 == "f") {echo(" disabled");} ?>></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="50%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者機能</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索ちゃん</font></td>
<td align="center"><input name="search" type="checkbox" value="t"<? if ($search == "t") {echo(" checked");} ?><? if ($func7 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>



<? if ($func31=="t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スタッフ・ポートフォリオ</font></td>
<td align="center"><input name="ccusr1" type="checkbox" value="t"<? if ($ccusr1 == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?>></td>
<td align="center">
<input name="ccadm1" type="checkbox" value="t"<? if ($ccadm1 == "t") {echo(" checked");} ?><? if ($func31 == "f") {echo(" disabled");} ?> onclick="setCCUsr1Auth();">
</td>
</tr>
<? } ?>






<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護支援</font></td>
<td align="center"><input name="amb" type="checkbox" value="t"<? if ($amb == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ambadm" type="checkbox" value="t"<? if ($ambadm == "t") {echo(" checked");} ?><? if ($func15 == "f") {echo(" disabled");} ?> onclick="setAmbAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 病床管理
echo($_label_by_profile["BED_MANAGE"][$profile_type]);
?></font></td>
<td align="center"><input name="ward" type="checkbox" value="t"<? if ($ward == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?>></td>
<td align="center">
<? if ($profile_type != "2") { ?>
<input name="ward_reg" type="checkbox" value="t"<? if ($ward_reg == "t") {echo(" checked");} ?><? if ($func2 == "f") {echo(" disabled");} ?> onclick="setWardAuth();">
<? } ?>
</td>
</tr>
<? if ($profile_type != "2") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 診療科／担当部門
echo($_label_by_profile["SECTION"][$profile_type]);
?></font></td>
<td></td>
<td align="center">
<input name="entity_flg" type="checkbox" value="t"<? if ($entity_flg == "t") {echo(" checked");} ?><? if ($func2 == "f" && $func6 == "f") {echo(" disabled");} ?>>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護観察記録</font></td>
<td align="center"><input name="nlcs" type="checkbox" value="t"<? if ($nlcs == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="nlcsadm" type="checkbox" value="t"<? if ($nlcsadm == "t") {echo(" checked");} ?><? if ($func10 == "f") {echo(" disabled");} ?> onclick="setNlcsAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん</font></td>
<td align="center"><input name="inci" type="checkbox" value="t"<? if ($inci == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="rm" type="checkbox" value="t"<? if ($rm == "t") {echo(" checked");} ?><? if ($func5 == "f") {echo(" disabled");} ?> onclick="setInciAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファントルくん＋</font></td>
<td align="center"><input name="fplus" type="checkbox" value="t"<? if ($fplus == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="fplusadm" type="checkbox" value="t"<? if ($fplusadm == "t") {echo(" checked");} ?><? if ($func13 == "f") {echo(" disabled");} ?> onclick="setFPlusAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">バリテス</font></td>
<td align="center"><input name="manabu" type="checkbox" value="t"<? if ($manabu == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="manabuadm" type="checkbox" value="t"<? if ($manabuadm == "t") {echo(" checked");} ?><? if ($func12 == "f") {echo(" disabled");} ?> onclick="setManabuAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// メドレポート／POレポート
echo($report_menu_label);
?></font></td>
<td align="center"><input name="med" type="checkbox" value="t"<? if ($med == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="medadm" type="checkbox" value="t"<? if ($medadm == "t") {echo(" checked");} ?><? if ($func6 == "f") {echo(" disabled");} ?> onclick="setMedAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カルテビューワー</font></td>
<td align="center"><input name="kview" type="checkbox" value="t"<? if ($kview == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="kviewadm" type="checkbox" value="t"<? if ($kviewadm == "t") {echo(" checked");} ?><? if ($func11 == "f") {echo(" disabled");} ?> onclick="setKViewAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経営支援</font></td>
<td align="center"><input name="biz" type="checkbox" value="t"<? if ($biz == "t") {echo(" checked");} ?><? if ($func3 == "f") {echo(" disabled");} ?>></td>
<td></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
// 勤務シフト作成
echo($shift_menu_label);
?></font></td>
<td align="center"><input name="shift" type="checkbox" value="t"<? if ($shift == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="shiftadm" type="checkbox" value="t"<? if ($shiftadm == "t") {echo(" checked");} ?><? if ($func9 == "f") {echo(" disabled");} ?> onclick="setShiftAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務表</font></td>
<td align="center"><input name="shift2" type="checkbox" value="t"<? if ($shift2 == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="shift2adm" type="checkbox" value="t"<? if ($shift2adm == "t") {echo(" checked");} ?><? if ($func18 == "f") {echo(" disabled");} ?> onclick="setShift2Auth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// クリニカルラダー／CAS
echo(get_cas_title_name());
?></font></td>
<td align="center"><input name="cas" type="checkbox" value="t"<? if ($cas == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="casadm" type="checkbox" value="t"<? if ($casadm == "t") {echo(" checked");} ?><? if ($func8 == "f") {echo(" disabled");} ?> onclick="setCasAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人事管理</font></td>
<td align="center"><input name="jinji" type="checkbox" value="t"<? if ($jinji == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="jinjiadm" type="checkbox" value="t"<? if ($jinjiadm == "t") {echo(" checked");} ?><? if ($func16 == "f") {echo(" disabled");} ?> onclick="setJinjiAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">クリニカルラダー</font></td>
<td align="center"><input name="ladder" type="checkbox" value="t"<? if ($ladder == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ladderadm" type="checkbox" value="t"<? if ($ladderadm == "t") {echo(" checked");} ?><? if ($func17 == "f") {echo(" disabled");} ?> onclick="setLadderAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">キャリア開発ラダー</font></td>
<td align="center"><input name="career" type="checkbox" value="t"<? if ($career == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="careeradm" type="checkbox" value="t"<? if ($careeradm == "t") {echo(" checked");} ?><? if ($func19 == "f") {echo(" disabled");} ?> onclick="setCareerAuth();"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日報・月報</font></td>
<td align="center"><input name="jnl" type="checkbox" value="t"<? if ($jnl == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="jnladm" type="checkbox" value="t"<? if ($jnladm == "t") {echo(" checked");} ?><? if ($func14 == "f") {echo(" disabled");} ?> onclick="setJnlAuth();"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="8"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="inner">
<tr height="22" bgcolor="#f6f9ff">
<td></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ユーザ機能</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月間・年間<? echo($intra_menu1); ?>参照</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラメニュー</font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 稼働状況統計
echo($intra_menu1);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 当直・外来分担表
echo($intra_menu2_4);
?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
// 私たちの生活サポート
echo($intra_menu3);
?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">イントラネット</font></td>
<td align="center"><input name="intra" type="checkbox" value="t"<? if ($intra == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?>></td>
<td align="center"><input name="ymstat" type="checkbox" value="t"<? if ($ymstat == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="intram" type="checkbox" value="t"<? if ($intram == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="stat" type="checkbox" value="t"<? if ($stat == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="allot" type="checkbox" value="t"<? if ($allot == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
<td align="center"><input name="life" type="checkbox" value="t"<? if ($life == "t") {echo(" checked");} ?><? if ($func4 == "f") {echo(" disabled");} ?> onclick="setIntraAuth();"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="hid_model_emp_name" value="<? echo($hid_model_emp_name); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
	while ($row = pg_fetch_array($sel)) {
		$id = $row[$id_col];
		$nm = $row[$nm_col];
		echo("<option value=\"$id\"");
		if (in_array($id, $tgt_ids)) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
}
?>
