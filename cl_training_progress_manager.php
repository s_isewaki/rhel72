<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
//require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_application_workflow_select_box.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();
$calendar = '';
$calendar .= write_calendar('_apply_from', 'date_apply_from_y', 'date_apply_from_m', 'date_apply_from_d');
$calendar .= write_calendar('_apply_to', 'date_apply_to_y', 'date_apply_to_m', 'date_apply_to_d');
$calendar .= write_calendar('_students_from', 'date_students_from_y', 'date_students_from_m', 'date_students_from_d');
$calendar .= write_calendar('_students_to', 'date_students_to_y', 'date_students_to_m', 'date_students_to_d');
//$calendar .= write_calendar('_eval_plan_from', 'date_eval_plan_from_y', 'date_eval_plan_from_m', 'date_eval_plan_from_d');
//$calendar .= write_calendar('_eval_plan_to', 'date_eval_plan_to_y', 'date_eval_plan_to_m', 'date_eval_plan_to_d');
$calendar .= write_calendar('_report_from', 'date_report_from_y', 'date_report_from_m', 'date_report_from_d');
$calendar .= write_calendar('_report_to', 'date_report_to_y', 'date_report_to_m', 'date_report_to_d');
//$calendar .= write_calendar('_eval_from', 'date_eval_from_y', 'date_eval_from_m', 'date_eval_from_d');
//$calendar .= write_calendar('_eval_to', 'date_eval_to_y', 'date_eval_to_m', 'date_eval_to_d');
$log->debug("calendar：".$calendar);

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);


/**
 * 研修区分HTML取得
 */
$training_div_options_str=get_training_div_options($_POST['training_div']);



/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";

/**
 * ラダーレベル
 */
$option_level = get_select_levels($_POST['now_level']);

/**
 * セクション名(部門・課・科・室)取得
 */
$pst_slct_bx_strct_str=get_post_select_box_strict(
	$con
	,$fname
	,$_POST['search_emp_class']
	,$_POST['search_emp_attribute']
	,$_POST['search_emp_dept']
	,$_POST['search_emp_room']
	,$obj
);

/**
 * 申請日
 */
$option_date_apply_from_y = cl_get_select_years($_POST['date_apply_from_y']);
$option_date_apply_from_m = cl_get_select_months($_POST['date_apply_from_m']);
$option_date_apply_from_d = cl_get_select_days($_POST['date_apply_from_d']);

$option_date_apply_to_y = cl_get_select_years($_POST['date_apply_to_y']);
$option_date_apply_to_m = cl_get_select_months($_POST['date_apply_to_m']);
$option_date_apply_to_d = cl_get_select_days($_POST['date_apply_to_d']);

/**
 * 受講日
 */
$option_date_students_from_y = cl_get_select_years($_POST['date_students_from_y']);
$option_date_students_from_m = cl_get_select_months($_POST['date_students_from_m']);
$option_date_students_from_d = cl_get_select_days($_POST['date_students_from_d']);

$option_date_students_to_y = cl_get_select_years($_POST['date_students_to_y']);
$option_date_students_to_m = cl_get_select_months($_POST['date_students_to_m']);
$option_date_students_to_d = cl_get_select_days($_POST['date_students_to_d']);

/**
 * 評価予定日
 */
$option_date_eval_plan_from_y = cl_get_select_years($_POST['date_eval_plan_from_y']);
$option_date_eval_plan_from_m = cl_get_select_months($_POST['date_eval_plan_from_m']);
$option_date_eval_plan_from_d = cl_get_select_days($_POST['date_eval_plan_from_d']);

$option_date_eval_plan_to_y = cl_get_select_years($_POST['date_eval_plan_to_y']);
$option_date_eval_plan_to_m = cl_get_select_months($_POST['date_eval_plan_to_m']);
$option_date_eval_plan_to_d = cl_get_select_days($_POST['date_eval_plan_to_d']);

/**
 * 報告日
 */
$option_date_report_from_y = cl_get_select_years($_POST['date_report_from_y']);
$option_date_report_from_m = cl_get_select_months($_POST['date_report_from_m']);
$option_date_report_from_d = cl_get_select_days($_POST['date_report_from_d']);

$option_date_report_to_y = cl_get_select_years($_POST['date_report_to_y']);
$option_date_report_to_m = cl_get_select_months($_POST['date_report_to_m']);
$option_date_report_to_d = cl_get_select_days($_POST['date_report_to_d']);

/**
 * 評価日
 */
$option_date_eval_from_y = cl_get_select_years($_POST['date_eval_from_y']);
$option_date_eval_from_m = cl_get_select_months($_POST['date_eval_from_m']);
$option_date_eval_from_d = cl_get_select_days($_POST['date_eval_from_d']);

$option_date_eval_to_y = cl_get_select_years($_POST['date_eval_to_y']);
$option_date_eval_to_m = cl_get_select_months($_POST['date_eval_to_m']);
$option_date_eval_to_d = cl_get_select_days($_POST['date_eval_to_d']);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/search/cl_training_progress_manager_model.php");
$progress_manager_model = new cl_training_progress_manager_model($mdb2,$emp_id);
$log->debug("研修受講進捗用DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
$education_committee_model = new cl_mst_nurse_education_committee_model($mdb2,$emp_id);
$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_setting_council_model.php");
$council_model = new cl_mst_setting_council_model($mdb2,$emp_id);
$log->debug("審議会DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
$manager_st_model = new cl_mst_nurse_manager_st_model($mdb2,$emp_id);
$log->debug("所属長DBA取得",__FILE__,__LINE__);

require_once("cl/model/master/cl_mst_supervisor_st_model.php");
$supervisor_st_model = new cl_mst_supervisor_st_model($mdb2,$emp_id);
$log->debug("看護部長DBA取得",__FILE__,__LINE__);

if ($_POST['regist_flg'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	define("DELETE_FLAG_PREFIX","delete_flag");

	$arr_field_name = array_keys($_POST);
	foreach($arr_field_name as $field_name){
		$pos = strpos($field_name,DELETE_FLAG_PREFIX);
		if ($pos === 0) {
			$arr_training_id = substr($field_name,strlen(DELETE_FLAG_PREFIX));
			if ($_POST[$field_name] != "") {
				$training_model->logical_delete($_POST[$field_name]);
			}
		}
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

$select_flg = true;

// 看護教育委員会チェック
$education_committee = $education_committee_model->check_education_committee($emp_id);

// ログインユーザーが看護教育委員会でない場合
if (count($education_committee) == 0){
	// 審議会チェック
	$council = $council_model->check_council($emp_id);

	// ログインユーザーが看護教育委員会・審議会で無い場合
	if (count($council) == 0){
		// 所属長(役職)チェック
		$supervisor = $supervisor_st_model->check_supervisor($emp_id);

		if (count($supervisor) > 0){
			// 所属長(部署)取得
			$supervisor_class = $progress_manager_model->get_supervisor_class();
		}

		// 看護部長(役職)チェック
		$nurse_manager = $manager_st_model->check_nurse_manager($emp_id);

		if (count($nurse_manager) > 0){
			// 看護部長(部署)取得
			$nurse_manager_class = $progress_manager_model->get_nurse_manager_class();
		}

		if (count($supervisor) == 0 && count($nurse_manager) == 0){
			$select_flg = false;
		} else {
			if(count($supervisor) == 0){
				$class_division = $nurse_manager_class['class_division'];
			} else if(count($nurse_manager) == 0){
				$class_division = $supervisor_class['class_division'];
			} else {
				if ($nurse_manager_class['class_division'] > $supervisor_class['class_division']){
					$class_division = $supervisor_class['class_division'];
				} else {
					$class_division = $nurse_manager_class['class_division'];
				}
			}
			$login_emp_id = $emp_id;
		}

	}
}

if ($select_flg && $_POST['research_flg'] == 'true') {
	if (
		$_POST['date_apply_from_y'] == ''
		|| $_POST['date_apply_from_m'] == ''
		|| $_POST['date_apply_from_d'] == ''
		|| $_POST['date_apply_from_y'] == '-'
		|| $_POST['date_apply_from_m'] == '-'
		|| $_POST['date_apply_from_d'] == '-'
	) {
		$apply_date_from = '';
	} else {
		$apply_date_from = $_POST['date_apply_from_y'].$_POST['date_apply_from_m'].$_POST['date_apply_from_d'];
	}

	if (
		$_POST['date_apply_to_y'] == ''
		|| $_POST['date_apply_to_m'] == ''
		|| $_POST['date_apply_to_d'] == ''
		|| $_POST['date_apply_to_y'] == '-'
		|| $_POST['date_apply_to_m'] == '-'
		|| $_POST['date_apply_to_d'] == '-'
	) {
		$apply_date_to = '';
	} else {
		$apply_date_to = $_POST['date_apply_to_y'].$_POST['date_apply_to_m'].$_POST['date_apply_to_d'];
	}

	if (
		$_POST['date_students_from_y'] == ''
		|| $_POST['date_students_from_m'] == ''
		|| $_POST['date_students_from_d'] == ''
		|| $_POST['date_students_from_y'] == '-'
		|| $_POST['date_students_from_m'] == '-'
		|| $_POST['date_students_from_d'] == '-'
	) {
		$students_date_from = '';
	} else {
		$students_date_from = $_POST['date_students_from_y'].$_POST['date_students_from_m'].$_POST['date_students_from_d'];
	}

	if (
		$_POST['date_students_to_y'] == ''
		|| $_POST['date_students_to_m'] == ''
		|| $_POST['date_students_to_d'] == ''
		|| $_POST['date_students_to_y'] == '-'
		|| $_POST['date_students_to_m'] == '-'
		|| $_POST['date_students_to_d'] == '-'
	) {
		$students_date_to = '';
	} else {
		$students_date_to = $_POST['date_students_to_y'].$_POST['date_students_to_m'].$_POST['date_students_to_d'];
	}

	if (
		$_POST['date_eval_plan_from_y'] == ''
		|| $_POST['date_eval_plan_from_m'] == ''
		|| $_POST['date_eval_plan_from_d'] == ''
		|| $_POST['date_eval_plan_from_y'] == '-'
		|| $_POST['date_eval_plan_from_m'] == '-'
		|| $_POST['date_eval_plan_from_d'] == '-'
	) {
		$eval_plan_date_from = '';
	} else {
		$eval_plan_date_from = $_POST['date_eval_plan_from_y'].$_POST['date_eval_plan_from_m'].$_POST['date_eval_plan_from_d'];
	}

	if (
		$_POST['date_eval_plan_to_y'] == ''
		|| $_POST['date_eval_plan_to_m'] == ''
		|| $_POST['date_eval_plan_to_d'] == ''
		|| $_POST['date_eval_plan_to_y'] == '-'
		|| $_POST['date_eval_plan_to_m'] == '-'
		|| $_POST['date_eval_plan_to_d'] == '-'
	) {
		$eval_plan_date_to = '';
	} else {
		$eval_plan_date_to = $_POST['date_eval_plan_to_y'].$_POST['date_eval_plan_to_m'].$_POST['date_eval_plan_to_d'];
	}

	if (
		$_POST['date_report_from_y'] == ''
		|| $_POST['date_report_from_m'] == ''
		|| $_POST['date_report_from_d'] == ''
		|| $_POST['date_report_from_y'] == '-'
		|| $_POST['date_report_from_m'] == '-'
		|| $_POST['date_report_from_d'] == '-'
	) {
		$report_date_from = '';
	} else {
		$report_date_from = $_POST['date_report_from_y'].$_POST['date_report_from_m'].$_POST['date_report_from_d'];
	}

	if (
		$_POST['date_report_to_y'] == ''
		|| $_POST['date_report_to_m'] == ''
		|| $_POST['date_report_to_d'] == ''
		|| $_POST['date_report_to_y'] == '-'
		|| $_POST['date_report_to_m'] == '-'
		|| $_POST['date_report_to_d'] == '-'
	) {
		$report_date_to = '';
	} else {
		$report_date_to = $_POST['date_report_to_y'].$_POST['date_report_to_m'].$_POST['date_report_to_d'];
	}

	if (
		$_POST['date_eval_from_y'] == ''
		|| $_POST['date_eval_from_m'] == ''
		|| $_POST['date_eval_from_d'] == ''
		|| $_POST['date_eval_from_y'] == '-'
		|| $_POST['date_eval_from_m'] == '-'
		|| $_POST['date_eval_from_d'] == '-'
	) {
		$eval_date_from = '';
	} else {
		$eval_date_from = $_POST['date_eval_from_y'].$_POST['date_eval_from_m'].$_POST['date_eval_from_d'];
	}

	if (
		$_POST['date_eval_to_y'] == ''
		|| $_POST['date_eval_to_m'] == ''
		|| $_POST['date_eval_to_d'] == ''
		|| $_POST['date_eval_to_y'] == '-'
		|| $_POST['date_eval_to_m'] == '-'
		|| $_POST['date_eval_to_d'] == '-'
	) {
		$eval_date_to = '';
	} else {
		$eval_date_to = $_POST['date_eval_to_y'].$_POST['date_eval_to_m'].$_POST['date_eval_to_d'];
	}

	$param = array(
		'now_level'				=> $_POST['now_level']
		,'emp_nm'				=> $_POST['emp_nm']
		,'class_id'				=> $_POST['search_emp_class']
		,'atrb_id'				=> $_POST['search_emp_attribute']
		,'dept_id'				=> $_POST['search_emp_dept']
		,'room_id'				=> $_POST['search_emp_room']
		,'training_id'			=> $_POST['training_id']
		,'training_name'		=> $_POST['training_name']
		,'teacher_name'			=> $_POST['teacher_name']
		,'apply_date_from'		=> $apply_date_from
		,'apply_date_to'		=> $apply_date_to
		,'students_date_from'	=> $students_date_from
		,'students_date_to'		=> $students_date_to
		,'eval_plan_date_from'	=> $eval_plan_date_from
		,'eval_plan_date_to'	=> $eval_plan_date_to
		,'report_date_from'		=> $report_date_from
		,'report_date_to'		=> $report_date_to
		,'eval_date_from'		=> $eval_date_from
		,'eval_date_to'			=> $eval_date_to
		,'login_emp_id'			=> $login_emp_id
		,'class_division'		=> $class_division
		,'training_div'			=> $_POST['training_div']
		// 2012/08/27 Yamagawa add(s)
		,'retire_disp_flg'		=> $_POST['retire_disp_flg']
		// 2012/08/27 Yamagawa add(e)
		);

	// 一覧取得
	if(
		$_POST['training_id'] == ''
		&& $_POST['training_name'] == ''
		&& $_POST['teacher_name'] == ''
		&& $apply_date_from == ''
		&& $apply_date_to == ''
		&& $students_date_from == ''
		&& $students_date_to == ''
		&& $eval_plan_date_from == ''
		&& $eval_plan_date_to == ''
		&& $report_date_from == ''
		&& $report_date_to == ''
		&& $eval_date_from == ''
		&& $eval_date_to == ''
		&& $training_div == ''
		) {
		$data = $progress_manager_model->get_user_list($param);
	} else {
		$data = $progress_manager_model->get_training_list($param);
	}
}

for ($i = 0; $i < count($data); $i++){

	// 氏名
	$data[$i]['emp_full_nm']  = $data[$i]['emp_lt_nm'];
	$data[$i]['emp_full_nm'] .= '　';
	$data[$i]['emp_full_nm'] .= $data[$i]['emp_ft_nm'];

	// 所属
	$data[$i]['affiliation']  = $data[$i]['class_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['atrb_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['dept_nm'];
	if ($data[$i]['room_nm'] != '') {
		$data[$i]['affiliation'] .= ' > ';
		$data[$i]['affiliation'] .= $data[$i]['room_nm'];
	}

	// レベル
	switch($data[$i]['now_level']){
		case 1:
			$data[$i]['now_level'] = 'I';
			break;
		case 2:
			$data[$i]['now_level'] = 'II';
			break;
		case 3:
			$data[$i]['now_level'] = 'III';
			break;
		case 4:
			$data[$i]['now_level'] = 'IV';
			break;
		case 5:
			$data[$i]['now_level'] = 'V';
			break;
		default:
			$data[$i]['now_level'] = '';
	}

	// 講師名
	$data[$i]['teacher_name'] = '';
	if ($data[$i]['teacher1_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher1_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher1_ft_nm'];
	}
	if ($data[$i]['teacher2_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher2_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher2_ft_nm'];
	}
	if ($data[$i]['teacher3_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher3_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher3_ft_nm'];
	}
	if ($data[$i]['teacher4_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher4_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher4_ft_nm'];
	}
	if ($data[$i]['teacher5_lt_nm'] != ''){
		if ($data[$i]['teacher_name'] != ''){
			$data[$i]['teacher_name'] .= ',';
		}
		$data[$i]['teacher_name'] .= $data[$i]['teacher5_lt_nm'];
		$data[$i]['teacher_name'] .= '　';
		$data[$i]['teacher_name'] .= $data[$i]['teacher5_ft_nm'];
	}

	// 申請日
	if ($data[$i]['apply_date'] != ''){
		$apply_date_year = substr($data[$i]['apply_date'],0,4);
		$apply_date_month = substr($data[$i]['apply_date'],5,2);
		$apply_date_day = substr($data[$i]['apply_date'],8,2);
		$data[$i]['apply_date']  = $apply_date_year;
		$data[$i]['apply_date'] .= '/';
		$data[$i]['apply_date'] .= $apply_date_month;
		$data[$i]['apply_date'] .= '/';
		$data[$i]['apply_date'] .= $apply_date_day;
	}

	// 受講日
	if ($data[$i]['plan_date'] != ''){
		$plan_date_year = substr($data[$i]['plan_date'],0,4);
		$plan_date_month = substr($data[$i]['plan_date'],5,2);
		$plan_date_day = substr($data[$i]['plan_date'],8,2);
		$data[$i]['plan_date'] = $plan_date_year;
		$data[$i]['plan_date'] .= '/';
		$data[$i]['plan_date'] .= $plan_date_month;
		$data[$i]['plan_date'] .= '/';
		$data[$i]['plan_date'] .= $plan_date_day;
	} else {
		$data[$i]['plan_date'] = '';
		if ($data[$i]['from_date'] != ''){
			$from_date_year = substr($data[$i]['from_date'],0,4);
			$from_date_month = substr($data[$i]['from_date'],5,2);
			$from_date_day = substr($data[$i]['from_date'],8,2);
			$data[$i]['plan_date'] .= $from_date_year;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $from_date_month;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $from_date_day;
		}
		if ($data[$i]['to_date'] != ''){
			$to_date_year = substr($data[$i]['to_date'],0,4);
			$to_date_month = substr($data[$i]['to_date'],5,2);
			$to_date_day = substr($data[$i]['to_date'],8,2);
			$data[$i]['plan_date'] .= ' - ';
			$data[$i]['plan_date'] .= $to_date_year;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $to_date_month;
			$data[$i]['plan_date'] .= '/';
			$data[$i]['plan_date'] .= $to_date_day;
		}
	}

	// 報告日
	if ($data[$i]['report_date'] != ''){
		$report_date_year = substr($data[$i]['report_date'],0,4);
		$report_date_month = substr($data[$i]['report_date'],4,2);
		$report_date_day = substr($data[$i]['report_date'],6,2);
		$data[$i]['report_date']  = $report_date_year;
		$data[$i]['report_date'] .= '/';
		$data[$i]['report_date'] .= $report_date_month;
		$data[$i]['report_date'] .= '/';
		$data[$i]['report_date'] .= $report_date_day;
	}

}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

/**
 * テンプレートマッピング
 */

$smarty->assign( 'cl_title'						, $cl_title						);
$smarty->assign( 'session'						, $session						);
$smarty->assign( 'yui_cal_part'					, $yui_cal_part					);
$smarty->assign( 'calendar'						, $calendar						);
$smarty->assign( 'workflow_auth'				, $workflow_auth				);
$smarty->assign( 'aplyctn_mnitm_str'			, $aplyctn_mnitm_str			);
$smarty->assign( 'js_str'						, $js_str						);

$smarty->assign( 'option_level'					, $option_level					);
$smarty->assign( 'emp_nm'						, $emp_nm						);
$smarty->assign( 'pst_slct_bx_strct_str'		, $pst_slct_bx_strct_str		);
$smarty->assign( 'training_id'					, $training_id					);
$smarty->assign( 'training_name'				, $training_name				);
$smarty->assign( 'teacher_name'					, $teacher_name					);
$smarty->assign( 'option_date_apply_from_y'		, $option_date_apply_from_y		);
$smarty->assign( 'option_date_apply_from_m'		, $option_date_apply_from_m		);
$smarty->assign( 'option_date_apply_from_d'		, $option_date_apply_from_d		);
$smarty->assign( 'option_date_apply_to_y'		, $option_date_apply_to_y		);
$smarty->assign( 'option_date_apply_to_m'		, $option_date_apply_to_m		);
$smarty->assign( 'option_date_apply_to_d'		, $option_date_apply_to_d		);
$smarty->assign( 'option_date_students_from_y'	, $option_date_students_from_y	);
$smarty->assign( 'option_date_students_from_m'	, $option_date_students_from_m	);
$smarty->assign( 'option_date_students_from_d'	, $option_date_students_from_d	);
$smarty->assign( 'option_date_students_to_y'	, $option_date_students_to_y	);
$smarty->assign( 'option_date_students_to_m'	, $option_date_students_to_m	);
$smarty->assign( 'option_date_students_to_d'	, $option_date_students_to_d	);
$smarty->assign( 'option_date_eval_plan_from_y'	, $option_date_eval_plan_from_y	);
$smarty->assign( 'option_date_eval_plan_from_m'	, $option_date_eval_plan_from_m	);
$smarty->assign( 'option_date_eval_plan_from_d'	, $option_date_eval_plan_from_d	);
$smarty->assign( 'option_date_eval_plan_to_y'	, $option_date_eval_plan_to_y	);
$smarty->assign( 'option_date_eval_plan_to_m'	, $option_date_eval_plan_to_m	);
$smarty->assign( 'option_date_eval_plan_to_d'	, $option_date_eval_plan_to_d	);
$smarty->assign( 'option_date_report_from_y'	, $option_date_report_from_y	);
$smarty->assign( 'option_date_report_from_m'	, $option_date_report_from_m	);
$smarty->assign( 'option_date_report_from_d'	, $option_date_report_from_d	);
$smarty->assign( 'option_date_report_to_y'		, $option_date_report_to_y		);
$smarty->assign( 'option_date_report_to_m'		, $option_date_report_to_m		);
$smarty->assign( 'option_date_report_to_d'		, $option_date_report_to_d		);
$smarty->assign( 'option_date_eval_from_y'		, $option_date_eval_from_y		);
$smarty->assign( 'option_date_eval_from_m'		, $option_date_eval_from_m		);
$smarty->assign( 'option_date_eval_from_d'		, $option_date_eval_from_d		);
$smarty->assign( 'option_date_eval_to_y'		, $option_date_eval_to_y		);
$smarty->assign( 'option_date_eval_to_m'		, $option_date_eval_to_m		);
$smarty->assign( 'option_date_eval_to_d'		, $option_date_eval_to_d		);
$smarty->assign( 'training_div_options_str'		, $training_div_options_str		);//検索条件:研修区分のHTML
// 2012/08/27 Yamagawa add(s)
if ($_POST['retire_disp_flg'] == '') {
	$retire_disp_flg = '';
} else {
	$retire_disp_flg = 'checked';
}
$smarty->assign( 'retire_disp_flg'				, $retire_disp_flg				);
// 2012/08/27 Yamagawa add(e)

$smarty->assign( 'data'							, $data							);

$log->debug("テンプレートマッピング",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート：".basename($_SERVER['PHP_SELF'],'.php').".tpl",__FILE__,__LINE__);
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");
$log->debug("テンプレート出力",__FILE__,__LINE__);

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function get_select_around_years($date)
{
	ob_start();
	show_years_around(1, 1, $date);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 年オプションHTML取得(報告日用)
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 必須レベルオプションHTML取得
 */
function get_select_levels($level)
{

	ob_start();
	show_levels_options($level);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

/**
 * 研修区分HTML取得
 */
function get_training_div_options($training_div)
{
	ob_start();
	show_training_div_options($training_div);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

function show_levels_options($level) {

	$arr_levels_nm = array("","I","II","III","IV","V");
	$arr_levels_id = array("","1","2","3","4","5");

	for($i=0;$i<count($arr_levels_nm);$i++) {

		echo("<option value=\"$arr_levels_id[$i]\"");
		if($level == $arr_levels_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_levels_nm[$i]\n");
	}
}


// 研修区分オプションを出力
function show_training_div_options($val)
{
	$arr_txt = array("院内研修","院外研修");
	$arr_val = array("1","2");

	echo("<option value=\"\">すべて");
	for($i=0;$i<count($arr_txt);$i++) {

		echo("<option value=\"$arr_val[$i]\"");
		if($val == $arr_val[$i]) {
			echo(" selected");
		}
		echo(">$arr_txt[$i]\n");
	}
}


/**
 * セクション名(部門・課・科・室)取得
 */
function get_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj)
{
	ob_start();
	show_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj, "", true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

	ob_start();
	write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
	$str_buff = ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

function show_years_around($before_count, $after_count, $num){

	$now  = date(Y);
	if ($num == ''){
		$num = $now;
	}

	// 当年まで
	for($i=0; $i<=$before_count;$i++){
		$yr = $now - $before_count + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

	// 翌年から
	for($i=1; $i<=$after_count;$i++){
		$yr = $now + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

}

$log->info(basename(__FILE__)." END");
$log->shutdown();
