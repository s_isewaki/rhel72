<?php
//************************************************************************
// 勤務シフト作成　勤務シフト作成画面
// 自動シフト作成(分散)頻度分布ＣＬＡＳＳ
//************************************************************************
require_once("about_postgres.php");

class duty_shift_auto_hindo_class
{
	/*************************************************************************/
	// コンストラクタ
	/*************************************************************************/
	function duty_shift_auto_hindo_class($group_id, $start_date, $end_date, $staff_table, $atdptn_table, $con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		
		// 頻度テーブル
		$this->_create_hindo_table($group_id, $start_date, $end_date, $staff_table, $atdptn_table);
		
	}
	/*************************************************************************/
	// 常勤・非常勤判定用 2012.5.16
	/*************************************************************************/
	function set_duty_form( $_arg ){
		$this->duty_form_type = $_arg;
	}
	/*************************************************************************/
	// 発生頻度分布テーブルの取得
	/*************************************************************************/
	function _create_hindo_table($group_id, $start_date, $end_date, $staff_table, $atdptn_table)
	{
		//staff_list
		$staff_array = array();
		foreach ( $staff_table as $emp_id => $staff ) {
			$staff_array[] = sprintf("'%s'", $emp_id);
		}
		$staff_list = implode(",",$staff_array);
		
		//SQL実行
		$hindo_table = array();
		if (!empty($staff_list)) {
			$sql = <<<_SQL_END_
SELECT
	emp_id,
	CASE
		WHEN c1.type='6' THEN '7'
		WHEN c2.type='6' THEN '8'
		ELSE DATE_PART('DOW',TO_DATE(a.date,'YYYYMMDD'))
	END AS week,
	pattern,
	COUNT(*) AS count
FROM atdbk a
LEFT JOIN calendar c1 ON a.date=c1.date
LEFT JOIN calendar c2 ON c2.date=TO_CHAR(TO_DATE(c1.date,'YYYYMMDD') + interval '1 days','YYYYMMDD')
_SQL_END_;
			$cond = <<<_COND_END_
WHERE emp_id IN ($staff_list)
AND a.date BETWEEN '$start_date' AND '$end_date'
GROUP BY emp_id,pattern,week
_COND_END_;
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//頻度テーブルの作成
			while ( $row = pg_fetch_assoc($sel) ) {
				if ( !empty($row["pattern"]) ) {
					$hindo_table[ $row["week"] ][ $row["pattern"] ][ $row["emp_id"] ] = (int)$row["count"];
				}
			}
		}
		
		//頻度テーブルから漏れているスタッフ、曜日、勤務パターンにゼロを入れる
		for($week=0; $week<=8; $week++) {
			foreach($atdptn_table as $pattern => $row){
				foreach($staff_table as $emp_id => $staff) {
					if (empty($hindo_table[ $week ][ $pattern ][ $emp_id ])) {
						$hindo_table[ $week ][ $pattern ][ $emp_id ] = 0;
					}
				}
			} 
		}
		$this->_hindo_table = $hindo_table;
	}
	
	/*******************************************************************************/
	// 頻度テーブルを集計して、多中少のグループを作成する。非常勤職員対応 2012.5.16
	/*******************************************************************************/
	function load_hindo_group() {
		$group_s = array();
		$group_m = array();
		$group_l = array();
		$group_p = array(); // 非常勤職員対応 2012.5.17 [p]art timer
		foreach($this->_hindo_table as $week => $pattern_table){
			foreach($pattern_table as $pattern => $table){
				$count = (int)(count($table) / 3);
				asort($table);
				$group_s[ $week ][ $pattern ] = array_slice(array_keys($table), 0, $count);
				$group_m[ $week ][ $pattern ] = array_slice(array_keys($table), $count, $count);
				$group_l[ $week ][ $pattern ] = array_slice(array_keys($table), $count*2);
				// ▽非常勤職員対応 2012.5.17▽
				//   非常勤職員は職員リストから非常勤職員リストへ移動させる
				$num_cnt=0;
				foreach($group_s[ $week ][ $pattern ] as $num=>$wk_emp_id ){
					if( $this->duty_form_type[$wk_emp_id] == 2 ){ 		// 非常勤職員のとき
						$group_p[ $week ][ $pattern ][$num_cnt] = $wk_emp_id;
						unset ($group_s[ $week ][ $pattern ][$num]);
						$num_cnt++;
					}
				}
				foreach($group_m[ $week ][ $pattern ] as $num=>$wk_emp_id ){
					if( $this->duty_form_type[$wk_emp_id] == 2 ){ 		// 非常勤職員のとき
						$group_p[ $week ][ $pattern ][$num_cnt] = $wk_emp_id;
						unset ($group_m[ $week ][ $pattern ][$num]);
						$num_cnt++;
					}
				}
				foreach($group_l[ $week ][ $pattern ] as $num=>$wk_emp_id ){
					if( $this->duty_form_type[$wk_emp_id] == 2 ){ 		// 非常勤職員のとき
						$group_p[ $week ][ $pattern ][$num_cnt] = $wk_emp_id;
						unset ($group_l[ $week ][ $pattern ][$num]);
						$num_cnt++;
					}
				}
				// △非常勤職員対応ここまで△
			} 
		}
		$this->_group_s = $group_s;
		$this->_group_m = $group_m;
		$this->_group_l = $group_l;
		$this->_group_p = $group_p;	// 非常勤職員対応 2012.5.17
	}
	
	/*************************************************************************/
	// ランダムに並べたスタッフリストの取得
	/*************************************************************************/
	function get_staff_array($week, $pattern) {
		shuffle($this->_group_s[$week][$pattern]);
		shuffle($this->_group_m[$week][$pattern]);
		shuffle($this->_group_l[$week][$pattern]);
		shuffle($this->_group_p[$week][$pattern]);	// 非常勤職員対応 2012.5.17
		
		// 非常勤職員対応、不具合対応 2012.7.13
		if ( empty($this->_group_p[$week][$pattern]) ){
			$ret =  array_merge($this->_group_s[$week][$pattern], 
					    $this->_group_m[$week][$pattern], 
					    $this->_group_l[$week][$pattern]);
		}else{
			$ret =  array_merge(
				$this->_group_s[$week][$pattern], 
				$this->_group_m[$week][$pattern], 
				$this->_group_l[$week][$pattern],
				$this->_group_p[$week][$pattern]
				);
		}
		return $ret;
	}
	
	/*************************************************************************/
	// 頻度加算
	/*************************************************************************/
	function plus($week, $pattern, $emp_id) {
		$this->_hindo_table[$week][$pattern][$emp_id]++;
	}
	
	/*************************************************************************/
	// 頻度減算
	/*************************************************************************/
	function minus($week, $pattern, $emp_id) {
		$this->_hindo_table[$week][$pattern][$emp_id]--;
	}
	
	/*************************************************************************/
	// 頻度テーブル
	/*************************************************************************/
	function get_hindo_table() {
		return $this->_hindo_table;
	}
}