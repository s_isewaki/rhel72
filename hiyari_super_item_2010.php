<?
//==============================
//本処理
//==============================

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0")
{
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// $edit_item_list = get_super_item($con, $fname);
//==============================
//選択項目情報の取得
//==============================
if($is_postback != "true")
{
    $edit_item_list = get_super_item($con, $fname);
    $line_count = count($edit_item_list);

    $other_flg = null;
    for($line_no=0;$line_no<$line_count;$line_no++){
      if ($edit_item_list[$line_no]["other_code"]>0){
          $other_flg = $edit_item_list[$line_no]['super_item_order'];
      }
    }
}

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
    //==============================
    //ポストバックデータより現在の表示内容を作成
    //==============================
    $edit_item_list = "";
    $tmp_other_flg = null;
    for($line_no=0;$line_no<$line_count;$line_no++)
    {
        $p_item_id     = "super_item_id_".$line_no;
        $p_item_num    = "super_item_num_".$line_no;
        $p_item_order  = "super_item_order_".$line_no;
        $p_item_name   = "super_item_name_".$line_no;
        $p_disp_flg    = "disp_flg_".$line_no;
        $p_available   = "available_".$line_no;
        $p_code        = "code_".$line_no;
        $p_export_code = "export_code_".$line_no;

        $tmp_array = array();

        $tmp_array["super_item_id"]    = $$p_item_id;
        $tmp_array["super_item_num"]   = $$p_item_num;
        $tmp_array["super_item_order"] = $line_no+1;
        $tmp_array["super_item_name"]  = $$p_item_name;
        $tmp_array["disp_flg"]         = $$p_disp_flg != 'true' ? 't' : 'f';
        $tmp_array["available"]        = $$p_available;
        $tmp_array["code"]             = $$p_code;
        $tmp_array["export_code"]      = $$p_export_code;

        $edit_item_list[$line_no] = $tmp_array;

        if ($other_flg==$$p_item_order){
          $tmp_other_flg = $tmp_array["super_item_order"];
        }
    }
    $other_flg=$tmp_other_flg;

    //==============================
    //行追加
    //==============================
    if($postback_mode == "add_line")
    {
        $tmp_array = array();

        $tmp_array["super_item_id"]    = '';
        $tmp_array["super_item_num"]   = '';
        $tmp_array["super_item_order"] = $line_count+1;
        $tmp_array["super_item_name"]  = '';
        $tmp_array["disp_flg"]         = 't';
        $tmp_array["available"]        = 't';
        $tmp_array["code"]             = '';
        $tmp_array["export_code"]      = '';

        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //既存行に割り込み追加の場合
            if($add_line_no == $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $tmp_array;
                $new_line_no++;
            }

            $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
            $new_line_no++;
        }

        //最終行に追加の場合
        if($add_line_no == $line_count)
        {
            $tmp_edit_item_list[$add_line_no] = $tmp_array;
            $new_line_no++;
        }

        $edit_item_list = $tmp_edit_item_list;
        $line_count++;

    }
    //==============================
    //行削除
    //==============================
    elseif($postback_mode == "delete_line")
    {
        $new_line_no = 0;
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            if($delete_line_no != $line_no)
            {
                $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                $new_line_no++;
            }
        }
        $edit_item_list = $tmp_edit_item_list;
        $line_count--;
    }
    //==============================
    //順番変更
    //==============================
    elseif($postback_mode == "change_line_no")
    {
        $tmp_edit_item_list = array();
        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            //移動するデータを先に割り当てる。
            $tmp_edit_item_list[$change_line_no_after_line_no] = $edit_item_list[$change_line_no_target_line_no];

            //移動するデータ以外を割り当てる
            if($line_no != $change_line_no_target_line_no)
            {
                //先頭方向に移動する場合
                if($change_line_no_target_line_no > $change_line_no_after_line_no)
                {
                    //移動元-移動先間は末尾方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no > $line_no && $change_line_no_after_line_no <= $line_no)
                    {
                        $new_line_no++;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //末尾方向に移動する場合
                elseif($change_line_no_target_line_no < $change_line_no_after_line_no)
                {
                    //移動元-移動先間は先頭方向へ１移動。それ以外は移動なし。
                    $new_line_no = $line_no;
                    if($change_line_no_target_line_no < $line_no && $change_line_no_after_line_no >= $line_no)
                    {
                        $new_line_no--;
                    }
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }
                //移動しない場合(ありえない)
                else
                {
                    //移動しない。
                    $new_line_no = $line_no;
                    $tmp_edit_item_list[$new_line_no] = $edit_item_list[$line_no];
                }

            }
        }
        $edit_item_list = $tmp_edit_item_list;
    }
    //==============================
    //ＤＢ更新
    //==============================
    elseif($postback_mode == "update")
    {
        // 更新する前に、一度情報を無効化する
        $sql = "UPDATE inci_super_item_2010 SET ";
        $set = array('available');
        $setvalue = array('f');
        unset($cond);

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        for($line_no=0;$line_no<$line_count;$line_no++)
        {
            // データを取得
            $update_item = $edit_item_list[$line_no];
            // データを変数に挿入
            $super_item_id    = $update_item['super_item_id'];
            $super_item_num   = $update_item['super_item_num'];
            $super_item_order = $line_no+1;
            $super_item_name  = $update_item['super_item_name'];
            $disp_flg         = $update_item['disp_flg'];
            $available        = $update_item['available'];
            $code             = $update_item['code'];
            $export_code      = $update_item['export_code'];
            $other_code       = ($other_flg == $update_item['super_item_order']) ? 20:null;

            // super_item_idが設定されていればUPDATE
            if($super_item_id) {
                $sql = "UPDATE inci_super_item_2010 SET ";
                if ($code != "") {
        					$set = array('super_item_order', 'super_item_name', 'disp_flg', 'available', 'other_code');
        					$setvalue = array($super_item_order, pg_escape_string($super_item_name), $disp_flg, $available, $other_code);
                }
                else {
                  $set = array('super_item_order', 'super_item_name', 'disp_flg', 'available', 'export_code', 'other_code');
                  $setvalue = array($super_item_order, pg_escape_string($super_item_name), $disp_flg, $available, $export_code, $other_code);
                }
                $cond = "WHERE super_item_id = '{$super_item_id}'";

                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                if ($upd == 0) {
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            // 新規はINSERT
            } else {
                // item_idのMAX値を取得する
                $sql = "SELECT max(super_item_id) FROM inci_super_item_2010";
                $cond = "";
                $sel = select_from_table($con,$sql,$cond,$fname);
                $max = pg_result($sel,0,"max");
                if($max == ""){
                    $super_item_id = "1";
                }else{
                    $super_item_id = $max + 1;
                }

                $sql = "INSERT INTO inci_super_item_2010(super_item_id, super_item_order, super_item_name, disp_flg, available, export_code, other_code) VALUES(";
                $cond = array($super_item_id, $super_item_order, pg_escape_string($super_item_name), $disp_flg, 't', $export_code, $other_code);

                $ins = insert_into_table($con, $sql, $cond, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }
        }
        
        $message = "更新処理を行いました";
    }
}

//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

//起動時の処理
function loadaction()
{

<?
//更新時エラーがある場合
if($error_message != "")
{
?>
alert("<?=$error_message?>");
<?
}
?>




<?
if($postback_mode == "add_line")
{
?>
var focus_obj_name = "super_item_name_<?=$add_line_no?>";
var focus_obj = document.getElementsByName(focus_obj_name)[0];
focus_obj.focus();
<?
}
?>




















}

//ＤＢ更新処理
function update_data()
{
    if(update_check())
    {
        document.form1.postback_mode.value = "update";
        document.form1.submit();
    }
}

//順番変更処理
function list_change_line_no(change_line_no_target_line_no,change_line_no_after_line_no)
{
    document.form1.postback_mode.value = "change_line_no";
    document.form1.change_line_no_target_line_no.value = change_line_no_target_line_no;
    document.form1.change_line_no_after_line_no.value = change_line_no_after_line_no;
    document.form1.submit();
}

//行削除処理
function list_delete_line(line_no)
{
    document.form1.postback_mode.value = "delete_line";
    document.form1.delete_line_no.value = line_no;
    document.form1.submit();
}

//行追加処理
function list_add_line(line_no)
{
    document.form1.postback_mode.value = "add_line";
    document.form1.add_line_no.value = line_no;
    document.form1.submit();
}

//削除-使用済みチェック/全非表示チェック
function delete_check(line_no,used_item_flg)
{
    if(used_item_flg)
    {
        alert("既に使用されている項目は削除できません。");
        return false;
    }

    return true;
}
//非表示-全非表示チェック
function disp_flg_change_check(disp_flg_obj)
{
    var all_no_disp_flg = true;
    var objs = document.getElementsByTagName("input");
    for (var i = 0, j = objs.length; i < j; i++)
    {
        var obj = objs[i];
        if(obj.name.indexOf("disp_flg_") != -1)
        {
            if(!obj.checked)
            {
                all_no_disp_flg = false;
                break;
            }
        }
    }

    if(all_no_disp_flg)
    {
        disp_flg_obj.checked = false;
        alert("全ての項目を非表示にすることはできません。");
        return false;
    }
    return true;
}
function update_check()
{
<?
for($line_no=0;$line_no<$line_count;$line_no++)
{
?>
    if(document.form1.super_item_name_<?=$line_no?>.value == "")
    {
        alert("表示内容が未入力です。");
        return false;
    }
<?
}
?>
    return true;
}


</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="loadaction()">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="super_item_id" value="<?=$super_item_id?>">
<input type="hidden" name="super_item_num" value="<?=$super_item_num?>">
<input type="hidden" name="super_item_order" value="<?=$super_item_order?>">
<input type="hidden" name="super_item_name" value="<?=$super_item_name?>">

<input type="hidden" name="line_count" value="<?=$line_count?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="change_line_no_after_line_no" value="">
<input type="hidden" name="change_line_no_target_line_no" value="">
<input type="hidden" name="delete_line_no" value="">
<input type="hidden" name="add_line_no" value="">



<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
    $PAGE_TITLE = "項目管理";
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<? // ヘッダータブ表示
show_item_edit_header_tab($session, $fname, $grp_code, $easy_item_code);
?>

<!-- メイン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>

<!--// トップリンク START -->
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="padding:3px 6px; background-color:#35B341; border:1px solid #ccc;">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<a href="hiyari_super_item_2010.php?session=<?=$session?>" style="color:#FFFFFF">概要</a>
			</font>
		</td>
		<td width="5"></td>
		<td style="padding:3px 6px; background-color:#f4f4f4; border:1px solid #ccc;">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<a href="hiyari_item_2010.php?session=<?=$session?>&item_code=kind&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">種類</a>
			</font>
		</td>
		<td width="5"></td>
		<td style="padding:3px 6px; background-color:#f4f4f4; border:1px solid #ccc;">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<a href="hiyari_item_2010.php?session=<?=$session?>&item_code=scene&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">場面</a>
			</font>
		</td>
		<td width="5"></td>
		<td style="padding:3px 6px; background-color:#f4f4f4; border:1px solid #ccc;">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<a href="hiyari_item_2010.php?session=<?=$session?>&item_code=content&id=<?= empty($_GET['id']) ? 1 : $_GET['id']?>">内容</a>
			</font>
		</td>
	</tr>
</table>
<!--// トップリンク END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="800" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">


<!-- 上部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><?=htmlspecialchars($easy_item_name)?></b></font>
</td>
<td>
    &nbsp
</td>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 上部更新ボタン等 START-->



<!-- 一覧 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
    <td width="300" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示内容</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示順</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コード</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行削除</font>
    </td>
    <td width="70" align="center" bgcolor="#DFFFDC" nowrap>
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行追加</font>
    </td>
</tr>

<?
// defaultのコードリスト取得
$code_list = get_default_code($con, $fname);

for($line_no=0;$line_no<$line_count;$line_no++)
{
    $edit_item = $edit_item_list[$line_no];
    $super_item_id    = $edit_item["super_item_id"];
    $super_item_num   = $edit_item["super_item_num"];
    $super_item_order = $edit_item["super_item_order"];
    $super_item_name  = $edit_item["super_item_name"];
    $disp_flg         = $edit_item["disp_flg"] == 't';
    $available        = $edit_item["available"];
    $code             = $edit_item["code"];
    $export_code      = $edit_item["export_code"];

    //$used_item_flg     = in_array($super_item_id,$used_super_item_id_list);
    $used_item_flg = false;
    if($super_item_id) $used_item_flg = get_judge_item($con, $fname, $super_item_id);

    //非表示欄のチェック状態のHTML
    $no_disp_checked_html = "";
    if(!$disp_flg)
    {
        $no_disp_checked_html = "checked";
    }

    //初期項目フラグの値のHTML
    $default_item_flg_value_html = "false";
    if($default_item_flg)
    {
        $default_item_flg_value_html = "true";
    }

    $used_item_flg_script = "false";
    if($used_item_flg)
    {
        $used_item_flg_script = "true";
    }
    
    //その他のチェック状態
    $other_flg_checked_html = "";
    if (!empty($other_flg)){
      if ($other_flg==$super_item_order) {
          $other_flg_checked_html = 'checked';
      }
    }
?>
<input type="hidden" name="super_item_id_<?=$line_no?>" value="<?=$super_item_id?>">
<input type="hidden" name="super_item_num_<?=$line_no?>" value="<?=$super_item_num?>">
<input type="hidden" name="available_<?=$line_no?>" value="<?=$available?>">
<input type="hidden" name="code_<?=$line_no?>" value="<?=$code?>">
<input type="hidden" name="super_item_order_<?=$line_no?>" value="<?=$super_item_order?>">
<!--//<input type="hidden" name="default_item_flg_<?=$line_no?>" value="<?=$default_item_flg_value_html?>">-->
<tr height="22">

<td align="left" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="text" name="super_item_name_<?=$line_no?>" value="<?=htmlspecialchars($super_item_name)?>" style="width:290px" maxlength="50">
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="radio" name="other_flg" value="<?=$super_item_order?>" <?=$other_flg_checked_html?>>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="checkbox" name="disp_flg_<?=$line_no?>" value="true" <?=$no_disp_checked_html?> onclick="disp_flg_change_check(this);" >
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if($line_no-1 > -1)
{
?>
    <img src="img/up.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no-1?>');" style="cursor: pointer;">
<?
}
?>
<?
if($line_no+1 < $line_count)
{
?>
    <img src="img/down.gif" onclick="list_change_line_no('<?=$line_no?>','<?=$line_no+1?>');" style="cursor: pointer;">
<?
}
?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
	<?
	if ($code == "") {
	?>
		<select name="export_code_<?=$line_no?>">
			<?
			for ($i = 0; $i < count($code_list); $i++) {
				$tmp_code = $code_list[$i]["code"];
				$selected_str = "";
				if (($i == (count($code_list) - 1) && $export_code == "") || $tmp_code == $export_code) {
					$selected_str = " selected";
				}
				?>
				<option value="<?=$tmp_code?>"<?=$selected_str?>><?=$tmp_code?></option>
			<?
			}
			?>
		</select>
	<?
	} else {
	?>
    	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<?=$export_code?>
		</font>
		<input type="hidden" name="export_code_<?=$line_no?>" value="<?=$export_code?>">
	<?
	}
	?>
</td>
















<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if(empty($super_item_num))
{
?>
    <input type="button" value="行削除" onclick="if(delete_check(<?=$line_no?>,<?=$used_item_flg_script?>)){list_delete_line('<?=$line_no?>');}" >
<?
}
?>
    </font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <input type="button" name="add_line" value="行追加" onclick="list_add_line('<?=$line_no+1?>');" >
    </font>
</td>

</tr>
<?
}
?>


</table>
<!-- 一覧 END -->


<!-- 下部更新ボタン等 START-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
    <input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 下部更新ボタン等 START-->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

<?
if ($message != "") {
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">
	<?= $message ?>
	</font>
<?
}
?>

</td>
</tr>
</table>
<!-- メイン END -->


</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?

//==============================
//DBコネクション終了
//==============================
pg_close($con);

function get_super_item($con, $fname) {
    $sql = "SELECT
                super_item_id,
                super_item_num,
                super_item_order,
                super_item_name,
                disp_flg,
                available,
                code,
                export_code,
                other_code
            FROM
                inci_super_item_2010
            WHERE
                available = 't'
            ORDER BY
                super_item_order";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}

function get_judge_item($con, $fname, $id) {
    $tmp_id = pg_escape_string($id);
    $sql = "
        SELECT
            count(item_id) as cnt
        FROM
            inci_item_2010
        WHERE
            available = 't' AND
            super_item_id = {$tmp_id}";

    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $result = pg_fetch_result($sel, 0, "cnt");

    return ($result > 0) ? true : false;
}

function get_default_code($con, $fname) {
	$sql = "
		select
			code
		from
			inci_super_item_2010
		where
			code <> ''
		order by
			super_item_order
	";

	$sel = select_from_table($con,$sql,"",$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	return pg_fetch_all($sel);
}