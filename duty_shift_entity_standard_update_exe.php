<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜管理 |施設基準「登録」 -->
<!-- ************************************************************************ -->

<?
require_once("about_session.php");
require_once("about_authority.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");
///-----------------------------------------------------------------------------
// 指定ＩＤのレコードを全削除
///-----------------------------------------------------------------------------
$sql = "delete from duty_shift_institution_standard";
$cond = "where standard_id = '$standard_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 選択された施設基準情報を登録
///-----------------------------------------------------------------------------
//時間設定
if (($prov_start_hh != "") ||
	($prov_start_mm != "") ||
	($prov_end_hh != "") ||
	($prov_end_mm != "")) {
	$prov_start_hhmm = sprintf("%02d%02d", $prov_start_hh, $prov_start_mm);
	$prov_end_hhmm = sprintf("%02d%02d", $prov_end_hh, $prov_end_mm);
}
for ($i=0; $i<3; $i++) {
	$send_start_hhmm[$i] = "";
	$send_end_hhmm[$i] = "";
}
for ($i=0; $i<$shift_cnt; $i++) {
	if (($send_start_hh[$i] != "") ||
		($send_start_mm[$i] != "") ||
		($send_end_hh[$i] != "") ||
		($send_end_mm[$i] != "")) {
		$send_start_hhmm[$i] = sprintf("%02d%02d", $send_start_hh[$i], $send_start_mm[$i]);
		$send_end_hhmm[$i] = sprintf("%02d%02d", $send_end_hh[$i], $send_end_mm[$i]);
	}
}
//ＳＱＬ
$sql = "insert into duty_shift_institution_standard (";
$sql .= "standard_id, ward_cnt, sickbed_cnt, report_kbn, report_patient_cnt, hosp_patient_cnt, nursing_assistance_flg, ";
$sql .= "hospitalization_cnt, ";    //平均在院日数 20130402項目復活
$sql .= "prov_start_hhmm, prov_end_hhmm, ";
$sql .= "limit_time, shift_flg, ";
$sql .= "send_start_hhmm_1, send_end_hhmm_1, ";
$sql .= "send_start_hhmm_2, send_end_hhmm_2, ";
$sql .= "send_start_hhmm_3, send_end_hhmm_3, ";
$sql .= "labor_cnt, ";
$sql .= "nurse_staffing_flg, acute_nursing_flg, acute_nursing_cnt, nursing_assistance_cnt, ";
$sql .= "acute_assistance_flg, acute_assistance_cnt, night_acute_assistance_flg, night_acute_assistance_cnt, night_shift_add_flg, ";
$sql .= "specific_type, nurse_staffing_cnt, assistance_staffing_cnt";
$sql .= ") values (";
$content = array($standard_id,
        pg_escape_string($ward_cnt),
        pg_escape_string($sickbed_cnt),
        pg_escape_string($report_kbn),
        pg_escape_string($report_patient_cnt),
        pg_escape_string($hosp_patient_cnt),
        pg_escape_string($nursing_assistance_flg),
        pg_escape_string($hospitalization_cnt),
        pg_escape_string($prov_start_hhmm),
        pg_escape_string($prov_end_hhmm),
        pg_escape_string($limit_time),
        pg_escape_string($shift_flg),
        pg_escape_string($send_start_hhmm[0]),
        pg_escape_string($send_end_hhmm[0]),
        pg_escape_string($send_start_hhmm[1]),
        pg_escape_string($send_end_hhmm[1]),
        pg_escape_string($send_start_hhmm[2]),
        pg_escape_string($send_end_hhmm[2]),
        pg_escape_string($labor_cnt),
        pg_escape_string($nurse_staffing_flg),
        pg_escape_string($acute_nursing_flg),
        pg_escape_string($acute_nursing_cnt),
        pg_escape_string($nursing_assistance_cnt),
        pg_escape_string($acute_assistance_flg),
        pg_escape_string($acute_assistance_cnt),
        pg_escape_string($night_acute_assistance_flg),
        pg_escape_string($night_acute_assistance_cnt),
        pg_escape_string($night_shift_add_flg),
        pg_escape_string($specific_type),
        pg_escape_string($nurse_staffing_cnt),
        pg_escape_string($assistance_staffing_cnt)
        );
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_entity_standard.php?session=$session&standard_id=$standard_id';</script>");
?>
