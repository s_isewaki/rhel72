<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	//更新
	set_auto_reload_setting($con,$fname,$report_gamen);
}

//==============================
//表示情報の取得
//==============================

$auto_reload_setting = get_auto_reload_setting($con,$fname);
$report_gamen = $auto_reload_setting["report_gamen"];


//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form id="form1" name="form1" action="" method="post">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- メイン START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>


<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="400" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">






<!-- 報告書画面設定 START-->
<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>報告書画面設定</B></font>
</td>
</tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="100%" bgcolor="#F5FFE5" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="report_gamen" value="t" <?if($report_gamen == "t"){?>checked<?}?> >1分ごとにセッション情報の自動更新を行う。(推奨)
</font>
</td>
</tr>
</table>
<!-- 報告書画面設定 START-->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="submit" value="更新">
</td>
</tr>
</table>




</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->


</td>
</tr>
</table>
<!-- メイン END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>