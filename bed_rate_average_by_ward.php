<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜平均病床利用率（病棟別）</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("show_calc_bed_by_ward.ini");
require("get_values.ini");
require("get_ward_div.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$ward = check_authority($session,14,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 入力チェック
if ($from_date == "") {
	$from_date = sprintf("%04d%02d%02d", $from_yr, $from_mon, $from_day);
} else {
	$from_yr = substr($from_date, 0, 4);
	$from_mon = substr($from_date, 4, 2);
	$from_day = substr($from_date, 6, 2);
}
if ($to_date == "") {
	$to_date = sprintf("%04d%02d%02d", $to_yr, $to_mon, $to_day);
} else {
	$to_yr = substr($to_date, 0, 4);
	$to_mon = substr($to_date, 4, 2);
	$to_day = substr($to_date, 6, 2);
}
if (!checkdate($from_mon, $from_day, $from_yr)) {
	echo("<script language=\"javascript\">alert(\"From日付が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if (!checkdate($to_mon, $to_day, $to_yr)) {
	echo("<script language=\"javascript\">alert(\"To日付が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}
if ($from_date > $to_date) {
	echo("<script language=\"javascript\">alert(\"期間が正しくありません\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// DBへのコネクション作成
$con = connect2db($fname);

// ベット数除外対象のURLクエリ文字列を作成
if (!is_array($omit_types)) {
	$omit_types = array();
} else {
	$omit_query = "";
	foreach ($omit_types as $omit_type) {
		$omit_query .= "&omit_types[]=$omit_type";
	}
}

// 病棟区分名を取得
$arr_ward_div = get_ward_div($con, $fname, false, true, true);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function openDetail(from_date, to_date, target) {
	window.open('bed_rate_average_detail_by_ward.php?session=<? echo($session); ?>&from_date=' + from_date + '&to_date=' + to_date + '&target=' + target + '&ward_type=<? echo($ward_type); ?><? echo($omit_query); ?>', 'detail', 'width=640,height=600,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#5279a5"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床利用率管理</b></font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="34%" height="22" align="center">
<a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率</font></a>
</td>
<td width="33%" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均病床利用率</font>
</td>
<td width="34%" align="center">
<a href="bed_rate_activity.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床稼動率</font></a>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- 期間 -->
<form action="bed_rate_average_by_ward.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
期間：<select name="from_yr">
<? show_select_years(15, $from_yr); ?>
</select>/<select name="from_mon">
<? show_select_months($from_mon); ?>
</select>/<select name="from_day">
<? show_select_days($from_day); ?>
</select>〜<select name="to_yr">
<? show_select_years(15, $to_yr); ?>
</select>/<select name="to_mon">
<? show_select_months($to_mon); ?>
</select>/<select name="to_day">
<? show_select_days($to_day); ?>
</select>
<img src="img/spacer.gif" alt="" width="5" height="1">
ベット数除外対象：<input type="checkbox" name="omit_types[]" value="3"<? if (in_array("3", $omit_types)) {echo(" checked");} ?>>回復室
<input type="checkbox" name="omit_types[]" value="4"<? if (in_array("4", $omit_types)) {echo(" checked");} ?>>ICU
<input type="checkbox" name="omit_types[]" value="5"<? if (in_array("5", $omit_types)) {echo(" checked");} ?>>HCU
<input type="checkbox" name="omit_types[]" value="6"<? if (in_array("6", $omit_types)) {echo(" checked");} ?>>NICU
<input type="checkbox" name="omit_types[]" value="7"<? if (in_array("7", $omit_types)) {echo(" checked");} ?>>GCU
<img src="img/spacer.gif" alt="" width="5" height="1">
<input type="submit" value="計算">
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo $session; ?>">
<!--<input type="hidden" name="ward_type" value="<? echo $ward_type; ?>">-->
</form>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100" height="22" align="center" bgcolor="#bdd1e7"><a href="bed_rate_average.php?session=<? echo($session); ?>&mode=calc1&from_yr=<? echo($from_yr); ?>&from_mon=<? echo($from_mon); ?>&from_day=<? echo($from_day); ?>&to_yr=<? echo($to_yr); ?>&to_mon=<? echo($to_mon); ?>&to_day=<? echo($to_day); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">区分別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟別</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- メニュー2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="20%" height="22" align="center">
<? if ($ward_type != "1" && $arr_ward_div["1"]["use_flg"]) { ?><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>&ward_type=1"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]["name"]); ?></font>
<? if ($ward_type != "1" && $arr_ward_div["1"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "2" && $arr_ward_div["2"]["use_flg"]) { ?><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>&ward_type=2"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]["name"]); ?></font>
<? if ($ward_type != "2" && $arr_ward_div["2"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "3" && $arr_ward_div["3"]["use_flg"]) { ?><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>&ward_type=3"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]["name"]); ?></font>
<? if ($ward_type != "3" && $arr_ward_div["3"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "4" && $arr_ward_div["4"]["use_flg"]) { ?><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>&ward_type=4"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]["name"]); ?></font>
<? if ($ward_type != "4" && $arr_ward_div["4"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "5" && $arr_ward_div["5"]["use_flg"]) { ?><a href="bed_rate_average_by_ward.php?session=<? echo($session); ?>&from_date=<? echo($from_date); ?>&to_date=<? echo($to_date); ?><? echo($omit_query); ?>&ward_type=5"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]["name"]); ?></font>
<? if ($ward_type != "5" && $arr_ward_div["5"]["use_flg"]) { ?></a><? } ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- 平均病床利用率 -->
<?
if ($ward_type != "") {
	show_avg_bed_rate($con, $from_date, $to_date, $ward_type, $arr_ward_div, $fname, $omit_types);
}
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// 平均病床利用率の出力
//------------------------------------------------------------------------------
function show_avg_bed_rate($con, $from_date, $to_date, $ward_type, $arr_ward_div, $fname, $omit_types) {

	define("WARDS_PER_ROW", 6);
	define("TERM_COL_WIDTH", 10);

	// 平均病床利用率の取得
	$arr_rate = get_arr_avg_bed_rate($con, $from_date, $to_date, $ward_type, $fname, $omit_types);

	// 病棟数の取得
	$rate_cnt = count($arr_rate[0]) - 1;

	// 表示テーブル数の算出
	$table_cnt = ceil($rate_cnt / WARDS_PER_ROW);

	// テーブルの表示
	for ($i = 1; $i <= $table_cnt; $i++) {
		$min_idx = ($i - 1) * WARDS_PER_ROW + 1;
		$max_idx = min($min_idx + WARDS_PER_ROW - 1, $rate_cnt);
		$cell_width = round((100 - TERM_COL_WIDTH) / WARDS_PER_ROW);
		$table_width = TERM_COL_WIDTH + ($max_idx - $min_idx + 1) * $cell_width;
		$pre_ward_type = "";

		echo("<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n");
		echo("<tr>\n");
		echo("<td>\n");

		echo("<table width='{$table_width}%' border='0' cellspacing='0' cellpadding='1' align='left' class='block'>\n");

		// ヘッダ1行目の表示
		$colspan = 1;
		echo("<tr bgcolor='#f6f9ff'>\n");
		echo("<td height=\"22\">&nbsp;</td>");
		for ($k = $min_idx; $k <= $max_idx; $k++) {
			$tmp_ward_type = $arr_rate[0][$k];
			$show_ward_type = $arr_ward_div[$tmp_ward_type]["name"];

			if ($k < $max_idx) {
				if ($tmp_ward_type == $arr_rate[0][$k + 1]) {
					$colspan++;
					continue;
				}
			}

			$colspan = 1;
		}

		// ヘッダ2行目の表示
		for ($k = $min_idx; $k <= $max_idx; $k++) {
			echo("<td width='{$cell_width}%' height=\"22\" align=\"right\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$arr_rate[1][$k]}</font></td>\n");
		}
		echo("</tr>\n");

		// 3行目以降の表示
		for ($j = 2, $rate_max = count($arr_rate); $j < $rate_max; $j++) {
			echo("<tr>\n");
			echo("<td width='" . TERM_COL_WIDTH . "%' height=\"22\" align=\"right\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$arr_rate[$j]["title"]}</font></td>\n");
			for ($k = $min_idx; $k <= $max_idx; $k++) {
				if ($k < count($arr_rate[$j])) {
					if ($j < $rate_max - 1) {
						echo("<td align=\"right\" onclick=\"openDetail('{$arr_rate[$j]["from_date"][$k]}', '{$arr_rate[$j]["to_date"][$k]}', '{$arr_rate[$j]["target"][$k]}');\" onmousemove=\"this.style.backgroundColor = '#FFC'; this.style.cursor = 'pointer';\" onmouseout=\"this.style.backgroundColor = '#FFF'; this.style.cursor = '';\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$arr_rate[$j][$k]}%</font></td>\n");
					} else {
						echo("<td align=\"right\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$arr_rate[$j][$k]}%</font></td>\n");
					}
				} else {
					echo("<td>&nbsp;</td>");
				}
			}
			echo("</tr>\n");
		}

		echo("</table>\n");

		echo("</td>\n");
		echo("</tr>\n");
		echo("</table>\n");

		echo("<img src='./img/spacer.gif' width='1' height='5'><br>\n");
	}

}
?>
