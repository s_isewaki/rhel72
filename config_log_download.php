<?
require_once("about_session.php");

$fname = $PHP_SELF;




//ログファイルのディレクトリ指定
$dir_name = './log';

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if($f_n != "")
{
	
	// ファイルを読み込んで出力します。
	$full_file_name = $dir_name."/".$f_n;
	$real_path = realpath($full_file_name);
	
	$red = file_exists($real_path);
	header("Cache-Control: public");
	header("Pragma: public");
	//ダウンロードファイル名
	header("Content-Disposition: attachment; filename=$f_n");
	//ダウンロードダイアログを表示する
	header("Content-Type: application/octet-stream;");
	header("Content-Length: " . filesize($full_file_name));
	
	readfile($full_file_name);
	
}


exit;
?>