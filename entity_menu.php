<? ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("show_select_values.ini");
require("referer_common.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require_once("get_menu_label.ini");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("summary_common.ini");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	//権限チェック
	$dept = check_authority($session,28,$fname);
	if(!$dept) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

if (@$_REQUEST["mode"] == "update"){
	$sql =
		" update sectmst set".
		" sect_hidden_flg = " .(int)@$_REQUEST["sect_hidden_flg"].
		",sect_sort_order = '" . pg_escape_string(@$_REQUEST["sect_sort_order"]) . "'".
		" where enti_id = ".(int)@$_REQUEST["enti_id"].
		" and sect_id = ".(int)@$_REQUEST["p_sect_id"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	ob_end_clean();
	header("Location: entity_menu.php?session=".$session);
	die;
}
if (@$_REQUEST["mode"] == "delete"){
	$sql =
		" update sectmst set".
		" sect_del_flg = true".
		" where enti_id = ".(int)@$_REQUEST["enti_id"].
		" and sect_id = ".(int)@$_REQUEST["p_sect_id"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

	$sql =
		" update drmst set".
		" dr_del_flg = true".
		" where enti_id = ".(int)@$_REQUEST["enti_id"].
		" and sect_id = ".(int)@$_REQUEST["p_sect_id"];
	$upd = update_set_table($con, $sql, array(), null, "", $fname);

	ob_end_clean();
	header("Location: entity_menu.php?session=".$session);
	die;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// メドレポート管理権限を取得
$summary_admin_auth = check_authority($session, 59, $fname);

//DBコネクションの作成
$con = connect2db($fname);

//==============================
// ログ
//==============================
summary_common_write_operate_log("access", $con, $fname, $session, "", "", "");

// 遷移元の設定
if (@$entity != "") {
	set_referer($con, $session, "entity", $entity, $fname);
	$referer = $entity;
} else {
	$referer = get_referer($con, $session, "entity", $fname);
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 病床管理/居室管理
$bed_manage_title = $_label_by_profile["BED_MANAGE"][$profile_type];

// 患者管理/利用者管理
$patient_manage_title = $_label_by_profile["PATIENT_MANAGE"][$profile_type];

// 病棟/施設
$ward_title = $_label_by_profile["WARD"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// 担当医/担当者１
$doctor_title = $_label_by_profile["DOCTOR"][$profile_type];

// 担当看護師/担当者２
$nurse_title = $_label_by_profile["NURSE"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = get_report_menu_label($con, $fname);


//=================================================
// 看護支援 - 初期表示準備  オプション値を取得する。
//=================================================
$menu_flg = 2;       // 患者検索
$worksheet_flg = 1;  // ワークシート

$emp_id = get_emp_id($con, $session, $fname);

$sql = "select menu_flg, worksheet_flg, bldg_cd, ward_cd, ptrm_room_no, team_id from sum_application_option";
$cond = "where emp_id = '".pg_escape_string($emp_id)."'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	summary_common_show_error_page_and_die();
}
if (pg_numrows($sel) == 1) {
	$menu_flg = (int)pg_fetch_result($sel, 0, "menu_flg");
	$worksheet_flg = (int)pg_fetch_result($sel, 0, "worksheet_flg");
	$sel_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
	$sel_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
	$sel_ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
	$sel_team_id = pg_fetch_result($sel, 0, "team_id");
}

$med_program_list = array(
	array("mode" => 1, "location"=>"summary_menu.php?session=$session",                        "lname"=>"お知らせ"),
	array("mode" => 2, "location"=>"summary_menu.php?session=$session&search_mode=1",          "lname"=>"患者検索"),
	array("mode" => 3, "location"=>"summary_search_bed.php?session=$session",                  "lname"=>"病床検索"),
	array("mode" => 4, "location"=>"summary_search_data.php?session=$session",                 "lname"=>"複合検索"),
	array("mode" => 5, "location"=>"sum_application_apply_list.php?session=$session",          "lname"=>"送信一覧"),
	array("mode" => 6, "location"=>"sum_application_approve_list.php?session=$session",        "lname"=>"受信一覧"),
	array("mode" => 7, "location"=>"sot_worksheet.php?session=$session&apply_date_default=on", "lname"=>"看護支援"),
);

$worksheet_program_list = array(
	array("mode" => 1, "location"=>"sot_worksheet.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",              "lname"=>"ワークシート"),
	array("mode" => 2, "location"=>"sot_worksheet_keikan_eiyou.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id", "lname"=>"経管栄養一覧"),
	array("mode" => 3, "location"=>"sot_worksheet_isisiji.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",      "lname"=>"医師指示一覧"),
	array("mode" => 4, "location"=>"sot_worksheet_ondoban.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",      "lname"=>"温度板"),
	array("mode" => 5, "location"=>"sot_worksheet_soaplist.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",     "lname"=>"チーム記録"),
	array("mode" => 6, "location"=>"sot_worksheet_fim.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",          "lname"=>"FIM評価表"),
	array("mode" => 7, "location"=>"sot_worksheet_weight.php?session=$session&apply_date_default=on&sel_bldg_cd=$sel_bldg_cd&sel_ward_cd=$sel_ward_cd&sel_ptrm_room_no=$sel_ptrm_room_no&sel_team_id=$sel_team_id",       "lname"=>"体重集計表"),
);

if ($menu_flg == 7) {
	foreach ($worksheet_program_list as $wlist) {
		if ($worksheet_flg == $wlist["mode"]) {
			$pname = $wlist["location"];
			break;
		}
	}
} else {
	foreach ($med_program_list as $plist) {
		if ($menu_flg == $plist["mode"]) {
			$pname = $plist["location"];
			break;
		}
	}
}



?>
<title>CoMedix マスターメンテナンス | <? echo ($section_title); ?>一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th { border:#5279a5 solid 1px; white-space:nowrap; font-weight:normal; }
.list td { border:#5279a5 solid 1px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "1") { /* 病床管理 */ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="<? echo ($bed_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b><? echo ($bed_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "2") { /* 患者管理 */ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($patient_manage_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($patient_manage_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="patient_info_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else if ($referer == "3") { /* メドレポート */ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="summary_menu.php?session=<? echo($session); ?>"><img src="img/icon/b57.gif" width="32" height="32" border="0" alt="<? echo($med_report_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="summary_menu.php?session=<? echo($session); ?>"><b><? echo($med_report_title); ?></b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="entity_menu.php?session=<? echo($session); ?>"><b><? echo ($section_title); ?></b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($pname); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">
<table width="118" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279A5">
<td height="22" class="spacing" colspan="2"><b><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">管理項目</font></b></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="entity_menu.php?session=<? echo($session); ?>"><? echo ($section_title); ?></a></font></td>
</tr>
<? if ($ward_admin_auth == "1" && $referer == "1") { ?>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="building_list.php?session=<? echo($session); ?>"><?=$ward_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="bed_display_setting.php?session=<? echo($session); ?>">表示設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="bed_bulk_inpatient_register.php?session=<? echo($session); ?>">一括登録</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="bed_master_menu.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>
<? } ?>
<? if ($patient_admin_auth == "1" && $referer == "2") { ?>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="bed_check_setting.php?session=<? echo($session); ?>">管理項目設定</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="patient_admin_menu.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>
<? } ?>
<? if ($summary_admin_auth == "1" && $referer == "3") { ?>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_kanri_kubun.php?session=<? echo($session); ?>"><?=$summary_kubun_title?></a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="sum_ordsc_med_list.php?session=<? echo($session); ?>">処方・注射オーダ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_kanri_tmpl.php?session=<? echo($session); ?>">テンプレート</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_mst.php?session=<? echo($session); ?>">マスタ管理</a></font></td>
</tr>

<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_access_log_daily.php?session=<? echo($session); ?>">アクセスログ</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_kensakekka_import.php?session=<? echo($session); ?>">検査結果</a></font></td>
</tr>
<tr height="20">
<td align="center">&nbsp;<?=$font?><b>・</b></td>
<td width="90%"><?=$font?><a href="summary_kanri_option.php?session=<? echo($session); ?>">オプション</a></font></td>
</tr>

<? } ?>
</table>
</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="90" align="center" bgcolor="#5279a5"><a href="entity_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?=$section_title?>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_list.php?session=<? echo($session); ?>"><?=$font?><?=$doctor_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="nurse_list.php?session=<? echo($session); ?>"><?=$font?><?=$nurse_title?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="section_register.php?session=<? echo($session); ?>"><?=$font?><?=$section_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="doctor_register.php?session=<? echo($session); ?>"><?=$font?><?=$doctor_title?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="nurse_register.php?session=<? echo($session); ?>"><?=$font?><?=$nurse_title?>登録</font></a></td>
<td width="" align="right"><!--input type="button" value="削除" onclick="deleteSect();"--></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>







<?//==========================================================================?>
<?// 一覧表示                                                                 ?>
<?//==========================================================================?>
<table width="500px" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff">
<th style="width:80%"><?=$font?><?=$section_title?>名</font></th>
<th style="width:5%"><?=$font?>表示</font></th>
<th style="width:5%"><?=$font?>表示順</font></th>
<th style="width:5%"><?=$font?>表示状態更新</font></th>
<th style="width:5%"><?=$font?>削除</font></th>
</tr>
<? $sql = "select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst"; ?>
<? $sel = select_from_table($con,"select * from (".$sql.") d where sect_del_flg = 'f' order by sort_order, sect_id","",$fname); ?>
<? while($row = @pg_fetch_array($sel)){ ?>
	<form action="entity_menu.php" method="get">
		<input type="hidden" name="session" value="<?=$session ?>">
		<input type="hidden" name="enti_id" value="<?=$row["enti_id"] ?>">
		<input type="hidden" name="p_sect_id" value="<?=$row["sect_id"] ?>">
		<input type="hidden" name="mode" value="update">
		<tr>
			<td><?=$font?><a href="" onclick="gotoEdit('<?=$row["enti_id"]?>','<?=$row["sect_id"]?>'); return false;"><?=$row["sect_nm"]?></a></font></td>
			<td><select name="sect_hidden_flg"><option value="">表示</option><option value="1" <?=$row["sect_hidden_flg"]?" selected":""?>>非表示</option></select></td>
			<td><input type="text" name="sect_sort_order" value="<?=$row["sect_sort_order"]?>" style="width:40px"></td>
			<td style="text-align:center"><input type="submit" value="更新" style="width:42px" /></td>
			<td><input type="button" value="削除" style="width:42px" onclick="deleteRecord('<?=$row["enti_id"]?>','<?=$row["sect_id"]?>');" /></td>
		</tr>
	</form>
<? } ?>
</table>
<script type="text/javascript">
	function gotoEdit(enti_id, sect_id){
		location.href = "section_update.php?session=<?=$session?>&enti_id="+enti_id+"&p_sect_id="+sect_id;
	}
	function deleteRecord(enti_id, sect_id){
		if (!confirm("削除します。よろしいですか？")) return;
		location.href = "entity_menu.php?session=<?=$session?>&enti_id="+enti_id+"&p_sect_id="+sect_id+"&mode=delete";
	}
</script>






</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
