<?php

ob_start();

// require_once
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

// short name: echo
function e($var) {
    echo $var;
}

// short name: htmlspecialchars
if (!function_exists("h")) {

function h($var, $flags = ENT_COMPAT, $charset = 'ISO-8859-1') {
    return htmlspecialchars($var, $flags, $charset);
}
}

// short name: urlencode
function u($var) {
    return urlencode($var);
}

// short name: pg_escape_string
function p($var) {
    return pg_escape_string($var);
}

// escape for SQL
function q() {
    $args = func_get_args();
    if (count($args) > 1) {
        $str = array_shift($args);
        return vsprintf($str, array_map("q", $args));
    }
    if (is_array($args[0])) {
        return array_map("q", $args[0]);
    }
    return is_null($args[0]) ? null : pg_escape_string($args[0]);
}

// escape for LIKE
function qlike($str, $encoding = "eucJP-win") {
    $original_encoding = mb_regex_encoding();
    if ($encoding !== $original_encoding) {
        mb_regex_encoding($encoding);
    }
    $result = mb_ereg_replace("[%_]", "\\\\0", pg_escape_string($str));
    if ($original_encoding !== $encoding) {
        mb_regex_encoding($original_encoding);
    }
    return $result;
}

// echo htmlspecialchars
function eh($var) {
    e(h($var));
}

// echo htmlspecialchars urlencode
function ehu($var) {
    e(h(u($var)));
}

// echo htmlspecialchars for Array
function eh_array($arr) {
    for ($i = 0, $j = count($arr) - 1; $i <= $j; $i++) {
        e(h($arr[$i]));
        if ($i < $j) e("<br>");
    }
}

// htmlspecialchars for js (don't use this in <script>)
function h_jsparam($var) {
    $buf = mb_ereg_replace("\\\\", "\\\\", $var);
    $buf = mb_ereg_replace("'", "\\'", $buf);
    $buf = mb_ereg_replace('"', '\\"', $buf);
    return h($buf, ENT_QUOTES);
}

// echo htmlspecialchars for js (don't use this in <script>)
function eh_jsparam($var) {
    e(h_jsparam($var));
}

// echo htmlspecialchars urlencode for js (don't use this in <script>)
function ehu_jsparam($var) {
    eh_jsparam(u($var));
}

// javascript alart
if (!function_exists("js_alert_exit")) {

    function js_alert_exit($mes, $form = "") {
        echo("<script type=\"text/javascript\">alert('{$mes}');</script>\n");
        if ($form === "items") {
            echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        } else {
            echo("<script type=\"text/javascript\">history.back();</script>\n");
        }
        exit;
    }

}

// javascript go to error page
if (!function_exists("js_error_exit")) {

    function js_error_exit() {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

}

// javascript back to login page
if (!function_exists("js_login_exit")) {

    function js_login_exit() {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }

}

// include js file with timestamp query
function include_js($path, $charset = "") {
    $mtime = filemtime($path);
    echo("<script type=\"text/javascript\" src=\"" . h("$path?$mtime") . "\"");
    if ($charset !== "") {
        echo(" charset=\"" . h($charset) . "\"");
    }
    echo("></script>\n");
}

/**
 * Converts PHP variable or array into a "JSON" (JavaScript value expression
 * or "object notation") string.
 *
 * @compat
 *    Output seems identical to PECL versions. "Only" 20x slower than PECL version.
 * @bugs
 *    Doesn't take care with unicode too much - leaves UTF-8 sequences alone.
 *
 * @param  $var mixed  PHP variable/array/object
 * @return string      transformed into JSON equivalent
 */
if (!function_exists("cmx_json_encode")) {

    function cmx_json_encode($var, /* emu_args */ $obj = FALSE) {

        #-- prepare JSON string
        $json = "";

        #-- add array entries
        if (is_array($var) || ($obj = is_object($var))) {

            #-- check if array is associative
            if (!$obj)
                foreach ((array) $var as $i => $v) {
                    if (!is_int($i)) {
                        $obj = 1;
                        break;
                    }
                }

            #-- concat invidual entries
            foreach ((array) $var as $i => $v) {
                $json .= ($json ? "," : "")    // comma separators
                    . ($obj ? ("\"$i\":") : "")   // assoc prefix
                    . (cmx_json_encode($v));    // value
            }

            #-- enclose into braces or brackets
            $json = $obj ? "{" . $json . "}" : "[" . $json . "]";
        }

        #-- strings need some care
        elseif (is_string($var)) {
            if (!utf8_decode($var)) {
                $var = utf8_encode($var);
            }
            $var = str_replace(array("\\", "\"", "/", "\b", "\f", "\n", "\r", "\t"),
                               array("\\\\", '\"', "\\/", "\\b", "\\f", "\\n", "\\r", "\\t"), $var);
            $json = '"' . $var . '"';
            //@COMPAT: for fully-fully-compliance   $var = preg_replace("/[\000-\037]/", "", $var);
        }

        #-- basic types
        elseif (is_bool($var)) {
            $json = $var ? "true" : "false";
        }
        elseif ($var === NULL) {
            $json = "null";
        }
        elseif (is_int($var) || is_float($var)) {
            $json = "$var";
        }

        #-- something went wrong
        else {
            trigger_error("cmx_json_encode: don't know what a '" . gettype($var) . "' is.",
                                                                           E_USER_ERROR);
        }

        #-- done
        return($json);
    }

}



/**
 * Parses a JSON (JavaScript value expression) string into a PHP variable
 * (array or object).
 *
 * @compat
 *    Behaves similar to PECL version, but is less quiet on errors.
 *    Now even decodes unicode \uXXXX string escapes into UTF-8.
 *    "Only" 27 times slower than native function.
 * @bugs
 *    Might parse some misformed representations, when other implementations
 *    would scream error or explode.
 * @code
 *    This is state machine spaghetti code. Needs the extranous parameters to
 *    process subarrays, etc. When it recursively calls itself, $n is the
 *    current position, and $waitfor a string with possible end-tokens.
 *
 * @param   $json string   JSON encoded values
 * @param   $assoc bool    pack data into php array/hashes instead of objects
 * @return  mixed          parsed into PHP variable/array/object
 */
if (!function_exists("cmx_json_decode")) {

    function cmx_json_decode($json, $assoc = FALSE, $limit = 512, /* emu_args */ $n = 0, $state = 0,
        $waitfor = 0) {

        #-- result var
        $val = NULL;
        static $lang_eq = array("true"  => TRUE, "false" => FALSE, "null"  => NULL);
        static $str_eq = array("n"  => "\012", "r"  => "\015", "\\" => "\\", '"'  => '"', "f"  => "\f", "b"  => "\b", "t"  => "\t", "/"  => "/");
        if ($limit < 0)
            return /* __cannot_compensate */;

        #-- flat char-wise parsing
        for (/* n */; $n < strlen($json); /* n */) {
            $c = $json[$n];

            #-= in-string
            if ($state === '"') {

                if ($c == '\\') {
                    $c = $json[++$n];
                    // simple C escapes
                    if (isset($str_eq[$c])) {
                        $val .= $str_eq[$c];
                    }

                    // here we transform \uXXXX Unicode (always 4 nibbles) references to UTF-8
                    elseif ($c == "u") {
                        // read just 16bit (therefore value can't be negative)
                        $hex = hexdec(substr($json, $n + 1, 4));
                        $n += 4;
                        // Unicode ranges
                        if ($hex < 0x80) {    // plain ASCII character
                            $val .= chr($hex);
                        }
                        elseif ($hex < 0x800) {   // 110xxxxx 10xxxxxx
                            $val .= chr(0xC0 + $hex >> 6) . chr(0x80 + $hex & 63);
                        }
                        elseif ($hex <= 0xFFFF) { // 1110xxxx 10xxxxxx 10xxxxxx
                            $val .= chr(0xE0 + $hex >> 12) . chr(0x80 + ($hex >> 6) & 63) . chr(0x80 + $hex & 63);
                        }
                        // other ranges, like 0x1FFFFF=0xF0, 0x3FFFFFF=0xF8 and 0x7FFFFFFF=0xFC do not apply
                    }

                    // no escape, just a redundant backslash
                    //@COMPAT: we could throw an exception here
                    else {
                        $val .= "\\" . $c;
                    }
                }

                // end of string
                elseif ($c == '"') {
                    $state = 0;
                }

                // yeeha! a single character found!!!!1!
                else/* if (ord($c) >= 32) */ { //@COMPAT: specialchars check - but native json doesn't do it?
                    $val .= $c;
                }
            }

            #-> end of sub-call (array/object)
            elseif ($waitfor && (strpos($waitfor, $c) !== false)) {
                return array($val, $n);  // return current value and state
            }

            #-= in-array
            elseif ($state === ']') {
                list($v, $n) = cmx_json_decode($json, $assoc, $limit, $n, 0, ",]");
                $val[] = $v;
                if ($json[$n] == "]") {
                    return array($val, $n);
                }
            }

            #-= in-object
            elseif ($state === '}') {
                list($i, $n) = cmx_json_decode($json, $assoc, $limit, $n, 0, ":");   // this allowed non-string indicies
                list($v, $n) = cmx_json_decode($json, $assoc, $limit, $n + 1, 0, ",}");
                $val[$i] = $v;
                if ($json[$n] == "}") {
                    return array($val, $n);
                }
            }

            #-- looking for next item (0)
            else {

                #-> whitespace
                if (preg_match("/\s/", $c)) {
                    // skip
                }

                #-> string begin
                elseif ($c == '"') {
                    $state = '"';
                }

                #-> object
                elseif ($c == "{") {
                    list($val, $n) = cmx_json_decode($json, $assoc, $limit - 1, $n + 1, '}', "}");

                    if ($val && $n) {
                        $val = $assoc ? (array) $val : (object) $val;
                    }
                }

                #-> array
                elseif ($c == "[") {
                    list($val, $n) = cmx_json_decode($json, $assoc, $limit - 1, $n + 1, ']', "]");
                }

                #-> comment
                elseif (($c == "/") && ($json[$n + 1] == "*")) {
                    // just find end, skip over
                    ($n = strpos($json, "*/", $n + 1)) or ($n = strlen($json));
                }

                #-> numbers
                elseif (preg_match("#^(-?\d+(?:\.\d+)?)(?:[eE]([-+]?\d+))?#", substr($json, $n), $uu)) {
                    $val = $uu[1];
                    $n += strlen($uu[0]) - 1;
                    if (strpos($val, ".")) {  // float
                        $val = (float) $val;
                    }
                    elseif ($val[0] == "0") {  // oct
                        $val = octdec($val);
                    }
                    else {
                        $val = (int) $val;
                    }
                    // exponent?
                    if (isset($uu[2])) {
                        $val *= pow(10, (int) $uu[2]);
                    }
                }

                #-> boolean or null
                elseif (preg_match("#^(true|false|null)\b#", substr($json, $n), $uu)) {
                    $val = $lang_eq[$uu[1]];
                    $n += strlen($uu[1]) - 1;
                }

                #-- parsing error
                else {
                    // PHPs native json_decode() breaks here usually and QUIETLY
                    trigger_error("json_decode: error parsing '$c' at position $n", E_USER_WARNING);
                    return $waitfor ? array(NULL, 1 << 30) : NULL;
                }
            }//state
            #-- next char
            if ($n === NULL) {
                return NULL;
            }
            $n++;
        }//for
        #-- final result
        return ($val);
    }

}
ob_end_clean();