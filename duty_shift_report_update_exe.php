<!-- ************************************************************************ -->
<!-- 勤務シフト作成｜様式９「登録」 -->
<!-- ************************************************************************ -->
<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");

require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///******************************************************************************
// 施設基準情報テーブル（履歴）
///******************************************************************************
///-----------------------------------------------------------------------------
// 対象の施設基準情報（履歴）を取得。施設基準再読込時は、施設基準情報。
///-----------------------------------------------------------------------------
if ($_POST["reload_btn_flg"] == "1") {
    $data_array = $obj->get_duty_shift_institution_standard_array($standard_id);
} else {
    $data_array = $obj->get_duty_shift_institution_history_array($standard_id, $duty_yyyy, $duty_mm);
}

if (count($data_array) > 0) {
    $ward_cnt                   = $data_array[0]["ward_cnt"];                      //病棟数
    $sickbed_cnt                = $data_array[0]["sickbed_cnt"];                   //病床数
    $report_kbn                 = $data_array[0]["report_kbn"];                    //届出区分
    $report_patient_cnt         = $data_array[0]["report_patient_cnt"];            //届出時入院患者数
    $hosp_patient_cnt           = $_POST["hosp_patient_cnt"];                      //一日平均入院患者数
    $nursing_assistance_flg     = $data_array[0]["nursing_assistance_flg"];        //看護補助加算の有無
    $hospitalization_cnt        = $_POST["hospitalization_cnt"];                   //平均在院日数

    $limit_time                 = $data_array[0]["limit_time"];                    //月平均夜勤時間上限
    $shift_flg                  = $data_array[0]["shift_flg"];                     //交代制

    $labor_cnt                  = $data_array[0]["labor_cnt"];                     //所定労働時間

    //夜勤時間帯
    $prov_start_hhmm            = $data_array[0]["prov_start_hhmm"];               //開始時分
    $prov_end_hhmm              = $data_array[0]["prov_end_hhmm"];                 //終了時分

    //申し送り時間
    for($i=0; $i<3; $i++) {
        $k = $i + 1;
        $send_start_hhmm[$i] = $data_array[0]["send_start_hhmm_$k"];               //開始時分
        $send_end_hhmm[$i]   = $data_array[0]["send_end_hhmm_$k"];                 //終了時分
    }
    $nurse_staffing_flg         = $data_array[0]["nurse_staffing_flg"];            //看護配置加算の有無
    $acute_nursing_flg          = $data_array[0]["acute_nursing_flg"];             //急性期看護補助加算の有無
    $acute_nursing_cnt          = $data_array[0]["acute_nursing_cnt"];             //急性期看護補助加算の配置比率
    $nursing_assistance_cnt     = $data_array[0]["nursing_assistance_cnt"];        //看護補助加算の配置比率

    $acute_assistance_flg       = $data_array[0]["acute_assistance_flg"];          //急性期看護補助体制加算の届出区分
    $acute_assistance_cnt       = $data_array[0]["acute_assistance_cnt"];          //急性期看護補助体制加算の配置比率
    $night_acute_assistance_flg = $data_array[0]["night_acute_assistance_flg"];    //夜間急性期看護補助体制加算の届出区分
    $night_acute_assistance_cnt = $data_array[0]["night_acute_assistance_cnt"];    //夜間急性期看護補助体制加算の配置比率
    $night_shift_add_flg        = $data_array[0]["night_shift_add_flg"];           //看護職員夜間配置加算の有無
    $specific_type              = $data_array[0]["specific_type"];                 //特定入院料種類
    $nurse_staffing_cnt         = $data_array[0]["nurse_staffing_cnt"];            //看護配置
    $assistance_staffing_cnt    = $data_array[0]["assistance_staffing_cnt"];       //看護補助配置
}

///-----------------------------------------------------------------------------
// 登録
///-----------------------------------------------------------------------------
$content = array(
        pg_escape_string($ward_cnt),
        pg_escape_string($sickbed_cnt),
        pg_escape_string($report_kbn),
        pg_escape_string($report_patient_cnt),
        pg_escape_string($hosp_patient_cnt),
        pg_escape_string($nursing_assistance_flg),
        pg_escape_string($inp_hospitalization_cnt),
        pg_escape_string($prov_start_hhmm),
        pg_escape_string($prov_end_hhmm),
        pg_escape_string($limit_time),
        pg_escape_string($shift_flg),
        pg_escape_string($send_start_hhmm[0]),
        pg_escape_string($send_end_hhmm[0]),
        pg_escape_string($send_start_hhmm[1]),
        pg_escape_string($send_end_hhmm[1]),
        pg_escape_string($send_start_hhmm[2]),
        pg_escape_string($send_end_hhmm[2]),
        pg_escape_string($labor_cnt),
        pg_escape_string($nurse_staffing_flg),
        pg_escape_string($acute_nursing_flg),
        pg_escape_string($acute_nursing_cnt),
        pg_escape_string($nursing_assistance_cnt),
        pg_escape_string($acute_assistance_flg),
        pg_escape_string($acute_assistance_cnt),
        pg_escape_string($night_acute_assistance_flg),
        pg_escape_string($night_acute_assistance_cnt),
        pg_escape_string($night_shift_add_flg),
        pg_escape_string($specific_type),
        pg_escape_string($nurse_staffing_cnt),
        pg_escape_string($assistance_staffing_cnt)
);

//データの存在チェック
$sql = "select count(*) as cnt from duty_shift_institution_history ";
$cond = "where standard_id = $standard_id ";
$cond .= "and duty_yyyy = $duty_yyyy ";
$cond .= "and duty_mm = $duty_mm ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$cnt = pg_fetch_result($sel, 0 ,"cnt");

if ($cnt == 0) {
    //ＳＱＬ
    $sql = "insert into duty_shift_institution_history (";
    $sql .= "standard_id, duty_yyyy, duty_mm, ";
    $sql .= "ward_cnt, sickbed_cnt, report_kbn, report_patient_cnt, hosp_patient_cnt, nursing_assistance_flg, ";
    $sql .= "hospitalization_cnt, ";
    $sql .= "prov_start_hhmm, prov_end_hhmm, ";
    $sql .= "limit_time, shift_flg, ";
    $sql .= "send_start_hhmm_1, send_end_hhmm_1, ";
    $sql .= "send_start_hhmm_2, send_end_hhmm_2, ";
    $sql .= "send_start_hhmm_3, send_end_hhmm_3, ";
    $sql .= "labor_cnt, ";
    $sql .= "nurse_staffing_flg, acute_nursing_flg, acute_nursing_cnt, nursing_assistance_cnt,";
    $sql .= "acute_assistance_flg, acute_assistance_cnt, night_acute_assistance_flg, night_acute_assistance_cnt, night_shift_add_flg, ";
    $sql .= "specific_type, nurse_staffing_cnt, assistance_staffing_cnt";
    $sql .= ") values (";

    // 登録
    $content = array_merge(array($standard_id, $duty_yyyy, $duty_mm), $content);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
} else {
    //ＳＱＬ
    $sql = "update duty_shift_institution_history set";
    $set = array(
            "ward_cnt",
            "sickbed_cnt",
            "report_kbn",
            "report_patient_cnt",
            "hosp_patient_cnt",
            "nursing_assistance_flg",
            "hospitalization_cnt",
            "prov_start_hhmm",
            "prov_end_hhmm",
            "limit_time",
            "shift_flg",
            "send_start_hhmm_1",
            "send_end_hhmm_1",
            "send_start_hhmm_2",
            "send_end_hhmm_2",
            "send_start_hhmm_3",
            "send_end_hhmm_3",
            "labor_cnt",
            "nurse_staffing_flg",
            "acute_nursing_flg",
            "acute_nursing_cnt",
            "nursing_assistance_cnt",
            "acute_assistance_flg",
            "acute_assistance_cnt",
            "night_acute_assistance_flg",
            "night_acute_assistance_cnt",
            "night_shift_add_flg",
            "specific_type",
            "nurse_staffing_cnt",
            "assistance_staffing_cnt"
    );
    
    $cond = "where standard_id = $standard_id ";
    $cond .= "and duty_yyyy = $duty_yyyy ";
    $cond .= "and duty_mm = $duty_mm ";
    $upd = update_set_table($con, $sql, $set, $content, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
}

/*実績はatdbkrsltから取得するため保存しない 20111019
///******************************************************************************
// 勤務実績情報テーブル（履歴）
///******************************************************************************
//実績予定フラグが"1"実績の場合
if ($plan_result_flag == "1") {
    ///-----------------------------------------------------------------------------
    // 取得期間設定
    ///-----------------------------------------------------------------------------
    //前月末から今月末までとする 20091028
    $chk_start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, "01");
    $start_date = date("Ymd", strtotime("-1 day", $obj->to_timestamp($chk_start_date)));
    //日数
    $day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
    $end_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $day_cnt);
    $calendar_array = $obj->get_calendar_array($start_date, $end_date);
    ///-----------------------------------------------------------------------------
    //ＤＢ(duty_shift_staff)より勤務シフトスタッフ情報）を取得
    ///-----------------------------------------------------------------------------
    
    $wk_group_id = "";
    $data_st = array();
    $data_job = array();
    $data_emp = array();
    $data_plan_staff = $obj->get_duty_shift_staff_array($wk_group_id, $data_st, $data_job, $data_emp);
    ///-----------------------------------------------------------------------------
    // 勤務実績情報を取得
    ///-----------------------------------------------------------------------------
    //指定施設基準の対象スタッフ（グループ）のみ設定
    $arr_emp_info = $obj->get_standard_empmst_array($standard_id, $duty_yyyy, $duty_mm);
    //print_r($arr_emp_info);
    $cond_add = "";
    for ($k=0; $k<count($arr_emp_info); $k++) {
        $wk_id = $arr_emp_info[$k]["id"];
        if ($cond_add == "") {
            $cond_add .= "and (a.emp_id = '$wk_id' ";
        } else {
            $cond_add .= "or a.emp_id = '$wk_id' ";
        }
    }
    if ($cond_add != "") {
        $cond_add .= ") ";
    }
    ///-----------------------------------------------------------------------------
    // 対象の勤務実績情報を取得
    ///-----------------------------------------------------------------------------
    //所属異動に対応するため「_for_report」のついた取得関数に変更 20091028 引数変更 20110901
    $data_rslt = $obj->get_atdbk_atdbkrslt_array_for_report("atdbkrslt", $cond_add, $calendar_array, $standard_id, "");
    ///-----------------------------------------------------------------------------
    // 削除
    ///-----------------------------------------------------------------------------
    $sql = "delete from duty_shift_atdbkrslt_history";
    $cond = "where standard_id = '$standard_id' ";
    $cond .= "and duty_yyyy = $duty_yyyy ";
    $cond .= "and duty_mm = $duty_mm ";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_reg_emp_id = array();
    ///-----------------------------------------------------------------------------
    // 登録
    ///-----------------------------------------------------------------------------
    for ($i=0; $i<count($data_rslt); $i++) {
        $wk_emp_id = $data_rslt[$i]["staff_id"];
        
        for ($k=1; $k<=count($calendar_array); $k++) {
            ///-----------------------------------------------------------------------------
            //データ設定
            ///-----------------------------------------------------------------------------
            $wk_date = $calendar_array[$k - 1]["date"];
            
            $wk_tmcd_group_id = $data_rslt[$i]["pattern_id_$k"];
            // データがない場合はスキップ
            if ($wk_tmcd_group_id == "") {
                continue;
            }
            // 施設基準を確認
            if ($data_rslt[$i]["standard_id_$k"] != $standard_id) {
                continue;
            }
            //重複を除く
            if ($arr_reg_emp_id[$wk_emp_id][$wk_date] == 1) {
                continue;
            }
            $arr_reg_emp_id[$wk_emp_id][$wk_date] = 1;
            
            $wk_pattern = $data_rslt[$i]["atdptn_ptn_id_$k"];
            $wk_start_time = $data_rslt[$i]["start_time_$k"];
            $wk_end_time = $data_rslt[$i]["end_time_$k"];
            $wk_meeting_time = $data_rslt[$i]["meeting_time_$k"];
            $wk_out_time = $data_rslt[$i]["out_time_$k"];
            $wk_ret_time = $data_rslt[$i]["ret_time_$k"];
            $wk_meeting_start_time = $data_rslt[$i]["meeting_start_time_$k"];
            $wk_meeting_end_time = $data_rslt[$i]["meeting_end_time_$k"];
            $wk_previous_day_flag = $data_rslt[$i]["previous_day_flag_$k"];
            if ($wk_previous_day_flag == "") {
                $wk_previous_day_flag = "0";
            }
            
            ///-----------------------------------------------------------------------------
            //ＳＱＬ
            ///-----------------------------------------------------------------------------
            $sql = "insert into duty_shift_atdbkrslt_history (";
            $sql .= "standard_id, ";
            $sql .= "duty_yyyy, ";
            $sql .= "duty_mm, ";
            $sql .= "emp_id, ";
            $sql .= "date, ";
            $sql .= "tmcd_group_id, ";
            $sql .= "pattern, ";
            $sql .= "start_time, ";
            $sql .= "end_time, ";
            $sql .= "meeting_time, ";
            $sql .= "out_time, ";
            $sql .= "ret_time, ";
            $sql .= "meeting_start_time, ";
            $sql .= "meeting_end_time, ";
            $sql .= "previous_day_flag ) values (";
            
            $content = array(
                    $standard_id,
                    $duty_yyyy,
                    $duty_mm,
                    pg_escape_string($wk_emp_id),
                    pg_escape_string($wk_date),
                    pg_escape_string($wk_tmcd_group_id),
                    pg_escape_string($wk_pattern),
                    pg_escape_string($wk_start_time),
                    pg_escape_string($wk_end_time),
                    pg_escape_string($wk_meeting_time),
                    pg_escape_string($wk_out_time),
                    pg_escape_string($wk_ret_time),
                    pg_escape_string($wk_meeting_start_time),
                    pg_escape_string($wk_meeting_end_time),
                    pg_escape_string($wk_previous_day_flag)
                    );
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}
*/
///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");
///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);
///-----------------------------------------------------------------------------
// 遷移元画面を再表示
///-----------------------------------------------------------------------------
echo("<script type=\"text/javascript\">location.href = 'duty_shift_report.php?session=$session&standard_id=$standard_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm';</script>");
?>
