<?
/*
  ワークシート機能実装より、テンプレートを「同時に」「複数」利用する必要があり、
  従来のようにテンプレートPHPファイルをrequire/includeすることができなくなった。

  このための解決方法として、テンプレートファイルをHTTPリクエスト越しに呼ぶようにしている。
  このファイルは、指定されたテンプレートの、指定された機能を代替実行するものである。
  実行結果は標準出力(echo/print)で返す。

  このファイルへの送受信は、sot_util.phpにより一元管理のもと行われている。
  例1) 入力テンプレートを表示させる流れ
     create_tmpl_xml.php(テンプレート作成タイミングのファイル)
     ⇒(関数呼出)⇒ sot_util.php
       ⇒(HTTP送信関数実行)⇒ sot_template_wrapper.php
         ⇒(require)⇒ 任意テンプレートファイルより、標準出力する
       ⇒ sot_util.phpの送信関数の戻り値で、内容の取得
     ⇒create_tmpl_xml.phpへ値を返す
*/

// 以降、エコーやプリント文による、標準出力は安易に行わないでください。
// XMLテキストの標準出力内容に混在してしまいます。ご注意ください。
// ここでは、about_authority.phpにて、HTMLを吐いているため、obを使用してこれをなくしています。
ob_start();
require_once("sot_util.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
ob_end_clean();


global $fname;
global $con;
if (!$fname) $fname = $PHP_SELF;
if (!$con) $con = connect2db($fname);

// テンプレートファイル（tmpl_XXXXXX.php）をリクワイアする
if (@$_REQUEST["wrap_template_file_name"]) {
	require($_REQUEST["wrap_template_file_name"]);
}
// XMLDOM作成ファイル(create_XXXXXX_xml.php）をリクワイアする
// (tmpl_XXXXX.phpと兼用の場合がある)
if (@$_REQUEST["wrap_template_file_name"] != @$_REQUEST["xml_creator"] && @$_REQUEST["xml_creator"]){
	require(@$_REQUEST["xml_creator"]);
}
//==========================================================
// sot_worksheet.phpからの呼び出し
// ワークシート用のHTMLのecho指示であれば、echoして終了。
// HTML出力は、write_tmpl_page_for_worksheet()関数で行う。
//==========================================================
if (@$_REQUEST["view_worksheet"] && @$_REQUEST["xml_file"]) {
	if (function_exists("write_tmpl_page_for_worksheet")){
		write_tmpl_page_for_worksheet(
			$con, $fname, $_REQUEST["xml_file"], $_REQUEST["target_ymd"], @$_REQUEST["enable_tmpl_chapter_codes"], @$_REQUEST["summary_seq"], @$_REQUEST["apply_id"], @$_REQUEST["global_element_index"]);
		die;
	}
}

//==========================================================
// create_tmpl_xml.phpからの呼び出し。
// xml_creator（create_XXXXXXX.xml_php）をインクルードした時点で、旧バージョンでは$textが作成される。
// 新バージョンでは、create_euc_xmldom_from_request()関数で行う。
//==========================================================
$text = "";
if (@$_REQUEST["xml_creator"]){
	if (function_exists("create_utf8_xmldom_from_request")){
		echo create_utf8_xmldom_from_request($con, $fname); // ここでecho
		die;
	}
	echo $text;
}
?>

