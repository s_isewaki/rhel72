<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理 | 掲示板 | 返信</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 親投稿の情報を取得
$sql = "select bbs_title as title, emp_id from bedbbs";
$cond = "where bbsthread_id = $theme_id and bbs_id = $post_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bbs_title = pg_fetch_result($sel, 0, "title");
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 表示値の編集
if (substr($bbs_title, 0, 4) != "RE: ") {
	$bbs_title = "RE: " . $bbs_title;
}

// ログインユーザの職員IDを取得
$login_emp_id = get_emp_id($con, $session, $fname);

// 初期表示時、内容、添付ファイルをクリア
if ($back != "t") {
	$bbs = "";
	$filename = array();
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer,paste",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "300",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid,pasteword",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}

function attachFile() {
	window.open('bed_menu_status_bbs_attach.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p.attach {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>返信</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<form action="bed_menu_status_bbs_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td width="480"><input name="bbs_title" type="text" size="50" maxlength="100" value="<? echo($bbs_title); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<td>
<textarea name="bbs" rows="15" cols="50" style="ime-mode:active;"><? echo($bbs); ?></textarea>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<div id="attach">
<?
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$ext = strrchr($tmp_filename, ".");

	echo("<p id=\"p_{$tmp_file_id}\" class=\"attach\">\n");
	echo("<a href=\"bbs/bed/tmp/{$session}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
	echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
	echo("</p>\n");
}
?>
</div>
<input type="button" value="追加" onclick="attachFile();">
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<!--<input type="button" value="一覧に戻る" onclick="location.href = 'bed_menu_status_bbs.php?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>&term=<? echo($term); ?>';">-->
<?
$back_url = ($emp_id == $login_emp_id) ? "bed_menu_status_bbs_post_update.php" : "bed_menu_status_bbs_detail.php" ;
?>
<input type="button" value="元投稿に戻る" onclick="location.href = '<? echo($back_url); ?>?session=<? echo($session); ?>&theme_id=<? echo($theme_id); ?>&post_id=<? echo($post_id); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>&term=<? echo($term); ?>';">
<input type="submit" value="登録">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="theme_id" value="<? echo($theme_id); ?>">
<input type="hidden" name="post_id" value="<? echo($post_id); ?>">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="show_content" value="<? echo($show_content); ?>">
<input type="hidden" name="term" value="<? echo($term); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
