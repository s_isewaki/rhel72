<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜入退院登録−退院情報参照</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("inpatient_list_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 患者氏名の取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");

// 退院情報の取得
$sql = "select * from inpthist";
$cond = "where ptif_id = '$pt_id' and inpt_in_dt = '$in_dt' and inpt_in_tm = '$in_tm'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$out_dt = pg_fetch_result($sel, 0, "inpt_out_dt");
$out_tm = pg_fetch_result($sel, 0, "inpt_out_tm");
$out_yr = substr($out_dt, 0, 4);
$out_mon = substr($out_dt, 4, 2);
$out_day = substr($out_dt, 6, 2);
$out_hr = substr($out_tm, 0, 2);
$out_min = substr($out_tm, 2, 2);
$out_dttm = $out_yr . "/" . $out_mon . "/" . $out_day . " " . $out_hr . ":" . $out_min;

$result = pg_fetch_result($sel, 0, "inpt_result");
switch ($result) {
case "1":
	$result = "死亡";
	break;
case "2":
	$result = "完治";
	break;
case "3":
	$result = "不変";
	break;
case "4":
	$result = "軽快";
	break;
case "5":
	$result = "後遺症残";
	break;
case "6":
	$result = "不明";
	break;
case "7":
	$result = "悪化";
	break;
case "8":
	$result = "その他";
	break;
default:
	$result = "";
	break;
}

$result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
if ($result_dtl != "") {
	$result_dtl = "詳細：" . $result_dtl;
}

$out_rsn_rireki = intval(pg_fetch_result($sel, 0, "inpt_out_rsn_rireki"));
$out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");

// 退院理由名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A308' and rireki_index = $out_rsn_rireki and item_cd = '$out_rsn_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$out_rsn = pg_fetch_result($sel2, 0, "item_name");

$out_pos_rireki = intval(pg_fetch_result($sel, 0, "inpt_out_pos_rireki"));
$out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");

// 退院先名称を取得
if ($out_pos_cd != "") {
	$sql = "select item_name from tmplitemrireki";
	$cond = "where mst_cd = 'A307' and rireki_index = $out_pos_rireki and item_cd = '$out_pos_cd'";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$out_pos = pg_fetch_result($sel2, 0, "item_name");
}

$out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
if ($out_pos_dtl != "") {
	$out_pos_dtl = "詳細：" . $out_pos_dtl;
}

// 退院先施設名を取得
$out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
if ($out_inst_cd != "") {
	$sql = "select mst_name from institemmst";
	$cond = "where mst_cd = '$out_inst_cd'";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$out_inst_name = pg_fetch_result($sel2, 0, "mst_name");

	// 診療科名・担当医名を取得
	$out_sect_rireki = pg_fetch_result($sel, 0, "inpt_out_sect_rireki");
	$out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
	$out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
	if ($out_sect_rireki != "" && $out_sect_cd != "") {
		$sql = "select * from institemrireki";
		$cond = "where mst_cd = '$out_inst_cd' and rireki_index = '$out_sect_rireki' and item_cd = '$out_sect_cd'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$out_sect_name = pg_fetch_result($sel2, 0, "item_name");
		$out_doctor_name = pg_fetch_result($sel2, 0, "doctor_name$out_doctor_no");
	}
}
$out_city = pg_fetch_result($sel, 0, "inpt_out_city");

// かかりつけ医情報を取得
$pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
if ($pr_inst_cd != "") {
	$sql = "select mst_name from institemmst";
	$cond = "where mst_cd = '$pr_inst_cd'";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pr_inst_name = pg_fetch_result($sel2, 0, "mst_name");

	// 診療科名・担当医名を取得
	$pr_sect_rireki = pg_fetch_result($sel, 0, "inpt_pr_sect_rireki");
	$pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
	$pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
	if ($pr_sect_rireki != "" && $pr_sect_cd != "") {
		$sql = "select * from institemrireki";
		$cond = "where mst_cd = '$pr_inst_cd' and rireki_index = '$pr_sect_rireki' and item_cd = '$pr_sect_cd'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$pr_sect_name = pg_fetch_result($sel2, 0, "item_name");
		$pr_doctor_name = pg_fetch_result($sel2, 0, "doctor_name$pr_doctor_no");
	}
}

$fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
switch ($fd_end) {
case "1":
	$fd_end = "朝";
	break;
case "2":
	$fd_end = "昼";
	break;
case "3":
	$fd_end = "夕";
	break;
default:
	$fd_end = "";
	break;
}

$out_comment = pg_fetch_result($sel, 0, "inpt_out_comment");

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
$back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
if ($back_bldg_cd != "" && $back_ward_cd != "") {
	$sql = "select ward_name from wdmst";
	$cond = "where bldg_cd = $back_bldg_cd and ward_cd = $back_ward_cd";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ward_name = pg_fetch_result($sel2, 0, "ward_name");
}

$ptrm = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
if ($back_bldg_cd != "" && $back_ward_cd != "" && $ptrm != "") {
	$sql = "select ptrm_name from ptrmmst";
	$cond = "where bldg_cd = $back_bldg_cd and ward_cd = $back_ward_cd and ptrm_room_no = $ptrm";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ptrm_name = pg_fetch_result($sel2, 0, "ptrm_name");
}

$bed = pg_fetch_result($sel, 0, "inpt_back_bed_no");

$back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
$back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
if ($back_in_dt != "" && $back_in_tm != "") {
	$back_in_dttm = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $back_in_dt) . " " . preg_replace("/(\d{2})(\d{2})/", "$1:$2", $back_in_tm);
}

$back_sect = pg_fetch_result($sel, 0, "inpt_back_sect_id");
if ($back_sect != "") {
	$sql = "select sect_nm from sectmst";
	$cond = "where sect_id = $back_sect";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sect_nm = pg_fetch_result($sel2, 0, "sect_nm");
}

$back_doc = pg_fetch_result($sel, 0, "inpt_back_dr_id");
if ($back_sect != "" && $back_doc != "") {
	$sql = "select dr_nm from drmst";
	$cond = "where sect_id = $back_sect and dr_id = $back_doc";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$dr_nm = pg_fetch_result($sel2, 0, "dr_nm");
}

$back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");

// チェック情報を取得
$sql = "select * from bedcheckout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 9; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="inpatient_detail_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院情報</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_nutrition_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="outpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>退院情報</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_dttm); ?></font></td>
</tr>
<? if ($display1 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$result $result_dtl"); ?></font></td>
</tr>
<? } ?>
<? if ($display2 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院理由</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_rsn); ?></font></td>
</tr>
<? } ?>
<? if ($display3 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$out_pos $out_pos_dtl"); ?></font></td>
</tr>
<? } ?>
<? if ($display4 == "t" || $display5 == "t") { ?>
<tr height="22">
<? if ($display4 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先施設名</font></td>
<td<? if ($display5 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_inst_name); ?></font></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td<? if ($display4 != "t") {echo(' colspan="3"');} ?> width="25%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_sect_name); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display6 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当医</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_doctor_name); ?></font></td>
</tr>
<? } ?>
<? if ($display7 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院先市区町村</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($out_city); ?></font></td>
</tr>
<? } ?>
<? if ($display9 == "t") { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">かかりつけ医</font></td>
<td colspan="3">

<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療機関名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pr_inst_name); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pr_sect_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当医</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pr_doctor_name); ?></font></td>
</tr>
</table>

</td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事終了帯</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($fd_end); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $out_comment)); ?></font></td>
</tr>
</table>
<? if ($display8 == "t") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="4">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">＜戻り入院の予定＞</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ward_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ptrm_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット番号</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($bed); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($back_in_dttm); ?></font></td>
</tr>
<tr height="22">
<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td width="34%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sect_nm); ?></font></td>
<td width="14%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主治医</font></td>
<td width="32%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dr_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転院目的</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $back_change_purpose)); ?></font></td>
</tr>
</table>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
