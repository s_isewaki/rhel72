<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="duty_shift_sync_setting.php" method="post">
<?
foreach ($_POST as $key => $val) {
	echo("<input type=\"hidden\" name=\"$key\" value=\"$val\">\n");
}
?>
<input type="hidden" name="postback" value="t">
</form>
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("duty_shift_common_class.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);
$obj = new duty_shift_common_class($con, $fname);

// 権限のチェック
if ($obj->check_authority_Management($session, $fname) == "") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
$links = array();
foreach ($_POST as $key => $val) {
	list($tmp_prefix, $tmp_group_id) = explode("_", $key);
	if ($tmp_prefix != "class" && $tmp_prefix != "atrb" && $tmp_prefix != "dept" && $tmp_prefix != "room") continue;
	if ($val == "") {
		if ($tmp_prefix != "room") {
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('リンク先は3階層目まで必須です。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}
		$val = null;
	}
	$links[$tmp_group_id][$tmp_prefix] = $val;
}

pg_query($con, "begin");

// 同期フラグの更新
$sql = "update duty_shift_option set";
$set = array("sync_flag");
$setvalue = array($sync_flag);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// リンク設定の更新
$sql = "delete from duty_shift_sync_link";
$del = delete_from_table($con, $sql, "", $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
foreach ($links as $tmp_group_id => $tmp_link) {
	$sql = "insert into duty_shift_sync_link (group_id, class_id, atrb_id, dept_id, room_id) values (";
	$content = array($tmp_group_id, $tmp_link["class"], $tmp_link["atrb"], $tmp_link["dept"], $tmp_link["room"]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

pg_query($con, "commit");
pg_close($con);

echo("<script type=\"text/javascript\">location.href = 'duty_shift_sync_setting.php?session=$session';</script>");
?>
</body>
