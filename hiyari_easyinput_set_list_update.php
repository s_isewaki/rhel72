<?php
//=========================================================
//
// 様式一覧 ソート
//
//=========================================================
require_once("about_comedix.php");
//---------------------------------------------------------
ob_start();
//ページ名
$fname = $_SERVER['PHP_SELF'];

// データベースに接続
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 48, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ソートの準備
$order_no = 1;
if(isset($_POST['eis_id'])){
	$row = $_POST['eis_id'];
}
//---------------------------------------------------------
// トランザクションを開始
pg_query($con, "begin transaction");

//--- classmst,atrbmst,deptmst,classroom

if($row != ""){
	foreach($row as $val){
		$sql = "update inci_easyinput_set set";
		$set = array("order_no");
		$setvalue = array($order_no);
		$cond = "where eis_id=$val";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$order_no++;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
ob_end_clean();
?>
<? //show_class_list($sel_class, $sel_atrb, $sel_dept, $sel_room, $class_id, $atrb_id, $dept_id, $session); ?>
<script type="text/javascript">location.href='hiyari_easyinput_set_list.php?session=<?=$session?>';</script>

