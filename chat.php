<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ネットカンファレンス | ネットカンファレンス</title>
<?
//------------------------------------------------------------------------------
// Title     :  発言表示フレーム（ネット会議画面）
// File Name :  chat.php
// History   :  2004/07/16 - リリース（岩本隆史）
// Copyright :
//------------------------------------------------------------------------------
require("about_session.php");
require("about_authority.php");

//==============================================================================
// 制御定数
//==============================================================================

$refresh_sec = 10;  // 自動リロード間隔（秒）

//==============================================================================
// 共通処理
//==============================================================================

$fname = $PHP_SELF;  // スクリプトパス

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ネット会議権限のチェック
$auth_nc = check_authority($session, 10, $fname);
if ($auth_nc == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================================================================
// 発言可能チェック
//==============================================================================

// データベースに接続
$con = connect2db($fname);

// ネット会議情報取得SQLの実行
$sql_nmrm = "select nmrm_start, nmrm_end, nmrm_lmt_flg, nmrm_end_flg, nmrm_del_flg from nmrmmst";
$cond_nmrm = "where nmrm_id = '$nmrm_id'";
$sel_nmrm = select_from_table($con, $sql_nmrm, $cond_nmrm, $fname);
if ($sel_nmrm == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$nmrm_lmt_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_lmt_flg");

// 制限フラグが立っていたらネット会議メンバーかどうかチェック
if ($nmrm_lmt_flg == 't') {
	$sql_nmme = "select count(*) from nmmemst";
	$cond_nmme = "where nmrm_id = '$nmrm_id' and emp_id = '$emp_id'";
	$sel_nmme = select_from_table($con, $sql_nmme, $cond_nmme, $fname);
	if ($sel_nmme == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	if (pg_fetch_result($sel_nmme, 0, 0) == 0) {
		pg_close($con);
		echo("<script language='javascript'>alert('メンバー以外は入室できません');");
		echo("parent.location.href = 'net_conference_menu.php?session=$session';</script>");
		exit;
	}
}

$now = date("YmdHi");
$nmrm_start = pg_fetch_result($sel_nmrm, 0, "nmrm_start");
$nmrm_end = pg_fetch_result($sel_nmrm, 0, "nmrm_end");
$nmrm_end_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_end_flg");
$nmrm_del_flg = pg_fetch_result($sel_nmrm, 0, "nmrm_del_flg");

// 開始日時
if ($nmrm_start > $now) {
	echo("<script language='javascript'>alert('会議は開始されていません');");
	echo("parent.location.href = 'net_conference_menu.php?session=$session';</script>");
	exit;
}

// 終了日時・終了フラグ
if (($nmrm_end <= $now) || ($nmrm_end_flg == "t")) {
	echo("<script language='javascript'>alert('会議は終了しました');");
	echo("parent.location.href = 'net_conference_menu.php?session=$session';</script>");
	exit;
}

// 削除フラグ
if ($nmrm_del_flg == "t") {
	echo("<script language='javascript'>alert('会議は中止されました');");
	echo("parent.location.href = 'net_conference_menu.php?session=$session';</script>");
	exit;
}

//==============================================================================
// 入力チェック
//==============================================================================

if (strlen($ntmt_msg) > 200) {
	echo("<script language='javascript'>alert('発言内容が長すぎます');");
	echo("history.back();</script>");
	exit;
}

//==============================================================================
// 発言登録処理
//==============================================================================

$inserted = false;  // 登録完了フラグ（JavaScript出力に使用）

if ($ntmt_msg != "") {

	// ネット会議発言IDの採番
	$sql_sel = "select count(*) + 1 as ntmt_msg_id from ntmtmst";
	$cond_sel = "where nmrm_id = '$nmrm_id'";
	$sel = select_from_table($con, $sql_sel, $cond_sel, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$ntmt_msg_id = pg_fetch_result($sel, 0, "ntmt_msg_id");

	// ネット会議発言登録SQLの実行
	$now = date("YmdHi");
	$sql_in = "insert into ntmtmst (nmrm_id, ntmt_msg_id, emp_id, ntmt_msg, ntmt_msg_time) values (";
	$content_in = array($nmrm_id, $ntmt_msg_id, $emp_id, $ntmt_msg, $ntmt_msg_time);
	$in = insert_into_table($con, $sql_in, $content_in, $fname);
	if ($in == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$inserted = true;

}

// データベース接続をクローズ
pg_close($con);

//==============================================================================
// HTML
//==============================================================================
?>
<script type="text/javascript">
<?
//------------------------------------------------------------------------------
// Title     :  初期表示時処理
// Name      :  initPage
// Arguments :  なし
// Return    :  なし
//------------------------------------------------------------------------------
?>
function initPage() {

<? if ($inserted) { ?>
	// 発言内容テキストボックスをクリアし、フォーカスする
	var txtbox = parent.document.forms['mainform'].elements['ntmt_msg'];
	txtbox.value = '';
	txtbox.focus();
	txtbox = null;
<? } ?>

	// 最新の発言が見られるよう一番下にスクロール
	scrollTo(0, document.body.scrollHeight);

	// リロード関数を実行するまでの時間を設定
	setTimeout("reloadPage();", <? echo $refresh_sec; ?> * 1000);
}

<?
//------------------------------------------------------------------------------
// Title     :  リロード処理
// Name      :  reloadPage
// Arguments :  なし
// Return    :  なし
//------------------------------------------------------------------------------
?>
function reloadPage() {

	location.href = 'chat.php?session=<? echo $session; ?>&nmrm_id=<? echo $nmrm_id ?>&emp_id=<? echo $emp_id; ?>';

}
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<? show_net_conference_msg_list($nmrm_id, $fname); ?>
</body>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// Title     :  ネット会議発言一覧表示
// Name      :  show_net_conference_msg_list
// Arguments :  nmrm_id - ネット会議ID
//              fname   - スクリプトパス
// Return    :  なし
//------------------------------------------------------------------------------
function show_net_conference_msg_list($nmrm_id, $fname) {

	// データベースに接続
	$con = connect2db($fname);

	// ネット会議発言一覧取得SQLの実行
	$sql = "select empmst.emp_lt_nm, ntmtmst.ntmt_msg from empmst inner join ntmtmst on empmst.emp_id = ntmtmst.emp_id";
	$cond = "where nmrm_id = '$nmrm_id' order by ntmt_msg_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// データベース接続をクローズ
	pg_close($con);

	// ネット会議発言一覧の出力
	while ($row = pg_fetch_array($sel)) {
		$emp_lt_nm = htmlspecialchars($row["emp_lt_nm"]);
		$ntmt_msg = htmlspecialchars($row["ntmt_msg"]);
?>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo $emp_lt_nm; ?>：<?echo $ntmt_msg; ?><br></font>
<?
	}
}
?>
