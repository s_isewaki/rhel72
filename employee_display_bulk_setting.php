<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 表示グループの一覧を配列に格納する
$sql = "select group_id, group_nm from dispgroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$disp_groups = array();
while ($row = pg_fetch_array($sel)) {
    $disp_groups[$row["group_id"]] = $row["group_nm"];
}

// デフォルトの設定内容を「表示グループで設定」にする
if ($action_mode == "") {
    $action_mode = "1";
}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
    $lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

// デフォルト値の設定
if ($back != "t") {
    if ($default_page == "") {$default_page = "01";}
    if ($logo_type == "") {$logo_type = "1";}
    if ($font_size == "") {$font_size = "1";}
    if ($mail_flg == "") {$mail_flg = "t";}
    if ($ext_flg == "") {$ext_flg = "t";}
    if ($inci_flg == "") {$inci_flg = "f";}
    if ($fplus_flg == "") {$fplus_flg = "f";}
    if ($manabu_flg == "") {$manabu_flg = "f";}
    if ($cas_flg == "") {$cas_flg = "f";}
    if ($event_flg == "") {$event_flg = "f";}
    if ($schd_flg == "") {$schd_flg = "t";}
    if ($info_flg == "") {$info_flg = "t";}
    if ($task_flg == "") {$task_flg = "t";}
    if ($whatsnew_flg == "") {$whatsnew_flg = "t";}
    if ($schdsrch_flg == "") {$schdsrch_flg = "t";}
    if ($msg_flg == "") {$msg_flg = "t";}
    if ($aprv_flg == "") {$aprv_flg = "t";}
    if ($fcl_flg == "") {$fcl_flg = "t";}
    if ($lib_flg == "") {$lib_flg = "t";}
    if ($bbs_flg == "") {$bbs_flg = "t";}
    if ($memo_flg == "") {$memo_flg = "t";}
    if ($link_flg == "") {$link_flg = "t";}
    if ($intra_flg == "") {$intra_flg = "t";}
    if ($bedinfo == "") {$bedinfo = "f";}
    if ($wic_flg == "") {$wic_flg = "f";}
    if ($schd_type == "") {$schd_type = "f";}
    if ($jnl_flg == "") {$jnl_flg = "f";}
    if ($top_free_text_flg == "") {$top_free_text_flg = "f";}
    if ($ladder_flg == "") {$ladder_flg = "f";}
    if ($top_timecard_error_flg == "") {$top_timecard_error_flg = "f";}
    if ($top_workflow_flg == "") {$top_workflow_flg = "f";}
    if ($top_ccusr1_flg == "") {$top_ccusr1_flg = "f";}
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | 表示一括設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function reverseAllOptions(box) {
    for (var i = 0, j = box.length; i < j; i++) {
        box.options[i].selected = !box.options[i].selected;
    }
}

function setRowsDisplay() {
    var group_mode = document.employee.action_mode[0].checked;
    $('tr.for_group').css('display', (group_mode) ? '' : 'none');
    $('tr.for_user').css('display', (group_mode) ? 'none' : '');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setRowsDisplay();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括設定</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>">権限</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示</font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_menu_bulk_setting.php?session=<? echo($session); ?>">メニュー</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="employee_condition_bulk_setting.php?session=<? echo($session); ?>">勤務条件</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示を一括設定しました。</font></td>
<? } else { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※設定内容は登録済みの職員にのみ反映されます。職員の追加登録時には再度一括設定して下さい。職員自身が「オプション設定」で変更することも可能です</font></td>
<? } ?>
</tr>
</table>
<form name="employee" action="employee_display_bulk_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="25%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td width="32%">
<select name="tgt_job[]" size="6" multiple>
<? show_options($sel_job, "job_id", "job_nm", $tgt_job); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_job[]']);">
</td>
<td width="11%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td width="32%">
<select name="tgt_st[]" size="6" multiple>
<? show_options($sel_st, "st_id", "st_nm", $tgt_st); ?>
</select>
<input type="button" value="全て反転" onclick="reverseAllOptions(this.form.elements['tgt_st[]']);">
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">設定内容</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="action_mode" value="1" onclick="setRowsDisplay();"<? if ($action_mode == "1") {echo(" checked");} ?>>表示グループで設定
<input type="radio" name="action_mode" value="2" onclick="setRowsDisplay();"<? if ($action_mode == "2") {echo(" checked");} ?>>個別に設定
</font></td>
</tr>
<tr class="for_group" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="disp_group_id">
<option value="">　　　　　</option>
<?
foreach ($disp_groups as $tmp_group_id => $tmp_group_nm) {
    echo("<option value=\"$tmp_group_id\"");
    if ($tmp_group_id == $disp_group_id) {
        echo(" selected");
    }
    echo(">$tmp_group_nm\n");
}
?>
</select>
</font></td>
</tr>
<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン時の表示ページ</font></td>
<td colspan="3"><? show_page_list($con, $default_page, $lcs_func, $fname); ?></td>
</tr>

<input type="hidden" name="logo_type" value="1" /><!-- ロゴタイプ:GIF -->

<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページの文字サイズ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="font_size" value="1"<? if ($font_size == "1") {echo(" checked");} ?>>標準
<input type="radio" name="font_size" value="2"<? if ($font_size == "2") {echo(" checked");} ?>>大
</font></td>
</tr>
<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページに表示</font></td>
<td colspan="3">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="mail_flg" value="t"<? if ($mail_flg == "t") {echo(" checked");} ?>>ウェブメール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="ext_flg" value="t"<? if ($ext_flg == "t") {echo(" checked");} ?>>一発電話検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="inci_flg" value="t"<? if ($inci_flg == "t") {echo(" checked");} ?><? if ($lcs_func["5"] == "f") {echo(" disabled");} ?>>ファントルくん</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fplus_flg" value="t"<? if ($fplus_flg == "t") {echo(" checked");} ?><? if ($lcs_func["13"] == "f") {echo(" disabled");} ?>>ファントルくん＋</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="jnl_flg" value="t"<? if ($jnl_flg == "t") {echo(" checked");} ?><? if ($lcs_func["14"] == "f") {echo(" disabled");} ?>>日報・月報</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="manabu_flg" value="t"<? if ($manabu_flg == "t") {echo(" checked");} ?><? if ($lcs_func["12"] == "f") {echo(" disabled");} ?>>バリテス</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="cas_flg" value="t"<? if ($cas_flg == "t") {echo(" checked");} ?><? if ($lcs_func["8"] == "f") {echo(" disabled");} ?>><? echo(get_cas_title_name()); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="event_flg" value="t"<? if ($event_flg == "t") {echo(" checked");} ?>><? echo($_label_by_profile["EVENT"][$profile_type]); ?></font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schd_flg" value="t"<? if ($schd_flg == "t") {echo(" checked");} ?>>スケジュール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="info_flg" value="t"<? if ($info_flg == "t") {echo(" checked");} ?>>お知らせ・回覧板</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="task_flg" value="t"<? if ($task_flg == "t") {echo(" checked");} ?>>タスク</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="whatsnew_flg" value="t"<? if ($whatsnew_flg == "t") {echo(" checked");} ?>>最新情報</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="schdsrch_flg" value="t"<? if ($schdsrch_flg == "t") {echo(" checked");} ?>>スケジュール検索</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="fcl_flg" value="t"<? if ($fcl_flg == "t") {echo(" checked");} ?>>設備予約</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="lib_flg" value="t"<? if ($lib_flg == "t") {echo(" checked");} ?>>文書管理</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bbs_flg" value="t"<? if ($bbs_flg == "t") {echo(" checked");} ?>>掲示板</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="memo_flg" value="t"<? if ($memo_flg == "t") {echo(" checked");} ?>>備忘録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="link_flg" value="t"<? if ($link_flg == "t") {echo(" checked");} ?>>リンクライブラリ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="intra_flg" value="t"<? if ($intra_flg == "t") {echo(" checked");} ?>>イントラネット</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bedinfo" value="t"<? if ($bedinfo == "t") {echo(" checked");} ?><? if ($lcs_func["2"] == "f") {echo(" disabled");} ?>><?
// 入退院カレンダー/入退室カレンダー
echo $_label_by_profile["BED_INFO"][$profile_type];
?>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="wic_flg" value="t"<? if ($wic_flg == "t") {echo(" checked");} ?>>WICレポート</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_free_text_flg" value="t"<? if ($top_free_text_flg == "t") {echo(" checked");} ?>>フリーテキスト</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="ladder_flg" name="ladder_flg" value="t"<? if ($ladder_flg == "t") {echo(" checked");} ?><? if ($lcs_func["17"] == "f") {echo(" disabled");} ?>>クリニカルラダー</font></td>
<!-- 打刻エラー表示 START -->
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" id="top_timecard_error_flg" name="top_timecard_error_flg" value="t"<? if ($top_timecard_error_flg == "t") {echo(" checked");} ?>>打刻エラー件数</font></td>
<!-- 打刻エラー表示 END -->
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_workflow_flg" value="t"<? if ($top_workflow_flg === "t") {echo(" checked");} ?>>決裁・申請</font></td>
<? if ($lcs_func[31]=="t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="top_ccusr1_flg" value="t"<? if ($top_ccusr1_flg === "t") {echo(" checked");} ?>>スタッフ・ポートフォリオ</font></td>
<? } else { ?>
<td></td>
<? } ?>
<td></td>
<td></td>
</tr>
</table>
</td>
</tr>
<tr class="for_user" height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュールに出勤予定を表示</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schd_type" value="t"<? if ($schd_type == "t") {echo(" checked");} ?>>する
<input type="radio" name="schd_type" value="f"<? if ($schd_type == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
    while ($row = pg_fetch_array($sel)) {
        $id = $row[$id_col];
        $nm = $row[$nm_col];
        echo("<option value=\"$id\"");
        if (in_array($id, $tgt_ids)) {
            echo(" selected");
        }
        echo(">$nm\n");
    }
}

function show_page_list($con, $selected, $lcs_func, $fname) {
    global $_label_by_profile;
    global $profile_type;

    require_once("get_menu_label.ini");
    $report_menu_label = get_report_menu_label($con, $fname);
    $shift_menu_label = get_shift_menu_label($con, $fname);

    echo("<select name=\"default_page\">");

    echo("<option value=\"01\"");
    if ($selected == "01") {echo(" selected");}
    echo(">マイページ");

    echo("<option value=\"02\"");
    if ($selected == "02") {echo(" selected");}
    echo(">ウェブメール");

    echo("<option value=\"03\"");
    if ($selected == "03") {echo(" selected");}
    echo(">スケジュール");

    echo("<option value=\"04\"");
    if ($selected == "04") {echo(" selected");}
    echo(">委員会・WG");

    echo("<option value=\"05\"");
    if ($selected == "05") {echo(" selected");}
    echo(">タスク");

    echo("<option value=\"06\"");
    if ($selected == "06") {echo(" selected");}
    echo(">伝言メモ");

    echo("<option value=\"07\"");
    if ($selected == "07") {echo(" selected");}
    echo(">設備予約");

    echo("<option value=\"08\"");
    if ($selected == "08") {echo(" selected");}
    echo(">出勤表");

    echo("<option value=\"09\"");
    if ($selected == "09") {echo(" selected");}
    echo(">決裁・申請");

    echo("<option value=\"10\"");
    if ($selected == "10") {echo(" selected");}
    echo(">ネットカンファレンス");

    echo("<option value=\"11\"");
    if ($selected == "11") {echo(" selected");}
    echo(">掲示板");

    echo("<option value=\"12\"");
    if ($selected == "12") {echo(" selected");}
    echo(">Q&amp;A");

    echo("<option value=\"13\"");
    if ($selected == "13") {echo(" selected");}
    echo(">文書管理");

    echo("<option value=\"22\"");
    if ($selected == "22") {echo(" selected");}
    echo(">お知らせ・回覧板");

    echo("<option value=\"14\"");
    if ($selected == "14") {echo(" selected");}
    echo(">アドレス帳");

    echo("<option value=\"23\"");
    if ($selected == "23") {echo(" selected");}
    echo(">内線電話帳");

    echo("<option value=\"26\"");
    if ($selected == "26") {echo(" selected");}
    echo(">リンクライブラリ");

    echo("<option value=\"15\"");
    if ($selected == "15") {echo(" selected");}
    echo(">パスワード・本人情報");

    echo("<option value=\"16\"");
    if ($selected == "16") {echo(" selected");}
    echo(">オプション設定");

//XX    echo("<option value=\"17\"");
//XX    if ($selected == "17") {echo(" selected");}
//XX    echo(">コミュニティサイト");

    if ($lcs_func["4"] == "t") {
        echo("<option value=\"21\"");
        if ($selected == "21") {echo(" selected");}
        echo(">イントラネット");
    }

    if ($lcs_func["7"] == "t") {
        echo("<option value=\"27\"");
        if ($selected == "27") {echo(" selected");}
        echo(">検索ちゃん");
    }

    if ($lcs_func["1"] == "t") {
        echo("<option value=\"18\"");
        if ($selected == "18") {echo(" selected");}
        // 患者管理/利用者管理
        echo(">".$_label_by_profile["PATIENT_MANAGE"][$profile_type]);
    }

    if ($lcs_func["15"] == "t") {
        echo("<option value=\"35\"");
        if ($selected == "35") {echo(" selected");}
        echo(">看護支援");
    }

    if ($lcs_func["2"] == "t") {
        echo("<option value=\"19\"");
        if ($selected == "19") {echo(" selected");}
        // 病床管理/居室管理
        echo(">".$_label_by_profile["BED_MANAGE"][$profile_type]);
    }

    if ($lcs_func["10"] == "t") {
        echo("<option value=\"30\"");
        if ($selected == "30") {echo(" selected");}
        echo(">看護観察記録");
    }

    if ($lcs_func["5"] == "t") {
        echo("<option value=\"24\"");
        if ($selected == "24") {echo(" selected");}
        echo(">ファントルくん");
    }

    if ($lcs_func["13"] == "t") {
        echo("<option value=\"33\"");
        if ($selected == "33") {echo(" selected");}
        echo(">ファントルくん＋");
    }

    if ($lcs_func["6"] == "t") {
        echo("<option value=\"25\"");
        if ($selected == "25") {echo(" selected");}
        // メドレポート/ＰＯレポート(Problem Oriented)
        echo(">$report_menu_label");
    }

    if ($lcs_func["12"] == "t") {
        echo("<option value=\"31\"");
        if ($selected == "31") {echo(" selected");}
        echo(">バリテス");
    }

    if ($lcs_func["9"] == "t") {
        echo("<option value=\"29\"");
        if ($selected == "29") {echo(" selected");}
        // 勤務シフト作成
        echo(">$shift_menu_label");
    }

    if ($lcs_func["11"] == "t") {
        echo("<option value=\"32\"");
        if ($selected == "32") {echo(" selected");}
        echo(">カルテビューワー");
    }

    if ($lcs_func["8"] == "t") {
        echo("<option value=\"28\"");
        if ($selected == "28") {echo(" selected");}
        require_once("get_cas_title_name.ini");
        echo(">" . get_cas_title_name());
    }

    if ($lcs_func["16"] == "t") {
        echo("<option value=\"36\"");
        if ($selected == "36") {echo(" selected");}
        echo(">人事管理");
    }

    if ($lcs_func["17"] == "t") {
        echo("<option value=\"37\"");
        if ($selected == "37") {echo(" selected");}
        echo(">クリニカルラダー");
    }

    if ($lcs_func["19"] == "t") {
        echo("<option value=\"39\"");
        if ($selected == "39") {echo(" selected");}
        echo(">キャリア開発ラダー");
    }

    if ($lcs_func["18"] == "t") {
        echo("<option value=\"38\"");
        if ($selected == "38") {echo(" selected");}
        echo(">勤務表");
    }

    if ($lcs_func["14"] == "t") {
        echo("<option value=\"34\"");
        if ($selected == "34") {echo(" selected");}
        echo(">日報・月報");
    }

    if ($lcs_func["3"] == "t") {
        echo("<option value=\"20\"");
        if ($selected == "20") {echo(" selected");}
        echo(">経営支援");
    }

    if ($lcs_func["31"] == "t") {
        echo("<option value=\"41\"");
        if ($selected == "41") {echo(" selected");}
        echo(">スタッフ・ポートフォリオ");
    }

    echo("</select>");
}
