<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

// ページ名
$fname = $PHP_SELF;

// セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if($qq1 == ""){
	echo("<script language=\"javascript\">alert(\"問1-1を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}


if($qq3 == ""){
	echo("<script language=\"javascript\">alert(\"問3を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($qq4 == ""){
	echo("<script language=\"javascript\">alert(\"問4を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($qq5_1 == ""){
	echo("<script language=\"javascript\">alert(\"問5（ある・なし）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

if($qq5_1 == "t" && $qq5_2 == ""){
	echo("<script language=\"javascript\">alert(\"問5（治療食・常食）を選択してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

// 登録値の編集
if($qq1_1 == "") $qq1_1 = "f";
else $qq1_1 = "t";

if($qq1_2 == ""){
	$qq1_2 = "f";
}else{
	$qq1_2 = "t";
}

if($qq1_3 == ""){
	$qq1_3 = "f";
}else{
	$qq1_3 = "t";
}

if($qq1_4 == ""){
	$qq1_4 = "f";
}else{
	$qq1_4 = "t";
}

if($qq2_1 == ""){
	$qq2_1 = "f";
}else{
	$qq2_1 = "t";
}

if($qq2_2 == ""){
	$qq2_2 = "f";
}else{
	$qq2_2 = "t";
}

if($qq2_3 == ""){
	$qq2_3 = "f";
}else{
	$qq2_3 = "t";
}

if($qq2_4 == ""){
	$qq2_4 = "f";
}else{
	$qq2_4 = "t";
}

if($qq2_5 == ""){
	$qq2_5 = "f";
}else{
	$qq2_5 = "t";
}

if($qq2_6 == ""){
	$qq2_6 = "f";
}else{
	$qq2_6 = "t";
}

if($qq2_7 == ""){
	$qq2_7 = "f";
}else{
	$qq2_7 = "t";
}

if($qq2_8 == ""){
	$qq2_8 = "f";
}else{
	$qq2_8 = "t";
}
if ($qq5_1 == "f") {
	$qq5_2 = "";
}
$up_date = date("Ymd") . date("Hi");

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con,"begin transaction");

// 栄養問診表情報の作成（delete - insert）
$sql = "delete from inptnut";
$cond = "where ptif_id = '$pt_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$content = array($pt_id,$qq1,$qq1_1,$qq1_2,$qq1_3,$qq1_4,$qq1_5_com,$qq1_6_com,$qq2_1,$qq2_2,$qq2_3,$qq2_4,$qq2_5,$qq2_6,$qq2_7,$qq2_8,$qq3,$qq4,$qq5_1,$qq5_2,$up_date);
$in = insert_into_table($con,$SQL124,$content,$fname);
if($in == 0){
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 栄養問診表画面を再表示
if ($path == "4" || $path == "5") {
	echo("<script language='javascript'>location.href = 'inpatient_nutrition.php?session=$session&pt_id=$pt_id&path=$path';</script>\n");
} else if ($path == "M") {
	echo("<script language='javascript'>location.href = 'summary_inpatient_nutrition.php?session=$session&pt_id=$pt_id';</script>\n");
} else {
	$url_pt_nm = urlencode($pt_nm);
	echo("<script language='javascript'>location.href = 'bed_menu_inpatient_nutrition.php?session=$session&pt_id=$pt_id&path=$path&pt_nm=$url_pt_nm&search_sect=$search_sect&search_doc=$search_doc';</script>\n");
}
?>
