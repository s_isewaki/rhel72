<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
require("about_session.php");
require("about_authority.php");
require("schedule_repeat_common.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

$current_regex_encoding = mb_regex_encoding('EUC-JP');
if (mb_ereg_replace("[ 　]", "", $schd_title) === "") {
	echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
mb_regex_encoding($current_regex_encoding);
if (strlen($schd_title) > 100) {
	echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($schd_plc) > 50) {
	echo("<script type=\"text/javascript\">alert('行先（テキスト）が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($rptopt == "" || $rptopt == "1") {
	if (!checkdate($schd_start_mth, $schd_start_day, $schd_start_yrs)) {
		echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}
if ($timeless == "") {
	if ("$schd_start_hrs$schd_start_min" >= "$schd_dur_hrs$schd_dur_min") {
		echo("<script type=\"text/javascript\">alert('時刻が不正です。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}
/*
if (strlen($schd_detail) > 200) {
	echo("<script type=\"text/javascript\">alert('詳細が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
*/

// 日付の設定
$date = ($rptopt == "" || $rptopt == "1") ? "$schd_start_yrs$schd_start_mth$schd_start_day" : "";

// トランザクションの開始
pg_query($con, "begin transaction");

// 更新対象スケジュール一覧を取得
$upd_schedules = get_repeated_project_schedules($con, $pjt_schd_id, $original_timeless, $rptopt, $fname);

// 行先が未指定の場合DBにnull設定
if ($place_id == "") {
	$place_id = null;
}

// スケジュールを更新
foreach ($upd_schedules as $tmp_schedule) {
	$tmp_pjt_schd_id = $tmp_schedule["id"];
	$tmp_timeless = $tmp_schedule["timeless"];

	// 時間指定の有無が変わらない場合
	if ($tmp_timeless == $timeless) {

		// スケジュール情報を更新
		$table_name = ($tmp_timeless != "t") ? "proschd" : "proschd2";
		$sql = "update $table_name set";
		$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
		$set = array("pjt_schd_title", "pjt_schd_place_id", "pjt_schd_plc", "pjt_schd_type", "pjt_schd_detail");
		$setvalue = array($schd_title, $place_id, $schd_plc, $schd_type, $schd_detail);
		if ($date != "") {
			array_push($set, "pjt_schd_start_date");
			array_push($setvalue, $date);
		}
		if ($tmp_timeless != "t") {
			array_push($set, "pjt_schd_start_time_v", "pjt_schd_dur_v");
			array_push($setvalue, "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
		}
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	// 時間指定の有無が変わる場合
	} else {

		// 更新元のスケジュール情報を取得
		$table_name = ($tmp_timeless != "t") ? "proschd" : "proschd2";
		$sql = "select emp_id, repeat_flg, reg_time, pjt_schd_start_date from $table_name";
		$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$emp_id = pg_fetch_result($sel, 0, "emp_id");
		$repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
		$reg_time = pg_fetch_result($sel, 0, "reg_time");
		$schd_start_date = pg_fetch_result($sel, 0, "pjt_schd_start_date");

		// 更新元のスケジュール情報を削除
		$sql = "delete from $table_name";
		$cond = "where pjt_schd_id = '$tmp_pjt_schd_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// スケジュール情報を登録
		$tmp_date = ($date == "") ? $schd_start_date : $date;
		$new_table_name = ($timeless != "t") ? "proschd" : "proschd2";
		$columns = array("pjt_schd_id", "pjt_id", "emp_id", "pjt_schd_title", "pjt_schd_place_id", "pjt_schd_plc", "pjt_schd_start_date", "pjt_schd_type", "pjt_schd_detail", "repeat_flg", "reg_time");
		$content = array($tmp_pjt_schd_id, $pjt_id, $emp_id, $schd_title, $place_id, $schd_plc, $tmp_date, $schd_type, $schd_detail, $repeat_flg, $reg_time);
		if ($timeless != "t") {
			// 24:00対応
			$schd_dur = "$schd_dur_hrs:$schd_dur_min";
			if ($schd_dur == "24:00") {
				$schd_dur = "23:59";
			}
			array_push($columns, "pjt_schd_start_time", "pjt_schd_dur", "pjt_schd_start_time_v", "pjt_schd_dur_v");
			array_push($content, "$schd_start_hrs:$schd_start_min", "$schd_dur_hrs:$schd_dur_min", "$schd_start_hrs$schd_start_min", "$schd_dur_hrs$schd_dur_min");
		}
		$sql = "insert into $new_table_name (" . join(", ", $columns) . ") values (";
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">window.opener.location.reload();</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
?>
