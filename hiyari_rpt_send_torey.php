<?php
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_auth_models.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("hiyari_report_class.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//テスト用。
//TODO リリース時はコメントアウト必須。
//ini_set("display_errors","1");

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//パラメータ精査
//==============================

//日付検索(年)初期値は今年
if($search_date_year == "")
{
	$search_date_year = date("Y");
}
//日付検索(月)初期値は今月
if($search_date_month == "")
{
	$search_date_month = date("m");
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//更新権限(範囲無視)
$is_report_db_update_able = is_report_db_update_able($session,$fname,$con);

// 参照権限のみか、更新権限ありか
if (is_inci_auth_emp($session,$fname,$con) && $is_report_db_update_able) {
     //更新権限あり
    $is_update = true;
}
else{
    $is_update = false;
}

//==============================
//処理モードごとの処理
//==============================
if($is_postback == "true")
{
	switch ($mode)
	{
		case "delete":
			
			//==============================
			//送信削除
			//==============================
			if(!empty($list_select))
			{
				$select_count=count($list_select);
				for($i=0;$i<$select_count;$i++)
				{
					if(!$list_select[$i]=="")
					{
						//メールID
						$mail_id = $list_select[$i];
						
						//受信者ID
						$send_emp_id = $emp_id;
						
						//受信メールの論理削除
						delete_send_mail($con,$fname,$mail_id,$send_emp_id);
					}
				}
			}
			break;
		default:
			break;
	}
}

//==============================
//送信メール一覧取得
//==============================
$where = "where send_emp_id = '$emp_id' and not send_del_flg = 't' and not kill_flg";

//日付による絞込み
if($search_date_mode == "month")
{
	$search_date_1 = $search_date_year."/".$search_date_month;
	$where .= " and substr(send_date,0,11) like '$search_date_1%' ";
}
elseif($search_date_mode == "ymd_st_ed")
{
	if($search_date_ymd_st != "")
	{
		$where .= " and substr(send_date,0,11) >= '$search_date_ymd_st' ";
	}
	if($search_date_ymd_ed != "")
	{
		$where .= " and substr(send_date,0,11) <= '$search_date_ymd_ed' ";
	}
}

if($search_patient_id != "")
{
	$search_patient_id2 = pg_escape_string($search_patient_id);
	$where .= " and input_item_210_10 = '$search_patient_id2' ";
}	

if($search_patient_name != "")
{
	$search_patient_name2 = pg_escape_string($search_patient_name);
	$where .= " and input_item_210_20 like '%$search_patient_name2%' ";
}

if($search_patient_year_from == "900")
{
	$where .= " and input_item_210_40 = '$search_patient_year_from' ";
}
else
{
	if($search_patient_year_from != "" && $search_patient_year_to != "")
	{
		$where .= " and input_item_210_40 >= '$search_patient_year_from' ";
		$where .= " and input_item_210_40 <= '$search_patient_year_to' ";
	}
	else if($search_patient_year_from != "" && $search_patient_year_to == "")
	{
		$where .= " and input_item_210_40 >= '$search_patient_year_from' ";
		$where .= " and input_item_210_40 < '900' ";
	}
	else if($search_patient_year_from == "" && $search_patient_year_to != "")
	{
		$where .= " and input_item_210_40 <= '$search_patient_year_to' ";
	}
}

if($search_report_no != "")
{
	$search_report_no2 = pg_escape_string($search_report_no);
	$where .= " and report_no like '{$search_report_no2}%' ";
}

if($search_torey_date_in_search_area_start != "")
{
	$where .= " and substr(send_date,0,11) >= '$search_torey_date_in_search_area_start' ";
}
if($search_torey_date_in_search_area_end != "")
{
	$where .= " and substr(send_date,0,11) <= '$search_torey_date_in_search_area_end' ";
}

if($search_incident_date_start != "")
{
	$where .= " and incident_date >= '$search_incident_date_start'";
}
if($search_incident_date_end != "")
{
	$where .= " and incident_date <= '$search_incident_date_end'";
}


//検索条件による絞込み
//※送信済みメールの情報から受信者を参照すると複数件となる。ここでは絞り込まず、SQL実行後に絞り込む。

if($sort_div == "0")
{
	$sort = "asc";
}
else
{
	$sort = "desc";
}	


$order = " order by";
switch($sort_item)
{
	case "REPORT_NO":
		$order .= " report_no $sort ,";
		break;
	case "SUBJECT":
		$order .= " subject $sort ,";
		break;
	case "SEND_DATE":
		$order .= " send_date $sort ,";
		break;
	case "LEVEL":
		$order .= " level_num $sort ,";
		break;
    case "SM_START":
        $order .= " sm_start_flg $sort ,";
        break;
    case "SM_END":
        $order .= " sm_end_flg $sort ,";
        break;
    case "SM_EVAL":
        $order .= " sm_evaluation_flg $sort ,";
        break;
    case "RM_START":
        $order .= " rm_start_flg $sort ,";
        break;
    case "RM_END":
        $order .= " rm_end_flg $sort ,";
        break;
    case "RM_EVAL":
        $order .= " rm_evaluation_flg $sort ,";
        break;
    case "RA_START":
        $order .= " ra_start_flg $sort ,";
        break;
    case "RA_END":
        $order .= " ra_end_flg $sort ,";
        break;
    case "RA_EVAL":
        $order .= " ra_evaluation_flg $sort ,";
        break;
    case "SD_START":
        $order .= " sd_start_flg $sort ,";
        break;
    case "SD_END":
        $order .= " sd_end_flg $sort ,";
        break;
    case "SD_EVAL":
        $order .= " sd_evaluation_flg $sort ,";
        break;
    case "MD_START":
        $order .= " md_start_flg $sort ,";
        break;
    case "MD_END":
        $order .= " md_end_flg $sort ,";
        break;
    case "MD_EVAL":
        $order .= " md_evaluation_flg $sort ,";
        break;
    case "HD_START":
        $order .= " hd_start_flg $sort ,";
        break;
    case "HD_END":
        $order .= " hd_end_flg $sort ,";
        break;
    case "HD_EVAL":
        $order .= " hd_evaluation_flg $sort ,";
        break;
        
	default:
		//何もしない。
		break;
}

$order .= " mail_id desc";

$fields = array(
			'report_id',
//			'report_title',
//			'job_id',
//			'registrant_id',
//			'registrant_name',
//			'registration_date',
//			'eis_id',
//			'eid_id',
//			'del_flag',
			'report_no',
//			'shitagaki_flg',
//			'report_comment',
//			'edit_lock_flg',
//			'anonymous_report_flg',
//			'registrant_class',
//			'registrant_attribute',
//			'registrant_dept',
//			'registrant_room',
			'kill_flg',

//			'report_emp_class',
//			'report_emp_attribute',
//			'report_emp_dept',
//			'report_emp_room',
//			'report_emp_job',
//			'report_emp_lt_nm',
//			'report_emp_ft_nm',
//			'report_emp_kn_lt_nm',
//			'report_emp_kn_ft_nm',

//			'[auth]_update_emp_id',
			'[auth]_start_flg',
			'[auth]_end_flg',
            '[auth]_evaluation_flg',
//			'[auth]_start_date',
//			'[auth]_end_date',
			'[auth]_use_flg',

			'mail_id',
			'subject',
			'send_emp_id',
//			'send_emp_name',
			'send_date',
			'send_del_flg',
			'send_job_id',
			'send_eis_id',
//			'anonymous_send_flg',
			'marker',

//			'send_emp_class',
//			'send_emp_attribute',
//			'send_emp_dept',
//			'send_emp_room',
//			'send_emp_job',
//			'send_emp_lt_nm',
//			'send_emp_ft_nm',
//			'send_emp_kn_lt_nm',
//			'send_emp_kn_ft_nm',

			'recv_emp_id',
//			'recv_emp_name',
//			'recv_class',
//			'recv_class_disp_index',
//			'recv_read_flg',
//			'recv_read_flg_update_date',
//			'recv_del_flg',
//			'anonymous_recv_flg',
//
//			'recv_emp_class',
//			'recv_emp_attribute',
//			'recv_emp_dept',
//			'recv_emp_room',
//			'recv_emp_job',
//			'recv_emp_lt_nm',
//			'recv_emp_ft_nm',
//			'recv_emp_kn_lt_nm',
//			'recv_emp_kn_ft_nm',
			
//			'report_link_id',
//			'report_link_status',
//			'main_report_id',
			'level',
			'level_num',
			'input_item_210_10',
			'input_item_210_20',
			'input_item_210_40',
			'incident_date',
            'eis_name',
            'eis_no',
	);

$list_data_array = get_send_mail_list($con, $fname, $where, $order, $fields);

//==============================
//検索条件による絞込み
//==============================
//※送信済みトレイ固有の処理

if($search_emp_class != "" || $search_emp_attribute != "" || $search_emp_dept != "" || $search_emp_room != "" || $search_emp_job != "" || $search_emp_name != "" ||
	$search_patient_id != "" || $search_patient_name != "" || $search_patient_year_from != "" || $search_patient_year_to != "" || $search_incident_date_start != "" || $search_incident_date_end != "")

{
	$list_data_array_work = array();
	foreach($list_data_array as $list_data)
	{
		//メールID
		$mail_id =  $list_data["mail_id"];
		
		//メールの受信者情報一覧取得
		$sql = "";
		$sql .= "select count(*) as cnt from (select * from inci_mail_recv_info where mail_id = $mail_id ) recv ";
//		$sql .= "select empmst.*, inci_mail_recv_info.anonymous_recv_flg from (select * from inci_mail_recv_info where mail_id = $mail_id ) recv ";
		$sql .= "inner join empmst on recv.recv_emp_id = empmst.emp_id ";


		$where = "where true ";

		$where_class = "";
		if($search_emp_class != "")
		{
			$where_class .= " emp_class = $search_emp_class ";
		}
		if($search_emp_attribute != "")
		{
			$where_class .= " and emp_attribute = $search_emp_attribute ";
		}
		if($search_emp_dept != "")
		{
			$where_class .= " and emp_dept = $search_emp_dept ";
		}
		if($search_emp_room != "")
		{
			$where_class .= " and emp_room = $search_emp_room ";
		}
		
		if($where_class != "")
		{
			$where .= " and recv_emp_id in (select emp_id from empmst where $where_class union select emp_id from concurrent where $where_class) ";
		}

		if($search_emp_job != "")
		{
			$where .= " and emp_job = $search_emp_job ";
		}

		if($search_emp_name != "")
		{
			$search_emp_name2 = mb_ereg_replace(" ", "", pg_escape_string($search_emp_name));
			$search_emp_name2 = mb_ereg_replace("　", "", $search_emp_name2);
			$where .= " and ";



			$where .= " ( ";
			$where .= "   ( ";
			$where .= "     anonymous_recv_flg ";
			$where .= "     and ";
			$where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
			$where .= "   ) ";
			$where .= "   or ";
			$where .= "   ( ";
			$where .= "     not anonymous_recv_flg ";
			$where .= "     and  ";
			$where .= "     ( ";
			$where .= "          emp_lt_nm like '%$search_emp_name2%' ";
			$where .= "       or emp_ft_nm like '%$search_emp_name2%' ";
			$where .= "       or emp_kn_lt_nm like '%$search_emp_name2%' ";
			$where .= "       or emp_kn_ft_nm like '%$search_emp_name2%' ";
			$where .= "       or (emp_lt_nm || emp_ft_nm) like '%$search_emp_name2%' ";
			$where .= "       or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$search_emp_name2%' ";
			$where .= "     ) ";
			$where .= "   ) ";
			$where .= " ) ";

		}
		
		$sel = select_from_table($con, $sql, $where, $fname);
		if ($sel == 0){
			pg_close($con);
			showErrorPage();
			exit;
		}		
		
		//検索条件に一致する受信者が１人以上いる場合は表示対象。
//		if(pg_num_rows($sel) != 0)
		if(pg_fetch_result($sel, 0, "cnt") > 0)
		{
			$list_data_array_work[] = $list_data;
		}
	}
	$list_data_array = $list_data_array_work;
}

//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
	$page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($list_data_array);
if($rec_count == 1 && $list_data_array[0] == "")
{
	$rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
	if($i >= $rec_count)
	{
		break;
	}
	$list_data_array_work[] = $list_data_array[$i];
}
$list_data_array = $list_data_array_work;

//==============================
//インシデント権限マスタ取得
//==============================
$arr_inci = get_inci_mst($con, $fname);

//SM表示フラグ取得/SM除去
$sm_disp_flg = get_sm_disp_flg($fname, $con);
if($sm_disp_flg != "t")
{
	$wk_arr_inci = null;
	foreach($arr_inci as $auth => $auth_info)
	{
		if($auth != "SM")
		{
			$wk_arr_inci[$auth] = $auth_info;
		}
	}
	$arr_inci = $wk_arr_inci;
}


//==============================
//患者影響レベル略称情報を取得
//==============================
$level_disp_list = get_inci_level_short_name($con,$fname);

//==============================
//表示
//==============================
$page_title = "送信トレイ";

//------------------------------
//実行アイコン
//------------------------------
$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$icon_disp = array();
$icon_disp['create'] = $arr_btn_flg['create_btn_show_flg'] == 't';
$icon_disp['return'] = FALSE;
$icon_disp['forward'] = $arr_btn_flg['forward_btn_show_flg'] == 't';
$icon_disp['delete1'] = TRUE;
$icon_disp['edit'] = is_mail_report_editable($session,$fname,$con);
$icon_disp['read_flg'] = FALSE;
$icon_disp['progress'] = is_progres_editable($session,$fname,$con);
$icon_disp['analysis'] = is_report_analysis_usable($session,$fname,$con);
$icon_disp['analysis_register'] = FALSE;
$icon_disp['excel'] = FALSE;
$icon_disp['delete2'] = FALSE;
$icon_disp['undelete'] = FALSE;
$icon_disp['kill'] = FALSE;
$icon_disp['search'] = TRUE;

//------------------------------
//進捗表示
//------------------------------
$auth_list = array();
$progress_use = array();
$evaluation_use = array();  //担当者毎の評価機能利用フラグ
$is_evaluation_use = false; //評価機能を利用する担当者がいるかどうかのフラグ
if ($design_mode == 1){
    foreach($arr_inci as $arr_key => $arr){
        $progress_use[$arr_key] = is_usable_auth($arr_key,$fname,$con);
    }
}
else{
    $arr_inci_progress = search_inci_report_progress_dflt_flg($con, $fname);
    $num = pg_numrows($arr_inci_progress);
    for($i=0; $i<$num; $i++) {
        $auth = pg_result($arr_inci_progress,$i,"auth");
        $progress_use[$auth] = (pg_result($arr_inci_progress,$i,"use_flg") == 't');
        $evaluation_use[$auth] = ($sysConf->get('fantol.evaluation.use.' . strtolower($auth) . '.flg') == 't');
        if ($progress_use[$auth] && ($auth != 'SM' || $sm_disp_flg == 't')){
            $auth_list[$auth]['auth_name'] = h(pg_result($arr_inci_progress,$i,"auth_short_name"));
            $auth_list[$auth]['eval_use'] = $evaluation_use[$auth];
        }
        if ($evaluation_use[$auth]){
            $is_evaluation_use = true;
        }
    }
}

//------------------------------
//一覧項目表示フラグ
//------------------------------
$disp_flag = array();
$disp_flag = get_send_torey_list_disp($con,$fname);

//------------------------------
//ステータス
//------------------------------
$report_ids = '';
foreach($list_data_array as $i => $list_data){
    if ($i > 0){
        $report_ids.= ",";
    }
    $report_ids.= "'" . $list_data["report_id"] . "'";
}

if ($report_ids != '') {
    //評価ステータス
    $emp_auth = get_emp_priority_auth($session,$fname,$con);
    if ($design_mode == 2){
        if ($evaluation_use[$emp_auth]){
            $sql = "SELECT report_id, auth, evaluation_flg FROM inci_report_progress WHERE report_id IN ($report_ids)";
            $sel = select_from_table($con, $sql, "", $fname);
            $rows = pg_fetch_all($sel);
            foreach ($rows as $r){
                $evaluation_status[$r['report_id']][$r['auth']] = $r['evaluation_flg'];
            }
        }
    }

    //分析ステータス
    $sql = "SELECT analysis_problem_id FROM inci_analysis_regist WHERE available AND analysis_problem_id IN ($report_ids)";
    $sel = select_from_table($con, $sql, "", $fname);
    $rows = pg_fetch_all($sel);
    foreach ($rows as $r){
        $analysis_status[] = $r['analysis_problem_id'];
    }
}


//------------------------------
//一覧データ
//------------------------------
$rep_obj = new hiyari_report_class($con, $fname);

//匿名表示
if ($design_mode == 1){
    $anonymous_name = h(get_anonymous_name($con,$fname));
}
else{
    $anonymous_name = '匿名';
}
$mouseover_popup_flg = is_anonymous_readable($session, $con, $fname);

$list = array();
foreach($list_data_array as $i => $list_data){
	$mail_id =  $list_data["mail_id"];
    $list[$i]['mail_id'] = $mail_id;
    $list[$i]['list_id'] = $mail_id;

	$report_id =  $list_data["report_id"];
    $list[$i]['report_id'] =  $report_id;

    $list[$i]['marker_style'] = get_style_by_marker($list_data["marker"]);

    //ステータス
    if ($design_mode == 2){
    $status[3]['flag'] = $rep_obj->get_sm_lock($report_id);
    $status[3]['name'] = 'ロック';

        if ($is_evaluation_use){
        if ($evaluation_use[$emp_auth]){
            $status[4]['flag'] = $evaluation_status[$report_id][$emp_auth] == 't';
        }
             else{
                 $status[4]['flag'] = false;
             }
            $status[4]['name'] = '評価';
    }

        $status[5]['flag'] = in_array($report_id, $analysis_status);
    $status[5]['name'] = '分析';

    $list[$i]['status'] = $status;
    }

    //事案番号
    $list[$i]['report_no'] = $list_data["report_no"];

    //件名
    $list[$i]['subject'] = h($list_data["subject"]);
    
    //送信時報告者職種
    $list[$i]['send_job_id'] = h($list_data["send_job_id"]);
    
    //送信時様式ID
    $list[$i]['send_eis_id'] = h($list_data["send_eis_id"]);
    
    //送信時様式No
    $list[$i]['eis_no'] = h($list_data["eis_no"]);

    //受信者（先頭の受信者のみ表示）
	$arr_all_recv_emp_names = get_all_recv_emp_names($con,$fname,$mail_id);
	if(count($arr_all_recv_emp_names) > 0){
        $real_name = h($arr_all_recv_emp_names[0]["recv_emp_name"]);
        $list[$i]['real_name'] = $real_name;

        $anonymous_flg = $arr_all_recv_emp_names[0]["anonymous_recv_flg"] == "t";
        $list[$i]['anonymous_flg'] = $anonymous_flg;

        if ($anonymous_flg){
            $list[$i]['disp_emp_name'] = $anonymous_name;
        }
        else{
            $list[$i]['disp_emp_name'] = $real_name;
        }        
        
        $list[$i]['mouseover_popup_flg'] = $mouseover_popup_flg;
    }

    //送信日
	$list[$i]["send_date"] =  substr($list_data["send_date"],0,10);

    //既読
	$recv_read_count = get_recv_count_for_send_mail($con,$fname,$mail_id,"TO","t");
	$recv_all_count = get_recv_count_for_send_mail($con,$fname,$mail_id,"TO");
	$list[$i]["read_count"] = $recv_read_count."/".$recv_all_count;
    
    //影響
	$level = $list_data["level"];
	if($level != ""){
		$level = $level_disp_list[$level];
	}
    $list[$i]["level"] = $level;

    //進捗
    $progress_comment_use_flg = get_progress_comment_use($con,$fname);

    $auth_info = array();
    foreach($arr_inci as $arr_key => $arr_val) {
        $progress_array = array();
        
        if($progress_use[$arr_key]){
                
            $sql = "select * from inci_report_progress_detail";
            $cond = "where report_id = '$report_id' and auth = '$arr_key' and use_flg = 't'";
            //$cond = "where mail_id =". $mail_id. " and auth = '$arr_key' and use_flg = 't'";
            $result = select_from_table($con,$sql,$cond,$fname);
            $progress_array = pg_fetch_all($result);
            
            //$start_flg ='f';
            //$end_flg ='f';
            //$eval_flg = 'f';
            
            $auth_ = strtolower($arr_key) .'_start_flg';
            $start_flg = $list_data["$auth_"] ;
            
            $auth_ = strtolower($arr_key) .'_end_flg';
            $end_flg = $list_data["$auth_"] ;
                    
            $auth_ = strtolower($arr_key) .'_evaluation_flg';
            $eval_flg = $list_data["$auth_"] ;       
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    if($value['start_flg']=='t'){
                        $start_flg = 't';
                    }else{
                        $start_flg ='f';
                        break;
                    }
                }
            }
            
            $start_flg_individual = 'f';
            if($start_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
                    $sql = "select start_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $start_flg_individual_tmp=pg_fetch_result($results, 0, "start_flg");
 
                    if($start_flg_individual_tmp=='t'){
                        $start_flg_individual = 't';
                    }else {
                        $start_flg_individual = 'f';
                    }
                }
            }
                        
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    if($value['end_flg']=='t'){
                        $end_flg = 't';
                    }else{
                        $end_flg ='f';
                        break;
                    }
                }
            }
            
            $end_flg_individual = 'f';
            if($end_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.confirm.flg') == 't'){
                    $sql = "select end_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $end_flg_individual_tmp=pg_fetch_result($results, 0, "end_flg");
                    
                    if($end_flg_individual_tmp=='t'){
                        $end_flg_individual = 't';
                    }else {
                        $end_flg_individual = 'f';
                    }
                }
            }
            
            foreach ($progress_array as $value) {
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    if($value['evaluation_flg']=='t'){
                        $eval_flg = 't';
                    }else{
                        $eval_flg ='f';
                        break;
                    }
                }
            }
              
            $eval_flg_individual = 'f';
            if($eval_flg =='f'){
                if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.eval.flg') == 't'){
                    $sql = "select evaluation_flg from inci_report_progress_detail";
                    $cond = " where mail_id = '$mail_id' and auth = '$arr_key' and emp_id= '$emp_id' and use_flg = 't'";
            
                    $results = select_from_table($con,$sql,$cond,$fname);
                    $eval_flg_individual_tmp=pg_fetch_result($results, 0, "evaluation_flg");
                    
                    if($eval_flg_individual_tmp=='t'){
                        $eval_flg_individual = 't';
                    }else {
                        $eval_flg_individual = 'f';
                    }
                }
            }
  
             
            if($start_flg  == "t"){
                $accepted  ="1";
            }else if($start_flg_individual == "t"){
               $accepted  ="2";
            }else{
                $accepted  ="0";
            }
            
            if($end_flg  == "t"){
                $confirmed  ="1";
            }else if($end_flg_individual == "t"){
               $confirmed  ="2";
            }else{
                $confirmed  ="0";
            }
            
            if($eval_flg  == "t"){
                $evaluated  ="1";
            }else if($eval_flg_individual == "t"){
               $evaluated  ="2";
            }else{
                $evaluated  ="0";
            }
            
            
            //$accepted  = ($start_flg  == "t");
            //$confirmed = ($end_flg   == "t");
            //$evaluated = ($eval_flg   == "t");   
                
               
            switch ($arr_key){
                case "SM":
                    $use_flag  = ($list_data["sm_use_flg"]        == "t");
                    //$accepted  = ($list_data["sm_start_flg"]      == "t");
                    //$confirmed = ($list_data["sm_end_flg"]        == "t");
                    //$evaluated = ($list_data["sm_evaluation_flg"] == "t");
                    break;
                case "RM":
                    $use_flag  = ($list_data["rm_use_flg"]        == "t");
                    //$accepted  = ($list_data["rm_start_flg"]      == "t");
                    //$confirmed = ($list_data["rm_end_flg"]        == "t");
                    //$evaluated = ($list_data["rm_evaluation_flg"] == "t");
                    break;
                case "RA":
                    $use_flag  = ($list_data["ra_use_flg"]        == "t");
                    //$accepted  = ($list_data["ra_start_flg"]      == "t");
                    //$confirmed = ($list_data["ra_end_flg"]        == "t");
                    //$evaluated = ($list_data["ra_evaluation_flg"] == "t");
                    break;
                case "SD":
                    $use_flag  = ($list_data["sd_use_flg"]        == "t");
                    //$accepted  = ($list_data["sd_start_flg"]      == "t");
                    //$confirmed = ($list_data["sd_end_flg"]        == "t");
                    //$evaluated = ($list_data["sd_evaluation_flg"] == "t");
                    break;
                case "MD":
                    $use_flag  = ($list_data["md_use_flg"]        == "t");
                    //$accepted  = ($list_data["md_start_flg"]      == "t");
                    //$confirmed = ($list_data["md_end_flg"]        == "t");
                    //$evaluated = ($list_data["md_evaluation_flg"] == "t");
                    break;
                case "HD":
                    $use_flag  = ($list_data["hd_use_flg"]        == "t");
                    //$accepted  = ($list_data["hd_start_flg"]      == "t");
                    //$confirmed = ($list_data["hd_end_flg"]        == "t");
                    //$evaluated = ($list_data["hd_evaluation_flg"] == "t");
                    break;
                default:
            }
            $auth_info[$arr_key]['use'] = $use_flag;
            $auth_info[$arr_key]['accepted'] = $accepted;
            $auth_info[$arr_key]['confirmed'] = $confirmed;
            if ($design_mode == 2){
                if ($evaluation_use[$arr_key]){
                    $auth_info[$arr_key]['evaluated'] = $evaluated;
                }
            }
		}
    }
    $list[$i]["auth_info"] = $auth_info;
    
    //更新権限
    $report_torey_update_flg = getToreyUpdateAuth($con, $session, $fname, $report_id);
    if ($report_torey_update_flg) {
        // HTML出力用更新権限フラグ ： 更新権限が有る場合は、1を代入
        $report_torey_update_flg_value = '1';
    }
    else {
        // HTML出力用更新権限フラグ ： 更新権限が無い場合は、0を代入
        $report_torey_update_flg_value = '0';
    }
    $list[$i]['report_torey_update_flg_value'] = $report_torey_update_flg_value;
}

include("hiyari_rpt_display.php");

//==============================
//DBコネクション終了
//==============================
pg_close($con);

