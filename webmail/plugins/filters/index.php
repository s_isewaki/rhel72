<?php

   /**
    **  index.php -- Displays the main frameset
    **
    **  Copyright (c) 1999-2004 The SquirrelMail development team
    **  Licensed under the GNU GPL. For full terms see the file COPYING.
    **
    **  Redirects to the login page.
    **
    **  $Id: index.php,v 1.4.2.1 2004/02/24 15:57:30 kink Exp $
    **/

   header("Location:../../src/login.php\n\n");
   exit();

?>
