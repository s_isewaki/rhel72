<?php
/*******************************************************************************

    Author ......... Jimmy Conner
    Contact ........ jimmy@advcs.org
    Home Site ...... http://www.advcs.org/
    Program ........ Archive Mail
    Version ........ 1.2
    Purpose ........ Allows you to download your email in a compressed archive

*******************************************************************************/

$c++;

$seen = $msgs[$k]['FLAG_SEEN'];
if (!$seen)
    sqimap_toggle_flag($imapConnection, $id, '\\Seen', false, true);

$email = $msgs[$k]['FROM'];
if (strpos($email, '<') !== false)
    $email = substr($email, strpos($email, '<') + 1, strpos($email, '>') - strpos($email, '<') - 1);

$subject = $msgs[$k]['SUBJECT'];
$subject = decodeHeader($subject);
$subject = str_replace('&nbsp;', ' ', $subject);
$subject = archive_replace_str($subject, '');
$subject = mb_convert_encoding($subject, 'SJIS', 'EUC-JP');

$timestamp = $msgs[$k]['TIME_STAMP'];
$date = date('Y-m-d', $timestamp);

$name = archive_names($archivefilenames, $email, $date, $c, $subject);
$max = $maxarray[$archivetype];
if (strlen($name) > $max && $max > 0)
    $name = substr($name, 0, $max);

$suffix = '.eml';
$increment = checkincrement(strtolower($name . $suffix));

$tmp_file_path = "$archive_dir_path/$name$increment$suffix";
$fp = fopen($tmp_file_path, "w");
sqimap_run_command($imapConnection, "FETCH $id RFC822", true, $response, $readmessage, $uid_support, false, $fp, true);
fclose($fp);

$size += filesize($tmp_file_path);
