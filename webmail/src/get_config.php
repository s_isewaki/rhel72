<?
/*
 * get_config.php
 */
require_once(dirname(__FILE__) . '/about_postgress_webmail.php');

function get_config($fname) {

	// データベースに接続
	$con = connect2db4webmail($fname);
	if ($con == "0") {
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("DBに接続できませんでした。");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}

	// 項目全てを取得
	$sql = "select * from config";
	$cond = "";
	$sel = select_from_table4webmail($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("環境設定の情報を取得できませんでした。");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}
	pg_close($con);

	return pg_fetch_array($sel);
}
