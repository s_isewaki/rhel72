<?
/*
 * about_postgress_webmail.php
 */

// about_postgres.phpから流用しrequireのディレクトリを変更。
// about_postgres.phpが変更される場合は同様に変更する。
function connect2db4webmail($fname){
	$conf_dir = dirname(dirname(dirname(__FILE__))) . '/conf';

	@require($conf_dir . '/conf.inf');
	if (!defined('CONF_FILE')) {
		require($conf_dir . '/error.inf');
		write_error4webmail($fname, $ERRORLOG, $ERROR001);
		return 0;
	}

	if (!($con = @pg_connect("host=$IP4DBSERVER port=$PORT4DB dbname=$NAME4DB user=$USER4DB password=$PASSWD4DB"))) {
		require($conf_dir . '/error.inf');
		write_error4webmail($fname, $ERRORLOG, $ERROR101);
		return 0;
	}else{
		return $con;
	}
}

//*****tableへのselect*****
function select_from_table4webmail($con, $SQL, $cond, $fname) {
	$conf_dir = dirname(dirname(dirname(__FILE__))) . '/conf';

	@require($conf_dir . '/conf.inf');
	if (!defined('CONF_FILE')) {
		require($conf_dir . '/error.inf');
		write_error4webmail($fname, $ERRORLOG, $ERROR001);
		return 0;
	}

	if (!($result = @pg_exec($con, trim("$SQL $cond")))) {
		require($conf_dir . '/error.inf');
		write_error4webmail($fname, $ERRORLOG, $ERROR102);
		return 0;
	}else{
		return $result;
	}
}

//*****エラーメッセージの書き込み*****
function write_error4webmail($fname, $ERRORLOG, $ERROR) {
	$log_dir = dirname(dirname(dirname(__FILE__))) . '/log';

//	if ($ERRORLOG == 'on') {
		$errorDate = date('D M j G:i:s T Y');
		error_log($errorDate . '--> ' . $fname . ':'. $ERROR . "\n", 3, $log_dir . '/error.log');
		return 1;
//	}
//	return 0;
}
?>
