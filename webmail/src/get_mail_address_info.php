<?
/*
 * get_mail_address_info.php
 */
require_once("about_postgress_webmail.php");

function get_mail_address_info($username, $fname) {

	// データベースに接続
	$con = connect2db4webmail($fname);
	if ($con == "0") {
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("DBに接続できませんでした。");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}

	// サイトIDを取得
	$sql = "select site_id, use_cyrus from config";
	$cond = "";
	$sel = select_from_table4webmail($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("環境設定のサイトIDを取得できませんでした。");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}
	$site_id = pg_fetch_result($sel, 0, "site_id");
	$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");

	// サイトIDがある場合はusernameから除く
	if ($site_id != "") {
		$tmp_site_id = "_".$site_id;
		$tmp_username = str_replace($tmp_site_id , "", $username);
		$id_col_name = "emp_login_mail";
	} else if ($use_cyrus == "f") {
		$tmp_username = $username;
		$id_col_name = "emp_login_mail";
	} else {
		$tmp_username = $username;
		$id_col_name = "emp_login_id";
	}
	// ログインユーザの職員情報を取得
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id), get_emp_email(emp_id) from empmst";
	$cond = "where emp_id = (select login.emp_id from login, authmst where $id_col_name = '$tmp_username' and login.emp_id = authmst.emp_id and authmst.emp_del_flg = 'f')";
	$sel = select_from_table4webmail($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("職員情報を取得できませんでした。ID=$username");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
	$emp_mail_login_id = pg_fetch_result($sel, 0, 3);
	$emp_email = pg_fetch_result($sel, 0, 4);

	$info["emp_id"] = $emp_id;
	$info["emp_nm"] = $emp_nm;
	$info["emp_mail_login_id"] = $emp_mail_login_id;
	$info["emp_email"] = $emp_email;

	// 環境設定のWEBメール差出人表示順を取得
	$sql = "select webmail_from_order1, webmail_from_order2, webmail_from_order3 from config";
	$cond = "";
	$sel = select_from_table4webmail($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
		$message = urlencode("環境設定のWEBメール差出人表示順を取得できませんでした。");
		echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
		exit;
	}
	pg_close($con);

	$webmail_from_order1 = pg_fetch_result($sel, 0, "webmail_from_order1");
	$webmail_from_order2 = pg_fetch_result($sel, 0, "webmail_from_order2");
	$webmail_from_order3 = pg_fetch_result($sel, 0, "webmail_from_order3");

	$info["webmail_from_order"] = array();
	$info["webmail_from_order"][1] = $webmail_from_order1;
	$info["webmail_from_order"][2] = $webmail_from_order2;
	$info["webmail_from_order"][3] = $webmail_from_order3;

	return $info;
}
