<?php

/**
 * compose.php
 *
 * Copyright (c) 1999-2004 The SquirrelMail Project Team
 * Licensed under the GNU GPL. For full terms see the file COPYING.
 *
 * This code sends a mail.
 *
 * There are 4 modes of operation:
 *    - Start new mail
 *    - Add an attachment
 *    - Send mail
 *    - Save As Draft
 *
 * $Id: compose.php,v 1.319.2.35 2004/05/31 17:32:34 tokul Exp $
 */

/* Path for SquirrelMail required files. */
define('SM_PATH','../');
$cwd = getcwd();
chdir(dirname(dirname(dirname(__FILE__))));
require_once(dirname(dirname(dirname(__FILE__)))."/Cmx.php");
require_once(dirname(dirname(dirname(__FILE__)))."/aclg_set.php");
require_once(dirname(dirname(dirname(__FILE__)))."/about_comedix.php");

$fname = $PHP_SELF;

// データベース接続
$con = connect2db($fname);
if ($con == "0") {
	echo("<script type=\"text/javascript\" src=\"../../js/showpage.js\"></script>");
	$message = urlencode("DBに接続できませんでした。");
	echo("<script type=\"text/javascript\">mainWin=getMainWin(window);mainWin.location='../../error_message.php?message=$message';</script>");
	exit;
}
echo '<!DOCTYPE html>'; //ドロップダウンメニュー使用のためHTML5とする

// オプション設定値を取得
$conf = new Cmx_SystemConfig();
$use_secret_checkbox = ($conf->get('webmail.use_secret_checkbox') == "t");

// アクセスログ
$fname = 'webmail/src/'.$fname;
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_FILES);
pg_close($con);
$fname = $PHP_SELF;

chdir($cwd);
/* SquirrelMail required files. */
require_once(SM_PATH . 'include/validate.php');
require_once(SM_PATH . 'functions/global.php');
require_once(SM_PATH . 'functions/imap.php');
require_once(SM_PATH . 'functions/date.php');
require_once(SM_PATH . 'functions/mime.php');
require_once(SM_PATH . 'functions/plugin.php');
require_once(SM_PATH . 'functions/display_messages.php');
require_once(SM_PATH . 'class/deliver/Deliver.class.php');
require_once(SM_PATH . 'functions/addressbook.php');
require_once(SM_PATH . 'functions/forms.php');
require_once('./get_mail_address_info.php');
require_once('./get_config.php');

/* --------------------- Get globals ------------------------------------- */
/** COOKIE VARS */
sqgetGlobalVar('key',       $key,           SQ_COOKIE);

/** SESSION VARS */
sqgetGlobalVar('username',  $username,      SQ_SESSION);
sqgetGlobalVar('onetimepad',$onetimepad,    SQ_SESSION);
sqgetGlobalVar('base_uri',  $base_uri,      SQ_SESSION);
sqgetGlobalVar('delimiter', $delimiter,     SQ_SESSION);

sqgetGlobalVar('composesession',    $composesession,    SQ_SESSION);
sqgetGlobalVar('compose_messages',  $compose_messages,  SQ_SESSION);

/** SESSION/POST/GET VARS */
sqgetGlobalVar('smaction',$action);
sqgetGlobalVar('session',$session);
sqgetGlobalVar('mailbox',$mailbox);
sqgetGlobalVar('identity',$identity);
sqgetGlobalVar('send_to',$send_to);
sqgetGlobalVar('send_to_cc',$send_to_cc);
sqgetGlobalVar('send_to_bcc',$send_to_bcc);
sqgetGlobalVar('subject',$subject);
sqgetGlobalVar('body',$body);
sqgetGlobalVar('mailprio',$mailprio);
sqgetGlobalVar('request_mdn',$request_mdn);
sqgetGlobalVar('is_secret_message',$is_secret_message);
sqgetGlobalVar('request_dr',$request_dr);
sqgetGlobalVar('html_addr_search',$html_addr_search);
sqgetGlobalVar('mail_sent',$mail_sent);
sqgetGlobalVar('passed_id',$passed_id);
sqgetGlobalVar('passed_ent_id',$passed_ent_id);
sqgetGlobalVar('send',$send);

sqgetGlobalVar('attach',$attach);

sqgetGlobalVar('draft',$draft);
sqgetGlobalVar('draft_id',$draft_id);
sqgetGlobalVar('ent_num',$ent_num);
sqgetGlobalVar('saved_draft',$saved_draft);
sqgetGlobalVar('delete_draft',$delete_draft);
sqgetGlobalVar('startMessage',$startMessage);

/** POST VARS */
sqgetGlobalVar('sigappend',             $sigappend,             SQ_POST);
sqgetGlobalVar('from_htmladdr_search',  $from_htmladdr_search,  SQ_POST);
sqgetGlobalVar('addr_search_done',      $html_addr_search_done, SQ_POST);
sqgetGlobalVar('send_to_search',        $send_to_search,        SQ_POST);
sqgetGlobalVar('do_delete',             $do_delete,             SQ_POST);
sqgetGlobalVar('delete',                $delete,                SQ_POST);
sqgetGlobalVar('restoremessages',       $restoremessages,       SQ_POST);
if ( sqgetGlobalVar('return', $temp, SQ_POST) ) {
  $html_addr_search_done = 'Use Addresses';
}

/** GET VARS */
sqgetGlobalVar('attachedmessages', $attachedmessages, SQ_GET);

/* Location (For HTTP 1.1 Header("Location: ...") redirects) */
$location = get_location();

// 環境設定からstmp情報を取得
$config_info = get_config($fname);

/* --------------------- Specific Functions ------------------------------ */

function replyAllString($header) {
   global $include_self_reply_all, $username, $data_dir;
   $excl_ar = array();
   /**
    * 1) Remove the addresses we'll be sending the message 'to'
    */
   $url_replytoall_avoid_addrs = '';
   if (isset($header->replyto)) {
      $excl_ar = $header->getAddr_a('replyto');
   }
   /**
    * 2) Remove our identities from the CC list (they still can be in the
    * TO list) only if $include_self_reply_all is turned off
    */
   if (!$include_self_reply_all) {
       $email_address = strtolower(trim(getPref($data_dir, $username, 'email_address')));
       $excl_ar[$email_address] = '';
       $idents = getPref($data_dir, $username, 'identities');
       if ($idents != '' && $idents > 1) {
              $first_id = false;
          for ($i = 1; $i < $idents; $i ++) {
             $cur_email_address = getPref($data_dir, $username,
                                         'email_address' . $i);
             $cur_email_address = strtolower(trim($cur_email_address));
             $excl_ar[$cur_email_address] = '';
         }
       }
   }

   /**
    * 3) get the addresses.
    */
   $url_replytoall_ar = $header->getAddr_a(array('to','cc'), $excl_ar);

   /**
    * 4) generate the string.
    */
   $url_replytoallcc = '';
   foreach( $url_replytoall_ar as $email => $personal) {
      if ($personal) {
         // if personal name contains address separator then surround
         // the personal name with double quotes.
         if (strpos($personal,',') !== false) {
             $personal = '"'.$personal.'"';
         }
         $url_replytoallcc .= ", $personal <$email>";
      } else {
         $url_replytoallcc .= ', '. $email;
      }
   }
   $url_replytoallcc = substr($url_replytoallcc,2);

   return $url_replytoallcc;
}

function getReplyCitation($orig_from, $orig_date) {
    global $reply_citation_style, $reply_citation_start, $reply_citation_end;
    $orig_from = decodeHeader($orig_from->getAddress(false),false,false);
//    $from = decodeHeader($orig_header->getAddr_s('from',"\n$indent"),false,false);
    /* First, return an empty string when no citation style selected. */
    if (($reply_citation_style == '') || ($reply_citation_style == 'none')) {
        return '';
    }

    /* Make sure our final value isn't an empty string. */
    if ($orig_from == '') {
        return '';
    }

    /* Otherwise, try to select the desired citation style. */
    switch ($reply_citation_style) {
    case 'author_said':
        $start = '';
        $end   = ' ' . _("said") . ':';
        break;
    case 'quote_who':
        $start = '<' . _("quote") . ' ' . _("who") . '="';
        $end   = '">';
        break;
    case 'date_time_author':
        $start = 'On ' . getLongDateString($orig_date) . ', ';
        $end = ' ' . _("said") . ':';
        break;
    case 'user-defined':
        $start = $reply_citation_start .
         ($reply_citation_start == '' ? '' : ' ');
        $end   = $reply_citation_end;
        break;
    default:
        return '';
    }

    /* Build and return the citation string. */
    return ($start . $orig_from . $end . "\n");
}

function getforwardHeader($orig_header) {
    global $editor_size;

   $display = array( _("Subject") => strlen(_("Subject")),
                     _("From")    => strlen(_("From")),
                     _("Date")    => strlen(_("Date")),
                     _("To")      => strlen(_("To")),
                     _("Cc")      => strlen(_("Cc")) );
   $maxsize = max($display);
   $indent = str_pad('',$maxsize+2);
   foreach($display as $key => $val) {
      $display[$key] = $key .': '. str_pad('', $maxsize - $val);
   }
   $from = decodeHeader($orig_header->getAddr_s('from',"\n$indent"),false,false);
   $from = str_replace('&nbsp;',' ',$from);
   $to = decodeHeader($orig_header->getAddr_s('to',"\n$indent"),false,false);
   $to = str_replace('&nbsp;',' ',$to);
   $subject = decodeHeader($orig_header->subject,false,false);
   $subject = str_replace('&nbsp;',' ',$subject);
   $bodyTop =  str_pad(' '._("Original Message").' ',$editor_size -2,'-',STR_PAD_BOTH) .
               "\n". $display[_("Subject")] . $subject . "\n" .
               $display[_("From")] . $from . "\n" .
               $display[_("Date")] . getLongDateString( $orig_header->date ). "\n" .
               $display[_("To")] . $to . "\n";
   if ($orig_header->cc != array() && $orig_header->cc !='') {
      $cc = decodeHeader($orig_header->getAddr_s('cc',"\n$indent"),false,false);
      $cc = str_replace('&nbsp;',' ',$cc);
     $bodyTop .= $display[_("Cc")] .$cc . "\n";
  }
  $bodyTop .= str_pad('', $editor_size -2 , '-') .
              "\n\n";
  return $bodyTop;
}
/* ----------------------------------------------------------------------- */

/*
 * If the session is expired during a post this restores the compose session
 * vars.
 */
if (sqsession_is_registered('session_expired_post')) {
    sqgetGlobalVar('session_expired_post', $session_expired_post, SQ_SESSION);
    /*
     * extra check for username so we don't display previous post data from
     * another user during this session.
     */
    if ($session_expired_post['username'] != $username) {
        unset($session_expired_post);
        sqsession_unregister('session_expired_post');
        session_write_close();
    } else {
        foreach ($session_expired_post as $postvar => $val) {
            if (isset($val)) {
                $$postvar = $val;
            } else {
                $$postvar = '';
            }
        }
        $compose_messages = unserialize(urldecode($restoremessages));
        sqsession_register($compose_messages,'compose_messages');
        sqsession_register($composesession,'composesession');
        if (isset($send)) {
            unset($send);
        }
        $session_expired = true;
    }
    unset($session_expired_post);
    sqsession_unregister('session_expired_post');
    session_write_close();
    if (!isset($mailbox)) {
        $mailbox = '';
    }
    if ($compose_new_win == '1') {
        compose_Header($color, $mailbox);
    } else {
        displayPageHeader($color, $mailbox);
    }
    showInputForm($session, false);
    exit();
}
if (!isset($composesession)) {
    $composesession = 0;
    sqsession_register(0,'composesession');
}

if (!isset($session) || (isset($newmessage) && $newmessage)) {
    sqsession_unregister('composesession');
    $session = "$composesession" +1;
    $composesession = $session;
    sqsession_register($composesession,'composesession');
}
if (!isset($compose_messages)) {
  $compose_messages = array();
}
if (!isset($compose_messages[$session]) || ($compose_messages[$session] == NULL)) {
/* if (!array_key_exists($session, $compose_messages)) {  /* We can only do this in PHP >= 4.1 */
  $composeMessage = new Message();
  $rfc822_header = new Rfc822Header();
  $composeMessage->rfc822_header = $rfc822_header;
  $composeMessage->reply_rfc822_header = '';
  $compose_messages[$session] = $composeMessage;
  sqsession_register($compose_messages,'compose_messages');
} else {
  $composeMessage=$compose_messages[$session];
}

if (!isset($mailbox) || $mailbox == '' || ($mailbox == 'None')) {
    $mailbox = 'INBOX';
}

if ($draft) {
    /*
     * Set $default_charset to correspond with the user's selection
     * of language interface.
     */
    set_my_charset();
    
    if ($_FILES['attachfile']['error'] == 2) {
	$AttachFailure = saveAttachedFiles($session);
    }

    if (isset($_FILES['attachfile']) &&
        $_FILES['attachfile']['tmp_name'] &&
        $_FILES['attachfile']['tmp_name'] != 'none') {
        $AttachFailure = saveAttachedFiles($session);
    }
    
    
    $composeMessage=$compose_messages[$session];
    if (! deliverMessage($composeMessage, true)) {
        showInputForm($session);
        exit();
    } else {
        unset($compose_messages[$session]);
        $draft_message = _("Draft Email Saved");
        /* If this is a resumed draft, then delete the original */
        if(isset($delete_draft)) {
            Header("Location: $location/delete_message.php?mailbox=" . urlencode($draft_folder) .
                   "&message=$delete_draft&sort=$sort&startMessage=1&saved_draft=yes");
            exit();
        }
        else {
            if ($compose_new_win == '1') {
                Header("Location: $location/compose.php?saved_draft=yes&session=$composesession");
                exit();
            }
            else {
                Header("Location: $location/right_main.php?mailbox=" . urlencode($draft_folder) . "&sort=$sort".
                       "&startMessage=1&note=".urlencode($draft_message));
                exit();
            }
        }
    }
}

if ($send) {
    if ($_FILES['attachfile']['error'] == 2) {
		$AttachFailure = saveAttachedFiles($session);
	}

	if (isset($_FILES['attachfile']) &&
        $_FILES['attachfile']['tmp_name'] &&
        $_FILES['attachfile']['tmp_name'] != 'none') {
        $AttachFailure = saveAttachedFiles($session);
    }

    // 院内メール、Emailアドレスの確認
    $in_mail_cnt = 0;
    $e_mail_cnt = 0;
    if (!isset($cmx_inner_domain)) $cmx_inner_domain = "";
    $arr_send_to = parseAddress($send_to);
    foreach ($arr_send_to as $i => $tmp_send_to) {
        if (isInsideAddress($tmp_send_to[0], $domain, $cmx_inner_domain)) {
            $in_mail_cnt++;
            $arr_send_to[$i]["type"] = "in";
        } else {
            $e_mail_cnt++;
            $arr_send_to[$i]["type"] = "e";
        }
    }

    if ($send_to_cc != "") {
        $arr_send_to_cc = parseAddress($send_to_cc);
        foreach ($arr_send_to_cc as $i => $tmp_send_to_cc) {
            if (isInsideAddress($tmp_send_to_cc[0], $domain, $cmx_inner_domain)) {
                $in_mail_cnt++;
                $arr_send_to_cc[$i]["type"] = "in";
            } else {
                $e_mail_cnt++;
                $arr_send_to_cc[$i]["type"] = "e";
            }
        }
    }

    if ($send_to_bcc != "") {
        $arr_send_to_bcc = parseAddress($send_to_bcc);
        foreach ($arr_send_to_bcc as $i => $tmp_send_to_bcc) {
            if (isInsideAddress($tmp_send_to_bcc[0], $domain, $cmx_inner_domain)) {
                $in_mail_cnt++;
                $arr_send_to_bcc[$i]["type"] = "in";
            } else {
                $e_mail_cnt++;
                $arr_send_to_bcc[$i]["type"] = "e";
            }
        }
    }

    if (checkInput(false) && !isset($AttachFailure)) {
                if ($mailbox == "All Folders") {
                        /* We entered compose via the search results page */
                        $mailbox="INBOX"; /* Send 'em to INBOX, that's safe enough */
                }
        $urlMailbox = urlencode (trim($mailbox));
        if (! isset($passed_id)) {
            $passed_id = 0;
        }
        /*
         * Set $default_charset to correspond with the user's selection
         * of language interface.
         */
        set_my_charset();
        /*
         * This is to change all newlines to \n
         * We'll change them to \r\n later (in the sendMessage function)
         */
        $body = str_replace("\r\n", "\n", $body);
        $body = str_replace("\r", "\n", $body);

        /*
         * Rewrap $body so that no line is bigger than $editor_size
         * This should only really kick in the sqWordWrap function
         * if the browser doesn't support "VIRTUAL" as the wrap type.
         */
        $body = explode("\n", $body);
        $newBody = '';
        foreach ($body as $line) {
            if( $line <> '-- ' ) {
               $line = rtrim($line);
            }
            if (strlen($line) <= $editor_size + 1) {
                $newBody .= $line . "\n";
            } else {
                sqWordWrap($line, $editor_size);
                $newBody .= $line . "\n";

            }

        }
        $body = $newBody;

        $composeMessage=$compose_messages[$session];
        $Result = deliverMessage($composeMessage);
        if (! $Result) {
            showInputForm($session);
            exit();
        }
        unset($compose_messages[$session]);
        if ( isset($delete_draft)) {
            Header("Location: $location/delete_message.php?mailbox=" . urlencode( $draft_folder ).
                   "&message=$delete_draft&sort=$sort&startMessage=1&mail_sent=yes");
            exit();
        }
        if ($compose_new_win == '1') {

            Header("Location: $location/compose.php?mail_sent=yes");
        }
        else {
            Header("Location: $location/right_main.php?mailbox=$urlMailbox&sort=$sort".
                   "&startMessage=$startMessage");
        }
    } else {
        if ($compose_new_win == '1') {
            compose_Header($color, $mailbox);
        }
        else {
            displayPageHeader($color, $mailbox);
        }
        if (isset($AttachFailure)) {
             plain_error_message(_("Could not move/copy file. File not attached"),
                                 $color);
        }
        checkInput(true);
        showInputForm($session);
        /* sqimap_logout($imapConnection); */
    }
} elseif (isset($html_addr_search_done)) {
        if ($compose_new_win == '1') {
            compose_Header($color, $mailbox);
        }
        else {
            displayPageHeader($color, $mailbox);
        }

    if (isset($send_to_search) && is_array($send_to_search)) {
        foreach ($send_to_search as $k => $v) {
            if (substr($k, 0, 1) == 'T') {
                if ($send_to) {
                    $send_to .= ', ';
                }
                $send_to .= $v;
            }
            elseif (substr($k, 0, 1) == 'C') {
                if ($send_to_cc) {
                    $send_to_cc .= ', ';
                }
                $send_to_cc .= $v;
            }
            elseif (substr($k, 0, 1) == 'B') {
                if ($send_to_bcc) {
                    $send_to_bcc .= ', ';
                }
                $send_to_bcc .= $v;
            }
        }
    }
    showInputForm($session);
} elseif (isset($html_addr_search)) {
    if (isset($_FILES['attachfile']) &&
        $_FILES['attachfile']['tmp_name'] &&
        $_FILES['attachfile']['tmp_name'] != 'none') {
        if(saveAttachedFiles($session)) {
            plain_error_message(_("Could not move/copy file. File not attached"), $color);
        }
    }
    /*
     * I am using an include so as to elminiate an extra unnecessary
     * click.  If you can think of a better way, please implement it.
     */
    include_once('./addrbook_search_html.php');
} elseif (isset($attach)) {
    if (saveAttachedFiles($session)) {
        plain_error_message(_("Could not move/copy file. File not attached"), $color);
    }
        if ($compose_new_win == '1') {
            compose_Header($color, $mailbox);
        }
        else {
            displayPageHeader($color, $mailbox);
        }
    showInputForm($session);
}
elseif (isset($sigappend)) {
    $idents = getPref($data_dir, $username, 'identities', 0);
    if ($idents > 1) {
       if ($identity == 'default' || substr($identity, 0, 2) == 'em') {
          $no = 'g';
       } else {
          $no = $identity;
       }
       $signature = getSig($data_dir, $username, $no);
    }
    $body .= "\n\n".($prefix_sig==true? "-- \n":'').$signature;
    if ($compose_new_win == '1') {
         compose_Header($color, $mailbox);
    } else {
        displayPageHeader($color, $mailbox);
    }
    showInputForm($session);
} elseif (isset($do_delete)) {
        if ($compose_new_win == '1') {
            compose_Header($color, $mailbox);
        }
        else {
            displayPageHeader($color, $mailbox);
        }

    if (isset($delete) && is_array($delete)) {
        $composeMessage = $compose_messages[$session];
        foreach($delete as $index) {
            $attached_file = $composeMessage->entities[$index]->att_local_name;
            unlink ($attached_file);
            unset ($composeMessage->entities[$index]);
        }
        $new_entities = array();
        foreach ($composeMessage->entities as $entity) {
            $new_entities[] = $entity;
        }
        $composeMessage->entities = $new_entities;
        $compose_messages[$session] = $composeMessage;
        sqsession_register($compose_messages, 'compose_messages');
    }
    showInputForm($session);
} else {
    /*
     * This handles the default case as well as the error case
     * (they had the same code) --> if (isset($smtpErrors))
     */

    if ($compose_new_win == '1') {
       compose_Header($color, $mailbox);
    } else {
       displayPageHeader($color, $mailbox);
    }

    $upload_max_filesize = trim(ini_get('upload_max_filesize'));
    preg_match("/([0-9]+)([a-zA-Z])/", $upload_max_filesize, $matches);
    $unit = strtolower($matches[2]);
    switch($unit) {
        case 'g':
            $matches[1] *= 1024;
        case 'm':
            $matches[1] *= 1024;
        case 'k':
            $matches[1] *= 1024;
    }

    $upload_max_filesize_bytes = $matches[1];
    if ($_SERVER["CONTENT_LENGTH"] > $upload_max_filesize_bytes) {
        plain_error_message(_("Could not move/copy file. File not attached"), $color);
    }

    $newmail = true;

    if (!isset($passed_ent_id)) {
        $passed_ent_id = '';
    }
    if (!isset($passed_id)) {
        $passed_id = '';
    }
    if (!isset($mailbox)) {
        $mailbox = '';
    }
    if (!isset($action)) {
        $action = '';
    }

    $values = newMail($mailbox,$passed_id,$passed_ent_id, $action, $session);

    /* in case the origin is not read_body.php */
    if (isset($send_to)) {
       $values['send_to'] = $send_to;
    }
    if (isset($send_to_cc)) {
       $values['send_to_cc'] = $send_to_cc;
    }
    if (isset($send_to_bcc)) {
       $values['send_to_bcc'] = $send_to_bcc;
    }
    if (isset($subject)) {
       $values['subject'] = $subject;
    }
    showInputForm($session, $values);
}

exit();

/**************** Only function definitions go below *************/


/* This function is used when not sending or adding attachments */
function newMail ($mailbox='', $passed_id='', $passed_ent_id='', $action='', $session='') {
    global $editor_size, $default_use_priority, $body,
           $use_signature, $composesession, $data_dir, $username,
           $username, $key, $imapServerAddress, $imapPort, $compose_messages,
           $composeMessage;
    global $languages, $squirrelmail_language;

    $send_to = $send_to_cc = $send_to_bcc = $subject = $identity = '';
    $mailprio = 3;

    if ($passed_id) {
        $imapConnection = sqimap_login($username, $key, $imapServerAddress,
        $imapPort, 0);

        sqimap_mailbox_select($imapConnection, $mailbox);
        $message = sqimap_get_message($imapConnection, $passed_id, $mailbox);

        $body = '';
        if ($passed_ent_id) {
            /* redefine the messsage in case of message/rfc822 */
            $message = $message->getEntity($passed_ent_id);
            /* message is an entity which contains the envelope and type0=message
            * and type1=rfc822. The actual entities are childs from
            * $message->entities[0]. That's where the encoding and is located
            */

            $entities = $message->entities[0]->findDisplayEntity
            (array(), $alt_order = array('text/plain'));
            if (!count($entities)) {
                $entities = $message->entities[0]->findDisplayEntity
                (array(), $alt_order = array('text/plain','html/plain'));
            }
            $orig_header = $message->rfc822_header; /* here is the envelope located */
            /* redefine the message for picking up the attachments */
            $message = $message->entities[0];

        } else {
            $entities = $message->findDisplayEntity (array(), $alt_order = array('text/plain'));
            if (!count($entities)) {
                $entities = $message->findDisplayEntity (array(), $alt_order = array('text/plain','html/plain'));
            }
            $orig_header = $message->rfc822_header;
        }

        $encoding = $message->header->encoding;
        $type0 = $message->type0;
        $type1 = $message->type1;
        foreach ($entities as $ent) {
            $unencoded_bodypart = mime_fetch_body($imapConnection, $passed_id, $ent);
            $body_part_entity = $message->getEntity($ent);
            $bodypart = decodeBody($unencoded_bodypart,
            $body_part_entity->header->encoding);
            if (isset($languages[$squirrelmail_language]['XTRA_CODE']) &&
                function_exists($languages[$squirrelmail_language]['XTRA_CODE'])) {
                if (mb_detect_encoding($bodypart) != 'ASCII') {
                    $bodypart = $languages[$squirrelmail_language]['XTRA_CODE']('decode', $bodypart);
                }
            }
            if ($type1 == 'html') {
                $bodypart = str_replace("\n", ' ', $bodypart);
                $bodypart = preg_replace(array('/<p>/i','/<br\s*(\/)*>/i'), "\n", $bodypart);
                $bodypart = str_replace(array('&nbsp;','&gt;','&lt;'),array(' ','>','<'),$bodypart);
                $bodypart = strip_tags($bodypart);

            }
            $body .= $bodypart;
        }
        if ($default_use_priority) {
            $mailprio = substr($orig_header->priority,0,1);
            if (!$mailprio) {
                $mailprio = 3;
            }
        } else {
            $mailprio = '';
        }
        //ClearAttachments($session);

        $identity = '';
        $idents = getPref($data_dir, $username, 'identities');
        $from_o = $orig_header->from;

        if (is_array($from_o)) {
            if (isset($from_o[0])) {
                $from_o = $from_o[0];
            }
        }
        if (is_object($from_o)) {
            $orig_from = $from_o->getAddress();
        } else {
            $orig_from = '';
        }
        $identities = array();
        if (!empty($idents) && $idents > 1) {
            $identities[]  = '"'. getPref($data_dir, $username, 'full_name')
              . '" <' .  getPref($data_dir, $username, 'email_address') . '>';
            for ($i = 1; $i < $idents; $i++) {
                $enc_from_name = '"'.
                    getPref($data_dir, $username, 'full_name' . $i) .
                        '" <' .
                    getPref($data_dir, $username, 'email_address' . $i) . '>';
                if ($enc_from_name == $orig_from && $i) {
                    $identity = $i;
                    break;
                }
                $identities[] = $enc_from_name;
            }

            $identity_match = $orig_header->findAddress($identities);
            if ($identity_match) {
                $identity = $identity_match;
            }
        }

        switch ($action) {
        case ('draft'):
            $use_signature = FALSE;
            $composeMessage->rfc822_header = $orig_header;
            $send_to = decodeHeader($orig_header->getAddr_s('to'),false,false);
            $send_to_cc = decodeHeader($orig_header->getAddr_s('cc'),false,false);
            $send_to_bcc = decodeHeader($orig_header->getAddr_s('bcc'),false,false);
            $subject = decodeHeader($orig_header->subject,false,false);
            /* remember the references and in-reply-to headers in case of an reply */
            $composeMessage->rfc822_header->more_headers['References'] = $orig_header->references;
            $composeMessage->rfc822_header->more_headers['In-Reply-To'] = $orig_header->in_reply_to;
            $body_ary = explode("\n", $body);
            $cnt = count($body_ary) ;
            $body = '';
            for ($i=0; $i < $cnt; $i++) {
                if (!ereg("^[>\\s]*$", $body_ary[$i])  || !$body_ary[$i]) {
                    sqWordWrap($body_ary[$i], $editor_size );
                    $body .= $body_ary[$i] . "\n";
                }
                unset($body_ary[$i]);
            }
            sqUnWordWrap($body);
            $composeMessage = getAttachments($message, $composeMessage, $passed_id, $entities, $imapConnection);
            break;
        case ('edit_as_new'):
            $send_to = decodeHeader($orig_header->getAddr_s('to'),false,false);
            $send_to_cc = decodeHeader($orig_header->getAddr_s('cc'),false,false);
            $send_to_bcc = decodeHeader($orig_header->getAddr_s('bcc'),false,false);
            $subject = decodeHeader($orig_header->subject,false,false);
            $mailprio = $orig_header->priority;
            $orig_from = '';
            $composeMessage = getAttachments($message, $composeMessage, $passed_id, $entities, $imapConnection);
            sqUnWordWrap($body);
            break;
        case ('forward'):
            $send_to = '';
            $subject = decodeHeader($orig_header->subject,false,false);
            if ((substr(strtolower($subject), 0, 4) != 'fwd:') &&
                (substr(strtolower($subject), 0, 5) != '[fwd:') &&
                (substr(strtolower($subject), 0, 6) != '[ fwd:')) {
                $subject = '[Fwd: ' . $subject . ']';
            }
            $body = getforwardHeader($orig_header) . $body;
            sqUnWordWrap($body);
            $composeMessage = getAttachments($message, $composeMessage, $passed_id, $entities, $imapConnection);
            $body = "\n" . $body;
            break;
        case ('forward_as_attachment'):
            $composeMessage = getMessage_RFC822_Attachment($message, $composeMessage, $passed_id, $passed_ent_id, $imapConnection);
            $body = '';
            break;
        case ('reply_all'):
            $send_to_cc = replyAllString($orig_header);
            $send_to_cc = decodeHeader($send_to_cc,false,false);
        case ('reply'):
            $send_to = $orig_header->reply_to;
            if (is_array($send_to) && count($send_to)) {
                $send_to = $orig_header->getAddr_s('reply_to');
            } else if (is_object($send_to)) { /* unnessecarry, just for falesafe purpose */
                $send_to = $orig_header->getAddr_s('reply_to');
            } else {
                $send_to = $orig_header->getAddr_s('from');
            }
            $send_to = decodeHeader($send_to,false,false);
            $subject = decodeHeader($orig_header->subject,false,false);
            $subject = str_replace('"', "'", $subject);
            $subject = trim($subject);
            if (substr(strtolower($subject), 0, 3) != 're:') {
                $subject = 'Re: ' . $subject;
            }
            /* this corrects some wrapping/quoting problems on replies */
            $rewrap_body = explode("\n", $body);
            $from =  (is_array($orig_header->from)) ? $orig_header->from[0] : $orig_header->from;
            sqUnWordWrap($body);
            $body = '';
            $cnt = count($rewrap_body);
            for ($i=0;$i<$cnt;$i++) {
              sqWordWrap($rewrap_body[$i], ($editor_size));
                if (preg_match("/^(>+)/", $rewrap_body[$i], $matches)) {
                    $gt = $matches[1];
                    $body .= '>' . str_replace("\n", "\n>$gt ", rtrim($rewrap_body[$i])) ."\n";
                } else {
                    $body .= '> ' . str_replace("\n", "\n> ", rtrim($rewrap_body[$i])) . "\n";
                }
                unset($rewrap_body[$i]);
            }
            $body = getReplyCitation($from , $orig_header->date) . $body;
            $composeMessage->reply_rfc822_header = $orig_header;

            break;
        default:
            break;
        }
        $compose_messages[$session] = $composeMessage;
        sqsession_register($compose_messages, 'compose_messages');
        session_write_close();
        sqimap_logout($imapConnection);
    }
    $ret = array( 'send_to' => $send_to,
                  'send_to_cc' => $send_to_cc,
                  'send_to_bcc' => $send_to_bcc,
                  'subject' => $subject,
                  'mailprio' => $mailprio,
                  'body' => $body,
                  'identity' => $identity );

    return ($ret);
} /* function newMail() */

function getAttachments($message, &$composeMessage, $passed_id, $entities, $imapConnection) {
    global $attachment_dir, $username, $data_dir, $squirrelmail_language;
    $hashed_attachment_dir = getHashedDir($username, $attachment_dir);
    if (!count($message->entities) ||
       ($message->type0 == 'message' && $message->type1 == 'rfc822')) {
        if ( !in_array($message->entity_id, $entities) && $message->entity_id) {
           switch ($message->type0) {
           case 'message':
                if ($message->type1 == 'rfc822') {
                    $filename = $message->rfc822_header->subject;
                    if ($filename == "") {
                        $filename = "untitled-".$message->entity_id;
                    }
                    $filename .= '.msg';
                 } else {
                   $filename = $message->getFilename();
                 }
             break;
           default:
             if (!$message->mime_header) { /* temporary hack */
                 $message->mime_header = $message->header;
             }
             $filename = $message->getFilename();
             break;
           }
           $filename = decodeHeader($filename, false, false, false);
           if (isset($languages[$squirrelmail_language]['XTRA_CODE']) &&
               function_exists($languages[$squirrelmail_language]['XTRA_CODE'])) {
                $filename =  $languages[$squirrelmail_language]['XTRA_CODE']('encode', $filename);
           }
           $localfilename = GenerateRandomString(32, '', 7);
           $full_localfilename = "$hashed_attachment_dir/$localfilename";
           while (file_exists($full_localfilename)) {
               $localfilename = GenerateRandomString(32, '', 7);
               $full_localfilename = "$hashed_attachment_dir/$localfilename";
           }
           $message->att_local_name = $full_localfilename;

	   $composeMessage->initAttachment($message->type0.'/'.$message->type1,$filename,
             $full_localfilename);

           /* Write Attachment to file */
           $fp = fopen ("$hashed_attachment_dir/$localfilename", 'wb');
           fputs($fp, decodeBody(mime_fetch_body($imapConnection,
              $passed_id, $message->entity_id),
              $message->header->encoding));
           fclose ($fp);
        }
    } else {
        for ($i=0, $entCount=count($message->entities); $i<$entCount;$i++) {
            $composeMessage=getAttachments($message->entities[$i], $composeMessage, $passed_id, $entities, $imapConnection);
        }
    }
    return $composeMessage;
}

function getMessage_RFC822_Attachment($message, $composeMessage, $passed_id,
                                      $passed_ent_id='', $imapConnection) {
    global $attachments, $attachment_dir, $username, $data_dir, $uid_support;
    $hashed_attachment_dir = getHashedDir($username, $attachment_dir);
    if (!$passed_ent_id) {
        $body_a = sqimap_run_command($imapConnection,
                                    'FETCH '.$passed_id.' RFC822',
                                    TRUE, $response, $readmessage,
                                    $uid_support);
    } else {
        $body_a = sqimap_run_command($imapConnection,
                                     'FETCH '.$passed_id.' BODY['.$passed_ent_id.']',
                                     TRUE, $response, $readmessage, $uid_support);
        $message = $message->parent;
    }
    if ($response == 'OK') {
        $subject = encodeHeader($message->rfc822_header->subject);
        array_shift($body_a);
        array_pop($body_a);
        $body = implode('', $body_a) . "\r\n";

        $localfilename = GenerateRandomString(32, 'FILE', 7);
        $full_localfilename = "$hashed_attachment_dir/$localfilename";

        $fp = fopen($full_localfilename, 'w');
        fwrite ($fp, $body);
        fclose($fp);
        $composeMessage->initAttachment('message/rfc822',$subject.'.msg',
                         $full_localfilename);
    }
    return $composeMessage;
}

function showInputForm ($session, $values=false) {
    global $send_to, $send_to_cc, $body, $startMessage,
           $passed_body, $color, $use_signature, $signature, $prefix_sig,
           $editor_size, $editor_height, $attachments, $subject, $newmail,
           $use_javascript_addr_book, $send_to_bcc, $passed_id, $mailbox,
           $from_htmladdr_search, $location_of_buttons, $attachment_dir,
           $username, $data_dir, $identity, $draft_id, $delete_draft,
           $mailprio, $default_use_mdn, $mdn_user_support, $compose_new_win,
           $saved_draft, $mail_sent, $sig_first, $edit_as_new, $action,
           $username, $compose_messages, $composesession, $default_charset,
           $fname;

    $composeMessage = $compose_messages[$session];
    if ($values) {
       $send_to = $values['send_to'];
       $send_to_cc = $values['send_to_cc'];
       $send_to_bcc = $values['send_to_bcc'];
       $subject = $values['subject'];
       $mailprio = $values['mailprio'];
       $body = $values['body'];
//       $identity = (int) $values['identity'];
       $identity = $values['identity'];
    } else {
       $send_to = decodeHeader($send_to, true, false);
       $send_to_cc = decodeHeader($send_to_cc, true, false);
       $send_to_bcc = decodeHeader($send_to_bcc, true, false);
    }
?>
<style type="text/css">
table{
  border-collapse:separate;
  border-radius:5px;
  -webkit-border-radius:5px;
  -moz-border-radius:5px;
  -ms-border-radius:5px;
}
#showoverlay{margin:25px;}
#modalbox {
  display:none;
  position:absolute;
  z-index:1000;
  width:400px;
  height:200px;
  padding:20px;
  background:#fff;
  border:5px solid #eee;
  border-radius:5px;
  -webkit-border-radius:5px;
  -moz-border-radius:5px;
  -ms-border-radius:5px;
}
#overlay {
  opacity:0;
  filter: alpha(opacity=0);
  position:absolute;
  top:0;
  left:0;
  z-index:900;
  width:100%;
  height:100%;
  background:#000;
}
#close {
  line-height:1;
  font-size:14px;
  position: absolute;
  top:2px;
  right:2px;
  color:red;
  text-decoration:none;
}
#dmenu1 {
  position: relative;
  z-index:300;
}
#dmenu2 {
  position: relative;
  z-index:200;
}
#dmenu3 {
  position: relative;
  z-index:100;
}
<?
$wk_bgcolor = ($compose_new_win == '1') ? $color[0] : $color[4]; //$color[0]:grey $color[4]white
$wk_hbgcolor = ($compose_new_win == '1') ? $color[4] : $color[0];
?>
#dropmenu {
  list-style-type: none;
  width: 70px;
  height: 20px;
  margin: 2px auto 2px;
  padding: 0;
  background: <? echo($wk_bgcolor); ?>;
  border: 0px solid #000000;
}
#dropmenu li {
  position: relative;
  width: 100%;
  float: left;
  margin: 0;
  padding: 0;
  text-align: right;
}
#dropmenu li a {
  display: block;
  margin: 0;
  padding: 4px 0 2px;
  color: #000000;
  font-size: 14px;
  font-weight: bold;
  line-height: 1;
  text-decoration: none;
}
#dropmenu li ul {
  list-style: none;
  position: absolute;
  top: 100%;
  left: 0;
  margin: 0;
  padding: 0;
}
#dropmenu li ul li{
  overflow: hidden;
  width: 100%;
  height: 0;
  color: <? echo($wk_bgcolor); ?>;
  -moz-transition: .2s;
  -webkit-transition: .2s;
  -o-transition: .2s;
  -ms-transition: .2s;
  transition: .2s;
}
#dropmenu li ul li a{
  padding: 3px 3px;
  background: <? echo($wk_bgcolor); ?>;
  text-align: left;
  font-size: 12px;
  font-weight: normal;
}
#dropmenu li:hover > a{
  background: <? echo($wk_hbgcolor); ?>;
  color: #000000;
}
#dropmenu li:hover ul li{
  overflow: visible;
  width: 160px;
  height: 18px;
  border-left: 1px solid #000000;
  border-right: 1px solid #000000;
  border-bottom: 1px solid #000000;
}
#dropmenu li:hover ul li:first-child{
  border-top: 1px solid #000000;
}
#dropmenu li:hover ul li:last-child{
  border-bottom: 1px solid #000000;
}
</style>

<?
    if ($use_javascript_addr_book) {
        echo "\n". '<SCRIPT LANGUAGE=JavaScript>'."\n<!--\n" .
             'function open_abook() { ' . "\n" .
             '  dx = screen.availWidth - 10;' . "\n" .
             '  dy = screen.top;' . "\n" .
             '  base = 0;' . "\n" .
             '  wx = 670;' . "\n" .
             '  wy = 600;' . "\n" .
             '  var nwin = window.open("addrbook_popup.php","abookpopup",' .
             '"left="+(dx-wx)+",top="+base+",width="+wx+",height="+wy+",resizable=yes,scrollbars=yes");' . "\n" .
             '  if((!nwin.opener) && (document.windows != null))' . "\n" .
             '    nwin.opener = document.windows;' . "\n" .
             "}\n" .
             "// -->\n</SCRIPT>\n\n";
    }

    echo "\n" . '<form name="compose" action="compose.php" method="post" ' .
         'enctype="multipart/form-data"';
    do_hook('compose_form');

    echo " onsubmit=\"skipUnloadConfirm = true;\">\n";

    echo addHidden('startMessage', $startMessage);

    if ($action == 'draft') {
        echo addHidden('delete_draft', $passed_id);
    }
    if (isset($delete_draft)) {
        echo addHidden('delete_draft', $delete_draft);
    }
    if (isset($session)) {
        echo addHidden('session', $session);
    }

    if (isset($passed_id)) {
        echo addHidden('passed_id', $passed_id);
    }

    if ($saved_draft == 'yes') {
        echo '<BR><CENTER><B>'. _("Draft Saved").'</CENTER></B>';
    }
    if ($mail_sent == 'yes') {
        echo '<BR><CENTER><B>'. _("Your Message has been sent").'</CENTER></B>';
    }
    echo '<table align="center" cellspacing="0" border="0" WIDTH="760">' . "\n";
    if ($compose_new_win == '1') {
        echo '<TABLE ALIGN=CENTER BGCOLOR="'.$color[0].'" WIDTH="100%" BORDER=0>'."\n" .
             '   <TR><TD></TD>'. html_tag( 'td', '', 'right' ) . '<INPUT TYPE="BUTTON" NAME="Close" onClick="return self.close()" VALUE='._("Close").'></TD></TR>'."\n";
    }
    if ($location_of_buttons == 'top') {
        showComposeButtonRow();
    }

    /* display select list for identities */
// メール情報取得
	$mail_addr_info = get_mail_address_info($username, $fname);
// 差出人
$tmp_color = ($compose_new_win == '1') ? $color[0] : $color[4];
    echo '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color, 'width="10%"' ) .
                _("From:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color, 'width="90%"' ) ;

// 院内メール情報を配列に格納
	$inner_address_data = array($mail_addr_info["emp_nm"], $mail_addr_info["emp_mail_login_id"]);

// 院外メール情報を配列に格納
	if ($mail_addr_info["emp_email"] != "") {
		$email_address_data = array($mail_addr_info["emp_nm"], $mail_addr_info["emp_email"]);
	} else {
		$email_address_data = array();
	}

// オプションメール情報を配列に格納
	$option_address_datum = array();
	$idents = getPref($data_dir, $username, 'identities', 0);
	if ($idents > 0) {
		$fn = getPref($data_dir, $username, 'full_name');
		$em = getPref($data_dir, $username, 'email_address');
		if ($fn != "" || $em != "") {
			$option_address_datum["default"] = array($fn, $em);
		}

		for ($i = 1; $i < $idents; $i ++) {
			$fn = getPref($data_dir, $username, 'full_name' . $i);
			$em = getPref($data_dir, $username, 'email_address' . $i);
			if ($fn != "" || $em != "") {
				$option_address_datum[$i] = array($fn, $em);
			}
		}
	}

// 表示順に従って差出人リストを作成
	$from_address_datum = array();
	for ($i = 1; $i <= 3; $i++) {
		switch ($mail_addr_info["webmail_from_order"][$i]) {
		case 1:
			$from_address_datum["em1"] = $inner_address_data;
			break;
		case 2:
			$from_address_datum["em2"] = $email_address_data;
			break;
		case 3:
			$from_address_datum = $from_address_datum + $option_address_datum;
			break;
		}
	}

// 差出人リストが0件となった場合は院内メールアドレスを使用
	if (count($from_address_datum) == 0) {
		$from_address_datum["em1"] = $inner_address_data;
	}

// 差出人が未選択の場合は先頭のオプションが指定されていることにする（署名を表示したい）
	if (empty($identity) && count($from_address_datum) > 0) {
		$identity = key($from_address_datum);
	}

// 複数件の場合はセレクトボックスで表示
	if (count($from_address_datum) > 1) {
		echo '         <select name="identity">' . "\n";
		foreach ($from_address_datum as $tmp_identity => $from_address_data) {
			$fn = $from_address_data[0];
			$em = $from_address_data[1];

			echo '<option value="' . $tmp_identity . '"';
			if (isset($identity) && $identity == "$tmp_identity") {
				echo ' selected';
			}
			echo '>' . h($fn);
			if ($em != '') {
				if($fn != '') {
					echo h(' <' . $em . '>') . "\n";
				} else {
					echo h($em) . "\n";
				}
			}
			echo '</option>';
		}
	    echo '</select>' . "\n";

// 1件のみの場合は固定文字列で表示
	} else {
		$from_address_data = each($from_address_datum);
		$tmp_identity = $from_address_data["key"];
		$fn = $from_address_data["value"][0];
		$em = $from_address_data["value"][1];

		echo("<input type=\"hidden\" name=\"identity\" value=\"$tmp_identity\">\n");
		echo h($fn);
		if ($em != '') {
			if($fn != '') {
				echo h(' <' . $em . '>') . "\n";
			} else {
				echo h($em) . "\n";
			}
		}
	}
    echo '      </td>' . "\n" .
         '   </tr>' . "\n";
/* Del 2004/11/26 Iwamoto
    echo '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $color[4], 'width="10%"' ) .
                _("To:") . '</TD>' . "\n" .
                html_tag( 'td', '', 'left', $color[4], 'width="90%"' ) .
		addInput('send_to', $send_to, 60). '<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $color[4] ) .
                _("CC:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $color[4] ) .
                addInput('send_to_cc', $send_to_cc, 60). '<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $color[4] ) .
                _("BCC:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $color[4] ) .
	        addInput('send_to_bcc', $send_to_bcc, 60).'<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $color[4] ) .
                _("Subject:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $color[4] ) . "\n";
*/
// Add 2004/11/26 Iwamoto Start
// オプションによりメールアドレスの入力方法を変更 20150604
$use_address_dialog = getPref($data_dir, $username, 'use_address_dialog', 0);
if ($use_address_dialog != "1") {
    echo '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color, 'width="10%"' ) .
                _("To:") . '</TD>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color, 'width="90%"' ) .
		'<input type="text" name="send_to" value="'.$send_to.'" style="width:448px;">'. "\n" .
		"<input type=\"button\" value=\"職員名簿\" style=\"margin-left:2em;width=10.5em;\" onclick=\"openEmployeeList();\">" . '<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color ) .
                _("CC:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color ) .
		'<input type="text" name="send_to_cc" value="'.$send_to_cc.'" style="width:448px;">'. "\n" .
        "<input type=\"button\" value=\"アドレス帳\" style=\"margin-left:2em;width=10.5em;\" onclick=\"openAddressList();\">" . '<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color ) .
                _("BCC:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color ) .
	        '<input type="text" name="send_to_bcc" value="'.$send_to_bcc.'" style="width:448px;">';
    showAddressBookButton();

}
else {
    echo '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color, 'width="70"' ); //10%
//echo                _("To:");
?>
<div id="dmenu1">
<ul id="dropmenu">
  <li><a href="javascript:void(0);">TO:</a>
    <ul>
      <li><a href="javascript:moveAddr(1, 2);">全ての宛先をCCへ移動</a></li>
      <li><a href="javascript:moveAddr(1, 3);">全ての宛先をBCCへ移動</a></li>
      <li><a href="javascript:clearAddr(1);">全ての宛先をクリアする</a></li>
    </ul>
  </li>
</ul>
</div>
<?
    echo '</TD>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color, 'width="680"' ) . //90%
                // address link 20150508
                '<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">' .
                '<tr>' .
                '<td id="disp_area1" width="450" height="26" style="border:#808080 solid 1px;font-size:0.8em;">' .
                '<div id="target_disp_area1"></div><br />' .
                '<span id="chk_disp_area" style="display: ;"></span><br />' .
                '</td>' .
                '<td style="border:none solid 0px;" valign="top" nowrap>' .
         '&nbsp;<input type="button" id="change1" value="開く" style="width:50px" onclick="changeHeight(1);" disabled>' . "\n" .
                "<input type=\"button\" value=\"職員名簿\" style=\"margin-left:2em;width=10.5em;\" onclick=\"openEmployeeList();\">" . '<br />' . "\n" .
                '</td>' .
                '</tr>' .
                '</table>' .
		'<input type="text" name="send_to_add" value="" style="width:448px;">'. "\n" .
		'<input type="button" value="追加" style="width:50px;" onclick="addAddr(1);">' . "\n" .
		addHidden('send_to', $send_to). 
         
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color );
//         echo       _("CC:");
?>
<div id="dmenu2">
<ul id="dropmenu">
  <li><a href="javascript:void(0);">CC:</a>
    <ul>
      <li><a href="javascript:moveAddr(2, 1);">全ての宛先をTOへ移動</a></li>
      <li><a href="javascript:moveAddr(2, 3);">全ての宛先をBCCへ移動</a></li>
      <li><a href="javascript:clearAddr(2);">全ての宛先をクリアする</a></li>
    </ul>
  </li>
</ul>
</div>

<?

         echo '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color ) .
                // address link 20150508
                '<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">' .
                '<tr>' .
                '<td id="disp_area2" width="450" height="26" style="border:#808080 solid 1px;font-size:0.8em;">' .
                '<div id="target_disp_area2"></div><br />' .
                '</td>' .
                '<td style="border:none solid 0px;" valign="top" nowrap>' .
         '&nbsp;<input type="button" id="change2" value="開く" style="width:50px;" onclick="changeHeight(2);" disabled>' . "\n" .
                "<input type=\"button\" value=\"アドレス帳\" style=\"margin-left:2em;width=10.5em;\" onclick=\"openAddressList();\">" . '<br />' . "\n" .
                '</td>' .
                '</tr>' .
                '</table>' .
		 '<input type="text" name="send_to_cc_add" value="" style="width:448px;">'. "\n" .
         '<input type="button" value="追加" style="width:50px;" onclick="addAddr(2);">' . "\n" .
                addHidden('send_to_cc', $send_to_cc). 
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color );
//         echo   _("BCC:");
?>
<div id="dmenu3">
<ul id="dropmenu">
  <li><a href="javascript:void(0);">BCC:</a>
    <ul>
      <li><a href="javascript:moveAddr(3, 1);">全ての宛先をTOへ移動</a></li>
      <li><a href="javascript:moveAddr(3, 2);">全ての宛先をCCへ移動</a></li>
      <li><a href="javascript:clearAddr(3);">全ての宛先をクリアする</a></li>
    </ul>
  </li>
</ul>
</div>
<?
         
         echo '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color ) .
                // address link 20150508
                '<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">' .
                '<tr>' .
                '<td id="disp_area3" width="450" height="26" style="border:#808080 solid 1px;font-size:0.8em;">' .
                '<div id="target_disp_area3"></div><br />' .
                '</td>' .
                '<td style="border:none solid 0px;" valign="top" nowrap>' .
         '&nbsp;<input type="button" id="change3" value="開く" style="width:50px" onclick="changeHeight(3);" disabled>' . "\n" ;
                showAddressBookButton();
    echo        '</td>' .
                '</tr>' .
                '</table>' .
		 '<input type="text" name="send_to_bcc_add" value="" style="width:448px;">'. "\n" .
         '<input type="button" value="追加" style="width:50px;" onclick="addAddr(3);">' . "\n" .
	        addHidden('send_to_bcc', $send_to_bcc);
    echo addHidden('mod_addr', '');
}

    echo '<br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n" .
         '   <tr>' . "\n" .
                html_tag( 'td', '', 'right', $tmp_color ) .
                _("Subject:") . '</td>' . "\n" .
                html_tag( 'td', '', 'left', $tmp_color ) . "\n";
// Add 2004/11/26 Iwamoto End
    echo '         '.
		'<input type="text" name="subject" value="'.$subject.'" style="width:448px;">'. "\n" .
//    addInput('subject', $subject, 60).
         '      </td>' . "\n" .
         '   </tr>' . "\n\n";

// Add 2004/12/01 Iwamoto Start
    /* This code is for attachments */
        if ((bool) ini_get('file_uploads')) {

    /* Calculate the max size for an uploaded file.
     * This is advisory for the user because we can't actually prevent
     * people to upload too large files. */
    $sizes = array();
    /* php.ini vars which influence the max for uploads */
    $configvars = array('post_max_size', 'memory_limit', 'upload_max_filesize');
    foreach($configvars as $var) {
        /* skip 0 or empty values */
        if( $size = getByteSize(ini_get($var)) ) {
            $sizes[] = $size;
        }
    }
// Add 2010/04/05 Iwamoto Start
    require(dirname(dirname(dirname(__FILE__))) . '/conf/conf.inf');
    if (isset($WEBMAIL_UPLOAD_MAX) && ($size = getByteSize($WEBMAIL_UPLOAD_MAX))) {
        $sizes[] = $size;
    }
// Add 2010/04/05 Iwamoto End

    if(count($sizes) > 0) {
        $maxsize = '(max.&nbsp;' . show_readable_size( min( $sizes ) ) . ')';
    } else {
        $maxsize = '';
    }
    echo addHidden('MAX_FILE_SIZE', min( $sizes ));
    echo '   <tr>' . "\n" .
                                 html_tag( 'td', '', 'right', '', 'valign="middle"' ) .
                                 _("Attach:") . '</td>' . "\n" .
                                 html_tag( 'td', '', 'left', '', 'valign="middle"' ) .
         '                          <input name="attachfile" size="48" type="file" />' . "\n" .
         '                          &nbsp;&nbsp;<input type="submit" name="attach"' .
                                    ' value="' . _("Add") .'">' . "\n" .
                                    $maxsize .
         '                       </td>' . "\n" .
         '                    </tr>' . "\n";

    $s_a = array();
    if ($composeMessage->entities) {
        foreach ($composeMessage->entities as $key => $attachment) {
           $attached_file = $attachment->att_local_name;
           if ($attachment->att_local_name || $attachment->body_part) {
                $attached_filename = decodeHeader($attachment->mime_header->getParameter('name'));
                $type = $attachment->mime_header->type0.'/'.
                        $attachment->mime_header->type1;

                $s_a[] = '<table'.
                ' border="0" cellpadding="0" cellspacing="0"><tr><td>'.
		addCheckBox('delete[]', FALSE, $key).
		    "</td><td>\n" . $attached_filename .
                    '</td><td>-</td><td> ' . $type . '</td><td>('.
                    show_readable_size( filesize( $attached_file ) ) . ')</td></tr></table>'."\n";
           }
        }
    }
    if (count($s_a)) {
       foreach ($s_a as $s) {
          echo '<tr><td></td>' . html_tag( 'td', '', 'left', '', '' ) . $s .'</td></tr>';
       }
       echo '<tr><td></td><td style="padding-bottom:0.5em;"><input type="submit" name="do_delete" value="' .
            _("Delete selected attachments") . "\">\n" .
            '</td></tr>';
    }
        } // End of file_uploads if-block
// Add 2004/12/01 Iwamoto End

    if ($location_of_buttons == 'between') {
        showComposeButtonRow();
    }

    /* why this distinction? */
    if ($compose_new_win == '1') {
        echo '   <TR>' . "\n" .
             '      <TD BGCOLOR="' . $color[0] . '" COLSPAN=2 ALIGN=CENTER>' . "\n" .
             '         <TEXTAREA NAME="body" ID="body" ROWS="' . (int)$editor_height .
             '" COLS="' . (int)$editor_size . '" WRAP="VIRTUAL" STYLE="ime-mode: active;">';
    }
    else {
        echo '   <TR>' . "\n" .
            '      <TD BGCOLOR="' . $color[4] . '" COLSPAN=2>' . "\n" .
            '         &nbsp;&nbsp;<TEXTAREA NAME="body" ID="body" ROWS="' . (int)$editor_height .
            '" COLS="' . (int)$editor_size . '" WRAP="VIRTUAL" STYLE="ime-mode: active;">';
    }

    if ($use_signature == true && $newmail == true && !isset($from_htmladdr_search)) {
        if ($idents > 1) {
            if ($identity == 'default' || substr($identity, 0, 2) == 'em') {
                $no = 'g';
            } else {
                $no = $identity;
            }
            $signature = getSig($data_dir, $username, $no);
        }

        if ($sig_first == '1') {
            if ($default_charset == 'iso-2022-jp') {
                echo "\n\n".($prefix_sig==true? "-- \n":'').mb_convert_encoding($signature, 'EUC-JP');
            } else {
            echo "\n\n".($prefix_sig==true? "-- \n":'').decodeHeader($signature,false,false);
            }
            echo "\n\n".h(decodeHeader($body,false,false));
        }
        else {
            echo "\n\n".h(decodeHeader($body,false,false));
            if ($default_charset == 'iso-2022-jp') {
                echo "\n\n".($prefix_sig==true? "-- \n":'').mb_convert_encoding($signature, 'EUC-JP');
            }else{
            echo "\n\n".($prefix_sig==true? "-- \n":'').decodeHeader($signature,false,false);
        }
    }
    }
    else {
       echo h(decodeHeader($body,false,false));
    }
    echo '</textarea><br />' . "\n" .
         '      </td>' . "\n" .
         '   </tr>' . "\n";


    if ($location_of_buttons == 'bottom') {
        showComposeButtonRow();
    } else {
        echo '   <tr>' . "\n" .
                    html_tag( 'td', '', 'right', '', 'colspan="2"' ) . "\n" .
             '         ' . addSubmit(_("Send"), 'send').
             '         &nbsp;&nbsp;&nbsp;&nbsp;<br /><br />' . "\n" .
             '      </td>' . "\n" .
             '   </tr>' . "\n";
    }

// Del 2004/12/01 Iwamoto Start
    /* This code is for attachments */
//        if ((bool) ini_get('file_uploads')) {

    /* Calculate the max size for an uploaded file.
     * This is advisory for the user because we can't actually prevent
     * people to upload too large files. */
//    $sizes = array();
    /* php.ini vars which influence the max for uploads */
//    $configvars = array('post_max_size', 'memory_limit', 'upload_max_filesize');
//    foreach($configvars as $var) {
        /* skip 0 or empty values */
//        if( $size = getByteSize(ini_get($var)) ) {
//            $sizes[] = $size;
//        }
//    }

//    if(count($sizes) > 0) {
//        $maxsize = '(max.&nbsp;' . show_readable_size( min( $sizes ) ) . ')';
//    } else {
//        $maxsize = '';
//    }
//    echo addHidden('MAX_FILE_SIZE', min( $sizes ));
//    echo '   <tr>' . "\n" .
//         '      <td colspan="2">' . "\n" .
//         '         <table width="100%" cellpadding="1" cellspacing="0" align="center"'.
//                   ' border="0" bgcolor="'.$color[9].'">' . "\n" .
//         '            <tr>' . "\n" .
//         '               <td>' . "\n" .
//         '                 <table width="100%" cellpadding="3" cellspacing="0" align="center"'.
//                           ' border="0">' . "\n" .
//         '                    <tr>' . "\n" .
//                                 html_tag( 'td', '', 'right', '', 'valign="middle"' ) .
//                                 _("Attach:") . '</td>' . "\n" .
//                                 html_tag( 'td', '', 'left', '', 'valign="middle"' ) .
//         '                          <input name="attachfile" size="48" type="file" />' . "\n" .
//         '                          &nbsp;&nbsp;<input type="submit" name="attach"' .
//                                    ' value="' . _("Add") .'">' . "\n" .
//                                    $maxsize .
//         '                       </td>' . "\n" .
//         '                    </tr>' . "\n";


//    $s_a = array();
//    if ($composeMessage->entities) {
//        foreach ($composeMessage->entities as $key => $attachment) {
//           $attached_file = $attachment->att_local_name;
//           if ($attachment->att_local_name || $attachment->body_part) {
//                $attached_filename = decodeHeader($attachment->mime_header->getParameter('name'));
//                $type = $attachment->mime_header->type0.'/'.
//                        $attachment->mime_header->type1;

//                $s_a[] = '<table bgcolor="'.$color[0].
//                '" border="0"><tr><td>'.
//		addCheckBox('delete[]', FALSE, $key).
//		    "</td><td>\n" . $attached_filename .
//                    '</td><td>-</td><td> ' . $type . '</td><td>('.
//                    show_readable_size( filesize( $attached_file ) ) . ')</td></tr></table>'."\n";
//           }
//        }
//    }
//    if (count($s_a)) {
//       foreach ($s_a as $s) {
//          echo '<tr>' . html_tag( 'td', '', 'left', $color[0], 'colspan="2"' ) . $s .'</td></tr>';
//       }
//       echo '<tr><td colspan="2"><input type="submit" name="do_delete" value="' .
//            _("Delete selected attachments") . "\">\n" .
//            '</td></tr>';
//    }
//    echo '                  </table>' . "\n" .
//         '               </td>' . "\n" .
//         '            </tr>' . "\n" .
//         '         </TABLE>' . "\n" .
//         '      </TD>' . "\n" .
//         '   </TR>' . "\n";
//        } // End of file_uploads if-block
// Del 2004/12/01 Iwamoto End

    /* End of attachment code */
    if ($compose_new_win == '1') {
        echo '</TABLE>'."\n";
    }

    echo '</TABLE>' . "\n" .
         addHidden('username', $username).
	 addHidden('smaction', $action).
	 addHidden('mailbox', $mailbox);
    /*
       store the complete ComposeMessages array in a hidden input value
       so we can restore them in case of a session timeout.
    */
    sqgetGlobalVar('QUERY_STRING', $queryString, SQ_SERVER);
    echo addHidden('restoremessages', serialize($compose_messages)).
         addHidden('composesession', $composesession).
	 addHidden('querystring', $queryString).
	 "</form>\n";
    if (!(bool) ini_get('file_uploads')) {
      /* File uploads are off, so we didn't show that part of the form.
         To avoid bogus bug reports, tell the user why. */
      echo 'Because PHP file uploads are turned off, you can not attach files ';
      echo "to this message.  Please see your system administrator for details.\r\n";
    }

    do_hook('compose_bottom');
    echo("<script type=\"text/javascript\" src=\"../../js/jquery/jquery-1.11.2.min.js\"></script>\n");
// Add 2004/11/26 Iwamoto Start
    echo("<script type=\"text/javascript\">\n");
?>
$(function() {
	//クリックイベント

$("#showoverlay").click(function() {
    //オーバーレイ用のボックスを作成

  $("body").append("<div id='overlay'></div>");

  //フェードエフェクト

  $("#overlay").fadeTo(200, 0.7);

  $("#modalbox").fadeIn(200);

});
//閉じる際のクリックイベント
$("#close").click(function() {

  $("#modalbox, #overlay").fadeOut(200, function() {

      $("#overlay").remove();

  });



});

$(window).resize();

});

<?
    echo("function openEmployeeList() {\n");
    echo("\tvar session = getSession();\n");
    echo("\tdx = screen.availWidth - 10;");
    echo("\tdy = screen.top;");
    echo("\tbase = 0;");
    echo("\twx = 800;");
    echo("\twy = 600;");
    echo("\tvar url = '../../emplist/emplist.php';\n");
    echo("\turl += '?mode=webmail';\n");
    echo("\twindow.open(url, 'abookpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');\n");
    echo("}\n");
    echo("function openAddressList() {\n");
    echo("\tvar session = getSession();\n");
    echo("\tdx = screen.availWidth - 10;");
    echo("\tdy = screen.top;");
    echo("\tbase = 0;");
    echo("\twx = 700;");
    echo("\twy = 600;");
    echo("\tvar url = '../../emplist/addresslist.php';\n");
    echo("\twindow.open(url, 'abookpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');\n");
    echo("}\n");
    echo("function getSession() {\n");
    echo("\tif (opener) {\n");
    echo("\t\treturn opener.parent.parent.webmail_title.document.mainform.session.value;\n");
    echo("\t} else {\n");
    echo("\t\treturn parent.parent.webmail_title.document.mainform.session.value;\n");
    echo("\t}\n");
    echo("}\n");
    echo("var skipUnloadConfirm = false;\n");
    echo("window.onbeforeunload = function (e) {\n");
    echo("\tif (skipUnloadConfirm) {\n");
    echo("\t\tskipUnloadConfirm = false;\n");
    echo("\t\treturn;\n");
    echo("\t}\n");
    echo("\tif (document.compose.body.value.replace(/[\\r\\n 　]/g, '').length == 0) return;\n");
    echo("\t// ブラウザによってはこのメッセージは使われない\n");
    echo("\tvar message = 'メールは送信されていません。';\n");
    echo("\tif (!e) e = window.event;\n");
    echo("\tif (e) e.returnValue = message;\n");
    echo("\treturn message;\n");
    echo("}\n");
    echo("</script>\n");
// Add 2004/11/26 Iwamoto End

// Addr Link 20150508
?>
<script type="text/javascript">

<?
	if ($compose_new_win == '1') {
		global $compose_width, $compose_height;
		if ($compose_width < 820) {
			$compose_width = 820;
		}
		echo "window.resizeTo($compose_width, $compose_height);";
	}
?>

var arr_item_nm = new Array('', 'send_to', 'send_to_cc', 'send_to_bcc');
function addAddr(target_id) {

    var item_nm = arr_item_nm[target_id];
    var item_add_nm = item_nm+'_add';

    var chkstr = document.compose[item_add_nm].value;
    
    if (!checkEmail(chkstr, 0)) {
        document.compose[item_add_nm].focus();
        return false;
    }

    var send_to_add = chkstr.replace(/ /g, '');
    if (send_to_add == '') {
        document.compose[item_add_nm].value = '';
        return;
    }
    var a_list = document.compose[item_nm].value;
    if (a_list == '') { 
        document.compose[item_nm].value = send_to_add;
    } else {
        arr_name_addr = getArrNameAddr(target_id);
        for(var i=0; i<arr_name_addr.length; i++) {
            if (send_to_add == arr_name_addr[i]['addr']) {
                document.compose[item_add_nm].value = '';
                return;
            }
        }
        document.compose[item_nm].value += ','+send_to_add;
    
    }
    document.compose[item_add_nm].value = '';
    setDispArea(target_id, 2);
}

function checkEmail(str, modi_flg) {
    if (str.indexOf(',') > -1) {
        alert('正しいメールアドレスを入力してください。');
        return false;
    }
    var chkreg=/[!#-9A-~]+@+[a-z0-9]+.+[^.]$/i;
    var chkno_at=/[!#-9A-~]+$/i;
    var chkkakko=/.+<.+>$/i;
    
    //<>
    var chkstr;
    var kakko_flg = false;
    var kakko_idx = str.indexOf('<');
    if (kakko_idx > -1) {
        if(!str.match(chkkakko)){
            alert('正しいメールアドレスを入力してください。');
            return false;
        }
        var nm = str.substring(0, kakko_idx);
        nm = nm.replace(/ /g ,'');
        if (nm.replace(/　/g ,'') == '') {
            alert('正しいメールアドレスを入力してください。');
            return false;
        }
        chkstr = str.slice(kakko_idx+1, -1);
        kakko_flg = true;
    }
    else {
        chkstr = str;
    }
    
    if (chkstr != '') {
        if (chkstr.match(chkreg) || (modi_flg && kakko_flg && chkstr.match(chkno_at))) {
            return true;
        }
        else {
            alert('正しいメールアドレスを入力してください。');
            return false;
        }
    }
    else {
        alert('メールアドレスを入力してください。');
        return false;
    }
    return true;
}

//close_flg 0:開いた状態 1:閉じた状態 2:状態に応じて（継続）
function setDispArea(target_id, close_flg) {
<?
if ($use_address_dialog != "1") {
    echo "return false;";
}
?>
    var item_nm = arr_item_nm[target_id];
    var a_list = document.compose[item_nm].value;
    if (a_list == '') {
    	document.getElementById('target_disp_area'+target_id).innerHTML = '';
        return;
    }
    var wk_close_flg = close_flg;
    if (wk_close_flg == 2) {
    	wk_close_flg = (document.getElementById('change'+target_id).value == '開く') ? 1 : 0;
    }
    var link_str = getLinkStr(target_id, wk_close_flg);

    document.getElementById('target_disp_area'+target_id).innerHTML = link_str;
    document.getElementById('change'+target_id).value = (wk_close_flg == 1 || close_flg == 1) ? '開く' : '閉じる';

}

function delAddr(target_id, idx, no_confirm) {
    var item_nm = arr_item_nm[target_id];
    var a_list = document.compose[item_nm].value;
    var arr_list = a_list.split(',');
    if (!no_confirm) {
        if (!confirm(arr_list[idx]+'を削除します。よろしいですか？')) {
            return;
        }
    }
    var str = '';
    var cnt = 0;
    for(var i=0; i<arr_list.length; i++) {
        if (i==idx) { continue; }
        if (cnt>0) { str += ','; }
        str += arr_list[i];
        cnt++;
    }
    document.compose[item_nm].value = str;
    var close_flg = (document.getElementById('change'+target_id).value == '開く') ? 1 : 0;
    var link_str = getLinkStr(target_id, close_flg);
    document.getElementById('target_disp_area'+target_id).innerHTML = link_str;
    //document.getElementById('change'+target_id).value = '開く';

    $("#close").click();
}

function modAddr() {
    var target_id = document.modiform.target_id.value;
    var idx = document.modiform.idx.value;
    var addr = document.modiform.addr.value;
    
    if (!checkEmail(addr ,1)) {
        document.modiform.addr.focus();
        return false;
    }

    var item_nm = arr_item_nm[target_id];
    var a_list = document.compose[item_nm].value;
    var arr_list = a_list.split(',');
    var str = '';
    var cnt = 0;
    for(var i=0; i<arr_list.length; i++) {
        if (cnt>0) { str += ','; }
        if (i==idx) {
            str += addr;
        }
        else {
            str += arr_list[i];
        }
        cnt++;
    }
    document.compose[item_nm].value = str;
    var close_flg = (document.getElementById('change'+target_id).value == '開く') ? 1 : 0;
    var link_str = getLinkStr(target_id, close_flg);
    document.getElementById('target_disp_area'+target_id).innerHTML = link_str;
    //document.getElementById('change'+target_id).value = '開く';

    $("#close").click();
}

function getLinkStr(target_id, close_flg) {
    var item_nm = arr_item_nm[target_id];
    var a_list = document.compose[item_nm].value;
    if (a_list == '') {
        return '';
    }
    a_list = a_list.replace(/, /g, ',');
    a_list = a_list.replace(/\"/g, '');
    var arr_list = a_list.split(',');
    var str = '';
    var threshold = 5;
    for(var i=0; i<arr_list.length; i++) {
        if (close_flg == 1) {
            if (i >= threshold) {
                if (arr_list.length > threshold) {
                    var othernum = arr_list.length - threshold;
                    str += ' <a href="javascript:void(0);" onClick="changeHeight('+target_id+');"><span style="white-space:nowrap;display: inline-block;cursor:pointer;">(他'+othernum+'名...)</span></a>';
                }
                break;
            }
        }
        if (i>0) {
            str += ',';
        }
        var idx = arr_list[i].indexOf('<');
        str += '<a href="javascript:void(0);" onClick="';
        if (idx > -1) {
            str += 'openModal('+target_id+','+i+',\''+arr_list[i]+'\');" title=\''+arr_list[i].slice(idx+1, -1)+'\'><span style="white-space:nowrap;display: inline-block;cursor:pointer;">';
            str += arr_list[i].substring(0, idx);
        } else {
            str += 'openModal('+target_id+','+i+',\''+arr_list[i]+'\');"><span style="white-space:nowrap;display: inline-block;cursor:pointer;">';
            str += arr_list[i];
        }
        str += "</span></a>";
    
    }
    if (close_flg == 0 && arr_list.length > threshold) {
        str += ' <a href="javascript:void(0);" onClick="changeHeight('+target_id+');"><span style="white-space:nowrap;display: inline-block;cursor:pointer;">(全'+arr_list.length+'名)</span></a>';
    }
    if (arr_list.length > threshold) {
        document.getElementById('change'+target_id).disabled = false;
    }
    return str;
}

function getArrNameAddr(target_id) {
    var item_nm = arr_item_nm[target_id];
    var a_list = document.compose[item_nm].value;
    var arr_list = a_list.split(',');
    var arr_name_addr = new Array(arr_list.length);
    for(var i=0; i<arr_list.length; i++) {
        var idx = arr_list[i].indexOf('<');
        if (idx > -1) {
            arr_name_addr[i] = {name:arr_list[i].slice(0, idx),addr:arr_list[i].slice(idx+1, -1)};
        } else {
            arr_name_addr[i] = {name:'',addr:arr_list[i]};
        
        }
    }
    return arr_name_addr;
}

function openAddrModify(target_id, no, addr) {
    skipUnloadConfirm = true;
    document.compose.mod_addr.value = addr;
    window.open('addr_modify.php?target_id='+target_id+'&no='+no, 'addrmodwin', 'width=500,height=300,scrollbars=yes');

}

function changeHeight(target_id) {
    var open_flg = document.getElementById('change'+target_id).value;
    if (open_flg == '閉じる') {
        var link_str = getLinkStr(target_id, 1);
        document.getElementById('target_disp_area'+target_id).innerHTML = link_str;
        document.getElementById('change'+target_id).value = '開く';
    }
    else {
        var link_str = getLinkStr(target_id, 0);
        document.getElementById('target_disp_area'+target_id).innerHTML = link_str;
        document.getElementById('change'+target_id).value = '閉じる';
    }
}
function openModal(target_id, idx, addr) {
    skipUnloadConfirm = true;
    $("body").append("<div id='overlay'></div>");

    //フェードエフェクト

    $("#overlay").fadeTo(200, 0.7);

    $("#modalbox").fadeIn(200);
    var margin_top = 150;
    var margin_left = 190;

    $("#modalbox").css({
        top:margin_top+"px", left:margin_left+"px"
    });
    document.modiform.target_id.value = target_id;
    document.modiform.idx.value = idx;
    document.modiform.addr.value = addr;
    document.modiform.addr.focus();

    $(function(){
        $("input").on("keydown", function(e) {
            if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
                return false;
            } else {
                return true;
            }
        });
    });
}
function moveAddr(src_id, dest_id) {
    var src_nm = arr_item_nm[src_id];
    var dest_nm = arr_item_nm[dest_id];

	if (document.compose[dest_nm].value != '') {
		document.compose[dest_nm].value += ',';
	}
	document.compose[dest_nm].value += document.compose[src_nm].value;
	document.compose[src_nm].value = '';

    setDispArea(src_id, 2);
    setDispArea(dest_id, 2);

}
function clearAddr(target_id) {
	
	if (!confirm('宛先をクリアします。よろしいですか？')) {
		return;
	}

    var target_nm = arr_item_nm[target_id];

	document.compose[target_nm].value = '';

    setDispArea(target_id, 2);

}
setDispArea(1, 1);
setDispArea(2, 1);
setDispArea(3, 1);

</script>
<div id="modalbox">

<a href="#" id="close"><img src="../../img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = '../../img/icon/closeo.gif';" onmouseout="this.src = '../../img/icon/close.gif';"></a>

<br>

<form method="post" name="modiform">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" name="addr" id="addr" size="54" value="">
<input type="hidden" name="target_id" id="target_id" value="">
<input type="hidden" name="idx" id="idx" value="">
</font></td>
</tr>
</table>
<table width="400" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="center"><input type="button" value="変更" style="width:100px;" onClick="return modAddr();"></td>
<td align="center"><input type="button" value="削除" style="width:100px;" onClick="return delAddr(document.modiform.target_id.value, document.modiform.idx.value, 1);"></td>
</tr>
</table>

</form>

</div>
<?

    echo '</BODY></HTML>' . "\n";
}

// Add 2004/12/02 Iwamoto Start
function showAddressBookButton() {
    require(dirname(dirname(dirname(__FILE__))) . "/conf/conf.inf");
    if (isset($webmail_addressbook_hide_flg) && $webmail_addressbook_hide_flg) {
        return;
    }

    global $use_javascript_addr_book;
    if ($use_javascript_addr_book) {
        echo "         <SCRIPT LANGUAGE=JavaScript><!--\n document.write(\"".
             "            <input type=button value=\\\""._("Addresses").
                                 "\\\" onclick='javascript:open_abook();' style=\\\"margin-left:2em;width=10.5em;\\\">\");".
             "            // --></SCRIPT><NOSCRIPT>\n".
             "            <input type=submit name=\"html_addr_search\" value=\"".
                              _("Addresses")."\" style=\"margin-left:2em;width=10.5em;\">".
             "         </NOSCRIPT>\n";
    } else {
        echo '         <input type=submit name="html_addr_search" value="'.
                                 _("Addresses").'" style="margin-left:2em;width=10.5em;">' . "\n";
    }
}
// Add 2004/12/02 Iwamoto End

function showComposeButtonRow() {
    global $use_javascript_addr_book, $save_as_draft,
           $default_use_priority, $mailprio, $default_use_mdn,
           $request_mdn, $request_dr, $use_secret_checkbox, $is_secret_message,
           $data_dir, $username;

    echo '   <TR>' . "\n" .
         '      <TD></TD>' . "\n" .
         '      <TD>' . "\n";
    if ($default_use_priority) {
        if(!isset($mailprio)) {
            $mailprio = '3';
        }
        echo '          ' . _("Priority") .
             addSelect('mailprio', array(
	         '1' => _("High"),
	         '3' => _("Normal"),
	         '5' => _("Low") ), $mailprio, TRUE);
    }
    $mdn_user_support=getPref($data_dir, $username, 'mdn_user_support',$default_use_mdn);
    if ($default_use_mdn) {
        if ($mdn_user_support) {
/*
            echo '          ' . _("Receipt") .': '.
	    addCheckBox('request_mdn', $request_mdn == '1', '1'). _("On Read").
	    addCheckBox('request_dr',  $request_dr  == '1', '1'). _("On Delivery");
*/
            echo '&nbsp;' . addCheckBox('request_mdn', $request_mdn == '1', '1'). "開封通知を要求する";
        }
    }

    if ($use_secret_checkbox) {
        echo '&nbsp;<input type="checkbox" name="is_secret_message" value="1"';
        if ($is_secret_message == '1') echo ' checked';
        echo ' onclick="changeSecretMode(this.checked);">機密';
?>
<script type="text/javascript">
function changeSecretMode(isSecret) {
    if (isSecret) {
        document.compose.subject.value = '【機密】' + document.compose.subject.value;
    } else {
        document.compose.subject.value = document.compose.subject.value.replace(/【機密】/g, '');
    }
}

function checkSecret() {
    if (document.compose.is_secret_message) {
        document.compose.is_secret_message.checked = (document.compose.subject.value.indexOf('【機密】') !== -1);
    }
}
</script>
<?
    }

    echo '      </TD>' . "\n" .
         '   </TR>' . "\n" .
         '   <TR>'  . "\n" .
         '      <TD></TD>' . "\n" .
         '      <TD style="position:relative;">' . "\n" .
         '         <INPUT TYPE=SUBMIT NAME="sigappend" VALUE="' . _("Signature") . '">' . "\n";
// Del 2004/12/02 Iwamoto Start
/*
    if ($use_javascript_addr_book) {
        echo "         <SCRIPT LANGUAGE=JavaScript><!--\n document.write(\"".
             "            <input type=button value=\\\""._("Addresses").
                                 "\\\" onclick='javascript:open_abook();'>\");".
             "            // --></SCRIPT><NOSCRIPT>\n".
             "            <input type=submit name=\"html_addr_search\" value=\"".
                              _("Addresses")."\">".
             "         </NOSCRIPT>\n";
    } else {
        echo '         <input type=submit name="html_addr_search" value="'.
                                 _("Addresses").'">' . "\n";
    }
*/
// Del 2004/12/02 Iwamoto End
    if ($save_as_draft) {
        echo '         <input type="submit" name ="draft" value="' . _("Save Draft") . "\">\n";
    }

    do_hook('compose_button_row');
    echo '         <SPAN style="position:absolute;right:0;"><INPUT TYPE=submit NAME=send VALUE="'. _("Send") . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN>' . "\n";

    echo '      </TD>' . "\n" .
         '   </TR>' . "\n\n";
}

function checkInput ($show) {
    /*
     * I implemented the $show variable because the error messages
     * were getting sent before the page header.  So, I check once
     * using $show=false, and then when i'm ready to display the error
     * message, show=true
     */
    global $body, $send_to, $send_to_bcc, $subject, $color;
    global $config_info, $in_mail_cnt, $e_mail_cnt, $is_secret_message;
    global $webmail_inside_only_flg;
    global $identity, $username, $fname, $data_dir, $popuser, $domain;

    if ($is_secret_message == "1" && mb_strpos($subject, "【機密】") === false) {
        $subject = "【機密】" . $subject;
    }

    if ($send_to == '' && $send_to_bcc == '') {
        if ($show) {
            plain_error_message(_("You have not filled in the \"To:\" field."), $color);
        }
        return false;
    }

    // 宛先が外部アドレスばかりの場合、機密メールを送信しようとしたらエラーにする
    if ($in_mail_cnt == 0 && $e_mail_cnt > 0 && mb_strpos($subject, "【機密】") !== false) {
        if ($show) {
            plain_error_message("【機密】のメールは外部に送信できません", $color);
        }
        return false;
    }

    // 内部アドレスから外部アドレス宛への送信を拒否するケース
    if (isset($webmail_inside_only_flg) && $webmail_inside_only_flg && $e_mail_cnt > 0) {
        if (isset($identity) && substr($identity, 0, 2) == 'em') {
            $mail_addr_info = get_mail_address_info($username, $fname);
            if ($identity == 'em1') {
                $from_mail = $mail_addr_info["emp_mail_login_id"];
            } else {
                $from_mail = $mail_addr_info["emp_email"];
            }
        } elseif (isset($identity) && $identity != 'default') {
            $from_mail = getPref($data_dir, $username, 'email_address' . $identity);
        } else {
            $from_mail = getPref($data_dir, $username, 'email_address');
        }
        if (!$from_mail) {
            $from_mail = "$popuser@$domain";
        }
    
        if (isInsideAddress($from_mail, $domain)) {
            if ($show) {
                plain_error_message("内部アドレスから外部アドレス宛には送信できません。", $color);
            }
            return false;
        }
    }

/*
    if ($config_info["smtp_server_address"] != "localhost" &&
    	$in_mail_cnt > 0 && $e_mail_cnt >0) {
        if ($show) {
            plain_error_message("SMTPサーバを指定している場合は院内メールとEmailアドレスへ同時に送信できません", $color);
        }
        return false;
    }
*/
    return true;
} /* function checkInput() */


/* True if FAILURE */
function saveAttachedFiles($session) {
    global $_FILES, $attachment_dir, $attachments, $username,
           $data_dir, $compose_messages;

    /* get out of here if no file was attached at all */
    if (! is_uploaded_file($_FILES['attachfile']['tmp_name']) ) {
        return true;
    }

// Add 2010/04/05 Iwamoto Start
    $configvars = array('post_max_size', 'memory_limit', 'upload_max_filesize');
    foreach($configvars as $var) {
        if( $size = getByteSize(ini_get($var)) ) {
            $sizes[] = $size;
        }
    }
    require(dirname(dirname(dirname(__FILE__))) . '/conf/conf.inf');
    if (isset($WEBMAIL_UPLOAD_MAX) && ($size = getByteSize($WEBMAIL_UPLOAD_MAX))) {
        $sizes[] = $size;
    }
    $maxsize = min($sizes);
    if ($_FILES['attachfile']['size'] > $maxsize) {
        return true;
    }
// Add 2010/04/05 Iwamoto End

    $hashed_attachment_dir = getHashedDir($username, $attachment_dir);
    $localfilename = GenerateRandomString(32, '', 7);
    $full_localfilename = "$hashed_attachment_dir/$localfilename";
    while (file_exists($full_localfilename)) {
        $localfilename = GenerateRandomString(32, '', 7);
        $full_localfilename = "$hashed_attachment_dir/$localfilename";
    }

    // FIXME: we SHOULD prefer move_uploaded_file over rename because
    // m_u_f works better with restricted PHP installes (safe_mode, open_basedir)
    if (!@rename($_FILES['attachfile']['tmp_name'], $full_localfilename)) {
            if (!@move_uploaded_file($_FILES['attachfile']['tmp_name'],$full_localfilename)) {
                return true;
                }
    }
    $message = $compose_messages[$session];
    $type = strtolower($_FILES['attachfile']['type']);
    if ($type == 'text/richtext') {
        $type = 'application/msword';
    }
    $name = $_FILES['attachfile']['name'];
    $message->initAttachment($type, $name, $full_localfilename);
    $compose_messages[$session] = $message;
    sqsession_register($compose_messages , 'compose_messages');
}

function ClearAttachments($composeMessage) {
    if ($composeMessage->att_local_name) {
        $attached_file = $composeMessage->att_local_name;
        if (file_exists($attached_file)) {
            unlink($attached_file);
        }
    }
    for ($i=0, $entCount=count($composeMessage->entities);$i< $entCount; ++$i) {
        ClearAttachments($composeMessage->entities[$i]);
    }
}

/* parse values like 8M and 2k into bytes */
function getByteSize($ini_size) {

    if(!$ini_size) {
        return FALSE;
    }

    $ini_size = trim($ini_size);

    // if there's some kind of letter at the end of the string we need to multiply.
    if(!is_numeric(substr($ini_size, -1))) {

        switch(strtoupper(substr($ini_size, -1))) {
            case 'G':
               $bytesize = 1073741824;
               break;
            case 'M':
               $bytesize = 1048576;
               break;
            case 'K':
               $bytesize = 1024;
               break;
        }

        return ($bytesize * (int)substr($ini_size, 0, -1));
    }

    return $ini_size;
}


/* temporary function to make use of the deliver class.
   In the future the responsable backend should be automaticly loaded
   and conf.pl should show a list of available backends.
   The message also should be constructed by the message class.
*/

function deliverMessage($composeMessage, $draft=false) {
    global $send_to, $send_to_cc, $send_to_bcc, $mailprio, $subject, $body,
           $username, $popuser, $usernamedata, $identity, $data_dir,
           $request_mdn, $request_dr, $default_charset, $color, $useSendmail,
           $domain, $action, $default_move_to_sent, $move_to_sent;
    global $imapServerAddress, $imapPort, $sent_folder, $key, $fname;
    global $send_mode, $send_to2, $send_to_cc2, $send_to_bcc2, $config_info;
    global $in_mail_cnt, $e_mail_cnt;
    global $arr_send_to, $arr_send_to_cc, $arr_send_to_bcc;

    $send_alert = false;

    // 機密メールで外部アドレスが宛先に指定されている場合
    if (!$draft && mb_strpos($subject, "【機密】") !== false && $e_mail_cnt > 0) {
        $orig_send_to     = $send_to;
        $orig_send_to_cc  = $send_to_cc;
        $orig_send_to_bcc = $send_to_bcc;
        list($send_to,     $e_mails_to)     = removeEmail($arr_send_to);
        list($send_to_cc,  $e_mails_to_cc)  = removeEmail($arr_send_to_cc);
        list($send_to_bcc, $e_mails_to_bcc) = removeEmail($arr_send_to_bcc);
        $e_mail_cnt = 0;
        $orig_body = $body;
        $send_alert = true;
    }

    $rfc822_header = $composeMessage->rfc822_header;

    $abook = addressbook_init(false, true);
    $rfc822_header->to = $rfc822_header->parseAddress($send_to,true, array(), '', $domain, array(&$abook,'lookup'));
    $rfc822_header->cc = $rfc822_header->parseAddress($send_to_cc,true,array(), '',$domain, array(&$abook,'lookup'));
    $rfc822_header->bcc = $rfc822_header->parseAddress($send_to_bcc,true, array(), '',$domain, array(&$abook,'lookup'));
    $rfc822_header->priority = $mailprio;
    $rfc822_header->subject = $subject;
    $special_encoding='';
    if (strtolower($default_charset) == 'iso-2022-jp') {
//        if (mb_detect_encoding($body) == 'ASCII') {
//            $special_encoding = '8bit';
//        } else {
            $body = mb_convert_encoding($body, 'JIS');
            $special_encoding = '7bit';
//        }
    }
    $composeMessage->setBody($body);

    // remove NUL characters
    $composeMessage->body_part = str_replace("\0", '', $composeMessage->body_part);

    if (ereg("^([^@%/]+)[@%/](.+)$", $username, $usernamedata)) {
       $popuser = $usernamedata[1];
       $domain  = $usernamedata[2];
       unset($usernamedata);
    } else {
       $popuser = $username;
    }
    $reply_to = '';
    // 差出人の設定
    if (isset($identity) && substr($identity, 0, 2) == 'em') {
		$mail_addr_info = get_mail_address_info($username, $fname);
		$full_name = $mail_addr_info["emp_nm"];
		// 院内メール
		if ($identity == 'em1') {
			$from_mail = $mail_addr_info["emp_mail_login_id"];
		} else {
		// 院外（email）
			$from_mail = $mail_addr_info["emp_email"];
		}
    } elseif (isset($identity) && $identity != 'default') {
        $from_mail = getPref($data_dir, $username,'email_address' . $identity);
        $full_name = getPref($data_dir, $username,'full_name' . $identity);
        $reply_to = getPref($data_dir, $username,'reply_to' . $identity);
    } else {
        $from_mail = getPref($data_dir, $username, 'email_address');
        $full_name = getPref($data_dir, $username, 'full_name');
        $reply_to = getPref($data_dir, $username,'reply_to');
    }
    if (!$from_mail) {
       $from_mail = "$popuser@$domain";
    }
    $rfc822_header->from = $rfc822_header->parseAddress($from_mail,true);
    if ($full_name) {
        $from = $rfc822_header->from[0];
        if (!$from->host) $from->host = $domain;
        $full_name_encoded = encodeHeader($full_name);
        if ($full_name_encoded != $full_name) {
            $from_addr = $full_name_encoded .' <'.$from->mailbox.'@'.$from->host.'>';
        } else {
            $from_addr = '"'.$full_name .'" <'.$from->mailbox.'@'.$from->host.'>';
        }
        $rfc822_header->from = $rfc822_header->parseAddress($from_addr,true);
    }
    if ($reply_to) {
       $rfc822_header->reply_to = $rfc822_header->parseAddress($reply_to,true);
    }
    /* Receipt: On Read */
    if (isset($request_mdn) && $request_mdn) {
       $rfc822_header->dnt = $rfc822_header->parseAddress($from_mail,true);
       if (is_array($rfc822_header->dnt)) {
	       for( $idx=0; $idx<count($rfc822_header->dnt); $idx++) {
		       if (!$rfc822_header->dnt[0]->host) $rfc822_header->dnt[0]->host = $from->host;
		   }
		}
    }
    /* Receipt: On Delivery */
    if (isset($request_dr) && $request_dr) {
       $rfc822_header->more_headers['Return-Receipt-To'] = $from_mail;
    }
    /* multipart messages */
    if (count($composeMessage->entities)) {
        $message_body = new Message();
        $message_body->body_part = $composeMessage->body_part;
        $composeMessage->body_part = '';
        $mime_header = new MessageHeader;
        $mime_header->type0 = 'text';
        $mime_header->type1 = 'plain';
        if ($special_encoding) {
            $mime_header->encoding = $special_encoding;
			if ($special_encoding == '8bit') {
				$mime_header->parameters['charset'] = 'us-ascii';
			} else {
				$mime_header->parameters['charset']=$default_charset;
			}
        } else {
            $mime_header->encoding = '8bit';
			if ($default_charset) {
				$mime_header->parameters['charset'] = $default_charset;
			}
        }
        $message_body->mime_header = $mime_header;
        array_unshift($composeMessage->entities, $message_body);
        $content_type = new ContentType('multipart/mixed');
    } else {
        $content_type = new ContentType('text/plain');
        if ($special_encoding) {
            $rfc822_header->encoding = $special_encoding;
			if ($special_encoding == '8bit') {
				$content_type->properties['charset'] = 'us-ascii';
			} else {
				$content_type->properties['charset']=$default_charset;
			}
        } else {
            $rfc822_header->encoding = '8bit';
			if ($default_charset) {
				$content_type->properties['charset']=$default_charset;
			}
        }
    }

    $rfc822_header->content_type = $content_type;
    $composeMessage->rfc822_header = $rfc822_header;

    /* Here you can modify the message structure just before we hand
       it over to deliver */
    $hookReturn = do_hook('compose_send', $composeMessage);
    /* Get any changes made by plugins to $composeMessage. */
    if ( is_object($hookReturn[1]) ) {
        $composeMessage = $hookReturn[1];
    }

    if (!$useSendmail && !$draft) {
        require_once(SM_PATH . 'class/deliver/Deliver_SMTP.class.php');
        $deliver = new Deliver_SMTP();
        global $smtpServerAddress, $smtpPort, $pop_before_smtp, $smtp_auth_mech;

        if ($smtp_auth_mech == 'none') {
                $user = '';
                $pass = '';
        } else {
                global $key, $onetimepad;
                $user = $username;
                $pass = OneTimePadDecrypt($key, $onetimepad);
        }

        $authPop = (isset($pop_before_smtp) && $pop_before_smtp) ? true : false;
		// あて先がEmailアドレスの場合、SMTPを環境設定の値に変更
		if ($e_mail_cnt > 0) {
			$smtpServerAddress = $config_info["smtp_server_address"];
			$smtpPort = $config_info["smtp_port"];
		}

        $stream = $deliver->initStream($composeMessage,$domain,0,
                          $smtpServerAddress, $smtpPort, $user, $pass, $authPop);
    } elseif (!$draft) {
       require_once(SM_PATH . 'class/deliver/Deliver_SendMail.class.php');
       global $sendmail_path;
       $deliver = new Deliver_SendMail();
       $stream = $deliver->initStream($composeMessage,$sendmail_path);
    } elseif ($draft) {
       global $draft_folder;
       require_once(SM_PATH . 'class/deliver/Deliver_IMAP.class.php');
       $imap_stream = sqimap_login($username, $key, $imapServerAddress,
                      $imapPort, 0);
       if (sqimap_mailbox_exists ($imap_stream, $draft_folder)) {
           require_once(SM_PATH . 'class/deliver/Deliver_IMAP.class.php');
           $imap_deliver = new Deliver_IMAP();
           $length = $imap_deliver->mail($composeMessage);
           sqimap_append ($imap_stream, $draft_folder, $length);
           $imap_deliver->mail($composeMessage, $imap_stream);
               sqimap_append_done ($imap_stream, $draft_folder);
           sqimap_logout($imap_stream);
           unset ($imap_deliver);
           return $length;
        } else {
           $msg  = '<br />'.sprintf(_("Error: Draft folder %s does not exist."), $draft_folder);
           plain_error_message($msg, $color);
           return false;
        }
    }
    $succes = false;
    if ($stream) {
        $length = $deliver->mail($composeMessage, $stream);
        $succes = $deliver->finalizeStream($stream);
    }
    if (!$succes) {
        $msg  = $deliver->dlv_msg . '<br />' .
                _("Server replied: ") . $deliver->dlv_ret_nr . ' '.
                $deliver->dlv_server_msg;
        plain_error_message($msg, $color);
    } else {
        unset ($deliver);
        $move_to_sent = getPref($data_dir,$username,'move_to_sent');
        $imap_stream = sqimap_login($username, $key, $imapServerAddress, $imapPort, 0);

        /* Move to sent code */
        if (isset($default_move_to_sent) && ($default_move_to_sent != 0)) {
            $svr_allow_sent = true;
        } else {
            $svr_allow_sent = false;
        }

        if (isset($sent_folder) && (($sent_folder != '') || ($sent_folder != 'none'))
           && sqimap_mailbox_exists( $imap_stream, $sent_folder)) {
            $fld_sent = true;
        } else {
            $fld_sent = false;
        }

        if ((isset($move_to_sent) && ($move_to_sent != 0)) || (!isset($move_to_sent))) {
            $lcl_allow_sent = true;
        } else {
            $lcl_allow_sent = false;
        }

        if (($fld_sent && $svr_allow_sent && !$lcl_allow_sent) || ($fld_sent && $lcl_allow_sent)) {
            sqimap_append ($imap_stream, $sent_folder, $length);
            require_once(SM_PATH . 'class/deliver/Deliver_IMAP.class.php');
            $imap_deliver = new Deliver_IMAP();
            $imap_deliver->mail($composeMessage, $imap_stream);
            sqimap_append_done ($imap_stream, $sent_folder);
            unset ($imap_deliver);
        }
        global $passed_id, $mailbox, $action;
        ClearAttachments($composeMessage);
        if ($action == 'reply' || $action == 'reply_all') {
            sqimap_mailbox_select ($imap_stream, $mailbox);
            sqimap_messages_flag ($imap_stream, $passed_id, $passed_id, 'Answered', false);
        }
            sqimap_logout($imap_stream);

        if ($send_alert) {
            sendAlert($from_mail, $e_mails_to, $e_mails_to_cc, $e_mails_to_bcc, $subject, $orig_send_to, $orig_send_to_cc, $orig_send_to_bcc, $orig_body);
        }
    }
    return $succes;
}

function isInsideAddress($address, $domain, $cmx_inner_domain) {
    if (strpos($address, "@") === false) {
        return true;
    } else if (strpos($address, "@{$domain}") !== false) {
        return true;
    } else if ($cmx_inner_domain != "" && strpos($address, "@{$cmx_inner_domain}") !== false) {
        return true;
    } else {
        return false;
    }
}

function removeEmail($arr_send_to) {
    $arr_send_to_removed = array();
    $e_mails_to = array();

    foreach ($arr_send_to as $tmp_send_to) {
        if ($tmp_send_to["type"] == "in") {

            // 名前あり
            if ($tmp_send_to[1] != "") {
                $tmp_address = "{$tmp_send_to[1]} <{$tmp_send_to[0]}>";

            // 名前なし
            } else {
                $tmp_address = $tmp_send_to[0];
            }
            $arr_send_to_removed[] = $tmp_address;
        } else {
            $e_mails_to[] = $tmp_send_to[0];
        }
    }

    return array(implode(", ", $arr_send_to_removed), $e_mails_to);
}

function sendAlert($mail_to, $e_mails_to, $e_mails_to_cc, $e_mails_to_bcc, $orig_subject, $orig_send_to, $orig_send_to_cc, $orig_send_to_bcc, $orig_body) {
    $subject = "【機密】メールを送信できませんでした。";

    $emails = "・ " . implode("\n・ ", array_unique(array_merge($e_mails_to, $e_mails_to_cc, $e_mails_to_bcc)));

    $wdays = array("日", "月", "火", "水", "木", "金", "土");
    $wday = $wdays[date("w")];
    $datetime = date("Y年 m月 d日 ($wday) h:i a");

    $orig_send_to     = ($orig_send_to     != "") ? "\nTo:     $orig_send_to"     : "";
    $orig_send_to_cc  = ($orig_send_to_cc  != "") ? "\nCC:     $orig_send_to_cc"  : "";
    $orig_send_to_bcc = ($orig_send_to_bcc != "") ? "\nBCC:    $orig_send_to_bcc" : "";

    $message = <<<EOS
機密メールなので、外部アドレスには送信されませんでした。
送付されなかったアドレスは下記の通りです。

$emails

-------------------------- オリジナルメッセージ --------------------------
件名:   $orig_subject
差出人: $mail_to
日付:   $datetime$orig_send_to$orig_send_to_cc$orig_send_to_bcc
--------------------------------------------------------------------------

$orig_body
EOS;

    $additional_headers = "From: noreply";

    mb_internal_encoding("EUC-JP");
    mb_language("Japanese");
    mb_send_mail($mail_to, $subject, $message, $additional_headers);
}
