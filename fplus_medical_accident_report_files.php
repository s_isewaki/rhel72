<?
ob_start();
require_once("about_comedix.php");
require_once("fplus_common.ini");
require_once("fplus_common_class.php");

//ob_end_clean();

$fname = $PHP_SELF;
$con = @connect2db($fname);

if(!$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

//====================================================================
// 定型
//====================================================================
$path = "./fplus/attachment_files/";
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
$message = "";

//====================================================================
// パラメータ取得
//====================================================================
$session = @$_REQUEST["session"];
$medical_accident_id = @$_REQUEST["medical_accident_id"];

//====================================================================
// DB操作
//====================================================================
// 登録処理
if($mode == "reg") {
	// データ取得
	$reg_file_no = @$_REQUEST["reg_file_no"];
	
	if (empty($reg_file_no)) {
		$message .= "ファイル番号を入力してください。<br>";
	}
	if ($_FILES["reg_file"]["size"] == 0) {
		$message .= "ファイルを指定してください。<br>";
	}
	
	if ($message == "") {
		// file_idの最大値取得
		$sql = "select";
			$sql .= " max(file_id) as max_file_id";
		$sql .= " from";
			$sql .= " fplus_medical_accident_report_files";
		
		$sel = @select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$result = pg_fetch_all($sel);
		
		// 取得できない場合
		if (is_null($result[0]["max_file_id"])) {
			$reg_file_id = 0;
		} else {
			$reg_file_id = ((int) $result[0]["max_file_id"]) + 1;
		}
		
		//====================================================================
		// インポート処理
		//====================================================================
		$str = "";				// 拡張子
		$new_name = "";			// パスに指定する名称
		$reg_file_name = "";	// ファイル名
		$reg_file_path = "";	// ファイルパス
		
		// インポートされたファイルかを調べる
		if (is_uploaded_file($_FILES["reg_file"]["tmp_name"])) {
			// ファイル格納ディレクトリの存在がない場合
			if (!(file_exists($path) && is_dir($path))) {
				mkdir($path, 0755);
			}
			
			// 拡張子取得
			$str = substr($_FILES["reg_file"]["name"], strrpos($_FILES["reg_file"]["name"], '.') + 1);
			
			// ファイルの移動を行う
			if (move_uploaded_file($_FILES["reg_file"]["tmp_name"], $path . $_FILES["reg_file"]["name"])) {
				// パーミッション設定
				chmod($path . $_FILES["reg_file"]["name"], 0644);
				
				$reg_file_name = $_FILES["reg_file"]["name"];
				
				$new_name = $reg_file_id . "." . $str;
				
				// 名称変更
				rename($path . $_FILES["reg_file"]["name"], $path . $new_name);
				
				$reg_file_path = $path . $new_name;
			}
		}
		
		//====================================================================
		// インサート処理
		//====================================================================
		// select句を生成
		$sql_field_arr = array(
			"file_id"
			, "medical_accident_id"
			, "file_no"
			, "file_name"
			, "file_path"
			, "update_time"
			, "del_flg"
		);
		
		// 登録するデータを配列に格納
		$sql_val_arr = array(
			$reg_file_id
			, $medical_accident_id
			, pg_escape_string($reg_file_no)
			, pg_escape_string($reg_file_name)
			, pg_escape_string($reg_file_path)
			, date("Y-m-d H:i:s")
			, "f"
		);
		
		// sql文を生成
		$sql = "insert into";
			$sql .= " fplus_medical_accident_report_files";
			$sql .= "(";
				$sql .= implode(", ", $sql_field_arr);
			$sql .= ") values(";
		
		// ==================================================
		// 登録SQLを実行
		// insert_into_table() from about_postgres.php
		// ==================================================
		$result = insert_into_table($con, $sql, $sql_val_arr, $fname);
		if ($result == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			showErrorPage();
			exit;
		}
		
		$message = "登録処理を行いました。";
		$reg_file_no = "";
		$reg_file_name = "";
		
		echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
	}
// 更新処理
} else if($mode == "udt") {
	// データ取得
	$data_cnt = @$_REQUEST["data_cnt"];
	
	$result_cnt = 0;
	
	for ($i = 0; $i < $data_cnt; $i++) {
		$file_id = @$_REQUEST["file_id_" . $i];
		$af_file_no = @$_REQUEST["af_file_no_" . $i];
		$bf_file_no = @$_REQUEST["bf_file_no_" . $i];
		
		if ($af_file_no != $bf_file_no) {
			// select句を生成
			$sql_field_arr = array(
				"file_no"
				, "update_time"
			);
			
			// 登録するデータを配列に格納
			$sql_val_arr = array(
				$af_file_no
				, date("Y-m-d H:i:s")
			);
			
			$sql = "update fplus_medical_accident_report_files set";
			
			$cond = "where";
				$cond .= " file_id = " . $file_id;
			
			// ==================================================
			// 更新SQLを実行
			// update_set_table() from about_postgres.php
			// ==================================================
			$result = update_set_table($con, $sql, $sql_field_arr, $sql_val_arr, $cond, $fname);
			if ($result == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				showErrorPage();
				exit;
			}
			
			$result_cnt++;
		}
	}
	if ($result_cnt > 0) {
		$message = "更新された " . $result_cnt . " 件の処理を行いました。";
	} else {
		$message = "更新された行はありませんでした。";
	}
	
// 削除処理
} else if($mode == "del") {
	// データ取得
	$target_id = @$_REQUEST["target_id"];
	
	// select句を生成
	$sql_field_arr = array(
		"update_time"
		, "del_flg"
	);
	
	// 登録するデータを配列に格納
	$sql_val_arr = array(
		date("Y-m-d H:i:s")
		, "t"
	);
	
	$sql = "update fplus_medical_accident_report_files set";
	
	$cond = "where";
		$cond .= " file_id = " . $target_id;
	
	// ==================================================
	// 更新SQLを実行
	// update_set_table() from about_postgres.php
	// ==================================================
	$result = update_set_table($con, $sql, $sql_field_arr, $sql_val_arr, $cond, $fname);
	if ($result == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$message = "削除処理を行いました。";
	
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
	
// ダウンロード処理
} else if($mode == "down") {

	// データ取得
	
	$target_id = @$_REQUEST["target_id"];
	
	$sql = "select";
		$sql .= " file_name";
		$sql .= ", file_path";
	$sql .= " from";
		$sql .= " fplus_medical_accident_report_files";
	$sql .= " where";
		$sql .= " file_id = " . $target_id;
	
	$sel = @select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$result = pg_fetch_all($sel);
	
	$db_file_name = mb_convert_encoding($result[0]["file_name"], 'SJIS', 'auto');
	$db_file_path = $result[0]["file_path"];

	ob_clean();
	//flush();

	// ヘッダ
	header("Content-Type: application/octet-stream");

	// ダイアログボックスに表示するファイル名
	header("Content-Disposition: attachment; filename=$db_file_name");
	
	// 対象ファイルを出力する。

	readfile($db_file_path);
	
	exit;
}

//====================================================================
// HTMLヘッダ
//====================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 関連ファイル一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>

<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script type="text/javascript">
	function reg_func() {
		document.frm.mode.value = "reg";
		document.frm.target = "_self";
		document.frm.submit();
	}
	
	function udt_func() {
		document.frm.mode.value = "udt";
		document.frm.target = "_self";
		document.frm.submit();
	}
	
	function del_func(target) {
		if (confirm("選択した行を削除します。よろしいですか？") == true) {
			document.frm.mode.value = "del";
			document.frm.target_id.value = target;
			document.frm.target = "_self";
			document.frm.submit();
		}
	}
	
	function download_func(target) {
		document.frm.mode.value = "down";
		document.frm.target_id.value = target;
		document.frm.target = "_self";
		document.frm.submit();
	}
</script>

</head>
<body style="margin:0" onload="load();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#E6B3D4">
		<td class="spacing">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>関連ファイル一覧</b></font>
		</td>
		<td width="32" align="center">
			<a href="javascript:void(0);" onclick="window.close();">
			<img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';">
			</a>
		</td>
	</tr>
</table>

<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_medical_accident_report_files.php?session=<?=$session?>&medical_accident_id=<?=$medical_accident_id?>" method="post" enctype="multipart/form-data">

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="medical_accident_id" value="<?=$medical_accident_id?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="target_id" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table style="width:100%;">
				<tr>
					<td style="width:2%;"></td>
					<td style="width:96%;" align="center">
						<?
						if ($message != "") {
						?>
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<font size="3" color="red" face="ＭＳ Ｐゴシック, Osaka" class="j12">
										<?= $message ?>
										</font>
									</td>
								</tr>
							</table>
							<img src="img/spacer.gif" height="3">
						<?
						}
						?>
						<table border="0" cellspacing="0" cellpadding="0" class="list1">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" class="list1">
										<tr>
											<td align="center" width="80" bgcolor="#feeae9" style="padding: 2px">
												<?=$font?>ファイル番号</font>
											</td>
											<td bgcolor="#fff8f7" style="padding: 2px">
												<?=$font?>
												<input type="text" name="reg_file_no" value="<?= $reg_file_no ?>" style="width: 150px">
												</font>
											</td>
											<td align="right" style="border-style: none; padding: 0px; padding-left: 3px"></td>
											<td align="center" width="80" bgcolor="#feeae9" style="padding: 2px">
												<?=$font?>ファイル</font>
											</td>
											<td width="1" bgcolor="#fff8f7" style="padding: 2px">
												<?=$font?>
												<input type="file" name="reg_file" value="" size="40">
												</font>
											</td>
										</tr>
									</table>
									<img src="img/spacer.gif" height="3">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list1">
										<tr>
											<td align="right" style="border-style: none; padding: 0px">
												<input type="button" value="登録" style="width: 60px" onclick="reg_func()">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td style="width:2%;"></td>
				</tr>
				<tr>
					<td style="width:2%;"></td>
					<td style="width:96%;" align="center">
						<?
						$sql = "select";
							$sql .= " file_id";
							$sql .= ", file_no";
							$sql .= ", file_name";
							$sql .= ", file_path";
						$sql .= " from";
							$sql .= " fplus_medical_accident_report_files";
						$sql .= " where";
							$sql .= " del_flg <> 't'";
							$sql .= " and medical_accident_id = " . $medical_accident_id;
						$sql .= " order by";
							$sql .= " file_no";
						
						$sel = @select_from_table($con, $sql, "", $fname);
						if ($sel == 0) {
							pg_close($con);
							echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
							echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
							exit;
						}
						$result = pg_fetch_all($sel);
						
						// 取得できない場合
						if ($result == false) {
							echo $font . "データが存在しません</font>";
						} else {
						?>
							<input type="hidden" name="data_cnt" value="<?= count($result) ?>">
							<table border="0" cellspacing="0" cellpadding="0" class="list1">
								<tr>
									<td width="80" align="center" bgcolor="#ffdfff" style="padding: 2px">
										<?=$font?>ファイル番号</font>
									</td>
									<td width="400" align="center" bgcolor="#ffdfff" style="padding: 2px">
										<?=$font?>ファイル名</font>
									</td>
									<td width="50" align="center" bgcolor="#ffdfff" style="padding: 2px">
										<?=$font?>削除</font>
									</td>
								</tr>
								<?
								for ($i = 0; $i < count($result); $i++) {
									$db_file_id = $result[$i]["file_id"];
									$db_file_no = $result[$i]["file_no"];
									$db_file_name = $result[$i]["file_name"];
									$db_file_path = $result[$i]["file_path"];
									?>
									<tr>
										<td style="padding: 2px">
											<?=$font?>
											<input type="text" name="af_file_no_<?= $i ?>" value="<?= $db_file_no ?>" style="width: 150px">
											</font>
											<input type="hidden" name="bf_file_no_<?= $i ?>" value="<?= $db_file_no ?>">
											<input type="hidden" name="file_id_<?= $i ?>" value="<?= $db_file_id ?>">
										</td>
										<td style="padding: 2px; padding-left: 4px">
											<a href="javascript:void(0)" onclick="download_func('<?= $db_file_id ?>')">
											<?=$font?>
											<?= $db_file_name ?>
											</font>
											</a>
										</td>
										<td style="padding: 2px">
											<?=$font?>
											<input type="button" value="削除" style="width: 50px" onclick="del_func('<?= $db_file_id ?>')">
											</font>
										</td>
									</tr>
								<?
								}
								?>
								<tr>
									<td align="right" colspan="3" style="padding: 2px; border-style: none">
										<?=$font?>
										<input type="button" value="更新" style="width: 50px" onclick="udt_func()">
										</font>
									</td>
								</tr>
							</table>
						<?
						}
						?>
					</td>
					<td style="width:2%;"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>

</body>
</html>
