<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜出勤状況確認</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("show_date_navigation_month.ini");
require("show_work_admin_status.ini");
require("show_timecard_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 年月の設定
if ($date == "") {$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));}
$year = date("Y", $date);
$month = date("m", $date);
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// DBコネクション作成
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function displayList() {
	var minutes = document.mainform.minutes.value;
	if (minutes == '') {
		alert('分数を入力してください。');
		return false;
	}
	if (!minutes.match(/^\d{1,2}$/)) {
		alert('分数は数字で入力してください。');
		return false;
	}
	return true;
}

function changeDate(dt) {
	location.href = 'work_admin_status.php?session=<? echo($session); ?>&minutes=<? echo($minutes); ?>&srch_flg=1&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
//サブメニュー表示
show_work_admin_submenu($session,$fname,$arr_option);
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="work_admin_status.php" method="post" onsubmit="return displayList();">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
出勤予定時刻の<input type="text" name="minutes" value="<? echo($minutes); ?>" size="3" maxlength="2" style="ime-mode:disabled;">分前までに出勤していない職員を
<input type="submit" value="表示">
</font></td>
</tr>
</table>
<?
if ($srch_flg == "1") {
	echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo("<a href=\"work_admin_status.php?session=$session&minutes=$minutes&srch_flg=1&date=$last_month\">&lt;前月</a>&nbsp;<select onchange=\"changeDate(this.options[this.selectedIndex].value);\">");
	show_date_options_m($date);
	echo("</select>&nbsp;<a href=\"work_admin_status.php?session=$session&minutes=$minutes&srch_flg=1&date=$next_month\">翌月&gt;</a>\n");
	echo("</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

	show_employee_list($con, $minutes, $year, $month, $session, $fname);
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
