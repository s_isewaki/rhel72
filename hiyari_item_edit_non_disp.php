<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");

//ini_set("display_errors","1");
//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//項目名の取得
//==============================
$easy_item_name = get_easy_item_name($con,$fname,$grp_code,$easy_item_code);

//==============================
//部署の取得
//==============================
$arr_class = split("-", $element_id);
$classes_name = get_classes_name($con, $fname, $arr_class);


if($postback_mode == "update")
{
	// トランザクションの開始
	pg_query($con, "begin transaction");

	set_item_element_no_disp($con, $fname, $grp_code, $easy_item_code, $arr_class, $non_disp_val);

	// トランザクションをコミット
	pg_query($con, "commit");
	
?>
	<script language='javascript'>

		if(window.opener && !window.opener.closed && window.opener.add_updated_class)
		{
			window.opener.add_updated_class('<?echo($element_id);?>');
		}
		window.close();
	</script>
<?	
}
//==============================
//選択項目情報の取得
//==============================
$arr_class = get_non_disp_item_override($con, $fname, $grp_code, $easy_item_code, $arr_class);
$edit_item_list = get_easy_item_list($con,$fname,$grp_code,$easy_item_code, $arr_class);




//========================================================================================================================
//HTML出力
//========================================================================================================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 管理項目変更</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">


//ＤＢ更新処理
function update_data()
{
	document.form1.postback_mode.value = "update";
	document.form1.submit();
}



</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">

<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="grp_code" value="<?=$grp_code?>">
<input type="hidden" name="easy_item_code" value="<?=$easy_item_code?>">
<input type="hidden" name="postback_mode" value="">



<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<?
	$PAGE_TITLE = "部署別非表示設定";
	show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- メイン START -->
<table width="600" border="0" cellspacing="0" cellpadding="5">
<tr>
<td>


<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- 上部更新ボタン等 START-->
<table width="470" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left" nowrap>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b><?=h($easy_item_name)?></b></font>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">(&nbsp;<?=h($classes_name);?>&nbsp;)</font>
</td>
<td align="right">
	<input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 上部更新ボタン等 START-->



<!-- 一覧 START-->
<table width="470" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
	<td width="400" align="center" bgcolor="#DFFFDC">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示内容</font>
	</td>
	<td width="70" align="center" bgcolor="#DFFFDC">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示</font>
	</td>
</tr>

<?
foreach($edit_item_list as $edit_item)
{	

	$easy_code        = $edit_item["easy_code"];
	$easy_name        = $edit_item["easy_name"];
	$disp_flg         = $edit_item["disp_flg"] == 't';
	
	//非表示欄のチェック状態のHTML
	$no_disp_checked_html = "";
	if(!$disp_flg)
	{
		$no_disp_checked_html = "checked";
	}

?>
<tr height="22">

<td align="left" bgcolor="#FFFFFF" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($easy_name)?></font>
</td>

<td align="center" bgcolor="#FFFFFF" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="non_disp_val[]" value="<?=$easy_code?>" <?=$no_disp_checked_html?>>
</font>
</td>
</tr>
<?
}
?>
</table>
<!-- 一覧 END -->


<!-- 下部更新ボタン等 START-->
<table width="470" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right">
	<input type="button" value="更新" onclick="update_data();">
</td>
</tr>
</table>
<!-- 下部更新ボタン等 START-->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->


</td>
</tr>
</table>
<!-- メイン END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</form>
</body>
</html>
<?






//========================================================================================================================
//内部関数
//========================================================================================================================

/**
 * 当画面の一覧表示データを取得します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return array 当画面の一覧表示データの配列。
 */
function get_easy_item_list($con, $fname, $grp_code, $easy_item_code, $arr_class)
{

	$class_id     = $arr_class["class_id"];
	$attribute_id = $arr_class["attribute_id"];
	$dept_id      = $arr_class["dept_id"];
	$room_id      = $arr_class["room_id"];

	$sql  =" select";
	$sql .=" m.easy_code,";
	$sql .=" m.easy_name,";
	$sql .=" case when e_disp.grp_code isnull then true else false end as disp_flg";
	$sql .=" from ";
	$sql .=" (";
	$sql .=" select * ";
	$sql .=" from inci_report_materials";
	$sql .=" where grp_code = $grp_code and easy_item_code = $easy_item_code";
	$sql .=" ) m";


	$sql .=" left join";
	$sql .=" (";


	$sql .=" select * from inci_easyinput_item_element_no_disp ";
	$sql .="	where";

	if($class_id != "")
	{
		$sql .= " class_id = $class_id and";
	}
	else
	{
		$sql .= " class_id is null and";
	}

	if($attribute_id != "")
	{
		$sql .= " attribute_id = $attribute_id and";
	}
	else
	{
		$sql .= " attribute_id is null and ";
	}

	if($dept_id != "")
	{
		$sql .= " dept_id = $dept_id and ";
	}
	else
	{
		$sql .= " dept_id is null and ";
	}

	if($room_id != "")
	{
		$sql .= " room_id = $room_id";
	}
	else
	{
		$sql .= " room_id is null ";
	}


	$sql.=" ) e_disp";
	$sql.=" on  e_disp.grp_code = m.grp_code";
	$sql.=" and e_disp.easy_item_code = m.easy_item_code";
	$sql.=" and e_disp.easy_code = m.easy_code";

    $cond .= " order by easy_num";

	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$arr = array();
	while($row = pg_fetch_array($sel))
	{
		$arr[] = array("easy_code" => $row["easy_code"], "easy_name" => $row["easy_name"], "disp_flg" => $row["disp_flg"]);
	}

	return $arr;
}

/**
 * 当画面の一覧表示データを取得します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @param array $arr_class 部署配列
 * @return array 当画面の一覧表示データの配列。
 */
function get_non_disp_item_override($con, $fname, $grp_code, $easy_item_code, $arr_class)
{
	$sql   = "select * from inci_easyinput_item_element_no_disp_override ";
	$cond  = "where grp_code = $grp_code and easy_item_code = $easy_item_code ";

	$cond .= "and (";
	$class_cnt = count($arr_class);
	switch($class_cnt)
	{
		case "4":
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id = $arr_class[3]) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id isnull) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id isnull and room_id isnull) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull) ";
			break;
		case "3":
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id isnull) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id isnull and room_id isnull) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull) ";
			break;
		case "2":
			$cond .= "(class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id isnull and room_id isnull) or ";
			$cond .= "(class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull) ";
			break;
		case "1":
			$cond .= "(class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull) ";
			break;
		default:
			break;
	}

	$cond .= ") ";
	$cond .= "order by class_id, attribute_id, dept_id, room_id ";

	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class = pg_fetch_result($sel, 0, "class_id");
	$attribute = pg_fetch_result($sel, 0, "attribute_id");
	$dept = pg_fetch_result($sel, 0, "dept_id");
	$room = pg_fetch_result($sel, 0, "room_id");

	$arr = array("class_id" => $class, "attribute_id" => $attribute, "dept_id" => $dept, "room_id" => $room);

	return $arr;
}


/**
 * 項目名を取得します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @return integer 新しい選択項目コード。
 */
function get_easy_item_name($con,$fname,$grp_code,$easy_item_code)
{
	$sql="";
	$sql.=" select easy_item_name from inci_easyinput_item_mst";
	$sql.=" where grp_code = $grp_code and easy_item_code = $easy_item_code";

	$sel = select_from_table($con,$sql,"",$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	return pg_fetch_result($sel,0,"easy_item_name");
}

/**
 * 非表示項目を登録します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @param array $arr_class 部署配列
 * @param array $non_disp_val 非表示項目
 */
function set_item_element_no_disp($con, $fname, $grp_code, $easy_item_code, $arr_class, $non_disp_val)
{

	$sql = "delete from inci_easyinput_item_element_no_disp";
	$cond = "where grp_code = $grp_code and easy_item_code = $easy_item_code ";

	$class_cnt = count($arr_class);
	switch($class_cnt)
	{
		case "4":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id = $arr_class[3] ";
			break;
		case "3":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id isnull ";
			break;
		case "2":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id isnull and room_id isnull ";
			break;
		case "1":
			$cond .= "and class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull ";
			break;
		default:
			break;
	}


	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class_id     = $arr_class[0] == ""? null : $arr_class[0];
	$attribute_id = $arr_class[1] == ""? null : $arr_class[1];
	$dept_id      = $arr_class[2] == ""? null : $arr_class[2];
	$room_id      = $arr_class[3] == ""? null : $arr_class[3];

	foreach($non_disp_val as $val) {

		$sql = "insert into inci_easyinput_item_element_no_disp (class_id, attribute_id, dept_id, room_id, grp_code, easy_item_code, easy_code) values (";
		$content = array($class_id, $attribute_id, $dept_id, $room_id, $grp_code, $easy_item_code, $val);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_exec($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	set_item_element_no_disp_override($con, $fname, $grp_code, $easy_item_code, $arr_class);
}


/**
 * 部署別設定済みテーブルに登録します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $grp_code グループコード
 * @param integer $easy_item_code 項目コード
 * @param array $arr_class 部署配列
 */
function set_item_element_no_disp_override($con, $fname, $grp_code, $easy_item_code, $arr_class)
{

	$sql = "select count(*) as cnt from inci_easyinput_item_element_no_disp_override";
	$cond = "where grp_code = $grp_code and easy_item_code = $easy_item_code ";
	$class_cnt = count($arr_class);
	switch($class_cnt)
	{
		case "4":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id = $arr_class[3] ";
			break;
		case "3":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id = $arr_class[2] and room_id isnull ";
			break;
		case "2":
			$cond .= "and class_id = $arr_class[0] and attribute_id = $arr_class[1] and dept_id isnull and room_id isnull ";
			break;
		case "1":
			$cond .= "and class_id = $arr_class[0] and attribute_id isnull and dept_id isnull and room_id isnull ";
			break;
		default:
			break;
	}


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$cnt = pg_fetch_result($sel, 0, "cnt");

	if($cnt == 0)
	{
		$class_id     = $arr_class[0];
		$attribute_id = $arr_class[1];
		$dept_id      = $arr_class[2];
		$room_id      = $arr_class[3];
	
		$sql = "insert into inci_easyinput_item_element_no_disp_override(class_id, attribute_id, dept_id, room_id, grp_code, easy_item_code) values (";
		$content = array($class_id, $attribute_id, $dept_id, $room_id, $grp_code, $easy_item_code);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_exec($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}


/**
 * 部署名を取得します。
 * 
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param array $arr_class 部署配列
 * @return array 部署名
 */
function get_classes_name($con, $fname, $arr_class)
{
	$classes_name = "";
	if($arr_class[0] != "")
	{
		$classes_name .= get_class_nm($con,$arr_class[0],$fname);
	}

	if($arr_class[1] != "")
	{
		$classes_name .= " > ";
		$classes_name .= get_atrb_nm($con,$arr_class[1],$fname);
	}

	if($arr_class[2] != "")
	{
		$classes_name .= " > ";
		$classes_name .= get_dept_nm($con,$arr_class[2],$fname);
	}

	if($arr_class[3] != "")
	{
		$classes_name .= " > ";
		$classes_name .= get_room_nm($con,$arr_class[3],$fname);
	}
	return $classes_name;
}
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>