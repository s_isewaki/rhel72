<?php

/**
 * Cmx.php
 * CoMedix Initiarize Module
 */
ob_start();
set_include_path(
        get_include_path()
        . PATH_SEPARATOR . dirname(__FILE__)
        . PATH_SEPARATOR . dirname(__FILE__) . '/PEAR'
        . PATH_SEPARATOR . dirname(__FILE__) . '/libs'
        . PATH_SEPARATOR . dirname(__FILE__) . '/class'
);
require_once('PEAR.php');
require('conf/conf.inf');  // require_once ���Ѥ��ʤ��褦����
require_once('Cmx/Core/Utils.php');

//------------------------------
// MercifulPolluter for RHEL7(register_globals�б�)
//------------------------------
if (PHP_VERSION >= '5.4.0') {
    //require_once(dirname(__FILE__) . '/composer/composer.php');
}

$server = $_SERVER['HTTP_X_FORWARDED_HOST'] ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER["SERVER_NAME"];
$scheme = $_SERVER['SERVER_PORT'] === '443'  ? "https://" : "http://";

define('CMX_BASE_DIR', dirname(__FILE__));
define('CMX_BASE_URL', "{$scheme}{$server}/" . basename(dirname(__FILE__)));
define('CMX_DB_DSN', "pgsql://{$USER4DB}:{$PASSWD4DB}@{$IP4DBSERVER}:{$PORT4DB}/{$NAME4DB}");

/******************************************************************************
 * �ƥ�ץ졼�Ƚ����Ѥ���� ��ǥ����ƥå��ɲ�
 *******************************************************************************/
define('WORKFLOW_KINTAI_HOME', dirname(__FILE__).'/kintai'); // HOME
define('WORKFLOW_INDIVIDUAL_HOME', WORKFLOW_KINTAI_HOME.'/individual'); // ���̥ۡ���
define('WORKFLOW_KINTAI_DIR', WORKFLOW_INDIVIDUAL_HOME.'/workflow');
define('KINTAI_VIEW_DIR', WORKFLOW_INDIVIDUAL_HOME.'/view');

define('WORKFLOW_OVTM_MODULE', "ovtm_workflow.php"); //���ֳ�������
define('WORKFLOW_HOL_MODULE', "hol_workflow.php"); //�����Ϥ���
define('WORKFLOW_HOL2_MODULE', "hol2_workflow.php"); //�ٲ˿�����
define('WORKFLOW_TRVL_MODULE', "travel_workflow.php"); // ι�Կ�����
define('WORKFLOW_LATE_MODULE', "late_workflow.php"); //�ٹ�������
define('WORKFLOW_LATE2_MODULE', "late2_workflow.php"); //�ٹ�������

define('WORKFLOW_OVTM_CLASS', "OvtmWorkflow"); //���ֳ�������
define('WORKFLOW_HOL_CLASS', "HolWorkflow"); //�����Ϥ���
define('WORKFLOW_HOL2_CLASS', "Hol2Workflow"); //�ٲ˿�����
define('WORKFLOW_TRVL_CLASS', "TravelWorkflow"); // ι�Կ�����
define('WORKFLOW_LATE_CLASS', "LateWorkflow"); //�ٹ�������
define('WORKFLOW_LATE2_CLASS', "Late2Workflow"); //�ٹ��������ͭ�뿶����

define('KINTAI_OVTMLIST_CHILD_VIEW', 'kintai_ovtmlist_child.php');

ob_end_clean();
