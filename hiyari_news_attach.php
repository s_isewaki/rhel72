<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "ファイル添付");
$smarty->assign("session", $session);

$smarty->assign("upload_max_filesize", ini_get("upload_max_filesize"));

if ($design_mode == 1){
    $smarty->display("hiyari_news_attach1.tpl");
}
else{
    $smarty->display("hiyari_news_attach2.tpl");
}

