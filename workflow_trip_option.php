<?php
// 出張旅費
// オプション設定
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

// オプション設定の取得
$sql = "SELECT * FROM trip_option";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
$row = pg_fetch_assoc($sel);

// 前渡しチケットメールの取得
$sql = "SELECT * FROM trip_preticket_mail LEFT JOIN empmst USING (emp_id)";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
$preticket_mail = pg_num_rows($sel) > 0 ? pg_fetch_all($sel) : array();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | オプション設定</title>
<script type="text/javascript" src="js/yui_2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/animation/animation-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/calendar/calendar-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/DDTableRow.js"></script>
<script type="text/javascript" src="js/workflow/trip_option.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
<link type="text/css" rel="stylesheet" href="js/yui_2.8.1/build/calendar/assets/calendar.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>

<form id="mstform" action="workflow_trip_option_submit.php" method="post">

<div class="button">
<button type="submit">更新</button>
</div>

<table id="form">
<tr>
	<td>長期日帰出張の日当</td>
	<td>日帰日当金額に <input type="number" name="long_daytrip_percentage" size="5" value="<?eh(!empty($row) ? $row["long_daytrip_percentage"] : "85");?>" style="ime-mode:disabled;" /> ％を乗じた金額</td>
</tr>
<tr>
	<td>長期宿泊出張の日当</td>
	<td>宿泊日当金額に <input type="number" name="long_stay_percentage" size="5" value="<?eh(!empty($row) ? $row["long_stay_percentage"] : "85");?>" style="ime-mode:disabled;" /> ％を乗じた金額</td>
</tr>
<tr>
	<td>長期宿泊出張の宿泊料</td>
	<td>宿泊料に <input type="number" name="long_hotel_percentage" size="5" value="<?eh(!empty($row) ? $row["long_hotel_percentage"] : "60");?>" style="ime-mode:disabled;" /> ％を乗じた金額</td>
</tr>
<tr>
	<td>ガソリン購入額<br />(1リットル当り)</td>
	<td>
		<input type="number" name="oil_price" size="5" value="<?eh(!empty($row) ? $row["oil_price"] : "");?>" style="ime-mode:disabled;" /> 円（最新）<br />
		<input type="number" name="oil_price2" size="5" value="<?eh(!empty($row) ? $row["oil_price2"] : "");?>" style="ime-mode:disabled;" /> 円
		（<input type="text" name="oil_price2_date" value="<?eh(!empty($row) ? str_replace("-","/",$row["oil_price2_date"]) : "");?>" size="12" readonly onclick="YAHOO.cal.cal1.show();" />まで）<div id="cal_oil2"></div><br />
		<input type="number" name="oil_price3" size="5" value="<?eh(!empty($row) ? $row["oil_price3"] : "");?>" style="ime-mode:disabled;" /> 円
		（<input type="text" name="oil_price3_date" value="<?eh(!empty($row) ? str_replace("-","/",$row["oil_price3_date"]) : "");?>" size="12" readonly onclick="YAHOO.cal.cal2.show();" />まで）<div id="cal_oil3"></div><br />
	</td>
</tr>
<tr>
	<td>燃料費1リットル当りの金額</td>
	<td>購入額に <input type="number" name="oil_percentage" size="5" value="<?eh(!empty($row) ? $row["oil_percentage"] : "5");?>" style="ime-mode:disabled;" /> ％加算した金額</td>
</tr>
<tr>
	<td>車両区分による走行距離：普通車</td>
	<td>1リットル当り <input type="number" name="normal_car_distance" size="5" value="<?eh(!empty($row) ? $row["normal_car_distance"] : "8");?>" style="ime-mode:disabled;" /> km</td>
</tr>
<tr>
	<td>車両区分による走行距離：軽自動車</td>
	<td>1リットル当り <input type="number" name="small_car_distance" size="5" value="<?eh(!empty($row) ? $row["small_car_distance"] : "13");?>" style="ime-mode:disabled;" /> km</td>
</tr>
<tr>
	<td>前渡しチケットメールの宛先<br><br><button type="button" onclick="openEmployeeList('<?=$session?>');">職員選択</button></td>
	<td id="preticket_mail">
<?foreach ($preticket_mail as $mail) {?>
		<div id="<?=$mail["emp_id"]?>">
			<input type="hidden" name="preticket_mail[]" value="<?=$mail["emp_id"]?>">
			<div style="width:150px;float:left"><?=$mail["emp_lt_nm"]?> <?=$mail["emp_ft_nm"]?></div>
			<button type="button" onclick="remove_row(this);">削除</button>
			<span style="clear:both;"></span>
		</div>
<?}?>
	</td>
</tr>

</table>

<div class="button">
<button type="submit">更新</button>
</div>

<input type="hidden" name="session" value="<?=$session?>" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);