<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplusadm_lib_register.php" method="post">

<input type="hidden" name="wkfw_title" value="<? echo($wkfw_title); ?>">
<input type="hidden" name="wkfw_content" value="<?
$wkfw_content = eregi_replace("_(textarea)", "/\\1", $wkfw_content);
$wkfw_content = eregi_replace("_(script)", "/\\1", $wkfw_content);
echo(h($wkfw_content)); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 79, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 本文をファイルへ格納
$ext = ".php";
$savefilename = "fplus/workflow/tmp/{$session}_t{$ext}";

// 内容書き込み
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);

// 入力チェック
if ($wkfw_title == "") {
	echo("<script type=\"text/javascript\">alert('テンプレート名称を入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strlen($wkfw_content) == 0) {
	echo("<script type=\"text/javascript\">alert('テンプレートを指定してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strpos($wkfw_content, "<? // XML") === false) {
	echo("<script type=\"text/javascript\">alert('XML作成用コードがありません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

if (strpos($wkfw_content, "dump_mem") === false) {
	echo("<script type=\"text/javascript\">alert('テンプレートが最後まで設定されていません。確認してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 内容チェック
$content_upper = strtoupper($wkfw_content);
$sqlchkflg = true;

$chkarray = array("INSERT", "UPDATE", "DELETE", "CONNECT2DB", "SELECT_FROM_TABLE", "PG_CONNECT", "PG_EXEC", "<FORM");
foreach($chkarray as $chkstr){
	if(!(strpos($content_upper, $chkstr) === false)){
		$sqlchkflg = false;
		break;
	}
}

if($sqlchkflg == false){
	echo("<script language=\"javascript\">alert('指定できない文字列があります($chkstr)。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 'エスケープ
$wkfw_title = pg_escape_string($wkfw_title);
$wkfw_content = pg_escape_string($wkfw_content);

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ワークフローライブラリIDを採番
$sql = "select max(wkfwlib_id) from fpluswkfwlib";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wkfwlib_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

$now = date("YmdHis");

// ワークフローライブラリ情報を登録
$sql = "insert into fpluswkfwlib (wkfwlib_id, wkfwlib_name, wkfwlib_content, wkfwlib_filename, wkfwlib_cr_date, wkfwlib_up_date) values (";
$content = array($wkfwlib_id, $wkfw_title, $wkfw_content, $wkfwlib_filename , $now, $now);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href='fplusadm_lib_list.php?session=$session';</script>");
?>
</body>
