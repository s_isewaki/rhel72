<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | ユーザ一覧</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_class_name.ini");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");
require_once("get_menu_label.ini");

$fname = $PHP_SELF;

// 機能名を取得
$gname = $_GET['name'];

// 表示条件のデフォルトは「利用中の職員を表示」
if ($view == "") {$view = "1";}

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 並び順のデフォルトは職員IDの昇順
if ($emp_o == "") {$emp_o = "1";}

// データベースに接続
$con = connect2db($fname);
// 組織階層情報を取得
$arr_class_name = get_class_name_array($con, $fname);

if ($group_id != "") {
	// 表示対象の権限グループ情報を取得
	$sql = "select group_nm from authgroup";
	$cond = "where group_id = $group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$group_nm = pg_fetch_result($sel, 0, "group_nm");
}

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 初期表示時はログインユーザの所属と職種をデフォルトとする
$is_initial_view = ($class_cond == "");
if ($is_initial_view) {
	$class_cond = "0";
	$atrb_cond = "0";
	$dept_cond = "0";
	$room_cond = "0";
	$job_cond = "0";
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu1 = pg_fetch_result($sel, 0, "menu1");
$intra_menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");

// 社会福祉法人の場合「当直・外来分担表」を「当直分担表」とする
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$intra_menu2_4 = str_replace("・外来", "", $intra_menu2_4);
}

$report_menu_label = get_report_menu_label($con, $fname);
$shift_menu_label = get_shift_menu_label($con, $fname);

$dsp_nm = array (
	"webml" => "ウェブメール（ユーザ機能）",
	"webmladm" => "ウェブメール（管理者機能）",
	"schd" => "スケジュール（ユーザ機能）",
	"schdplc" => "スケジュール（管理者機能）",
	"pjt" => "委員会・WG（ユーザ機能）",
	"pjtadm" => "委員会・WG（管理者機能）",
	"newsuser" => "お知らせ・回覧板（ユーザ機能）",
	"news" => "お知らせ・回覧板（管理者機能）",
	"work" => "タスク",
	"phone" => "伝言メモ",
	"resv" => "設備予約（ユーザ機能）",
	"fcl" => "設備予約（管理者機能）",
	"attdcd" => "出勤表／勤務管理（ユーザ機能）",
	"wkadm" => "出勤表／勤務管理（管理者機能）",
	"aprv" => "決裁・申請／ワークフロー（ユーザ機能）",
	"wkflw" => "決裁・申請／ワークフロー（管理者機能）",
	"conf" => "ネットカンファレンス",
	"bbs" => "掲示板･電子会議室（ユーザ機能）",
	"bbsadm" => "掲示板･電子会議室（管理者機能）",
	"qa" => "Q&A（ユーザ機能）",
	"qaadm" => "Q&A（管理者機能）",
	"lib" => "文書管理（ユーザ機能）",
	"libadm" => "文書管理（管理者機能）",
	"adbk" => "アドレス帳（ユーザ機能）",
	"adbkadm" => "アドレス帳（管理者機能）",
	"ext" => "内線電話帳（ユーザ機能）",
	"extadm" => "内線電話帳（管理者機能）",
	"link" => "リンクライブラリ（ユーザ機能）",
	"linkadm" => "リンクライブラリ（管理者機能）",
	"pass" => "パスワード・本人情報",
	"cmt" => "コミュニティサイト（ユーザ機能）",
	"cmtadm" => "コミュニティサイト（管理者機能）",
	"memo" => "備忘録",
	"cauth" => "端末認証",
	"tmgd" => $_label_by_profile["EVENT_REG"][$profile_type]."（管理者機能）",
	"reg" => "職員登録（管理者機能）",
	"empif" => "職員登録（職員参照）",
	"hspprf" => "マスターメンテナンス（組織プロフィール）",
	"dept" => "マスターメンテナンス（組織）",
	"job" => "マスターメンテナンス（職種）",
	"status" => "マスターメンテナンス（役職）",
	"attditem" => "マスターメンテナンス（カレンダー）",
	"config" => "環境設定（管理者機能）",
	"lcs" => "ライセンス管理（管理者機能）",
	"accesslog" => "アクセスログ",
	"ptreg" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（".$_label_by_profile["PATIENT_REG"][$profile_type]."）",
	"ptif" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（".$_label_by_profile["PATIENT_INFO"][$profile_type]."）",
	"outreg" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（".$_label_by_profile["OUT_REG"][$profile_type]."）",
	"inout" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（".$_label_by_profile["IN_OUT"][$profile_type]."）",
	"disreg" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（プロブレムリスト）",
	"dishist" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（プロブレム）",
	"ptadm" => $_label_by_profile["PATIENT_MANAGE"][$profile_type]."（管理者権限）",
	"search" => "検索ちゃん",
	"amb" => "看護支援（ユーザ機能）",
	"ambadm" => "看護支援（管理者機能）",
	"ward" => $_label_by_profile["BED_MANAGE"][$profile_type]."（ユーザ機能）",
	"ward_reg" => $_label_by_profile["BED_MANAGE"][$profile_type]."（管理者機能）",
	"entity" => $_label_by_profile["SECTION"][$profile_type],
	"nlcs" => "看護観察記録（ユーザ機能）",
	"nlcsadm" => "看護観察記録（管理者機能）",
	"inci" => "ファントルくん（ユーザ機能）",
	"rm" => "ファントルくん（管理者機能）",
	"fplus" => "ファントルくん＋（ユーザ機能）",
	"fplusadm" => "ファントルくん＋（管理者機能）",
	"manabu" => "バリテス（ユーザ機能）",
	"manabuadm" => "バリテス（管理者機能）",
	"med" => $report_menu_label."（ユーザ機能）",
	"medadm" => $report_menu_label."（管理者機能）",
	"kview" => "カルテビューワー（ユーザ機能）",
	"kviewadm" => "カルテビューワー（管理者機能）",
	"biz" => "経営支援",
	"shift_staff" => $shift_menu_label."（登録職員）",
	"shiftadm" => $shift_menu_label."（管理者機能）",
	"shift2_staff" => "勤務表（登録職員）",
	"shift2adm" => "勤務表（管理者機能）",
	"cas" => get_cas_title_name()."（ユーザ機能）",
	"casadm" => get_cas_title_name()."（管理者機能）",
	"jinji" => "人事管理（ユーザ機能）",
	"jinjiadm" => "人事管理（管理者機能）",
	"ladder" => "クリニカルラダー（ユーザ機能）",
	"ladderadm" => "クリニカルラダー（管理者機能）",
	"jnl" => "日報・月報（ユーザ機能）",
	"jnladm" => "日報・月報（管理者機能）",
	"intra" => "イントラネット（ユーザ機能）",
	"ymstat" => "イントラネット（月間・年間".$intra_menu1."参照）",
	"intram" => "イントラネット（イントラメニュー）",
	"stat" => "イントラネット（".$intra_menu1."）",
	"allot" => "イントラネット（".$intra_menu2_4."）",
	"life" => "イントラネット（".$intra_menu3."）",
	"ccusr1" => "スタッフ・ポートフォリオ（ユーザ機能）",
	"ccadm1" => "スタッフ・ポートフォリオ（管理者機能）"
);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setAtrbOptions() {
	clearOptions(document.list.atrb_cond);
	addOption(document.list.atrb_cond, '0', 'すべて');
	var class_id = document.list.class_cond.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
	if (class_id == '<?php echo $row["class_id"]; ?>') {
		addOption(document.list.atrb_cond, '<?php echo $row["atrb_id"]; ?>', '<?php echo $row["atrb_nm"]; ?>');
	}
<? } ?>
	setDeptOptions();
}

function setDeptOptions() {
	clearOptions(document.list.dept_cond);
	addOption(document.list.dept_cond, '0', 'すべて');
	var atrb_id = document.list.atrb_cond.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
	if (atrb_id == '<?php echo $row["atrb_id"]; ?>') {
		addOption(document.list.dept_cond, '<?php echo $row["dept_id"]; ?>', '<?php echo $row["dept_nm"]; ?>');
	}
<? } ?>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
	setRoomOptions();
<? } ?>
}

<? if ($arr_class_name["class_cnt"] == 4) { ?>
function setRoomOptions() {
	clearOptions(document.list.room_cond);
	addOption(document.list.room_cond, '0', 'すべて');
	var dept_id = document.list.dept_cond.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
	if (dept_id == '<?php echo $row["dept_id"]; ?>') {
		addOption(document.list.room_cond, '<?php echo $row["room_id"]; ?>', '<?php echo $row["room_nm"]; ?>');
	}
<? } ?>
}
<? } ?>

function clearOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function downloadCSV() {
	document.csv.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list th {border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">

<?
if($tool_nm == ""){
	$detail = $group_nm;
}else{
	$detail = $dsp_nm[$tool_nm];
}

?>

<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?php echo $detail; ?> | ユーザ一覧</b></font></td>

<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<div style="position: absolute; background-color: white;">
<form name="list" action="employee_auth_list_detail.php" method="post">
<table border="0" cellspacing="0" cellpadding="1">
<tr height="25" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?php echo($arr_class_name["class_nm"]); ?> <select name="class_cond" onchange="setAtrbOptions();">
<option value="0">すべて
<?
while ($row = pg_fetch_array($sel_class)) {
	echo("<option value=\"{$row["class_id"]}\"");
	if ($row["class_id"] == $class_cond) {echo(" selected");}
	echo(">{$row["class_nm"]}\n");
}
?>
</select>
<?php echo($arr_class_name["atrb_nm"]); ?> <select name="atrb_cond" onchange="setDeptOptions();">
<option value="0">すべて
<?
pg_result_seek($sel_atrb, 0);
while ($row = pg_fetch_array($sel_atrb)) {
	if ($row["class_id"] == $class_cond) {
		echo("<option value=\"{$row["atrb_id"]}\"");
		if ($row["atrb_id"] == $atrb_cond) {echo(" selected");}
		echo(">{$row["atrb_nm"]}\n");
	}
}
?>
</select>
<?php echo($arr_class_name["dept_nm"]); ?> <select name="dept_cond"<? if ($arr_class_name["class_cnt"] == 4) {echo(" onchange=\"setRoomOptions();\"");} ?>>
<option value="0">すべて
<?
pg_result_seek($sel_dept, 0);
while ($row = pg_fetch_array($sel_dept)) {
	if ($row["atrb_id"] == $atrb_cond) {
		echo("<option value=\"{$row["dept_id"]}\"");
		if ($row["dept_id"] == $dept_cond) {echo(" selected");}
		echo(">{$row["dept_nm"]}\n");
	}
}
?>
</select>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<?php echo($arr_class_name["room_nm"]); ?> <select name="room_cond">
<option value="0">すべて
<?
pg_result_seek($sel_room, 0);
while ($row = pg_fetch_array($sel_room)) {
	if ($row["dept_id"] == $dept_cond) {
		echo("<option value=\"{$row["room_id"]}\"");
		if ($row["room_id"] == $room_cond) {echo(" selected");}
		echo(">{$row["room_nm"]}\n");
	}
}
?>
</select>
<? } ?>
</font></td>
</tr>
<tr height="25" bgcolor="#f6f9ff">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職種 <select name="job_cond">
<option value="0">すべて
<?
while ($row = pg_fetch_array($sel_job)) {
	echo("<option value=\"{$row["job_id"]}\"");
	if ($row["job_id"] == $job_cond) {echo(" selected");}
	echo(">{$row["job_nm"]}\n");
}
?>
</select>
<? if($tool_nm == ""){ ?>
<input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
<? }else{ ?>
<input type="hidden" name="tool_nm" value="<?php echo $tool_nm; ?>">
<? } ?>
<input type="submit" value="検索">
</font></td>
<td><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo $session; ?>">
<input type="hidden" name="emp_o" value="<?php echo $emp_o; ?>">
<select name="view" onchange="document.list.submit();">
<option value="1"<? if ($view == "1") {echo(" selected");}?>>利用中の職員を表示
<option value="2"<? if ($view == "2") {echo(" selected");}?>>利用停止中の職員を表示
</select>
</form>
<? show_next($con, $session, $fname, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $page, $view); ?>
<!-- table border="0" cellspacing="0" cellpadding="2" class="list" -->
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職員ID
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "1") { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=1">職員名</a>
<? } else { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=2">職員名</a>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "3") { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=3">所属</a>
<? } else { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=4">所属</a>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "5") { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=5">職種</a>
<? } else { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=6">職種</a>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "7") { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=7">役職</a>
<? } else { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=8">役職</a>
<? } ?>
</font></th>
<th><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o != "9") { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=9">権限グループ</a>
<? } else { ?>
<a href="employee_auth_list_detail.php?session=<?php echo $session; ?>&class_cond=<?php echo $class_cond; ?>&atrb_cond=<?php echo $atrb_cond; ?>&dept_cond=<?php echo $dept_cond; ?>&room_cond=<?php echo $room_cond; ?>&job_cond=<?php echo $job_cond; ?>&tool_nm=<?php echo $tool_nm; ?>&group_id=<?php echo $group_id; ?>&page=<?php echo $page; ?>&emp_o=10">権限グループ</a>
<? } ?>
</font></th>
</tr>
<?
show_emp_list($con, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $page, $view);
?>
<? pg_close($con); ?>
</table>
<form name="csv" action="employee_auth_list_detail_csv.php" method="get" target="download" >
<input type="hidden" name="session" value="<?php echo $session; ?>">
<input type="hidden" name="tool_nm" value="<?php echo $tool_nm; ?>">
<input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
<input type="hidden" name="class_cond" value="<?php echo $class_cond; ?>">
<input type="hidden" name="atrb_cond" value="<?php echo $atrb_cond; ?>">
<input type="hidden" name="dept_cond" value="<?php echo $dept_cond; ?>">
<input type="hidden" name="room_cond" value="<?php echo $room_cond; ?>">
<input type="hidden" name="job_cond" value="<?php echo $job_cond; ?>">
<input type="hidden" name="emp_o" value="<?php echo $emp_o; ?>">
<input type="hidden" name="detail" value="<?php echo $detail; ?>">
<input type="hidden" name="view" value="<?php echo $view; ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<br /></div>
</body>
</html>
<?
// [姓][名][第一階層][第二階層][第三階層][第四階層][権限グループID][権限グループ名][利用停止フラグ]を取得
function show_emp_list($con, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $page, $view) {
	$limit = 20;
	$del_flg = ($view == "1") ? "f" : "t";

	//if($tool_nm == "shift_staff") {
	$sql = "select empmst.emp_personal_id,emp_lt_nm,emp_ft_nm,classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,classroom.room_nm,stmst.st_nm,jobmst.job_nm,authmst.group_id,authgroup.group_nm from empmst";
	$sql .= " left join classmst on classmst.class_id = empmst.emp_class";
	$sql .= " left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute";
	$sql .= " left join deptmst on deptmst.dept_id = empmst.emp_dept";
	$sql .= " left join classroom on classroom.room_id = empmst.emp_room";
	$sql .= " left join stmst on stmst.st_id = empmst.emp_st";
	$sql .= " left join jobmst on jobmst.job_id = empmst.emp_job";
	$sql .= " left join authmst on authmst.emp_id = empmst.emp_id";
	$sql .= " left join authgroup on authmst.group_id = authgroup.group_id";

	$cond = "where authmst.emp_del_flg = '{$del_flg}'";

	if($tool_nm == "shift_staff" || $tool_nm == "shift2_staff") {
		$cond .= " and exists(select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id)";
	}
	else if($group_id == "") {
		$cond .= " and (authmst.emp_".$tool_nm."_flg = 't' or authgroup.".$tool_nm."_flg = 't')";
	}
	else {
		$cond .= " and (authgroup.group_id='$group_id')";
	}

	// 各階層を判定
	if ($class_cond != 0) {
		$cond .= " and (empmst.emp_class=$class_cond)";
	}
	if ($atrb_cond != 0) {
		$cond .= " and (empmst.emp_attribute=$atrb_cond)";
	}
	if ($dept_cond != 0) {
		$cond .= " and (empmst.emp_dept=$dept_cond)";
	}
	if ($room_cond != 0) {
		$cond .= " and (empmst.emp_room=$room_cond)";
	}
	if ($job_cond != 0) {
		$cond .= " and emp_job = $job_cond";
	}
	if ($emp_o == "1") {
		$cond .= " order by emp_kn_lt_nm,emp_kn_ft_nm";
	} else if ($emp_o == "2") {
		$cond .= " order by emp_kn_lt_nm desc,emp_kn_ft_nm desc";
	} else if ($emp_o == "3") {
		$cond .= " order by classmst.order_no,atrbmst.order_no,deptmst.order_no,classroom.order_no";
	} else if ($emp_o == "4") {
		$cond .= " order by classmst.order_no desc,atrbmst.order_no desc,deptmst.order_no desc,classroom.order_no desc";
	} else if ($emp_o == "5") {
		$cond .= " order by job_id";
	} else if ($emp_o == "6") {
		$cond .= " order by job_id desc";
	} else if ($emp_o == "7") {
		$cond .= " order by st_id";
	} else if ($emp_o == "8") {
		$cond .= " order by st_id desc";
	} else if ($emp_o == "9") {
		$cond .= " order by group_id";
	} else {
		$cond .= " order by group_id desc";
	}

	$cond .= " limit $limit";
	if ($page > 0) {
		$offset = $page * $limit;
		$cond .= " offset $offset";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 職員表示
	while($row = pg_fetch_array($sel)){
		$emp_personal_id = $row["emp_personal_id"];
		$emp_id = $row["emp_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$tmp_position = "{$row["class_nm"]}＞{$row["atrb_nm"]}＞{$row["dept_nm"]}";
		if ($row["room_nm"] != "") {
			$tmp_position .= "＞{$row["room_nm"]}";
		}
		$job_nm = $row["job_nm"];
		$st_nm = $row["st_nm"];
		$group_nm = $row["group_nm"];

		echo("<tr>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_personal_id</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$emp_lt_nm $emp_ft_nm</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_position</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$job_nm</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$st_nm</font></td>");
		echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$group_nm</font></td>");
		echo("</tr>");
	}
}

function show_next($con, $session, $fname, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $page, $view) {
	$limit = 20;
	$del_flg = ($view == "1") ? "f" : "t";

	$sql = "select empmst.* from empmst";
	$sql .= " left join classmst on classmst.class_id = empmst.emp_class";
	$sql .= " left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute";
	$sql .= " left join deptmst on deptmst.dept_id = empmst.emp_dept";
	$sql .= " left join classroom on classroom.room_id = empmst.emp_room";
	$sql .= " left join stmst on stmst.st_id = empmst.emp_st";
	$sql .= " left join jobmst on jobmst.job_id = empmst.emp_job";
	$sql .= " left join authmst on authmst.emp_id = empmst.emp_id";
	$sql .= " left join authgroup on authmst.group_id = authgroup.group_id";
	$cond = "where authmst.emp_del_flg = '{$del_flg}'";

	if($tool_nm == "shift_staff" || $tool_nm == "shift2_staff") {
		$cond .= " and exists(select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id)";
	}
	else if($group_id == ""){
		$cond .= " and (authmst.emp_".$tool_nm."_flg = 't' or authgroup.".$tool_nm."_flg = 't')";
	}
	else{
		$cond .= " and (authgroup.group_id='$group_id')";
	}

	// 各階層を判定
	if ($class_cond != 0) {
		$cond .= " and (empmst.emp_class=$class_cond)";
	}
	if ($atrb_cond != 0) {
		$cond .= " and (empmst.emp_attribute=$atrb_cond)";
	}
	if ($dept_cond != 0) {
		$cond .= " and (empmst.emp_dept=$dept_cond)";
	}
	if ($room_cond != 0) {
		$cond .= " and (empmst.emp_room=$room_cond)";
	}
	if ($job_cond != 0) {
		$cond .= " and emp_job = $job_cond";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$emp_count = intval(pg_numrows($sel));

	$total_page = ceil($emp_count / $limit);

	if ($total_page <= 1) {
		return;
	}

	echo("<table align=\"right\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo("ページ&nbsp;");
	$t = 10;
	if ($page > 0) {
		echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=0\">|&lt;&lt;</a>&nbsp;");
		echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=" . ($page - 1) ."\">←</a>&nbsp;");
	} else {
		echo("&nbsp;");
	}
	$min = $page - 4;
	$max = $page + 4;
	if(($total_page - 1) < $max){
		$max = ($max - ($total_page - 1)) + 4;
	} else {
		$max = 4;
	}

	if(0 > $min){
		$min = (0 - $min) + 4;
	} else {
		$min = 4;
	}

	for($i = $max; $i > 0; $i--){
		if(($page - $i) >= 0){
			echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=".($page - $i)."\">".(($page - $i) + 1)."</a>&nbsp;");
		}
	}

	echo("[".($page + 1)."]&nbsp;");

	for($i = 1; $i <= $min; $i++){
		if(($page + $i) <= ($total_page - 1)){
			echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=".($page + $i)."\">".(($page + $i) + 1)."</a>&nbsp;");
		}
	}
	if ($page < ($total_page - 1)) {
		echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=".($page + 1)."\">→</a>&nbsp;");
		echo("<a href=\"$fname?session=$session&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&emp_o=$emp_o&tool_nm=$tool_nm&group_id=$group_id&view=$view&page=".($total_page - 1)."\">&gt;&gt;|</a>&nbsp;");
	} else {
		echo("&nbsp;");
	}
	echo("</font></td>\n");
	echo("</tr>\n");
	echo("</table><br />\n");
}
?>