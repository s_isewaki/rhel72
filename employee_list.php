<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録｜一覧</title>
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("show_class_name.ini");
require_once("show_emp_next.ini");
require_once("show_emp_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 職員登録権限を取得
$reg = check_authority($session, 19, $fname);

// 並び順のデフォルトは職員IDの昇順
if ($emp_o == "") {$emp_o = "1";}

// 表示条件のデフォルトは「利用中の職員を表示」
if ($view == "") {$view = "1";}

// データベースに接続
$con = connect2db($fname);

// 組織階層情報を取得
$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 初期表示時
$is_initial_view = ($class_cond == "");
if ($is_initial_view) {

/*
	// 2012/05/13 ISEWAKI_Revise
	// 初期表示時はログインユーザの所属と職種をデフォルトとする
	$sql = "select emp_class, emp_attribute, emp_dept, emp_room, emp_job from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$class_cond = pg_fetch_result($sel, 0, "emp_class");
	$atrb_cond = pg_fetch_result($sel, 0, "emp_attribute");
	$dept_cond = pg_fetch_result($sel, 0, "emp_dept");
	$room_cond = pg_fetch_result($sel, 0, "emp_room");
	$job_cond = pg_fetch_result($sel, 0, "emp_job");
*/
	// 初期表示時は「すべて」とする
	$class_cond = "0";
	$atrb_cond = "0";
	$dept_cond = "0";
	$room_cond = "0";
	$job_cond = "0";
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setAtrbOptions() {
	clearOptions(document.list.atrb_cond);
	addOption(document.list.atrb_cond, '0', 'すべて');
	var class_id = document.list.class_cond.value;
<? while ($row = pg_fetch_array($sel_atrb)) { ?>
	if (class_id == '<? echo $row["class_id"]; ?>') {
		addOption(document.list.atrb_cond, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>');
	}
<? } ?>
	setDeptOptions();
}

function setDeptOptions() {
	clearOptions(document.list.dept_cond);
	addOption(document.list.dept_cond, '0', 'すべて');
	var atrb_id = document.list.atrb_cond.value;
<? while ($row = pg_fetch_array($sel_dept)) { ?>
	if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
		addOption(document.list.dept_cond, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>');
	}
<? } ?>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
	setRoomOptions();
<? } ?>
}

<? if ($arr_class_name["class_cnt"] == 4) { ?>
function setRoomOptions() {
	clearOptions(document.list.room_cond);
	addOption(document.list.room_cond, '0', 'すべて');
	var dept_id = document.list.dept_cond.value;
<? while ($row = pg_fetch_array($sel_room)) { ?>
	if (dept_id == '<? echo $row["dept_id"]; ?>') {
		addOption(document.list.room_cond, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>');
	}
<? } ?>
}
<? } ?>

function clearOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function downloadCSV() {
	document.csv.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<? if ($reg == 1) { ?>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td align="right"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="list" action="employee_list.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="26" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($arr_class_name["class_nm"]); ?> <select name="class_cond" onchange="setAtrbOptions();">
<option value="0">すべて
<?
while ($row = pg_fetch_array($sel_class)) {
	echo("<option value=\"{$row["class_id"]}\"");
	if ($row["class_id"] == $class_cond) {echo(" selected");}
	echo(">{$row["class_nm"]}\n");
}
?>
</select>
<? echo($arr_class_name["atrb_nm"]); ?> <select name="atrb_cond" onchange="setDeptOptions();">
<option value="0">すべて
<?
pg_result_seek($sel_atrb, 0);
while ($row = pg_fetch_array($sel_atrb)) {
	if ($row["class_id"] == $class_cond) {
		echo("<option value=\"{$row["atrb_id"]}\"");
		if ($row["atrb_id"] == $atrb_cond) {echo(" selected");}
		echo(">{$row["atrb_nm"]}\n");
	}
}
?>
</select>
<? echo($arr_class_name["dept_nm"]); ?> <select name="dept_cond"<? if ($arr_class_name["class_cnt"] == 4) {echo(" onchange=\"setRoomOptions();\"");} ?>>
<option value="0">すべて
<?
pg_result_seek($sel_dept, 0);
while ($row = pg_fetch_array($sel_dept)) {
	if ($row["atrb_id"] == $atrb_cond) {
		echo("<option value=\"{$row["dept_id"]}\"");
		if ($row["dept_id"] == $dept_cond) {echo(" selected");}
		echo(">{$row["dept_nm"]}\n");
	}
}
?>
</select>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<? echo($arr_class_name["room_nm"]); ?> <select name="room_cond">
<option value="0">すべて
<?
pg_result_seek($sel_room, 0);
while ($row = pg_fetch_array($sel_room)) {
	if ($row["dept_id"] == $dept_cond) {
		echo("<option value=\"{$row["room_id"]}\"");
		if ($row["room_id"] == $room_cond) {echo(" selected");}
		echo(">{$row["room_nm"]}\n");
	}
}
?>
</select>
<? } ?>
</font></td>
<td align="right">
<select name="view"<? if (!$is_initial_view) { ?> onchange="this.form.submit();"<? } ?>>
<option value="1"<? if ($view == "1") {echo(" selected");}?>>利用中の職員を表示
<option value="2"<? if ($view == "2") {echo(" selected");}?>>利用停止中の職員を表示
</select>
</td>
</tr>
<tr height="26" bgcolor="#f6f9ff">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
職種 <select name="job_cond">
<option value="0">すべて
<?
while ($row = pg_fetch_array($sel_job)) {
	echo("<option value=\"{$row["job_id"]}\"");
	if ($row["job_id"] == $job_cond) {echo(" selected");}
	echo(">{$row["job_nm"]}\n");
}
?>
</select>
<input type="submit" value="検索">
</font></td>
</tr>
</table>
<? if (!$is_initial_view) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<? show_next($con, $arr_class_name["class_cnt"], $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $view, $emp_o, $page, $session, $fname); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" valign="top">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o == "1") { ?>
<a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&view=<? echo($view); ?>&emp_o=2&page=<? echo($page); ?>">職員ID</a>
<? } else { ?>
<a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&view=<? echo($view); ?>&emp_o=1&page=<? echo($page); ?>">職員ID</a>
<? } ?>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($emp_o == "3") { ?>
<a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&view=<? echo($view); ?>&emp_o=4&page=<? echo($page); ?>">職員氏名</a>
<? } else { ?>
<a href="employee_list.php?session=<? echo($session); ?>&class_cond=<? echo($class_cond); ?>&atrb_cond=<? echo($atrb_cond); ?>&dept_cond=<? echo($dept_cond); ?>&room_cond=<? echo($room_cond); ?>&job_cond=<? echo($job_cond); ?>&view=<? echo($view); ?>&emp_o=3&page=<? echo($page); ?>">職員氏名</a>
<? } ?>
</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属<? echo($arr_class_name["class_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務条件</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></td>
</tr>
<? show_emp_list($con, $arr_class_name["class_cnt"], $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $view, $emp_o, $page, $session, $fname); ?>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
</form>
<form name="csv" action="employee_list_csv.php" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="class_cond" value="<? echo($class_cond); ?>">
<input type="hidden" name="atrb_cond" value="<? echo($atrb_cond); ?>">
<input type="hidden" name="dept_cond" value="<? echo($dept_cond); ?>">
<input type="hidden" name="room_cond" value="<? echo($room_cond); ?>">
<input type="hidden" name="job_cond" value="<? echo($job_cond); ?>">
<input type="hidden" name="view" value="<? echo($view); ?>">
<input type="hidden" name="emp_o" value="<? echo($emp_o); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
