<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,23,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con=connect2db($fname);
if($con==0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$in="where dept_id in (";
$count=count($trashbox);
for($i=0;$i<$count;$i++){
	if(!$trashbox[$i]==""){
		$in.="'$trashbox[$i]'";
		if($i<($count-1)){
			$in.=",";
		}
	}
}
$in.=")";
$delete_department="update deptmst set dept_del_flg='t' $in";
$result_delete=pg_exec($con,$delete_department);
pg_close($con);
if($result_delete==true){
	echo("<script language='javascript'>location.href=\"./department_menu.php?session=$session&class_id=$class_id&atrb_id=$atrb_id\";</script>");
	exit;
}else{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>