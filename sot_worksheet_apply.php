<?
$error = "";

// 画面パラメータ取得
$session = @$_REQUEST["session"];
$ymd = (int)@$_REQUEST["ymd"];
$summary_seq = (int)@$_REQUEST["summary_seq"];
$apply_id = (int)@$_REQUEST["apply_id"];
$schedule_various_seq = (int)@$_REQUEST["schedule_various_seq"];
$mode = @$_REQUEST["mode"];
$memo = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["memo"])), "EUC-JP", "UTF-8"); // 文字化け対策のため、２重戻し
$global_element_index = (int)@$_REQUEST["global_element_index"];
$is_delete = (int)@$_REQUEST["is_delete"];
$emp_id = "";
$emp_name = "";
$hms = @$_REQUEST["hms"];
$is_accepted = "";
$is_operated = "";
$is_memo_registed = "";

$fname = $_SERVER["PHP_SELF"];

for (;;){
	// リクワイア   リクワイア先でのecho/print文を除去
	ob_start();
	require_once("about_postgres.php");
	require_once("about_session.php");
	require_once("about_authority.php");
	ob_end_clean();

	// DB接続
	$con = connect2db($fname);
	if($con == "0") { $error = "03"; break; }

	//セッションのチェック
	$session = qualify_session(@$session,$fname);
	if($session == "0") { $error = "01 / ". $session; break; }

	//権限チェック
	$summary_check=check_authority($session,57,$fname);
	if($summary_check == "0") { $error = "02"; break; }

	//職員情報取得
	$sql = "select * from empmst where emp_id in (select emp_id from session where session_id='".$session."') limit 1";
	$sel = select_from_table($con, $sql, "", $fname);
	if (!$sel) { $error = "04"; break; }
	$emp = pg_fetch_array($sel);
	if (!$emp || !count($emp)) { $error = "05"; break; }
	$emp_id  = $emp["emp_id"];
	$emp_name = trim($emp["emp_lt_nm"] . " " . $emp["emp_ft_nm"]);
	if (!$emp_id) { $error = "06"; break; }

	// 画面パラメータチェック
	if (!$is_delete){
		if ($ymd < 1) { $error = "11"; break; }
	}
	if ($schedule_various_seq < 1){
		if ($summary_seq < 1) { $error = "13"; break; }
		if ($global_element_index < 1) { $error = "14"; break; }
	}
	if (!$mode) { $error = "15"; break; }
	if ($mode!="accept" && $mode!="operate" && $mode!="memo") { $error = "16"; break; }
	if (!$is_delete){
		if ($mode=="accept" || $mode=="operate"){
			$tmp = explode(":", $hms);
			if (count($tmp) != 3) { $error = "17"; break; }
			if (!preg_match("/^[0-9]+$/", $tmp[0]) || !preg_match("/^[0-9]+$/", $tmp[1]) || !preg_match("/^[0-9]+$/", $tmp[2])) { $error = "18"; break; }
			if ($tmp[0] < 0 || $tmp[0] > 23) { $error = "19a"; break; }
			if ($tmp[1] < 0 || $tmp[1] > 59) { $error = "19b"; break; }
			if ($tmp[2] < 0 || $tmp[2] > 59) { $error = "19c"; break; }
		}
	}

	// WHERE句の準備
	if ($schedule_various_seq > 0) $where = " where schedule_various_seq = ". $schedule_various_seq;
	else $where = " where summary_seq = " . $summary_seq;
//	else $where = " where apply_id = ". $apply_id." and summary_seq = " . $summary_seq;
	$where .= " and worksheet_date = ". $ymd;

	// 既存データを検索する。
	$sel = select_from_table($con, "select * from sot_worksheet_apply".$where." limit 1", "", $fname);
	if (!$sel) { pg_close($con); $error = "21"; break; }
	$accepted_emp_id = @pg_fetch_result($sel, 0, "accepted_emp_id");

	// データがなければ仮登録する。
	if (!$is_delete){
		if (!@pg_fetch_result($sel, 0, "worksheet_date")){
			$sql =
				" insert into sot_worksheet_apply (worksheet_date, apply_id, summary_seq, schedule_various_seq)".
				" values (".$ymd.",".$apply_id.",".$summary_seq.",".$schedule_various_seq.")";
			if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "22"; break; }
			$is_accepted = 1;
		}
	}

	// 更新する。受信の場合
	if ($mode == "accept"){
		if ($is_delete){
			$sql =
				" update sot_worksheet_apply set".
				" accepted_emp_id = null".
				",accepted_datetime = null".
				",accepted_systime = current_timestamp".$where;
		} else {
			$sql =
				" update sot_worksheet_apply set".
				" accepted_emp_id = '".$emp_id."'".
				",accepted_datetime = '".date('Y-m-d')." ".pg_escape_string($hms)."'".
				",accepted_systime = current_timestamp".$where;
		}
		if (!update_set_table($con, $sql, array(), null, "", $fname)) { $error = "31"; break; }
		$is_accepted = 1;
	}
	// 更新する。実施の場合。受信されていなければ、ついでに受信マークする。
	if ($mode == "operate"){
		if ($is_delete){
			$sql =
				" update sot_worksheet_apply set".
				" operated_emp_id = null".
				",operated_datetime = null".
				",operated_systime = current_timestamp".$where;
		} else {
			$sql =
				" update sot_worksheet_apply set".
				" operated_emp_id = '".$emp_id."'".
				",operated_datetime = '".date('Y-m-d')." ".pg_escape_string($hms)."'".
				",operated_systime = current_timestamp".$where;
		}
		if (!update_set_table($con, $sql, array(), null, "", $fname)) { $error = "32"; break; }
		$is_operated = 1;

		if (!$is_delete){
			if ($accepted_emp_id == ""){
				$sql =
					" update sot_worksheet_apply set".
					" accepted_emp_id = '".$emp_id."'".
					",accepted_datetime = '".date('Y-m-d')." ".pg_escape_string($hms)."'".
					",accepted_systime = current_timestamp".
					$where;
				if (!update_set_table($con, $sql, array(), null, "", $fname)) { $error = "33"; break; }
				$is_accepted = 1;
			}
		}
	}
	// 更新する。メモの場合
	if ($mode == "memo"){
		$sql =
			" update sot_worksheet_apply set".
			" memo = '".pg_escape_string($memo)."'".
			",memo_update_emp_id = '" .pg_escape_string($emp_id)."'".
			",memo_update_datetime = current_timestamp".
			$where;
		if (!update_set_table($con, $sql, array(), null, "", $fname)) { $error = "34"; break; }
		$is_memo_registed = 1;
	}
	break;
}
if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{"ymd":"<?=$ymd?>","apply_id":"<?=$apply_id?>","summary_seq":"<?=$summary_seq?>","schedule_various_seq":"<?=$schedule_various_seq?>",
"mode":"<?=$mode?>","global_element_index":"<?=$global_element_index?>",
"emp_id":"<?=$emp_id?>","emp_name":"<?=$emp_name?>","error":"<?=$error?>",
"is_accepted":"<?=$is_accepted?>","is_operated":"<?=$is_operated?>","is_memo_registed":"<?=$is_memo_registed?>","is_delete":"<?=$is_delete?>"}
