<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | スケジュール印刷（日）</title>
<?
$fname = $PHP_SELF;

require_once("about_comedix.php");
require("time_check.ini");
require("project_check.ini");
require("show_date_navigation_day.ini");
require("holiday.php");
require("show_schedule_common.ini");
require("show_schedule_chart.ini");
require("show_calendar_memo.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
$last_month = get_last_month($date);
$last_day = get_last_day($date);
$next_day = get_next_day($date);
$next_month = get_next_month($date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 院内行事チェックボックスの処理
if ($timegd == "on" || $timegd == "off") {
	time_update($con, $emp_id, $timegd, $fname);
} else if ($timegd == "") {
	$timegd = time_check($con, $emp_id, $fname);
}

// 委員会・WGチェックボックスの処理
if ($pjtgd == "on" || $pjtgd == "off") {
	project_update($con, $emp_id, $pjtgd, $fname);
} else if ($pjtgd == "") {
	$pjtgd = project_check($con, $emp_id, $fname);
}

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// 当日の出勤パターンを取得
$schd_type_name = get_schd_name($con, $emp_id, $emp_id, "$year$month$day", $fname);

// 時間指定のないスケジュールの情報を配列で取得
$timeless_schedules = get_timeless_schedules($con, $emp_id, $pjtgd, $timegd, "$year$month$day", $fname);

// 時間指定のあるスケジュール情報を配列で取得
$normal_schedules = get_normal_schedules($con, $emp_id, $pjtgd, $timegd, "$year$month$day", $fname);

// ログインユーザの職員名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22" bgcolor="#f6f9ff">
<td width="194"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$year/$month/$day");?>(<? echo(get_weekday($date)); ?>)</font></td>
<td width="154"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_nm); ?></font></td>
<td align="center" width="138"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($schd_type_name); ?></font></td>
<td></td>
</tr>
</table>
<?
$start_date = date("Ymd", $date);
// カレンダーのメモを取得
$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $start_date);

$holiday = ktHolidayName($date);
if ($holiday != "") {
	$holiday_bgcolor = "#fadede";
} else if ($arr_calendar_memo["{$start_date}_type"] == "5") {
	$holiday_bgcolor = "#defafa";
} else {
	$holiday_bgcolor = "#ffffff";
}

if ($arr_calendar_memo["$start_date"] != "") {
	if ($holiday == "") {
		$holiday = $arr_calendar_memo["$start_date"];
	} else {
		$holiday .= "&nbsp;".$arr_calendar_memo["$start_date"];
	}
}

if ($holiday != "") {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td bgcolor="<?=$holiday_bgcolor?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><? echo($holiday); ?></font></td>
</tr>
</table>
<? } ?>
<?
echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n");

// 時間指定なし行を表示
show_timeless_schedules($timeless_schedules, $normal_schedules, $session, true, true);

// 時間指定あり行を表示
show_normal_schedules($timeless_schedules, $normal_schedules, $session, true, true);

echo("</table>\n");
?>
</form>
<? /* 凡例非表示
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width=""><? show_schd_type_color_list($con, $fname); ?></td>
<td width="1" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#bdd1e7"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
*/ ?>
</center>
</body>
<? pg_close($con); ?>
</html>
