<?
// 勤務シフト作成｜勤務分担表Excel出力

//ini_set("display_errors","1");
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_common.ini");
require_once("date_utils.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
//	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

	$download_data = get_xls_data($con, $fname, $obj, $group_id, $emp_id, $start_yr, $start_mon ,$start_day);

	///-----------------------------------------------------------------------------
	//ファイル名
	///-----------------------------------------------------------------------------
	$filename = 'allotment_'.$start_yr.$start_mon.$start_day.'.xls';
	if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	    $encoding = 'sjis';
	} else {
	    $encoding = mb_http_output();   // Firefox
	}
	$filename = mb_convert_encoding(
	                $filename,
	                $encoding,
	                mb_internal_encoding());

	ob_clean();
	header("Content-Type: application/vnd.ms-excel");
	header('Content-Disposition: attachment; filename=' . $filename);
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
	header('Pragma: public');
	echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());

	ob_end_flush();

	pg_close($con);



function get_xls_data($con, $fname, $obj, $group_id, $emp_id, $start_yr, $start_mon ,$start_day) {

	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	$arr_title = array(
		"チーム",	//0
		"職種",
		"役職",
		"氏名",
		"勤務予定",
		"受け持ち患者　　　　　　　　　　　　　　　　　　　　　　　",		//5
		"業務分担",
		"休憩"
	);
	$arr_width = array(
		"40",	//0
		"50",
		"50",
		"80",
		"65",
		"250",		//5
		"55",
		"30"
	);

	///-----------------------------------------------------------------------------
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
	$data_wktmgrp = $obj->get_wktmgrp_array();
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	///-----------------------------------------------------------------------------
	$group_array = $obj->get_duty_shift_group_array($group_id, "", $data_wktmgrp);

	$data_atdptn_all = $obj->get_atdptn_array("");
	$data_pattern_all = $obj->get_duty_shift_pattern_array("", $data_atdptn_all);

	$reason_2_array = $obj->get_reason_2("1");

	$start_date = $start_yr.$start_mon.$start_day;

	//下書き有無確認用期間
	$arr_date = $obj->get_term_from_group_date($group_id, $group_array, $start_date);
	$proc_flg = 1; //処理フラグ 1:下書き有無確認する
	$db_data = get_allotment_data($con, $fname, $group_id, $group_array, $start_date, $data_pattern_all, $reason_2_array, $data_atdptn_all, $proc_flg, $arr_date);

	$wd = date_utils::get_weekday_name($start_date);

	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";

	$data .= "<table width=\"650\" border=\"0\" cellspacing=\"2\" cellpadding=\"2\">\n";
	$data .= "<tr><td colspan=\"2\"><font size=\"4\">勤務分担表</font></td></tr>\n";
	$data .= "</table>\n";
	$data .= "<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
	$data .= "<tr height=\"32\">\n";
	$data .= "<td width=\"180\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><u>{$group_array[0]['group_name']}</u></font></td>\n";
	$data .= "<td width=\"40\" align=\"center\" style=\"border:solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">行事</font></td>\n";
// style=\"border:solid 1px;\"
	$data .= "<td width=\"290\" colspan=\"3\" style=\"border:solid 1px;\">&nbsp;</td>\n";
	$data .= "<td width=\"140\" colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><u>{$start_mon}月{$start_day}日（{$wd}）</u></font></td>\n";
	$data .= "</tr>\n";
	$data .= "</table>\n";

		$data .= "<table width=\"650\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n";
			$data .= "<tr height=\"22\" bgcolor=\"#f6f9ff\">\n";

			for ($i=0; $i<count($arr_title); $i++) {
				$data .= "<td align=\"left\" width=\"{$arr_width[$i]}\"><font size=\"1\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> \n";
				$data .= "$arr_title[$i]";
				$data .= "</font></td>\n";
			}

			$data .= "</tr>\n";

			for ($i=0; $i<count($db_data); $i++) {
				$data .= "<tr height=\"40\" valign=\"middle\"> \n";
				for ($j=0; $j<count($arr_title); $j++) {
					if ($j == 3) {
						$fontsize = "2";
					} else {
						$fontsize = "1";
					}
					$data .= "<td align=\"left\" width=\"{$arr_width[$j]}\" valign=\"middle\"><font size=\"$fontsize\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n";
					$data .= $db_data[$i][$j];
					$data .= "</font></td> \n";
				}
				$data .= "</tr>\n";
			}

		$data .= "</table>\n";

	return $data;
}
?>

