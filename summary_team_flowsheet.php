<?/*

// summary_flowsheet.phpを流用改造 2012.7.6

画面パラメータ
	$session
		セッションID
		(必須)
	$pt_id
		患者ID
		(必須)
	$s_date
		日付（YYYYMMDD）
		省略時はシステム日付。
	$offset
		読み飛ばす件数
		省略時は0
	$problem_id
		プロブレムID(抽出条件)
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<?
require_once("about_comedix.php");
require_once("summary_tmpl_util.ini");
require_once("summary_common.ini");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");
require_once("get_menu_label.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

//==============================
// 診療記録区分を作成
//==============================
$diag_name = array();
$diag_name[0] = "看護･介護記録";
$diag_name[1] = "コメディカル記録";
$diag_name[2] = "リハ記録";
$diag_name[3] = "医師記録";
$diag_name[4] = "すべて";
$diag_div = $diag_name[$page_num]; // 初期値

//==============================
// プロブレム一覧を取得
//==============================
$sql = "SELECT ptif_id,problem_id,problem FROM summary_problem_list ";
$cond = " WHERE ptif_id='$pt_id' AND end_date='' ORDER BY problem_id ";
$sel_prob = select_from_table($con,$sql,$cond,$fname);
if($sel_prob == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$prob_cnt = $sel_tmpl_num = pg_numrows($sel_prob);
$problem_list = array();
$problem_list[0]["id"] = "";
$problem_list[0]["name"] = "";
for($i=0; $i<=$prob_cnt; $i++){
	$j=$i+1;
	$problem_list[$j]["id"] = pg_result($sel_prob,$i,"problem_id");
	$problem_list[$j]["name"] = pg_result($sel_prob,$i,"problem");
}
$problem_list[$i]["id"] = "";
$problem_list[$i]["name"] = "すべて";

$prob_name = $prob_name[$page_num]; // 初期値

//================================================================================
//テンプレート定義を読み込む。tmpl_SOAP_Teikeibun.php テンプレートファイル名・固定
//================================================================================
// 
require_once("tmpl_SOAP_Teikeibun.php");
$is_flowsheet_usable_tmpl = function_exists("write_flowsheet_page");


//一覧表示件数
$list_disp_count = 4;

// 日付・読み飛ばし件数
$s_date = isset($_REQUEST["s_date"]) ? $_REQUEST["s_date"] : date("Ymd");
$offset = isset($_REQUEST["offset"]) ? $_REQUEST["offset"] : 0;

// SQL作成
$diag_name_count = count($diag_name) - 1; // 最後の「すべて」を除く
$conda = "(SELECT tmpl_id FROM tmplmst WHERE tmpl_name='SOAP')";
if ($offset >= 0) { // tmpl_idはSOAPを探す
	if( $page_num == $diag_name_count){
		$sql = "SELECT DISTINCT * FROM summary WHERE ptif_id = '$pt_id' and tmpl_id = ".$conda;
	}else{
		$sql = "SELECT DISTINCT * FROM summary WHERE ptif_id = '$pt_id' and tmpl_id = ".$conda." AND diag_div LIKE '$diag_div'";
	}
	if (@$problem_id != "") {
		$sql .= " AND problem_id = $problem_id";
	}
	$sql .= " AND cre_date <= '$s_date' ORDER BY cre_date DESC, summary_id DESC OFFSET $offset LIMIT $list_disp_count";
} else {
	if( $page_num == $diag_name_count){
		$sql = "SELECT * FROM (SELECT distinct * FROM summary WHERE ptif_id = '$pt_id' and tmpl_id = ".$conda;
	}else{
		$sql = "SELECT * FROM (SELECT distinct * FROM summary WHERE ptif_id = '$pt_id' and tmpl_id = ".$conda." AND diag_div LIKE '$diag_div'";
	}
	if (@$problem_id != "") {
		$sql .= " AND problem_id = $problem_id";
	}
	$sql .= " AND cre_date > '$s_date' ORDER BY cre_date, summary_id OFFSET " . (($offset + $list_disp_count) * -1) . " LIMIT $list_disp_count) t ORDER BY cre_date DESC, summary_id DESC";
}
$cond =  "";

// SQL実行
$sel_sum = select_from_table($con,$sql,$cond,$fname);
if($sel_sum == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//==========================================================================================
//HTML出力
//==========================================================================================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜フローシート</title>
<script type="text/javascript">

function load_action() {
	initcal();
	updateCal1();
}

var m_param_s_date = "<?=@$s_date?>";
var m_param_offset = "<?=@$offset?>";
var m_param_tmpl_id = "<?=@$disp_tmpl_id_param?>";
var m_param_sct_id = "<?=@$sct_id?>";
var m_param_problem_id = "<?=@$problem_id?>";
var m_param_creater_emp_id = "<?=@$creater_emp_id?>";
var m_page_num = "<?=$page_num?>";

function clear_extract_params() {
	m_param_sct_id = "";
	m_param_problem_id = "";
	m_param_creater_emp_id = "";
}

function postback(page) {
	prob = document.getElementById('select_probrem').value;
	var url = "summary_team_flowsheet.php"
			+ "?session=<?=@$session?>"
			+ "&pt_id=<?=@$pt_id?>"
			+ "&s_date=" + m_param_s_date
			+ "&offset=" + m_param_offset
//			+ "&tmpl_id=" + m_param_tmpl_id
			+ "&sct_id=" + m_param_sct_id
			+ "&problem_id=" + prob
//			+ "&creater_emp_id=" + m_param_creater_emp_id
			+ "&page_num=" + page
			;
	location.href = url;
}

function changeDate() {
	var s_year  = document.getElementById('date_y1').value;
	var s_month = document.getElementById('date_m1').value;
	var s_day   = document.getElementById('date_d1').value;

	var year  = parseInt(s_year , 10);
	var month = parseInt(s_month, 10) - 1;
	var day   = parseInt(s_day  , 10);

	var date = new Date(year, month, day);
	if (date.getFullYear() != year || date.getMonth() != month || date.getDate() != day) {
		alert('日付が不正です。');
		return;
	}

	m_param_s_date = s_year.concat(s_month).concat(s_day);
	m_param_offset = "0";
	postback(<?=$page_num?>);
}
</script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>
</head>
<body onload="load_action();">
<div style="padding:4px">

<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("フローシート"); ?>


<? //本体 出力 ?>

<!-- 診療区分情報 START -->
<table class="list_table" style="margin-top:4px">
	<tr>
		<th align="center" width="100"><nobr>診療区分</nobr></th>
		<td><nobr><? echo "$diag_name[$page_num]" ?></nobr></td>
	</tr>
</table>


<!-- 診療区分情報 END -->

<!--診療区分リンク-->
<BR>
<?
for($i=0; $i<count($diag_name); $i++ ){
?>
 <A HREF="javascript:postback(<?=$i?>)"><?echo $diag_name[$i];?></A>&nbsp;&nbsp;
<?
}
?>
<BR>
<!-- テンプレート選択 START -->
<div style="background-color:#f4f4f4; padding-top:5px; padding-bottom:5px; margin-top:10px">
<table style="width:100%">
	<tr>
	<!-- 日付選択 START -->
<?
$date_y1 = substr($s_date, 0, 4);
$date_m1 = substr($s_date, 4, 2);
$date_d1 = substr($s_date, 6, 2);
$sum_count = pg_num_rows($sel_sum);
?>
		<td >
<div style="float:left;"><select id="date_y1"><? show_select_years_span(min($date_y1, 1970), max($date_y1, date("Y") + 1), $date_y1, false); ?></select>/<select id="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1"><? show_select_days($date_d1, true); ?></select></div>
<div style="float:left;margin-left:2px;margin-right:3px;"><img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><br><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></div>
		<!-- 日付選択 END -->
<div style="float:left;">&nbsp;&nbsp;
<SELECT ID="select_probrem">
<? // 2012.7.9 追加
for ($i=0;$i<count($problem_list);$i++ ){
	$selected = "";
	if ( $problem_list[$i]["id"] == $problem_id ){
		$selected="SELECTED";
	}
	$wk=$problem_list[$i]["id"];
	echo "<OPTION VALUE='".$wk."' ".$selected.">".$problem_list[$i]["name"]."</OPTION>";
}
?>
<!--OPTION VALUE="" <?=$selected?>>すべて</OPTION-->
</SELECT>&nbsp;&nbsp;
</div>
<div style="float:left;"><input type="button" value="表示" onclick="changeDate();"></div>
<div style="float:left;margin-left:20px;"><a href="javascript:m_param_offset='<? echo($offset + $list_disp_count); ?>';postback(<?=$page_num?>);">＜前</a></div>
<div style="float:left;margin-left:20px;"><a href="javascript:m_param_offset='<? echo($offset - $list_disp_count); ?>';postback(<?=$page_num?>);">後＞</a></div>

</td>
	</tr>
</table>
</div>
<!-- 選択ヘッダー END -->

<?
if ($sum_count > 0) {
?>

<!-- 一覧 START -->
<table class="prop_table">



	<!-- 一覧:作成年月日 START -->
	<tr>
		<th align="center" width="150"><nobr>作成年月日</nobr></th>
		<?
			for ($i = $sum_count - 1; $i >= 0; $i--) {
				$cre_date = pg_fetch_result($sel_sum, $i, "cre_date");
				$disp_date = substr($cre_date,0,4)."年".((int)substr($cre_date,4,2))."月".((int)substr($cre_date,6,2))."日";
				echo '<th align="center"><nobr>'.$disp_date.'</nobr></th>';
			}
		?>
	</tr>
	<!-- 一覧:作成年月日 END -->



	<!-- 一覧:診療科 START -->
	<tr>
		<th align="center" width="150"><nobr><?=$_label_by_profile["SECTION"][$profile_type] ?></nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_sect_id = pg_fetch_result($sel_sum, $i, "sect_id");
			$line_sect_name = get_sect_name($line_sect_id,$con,$fname);
		?>
		<td align="left">
			<nobr>
				<!--a href="javascript:clear_extract_params();m_param_sct_id='<?=$line_sect_id?>';postback(<?=$page_num?>);"-->
				<? echo h($line_sect_name); ?>
				<!--/a-->
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:診療科 END -->



	<!-- 一覧:プロブレム START -->
	<tr>
		<th align="center" width="150"><nobr>プロブレム</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_problem_id = pg_fetch_result($sel_sum, $i, "problem_id");
			$line_problem = pg_fetch_result($sel_sum, $i, "problem");
		?>
		<td align="left">
			<nobr>
			<!--a href="javascript:clear_extract_params();m_param_problem_id='<?=$line_problem_id?>';postback(<?=$page_num?>);"-->
			<? echo h($line_problem); ?>
			<!--/a-->
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:プロブレム END -->



	<!-- 一覧:記載者 START -->
	<tr>
		<th align="center" width="150"><nobr>記載者</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$line_creater_emp_id = pg_fetch_result($sel_sum, $i, "emp_id");
			$line_creater_emp_name = get_emp_kanji_name($con,$line_creater_emp_id,$fname);
		?>
		<td align="left">
			<nobr>
			<!--a href="javascript:clear_extract_params();m_param_creater_emp_id='<?=$line_creater_emp_id?>';postback(<?=$page_num?>);"-->
			<? echo h($line_creater_emp_name); ?>
			<!--/a-->
			</nobr>
		</td>
		<? } ?>
	</tr>
	<!-- 一覧:記載者 END -->



	<!-- 一覧:記事重要度 START -->
	<tr>
		<th align="center" width="150"><nobr>記事重要度</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$list_priority = pg_fetch_result($sel_sum, $i, "priority");
		?>
		<td align="left"><nobr><? echo h($list_priority); ?></nobr></td>
		<? } ?>
	</tr>
	<!-- 一覧:記事重要度 END -->



	<!-- 一覧:内容 START -->
<?
	if($is_flowsheet_usable_tmpl) { //フローシート対応テンプレートの場合
		//データXMLファイル配列を取得する。
		$xml_files = array();
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$creater_emp_id = pg_fetch_result($sel_sum, $i, "emp_id");
			$summary_id = pg_fetch_result($sel_sum, $i, "summary_id");
			$xml_file = get_tmpl_xml_file_name($creater_emp_id, $pt_id, $summary_id);
			$xml_files[] = $xml_file;
		}
		write_flowsheet_page($xml_files,count($xml_files));//テンプレートインターフェース関数より一覧を出力
	} else {//フローシート未対応の場合
	//テンプレート未対応の場合は内容のみを表示
?>
	<tr>
		<th align="center"><nobr>内容</nobr></th>
		<?
		for ($i = $sum_count - 1; $i >= 0; $i--) {
			$summary_id = pg_fetch_result($sel_sum, $i, "summary_id");
			$naiyo = get_summary_naiyo_html($con,$fname,$summary_id,$pt_id);
			if($disp_tmpl_id != "") {//テンプレートの場合
		?>
		<td align="left" valign="top">
			<?=$naiyo?>
		</td>
		<? } else {//フリー入力の場合 ?>
		<td align="left" valign="top"><?=$naiyo?></td>
		<? } ?>
<? } ?>
	</tr>
<? } ?>
<!-- 一覧:内容 END -->
</table>

<? } else { ?>
<p style="margin:0;">表示対象がありません。</p>
<? } ?>

<!-- 一覧 END -->
</div>
</body>
</html>
<?

pg_close($con);
?>
