<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_apply_history.ini");
require_once("cl_application_apply_detail.ini");
require_once("show_select_values.ini");
require_once("cl_application_imprint_common.ini");
require_once("cl_yui_calendar_util.ini");
require_once("get_values.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");
require_once(dirname(__FILE__) . "/Cmx.php");
require_once('MDB2.php');
require_once("cl_common_log_class.inc");

$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	$log->error('セッションのチェックエラー',__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	$log->error('権限チェックエラー',__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	$log->error('DBコネクション取得エラー',__FILE__,__LINE__);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 申請詳細</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cl_common.js"></script>
<?
if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

//------------------------------------------------------------------------------
// 申請・ワークフロー情報取得
//------------------------------------------------------------------------------
$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($target_apply_id);
$wkfw_content      = $arr_apply_wkfwmst[0]["wkfw_content"];
$wkfw_content_type = $arr_apply_wkfwmst[0]["wkfw_content_type"];
$apply_content     = $arr_apply_wkfwmst[0]["apply_content"];
$wkfw_id           = $arr_apply_wkfwmst[0]["wkfw_id"];
$wkfw_history_no   = $arr_apply_wkfwmst[0]["wkfw_history_no"];
$short_wkfw_name   = $arr_apply_wkfwmst[0]["short_wkfw_name"];


//カレンダー外部ファイルの読み込み
write_yui_calendar_use_file_read_0_12_2();

?>
<script type="text/javascript">

function reload_apply_page()
{
	document.apply.action="cl_application_apply_detail.php?session=<?=$session?>&apply_id=<?=$apply_id?>&mode=show_flg_update";
	document.apply.submit();
}


function history_select(apply_id, target_apply_id) {

	document.apply.apply_id.value = apply_id;
	document.apply.target_apply_id.value = target_apply_id;
	document.apply.action="cl_application_apply_detail.php?session=<?=$session?>";
	document.apply.submit();
}

function set_regist_emp_id(obj, idx) {
	var emp_id = 'regist_emp_id' + idx;
	document.apply.elements[emp_id].value = obj.value;
}

function attachFile() {
	window.open('cl_apply_attach.php?session=<? echo($session); ?>', 'newwin2', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function apply_update() {

	// 申請書入力チェック
	var rtn=AplDetail_Validate();
	if(rtn==false){
		return;
	}

	if (window.InputCheck) {
		if (!InputCheck()) {
			return;
		}
	}

	if (confirm('更新します。よろしいですか？')) {
		document.apply.action="cl_application_apply_detail_update.php";
		document.apply.mode.value="apply_update";
		document.apply.submit();
	}
}

function apply_cancel() {

	// 2012/07/31 Yamagawa add(s)
	if (window.Cancel_Validate) {
		if(!Cancel_Validate()) {
			return;
		}
	}
	// 2012/07/31 Yamagawa add(e)

	if (confirm('申請取消します。よろしいですか？')) {
		document.apply.action="cl_application_apply_detail_cancel.php";
		document.apply.mode.value="apply_cancel";
		document.apply.submit();
	}
}

function re_apply() {

	// 2012/06/14 Yamagawa add(s)
	// 申請書入力チェック
	var rtn=ReApl_Validate();
	if(rtn==false){
		return;
	}
	// 2012/06/14 Yamagawa add(e)

	if (confirm('再申請します。よろしいですか？')) {
		document.apply.action="cl_application_apply_detail_re_apply.php";
		document.apply.mode.value="re_apply";
		document.apply.submit();
	}
}

function init() {

	if('<?=$back?>' == 't') {
<?

		$approve_num = $_POST[approve_num];
		for($i=1; $i<=$approve_num;$i++) {

			$regist_emp_id = "regist_emp_id$i";
			$radio_emp_id = "radio_emp_id$i";
			$approve_name = "approve_name$i";
?>
			if(document.getElementById('<?=$radio_emp_id?>')) {
				for(j=0; j<document.apply.elements['<?=$radio_emp_id?>'].length; j++) {

					if(document.apply.elements['<?=$radio_emp_id?>'][j].value == '<?=$_POST[$regist_emp_id];?>') {
						document.apply.elements['<?=$radio_emp_id?>'][j].checked = true;
					}
				}
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}


			if(document.getElementById('<?=$approve_name?>')) {
				document.apply.elements['<?=$approve_name?>'].value = '<?=$_POST[$approve_name];?>'
				document.apply.elements['<?=$regist_emp_id?>'].value = '<?=$_POST[$regist_emp_id];?>';
			}
<?
		}
?>
	}

}

function apply_printwin()
{
	//window.open('cl_application_apply_detail_print.php?session=<?=$session?>&fname=<?=$fname?>&target_apply_id=<?=$apply_id?>&back=f&mode=apply_print&simple=<?=$simple?>', 'apply_print', 'width=670,height=700,scrollbars=yes');

	window.open('', 'apply_print', 'width=670,height=700,scrollbars=yes');

	document.apply.action="cl_application_apply_detail_print.php";
	document.apply.target="apply_print";
	document.apply.mode.value = "apply_print";
	//session,target_apply_idはhidden初期設定値を使用
	document.apply.submit();

	document.apply.target="_self";





//	document.apply.mode.value = "apply_printwin";
//	document.apply.action="cl_application_apply_detail.php?session=<?=$session?>";
//	document.apply.submit();
}

function apply_simpleprintwin() {
	document.apply.simple.value = "t";
	apply_printwin();
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        resize_history_tbl();
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];

		//不活性では無い場合はリサイズの対象外
	    if(! t.disabled)
	    {
		    continue;
		}

		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}

// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}

function DisplayControl(apply_id){
	// テンプレートの申請詳細　表示コントロールを起動する
	AplDetail_Display(apply_id);
}

//研修選択時　承認者欄　主催者表示
function SetApvEmpTraining_Opener(training_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_opener_json.php'		,	// データ取得スクリプト
		{training_id:training_id}					,	// パラメーター
		function( param , status ) {					// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTraining_Opener( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

// 研修選択時　承認者欄　講師表示
function SetApvEmpTraining_Teacher(training_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_teacher_json.php'		,	// データ取得スクリプト
		{training_id:training_id}					,	// パラメーター
		function( param , status ) {					// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTraining_Teacher( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

// 課題選択時　承認者欄　課題提出先表示
function SetApvEmpTheme_Present(theme_id){
	iDummy = 0;
	jQuery.getJSON(
		'cl/common/cl_apvemp_theme_present_json.php'		,	// データ取得スクリプト
		{theme_id:theme_id}									,	// パラメーター
		function( param , status ) {							// 画面表示処理

			if(status=="success"){
				/* 読み込んだデータで表示する */
				Display_ApvEmpTheme_Present( param );
			}
			else{
				alert("承認者の取得に失敗しました。");
				showLoginPage(window);
			}
		}
	);
}

/**
 * 主催者表示
 * @param object data 院内研修データ
 */
function Display_ApvEmpTraining_Opener(data){

	var pos = -1;
	var no = 0;

	var rows = document.getElementsByTagName('span');
	for (i = 0; i < rows.length; i++) {
		pos = rows[i].id.indexOf('apvopener_');
		if (pos == 0){
			no = rows[i].id.substr(10);
			rows[i].innerHTML = data.emp_lt_nm + ' ' + data.emp_ft_nm;
			document.getElementById('regist_emp_id' + no).value = data.training_opener;
			document.getElementById('st_div' + no).value = '5';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
		}

	}

}

/**
 * 講師表示
 * @param object data 院内研修データ
 */
function Display_ApvEmpTraining_Teacher(data){

	var html_build = '';
	var pos = -1;
	var no = 0;
	var empcnt = 0;
	var apvteacher_num = 0;
	var apvteacher_span_num = 0;
	var emp_single_id = '';
	var emp_single_nm = '';

	if (data.training_teacher5 != ''){
		empcnt++;
		emp_single_id = data.training_teacher5;
		emp_single_nm  = data.emp_lt_nm5;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm5;
	}
	if (data.training_teacher4 != ''){
		empcnt++;
		emp_single_id = data.training_teacher4;
		emp_single_nm  = data.emp_lt_nm4;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm4;
	}
	if (data.training_teacher3 != ''){
		empcnt++;
		emp_single_id = data.training_teacher3;
		emp_single_nm  = data.emp_lt_nm3;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm3;
	}
	if (data.training_teacher2 != ''){
		empcnt++;
		emp_single_id = data.training_teacher2;
		emp_single_nm  = data.emp_lt_nm2;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm2;
	}
	if (data.training_teacher1 != ''){
		empcnt++;
		emp_single_id = data.training_teacher1;
		emp_single_nm  = data.emp_lt_nm1;
		emp_single_nm += ' ';
		emp_single_nm += data.emp_ft_nm1;
	}

	var rows = document.getElementsByTagName('span');

	for (i = 0; i < rows.length; i++) {

		pos = rows[i].id.indexOf('apvteacher_');
		if (pos == 0){
			apvteacher_span_num++;
			no = rows[i].id.substr(11);
			if (empcnt == 0){
				html_build = '該当者なし';
			} else if(empcnt == 1){
				html_build = emp_single_nm;
				apvteacher_num += 1;
			} else {
				if (document.getElementById('multi_apv_flg' + no).value == 't') {
					html_build  = HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher1 ,data.emp_lt_nm1 ,data.emp_ft_nm1 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher2 ,data.emp_lt_nm2 ,data.emp_ft_nm2 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher3 ,data.emp_lt_nm3 ,data.emp_ft_nm3 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher4 ,data.emp_lt_nm4 ,data.emp_ft_nm4 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(true ,no ,data.training_teacher5 ,data.emp_lt_nm5 ,data.emp_ft_nm5 ,true);
					apvteacher_num += empcnt;
				} else {
					html_build  = HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher1 ,data.emp_lt_nm1 ,data.emp_ft_nm1 ,true);
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher2 ,data.emp_lt_nm2 ,data.emp_ft_nm2 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher3 ,data.emp_lt_nm3 ,data.emp_ft_nm3 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher4 ,data.emp_lt_nm4 ,data.emp_ft_nm4 ,html_build == '');
					html_build += HTML_Build_ApvEmpTraining_Teacher(false ,no ,data.training_teacher5 ,data.emp_lt_nm5 ,data.emp_ft_nm5 ,html_build == '');
					apvteacher_num += 1;
				}
			}
			rows[i].innerHTML = html_build;
			document.getElementById('regist_emp_id' + no).value = emp_single_id;
			document.getElementById('st_div' + no).value = '6';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
			if (apvteacher_num > 0) {
				var approve_num = parseInt(document.getElementById('approve_num').value);
				var apvteacher_num_obj = document.getElementById('apvteacher_num');
				approve_num = approve_num - apvteacher_span_num + apvteacher_num;
				apvteacher_num_obj.value = empcnt;
				document.getElementById('approve_num_view').innerHTML = approve_num;
			}
		}

	}

}

function HTML_Build_ApvEmpTraining_Teacher(multi_apv_flg,no,emp_id,emp_lt_nm,emp_ft_nm,checkflg){

	var html_build = '';

	if (emp_id != '') {

		if (multi_apv_flg){
			html_build  = '<input type="checkbox" name="check_emp_id';
			html_build += no;
			html_build += '_';
			html_build += emp_id;
			html_build += '" id="check_emp_id';
			html_build += no;
			html_build += '_';
			html_build += emp_id;
			html_build += '" onclick="set_approve_num(this);';
			html_build += '" value="';
			html_build += emp_id;
		} else {
			html_build  = '<input type="radio" name="radio_emp_id';
			html_build += no;
			html_build += '" onclick="set_emp_st(this, ' + "'" + no + "','6','','');";
			html_build += '" value="';
			html_build += emp_id;
			html_build += ',講師';
		}

		if (checkflg) {
			html_build += '" checked>';
		} else {
			html_build += '" >';
		}

		html_build += emp_lt_nm;
		html_build += ' ';
		html_build += emp_ft_nm;
		html_build += '<br>';

	}

	return html_build;

}

/**
 * 課題提出先表示
 * @param object data 研修課題データ
 */
function Display_ApvEmpTheme_Present(data){

	var pos = -1;
	var no = 0;

	var rows = document.getElementsByTagName('span');
	for (i = 0; i < rows.length; i++) {

		pos = rows[i].id.indexOf('apvpresent_');
		if (pos == 0){
			no = rows[i].id.substr(11);
			rows[i].innerHTML = data.emp_lt_nm + ' ' + data.emp_ft_nm;
			document.getElementById('regist_emp_id' + no).value = data.theme_present;
			document.getElementById('st_div' + no).value = '7';
			document.getElementById('parent_pjt_id' + no).value = '';
			document.getElementById('child_pjt_id' + no).value = '';
		}

	}

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#A0D25A solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#A0D25A solid 0px;}
table.block3 td {border-width:0;}


p {margin:0;}

</style>

<script type="text/javascript">

//onload()時の処理
function do_onload()
{
	//初期処理
	init();

	//テンプレート表示コントロール
	DisplayControl('<?=$apply_id?>');
}

//テンプレートの表示が完了した後の処理
function templateDispAfter()
{
	//テキストエリアのリサイズ処理
	resizeAllTextArea();

	//履歴表示tableの高さ調節
	resize_history_tbl();

}


</script>


</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="do_onload()">
<!-- 2012/01/06 Yamagawa upd(s) idを追加 -->
<form id="apply" name="apply" action="#" method="post">
<input type="hidden" id="session" name="session" value="<? echo($session); ?>">
<input type="hidden" id="apply_id" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" id="target_apply_id" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" id="mode" name="mode" value="">
<input type="hidden" id="simple" name="simple" value="">
<input type="hidden" id="wkfw_id" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" id="wkfw_history_no" name="wkfw_history_no" value="<? echo($wkfw_history_no); ?>">
<!-- 2012/01/06 Yamagawa upd(e) -->

<input type="hidden" id="short_wkfw_name" name="short_wkfw_name" value="<?=$short_wkfw_name?>">
<input type="hidden" id="achievement_order" name="achievement_order" value="<?=$achievement_order?>">
<input type="hidden" id="session" name="session" value="<?=$session?>">


<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>申請詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="top" width="25%">
<? show_application_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="75%">
<? show_application_apply_detail($con, $session, $fname, $target_apply_id, $apply_title, $content, $file_id, $filename, $back, $mode); ?>
</td>
</tr>
</table>
</center>


</form>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト START -->
<form id="common_module_call_form" name="common_module_call_form" method="post" action="cl_common_call.php" target="common_module_window"></form>
<script type="text/javascript">
//子画面(共通モジュール)を開きます。
//
//moduleName 子画面のモジュール名を指定します。
//params     子画面へ送信する専用パラメータを設定します。Objectを指定してください。Objectの属性名が送信パラメータ名となり、値がパラメータとなります。
//option     子画面を開くときのオプションを指定します。(JavaScriptのopen関数のoptionと同じです。)
function commonOpen(moduleName,params,option)
{
	//子画面(共通モジュール)送信用フォームタグ
	var f = document.getElementById("common_module_call_form");

	//共通パラメータ用のhiddenを作成、値を設定。
	var html = "";
	html = html + '<input type="hidden" name="common_module" value="' + moduleName + '">';
	html = html + '<input type="hidden" name="session"       value="<?=$session ?>">';

	//専用パラメータ用のhiddenをparamsから作成。
	for(var key in params)
	{
		html = html + '<input type="hidden" name="' + key + '" value="">';
	}
	f.innerHTML = html;

	//専用パラメータ用のhiddenにparamsから値を設定。
	for(var key in params)
	{
		var hidden = eval("f." + key);
		hidden.value = eval("params." + key);
	}

	//子画面を開く
	window.open('', 'common_module_window', option);

	//子画面へ送信
	f.submit();
}
</script>
<!-- 子画面(共通モジュール)を開くためのフォーム及びスクリプト END -->
</body>
</html>

<?
$update_cnt = 0;
$show_flg_name = "";
if($mode == 'show_flg_update') {

	// トランザクションを開始
	pg_query($con, "begin");

	$sql = "select apply_stat, apv_fix_show_flg, apv_ng_show_flg, apv_bak_show_flg from cl_apply";
	$cond = "where apply_id = '$apply_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	$log->debug('$apply_id:'.$apply_id,__FILE__,__LINE__);
	if ($sel == 0) {
		$log->error('$sel == 0',__FILE__,__LINE__);
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_stat = pg_fetch_result($sel, 0, "apply_stat");
	$apv_fix_show_flg = pg_fetch_result($sel, 0, "apv_fix_show_flg");
	$apv_ng_show_flg = pg_fetch_result($sel, 0, "apv_ng_show_flg");
	$apv_bak_show_flg = pg_fetch_result($sel, 0, "apv_bak_show_flg");

	if ($apply_stat == "1" && $apv_fix_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_fix_show_flg";
	}
	if ($apply_stat == "2" && $apv_ng_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_ng_show_flg";
	}
	if ($apply_stat == "3" && $apv_bak_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_bak_show_flg";
	}

	if ($update_cnt > 0) {
		$sql = "update cl_apply set";
		$set = array($show_flg_name);
		$setvalue = array('f');
		$cond = "where apply_id = '$apply_id' and $show_flg_name = 't' and apply_stat = '$apply_stat'";
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			$log->error('$up ==0',__FILE__,__LINE__);
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

// 一部承認の場合
	if ($apply_stat == "0") {
		$sql = "select count(apply_id) as show_flg_cnt from cl_applyapv";
		$cond = "where apply_id = '$apply_id' and apv_stat = '1' and apv_fix_show_flg = 't'";

		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			$log->error('$sel == 0',__FILE__,__LINE__);
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$show_flg_cnt = pg_fetch_result($sel, 0, "show_flg_cnt");

		if ($show_flg_cnt > 0) {
			$update_cnt = 1;
			$sql = "update cl_applyapv set";
			$set = array("apv_fix_show_flg");
			$setvalue = array('f');
			$cond = "where apply_id = '$apply_id' and apv_fix_show_flg = 't' and apv_stat = '1'";
			$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if($up ==0){
				$log->error('$up ==0',__FILE__,__LINE__);
				pg_exec($con,"rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}

	}
	// トランザクションをコミット
	pg_query($con, "commit");
}

pg_close($con);
?>

<script type="text/javascript">
<!--
function page_close() {
<?
if($mode == 'show_flg_update' && $update_cnt > 0) {
?>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?
} else {
?>
	window.close();
<?
}
?>

}
//-->
</script>