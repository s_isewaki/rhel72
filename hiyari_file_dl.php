<?
ob_start();
ini_set("max_execution_time", 600);

//セッション
$session = $s;

//report_id
$report_id = $r;

//ファイル名（暗号化）
$file_enc = $u;

require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("library_common.php");
require_once("Cmx.php");

$fname = $PHP_SELF;

if ($session != "") {

	// セッションのチェック
	$session = qualify_session($session, $fname);
	if ($session == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// セッションIDがあれば職員IDを取得
if ($session != "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
} else {
	$emp_id = null;
}

// 添付ファイル情報を取得
$sql = "select report_file_enc, report_file_name, registration_date from inci_file join inci_report on inci_file.report_id=inci_report.report_id ";
$cond = "where inci_file.report_id = '$report_id' and report_file_enc = '$file_enc' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($this->_db_con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//添付ファイル名を取得
$file_name = pg_fetch_result($sel, 0, "report_file_name");
$load_file_arr = explode(".", $file_name);
$load_file_ext = $load_file_arr[count($load_file_arr) - 1];


$registration_date = pg_result($sel, 0, "registration_date");
$reg_date_arr = explode("/", $registration_date);

//作成年
$reg_year = $reg_date_arr[0];

//作成月
$reg_month = $reg_date_arr[1];

//$hiyari_file_url = "inci_file/".$report_id."/".$file_enc.".".$load_file_ext;
$hiyari_file_url = "inci_file/".$reg_year."/".$reg_month."/".$report_id."/".$file_enc.".".$load_file_ext;



// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

$file_path = $hiyari_file_url;

// file download
if (is_file($file_path)) {
    $filename_flg = lib_get_filename_flg();
    if ($filename_flg == 0) { // システム名
        $file_name = $file_enc.".".$load_file_ext;
    } else if ($filename_flg == 1) { // MSIE
        $file_name = to_sjis($file_name);
    } else if ($filename_flg == 2) { // ff
        $file_name = to_utf8($file_name);
    } else {
        $file_name = rawurlencode(to_utf8($file_name));
    }

    ob_end_clean();
    header("Content-Disposition: attachment; filename=\"$file_name\"");
    header("Content-Type: application/octet-stream; name=$file_name");
    header("Content-Length: " . filesize($file_path));
    readfile($file_path);
} else {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
}
