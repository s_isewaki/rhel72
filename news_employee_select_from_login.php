<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ｜発信者選択</title>
<?
//ini_set("display_errors","1");
require("about_postgres.php");
//require("about_session.php");
//require("about_authority.php");
require("show_select_values.ini");

$fname = $PHP_SELF;

/*
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
*/
// データベースに接続
$con = connect2db($fname);

// 部門のデフォルトは最初の部門
if ($class_id == 0) {
	$sql = "select min(class_id) as class_id from classmst";
	$cond = "where class_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$class_id = pg_fetch_result($sel, 0, "class_id");
}

// イニシャルのデフォルトは「あ行」
if ($in_id == "") {
	$in_id = 1;
}
?>
<script language="javascript">
function classOnChange(class_id) {
	location.href = '<? echo($fname); ?>?class_id='.concat(class_id);
}

function employeeOnClick(emp_id, emp_nm) {
	opener.document.mainform.src_emp_id.value = emp_id;
	opener.document.mainform.src_emp_nm.value = emp_nm;
	opener.document.mainform.hid_src_emp_nm.value = emp_nm;
	window.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>発信者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><select name="class_id" onchange="classOnChange(this.value);"><? show_class_options($con, $class_id, $fname); ?></select></td>
</tr>
<tr height="22">
<td><? show_initial_list($class_id, $in_id, $fname); ?></td>
</tr>
<tr height="22">
<td><? show_employee_list($con, $class_id, $in_id, $fname); ?></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_class_options($con, $class_id, $fname) {
	$sql = "select class_id, class_nm from classmst";
	$cond = "where class_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		echo("<option value=\"{$row["class_id"]}\"");
		if ($row["class_id"] == $class_id) {
			echo(" selected");
		}
		echo(">{$row["class_nm"]}\n");
	}
}

function show_initial_list($class_id, $in_id, $fname) {
	$alphabet = array("あ行", "か行", "さ行", "た行", "な行", "は行", "ま行", "や行", "ら行", "わ行");
	for ($i = 0; $i < 10; $i++) {
		$index = $i + 1;
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($in_id != $index) {
			echo("<a href=\"$fname?class_id=$class_id&in_id=$index\">{$alphabet[$i]}</a>\n");
		} else {
			echo("{$alphabet[$i]}\n");
		}
		echo("</font>");
	}
}

function show_employee_list($con, $class_id, $in_id, $fname) {
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";

	switch ($in_id) {
	case "1":
		$keys = array("01", "02", "03", "04", "05");
		break;
	case "2":
		$keys = array("06", "07", "08", "09", "10", "11", "12", "13", "14", "15");
		break;
	case "3":
		$keys = array("16", "17", "18", "19", "20", "21", "22", "23", "24", "25");
		break;
	case "4":
		$keys = array("26", "27", "28", "29", "30", "31", "32", "33", "34", "35");
		break;
	case "5":
		$keys = array("36", "37", "38", "39", "40");
		break;
	case "6":
		$keys = array("41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
		break;
	case "7":
		$keys = array("56", "57", "58", "59", "60");
		break;
	case "8":
		$keys = array("61", "62", "63");
		break;
	case "9":
		$keys = array("64", "65", "66", "67", "68");
		break;
	case "10":
		$keys = array("69", "70", "71", "72", "73", "99");
		break;
	}
	$cond = "where emp_class = $class_id and (emp_keywd like '" . join("%' or emp_keywd like '", $keys) . "%') ";
	$cond .= "and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') order by emp_keywd";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) {
		$emp_id = $row["emp_id"];
		$emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<a href=\"javascript:void(0);\" onclick=\"employeeOnClick('$emp_id', '$emp_nm');\">$emp_nm</a>");
		echo("</font>");
		echo("<br>\n");
	}
}
?>
