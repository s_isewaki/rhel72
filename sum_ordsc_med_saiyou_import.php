<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require("sot_util.php");
require("summary_common.ini");
//require_once("tmpl_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if ($_REQUEST["give_me"] != "current_status"){
	if(!$session || !$fplusadm_auth || !$con){
	  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	  exit;
	}
}


//****************************************************************************
// インポート進捗の問い合わせ
//****************************************************************************
if ($_REQUEST["give_me"] == "current_status"){
	$sel = select_from_table($con, "select medmst_upload_message from summary_option", "", $fname);
	echo '{"msg":"'.pg_fetch_result($sel,0,0).'"}';
	die;
}





//****************************************************************************
// インポート開始
//****************************************************************************
if ($_REQUEST["mode"]=="import"){
	$errmsg = array();
	$medmst_keylist = array();
	$saiyou_keylist = array();
	//	$saiyou_hot13list = array();
	$mstcnt = 0;
	$rowcnt = 0;
	$regcnt = 0;
	$saicnt = 0;
	$updcnt = 0;

	// YJコードかHOTコードの選択
	$code_sel = $_REQUEST["code_sel"];
	// 一度削除するオプションの選択
	$remove_sel = $_REQUEST["remove_sel"];



	//==============================
	// 一度削除するを選択した場合
	//==============================

	if($remove_sel == 't'){
		$sql = "update sum_med_saiyou_mst set used_counter = 0";
		$upd = pg_exec($con, $sql);
		if (!@$upd){
		 $errmsg[] = "データの初期化に失敗しました。";
		 continue;
		}

	}

	$upd = pg_exec($con, "delete from sum_medmst_import_log");
	$upd = pg_exec($con, "update summary_option set medmst_upload_message = '登録済みデータの状態を確認しています。'");

	for (;;){
		if (!@$_FILES['upfile']['name']){
			$errmsg[] = "ファイルを指定してください。";
			break;
		}
		if (@$_FILES['upfile']['error']){
			$errmsg[] = "アップロードエラーが発生しました。権限またはファイルサイズ容量がサーバ設定されていないか可能性があります。システム管理者にお問い合わせください。";
			break;
		}
		if (!@$_FILES['upfile']['size']){
			$errmsg[] = "ファイルサイズが確認できません。正しいファイルを指定してください。";
			break;
		}
		$upfile_name = $_FILES["upfile"]["tmp_name"];
		if (($fp = fopen($upfile_name, "r")) == FALSE){
			$errmsg[] = "ファイルの読込みに失敗しました。";
			break;
		}
		while (($csvline = @fgets($fp, 512)) !== FALSE){
			if (count($errmsg) > 100) break;
			$rowcnt++;
			$upd = pg_exec($con, "update summary_option set medmst_upload_message = '".$rowcnt."行目を処理中 '");

			if (strlen($csvline) > 500){
				$errmsg[] = "(".$rowcnt."行目) 行データが長すぎます。正しいファイルを指定してください。処理を中断します。";
				break;
			}

			$csv = explode(",", mb_convert_encoding($csvline,"euc","sjis"));
			$fieldlen = count($csv);
			if ($fieldlen != 3) {
				$errmsg[] = "(".$rowcnt."行目) カンマ区切りフィールド数が".$fieldlen."です。3フィールドである必要があります。この行は無視されます。";
				continue;
			}
			for ($i = 0; $i < 24; $i++) $csv[$i] = trim(trim($csv[$i]), '"');
			if($code_sel == 1){
				if (strlen($csv[0]) != 12){
					$errmsg[] = "(".$rowcnt."行目) 1フィールド目の値が不正です。12バイトである必要があります。処理を中断します。";
					break;
				}
			}
			else{
				if (strlen($csv[0]) != 13){
					$errmsg[] = "(".$rowcnt."行目) 1フィールド目の値が不正です。13バイトである必要があります。処理を中断します。";
					break;
				}
			}


			//==============================
			// 薬品マスタ
			//==============================
			// YJコードでのインポート
			if($code_sel == 1){
				$sql  = "select ";
				$sql .= "  count(*) as cnt ";
				$sql .= "from sum_medmst ";
				$sql .= "where yj_cd = '$csv[0]' OR (yj_cd = '$csv[0]' and maker_name = '$csv[1]')";
			}

			// HOTコードでのインポート
			if($code_sel == 2){
				$sql  = "select ";
				$sql .= "  count(*) as cnt ";
				$sql .= "from sum_medmst ";
				$sql .= "where hot13_cd = '$csv[0]' OR (hot13_cd = '$csv[0]' and maker_name = '$csv[1]')";
			}

			$sel = select_from_table($con, $sql, "", $fname);
			$cnt = pg_fetch_result($sel,0,0);
			if($cnt == 0){
				$errmsg[] = "(".$rowcnt."行目) 該当する薬品が薬品マスタ内にありません。（[".$csv[0]."][".$csv[1]."][".$csv[2]."]）";
				continue;
			}

			// YJコード用
			if($code_sel == 1){
				$sql  = "select  ";
				$sql .= "    med.hanbai_name ";
				$sql .= "  , med.hot13_cd ";
				$sql .= "  , med.kokuji_name ";
				$sql .= "  , med.zaikei_bunrui ";
				$sql .= "  , med.seibun_bunrui ";
				$sql .= "  , med.yakkou_bunrui_cd  ";
				$sql .= "  , med.maker_name  ";
				$sql .= "from sum_medmst med  ";
				$sql .= "where med.yj_cd = '$csv[0]'  ";
				$sql .= "OR    ( ";
				$sql .= "            med.yj_cd = '$csv[0]'  ";
				$sql .= "        and med.maker_name = '$csv[1]' ";
				$sql .= "      )  ";
				$sql .= "order by med.maker_name desc, med.hot13_cd ";
			}

			//HOTコード用
			if($code_sel == 2){
				$sql  = "select  ";
				$sql .= "    med.hanbai_name ";
				$sql .= "  , med.hot13_cd ";
				$sql .= "  , med.kokuji_name ";
				$sql .= "  , med.zaikei_bunrui ";
				$sql .= "  , med.seibun_bunrui ";
				$sql .= "  , med.yakkou_bunrui_cd  ";
				$sql .= "  , med.maker_name  ";
				$sql .= "from sum_medmst med  ";
				$sql .= "where med.hot13_cd = '$csv[0]'  ";
				$sql .= "OR    ( ";
				$sql .= "            med.hot13_cd = '$csv[0]'  ";
				$sql .= "        and med.maker_name = '$csv[1]' ";
				$sql .= "      )  ";
				$sql .= "order by med.maker_name desc, med.hot13_cd ";
			}


			$sel = select_from_table($con, $sql, "", $fname);
			$row = pg_fetch_row($sel,0);
			if($row[5] == ""){
				$sql  = "select maker_name from sum_medmst where hot13_cd = '$row[0]' and maker_name is not null order by  hot13_cd";
				$sel = select_from_table($con, $sql, "", $fname);
				$row[5] = pg_fetch_result($sel,0,0);
			}
			//==============================
			// 採用薬マスタ検索
			//==============================
			$cnt = 0;

			/*	$sql  = "select count(*) as cnt from sum_med_saiyou_mst";
				$sql .= " where med_name = '$row[0]'";
				$sql .= " and   kokuji_name = '$row[2]'";
				$sql .= " and   zaikei_bunrui = '$row[3]'";
				$sql .= " and   seibun_bunrui = '$row[4]'";
				$sql .= " and   yakkou_bunrui_cd = '$row[5]'";
				$sel = select_from_table($con, $sql, "", $fname);
				$cnt = pg_fetch_result($sel,0,0);
			*/
/*
			if ($cnt == 0){
				$sel = select_from_table($con, "select nextval('sum_med_saiyou_mst_seq')", "", $fname);
				$seq = (int)pg_fetch_result($sel, 0, 0);
				$sql =
					" insert into sum_med_saiyou_mst (".
					" seq".
					",med_name".
					",hot13_cd".
					",kokuji_name".
					",zaikei_bunrui".
					",seibun_bunrui".
					",yakkou_bunrui_cd".
					",is_visible_at_selector".
					",remark".
					",units_separated_tab".
					",used_counter".
					",sort_order".
					",is_user_data".
					" ) values (".
					" ".$seq.
					",'".pg_escape_string($row[0])."'". //  med_name
					",'".pg_escape_string($row[1])."'". //  hot13_cd
					",'".pg_escape_string($row[2])."'". //  kokuji_name
					",'".pg_escape_string($row[3])."'". //  zaikei_bunrui
					",'".pg_escape_string($row[4])."'". //  seibun_bunrui
					",'".pg_escape_string($row[5])."'". //  yakkou_bunrui_cd
					",1".	 //  is_visible_at_selector
					",''". //  remark
					",''". //  units_separated_tab
					",1".	 //  used_counter
					",0".	 //  sort_order
					",0".	 //  is_user_data
					")";
				$upd = pg_exec($con, $sql);
				if (!@$upd){
					$errmsg[] = "(".$rowcnt."行目) 薬品名データの新規登録に失敗しました。（".$csv[10]." " .$sql."）";
					continue;
				}

				$sql =
					" update sum_medmst set".
					" is_user_data = 1".
					" where hot13_cd = '" . pg_escape_string($row[1])."'";
				$upd = pg_exec($con, $sql);
				if (!@$upd){
					$errmsg[] = "(".$rowcnt."行目) データの更新に失敗しました。（".$csv[0]."）";
					continue;
				}

				$regcnt++;
			} else {
*/				$sql =
					" update sum_medmst set".
					" is_user_data = 1".
					" where hot13_cd = '" . pg_escape_string($row[1])."'";
				$upd = pg_exec($con, $sql);
				if (!@$upd){
					$errmsg[] = "(".$rowcnt."行目) データの更新に失敗しました。（".$csv[0]."）";
					continue;
				}

				$sql  = " update sum_med_saiyou_mst set ";
				$sql .= "   med_name = '$row[0]' ";
				//$sql .= " , hot13_cd = '$row[1]' ";
				$sql .= " , kokuji_name = '$row[2]' ";
				$sql .= " , zaikei_bunrui = '$row[3]' ";
				$sql .= " , seibun_bunrui = '$row[4]' ";
				$sql .= " , yakkou_bunrui_cd = '$row[5]' ";
				$sql .= " , is_visible_at_selector = 1 ";
				$sql .= " , used_counter = 1 ";
				$sql .= " where med_name = '$row[0]'";
				$sql .= " and   kokuji_name = '$row[2]'";
				if($code_sel == 2) $sql .= " and   hot13_cd = '$row[1]'";
				$sql .= " and   zaikei_bunrui = '$row[3]'";
				$sql .= " and   seibun_bunrui = '$row[4]'";
				$sql .= " and   yakkou_bunrui_cd = '$row[5]'";
				$upd = pg_exec($con, $sql);
				unset($medmst_keylist[$csv[0]]);
				if (!@$upd){
					$errmsg[] = "(".$rowcnt."行目) データの更新に失敗しました。（".$csv[0]."）";
					continue;
				}
				$updcnt++;
			}
	//}
		break;
	}

	//==============================
	// 完了。ログを格納して、画面遷移用のHTMLを出力して終了。
	//==============================
	$upd = pg_exec($con, "update summary_option set medmst_upload_message = 'しばらくお待ちください。'");
	$seq = 0;
	if ($mstcnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'mst_count','".$mstcnt."');");
	}
	if ($rowcnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'row_count','".$rowcnt."');");
	}
	if ($regcnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'reg_count','".$regcnt."');");
	}
	if ($updcnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'upd_count','".$updcnt."');");
	}
	if ($saicnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'sai_count','".$saicnt."');");
	}
	$errcnt = count($errmsg);
	if ($errcnt > 100) $errcnt = 999999;
	if ($errcnt){
		$seq++;
		$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'err_count','".$errcnt."');");
		$displen = min($errcnt, 100);
		for ($i = 0; $i < $displen; $i++){
			$seq++;
			$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'err_msg','・ ".pg_escape_string($errmsg[$i])."');");
		}
		if ($errcnt > 100){
			$seq++;
			$upd = pg_exec($con, "insert into sum_medmst_import_log (seq, kubun, message) values (".$seq.",'err_msg','……………（以下省略）');");
		}
	}
	//==============================
	// 画面遷移用のHTMLを出力して終了。
	//==============================
?>

<html>
	<head>
		<script type="text/javascript">
			function loaded() { window.parent.location.href = "sum_ordsc_med_saiyou_import.php?session=<?=$session?>&mode=finished_import"; }
		</script>
	</head>
	<body onload="loaded();"></body>
</html>
<?
	die;
}













//****************************************************************************
// 画面描画
//****************************************************************************

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];
// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];
// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <? echo($med_report_title); ?>管理 | 処方・注射オーダ</title>

<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td { border:#5279a5 solid 1px; text-align:center }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_ORDSC"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">


<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<?//****************************************************************************?>
<?// 処方・注射メニュー                                                         ?>
<?//****************************************************************************?>
<?= summary_common_show_ordsc_right_menu($session, "薬品")?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<?//****************************************************************************?>
<?// サブメニュー                                                               ?>
<?//****************************************************************************?>

<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px">
	<tr height="22">
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_list.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>薬品名一覧</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_regist.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>新規登録/編集</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#e8e4bd">
			<a href="sum_ordsc_med_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>インポート</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td width="100" align="center" bgcolor="#958f28">
			<a href="sum_ordsc_med_saiyou_import.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>採用薬一括登録</b></font></a>
		</td>
		<td width="5">&nbsp;</td>
		<td></td>
	</tr>
</table>



<? //**************************************************************************** ?>
<? // インポート状況のリアルタイムチェック                                        ?>
<? //**************************************************************************** ?>
<script type="text/javascript">
	var ajaxObj = null;
	var currentMessage = "処理を開始しています";

	function startImport(){
		document.frm.btn_import.disabled = "disabled";
		document.getElementById('result_div').style.display='none';
		document.getElementById("status_div").style.display = '';
		document.frm.submit();
		inquiryImportCheck();
		dotCounter(5);
	}

	function dotCounter(cnt){
		var dots = "";
		for(var i = 0; i < cnt; i++) dots += "...";
		cnt++;
		if (cnt > 10) cnt = 1;
		setTimeout("dotCounter("+cnt+")", 600);
		document.getElementById("current_status_div").innerHTML = currentMessage + dots;
	}

	function inquiryImportCheck(){
		if (window.XMLHttpRequest) ajaxObj = new XMLHttpRequest();
		else {
			try { ajaxObj = new ActiveXObject("Msxml2.XMLHTTP"); }
			catch(e) { ajaxObj = new ActiveXObject("Microsoft.XMLHTTP"); }
		}
	  ajaxObj.onreadystatechange = returnImportCheck;
	  ajaxObj.open("POST", "sum_ordsc_med_saiyou_import.php", true);
	  ajaxObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	  ajaxObj.send("session=<?=$session?>&give_me=current_status");
	}

	function returnImportCheck(){
		if (ajaxObj == null) return;
		if (ajaxObj.readyState != 4) return;
		if (typeof(ajaxObj.status) != "number") return;
		if (ajaxObj.status != 200) return;
		if (!ajaxObj.responseText) return;
		try{ var ret = false; eval("ret = " + ajaxObj.responseText); if (ret.error) { alert(ret.error); return; }}
		catch(e){ alert("[response invalid]\n"+ajaxObj.responseText); return; }
		if (!ret) return;
		currentMessage = ret.msg;
		document.getElementById("current_status_div").style.display = '';
		ajaxObj = null;
		setTimeout("inquiryImportCheck()", 3000);
	}
</script>



<? //**************************************************************************** ?>
<? // アップロード                                                                ?>
<? //**************************************************************************** ?>
<div style="padding:6px; background-color:#e3ecf4; margin-top:4px"><?=$font?>
	・ 採用薬一括登録用のCSVファイルを指定してください。（Shift_JIS・20Mbまで）
</font></div>

<div>
<?=$font?>
<form action="sum_ordsc_med_saiyou_import.php" method="post" name="frm" style="margin-top:4px" enctype="multipart/form-data" target="import_frame">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="import">
<?
  if($code_sel == "2"){
?>
  YJコード<input type="radio" name="code_sel" id="code_sel" value="1"  /></font>
  HOTコード<input type="radio" name="code_sel" id="code_sel" value="2" checked /></font></br>
<?
  }
  else {
?>
  YJコード<input type="radio" name="code_sel" id="code_sel" value="1"  checked /></font>
  HOTコード<input type="radio" name="code_sel" id="code_sel" value="2"  /></font></br>
<? } ?>
  <input type="file" name="upfile" size="100">
  <input type="button" value="インポート" onclick="startImport();" name="btn_import" /></br>
  </br>初期化してインポート<input type="checkbox" name="remove_sel" id="remove_sel" value="t"  /></font></br>
  <font color="red" >※ 初期化する場合は時間がかかる場合があります。</font>

</form>
</font>
</div>

</br>
CSVレイアウト<BR>
　YJコードもしくはHOTコード,製造会社名称,薬品名称<BR>
　例<BR>
　3229005F1080,田辺三菱,アスパラカリウム錠300mg<BR>
　3214001F1020,田辺三菱,アスパラ-CA錠200<BR>
　3969003F1026,バイエル,グルコバイ錠50mg<BR>
<BR>

<?=$font?>
	<div id="status_div" style="margin-top:20px; display:none; padding:8px;">完了まで、このまましばらくお待ちください。</div>
	<div id="current_status_div" style="margin:4px; background-color:#fcffbf; padding:8px; display:none"></div>
</font>








<? //**************************************************************************** ?>
<? // 完了通知                                                                    ?>
<? //**************************************************************************** ?>
<?
	$errmsg = array();
	$counts = array();
	if ($_REQUEST["mode"]=="finished_import"){
		$sel = select_from_table($con, "select kubun, message from sum_medmst_import_log order by seq", "", $fname);
		while($row = pg_fetch_array($sel)) {
			$kubun = $row["kubun"];
			if ($kubun != "err_msg") $counts[$kubun] = $row["message"];
			else $errmsg[] = $row["message"];
		}
	}
?>

<?=$font?>
<div id="result_div">

	<? if ($_REQUEST["mode"]=="finished_import"){ ?>
	<div style="margin:4px; margin-top:20px; background-color:#fcffbf; padding:8px"><b>処理が完了しました。</b></div>
	<? } ?>

	<? if (@$counts["row_count"]){ ?>
	<div style="margin:4px; margin-top:10px">--- <?=max(0, ((int)$counts["row_count"]))?>件の処理を行いました。</div>
	<? } ?>

	<? if (@$counts["reg_count"]) { ?>
	<div style="margin:4px; margin-top:10px">--- <?=$counts["reg_count"]?>件の薬品情報が新規登録されました。</div>
	<? } ?>

	<? if (@$counts["upd_count"]) { ?>
	<div style="margin:4px; margin-top:10px">--- 既存<?=$counts["mst_count"]?>件のレコード中、<?=$counts["upd_count"]?>件の薬品情報の更新が行われました。</div>
	<? } ?>

	<? if (@$counts["sai_count"]) { ?>
	<div style="margin:4px; margin-top:10px">--- <?=$counts["sai_count"]?>件の薬剤名が新規登録されました。</div>
	<? } ?>

	<? $errlen = (int)@$counts["err_count"]; ?>
	<? if ($errlen) { ?>
	<div style="margin:4px; background-color:#fdccee;">
		<div style="padding:4px; background-color:#8b0563; color:#fff"><b><?=$errlen==999999 ? "100件を超えるエラーが発生しました。" : $errlen."件のエラーが発生しました。"?></b></div>
		<div style="margin:4px; padding:8px">
		<? foreach($errmsg as $msg) echo $msg."<br/>\n"; ?>
		</div>
	</div>
	<? } ?>

</font>
</div>




</td>
<!-- right -->
</tr>
</table>
<iframe name="import_frame" style="display:none"></iframe>
</body>
<? pg_close($con); ?>
</html>
