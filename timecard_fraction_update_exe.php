<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (($fraction1 != "--" && $fraction1_min == "--") || ($fraction2 != "--" && $fraction2_min == "--") || ($fraction4 != "--" && $fraction4_min == "--") || ($fraction5 != "--" && $fraction5_min == "--")) {
	echo("<script type=\"text/javascript\">alert('端数処理を行う場合は分を選択してください。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

if ($fraction1 == "--") {
	$fraction1 = "";
}
if ($fraction1_min == "--") {
	$fraction1_min = "";
}
if ($fraction2 == "--") {
	$fraction2 = "";
}
if ($fraction2_min == "--") {
	$fraction2_min = "";
}
if ($fraction3 == "--") {
	$fraction3 = "";
}
if ($fraction4 == "--") {
	$fraction4 = "";
}
if ($fraction4_min == "--") {
	$fraction4_min = "";
}
if ($fraction5 == "--") {
	$fraction5 = "";
}
if ($fraction5_min == "--") {
	$fraction5_min = "";
}
if ($fraction_over_unit == "--") {
	$fraction_over_unit = "";
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// タイムカードレコードがなければ作成、あれば更新
$sql = "select count(*) from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) == 0) {
	$sql = "insert into timecard (fraction1, fraction1_min, fraction2, fraction2_min, fraction3, fraction4, fraction4_min, fraction5, fraction5_min, fraction_over_unit) values (";
	$content = array($fraction1, $fraction1_min, $fraction2, $fraction2_min, $fraction3, $fraction4, $fraction4_min, $fraction5, $fraction5_min, $fraction_over_unit);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update timecard set";
	$set = array("fraction1", "fraction1_min", "fraction2", "fraction2_min", "fraction3", "fraction4", "fraction4_min", "fraction5", "fraction5_min", "fraction_over_unit");
	$setvalue = array($fraction1, $fraction1_min, $fraction2, $fraction2_min, $fraction3, $fraction4, $fraction4_min, $fraction5, $fraction5_min, $fraction_over_unit);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 端数処理画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_fraction.php?session=$session';</script>");
?>
