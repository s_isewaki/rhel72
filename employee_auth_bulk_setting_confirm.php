<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="employee_auth_bulk_setting.php" method="post">
<?
foreach ($_POST as $key => $val) {
	if (is_array($val)) {
		foreach ($val as $val2) {
			echo("<input type=\"hidden\" name=\"{$key}[]\" value=\"$val2\">\n");
		}
	} else {
		echo("<input type=\"hidden\" name=\"$key\" value=\"$val\">\n");
	}
}
?>
<input type="hidden" name="result" value="">
<input type="hidden" name="hid_model_emp_name" value="<? echo($hid_model_emp_name); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($tgt_job)) {
	echo("<script type=\"text/javascript\">alert('職種が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!is_array($tgt_st)) {
	echo("<script type=\"text/javascript\">alert('役職が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// トランザクションを開始
pg_query($con, "begin");
//XX mysql_query("start transaction", $con_mysql);

// 処理対象の職員一覧を取得
$cond_detail = "";
if (is_array($tgt_job)) {
	$cond_detail .= " and emp_job in (" . implode(", ", $tgt_job) . ")";
}
if (is_array($tgt_st)) {
	$cond_detail .= " and emp_st in (" . implode(", ", $tgt_st) . ")";
}
if ($tgt_class != "") {
	$cond_detail .= " and emp_class = $tgt_class";
}
if ($tgt_atrb != "") {
	$cond_detail .= " and emp_attribute = $tgt_atrb";
}
if ($tgt_dept != "") {
	$cond_detail .= " and emp_dept = $tgt_dept";
}
if ($tgt_room != "") {
	$cond_detail .= " and emp_room = $tgt_room";
}
$cond_detail = substr($cond_detail, 5);
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, login.emp_login_id, login.emp_login_pass from (select emp_id, emp_lt_nm, emp_ft_nm from empmst where $cond_detail) empmst inner join (select emp_id, emp_login_id, emp_login_pass from login) login on login.emp_id = empmst.emp_id inner join (select emp_id from authmst where emp_del_flg = 'f') authmst on authmst.emp_id = empmst.emp_id";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query("rollback");
	pg_close($con);
//XX 	mysql_query("rollback", $con_mysql);
//XX 	mysql_close($con_mysql);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$employees = pg_fetch_all($sel);

// 権限グループ設定の場合
if ($action_mode == "1") {

	// 該当職員の権限グループを更新
	foreach ($employees as $tmp_employee) {
//XX		auth_change_group_of_employee($con, $con_mysql, $tmp_employee["emp_id"], $group_id, $fname);
		auth_change_group_of_employee($con, $tmp_employee["emp_id"], $group_id, $fname);
	}

// それ以外の場合
} else {

	// 選択された権限を配列化
	$emp_auth = array();
	auth_set_overwrite($webml,      "emp_webml_flg",     $emp_auth);
	auth_set_overwrite($bbs,        "emp_bbs_flg",       $emp_auth);
	auth_set_overwrite($attditem,   "emp_attditem_flg",  $emp_auth);
	auth_set_overwrite($schd,       "emp_schd_flg",      $emp_auth);
	auth_set_overwrite($wkflw,      "emp_wkflw_flg",     $emp_auth);
	auth_set_overwrite($phone,      "emp_phone_flg",     $emp_auth);
	auth_set_overwrite($attd,       "emp_attdcd_flg",    $emp_auth);
	auth_set_overwrite($qa,         "emp_qa_flg",        $emp_auth);
	auth_set_overwrite($aprv_use,   "emp_aprv_flg",      $emp_auth);
	auth_set_overwrite($pass_flg,   "emp_pass_flg",      $emp_auth);
	auth_set_overwrite($adbk,       "emp_adbk_flg",      $emp_auth);
	auth_set_overwrite($empif,      "emp_empif_flg",     $emp_auth);
	auth_set_overwrite($reg,        "emp_reg_flg",       $emp_auth);
	auth_set_overwrite($conf,       "emp_conf_flg",      $emp_auth);
	auth_set_overwrite($resv,       "emp_resv_flg",      $emp_auth);
	auth_set_overwrite($tmgd,       "emp_tmgd_flg",      $emp_auth);
	auth_set_overwrite($schdplc,    "emp_schdplc_flg",   $emp_auth);
	auth_set_overwrite($ward,       "emp_ward_flg",      $emp_auth);
	auth_set_overwrite($ward_reg,   "emp_ward_reg_flg",  $emp_auth);
	auth_set_overwrite($ptreg,      "emp_ptreg_flg",     $emp_auth);
	auth_set_overwrite($ptif,       "emp_ptif_flg",      $emp_auth);
	auth_set_overwrite($dept_reg,   "emp_dept_flg",      $emp_auth);
	auth_set_overwrite($news,       "emp_news_flg",      $emp_auth);
	auth_set_overwrite($master,     "emp_master_flg",    $emp_auth);
	auth_set_overwrite($job_flg,    "emp_job_flg",       $emp_auth);
	auth_set_overwrite($status_flg, "emp_status_flg",    $emp_auth);
	auth_set_overwrite($entity_flg, "emp_entity_flg",    $emp_auth);
	auth_set_overwrite($work,       "emp_work_flg",      $emp_auth);
	auth_set_overwrite($pjt,        "emp_pjt_flg",       $emp_auth);
	auth_set_overwrite($lib,        "emp_lib_flg",       $emp_auth);
	auth_set_overwrite($config,     "emp_config_flg",    $emp_auth);
	auth_set_overwrite($biz,        "emp_biz_flg",       $emp_auth);
	auth_set_overwrite($inout,      "emp_inout_flg",     $emp_auth);
	auth_set_overwrite($dishist,    "emp_dishist_flg",   $emp_auth);
	auth_set_overwrite($outreg,     "emp_outreg_flg",    $emp_auth);
	auth_set_overwrite($disreg,     "emp_disreg_flg",    $emp_auth);
	auth_set_overwrite($hspprf,     "emp_hspprf_flg",    $emp_auth);
	auth_set_overwrite($lcs,        "emp_lcs_flg",       $emp_auth);
	auth_set_overwrite($fcl,        "emp_fcl_flg",       $emp_auth);
	auth_set_overwrite($wkadm,      "emp_wkadm_flg",     $emp_auth);
	auth_set_overwrite($cmt,        "emp_cmt_flg",       $emp_auth);
	auth_set_overwrite($cmtadm,     "emp_cmtadm_flg",    $emp_auth);
	auth_set_overwrite($memo,       "emp_memo_flg",      $emp_auth);
	auth_set_overwrite($newsuser,   "emp_newsuser_flg",  $emp_auth);
	auth_set_overwrite($inci,       "emp_inci_flg",      $emp_auth);
	auth_set_overwrite($rm,         "emp_rm_flg",        $emp_auth);
	auth_set_overwrite($fplus,      "emp_fplus_flg",     $emp_auth);
	auth_set_overwrite($fplusadm,   "emp_fplusadm_flg",  $emp_auth);
	auth_set_overwrite($ext,        "emp_ext_flg",       $emp_auth);
	auth_set_overwrite($extadm,     "emp_extadm_flg",    $emp_auth);
	auth_set_overwrite($intram,     "emp_intram_flg",    $emp_auth);
	auth_set_overwrite($stat,       "emp_stat_flg",      $emp_auth);
	auth_set_overwrite($allot,      "emp_allot_flg",     $emp_auth);
	auth_set_overwrite($life,       "emp_life_flg",      $emp_auth);
	auth_set_overwrite($intra,      "emp_intra_flg",     $emp_auth);
	auth_set_overwrite($ymstat,     "emp_ymstat_flg",    $emp_auth);
	auth_set_overwrite($med,        "emp_med_flg",       $emp_auth);
	auth_set_overwrite($qaadm,      "emp_qaadm_flg",     $emp_auth);
	auth_set_overwrite($medadm,     "emp_medadm_flg",    $emp_auth);
	auth_set_overwrite($link,       "emp_link_flg",      $emp_auth);
	auth_set_overwrite($linkadm,    "emp_linkadm_flg",   $emp_auth);
	auth_set_overwrite($bbsadm,     "emp_bbsadm_flg",    $emp_auth);
	auth_set_overwrite($libadm,     "emp_libadm_flg",    $emp_auth);
	auth_set_overwrite($adbkadm,    "emp_adbkadm_flg",   $emp_auth);
	auth_set_overwrite($search,     "emp_search_flg",    $emp_auth);
	auth_set_overwrite($webmladm,   "emp_webmladm_flg",  $emp_auth);
	auth_set_overwrite($shift,      "emp_shift_flg",     $emp_auth);
	auth_set_overwrite($shiftadm,   "emp_shiftadm_flg",  $emp_auth);
	auth_set_overwrite($cas,        "emp_cas_flg",       $emp_auth);
	auth_set_overwrite($casadm,     "emp_casadm_flg",    $emp_auth);
	auth_set_overwrite($jnl,        "emp_jnl_flg",       $emp_auth);
	auth_set_overwrite($jnladm,     "emp_jnladm_flg",    $emp_auth);
	auth_set_overwrite($nlcs,       "emp_nlcs_flg",      $emp_auth);
	auth_set_overwrite($nlcsadm,    "emp_nlcsadm_flg",   $emp_auth);
	auth_set_overwrite($manabu,     "emp_manabu_flg",    $emp_auth);
	auth_set_overwrite($manabuadm,  "emp_manabuadm_flg", $emp_auth);
	auth_set_overwrite($kview,      "emp_kview_flg",     $emp_auth);
	auth_set_overwrite($kviewadm,   "emp_kviewadm_flg",  $emp_auth);
	auth_set_overwrite($ptadm,      "emp_ptadm_flg",     $emp_auth);
	auth_set_overwrite($pjtadm,     "emp_pjtadm_flg",    $emp_auth);
	auth_set_overwrite($amb,        "emp_amb_flg",       $emp_auth);
	auth_set_overwrite($ambadm,     "emp_ambadm_flg",    $emp_auth);
	auth_set_overwrite($jinji,      "emp_jinji_flg",     $emp_auth);
	auth_set_overwrite($jinjiadm,   "emp_jinjiadm_flg",  $emp_auth);
	auth_set_overwrite($ladder,     "emp_ladder_flg",    $emp_auth);
	auth_set_overwrite($ladderadm,  "emp_ladderadm_flg", $emp_auth);
	auth_set_overwrite($shift2,     "emp_shift2_flg",    $emp_auth);
	auth_set_overwrite($shift2adm,  "emp_shift2adm_flg", $emp_auth);
	auth_set_overwrite($cauth,      "emp_cauth_flg",     $emp_auth);
	auth_set_overwrite($aclg,       "emp_accesslog_flg", $emp_auth);
	auth_set_overwrite($career,     "emp_career_flg",    $emp_auth);
	auth_set_overwrite($careeradm,  "emp_careeradm_flg", $emp_auth);
	auth_set_overwrite($ccusr1,     "emp_ccusr1_flg",    $emp_auth);
	auth_set_overwrite($ccadm1,     "emp_ccadm1_flg",    $emp_auth);

	// 権限追加設定の場合
	if ($action_mode == "2") {

		if (!count($emp_auth)) {
			echo("<script type=\"text/javascript\">alert('権限チェックボックスが選択されていません。');</script>");
			echo("<script type=\"text/javascript\">document.items.submit();</script>");
			exit;
		}

		// 管理権限とその他権限の整合性を取る
		auth_fix_relations($emp_auth);

		// 該当職員の権限情報を更新
		foreach ($employees as $tmp_employee) {
			$sql = "update authmst set";
			$set = array_keys($emp_auth);
			$setvalue = array_values($emp_auth);
			$cond = "where emp_id = '{$tmp_employee["emp_id"]}'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query("rollback");
				pg_close($con);
//XX				mysql_query("rollback", $con_mysql);
//XX				mysql_close($con_mysql);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

//XX			// XOOPSデータを更新
//XX			auth_update_xoops($con, $con_mysql, $tmp_employee["emp_id"], $fname);
		}

	// 権限再設定の場合
	} elseif ($action_mode == "3") {

		// 該当職員の追加権限情報を更新
		foreach ($employees as $tmp_employee) {
//XX			auth_change_extra_of_employee($con, $con_mysql, $tmp_employee["emp_id"], $emp_auth, $fname);
			auth_change_extra_of_employee($con, $tmp_employee["emp_id"], $emp_auth, $fname);
		}
	}
}

// 整合性のチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname, false);
auth_check_consistency($con, $session, $fname, false);

// トランザクションをコミット
pg_query($con, "commit");
//XX mysql_query("commit", $con_mysql);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 権限一括設定画面を再表示
echo("<script type=\"text/javascript\">document.items.result.value = 't';</script>");
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
