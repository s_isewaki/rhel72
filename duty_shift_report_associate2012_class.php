<?
//<!-- ***************************************************************** -->
//<!-- 勤務シフト作成　届出書添付書類画面 様式９ ＣＬＡＳＳ PHP Excel対応-->
//<!-- 看護協会版届出様式「一般病棟・療養病棟１・２入院基本料金表」      -->
//<!--			２０１２年改訂版			       -->
//<!-- ***************************************************************** -->

	//
	//
	//	末尾に行変数の説明があります
	//
	//
require_once("about_comedix.php");
require_once("duty_shift_common_class.php");
require_once("duty_shift_time_common_class.php");
require_once("duty_shift_report_common_class.php");

class duty_shift_report_associate2012_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;	// 勤務シフト作成共通クラス
	var $obj_time;	// 勤務シフト作成 時刻 共通クラス
	var $obj_rpt;	// 勤務シフト作成 様式9 共通クラス
	var $excelColumnName = array(); // Excelカラム名配列
	var $MSPgothic;			// フォント指定用、MSP-ゴシック
	var $MSPmincho;			// フォント指定用、MSP-明朝
	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_report_associate2012_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
		$this->obj_time = new duty_shift_time_common_class($con, $fname);
		$this->obj_rpt = new duty_shift_report_common_class($con, $fname);
		$this->excelColumnName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN');
		$this->MSPgothic = mb_convert_encoding('ＭＳ Ｐゴシック','UTF-8','EUC-JP');
		$this->MSPmincho = mb_convert_encoding('ＭＳ Ｐ明朝','UTF-8','EUC-JP');
		$this->green = "CCFFCC";
		$this->yellow = "FFFF99";
	}

    /**
     * EXCEL OR関数を返す
     * @param type $cell
     * @param type $array
     * @return type
     */
    function excel_or($cell, $array) {
        $or = array();
        foreach ($array as $item) {
            $or[] = sprintf('%s="%s"', $cell, $item);
        }
        return 'OR(' . implode(',', $or) . ')';
    }

	//////////////////////////////////////////////////
	//						//
	//	一般病棟・療養病棟１・２、共通部分	//
	//		2012年対応			//
	//////////////////////////////////////////////////
	function publicHeader(
			$excelObj,	// ExcelObject
			$plan_result_flag, // 1予定 2実績 3予定+実績
			$date_y1,	// 作成年月日
			$date_m1 ,
			$date_d1 ,
			$hospital_name,	// 保険医療機関名
			$ward_cnt ,	// 病棟数
			$sickbed_cnt,	// 病床数
            $report_kbn   //届出区分
	){
		// Excel基本書式設定
		// 印刷
        $excelObj->SetSheetName("ns100");
        $excelObj->PaperSize("A3");
		$excelObj->PageSetUp("horizontial");
		$excelObj->PageRatio("USER","65");
		$excelObj->SetMargin( 1 , 0.5 , 1.5 , 1.5 , 0.5 , 0.5 ); // 左右上下頭足
		// 画面表示
		$excelObj->DisplayZoom( "65" ); // 65%表示
		$excelObj->SetGridline( "true" ); // 枠線表示

		// 列幅 --- 隠し列のAW-AZ列は最後に0行設定
		$excelObj->SetColDim("A",1.13);
		$excelObj->SetColDim("B",7.50);
		$excelObj->SetColDim("C",6.25);
		$excelObj->SetColDim("D",6.25);
		$excelObj->SetColDim("E",18);
		$excelObj->SetColDim("F",3.25);
		$excelObj->SetColDim("G",3.75);
		$excelObj->SetColDim("H",4);
		$excelObj->SetColDim("I",6.63);
		$excelObj->SetColDim("J",2.88);
		$excelObj->SetColDim("K",2.88);
		$excelObj->SetColDim("L",2.88);
		$excelObj->SetColDim("M",7.13);
		$excelObj->SetColDim("N",8.38);
		for($col=14; $col<45; $col++){ // O:AS列
			$excelObj->SetColDim($this->excelColumnName[$col],5);
		}
		$excelObj->SetColDim("AT",8.38);
		$excelObj->SetColDim("AU",8.38);
		$excelObj->SetColDim("AV",8.38);
		$excelObj->SetColDim("AW",8.38);

		// 帳票共通出力
		// 1行目
		$excelObj->SetRowDim(1,18.75);

        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("B1","(様式９の３)地域包括ケア病棟入院料等の施設基準に係る届出書添付書類","BOLD",$this->MSPgothic,14);
        }
        else {
            $excelObj->SetValueJP2("C1","(様式９)入院基本料の施設基準等に係る届出書添付書類","BOLD",$this->MSPgothic,14);
        }
        $excelObj->SetValueJP2("O1","作成年月日","VCENTER",$this->MSPgothic,11);
		$excelObj->SetValuewith("R1", $date_y1 , "VCENTER");$excelObj->SetValueJP2("S1","年","VCENTER",$this->MSPgothic,11);
		$excelObj->SetValuewith("T1", $date_m1 , "VCENTER");$excelObj->SetValueJP2("U1","月","VCENTER",$this->MSPgothic,11);
		$excelObj->SetValuewith("V1", $date_d1 , "VCENTER");$excelObj->SetValueJP2("W1","日","VCENTER",$this->MSPgothic,11);
		$excelObj->SetArea("R1");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("T1");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("V1");$excelObj->SetBorder("MEDIUM","outline");

		$excelObj->SetArea("R1:W1");
			$excelObj->SetFont($this->MSPgothic,11);

		switch ($plan_result_flag){
			case 2:$excelObj->SetValueJPwith("X1","(予定から作成)","VCENTER");break;
			case 3:$excelObj->SetValueJPwith("X1","(実績と予定を組み合わせて作成 )","VCENTER");break;
			default:;
		}

		// 2行目
		$excelObj->SetRowDim(2,9);
		// 3行目
		$excelObj->SetRowDim(3,18.75);

		$excelObj->SetValueJP2("E3","保険医療機関名","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->SetArea("F3:L3");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->CellMergeReady();
		$excelObj->SetValueJPwith("F3", $hospital_name ,"VCENTER" );

		$excelObj->SetValueJP2("N3","病棟数","VCENTER",$this->MSPgothic,14);
		$excelObj->SetArea("O3:P3");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->CellMergeReady();
		$excelObj->SetValueJPwith("O3", $ward_cnt ,"VCENTER" );

		$excelObj->SetValueJP2("S3","病床数","VCENTER",$this->MSPgothic,11);
		$excelObj->SetArea("U3:V3");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->CellMergeReady();
		$excelObj->SetValueJPwith("U3", $sickbed_cnt ,"VCENTER" );

		// 4行目
		$excelObj->SetRowDim(4,11.25);
	}

	//////////////////////////////////////////
	//					//
	// --------------看護職員表------------ //
	// 		 2012年対応		//
	//////////////////////////////////////////
	function ShowNurseData1(
			$excelObj,		// PHP-Excelオブジェクト
			$start_row_number,	// 表を開始する行位置
			$obj,			// 共通クラス
			$data_array,		// 看護師／准看護師情報
			$week_array,		// 曜日配列
			$day_cnt,		// 日数
			$kango_space_line	// 看護職員表、空白行数
	){

		$arg_array = array();// 戻り値用

        // 看護師
        $nurse1_name = $this->obj_rpt->get_nurse_job_name(1);
        // 准看護師
        $nurse2_name = $this->obj_rpt->get_nurse_job_name(2);

		// 追加する空白行にゴミを出さないよう、追加行分の内容をクリア
		if( $kango_space_line != 0 ){
			$add_line_count = count($data_array) + $kango_space_line;
			for ( $r=count($data_array); $r<$add_line_count; $r++){
				$data_array[$i]["job_name"] = "";
				$data_array[$i]["main_group_name"] = "";
				$data_array[$i]["staff_name"] = "";
				$data_array[$i]["duty_form"] = "";
				$data_array[$i]["other_post_flg"] = "";
				$data_array[$i]["night_duty_flg"] = "";
				for($k=1; $k<=$day_cnt; $k++) {
					$data_array[$i]["duty_time_$k"]= "";;
					$data_array[$i]["night_time_$k"]= "";;
				}
			}
		}
		$RowNumber   = $start_row_number;
		$KangoRowTop = $start_row_number + 2; // 看護職員表開始のEXCEL行番号
		$MidashiRow1 = $KangoRowTop;
		$MidashiRow2 = $MidashiRow1 + 1;
		$MidashiRow3 = $MidashiRow1 + 2;
		$DayCntRow   = $KangoRowTop - 4;
		$LaborCntRow = $KangoRowTop - 3;
		// 看護職員表の見出し

		// 表名
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("B".$RowNumber,"【勤務計画表】","BOLD VCENTER",$this->MSPgothic ,16);

		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("B".$RowNumber,"《看護職員表》","BOLD VCENTER",$this->MSPgothic ,14);

		// 行高さ設定
		$excelObj->SetRowDim($MidashiRow1,54);
		$excelObj->SetRowDim($MidashiRow2,13.5);
		$excelObj->SetRowDim($MidashiRow3,13.5);

		// 見出し
		$excelObj->SetValueJPwith("B".$MidashiRow1,"種別","BOLD");
		$excelObj->CellMerge("B".$MidashiRow1.":B".$MidashiRow3);

		$excelObj->SetValueJPwith("C".$MidashiRow1,"番号","BOLD");
		$excelObj->CellMerge("C".$MidashiRow1.":C".$MidashiRow3);

		$excelObj->SetValueJPwith("D".$MidashiRow1,"病棟名","BOLD");
		$excelObj->CellMerge("D".$MidashiRow1.":D".$MidashiRow3);

		$excelObj->SetValueJPwith("E".$MidashiRow1,"氏    名","BOLD HCENTER FITSIZE");
		$excelObj->CellMerge("E".$MidashiRow1.":E".$MidashiRow3);

		$excelObj->SetValueJPwith("F".$MidashiRow1,"雇用・勤務形態","BOLD");
		$excelObj->CellMerge("F".$MidashiRow1.":I".$MidashiRow3);

		$excelObj->SetValueJPwith("J".$MidashiRow1,"夜勤の有無","BOLD");
		$excelObj->CellMerge("J".$MidashiRow1.":L".$MidashiRow3);

		$excelObj->SetValueJP2("M".$MidashiRow1,"夜勤従事者数への計上","WRAP",$this->MSPgothic ,10);
		$excelObj->CellMerge("M".$MidashiRow1.":M".$MidashiRow3);

		$excelObj->CellMerge("N".$MidashiRow1.":N".$MidashiRow3);

		// 「日付別の勤務時間数」見出しのセルマージ領域。1ヶ月が31未満でも31日分のセルを用意する
		$excelObj->CellMerge2("O".$MidashiRow1.":AS".$MidashiRow1,"thin","bottom","");
		$excelObj->SetValueJP2("O".$MidashiRow1,"日付別の勤務時間数","BOLD VCENTER",$this->MSPgothic ,12);

		// 日数
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+13].$MidashiRow2;
			$excelObj->SetValueJPwith($cellArea , $week_array[$c]["day"]."日" , ""); //$c -> $week_array[$c]["day"] 20150421
		}
		// 曜日
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+13]."".$MidashiRow3;
			$excelObj->SetValueJPwith($cellArea ,$week_array[$c]["name"]."曜" , "");
		}

		$excelObj->CellMerge("AT".$MidashiRow1.":AT".$MidashiRow3);
		$excelObj->SetValueJP2("AT".$MidashiRow1,"月勤務時間数（延べ時間数）","WRAP",$this->MSPgothic ,10);

		$excelObj->CellMerge("AU".$MidashiRow1.":AU".$MidashiRow3);
		$excelObj->SetValueJP2("AU".$MidashiRow1,"（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数 \n（短時間正職員の場合は12時間より短いもの）","WRAP",$this->MSPgothic ,9);

		$excelObj->CellMerge("AV".$MidashiRow1.":AW".$MidashiRow2);
		$excelObj->SetValueJP2("AV".$MidashiRow1,"職種別月勤務時間数","FITSIZE",$this->MSPgothic ,11);

		// *** 隠し列 ***
		$excelObj->CellMerge("AX".$MidashiRow1.":AY".$MidashiRow2);
		$excelObj->SetValueJP2("AX".$MidashiRow1,"職種別\n常勤換算配置数","WRAP",$this->MSPgothic ,11);

		// 見える列
		$excelObj->SetValueJPwith("AV".$MidashiRow3,"看護師","WRAP");
		$excelObj->SetValueJPwith("AW".$MidashiRow3,"准看護師","WRAP");

		// ** 隠し列 **
		$excelObj->SetValueJPwith("AX".$MidashiRow3,"看護師","WRAP");
		$excelObj->SetValueJPwith("AY".$MidashiRow3,"准看護師","WRAP");

		// AU-AXまとめて処理
		$excelObj->SetArea("AV".$MidashiRow3.":AY".$MidashiRow3);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetBorder2("double","","top");

		// AY-AZ(罫線無し)
		$excelObj->SetValueJPwith("AZ".$MidashiRow3,"全体","");
		$excelObj->SetPosH("CENTER");
		$excelObj->SetValueJPwith("BA".$MidashiRow3,"夜専","");
		$excelObj->SetPosH("CENTER");

		// B-AZまとめて
		$excelObj->SetArea("B".$MidashiRow1.":BA".$MidashiRow3,$this->MSPgothic ,12);
		$excelObj->SetPosV("CENTER");
		$excelObj->SetShrinkToFit();
		$excelObj->SetArea("B".$MidashiRow1.":M".$MidashiRow3,$this->MSPgothic ,12);
		$excelObj->SetPosH("CENTER");

		//
		// データ行
		//
		$RowNumber = $KangoRowTop + 3;  // 行位置、一行ずつカウントして行く。2段表示のときは$RowNextを下段位置とする
		$KangoRowDataTop = $RowNumber ; // 看護職員表データ行開始のEXCEL行番号
		$KangoRowSmallTop = $RowNumber ; // 小計データ開始のEXCEL行番号 20150115

		// 縦集計計算式用配列
		$VSumU = array(); // 上段
		$VSumL = array(); // 下段
		for($i=0; $i<52; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumU[$i] = "=";
			$VSumL[$i] = "=";
		}
		if ( $kango_space_line != 0 ){
			$row_max = $add_line_count;
		}else{
			$row_max = count($data_array);
		}
		//小計対応 20150114
		$stotal_line = 100; //小計出力の単位
		$stotal_start = 1;
		$VSumUT = array(); // 上段
		$VSumLT = array(); // 下段
		for($i=0; $i<52; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumUT[$i] = "=";
			$VSumLT[$i] = "=";
		}
		for($i=0; $i<$row_max; $i++) {

			//小計対応 20150114
			if ($i > 0 && ($i % $stotal_line) == 0 && $row_max > 200) {
				$KangoRowSmallSum = $RowNumber ; // 看護職員表小計のExcel行番号
				$RowNext = $RowNumber + 1;
				// 左見出し
				$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);

				$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
				$CellArea = "C".$RowNumber;
				$excelObj->SetValueJPwith($CellArea, "小計" ,"LEFT");
				$excelObj->SetPosH("LEFT");

				$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

				$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
				//小計対応 20150114
				$stotal_start_str = sprintf( '%003d' , $stotal_start );
				$stotal_end_str = sprintf( '%003d' , ($stotal_start+$stotal_line-1) );
				$stotal_start += $stotal_line;
				$name = "$stotal_start_str 〜 $stotal_end_str";
				$CellArea = "E".$RowNumber;
				$excelObj->SetValueJPwith($CellArea, $name, "");

				// 明細行内の見出し
				$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
				$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
				$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
				$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
				$CellArea="F".$RowNumber.":I".$RowNumber;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,8);
				$excelObj->SetValueJPwith("J".$RowNumber, "有","");
				$excelObj->SetValueJPwith("K".$RowNumber, "無","");
				$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
				$CellArea="J".$RowNumber.":M".$RowNumber;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
				$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
				$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

				for($k=5; $k<=12; $k++) { // F〜M列
					$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$k].$RowNext;
					$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
				}
				$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

				for($k=1; $k<=$day_cnt; $k++) { // 日毎
					$colPos = $k + 13; // N列から->O列から、へ変更2012.4.27
					// 上段
					$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$colPos].$RowNumber;
					$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
					// 下段
					$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$colPos].$RowNext;
					$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
				}
				// AT列
				$colPos = 45 ;
				// 上段
				$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNumber;
				$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AU列、下段だけ
				$colPos ++;
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AV列
				$colPos ++;
				// 上段
				$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNumber;
				$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AW列
				$colPos ++;
				// 上段
				$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNumber;
				$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AX列(隠し列)下段のみ
				$colPos ++;
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AY列(隠し列)下段のみ
				$colPos ++;
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AZ-BA(隠し列)
				$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);
				$excelObj->SetValueJP2("BA".$RowNext, "小計","",$this->MSPgothic ,11);

				// 行間の横線
				$CellArea = "B".$RowNumber.":AY".$RowNumber; // 細い実線
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("thin","","top");
				$CellArea = "F".$RowNext.":AY".$RowNext; // 細い破線
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("dotted","","top");

				//小計対応 20150114
				//小計用計算式クリア
				for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
					$VSumU[$k] = "=";
					$VSumL[$k] = "=";
				}
				//合計用計算式
				for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
					$VSumUT[$k] = $VSumUT[$k].$this->excelColumnName[$k].$RowNumber."+";
					$VSumLT[$k] = $VSumLT[$k].$this->excelColumnName[$k].($RowNumber+1)."+";
				}
				
				$RowNumber += 2;
				$RowNext = $RowNumber + 1;
				// 列集計（小計）おわり。

				$KangoRowMax = $RowNumber - 1; // 看護職員表終了時のExcel行番号
				$KangoRowSum = $RowNumber ; // 看護職員表終了時のExcel行番号

				// B列のみ「小計」の行まで看護職員表まとめて設定
				// 罫線、表示位置
				$CellArea = "B".$KangoRowTop.":B".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("thin","","right");
				$excelObj->SetPosH("CENTER");

				$CellArea = "B".$KangoRowTop.":AY".$KangoRowMax; // 周囲をやや太い線で囲む。後で小計行の上下へ二重線を引く
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("medium","","outline");
				// C列より右は「計」行まで
				$KangoRowMax += 2;

				// フォントB-D列
				$CellArea = "B".$KangoRowTop.":D".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				// フォントE列
				$CellArea = "E".$KangoRowTop.":E".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,12);
				// フォントN列
				$CellArea = "N".$KangoRowTop.":N".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,10);
				// フォント、スタイル N-AY列
				$CellArea = "N".$KangoRowDataTop.":AY".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 ");

				// 色指定 J-K列
				$CellArea = "J".$KangoRowDataTop.":K".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑
				// 色指定 M列
				$CellArea = "M".$KangoRowDataTop.":M".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑
				// 色指定 AT-AY列
				$CellArea = "AT".$KangoRowDataTop.":AY".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑

				// 表示位置・縦横
				$wk = $KangoRowSmallSum - 1;
				$CellArea = "C".$KangoRowSmallTop.":C".$wk; // 右よりはデータ行。見出しは中央 20150115
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("RIGHT");
				$KangoRowSmallTop = $RowNumber + 2; //20150115

				$CellArea = "D".$KangoRowTop.":D".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("CENTER");

				$CellArea = "E".$KangoRowDataTop.":E".$KangoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("LEFT");

				// 「小計」行の横罫線引きなおして、背景黄色にする->緑へ変更
				$beforRowU = $RowNumber-2;
				$beforRowL = $RowNext-2;
				$CellArea = "B".$beforRowU.":AY".$beforRowL;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("double","","top");
				$excelObj->SetBorder2("double","","bottom");
				$excelObj->SetBackColor("CCFFCC"); // 薄い黄色->緑へ変更
			}
			$no = $i + 1;
			$RowNext = $RowNumber + 1;
			// 種別
			$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);
			$excelObj->SetValueJPwith("B".$RowNumber, $data_array[$i]["job_name"],"");

			// 番号
			$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
			$excelObj->SetValuewith("C".$RowNumber, $no,"");

			// 病棟名
			$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);
			$excelObj->SetValueJPwith("D".$RowNumber, $data_array[$i]["main_group_name"],"");
			$excelObj->SetWrapText(); // セル内折り返し

			// 氏名
			$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
			$excelObj->SetValueJPwith("E".$RowNumber, $data_array[$i]["staff_name"],"");
			$excelObj->SetShrinkToFit(); // セル内縮小して全表示

			// 明細行内の見出し
			$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
			$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");

			$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
			$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
			$CellArea="F".$RowNumber.":I".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,8);
			$excelObj->SetValueJPwith("J".$RowNumber, "有","");
			$excelObj->SetValueJPwith("K".$RowNumber, "無","");
			$excelObj->SetValueJPwith("L".$RowNumber, "夜専","");
			$CellArea="J".$RowNumber.":L".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,10);
			$excelObj->SetValueJPwith("M".$RowNumber, "夜勤従事者","");
			$excelObj->SetFont($this->MSPgothic ,8);
			$excelObj->SetValueJPwith("N".$RowNumber, "日勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10);
			$excelObj->SetValueJPwith("N".$RowNext, "夜勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10);

			// データ
			// duty_shift_report1_common_class.phpから流用
			//常勤／非常勤／他部署兼務
			//夜勤：有り／夜勤：無し／夜専
			$wk1 = '0';	// 常勤
			$wk2 = '0';	// 非常勤
			$wk3 = '0';	// 他部署兼務
//			$wk4 = "　";	// 夜勤有り→Excel計算式で求める
//			$wk5 = "　";	// 夜勤無し→Excel計算式で求める
			$wk6 = '0';	// 夜専
			$wk7 = '0';	// 短時間 2012.04.27追加
			if ($data_array[$i]["duty_form"] == "1") { $wk1 = "1"; } // 常勤
			if ($data_array[$i]["duty_form"] == "3") { $wk7 = "1"; } // 短時間 【NEW】2012.04.27
			if ($data_array[$i]["duty_form"] == "2") { $wk2 = "1"; } // 非常勤
			if ($data_array[$i]["other_post_flg"] == "1") { $wk3 = "1"; }
			//夜専以外
//			if ($data_array[$i]["duty_form"] == "3") {
//				//12以上
//				if (($wk_times_1 + $wk_times_2) >= 12) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
//			}
			if ($data_array[$i]["night_duty_flg"] != "2") {
				//16時間超を夜勤有りとする→Excel計算式で求める
//				if (($wk_times_1 + $wk_times_2) > 16) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
			} else {
				//夜専
				$wk6 = "1";
			}
			$excelObj->SetValuewith("F".$RowNext, $wk1,""); // 常勤
			$excelObj->SetValuewith("G".$RowNext, $wk7,""); // 短時間【NEW】2012.04.27
			$excelObj->SetValuewith("H".$RowNext, $wk2,""); // 非常勤
			$excelObj->SetValuewith("I".$RowNext, $wk3,""); // 他部署兼務
			$wk0 = $wk1 + $wk2 + $wk3;
			if( $wk0 == 0 ){
				$excelObj->SetArea("F".$RowNext.":I".$RowNext);
				$excelObj->SetCharColor("FFC0C0C0");
			}

			// J列
			//=IF(AND(F47=0,G47=0,H47=0,I47=0), 0, IF(OR(AND(G47=1,AT47>=12),AND(G47<>1,AT47>16)),1,0))
			$formulae = "=IF(AND(F$RowNext=0,G$RowNext=0,H$RowNext=0,I$RowNext=0), 0, IF(OR(AND(G$RowNext=1,AT$RowNext>=12),AND(G$RowNext<>1,AT$RowNext>16)),1,0))";
			$excelObj->SetValuewith("J".$RowNext, $formulae,"");

			// K列
			// =IF(AS39<=16,1,0)
			$formulae = "=IF(AND(F$RowNext=0,G$RowNext=0,H$RowNext=0,I$RowNext=0), 0, IF(J$RowNext<>1, 1, 0))";
			$excelObj->SetValuewith("K".$RowNext, $formulae,"");
			// L
			$excelObj->SetValuewith("L".$RowNext, $wk6,"");
			// M
			$CellArea="F".$RowNext.":M".$RowNext;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,11);

			// 縦集計の計算式
			$VSumL[5]  = $VSumL[5]."F".$RowNext."+";
			$VSumL[6]  = $VSumL[6]."G".$RowNext."+";
			$VSumL[7]  = $VSumL[7]."H".$RowNext."+";
			$VSumL[8]  = $VSumL[8]."I".$RowNext."+";
			$VSumL[9]  = $VSumL[9]."J".$RowNext."+";
			$VSumL[10] = $VSumL[10]."K".$RowNext."+";
			$VSumL[11] = $VSumL[11]."L".$RowNext."+";
			$VSumL[12] = $VSumL[12]."M".$RowNext."+"; // 追加 2012.4.27

			//夜勤従事者数
			//=IF(OR(L47=1, K47=1), 0, IF(AND(F47=1, G47=0, H47=0, I47=0), 1, MIN(1,(AT46+AT47)/($N$40*$N$39/7))))
			$formulae = "=IF(OR(L$RowNext=1, K$RowNext=1), 0, IF(AND(F$RowNext=1, G$RowNext=0, H$RowNext=0, I$RowNext=0), 1, MIN(1,(AT$RowNumber+AT$RowNext)/(\$N\$$LaborCntRow*\$N\$$DayCntRow/7))))";

			$excelObj->SetValuewith("M".$RowNext , $formulae, "");
			$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // M列

			// 日付別の勤務時間数（日勤時間数、夜勤時間数）duty_shift_report1_common_class.phpから流用
			// Excelなので1回のdayループで日勤(上段)と夜勤(下段)の両方を処理する
			$wk_times_1 = 0.0;
			$wk_times_2 = 0.0;
			for($k=1; $k<=$day_cnt; $k++) {
				$colPos = $k + 13; // N列から->O列からへ変更 2012.4.27
				//////////////
				// 日勤時間 //
				//////////////
				$wk = $data_array[$i]["duty_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNumber; // 上段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumU[$colPos] = $VSumU[$colPos].$CellArea."+"; // 縦集計用の計算式

				//////////////
				// 夜勤時間 //
				//////////////
				$wk = $data_array[$i]["night_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNext; // 下段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumL[$colPos] = $VSumL[$colPos].$CellArea."+"; // 縦集計用の計算式
			}
			////////////
			// 行集計 //
			////////////
			// AT列
			$colPos = 45 ;
			$CellArea = "AT".$RowNumber; // 上段
			$formulae = "=SUM(O".$RowNumber.":AS".$RowNumber.")"; // =SUM(N38:AR38)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			$CellArea = "AT".$RowNext; // 下段
			$formulae = "=SUM(O".$RowNext.":AS".$RowNext.")"; // =SUM(N39:AR39)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[$colPos] = $VSumU[$colPos]."AT".$RowNumber."+";
			$VSumL[$colPos] = $VSumL[$colPos]."AT".$RowNext."+";

			// AU列
			$colPos ++ ;
			$CellArea = "AU".$RowNext; // 下段
			// =IF(OR(L47=1,K47=1),AT47,0)
			$formulae = "=IF(OR(L$RowNext=1,K$RowNext=1),AT$RowNext,0)";
			$excelObj->SetValuewith($CellArea , $formulae, "");
			// 縦集計計算式
			$VSumL[$colPos] = $VSumL[$colPos]."AU".$RowNext."+";

			// AV列上
			$colPos ++ ;
			// =IF(B46="看護師",AT46,0)

			$formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse1_name) .",AT".$RowNumber.",0)";
			$CellArea = "AV".$RowNumber;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AV列下
			// =IF(B46="看護師",AT47,0)
			$formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse1_name) .",AT".$RowNext.",0)";
			$CellArea = "AV".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[$colPos] = $VSumU[$colPos]."AV".$RowNumber."+";
			$VSumL[$colPos] = $VSumL[$colPos]."AV".$RowNext."+";

			// AW列上
			$colPos ++ ;
			// =IF(B46="准看護師",AT46,0)
            $formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse2_name) .",AT".$RowNumber.",0)";
			$CellArea = "AW".$RowNumber;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// AW列下
			// =IF(B46="准看護師",AT47,0)
			$formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse2_name) .",AT".$RowNext.",0)";
			$CellArea = "AW".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[$colPos] = $VSumU[$colPos]."AW".$RowNumber."+";
			$VSumL[$colPos] = $VSumL[$colPos]."AW".$RowNext."+";

			// *** 隠し列 ***
			// AX列下
			$colPos ++ ;
			$CellArea = "AX".$RowNext;
			// =IF($B46="看護師",IF(AND($F47=1, $G47=0, $H47=0, $I47=0),1,MIN(1,(AV46+AV47)/($N$40*$N$39/7))),0)
			$formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse1_name) .",IF(AND(\$F$RowNext=1, \$G$RowNext=0, \$H$RowNext=0, \$I$RowNext=0),1,MIN(1,(AV$RowNumber+AV$RowNext)/(\$N\$$LaborCntRow*\$N\$$DayCntRow/7))),0)";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			$VSumL[$colPos] = $VSumL[$colPos]."AX".$RowNext."+";

			// AY列下
			$colPos ++ ;
			$CellArea = "AY".$RowNext;
			// =IF($B46="准看護師",IF(AND($F47=1, $G47=0, $H47=0, $I47=0),1,MIN(1,(AW46+AW47)/($N$40*$N$39/7))),0)
			$formulae = "=IF(". $this->excel_or("B{$RowNumber}", $nurse2_name) .",IF(AND(\$F$RowNext=1, \$G$RowNext=0, \$H$RowNext=0, \$I$RowNext=0),1,MIN(1,(AW$RowNumber+AW$RowNext)/(\$N\$$LaborCntRow*\$N\$$DayCntRow/7))),0)";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			$VSumL[$colPos] = $VSumL[$colPos]."AY".$RowNext."+";

			// AZ列下
			$CellArea = "AZ".$RowNext;
			// 	     =IF(AND(AT46	       =0,AT47		  =0), "-" ,AT47)
			$formulae = "=IF(AND(AT"."$RowNumber"."=0,AT"."$RowNext"."=0),\"-\",AT"."$RowNext".")";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// BA列下
			$CellArea = "BA".$RowNext;
			//	     =IF(AND(AT46	       =0,AT47		  =0), "-", IF(L47	      =1,AT47,		   "-"))
			$formulae = "=IF(AND(AT"."$RowNumber"."=0,AT"."$RowNext"."=0),\"-\",IF(L"."$RowNext"."=1,AT"."$RowNext".",\"-\"))";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 行間の横線
			$CellArea = "B".$RowNumber.":AY".$RowNumber; // 細い実線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("thin","","top");
			$CellArea = "F".$RowNext.":AY".$RowNext; // 細い破線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("dotted","","top");
			$RowNumber += 2;
		}
		//////////////////
		//    列集計    //
		//////////////////

		// 小計

		$KangoRowSmallSum = $RowNumber ; // 看護職員表小計のExcel行番号
		$RowNext = $RowNumber + 1;
		// 左見出し
		$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);

		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
		$CellArea = "C".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, "小計" ,"LEFT");

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		//小計対応 20150114
		$name = $row_max ;
		$name = sprintf( '%003d' , $name );
		$stotal_start_str = sprintf( '%003d' , $stotal_start );
		$name = "$stotal_start_str 〜 ".$name;
		$CellArea = "E".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, $name, "");

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
		$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":I".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8);
		$excelObj->SetValueJPwith("J".$RowNumber, "有","");
		$excelObj->SetValueJPwith("K".$RowNumber, "無","");
		$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
		$CellArea="J".$RowNumber.":M".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
		$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
		$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

		for($k=5; $k<=12; $k++) { // F〜M列
			$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$k].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 13; // N列から->O列から、へ変更2012.4.27
			// 上段
			$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
			// 下段
			$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		}
		// AT列
		$colPos = 45 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AU列、下段だけ
		$colPos ++;
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AV列
		$colPos ++;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AW列
		$colPos ++;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AX列(隠し列)下段のみ
		$colPos ++;
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AY列(隠し列)下段のみ
		$colPos ++;
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AZ-BA(隠し列)
		$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("BA".$RowNext, "小計","",$this->MSPgothic ,11);

		// 行間の横線
		$CellArea = "B".$RowNumber.":AY".$RowNumber; // 細い実線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "F".$RowNext.":AY".$RowNext; // 細い破線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","top");

		//小計対応 20150114
		//合計用計算式
		for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumUT[$k] = $VSumUT[$k].$this->excelColumnName[$k].$RowNumber;
			$VSumLT[$k] = $VSumLT[$k].$this->excelColumnName[$k].($RowNumber+1);
		}

		$RowNumber += 2;
		$RowNext = $RowNumber + 1;
		// 列集計（小計）おわり。

		$KangoRowMax = $RowNumber - 1; // 看護職員表終了時のExcel行番号
		$KangoRowSum = $RowNumber ; // 看護職員表終了時のExcel行番号

		// B列のみ「小計」の行まで看護職員表まとめて設定
		// 罫線、表示位置
		$CellArea = "B".$KangoRowTop.":B".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","right");
		$excelObj->SetPosH("CENTER");

		$CellArea = "B".$KangoRowTop.":AY".$KangoRowMax; // 周囲をやや太い線で囲む。後で小計行の上下へ二重線を引く
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
		// C列より右は「計」行まで
		$KangoRowMax += 2;

		// フォントB-D列
		$CellArea = "B".$KangoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		// フォントE列
		$CellArea = "E".$KangoRowTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,12);
		// フォントN列
		$CellArea = "N".$KangoRowTop.":N".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,10);
		// フォント、スタイル N-AY列
		$CellArea = "N".$KangoRowDataTop.":AY".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 ");

		// 色指定 J-K列
		$CellArea = "J".$KangoRowDataTop.":K".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 M列
		$CellArea = "M".$KangoRowDataTop.":M".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 AT-AY列
		$CellArea = "AT".$KangoRowDataTop.":AY".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		// 表示位置・縦横
		$wk = $KangoRowSmallSum - 1;
		$CellArea = "C".$KangoRowSmallTop.":C".$wk; // 右よりはデータ行。見出しは中央 20150115
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("RIGHT");

		$CellArea = "D".$KangoRowTop.":D".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("CENTER");

		$CellArea = "E".$KangoRowDataTop.":E".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("LEFT");

		// 「小計」行の横罫線引きなおして、背景黄色にする->緑へ変更
		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		$CellArea = "B".$beforRowU.":AY".$beforRowL;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","top");
		$excelObj->SetBorder2("double","","bottom");
		$excelObj->SetBackColor("CCFFCC"); // 薄い黄色->緑へ変更

		// 「計」開始
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);

		$excelObj->SetValueJP2("C".$RowNumber, "計" ,"LEFT",$this->MSPgothic ,11);

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
		$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":I".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8);
		$excelObj->SetValueJPwith("J".$RowNumber, "有","");
		$excelObj->SetValueJPwith("K".$RowNumber, "無","");
		$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
		$CellArea="J".$RowNumber.":M".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
		$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
		$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		for($k=5; $k<=12; $k++) { // F〜M列
			$colPos=$k;
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			//小計対応 20150114
			$formulae = $VSumLT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列
		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 13; // N列から->O列からに変更
			// 上段
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			//小計対応 20150114
			$formulae = $VSumUT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
			// 下段
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			//小計対応 20150114
			$formulae = $VSumLT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		// AT列
		// 上段
		$colPos = 45 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		//小計対応 20150114
		$formulae = $VSumUT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AU列
		$colPos ++;
		// 上段
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		//小計対応 20150114
		$formulae = $VSumUT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AV列
		// 上段
		$colPos ++;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		//小計対応 20150114
		$formulae = $VSumUT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AW列
		// 上段
		$colPos ++;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		//小計対応 20150114
		$formulae = $VSumUT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AX列(隠し列)下段のみ
		$colPos ++ ;
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AY列(隠し列)下段のみ
		$colPos ++;
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AZ-BA(隠し列)
		$excelObj->SetValueJP2("AZ".$RowNext, "計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("BA".$RowNext, "計","",$this->MSPgothic ,11);

		// 「計」行をまとめて設定。横線など
		$CellArea = "C".$RowNumber.":AY".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("hair","","vertical");
		$excelObj->SetBackColor("99CC00"); // 濃い緑 R153 G204 B0
		$excelObj->SetBorder2("medium","","bottom");

		$CellArea = "F".$RowNumber.":AX".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","bottom");

		// 縦線まとめて
		$CellArea = "C".$KangoRowTop.":AY".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");
		$excelObj->SetBorder2("hair","","vertical"); // new

		// AT列の二重線
		$CellArea = "AT".$KangoRowTop.":AT".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","left");
		$excelObj->SetBorder2("double","","right");

		// AV:AW、AX:AYのやや太い枠
		$CellArea = "AV".$KangoRowTop.":AW".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","right");

		// AZ:BAの背景色、灰色
		$wk = $KangoRowTop + 2;
		$CellArea = "AZ".$wk.":BA".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("C0C0C0"); // 灰色

		// 縮小して全体を表示する指定
		$CellArea = "E".$KangoRowDataTop.":AS".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetShrinkToFit();

		// 全体のセル内縦位置をCENTERにする
		$CellArea = "B".$KangoRowDataTop.":AS".$KangoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosV("CENTER");

		$arg_array["KangoRowTop"] = $KangoRowTop;
		$arg_array["KangoRowDataTop"] = $KangoRowDataTop;
		$arg_array["KangoRowSum"] = $KangoRowSum;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}

	//////////////////////////////////////////
	//					//
	//	一般病棟・看護職員集計		//
	// 		 2012年対応		//
	//					//
	//////////////////////////////////////////
	function ShowNurseSum01( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KangoRowSum = $arg_array["KangoRowSum"];
        $report_kbn = $arg_array["report_kbn"];
        
		$KEI_RowU = $KangoRowSum;
		$KEI_RowL = $KangoRowSum+1;

		// 中間集計１行目
		$RowNumber += 3;
		$RowNext = $RowNumber + 1;
		$KMidSumRow1 = $RowNumber;
		$KMidSumRow2 = $KMidSumRow1 + 2;
		$KMidSumRow3 = $KMidSumRow1 + 4;

		$KNext1 = $KMidSumRow1 + 1;
		$KNext2 = $KMidSumRow2 + 1;
		$KNext3 = $KMidSumRow3 + 1;

		$excelObj->SetRowDim($RowNumber,26.25);

		// 罫線はあとでまとめて
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","看護職員の勤務実績(勤務時間数)","BOLD VCENTER HCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":P".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","夜勤従事者数(夜勤ありの職員数)〔Ｂ〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("Q".$RowNumber.":S".$RowNumber);
		$excelObj->SetValueJP2("Q$RowNumber","=ROUNDDOWN(M$KEI_RowL,1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->CellMerge("T".$RowNumber.":Y".$RowNumber);
		$excelObj->SetValueJP2("T$RowNumber","月延べ勤務時間数の計〔Ｃ〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("Z".$RowNumber.":AA".$RowNumber);
		$excelObj->SetValueJP2("Z$RowNumber","=AT$KEI_RowU+AT$KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		// 2行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":P".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月延べ夜勤時間数　〔D−E〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("Q".$RowNumber.":S".$RowNumber);
		$excelObj->SetValueJP2("Q$RowNumber","=Z$KNext1-AM$KNext1","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->CellMerge("T".$RowNumber.":Y".$RowNumber);
		$excelObj->SetValueJP2("T$RowNumber","月延べ夜勤時間数の計〔Ｄ〕","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->CellMerge("Z".$RowNumber.":AA".$RowNumber);
		$excelObj->SetValueJP2("Z$RowNumber","=AT$KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->CellMerge("AB".$RowNumber.":AL".$RowNumber);
		$excelObj->SetValueJP2("AB$RowNumber","夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->CellMerge("AM".$RowNumber.":AN".$RowNumber);
		$excelObj->SetValueJP2("AM$RowNumber","=AU$KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);

		// 3行目
		$RowNumber++;
		$RowNext = $RowNumber + 1;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","1日当たり看護配置数","BOLD VCENTER HCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":T".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月平均１日当たり看護配置数〔C／(日数×８）〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("U".$RowNumber.":V".$RowNumber);
		$excelObj->SetValueJP2("U$RowNumber","=ROUNDDOWN(Z$KMidSumRow1/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("W$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
		// 4 LINE
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":T".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","1日看護配置数　〔(A ／届出区分の数)×３〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("U".$RowNumber.":V".$RowNumber);
		$excelObj->SetValueJP2("U$RowNumber","=K17",  "BOLD VCENTER FITSIZE"  ,$this->MSPgothic,12);
		$excelObj->SetValueJP2("W$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 5行目
		$RowNumber++;
		$RowNext = $RowNumber + 1;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","＜看護職員夜間看護配置加算＞\n1日当たり夜間看護配置数","BOLD VCENTER HCENTER WRAP",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":T".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月平均１日当たり夜間看護配置数〔D／(日数×１６）〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("U".$RowNumber.":V".$RowNumber);
		$excelObj->SetValueJP2("U$RowNumber","=ROUNDDOWN(Z$KNext1/(N39*16),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("W$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
		// 6 LINE
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":T".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","1日看護夜間配置数　〔A ／１２〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("U".$RowNumber.":V".$RowNumber);
		$excelObj->SetValueJP2("U$RowNumber","=K20","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("W$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$excelObj->SetArea("C".$KMidSumRow1.":AA".$KMidSumRow1);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","top");
		$excelObj->SetBorder2("medium","","right");

		$excelObj->SetArea("AB".$KNext1.":AL".$KNext1);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","top");

		$excelObj->SetArea("C".$KNext1.":AL".$KNext1);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");

		$excelObj->SetArea("Q".$KMidSumRow1.":S".$KNext1);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$excelObj->SetArea("Z".$KMidSumRow1.":Z".$KNext1);
		$excelObj->SetBorder2("thin","","left");

		$excelObj->SetArea("J".$KMidSumRow1.":AA".$KMidSumRow1);
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("AB".$KNext1.":AB".$KNext1);
		$excelObj->SetBorder2("thin","","left");

		$excelObj->SetArea("AM".$KNext1.":AN".$KNext1);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetArea("C".$KMidSumRow2.":V".$KNext2);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetArea("J".$KMidSumRow2.":V".$KMidSumRow2);
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("C".$KMidSumRow3.":V".$KNext3);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetArea("J".$KMidSumRow3.":V".$KMidSumRow3);
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("J".$KMidSumRow1.":J".$KNext1);
		$excelObj->SetBorder2("thin","","left");

		$excelObj->SetArea("J".$KMidSumRow2.":T".$KNext3);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		// 色指定
		$excelObj->SetArea("Q".$KMidSumRow1.":S".$KNext1);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("Z".$KMidSumRow1.":AA".$KNext1);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("AM".$KNext1.":AN".$KNext1);
		$excelObj->SetBackColor($this->green);

		$excelObj->SetArea("U".$KMidSumRow2.":V".$KMidSumRow2);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("U".$KNext2.":V".$KNext2);
		$excelObj->SetBackColor($this->yellow);

		$excelObj->SetArea("U".$KMidSumRow3.":V".$KMidSumRow3);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("U".$KNext3.":V".$KNext3);
		$excelObj->SetBackColor($this->yellow);

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber , 13.5);

		$arg_array["KMidSumRow1"] = $KMidSumRow1;
		$arg_array["KMidSumRow2"] = $KMidSumRow2;
		$arg_array["KMidSumRow3"] = $KMidSumRow3;
		$arg_array["RowNumber"] = $RowNumber;

		return $arg_array;
	}

	////////////////////////////////////////////
	//					  //
	// ++++++++++++ 看護補助者表 ++++++++++++ //
	// 		 2012年対応		  //
	// 					  //
	////////////////////////////////////////////
	function ShowNurseData2( // 看護職員表
				$excelObj,		// PHP-Excelオブジェクト
				$start_row_number,	// 表を開始する行位置
				$obj,			// 共通クラス
				$data_array,		//看護補助者情報
				$week_array,		// 曜日配列
				$day_cnt,		// 日数
				$arg_array,		// 行位置情報
				$hojo_space_line )	// 看護補助者表、空白行数
	{

		// 追加する空白行にゴミを出さないよう、追加行分の内容をクリア
		if( $hojo_space_line != 0 ){
			$add_line_count = count($data_array) + $hojo_space_line;
			for ( $r=count($data_array); $r<$add_line_count; $r++){
				$data_array[$i]["main_group_name"];
				$data_array[$i]["staff_name"];
				$data_array[$i]["duty_form"];
				$data_array[$i]["other_post_flg"];
				$data_array[$i]["night_duty_flg"];
				for($k=1; $k<=$day_cnt; $k++) {
					$data_array[$i]["duty_time_$k"];
					$data_array[$i]["night_time_$k"];
				}
			}
		}

		$KangoRowTop = $arg_array["KangoRowTop"];
		$DayCntRow   = $KangoRowTop - 4;
		$LaborCntRow = $KangoRowTop - 3;

		$RowNumber = $arg_array["RowNumber"];
		$RowNumber ++;

		$excelObj->SetRowDim($RowNumber , 18.75);
		$excelObj->SetValueJP2("B".$RowNumber,"《看護補助者表》","BOLD VCENTER",$this->MSPgothic ,14);

		$RowNumber ++;

		$HojoRowTop  = $RowNumber;
		$MidashiRow1 = $HojoRowTop;
		$MidashiRow2 = $MidashiRow1 + 1;
		$MidashiRow3 = $MidashiRow1 + 2;

		// 行高さ設定
		$excelObj->SetRowDim($MidashiRow1,54);
		$excelObj->SetRowDim($MidashiRow2,13.5);
		$excelObj->SetRowDim($MidashiRow3,13.5);

		// 行高さ設定
		$excelObj->SetRowDim($MidashiRow1,54);
		$excelObj->SetRowDim($MidashiRow2,13.5);
		$excelObj->SetRowDim($MidashiRow3,13.5);

		// 見出し
//		$excelObj->SetValueJPwith("B".$MidashiRow1,"種別","BOLD");
//		$excelObj->CellMerge("B".$MidashiRow1.":B".$MidashiRow3);

		$excelObj->SetValueJPwith("C".$MidashiRow1,"番号","BOLD");
		$excelObj->CellMerge("C".$MidashiRow1.":C".$MidashiRow3);

		$excelObj->SetValueJPwith("D".$MidashiRow1,"病棟名","BOLD");
		$excelObj->CellMerge("D".$MidashiRow1.":D".$MidashiRow3);

		$excelObj->SetValueJPwith("E".$MidashiRow1,"氏    名","BOLD HCENTER FITSIZE");
		$excelObj->CellMerge("E".$MidashiRow1.":E".$MidashiRow3);

		$excelObj->SetValueJPwith("F".$MidashiRow1,"雇用・勤務形態","BOLD");
		$excelObj->CellMerge("F".$MidashiRow1.":I".$MidashiRow3);

		$excelObj->SetValueJPwith("J".$MidashiRow1,"夜勤の有無","BOLD");
		$excelObj->CellMerge("J".$MidashiRow1.":L".$MidashiRow3);

		$excelObj->SetValueJP2("M".$MidashiRow1,"夜勤従事者数への計上","WRAP",$this->MSPgothic ,10);
		$excelObj->CellMerge("M".$MidashiRow1.":M".$MidashiRow3);

		$excelObj->CellMerge("N".$MidashiRow1.":N".$MidashiRow3);

		// 「日付別の勤務時間数」見出しのセルマージ領域。1ヶ月が31未満でも31日分のセルを用意する
		$excelObj->CellMerge2("O".$MidashiRow1.":AS".$MidashiRow1,"thin","bottom","");
		$excelObj->SetValueJP2("O".$MidashiRow1,"日付別の勤務時間数","BOLD VCENTER",$this->MSPgothic ,12);

		// 日数
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+13].$MidashiRow2;
			$excelObj->SetValueJPwith($cellArea , $week_array[$c]["day"]."日" , ""); //$c -> $week_array[$c]["day"] 20150421
		}
		// 曜日
		for($c=1; $c<=$day_cnt; $c++){
			$cellArea=$this->excelColumnName[$c+13]."".$MidashiRow3;
			$excelObj->SetValueJPwith($cellArea ,$week_array[$c]["name"]."曜" , "");
		}

		$excelObj->CellMerge("AT".$MidashiRow1.":AT".$MidashiRow3);
		$excelObj->SetValueJP2("AT".$MidashiRow1,"月勤務時間数（延べ時間数）","WRAP",$this->MSPgothic ,10);

		$excelObj->CellMerge("AU".$MidashiRow1.":AU".$MidashiRow3);
		$excelObj->SetValueJP2("AU".$MidashiRow1,"（再掲）夜勤専従者及び月16時間以下の者の夜勤時間数 \n（短時間正職員の場合は12時間より短いもの）","WRAP",$this->MSPgothic ,8);

		$excelObj->CellMerge("AV".$MidashiRow1.":AV".$MidashiRow3);
		$excelObj->SetValueJP2("AV".$MidashiRow1,"常勤換算配置数","WRAP",$this->MSPgothic ,11);

		// *** 隠し列 ***
//		$excelObj->CellMerge("AX".$MidashiRow1.":AY".$MidashiRow2);
//		$excelObj->SetValueJP2("AX".$MidashiRow1,"職種別\n常勤換算配置数","WRAP",$this->MSPgothic ,11);

		// 見える列
//		$excelObj->SetValueJPwith("AV".$MidashiRow3,"看護師","WRAP");
//		$excelObj->SetValueJPwith("AW".$MidashiRow3,"准看護師","WRAP");

		// ** 隠し列 **
//		$excelObj->SetValueJPwith("AX".$MidashiRow3,"看護師","WRAP");
//		$excelObj->SetValueJPwith("AY".$MidashiRow3,"准看護師","WRAP");

		// AU-AXまとめて処理
//		$excelObj->SetArea("AV".$MidashiRow3.":AY".$MidashiRow3);
//		$excelObj->SetFont($this->MSPgothic ,11);
//		$excelObj->SetBorder2("double","","top");

		// ** 隠し列 ** AZ-BA(罫線無し)
		$excelObj->SetValueJPwith("AZ".$MidashiRow3,"全体","");
		$excelObj->SetPosH("CENTER");
		$excelObj->SetValueJPwith("BA".$MidashiRow3,"夜専","");
		$excelObj->SetPosH("CENTER");

		// C-BAまとめて
		$excelObj->SetArea("C".$MidashiRow1.":BA".$MidashiRow3,$this->MSPgothic ,12);
		$excelObj->SetPosV("CENTER");
		$excelObj->SetShrinkToFit();
		$excelObj->SetArea("C".$MidashiRow1.":M".$MidashiRow3,$this->MSPgothic ,12);
		$excelObj->SetPosH("CENTER");

		//
		// データ行
		//
		$RowNumber = $HojoRowTop + 3;  // 行位置、一行ずつカウントして行く。2段表示のときは$RowNextを下段位置とする
		$HojoRowDataTop = $RowNumber ;  // 看護補助者表データ行開始のEXCEL行番号
		$HojoRowSmallTop = $RowNumber ;  // 小計データ開始のEXCEL行番号 20150115
		// 縦集計計算式用配列
		$VSumU = array(); // 上段
		$VSumL = array(); // 下段
		for($i=0; $i<52; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumU[$i] = "=";
			$VSumL[$i] = "=";
		}
		if ( $hojo_space_line != 0 ){
			$row_max = $add_line_count;
		}else{
			$row_max = count($data_array);
		}
		//小計対応 20150114
		$stotal_line = 100; //小計出力の単位
		$stotal_start = 1;
		$VSumUT = array(); // 上段
		$VSumLT = array(); // 下段
		for($i=0; $i<52; $i++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumUT[$i] = "=";
			$VSumLT[$i] = "=";
		}
		for($i=0; $i<$row_max; $i++) {
			//小計対応 20150114
			if ($i > 0 && ($i % $stotal_line) == 0 && $row_max > 200) {
				$HojoRowSmallSum = $RowNumber;
				$RowNext = $RowNumber + 1;
				// 左見出し
				$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
				$CellArea = "C".$RowNumber;
				$excelObj->SetValueJPwith($CellArea, "小計" ,"");
				$excelObj->SetPosH("LEFT");

				$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

				$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
				//小計対応 20150114
				$stotal_start_str = sprintf( '%003d' , $stotal_start );
				$stotal_end_str = sprintf( '%003d' , ($stotal_start+$stotal_line-1) );
				$stotal_start += $stotal_line;
				$name = "$stotal_start_str 〜 $stotal_end_str";
				$CellArea = "E".$RowNumber;
				$excelObj->SetValueJPwith($CellArea, $name, "");

				// 小計・明細行内の見出し
				$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
				$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
				$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
				$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
				$CellArea="F".$RowNumber.":I".$RowNumber;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,8);
				$excelObj->SetValueJPwith("J".$RowNumber, "有","");
				$excelObj->SetValueJPwith("K".$RowNumber, "無","");
				$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
				$CellArea="J".$RowNumber.":M".$RowNumber;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
				$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
				$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

				for($k=5; $k<=12; $k++) { // F〜M列
					$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$k].$RowNext;
					$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
				}
				$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

				for($k=1; $k<=$day_cnt; $k++) { // 日毎
					$colPos = $k + 13; // N列から->O列から、へ変更2012.4.27
					// 上段
					$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$colPos].$RowNumber;
					$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
					// 下段
					$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
					$CellArea = $this->excelColumnName[$colPos].$RowNext;
					$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
				}
				// AT列
				$colPos = 45 ;
				// 上段
				$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNumber;
				$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AU列、下段のみ
				$colPos ++;
				// 下段
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// AV列、下段のみ
				$colPos++;
				$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
				$CellArea = $this->excelColumnName[$colPos].$RowNext;
				$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

				// 行間の横線
				$CellArea = "C".$RowNumber.":AV".$RowNumber; // 細い実線
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("thin","","top");
				$CellArea = "F".$RowNext.":AV".$RowNext; // 細い破線
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("dotted","","top");
				// 隠し行 AZ-BA
				$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);
				$excelObj->SetValueJP2("BA".$RowNext, "小計","",$this->MSPgothic ,11);

				//小計対応 20150114
				//小計用計算式クリア
				for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
					$VSumU[$k] = "=";
					$VSumL[$k] = "=";
				}
				//合計用計算式
				for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
					$VSumUT[$k] = $VSumUT[$k].$this->excelColumnName[$k].$RowNumber."+";
					$VSumLT[$k] = $VSumLT[$k].$this->excelColumnName[$k].($RowNumber+1)."+";
				}

				$RowNumber += 2;
				$RowNext = $RowNumber + 1;
				// 列集計（小計）おわり。

				$HojoRowMax = $RowNumber - 1; // 看護補助者表終了時のExcel行番号
				$HojoRowSum = $RowNumber;

				// B列のみ「小計」の行まで看護職員表まとめて設定
				// 罫線、表示位置
		//		$CellArea = "B".$HojoRowTop.":B".$HojoRowMax;
		//		$excelObj->SetArea($CellArea);
		//		$excelObj->SetBorder2("thin","","right");
		//		$excelObj->SetPosH("CENTER");

				$CellArea = "C".$HojoRowTop.":AV".$HojoRowMax; // 周囲をやや太い線で囲む。後で小計行の上下へ二重線を引く
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("medium","","outline");
				// C列より右は「計」行まで
				$HojoRowMax += 2;

				// フォントC-D列
				$CellArea = "C".$HojoRowTop.":D".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				// フォントE列
				$CellArea = "E".$HojoRowTop.":E".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,12);
				// フォントN列
				$CellArea = "N".$HojoRowTop.":N".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,10);
				// フォント、スタイル O-BA列
				$CellArea = "O".$HojoRowDataTop.":BA".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetFont($this->MSPgothic ,11);
				$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 ");

				// 色指定 J-K列
				$CellArea = "J".$HojoRowDataTop.":K".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑
				// 色指定 M列
				$CellArea = "M".$HojoRowDataTop.":M".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑
				// 色指定 AT-AV列
				$CellArea = "AT".$HojoRowDataTop.":AV".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBackColor("CCFFCC"); // 薄い緑

				// 表示位置・縦横
				$wk = $HojoRowSmallSum - 1;
				$CellArea = "C".$HojoRowSmallTop.":C".$wk; // 右よりはデータ行。見出しは中央 20150115
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("RIGHT");
				$HojoRowSmallTop = $RowNumber + 2; //20150115

				$CellArea = "D".$HojoRowTop.":D".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("CENTER");

				$CellArea = "E".$HojoRowDataTop.":E".$HojoRowMax;
				$excelObj->SetArea($CellArea);
				$excelObj->SetPosH("LEFT");

				// 「小計」行の横罫線引きなおして、背景黄色にする->緑へ変更
				$beforRowU = $RowNumber-2;
				$beforRowL = $RowNext-2;
				$CellArea = "C".$beforRowU.":AV".$beforRowL;
				$excelObj->SetArea($CellArea);
				$excelObj->SetBorder2("double","","top");
				$excelObj->SetBorder2("double","","bottom");
				$excelObj->SetBackColor("CCFFCC"); // 薄い黄色->緑へ変更
			}
			$no = $i + 1;
			$RowNext = $RowNumber + 1;
			// 種別
//			$excelObj->CellMerge("B".$RowNumber.":B".$RowNext);
//			$excelObj->SetValueJPwith("B".$RowNumber, $data_array[$i]["job_name"],"");

			// 番号
			$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
			$excelObj->SetValuewith("C".$RowNumber, $no,"");

			// 病棟名
			$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);
			$excelObj->SetValueJPwith("D".$RowNumber, $data_array[$i]["main_group_name"],"");
			$excelObj->SetWrapText(); // セル内折り返し

			// 氏名
			$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
			$excelObj->SetValueJPwith("E".$RowNumber, $data_array[$i]["staff_name"],"");
			$excelObj->SetShrinkToFit(); // セル内縮小して全表示

			// 明細行内の見出し
			$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
			$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");

			$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
			$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
			$CellArea="F".$RowNumber.":I".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,8);
			$excelObj->SetValueJPwith("J".$RowNumber, "有","");
			$excelObj->SetValueJPwith("K".$RowNumber, "無","");
			$excelObj->SetValueJPwith("L".$RowNumber, "夜専","");
			$CellArea="J".$RowNumber.":L".$RowNumber;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,10);
			$excelObj->SetValueJPwith("M".$RowNumber, "夜勤従事者","");
			$excelObj->SetFont($this->MSPgothic ,8);
			$excelObj->SetValueJPwith("N".$RowNumber, "日勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10);
			$excelObj->SetValueJPwith("N".$RowNext, "夜勤時間数","");
			$excelObj->SetFont($this->MSPgothic ,10);

			// データ
			// duty_shift_report1_common_class.phpから流用
			//常勤／非常勤／他部署兼務
			//夜勤：有り／夜勤：無し／夜専
			$wk1 = '0';	// 常勤
			$wk2 = '0';	// 非常勤
			$wk3 = '0';	// 他部署兼務
//			$wk4 = "　";	// 夜勤有り→Excel計算式で求める
//			$wk5 = "　";	// 夜勤無し→Excel計算式で求める
			$wk6 = '0';	// 夜専
			$wk7 = '0';	// 短時間 2012.04.27追加
			if ($data_array[$i]["duty_form"] == "1") { $wk1 = "1"; } // 常勤
			if ($data_array[$i]["duty_form"] == "3") { $wk7 = "1"; } // 短時間 【NEW】2012.04.27
			if ($data_array[$i]["duty_form"] == "2") { $wk2 = "1"; } // 非常勤
			if ($data_array[$i]["other_post_flg"] == "1") { $wk3 = "1"; }
			//夜専以外
//			if ($data_array[$i]["duty_form"] == "3") {
//				//12以上
//				if (($wk_times_1 + $wk_times_2) >= 12) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
//			}
			if ($data_array[$i]["night_duty_flg"] != "2") {
				//16時間超を夜勤有りとする→Excel計算式で求める
//				if (($wk_times_1 + $wk_times_2) > 16) {
//					$wk4 = "1";
//				} else {
//					$wk5 = "1";
//				}
			} else {
				//夜専
				$wk6 = "1";
			}
			$excelObj->SetValuewith("F".$RowNext, $wk1,""); // 常勤
			$excelObj->SetValuewith("G".$RowNext, $wk7,""); // 短時間
			$excelObj->SetValuewith("H".$RowNext, $wk2,""); // 非常勤
			$excelObj->SetValuewith("I".$RowNext, $wk3,""); // 他部署兼務
			$wk0 = $wk1 + $wk2 + $wk3;
			if( $wk0 == 0 ){
				$excelObj->SetArea("F".$RowNext.":I".$RowNext);
				$excelObj->SetCharColor("FFC0C0C0");
			}

			//=IF(AND(F47=0,G47=0,H47=0,I47=0), 0, IF(OR(AND(G47=1,AT47>=12),AND(G47<>1,AT47>16)),1,0))
			$formulae = "=IF(AND(F$RowNext=0,G$RowNext=0,H$RowNext=0,I$RowNext=0), 0, IF(OR(AND(G$RowNext=1,AT$RowNext>=12),AND(G$RowNext<>1,AT$RowNext>16)),1,0))";
			$excelObj->SetValuewith("J".$RowNext, $formulae,"");
			// =IF(AS39<=16,1,0)
			$formulae = "=IF(AND(F$RowNext=0,G$RowNext=0,H$RowNext=0,I$RowNext=0), 0, IF(J$RowNext<>1, 1, 0))";
			$excelObj->SetValuewith("K".$RowNext, $formulae,"");
			$excelObj->SetValuewith("L".$RowNext, $wk6,"");
			$CellArea="F".$RowNext.":M".$RowNext;
			$excelObj->SetArea($CellArea);
			$excelObj->SetFont($this->MSPgothic ,11);

			// 縦集計の計算式
			$VSumL[5]  = $VSumL[5]."F".$RowNext."+";
			$VSumL[6]  = $VSumL[6]."G".$RowNext."+";
			$VSumL[7]  = $VSumL[7]."H".$RowNext."+";
			$VSumL[8]  = $VSumL[8]."I".$RowNext."+";
			$VSumL[9]  = $VSumL[9]."J".$RowNext."+";
			$VSumL[10] = $VSumL[10]."K".$RowNext."+";
			$VSumL[11] = $VSumL[11]."L".$RowNext."+";
			$VSumL[12] = $VSumL[12]."M".$RowNext."+"; // 追加 2012.4.27

			//夜勤従事者数
			//=IF(OR(L47=1, K47=1), 0, IF(AND(F47=1, G47=0, H47=0, I47=0), 1, MIN(1,(AT46+AT47)/($N$40*$N$39/7))))
			$formulae = "=IF(OR(L$RowNext=1, K$RowNext=1), 0, IF(AND(F$RowNext=1, G$RowNext=0, H$RowNext=0, I$RowNext=0), 1, MIN(1,(AT$RowNumber+AT$RowNext)/(\$N\$$LaborCntRow*\$N\$$DayCntRow/7))))";
			$excelObj->SetValuewith("M".$RowNext , $formulae, "");
			$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // M列

			// 日付別の勤務時間数（日勤時間数、夜勤時間数）duty_shift_report1_common_class.phpから流用
			// Excelなので1回のdayループで日勤(上段)と夜勤(下段)の両方を処理する
			$wk_times_1 = 0.0;
			$wk_times_2 = 0.0;
			for($k=1; $k<=$day_cnt; $k++) {
				$colPos = $k + 13; // N列から->O列からへ変更 2012.4.27
				//////////////
				// 日勤時間 //
				//////////////
				$wk = $data_array[$i]["duty_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNumber; // 上段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumU[$colPos] = $VSumU[$colPos].$CellArea."+"; // 縦集計用の計算式

				//////////////
				// 夜勤時間 //
				//////////////
				$wk = $data_array[$i]["night_time_$k"];
				if ($wk <= 0) {
					$wk = "　";
				}
				$CellArea = $this->excelColumnName[$colPos].$RowNext; // 下段
				$excelObj->SetValuewith($CellArea , $wk, "");
				$VSumL[$colPos] = $VSumL[$colPos].$CellArea."+"; // 縦集計用の計算式
			}

			////////////
			// 行集計 //
			////////////
			// AT列
			$colPos = 45 ;
			$CellArea = "AT".$RowNumber; // 上段
			$formulae = "=SUM(O".$RowNumber.":AS".$RowNumber.")"; // =SUM(O263:AS263)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			$CellArea = "AT".$RowNext; // 下段
			$formulae = "=SUM(O".$RowNext.":AS".$RowNext.")"; // =SUM(O264:AS264)
			$excelObj->SetValuewith($CellArea , $formulae, "");

			// 縦集計計算式
			$VSumU[$colPos] = $VSumU[$colPos]."AT".$RowNumber."+";
			$VSumL[$colPos] = $VSumL[$colPos]."AT".$RowNext."+";

			// AU列下のみ
			$colPos ++;
			$CellArea = "AU".$RowNext; // 下段
			// =IF(OR(L264=1,K264=1),AT264,0)
			$formulae = "=IF(OR(L$RowNext=1,K$RowNext=1),AT$RowNext,0)";
			$excelObj->SetValuewith($CellArea , $formulae, "");
			// 縦集計計算式
			$VSumL[$colPos] = $VSumL[$colPos]."AU".$RowNext."+";

			// AV列下のみ
			$colPos ++;
			// =IF(AND($F264=1, $G264=0, $H264=0, $I264=0),1,MIN(1,(AT263+AT264)/($N$40*$N$39/7)))  <------- 40 & 39
			$formulae = "=IF(AND(\$F$RowNext=1, \$G$RowNext=0, \$H$RowNext=0, \$I$RowNext=0),1,MIN(1,(AT$RowNumber+AT$RowNext)/(\$N\$$LaborCntRow*\$N\$$DayCntRow/7)))";
			$CellArea = "AV".$RowNext;
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			$VSumL[$colPos] = $VSumL[$colPos]."AV".$RowNext."+";

			// 縦集計計算式
//			$VSumU[46] = $VSumU[46]."AU".$RowNumber."+";
//			$VSumL[46] = $VSumL[46]."AU".$RowNext."+";

			// AZ列下
			$CellArea = "AZ".$RowNext;
			// =IF(AND(AT263=0,AT264=0),"-",AT264)
			$formulae = "=IF(AND(AT$RowNumber=0,AT$RowNext=0),\"-\",AT$RowNext)";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");
			// BA列下
			$CellArea = "BA".$RowNext;
			// =IF(AND(AT263=0,AT264=0),"-",IF(L264=1,AT264,"-"))
			$formulae = "=IF(AND(AT$RowNumber=0,AT$RowNext=0),\"-\",IF(L$RowNext=1,AT$RowNext,\"-\"))";
			$excelObj->SetValueJPwith($CellArea , $formulae, "");

			// 行間の横線
			$CellArea = "C".$RowNumber.":AV".$RowNumber; // 細い実線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("thin","","top");
			$CellArea = "F".$RowNext.":AV".$RowNext; // 細い破線
			$excelObj->SetArea($CellArea);
			$excelObj->SetBorder2("dotted","","top");
			$RowNumber += 2;
		}

		//////////////////
		//    列集計    //
		//////////////////
		$HojoRowSmallSum = $RowNumber;
		$RowNext = $RowNumber + 1;
		// 左見出し
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);
		$CellArea = "C".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, "小計" ,"");
		$excelObj->SetPosH("LEFT");

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);
		//小計対応 20150114
		$name = $row_max ;
		$name = sprintf( '%003d' , $name );
		$stotal_start_str = sprintf( '%003d' , $stotal_start );
		$name = "$stotal_start_str 〜 ".$name;
		$CellArea = "E".$RowNumber;
		$excelObj->SetValueJPwith($CellArea, $name, "");

		// 小計・明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
		$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":I".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8);
		$excelObj->SetValueJPwith("J".$RowNumber, "有","");
		$excelObj->SetValueJPwith("K".$RowNumber, "無","");
		$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
		$CellArea="J".$RowNumber.":M".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
		$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
		$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

		for($k=5; $k<=12; $k++) { // F〜M列
			$VSumL[$k] = substr( $VSumL[$k] , 0 , strlen($VSumL[$k]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$k].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$k], "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列

		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 13; // N列から->O列から、へ変更2012.4.27
			// 上段
			$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
			// 下段
			$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");
		}
		// AT列
		$colPos = 45 ;
		// 上段
		$VSumU[$colPos] = substr( $VSumU[$colPos] , 0 , strlen($VSumU[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		$excelObj->SetValuewith($CellArea, $VSumU[$colPos], "");
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AU列、下段のみ
		$colPos ++;
		// 下段
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// AV列、下段のみ
		$colPos++;
		$VSumL[$colPos] = substr( $VSumL[$colPos] , 0 , strlen($VSumL[$colPos]) - 1 ); // 末尾の + を消す
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		$excelObj->SetValuewith($CellArea, $VSumL[$colPos], "");

		// 行間の横線
		$CellArea = "C".$RowNumber.":AV".$RowNumber; // 細い実線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "F".$RowNext.":AV".$RowNext; // 細い破線
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","top");
		// 隠し行 AZ-BA
		$excelObj->SetValueJP2("AZ".$RowNext, "小計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("BA".$RowNext, "小計","",$this->MSPgothic ,11);

		//小計対応 20150114
		//合計用計算式
		for($k=0; $k<52; $k++){ //0:A列、1:B列、・・・ 25:Z列・・・BA列まで対応
			$VSumUT[$k] = $VSumUT[$k].$this->excelColumnName[$k].$RowNumber;
			$VSumLT[$k] = $VSumLT[$k].$this->excelColumnName[$k].($RowNumber+1);
		}

		$RowNumber += 2;
		$RowNext = $RowNumber + 1;
		// 列集計（小計）おわり。

		$HojoRowMax = $RowNumber - 1; // 看護補助者表終了時のExcel行番号
		$HojoRowSum = $RowNumber;

		// B列のみ「小計」の行まで看護職員表まとめて設定
		// 罫線、表示位置
//		$CellArea = "B".$HojoRowTop.":B".$HojoRowMax;
//		$excelObj->SetArea($CellArea);
//		$excelObj->SetBorder2("thin","","right");
//		$excelObj->SetPosH("CENTER");

		$CellArea = "C".$HojoRowTop.":AV".$HojoRowMax; // 周囲をやや太い線で囲む。後で小計行の上下へ二重線を引く
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
		// C列より右は「計」行まで
		$HojoRowMax += 2;

		// フォントC-D列
		$CellArea = "C".$HojoRowTop.":D".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		// フォントE列
		$CellArea = "E".$HojoRowTop.":E".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,12);
		// フォントN列
		$CellArea = "N".$HojoRowTop.":N".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,10);
		// フォント、スタイル O-BA列
		$CellArea = "O".$HojoRowDataTop.":BA".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 ");

		// 色指定 J-K列
		$CellArea = "J".$HojoRowDataTop.":K".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 M列
		$CellArea = "M".$HojoRowDataTop.":M".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		// 色指定 AT-AV列
		$CellArea = "AT".$HojoRowDataTop.":AV".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑

		// 表示位置・縦横
		$wk = $HojoRowSmallSum - 1;
		$CellArea = "C".$HojoRowSmallTop.":C".$wk; // 右よりはデータ行。見出しは中央 20150115
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("RIGHT");

		$CellArea = "D".$HojoRowTop.":D".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("CENTER");

		$CellArea = "E".$HojoRowDataTop.":E".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosH("LEFT");

		// 「小計」行の横罫線引きなおして、背景黄色にする->緑へ変更
		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		$CellArea = "C".$beforRowU.":AV".$beforRowL;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","top");
		$excelObj->SetBorder2("double","","bottom");
		$excelObj->SetBackColor("CCFFCC"); // 薄い黄色->緑へ変更

		// 「計」開始
		$excelObj->CellMerge("C".$RowNumber.":C".$RowNext);

		$excelObj->SetValueJP2("C".$RowNumber, "計" ,"LEFT",$this->MSPgothic ,11);

		$excelObj->CellMerge("D".$RowNumber.":D".$RowNext);

		$excelObj->CellMerge("E".$RowNumber.":E".$RowNext);

		// 明細行内の見出し
		$excelObj->SetValueJPwith("F".$RowNumber, "常勤","");
		$excelObj->SetValueJPwith("G".$RowNumber, "短時間","");
		$excelObj->SetValueJPwith("H".$RowNumber, "非常勤","");
		$excelObj->SetValueJPwith("I".$RowNumber, "他部署兼務","");
		$CellArea="F".$RowNumber.":I".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,8);
		$excelObj->SetValueJPwith("J".$RowNumber, "有","");
		$excelObj->SetValueJPwith("K".$RowNumber, "無","");
		$excelObj->SetValueJPwith("L".$RowNumber, "夜専","FITSIZE");
		$CellArea="J".$RowNumber.":M".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者","",$this->MSPgothic ,8);
		$excelObj->SetValueJP2("N".$RowNumber, "日勤時間数","",$this->MSPgothic ,10);
		$excelObj->SetValueJP2("N".$RowNext, "夜勤時間数","",$this->MSPgothic ,10);

		$beforRowU = $RowNumber-2;
		$beforRowL = $RowNext-2;
		for($k=5; $k<=12; $k++) { // F〜M列
			$colPos=$k;
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
			$formulae = "=".$this->excelColumnName[$colPos].$beforRowL;
			//小計対応 20150114
			$formulae = $VSumLT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		$excelObj->SetFormatStyle("[Red][<0.00](0.00) ;[Black][>=0]0.00 "); // L列
		for($k=1; $k<=$day_cnt; $k++) { // 日毎
			$colPos = $k + 13; // N列から->O列からに変更
			// 上段
			$CellArea = $this->excelColumnName[$colPos].$RowNumber;
			//小計対応 20150114
			$formulae = $VSumUT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
			// 下段
			$CellArea = $this->excelColumnName[$colPos].$RowNext;
			//小計対応 20150114
			$formulae = $VSumLT[$colPos];
			$excelObj->SetValuewith($CellArea, $formulae, "");
		}
		// AT列
		// 上段
		$colPos = 45 ;
		$CellArea = $this->excelColumnName[$colPos].$RowNumber;
		//小計対応 20150114
		$formulae = $VSumUT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");
		// 下段
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AU列
		$colPos ++;
		// 下段のみ
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AV列
		$colPos ++;
		// 下段のみ
		$CellArea = $this->excelColumnName[$colPos].$RowNext;
		//小計対応 20150114
		$formulae = $VSumLT[$colPos];
		$excelObj->SetValuewith($CellArea, $formulae, "");

		// AW列無い
		$colPos ++;

		// AX列無い
		$colPos ++ ;

		// AY列無い
		$colPos ++;

		// AZ-BA(隠し列)
		$excelObj->SetValueJP2("AZ".$RowNext, "計","",$this->MSPgothic ,11);
		$excelObj->SetValueJP2("BA".$RowNext, "計","",$this->MSPgothic ,11);

		// 「計」行をまとめて設定。横線など
		$CellArea = "C".$RowNumber.":AV".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("hair","","vertical");
		$excelObj->SetBackColor("99CC00"); // 濃い緑 R153 G204 B0
		$excelObj->SetBorder2("medium","","bottom");

		$CellArea = "F".$RowNumber.":AV".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("dotted","","bottom");

		// 縦線まとめて
		$CellArea = "C".$HojoRowTop.":AV".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");
		$excelObj->SetBorder2("hair","","vertical"); // new

		// AT列の二重線
		$CellArea = "AT".$HojoRowTop.":AT".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","left");
		$excelObj->SetBorder2("double","","right");

		// AV:AW、AX:AYのやや太い枠
//		$CellArea = "AW".$HojoRowTop.":AW".$RowNext;
//		$excelObj->SetArea($CellArea);
//		$excelObj->SetBorder2("medium","","left");
//		$excelObj->SetBorder2("medium","","right");

		// AZ:BAの背景色、灰色
		$wk = $HojoRowTop + 2;
		$CellArea = "AZ".$wk.":BA".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("C0C0C0"); // 灰色

		// やや太い枠で全体を囲み、左端(C列の左)は細い縦線を引きなおす
		$CellArea = "C".$HojoRowTop.":AV".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBorder2("thin","","left");

		$CellArea = "AV".$HojoRowTop.":AV".$RowNext;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","left");

		// 隠し列の背景色、灰色
		$wk = $HojoRowTop + 2;
		$CellArea = "AZ".$wk.":BA".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("C0C0C0"); // 灰色

		// 縮小して全体を表示する指定
		$CellArea = "F".$HojoRowDataTop.":BA".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetShrinkToFit();

		// 全体のセル内縦位置をCENTERにする
		$CellArea = "C".$HojoRowDataTop.":BA".$HojoRowMax;
		$excelObj->SetArea($CellArea);
		$excelObj->SetPosV("CENTER");

		$H_KEI_RowU = $RowNumber;
		$H_KEI_RowL = $RowNumber+1;

		$RowNumber++;

		// 空行
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$arg_array["RowNumber"] = $RowNumber;
		$arg_array["HojoRowDataTop"] = $HojoRowDataTop;
		$arg_array["HojoRowSum"] = $HojoRowSum;

		return $arg_array;
	}
	//////////////////////////////////
	//				//
	//   一般病棟・看護補助者集計   //
	// 	 2012年対応		//
	//				//
	//////////////////////////////////
	function ShowNurseSum02( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$HojoRowSum = $arg_array["HojoRowSum"];
		$H_KEI_RowU = $HojoRowSum;
		$H_KEI_RowL = $HojoRowSum+1;

		$HMidSumRow1 = $RowNumber + 2;
		$HMidSumRow2 = $HMidSumRow1 + 3;
		$HMidSumRow3 = $HMidSumRow2 + 2;
		$HMidSumRow4 = $HMidSumRow3 + 2;

		$HNext1m = $HMidSumRow1 + 1;
		$HNext1b = $HMidSumRow1 + 2;
		$HNext2 = $HMidSumRow2 + 1;
		$HNext3 = $HMidSumRow3 + 1;
		$HNext4 = $HMidSumRow4 + 1;

		////////////////
		// 中間集計行 //
		////////////////
		// 1行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->SetValueJP2("B$RowNumber","〔急性期看護補助体制加算・看護補助加算等を届け出る場合の看護補助者の算出方法〕","BOLD",$this->MSPgothic,14);
		// 2行目
		$RowNumber ++;
		$RowNext2 = $RowNumber + 2;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext2);
		$excelObj->SetValueJP2("C$RowNumber","看護補助者の勤務実績(勤務時間数)","BOLD HCENTER VCENTER WRAP",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","看護補助者のみの月延べ勤務時間数の計〔F〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=AT$H_KEI_RowU+AT$H_KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		// 3行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=MAX(Z$KMidSumRow1-Y17, 0)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		// 4行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","看護補助者のみの月延べ夜勤時間数の計〔Ｈ〕 看護補助者（みなしを除く）のみの〔D〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=AT$H_KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);

		// 5行目
		$RowNumber ++;
		$RowNext = $RowNumber + 1;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","みなし補助者を含む \n 1日当たり看護補助者配置数","BOLD HCENTER VCENTER WRAP",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月平均1日当たり看護補助者配置数(みなし看護補助者含む）　〔(F+G) ／ (日数×８) 〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=ROUNDDOWN((AG$HMidSumRow1+AG$HNext1m)/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
		// 6行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","1日看護補助配置数 〔I〕　〔(A ／届出区分の数)×３〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=K34","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("AI$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 7行目
		$RowNumber ++;
		$RowNext = $RowNumber + 1;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","＜25対1急性期看護補助体制加算＞ \n みなし補助者を除く1日当たり看護補助者配置数\nならびに看護補助者の割合","BOLD HCENTER VCENTER WRAP",$this->MSPgothic,12);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月平均1日当たり看護補助者配置数（みなし看護補助者除く）〔Ｊ〕　　〔F ／ (日数×８) 〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=ROUNDDOWN(AG$HMidSumRow1/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		// 8行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","看護補助者（みなし看護補助者を含む）の最小必要数に対する看護補助者（みなし看護補助者を除く）の割合（％） 〔（J/I）×１００〕","BOLD FITSIZE VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=IF(ISNUMBER(K34), ROUNDDOWN((AG$HMidSumRow3/AG$HNext2)*100,1), \"\")","BOLD VCENTER FITSIZE",$this->MSPgothic,12); //*100追加 20140901

		// 9行目
		$RowNumber ++;
		$RowNext = $RowNumber + 1;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
		$excelObj->SetValueJP2("C$RowNumber","＜夜間急性期看護補助体制加算＞ \n1 日当たり夜間看護補助者配置数","BOLD HCENTER VCENTER WRAP",$this->MSPgothic,14);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","月平均1日当たり夜間看護補助者配置数（H/(日数×１６）〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=ROUNDDOWN(AG$HNext1b/(N39*16),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
		// 10行目
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,26.25);
		$excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
		$excelObj->SetValueJP2("J$RowNumber","1日夜間看護補助配置数〔A/届出区分の数〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
		$excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
		$excelObj->SetValueJP2("AG$RowNumber","=K37","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		$excelObj->SetValueJP2("AI$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$excelObj->SetArea("C".$HMidSumRow1.":AH".$HNext4);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetArea("C".$HMidSumRow2.":AH".$HNext2);
		$excelObj->SetBorder2("medium","","top");
		$excelObj->SetBorder2("medium","","bottom");

		$excelObj->SetArea("C".$HMidSumRow3.":AH".$HNext3);
		$excelObj->SetBorder2("medium","","bottom");

		$excelObj->SetArea("J".$HMidSumRow1.":AF".$HNext4);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$excelObj->SetArea("J".$HNext1m.":AH".$HNext1m);
		$excelObj->SetBorder2("thin","","top");
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("J".$HMidSumRow2.":AH".$HMidSumRow2);
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("J".$HMidSumRow3.":AH".$HMidSumRow3);
		$excelObj->SetBorder2("thin","","bottom");

		$excelObj->SetArea("J".$HMidSumRow4.":AH".$HMidSumRow4);
		$excelObj->SetBorder2("thin","","bottom");


		// 色指定
		$excelObj->SetArea("AG".$HMidSumRow1.":AH".$HMidSumRow2);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("AG".$HNext2.":AH".$HNext2);
		$excelObj->SetBackColor($this->yellow);

		$excelObj->SetArea("AG".$HMidSumRow3.":AH".$HMidSumRow4);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetArea("AG".$HNext4.":AH".$HNext4);
		$excelObj->SetBackColor($this->yellow);

		// 中間集計　おわり。

		// 空行2行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$arg_array["HMidSumRow1"] = $HMidSumRow1;
		$arg_array["HMidSumRow2"] = $HMidSumRow2;
		$arg_array["HMidSumRow3"] = $HMidSumRow3;
		$arg_array["HMidSumRow4"] = $HMidSumRow4;
		$arg_array["RowNumber"] = $RowNumber;

		// 縮小して全体を表示する
//		$excelObj->SetArea("AG".$KMidSumRow3.":AH".$KMidSumRow4);
//		$excelObj->SetShrinkToFit();

		return $arg_array;
	}
    //////////////////////////////////
    //				//
    //   地域包括ケア看護職員用  //
    // 	 2014年対応		//
    //				//
    //////////////////////////////////
    function ShowNurseSum03( $excelObj , $arg_array ){
        
        $RowNumber = $arg_array["RowNumber"];
        $KMidSumRow1 = $arg_array["KMidSumRow1"];
        $HojoRowSum = $arg_array["HojoRowSum"];
        $H_KEI_RowU = $HojoRowSum;
        $H_KEI_RowL = $HojoRowSum+1;
        
        // 位置情報
        // 看護職員 kanGoのG
        $GMidSumRow1 = $RowNumber + 1;
        $GMidSumRow2 = $GMidSumRow1 + 1;
        $GMidSumRow3 = $GMidSumRow2 + 1;

        ////////////////
        // 中間集計行 //
        ////////////////
        // 1行目
        //$RowNumber ++; //ShowNurseSum01()が2行改行しているためコメント化
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->SetValueJP2("B$RowNumber","〔看護職員配置加算を届け出る場合の看護職員数の算出方法〕","BOLD",$this->MSPgothic,14);
        // 2行目
        $RowNumber ++;
        $RowNext2 = $RowNumber + 2;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("C".$RowNumber.":Y".$RowNumber);
        $excelObj->SetValueJP2("C$RowNumber","１日看護配置数〔F〕　　〔(A/届出区分の数)×３〕","BOLD VCENTER WRAP",$this->MSPgothic,14);
        //[F]=[(A/13)×3]
        $excelObj->CellMerge("Z".$RowNumber.":AA".$RowNumber);
        $excelObj->SetValueJP2("Z$RowNumber","=K17","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        $excelObj->SetValueJP2("AB$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);
        // 3行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("C".$RowNumber.":Y".$RowNumber);
        $excelObj->SetValueJP2("C$RowNumber","月平均１日当たり看護配置数〔G〕　　〔看護職員のみのC/(日数×８(時間)〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
        //[G]=[看護職員のみのC/(日数×8(時間)]
        $excelObj->CellMerge("Z".$RowNumber.":AA".$RowNumber);
        $excelObj->SetValueJP2("Z$RowNumber","=ROUNDDOWN(Z$KMidSumRow1/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        $excelObj->SetValueJP2("AB$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
        // 4行目
        $RowNumber ++;
        $RowPrev2 = $RowNumber - 2;
        $excelObj->SetRowDim($RowNumber,36.00);
        $excelObj->CellMerge("C".$RowNumber.":Y".$RowNumber);
        $excelObj->SetValueJP2("C$RowNumber","月平均１日当たり当該入院料の施設基準の最小必要人数以上の看護職員配置数看護職員数\n{〔看護職員のみのC〕 − （〔F〕×日数×８（時間））} /（日数×８（時間））","BOLD VCENTER WRAP",$this->MSPgothic,14);
        //{[看護職員のみのC] -（[F]×日数×8（時間））} /（日数×８（時間））
        $excelObj->CellMerge("Z".$RowNumber.":AA".$RowNumber);
        $excelObj->SetValueJP2("Z$RowNumber","=MAX(ROUNDDOWN((Z$KMidSumRow1-(Z$RowPrev2*N39*8))/(N39*8),1), 0)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        
        // 空行
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,13.5);

        // 罫線
        // 看護職員
        $excelObj->SetArea("C".$GMidSumRow1.":AA".$GMidSumRow3);
        $excelObj->SetBorder2("medium","","outline");

        $excelObj->SetArea("Y".$GMidSumRow1.":Y".$GMidSumRow3);
        $excelObj->SetBorder2("thin","","right");
        
        $excelObj->SetArea("C".$GMidSumRow2.":AA".$GMidSumRow2);
        $excelObj->SetBorder2("thin","","top");
        $excelObj->SetBorder2("thin","","bottom");
        
        // 色指定
        $excelObj->SetArea("Z".$GMidSumRow2.":AA".$GMidSumRow3);
        $excelObj->SetBackColor($this->green);
        $excelObj->SetArea("Z".$GMidSumRow1.":AA".$GMidSumRow1);
        $excelObj->SetBackColor($this->yellow);
        
        
        // 中間集計　おわり。
        
        $arg_array["GMidSumRow1"] = $GMidSumRow1;
        $arg_array["GMidSumRow2"] = $GMidSumRow2;
        $arg_array["GMidSumRow3"] = $GMidSumRow3;
        $arg_array["RowNumber"] = $RowNumber;
        
        return $arg_array;
    }
    
    //////////////////////////////////
    //				//
    //   地域包括ケア看護補助者用  //
    // 	 2014年対応		//
    //				//
    //////////////////////////////////
    function ShowNurseSum04( $excelObj , $arg_array ){
        
        //2015年04月みなしを除く対応
        global $wk_duty_yyyymm;

        $RowNumber = $arg_array["RowNumber"];
        $KMidSumRow1 = $arg_array["KMidSumRow1"];
        $GMidSumRow1 = $arg_array["GMidSumRow1"];
        $HojoRowSum = $arg_array["HojoRowSum"];
        $nurse_staffing_flg = $arg_array["nurse_staffing_flg"];	// 看護職員配置加算の有無
        $H_KEI_RowU = $HojoRowSum;
        $H_KEI_RowL = $HojoRowSum+1;
        
        // 位置情報
        // 看護補助職員
        $HMidSumRow1 = $RowNumber + 2;
        $HMidSumRow2 = $HMidSumRow1 + 3;
        $HMidSumRow3 = $HMidSumRow2 + 2;
        $HMidSumRow4 = $HMidSumRow3 + 2;
        
        $HNext1m = $HMidSumRow1 + 1;
        $HNext1b = $HMidSumRow1 + 2;
        $HNext2 = $HMidSumRow2 + 1;
        $HNext3 = $HMidSumRow3 + 1;
        $HNext4 = $HMidSumRow4 + 1;
        
        ////////////////
        // 中間集計行 //
        ////////////////
        // 1行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->SetValueJP2("B$RowNumber","〔看護補助者配置加算を届け出る場合の看護補助者の算出方法〕","BOLD",$this->MSPgothic,14);
        // 2行目
        $RowNumber ++;
        $RowNext2 = $RowNumber + 2;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("C".$RowNumber.":I".$RowNext2);
        //取消線 20150318
        $strike_str = ($wk_duty_yyyymm >= "201504") ? "STRIKE" : "";
        $excelObj->SetValueJP2("C$RowNumber","看護補助者の勤務実績(勤務時間数)","BOLD HCENTER VCENTER WRAP $strike_str",$this->MSPgothic,14);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","看護補助者のみの月延べ勤務時間数の計〔H〕　　〔看護補助者のみのC〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        $excelObj->SetValueJP2("AG$RowNumber","=AT$H_KEI_RowU+AT$H_KEI_RowL","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        $excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER",$this->MSPgothic,11);
        // 3行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,36.00);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","みなし看護補助者の月延べ勤務時間数の計〔I〕　　〔I〕のうち、看護職員配置加算を届け出る場合\n〔看護職員のみのC〕 − [〔F〕×日数×８（時間）] − [(A/50)×３×日数×８(時間)]","BOLD VCENTER WRAP $strike_str",$this->MSPgothic,14);
        //[看護職員のみのC] -[[F]×日数×8（時間）]-[(A/50)×3×日数×8(時間)]
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        //斜め線 20150318
        if ($wk_duty_yyyymm >= "201504") {
        	$excelObj->setBorderDiagonal('THIN', 'UP', '000000');
        }
        else {
        	$excelObj->SetValueJP2("AG$RowNumber","=MAX(ROUNDDOWN(Z$KMidSumRow1-(Z$GMidSumRow1*N39*8)-((J14/50)*3*N39*8), 2), 0)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        }
        $excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER $strike_str",$this->MSPgothic,11);
        // 4行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,36.00);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","〔I〕のうち、看護職員配置加算を届け出ない場合\n〔看護職員のみのC〕 − [〔F〕×日数×８（時間）]","BOLD VCENTER WRAP $strike_str",$this->MSPgothic,14);
        //[看護職員のみのC] -[[F]×日数×8（時間）]
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        //斜め線 20150318
        if ($wk_duty_yyyymm >= "201504") {
        	$excelObj->setBorderDiagonal('THIN', 'UP', '000000');
        }
        else {
	        $excelObj->SetValueJP2("AG$RowNumber","=MAX(ROUNDDOWN(Z$KMidSumRow1-(Z$GMidSumRow1*N39*8), 2), 0)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
         }
       $excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER $strike_str",$this->MSPgothic,11);
        
        // 5行目
        $RowNumber ++;
        $RowNext = $RowNumber + 1;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
        $excelObj->SetValueJP2("C$RowNumber","みなし補助者を含む \n 1日当たり看護補助者配置数","BOLD HCENTER VCENTER WRAP $strike_str",$this->MSPgothic,14);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","月平均1日当たり看護補助者配置数(みなし看護補助者含む）　〔(H+I) ／ (日数×８(時間)) 〕","BOLD VCENTER FITSIZE $strike_str",$this->MSPgothic,14);
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        //看護職員配置加算の有無により[I]に対応するセルを変更
        //斜め線 20150318
        if ($wk_duty_yyyymm >= "201504") {
        	$excelObj->setBorderDiagonal('THIN', 'UP', '000000');
        }
        else {
        	$excelObj->SetValueJP2("AG$RowNumber","=ROUNDDOWN((AG$HMidSumRow1+IF(M7=\"有\", AG$HNext1m, AG$HNext1b))/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		}
        $excelObj->SetValueJP2("AI$RowNumber","（実績値）","BOLD VCENTER $strike_str",$this->MSPgothic,11);
        // 6行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","1日看護補助配置数（基準値）〔J〕　　〔(A ／25)×３〕","BOLD VCENTER FITSIZE",$this->MSPgothic,14);
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        $excelObj->SetValueJP2("AG$RowNumber","=K37","BOLD VCENTER FITSIZE",$this->MSPgothic,12); //K34から変更
        $excelObj->SetValueJP2("AI$RowNumber","（基準値）","BOLD VCENTER",$this->MSPgothic,11);
        
        // 7行目
        $RowNumber ++;
        $RowNext = $RowNumber + 1;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("C".$RowNumber.":I".$RowNext);
        $excelObj->SetValueJP2("C$RowNumber","みなし補助者を除く1日当たり看護補助者配置数\nならびに看護補助者の割合","BOLD HCENTER VCENTER WRAP $strike_str",$this->MSPgothic,12);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","月平均1日当たり看護補助者配置数（みなし看護補助者除く）〔K〕　　〔H ／ (日数×８(時間)) 〕","BOLD VCENTER",$this->MSPgothic,14);
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        $excelObj->SetValueJP2("AG$RowNumber","=ROUNDDOWN(AG$HMidSumRow1/(N39*8),1)","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
        // 8行目
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,26.25);
        $excelObj->CellMerge("J".$RowNumber.":AF".$RowNumber);
        $excelObj->SetValueJP2("J$RowNumber","看護補助者（みなし看護補助者を含む）の最小必要数に対する看護補助者（みなし看護補助者を除く）の割合（％）　　〔（K/J）×１００〕","BOLD FITSIZE VCENTER $strike_str",$this->MSPgothic,14);
        $excelObj->CellMerge("AG".$RowNumber.":AH".$RowNumber);
        //斜め線 20150318
        if ($wk_duty_yyyymm >= "201504") {
        	$excelObj->setBorderDiagonal('THIN', 'BOTH', '000000');
        }
        else {
        	$excelObj->SetValueJP2("AG$RowNumber","=IF(ISNUMBER(K34), ROUNDDOWN((AG$HMidSumRow3/AG$HNext2)*100,1), \"\")","BOLD VCENTER FITSIZE",$this->MSPgothic,12);
		}

        // 罫線
        // 看護補助職員
        $excelObj->SetArea("C".$HMidSumRow1.":AH".$HNext3);
        $excelObj->SetBorder2("medium","","outline");
        
        $excelObj->SetArea("C".$HMidSumRow2.":AH".$HNext2);
        $excelObj->SetBorder2("medium","","top");
        $excelObj->SetBorder2("medium","","bottom");
        
        $excelObj->SetArea("C".$HMidSumRow3.":AH".$HNext3);
        $excelObj->SetBorder2("medium","","bottom");
        
        $excelObj->SetArea("J".$HMidSumRow1.":AF".$HNext3);
        $excelObj->SetBorder2("thin","","left");
        $excelObj->SetBorder2("thin","","right");
        
        $excelObj->SetArea("J".$HNext1m.":AH".$HNext1m);
        $excelObj->SetBorder2("thin","","top");
        $excelObj->SetBorder2("thin","","bottom");
        
        $excelObj->SetArea("J".$HMidSumRow2.":AH".$HMidSumRow2);
        $excelObj->SetBorder2("thin","","bottom");
        
        $excelObj->SetArea("J".$HMidSumRow3.":AH".$HMidSumRow3);
        $excelObj->SetBorder2("thin","","bottom");
        
        // 色指定
        $excelObj->SetArea("AG".$HMidSumRow1.":AH".$HMidSumRow2);
        $excelObj->SetBackColor($this->green);
        $excelObj->SetArea("AG".$HNext2.":AH".$HNext2);
        $excelObj->SetBackColor($this->yellow);
        
        $excelObj->SetArea("AG".$HMidSumRow3.":AH".$HNext3);
        $excelObj->SetBackColor($this->green);
        
        // 中間集計　おわり。
        
        // 空行2行
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,13.5);
        $RowNumber ++;
        $excelObj->SetRowDim($RowNumber,13.5);
        
        $arg_array["HMidSumRow1"] = $HMidSumRow1;
        $arg_array["HMidSumRow2"] = $HMidSumRow2;
        $arg_array["HMidSumRow3"] = $HMidSumRow3;
        $arg_array["HMidSumRow4"] = $HMidSumRow4;
        $arg_array["RowNumber"] = $RowNumber;
        
        return $arg_array;
    }
    
    //////////////////////////////////////////////////////
	//						    //
	// 月総夜勤時間別の看護職員数（夜勤従事者数）の分布 //
	//						    //
	// 一般病棟・療養病棟１、２、共通                   //
	//						    //
	// 		 2012年対応			    //
	//						    //
	//////////////////////////////////////////////////////
	function ShowNurseSum3( $excelObj , $arg_array ){

		$RowNumber	= $arg_array["RowNumber"];
		$KMidSumRow1	= $arg_array["KMidSumRow1"];
		$KangoRowDataTop= $arg_array["KangoRowDataTop"];
		$KangoRowSum	= $arg_array["KangoRowSum"];
		$HojoRowDataTop = $arg_array["HojoRowDataTop"];
		$HojoRowSum	= $arg_array["HojoRowSum"];

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$excelObj->SetValueJP2("M".$RowNumber, "参考", "BOLD HCENTER VCENTER",$this->MSPgothic ,12);
		$excelObj->SetBorder2("medium","","outline");

		$excelObj->SetValueJP2("N".$RowNumber, "月総夜勤時間別の看護職員数（夜勤従事者数）の分布　　…「入院基本料等に関する実施状況報告書」用", "BOLD UNDER",$this->MSPgothic ,14);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,17.25);

		$excelObj->SetValueJP2("N".$RowNumber, "※夜勤専従者数は再掲として（　）に人数を記入のこと", "BOLD",$this->MSPmincho ,10);

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		// 行が前後するので要注意
		// M列〜A列のセル結合、3表まとめて処理
		for($k=12; $k<38; $k+=2){ // 2012対応済み
			// 上下
			for($ss=0;$ss<3;$ss++){
				$stp = $ss * 4;
				$rwk = $RowNumber + 1 + $stp;
				$CellArea1 = $this->excelColumnName[$k].$rwk;
				$CellArea2 = $this->excelColumnName[$k+1].$rwk;
				$excelObj->CellMerge($CellArea1.":".$CellArea2);
			}
		}

		$excelObj->SetValueJP2("M".$RowNumber, "【看護職員】", "BOLD VCENTER",$this->MSPgothic ,11);

		$RowNumber ++;  // 見出し行

		// L列〜AJ列の見出し設定、3表まとめてつける
		for($ss=0;$ss<3;$ss++){
			$stp = $ss * 4;
			$rwk = $RowNumber + $stp;
			$excelObj->SetValueJPwith( "M".$rwk, "夜勤時間数", "");
			$excelObj->SetValueJPwith( "O".$rwk, "16時間未満", "");
			$excelObj->SetValueJPwith( "Q".$rwk, "16〜32時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith( "S".$rwk, "32〜48時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith( "U".$rwk, "48〜64時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith( "W".$rwk, "64〜72時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith( "Y".$rwk, "72〜80時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AA".$rwk, "80〜88時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AC".$rwk, "88〜96時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AE".$rwk, "96〜112時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AG".$rwk, "112〜144時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AI".$rwk, "144〜152時間", "");
			$excelObj->SetBorder2("thin","","left");
			$excelObj->SetValueJPwith("AK".$rwk, "152時間以上", "");
			$excelObj->SetBorder2("thin","","left");

			$excelObj->SetArea("M".$rwk.":AK".$rwk);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("CENTER");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetShrinkToFit();
		}

		$RowNumber ++;
		$GSumRow1 = $RowNumber;
		$excelObj->SetRowDim($RowNumber,21);

		$CalcArrayL = array();
		$CalcArrayR = array();

		$excelObj->CellMerge("M".$RowNumber.":N".$RowNumber);

		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11);

		$CellArea1= $KangoRowDataTop - 1;
		$CellArea2= $KangoRowSum + 1;

		$CalcArrayL[0] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayL[1] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayL[2] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")";
		$CalcArrayL[3] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")";
		$CalcArrayL[4] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")";
		$CalcArrayL[5] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")";
		$CalcArrayL[6] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")";
		$CalcArrayL[7] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")";
		$CalcArrayL[8] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")";
		$CalcArrayL[9] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")";
		$CalcArrayL[10]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<152\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")";
		$CalcArrayL[11]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\">=152\")";

		$CalcArrayR[0] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<16\")";
		$CalcArrayR[1] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<32\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<16\")";
		$CalcArrayR[2] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<48\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<32\")";
		$CalcArrayR[3] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<64\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<48\")";
		$CalcArrayR[4] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<72\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<64\")";
		$CalcArrayR[5] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<80\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<72\")";
		$CalcArrayR[6] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<88\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<80\")";
		$CalcArrayR[7] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<96\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<88\")";
		$CalcArrayR[8] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<112\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<96\")";
		$CalcArrayR[9] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<144\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<112\")";
		$CalcArrayR[10]= "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<152\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<144\")";
		$CalcArrayR[11]= "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\">=152\")";

		// O列〜AL列の計算式 【看護職員】
		$c=0;
		for($k=14; $k<=36; $k+=2){ // 2012対応済み
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayL[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("RIGHT");

			$CellArea = $this->excelColumnName[$k+1].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayR[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("LEFT");
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}

		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->SetValueJP2("M".$RowNumber, "【看護補助者】", "BOLD VCENTER",$this->MSPgothic ,11);

		$excelObj->CellMerge("M".$RowNumber.":N".$RowNumber);

		$RowNumber ++; // 見出し行
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);
		$GSumRow2 = $RowNumber;

		$excelObj->CellMerge("M".$RowNumber.":N".$RowNumber);

		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11);

		$CellArea1= $HojoRowDataTop - 1;
		$CellArea2= $HojoRowSum + 1;

		$CalcArrayL[0] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayL[1] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<16\")";
		$CalcArrayL[2] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<32\")";
		$CalcArrayL[3] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<48\")";
		$CalcArrayL[4] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<64\")";
		$CalcArrayL[5] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<72\")";
		$CalcArrayL[6] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<80\")";
		$CalcArrayL[7] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<88\")";
		$CalcArrayL[8] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<96\")";
		$CalcArrayL[9] = "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<112\")";
		$CalcArrayL[10]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<152\")-COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\"<144\")";
		$CalcArrayL[11]= "=COUNTIF(\$AZ$CellArea1:\$AZ$CellArea2,\">=152\")";

		$CalcArrayR[0] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<16\")";
		$CalcArrayR[1] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<32\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<16\")";
		$CalcArrayR[2] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<48\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<32\")";
		$CalcArrayR[3] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<64\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<48\")";
		$CalcArrayR[4] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<72\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<64\")";
		$CalcArrayR[5] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<80\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<72\")";
		$CalcArrayR[6] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<88\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<80\")";
		$CalcArrayR[7] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<96\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<88\")";
		$CalcArrayR[8] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<112\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<96\")";
		$CalcArrayR[9] = "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<144\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<112\")";
		$CalcArrayR[10]= "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<152\")-COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\"<144\")";
		$CalcArrayR[11]= "=COUNTIF(\$BA$CellArea1:\$BA$CellArea2,\">=152\")";

		// N列〜AK列の計算式【看護補助者】
		$c=0;
		for($k=14; $k<=36; $k+=2){	// 2012対応済み
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayL[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("RIGHT");

			$CellArea = $this->excelColumnName[$k+1].$RowNumber;
			$excelObj->SetValuewith($CellArea, $CalcArrayR[$c], "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("LEFT");
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}
		// 空行
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);

		$excelObj->CellMerge("M".$RowNumber.":Q".$RowNumber);
		$excelObj->SetValueJP2("M".$RowNumber, "【看護職員＋看護補助者】", "BOLD VCENTER",$this->MSPgothic ,11);

		$excelObj->CellMerge("M".$RowNumber.":N".$RowNumber);

		$RowNumber ++; // 見出し行
		$excelObj->SetRowDim($RowNumber,19);

		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,21);
		$GSumRow3 = $RowNumber;

		$excelObj->CellMerge("M".$RowNumber.":N".$RowNumber);

		$excelObj->SetValueJP2("M".$RowNumber, "夜勤従事者数", "VCENTER HCENTER",$this->MSPgothic ,11);

		// N列〜AK列の計算式【看護職員＋看護補助者】
		$c=0;
		for($k=14; $k<=36; $k+=2){	// 2012対応済み
			$CellArea = $this->excelColumnName[$k].$RowNumber;
			$formulae = "=".$this->excelColumnName[$k].$GSumRow1."+".$this->excelColumnName[$k].$GSumRow2;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("RIGHT");

			$wk=$k+1;
			$CellArea = $this->excelColumnName[$wk].$RowNumber;
			$formulae = "=".$this->excelColumnName[$wk].$GSumRow1."+".$this->excelColumnName[$wk].$GSumRow2;
			$excelObj->SetValuewith($CellArea, $formulae, "");
			$excelObj->SetFont($this->MSPgothic ,11);
			$excelObj->SetPosV("CENTER");
			$excelObj->SetPosH("LEFT");
			$excelObj->SetFormatStyle("(0)");
			$excelObj->SetBorder2("thin","","right");

			$c++;
		}

		// 罫線
		$a = $GSumRow1 - 1;
		$b = $GSumRow1;
		$CellArea = "M".$a.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "M".$b.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("thin","","top");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$CellArea = "M".$a.":N".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$a = $GSumRow2 - 1;
		$b = $GSumRow2;
		$CellArea = "M".$a.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "M".$b.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "M".$a.":N".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$a = $GSumRow3 - 1;
		$b = $GSumRow3;
		$CellArea = "M".$a.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("medium","","outline");
//		$excelObj->SetBorder2("thin","","vertical");
		$CellArea = "M".$b.":AL".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetBorder2("thin","","top");
		$CellArea = "M".$a.":N".$b;
		$excelObj->SetArea($CellArea);
		$excelObj->SetBorder2("double","","right");
		$excelObj->SetBackColorCancel(); // 色外す

		$arg_array["GSumRow3"] = $GSumRow3;
		return $arg_array;
	}

	/////////////////////////////////////////////////////
	//						   //
	// 一般病棟 入院基本料に係る届出書添付書類(様式９) //
	//		ＴＯＰ	2012年度版		   //
    //		　　　	2014年度版地域包括ケア対応を追加 20140325 //
    //						   //
	/////////////////////////////////////////////////////
	function ShowData_0(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt, //看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$report_kbn_count	// 一般病棟、n対１入院基本料金
		){
		$KangoRowDataTop= $arg_array["KangoRowDataTop"];
		$KangoRowSum	= $arg_array["KangoRowSum"];
		$RowNumber	= $arg_array["RowNumber"];
		$KMidSumRow1	= $arg_array["KMidSumRow1"];
		$KMidSumRow2	= $arg_array["KMidSumRow2"];
		$KMidSumRow3	= $arg_array["KMidSumRow3"];
		$HojoRowDataTop	= $arg_array["HojoRowDataTop"];
		$HojoRowSum	= $arg_array["HojoRowSum"];
		$HMidSumRow1	= $arg_array["HMidSumRow1"];
		$HMidSumRow2	= $arg_array["HMidSumRow2"];
		$HMidSumRow3	= $arg_array["HMidSumRow3"];
		$HMidSumRow4	= $arg_array["HMidSumRow4"];

		$GMidSumRow3	= $arg_array["GMidSumRow3"];

		$hospital_name	= $arg_array["hospital_name"];		//保険医療機関名
		$ward_cnt	= $arg_array["ward_cnt"];		//病棟数
		$sickbed_cnt	= $arg_array["sickbed_cnt"];		//病床数
		$acute_assistance_cnt 	    = $arg_array["acute_assistance_cnt"];	// 急性期看護補助体制加算の届出区分
		$night_acute_assistance_cnt = $arg_array["night_acute_assistance_cnt"];	//夜間急性期看護補助体制加算の届出区分
		$night_shift_add_flg 	    = $arg_array["night_shift_add_flg"];	//看護職員夜間配置加算の有無

		$KangoRowNext	= $KangoRowSum + 1;
		$HojoRowNext	= $HojoRowSum + 1;
		$KMidNext1	= $KMidSumRow1 + 1;
		$KMidNext2	= $KMidSumRow2 + 1;
		$KMidNext3	= $KMidSumRow3 + 1;
		$HMidSumNext1m	= $HMidSumRow1 + 1;
		$HMidSumNext1b	= $HMidSumRow1 + 2;

		// 縦位置中央の設定はあとでまとめて
		// 5行目
		$RowNumber=5;
		$excelObj->SetRowDim($RowNumber,18);
        $excelObj->SetValueJP2("E$RowNumber","届出区分","BOLD",$this->MSPgothic,12);
        $excelObj->SetArea("I$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValueJP2("I$RowNumber",$report_kbn_count,"BOLD",$this->MSPgothic,11);
        $excelObj->SetValueJP2("J$RowNumber","対１入院基本料","BOLD",$this->MSPgothic,14);
		$excelObj->SetValueJP2("N$RowNumber","届出時入院患者数","",$this->MSPgothic,11);
		$excelObj->CellMerge("Q$RowNumber:S$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Q$RowNumber",$report_patient_cnt,"",$this->MSPgothic,11);
		// 6行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,14.25);
		// 7行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,19.50);
        //地域包括ケア 20140325
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("E$RowNumber","看護職員配置加算(50対1)の有無","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $wk_night_shift_add_str = ($nurse_staffing_flg == "1") ? "有" : "無";
            $excelObj->SetValueJP2("M$RowNumber",$wk_night_shift_add_str,"BOLD",$this->MSPgothic,11);
        }
        else {
            $excelObj->SetValueJP2("E$RowNumber","看護配置加算の有無","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $wk_night_shift_add_str = ($nurse_staffing_flg == "1") ? "有" : "無";
            $excelObj->SetValueJP2("M$RowNumber",$wk_night_shift_add_str,"BOLD",$this->MSPgothic,11);
        }
        // 8行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18);
        //地域包括ケア 20140325
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("E$RowNumber","看護補助者配置加算(25対1)の届出区分","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $wk_nursing_assistanc_add_str = ($nursing_assistance_flg == "1") ? "有" : "無";
            $excelObj->SetValueJP2("M$RowNumber",$wk_nursing_assistanc_add_str,"BOLD",$this->MSPgothic,11);
        }
        else {
            $excelObj->SetValueJP2("E$RowNumber","急性期看護補助体制加算の届出区分","BOLD",$this->MSPgothic,12);
            $excelObj->SetValueJP2("K$RowNumber","=IF(M8>0, \"\", \"無\")","BOLD",$this->MSPgothic,14);
            $excelObj->SetValueJP2("L$RowNumber","（","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("M$RowNumber",$acute_assistance_cnt,"BOLD",$this->MSPgothic,11); // <---【新】急性期看護補助体制加算の届出区分
            //				     $acute_assistance_cnt
            // *** 475 -> $HMidSumRow3 + 1
            $wkrow = $HMidSumRow3 + 1;
            $excelObj->SetValueJP2("N8","=IF(M$RowNumber=25, IF(AG$wkrow>=0.5, \"対１　）　看護補助者５割以上\", \"対１　）　看護補助者５割未満\"), \"対１　）\")","BOLD",$this->MSPgothic,14);
        }
		// 9行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("E$RowNumber","夜間急性期看護補助体制加算の届出区分","BOLD",$this->MSPgothic,12);
            $excelObj->SetValueJP2("L$RowNumber","=IF(N9>0, \"\", \"無\")","BOLD",$this->MSPgothic,14);
            $excelObj->SetValueJP2("M$RowNumber","（","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("N$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("O$RowNumber","対１　）","BOLD",$this->MSPgothic,14);
            $excelObj->SetValueJP2("N$RowNumber", $night_acute_assistance_cnt ,"BOLD",$this->MSPgothic,14); // <---【新】夜間急性期看護補助体制加算の届出区分
        }
		//				      $night_acute_assistance_cnt
		// 10行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("E$RowNumber","看護職員夜間配置加算の有無","BOLD",$this->MSPgothic,12);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $wk_night_shift_add_str = ($night_shift_add_flg == "1") ? "有" : "無";
            $excelObj->SetValueJP2("M$RowNumber",$wk_night_shift_add_str,"BOLD",$this->MSPgothic,12); // <---【新】看護職員夜間配置加算の有無
        }
		// 11行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("E$RowNumber","看護補助加算の届出区分","BOLD",$this->MSPgothic,12);
            $excelObj->SetValueJP2("K$RowNumber","=IF(M$RowNumber=30, \"1\", IF(M$RowNumber=50, \"2\", IF(M$RowNumber=75, \"3\", \"無\")))","BOLD",$this->MSPgothic,14);
            $excelObj->SetValueJP2("L$RowNumber","（","BOLD",$this->MSPgothic,14);
            $excelObj->SetArea("M$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("N$RowNumber","対１　）","BOLD",$this->MSPgothic,14);
            $excelObj->SetValueJP2("M$RowNumber",$nursing_assistance_cnt,"BOLD",$this->MSPgothic,14);
        }
		// 12行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);
		// 13行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,9);
		// 14行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E$RowNumber","○１日平均入院患者数〔A〕","BOLD",$this->MSPgothic,14);
		$excelObj->CellMerge("J$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("J14",$hosp_patient_cnt,"BOLD",$this->MSPgothic,12);
		$excelObj->SetValueJP2("M14","人","",$this->MSPgothic,14);

		$excelObj->SetValueJP2("N$RowNumber","(算出期間）","",$this->MSPgothic,11);
		$excelObj->SetArea("P$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("P$RowNumber",$compute_start_yyyy,"",$this->MSPgothic,11);
		$excelObj->SetValueJP2("Q$RowNumber","年","",$this->MSPgothic,11);

		$excelObj->SetArea("R$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("R$RowNumber",$compute_start_mm,"",$this->MSPgothic,11);
		$excelObj->SetValueJP2("S$RowNumber","月〜","",$this->MSPgothic,11);

		$excelObj->SetArea("T$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("T$RowNumber",$compute_end_yyyy,"",$this->MSPgothic,11);
		$excelObj->SetValueJP2("U$RowNumber","年","",$this->MSPgothic,11);

		$excelObj->SetArea("V$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("V$RowNumber",$compute_end_mm,"",$this->MSPgothic,11);
		$excelObj->SetValueJP2("W$RowNumber","月","",$this->MSPgothic,11);
		// 15行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E$RowNumber","※端数切上げ整数入力","",$this->MSPgothic,11);
		// 16行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber","�〃酳振傳影�当たり看護配置数","BOLD",$this->MSPgothic,14);
		$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(U$KMidSumRow2,1)","BOLD",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("M$RowNumber","人(実績値)","BOLD",$this->MSPgothic,14);
		$excelObj->SetValueJP2("X$RowNumber","『月延べ勤務時間数』","BOLD RIGHT",$this->MSPgothic,12);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=Z$KMidSumRow1","BOLD",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$RowNumber","時間(実績値)","BOLD",$this->MSPgothic,12);
		// 17行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C$RowNumber","1日看護配置数　　〔(A ／配置比率)×３〕※端数切上げ","",$this->MSPgothic,11);
		$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValueJP2("K$RowNumber","=IF(AND(ISNUMBER(J14),ISNUMBER(I5)),ROUNDUP(((J14/I5)*3),0), \"\")","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->yellow);
		$excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,14);
		$excelObj->SetValueJP2("X$RowNumber","※「1日平均看護配置数」を満たす「月延べ勤務時間数」","RIGHT",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K17), ISNUMBER(N39)), K17*8*N39, \"\")","",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->yellow);
		$excelObj->SetValueJP2("AA$RowNumber","時間(基準値)","",$this->MSPgothic,11);
		// 18行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("C$RowNumber","＜看護職員夜間配置加算を届け出る場合＞","BOLD",$this->MSPgothic,14);
        }
		// 19行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("C$RowNumber","��-2　うち、月平均１日当たり夜間看護配置数","",$this->MSPgothic,11);
            $excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(U$KMidSumRow3,1)","BOLD",$this->MSPgothic,12);
            $excelObj->SetBackColor($this->green);
            $excelObj->SetValueJP2("M$RowNumber","人(実績値)","",$this->MSPgothic,14);
        }
		$excelObj->SetValueJP2("X$RowNumber","『月延べ夜勤時間数』","RIGHT BOLD",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=Z$KMidNext1","BOLD",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$RowNumber","時間（実績値）","BOLD",$this->MSPgothic,11);
		// 20行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber","1日夜間看護配置数 〔A/12〕※端数切上げ","",$this->MSPgothic,11);
		$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("K$RowNumber","=IF(ISNUMBER(J14), ROUNDUP(J14/12,0), \"\")","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->yellow);
		$excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("X$RowNumber","※「１日夜間看護配置数」を満たす「月延べ夜勤時間数」","RIGHT",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K20), ISNUMBER(N39)), K20*16*N39, \"\")","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->yellow);
		$excelObj->SetValueJP2("AA$RowNumber","時間（基準値）","",$this->MSPgothic,11);
		// 21行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,17.25);
		// 22行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,15);
		// 23行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber","��看護職員中の看護師の比率","BOLD",$this->MSPgothic,14);
		$excelObj->CellMerge("J$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("J$RowNumber","=ROUNDDOWN(IF((Y23/Y17)*100>100,100,(Y23/Y17)*100),1)","BOLD",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("M$RowNumber","％","BOLD",$this->MSPgothic,11);
		$excelObj->SetValueJP2("N$RowNumber","看護要員の内訳","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("R$RowNumber","看護師","RIGHT",$this->MSPgothic,11);
		$excelObj->SetArea("S$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("S$RowNumber","=ROUNDDOWN(AX$KangoRowNext,2)","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("T$RowNumber","人","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("U$RowNumber","月間総勤務時間数","",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=AV$KangoRowSum+AV$KangoRowNext","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$RowNumber","時間","",$this->MSPgothic,11);
		// 24行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,15);
		$excelObj->SetValueJP2("C$RowNumber","月平均1日当たり配置数","BOLD",$this->MSPgothic,11);
		$excelObj->SetValueJP2("I$RowNumber","看護師","BOLD",$this->MSPgothic,11);
		$excelObj->CellMerge("J$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("J$RowNumber","=ROUNDDOWN((AV$KangoRowSum+AV$KangoRowNext)/(N39*8),1)","BOLD",$this->MSPgothic,12);
		$excelObj->SetValueJP2("M$RowNumber","人","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("R$RowNumber","准看護師","RIGHT",$this->MSPgothic,11);
		$excelObj->SetValueJP2("S$RowNumber","=ROUNDDOWN(AY$KangoRowNext,2)","",$this->MSPgothic,11);
		$excelObj->SetArea("S$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("T$RowNumber","人","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("U$RowNumber","月間総勤務時間数","",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=AW$KangoRowSum+AW$KangoRowNext","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$RowNumber","時間","",$this->MSPgothic,11);
		// 25行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("R$RowNumber","看護補助者","RIGHT",$this->MSPgothic,11);
		$excelObj->SetValueJP2("S$RowNumber","=ROUNDDOWN(AV$HojoRowNext,2)","",$this->MSPgothic,11);
		$excelObj->SetArea("S$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("T$RowNumber","人","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("U$RowNumber","月間総勤務時間数","",$this->MSPgothic,11);
		$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("Y$RowNumber","=AT$HojoRowSum+AT$HojoRowNext","",$this->MSPgothic,11);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$RowNumber","時間","",$this->MSPgothic,11);
		// 26行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,11.25);
		// 27行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber","�Ｊ振兀澑‘�数 ※端数切上げ整数入力","BOLD",$this->MSPgothic,14);
		$excelObj->CellMerge("J$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("J$RowNumber",$hospitalization_cnt,"BOLD",$this->MSPgothic,12); // 平均在院日数 $hospitalization_cnt
		$excelObj->SetValueJP2("M$RowNumber","日","BOLD",$this->MSPgothic,12);
		$excelObj->SetValueJP2("N$RowNumber","(算出期間）","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("P$RowNumber",$ave_start_yyyy,"",$this->MSPgothic,11);	// 開始年
		$excelObj->SetValueJP2("Q$RowNumber","年","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("R$RowNumber",$ave_start_mm,"",$this->MSPgothic,11);	// 開始月
		$excelObj->SetValueJP2("S$RowNumber","月〜","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("T$RowNumber",$ave_end_yyyy,"",$this->MSPgothic,11);	// 終了年
		$excelObj->SetValueJP2("U$RowNumber","年","",$this->MSPgothic,11);
		$excelObj->SetValueJP2("V$RowNumber",$ave_end_mm,"",$this->MSPgothic,11);	// 終了月
		$excelObj->SetValueJP2("W$RowNumber","月","",$this->MSPgothic,11);
		// 枠
		$excelObj->SetArea("P$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("R$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("T$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("V$RowNumber");$excelObj->SetBorder("MEDIUM","outline");

		// 28行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,17.25);
		// 29行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,17.25);
		$excelObj->SetValueJP2("C$RowNumber","�ぬ覿仍�間帯(16時間)","BOLD",$this->MSPgothic,14);
		$wk = " ".$prov_start_hh."時".$prov_start_mm."分 ";	//夜勤時間帯、開始時分
		$excelObj->SetValueJP2("I$RowNumber", $wk ,"BOLD UNDER",$this->MSPgothic ,12);
		$excelObj->SetValueJP2("M$RowNumber","〜","",$this->MSPgothic,14);
		$wk   = " ".intval($prov_end_hh, 10)."時".$prov_end_mm."分 ";	//夜勤時間帯、終了時分
		$excelObj->SetValueJP2("N$RowNumber", $wk ,"BOLD UNDER",$this->MSPgothic ,12);
		// 30行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.50);
		// 31行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->CellMerge("J$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("C$RowNumber","�シ酳振冖覿仍�間数〔(D-E)／B〕","BOLD",$this->MSPgothic,14);
		$excelObj->SetValueJP2("J$RowNumber","=ROUNDDOWN(Q$KMidNext1/Q$KMidSumRow1,1)","BOLD",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("M$RowNumber","時間","BOLD",$this->MSPgothic,14);
		//20150320
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
			$excelObj->SetValueJP2("X$RowNumber","�ァ併温諭亡埜酳篏�者（みなしは除く）の月平均夜勤時間数","RIGHT BOLD",$this->MSPgothic,11);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetBackColor($this->green);
            $excelObj->SetValueJP2("Y$RowNumber","=ROUNDDOWN((AG$HMidSumNext1b-AU$HojoRowNext)/ROUNDDOWN(M$HojoRowNext,1),1)","",$this->MSPgothic,11);
	            
			$excelObj->SetValueJP2("AA$RowNumber","時間","",$this->MSPgothic,11);
		}
		// 32行目
		$RowNumber++;
        //地域包括ケア以外の場合 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
			$excelObj->SetRowDim($RowNumber,18.75);
            $excelObj->SetValueJP2("C$RowNumber","=IF(ISNUMBER(J31), IF(J31>79.2, \"7対1特別入院基本料、10対1特別入院基本料の届出が必要となります。\", IF(J31>72, \"72時間を超過していますので、翌月には改善するようにしましょう。\", \"\")), \"\")","BOLD",$this->MSPgothic,14);
			$excelObj->SetValueJP2("Q$RowNumber","[（看護補助者のみの夜勤時間数（Ｈ）−夜勤専従者及び月16時間以下の者の夜勤時間数）/看護補助者の夜勤従事者数]","",$this->MSPgothic,11);
        }
        else {
			$excelObj->SetRowDim($RowNumber,43.90);
            $excelObj->SetValueJP2("C$RowNumber","＜看護職員配置加算を届け出る場合＞". "\r\n" ."月平均１日当たり当該入院料の施設基準の最小必要人数以上の看護職員配置数看護職員数","BOLD",$this->MSPgothic,14);
			$excelObj->SetWrapText();
			$excelObj->CellMerge("C$RowNumber:T$RowNumber");
			$wk_kaigyoarea = "C$RowNumber:T$RowNumber";
			//$excelObj->SetPosV("BOTTOM");
        }
		// 33行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
			$excelObj->SetValueJP2("C$RowNumber","�Ψ酳振傳影�当たり看護補助者配置数","BOLD",$this->MSPgothic,14);
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(AG$HMidSumRow2,1)","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("M$RowNumber","人(実績値)","BOLD",$this->MSPgothic,14);
			$excelObj->SetValueJP2("X$RowNumber","『月延べ勤務時間数』","RIGHT BOLD",$this->MSPgothic,12);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
	        //地域包括ケア、看護職員配置加算がなしの場合、行を変更（2番目の[I]）
	        if (($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) && $nurse_staffing_flg != "1") {
	            $wkrow = $HMidSumNext1b;
	        }
	        else {
	            $wkrow = $HMidSumNext1m; //みなし看護補助者の月延べ勤務時間数の計
	        }
	        $excelObj->SetValueJP2("Y$RowNumber","=AG$HMidSumRow1+AG$wkrow","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("AA$RowNumber","時間(実績値)","BOLD",$this->MSPgothic,12);
		}
		else {
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(Z$GMidSumRow3,1)","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("M$RowNumber","人(実績値)","BOLD",$this->MSPgothic,14);
			//右側『月延べ勤務時間数』
			$excelObj->SetValueJP2("X$RowNumber","『月延べ勤務時間数』","RIGHT BOLD",$this->MSPgothic,12);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
	        //=ROUNDDOWN(Z247-(AB250*(N35*8)),2)
	        $excelObj->SetValueJP2("Y$RowNumber","=ROUNDDOWN(Z$KMidSumRow1-(U$KMidNext2*(N39*8)),2)","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("AA$RowNumber","時間(実績値)","BOLD",$this->MSPgothic,12);
		}
		// 34行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
	        //地域包括ケア 20140325
	        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
	            $wk_hiritsu_str = "25";
	        }
	        else {
	            $wk_hiritsu_str = "配置比率";
	        }
	        $excelObj->SetValueJP2("C$RowNumber","1日看護補助者配置数　　〔(A ／$wk_hiritsu_str)×３〕※端数切上げ","",$this->MSPgothic,11);
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
	        //地域包括ケア 20140325
	        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
	            $excelObj->SetValueJP2("K$RowNumber","=IF(ISNUMBER(J14), ROUNDUP((J14/25*3),0), \"\")","",$this->MSPgothic,11);
	        }
	        else {
	            $excelObj->SetValueJP2("K$RowNumber","=IF(AND(ISNUMBER(J14), OR(AND(I5>=13, ISNUMBER(M11)), AND(I5<13, ISNUMBER(M8)))), ROUNDUP((J14/IF(I5>=13, M11, M8)*3),0), \"\")","",$this->MSPgothic,11);
	        }
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,11);
			$excelObj->SetValueJP2("X$RowNumber","※「1日平均看護補助者配置数」を満たす「月延べ勤務時間数」","RIGHT",$this->MSPgothic,11);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K34), ISNUMBER(N39)), K34*8*N39, \"\")","",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("AA$RowNumber","時間(基準値)","",$this->MSPgothic,11);
		}
		else {
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("K$RowNumber","=IF(ISNUMBER(J14), ROUNDUP((J14/50*3),0), \"\")","",$this->MSPgothic,11);
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,11);

			$excelObj->SetValueJP2("X$RowNumber","※「1日平均看護職員配置数」を満たす「月延べ勤務時間数」","RIGHT",$this->MSPgothic,11);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K34), ISNUMBER(N39)), K34*8*N39, \"\")","",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("AA$RowNumber","時間(基準値)","",$this->MSPgothic,11);
		}
		// 35行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("C$RowNumber","＜夜間急性期看護補助体制加算を届け出る場合＞","BOLD",$this->MSPgothic,14);
        }
        else {
            $excelObj->SetValueJP2("C$RowNumber","＜看護補助者配置加算を届け出る場合＞","BOLD",$this->MSPgothic,14);
        }
		// 36行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("C$RowNumber","��-2　うち、月平均１日当たり夜間看護補助者配置数","",$this->MSPgothic,11);
            $excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(AG$HMidSumRow4,1)","",$this->MSPgothic,12);
            $excelObj->SetBackColor($this->green);
            $excelObj->SetValueJP2("M$RowNumber","人(実績値) ","",$this->MSPgothic,14);

			$excelObj->SetValueJP2("X$RowNumber","『月延べ夜勤時間数』","BOLD RIGHT",$this->MSPgothic,11);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			//$excelObj->SetValueJP2("Y$RowNumber","=AG$HMidSumRow1+AG$HMidSumNext1m","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
		    $excelObj->SetValueJP2("Y$RowNumber","=AG$HMidSumNext1b","",$this->MSPgothic,11);
			$excelObj->SetValueJP2("AA$RowNumber","時間（実績値）","",$this->MSPgothic,11);
        }
        //20150320
        else {
	        $wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
	        if ($wk_duty_yyyymm >= "201504") {
	        	$wkrow = $HMidSumRow2 + 2; //行を変更、月平均1日当たり看護補助者配置数（みなし看護補助者除く）〔K〕
	        }
	        else {
		        $wkrow = $HMidSumRow2;
	        }
			$excelObj->SetValueJP2("C$RowNumber","�Ψ酳振傳影�当たり看護補助者配置数","BOLD",$this->MSPgothic,14);
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("K$RowNumber","=ROUNDDOWN(AG$wkrow,1)","BOLD",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("M$RowNumber","人(実績値)","BOLD",$this->MSPgothic,14);
			$excelObj->SetValueJP2("X$RowNumber","『月延べ勤務時間数』","RIGHT BOLD",$this->MSPgothic,12);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
	        //地域包括ケア、看護職員配置加算がなしの場合、行を変更（2番目の[I]）
	        if (($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) && $nurse_staffing_flg != "1") {
	            $wkrow = $HMidSumNext1b;
	        }
	        else {
	            $wkrow = $HMidSumNext1m; //みなし看護補助者の月延べ勤務時間数の計
	        }
	        if ($wk_duty_yyyymm >= "201504") { //+AG$wkrow（みなし分）を除く
		        $excelObj->SetValueJP2("Y$RowNumber","=AG$HMidSumRow1","BOLD",$this->MSPgothic,12);
	        }
	        else {
		        $excelObj->SetValueJP2("Y$RowNumber","=AG$HMidSumRow1+AG$wkrow","BOLD",$this->MSPgothic,12);
	        }
			$excelObj->SetBackColor($this->green);
			$excelObj->SetValueJP2("AA$RowNumber","時間(実績値)","BOLD",$this->MSPgothic,12);
        }
		// 37行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
        //地域包括ケア 20140325
        if ($report_kbn != REPORT_KBN_COMMUNITY_CARE && $report_kbn != REPORT_KBN_COMMUNITY_CARE2) {
            $excelObj->SetValueJP2("C$RowNumber","1日夜間看護補助者配置数 〔A/配置比率〕","",$this->MSPgothic,11);
            $excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("K$RowNumber","=IF(AND(ISNUMBER(J14), ISNUMBER(N9)), ROUNDUP(J14/N9,0), \"\")","BOLD",$this->MSPgothic,12);
            $excelObj->SetBackColor($this->yellow);
            $excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,14);
            $excelObj->SetValueJP2("X$RowNumber","※「1日夜間看護補助者配置数」を満たす「月延べ夜勤時間数」","RIGHT",$this->MSPgothic,11);
            $excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K37), ISNUMBER(N39)), K37*16*N39, \"\")","BOLD",$this->MSPgothic,12);
            $excelObj->SetBackColor($this->yellow);
            $excelObj->SetValueJP2("AA$RowNumber","時間（基準値）","",$this->MSPgothic,11);
        }
		//20150320
        else {
            $wk_hiritsu_str = "25";
	        $excelObj->SetValueJP2("C$RowNumber","1日看護補助者配置数　　〔(A ／$wk_hiritsu_str)×３〕※端数切上げ","",$this->MSPgothic,11);
			$excelObj->CellMerge("K$RowNumber:L$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
            $excelObj->SetValueJP2("K$RowNumber","=IF(ISNUMBER(J14), ROUNDUP((J14/25*3),0), \"\")","",$this->MSPgothic,11);
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("M$RowNumber","人(基準値)","",$this->MSPgothic,11);
			$excelObj->SetValueJP2("X$RowNumber","※「1日平均看護補助者配置数」を満たす「月延べ勤務時間数」","RIGHT",$this->MSPgothic,11);
			$excelObj->CellMerge("Y$RowNumber:Z$RowNumber");$excelObj->SetBorder("MEDIUM","outline");
			$excelObj->SetValueJP2("Y$RowNumber","=IF(AND(ISNUMBER(K37), ISNUMBER(N39)), K37*8*N39, \"\")","",$this->MSPgothic,12);
			$excelObj->SetBackColor($this->yellow);
			$excelObj->SetValueJP2("AA$RowNumber","時間(基準値)","",$this->MSPgothic,11);
        }
		// 38行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,15);
		// 39行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber",$duty_yyyy,"BOLD",$this->MSPgothic,14);	//年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("D$RowNumber","年","",$this->MSPgothic,14);
        $excelObj->SetValueJP2("F$RowNumber","※今月の稼働日数","BOLD",$this->MSPgothic,12);
		$excelObj->SetValueJP2("N$RowNumber",$day_cnt,"BOLD",$this->MSPgothic,12);$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("O$RowNumber","日","",$this->MSPgothic,11);
		// 40 LINE
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C$RowNumber",$duty_mm,"BOLD",$this->MSPgothic,14);	//月
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValueJP2("D$RowNumber","月","",$this->MSPgothic,14);
		$excelObj->SetValueJP2("F$RowNumber","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic,12);
		$excelObj->SetValueJP2("N$RowNumber",$labor_cnt,"BOLD",$this->MSPgothic,12);	$excelObj->SetBorder("MEDIUM","outline"); //常勤職員の週所定労働時間
		$excelObj->SetValueJP2("O$RowNumber","時間","",$this->MSPgothic,11);

		// 縦位置
		$excelObj->SetArea("A1:BA40");
		$excelObj->SetPosV("CENTER");
		
        if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
			$excelObj->SetArea($wk_kaigyoarea);
			$excelObj->SetPosV("BOTTOM");
        }

	}
	/////////////////////////////////////////////////////
	// 療養病棟 入院基本料に係る届出書添付書類(様式９) //
	//		2012年対応			   //
	/////////////////////////////////////////////////////
	function ShowData_1(
			$excelObj,		// PHP-Excelオブジェクト
			$obj,			// 共通クラス
//			$excel_flg,		//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
//			$recomputation_flg,	//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
//			$data_array,		//看護師／准看護師情報
//			$data_sub_array,	//看護補助者情報
//			$hospital_name,		//保険医療機関名
//			$ward_cnt,		//病棟数
//			$sickbed_cnt,		//病床数
			$report_kbn,		//届出区分
			$report_kbn_name,	//届出区分名
			$report_patient_cnt,	//届出時入院患者数
			$hosp_patient_cnt,	//１日平均入院患者数
			$compute_start_yyyy,	//１日平均入院患者数 算出期間（開始年）
			$compute_start_mm,	//１日平均入院患者数 算出期間（開始月）
			$compute_end_yyyy,	//１日平均入院患者数 算出期間（終了年）
			$compute_end_mm,	//１日平均入院患者数 算出期間（終了月）
			$nurse_sumtime,		//勤務時間数（看護師）
			$sub_nurse_sumtime,	//勤務時間数（准看護師）
			$assistance_nurse_sumtime, //勤務時間数（看護補助者）
			$nurse_cnt,		//看護師数
			$sub_nurse_cnt,		//准看護師数
			$assistance_nurse_cnt,	//看護補助者数
			$hospitalization_cnt,	//平均在院日数
			$ave_start_yyyy,	//平均在院日数 算出期間（開始年）
			$ave_start_mm,		//平均在院日数 算出期間（開始月）
			$ave_end_yyyy,		//平均在院日数 算出期間（終了年）
			$ave_end_mm,		//平均在院日数 算出期間（終了月）
			$prov_start_hh,		//夜勤時間帯 （開始時）
			$prov_start_mm,		//夜勤時間帯 （開始分）
			$prov_end_hh,		//夜勤時間帯 （終了時）
			$prov_end_mm,		//夜勤時間帯 （終了分）
//			$duty_yyyy,		//対象年
			$duty_mm,		//対象月
			$day_cnt,		//日数
//			$date_y1,		//カレンダー用年
//			$date_m1,		//カレンダー用月
//			$date_d1,		//カレンダー用日
			$labor_cnt,		//常勤職員の週所定労働時間
			$create_yyyy,		//作成年
			$create_mm,		//作成月
			$create_dd,		//作成日
			$nurse_staffing_flg,	//看護配置加算の有無
			$acute_nursing_flg,	//急性期看護補助加算の有無
			$acute_nursing_cnt,	//急性期看護補助加算の配置比率
			$nursing_assistance_flg,//看護補助加算の有無
			$nursing_assistance_cnt, //看護補助加算の配置比率
			$arg_array,		// 行位置情報
			$ReportType		// 1:療養病棟１、2:療養病棟２
		){
		$KangoRowDataTop= $arg_array["KangoRowDataTop"];
		$KangoRowSum	= $arg_array["KangoRowSum"];
		$KangoRowNext	= $KangoRowSum + 1;
		$KMidSumRow1	= $arg_array["KMidSumRow1"];
		$KMidSumRow2	= $arg_array["KMidSumRow2"];
		$KMidNext1	= $KMidSumRow1 + 1;
		$KMidNext2	= $KMidSumRow2 + 1;
		$HojoRowDataTop	= $arg_array["HojoRowDataTop"];
		$HojoRowSum	= $arg_array["HojoRowSum"];
		$HojoRowNext	= $HojoRowSum + 1;
		$HMidSumRow1	= $arg_array["HMidSumRow1"];
		$HMidSumRow2	= $arg_array["HMidSumRow2"];
		$HMidNext1m	= $HMidSumRow1 + 1;
		$HMidNext1b	= $HMidSumRow1 + 2;
		$HMidNext2	= $HMidSumRow2 + 1;

/* debug 看護協会の雛形での行位置
		$KangoRowDataTop= 35;
		$KangoRowSum	= 237;
		$KangoRowNext	= 238;
		$KMidSumRow1	= 240;
		$KMidNext1	= 241;
		$KMidSumRow2	= 242;
		$KMidNext2	= 242;
		$HojoRowSum	= 452;
		$HojoRowNext	= 453;
		$HMidSumRow1	= 456;
		$HMidNext1m	= 457;
		$HMidNext1b	= 458;
		$HMidSumRow2	= 459;
		$HMidNext2	= 460;
// debug end.
*/
		if ( $ReportType == 1 ){
			$calcUnit = 20;
		}else{
			$calcUnit = 25;
		}

		// 縦位置(VCENTER)はあとでまとめて指定
		// 5行目
		$RowNumber=5;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E5","届出区分","BOLD",$this->MSPgothic ,14);
		$excelObj->CellMerge2("F".$RowNumber.":M".$RowNumber,"medium","outline","CC99FF");

		$excelObj->SetValueJP2("F5", $report_kbn_name ,"BOLD",$this->MSPgothic ,12);
		$excelObj->SetValueJP2("N5", "届出時入院患者数" ,"",$this->MSPgothic ,11);

		$excelObj->CellMerge("Q".$RowNumber.":S".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValueJPwith("Q5", $report_patient_cnt ,"BOLD");

		// 6行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

		// 7行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,19.5);

		if ( $ReportType == 1 ){
			$wk = "看護配置20対1以上・看護補助配置20対1以上　　看護職員中看護師比率20％以上　";
		}else{
			$wk = "看護配置25対1以上・看護補助配置25対1以上　　看護職員中看護師比率20％以上　看護要員(看護職員・看護補助者)の月平均夜勤時間数72時間以内";
		}
		$excelObj->CellMerge2("E".$RowNumber.":AC".$RowNumber,"medium","outline","FFFF99");
		$excelObj->SetValueJP2("E7", $wk ,"BOLD VPOS:CENTER",$this->MSPgothic ,12);

		// 8行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,17.25);

		// 9行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,9);

		// 10行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E10", "○１日平均入院患者数〔A〕" ,"BOLD",$this->MSPgothic ,14);

		$excelObj->CellMerge("J".$RowNumber.":L".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValuewith("J10", $hosp_patient_cnt ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetValueJP2("M10", "人" ,"BOLD",$this->MSPgothic ,14);
		$excelObj->SetValueJPwith("N10", "(算出期間）" ,"");

		$excelObj->SetValueJPwith("Q10","年","");
		$excelObj->SetValueJPwith("R10","月〜","");
		$excelObj->SetValueJPwith("U10","年","");
		$excelObj->SetValueJPwith("W10","月","");

		//１日平均入院患者数 算出期間
		$excelObj->SetValuewith("P10",$compute_start_yyyy,"");	// 開始年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("R10",$compute_start_mm,"");	// 開始月
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("T10",$compute_end_yyyy,"");	// 終了年
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetValuewith("V10",$compute_end_mm,"");	// 終了月
		$excelObj->SetBorder("MEDIUM","outline");

		$CellArea = "N".$RowNumber.":W".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);

		// 11行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("E11", "※端数切上げ整数入力" ,"",$this->MSPgothic ,11);

		// 12行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C12", "�〃酳振傳影�当たり看護配置数" ,"BOLD",$this->MSPgothic ,14);
		$excelObj->CellMerge2("J".$RowNumber.":L".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =ROUNDDOWN(W242,1)
		$formulae="=ROUNDDOWN(S$KMidSumRow2,1)";
		$excelObj->SetValuewith("J12", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetValueJP2("M12", "人(実績値）" ,"BOLD",$this->MSPgothic ,14);

		$excelObj->SetValueJP2("X12", "『月延べ勤務時間数』" ,"BOLD RIGHT",$this->MSPgothic ,12);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =R240
		$formulae="=X$KMidSumRow1";
		$excelObj->SetValuewith("Y12", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA12", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

		// 13行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		//配置比率表示不具合対応 20->$calcUnit 20141111
		$excelObj->SetValueJP2("C13", "1日看護配置数　　〔(A ／配置比率{$calcUnit})×３〕" ,"",$this->MSPgothic ,11);
		$excelObj->CellMerge2("K".$RowNumber.":L".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色
		//         =IF(ISNUMBER(J10), ROUNDUP(((J10/20)*3),0), "")
		$formulae='=IF(ISNUMBER(J10), ROUNDUP(((J10/'.$calcUnit.')*3),0), "")';
		$excelObj->SetValuewith("K13", $formulae ,"BOLD");

		$excelObj->SetValueJP2("X13", "※「1日平均看護配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色
		//	   =IF(AND(ISNUMBER(K13), ISNUMBER(N28)),K13*8*N28,"")
		$formulae='=IF(AND(ISNUMBER(K13), ISNUMBER(N28)),K13*8*N28,"")';
		$excelObj->SetValuewith("Y13", $formulae ,"BOLD");
		$excelObj->SetValueJP2("AA13", "時間(基準値)" ,"",$this->MSPgothic ,11);

		// 14行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,15);

		// 15行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		$excelObj->SetValueJP2("C15", "��-2 月平均１日当たり看護補助配置数" ,"BOLD",$this->MSPgothic ,14);
		$excelObj->CellMerge2("K".$RowNumber.":L".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =ROUNDDOWN(Y459,1)
		$formulae="=ROUNDDOWN(Y$HMidSumRow2,1)";
		$excelObj->SetValuewith("K15", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("M15", "人(実績値）" ,"BOLD",$this->MSPgothic ,12);

		$excelObj->SetValueJP2("X15", "『月延べ勤務時間数』" ,"BOLD RIGHT",$this->MSPgothic ,12);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		// =Y456
		$formulae="=Y$HMidSumRow1";
		$excelObj->SetValuewith("Y15", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA15", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

		// 16行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber,18.75);
		//配置比率表示不具合対応 20->$calcUnit 20141111
		$excelObj->SetValueJP2("C16", "1日看護補助配置数　　〔(A ／配置比率{$calcUnit})×３〕" ,"",$this->MSPgothic ,11);

		$excelObj->CellMerge2("K".$RowNumber.":L".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色
		//	   =IF(ISNUMBER(J10), ROUNDUP(((J10/20)*3),0),"")
		$formulae='=IF(ISNUMBER(J10), ROUNDUP(((J10/'.$calcUnit.')*3),0),"")';
		$excelObj->SetValuewith("K16", $formulae ,"BOLD");
		$excelObj->SetValueJP2("M16", "人" ,"",$this->MSPgothic ,12);

		$excelObj->SetValueJP2("X16", "※「1日平均看護補助配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","FFFF99"); // 薄い黄色
		//	   =IF(AND(ISNUMBER(K16), ISNUMBER(N28)), K16*8*N28, "")
		$formulae='=IF(AND(ISNUMBER(K16), ISNUMBER(N28)), K16*8*N28, "")';
		$excelObj->SetValuewith("Y16", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA16", "時間(基準値)" ,"",$this->MSPgothic ,11);

		// 17行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 15);

		// 18行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C18", "��看護職員中の看護師の比率" ,"BOLD",$this->MSPgothic ,14);
		$excelObj->CellMerge2("J".$RowNumber.":L".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		//	   =ROUNDDOWN(IF((Y18/Y13)*100>100,100,(Y18/Y13)*100),1)
		$formulae="=ROUNDDOWN(IF((Y18/Y13)*100>100,100,(Y18/Y13)*100),1)";
		$excelObj->SetValuewith("J18", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetFormatStyle("0.00");

		$excelObj->SetValueJP2("N$RowNumber", "看護要員の内訳" ,"",$this->MSPgothic ,11);

		$excelObj->SetValueJP2("Q$RowNumber", "看護師" ,"",$this->MSPgothic ,11);
		//	   =ROUNDDOWN(AX238,2)
		$formulae="=ROUNDDOWN(AX$KangoRowNext,2)";
		$excelObj->SetValuewith("S$RowNumber", $formulae ,"");
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetValueJP2("T$RowNumber", "人" ,"",$this->MSPgothic ,11);

		$excelObj->SetValueJP2("U$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		//	   =AV237+AV238
		$formulae="=AV$KangoRowSum+AV$KangoRowNext";
		$excelObj->SetValuewith("Y$RowNumber", $formulae ,""); // OK
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA$RowNumber", "時間" ,"",$this->MSPgothic ,11);

		// 19行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);
		$excelObj->SetValueJP2("Q$RowNumber", "准看護師" ,"",$this->MSPgothic ,11);
		//	   =ROUNDDOWN(AY238,2)
		$formulae="=ROUNDDOWN(AY$KangoRowNext,2)";
		$excelObj->SetValuewith("S$RowNumber", $formulae ,"FITSIZE");
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetValueJP2("T$RowNumber", "人" ,"",$this->MSPgothic ,11);

		$excelObj->SetValueJP2("U$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		//	   =AW237+AW238
		$formulae="=AW$KangoRowSum+AW$KangoRowNext";
		$excelObj->SetValuewith("Y$RowNumber", $formulae ,"FITSIZE");
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA$RowNumber", "時間" ,"",$this->MSPgothic ,11);

		// 20行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);
		$excelObj->SetValueJP2("Q$RowNumber", "看護補助者" ,"HCENTER",$this->MSPgothic ,11);
		//	   =ROUNDDOWN(AV453,2)
		$formulae="=ROUNDDOWN(AV$HojoRowNext,2)";
		$excelObj->SetValuewith("S$RowNumber", $formulae ,"FITSIZE");
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetBackColor("CCFFCC"); // 薄い緑
		$excelObj->SetValueJP2("T$RowNumber", "人" ,"",$this->MSPgothic ,11);

		$excelObj->SetValueJP2("U$RowNumber", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
		$excelObj->CellMerge2("Y".$RowNumber.":Z".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		//	   =AT452+AT453
		$formulae="=AT$HojoRowSum+AT$HojoRowNext";
		$excelObj->SetValuewith("Y$RowNumber", $formulae ,"FITSIZE");
		$excelObj->SetFont($this->MSPgothic ,11);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetValueJP2("AA$RowNumber", "時間" ,"",$this->MSPgothic ,11);

		// 21行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 11.25);

		// 22行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C$RowNumber", "�Ｊ振兀澑‘�数 ※端数切上げ整数入力" ,"BOLD",$this->MSPgothic ,14);

		$excelObj->CellMerge("J".$RowNumber.":L".$RowNumber);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValueJP2("M$RowNumber", "日" ,"BOLD",$this->MSPgothic ,14);

		$excelObj->SetValueJPwith("N$RowNumber", "(算出期間）" ,"");

		$excelObj->SetValueJPwith("Q$RowNumber","年","");
		$excelObj->SetValueJPwith("S$RowNumber","月〜","");
		$excelObj->SetValueJPwith("U$RowNumber","年","");
		$excelObj->SetValueJPwith("W$RowNumber","月","");

		//１日平均入院患者数 算出期間
		$excelObj->SetArea("P$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("R$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("T$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetArea("V$RowNumber");
		$excelObj->SetBorder("MEDIUM","outline");

		$CellArea = "N".$RowNumber.":W".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetFont($this->MSPgothic ,11);

		$CellArea = "C".$RowNumber.":W".$RowNumber;
		$excelObj->SetArea($CellArea);
		$excelObj->SetMask('darkGrid'); // 網掛け

		// 23行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 17.25);

		// 24行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 17.25);

		$excelObj->SetValueJP2("C$RowNumber", "�ぬ覿仍�間帯(16時間)" ,"BOLD",$this->MSPgothic ,14);

		$wk = " ".$prov_start_hh."時 ".$prov_start_mm."分　";
		$excelObj->SetValueJP2("I$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

		$excelObj->SetValueJP2("M$RowNumber", "〜", "BOLD",$this->MSPgothic ,14);

		$wk = " ".intval($prov_end_hh, 10)."時 ".$prov_end_mm."分　";
		$excelObj->SetValueJP2("N$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

		// 25行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 13.50);

		// 26行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);
		$excelObj->SetValueJP2("C$RowNumber", "�ゴ埜醉廾�の月平均夜勤時間数〔(D-E)／B〕" ,"BOLD",$this->MSPgothic ,14);

		$excelObj->CellMerge2("L".$RowNumber.":N".$RowNumber,"medium","outline","CCFFCC"); // 薄い緑
		//	   =ROUNDDOWN((O241+Y458-AU453)/(O240+M453),1)
		$formulae="=ROUNDDOWN((O$KMidNext1+Y$HMidNext1b-AU$HojoRowNext)/(O$KMidSumRow1+M$HojoRowNext),1)";
		$excelObj->SetValuewith("L$RowNumber", $formulae ,"BOLD");
		$excelObj->SetFont($this->MSPgothic ,12);
		$excelObj->SetValueJP2("P$RowNumber", "※看護職員・看護補助者の月平均夜勤時間数" ,"",$this->MSPgothic ,11);

 		if ( $ReportType == 1 ){
			//	   =IF(ISNUMBER(L26), IF(L26>72, "72時間を超過していますので、看護職員の労務管理に留意しましょう。", ""), "")
			$formulae='=IF(ISNUMBER(L26), IF(L26>72, "72時間を超過していますので、看護職員の労務管理に留意しましょう。", ""), "")';
		}else{
			//	   =IF(ISNUMBER(L26), IF(L26>79.2, "特別入院基本料の届出が必要となります。", IF(L26>72, "72時間を超過していますので、翌月には改善するようにしましょう。", "")), "")
			$formulae='=IF(ISNUMBER(L26), IF(L26>79.2, "特別入院基本料の届出が必要となります。", IF(L26>72, "72時間を超過していますので、翌月には改善するようにしましょう。", "")), "")';
		}
		$excelObj->SetValueJP2("Y$RowNumber", $formulae ,"BOLD",$this->MSPgothic ,14);

		// 27行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 13.50);

		// 28行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("F$RowNumber", "※今月の稼働日数" ,"BOLD",$this->MSPgothic ,12);
		$excelObj->SetValueJP2("N$RowNumber", $day_cnt ,"BOLD",$this->MSPgothic ,12);
		$excelObj->SetBorder2("medium","","outline");
		if ( $day_cnt <= 0 || $day_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}
//		$excelObj->SetStyleConditional( 'lessThanOrEqual' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない
		$excelObj->SetValueJP2("O$RowNumber", "日" ,"",$this->MSPgothic ,11);

		// 29行目
		$RowNumber++;
		$excelObj->SetRowDim($RowNumber, 18.75);

		$excelObj->SetValueJP2("C$RowNumber", $duty_mm ,"BOLD",$this->MSPgothic ,12);
		$excelObj->SetBorder2("medium","","outline");
		$excelObj->SetValueJP2("D$RowNumber","月","BOLD",$this->MSPgothic ,14);

		$excelObj->SetValueJP2("F$RowNumber","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic ,12);
		$excelObj->SetValuewith("N$RowNumber",$labor_cnt,"BOLD");	//常勤職員の週所定労働時間
		if ( $labor_cnt <= 0 || $labor_cnt == '' ){
			$excelObj->SetBackColor("FF0000"); // rgb 赤
		}
//		$excelObj->SetStyleConditional( 'equal' , '0' , 'FFFF0000' ); // 2012.2.20現在、機能しない
		$excelObj->SetBorder("MEDIUM","outline");
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetFont($this->MSPgothic ,12);

		$excelObj->SetValueJP2("O$RowNumber","時間","",$this->MSPgothic ,11);

		$excelObj->SetColDim("G",3.25); // G列は一般と療養で幅が異なる

		// 縦位置
		$excelObj->SetArea("A1:BA29");
		$excelObj->SetPosV("CENTER");
	}

    /////////////////////////////////////////////////////
    // 特定入院料 届出書添付書類(様式９)               //
    /////////////////////////////////////////////////////
    function ShowData_3(
            $excelObj,      // PHP-Excelオブジェクト
            $obj,           // 共通クラス
            $report_kbn,        //届出区分
            $report_kbn_name,   //届出区分名
            $report_patient_cnt,    //届出時入院患者数
            $hosp_patient_cnt,  //１日平均入院患者数
            $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
            $compute_start_mm,  //１日平均入院患者数 算出期間（開始月）
            $compute_end_yyyy,  //１日平均入院患者数 算出期間（終了年）
            $compute_end_mm,    //１日平均入院患者数 算出期間（終了月）
            $nurse_sumtime,     //勤務時間数（看護師）
            $sub_nurse_sumtime, //勤務時間数（准看護師）
            $assistance_nurse_sumtime, //勤務時間数（看護補助者）
            $nurse_cnt,     //看護師数
            $sub_nurse_cnt,     //准看護師数
            $assistance_nurse_cnt,  //看護補助者数
            $hospitalization_cnt,   //平均在院日数
            $ave_start_yyyy,    //平均在院日数 算出期間（開始年）
            $ave_start_mm,      //平均在院日数 算出期間（開始月）
            $ave_end_yyyy,      //平均在院日数 算出期間（終了年）
            $ave_end_mm,        //平均在院日数 算出期間（終了月）
            $prov_start_hh,     //夜勤時間帯 （開始時）
            $prov_start_mm,     //夜勤時間帯 （開始分）
            $prov_end_hh,       //夜勤時間帯 （終了時）
            $prov_end_mm,       //夜勤時間帯 （終了分）
            $duty_mm,       //対象月
            $day_cnt,       //日数
            $labor_cnt,     //常勤職員の週所定労働時間
            $create_yyyy,       //作成年
            $create_mm,     //作成月
            $create_dd,     //作成日
            $nurse_staffing_flg,    //看護配置加算の有無
            $acute_nursing_flg, //急性期看護補助加算の有無
            $acute_nursing_cnt, //急性期看護補助加算の配置比率
            $nursing_assistance_flg,//看護補助加算の有無
            $nursing_assistance_cnt, //看護補助加算の配置比率
            $specific_type,    //特定入院料種類
            $nurse_staffing_cnt,    //看護配置
            $assistance_staffing_cnt,   //看護補助配置
            $arg_array     // 行位置情報
        ){
        $KangoRowDataTop= $arg_array["KangoRowDataTop"];
        $KangoRowSum    = $arg_array["KangoRowSum"];
        $KangoRowNext   = $KangoRowSum + 1;
        $KMidSumRow1    = $arg_array["KMidSumRow1"];
        $KMidSumRow2    = $arg_array["KMidSumRow2"];
        $KMidNext1  = $KMidSumRow1 + 1;
        $KMidNext2  = $KMidSumRow2 + 1;
        $HojoRowDataTop = $arg_array["HojoRowDataTop"];
        $HojoRowSum = $arg_array["HojoRowSum"];
        $HojoRowNext    = $HojoRowSum + 1;
        $HMidSumRow1    = $arg_array["HMidSumRow1"];
        $HMidSumRow2    = $arg_array["HMidSumRow2"];
        $HMidNext1m = $HMidSumRow1 + 1;
        $HMidNext1b = $HMidSumRow1 + 2;
        $HMidNext2  = $HMidSumRow2 + 1;

        // 縦位置(VCENTER)はあとでまとめて指定
        // 5行目
        $RowNumber=5;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "特定入院料種類", "BOLD", $this->MSPgothic, 14);
        $excelObj->CellMerge2("H{$RowNumber}:N{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValueJP2("H{$RowNumber}", $specific_type, "BOLD", $this->MSPgothic, 14);

        // 6行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}","看護配置","BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("F{$RowNumber}:G{$RowNumber}", "medium", "outline", "FF99CC");
        $excelObj->SetValueJP2("F{$RowNumber}", $nurse_staffing_cnt, "BOLD", $this->MSPgothic, 14);
        $excelObj->SetValueJP2("H{$RowNumber}", "対１", "BOLD", $this->MSPgothic, 14);
        $excelObj->SetValueJP2("L{$RowNumber}", "看護補助配置", "BOLD RIGHT", $this->MSPgothic, 12);
        $excelObj->SetValueJP2("M{$RowNumber}", $assistance_staffing_cnt, "BOLD", $this->MSPgothic, 12);
        $excelObj->SetArea("M{$RowNumber}");$excelObj->SetBorder("MEDIUM","outline");$excelObj->SetBackColor("FF99CC");
        $excelObj->SetValueJP2("N{$RowNumber}", "対１", "BOLD", $this->MSPgothic, 12);

        $excelObj->SetValueJP2("P{$RowNumber}", "届出時入院患者数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge("S{$RowNumber}:U{$RowNumber}");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValueJPwith("S{$RowNumber}", $report_patient_cnt, "");

        // 7行目
        $RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

        // 8行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,9);

        // 9行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "○１日平均入院患者数〔A〕" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge("J{$RowNumber}:L{$RowNumber}");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValuewith("J{$RowNumber}", $hosp_patient_cnt ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetValueJP2("M{$RowNumber}", "人" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->SetValueJPwith("N{$RowNumber}", "(算出期間）" ,"");

        $excelObj->SetValueJPwith("Q{$RowNumber}","年","");
        $excelObj->SetValueJPwith("S{$RowNumber}","月〜","");
        $excelObj->SetValueJPwith("U{$RowNumber}","年","");
        $excelObj->SetValueJPwith("W{$RowNumber}","月","");

        //１日平均入院患者数 算出期間
        $excelObj->SetValuewith("P{$RowNumber}",$compute_start_yyyy,"");  // 開始年
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("R{$RowNumber}",$compute_start_mm,"");    // 開始月
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("T{$RowNumber}",$compute_end_yyyy,"");    // 終了年
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("V{$RowNumber}",$compute_end_mm,"");  // 終了月
        $excelObj->SetBorder("MEDIUM","outline");

        $CellArea = "N{$RowNumber}:W{$RowNumber}";
        $excelObj->SetArea($CellArea);
        $excelObj->SetFont($this->MSPgothic ,11);

        // 10行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "※端数切上げ整数入力" ,"",$this->MSPgothic ,11);

        // 11行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "�〃酳振傳影�当たり看護配置数" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("J{$RowNumber}:L{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(S{$KMidSumRow2},1)";
        $excelObj->SetValuewith("J{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("M{$RowNumber}", "人(実績値）" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("X{$RowNumber}", "『月延べ勤務時間数』" ,"BOLD RIGHT",$this->MSPgothic ,12);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=X{$KMidSumRow1}";
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

        // 12行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "1日看護配置数　　〔(A ／配置比率)×３〕※端数切上げ" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("K{$RowNumber}:L{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(J9),ISNUMBER(F6)), ROUNDUP(((J9/F6)*3),0), "")';
        $excelObj->SetValuewith("K{$RowNumber}", $formulae ,"");
        $excelObj->SetValueJP2("M{$RowNumber}", "人" ,"",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("X{$RowNumber}", "※「1日平均看護配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(K12), ISNUMBER(N27)), K12*8*N27, "")';
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間(基準値)" ,"",$this->MSPgothic ,11);

        // 13行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,15);

        // 14行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "��-2 月平均１日当たり看護補助配置数" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("K{$RowNumber}:L{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(Y{$HMidSumRow2},1)";
        $excelObj->SetValuewith("K{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("M{$RowNumber}", "人(実績値）" ,"BOLD",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("X{$RowNumber}", "『月延べ勤務時間数』" ,"BOLD RIGHT",$this->MSPgothic ,12);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae = sprintf('=Y%d+Y%d', $HMidSumRow1, $HMidSumRow1+1);
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

        // 15行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "1日看護補助配置数　　〔(A ／配置比率)×３〕※端数切上げ" , "", $this->MSPgothic, 11);

        $excelObj->CellMerge2("K{$RowNumber}:L{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(J9),ISNUMBER(M6)), ROUNDUP(((J9/M6)*3),0), "")';
        $excelObj->SetValuewith("K{$RowNumber}", $formulae, "");
        $excelObj->SetValueJP2("M{$RowNumber}", "人" ,"",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("X{$RowNumber}", "※「1日平均看護補助配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(K15), ISNUMBER(N27)), K15*8*N27, "")';
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間(基準値)" ,"",$this->MSPgothic ,11);

        // 16行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 15);

        // 17行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "��看護職員中の看護師の比率" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("J{$RowNumber}:L{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(IF((Y17/Y12)*100>100,100,(Y17/Y12)*100),1)";
        $excelObj->SetValuewith("J{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("M{$RowNumber}", "％" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("N{$RowNumber}", "看護要員の内訳" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("Q{$RowNumber}", "看護師" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AX$KangoRowNext,2)";
        $excelObj->SetValuewith("S{$RowNumber}", $formulae ,"");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("T{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("U{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AV$KangoRowSum+AV$KangoRowNext";
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,""); // OK
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 18行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("Q{$RowNumber}", "准看護師" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AY$KangoRowNext,2)";
        $excelObj->SetValuewith("S{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("T{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("U{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AW$KangoRowSum+AW$KangoRowNext";
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 19行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("Q{$RowNumber}", "看護補助者" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AV$HojoRowNext,2)";
        $excelObj->SetValuewith("S{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("T{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("U{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("Y{$RowNumber}:Z{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AT$HojoRowSum+AT$HojoRowNext";
        $excelObj->SetValuewith("Y{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AA{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 20行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 11.25);

        // 21行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "�Ｊ振兀澑‘�数 ※端数切上げ整数入力" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge2("J{$RowNumber}:L{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValueJP2("J{$RowNumber}", $hospitalization_cnt, "BOLD", $this->MSPgothic, 12);
        $excelObj->SetValueJP2("M{$RowNumber}", "日" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJPwith("N{$RowNumber}", "(算出期間）" ,"");

        $excelObj->SetValueJP2("P{$RowNumber}",$ave_start_yyyy,"",$this->MSPgothic,11);	// 開始年
        $excelObj->SetValueJPwith("Q{$RowNumber}","年","");
        $excelObj->SetValueJP2("R{$RowNumber}",$ave_start_mm,"",$this->MSPgothic,11);	// 開始月
        $excelObj->SetValueJPwith("S{$RowNumber}","月〜","");
        $excelObj->SetValueJP2("T{$RowNumber}",$ave_end_yyyy,"",$this->MSPgothic,11);	// 終了年
        $excelObj->SetValueJPwith("U{$RowNumber}","年","");
        $excelObj->SetValueJP2("V{$RowNumber}",$ave_end_mm,"",$this->MSPgothic,11);	// 終了月
        $excelObj->SetValueJPwith("W{$RowNumber}","月","");

        //算出期間
        $excelObj->SetArea("P{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("R{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("T{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("V{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");

        $CellArea = "N{$RowNumber}:W{$RowNumber}";
        $excelObj->SetArea($CellArea);
        $excelObj->SetFont($this->MSPgothic ,11);

        $CellArea = "C{$RowNumber}:W{$RowNumber}";
        $excelObj->SetArea($CellArea);

        // 22行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 17.25);

        // 23行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 17.25);

        $excelObj->SetValueJP2("C$RowNumber", "�ぬ覿仍�間帯(16時間)" ,"BOLD",$this->MSPgothic ,14);

        $wk = " ".$prov_start_hh."時 ".$prov_start_mm."分　";
        $excelObj->SetValueJP2("I$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("M$RowNumber", "〜", "BOLD",$this->MSPgothic ,14);

        $wk = " ".intval($prov_end_hh, 10)."時 ".$prov_end_mm."分　";
        $excelObj->SetValueJP2("N$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

        // 24行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 13.50);

        // 25行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "�シ酳振冖覿仍�間数〔(D-E)／B〕" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge2("J{$RowNumber}:L{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(O{$KMidNext1}/O{$KMidSumRow1},1)";
        $excelObj->SetValuewith("J{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetValueJP2("M{$RowNumber}", "時間" , "BOLD", $this->MSPgothic, 14);

        $formulae='=IF(ISNUMBER(L26), IF(L26>72, "72時間を超過していますので、看護職員の労務管理に留意しましょう。", ""), "")';
        $excelObj->SetValueJP2("N{$RowNumber}", $formulae ,"BOLD",$this->MSPgothic ,14);

        // 26行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 13.50);

        // 27行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("F{$RowNumber}", "※今月の稼働日数" ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetValueJP2("N{$RowNumber}", $day_cnt ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetBorder2("medium","","outline");
        if ( $day_cnt <= 0 || $day_cnt == '' ){
            $excelObj->SetBackColor("FF0000"); // rgb 赤
        }
        $excelObj->SetValueJP2("O{$RowNumber}", "日" ,"",$this->MSPgothic ,11);

        // 28行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", $duty_mm ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValueJP2("D{$RowNumber}","月","BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("F{$RowNumber}","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic ,12);
        $excelObj->SetValuewith("N{$RowNumber}",$labor_cnt,"BOLD");   //常勤職員の週所定労働時間
        if ( $labor_cnt <= 0 || $labor_cnt == '' ){
            $excelObj->SetBackColor("FF0000"); // rgb 赤
        }
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetFont($this->MSPgothic ,12);

        $excelObj->SetValueJP2("O{$RowNumber}","時間","",$this->MSPgothic ,11);

        $excelObj->SetColDim("G",3.25); // G列は一般と療養で幅が異なる

        // 縦位置
        $excelObj->SetArea("A1:BA29");
        $excelObj->SetPosV("CENTER");
    }

    ///////////////////////////////////////////////////////////////
    // 特定入院料（看護職員＋看護補助者） 届出書添付書類(様式９) //
    ///////////////////////////////////////////////////////////////
    function ShowData_4(
            $excelObj,      // PHP-Excelオブジェクト
            $obj,           // 共通クラス
            $report_kbn,        //届出区分
            $report_kbn_name,   //届出区分名
            $report_patient_cnt,    //届出時入院患者数
            $hosp_patient_cnt,  //１日平均入院患者数
            $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
            $compute_start_mm,  //１日平均入院患者数 算出期間（開始月）
            $compute_end_yyyy,  //１日平均入院患者数 算出期間（終了年）
            $compute_end_mm,    //１日平均入院患者数 算出期間（終了月）
            $nurse_sumtime,     //勤務時間数（看護師）
            $sub_nurse_sumtime, //勤務時間数（准看護師）
            $assistance_nurse_sumtime, //勤務時間数（看護補助者）
            $nurse_cnt,     //看護師数
            $sub_nurse_cnt,     //准看護師数
            $assistance_nurse_cnt,  //看護補助者数
            $hospitalization_cnt,   //平均在院日数
            $ave_start_yyyy,    //平均在院日数 算出期間（開始年）
            $ave_start_mm,      //平均在院日数 算出期間（開始月）
            $ave_end_yyyy,      //平均在院日数 算出期間（終了年）
            $ave_end_mm,        //平均在院日数 算出期間（終了月）
            $prov_start_hh,     //夜勤時間帯 （開始時）
            $prov_start_mm,     //夜勤時間帯 （開始分）
            $prov_end_hh,       //夜勤時間帯 （終了時）
            $prov_end_mm,       //夜勤時間帯 （終了分）
            $duty_mm,       //対象月
            $day_cnt,       //日数
            $labor_cnt,     //常勤職員の週所定労働時間
            $create_yyyy,       //作成年
            $create_mm,     //作成月
            $create_dd,     //作成日
            $nurse_staffing_flg,    //看護配置加算の有無
            $acute_nursing_flg, //急性期看護補助加算の有無
            $acute_nursing_cnt, //急性期看護補助加算の配置比率
            $nursing_assistance_flg,//看護補助加算の有無
            $nursing_assistance_cnt, //看護補助加算の配置比率
            $specific_type,    //特定入院料種類
            $nurse_staffing_cnt,    //看護配置
            $assistance_staffing_cnt,   //看護補助配置
            $arg_array     // 行位置情報
        ){
        $KangoRowDataTop= $arg_array["KangoRowDataTop"];
        $KangoRowSum    = $arg_array["KangoRowSum"];
        $KangoRowNext   = $KangoRowSum + 1;
        $KMidSumRow1    = $arg_array["KMidSumRow1"];
        $KMidSumRow2    = $arg_array["KMidSumRow2"];
        $KMidNext1  = $KMidSumRow1 + 1;
        $KMidNext2  = $KMidSumRow2 + 1;
        $HojoRowDataTop = $arg_array["HojoRowDataTop"];
        $HojoRowSum = $arg_array["HojoRowSum"];
        $HojoRowNext    = $HojoRowSum + 1;
        $HMidSumRow1    = $arg_array["HMidSumRow1"];
        $HMidSumRow2    = $arg_array["HMidSumRow2"];
        $HMidNext1m = $HMidSumRow1 + 1;
        $HMidNext1b = $HMidSumRow1 + 2;
        $HMidNext2  = $HMidSumRow2 + 1;

        // 縦位置(VCENTER)はあとでまとめて指定
        // 5行目
        $RowNumber=5;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "特定入院料種類", "BOLD", $this->MSPgothic, 14);
        $excelObj->CellMerge2("H{$RowNumber}:N{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValueJP2("H{$RowNumber}", $specific_type, "BOLD", $this->MSPgothic, 14);

        // 6行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}","看護配置・看護補助配置","RIGHT BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("F{$RowNumber}:G{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValueJP2("F{$RowNumber}", $nurse_staffing_cnt, "BOLD", $this->MSPgothic, 14);
        $excelObj->SetValueJP2("H{$RowNumber}", "対１", "BOLD", $this->MSPgothic, 14);

        $excelObj->SetValueJP2("P{$RowNumber}", "届出時入院患者数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge("S{$RowNumber}:U{$RowNumber}");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValueJPwith("S{$RowNumber}", $report_patient_cnt, "");

        // 7行目
        $RowNumber++;
		$excelObj->SetRowDim($RowNumber,13.5);

        // 8行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,9);

        // 9行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "○１日平均入院患者数〔A〕" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge("J{$RowNumber}:L{$RowNumber}");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValuewith("J{$RowNumber}", $hosp_patient_cnt ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetValueJP2("M{$RowNumber}", "人" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->SetValueJPwith("P{$RowNumber}", "(算出期間）" ,"");

        $excelObj->SetValueJPwith("S{$RowNumber}","年","");
        $excelObj->SetValueJPwith("U{$RowNumber}","月〜","");
        $excelObj->SetValueJPwith("W{$RowNumber}","年","");
        $excelObj->SetValueJPwith("Y{$RowNumber}","月","");

        //１日平均入院患者数 算出期間
        $excelObj->SetValuewith("R{$RowNumber}",$compute_start_yyyy,"");  // 開始年
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("T{$RowNumber}",$compute_start_mm,"");    // 開始月
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("V{$RowNumber}",$compute_end_yyyy,"");    // 終了年
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetValuewith("X{$RowNumber}",$compute_end_mm,"");  // 終了月
        $excelObj->SetBorder("MEDIUM","outline");

        $CellArea = "P{$RowNumber}:Y{$RowNumber}";
        $excelObj->SetArea($CellArea);
        $excelObj->SetFont($this->MSPgothic ,11);

        // 10行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("E{$RowNumber}", "※端数切上げ整数入力" ,"",$this->MSPgothic ,11);

        // 11行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "��-1 月平均１日当たり看護配置数（看護職員+看護補助者）" ,"BOLD",$this->MSPgothic ,12);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(AA11/(N30*8),1)";
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("N{$RowNumber}", "人(実績値）" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("Z{$RowNumber}", "『月延べ勤務時間数』(看護職員＋看護補助者）" ,"BOLD RIGHT",$this->MSPgothic ,12);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=X{$KMidSumRow1}+Y{$HMidSumRow1}";
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

        // 12行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "1日看護配置数　　〔(A ／配置比率)×３〕※端数切上げ" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(J9),ISNUMBER(F6)), ROUNDUP(((J9/F6)*3),0), "")';
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"");
        $excelObj->SetValueJP2("N{$RowNumber}", "人(基準値)" ,"",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("Z{$RowNumber}", "※「1日平均看護配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(L12),ISNUMBER(N30)), L12*8*N30,"")';
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間(基準値)" ,"",$this->MSPgothic ,11);

        // 13行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,15);

        // 14行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,15);

        // 15行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "��-2 月平均１日当たり看護配置数（看護職員）" ,"BOLD",$this->MSPgothic ,12);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(AA15/(N30*8),1)";
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("N{$RowNumber}", "人(実績値）" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("Z{$RowNumber}", "『月延べ勤務時間数』(看護職員）" ,"BOLD RIGHT",$this->MSPgothic ,12);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae = "=X{$KMidSumRow1}";
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間(実績値)" ,"BOLD",$this->MSPgothic ,12);

        // 16行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber,18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "看護職員の必要最小な1日看護配置数※端数切上げ" , "", $this->MSPgothic, 11);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(L12), ISNUMBER(L19)),ROUNDUP(L12*L19/100,0),"")';
        $excelObj->SetValuewith("L{$RowNumber}", $formulae, "");
        $excelObj->SetValueJP2("N{$RowNumber}", "人(基準値)" ,"",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("Z{$RowNumber}", "※「1日平均看護配置数」を満たす「月延べ勤務時間数」" ,"RIGHT",$this->MSPgothic ,11);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "FFFF99"); // 薄い黄色
        $formulae='=IF(AND(ISNUMBER(L16), ISNUMBER(N30)), L16*8*N30,"")';
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間(基準値)" ,"",$this->MSPgothic ,11);

        // 17行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 15);

        // 18行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "��-1看護要員中の看護職員の比率" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(IF((AA15/AA12)*100>100,100,(AA15/AA12)*100),1)";
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("N{$RowNumber}", "％" ,"BOLD",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("P{$RowNumber}", "看護要員の内訳" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("S{$RowNumber}", "看護師" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AX{$KangoRowNext}, 2)";
        $excelObj->SetValuewith("U{$RowNumber}", $formulae ,"");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("V{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("W{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AV$KangoRowSum+AV$KangoRowNext";
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,""); // OK
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 19行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "※看護要員中に必要な看護職員の比率" , "", $this->MSPgothic, 11);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValuewith("L{$RowNumber}", "50", "BOLD");
        $excelObj->SetValueJP2("N{$RowNumber}", "％(基準値)" ,"BOLD",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("S{$RowNumber}", "准看護師" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AY$KangoRowNext,2)";
        $excelObj->SetValuewith("U{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("V{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("W{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AW$KangoRowSum+AW$KangoRowNext";
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 20行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("S{$RowNumber}", "看護補助者" ,"",$this->MSPgothic ,11);
        $formulae="=ROUNDDOWN(AV$HojoRowNext,2)";
        $excelObj->SetValuewith("U{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetBackColor("CCFFCC"); // 薄い緑
        $excelObj->SetValueJP2("V{$RowNumber}", "人" ,"",$this->MSPgothic ,11);

        $excelObj->SetValueJP2("W{$RowNumber}", "月間総勤務時間数" ,"",$this->MSPgothic ,11);
        $excelObj->CellMerge2("AA{$RowNumber}:AB{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=AT$HojoRowSum+AT$HojoRowNext";
        $excelObj->SetValuewith("AA{$RowNumber}", $formulae ,"FITSIZE");
        $excelObj->SetFont($this->MSPgothic ,11);
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetValueJP2("AC{$RowNumber}", "時間" ,"",$this->MSPgothic ,11);

        // 21行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "��-2看護職員中の看護師の比率" ,"BOLD",$this->MSPgothic ,14);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(IF((AA18/AA16)*100>100,100,(AA18/AA16)*100),1)";
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetFont($this->MSPgothic ,12);
        $excelObj->SetFormatStyle("0.0");
        $excelObj->SetValueJP2("N{$RowNumber}", "％" ,"BOLD",$this->MSPgothic ,12);

        // 22行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "※看護職員中に必要な看護師の比率" , "", $this->MSPgothic, 11);
        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValuewith("L{$RowNumber}", "20", "BOLD");
        $excelObj->SetValueJP2("N{$RowNumber}", "％(基準値)" ,"BOLD",$this->MSPgothic ,12);

        // 23行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 11.25);

        // 24行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", "�Ｊ振兀澑‘�数 ※端数切上げ整数入力" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "");
        $excelObj->SetValueJP2("L{$RowNumber}", $hospitalization_cnt, "BOLD", $this->MSPgothic, 12);
        $excelObj->SetValueJP2("N{$RowNumber}", "日" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJPwith("P{$RowNumber}", "(算出期間）" ,"");

        $excelObj->SetValueJP2("R{$RowNumber}",$ave_start_yyyy,"",$this->MSPgothic,11);	// 開始年
        $excelObj->SetValueJPwith("S{$RowNumber}","年","");
        $excelObj->SetValueJP2("T{$RowNumber}",$ave_start_mm,"",$this->MSPgothic,11);	// 開始月
        $excelObj->SetValueJPwith("U{$RowNumber}","月〜","");
        $excelObj->SetValueJP2("V{$RowNumber}",$ave_end_yyyy,"",$this->MSPgothic,11);	// 終了年
        $excelObj->SetValueJPwith("W{$RowNumber}","年","");
        $excelObj->SetValueJP2("X{$RowNumber}",$ave_end_mm,"",$this->MSPgothic,11);	// 終了月
        $excelObj->SetValueJPwith("Y{$RowNumber}","月","");

        //算出期間
        $excelObj->SetArea("R{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("T{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("V{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetArea("X{$RowNumber}");
        $excelObj->SetBorder("MEDIUM","outline");

        $CellArea = "N{$RowNumber}:W{$RowNumber}";
        $excelObj->SetArea($CellArea);
        $excelObj->SetFont($this->MSPgothic ,11);

        $CellArea = "C{$RowNumber}:W{$RowNumber}";
        $excelObj->SetArea($CellArea);

        // 25行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 17.25);

        // 26行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 17.25);

        $excelObj->SetValueJP2("C$RowNumber", "�ぬ覿仍�間帯(16時間)" ,"BOLD",$this->MSPgothic ,14);

        $wk = " ".$prov_start_hh."時 ".$prov_start_mm."分　";
        $excelObj->SetValueJP2("I$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

        $excelObj->SetValueJP2("M$RowNumber", "〜", "BOLD",$this->MSPgothic ,14);

        $wk = " ".intval($prov_end_hh, 10)."時 ".$prov_end_mm."分　";
        $excelObj->SetValueJP2("N$RowNumber", $wk, "UNDER BOLD",$this->MSPgothic ,12);

        // 27行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 13.50);

        // 28行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);
        $excelObj->SetValueJP2("C{$RowNumber}", "�シ酳振冖覿仍�間数〔(D-E)／B〕(看護職員)" ,"BOLD",$this->MSPgothic ,14);

        $excelObj->CellMerge2("L{$RowNumber}:M{$RowNumber}", "medium", "outline", "CCFFCC"); // 薄い緑
        $formulae="=ROUNDDOWN(O{$KMidNext1}/O{$KMidSumRow1},1)";
        $excelObj->SetValuewith("L{$RowNumber}", $formulae ,"BOLD");
        $excelObj->SetValueJP2("N{$RowNumber}", "時間" , "BOLD", $this->MSPgothic, 14);

        $formulae='=IF(ISNUMBER(L26), IF(L26>72, "72時間を超過していますので、看護職員の労務管理に留意しましょう。", ""), "")';
        $excelObj->SetValueJP2("Q{$RowNumber}", $formulae ,"BOLD",$this->MSPgothic ,14);

        // 29行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 13.50);

        // 30行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("F{$RowNumber}", "※今月の稼働日数" ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetValueJP2("N{$RowNumber}", $day_cnt ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetBorder2("medium","","outline");
        if ( $day_cnt <= 0 || $day_cnt == '' ){
            $excelObj->SetBackColor("FF0000"); // rgb 赤
        }
        $excelObj->SetValueJP2("O{$RowNumber}", "日" ,"",$this->MSPgothic ,11);

        // 31行目
        $RowNumber++;
        $excelObj->SetRowDim($RowNumber, 18.75);

        $excelObj->SetValueJP2("C{$RowNumber}", $duty_mm ,"BOLD",$this->MSPgothic ,12);
        $excelObj->SetBorder2("medium","","outline");
        $excelObj->SetValueJP2("D{$RowNumber}","月","BOLD",$this->MSPgothic ,14);

        $excelObj->SetValueJP2("F{$RowNumber}","※常勤職員の週所定労働時間","BOLD",$this->MSPgothic ,12);
        $excelObj->SetValuewith("N{$RowNumber}",$labor_cnt,"BOLD");   //常勤職員の週所定労働時間
        if ( $labor_cnt <= 0 || $labor_cnt == '' ){
            $excelObj->SetBackColor("FF0000"); // rgb 赤
        }
        $excelObj->SetBorder("MEDIUM","outline");
        $excelObj->SetFormatStyle("0.00");
        $excelObj->SetFont($this->MSPgothic ,12);

        $excelObj->SetValueJP2("O{$RowNumber}","時間","",$this->MSPgothic ,11);

        $excelObj->SetColDim("G",3.25); // G列は一般と療養で幅が異なる

        // 縦位置
        $excelObj->SetArea("A1:BA29");
        $excelObj->SetPosV("CENTER");
    }

	//////////////////////////////////
	//				//
	//   療養病棟・看護職員集計	//
	//	2012年対応		//
	//				//
	//////////////////////////////////
	function ShowNurseSum11($excelObj, $arg_array, $ReportType = 1){

		$RowNumber = $arg_array["RowNumber"];
		$KangoRowSum = $arg_array["KangoRowSum"];

		$KEI_RowU = $KangoRowSum;
		$KEI_RowL = $KangoRowSum+1;

/*
[看護職員の勤務実績(勤務時間数)	][夜勤従事者数(夜勤ありの職員数)〔Ｂ〕][計算式][月延べ勤務時間数の計〔Ｃ〕][計算式] <-------- $KMidSumRow1
				 [月延べ夜勤時間数　〔D−E〕	      ][計算式][月延べ夜勤時間数の計〔Ｄ〕][計算式][夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕][計算式] <---$KMidNext1
[1日当たり看護配置数]            [月平均１日当たり看護配置数〔C／(日数×８）〕	][計算式] <-------- $KMidSumRow2
				 [1日看護配置数　〔(A ／届出区分の数)×３〕	][計算式] <-------- $KMidNext2
*/

		// 中間集計１行目
		$RowNumber += 3;
		$KMidSumRow1 = $RowNumber;
		$KMidSumRow2 = $KMidSumRow1 + 2;
		$KMidNext1   = $KMidSumRow1 + 1;
		$KMidNext2   = $KMidSumRow2 + 1;

		// 1 LINE
		$excelObj->SetRowDim($KMidSumRow1,26.25);
		$excelObj->CellMerge("C".$KMidSumRow1.":G".$KMidNext1);
		$excelObj->SetValueJP2("C$KMidSumRow1","看護職員の勤務実績(勤務時間数)","BOLD HCENTER VCENTER",$this->MSPgothic,12);

		$excelObj->CellMerge("H".$KMidSumRow1.":N".$KMidSumRow1);
		$excelObj->SetValueJP2("H$KMidSumRow1","夜勤従事者数(夜勤ありの職員数)〔Ｂ〕","BOLD LEFT VCENTER FITSIZE",$this->MSPgothic,14);

		$excelObj->CellMerge("O".$KMidSumRow1.":Q".$KMidSumRow1);
		$excelObj->SetValueJP2("O$KMidSumRow1","=ROUNDDOWN(M$KEI_RowL,1)","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("R".$KMidSumRow1.":W".$KMidSumRow1);
		$excelObj->SetValueJP2("R$KMidSumRow1","月延べ勤務時間数の計〔Ｃ〕","BOLD VCENTER",$this->MSPgothic,14);

		$excelObj->CellMerge("X".$KMidSumRow1.":Y".$KMidSumRow1);
		$excelObj->SetValueJP2("X$KMidSumRow1","=AT$KEI_RowU+AT$KEI_RowL","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->SetBackColor($this->green);

		// 2 LINE
		$excelObj->SetRowDim($KMidNext1,26.25);
		$excelObj->CellMerge("H".$KMidNext1.":N".$KMidNext1);
		$excelObj->SetValueJP2("H$KMidNext1","月延べ夜勤時間数　〔D−E〕","LEFT VCENTER BOLD",$this->MSPgothic,14);

		$excelObj->CellMerge("O".$KMidNext1.":Q".$KMidNext1);
		$excelObj->SetValueJP2("O$KMidNext1","=X$KMidNext1-AK$KMidNext1","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("R".$KMidNext1.":W".$KMidNext1);
		$excelObj->SetValueJP2("R$KMidNext1","月延べ夜勤時間数の計〔Ｄ〕","BOLD VCENTER",$this->MSPgothic,14);

		$excelObj->CellMerge("X".$KMidNext1.":Y".$KMidNext1);
		$excelObj->SetValueJP2("X$KMidNext1","=AT$KEI_RowL","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("Z".$KMidNext1.":AJ".$KMidNext1);
		$excelObj->SetValueJP2("Z$KMidNext1","夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕","VCENTER BOLD LEFT",$this->MSPgothic,12);

		$excelObj->CellMerge("AK".$KMidNext1.":AL".$KMidNext1);
		$excelObj->SetValueJP2("AK$KMidNext1","=AU$KEI_RowL","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->SetBackColor($this->green);

		// 3 LINE
		$excelObj->SetRowDim($KMidSumRow2,26.25);
		$excelObj->CellMerge("C".$KMidSumRow2.":G".$KMidNext2);
		$excelObj->SetValueJP2("C$KMidSumRow2","1日当たり看護配置数","HCENTER VCENTER BOLD WRAP",$this->MSPgothic,12);

		$excelObj->CellMerge("H".$KMidSumRow2.":R".$KMidSumRow2);
		$excelObj->SetValueJP2("H$KMidSumRow2","月平均１日当たり看護配置数〔C／(日数×８）〕","LEFT BOLD VCENTER FITSIZE",$this->MSPgothic,14);

		$excelObj->CellMerge("S".$KMidSumRow2.":T".$KMidSumRow2);
		if ($ReportType == 3) { // 届出区分が特定入院料
			$excelObj->SetValueJP2("S$KMidSumRow2","=ROUNDDOWN(X$KMidSumRow1/(N27*8),1)","VCENTER BOLD",$this->MSPgothic,12);
		} else {
			$excelObj->SetValueJP2("S$KMidSumRow2","=ROUNDDOWN(X$KMidSumRow1/(N28*8),1)","VCENTER BOLD",$this->MSPgothic,12);
		}
		$excelObj->SetBackColor($this->green);

		$excelObj->SetValueJP2("U$KMidSumRow2","（実績値）","VCENTER BOLD",$this->MSPgothic,11);

		// 4 LINE
		$excelObj->SetRowDim($KMidNext2,26.25);
		$excelObj->CellMerge("H".$KMidNext2.":R".$KMidNext2);
		$excelObj->SetValueJP2("H$KMidNext2","1日看護配置数　〔(A ／届出区分の数)×３〕","BOLD LEFT VCENTER",$this->MSPgothic,14);

		$excelObj->CellMerge("S".$KMidNext2.":T".$KMidNext2);
		if ($ReportType == 3) { // 届出区分が特定入院料
			$excelObj->SetValueJP2("S$KMidNext2","=K12","BOLD VCENTER",$this->MSPgothic,12);
		} else {
			$excelObj->SetValueJP2("S$KMidNext2","=K13","BOLD VCENTER",$this->MSPgothic,12);
		}
		$excelObj->SetBackColor($this->yellow);

		$excelObj->SetValueJP2("U$KMidNext2","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$cellarea = "C".$KMidSumRow1.":Y".$KMidSumRow1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","top");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H".$KMidSumRow1.":Y".$KMidSumRow1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$cellarea = "C".$KMidNext1.":AL".$KMidNext1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "Z".$KMidNext1.":AL".$KMidNext1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","top");

		$excelObj->SetArea("AJ".$KMidNext1);
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H".$KMidSumRow1.":N".$KMidNext1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "R".$KMidSumRow1.":W".$KMidNext1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "C".$KMidSumRow2.":T".$KMidNext2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H".$KMidSumRow2.":R".$KMidNext2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "H".$KMidSumRow2.":T".$KMidSumRow2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$arg_array["KMidSumRow1"] = $KMidSumRow1;
		$arg_array["KMidSumRow2"] = $KMidSumRow2;
		$arg_array["RowNumber"] = $RowNumber + 5 ;	// 行数4行+空行1行

		return $arg_array;
	}
	//////////////////////////////////////////
	//					//
	//	療養病棟・看護補助者集計	//
	//		2012年対応		//
	//					//
	//////////////////////////////////////////
	function ShowNurseSum12($excelObj, $arg_array, $ReportType = 1){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$HojoRowSum = $arg_array["HojoRowSum"];	// 看護補助者表の「計」行、上段
		$HojoRowSumNext = $HojoRowSum + 1;	// 同上、下段

		////////////////
		// 中間集計行 //
		////////////////
//		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$RowNumber ++;
		$HMidSumRow1 = $RowNumber;
		$HMidNext1m  = $HMidSumRow1 + 1;
		$HMidNext1b  = $HMidSumRow1 + 2;
		$HMidSumRow2 = $HMidSumRow1 + 3;
		$HMidNext2   = $HMidSumRow2 + 1;
/*

[看護補助者の勤務実績(勤務時間数)][看護補助者のみの月延べ勤務時間数の計〔F〕					   ][計算式] <--- $HMidSumRow1
				  [みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕	   ][計算式] <--- $HMidNext1m
				  [看護補助者のみの月延べ夜勤時間数の計〔Ｈ〕 看護補助者（みなしを除く）のみの〔D〕][計算式] <--- $HMidNext1b
[みなし補助者を含む		 ][月平均1日当たり看護補助者配置数(みなし看護補助者含む）　〔(F+G) ／ (日数×８) 〕][計算式] <--- $HMidSumRow2
[1日当たり看護補助者配置数	 ][	1日看護補助配置数 〔I〕　〔(A ／届出区分の数)×３〕			   ][計算式] <--- $HMidNext2

*/

		// 1行目
		$excelObj->SetRowDim($HMidSumRow1,26.25);
		$excelObj->CellMerge("C".$HMidSumRow1.":G".$HMidNext1b);
		$excelObj->SetValueJP2("C$HMidSumRow1","看護補助者の勤務実績(勤務時間数)","BOLD HCENTER VCENTER",$this->MSPgothic,12);
		$excelObj->CellMerge("H".$HMidSumRow1.":X".$HMidSumRow1);
		$excelObj->SetValueJP2("H$HMidSumRow1","看護補助者のみの月延べ勤務時間数の計〔F〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidSumRow1.":Z".$HMidSumRow1);
		$excelObj->SetValueJP2("Y$HMidSumRow1","=AT$HojoRowSum+AT$HojoRowSumNext","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);

		// 2行目
		$excelObj->SetRowDim($HMidNext1m,26.25);
		$excelObj->CellMerge("H".$HMidNext1m.":X".$HMidNext1m);
		$excelObj->SetValueJP2("H$HMidNext1m","みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidNext1m.":Z".$HMidNext1m);
		if ($ReportType == 3) { // 届出区分が特定入院料
			$excelObj->SetValueJP2("Y$HMidNext1m","=MAX(X$KMidSumRow1-Y12, 0)","BOLD VCENTER",$this->MSPgothic,12);
		} else {
			$excelObj->SetValueJP2("Y$HMidNext1m","=MAX(X$KMidSumRow1-Y13, 0)","BOLD VCENTER",$this->MSPgothic,12);
		}
		$excelObj->SetBackColor($this->green);

		// 3行目
		$excelObj->SetRowDim($HMidNext1b,26.25);
		$excelObj->CellMerge("H".$HMidNext1b.":X".$HMidNext1b);
		$excelObj->SetValueJP2("H$HMidNext1b","看護補助者のみの月延べ夜勤時間数の計〔Ｈ〕 看護補助者（みなしを除く）のみの〔D〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidNext1b.":Z".$HMidNext1b);
		$excelObj->SetValueJP2("Y$HMidNext1b","=AT$HojoRowSumNext","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);

		// 4行目
		$excelObj->SetRowDim($HMidSumRow2,26.25);
		$excelObj->CellMerge("C".$HMidSumRow2.":G".$HMidNext2);
		$excelObj->SetValueJP2("C$HMidSumRow2","みなし補助者を含む\n1日当たり看護補助者配置数","WRAP BOLD HCENTER VCENTER",$this->MSPgothic,12);
		$excelObj->CellMerge("H".$HMidSumRow2.":X".$HMidSumRow2);
		$excelObj->SetValueJP2("H$HMidSumRow2","月平均1日当たり看護補助者配置数(みなし看護補助者含む）　〔(F+G) ／ (日数×８) ","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidSumRow2.":Z".$HMidSumRow2);
		if ($ReportType == 3) { // 届出区分が特定入院料
			$excelObj->SetValueJP2("Y$HMidSumRow2","=ROUNDDOWN((Y$HMidSumRow1+Y$HMidNext1m)/(N27*8),1)","BOLD VCENTER",$this->MSPgothic,12);
		} else {
			$excelObj->SetValueJP2("Y$HMidSumRow2","=ROUNDDOWN((Y$HMidSumRow1+Y$HMidNext1m)/(N28*8),1)","BOLD VCENTER",$this->MSPgothic,12);
		}
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$HMidSumRow2","(実績値)","BOLD VCENTER",$this->MSPgothic,11);

		// 5行目
		$excelObj->SetRowDim($HMidNext2,26.25);
		$excelObj->CellMerge("H".$HMidNext2.":X".$HMidNext2);
		$excelObj->SetValueJP2("H$HMidNext2","1日看護補助配置数 〔I〕　〔(A ／届出区分の数)×３〕 ","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidNext2.":Z".$HMidNext2);
		if ($ReportType == 3) { // 届出区分が特定入院料
			$excelObj->SetValueJP2("Y$HMidNext2","=K15","BOLD VCENTER",$this->MSPgothic,12);
		} else {
			$excelObj->SetValueJP2("Y$HMidNext2","=K16","BOLD VCENTER",$this->MSPgothic,12);
		}
		$excelObj->SetBackColor($this->yellow);
		$excelObj->SetValueJP2("AA$HMidNext2","(基準値)","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$cellarea = "C".$HMidSumRow1.":Z".$HMidNext1b;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","outline");

		$cellarea = "C".$HMidSumRow2.":Z".$HMidNext2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H".$HMidSumRow1.":X".$HMidNext2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "H".$HMidSumRow1.":Z".$HMidSumRow1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");
		$cellarea = "H".$HMidNext1m.":Z".$HMidNext1m;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");
		$cellarea = "H".$HMidSumRow2.":Z".$HMidSumRow2;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$arg_array["HMidSumRow1"] = $HMidSumRow1;
		$arg_array["HMidSumRow2"] = $HMidSumRow2;
		$arg_array["RowNumber"]	  = $RowNumber + 6; // 行数5行 + 空行1行

		return $arg_array;
	}

	//////////////////////////////////////////////////////////
	//														//
	//   特定入院料（看護職員＋看護補助者）・看護職員集計	//
	//														//
	//////////////////////////////////////////////////////////
	function ShowNurseSumSpecificPlus( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KangoRowSum = $arg_array["KangoRowSum"];

		$KEI_RowU = $KangoRowSum;
		$KEI_RowL = $KangoRowSum+1;

/*
[看護職員の勤務実績(勤務時間数)	][夜勤従事者数(夜勤ありの職員数)〔Ｂ〕][計算式][月延べ勤務時間数の計〔Ｃ〕][計算式] <-------- $KMidSumRow1
				 [月延べ夜勤時間数　〔D−E〕	      ][計算式][月延べ夜勤時間数の計〔Ｄ〕][計算式][夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕][計算式] <---$KMidNext1
[1日当たり看護配置数]            [月平均１日当たり看護配置数〔C／(日数×８）〕	][計算式] <-------- $KMidSumRow2
				 [1日看護配置数　〔(A ／届出区分の数)×３〕	][計算式] <-------- $KMidNext2
*/

		// 中間集計１行目
		$RowNumber += 3;
		$KMidSumRow1 = $RowNumber;
		$KMidSumRow2 = $KMidSumRow1 + 2;
		$KMidNext1   = $KMidSumRow1 + 1;
		$KMidNext2   = $KMidSumRow2 + 1;

		// 1 LINE
		$excelObj->SetRowDim($KMidSumRow1,26.25);
		$excelObj->CellMerge("C{$KMidSumRow1}:G{$KMidNext1}");
		$excelObj->SetValueJP2("C{$KMidSumRow1}","看護職員の勤務実績(勤務時間数)","BOLD HCENTER VCENTER",$this->MSPgothic,12);

		$excelObj->CellMerge("H{$KMidSumRow1}:N{$KMidSumRow1}");
		$excelObj->SetValueJP2("H{$KMidSumRow1}","夜勤従事者数(夜勤ありの職員数)〔Ｂ〕","BOLD LEFT VCENTER FITSIZE",$this->MSPgothic,14);

		$excelObj->CellMerge("O{$KMidSumRow1}:Q{$KMidSumRow1}");
		$excelObj->SetValueJP2("O{$KMidSumRow1}","=M{$KEI_RowL}","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("R{$KMidSumRow1}:W{$KMidSumRow1}");
		$excelObj->SetValueJP2("R{$KMidSumRow1}","月延べ勤務時間数の計〔Ｃ〕","BOLD VCENTER",$this->MSPgothic,14);

		$excelObj->CellMerge("X{$KMidSumRow1}:Y{$KMidSumRow1}");
		$excelObj->SetValueJP2("X{$KMidSumRow1}","=AT$KEI_RowU+AT$KEI_RowL","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		// 2 LINE
		$excelObj->SetRowDim($KMidNext1,26.25);
		$excelObj->CellMerge("H{$KMidNext1}:N{$KMidNext1}");
		$excelObj->SetValueJP2("H{$KMidNext1}","月延べ夜勤時間数　〔D−E〕","LEFT VCENTER BOLD",$this->MSPgothic,14);

		$excelObj->CellMerge("O{$KMidNext1}:Q{$KMidNext1}");
		$excelObj->SetValueJP2("O{$KMidNext1}","=X{$KMidNext1}-AK{$KMidNext1}","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("R{$KMidNext1}:W{$KMidNext1}");
		$excelObj->SetValueJP2("R{$KMidNext1}","月延べ夜勤時間数の計〔Ｄ〕","BOLD VCENTER",$this->MSPgothic,14);

		$excelObj->CellMerge("X{$KMidNext1}:Y{$KMidNext1}");
		$excelObj->SetValueJP2("X{$KMidNext1}","=AT{$KEI_RowL}","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		$excelObj->CellMerge("Z{$KMidNext1}:AJ{$KMidNext1}");
		$excelObj->SetValueJP2("Z{$KMidNext1}","夜勤専従者及び月16時間以下の者の夜勤時間数〔E〕","VCENTER BOLD LEFT",$this->MSPgothic,12);

		$excelObj->CellMerge("AK{$KMidNext1}:AL{$KMidNext1}");
		$excelObj->SetValueJP2("AK{$KMidNext1}","=AU{$KEI_RowL}","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		// 3 LINE
		$excelObj->SetRowDim($KMidSumRow2,26.25);
		$excelObj->CellMerge("C{$KMidSumRow2}:G{$KMidNext2}");
		$excelObj->SetValueJP2("C{$KMidSumRow2}","1日当たり看護配置数","HCENTER VCENTER BOLD WRAP",$this->MSPgothic,12);

		$excelObj->CellMerge("H{$KMidSumRow2}:W{$KMidSumRow2}");
		$excelObj->SetValueJP2("H{$KMidSumRow2}","月平均１日当たり看護配置数 (看護職員) 〔C／(日数×８）〕","LEFT BOLD VCENTER FITSIZE",$this->MSPgothic,14);

		$excelObj->CellMerge("X{$KMidSumRow2}:Y{$KMidSumRow2}");
		$excelObj->SetValueJP2("X{$KMidSumRow2}","=ROUNDDOWN(X{$KMidSumRow1}/(N30*8),1)","VCENTER BOLD",$this->MSPgothic,12);
		$excelObj->SetFormatStyle("0.0");
		$excelObj->SetBackColor($this->green);

		$excelObj->SetValueJP2("Z{$KMidSumRow2}","（実績値）","VCENTER BOLD",$this->MSPgothic,11);

		// 4 LINE
		$excelObj->SetRowDim($KMidNext2,26.25);
		$excelObj->CellMerge("H{$KMidNext2}:W{$KMidNext2}");
		$excelObj->SetValueJP2("H{$KMidNext2}","1日看護配置数（看護職員） 〔(A ／届出区分の数)×３×看護要員中に必要な看護職員の比率〕","BOLD LEFT VCENTER FITSIZE",$this->MSPgothic,14);

		$excelObj->CellMerge("X{$KMidNext2}:Y{$KMidNext2}");
		$excelObj->SetValueJP2("X{$KMidNext2}","=L16","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->yellow);

		$excelObj->SetValueJP2("Z{$KMidNext2}","（基準値）","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$cellarea = "C{$KMidSumRow1}:Y{$KMidSumRow1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","top");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H{$KMidSumRow1}:Y{$KMidSumRow1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$cellarea = "C{$KMidNext1}:AL{$KMidNext1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "Z{$KMidNext1}:AL{$KMidNext1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("medium","","top");

		$excelObj->SetArea("AJ{$KMidNext1}");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H{$KMidSumRow1}:N{$KMidNext1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "R{$KMidSumRow1}:W{$KMidNext1}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "C{$KMidSumRow2}:Y{$KMidNext2}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H{$KMidSumRow2}:W{$KMidNext2}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");

		$cellarea = "H{$KMidSumRow2}:Y{$KMidSumRow2}";
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$arg_array["KMidSumRow1"] = $KMidSumRow1;
		$arg_array["KMidSumRow2"] = $KMidSumRow2;
		$arg_array["RowNumber"] = $RowNumber + 5 ;	// 行数4行+空行1行

		return $arg_array;
	}

	//////////////////////////////////////////////////////////
	//														//
	//	特定入院料（看護職員＋看護補助者）・看護補助者集計	//
	//		2012年対応										//
	//														//
	//////////////////////////////////////////////////////////
	function ShowNurseAssistanceSumSpecificPlus( $excelObj , $arg_array ){

		$RowNumber = $arg_array["RowNumber"];
		$KMidSumRow1 = $arg_array["KMidSumRow1"];
		$HojoRowSum = $arg_array["HojoRowSum"];	// 看護補助者表の「計」行、上段
		$HojoRowSumNext = $HojoRowSum + 1;	// 同上、下段

		////////////////
		// 中間集計行 //
		////////////////
		$excelObj->SetRowDim($RowNumber,13.5);
		$RowNumber ++;
		$excelObj->SetRowDim($RowNumber,18.75);

		$RowNumber ++;
		$HMidSumRow1 = $RowNumber;
		$HMidNext1m  = $HMidSumRow1 + 1;
		$HMidNext1b  = $HMidSumRow1 + 2;
		$HMidSumRow2 = $HMidSumRow1 + 3;
		$HMidNext2   = $HMidSumRow2 + 1;
/*

[看護補助者の勤務実績(勤務時間数)][看護補助者のみの月延べ勤務時間数の計〔F〕					   ][計算式] <--- $HMidSumRow1
				  [みなし看護補助者の月延べ勤務時間数の計〔G〕　〔C - 1日看護配置数×８×日数〕	   ][計算式] <--- $HMidNext1m
				  [看護補助者のみの月延べ夜勤時間数の計〔Ｈ〕 看護補助者（みなしを除く）のみの〔D〕][計算式] <--- $HMidNext1b
[みなし補助者を含む		 ][月平均1日当たり看護補助者配置数(みなし看護補助者含む）　〔(F+G) ／ (日数×８) 〕][計算式] <--- $HMidSumRow2
[1日当たり看護補助者配置数	 ][	1日看護補助配置数 〔I〕　〔(A ／届出区分の数)×３〕			   ][計算式] <--- $HMidNext2

*/

		// 1行目
		$excelObj->SetRowDim($HMidSumRow1,26.25);
		$excelObj->CellMerge("C".$HMidSumRow1.":G".$HMidNext1m);
		$excelObj->SetValueJP2("C$HMidSumRow1","看護補助者の勤務実績(勤務時間数)","BOLD HCENTER VCENTER",$this->MSPgothic,12);
		$excelObj->CellMerge("H".$HMidSumRow1.":X".$HMidSumRow1);
		$excelObj->SetValueJP2("H$HMidSumRow1","看護補助者のみの月延べ勤務時間数の計〔F〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidSumRow1.":Z".$HMidSumRow1);
		$excelObj->SetValueJP2("Y$HMidSumRow1","=AT$HojoRowSum+AT$HojoRowSumNext","BOLD VCENTER",$this->MSPgothic,12);
        $excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		// 2行目
		$excelObj->SetRowDim($HMidNext1m,26.25);
		$excelObj->CellMerge("H".$HMidNext1m.":X".$HMidNext1m);
		$excelObj->SetValueJP2("H$HMidNext1m","看護補助者のみの月延べ夜勤時間数の計〔Ｈ〕 看護補助者（みなしを除く）のみの〔D〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidNext1m.":Z".$HMidNext1m);
		$excelObj->SetValueJP2("Y$HMidNext1m","=AT$HojoRowSumNext","BOLD VCENTER",$this->MSPgothic,12);
        $excelObj->SetFormatStyle("0.00");
		$excelObj->SetBackColor($this->green);

		// 3行目
		$excelObj->SetRowDim($HMidSumRow2,26.25);
		$excelObj->CellMerge("C".$HMidNext1b.":G".$HMidNext1b);
		$excelObj->SetValueJP2("C$HMidNext1b","1日当たり看護補助者配置数","WRAP BOLD HCENTER VCENTER",$this->MSPgothic,12);
		$excelObj->CellMerge("H".$HMidNext1b.":X".$HMidNext1b);
		$excelObj->SetValueJP2("H$HMidNext1b","月平均1日当たり看護補助者配置数（みなし補助者除く）〔Ｊ〕　〔Ｆ／（日数×８）〕","BOLD VCENTER",$this->MSPgothic,14);
		$excelObj->CellMerge("Y".$HMidNext1b.":Z".$HMidNext1b);
		$excelObj->SetValueJP2("Y$HMidNext1b","=ROUNDDOWN(Y$HMidSumRow1/(N30*8),1)","BOLD VCENTER",$this->MSPgothic,12);
		$excelObj->SetBackColor($this->green);
		$excelObj->SetValueJP2("AA$HMidNext1b","(実績値)","BOLD VCENTER",$this->MSPgothic,11);

		// 罫線
		$cellarea = "C".$HMidSumRow1.":Z".$HMidNext1m;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","outline");

		$cellarea = "C".$HMidNext1b.":Z".$HMidNext1b;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("medium","","left");
		$excelObj->SetBorder2("medium","","bottom");
		$excelObj->SetBorder2("medium","","right");

		$cellarea = "H".$HMidSumRow1.":X".$HMidNext1b;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","left");
		$excelObj->SetBorder2("thin","","right");
		$cellarea = "H".$HMidSumRow1.":Z".$HMidSumRow1;
		$excelObj->SetArea($cellarea);
		$excelObj->SetBorder2("thin","","bottom");

		$arg_array["HMidSumRow1"] = $HMidSumRow1;
		$arg_array["HMidSumRow2"] = $HMidSumRow2;
		$arg_array["RowNumber"]	  = $RowNumber + 4; // 行数3行 + 空行1行

		return $arg_array;
	}

	//////////////////////////
	//			//
	// エクセルシートの調整 //
	//	2012年版	//
	//			//
	//////////////////////////
	function SetExcelSheet($excelObj, $arg_array, $HeaderMessage = ''){
		// 非表示列を画面から消す
		$excelObj->SetValueJPwith("AX1","非表示列","wrap");
		$excelObj->SetValueJPwith("AY1","非表示列","wrap");
		$excelObj->SetValueJPwith("AZ1","非表示列","wrap");
		$excelObj->SetValueJPwith("BA1","非表示列","wrap");
		$excelObj->SetArea("AX1:BA1");
		$excelObj->SetCharColor("FFFF0000"); // 赤文字 argvで指定、alpha値は機能しない
		$excelObj->SetBackColor("C0C0C0"); // 灰色
		$excelObj->SetFont($this->MSPgothic,11);
		$excelObj->SetPosV("CENTER");

		$excelObj->SetColDim("AX",-0.62);
		$excelObj->SetColDim("AY",-0.62);
		$excelObj->SetColDim("AZ",-0.62);
		$excelObj->SetColDim("BA",-0.62);

		// 印刷範囲設定
//		if ( $arg_array["HMidSumRow3"]  != 0 ){
//			$EndArea = $arg_array["HMidSumRow3"];
//		}else{
//			$EndArea = $arg_array["HMidSumRow2"];
//		}
		$EndArea = $arg_array["EndArea"];
		$excelObj->SetPritingArea("A1:AW".$EndArea);
		// 印刷ヘッダー設定
        if ($HeaderMessage == '') {
		    $HeaderMessage = '&L'; //'作成　公益社団法人　日本看護協会　NS100'削除 20140901 
		    $HeaderMessage = $HeaderMessage . "&R平成２６年度診療報酬改定対応版"; //'\n更新：平成２４年３月'削除 ２４ー＞２６年度に変更 20140901
        }
		$excelObj->SetPrintHeader( $HeaderMessage );

		// エクセル画面を開いたときのカーソル位置を左上に設定
		$excelObj->SetArea("A1");
		$excelObj->SetPosV("");
	}
}

/* DEBUG情報

看護協会版届出様式2012年度版
・一般病棟入院基本料表
・療養病棟入院基本料表１
・療養病棟入院基本料表２

(△印はブランクを示します)

  (様式９)入院基本料の施設基準等に係る届出書添付書類
  …………………
　……(中略)……
  …………………
 △…………△※今月の稼働日数△…………△[ ]日	 <-- $DayCntRow = $KangoRowTop - 4;
 [  ]月△…△※常勤職員の週所定労働時間△[ ]時間 <-- $LaborCntRow = $KangoRowTop - 3;
【勤務計画表】
《看護職員表》
+--------------------------
|見出し3行			<-- 見出しトップ行 $KangoRowTop
+--------------------------
|職員毎明細データ行		<-- データトップ行 $KangoRowDataTop
|
|	データ件数は count($data_array)
|
+--------------------------
|小計				<-- $KangoRowSmallSum (上段)
+-+------------------------
△|計				<-- $KangoRowSum (上段)
　+------------------------
(空行1行)
△[看護職員の勤務実績(勤務時間数)……	<- $KMidSumRow1
△[
△[1日当たり看護配置数……		<- $KMidSumRow2
△[
△[＜看護職員夜間看護配置加算＞……	<- $KMidSumRow3	(療養病棟にはありません)
△[　1日当たり夜間看護配置数
(空行2行)

《看護補助者表》
 +-------------------------
 |見出し3行			<-- 見出しトップ行 $HojoRowTop
 +-------------------------
 |職員毎明細データ行		<-- データトップ行 $HojoRowDataTop
 |
 |	データ件数は count($data_array)
 |
 +-------------------------
 |小計				<-- $HojoRowSmallSum (上段)
 +-------------------------
 |計				<-- $HojoRowSum (上段)
 +-------------------------
(空行1行)
 ※……				左側の文言は一般病棟と療養病棟で異なります
 [……………			<-- $HMidSumRow1
 [……
 [……
 [……………			<-- $HMidSumRow2
 [……
 [……………			<-- $HMidSumRow3 (療養病棟にはありません)
 [……
 [……………			<-- $HMidSumRow4 (療養病棟にはありません)
 [……

(空行3行)

△…△[参考][月総夜勤時間……
△……………△※夜勤専従者数は……
(空行1行)
△……………△【看護職員】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow1
(空行1行)
△……………△【看護補助者】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow2
(空行1行)
△……………△【看護職員＋看護補助者】
△……………△ (みだし)
△……………△ (計算式)			<-- $GSumRow3


・計算式・書式等は必ず看護協会の雛形と照合してください。
・罫線はなるべくまとめて範囲指定で引くようにしないと遅いです
・行数はデータ件数から算出できますが実行数をカウントしています。

at 2012.卯月吉日
*/
?>