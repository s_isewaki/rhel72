<?php
require_once('jnl_indicator_data.php');
require_once('jnl_indicator_facility.php');
require_once('jnl_indicator_nurse_home.php');
require_once('jnl_indicator_return.php');
/**
 * ajax 関連で使用するクラス
 */
class IndicatorAjax extends IndicatorData
{
    var $jnlInspectorList;

    var $indicatorFacility;

    var $indicatorNurseHome;

    var $indicatorReturn;

    var $conArr;

    var $selectMessage = '選択してください';

    /**
     * コンストラクタ
     */
    function IndicatorAjax($arr)
    {
        parent::Indicator($arr);
        $this->setConArr($arr);
    }

    # setter, getter
    function setConArr($arr)
    {
        $this->conArr = $arr;
    }

    function getConArr()
    {
        return $this->conArr;
    }

    function setJnlInspectorList()
    {
        $this->jnlInspectorList = new JnlInspectorList($this->getConArr());
    }

    function getJnlInspectorList()
    {
        if (!is_object($this->jnlInspectorList)) { $this->setJnlInspectorList(); }
        return $this->jnlInspectorList;
    }

    function setIndicatorFacility()
    {
        $this->indicatorFacility = new IndicatorFacility($this->getConArr());
    }

    function getIndicatorFacility()
    {
        if (!is_object($this->indicatorFacility)) { $this->setIndicatorFacility(); }
        return $this->indicatorFacility;
    }

    function setIndicatorNurseHome()
    {
        $this->indicatorNurseHome = new IndicatorNurseHome($this->getConArr());
    }

    function getIndicatorNurseHome()
    {
        if (!is_object($this->indicatorNurseHome)) { $this->setIndicatorNurseHome(); }
        return $this->indicatorNurseHome;
    }

    function setIndicatorReturn()
    {
        $this->indicatorReturn = new IndicatorReturn($this->getConArr());
    }

    function getIndicatorReturn()
    {
        if (!is_object($this->indicatorReturn)) { $this->setIndicatorReturn(); }
        return $this->indicatorReturn;
    }

    # main

    /**
     * @access public
     * @param  object $obj
     * @return json
     */
    function getCtg($obj)
    {
        $empId  = null;
        $rtnArr = array();

        if (is_object($obj)) { $empId = $obj->emp_id; }

        $resArr    = $this->getCategory($empId);
        $rtnArr[0] = array('num' => null, 'name' => $this->selectMessage);
        foreach ($resArr as $resKey => $resVal) { $rtnArr[] = array('num' => $resKey, 'name' => $resVal); }
        return $this->json_encode($resArr);
    }

    /**
     * @access public
     * @param  object $obj
     * @return json
     */
    function getSubCtg($obj)
    {
        $rtnArr = array();
        $empId  = null;
        $ctg    = null;

        if (is_object($obj)) {
            $empId = $obj->emp_id;
            $ctg   = $obj->ctg;
        }

        $resArr    = $this->getSubCategory($empId, $ctg);
        $rtnArr[0] = array('num' => null, 'name' => $this->selectMessage);
        foreach ($resArr as $resKey => $resVal) { $rtnArr[] = array('num' => $resKey, 'name' => $resVal); }
        return $this->json_encode($rtnArr);
    }

    /**
     * @access public
     * @param  object  $obj
     * @return json
     */
    function getFid($obj)
    {
        $rtnArr = array();
        $empId  = null;
        $ctg    = null;
        $subCtg = null;

        if (is_object($obj)) {
            $empId  = $obj->emp_id;
            $ctg    = $obj->ctg;
            $subCtg = $obj->sub_ctg;
        }

        $rtnArr[0] = array('num' => null, 'name' => $this->selectMessage);
        $resArr    = $this->getFacility($empId, $ctg, $subCtg);
        foreach ($resArr as $resKey => $resVal) { $rtnArr[] = array('num' => $resKey, 'name' => $resVal); }
        return $this->json_encode($rtnArr);
    }

    /**
     * @access public
     * @param  object $obj
     * @return json
     */
    function getNo($obj)
    {
        $rtnArr     = array();
        $empId      = null;
        $ctg        = null;
        $subCtg     = null;
        $facilityId = null;

        if (is_object($obj)) {
            $empId      = $obj->emp_id;
            $ctg        = $obj->ctg;
            $subCtg     = $obj->sub_ctg;
            $facilityId = $obj->facility_id;
        }

        $rtnArr[0] = array('num' => null, 'name' => $this->selectMessage);
        $resArr    = $this->getReportNo($empId, $ctg, $subCtg, $facilityId);
        foreach ($resArr as $resKey => $resVal) { $rtnArr[] = array('num' => $resKey, 'name' => $resVal); }
        return $this->json_encode($rtnArr);
    }

    /**
     * (non-PHPdoc)
     * @see Indicator#getDisplayNoNameArr($cNum, $scNum)
     */
    function getDisplayNoNameArr($ctg, $subCtg, $reportNo=null, $empId=null, $plant=null)
    {
        $rtnArr  = array();
        $dataArr = $this->getNoForCategory($ctg, $subCtg);
        if (!is_null($empId)) {
            $checkedNoArr = $this->checkNo(array('emp_id' => $empId, 'ctg' => $ctg, 'sub_ctg' => $subCtg, 'facility_id' => $plant));
            foreach ($dataArr as $dataKey => $dataVal) {
                if ($this->getArrValueByKey($dataKey, $checkedNoArr)) {
                    if (is_null($reportNo)) { $rtnArr[$dataKey] = $dataVal['name']; }
                    else if ($dataKey === $reportNo) { $rtnArr[$dataKey] = $dataVal['name']; }
                }
            }
        }
        else {
            foreach ($dataArr as $dataKey => $dataVal) {
                if (is_null($reportNo)) { $rtnArr[$dataKey] = $dataVal['name']; }
                else if ($dataKey === $reportNo) { $rtnArr[$dataKey] = $dataVal['name']; }
            }
        }
        return $rtnArr;
    }

    function getDisplayNoFileNameArr($ctg, $subCtg, $reportNo=null)
    {
        $rtnArr  = array();
        $dataArr = $this->getNoForCategory($ctg, $subCtg);
        foreach ($dataArr as $dataKey => $dataVal) {
            if (is_null($reportNo)) { $rtnArr[$dataKey] = $dataVal['file']; }
            else if (!is_null($reportNo) && $dataKey === $reportNo) { $rtnArr[$dataKey] = $dataVal['file']; }
        }
        return $rtnArr;
    }

    /**
     * (non-PHPdoc)
     * @see Indicator#getCategory($cNum, $minFlg)
     */
    function getCategory($empId)
    {
        $rtnArr  = array();
        $ctgArr  = parent::getCategory();
        $listArr = $this->checkCtg(array('emp_id' => $empId));
        foreach ($ctgArr as $ctgKey => $ctgVal) { if ($this->getArrValueByKey($ctgKey, $listArr) !== null) { $rtnArr[$ctgKey] = $ctgVal; } }
        return $rtnArr;
    }

    /**
     * (non-PHPdoc)
     * @see Indicator#getSubCategory($cNum, $scNum)
     */
    function getSubCategory($empId=null, $ctg)
    {
        $rtnArr            = array();
        /* @var $indicatorFacility IndicatorFacility */
        $indicatorFacility = $this->getIndicatorFacility();
        $subCtgArr         = $indicatorFacility->getSubCategory($ctg);
        if (is_null($subCtgArr)) {
            $subCtgArr = parent::getSubCategory($ctg);
            if (is_null($subCtgArr)) {
                /* @var $indicatorNurseHome IndicatorNurseHome */
                $indicatorNurseHome = $this->getIndicatorNurseHome();
                $subCtgArr          = $indicatorNurseHome->getSubCategory($ctg);
                if (is_null($subCtgArr)) {
                    /* @var $indicatorReturn IndicatorReturn */
                    $indicatorReturn = $this->getIndicatorReturn();
                    $subCtgArr       = $indicatorReturn->getSubCategory($ctg);
                }
            }
        }
        $listArr = $this->checkSubCtg(array('emp_id' => $empId, 'ctg' => $ctg));
        foreach ($subCtgArr as $subCtgKey => $subCtgVal) { if ($this->getArrValueByKey($subCtgKey, $listArr)) { $rtnArr[$subCtgKey] = $subCtgVal; } }
        return $rtnArr;
    }

    /**
     * (non-PHPdoc)
     * @see IndicatorData#getFacility($facilityId, $facilityType)
     */
    function getFacility($empId, $ctg, $subCtg)
    {
        $rtnArr        = array();
        $facilityIdArr = parent::getFacility(null, $subCtg);
        $listArr       = $this->checkFid(array('emp_id' => $empId, 'ctg' => $ctg, 'sub_ctg' => $subCtg));
        foreach ($facilityIdArr as $facilityIdVal) {
            $facilityId = $this->getArrValueByKey('facility_id', $facilityIdVal);
            if ($this->getArrValueByKey($facilityId, $listArr)) { $rtnArr[$facilityId] = $this->getArrValueByKey('facility_name', $facilityIdVal); }
        }
        return $rtnArr;
    }

    /**
     * @param  string $ctg
     * @param  string $subCtg
     * @return array  $noArr
     */
    function getNoForCategory($ctg, $subCtg)
    {
        /* @var $indicatorFacility IndicatorFacility */
        $indicatorFacility = $this->getIndicatorFacility();
        $noArr             = $indicatorFacility->getNoForCategory($ctg, $subCtg);
        if (count($noArr) === 0) {
            $noArr = parent::getNoForCategory($ctg, $subCtg);
            if (count($noArr) === 0) {
                /* @var $indicatorNurseHome IndicatorNurseHome */
                $indicatorNurseHome = $this->getIndicatorNurseHome();
                $noArr              = $indicatorNurseHome->getNoForCategory($ctg, $subCtg);
                if (count($noArr) === 0) {
                    /* @var $indicatorReturn IndicatorReturn */
                    $indicatorReturn = $this->getIndicatorReturn();
                    $noArr           = $indicatorReturn->getNoForCategory($ctg, $subCtg);
                }
            }
        }
        return $noArr;
    }

    /**
     * @param  string $empId
     * @param  string $ctg
     * @param  string $subCtg
     * @param  string $facilityId
     * @return array  $rtnArr
     */
    function getReportNo($empId, $ctg, $subCtg=null, $facilityId=null)
    {
        $rtnArr  = array();
        $noArr   = $this->getNoForCategory($ctg, $subCtg);
        $listArr = $this->checkNo(array('emp_id' => $empId, 'ctg' => $ctg, 'sub_ctg' => $subCtg, 'facility_id' => $facilityId));
        foreach ($noArr as $noKey => $noVal) { if ($this->getArrValueByKey($noKey, $listArr)) { $rtnArr[$noKey] = $this->getArrValueByKey('name', $noVal); } }
        return $rtnArr;
    }

    /**
     * @access protected
     * @param  array     $arr
     * @return array     $rtnArr
     */
    function checkCtg($arr)
    {
        $rtnArr           = array();
        $jnlInspectorList = $this->getJnlInspectorList();
        $listArr          = $jnlInspectorList->getCtg($arr);
        if (!is_array($listArr)) { return false; }
        foreach ($listArr as $listVal) {
            $reportCtg          = $this->getArrValueByKey('report_ctg', $listVal);
            $rtnArr[$reportCtg] = $reportCtg;
        }
        return $rtnArr;
    }

    /**
     * @access protected
     * @param  array     $arr
     * @return array     $rtnArr
     */
    function checkSubCtg($arr)
    {
        $rtnArr           = array();
        /* @var $jnlInspectorList JnlInspectorList */
        $jnlInspectorList = $this->getJnlInspectorList();
        $listArr          = $jnlInspectorList->getSubCtg($arr);
        if (!is_array($listArr)) { return false; }
        foreach ($listArr as $listKey => $listVal) {
            $reportSubCtg          = $this->getArrValueByKey('report_sub_ctg', $listVal);
            $rtnArr[$reportSubCtg] = $reportSubCtg;
        }
        return $rtnArr;
    }

    /**
     * @param  array $arr
     * @return array $rtnArr
     */
    function checkFid($arr)
    {
        $rtnArr           = array();
        /* @var $jnlInspectorList JnlInspectorList */
        $jnlInspectorList = $this->getJnlInspectorList();
        $listArr          = $jnlInspectorList->getFacilityId($arr);
        if (!is_array($listArr)) { return false; }
        foreach ($listArr as $listKey => $listVal) {
            $facilityId          = $this->getArrValueByKey('jnl_facility_id', $listVal);
            $rtnArr[$facilityId] = $facilityId;
        }
        return $rtnArr;
    }

    /**
     * @param  array $arr
     * @return array $rtnArr
     */
    function checkNo($arr)
    {
        $rtnArr           = array();
        /* @var $jnlInspectorList JnlInspectorList */
        $jnlInspectorList = $this->getJnlInspectorList();
        $listArr          = $jnlInspectorList->getReportNo($arr);
        if (!is_array($listArr)) { return false; }
        foreach ($listArr as $listKey => $listVal) {
            $reportNo          = $this->getArrValueByKey('report_no', $listVal);
            $rtnArr[$reportNo] = $reportNo;
        }
        return $rtnArr;
    }

    ####################################################################
    ### 注）以下、 php4 になくて php5 から存在する function の代わり ###
    ####################################################################

    /**
     * php4 には json_encode がないのでコピー
     * [http://upgradephp.berlios.de/]
     *
     * php5 へ以降する際はこの関数は不要
     * @param  array  $arr
     * @return string $rtnStr
     */
    function json_encode($var, $obj=false)
    {
        $json = "";
        if (is_array($var) || ($obj=is_object($var))) {
            if (!$obj) foreach ((array)$var as $i=>$v) {
                if (!is_int($i)) {
                    $obj = 1;
                    break;
                }
            }

            foreach ((array)$var as $i=>$v) { $json .= ($json ? "," : "").($obj ? ("\"$i\":") : "").($this->json_encode($v)); }
            $json = $obj ? "{".$json."}" : "[".$json."]";
        }
        elseif (is_string($var)) {
            if (!utf8_decode($var)) { $var = utf8_encode($var); }
            $var = str_replace(array("\"", "\\", "/", "\b", "\f", "\n", "\r", "\t"), array("\\\"", "\\\\", "\\/", "\\b", "\\f", "\\n", "\\r", "\\t"), $var);
            $json = '"' . $var . '"';
        }
        elseif (is_bool($var)) { $json = $var ? "true" : "false"; }
        elseif ($var === NULL) { $json = "null"; }
        elseif (is_int($var) || is_float($var)) { $json = "$var"; }
        else { trigger_error("json_encode: don't know what a '" .gettype($var). "' is.", E_USER_ERROR); }

        return($json);
    }

    /**
     * json_encode に同じ
     * @param $json
     * @param $assoc
     * @param $n
     * @param $state
     * @param $waitfor
     * @return unknown_type
     */
    function json_decode($json, $assoc=FALSE, /*emu_args*/$n=0,$state=0,$waitfor=0)
    {
        $val = NULL;
        static $lang_eq = array("true" => TRUE, "false" => FALSE, "null" => NULL);
        static $str_eq  = array("n"=>"\012", "r"=>"\015", "\\"=>"\\", '"'=>'"', "f"=>"\f", "b"=>"\b", "t"=>"\t", "/"=>"/");

        #-- flat char-wise parsing
        for (/*n*/; $n<strlen($json); /*n*/) {
            $c = $json[$n];
            #-= in-string
            if ($state==='"') {
                if ($c == '\\') {
                    $c = $json[++$n];
                    # simple C escapes
                    if (isset($str_eq[$c])) { $val .= $str_eq[$c]; }
                    # here we transform \uXXXX Unicode (always 4 nibbles) references to UTF-8
                    elseif ($c == "u") {
                        # read just 16bit (therefore value can't be negative)
                        $hex = hexdec( substr($json, $n+1, 4) );
                        $n += 4;
                        # Unicode ranges
                        # plain ASCII character
                        if ($hex < 0x80) { $val .= chr($hex); }
                        # 110xxxxx 10xxxxxx
                        elseif ($hex < 0x800) { $val .= chr(0xC0 + $hex>>6) . chr(0x80 + $hex&63); }
                        # 1110xxxx 10xxxxxx 10xxxxxx
                        elseif ($hex <= 0xFFFF) { $val .= chr(0xE0 + $hex>>12) . chr(0x80 + ($hex>>6)&63) . chr(0x80 + $hex&63); }
                        # other ranges, like 0x1FFFFF=0xF0, 0x3FFFFFF=0xF8 and 0x7FFFFFFF=0xFC do not apply
                    }
                    # no escape, just a redundant backslash
                    #@COMPAT: we could throw an exception here
                    else { $val .= "\\" . $c; }
                }
                # end of string
                elseif ($c == '"') { $state = 0; }
                # yeeha! a single character found!!!!1!
                #@COMPAT: specialchars check - but native json doesn't do it?
                else /*if (ord($c) >= 32)*/ { $val .= $c; }
            }
            #-> end of sub-call (array/object)
            # return current value and state
            elseif ($waitfor && (strpos($waitfor, $c) !== false)) { return array($val, $n); }
            #-= in-array
            elseif ($state===']') {
                list($v, $n) = $this->json_decode($json, 0, $n, 0, ",]");
                $val[] = $v;
                if ($json[$n] == "]") { return array($val, $n); }
            }
            #-= in-object
            elseif ($state==='}') {
                list($i, $n) = $this->json_decode($json, 0, $n, 0, ":");   # this allowed non-string indicies
                list($v, $n) = $this->json_decode($json, 0, $n+1, 0, ",}");
                $val[$i] = $v;
                if ($json[$n] == "}") { return array($val, $n); }
            }
            #-- looking for next item (0)
            else {
                #-> whitespace
                # skip
                if (preg_match("/\s/", $c)) {}
                #-> string begin
                elseif ($c == '"') { $state = '"'; }
                #-> object
                elseif ($c == "{") {
                    list($val, $n) = $this->json_decode($json, $assoc, $n+1, '}', "}");
                    if ($val && $n && !$assoc) {
                        $obj = new stdClass();
                        foreach ($val as $i=>$v) { $obj->{$i} = $v; }
                        $val = $obj;
                        unset($obj);
                    }
                }
                #-> array
                elseif ($c == "[") { list($val, $n) = $this->json_decode($json, $assoc, $n+1, ']', "]"); }
                #-> comment
                # just find end, skip over
                elseif (($c == "/") && ($json[$n+1]=="*")) { ($n = strpos($json, "*/", $n+1)) or ($n = strlen($json)); }
                #-> numbers
                elseif (preg_match("#^(-?\d+(?:\.\d+)?)(?:[eE]([-+]?\d+))?#", substr($json, $n), $uu)) {
                    $val = $uu[1];
                    $n += strlen($uu[0]) - 1;
                    # float
                    if (strpos($val, ".")) { $val = (float)$val; }
                    # oct
                    elseif ($val[0] == "0") { $val = octdec($val); }
                    else { $val = (int)$val; }
                    # exponent?
                    if (isset($uu[2])) { $val *= pow(10, (int)$uu[2]); }
                }
                #-> boolean or null
                elseif (preg_match("#^(true|false|null)\b#", substr($json, $n), $uu)) {
                    $val = $lang_eq[$uu[1]];
                    $n += strlen($uu[1]) - 1;
                }
                #-- parsing error
                else {
                    # PHPs native json_decode() breaks here usually and QUIETLY
                    trigger_error("json_decode: error parsing '$c' at position $n", E_USER_WARNING);
                    return $waitfor ? array(NULL, 1<<30) : NULL;
                }
            } # state
            #-- next char
            if ($n === NULL) { return NULL; }
            $n++;
        } # for

        #-- final result
        return ($val);
    }
}

class JnlInspectorList extends IndicatorData
{
    /**
     * コンストラクタ
     * @param $arr
     */
    function JnlInspectorList($arr)
    {
        parent::IndicatorData($arr);
    }

    /**
     *
     * @param  array $arr
     * @return array
     */
    function getCtg($arr)
    {
        $empId = $this->getArrValueByKey('emp_id', $arr);
        $sql   = "SELECT report_ctg FROM jnl_inspector_list WHERE emp_id IS NOT NULL";

        if (!is_null($empId)) { $sql .= sprintf(" AND emp_id = '%s'", $empId); }

        $sql .= " GROUP BY report_ctg ";
        return $this->getDateList($sql);
    }

    /**
     *
     * @param  array $arr
     * @return array
     */
    function getSubCtg($arr)
    {
        $empId = $this->getArrValueByKey('emp_id', $arr);
        $ctg   = $this->getArrValueByKey('ctg',    $arr);
        $sql   = "SELECT report_sub_ctg FROM jnl_inspector_list WHERE emp_id IS NOT NULL";

        if (!is_null($empId)) { $sql .= sprintf(" AND emp_id = '%s'", $empId); }
        if (!is_null($ctg))   { $sql .= sprintf(" AND report_ctg = '%s'", $ctg); }

        $sql .= " GROUP BY report_sub_ctg ";
        return $this->getDateList($sql);
    }

    /**
     * @param  array $arr
     * @return array
     */
    function getFacilityId($arr)
    {
        $empId = $this->getArrValueByKey('emp_id', $arr);
        $ctg   = $this->getArrValueByKey('ctg',    $arr);
        $sql   = "SELECT jnl_facility_id FROM jnl_inspector_list WHERE emp_id IS NOT NULL";

        if (!is_null($empId)) { $sql .= sprintf(" AND emp_id = '%s'", $empId); }
        if (!is_null($ctg))   { $sql .= sprintf(" AND report_ctg = '%s'", $ctg); }

        $sql .= " GROUP BY jnl_facility_id ";
        return $this->getDateList($sql);
    }

    /**
     * @param  array $arr
     * @return array
     */
    function getReportNo($arr)
    {
        $empId      = $this->getArrValueByKey('emp_id',      $arr);
        $ctg        = $this->getArrValueByKey('ctg',         $arr);
        $subCtg     = $this->getArrValueByKey('sub_ctg',     $arr);
        $facilityId = $this->getArrValueByKey('facility_id', $arr);

        $sql = "SELECT report_ctg, report_sub_ctg, report_no FROM jnl_inspector_list WHERE emp_id IS NOT NULL";

        if (!is_null($empId))      { $sql .= sprintf(" AND emp_id = '%s'", $empId); }
        if (!is_null($ctg))        { $sql .= sprintf(" AND report_ctg = '%s'", $ctg); }
        if (!is_null($subCtg))     { $sql .= sprintf(" AND report_sub_ctg = '%s'", $subCtg); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%s'", $facilityId); }

        $sql .= " GROUP BY emp_id, report_ctg, report_sub_ctg, report_no ";
        return $this->getDateList($sql);
    }
}