<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID
	$mode
		"insert":新規登録(デフォルト)
		"update";更新
	$problem_id
		プロブレムＩＤ(更新用)
	$default_class
		初期表示の問題種別(新規登録用)

*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("show_kanja_joho.ini"); ?>
<? require("summary_common.ini"); ?>
<? require("show_problem_input_form.ini"); ?>
<? require("summary_ymd_select_util.ini"); ?>
<? require_once("get_menu_label.ini"); ?>
<?
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
// 画面パラメータの整理
//==============================
if($mode != "update")
{
	$mode = "insert";
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//表示文言切り替え
//==============================
$touroku_koushin = "更新";
if($mode=="insert")
{
	$touroku_koushin = "登録";
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title><?=get_report_menu_label($con, $fname)?>｜プロブレム<?=$touroku_koushin?></title>
<?
//カレンダー外部ファイル
write_yui_calendar_use_file_read();
?>
<script type="text/javascript">

function load_action()
{
	//フォームに対するロード時の処理を行います。
	onload_show_proclem_input_form();
}

<?
//カレンダーJavaScript
write_common_ymd_ctl_script();
write_yui_calendar_script("start_date","start_date_year","start_date_month","start_date_day","start_date_caller","start_date_calendar");
write_yui_calendar_script("end_date","end_date_year","end_date_month","end_date_day","end_date_caller","end_date_calendar");
?>

</script>
</head>
<body onload="load_action();">
<div style="padding:4px">


<!-- カレンダー用 START -->
<div id="start_date_calendar" style="position:absolute;display:none;z-index:10000;"> </div>
<div id="end_date_calendar" style="position:absolute;display:none;z-index:10000;"> </div>
<!-- カレンダー用 END -->

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("プロブレム".$touroku_koushin); ?>

<? //入力フィールド 出力 ?>

<div style="margin-top:8px">
<? show_problem_input_form($con,$fname,$mode,$session,$pt_id,@$problem_id,@$default_class); ?>
</div>


</div>
</body>
</html>
<?

pg_close($con);
?>
