<?php

//タイムカード管理画面用のSQLを取得する
function get_cond_work_admin($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg) {

	//利用停止者も含める
    if ($sus_flg != "") {
		$cond = "where true ";
    }
	else {
		$cond = "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
	}
    //検索条件　職員ID
    if ($srch_id != "") {
        $tmp_srch_id = str_replace(" ", "", $srch_id);
        $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
    }
    //検索条件　職員名
	if ($srch_name != "") {
		$tmp_srch_name = str_replace(" ", "", $srch_name);
        $cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
	}
	//検索条件　属性：事務所
	if ($cls != "" && substr($cls, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_class = $cls) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $cls))";
	}
	//検索条件　属性：課
	if ($atrb != "" && substr($atrb, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_attribute = $atrb) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = $atrb))";
	}
	//検索条件　属性：科
	if ($dept != "" && substr($dept, 0, 1) != "-") {
		$cond .= " and ((empmst.emp_dept = $dept) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = $dept))";
	}
	//検索条件　雇用・勤務形態
	if ($duty_form_jokin != $duty_form_hijokin) {
		if ($duty_form_hijokin == "checked") {
			//非常勤
			$cond .= " and ";
		}
		else {
			//常勤
			$cond .= " and NOT ";
		}
		$cond .= "EXISTS(SELECT * FROM empcond WHERE empcond.emp_id = empmst.emp_id AND duty_form = 2)";
	}
	//検索条件　出勤グループ
	if ($group_id != "" && $group_id != "-") {
		$cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and empcond.tmcd_group_id = $group_id)";
	}
	
	//検索条件　シフトグループ
	if ($shift_group_id != "" && $shift_group_id != "-") {
		$cond .= " and exists (select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id and duty_shift_staff.group_id = '$shift_group_id')"; //職員設定の情報から
	}
	
	//検索条件　月集計CSVレイアウト
	if ($csv_layout_id != "" && $csv_layout_id != "-") {
		$cond .= " and exists (select * from empcond where empcond.emp_id = empmst.emp_id and (empcond.csv_layout_id = '$csv_layout_id'";
		if ($csv_layout_id == "01") {
			$cond .= " or empcond.csv_layout_id is null"; 
		}
		$cond .= "))"; 
	}
	//ユーザ画面からの場合、所属権限を確認
	global $mmode, $timecard_tantou_branches;
	if ($mmode == "usr") {
        $cond .= " and (1 <> 1";
        foreach ($timecard_tantou_branches as $r) {
            if ($r["class_id"]) $cond .= " or (";
            if ($r["class_id"]) $cond .= "     empmst.emp_class=".$r["class_id"];
            if ($r["atrb_id"])  $cond .= "     and empmst.emp_attribute=".$r["atrb_id"];
            if ($r["dept_id"])  $cond .= "     and empmst.emp_dept=".$r["dept_id"];
            if ($r["class_id"]) $cond .= " )";
        }
        //兼務
        $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and (1 <> 1";
        foreach ($timecard_tantou_branches as $r) {
            if ($r["class_id"]) $cond_ccr .= " or (";
            if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
            if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
            if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
            if ($r["class_id"]) $cond_ccr .= " )";
        }
        $cond_ccr .= "))";
        $cond .= $cond_ccr;
        $cond .= ")";
	}
	return $cond;
}

//検索条件前後の職員を取得する
function get_move_emp_id($con, $fname, $emp_move_flg, $emp_personal_id, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg) {
	
	$sql = "select emp_id, emp_personal_id from empmst";
	
	//検索条件SQLを取得
    $cond = get_cond_work_admin($con, $fname, $srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $shift_group_id, $csv_layout_id, $srch_id, $sus_flg);

	//前後の職員取得条件追加
	$cond .= " and emp_personal_id "; 
	if ($emp_move_flg == "next") {
		$cond .= " > "; 
	} else {
		$cond .= " < "; 
	}
	$cond .= "  '$emp_personal_id' "; 
	if ($emp_move_flg == "next") {
		$cond .= " order by emp_personal_id limit 1"; 
	} else {
		$cond .= " order by emp_personal_id desc  limit 1"; 
	}
	
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_num_rows($sel);
	if ($num > 0) {
		$emp_id = pg_fetch_result($sel, 0, "emp_id");
	}
	
	return $emp_id;
}


?>