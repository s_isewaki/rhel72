<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜対象者検索</title>
<?
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_select_values.ini");
require_once("atdbk_common_class.php");
require_once("work_admin_paid_summer_common.php");
require_once("show_timecard_common.ini");
require_once("atdbk_menu_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}
//1ページあたり件数
$limit = 50;
$url_srch_name = urlencode($srch_name);

// DBコネクション作成
$con = connect2db($fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

//付与年月日入力の場合
if ($select_add_yr != "-" && $select_add_yr != "") {
	$year = $select_add_yr;
}
else {
	$cur_year = date("Y");
	$md = date("md");;
}

//年一覧開始年
if ($start_year == "") {
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
$end_year = $start_year + 6;

$arr_class_name = get_class_name_array($con, $fname);

// 組織情報を取得し、配列に格納
$arr_cls = array();
$arr_clsatrb = array();
$arr_atrbdept = array();
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));

	$sql2 = "select atrb_id, atrb_nm from atrbmst";
    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' order by order_no";
	$sel2 = select_from_table($con, $sql2, $cond2, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_atrb = array();
	while ($row2 = pg_fetch_array($sel2)) {
		$tmp_atrb_id = $row2["atrb_id"];
		$tmp_atrb_nm = $row2["atrb_nm"];
		array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));

		$sql3 = "select dept_id, dept_nm from deptmst";
        $cond3 = "where atrb_id = '$tmp_atrb_id' and dept_del_flg = 'f' order by order_no";
		$sel3 = select_from_table($con, $sql3, $cond3, $fname);
		if ($sel3 == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_dept = array();
		while ($row3 = pg_fetch_array($sel3)) {
			$tmp_dept_id = $row3["dept_id"];
			$tmp_dept_nm = $row3["dept_nm"];
			array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
		}
		array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
	}
	array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
}

// 該当する職員を検索
$sel_emp = false;
$arr_data = array();

if ($srch_flg == "1") {
	$sql = "select count(*) from empmst";
	$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
    $sql .= " left join (select distinct on (b.emp_id) b.* from emppaid_summer b ";
    $sql .= " ) k on k.emp_id = empmst.emp_id ";

    $cond = get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $srch_id);


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPageselect_close_yr(window);</script>");
		exit;
	}
	$emp_cnt = pg_fetch_result($sel, 0, 0);

	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}
	//sql文取得
	$sql = get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $year, $sort);

	$offset = $page * $limit;
	//ソート順
	if ($sort == "") {
		$sort = "1";
	}
	switch ($sort) {
		case "1":
			$orderby = "empmst.emp_personal_id ";
			break;
		case "2":
			$orderby = "empmst.emp_personal_id desc ";
			break;
		case "3":
			$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
			break;
		case "4":
			$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
			break;
		case "5":
			$orderby = "c.order_no, d.order_no, e.order_no, empmst.emp_personal_id ";
			break;
		case "6":
			$orderby = "c.order_no desc, d.order_no desc, e.order_no desc, empmst.emp_personal_id ";
			break;
	}

	$cond .= " order by $orderby offset $offset limit $limit";

    $sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

}

$sql = "select closing_paid_holiday from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol_summer order by order_no";
$cond = "";
$paid_sel = select_from_table($con, $sql, $cond, $fname);
if ($paid_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
while($row = pg_fetch_array($paid_sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=1; $i++) {
		$varname = "add_day".$i;
		$arr_paid_hol[$paid_hol_tbl_id][$i] = $row["$varname"];
	}
}

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function initPage() {
	classOnChange('<? echo($atrb); ?>', '<? echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<? foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<? echo $tmp_clsatrb["class_id"]; ?>') {
	<? foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<? echo $tmp_atrb["id"]; ?>', '<? echo $tmp_atrb["name"]; ?>', atrb_id);
	<? } ?>
	}
<? } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<? foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<? echo $tmp_atrbdept["atrb_id"]; ?>') {
	<? foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<? echo $tmp_dept["id"]; ?>', '<? echo $tmp_dept["name"]; ?>', dept_id);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}

//検索時のパラメータチェック
function chkPara() {
	if ((document.mainform.select_add_yr.selectedIndex == 0 &&
		document.mainform.select_add_mon.selectedIndex == 0 &&
		document.mainform.select_add_day.selectedIndex == 0) ||
		(document.mainform.select_add_yr.selectedIndex != 0 &&
		document.mainform.select_add_mon.selectedIndex != 0)) {

        if (document.mainform.select_add_day.selectedIndex != 0){
            var y=document.mainform.select_add_yr.value;
            var m=document.mainform.select_add_mon.value;
            var d=document.mainform.select_add_day.value;
            if (isValidDate(y,m,d) != 0) {
                alert('付与年月日の検索が誤っています。\n' +y+'年'+m+'月'+d+'日は存在しません。' );
                return false;
            }
        }

	} else {
		alert('付与年月日の検索には、年月日または年月を指定してください');
		return false;
	}
	document.mainform.page.value = 0
	return true;
}

//ソート
function set_sort(id) {

	sort_key = document.mainform.sort.value;

	if (id == "EMP_ID") {
		sort_key = (sort_key == "1") ? "2" : "1";
	} else if (id == "EMP_NAME") {
		sort_key = (sort_key == "3") ? "4" : "3";
	} else if (id == "DEPT") {
		sort_key = (sort_key == "5") ? "6" : "5";
	}
	document.mainform.sort.value = sort_key;
	document.mainform.page.value = 0;
	document.mainform.submit();
}

//修正画面
function openEdit(emp_id, paid_hol_tbl_id, paid_hol_add_date, service_year, service_mon) {

	base_left = 20;
	base = 20;
	wx = 900;
	wy = 600;

	url = 'work_admin_paid_edit_summer.php?session=<? echo($session); ?>&emp_id='+emp_id+'&arg_tbl_id='+paid_hol_tbl_id+'&arg_add_date='+paid_hol_add_date+'&service_year='+service_year+'&service_mon='+service_mon;

	childwin = window.open(url , 'EditChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}

//日付の整合性チェック
function isValidDate(y,m,d){
    var di = new Date(y,m-1,d);

    if(di.getFullYear() == y && di.getMonth() == m-1 && di.getDate() == d){
        return 0;
    }

    return -1;
}

//有休付与レコード一括作成
function bulkData() {

    if ((!document.mainform.data_cnt)||(document.mainform.data_cnt.value==0)){
		alert('対象データがありません。検索を実行してください。');
		return false;
    }

    var y=document.mainform.select_add_yr2.value;
    var m=document.mainform.select_add_mon2.value;
    var d=document.mainform.select_add_day2.value;
    if (isValidDate(y,m,d) != 0) {
        alert('付与年月日の指定が誤っています。\n' +y+'年'+m+'月'+d+'日は存在しません。' );
        return false;
    }

    var add_days_sel = document.getElementsByName("add_days_sel");
    for(var i=0;i<add_days_sel.length;i++) {    //radioボタンの場合、同じ名前のHTML要素が１つ以上存在する
        if (add_days_sel[i].checked && add_days_sel[i].value == '2') {    //
            days = document.mainform.add_days1.value;
            if (days == "" || !days.match(/^\d$|^\d+\.?\d+$/g)) {
                alert('付与日数には半角数字を入力してください。');
                return false;
            }
        }
    }

	if (confirm('指定された付与年月日の夏休付与情報がない場合に新規作成します。よろしいですか？')) {
		document.mainform.action = "work_admin_paid_summer_bulk.php";
		document.mainform.submit();
	} else {
		return false;
	}
}

//クリア
function clearAddYmd() {
    document.mainform.select_add_yr.value = "-";
    document.mainform.select_add_mon.value = "-";
    document.mainform.select_add_day.value = "-";
}
//当日設定
function setTodayAddYmd() {
	today = new Date();
	yy = today.getFullYear();
	mm = today.getMonth() + 1;
	dd = today.getDate();
	if (mm < 10) { mm = "0" + mm; }
	if (dd < 10) { dd = "0" + dd; }

    document.mainform.select_add_yr.value = yy;
    document.mainform.select_add_mon.value = mm;
    document.mainform.select_add_day.value = dd;
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#f6f9ff">
        <td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>">
          <img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>">
          <b>勤務管理</b></a></font></td>
      </tr>
    </table>
<? } else { ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#f6f9ff">
        <td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>">
          <img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
        <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>">
          <b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
        <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>">
          <b>ユーザ画面へ</b></a></font></td>
      </tr>
    </table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
  </tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
//サブメニュー表示
show_work_admin_submenu($session,$fname,$arr_option);
?>
  <img src="img/spacer.gif" alt="" width="1" height="5"><br>
  <form name="mainform" action="work_admin_paid_summer.php" method="post">

<table width="1000" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td>
  <table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
	  <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
	  <td><input type="text" name="srch_id" value="<? echo($srch_id); ?>" size="15" maxlength="12" style="ime-mode:inactive;"></td>
	</tr>
	<tr height="22">
	  <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
	  <td><input type="text" name="srch_name" value="<? echo($srch_name); ?>" size="25" maxlength="50" style="ime-mode:active;"></td>
	</tr>
	<tr height="22">
	  <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">属性</font></td>
	  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="cls" onchange="classOnChange();">
		<option value="-">----------
<?
        foreach ($arr_cls as $tmp_cls) {
            $tmp_id = $tmp_cls["id"];
            $tmp_name = $tmp_cls["name"];
            echo("<option value=\"$tmp_id\"");
            if ($tmp_id == $cls) {
                echo(" selected");
            }
            echo(">$tmp_name");
        }
?>
		</select><? echo($arr_class_name[0]); ?><br>
		<select name="atrb" onchange="atrbOnChange();">
		</select><? echo($arr_class_name[1]); ?><br>
		<select name="dept">
		</select><? echo($arr_class_name[2]); ?>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;グループ
        <select name="group_id">
        <option value="-">----------</option>
<?
        foreach ($group_names as $tmp_group_id => $group_name) {
            echo("<option value=\"$tmp_group_id\"");
            if ($tmp_group_id == $group_id) {
                echo(" selected");
            }
            echo(">$group_name\n");
        }
?>
        </select></font>
      </td>
	</tr>
	<tr height="22">
	  <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">付与年月日</font></td>
	  <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="select_add_yr"><? show_select_years(2, $select_add_yr, true, true); ?></select>年
		<select name="select_add_mon"><? show_select_months($select_add_mon, true); ?></select>月
		<select name="select_add_day"><? show_select_days($select_add_day, true); ?></select>日
		</font>
		<input type="button" value="クリア" onclick="clearAddYmd();";>
		<input type="button" value="当日" onclick="setTodayAddYmd();";>
	  </td>
	</tr>
  </table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2" align="right"><input type="submit" value="検索" onclick="return chkPara();"></td>
</tr>
</table>
</td>

<td width="100">
  <table  width="100" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
      </td>
    </tr>
  </table>
</td>

<td width="440">
  <table  width="440" border="0" cellspacing="0" cellpadding="2" class="list">
    <tr>
	  <td align="right" bgcolor="#f6f9ff" style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">付与手順</td>
      <td style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">検索データ表示後、『付与年月日』･『付与日数』を指定し<BR>『夏休付与レコード一括作成』ボタンを押して下さい。<br>検索結果一覧全データに対して付与データが作成されます。<br>※個別の休暇付与は一覧データ『職員ID』から付与できます。
      </td>
    </tr>
	<tr height="22">
	  <td align="right" bgcolor="#f6f9ff" style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">付与年月日</td>
	  <td style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">
		<select name="select_add_yr2"><? show_select_years_span(date("Y")-1, date("Y")+1, date("Y"), $asc = false); ?></select>年
		<select name="select_add_mon2"><? show_select_months(date("m"), false); ?></select>月
		<select name="select_add_day2"><? show_select_days(date("d"), false); ?></select>日
	  </td>
	</tr>
    <tr height="22">
      <td align="right" bgcolor="#f6f9ff" style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">付与日数</td>
   	  <td style="font-size:13px;font-family:ＭＳ Ｐゴシック,Osaka;">
        <input name="add_days_sel" value="1" type="radio" id="radio1" checked="checked"><label for="radio1">夏休表</label>&nbsp;&nbsp;
        <input name="add_days_sel" value="2" type="radio" id="radio2"><label for="radio2">任意指定</label>&nbsp;
        <input type="text" name="add_days1" value="<? echo($add_days1); ?>" size="3" maxlength="3" style="ime-mode:disabled;">日
      </td>
    </tr>
    <tr>
      <td align="right" bgcolor="#f6f9ff"></td>
      <td align="center"><input type="button" value="夏休付与レコード一括作成" onclick="bulkData();"></td>
    </tr>
  </table>
</td>

</tr>
</table>

<?
	$select_add_date = $select_add_yr.$select_add_mon.$select_add_day;

    show_navi($emp_cnt, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort);

    // 一覧表示
    get_emp_list_data($con, $fname, $sel_emp, $arr_paid_hol, $year, $atdbk_common_class, $sort, $select_add_date);

    show_navi($emp_cnt, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort);
?>
    <input type="hidden" name="session" value="<? echo($session); ?>">
    <input type="hidden" name="srch_flg" value="1">
    <input type="hidden" name="page" value="<? echo($page); ?>">
    <input type="hidden" name="year" value="<? echo($year); ?>">
    <input type="hidden" name="start_year" value="<? echo($start_year); ?>">
    <input type="hidden" name="sort" value="<? echo($sort); ?>">
    </form>
    </td>
    </tr>
    </table>

</body>
<? pg_close($con); ?>
</html>
<?

/**
 * ページ切り替えリンクを表示
 *
 * @param string $num 職員数
 * @param string $limit 1ページあたり件数
 * @param string $page ページ数
 * @param string $year 処理年度
 * @param string $start_year リンク開始年
 * @param string $default_start_year デフォルト開始年
 * @param string $fname プログラム名
 * @param string $sort ソート順
 * @return なし
 *
 */
function show_navi($num, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort) {

    global $session, $url_srch_name, $cls, $atrb, $dept, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $srch_id;

    $url_search_cond = "&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&select_add_yr=$select_add_yr&select_add_mon=$select_add_mon&select_add_day=$select_add_day&sort=$sort&srch_id=$srch_id";

	$total_page = ceil($num/$limit);
	$check_next = $num % $limit;
	$check_last = $total_page - 1;

    echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");
	echo("<table width=\"1040\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	//年
	$cnt_per_page = 7;
	echo("<td width=\"260\">\n");
	echo("&nbsp;</td>\n");
	echo("<td align=\"left\" width=\"380\">\n");
	if ($total_page >= 2) {
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("ページ");
		echo("&nbsp;");
		echo("</font>");

		if ($page > 0) {
			$back = $page - 1;
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
            echo("<a href=\"$fname?session=$session$url_search_cond&page=$back&srch_flg=1&start_year={$start_year}&year={$year}\">←</a></font>");
		}

		for ($i = 0; $i < $total_page; $i++) {
			$j = $i + 1;
			if ($page != $i) {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
                echo("<a href=\"$fname?session=$session$url_search_cond&page=$i&srch_flg=1&start_year={$start_year}&year={$year}\">$j</a> </font>");
			} else {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("［{$j}］");
				echo("</font>");
			}
		}

		if ($check_last != $page) {
			$next = $page + 1;
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
            echo("<a href=\"$fname?session=$session$url_search_cond&page=$next&srch_flg=1&start_year={$start_year}&year={$year}\">→</a></font>");
		}

	}
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

?>
