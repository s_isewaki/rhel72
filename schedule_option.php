<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_schedule_chart.ini");
require_once("show_select_values.ini");
require_once("show_facility.ini");
require_once("show_class_name.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// オプション設定情報を取得
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$schedule1 = pg_fetch_result($sel, 0, "schedule1_default");
$schedule2 = pg_fetch_result($sel, 0, "schedule2_default");
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
$time_chart_start = pg_fetch_result($sel, 0, "time_chart_start1");
$time_chart_end = pg_fetch_result($sel, 0, "time_chart_end1");
$pub_limit = pg_fetch_result($sel, 0, "schd_pub_limit");

// 時刻を時と分に分割
$time_chart_start_hour = substr($time_chart_start, 0, 2);
$time_chart_start_min = substr($time_chart_start, 2, 2);
$time_chart_end_hour = substr($time_chart_end, 0, 2);
$time_chart_end_min = substr($time_chart_end, 2, 2);

$arr_class_name = get_class_name_array($con, $fname);

// 公開範囲用のグループ一覧を作成
$pub_limit_group = array();
$sql = "select m.group_id, m.group_nm from comgroupmst m";
$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$emp_id') order by m.group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $pub_limit_group["c" . $row["group_id"]] = $row["group_nm"];
}

if ($pub_limit != "") {
    switch (substr($pub_limit, 0, 1)) {
        case "d":
            $pub_limit_dept = substr($pub_limit, 1);
            $pub_limit = "d";
            break;
        case "c":
            if (isset($pub_limit_group[$pub_limit])) {
                $pub_limit_group_id = $pub_limit;
                $pub_limit = "g";
            }
            break;
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | オプション</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkEndMinute() {
    if (document.mainform.time_chart_end_hour.value == '24' &&
        document.mainform.time_chart_end_min.value != '00') {
        document.mainform.time_chart_end_min.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイグループ</font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
</form>

<form action="schedule_option_insert.php" name="mainform">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>初期画面設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">スケジュール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schedule1" value="1"<? if ($schedule1 == "1") {echo " checked";} ?>>日
<input type="radio" name="schedule1" value="2"<? if ($schedule1 == "2") {echo " checked";} ?>>週
<input type="radio" name="schedule1" value="3"<? if ($schedule1 == "3") {echo " checked";} ?>>月
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schedule2" value="1"<? if ($schedule2 == "1") {echo " checked";} ?>>日
<input type="radio" name="schedule2" value="2"<? if ($schedule2 == "2") {echo " checked";} ?>>週
<input type="radio" name="schedule2" value="3"<? if ($schedule2 == "3") {echo " checked";} ?>>月
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>カレンダー設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週の開始曜日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="calendar_start" value="1"<? if ($calendar_start == "1") {echo(" checked");} ?>>日曜
<input type="radio" name="calendar_start" value="2"<? if ($calendar_start == "2") {echo(" checked");} ?>>月曜
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>タイムチャート設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="time_chart_start_hour">
<? show_hour_options_0_23($time_chart_start_hour, false); ?>
</select>：<select name="time_chart_start_min">
<? show_select_min_30($time_chart_start_min); ?>
</select>〜<select name="time_chart_end_hour" onchange="checkEndMinute();">
<? show_select_hrs_by_args(0, 24, $time_chart_end_hour, false); ?>
</select>：<select name="time_chart_end_min" onchange="checkEndMinute();">
<? show_select_min_30($time_chart_end_min); ?>
</select>
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>予定の公開設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
echo("<input type=\"radio\" name=\"pub_limit\" value=\"\"");
if ($pub_limit == "") echo(" checked");
echo(">全て\n");

echo("<input type=\"radio\" name=\"pub_limit\" value=\"d\"");
if ($pub_limit == "d") echo(" checked");
echo(">所属 ");
echo("<select name=\"pub_limit_dept\" onchange=\"this.form.pub_limit[1].checked = true;\">\n");
    echo("<option value=\"1\"");
    if ($pub_limit_dept == "1") echo(" selected");
    echo(">" . $arr_class_name["class_nm"] . "\n");

    echo("<option value=\"2\"");
    if ($pub_limit_dept == "2") echo(" selected");
    echo(">" . $arr_class_name["atrb_nm"] . "\n");

    echo("<option value=\"3\"");
    if ($pub_limit_dept == "3") echo(" selected");
    echo(">" . $arr_class_name["dept_nm"] . "\n");

    if ($arr_class_name["class_cnt"] == 4) {
        echo("<option value=\"4\"");
        if ($pub_limit_dept == "4") echo(" selected");
        echo(">" . $arr_class_name["room_nm"] . "\n");
    }
echo("</select>\n");

if (count($pub_limit_group) > 0) {
    echo("<input type=\"radio\" name=\"pub_limit\" value=\"g\"");
    if ($pub_limit == "g") echo(" checked");
    echo(">");
    echo("<select name=\"pub_limit_group_id\" onchange=\"this.form.pub_limit[2].checked = true;\">\n");
    foreach ($pub_limit_group as $tmp_pub_limit_group_id => $tmp_pub_limit_group_nm) {
        echo("<option value=\"$tmp_pub_limit_group_id\"");
        if ($pub_limit_group_id == $tmp_pub_limit_group_id) echo(" selected");
        echo(">" . $tmp_pub_limit_group_nm . "\n");
    }
    echo("</select>\n");
}
?>
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="date" value="<?php eh($date); ?>">
</form>
</td>
</tr>
</table>
<? pg_close($con); ?>
</body>
</html>
