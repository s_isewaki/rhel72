<?
/*
項目を選択可能としたためこのプログラムは未使用
work_admin_csv_donwload.phpが新しいプログラムになります。
*/

ob_start();
ini_set("max_execution_time", 0);

require("about_session.php");
require("about_authority.php");
require("show_timecard_common.ini");
require("show_attendance_pattern.ini");
require("holiday.php");
require("timecard_common_class.php");
require_once("atdbk_common_class.php");
require_once("date_utils.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 締め日を取得
// ************ oose update start *****************
//$sql = "select closing from timecard";
$sql = "select closing, closing_parttime from timecard";
// ************ oose update end *****************
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
// ************ oose add start *****************
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
// ************ oose add end *****************

// 締め日が未登録の場合はエラーとする
// ************ oose update start *****************
//if ($closing == "") {
if (($closing == "") || ($closing_parttime == "")) {
// ************ oose update end *****************
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}

$org_yyyymm = $yyyymm;
while (true) {
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	if (substr($start_date, 0, 6) == $org_yyyymm) {
		break;
	}
	$tmp_month_y = $year;
	$tmp_month_m = $month + 1;
	if ($tmp_month_m == 13) {
		$tmp_month_y++;
		$tmp_month_m = 1;
	}
	$yyyymm = $tmp_month_y . sprintf("%02d", $tmp_month_m);
}
// ************ oose add start *****************
//非常勤の締め日より開始／終了日算出
while (true) {
	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
	}

	$last_month_y = $year;
	$last_month_m = $month - 1;
	if ($last_month_m == 0) {
		$last_month_y--;
		$last_month_m = 12;
	}
	$last_yyyymm = $last_month_y . sprintf("%02d", $last_month_m);

	$next_month_y = $year;
	$next_month_m = $month + 1;
	if ($next_month_m == 13) {
		$next_month_y++;
		$next_month_m = 1;
	}
	$next_yyyymm = $next_month_y . sprintf("%02d", $next_month_m);

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing_parttime) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}

			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;

			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
	$start_date_parttime = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date_parttime = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

	if (substr($start_date_parttime, 0, 6) == $org_yyyymm) {
		break;
	}
	$tmp_month_y = $year;
	$tmp_month_m = $month + 1;
	if ($tmp_month_m == 13) {
		$tmp_month_y++;
		$tmp_month_m = 1;
	}
	$yyyymm = $tmp_month_y . sprintf("%02d", $tmp_month_m);
}
// ************ oose add end *****************

// タイムカード設定情報を取得
$sql = "select ovtmscr_tm from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
}
if ($ovtmscr_tm == "") {$ovtmscr_tm = 0;}

// 対象年月度を変数にセット
$c_yyyymm = substr($start_date, 0, 6);

// 事由ID、表示フラグの配列
$arr_reason_id = $atdbk_common_class->get_add_disp_reason_id();
$arr_disp_flg = array();
for ($i=0; $i<count($arr_reason_id); $i++) {
	$arr_disp_flg[$i] = $atdbk_common_class->get_reason_display_flag($arr_reason_id[$i]);
}

//グループ指定group_idの有無により見出し出力を制御
// 指定あり、所属グループ分は見出しあり、応援追加は見出しなし
// 指定なし、どちらも見出しなし

// 月集計情報をCSV形式で取得
// ************ oose update start *****************
//$csv = get_attendance_book_csv($con, $emp_id_list, $start_date, $end_date, $c_yyyymm, $ovtmscr_tm, $fname);
$csv = get_attendance_book_csv($con, $emp_id_list,
			$start_date, $end_date,
			$start_date_parttime, $end_date_parttime,
			$c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
			$group_id, $atdbk_common_class);
// ************ oose update end *****************
//echo $csv;

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = sprintf("%04d%02d", $start_year, $start_month) . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 月集計情報をCSV形式で取得
// ************ oose update start *****************
//function get_attendance_book_csv($con, $emp_id_list, $start_date, $end_date, $c_yyyymm, $ovtmscr_tm, $fname) {
function get_attendance_book_csv($con, $emp_id_list,
			$start_date_fulltime, $end_date_fulltime,
			$start_date_parttime, $end_date_parttime,
			$c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
			$group_id, $atdbk_common_class) {
// ************ oose update end *****************

	//グループ毎のテーブルから勤務時間を取得する
	$timecard_common_class = new timecard_common_class($con, $fname);

//	$buf = "職員ID,職員氏名,要勤務日数,出勤日数,休日出勤,代出,振出,代休,振休,有休,欠勤,その他,遅刻,早退,基準時間,稼働時間,残業時間,深夜勤務,当直(平日),当直(土曜),当直(休日)";
	$buf = "職員ID,職員氏名,要勤務日数,出勤日数,休日出勤,代出,振出,代休,振休,年休・有休,欠勤,その他,遅刻,早退,基準時間,稼働時間,残業時間,深夜勤務,法定内勤務,法定内残業,法定外残業,休日勤務";
	// 表示用名称の配列
//	$arr_reason_name = array("公休", "特休", "産休", "育休", "介休", "傷休", "学休", "忌引", "夏休", "結休", "リフレッシュ", "初盆休暇");
	$arr_reason_name = $atdbk_common_class->get_add_disp_reason_name2();

	// 追加事由
	for ($i=0; $i<count($arr_reason_id); $i++) {
		if ($arr_disp_flg[$i] == "t") {
			$wk_str = str_replace("<br>", "", $arr_reason_name[$i]);
 			$buf .= ",".$wk_str;
		} 
	}

	// 勤務シフト
	$buf .= ",シフトグループ名";
	// グループ指定あり、名前を取得
	if ($group_id != "-") {
		$arr_atdptn_nm = get_arr_atdptn_nm($con, $fname, $group_id);
		for ($i=0; $i<count($arr_atdptn_nm); $i++) {
			$buf .= ",".$arr_atdptn_nm[$i];
		}
	} else {
	// グループ指定なし、最大のシフト項目数を取得
		$max_cnt = get_max_cnt_atdptn_id($con, $fname);
		for ($i=0; $i<$max_cnt; $i++) {
			$buf .= ",";
		}
	}
	$buf .= ",当直(平日),当直(土曜),当直(休日)";

	$arr_allow = get_allow($con, $fname);

	foreach($arr_allow as $allow)
	{
		$allow_contents = $allow["allow_contents"];
		$buf .= ",手当(".$allow_contents.")";
	}

	$buf .= "\r\n";

	// 端数処理情報を取得
	$sql = "select * from timecard";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = pg_fetch_result($sel, 0, "fraction$i");
			$$var_name_min = pg_fetch_result($sel, 0, "fraction{$i}_min");
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = pg_fetch_result($sel, 0, "others$i");
		}
		$rest1 = intval(pg_fetch_result($sel, 0, "rest1"));
		$rest2 = intval(pg_fetch_result($sel, 0, "rest2"));
		$rest3 = intval(pg_fetch_result($sel, 0, "rest3"));

        $night_workday_count = pg_fetch_result($sel, 0, "night_workday_count");
	} else {
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "fraction$i";
			$var_name_min = "fraction{$i}_min";
			$$var_name = "";
			$$var_name_min = "";
		}
		for ($i = 1; $i <= 5; $i++) {
			$var_name = "others$i";
			$$var_name = "";
		}
		$rest1 = 0;
		$rest2 = 0;
		$rest3 = 0;

        $night_workday_count = "";
	}

	// 指定範囲の勤務日種別を取得
	if ($start_date_fulltime <= $start_date_parttime) {
		$wk_start_date = $start_date_fulltime;
	} else {
		$wk_start_date = $start_date_parttime;
	}
	if ($end_date_fulltime >= $end_date_parttime) {
		$wk_end_date = $end_date_fulltime;
	} else {
		$wk_end_date = $end_date_parttime;
	}
	$wd = date("w", to_timestamp($wk_start_date));
	while ($wd != 0) {
		$wk_start_date = last_date($wk_start_date);
		$wd = date("w", to_timestamp($wk_start_date));
	}

	$arr_type = array();
	$sql = "select date, type from calendar";
	$cond = "where date >= '$wk_start_date' and date <= '$wk_end_date'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$date = $row["date"];
		$arr_type[$date] = $row["type"];
	}
	// 通常勤務日の労働時間を取得
	$sql = "select day1_time from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day1_time = pg_fetch_result($sel, 0, "day1_time");
	}
	else{
		$day1_time = "";
	}


	// 選択された全職員をループ
	$emp_ids = explode(",", $emp_id_list);
	foreach ($emp_ids as $tmp_emp_id) {

// ************ oose add start *****************
		// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
		// 給与支給区分・祝日計算方法、残業管理を取得
		$sql = "select duty_form, wage, hol_minus, no_overtime from empcond";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$duty_form = pg_fetch_result($sel, 0, "duty_form");
			$wage = pg_fetch_result($sel, 0, "wage");
			$hol_minus = pg_fetch_result($sel, 0, "hol_minus");
			$no_overtime = pg_fetch_result($sel, 0, "no_overtime");
		} else {
			$duty_form = "";
			$wage = "";
			$hol_minus = "";
			$no_overtime = "";
		}
//		$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
		if ($duty_form == 2) {
			//非常勤
			$c_yyyymm = substr($start_date_parttime, 0, 6);
			$start_date = $start_date_parttime;
			$end_date = $end_date_parttime;
		} else {
			//常勤
			$c_yyyymm = substr($start_date_fulltime, 0, 6);
			$start_date = $start_date_fulltime;
			$end_date = $end_date_fulltime;
		}
// ************ oose add end *****************
/*
		// 出勤グループを取得
		$tmcd_group_id = get_timecard_group_id($con, $tmp_emp_id, $fname);

		// 出勤パターン×勤務日種別の時間帯情報を配列に格納
		$officehours = array();
		for ($i = 1; $i <= 15; $i++) {
			$officehours["$i"] = array();
			for ($j = 1; $j <= 3; $j++) {
				$officehours["$i"]["$j"] = array();
				for ($k = 1; $k <= 5; $k++) {
					$officehours["$i"]["$j"]["start$k"] = "";
					$officehours["$i"]["$j"]["end$k"] = "";
				}
			}
		}
		$sql = "select * from officehours$tmcd_group_id";
		$cond = "order by pattern, officehours_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			for ($i = 1; $i <= 5; $i++) {
				$pattern = $row["pattern"];
				$id = $row["officehours_id"];
				$officehours[$pattern][$id]["start$i"] = $row["officehours{$i}_start"];
				$officehours[$pattern][$id]["end$i"] = $row["officehours{$i}_end"];
			}
		}
*/
		// 職員ID・職員氏名を取得
		$sql = "select emp_personal_id, emp_lt_nm, emp_ft_nm from empmst";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");
//		if ($tmp_emp_personal_id == "") {
//			$tmp_emp_personal_id = "&nbsp;";
//		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");

		// 変則労働適用情報を配列で取得
		$arr_irrg_info = get_irrg_info_array($con, $tmp_emp_id, $fname);

		// 対象年月の締め状況を取得
		$sql = "select count(*) from atdbkclose";
		$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_closed = (pg_fetch_result($sel, 0, 0) > 0);

		// 集計結果格納用配列を初期化
		$idx = 21 + count($arr_allow);
		$sums = array_fill(0, $idx, 0);

		// 追加事由分
		$add_sums = array_fill(0, count($arr_reason_id), 0);

		$hotei_sums = array_fill(0, 4, 0);

		// 締め未済の場合
		if (!$tmp_closed) {

			// 全日付をループ（変則労働時間・週の場合、週の開始曜日からスタート）
			$tmp_date = $start_date;

			$wd = date("w", to_timestamp($tmp_date));
			if ($arr_irrg_info["irrg_type"] == "1") {
				while (($arr_irrg_info["irrg_wd"] == "1" && $wd != 0) || ($arr_irrg_info["irrg_wd"] == "2" && $wd != 1)) {
					$tmp_date = last_date($tmp_date);
					$wd = date("w", to_timestamp($tmp_date));
				}
			} else {
			//変則労働時間・週以外の場合、日曜から
				while ($wd != 0) {
					$tmp_date = last_date($tmp_date);
					$wd = date("w", to_timestamp($tmp_date));
				}
			}
			$counter_for_week = 1;
			$holiday_count = 0;

			//データ検索用開始日
			$wk_start_date = $tmp_date;

			// 処理日付の勤務予定情報を取得
			$sql =	"SELECT ".
						"atdbk.pattern, ".
						"atdbk.reason, ".
						"atdbk.night_duty, ".
						"atdbk.prov_start_time, ".
						"atdbk.prov_end_time, ".
						"atdbk.tmcd_group_id, ".
						"atdptn.workday_count ".
					"FROM ".
						"atdbk ".
							"LEFT JOIN atdptn ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ";
			$cond = "where emp_id = '$tmp_emp_id' and ";
			$cond .= " date >= '$wk_start_date' and date <= '$end_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
				$arr_atdbk[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
				$arr_atdbk[$date]["pattern"] = $row["pattern"];
				$arr_atdbk[$date]["reason"] = $row["reason"];
				$arr_atdbk[$date]["night_duty"] = $row["night_duty"];
				$arr_atdbk[$date]["allow_id"] = $row["allow_id"];
				$arr_atdbk[$date]["prov_start_time"] = $row["prov_start_time"];
				$arr_atdbk[$date]["prov_end_time"] = $row["prov_end_time"];
				$arr_atdbk[$date]["workday_count"] = $row["workday_count"];
			}

			// 処理日付の勤務実績を取得
			$sql =	"SELECT ".
						"atdbkrslt.*, ".
						"atdptn.workday_count, ".
						"ovtmapply.apply_id     AS ovtm_apply_id, ".
						"ovtmapply.apply_status AS ovtm_apply_status, ".
						"COALESCE(atdbk_reason_mst.day_count, 0) AS reason_day_count ".
					"FROM ".
						"atdbkrslt ".
							"LEFT JOIN atdptn ON atdbkrslt.tmcd_group_id = atdptn.group_id AND atdbkrslt.pattern = CAST(atdptn.atdptn_id AS varchar) ".
							"LEFT JOIN ovtmapply ON atdbkrslt.emp_id = ovtmapply.emp_id AND CAST(atdbkrslt.date AS varchar) = ovtmapply.target_date AND ovtmapply.delete_flg = false AND ovtmapply.re_apply_id IS NULL ".
							"LEFT JOIN atdbk_reason_mst ON atdbkrslt.reason = atdbk_reason_mst.reason_id ";
			$cond = "where atdbkrslt.emp_id = '$tmp_emp_id' and ";
			$cond .= " atdbkrslt.date >= '$wk_start_date' and atdbkrslt.date <= '$end_date'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			while ($row = pg_fetch_array($sel)) {
				$date = $row["date"];
				$arr_atdbkrslt[$date]["pattern"] = $row["pattern"];
				$arr_atdbkrslt[$date]["reason"] = $row["reason"];
				$arr_atdbkrslt[$date]["night_duty"] = $row["night_duty"];
				$arr_atdbkrslt[$date]["allow_id"] = $row["allow_id"];
				$arr_atdbkrslt[$date]["start_time"] = $row["start_time"];
				$arr_atdbkrslt[$date]["end_time"] = $row["end_time"];
				$arr_atdbkrslt[$date]["out_time"] = $row["out_time"];
				$arr_atdbkrslt[$date]["ret_time"] = $row["ret_time"];
				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					$end_var = "o_end_time$i";
					$arr_atdbkrslt[$date][$start_var] = $row[$start_var];
					$arr_atdbkrslt[$date][$end_var] = $row[$end_var];
				}
				$arr_atdbkrslt[$date]["tmcd_group_id"] = $row["tmcd_group_id"];
				$arr_atdbkrslt[$date]["meeting_time"] = $row["meeting_time"];
				$arr_atdbkrslt[$date]["workday_count"] = $row["workday_count"];
				$arr_atdbkrslt[$date]["previous_day_flag"] = $row["previous_day_flag"];
				$arr_atdbkrslt[$date]["next_day_flag"] = $row["next_day_flag"];
				$arr_atdbkrslt[$date]["ovtm_apply_id"] = $row["ovtm_apply_id"];
				$arr_atdbkrslt[$date]["ovtm_apply_status"] = $row["ovtm_apply_status"];
				$arr_atdbkrslt[$date]["reason_day_count"] = $row["reason_day_count"];
			}

			//法定内勤務、法定内残業、法定外残業
			$hoteinai = 0;
			$hoteinai_zan = 0;
			$hoteigai = 0;

			//休日出勤テーブル 2008/10/23
			$arr_hol_work_date = array();
			while ($tmp_date <= $end_date) {

				if ($counter_for_week >= 8) {
					$counter_for_week = 1;
				}
				if ($counter_for_week == 1) {
					//前日までの計
					$last_day_total = 0;
					$work_time_for_week = 0;
					if ($arr_irrg_info["irrg_type"] == "1") {
						$holiday_count = 0;
					}
				}
				$work_time = 0;
				$wk_return_time = 0;

				$holiday_name = get_holiday_name($tmp_date);
				if ($holiday_name != "") {
					$holiday_count++;
				}

				// 処理日付の勤務日種別を取得
				$type = $arr_type[$tmp_date];

				// 処理日付の勤務予定情報を取得
				$prov_tmcd_group_id = $arr_atdbk[$tmp_date]["tmcd_group_id"];
				$prov_pattern = $arr_atdbk[$tmp_date]["pattern"];
				$prov_reason = $arr_atdbk[$tmp_date]["reason"];
				$prov_night_duty = $arr_atdbk[$tmp_date]["night_duty"];
				$prov_allow_id = $arr_atdbk[$tmp_date]["allow_id"];

				$prov_start_time = $arr_atdbk[$tmp_date]["prov_start_time"];
				$prov_end_time = $arr_atdbk[$tmp_date]["prov_end_time"];
				$prov_start_time = ($prov_start_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_start_time) : "";
				$prov_end_time =  ($prov_end_time != "") ? preg_replace("/(\d{2})(\d{2})/", "$1:$2", $prov_end_time) : "";
				$prov_workday_count = $arr_atdbk[$tmp_date]["workday_count"];

				if ($prov_workday_count == "") {
					$prov_workday_count = 0;
				}
				// 要勤務日数をカウント
				if ($tmp_date >= $start_date) {
					// カレンダーの属性を元にカウントする 2008/10/21
					$sums[0] += $timecard_common_class->get_you_kinmu_nissu($type);
/*
					// 予定が未入力でも「休暇」でもなければカウント
					if ($prov_pattern != "" && $prov_pattern != "10") {

					// 当直あり
					if($prov_night_duty == "1")
					{
						//曜日に対応したworkday_countを取得する
						$prov_workday_count = $prov_workday_count + $timecard_common_class->get_prov_night_workday_count($tmp_date);
					}
					$sums[0] = $sums[0] + $prov_workday_count;
					}
*/
				}

				// 処理日付の勤務実績を取得
				$pattern = $arr_atdbkrslt[$tmp_date]["pattern"];
				$reason = $arr_atdbkrslt[$tmp_date]["reason"];
				$night_duty = $arr_atdbkrslt[$tmp_date]["night_duty"];
				$allow_id = $arr_atdbkrslt[$tmp_date]["allow_id"];
				$start_time = $arr_atdbkrslt[$tmp_date]["start_time"];
				$end_time = $arr_atdbkrslt[$tmp_date]["end_time"];
				$out_time = $arr_atdbkrslt[$tmp_date]["out_time"];
				$ret_time = $arr_atdbkrslt[$tmp_date]["ret_time"];

				for ($i = 1; $i <= 10; $i++) {
					$start_var = "o_start_time$i";
					$end_var = "o_end_time$i";
					$$start_var = $arr_atdbkrslt[$tmp_date]["$start_var"];
					$$end_var = $arr_atdbkrslt[$tmp_date]["$end_var"];

					if ($$start_var != "") {
						$$start_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$start_var);
					}
					if ($$end_var != "") {
						$$end_var = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $$end_var);
					}
				}

				$tmcd_group_id = $arr_atdbkrslt[$tmp_date]["tmcd_group_id"];
				$meeting_time = $arr_atdbkrslt[$tmp_date]["meeting_time"];

				$rslt_workday_count = $arr_atdbkrslt[$tmp_date]["workday_count"];

				$previous_day_flag = $arr_atdbkrslt[$tmp_date]["previous_day_flag"];
				$next_day_flag = $arr_atdbkrslt[$tmp_date]["next_day_flag"];

				$ovtm_apply_id = $arr_atdbkrslt[$tmp_date]["ovtm_apply_id"];
				$ovtm_apply_status = $arr_atdbkrslt[$tmp_date]["ovtm_apply_status"];
				$reason_day_count = $arr_atdbkrslt[$tmp_date]["reason_day_count"];

				if ($rslt_workday_count == "") {
					$rslt_workday_count = 0;
				}
				if ($previous_day_flag == "") {
					$previous_day_flag = 0;
				}
				if ($next_day_flag == "") {
					$next_day_flag = 0;
				}
				if ($reason_day_count == "") {
					$reason_day_count = 0;
				}

				// 残業表示用に退勤時刻を退避
//				$show_end_time = $end_time;

				//当直の有効判定
				$night_duty_flag = $timecard_common_class->is_valid_duty($night_duty, $tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $tmcd_group_id);

				if ($tmp_date >= $start_date) {

					// 出勤日数をカウント（条件：出勤の打刻あり）
					if ($start_time != "") {

						if($night_duty_flag)
						{
							//曜日に対応したworkday_countを取得する
							$rslt_workday_count = $rslt_workday_count + $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
						}
						$sums[1] = $sums[1] + $rslt_workday_count;
					}

					// 事由ベースの日数計算
					switch ($reason) {
//					case "16":  // 休日出勤
//						$sums[2] += $reason_day_count;
//						break;
					case "14":  // 代替出勤
						$sums[3] += $reason_day_count;
						break;
					case "15":  // 振替出勤
						$sums[4] += $reason_day_count;
						break;
					case "4":  // 代替休暇
					case "18":  // 半前代替休
					case "19":  // 半後代替休
						$sums[5] += $reason_day_count;
						break;
					case "17":  // 振替休暇
					case "20":  // 半前振替休
					case "21":  // 半後振替休
						$sums[6] += $reason_day_count;
						break;
					case "1":  // 有休休暇
					case "2":  // 午前有休
					case "3":  // 午後有休
					case "37": // 年休
					case "38": // 午前年休
					case "39": // 午後年休
						$sums[7] += $reason_day_count;
						break;
					case "6":  // 一般欠勤
					case "7":  // 病傷欠勤
						$sums[8] += $reason_day_count;
						break;
					case "8":  // その他休
						$sums[9] += $reason_day_count;
						break;
					case "24":  // 公休
					case "35":  // 午前公休
					case "36":  // 午後公休
					case "22":  // 法定休暇
					case "23":  // 所定休暇
						$add_sums[0] += $reason_day_count;
						break;
					case "5":   // 特別休暇
						$add_sums[1] += $reason_day_count;
						break;
					case "25":  // 産休
						$add_sums[2] += $reason_day_count;
						break;
					case "26":  // 育児休業
						$add_sums[3] += $reason_day_count;
						break;
					case "27":  // 介護休業
						$add_sums[4] += $reason_day_count;
						break;
					case "28":  // 傷病休職
						$add_sums[5] += $reason_day_count;
						break;
					case "29":  // 学業休職
						$add_sums[6] += $reason_day_count;
						break;
					case "30":  // 忌引
						$add_sums[7] += $reason_day_count;
						break;
					case "31":   // 夏休
						$add_sums[8] += $reason_day_count;
						break;
					case "32":   // 結婚休
						$add_sums[9] += $reason_day_count;
						break;
					case "40":   // リフレッシュ休暇
					case "42":   // 午前リフレッシュ休暇
					case "43":   // 午後リフレッシュ休暇
						$add_sums[10] += $reason_day_count;
						break;
					case "41":  // 初盆休暇
						$add_sums[11] += $reason_day_count;
						break;
					}
				}

				// 各時間帯の開始時刻・終了時刻を変数に格納
				$tmp_type = ($type == "4" || $type == "5") ? "1" : $type;
/*
				for ($i = 1; $i <= 5; $i++) {
					$var_name_s = "start$i";
					$var_name_e = "end$i";
					$$var_name_s = $officehours[$pattern][$tmp_type]["start$i"];
					$$var_name_e = $officehours[$pattern][$tmp_type]["end$i"];
				}
*/
				$start2 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start2" );
				$end2 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end2" );
				$start4 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start4" );
				$end4 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end4" );
				$start5 = $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "start5" );
				$end5 =   $timecard_common_class->get_officehours_info( $tmcd_group_id, $pattern, $tmp_type, "end5" );

				// 所定労働時間帯が確定できない場合、予定時刻を所定労働時間帯として使用
				if (($start2 == "" || $end2 == "") && $prov_start_time != "" && $prov_end_time != "") {
					$start2 = $prov_start_time;
					$end2 = $prov_end_time;
				}

				// 残業情報を取得
				//ovtm_apply_statusからlink_type取得
				$night_duty_time_array  = array();
				$duty_work_time         = 0;
				if ($night_duty_flag){
					//当直時間の取得
					$night_duty_time_array = $timecard_common_class->get_duty_time_array($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);

					//日数換算の数値ｘ通常勤務日の労働時間の時間
					$duty_work_time = date_utils::hi_to_minute($day1_time) * $timecard_common_class->get_night_workday_count($tmp_date, $end_time, $next_day_flag, $tmcd_group_id);
				}
				$overtime_link_type = $timecard_common_class->get_link_type_overtime($tmp_date, $start_time, $previous_day_flag, $end_time, $next_day_flag, $night_duty, $start2, $end2, $ovtm_apply_status, $tmcd_group_id);
				$overtime_apply_id  = $ovtm_apply_id;

				// 所定労働・休憩開始終了日時の取得
				$start2_date_time = $tmp_date.$start2;
				$start4_date_time = $tmp_date.$start4;
				$end2_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start2, $end2);
				$end4_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $start4, $end4);

				//外出復帰日時の取得
				$out_date_time = $tmp_date.$out_time;
				$ret_date_time = $timecard_common_class->get_schedule_end_date_time($tmp_date, $out_time, $ret_time);

				// 時間帯データを配列に格納
				$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
				// 前日出勤フラグ、翌日退勤フラグを考慮した休憩時間配列を取得
				$arr_time4 = $timecard_common_class->get_minute_time_array2($start4_date_time, $end4_date_time, $previous_day_flag, $next_day_flag);  // 休憩

				// 処理当日の所定労働時間を求める
				if ($holiday_name != "" && $hol_minus == "t") {  // 祝日かつ祝日減算設定
					$specified_time = 0;
				} else if ($wage == "4" && $others3 == "2") {  // 時間給者かつ休憩を実働加算
					$specified_time = count($arr_time2);
				} else {
					$specified_time = count(array_diff($arr_time2, $arr_time4));
				}

// 基準時間の計算を変更 2008/10/21
				// 変則労働期間が月でない場合、所定労働時間を基準時間としてカウント
//				if ($tmp_date >= $start_date && $reason != "16") {
//					if ($arr_irrg_info["irrg_type"] != "2") {
//						$sums[14] += $specified_time;
						//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加
//						if ($night_duty_flag){
//							$sums[14] += $duty_work_time;
//						}
//					}
//				}

				// 出退勤とも打刻済みの場合
				if ($start_time != "" && $end_time != "") {

					// ここまでの処理で所定労働時間帯が未確定の場合、勤務実績で上書き
					$start_date_time = $timecard_common_class->get_timecard_start_date_time($tmp_date, $start_time, $previous_day_flag);
					$end_date_time   = $timecard_common_class->get_timecard_end_date_time($tmp_date, $end_time, $next_day_flag);
					if (empty($start2_date_time) || empty($end2_date_time)) {
						$start2_date_time = $start_date_time;
						$end2_date_time    = $end_date_time;
					}

					// 残業管理をする場合、（しない場合には退勤時刻を変えずに稼動時間の計算をする）
					if ($no_overtime != "t") {
						// 残業申請中の場合は定時退勤時刻を退勤時刻として計算
						if (!($overtime_link_type == "0" || $overtime_link_type == "3" || $overtime_link_type == "4")) {
	//						$end_time = $end2;
							$end_date_time = $end2_date_time;
						}
					}

					// 出勤〜退勤までの時間を配列に格納
					$arr_tmp_work_time = $timecard_common_class->get_minute_time_array($start_date_time, $end_date_time);

					//出勤同時刻退勤または、出勤時間＞退勤時間の場合は処理を行わない
					if (count($arr_tmp_work_time) > 0){
						// 時間帯データを配列に格納
						switch ($reason) {
						case "2":  // 午前有休
						case "38": // 午前年休
							$arr_time2 = $timecard_common_class->get_minute_time_array($start4_date_time, $end2_date_time);  // 休憩後の所定労働
							break;
						case "3":  // 午後有休
						case "39": // 午後年休
							$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end4_date_time);  // 休憩前の所定労働
							break;
						default:
							$arr_time2 = $timecard_common_class->get_minute_time_array($start2_date_time, $end2_date_time);  // 所定労働
							break;
						}
						//前日・翌日の深夜帯も追加する
						$arr_time5 = $timecard_common_class->get_late_night_shift_array($tmp_date, $start5, $end5);  // 深夜帯

						// 確定出勤時刻／遅刻回数を取得
						$start_time_info = $timecard_common_class->get_start_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction1, $fraction1_min, $fraction3, $others4, $others5);
						$fixed_start_time = $start_time_info["fixed_time"];
						//前日出勤の場合、時刻は前日時刻を設定 2008/10/23
						if ($previous_day_flag) {
							$fixed_start_time = $start_date_time;
						}
						if ($tmp_date >= $start_date) {
							$sums[10] += $start_time_info["diff_count"];
							$sums[12] += $start_time_info["diff_minutes"];
						}

						// 確定退勤時刻／早退回数を取得
						$end_time_info = $timecard_common_class->get_end_time_info($arr_tmp_work_time, $arr_time2, $arr_time4, $fraction2, $fraction2_min, $fraction3, $others4);
						$fixed_end_time = $end_time_info["fixed_time"];
						if ($tmp_date >= $start_date) {
							$sums[11] += $end_time_info["diff_count"];
							$sums[13] += $end_time_info["diff_minutes"];
						}

						// 実働時間・所定労働時間を配列に格納（休憩を除く）・有休時間を算出
						switch ($reason) {
						case "2":  // 午前有休
						case "38": // 午前年休
							if (in_array($fixed_start_time, $arr_time2)) {
								$tmp_start_time = $fixed_start_time;
							} else {
								$tmp_start_time = $end4_date_time;
							}
							$tmp_end_time = $fixed_end_time;
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

							$arr_specified_time = $arr_time2;

							$paid_time = date_utils::get_time_difference($start2_date_time, $start4_date_time);

							break;
						case "3":  // 午後有休
						case "39": // 午後年休
							$tmp_start_time = $fixed_start_time;
							if (in_array($fixed_end_time, $arr_time2)) {
								$tmp_end_time = $fixed_end_time;
							} else {
								$tmp_end_time = $start4_date_time;
							}
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);

							$arr_specified_time = $arr_time2;

							$paid_time = date_utils::get_time_difference($end4_date_time, $end2_date_time);

							break;
						default:
							$tmp_start_time = $fixed_start_time;
							$tmp_end_time = $fixed_end_time;
							$arr_effective_time = $timecard_common_class->get_minute_time_array($tmp_start_time, $tmp_end_time);
							if ($wage != "4") {
								$arr_effective_time = array_diff($arr_effective_time, $arr_time4);
							}

							$arr_specified_time = array_diff($arr_time2, $arr_time4);

							$paid_time = 0;

							break;
						}

						//休日勤務時間（分）と休日出勤を取得 2008/10/23
						if ($tmp_date >= $start_date) {
							$arr_hol_time = $timecard_common_class->get_holiday_work($arr_type, $tmp_date, $arr_effective_time, $start_time, $previous_day_flag, $end_time, $next_day_flag, $reason);
							$hotei_sums[3] += $arr_hol_time[3];

							// 休日出勤した日付を配列へ設定
							for ($i=4; $i<7; $i++) {
								if ($arr_hol_time[$i] != "") {
									$arr_hol_work_date[$arr_hol_time[$i]] = 1;
								}
							}
						}
						//当直の場合当直時間を計算から除外する
						if ($night_duty_flag){
							//稼働 - (当直 - 所定労働)
							$arr_effective_time = array_diff($arr_effective_time, array_diff($night_duty_time_array, $arr_time2));
						}

						// 普外時間を算出（分単位）
						$arr_out_time = $timecard_common_class->get_minute_time_array($out_date_time, $ret_date_time);
						$time3 = count(array_intersect($arr_out_time, $arr_specified_time));

						// 残外時間を算出（分単位）
						$time4 = count(array_diff($arr_out_time, $arr_specified_time));

						// 実働時間を算出（分単位）
						$effective_time = count($arr_effective_time);
						if ($wage == "4" && $others3 == "1") {  // 時間給かつ休憩除外
							if ($effective_time > 540) {  // 9時間を超える場合
								$effective_time -= $rest3;
								$time_rest = $rest3;
							} else if ($effective_time > 480) {  // 8時間を超える場合
								$effective_time -= $rest2;
								$time_rest = $rest2;
							} else if ($effective_time > 360) {  // 6時間を超える場合
								$effective_time -= $rest1;
								$time_rest = $rest1;
							}
							//開始した日が休日の場合に休日勤務から休憩を減算 2008/10/23
							if (($previous_day_flag == true && $arr_hol_time[0] > 0) ||
								($previous_day_flag == false && $arr_hol_time[1] > 0)) {
								$hotei_sums[3] = $hotei_sums[3] - $time_rest;
							}
						}

						// 代替出勤または振替出勤の場合、実働時間から所定労働時間を減算
						if ($reason == "14" || $reason == "15") {
							$effective_time -= count($arr_specified_time);
						}

						// 稼働時間を算出（分単位）
						$work_time = $effective_time + $paid_time - $time3 - $time4;
						// 退勤後復帰回数・時間を算出
						$return_count = 0;
						$return_time = 0;
						for ($i = 1; $i <= 10; $i++) {
							$start_var = "o_start_time$i";
							if ($$start_var == "") {
								break;
							}
							$return_count++;

							$end_var = "o_end_time$i";
							if ($$end_var == "") {
								break;
							}

							$arr_ret = $timecard_common_class->get_return_array($end_date_time, $$start_var, $$end_var);
							$return_time += count($arr_ret);

							// 月の深夜勤務時間に退勤後復帰の深夜分を追加
							$sums[17] += count(array_intersect($arr_ret, $arr_time5));
						}
						$wk_return_time = $return_time;

						// 前日までの計 = 週計
						$last_day_total = $work_time_for_week;

						$work_time_for_week += $work_time;
						$work_time_for_week += $wk_return_time;
						if ($tmp_date < $start_date) {
							$tmp_date = next_date($tmp_date);
							$counter_for_week++;
							continue;
						}
						$sums[15] += $work_time;

						//当直の場合稼働時間に日数換算の数値ｘ通常勤務日の労働時間の時間を追加（当直の時間を除外して計算している為)
						if ($night_duty_flag){
							$sums[15] += $duty_work_time;
						}

						// 残業時間を算出（分単位）
						if ($arr_irrg_info["irrg_type"] == "") {  // 変則労働適用外の場合

							// 日々の表示値は普外・残外の減算前
//							$sums[16] += ($effective_time > 480) ? $effective_time - 480 : 0;
							if ($effective_time > $specified_time) {
								$sums[16] += $effective_time - $specified_time;
							} else {
								$sums[16] += 0;
							}
							switch ($others1) {
							case "2":  // 遅刻時間に加算する
								$sums[12] += $time3;
								break;
							case "3":  // 早退時間に加算する
								$sums[13] += $time3;
								break;
							case "4":  // 残業時間から減算する
								$sums[16] -= $time3;
								break;
							}
							switch ($others2) {
							case "2":  // 遅刻時間に加算する
								$sums[12] += $time4;
								break;
							case "3":  // 早退時間に加算する
								$sums[13] += $time4;
								break;
							case "4":  // 残業時間から減算する
								$sums[16] -= $time4;
								break;
							}

						} else if ($arr_irrg_info["irrg_type"] == "1") {  // 変則労働期間・週の場合
							if ($counter_for_week == 7) {
								$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
								if ($hol_minus == "t") {
									$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
								}
								$sums[16] += ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
							}
						}

						// 深夜勤務時間を算出（分単位）
						$time2 = count(array_intersect($arr_effective_time, $arr_time5));
						$sums[17] += $time2;

						// 月の稼働時間・残業時間に退勤後復帰分の残業時間を追加
						$sums[15] += $return_time;
						$sums[16] += $return_time;
					} else {
					// 出勤同時刻退勤または、出勤時間＞退勤時間の場合で
					// 月の開始日前は処理しない
						if ($tmp_date < $start_date) {
							$tmp_date = next_date($tmp_date);
							$counter_for_week++;
							continue;
						}
					}
				} else {

					// 事由が「代替休暇」「振替休暇」の場合、
					// 所定労働時間を稼働時間にプラス
					// 「有給休暇」を除く $reason == "1" || 2008/10/14 
					if ($reason == "4" || $reason == "17") {
						if ($tmp_date >= $start_date) {
							$sums[15] += $specified_time;
							$work_time_for_week += $specified_time;
						}
					}

					if ($arr_irrg_info["irrg_type"] == "1" && $counter_for_week == 7) {
						$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
						if ($hol_minus == "t") {
							$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
						}
						$sums[16] += ($work_time_for_week > $tmp_irrg_minutes) ? $work_time_for_week - $tmp_irrg_minutes : 0;
					}

					if ($tmp_date < $start_date) {
						$tmp_date = next_date($tmp_date);
						$counter_for_week++;
						continue;
					}
				}

				//法定内勤務、法定内残業、法定外残業
				// 前日までの累計 >= 40時間
				if ($last_day_total >= 40 * 60 ) {
				//  法定外 += 稼動時間
					$hoteigai += $work_time + $wk_return_time;
				} else if ($work_time + $wk_return_time > 0){
				// 上記以外、かつ、 稼動時間がある場合

					//法定内勤務と法定外残業
					//法定内勤務のうち週40時間を超える分から法定外残業へ移動
					switch ($rslt_workday_count) {
					case 0.5:
					case 1.0:
						$wk_base_time = 8 * 60;
						break;
					case 1.5:
						$wk_base_time = 12 * 60;
						break;
					case 2.0:
						$wk_base_time = 16 * 60;
						break;
					default:
						$wk_base_time = 8 * 60;
					}
					//wk法定外
					$wk_hoteigai = $work_time + $wk_return_time - $wk_base_time;
					if ($wk_hoteigai > 0) {
						//wk法定内 = (稼動時間 - wk法定外)
						$wk_hoteinai = ($work_time + $wk_return_time - $wk_hoteigai);
						//法定外 += wk法定外
						$hoteigai += $wk_hoteigai;
					} else {
						//wk法定内 = (稼動時間)
						$wk_hoteinai = ($work_time + $wk_return_time);
					}
		//echo("$tmp_date n=$wk_hoteinai g=$wk_hoteigai "); //debug
					//前日までの累計 ＋ wk法定内　＞ 40時間 の場合
					if ($last_day_total + $wk_hoteinai > 40 * 60) {
						//法定内 += (40時間 - 前日までの累計)
						$hoteinai += (40 * 60) - $last_day_total;
						//法定外 += wk法定内 - (40時間 - 前日までの累計)
						$hoteigai += $wk_hoteinai - ((40 * 60) - $last_day_total);
					} else {
					//上記以外
						//法定内 += wk法定内
						$hoteinai += $wk_hoteinai;
					}

					// 法定内残業、所定労働時間から8時間まで間を加算
					// 稼動時間が所定労働時間より大きい場合
					$wk_minute = date_utils::hi_to_minute($day1_time);
					if ($wk_minute != 0 && $wk_minute < 480 && ($work_time + $wk_return_time) > $wk_minute) {

						//稼動時間が8時間以上
						if (($work_time + $wk_return_time) >= 480) {
							//基準法定内残業
							$wk_std_hoteinai_zan = 480 - $wk_minute;
						} else {
						//8時間未満の場合、稼動時間-所定労働時間
							$wk_std_hoteinai_zan = ($work_time + $wk_return_time) - $wk_minute;
						}

						//前日までの累計 + 所定時間 ＜ 40時間 の場合
						if ($last_day_total + $wk_minute < 40 * 60) {
							//wk法定内残業 = 40時間 - (前日までの累計 + 所定時間)
							$wk_hoteinai_zan = 40 * 60 - ($last_day_total + $wk_minute);
							//wk法定内残業 > 基準法定内残業
							if ($wk_hoteinai_zan > $wk_std_hoteinai_zan) {
								// wk法定内残業 = 基準法定内残業
								$wk_hoteinai_zan = $wk_std_hoteinai_zan;
							}
							//法定内勤務 -= wk法定内残業
							$hoteinai -= $wk_hoteinai_zan;
							//法定内残業 += wk法定内残業
							$hoteinai_zan += $wk_hoteinai_zan;
						}
					}
		//echo("$tmp_date m=$wk_minute l=$last_day_total z=$wk_hoteinai_zan "); //debug
				}

				//事由が休日出勤の場合
//				if ($reason == "16") {
//					$hotei_sums[3] += $work_time + $wk_return_time;
//				}

				$tmp_date = next_date($tmp_date);
				$counter_for_week++;
			} // end of while

			// 休日出勤 2008/10/23
			$sums[2] = count($arr_hol_work_date);
			// 法定内勤務
			$hotei_sums[0] = $hoteinai;
			// 法定内残業
			$hotei_sums[1] = $hoteinai_zan;
			// 法定外残業
			$hotei_sums[2] = $hoteigai;

			// 変則労働期間が月の場合
			if ($arr_irrg_info["irrg_type"] == "2") {

				// 所定労働時間を基準時間とする
				$tmp_irrg_minutes = $arr_irrg_info["irrg_minutes"];
				if ($hol_minus == "t") {
					$tmp_irrg_minutes -= (8 * 60 * $holiday_count);
				}
				$sums[14] = ($tmp_irrg_minutes > 0) ? $tmp_irrg_minutes : 0;

				// 稼働時間−基準時間を残業時間とする
				$sums[16] = $sums[15] - $sums[14];
			}
			else {
			// 基準時間の計算を変更 2008/10/21
			// 要勤務日数 × 所定労働時間 
				$sums[14] = $sums[0] * date_utils::hi_to_minute($day1_time);
			}

			// 月集計値の稼働時間・残業時間・深夜勤務を端数処理
			for ($i = 15; $i <= 17; $i++) {
				if ($sums[$i] < 0) {
					$sums[$i] = 0;
				}
				$sums[$i] = round_time($sums[$i], $fraction4, $fraction4_min);
			}

			// 残業管理をしない場合、残業時間、法定内勤務、法定内残業、法定外残業を０とする
			if ($no_overtime == "t") {
				$sums[16] = 0;
				$hotei_sums[0] = 0;
				$hotei_sums[1] = 0;
				$hotei_sums[2] = 0;
			}

			// 当直
			$arr_night_duty_date = get_night_duty_date($con, $fname, $tmp_emp_id, $start_date, $end_date, "atdbkrslt");
			foreach($arr_night_duty_date as $night_duty_date)
			{
				$date = $night_duty_date["date"];
				$timestamp = to_timestamp($date);

				// 日付から休日かを求める
				$holiday = ktHolidayName($timestamp);
				// 日付から曜日を求める
				$wd = get_weekday($timestamp);

				if($holiday != "" || $wd == "日")
				{
					$sums[20]++;
				}
				else if($wd == "土")
				{
					$sums[19]++;
				}
				else
				{
					$sums[18]++;
				}
			}

			// 手当
			$arr_allow_summary = get_allow_summary_for_manage($con, $fname, $tmp_emp_id, $start_date, $end_date, "atdbkrslt");
			$allow_cnt = 20;
			foreach($arr_allow_summary as $allow_summary)
			{
				$allow_cnt++;
				$sums[$allow_cnt] = $allow_summary["allow_cnt"];
			}

		// 締め済みの場合
		} else {

			// 月集計値の取得
			$sql = "select days1, days2, days3, days4, days5, days6, days7, days8, days9, days10, count1, count2, time1, time2, time3, time4, time5, time6, days11, days12, days13, days14, days15, days16, days17, days18, days19, days20, days21, days22, time7, time8, time9, time10 from wktotalm";
			$cond = "where emp_id = '$tmp_emp_id' and yyyymm = '$c_yyyymm'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$sums[0] += pg_fetch_result($sel, 0, "days1");
			$sums[1] += pg_fetch_result($sel, 0, "days2");
			$sums[2] += pg_fetch_result($sel, 0, "days3");
			$sums[3] += pg_fetch_result($sel, 0, "days4");
			$sums[4] += pg_fetch_result($sel, 0, "days5");
			$sums[5] += pg_fetch_result($sel, 0, "days6");
			$sums[6] += pg_fetch_result($sel, 0, "days7");
			$sums[7] += pg_fetch_result($sel, 0, "days8");
			$sums[8] += pg_fetch_result($sel, 0, "days9");
			$sums[9] += pg_fetch_result($sel, 0, "days10");
			$sums[10] += pg_fetch_result($sel, 0, "count1");
			$sums[11] += pg_fetch_result($sel, 0, "count2");
			$sums[12] += hmm_to_minute(pg_fetch_result($sel, 0, "time1"));
			$sums[13] += hmm_to_minute(pg_fetch_result($sel, 0, "time2"));
			$sums[14] += hmm_to_minute(pg_fetch_result($sel, 0, "time3"));
			$sums[15] += hmm_to_minute(pg_fetch_result($sel, 0, "time4"));
			$sums[16] += hmm_to_minute(pg_fetch_result($sel, 0, "time5"));
			$sums[17] += hmm_to_minute(pg_fetch_result($sel, 0, "time6"));
			// 追加事由
			$add_sums[0] = pg_fetch_result($sel, 0, "days11");
			$add_sums[1] = pg_fetch_result($sel, 0, "days12");
			$add_sums[2] = pg_fetch_result($sel, 0, "days13");
			$add_sums[3] = pg_fetch_result($sel, 0, "days14");
			$add_sums[4] = pg_fetch_result($sel, 0, "days15");
			$add_sums[5] = pg_fetch_result($sel, 0, "days16");
			$add_sums[6] = pg_fetch_result($sel, 0, "days17");
			$add_sums[7] = pg_fetch_result($sel, 0, "days18");
			$add_sums[8] = pg_fetch_result($sel, 0, "days19");
			$add_sums[9] = pg_fetch_result($sel, 0, "days20");
			$add_sums[10] = pg_fetch_result($sel, 0, "days21");
			$add_sums[11] = pg_fetch_result($sel, 0, "days22");
			//法定内勤務〜法定外残業、休日勤務
			$hotei_sums[0] = hmm_to_minute(pg_fetch_result($sel, 0, "time7"));
			$hotei_sums[1] = hmm_to_minute(pg_fetch_result($sel, 0, "time8"));
			$hotei_sums[2] = hmm_to_minute(pg_fetch_result($sel, 0, "time9"));
			$hotei_sums[3] = hmm_to_minute(pg_fetch_result($sel, 0, "time10"));

	        // 当直
		    $arr_night_duty_date = get_night_duty_date($con, $fname, $tmp_emp_id, $start_date, $end_date, "wktotald");
		    foreach($arr_night_duty_date as $night_duty_date)
		    {
		        $date = $night_duty_date["date"];
		        $timestamp = to_timestamp($date);

		        // 日付から休日かを求める
		        $holiday = ktHolidayName($timestamp);
		        // 日付から曜日を求める
		        $wd = get_weekday($timestamp);

		        if($holiday != "" || $wd == "日")
		        {
		            $sums[20]++;
		        }
		        else if($wd == "土")
		        {
		            $sums[19]++;
		        }
		        else
		        {
		            $sums[18]++;
		        }
		    }

	        // 手当
	        $arr_allow_summary = get_allow_summary_for_manage($con, $fname, $tmp_emp_id, $start_date, $end_date, "wktotald");
	        $allow_cnt = 20;
	        foreach($arr_allow_summary as $allow_summary)
	        {
	            $allow_cnt++;
	            $sums[$allow_cnt] = $allow_summary["allow_cnt"];
	        }
		} // end of if 締め済み

		// 月集計値を表示用に編集
/*
		for ($i = 0; $i <= 9; $i++) {
			$sums[$i] .= "日";
		}
		for ($i = 10; $i <= 11; $i++) {
			$sums[$i] .= "回";
		}
*/
		for ($i = 12; $i <= 17; $i++) {
			$sums[$i] = ($sums[$i] == 0) ? "0:00" : minute_to_hmm($sums[$i]);
		}
/*
	    for ($i = 18; $i <= 20; $i++) {
		    $sums[$i] .= "回";
	    }
*/
//		$buf .= "$tmp_emp_personal_id,$tmp_emp_name," . implode(",", $sums) . "\r\n";

// 遅刻時間・早退時間は不要
//$values = array();
//for ($i = 0; $i <= count($sums); $i++) {
//	if ($i == 12 || $i == 13) {
//		continue;
//	}
//	$values[] = $sums[$i];
//}
//$buf .= "$tmp_emp_personal_id,$tmp_emp_name," . implode(",", $values) . "\r\n";
		//出力順変更
		$arr_dat = array();

		$idx = 0;
		for ($i = 0; $i <= 17; $i++) {
			// 時間・早退時間は不要
			if ($i == 12 || $i == 13) {
				continue;
			}
			$arr_dat[$idx] = $sums[$i];
			$idx++;
		}

		//法定内勤務、法定内残業、法定外残業、休日勤務
		for ($i = 0; $i <= 3; $i++) {
			$arr_dat[$idx] = ($hotei_sums[$i] == 0) ? "0:00" : minute_to_hmm($hotei_sums[$i]);
			$idx++;
		}

		// 追加事由
		for ($i=0; $i<count($arr_reason_id); $i++) {
			if ($arr_disp_flg[$i] == "t") {
//				$arr_dat[$idx] = $add_sums[$i]."日";
				$arr_dat[$idx] = $add_sums[$i];
				$idx++;
			} 
		}

		// 勤務シフト
		list($shift_summary_a, $shift_summary_b) = get_arr_shift_summary_for_manage($con, $fname, $atdbk_closed, $tmp_emp_id, $start_date, $end_date);
		//件数を取得、シフト項目数の調整
		for ($i=0; $i<count($shift_summary_a); $i++) {
//		for ($i=0; $i<10; $i++) {
			$arr_dat[$idx] = $shift_summary_a[$i]["cnt"];
			$idx++;
		}
//$group_id指定無の場合、シフト項目数の調整
		if ($group_id == "-") {
			$max_cnt = get_max_cnt_atdptn_id($con, $fname);
			for ($i=count($shift_summary_a); $i<=$max_cnt; $i++) {
				$arr_dat[$idx] = "";
				$idx++;
			}
		} else {
		//見出し件数と合わせる
			for ($i=count($shift_summary_a); $i<=count($arr_atdptn_nm); $i++) {
				$arr_dat[$idx] = "";
				$idx++;
			}
		}
		
		// 手当
		for ($i = 18; $i <= 20; $i++) {
			$arr_dat[$idx] = $sums[$i];
			$idx++;
		}

		foreach($arr_allow_summary as $allow_summary)
		{
//			$arr_dat[$idx] = $allow_summary["allow_cnt"]."回";
			$arr_dat[$idx] = $allow_summary["allow_cnt"];
			$idx++;
		}

		// 応援追加分勤務シフト
		for ($i=0; $i<count($shift_summary_b); $i++) {
			$arr_dat[$idx] = $shift_summary_b[$i]["cnt"];
			$idx++;
		}

		$buf .= "$tmp_emp_personal_id,$tmp_emp_name," . join(",", $arr_dat) . "\r\n";

	} // end of foreach

	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}

function get_allow($con, $fname)
{
    $sql   = "select allow_id, allow_contents from tmcdallow ";
    $cond  = "where not del_flg order by allow_id asc ";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $arr = array();
    while ($row = pg_fetch_array($sel))
    {
        $arr[] = array( "allow_id" => $row["allow_id"], "allow_contents" => $row["allow_contents"]);
    }

    return $arr;

}

?>
