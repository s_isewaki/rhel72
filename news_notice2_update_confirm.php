<?

require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];
$notice_id = $_REQUEST['notice_id'];
$notice_name = $_REQUEST['notice_name'];

// セッションのチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
//aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

// 入力チェック
if ($notice_name == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('通達区分が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}
if (strlen($notice_name) > 30) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('通達区分が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// 通達区分名の更新
$sql = "update newsnotice2 set";
$set = array("notice2_name");
$setvalue = array(p($notice_name));
$cond = "where notice2_id = " . p($notice_id);
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_close($con);
    js_error_exit();
}

// データベース接続を閉じる
pg_close($con);

// 通達区分一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_notice2_list.php?session=$session';</script>");
