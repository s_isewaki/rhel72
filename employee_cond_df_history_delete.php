<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

//勤務シフト作成の個人勤務条件からの場合を除く
if ($wherefrom != "1") { 
    // 権限のチェック
    $checkauth = check_authority($session, 18, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }
    
    // 職員登録権限を取得
    $reg_auth = check_authority($session, 19, $fname);
}
// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// 変更履歴を取得
if (!empty($_POST["empcond_duty_form_history_id"])) {
    $empcond_duty_form_history_id = pg_escape_string($_POST["empcond_duty_form_history_id"]);
    $sql = "DELETE FROM empcond_duty_form_history WHERE empcond_duty_form_history_id=$empcond_duty_form_history_id";
    $del = delete_from_table($con, $sql, "", $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
<?
//勤務シフト作成の個人勤務条件からの場合
if ($wherefrom == "1") { ?>
location.href = 'employee_cond_df_history_list.php?session=<?=$session?>&emp_id=<?=$emp_id?>&wherefrom=<?=$wherefrom?>';
<? }
else {
    ?>
location.href = 'employee_condition_setting.php?session=<?=$session?>&emp_id=<?=$emp_id?>';
<? } ?>
</script>