<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_date_navigation_month.ini");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 56, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 稼動状況統計設定権限の取得
$stat_setting_auth = check_authority($session, 52, $fname);

$link_id = $item_id;
list($item_id, $assist_id) = explode("_", $item_id);

// データベースに接続
$con = connect2db($fname);

// 管理項目一覧を取得
$sql = "select statitem_id, name, total_flg, total_start_month, total_start_day from statitem";
$cond = "where del_flg = 'f' order by statitem_id";
$sel_item = select_from_table($con, $sql, $cond, $fname);
if ($sel_item == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$items = array();
while ($row = pg_fetch_array($sel_item)) {
	$items[$row["statitem_id"]] = array(
		"name" => $row["name"],
		"total_flg" => $row["total_flg"],
		"total_start_month" => $row["total_start_month"],
		"total_start_day" => $row["total_start_day"],
		"assists" => array()
	);
}

// 補助項目一覧を取得
$sql = "select statassist_id, statassist_name, statitem_id, assist_type, calc_var1_type, calc_var1_const_id, calc_var1_item_id, calc_var1_assist_id, calc_operator, calc_var2_type, calc_var2_const_id, calc_var2_item_id, calc_var2_assist_id, calc_format from statassist";
$cond = "where del_flg = 'f' order by statassist_id";
$sel_assist = select_from_table($con, $sql, $cond, $fname);
if ($sel_assist == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel_assist)) {
	$items[$row["statitem_id"]]["assists"][$row["statassist_id"]] = array(
		"statassist_name" => $row["statassist_name"],
		"assist_type" => $row["assist_type"],
		"var1_type" => $row["calc_var1_type"],
		"var1_const_id" => $row["calc_var1_const_id"],
		"var1_item_id" => $row["calc_var1_item_id"],
		"var1_assist_id" => $row["calc_var1_assist_id"],
		"operator" => $row["calc_operator"],
		"var2_type" => $row["calc_var2_type"],
		"var2_const_id" => $row["calc_var2_const_id"],
		"var2_item_id" => $row["calc_var2_item_id"],
		"var2_assist_id" => $row["calc_var2_assist_id"],
		"format" => $row["calc_format"]
	);
}

// 定数一覧を取得
$sql = "select statconst_id, statconst_value from statconst";
$cond = "where del_flg = 'f' order by statconst_id";
$sel_const = select_from_table($con, $sql, $cond, $fname);
if ($sel_const == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$consts = array();
while ($row = pg_fetch_array($sel_const)) {
	$consts[$row["statconst_id"]] = $row["statconst_value"];
}

// 管理項目名を変数に格納
$item_name = $items[$item_id]["name"];

// 補助項目の場合は補助項目情報を変数に格納
if ($assist_id != "") {
	$assist = $items[$item_id]["assists"][$assist_id];
}

// 施設を取得
$sql = "select char(1) '1' as area_type, statfcl_id, name, area_id, (select order_no from area where area.area_id = statfcl.area_id) as order_no from statfcl where area_id is not null and del_flg = 'f' union select '2', statfcl_id, name, area_id, statfcl_id from statfcl where area_id is null and del_flg = 'f'";
$cond = "order by 1, order_no";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 当月の全統計値を取得
$selected_ym = date("Ym", $date);
$sql = "select statfcl_id, statitem_id, statassist_id, stat_date, stat_val from stat";
$cond = "where stat_date like '{$selected_ym}__'";
$sel_stat = select_from_table($con, $sql, $cond, $fname);
if ($sel_stat == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$stats = array();
while ($row = pg_fetch_array($sel_stat)) {
	$tmp_fcl_id = $row["statfcl_id"];
	$tmp_item_id = $row["statitem_id"];
	$tmp_assist_id = $row["statassist_id"];
	$tmp_date = $row["stat_date"];
	$tmp_val = $row["stat_val"];

	if ($tmp_val === "") {
		continue;
	}

	if ($tmp_assist_id == "") {
		$tmp_id = $tmp_item_id;
	} else {
		$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
	}

	$tmp_day = intval(substr($tmp_date, 6, 2));

	$stats[$tmp_fcl_id][$tmp_id][$tmp_day] = $tmp_val;
}

// イントラメニュー情報を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu1 = pg_fetch_result($sel, 0, "menu1");

$last_month = get_last_month($date);
$next_month = get_next_month($date);
$year = date("Y", $date);
$month = date("m", $date);
$days = days_in_month($year, $month);
?>
<title>イントラネット | <? echo($menu1); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_stat_item_day.php?session=<? echo($session); ?>&item_id=<? echo($link_id); ?>&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_stat.php?session=<? echo($session); ?>"><b><? echo($menu1); ?></b></a></font></td>
<? if ($stat_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="stat_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu1)); ?>" align="center" bgcolor="#5279a5"><a href="intra_stat.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu1); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat_item_day.php?session=<? echo($session); ?>&date=<? echo($last_month); ?>&item_id=<? echo($link_id); ?>">&lt;前月</a>&nbsp;<select onchange="changeDate(this.value);"><? show_date_options_m($date); ?></select>&nbsp;<a href="intra_stat_item_day.php?session=<? echo($session); ?>&date=<? echo($next_month); ?>&item_id=<? echo($link_id); ?>">翌月&gt;</a></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="intra_stat_item_month.php?session=<? echo($session); ?>&date=<? echo($date); ?>&item_id=<? echo($link_id); ?>">月</a><img src="img/spacer.gif" alt="" width="10" height="1"><a href="intra_stat.php?session=<? echo($session); ?>&date=<? echo($date); ?>">日</a></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2" class="list">
<? show_chart(true, 1, 15); ?>
<tr>
<td colspan="16" height="5" style="border-style:none;"></td>
</tr>
<? show_chart(false, 16, $days); ?>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_chart($show_name, $start_day, $end_day) {
	global $link_id;
	global $assist_id;
	global $month;
	global $year;
	global $item_name;
	global $assist;
	global $sel_fcl;
	global $items;
	global $consts;
	global $stats;
	global $session;
?>
<tr height="22" bgcolor="#f6f9ff" valign="top">
<td width="150"><? if ($show_name) { ?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($item_name); ?><? if ($assist_id != "") { ?><br>&nbsp;&gt; <? echo($assist["statassist_name"]); ?><? } ?></b></font><? } ?></td>
<?
	for ($i = $start_day; $i <= $end_day; $i++) {
		$tmp_date = mktime(0, 0, 0, $month, $i, $year);
		echo("<td width=\"35\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"intra_stat.php?session=$session&date=$tmp_date\">{$i}(" . get_weekday($tmp_date) . ")</a></font></td>\n");
	}
?>
</tr>
<?
	pg_result_seek($sel_fcl, 0);
	while ($row = pg_fetch_array($sel_fcl)) {
		$fcl_id = $row["statfcl_id"];
		$fcl_name = $row["name"];

		echo("<tr height=\"22\">\n");
		echo("<td bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$fcl_name</font></td>\n");

		for ($i = $start_day; $i <= $end_day; $i++) {

			// 管理項目か入力系の補助項目の場合
			if ($assist_id == "" || $assist["assist_type"] == "1") {
				$tmp_val = $stats[$fcl_id][$link_id][$i];

			// 計算系の補助項目の場合
			} else {
				$tmp_var1_type = $assist["var1_type"];
				$tmp_var1_const_id = $assist["var1_const_id"];
				$tmp_var1_item_id = $assist["var1_item_id"];
				$tmp_var1_assist_id = $assist["var1_assist_id"];
				$tmp_operator = $assist["operator"];
				$tmp_var2_type = $assist["var2_type"];
				$tmp_var2_const_id = $assist["var2_const_id"];
				$tmp_var2_item_id = $assist["var2_item_id"];
				$tmp_var2_assist_id = $assist["var2_assist_id"];
				$tmp_format = $assist["format"];

				// 項目1が定数の場合
				if ($tmp_var1_type == "1") {
					$tmp_var1_val = $consts[$tmp_var1_const_id];

				// 項目1が項目指定の場合
				} else {
					if ($tmp_var1_assist_id == "") {
						$tmp_var1_val = $stats[$fcl_id][$tmp_var1_item_id][$i];
					} else {
						$tmp_var1_val = $stats[$fcl_id]["{$tmp_var1_item_id}_{$tmp_var1_assist_id}"][$i];
					}
				}

				// 項目1が空の場合、計算しない
				if ($tmp_var1_val == "") {
					echo("<td></td>\n");
					continue;
				}

				// 項目2が定数の場合
				if ($tmp_var2_type == "1") {
					$tmp_var2_val = $consts[$tmp_var2_const_id];

				// 項目2が項目指定の場合
				} else {
					if ($tmp_var2_assist_id == "") {
						$tmp_var2_val = $stats[$fcl_id][$tmp_var2_item_id][$i];
					} else {
						$tmp_var2_val = $stats[$fcl_id]["{$tmp_var2_item_id}_{$tmp_var2_assist_id}"][$i];
					}
				}

				// 項目2が空の場合、計算しない
				if ($tmp_var2_val == "") {
					echo("<td></td>\n");
					continue;
				}

				// 四則演算
				$tmp_var1_val = floatval($tmp_var1_val);
				$tmp_var2_val = floatval($tmp_var2_val);
				switch ($tmp_operator) {
				case "1":  // ＋
					$tmp_val = $tmp_var1_val + $tmp_var2_val;
					break;
				case "2":  // −
					$tmp_val = $tmp_var1_val - $tmp_var2_val;
					break;
				case "3":  // ×
					$tmp_val = $tmp_var1_val * $tmp_var2_val;
					break;
				case "4":  // ÷
					if ($tmp_var2_val <> 0) {
						$tmp_val = $tmp_var1_val / $tmp_var2_val;
					} else {
						$tmp_val = null;
					}
					break;
				}

				// 実数表示の場合
				if ($tmp_format == "1") {
					$tmp_val = round($tmp_val, 1);

				// %表示の場合
				} else {
					$tmp_val *= 100;
					$tmp_val = round($tmp_val, 1) . "%";
				}
			}

			echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_val</font></td>\n");
		}

		echo("</tr>\n");
	}
}

function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

function get_weekday($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}
?>
