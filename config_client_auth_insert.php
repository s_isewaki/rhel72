<?
ob_start();

require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 入力チェック
if (strlen($client_no) > 50) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('端末番号が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($note) > 200) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('備考が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 認証キーの生成
$auth_key = md5(uniqid(mt_rand(), true));

// 発行者（ログインユーザ）の職員IDを取得
$sql = "select emp_id from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$issuer_emp_id = pg_fetch_result($sel, 0, "emp_id");

// 端末認証履歴情報の登録
$sql = "insert into client_auth (client_no, note, issuer_emp_id, issue_datetime, auth_key) values (";
$content = array($client_no, $note, $issuer_emp_id, date("YmdHi"), $auth_key);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

pg_close($con);

// cookieの送信
ob_end_clean();
setcookie("caid", $auth_key, mktime(0, 0, 0, 1, 1, 2030), "", "", true);

echo("<script type=\"text/javascript\">opener.location.href = 'config_client_auth.php?session=$session'; window.close();</script>");
