<html>
<head>
<title>CoMedix 病床管理 | 外泊詳細</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>外出詳細</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<? if ($term == "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center"><a href="inpatient_go_out_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>外泊</b></font></td>
<td>&nbsp;</td>
</tr>
</table>
<? } ?>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日時</font></td>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
</tr>
<?
$sql = "select * from inptgoout";
$cond = "where ptif_id = '$pt_id' and out_type = '2' and to_date(out_date, 'YYYYMMDD') >= to_date((select inpt_in_dt from inptmst where inptmst.ptif_id = inptgoout.ptif_id), 'YYYYMMDD')";
$now = date("YmdHi");
$date = date("Ymd");
if ($term == "1") {
	$cond .= " and to_date('$date', 'YYYYMMDD') > to_date(ret_date, 'YYYYMMDD')";
} else if ($term == "2") {
	$cond .= " and to_date('$date', 'YYYYMMDD') between to_date(out_date, 'YYYYMMDD') and to_date(ret_date, 'YYYYMMDD')";
} else if ($term == "3") {
	$cond .= " and to_date('$date', 'YYYYMMDD') < to_date(out_date, 'YYYYMMDD')";
}
$cond .= " order by out_date desc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

while ($row = pg_fetch_array($sel)) {
	$out_dt = format_date($row["out_date"]);
	$ret_dt = format_date($row["ret_date"]);

	if ($row["stop_out_fd1"] == "1") {
		$fd1 = "朝から";
	} else if ($row["stop_out_fd1"] == "2") {
		$fd1 = "昼から";
	} else if ($row["stop_out_fd1"] == "3") {
		$fd1 = "夕から";
	}
	if ($row["stop_out_fd2"] == "1") {
		$fd2 = "朝まで";
	} else if ($row["stop_out_fd2"] == "2") {
		$fd2 = "昼まで";
	} else if ($row["stop_out_fd2"] == "3") {
		$fd2 = "夕まで";
	}

	$place = $row["place"];
	$comment = str_replace("\n", "<br>", $row["comment"]);

	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$out_dt");
	echo("〜");
	echo("$ret_dt</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">食止め：開始：{$fd1}&nbsp;終了：{$fd2}<br>外泊先：$place<br>コメント：$comment</font></td>\n");
	echo("</tr>\n");
}
?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み日付取得
// Name      :  format_date
// Arguments :  dt - 日付（「YYYYMMDD」形式）
// Return    :  日付（「YYYY/MM/DD」形式）
//------------------------------------------------------------------------------
function format_date($dt) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})$/';
	$replacement = '$1/$2/$3';
	return preg_replace($pattern, $replacement, $dt);
}

//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み時刻取得
// Name      :  format_time
// Arguments :  tm - 時刻（「HHMM」形式）
// Return    :  時刻（「HH:MM」形式）
//------------------------------------------------------------------------------
function format_time($tm) {
	$pattern = '/^(\d{2})(\d{2})$/';
	$replacement = '$1:$2';
	return preg_replace($pattern, $replacement, $tm);
}
?>
