<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_mail_input.ini");
require_once("hiyari_report_auth_models.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//送信の場合の処理
//==============================
if($is_postback == "true")
{
    //==============================
    //メール送信
    //==============================
    $job_id = get_job_id_from_mail_id($con,$fname,$mail_id);
    $eis_id = get_eis_id_from_mail_id($con,$fname,$mail_id);

    $report_html = '';
    if ($design_mode == 2){
        $send_info = get_send_mail_info($con, $fname, $mail_id);
        foreach($send_info['send_mail'] as $info){
            //テキストファイルに保存したデータを取得（報告書データとメッセージ）
            $moto_mail_message = get_mail_message($info['mail_id']);
            if ($moto_mail_message != "メッセージを表示できません。"){
                $report_html = $mail_message."<br><br><br>";
                $report_html.= "<div style='background-color:#FFDDFD'>";
                $report_html.= "<nobr>**************************************************</nobr><br>";
                $report_html.= $moto_mail_sender."の報告内容：<br>".$moto_mail_message;
                $report_html.= "</div>";
                break;
            }
        }
    }
    $send_mail_id = send_mail_from_submit_data($session,$con,$fname,$_POST,$report_id,$job_id,$eis_id,'','',$mail_id, $report_html);

    //==============================
    //受信メールのステータス更新
    //==============================
    $emp_id = get_emp_id($con,$session,$fname);
    update_mail_status($con, $fname, $mode, $mail_id, $send_mail_id, $emp_id);

    //==============================
    //自分画面を閉じる。
    //==============================
    echo("<script language='javascript'>");
    echo("if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}");
    echo("window.close();");
    echo("</script>");
    exit;
}

//==============================
//処理モードごとの処理
//==============================
switch($mode)
{
    case "forward":
        $title_str = "メッセージ転送";
        $subject_header = "Fw: ";
        break;
    case "return":
    default:
        $mode = "return";
        $title_str = "メッセージ返信";
        $subject_header = "Re: ";
        break;
}


//==============================
//元メール情報取得
//==============================

$where = "where mail_id = '$mail_id'";
$order = "";

$fields = array(
        'report_id',
//      'report_title',
//      'job_id',
        'registrant_id',
//      'registrant_name',
//      'registration_date',
//      'eis_id',
//      'eid_id',
//      'del_flag',
//      'report_no',
//      'shitagaki_flg',
//      'report_comment',
//      'edit_lock_flg',
//      'anonymous_report_flg',
        'registrant_class',
        'registrant_attribute',
        'registrant_dept',
        'registrant_room',
        'doctor_emp_id',

//      'report_emp_class',
//      'report_emp_attribute',
//      'report_emp_dept',
//      'report_emp_room',
//      'report_emp_job',
//      'report_emp_lt_nm',
//      'report_emp_ft_nm',
//      'report_emp_kn_lt_nm',
//      'report_emp_kn_ft_nm',

//      '[auth]_update_emp_id',
//      '[auth]_start_flg',
//      '[auth]_end_flg',
//      '[auth]_start_date',
//      '[auth]_end_date',
//      '[auth]_use_flg',

        'mail_id',
        'subject',
        'send_emp_id',
        'send_emp_name',
//      'send_date',
//      'send_del_flg',
//      'send_job_id',
//      'send_eis_id',
        'anonymous_send_flg',
//      'marker',
//
//      'send_emp_class',
//      'send_emp_attribute',
//      'send_emp_dept',
//      'send_emp_room',
//      'send_emp_job',
//      'send_emp_lt_nm',
//      'send_emp_ft_nm',
//      'send_emp_kn_lt_nm',
//      'send_emp_kn_ft_nm',
        'message',
        'original_mail_id'
    );

$moto_mail_info = get_send_mail_list($con,$fname,$where,$order);
if(count($moto_mail_info) < 1)
{
    //パラメータ異常。
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
$moto_mail_info = $moto_mail_info[0];

$report_id = $moto_mail_info["report_id"];

$moto_mail_sender_id = $moto_mail_info["send_emp_id"];
$is_anonymous_moto_mail = $moto_mail_info["anonymous_send_flg"] == "t";
//$moto_mail_sender = get_emp_kanji_name_anonymous($con,$moto_mail_sender_id,$is_anonymous_moto_mail,$fname);
$moto_mail_sender = get_emp_kanji_name($con,$moto_mail_sender_id,$fname);

//==============================
//初期表示情報の作成
//==============================

//件名
$default_subject = $moto_mail_info["subject"];
// メッセージ返信記号の追加処理コメントアウト 20100408
/*if( strlen($default_subject) >= 4 && ( substr($default_subject,0,4) == "Re: " || substr($default_subject,0,4) == "Fw: " ) )
{
    $default_subject = substr($default_subject,4);
}*/
$default_subject = $subject_header.$default_subject;
$default_subject = h($default_subject);

//メッセージ用送信元の送信者名
if($is_anonymous_moto_mail)
{
    $moto_mail_sender2 = get_anonymous_name($con,$fname);
}
else
{
    $moto_mail_sender2 = h($moto_mail_sender)."さん";
}

//宛先
if($mode == "return")
{
    $is_to_editable = false;
    $is_cc_editable = !($moto_mail_info["anonymous_send_flg"] == "t");//匿名の場合はCC指定不可能。
    $default_to_list = array();
    $default_to_list[] = array(
            "emp_id" => $moto_mail_info["send_emp_id"] ,
            "emp_name" => $moto_mail_info["send_emp_name"] ,
            "is_anonymous" => ($moto_mail_info["anonymous_send_flg"] == "t")
        );
    $default_cc_list = array();//空配列
}
else
{
    $is_to_editable = true;
    $is_cc_editable = true;
    $default_to_list = array();//空配列
    $default_cc_list = array();//空配列
}

//==============================
//編集・進捗登録ボタン表示チェック
//==============================
$rep_obj = new hiyari_report_class($con, $fname);
if($rep_obj->is_deleted_report($report_id))
{
    //論理削除済みの場合は非表示(編集は参照のみ)
    $report_edit_flg = is_mail_report_editable($session,$fname,$con);
    $progres_edit_flg = false;
}
else
{
    $report_edit_flg = is_mail_report_editable($session,$fname,$con);
    $progres_edit_flg = is_progres_editable($session,$fname,$con);
}

//==============================
// トレイの更新権限取得
//==============================
$report_torey_update_flg = getToreyUpdateAuth($con, $session, $fname, $report_id);


//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("fname", $fname);
$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("title", $title_str);

//メール情報
$smarty->assign("mail_id", $mail_id);
$smarty->assign("mail_id_mode", $mail_id_mode);
$smarty->assign("report_id", $report_id);
$smarty->assign("mode", $mode);

//実行アイコン
$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$smarty->assign("arr_btn_flg", $arr_btn_flg);
$smarty->assign("report_edit_flg", $report_edit_flg);
$smarty->assign("report_torey_update_flg", $report_torey_update_flg);
$smarty->assign("progres_edit_flg", $progres_edit_flg);

//メールヘッダー
$registrant_class     = $moto_mail_info["registrant_class"];
$registrant_attribute = $moto_mail_info["registrant_attribute"];
$registrant_dept      = $moto_mail_info["registrant_dept"];
$registrant_room      = $moto_mail_info["registrant_room"];
$doctor_emp_id        = $moto_mail_info["doctor_emp_id"];
$report_registrant_id = $moto_mail_info["registrant_id"];
assign_mail_header_data($smarty,$session,$con,$fname,$registrant_class,$registrant_attribute,$registrant_dept,$registrant_room,$doctor_emp_id,$report_registrant_id,$default_subject,$default_to_list,$default_cc_list,$is_to_editable,$is_cc_editable);

//------------------------------
//メッセージと報告書内容
//------------------------------
//送信情報を取得
$send_info = get_send_mail_info($con, $fname, $mail_id);

//新デザインで、DBから送信時の報告書データが取得できた場合（新デザインで送信したメール）
$send_mail = array();
if ($design_mode == 2 && count($send_info['rpt_data']) > 0){
    $send_mail = $send_info['send_mail'];
    $smarty->assign("rpt_data", $send_info['rpt_data']);
}

//旧デザイン、またはDBから送信時の報告書データが取得できない場合（旧デザインで送信したメール）
else{
    foreach($send_info['send_mail'] as $info){
        //テキストファイルに保存したデータを取得（報告書データとメッセージ）
        $moto_mail_message = get_mail_message($info['mail_id']);
        if ($moto_mail_message != "メッセージを表示できません。"){
            if ($design_mode == 2){
                $smarty->assign("old_message", "※旧デザインで送信された報告書です。");
            }
            
            if ($design_mode == 1){
                $mail_message = "<br><br><br>";
                $mail_message.= "<div style='background-color:#FFDDFD'>";                
            }
            $mail_message.= "<nobr>**************************************************</nobr><br>";
            $mail_message.= $moto_mail_sender2."の報告内容：<br>".$moto_mail_message;
            if ($design_mode == 1){
                $mail_message.= "</div>";
            }
            $smarty->assign("mail_message", $mail_message);
            
            //メッセージ(Opera用タグ抜きTEXT)
            if ($design_mode == 1){
                $mail_message_non_tag = $moto_mail_message;
                $mail_message_non_tag = str_replace("\r\n","\n",$mail_message_non_tag);//念のため
                $mail_message_non_tag = str_replace("\r","",$mail_message_non_tag);//念のため
                $mail_message_non_tag = str_replace("<br>","\n",$mail_message_non_tag);//TEXT→HTML変換時の改行
                $mail_message_non_tag = html_to_text_for_tinymce($mail_message_non_tag);//TinyMCEの変換
                $mail_message_non_tag = "\n\n\n**************************************************\n".$moto_mail_sender2."の報告内容：\n".$mail_message_non_tag;//引用
                $mail_message_non_tag = str_replace("\n","\\n",$mail_message_non_tag);//JavaScript用
                $mail_message_non_tag = str_replace("\"","\\\"",$mail_message_non_tag);//JavaScript用
                $smarty->assign("mail_message_non_tag", $mail_message_non_tag);
            }
            
            break;
        }

        //テキストファイルに保存したデータがない分は、別途保持
        $send_mail[] = $info;
    }    
}
$smarty->assign("moto_mail_sender", $moto_mail_sender2);

//関連する送信メール情報（メールID,送信者名,メッセージの配列）
if (count($send_mail) > 0){
    $smarty->assign("send_mail", $send_mail);
}

if ($design_mode == 1){
    $smarty->display("hiyari_rp_mail_input1.tpl");
}
else{
    $smarty->display("hiyari_rp_mail_input2.tpl");
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);
