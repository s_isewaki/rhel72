<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix メドレポート｜履歴一覧</title>
<?
require_once("about_comedix.php");
require_once("show_patient_summary_list.ini");
require_once("show_patient_next.ini");
require_once("show_select_values.ini");
require_once("get_values.ini");
require_once("conf/sql.inf");
require_once("sot_util.php");
require_once("sum_exist_workflow_check_exec.ini");
require_once("yui_calendar_util.ini");

//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<script type="text/javascript">
	// この子画面の現在の高さを求める
	function getCurrentHeight(){
		if (!window.parent) return 0;
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = Math.max(h, 300);
		return (h*1+50);
	}
	// この子画面の最初の高さを親画面(summary_rireki.php)に通知しておく。onloadで呼ばれる。
	// 一旦サイズを小さくしたのち、正しい値を再通知するようにしないといけない。
	function setDefaultHeight(){
		if (!window.parent) return;
		window.parent.setDefaultHeight("hidari", 300);
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.setDefaultHeight("hidari",h);
	}
	// この子画面がリサイズされたら、高さを再測定して親画面(summary_rireki.php)に通知する。
	// 「サマリ内容ポップアップ」が開くときも呼ばれる。onloadでも呼ばれる。
	// 親画面はこの子画面のIFRAMEサイズを変更する。
	// 一旦大きくしたIFRAMEのサイズは通常小さくできない。
	// よって、getCurrentHeightでサイズ測定する前に、画面を一旦元に戻さないといけない。
	function resizeIframeHeight(){
		resetIframeHeight();
		var h = getCurrentHeight();
		if (h) window.parent.resizeIframeHeight("hidari", h);
	}
	// この子画面の高さを、最初の高さに戻す。「サマリ内容ポップアップ」が閉じるとき呼ばれる。
	// 親画面(summary_rireki.php)は、この子画面IFRAMEの高さを最初の値に戻す。
	function resetIframeHeight(){
		if (!window.parent) return;
		window.parent.resetIframeHeight("hidari");
	}
</script>
<?
write_yui_calendar_use_file_read_0_12_2();
write_yui_calendar_script2(1);
?>
</head>
<body style="background-color:#f4f4f4" onload="setDefaultHeight();resizeIframeHeight();if (document.getElementById('cal1Container')) {initcal();}">
<? $emp_id = get_emp_id($con, $session, $fname); ?>
	<script type="text/javascript" src="js/summary_naiyo_pupup.js"></script>
	<script type="text/javascript">
		var popup_visible = true;
		function popupNaiyo(top_message,problem,emp_name,naiyou, e, title_flg) {
			if(!popup_visible) return;
			wk_title = (title_flg == "1" ? 'プロブレム' : 'テンプレート');
			popupDetailNaiyo(top_message, new Array(wk_title,problem, '記載者', emp_name, '内容', naiyou), 300, 60, e);
			resizeIframeHeight();
		}
		function popupNaiyoWithFiles(top_message,problem,emp_name,files,naiyou, e, title_flg) {
			if(!popup_visible) return;
			wk_title = (title_flg == "1" ? 'プロブレム' : 'テンプレート');
			popupDetailNaiyo(top_message, new Array(wk_title,problem, '記載者', emp_name, '添付ファイル', files, '内容', naiyou), 300, 60, e);
			resizeIframeHeight();
		}
		function summary_copy(copy_summary_id) {
			document.copy_form.copy_summary_id.value = copy_summary_id;
			document.copy_form.submit();
		}
		function show_flowsheet(encorded_diag_div) {
			var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
			var url = "summary_flowsheet.php?session=<?=$session?>&pt_id=<?=$pt_id?>&diag_div=" + encorded_diag_div;
			window.open(url, 'summary_frowsheet_window',option);
		}

function dosubmit()
{
  document.frm.itm_ord.value=parent.document.getElementById("itm_ord").value;
  document.frm.submit();
}

function changeDateTo(st_date) {
	document.date_form.st_date.value = st_date;
	document.date_form.submit();
}

function changeDate() {
	var s_year  = document.getElementById('date_y1').value;
	var s_month = document.getElementById('date_m1').value;
	var s_day   = document.getElementById('date_d1').value;

	var year  = parseInt(s_year , 10);
	var month = parseInt(s_month, 10) - 1;
	var day   = parseInt(s_day  , 10);

	var date = new Date(year, month, day);
	if (date.getFullYear() != year || date.getMonth() != month || date.getDate() != day) {
		alert('日付が不正です。');
		return;
	}

	document.date_form.st_date.value = s_year.concat(s_month).concat(s_day);
	document.date_form.submit();
}

	</script>

	<form id="date_form" name="date_form" action="summary_rireki_table.php" method="GET">
		<input type="hidden" name="session" value="<?=$session?>">
		<input type="hidden" name="pt_id" value="<?=$pt_id?>">
		<input type="hidden" name="key1" value="<?=$key1?>">
		<input type="hidden" name="st_date" value="">
		<input type="hidden" name="mode" value="<?=$mode?>">
	</form>

	<form id="copy_form" name="copy_form" action="summary_new.php" target="migi" method="POST">
		<input type="hidden" name="session" value="<?=$session?>">
		<input type="hidden" name="copy_summary_id" value="">
		<input type="hidden" name="pt_id" value="<?=$pt_id?>">
	</form>

	<?
	$popup_copy_message   = "<img src=\\'img/summary_rireki_copy.gif\\' class=\\'baseline\\'>をクリックすると、この内容で新規作成します。";
	$popup_detail_message = "リンクをクリックすると、詳細ページを表示します。";

	// 操作者の職種を取得
	$sql = "select emp_job from empmst where emp_id = '".pg_escape_string($emp_id)."'";
	$sel = select_from_table($con,$sql,"",$fname);
	$emp_job_id = pg_fetch_result($sel, 0, "emp_job");


	//=================================================
	// 開始、終了日を設定する。開始、終了期間は５日間。
	//=================================================
	$t_date = date("Ymd", strtotime("today")) ;  // 今日の日付を取得

	// 検索対象日付の指定がない場合
	if (!$_REQUEST["st_date"]) {
		$s_date = $t_date ;
	} else {
		$s_date = $_REQUEST["st_date"];
	}
	// ４日前の日付取得
	$e_date = date("Ymd",strtotime("-4 day", strtotime($s_date)));


	// 時系列情報モード
	if($submode == ''){

		//=================================================
		// 対象日付リストを作成する。本日を右端にして５日間
		//=================================================
		$cre_date = array();
		for ($i = 0; $i < 5; $i++){
			$cre_date[$i] = date("Ymd", strtotime("+".$i." day", strtotime($e_date)));
		}

	// 最新情報モード
	} else if($submode == "new") {

		//===================================
		// 各診療区分に出されたテンプレートのうち、最新のものをひとつづつピックアップする。
		//===================================
		$sel_last_orders = array();

		$where = "" ;

		$paramname = "diag_div"; // 診療記録区分
		if (@$mode == "section") $paramname = "sect_id"; // 診療科
		if (@$mode == "problem") $paramname = "problem"; // プロブレム

		$sql =
			" select ".$paramname.", tmpl_id, max(summary_id) as max_summary_id from summary".
			" where ptif_id = '".pg_escape_string($pt_id)."'".
			" " .$where.
			" group by tmpl_id, ".$paramname;
		$sel3 = select_from_table($con, $sql, "", $fname);
		while($row = pg_fetch_array($sel3)) {
			$sel_last_orders[$row[$paramname]][] = $row;
		}

	}

	//===================================
	//一覧表示の縦項目の一覧を取得
	//===================================
	if(@$mode == "") {// 診療記録区分
		$sql =
			" select * from (".
			"   select *, case when div_sort_order = '' then null else div_sort_order end as sort_order from divmst".
			"   where (div_display_auth_flg is null or div_display_auth_flg = 0 or div_display_auth_stid_list like '%,".$emp_job_id.",%')".
			"   or (div_modify_auth_flg is null or div_modify_auth_flg = 0 or div_modify_auth_stid_list like '%,".$emp_job_id.",%')".
			") d".
			" where div_del_flg = 'f' and (div_hidden_flg is null or div_hidden_flg <> 1) order by sort_order, div_id";
	} else if (@$mode == "section") {// 診療科
		$sql =
			" select * from (".
			"   select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
			"   where sect_del_flg = 'f' and (sect_hidden_flg is null or sect_hidden_flg <> 1)".
			" ) d".
			" order by sort_order, sect_id";
	}else{// プロブレム
		$sql = "select distinct problem from summary where (ptif_id = '$pt_id')";
	}
	$divsel = select_from_table($con,$sql,"",$fname);
	if($divsel == 0){
		pg_close($con);
		exit;
	}

	//===================================
	//表示する日の数(=$loop)及び表示の期間を決定
	//===================================

	//診療記録区分/プロブレムの範囲日付
	$yoko_s_date = $e_date;  // from_date
	$yoko_e_date = $s_date;  // to_date

	$loop = $num = 5 ;
	$divnum = pg_numrows($divsel);

	$sel2 = select_from_table($con,"select * from tmplmst","",$fname);
	if($sel2 == 0){
		pg_close($con);
		exit;
	}
	$arr_tmplname = array();
	$num2 = pg_numrows($sel2);
	for ($ii=0; $ii<$num2; $ii++) {
		$id = pg_result($sel2,$ii,"tmpl_id");
		$arr_tmplname[$id] = pg_result($sel2,$ii,"tmpl_name");
	}

	//===================================
	//最新／時系列へのリンクを作成
	//===================================
	if($submode == ''){
		$submode_link = "<a href=\"./summary_rireki_table.php?session=$session&pt_id=$pt_id&key1=".@$key1."&key2=".@$key2."&mode=".@$mode."&submode=new\" style=\"color:blue;\">最新情報</a>";
	} else if($submode == 'new'){
		$submode_link = "<a href=\"./summary_rireki_table.php?session=$session&pt_id=$pt_id&key1=".@$key1."&key2=".@$key2."&mode=".@$mode."&submode=\" style=\"color:blue;\">時系列情報</a>";
	}


	//===================================
	// 温度板へのボタンを作成
	//===================================
	$ondoban_button_html = "";
	$date = $s_date;
	$yy = substr($date,0,4); $mm = substr($date,4,2); $dd = substr($date,6,2);
	$ondoban_button_html =
		'<wbr/><div style="margin-top:3px"><input type="button"'.
		' onclick="parent.location.href=\'sot_worksheet_ondoban.php?'.
		'session='.$session.'&date_y='.@$yy.'&date_m='.@$mm.'&date_d='.@$dd.'&sel_ptif_id='.$pt_id.'\'; return false;"'.
		' style="width:70px;" value="温度板"></div>';

	?>

  <table border="0" cellspacing="0" cellpadding="0">
  <tr height="22">
  <td width="110" align="center" bgcolor="#156cec"><a href="./summary_rireki_table.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>医療文書</b></font></a></td>
  <td width="5">&nbsp;</td>
  <td width="110" align="center" bgcolor="#63bafe"><a href="#" onclick="dosubmit();" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">検査時系列</font></a></td>
  <td width="5">&nbsp;</td>
  <td width="110" align="center" bgcolor="#63bafe"><a href="summary_disease_list.php?session=<?=$session?>&pt_id=<?=$pt_id?>" style="text-decoration:none;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#0055dd">傷病名一覧</font></a></td>
  </tr>
  </table>

<form name="frm" action="summary_kensakekka_timeseries.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>">
<input type="hidden" style="width:100%;" name="itm_ord" id="itm_ord" value="">
</form>

  <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
  <td bgcolor="#156cec"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
  </tr></table>

<?
// 日付UI
$date_y1 = substr($s_date, 0, 4);
$date_m1 = substr($s_date, 4, 2);
$date_d1 = substr($s_date, 6, 2);
?>
  <table align="right">
  <tr height="24">
  <td style="padding-right:2px;"><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($submode == '') { ?>
<div style="float:left;">
  <input type="button" value="&lt;&lt;" onclick="changeDateTo('<? echo(date("Ymd", strtotime("-5 days", strtotime($s_date)))); ?>');">
  <input type="button" value=" &lt; " onclick="changeDateTo('<? echo(date("Ymd", strtotime("-1 day", strtotime($s_date)))); ?>');">
  <select id="date_y1"><? show_select_years_span(min($date_y1, 1970), max($date_y1, date("Y") + 1), $date_y1, false); ?></select>/<select id="date_m1"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1"><? show_select_days($date_d1, true); ?></select>
</div>
<div style="float:left;margin-left:2px;margin-right:3px;">
  <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"><br><div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</div>
<div style="float:left;">
  <input type="button" value=" &gt; " onclick="changeDateTo('<? echo(date("Ymd", strtotime("+1 day", strtotime($s_date)))); ?>');">
  <input type="button" value="&gt;&gt;" onclick="changeDateTo('<? echo(date("Ymd", strtotime("+5 days", strtotime($s_date)))); ?>');">
  <input type="button" value="本日" onclick="changeDateTo('<? echo(date("Ymd")); ?>');">
  <input type="button" value="表示" onclick="changeDate();">
  <?=@$submode_link?>
</div>
<? } else if ($submode == 'new') { ?>
  <?=@$submode_link?>
<? } ?>
  </font></td>
  </tr>
  </table>


  <table id="rireki_table" class="prop_table" cellspacing="1" style="clear:both;">

  <? //=================================== ?>
  <? //マトリックス表（一行目:日付行）     ?>
  <? //=================================== ?>
  <tr align="center">
    <th height="24"><br/></th>

    <? // 時系列情報モード ?>
    <? if ($submode == '') { ?>

      <? for($i=0;$i<5;$i++){ ?>
      <?   $date = $cre_date[$i]; ?>
      <?   $disp_date = substr($date,2,2)."/".substr($date,4,2)."/".substr($date,6,2); ?>

      <th style="width:15%">
      <div>
      <?   if($disp_date != "//"){ ?>
        <a href="summary_rireki_naiyo.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&date=<?=$date?>" target="hidari"><?=$disp_date?></a>
      <?   } ?>
      </div>
      <div><img src="img/spacer.gif" style="width:50px; height:1px" alt=""></div>
      <? } ?>
      </th>

    <? // 最新情報モード ?>
    <? } else if ($submode == 'new'){ ?>
      <th style="width:75%">
      <div>最新情報</div>
      <div><img src="img/spacer.gif" style="width:50px; height:1px" alt=""></div>
      </th>

    <? } ?>

    </tr>

<?
	//===================================
	//テンプレートを使用していない診療記録の表示対象テンプレート名
	//===================================
	$disp_tmpl_name = 'フリー入力' ;

	$w_chktbl = array() ;
	$chktbl = array() ;
	//===================================
	//マトリックス表を作成（２行目以降）
	//===================================
	for($j=0;$j<$divnum;$j++){
		echo("<tr align=\"center\">\n");

		//===================================
		//行ヘッダの出力
		//===================================
		echo("<th align=\"center\" height=\"60\" valign=\"middle\">\n");

		// 診療記録区分
		if(@$mode == ""){
			$diag_div = pg_result($divsel,$j,"diag_div");
			$div_modify_auth_stid_list = explode(",", pg_result($divsel, $j, "div_modify_auth_stid_list"));
			$div_modify_auth_flg = pg_result($divsel, $j, "div_modify_auth_flg");
			echo("<a href=\"summary_rireki_naiyo.php?session=".$session."&pt_id=".$pt_id."&emp_id=".$emp_id."&diag_div=".urlencode($diag_div)."&from_date=".$yoko_s_date."&to_date=".$yoko_e_date."\" target=\"hidari\">");
			echo($diag_div."</A><BR>\n");

			if (pg_result($divsel, $j, "div_show_link_flowsheet_button")){
				echo('<div style="margin-top:3px">');
				echo('<input type="button" onclick="show_flowsheet(\''.urlencode($diag_div).'\'); return false;" value="フローシート"></div>');
			}
			if (pg_result($divsel, $j, "div_show_link_ondoban_button")){
				echo $ondoban_button_html;
			}

		// 診療科
		} else if (@$mode == "section") {
			$sect_id = pg_fetch_result($divsel, $j, "sect_id");
			$sect_nm = pg_fetch_result($divsel, $j, "sect_nm");
			echo("<a href=\"summary_rireki_naiyo.php?session=$session&amp;pt_id=$pt_id&amp;emp_id=$emp_id&amp;sect_id=$sect_id&amp;from_date=$yoko_s_date&amp;to_date=$yoko_e_date\" target=\"hidari\">$sect_nm</a>\n");
		// プロブレム
		}else{
			$problem = pg_result($divsel,$j,"problem");
			echo("<a href=\"summary_rireki_naiyo.php?session=".$session."&pt_id=".$pt_id."&emp_id=".$emp_id."&problem=".urlencode($problem)."&from_date=".$yoko_s_date."&to_date=".$yoko_e_date."\" target=\"hidari\">".$problem."</a>\n");
		}
		echo("</th>\n");


		// 時系列情報モード
		if ($submode == '') {

			// 診療記録区分
			if(@$mode == ""){
				$sql2 = "select distinct * from summary where (ptif_id = '$pt_id') and (cre_date between '{$cre_date[0]}' and '{$cre_date[4]}') and (diag_div like '$diag_div') order by problem,summary_id";
			// 診療科
			} else if (@$mode == "section") {
				$sql2 = "select distinct * from summary where (ptif_id = '$pt_id') and (cre_date between '{$cre_date[0]}' and '{$cre_date[4]}') and (sect_id = '$sect_id') order by problem,summary_id";
			// プロブレム
			}else{
				$sql2 = "select distinct * from summary where (ptif_id = '$pt_id') and (cre_date between '{$cre_date[0]}' and '{$cre_date[4]}') and (problem = '$problem') order by diag_div,summary_id";
			}

		// 最新情報モード
		} else if ($submode == 'new') {

			$sql2 = "";
			$paramvalue = @$diag_div; // 診療記録区分
			if (@$mode == "section") $paramvalue = @$sect_id; // 診療科
			if (@$mode == "problem") $paramvalue = @$problem; // プロブレム
			foreach ((array)@$sel_last_orders[$paramvalue] as $key => $row){
				if ($sql2 != "") $sql2 .= " union";
				$sql2 .=
					" select * from summary".
					" where summary_id = ".(int)$row["max_summary_id"].
					" and tmpl_id = ".(int)$row["tmpl_id"] .
					" and ptif_id = '".pg_escape_string($pt_id)."'";
			}
		}

		if ($sql2 != "") {
			$sel = select_from_table($con,$sql2,"",$fname);
			if($sel == 0){ pg_close($con); exit; }
			$rows = pg_fetch_all($sel);
		} else {
			$rows = array();
		}

		$loopcnt = ($submode == 'new') ? 1: 5;
		for($i=0;$i<$loopcnt;$i++){

			$n = 0;
			foreach ($rows as $key => $row){
				if ($submode == '') {
					if ($row["cre_date"] == $cre_date[$i]) {
    					$w_chktbl[$cre_date[$i]][$n++] = $row ;
					}
				} else if ($submode == 'new') {
					$w_chktbl["new"][$n++] = $row ;
				}
			}

			//===================================
			//マトリックスセル内を出力
			//===================================
			echo("<td align=\"left\" valign=\"middle\" height=\"60\">\n");

			$chknum = $n;
			if($chknum <= 0){
				echo("<br/></td>\n");
				continue;
			}

			$workdiv = "";
			for($k=0; $k<$chknum; $k++){

				if ($submode == '') {
					$chktbl = $w_chktbl[$cre_date[$i]][$k];
				} else if ($submode == 'new') {
					$chktbl = $w_chktbl["new"][$k];
				}

				require_once("summary_tmpl_util.ini");
				$summary_id = $chktbl["summary_id"];
				$emp_id2     = $chktbl["emp_id"];
				$summary_seq = $chktbl["summary_seq"];
				// @@@ 表示用HTMLデータはsummaryテーブルではなくsum_xmlテーブルから取得するように変更 (2011/02/24) @@@
				$naiyoDisp = h(get_summary_naiyo_html_from_sum_xml2($con,$fname,$summary_id,$pt_id,$summary_seq)) ;
				$naiyoDisp = str_replace("'","\'", str_replace("\"","&#34;", $naiyoDisp));
				$emp_namePop = get_emp_kanji_name($con,$chktbl["emp_id"],$fname);

				//===================================
				//診療記録区分単位で表示する場合に、プロブレムの一覧を表示
				//===================================
				$title_flg = "1";
				$problemPop = $chktbl["problem"];
				$seldiv_or_pro = $chktbl["diag_div"];
				if(@$mode == ""){ // 診療記録区分
					$tmpl_id = $chktbl["tmpl_id"];
					//if ($chktbl["problem_id"] == 0 && $tmpl_id != "") {
					$problemPop = $arr_tmplname[$tmpl_id];
					$title_flg = "2";
					//}
					if ($problemPop == "") $problemPop = $disp_tmpl_name ;
					$seldiv_or_pro = $problemPop;
					if($submode == ''){
						//4文字程度で省略表示する。
						if(mb_strlen($seldiv_or_pro, "EUC-JP")>5) $seldiv_or_pro = mb_substr($seldiv_or_pro,0,5, "EUC-JP");
					}
				}
				if(@$mode == "problem" || @$mode=="section"){ // 診療科またはプロブレム
					$tmpl_id = $chktbl["tmpl_id"];
					//if ($chktbl["problem_id"] == 0 && $tmpl_id != "") {
					$seldiv_or_pro = $arr_tmplname[$tmpl_id];
					//}
					if ($seldiv_or_pro == "") $seldiv_or_pro = $disp_tmpl_name ;
					if($submode == ''){
						//4文字程度で省略表示する。
						if(mb_strlen($seldiv_or_pro, "EUC-JP")>5) $seldiv_or_pro = mb_substr($seldiv_or_pro,0,5, "EUC-JP");
					}
				}

				$should_popup_files = false;
				if ($tmpl_id != "") {
					$files = array();
					$sql =
						" select apply_id from sum_apply".
						" where summary_id = ".(int)$summary_id .
						" and ptif_id = '".pg_escape_string($pt_id)."'".
						" and delete_flg = 'f' order by apply_id desc limit 1";
					$sum_apply = select_from_table($con, $sql, "", $fname);
					if (pg_num_rows($sum_apply) > 0) {
						$should_popup_files = true;
						$apply_id = (int)@pg_fetch_result($sum_apply, 0, "apply_id");
						$sql = "select applyfile_name from sum_applyfile where apply_id = ".(int)$apply_id;
						$sel1 = select_from_table($con, $sql, "", $fname);
						while($row1 = pg_fetch_array($sel1)){
							$files[] = $row1["applyfile_name"];
						}
					}
					$filesPop = implode("<br>", $files);
				}

				echo("<span onmousemove=\"popupNaiyo('$popup_copy_message','$problemPop','$emp_namePop','$naiyoDisp', event, '$title_flg');\"");
				echo("onmouseout=\"closeDetail(); resetIframeHeight();\">");
				if (!@$div_modify_auth_flg || in_array($emp_job_id, $div_modify_auth_stid_list)){
					echo "<img src=\"img/summary_rireki_copy.gif?\"";
					echo " style=\"vertical-align:baseline; cursor:pointer\" onclick=\"summary_copy('".$summary_id."')\" alt=\"\">";
				}
				echo("</span>&nbsp");
				echo("<A href='summary_rireki_naiyo.php?session=".$session."&pt_id=".$pt_id."&emp_id=".$emp_id."&emp_id2=".$emp_id2."&summary_id=".$summary_id."&summary_seq=".$summary_seq."' style=\"text-decoration:none; font-size:12px\" ");
				if ($should_popup_files) {
					echo("onmousemove=\"popupNaiyoWithFiles('$popup_detail_message','$problemPop','$emp_namePop','$filesPop','$naiyoDisp', event, '$title_flg');\"");
				} else {
					echo("onmousemove=\"popupNaiyo('$popup_detail_message','$problemPop','$emp_namePop','$naiyoDisp', event, '$title_flg');\"");
				}
				echo("onmouseout=\"closeDetail(); resetIframeHeight();\"");
				echo("onclick=\"popup_visible=false;closeDetail(); resetIframeHeight();\">");
				echo($seldiv_or_pro);
				if ($submode =='') {
					echo("</A><br/>\n");
				} else if ($submode =='new') {
					if ((($k+1) % 4) == 0) {
						echo("</A><br/>\n");
					} else {
						echo("</A>&nbsp;&nbsp;&nbsp;\n");
					}
				}
			}
			echo("</td>\n");
		}
		echo("</tr>\n");
	}
	echo("</table>\n");
?>

</body>
</html>
<? pg_close($con); ?>
