<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 退院先マスタを取得
$out_pos_master = get_out_pos_master($con, $fname);

// 転院者リストを作成
$sql  = "select i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, i.inpt_out_dt, w.ward_name, i.inpt_lt_kj_nm, i.inpt_ft_kj_nm, d.dr_nm, i.inpt_out_pos_rireki, i.inpt_out_pos_cd, i.inpt_out_pos_dtl, i.inpt_back_change_purpose, i.out_updated from inpthist i ";
$sql .= "inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd ";
$sql .= "left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id";
$cond  = "where i.inpt_back_ward_cd is not null ";
$cond .= "and not exists (select * from inptmst n where n.ptif_id = i.ptif_id and (n.inpt_in_dt > i.inpt_out_dt or (n.inpt_in_dt = i.inpt_out_dt and n.inpt_in_tm > i.inpt_out_tm))) ";
$cond .= "and not exists (select * from inpthist n where n.ptif_id = i.ptif_id and (n.inpt_in_dt > i.inpt_out_dt or (n.inpt_in_dt = i.inpt_out_dt and n.inpt_in_tm > i.inpt_out_tm))) ";
$cond .= "order by i.inpt_out_dt, i.inpt_out_tm";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$changes = array();
while ($row = pg_fetch_array($sel)) {
	$changes[] = array(
		"out_dt" => $row["inpt_out_dt"],
		"ward_name" => $row["ward_name"],
		"pt_nm" => $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"],
		"dr_nm" => $row["dr_nm"],
		"operators" => get_operators($con, $row["ptif_id"], $row["inpt_in_dt"], $row["inpt_in_tm"], $fname),
		"out_pos" => format_out_pos($out_pos_master, $row["inpt_out_pos_rireki"], $row["inpt_out_pos_cd"], $row["inpt_out_pos_dtl"]),
		"purpose" => $row["inpt_back_change_purpose"],
		"updated" => $row["out_updated"]
	);
}

unset($sel);
?>
</head>
<body>
<table border="1">
<tr bgcolor="#CCFFFF">
<td nowrap align="center">NEW</td>
<td nowrap align="center">転院月日</td>
<td nowrap align="center">移動病棟</td>
<td nowrap align="center">氏名</td>
<td nowrap align="center">主治医</td>
<td nowrap align="center">担当</td>
<td nowrap align="center">転院先</td>
<td nowrap align="center">目的</td>
</tr>
<?
$new_start = date("YmdHis", strtotime("-1 day"));
foreach ($changes as $change) {
?>
<tr>
<td nowrap align="center"><? echo(format_new($new_start, $change["updated"])); ?></td>
<td nowrap align="center"><? echo(format_date($change["out_dt"])); ?></td>
<td nowrap><? echo($change["ward_name"]); ?></td>
<td nowrap><? echo($change["pt_nm"]); ?></td>
<td nowrap><? echo($change["dr_nm"]); ?></td>
<td nowrap><? echo(format_operators($change["operators"])); ?></td>
<td nowrap><? echo($change["out_pos"]); ?></td>
<td nowrap><? echo(format_text($change["purpose"])); ?></td>
</tr>
<?
}
?>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
$excel_data = mb_convert_encoding(ob_get_clean(), "Shift_JIS", 'EUC-JP');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=change_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);

function get_out_pos_master($con, $fname) {
	$sql = "select rireki_index, item_cd, item_name from tmplitemrireki";
	$cond = "where mst_cd = 'A307' and disp_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["rireki_index"]][$row["item_cd"]] = $row["item_name"];
	}
	return $master;
}

function get_operators($con, $ptif_id, $in_dt, $in_tm, $fname) {
	$sql = "select e.emp_lt_nm, e.emp_ft_nm from inptophist i inner join empmst e on e.emp_id = i.emp_id";
	$cond = "where i.ptif_id = '$ptif_id' and i.inpt_in_dt = '$in_dt' and i.inpt_in_tm = '$in_tm' and exists (select * from jobmst j where j.job_id = e.emp_job and j.bed_op_flg) order by i.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
	}
	return $operators;
}

function format_new($new_start, $updated) {
	if ($updated >= $new_start) {
		return "NEW";
	}
}

function format_date($ymd) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $ymd);
}

function format_operators($operators) {
	return join("<br>", $operators);
}

function format_out_pos($out_pos_master, $out_pos_rireki, $out_pos_cd, $out_pos_dtl) {
	if ($out_pos_master[$out_pos_rireki][$out_pos_cd] == "" && $out_pos_dtl == "") {
		return "";
	}
	$buf = $out_pos_master[$out_pos_rireki][$out_pos_cd];
	if ($out_pos_dtl != "") $buf .= "（{$out_pos_dtl}）";
	return trim($buf);
}

function format_text($text) {
	return str_replace("\r\n", "<br>", $text);
}
?>
