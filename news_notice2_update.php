<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require_once("referer_common.ini");

$fname = $_SERVER['PHP_SELF'];
$notice_id = $_REQUEST['notice_id'];

// セッションのチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// 通達区分名を取得
$sql = "select notice2_name from newsnotice2 where notice2_id = " . p($notice_id);
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$notice_name = pg_fetch_result($sel, 0, "notice2_name");

// お知らせ集計機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_aggregate = $conf->get('setting_aggregate');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_aggregate != "t") {
    $setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | お知らせ区分詳細</title>
<? include_js("js/fontsize.js"); ?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<?=h($session);?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?=h($session);?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?=h($session);?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?=h($session);?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<?=h($session);?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<?=h($session);?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="news_notice2_update.php?session=<?=h($session);?>&notice_id=<?=h($notice_id);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>お知らせ区分<br>詳細</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_option.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="5">&nbsp;</td>
<?php if($setting_aggregate == 't'){?>
	<td width="70" align="center" bgcolor="#bdd1e7">
		<a href="news_aggregate.php?session=<?=h($session);?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font></a>
	</td>
<? } ?>
<td width="">&nbsp;</td>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10">

<form action="news_notice2_update_confirm.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分</font></td>
<td><input type="text" name="notice_name" value="<?=h($notice_name);?>" size="30" maxlength="30" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?=h($session); ?>">
<input type="hidden" name="notice_id" value="<?=h($notice_id); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
