<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜管理指標</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("jnl_application_imprint_common.ini");
require("./conf/sql.inf");

require_once("jnl_application_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
  exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,80,$fname);
if($wkfw=="0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, 81, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);

require_once('jnl_indicator_return_data.php');
$indicatorData = new IndicatorReturnData(array('fname' => $fname, 'con' => $con));
$noArr = $indicatorData->getNoForNo($no);

?>



<script type="text/javascript">
<!--
//Year selected from Drop down
function selectYear(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('_plant');
    plant.name = 'plant';
    subCategory  = document.getElementById('sub_category');
    displayYear  = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';

        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayYear.value  = e.value;

    wkfwForm.submit();
}

function selectMonth(e, gCNum, gSCNum, gNo, gType, gPlant) {
    wkfwForm = document.getElementById('wkfw');
    wkfwForm.action = '<?= $noArr['file'] ?>';
    wkfwForm.method = 'get';

    type = document.getElementById('type');
    category = document.getElementById('category');
    plant = document.getElementById('_plant');
    plant.name = 'plant';
    subCategory  = document.getElementById('sub_category');
    displayYear  = document.getElementById('display_year');
    displayMonth = document.getElementById('display_month');
    displayDay   = document.getElementById('display_day');

    if ((type!=null && type.value=='plant') || (category!=null && category.value==0) && (subCategory!=null && subCategory.value=='')) {
        if (plant!= null && plant.value!=null) {
            hiddenType = document.getElementById('hidden_type');
            category.value = hiddenType.value;
            type.value = 'plant';

        }
        else {
            category.value = '0';
            category.parentNode.removeChild(category);
            subCategory.parentNode.removeChild(subCategory);
        }
    }
    else if (category==null) {
        alert(category.value);
        alert(subCategory.value);
        alert(no.value);
    }
    else if (subCategory==null) {
        alert(category.value);
    }
    else {
        category.parentNode.removeChild(category);
        subCategory.parentNode.removeChild(subCategory);
    }

    displayMonth.value  = e.value;

    wkfwForm.submit();
}



//Export button clicked

function clickDownLoad() {
  wkfwForm = document.getElementById("wkfw");
  wkfwForm.action = 'jnl_indicator_return_analysis_excel.php';
  wkfwForm.method = 'get';
   plant = document.getElementById('_plant');
    plant.name = 'plant';

  wkfwForm.submit();
}

//-->
</script>
<script type="text/javascript" src="js/jnl/jnl_indicator_menu_list.js"></script>
<script type="text/javascript" src="js/jnl/jnl_indicator_ajax.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
<style type="text/css">
.this_table {border: medium solid 1px #5279A5;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="決裁・管理指標"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_application_indicator_menu.php?session=<? echo($session); ?>"><b>管理指標</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?
show_applycation_menuitem($session,$fname,"");
?>

</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form id="wkfw" name="wkfw" action="jnl_application_imprint.php" method="post" enctype="multipart/form-data">
    <input type="hidden" id="category" name="category" value="<?= $category?>" />
    <input type="hidden" id="sub_category" name="sub_category" value="<?= $sub_category?>" />
<?php
if (!empty($plant)) {
?>
    <!--<input type="hidden" id="plant" name="plant" value="<?= $plant?>" />
    --><input type="hidden" id="type" name="type" value="<?= $type?>" />
    <input type="hidden" id="hidden_type" name="hidden_type" value="<?= $category?>" />
<?php
}
?>
    <input type="hidden" id="no" name="no" value="<?= $no?>" />
<?php
require_once('jnl_indicator_return_display.php');
$indicatorDisplay = new IndicatorReturnDisplay(array('con' => $con, 'session' => $session, 'display_year' => $display_year, 'display_month' => $display_month));

/* menu list */
echo $indicatorDisplay->getMenuList(
    array('category' => $category, 'sub_category' => $sub_category, 'no' => $no, 'plant' => $plant, 'type' => $type)
  , null
  , null
  , $emp_id
);

        $selectData =  $indicatorData->getHospitalList();



if($display_year==null || $display_month==null  ){
	
	$display_month=date("m", strtotime("-1 month"));
	$display_year=date("Y", strtotime("-1 month"));
        //$display_month=date("m");
        //$display_year=date("Y");

    }
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>
<?php
# FIXME a 表示年月日指定用 select 作成途中
# debug
//echo $indicatorDisplay->getSelectDislpayYear(array(
//    'display_year' => $display_year,
//    'category'     => $category,
//    'sub_category' => $sub_category,
//    'no'           => $no,
//    'type'         => $type,
//    'plant'        => $plant,
//));



$fontArr = $indicatorDisplay->getFont();
$fontArr['color'] = $indicatorDisplay->getRgbColor('ash');
$fontTag = $indicatorDisplay->getFontTag($fontArr);
?>
                <b><span style="<?= $indicatorDisplay->getRgbColor('yellow', 'background-color') ?>" ><?= sprintf($fontTag, '申請中') ?></span></b>
                <b><span style="<?= $indicatorDisplay->getRgbColor('red', 'background-color') ?>" ><?= sprintf($fontTag, '日報未入力') ?></span></b>
            </td>
        </tr>
<tr>

        <td align="left">
        <?php
        # FIXME a 表示年月日指定用 select 作成途中
        # debug
        //var_dump(array('display_year'=> $display_year,'category'=> $category,'sub_category'=> $sub_category,'no'=> $no,'type'=> $type,'plant'=> $plant,));
        //Get the Year drop down
        echo $indicatorDisplay->getSelectDislpayYear(array(
            'display_year' => $display_year,
            'category'     => $category,
            'sub_category' => $sub_category,
            'no'           => $no,
            'type'         => $type,
            'plant'        => $plant,
        ));
        ?>
        <?php
        //Get the Month drop down
        echo $indicatorDisplay->getSelectDislpayMonth(array(
            'display_month' => $display_month,
            'category'     => $category,
            'sub_category' => $sub_category,
            'no'           => $no,
            'type'         => $type,
            'plant'        => $plant,
        ));
//        $fontArr = $indicatorDisplay->getFont();
//        $fontArr['color'] = '#888888';
//        $fontTag = $indicatorDisplay->getFontTag($fontArr);
        $fontTag = $indicatorDisplay->getFontTag();

?>
        </td>
        <td align="right">
                <input type="button" value="Excel出力" onclick="clickDownLoad();">
            </td>
        </tr>
   </tbody>
</table>
<?php




 $hspName=$indicatorData->getHospitalList($plant);

$dataPointAcc=$indicatorData->getjnlIndicatorAnalysisPointAccount(array('year' => $display_year, 'month' => $display_month), $plant);

echo '<table  width="100%">';
echo '<tr><td><table border="1" class="list this_table" >';
$lastMonth = sprintf('%02s', date('m',mktime(0, 0, 0, $display_month-1, 1, $display_year)));
echo '<tr><td align="right" >'.sprintf($fontTag, '病院名： '.$hspName[0]['facility_name']).'<td align="right" >'.sprintf($fontTag, sprintf('%04s', date('Y',mktime(0, 0, 0, $display_month, 1, $display_year))).'年'. $display_month. '月').'</td></tr>';
echo '<tr><td align="right" >'.sprintf($fontTag, $lastMonth.'月分総件数').'</td><td align="right" >'.sprintf($fontTag, number_format($dataPointAcc[0]['total_account'],0,".",",")).'</td><td align="left" > '.sprintf($fontTag, '件').'</td></tr>';
echo '<tr><td align="right" >'.sprintf($fontTag, '総点数').'</td><td align="right" >'.sprintf($fontTag, number_format($dataPointAcc[0]['total_point'],0,".",",")).'</td><td align="left" >'.sprintf($fontTag, '点').'</td></tr>';
echo '</table></td></tr>';
echo '<tr><td width="50%" align="left" VALIGN="top">';
echo $indicatorDisplay->getReturnAnalysisTable1DisplayData(
    $indicatorData->getjnlIndicatorReturnAnalysisTable1( array('year' => $display_year, 'month' => $display_month), $plant),
$indicatorData->getjnlIndicatorReturnAnalysisTable1LastRowCol1( array('year' => $display_year, 'month' => $display_month), $plant),
$indicatorData->getjnlIndicatorReturnAnalysisTable1LastRowCol2( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getHospitalList($plant),
    $noArr,
array('year' => $display_year,'month' => $display_month)
);
echo '</td><td width="4%"></td><td>';
echo $indicatorDisplay->getReturnAnalysisTable4DisplayData(
    $indicatorData->getjnlIndicatorReturnAnalysisTable4( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getHospitalList($plant),
    $noArr,
array('year' => $display_year,'month' => $display_month)
);
echo '</td><tr height="10px"><td></td></tr></tr>';
echo '<tr><td align="left" VALIGN="top">';
echo $indicatorDisplay->getReturnAnalysisTable2DisplayData(
    $indicatorData->getjnlIndicatorReturnAnalysisTable2( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getHospitalList($plant),
    $noArr,
array('year' => $display_year,'month' => $display_month)
);
echo '</td><td width="5%"></td><td>';
echo $indicatorDisplay->getReturnAnalysisTable5DisplayData(
    $indicatorData->getjnlIndicatorReturnAnalysisTable5A( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getjnlIndicatorReturnAnalysisTable5B( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getHospitalList($plant),
    $noArr
);
echo '</td><tr height="10px"><td></td></tr></tr>';
echo '<tr><td align="left" VALIGN="top">';

/* */
echo $indicatorDisplay->getReturnAnalysisTable3DisplayData(
    $indicatorData->getjnlIndicatorReturnAnalysisTable3( array('year' => $display_year, 'month' => $display_month), $plant),
    $indicatorData->getHospitalList($plant),
    $noArr,
array('year' => $display_year,'month' => $display_month)
);
echo '</td></tr>';
echo '</table>';
?>

<input type="hidden" name="session" value="<? echo($session); ?>">

</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>

</html>
