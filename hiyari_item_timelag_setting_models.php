<?php
// +----------------------------------------------------------------------+
// | PHP version 4, 5                                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2011-2011 Masahiko Jinno                               |
// +----------------------------------------------------------------------+
//
// 発見時間 〜 報告時間のタイムラグに関する関数郡



require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("get_values.php");
require_once("hiyari_report_class.php");
require_once("hiyari_item_time_setting_models.php");


/**
 * 発見時間 〜 報告時間の設定情報を取得する。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 表示設定情報
 */
function getDispTimelagSetting($con, $fname) {
    $sql = "select * from inci_timelag_setting";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $result = pg_fetch_assoc($sel);

    return $result['disp_report_timelag'] == 't' ? true : false ;
}

/**
 * 発見時間 〜 報告時間の設定情報を更新します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param boolean $disp_report_timelag タイムラグ表示フラグ
 */
function setDispTimelagSetting($con, $fname, $disp_report_timelag) {

    $disp_report_timelag_flg    = ($disp_report_timelag) ? "t":"f";

    $sql = 'update inci_timelag_setting set';
    $set = array( 'disp_report_timelag' );
    $setvalue = array( $disp_report_timelag_flg );
    $cond = '';
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);

    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/**
 *  タイムラグ区分を取得する関数
 *  @param Object  $con             DBコネクション
 *  @param String  $fname           実行ファイル名
 *  @param Integer $now_time        タイムスタンプ（第一報時間）
 *  @param Array   $input           報告項目の配列
 * 
 *  @return String $inputs_105_70   タイムラグ区分 00 〜 07
 */
function getTimelagClass($con, $fname, $time_stamp, $inputs ) {
    

    // *** 発見・発生日時のモード設定 ***
    // 設定情報を取得
    $time_setting_flg = getTimeSetting($con, $PHP_SELF);

    $inputs_105_70 = '';

    
    // 設定取得
    $sql = "select * from inci_timelag_setting";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // データ取得
    $result = pg_fetch_assoc($sel);

    // 発生日時の入力方法が時刻入力である
    // かつ発見時刻：時が未入力でない
    // かつ発見時刻：分が未入力でない
    // かつ発見日付が未入力でない
	if ( $time_setting_flg === false
		&& isset($inputs['_105_60'])
		&& isset($inputs['_105_65'])
		&& isset($inputs['_105_5'])
		&& (string)$inputs['_105_5'] !== ''
		&& (string)$inputs['_105_60'] !== ''
		&& (string)$inputs['_105_65'] !== ''
        ) {

        // 発見時刻（タイムスタンプ）取得
        $discovery_time = mktime(
            $inputs['_105_60'],
            $inputs['_105_65'],
            0,                                  
            substr($inputs['_105_5'], 5, 2),
            substr($inputs['_105_5'], 8, 2), 
            substr($inputs['_105_5'], 0, 4) 
        );

        // タイムラグ：タイムスタンプ
        $timelag_stamp = $time_stamp - $discovery_time;

        // タイムラグ：経過分
        $timelag_progress_i = floor( $timelag_stamp / 60 ) ;

        // タイムラグ1時間未満
        if ( $timelag_progress_i < ( $result['time_1'] * 60 ) ) {
            $inputs_105_70 = '00';
        }

        // タイムラグ3時間未満
        else if($timelag_progress_i >= ( $result['time_1'] * 60 ) && $timelag_progress_i < ( $result['time_2'] * 60 ) ) {
            $inputs_105_70 = '01';
            
        }

        // タイムラグ6時間未満
        else if($timelag_progress_i >= ( $result['time_2'] * 60 ) && $timelag_progress_i < ( $result['time_3'] * 60 ) ) {
            $inputs_105_70 = '02';
            
        }

        // タイムラグ12時間未満
        else if($timelag_progress_i >= ( $result['time_3'] * 60 ) && $timelag_progress_i <= ( $result['time_4'] * 60 ) ) {
            $inputs_105_70 = '03';
            
        }

        // タイムラグ24時間未満
        else if($timelag_progress_i >= ($result['time_4'] * 60 ) && $timelag_progress_i <= ( $result['time_5'] * 60 ) ) {
            $inputs_105_70 = '04';
            
        }

        // タイムラグ48時間未満
        else if($timelag_progress_i >= ( $result['time_5'] * 60 ) && $timelag_progress_i <= ( $result['time_6'] * 60 ) ) {
            $inputs_105_70 = '05';
            
        }

        // タイムラグ72時間未満
        else if($timelag_progress_i >= ( $result['time_6'] * 60 ) && $timelag_progress_i <= ( $result['time_7'] * 60 ) ) {
            $inputs_105_70 = '06';
            
        }

        // タイムラグ72時間以上
        else if($timelag_progress_i > ( $result['time_7'] * 60 ) ) {
            $inputs_105_70 = '07';
            
        }
    }

    return $inputs_105_70;
}

/**
 *  タイムラグを取得する関数
 *  @param Object  $con             DBコネクション
 *  @param String  $fname           実行ファイル名
 *  @param Integer $now_time        タイムスタンプ（第一報時間）
 *  @param Array   $input           報告項目の配列
 * 
 *  @return String $inputs_105_75   タイムラグ区分 00 〜 07
 */
function getTimelag($con, $fname, $time_stamp, $inputs ) {
    
    // *** 発見・発生日時のモード設定 ***
    // 設定情報を取得
    $time_setting_flg = getTimeSetting($con, $PHP_SELF);

    // タイムラグ 形式[ D+,HH,MM,SS ]
    $inputs_105_75 = '';

    // 発生日時の入力方法が時刻入力である
    // かつ発見時刻：時が未入力でない
    // かつ発見時刻：分が未入力でない
    // かつ発見日付が未入力でない
    if (   $time_setting_flg === false 
        && isset($inputs['_105_60']) 
        && isset($inputs['_105_65'])
        && isset($inputs['_105_5'])
		&& (string)$inputs['_105_5'] !== ''
		&& (string)$inputs['_105_60'] !== ''
		&& (string)$inputs['_105_65'] !== ''
        ) {

        // 発見時刻（タイムスタンプ）取得
        $discovery_time = mktime(
            $inputs['_105_60'],
            $inputs['_105_65'],
            0,                                  
            substr($inputs['_105_5'], 5, 2),
            substr($inputs['_105_5'], 8, 2), 
            substr($inputs['_105_5'], 0, 4) 
        );

        // タイムラグ
        $timelag = $time_stamp - $discovery_time;

        // タイムラグを設定
        $inputs_105_75 = $timelag;
    }
	
    // 発見日時が入力されていない場合の処理
    else {
        // タイムラグを設定
        $inputs_105_75 = -1;
    }

    return $inputs_105_75;
}

/**
 *  タイムラグ(表示)を取得する関数
 *  @param integer  $timelag_stamp       タイムラグ（秒）
 * 
 *  @return String $timelag_view    タイムラグ表示テキスト 形式 "%d時間 %02分" 
 */
function getTimelagView($timelag) {
    
    if ($timelag >= 0) {
        // タイムラグ：時
        $timelag_h = $timelag / 60 / 60;

        // タイムラグを設定
        $timelag_view = sprintf("%.2f時間", $timelag_h);
    }
    else {
        $timelag_view = '発見時刻を正しく入力してください。';
    }

    return (string)$timelag_view;
}