<?php
// 出張旅費
// 新幹線駅名マスタ・フォーム登録
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// 入力項目の確認
if (empty($_REQUEST["title"])) {
	js_alert_exit('駅名が入力されていません。');
}
if (strlen($_REQUEST["title"]) > 100) {
	js_alert_exit('駅名は100文字以内に収めて下さい。');
}
if (!empty($_REQUEST["id"]) and !is_numeric($_REQUEST["id"])) {
	js_alert_exit('IDが異常です。もう一度最初からやり直して下さい。');
}


// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 追加
if (empty($_REQUEST["id"])) {
	$sql = "SELECT MAX(station_id) FROM trip_station";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		js_error_exit();
	}
	$id = pg_fetch_result($sel, 0, 0) + 1;

	$sql = "insert into trip_station (station_id,title,no,created_on,updated_on) values (";
	$content = array(
		$id,
		p($_REQUEST["title"]),
		$id,
		date('Y-m-d H:i:s'),
		date('Y-m-d H:i:s')
	);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 更新
else {
	$set = array(
		"title" => p($_REQUEST["title"]),
		"updated_on" => date('Y-m-d H:i:s')
	);
	$sql = "UPDATE trip_station SET ";
	$cond = "WHERE station_id={$_REQUEST["id"]}";
	$upd = update_set_table($con, $sql, array_keys($set), array_values($set), $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

?>
<script type="text/javascript">
location.href = 'workflow_trip_station.php?session=<?=$session?>';
</script>