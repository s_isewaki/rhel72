<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 月</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_month_common.ini");
require("holiday.php");
require("label_by_profile_type.ini");
require("show_calendar_memo.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
$this_month = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
if ($date == "") {
	$date = $this_month;
}

// データベースに接続
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// アクセスログ
$optsql = "select facility_default from option";
$optcond = "where emp_id = '{$emp_id}'";
$optsel = select_from_table($con, $optsql, $optcond, $fname);
if ($optsel == 0) {
    pg_close($con);
    js_error_exit();
}
$facility_default = pg_fetch_result($optsel, 0, "facility_default");

if ($facility_default === '3') {
	aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);
}

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 前月・翌月のタイプスタンプを変数に格納
$last_month = get_last_month($date);
$next_month = get_next_month($date);

// 患者氏名/利用者氏名
$patient_name_title = $_label_by_profile["PATIENT_NAME"][$profile_type];
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.view.date.value = dt;
	document.view.submit();
}

function popupReservationDetail(facility, date, time, title, type, text, user_count, reg_emp, upd_emp, e, with_patient, patient, show_type, ext, tmp_class) {

	var arr_value = new Array(
			'施設・設備', facility,
			'日付', date,
			'時刻', time,
			'タイトル', title
		);

	if (show_type) {
		arr_value.push( '種別', type );
	}
	if (with_patient) {
		arr_value.push( '<?=$patient_name_title?>', patient );
	}
	arr_value.push(
			'内容', text,
			'利用人数', user_count,
			'登録者', reg_emp + '<br />' + tmp_class,
			'登録者内線番号', ext,
			'最終更新者', upd_emp
		);

	popupDetailBlue(arr_value, 400, 100, e);
}

function openPrintWindow() {
	window.open('facility_print_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>', 'newwin', 'width=840,height=600,scrollbars=yes');
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'facility_csv.php';
    document.csv.submit();

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>月</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrintWindow();">
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="view" action="facility_month.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲
<select name="range" style="vertical-align:middle;" onchange="this.form.submit();"><? show_range_options($categories, $range); ?></select><img src="img/spacer.gif" width="50" height="1" alt=""><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_month); ?>">&lt;前月</a>
<a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($this_month); ?>">今月</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options($date, $session, $range); ?></select>
<a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_month); ?>">翌月&gt;</a></font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<table width="100%" border="1" cellspacing="0" cellpadding="1">
<tr>
<td width="9%" height="22">&nbsp;</td>
<?
for ($i = 0; $i < 7; $i++) {
	$tmp_wd = $start_wd + $i;
	if ($tmp_wd >= 7) {
		$tmp_wd -= 7;
	}
	switch ($tmp_wd) {
	case 0:
		$show_wd = "日";
		$bgcolor = " bgcolor=\"#fadede\"";
		break;
	case 1:
		$show_wd = "月";
		$bgcolor = "";
		break;
	case 2:
		$show_wd = "火";
		$bgcolor = "";
		break;
	case 3:
		$show_wd = "水";
		$bgcolor = "";
		break;
	case 4:
		$show_wd = "木";
		$bgcolor = "";
		break;
	case 5:
		$show_wd = "金";
		$bgcolor = "";
		break;
	case 6:
		$show_wd = "土";
		$bgcolor = " bgcolor=\"#defafa\"";
		break;
	}
	echo("<td width=\"13%\" align=\"center\"$bgcolor><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
}
?>
</tr>
<?
$reservations = get_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname);
show_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname, $reservations);
?>
</table>
</td>
</tr>
</table>
</body>
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo date("Y",$date).date("m",$date).'01' ?>">
    <input type="hidden" name="end_date" value="<?php echo date("Y",$date).date("m",$date).date('t',$date) ?>">
    <input type="hidden" name="range" value="<?php echo $range; ?>">
</form>

<? pg_close($con); ?>
</html>
<?
// 日付オプションを出力
function show_date_options($date, $session, $range) {

	$arr_date = array();
	$arr_date[6] = $date;
	for ($i = 5; $i >= 0; $i--) {
		$arr_date[$i] = get_last_month($arr_date[$i + 1]);
	}
	for ($i = 7; $i <= 12; $i++) {
		$arr_date[$i] = get_next_month($arr_date[$i - 1]);
	}

	for ($i = 0; $i <= 5; $i++) {
		$ym = date("Y/m", $arr_date[$i]);
		echo("<option value=\"{$arr_date[$i]}\">$ym");
	}

	$ym = date("Y/m", $arr_date[6]);
	echo("<option value=\"{$arr_date[6]}\" selected>【{$ym}】");

	for ($i = 7; $i <= 12; $i++) {
		$ym = date("Y/m", $arr_date[$i]);
		echo("<option value=\"{$arr_date[$i]}\">$ym");
	}

}

// 予約状況を出力
function show_reservation($con, $categories, $range, $emp_id, $date, $start_wd, $session, $fname, $reservations) {
	list($selected_category_id, $selected_facility_id) = split("-", $range);

	// 不要なカテゴリー情報を削除
	if ($selected_category_id != "all") {
		$categories = array($selected_category_id => $categories[$selected_category_id]);
	}

	// 不要な設備情報を削除
	if ($selected_facility_id != "") {
		$categories[$selected_category_id]["facilities"] = array(
			$selected_facility_id => $categories[$selected_category_id]["facilities"][$selected_facility_id]
		);
	}

	// 予約登録可能な施設・設備が存在するかどうかチェック
	$reservable = false;
	foreach ($categories as $tmp_category) {
		foreach ($tmp_category["facilities"] as $tmp_facility) {
			if ($tmp_facility["reservable"]) {
				$reservable = true;
				break 2;
			}
		}
	}

	// 当月1日〜末日の配列を作成
	$year = date("Y", $date);
	$month = date("m", $date);
	$arr_date = array();
	for ($i = 1; $i <= 31; $i++) {
		$day = substr("0" . $i, -2);
		if (checkdate($month, $day, $year)) {
			array_push($arr_date, "$year$month$day");
		} else {
			break;
		}
	}

	// 空白セル分の日付を配列の先頭に追加
	$first_day = mktime(0, 0, 0, $month, "01", $year);
	$empty = date("w", $first_day) - $start_wd;
	if ($empty < 0) {
		$empty += 7;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_unshift($arr_date, date("Ymd", strtotime("-$i days", $first_day)));
	}

	// 空白セル分の日付を配列の末尾に追加
	$day = substr($arr_date[count($arr_date) - 1], -2);
	$end_day = mktime(0, 0, 0, $month, $day, $year);
	$empty = 7 - (count($arr_date) % 7);
	if ($empty == 7) {
		$empty = 0;
	}
	for ($i = 1; $i <= $empty; $i++) {
		array_push($arr_date, date("Ymd", strtotime("$i days", $end_day)));
	}

	// 配列を週単位に分割
	$arr_week = array_chunk($arr_date, 7);

	// 「週表示」リンク出力用インデックスを求める
	$index = date("w", $date) - $start_wd;
	if ($index < 0) {
		$index += 7;
	}

	// 表示対象の設備IDを配列に格納
	$facility_ids = array();
	if ($selected_category_id == "all") {
		foreach ($categories as $tmp_category) {
			$facility_ids = array_merge(
				$facility_ids, array_keys($tmp_category["facilities"])
			);
		}
	} else if ($selected_facility_id == "") {
		$facility_ids = array_keys($categories["$selected_category_id"]["facilities"]);
	} else {
		$facility_ids[] = $selected_facility_id;
	}

	$start_date = "$year{$month}01";
	$end_date = "$year{$month}31";
	// カレンダーのメモを取得
	$arr_calendar_memo = get_calendar_memo($con, $fname, $start_date, $end_date);

	// 予定行を表示
	foreach ($arr_week as $arr_date) {
		echo("<tr>\n");

		// 「週表示」セルを出力（リンク先は処理日と同じ曜日）
		$tmp_year = substr($arr_date[$index], 0, 4);
		$tmp_month = substr($arr_date[$index], 4, 2);
		$tmp_day = substr($arr_date[$index], 6, 2);
		$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
		echo("<td align=\"center\" height=\"80\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"facility_week.php?session=$session&range=$range&date=$tmp_date\">週表示</a></font></td>\n");

		// 日付セルを出力
		foreach ($arr_date as $tmp_ymd) {
			$tmp_year = substr($tmp_ymd, 0, 4);
			$tmp_month = substr($tmp_ymd, 4, 2);
			$tmp_day = substr($tmp_ymd, 6, 2);
			$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
			$wd = date("w", $tmp_date);

			if ($tmp_ymd == date("Ymd")) {
				$bgcolor = " bgcolor=\"#ccffcc\"";
			} else {
				if ($wd == 6) {
					$bgcolor = " bgcolor=\"#defafa\"";
				} else if ($wd == 0) {
					$bgcolor = " bgcolor=\"#fadede\"";
				} else {
					$bgcolor = "";
				}
			}

			// 他の月の場合は空表示
			if ($tmp_month != $month) {
				echo("<td$bgcolor>&nbsp;</td>\n");

			// 当月の場合は詳細表示
			} else {
				$holiday_name = ktHolidayName($tmp_date);
				if ($tmp_ymd == date("Ymd")) {
					$bgcolor = " bgcolor=\"#ccffcc\"";
				} else if ($holiday_name != "") {
					$bgcolor = " bgcolor=\"#fadede\"";
				} else if ($arr_calendar_memo["{$tmp_ymd}_type"] == "5") {
					$bgcolor = " bgcolor=\"#defafa\"";
				}

				echo("<td$bgcolor valign=\"top\">\n");
				echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

				$tmp_ymd = "$tmp_year$tmp_month$tmp_day";
				$tmp_day = intval($tmp_day);
				echo("<tr>\n");
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><b><a href=\"facility_menu.php?session=$session&range=$range&date=$tmp_date\">$tmp_day</a></b></font></td>\n");
				if ($reservable) {
					echo("<td align=\"right\"><img src=\"img/pencil.gif\" alt=\"\" width=\"13\" height=\"13\" onclick=\"window.open('facility_reservation_register.php?session=$session&cate_id=$selected_category_id&facility_id=$selected_facility_id&ymd=$tmp_ymd&path=3&range=$range', 'newwin', 'width=730,height='+((screen.availHeight-60<750)? screen.availHeight-60: 750)+' ,scrollbars=yes');\" style=\"cursor:pointer;\"></td>\n");
				}
				echo("</tr>\n");

				// カレンダーのメモがある場合は設定する
				if ($arr_calendar_memo["$tmp_ymd"] != "") {
					if ($holiday_name == "") {
						$holiday_name = $arr_calendar_memo["$tmp_ymd"];
					} else {
						$holiday_name .= "<br>".$arr_calendar_memo["$tmp_ymd"];
					}
				}

				if ($holiday_name != "") {
					echo("<tr>\n");
					echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"red\">$holiday_name</font></td>\n");
					echo("</tr>\n");
				}

				// 予定情報を取得
				if (count($facility_ids) > 0) {
					// 予約情報配列を参照
					$tmp_reservation = $reservations["{$tmp_ymd}"];
					$rsv_count = count($tmp_reservation);
				} else {
					$rsv_count = 0;
				}
				for ($i = 0; $i < $rsv_count; $i++) {
					echo("<tr>\n");

					// ボーダーの設定
					if ($i == 0) {
						if ($rsv_count == 1) {
							$border = "";
						} else {
							$border = "border-bottom-style:none;";
						}
					} else if ($i < $rsv_count - 1) {
						$border = "border-top:silver dotted 1px;border-bottom-style:none;";
					} else {
						$border = "border-top:silver dotted 1px;";
					}

					$tmp_rsv_id = $tmp_reservation[$i]["reservation_id"];
					$tmp_rsv_emp_id = $tmp_reservation[$i]["emp_id"];
					$tmp_facility_name = $tmp_reservation[$i]["facility_name"];
					$tmp_s_time = $tmp_reservation[$i]["start_time"];
					$tmp_e_time = $tmp_reservation[$i]["end_time"];
					$tmp_title = $tmp_reservation[$i]["title"];
					$tmp_type1_nm = $tmp_reservation[$i]["type1_nm"];
					$tmp_type2_nm = $tmp_reservation[$i]["type2_nm"];
					$tmp_type3_nm = $tmp_reservation[$i]["type3_nm"];
					$tmp_category_id = $tmp_reservation[$i]["fclcate_id"];
					$tmp_faclity_id = $tmp_reservation[$i]["facility_id"];
					$tmp_content = $tmp_reservation[$i]["content"];
					$tmp_users = $tmp_reservation[$i]["users"];
					$tmp_reg_emp_nm = $tmp_reservation[$i]["reg_emp_nm"];
					$tmp_upd_emp_nm = $tmp_reservation[$i]["upd_emp_nm"];
					$tmp_pt_flg = $tmp_reservation[$i]["pt_flg"];
					$tmp_marker = $tmp_reservation[$i]["marker"];
					$tmp_fclhist_content_type = $tmp_reservation[$i]["fclhist_content_type"];
					$tmp_ext = $tmp_reservation[$i]["emp_ext"];

					$sql = "select class_nm, atrb_nm, dept_nm, room_nm from empmst inner join classmst on empmst.emp_class = classmst.class_id inner join atrbmst on empmst.emp_attribute = atrbmst.atrb_id inner join deptmst on empmst.emp_dept = deptmst.dept_id left join classroom on empmst.emp_room = classroom.room_id";
					$cond = "where emp_id = '{$tmp_rsv_emp_id}'";
					$sel = select_from_table($con, $sql, $cond, $fname);
					if ($sel == 0) {
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
					$tmp_class = pg_fetch_result($sel, 0, "class_nm");
					$tmp_atrb = pg_fetch_result($sel, 0, "atrb_nm");
					$tmp_dept = pg_fetch_result($sel, 0, "dept_nm");
					$tmp_room = pg_fetch_result($sel, 0, "room_nm");

					$tmp_class_disp = "$tmp_class &gt; $tmp_atrb &gt; $tmp_dept";
					if ($tmp_room != "") {
						$tmp_class_disp .= " &gt; $tmp_room";
					}

					// テンプレートの場合はXML形式データから内容編集
					if ($tmp_fclhist_content_type == "2") {
						$tmp_content = get_text_from_xml($tmp_content);
					}

					$tmp_category_name = $categories["$tmp_category_id"]["name"];
					if ($selected_facility_id != "") {
						$tmp_fcl_name_for_title = "";
					} else {
						$tmp_fcl_name_for_title = "$tmp_facility_name<br>";
					}
					$tmp_date_str = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_ymd);
					$tmp_types = array();
					if ($tmp_type1_nm != "") {$tmp_types[] = $tmp_type1_nm;}
					if ($tmp_type2_nm != "") {$tmp_types[] = $tmp_type2_nm;}
					if ($tmp_type3_nm != "") {$tmp_types[] = $tmp_type3_nm;}
					$tmp_type_nm = join(", ", $tmp_types);
					if ($tmp_s_time != "") {
						$tmp_s_time = substr($tmp_s_time, 0, 2) . ":" . substr($tmp_s_time, 2, 2);
						$tmp_e_time = substr($tmp_e_time, 0, 2) . ":" . substr($tmp_e_time, 2, 2);
						$time_str = "$tmp_s_time-$tmp_e_time<br>";
						$time_str_popup = "$tmp_s_time 〜 $tmp_e_time";
					} else {
						$time_str = "<img src=\"img/icon/timeless.gif\" alt=\"時刻指定なし\" width=\"8\" height=\"8\">&nbsp;";
						$time_str_popup = "指定なし";
					}
					$tmp_users = ($tmp_users != "") ? $tmp_users . "名" : "";
					$with_patients = ($tmp_pt_flg == "t") ? "true" : "false";
					if ($tmp_pt_flg == "t") {
						$tmp_patients = $tmp_reservation[$i]["pt_name"];
					}

					$style = get_style_by_marker($tmp_marker);

					$tmp_admin_flg = $categories["$tmp_category_id"]["facilities"]["$tmp_faclity_id"]["admin_flg"];
					if ($tmp_rsv_emp_id == $emp_id || $tmp_admin_flg) {
						$child = "facility_reservation_update.php?session=$session&rsv_id=$tmp_rsv_id&path=3&range=$range";
						$height = "'+((screen.availHeight-60<760)? screen.availHeight-60: 760)+'";
					} else {
						$child = "facility_reservation_detail.php?session=$session&rsv_id=$tmp_rsv_id&path=3&range=$range";
						$height = "480";
					}
					if ($tmp_s_time == "") {
						$child .= "&timeless=t";
					}
					$tmp_show_type_flg = $categories["$tmp_category_id"]["facilities"]["$tmp_faclity_id"]["show_type_flg"];
					$show_type = ($tmp_show_type_flg == 't') ? "true" : "false";

					echo("<td colspan=\"2\" height=\"22\" valign=\"top\"$bgcolor style=\"$border padding:4px 0;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_fcl_name_for_title{$time_str}<a href=\"javascript:void(0);\" onclick=\"window.open('$child', 'newwin', 'width=730,height=$height,scrollbars=yes');\" onmousemove=\"popupReservationDetail('$tmp_category_name &gt; $tmp_facility_name', '$tmp_date_str', '$time_str_popup', '" . h(h_jsparam($tmp_title)) . "', '$tmp_type_nm', '" . h(preg_replace("/\r?\n/", "<br>", h_jsparam($tmp_content))) . "', '$tmp_users', '$tmp_reg_emp_nm', '$tmp_upd_emp_nm', event, $with_patients, '$tmp_patients', $show_type, '$tmp_ext', '$tmp_class_disp');\" onmouseout=\"closeDetail();\"$style>" . h($tmp_title) . "</a>\n");

					// 予定内容を表示の場合
					$show_content = $categories["$tmp_category_id"]["show_content"];
					if ($show_content == "t") {
						echo("<br>" . preg_replace("/\r?\n/", "<br>", h($tmp_content)));
					}
					echo("</font></td>\n");
					echo("</tr>\n");
				}

				echo("</table>\n");
				echo("</td>\n");
			}
		}

		echo("</tr>\n");
	}

}
?>
