<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
?>
<title><? echo($menu3_1); ?> | 登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setTitle(path, titlebox) {
	var fname;
	if (path.indexOf('\\') != -1) {
		fname = path.replace(/^.*\\/, '');
	} else if (path.indexOf('/') != -1) {
		fname = path.replace(/^.*\//, '');
	} else {
		fname = path;
	}
	titlebox.value = fname.replace(/\.[^.]*$/, '');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="intra_life_coupon_insert.php" method="post" enctype="multipart/form-data">
<table width="600" border="0" cellspacing="0" cellpadding="3" class="list">
<tr>
<td width="140" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">ファイル</font></td>
<td><input type="file" name="file" size="50" onchange="setTitle(this.value, this.form.title);"></td>
</tr>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">タイトル</font></td>
<td><input type="text" name="title" size="50" maxlength="100"><br><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">※同じタイトルのファイルが既にある場合、今回のファイルで上書きされます。</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
