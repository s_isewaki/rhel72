<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 入院状況レコードを更新
update_inpatient_condition($con, $pt_id, $date, $time, "8", $session, $fname);

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面があればリフレッシュし、自画面を閉じる
?>
<script type="text/javascript">
try {
	opener.location.href = 'inpatient_go_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>';
} catch (e) {
}
self.close();
</script>
