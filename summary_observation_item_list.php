<!DOCTYPE html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="STYLEsheet" type="text/css" href="css/main_summary.css"/>
<?
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("summary_tmpl_util.ini");
require_once("summary_common.ini");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
    showLoginPage();
    exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

$data = getObservationItemData($con, $fname);    // データ取得
pg_close($con);    // DB切断
?>
<title>CoMedix メドレポート｜観察項目一覧</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript">
var mode="<? echo $mode; ?>";
function onAddButtonClick() {
    // 観察一覧の項目を取得
    var elements = document.getElementsByClassName('observation_item');

    // --------------------
    // 親画面の観察項目に追加
    // --------------------
    var observation = window.opener.document.getElementById('left_observation');

    // 親画面で既に選択済である観察項目のidを取得
    var parentElements = window.opener.document.getElementsByClassName('observation_item');
    var selectedIDs = new Array(parentElements.length);
    for (var j = 0; j < parentElements.length; j++) {
        selectedIDs[j] = parentElements[j].id;
    }
    
    for (var i = 0; i < elements.length; i++) {
        var check = document.getElementById(elements[i].id + '_check');
        if (!check.checked) {    // チェックされていなければ追加しない
            continue;
        }
        
        // 既に選択済であれば追加しない
        var index = jQuery.inArray(elements[i].id, selectedIDs);
        if (index != -1) {
            continue;
        }
        observation.innerHTML += '<div id="' + elements[i].id + '_div">' + '<input type="checkbox" class="observation_item" id="' + elements[i].id + '" name="' + elements[i].id + '">' + elements[i].innerHTML + '</div>';
    }
    // チーム計画マスター生成時はDB更新させる
    if( mode=="master"){
	window.opener.submitUpd(41);
    }
    window.close();
}
</script>
</head>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list th {
  border: #ffffff solid 1px;
  background-color: #4682b4;
  color: #ffffff;
  font-size: 16px;
  padding: 2px;
}

.list td {
  border: #ffffff solid 1px;
  font-size: 14px;
  padding: 2px;
}

.observation_item {}
</style>
<body>
<div style="padding:4px">
<?
// ヘッダ出力
echo summary_common_show_dialog_header("観察項目一覧");
?>
<div style="text-align: right; padding-top: 5px;">
<?
if ($data) {
    echo '<input type="button" value="追加" name="add" onclick="onAddButtonClick()">';
}
?>
</div>
<table width="100%" class="list" style="margin-top: 4px;">
  <tr>
    <th width="40">&nbsp;</th>
    <th>観察項目</th>
  </tr>
<?
if (!$data) {
    exit;
}

$count = count($data);
for ($i = 0; $i < $count; $i++) {    // 一覧作成start
    if ($i % 2 == 0) {
        $bgcolor = '#e6e6fa';
    } else {
        $bgcolor = '#f5f5f5';
    }
?>
  <tr>
    <td align="center" bgcolor="<? echo $bgcolor; ?>"><input type="checkbox" id="<? echo $data[$i]['item_cd']; ?>_check"></td>
    <td bgcolor="<? echo $bgcolor; ?>" class="observation_item" id="<? echo $data[$i]['item_cd']; ?>"><? echo $data[$i]['item_name']; ?></td>
  </tr>
<?
}    // 一覧作成end
?>
  </table>
</div>
</body>
</html>
<?
/**
 * 観察項目データを取得する
 * 
 * @param resource $con データベース接続リソース
 * @param string   $fname ページ名
 * 
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getObservationItemData($con, $fname)
{
    $sql = "select item_cd, item_name from tmplitem where mst_cd = 'A263' and disp_flg = true order by item_cd";
    $result = select_from_table($con, $sql, '', $fname);
    $data = pg_fetch_all($result);
    return $data;
}
?>
