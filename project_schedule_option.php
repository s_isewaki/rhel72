<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | スケジュールオプション</title>
<?
$fname = $PHP_SELF;
require("about_session.php");
require("about_authority.php");
require("show_project_schedule_chart.ini");
require("show_select_values.ini");
require("show_facility.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 日付の設定
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// オプション設定情報を取得
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start2");
$time_chart_start = pg_fetch_result($sel, 0, "time_chart_start2");
$time_chart_end = pg_fetch_result($sel, 0, "time_chart_end2");

// 時刻を時と分に分割
$time_chart_start_hour = substr($time_chart_start, 0, 2);
$time_chart_start_min = substr($time_chart_start, 2, 2);
$time_chart_end_hour = substr($time_chart_end, 0, 2);
$time_chart_end_min = substr($time_chart_end, 2, 2);

// 委員会・WG名を取得
$sql = "select pjt_name from project";
$cond = "where pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$project_name = pg_result($sel, 0, "pjt_name");

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkEndMinute() {
	if (document.mainform.time_chart_end_hour.value == '24' &&
		document.mainform.time_chart_end_min.value != '00') {
		document.mainform.time_chart_end_min.value = '00';
		alert('終了時刻が24時の場合には00分しか選べません。');
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>

<? }else{ ?>
	<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>

<? } ?>


</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_schedule_option.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>スケジュール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5" colspan="10"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($project_name); ?></b></font></td>
</tr>
</table>
<form name="timegd">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_week_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="project_schedule_year_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($year); ?>&s_date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="project_schedule_list_upcoming.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="project_schedule_option.php?session=<? echo($session); ?>&date=<? echo($date); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
</form>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form action="project_schedule_option_insert.php" name="mainform">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>カレンダー設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週の開始曜日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="calendar_start" value="1"<? if ($calendar_start == "1") {echo(" checked");} ?>>日曜
<input type="radio" name="calendar_start" value="2"<? if ($calendar_start == "2") {echo(" checked");} ?>>月曜
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>タイムチャート設定</b></font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="time_chart_start_hour">
<? show_hour_options_0_23($time_chart_start_hour, false); ?>
</select>：<select name="time_chart_start_min">
<? show_select_min_30($time_chart_start_min); ?>
</select>〜<select name="time_chart_end_hour" onchange="checkEndMinute();">
<? show_select_hrs_by_args(0, 24, $time_chart_end_hour, false); ?>
</select>：<select name="time_chart_end_min" onchange="checkEndMinute();">
<? show_select_min_30($time_chart_end_min); ?>
</select>
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
