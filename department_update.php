<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
require("about_authority.php");
require("about_session.php");
require("maintenance_menu_list.ini");
require("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 科情報を取得
$sql = "select *, (select class_nm from classmst where class_id = '$class_id') as class_nm, (select atrb_nm from atrbmst where atrb_id = '$atrb_id') as atrb_nm from deptmst";
$cond = "where dept_id = '$dept_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$class_name = pg_fetch_result($sel, 0, "class_nm");
$atrb_name = pg_fetch_result($sel, 0, "atrb_nm");
$dept_name = pg_fetch_result($sel, 0, "dept_nm");
$link_key1 = pg_fetch_result($sel, 0, "link_key1");
$link_key2 = pg_fetch_result($sel, 0, "link_key2");
$link_key3 = pg_fetch_result($sel, 0, "link_key3");

// 室一覧の取得
$sql = "select room_id, room_nm from classroom";
$cond = "where dept_id = $dept_id and room_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$rooms = array();
while ($row = pg_fetch_array($sel)) {
    $rooms[$row["room_id"]] = $row["room_nm"];
}
?>
<title>CoMedix マスターメンテナンス | <?php echo($arr_class_name[2]); ?>更新</title>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.core.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.widget.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.mouse.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.sortable.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    function sortableHelper(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    }

    $('#rooms').sortable({
        axis: 'y',
        placeholder: 'dragging',
        opacity: 0.4,
        start: function(e, tr) {
            $('.dragging').height(30);
        }
    });
});

function saveOrder() {
    var buf = $('#rooms').sortable('serialize').split('&');
    for (var i = 0, len = buf.length; i < len; i++) {
        var pair = buf[i].split('=');
        var elm = document.createElement('input');
        elm.type = 'hidden';
        elm.name = pair[0];
        elm.value = pair[1];
        document.orderform.appendChild(elm);
    }
    document.orderform.submit();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.dragging {background-color:#ffff99;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<?php echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="class_menu.php?session=<?php echo($session); ?>"><b>組織</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<?php write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="60" align="center" bgcolor="#bdd1e7"><a href="class_menu.php?session=<?php echo($session); ?>&class_id=<?php echo($class_id); ?>&atrb_id=<?php echo($atrb_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="class_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[0]); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="attribute_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[1]); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="department_update.php?session=<?php echo($session); ?>&class_id=<?php echo($class_id); ?>&atrb_id=<?php echo($atrb_id); ?>&dept_id=<?php echo($dept_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><?php echo($arr_class_name[2]); ?>更新</b></font></a></td>
<?php if ($arr_class_name["class_cnt"] == 4) { ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="class_room_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[3]); ?>登録</font></a></td>
<?php } ?>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="class_name.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織名</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="area_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="department_update_exe.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[0]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($class_name); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[1]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($atrb_name); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[2]); ?>名</font></td>
<td><input type="text" name="dept_name" value="<?php echo($dept_name); ?>" size="25" maxlength="30" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部連携キー1</font></td>
<td>
    <!--<input type="text" name="link_key1" value="<?php echo($link_key1); ?>" size="25" maxlength="12" style="ime-mode:inactive;">-->
    <input type="text" name="link_key1" value="<?php echo($link_key1); ?>" size="25" style="ime-mode:inactive;">
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部連携キー2</font></td>
<td>
    <!--<input type="text" name="link_key2" value="<?php echo($link_key2); ?>" size="25" maxlength="12" style="ime-mode:inactive;">-->
    <input type="text" name="link_key2" value="<?php echo($link_key2); ?>" size="25" style="ime-mode:inactive;">
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部連携キー3</font></td>
<td>
    <!--<input type="text" name="link_key3" value="<?php echo($link_key3); ?>" size="25" maxlength="12" style="ime-mode:inactive;">-->
    <input type="text" name="link_key3" value="<?php echo($link_key3); ?>" size="25" style="ime-mode:inactive;">
</td>
</tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="class_id" value="<?php echo($class_id); ?>">
<input type="hidden" name="atrb_id" value="<?php echo($atrb_id); ?>">
<input type="hidden" name="dept_id" value="<?php echo($dept_id); ?>">
</form>

<?php if (count($rooms) >= 1) { ?>
<form name="orderform" action="department_update_save_order.php" method="POST">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list" style="margin-top:10px;">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($arr_class_name[3]); ?>一覧</font> <font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10">※ドラッグ＆ドロップすると表示順が入れ替えできます。</font></td>
</tr>
<tr>
<td>
<ul id="rooms" style="list-style:none;padding:0;margin:0;">
<?php foreach ($rooms as $tmp_room_id => $tmp_room_nm) { ?>
<li id="room_<?php echo($tmp_room_id); ?>" style="background-color:#f8f8f8; margin:3px; border:#90bdb1 solid 1px; padding:2px; cursor:move;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?php echo($tmp_room_nm); ?></font></li>
<?php } ?>
</ul>
<p style="text-align:right; margin:3px;"><input type="button" value="表示順更新" onclick="saveOrder();"></p>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="class_id" value="<?php echo($class_id); ?>">
<input type="hidden" name="atrb_id" value="<?php echo($atrb_id); ?>">
<input type="hidden" name="dept_id" value="<?php echo($dept_id); ?>">
</form>
<?php } ?>

</td>
<!-- right -->
</tr>
</table>
</body>
</html>
<?php pg_close($con);
