<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 共通グループ</title>
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 13, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// スケジュールの初期画面を取得
$sql = "select schedule1_default from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
switch ($schedule1_default) {
case "1":
	$default_url = "schedule_menu.php";
	break;
case "2":
	$default_url = "schedule_week_menu.php";
	break;
case "3":
	$default_url = "schedule_month_menu.php";
	break;
}

// 共通グループ一覧を配列に格納
$groups = array();
$sql = "select group_id, group_nm, public_flg from comgroupmst";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = array("name" => $row["group_nm"], "public_flg" => $row["public_flg"]);
}

// 初期表示時に表示するグループを設定
if ($group_id == "" && count($groups) > 0) {
	$group_id = key($groups);
}
if ($group_id == "") {
	$group_id = 0;
}

// メンバー情報を配列に格納
$sql = "select comgroup.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm from comgroup inner join empmst on empmst.emp_id = comgroup.emp_id";
$cond = "where comgroup.group_id = $group_id and exists (select * from authmst where authmst.emp_id = comgroup.emp_id and authmst.emp_del_flg = 'f') order by comgroup.order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$members = array();
while ($row = pg_fetch_array($sel)) {
	$members[] = array(
		"emp_id" => $row["emp_id"],
		"emp_name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"]
	);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function editGroupName() {
	var group_id = '';
	if (document.subform.elements['group_ids[]']) {
		if (document.subform.elements['group_ids[]'].length) {
			for (var i = 0, j = document.subform.elements['group_ids[]'].length; i < j; i++) {
				if (document.subform.elements['group_ids[]'][i].checked) {
					group_id = document.subform.elements['group_ids[]'][i].value;
					break;
				}
			}
		} else if (document.subform.elements['group_ids[]'].checked) {
			group_id = document.subform.elements['group_ids[]'].value;
		}
	}

	if (group_id == '') {
		alert('編集対象が選択されていません。');
		return;
	}

	window.open('schedule_comgroup_name_update.php?session=<? echo($session); ?>&group_id='.concat(group_id), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function createGroupName() {
	window.open('schedule_comgroup_name_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteGroupName() {
	var selected = false;
	if (document.subform.elements['group_ids[]']) {
		if (document.subform.elements['group_ids[]'].length) {
			for (var i = 0, j = document.subform.elements['group_ids[]'].length; i < j; i++) {
				if (document.subform.elements['group_ids[]'][i].checked) {
					selected = true;
					break;
				}
			}
		} else if (document.subform.elements['group_ids[]'].checked) {
			selected = true;
		}
	}

	if (!selected) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.subform.submit();
	}
}

function raiseMember() {
	var members = document.mainform.members;
	var selectedIndex = members.selectedIndex;
	if (selectedIndex < 1) {
		return;
	}

	selectedValue = members.options[selectedIndex].value;
	selectedText  = members.options[selectedIndex].text;

	upperValue = members.options[selectedIndex - 1].value;
	upperText  = members.options[selectedIndex - 1].text;

	members.options[selectedIndex - 1].value    = selectedValue;
	members.options[selectedIndex - 1].text     = selectedText;
	members.options[selectedIndex - 1].selected = true;

	members.options[selectedIndex].value    = upperValue;
	members.options[selectedIndex].text     = upperText;
	members.options[selectedIndex].selected = false;

	updateMemberCSV();
}

function dropMember() {
	var members = document.mainform.members;
	var selectedIndex = members.selectedIndex;
	if (selectedIndex < 0 || selectedIndex >= members.options.length - 1) {
		return;
	}

	selectedValue = members.options[selectedIndex].value;
	selectedText  = members.options[selectedIndex].text;

	underValue = members.options[selectedIndex + 1].value;
	underText  = members.options[selectedIndex + 1].text;

	members.options[selectedIndex + 1].value    = selectedValue;
	members.options[selectedIndex + 1].text     = selectedText;
	members.options[selectedIndex + 1].selected = true;

	members.options[selectedIndex].value    = underValue;
	members.options[selectedIndex].text     = underText;
	members.options[selectedIndex].selected = false;

	updateMemberCSV();
}

function deleteAllMembers() {
	deleteAllOptions(document.mainform.members);
	updateMemberCSV();
}

var childWindow = null;
function openEmployeeList(item_id) {
	var width = 720;
	var height = 600;
	childWindow = window.open('emplist_popup.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>&mode=20&item_id=' + item_id, 'emplistpopup', 'left=' + (screen.availWidth - 10 - width) + ',top=0,width=' + width + ',height=' + height + ',scrollbars=yes,resizable=yes');
	childWindow.focus();
}

function add_target_list(item_id, emp_id, emp_name) {
	var emp_ids = emp_id.split(', ');
	var emp_names = emp_name.split(', ');

	for (var i = 0, j = emp_ids.length; i < j; i++) {
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.mainform.members.options.length; k < l; k++) {
			if (document.mainform.members.options[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.mainform.members, emp_id, emp_name);
		}
	}

	updateMemberCSV();
}

function closeEmployeeList() {
	if (childWindow != null && !childWindow.closed) {
		childWindow.close();
	}
	childWindow = null;
}

function deleteEmp() {
	for (var i = document.mainform.members.options.length - 1; i >= 0; i--) {
		if (document.mainform.members.options[i].selected) {
			document.mainform.members.options[i] = null;
		}
	}
	updateMemberCSV();
}

function updateMemberCSV() {
	document.mainform.member_csv.value = '';
	for (var i = 0, j = document.mainform.members.options.length; i < j; i++) {
		if (i > 0) {
			document.mainform.member_csv.value += ',';
		}
		document.mainform.member_csv.value += document.mainform.members.options[i].value;
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
		return;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>スケジュール</b></a> &gt; <a href="schedule_place_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_place_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="schedule_comgroup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>共通グループ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_setting_etc.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td>
<form name="subform" action="schedule_comgroup_name_delete.php" method="post">
<table width="150" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
共通グループ名<br>
<input type="button" value="編集" onclick="editGroupName();">
<input type="button" value="作成" onclick="createGroupName();">
<input type="button" value="削除" onclick="deleteGroupName();">
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<?
if ($group_id == 0) {
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成されていません。</font>");
} else {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	foreach ($groups as $tmp_group_id => $tmp_group) {
		echo("<tr height=\"22\" valign=\"middle\">\n");
		echo("<td width=\"1\"><input type=\"checkbox\" name=\"group_ids[]\" value=\"$tmp_group_id\"></td>\n");
		echo("");
		if ($tmp_group_id != $group_id) {
			echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"schedule_comgroup.php?session=$session&group_id=$tmp_group_id\">{$tmp_group["name"]}</a></font></td>\n");
		} else {
			echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_group["name"]}</font></td>\n");
		}
		echo("</tr>\n");
	}
	echo("</table>\n");
}
?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>
<form name="mainform" action="schedule_comgroup_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録対象職員</font></td>
<td width="480">
<table width="480" border="0" cellspacing="0" cellpadding="0">
<tr height="2">
<td colspan="4"></td>
</tr>
<tr>
<td width="30"></td>
<td width="150" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象</font></td>
<td width="80"></td>
<td width="220">
</td>
</tr>
<tr>
<td align="center"><input type="button" value="↑" onclick="raiseMember();"><br><br><input type="button" value="↓" onclick="dropMember();"></td>
<td>
<select name="members" size="10" style="width:150px;">
<? write_member_options($members); ?>
</select>
</td>
<td align="center">
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');">
<br><br>
<input type="button" value="削除 &gt;" style="margin-left:2em;width=5.5em;" onclick="deleteEmp();">
</td>
<td>
</td>
</tr>
<tr height="22">
<td></td>
<td><input type="button" value="全て消去" onclick="deleteAllMembers();"></td>
<td></td>
</tr>
<tr height="2">
<td colspan="4"></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開範囲</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="public_flg" value="t"<? if ($groups[$group_id]["public_flg"] == "t") {echo(" checked");} ?>>制限無し
<input type="radio" name="public_flg" value="f"<? if ($groups[$group_id]["public_flg"] == "f") {echo(" checked");} ?>>登録職員のみ
</font></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新" onclick="closeEmployeeList();" <? if ($group_id == 0) {echo(" disabled");} ?>></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
<input type="hidden" name="member_csv" value="<? write_member_csv($members); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function write_member_options($members) {
	foreach ($members as $member) {
		echo("<option value=\"{$member["emp_id"]}\">{$member["emp_name"]}\n");
	}
}

function write_member_csv($members) {
	for ($i = 0, $j = count($members); $i < $j; $i++) {
		if ($i > 0) {
			echo(",");
		}
		echo($members[$i]["emp_id"]);
	}
}
