<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜患者検索</title>
<?
define("ROWS_PER_PAGE", 15);

require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$cond_id = (isset($_GET["cond_id"])) ? $_GET["cond_id"] : "";
$cond_name = (isset($_GET["cond_name"])) ? $_GET["cond_name"] : "";

if ($cond_id !== "" || $cond_name !== "") {
	$sql = "select count(*) from ptifmst";
	$cond = generate_search_condition($con, $cond_id, $cond_name, $fname);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$pt_count = intval(pg_fetch_result($sel, 0, 0));

	if ($pt_count > 0) {
		$sql = "select ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_buis from ptifmst";
		$cond = generate_search_condition($con, $cond_id, $cond_name, $fname);
		if ($page == "") $page = 1;
		$offset = ($page - 1) * ROWS_PER_PAGE;
		$cond .= " order by ptif_id offset $offset limit " . ROWS_PER_PAGE;
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function selectPatient(ptif_id, lt_kaj_nm, ft_kaj_nm, lt_kana_nm, ft_kana_nm, birth_y, birth_m, birth_d, sex, buis) {
	if (!opener || opener.closed) return;
	opener.document.patient.pt_id.value = ptif_id;
	opener.document.patient.lt_nm.value = lt_kaj_nm;
	opener.document.patient.ft_nm.value = ft_kaj_nm;
	opener.document.patient.lt_kana_nm.value = lt_kana_nm;
	opener.document.patient.ft_kana_nm.value = ft_kana_nm;
	opener.document.patient.birth_yr.value = birth_y;
	opener.document.patient.birth_yr2.value = birth_y;
	opener.document.patient.birth_mon.value = birth_m;
	opener.document.patient.birth_day.value = birth_d;
	opener.document.patient.sex.value = sex;
	opener.document.patient.job.value = buis;
	opener.change_age();
	self.close();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>患者検索</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="patient_search_popup.php" method="get">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
患者ID <input name="cond_id" type="text" value="<? echo($cond_id); ?>" maxlength="12" style="ime-mode:inactive;">
患者氏名 <input name="cond_name" type="text" value="<? echo($cond_name); ?>" maxlength="20" style="ime-mode:active;">
<input type="submit" value="検索">
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<? if ($cond_id !== "" || $cond_name !== "") { ?>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<? if ($pt_count == 0) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">該当する患者はいません。</font></td>
</tr>
</table>
<? } else { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="140"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td width="70" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font></td>
</tr>
<? while ($row = pg_fetch_array($sel)) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["ptif_id"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($row["ptif_lt_kaj_nm"]); ?> <? echo($row["ptif_ft_kaj_nm"]); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_age($row["ptif_birth"])); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(format_sex($row["ptif_sex"])); ?></font></td>
<td align="center"><input type="button" value="選択" onclick="selectPatient('<? echo($row["ptif_id"]); ?>', '<? echo($row["ptif_lt_kaj_nm"]); ?>', '<? echo($row["ptif_ft_kaj_nm"]); ?>', '<? echo($row["ptif_lt_kana_nm"]); ?>', '<? echo($row["ptif_ft_kana_nm"]); ?>', '<? echo(substr($row["ptif_birth"], 0, 4)); ?>', '<? echo(substr($row["ptif_birth"], 4, 2)); ?>', '<? echo(substr($row["ptif_birth"], 6, 2)); ?>', '<? echo($row["ptif_sex"]); ?>', '<? echo($row["ptif_buis"]); ?>');"></td>
</tr>
<? } ?>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_navigation($cond_id, $cond_name, $pt_count, $page, $session); ?></font></td>
</tr>
</table>
<? } ?>
<? } ?>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function generate_search_condition($con, $cond_id, $cond_name, $fname) {
	mb_regex_encoding("EUC-JP");
	$cond_id = mb_ereg_replace("[ 　]", "", $cond_id);
	$cond_name = mb_ereg_replace("[ 　]", "", $cond_name);
	$conds = array();
	if ($cond_id !== "") {
		$conds[] = "(ptif_id = '$cond_id')";
	}
	if ($cond_name !== "") {
		$conds[] = "(ptif_lt_kaj_nm || ptif_ft_kaj_nm like '%$cond_name%') or (ptif_lt_kana_nm || ptif_ft_kana_nm like '%$cond_name%')";
	}
	return "where (not ptif_del_flg) and (" . join(" or ", $conds) . ")";
}

function show_navigation($cond_id, $cond_name, $pt_count, $page, $session) {
	$max_page = ceil($pt_count / ROWS_PER_PAGE);
	$start = ($page - 1) * ROWS_PER_PAGE + 1;
	$end = $page * ROWS_PER_PAGE;
	if ($end > $pt_count) $end = $pt_count;
	$encoded_cond_id = urlencode($cond_id);
	$encoded_cond_name = urlencode($cond_name);

	echo("<span style=\"margin-right:15px;\">");
	if ($page > 1) {
		echo("<a href=\"?session=$session&cond_id=$encoded_cond_id&cond_name=$encoded_cond_name&page=" . ($page - 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("←前のページ");
	if ($page > 1) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span>{$pt_count}件中 {$start}");
	if ($start != $end) {
		echo("〜{$end}");
	}
	echo("件目を表示中<span style=\"margin-left:15px;\">");
	if ($page < $max_page) {
		echo("<a href=\"?session=$session&cond_id=$encoded_cond_id&cond_name=$encoded_cond_name&page=" . ($page + 1) . "\">");
	} else {
		echo("<font color=\"gray\">");
	}
	echo("次のページ→");
	if ($page < $max_page) {
		echo("</a>");
	} else {
		echo("</font>");
	}
	echo("</span>");
}

function format_age($birth) {
	if ($birth == "") return "";
	$age = date("Y") - substr($birth, 0, 4);
	if (date("md") < substr($birth, 4, 8)) {
		$age -= 1;
	}
	return "{$age}歳";
}

function format_sex($sex) {
	switch ($sex) {
	case "0":
		return "不明";
	case "1":
		return "男性";
	case "2":
		return "女性";
	}
}
