<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 施設・設備 | 施設・設備更新</title>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_facility.ini");
require_once("get_values.ini");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 職員ID取得
$emp_id = get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
    $can_regist_flg = false;
}
// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

if ($back != "t") {
// 施設・設備情報を取得
    $sql = "select * from facility";
    $cond = "where facility_id = $facility_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $facility_name = pg_fetch_result($sel, 0, "facility_name");
    $remarks = pg_fetch_result($sel, 0, "remarks");
    $presentation = pg_fetch_result($sel, 0, "presentation");
    $capacity = pg_fetch_result($sel, 0, "capacity");
    $auth_flg = pg_fetch_result($sel, 0, "auth_flg");
    $has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");
    $show_type_flg = pg_fetch_result($sel, 0, "show_type_flg");
    $show_repeat_flg = pg_fetch_result($sel, 0, "show_repeat_flg");
    $show_timeless_flg = pg_fetch_result($sel, 0, "show_timeless_flg");
    $detail = ($presentation != "") ? "on" : "";

    $fclhist_id = pg_fetch_result($sel, 0, "fclhist_id");

    $start_date = pg_fetch_result($sel, 0, "start_date");
    $end_date_flg = pg_fetch_result($sel, 0, "end_date_flg");
    $end_date = pg_fetch_result($sel, 0, "end_date");
    $end_month = pg_fetch_result($sel, 0, "end_month");
    if ($start_date != "") {
        $start_date1 = substr($start_date, 0, 4);
        $start_date2 = substr($start_date, 4, 2);
        $start_date3 = substr($start_date, 6, 2);
    }
    if ($end_date != "") {
        $end_date1 = substr($end_date, 0, 4);
        $end_date2 = substr($end_date, 4, 2);
        $end_date3 = substr($end_date, 6, 2);
    }

    // テンプレート情報を取得
    // 存在しない場合の対処
    $content_type = "1"; // テキスト
    $tmpl_content = "";
    if ($fclhist_id > 0) {
        $sql = "select * from fclhist";
        $cond = "where facility_id = $facility_id and fclhist_id = $fclhist_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        if (pg_num_rows($sel) > 0) {
            $content_type = pg_fetch_result($sel, 0, "fclhist_content_type");
            $fclhist_content = pg_fetch_result($sel, 0, "fclhist_content");
            $tmpl_content = $fclhist_content;
        }
    }

}
$tmpl_content = eregi_replace("/(textarea)", "_\\1", $tmpl_content);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function showDetail(flg) {
    document.getElementById('id_detail').style.display = (flg) ? 'block' : 'none';
}

function referTemplate() {
    window.open('facility_master_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
    document.facility.action = "facility_master_update.php";
    document.facility.preview_flg.value = "1";
    document.facility.submit();
}

function show_preview_window(url){
    var option = 'directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left='+(screen.availWidth-740)+',top=10,width=730,height='+((screen.availHeight-70<750)? screen.availHeight-70: 750);
    window.open(url, 'preview_window',option);
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="showDetail(document.facility.detail.checked);">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_menu.php?session=<? echo($session); ?>&opens=<? echo($cate_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="facility_master_update.php?session=<? echo($session); ?>&cate_id=<? echo($cate_id); ?>&facility_id=<? echo($facility_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>施設・設備更新</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="facility" action="facility_master_update_exe.php" method="post">
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td nowrap width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ</font></td>
<td><? show_cate_list($con, $fname, $cate_id); ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称</font></td>
<td><input type="text" name="facility_name" value="<? echo($facility_name); ?>" size="40" maxlength="100" style="ime-mode: active;"></td>
</tr>
<tr height="22">
<td nowrap align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td><textarea name="remarks" rows="5" cols="40" style="ime-mode: active;"><? echo($remarks); ?></textarea></td>
</tr>
<tr height="22">
<td nowrap align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約可能権限設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="auth_flg" value="t"<? if ($auth_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="auth_flg" value="f"<? if ($auth_flg != "t") {echo(" checked");} ?>>しない
<br><font color="red">※「する」を選択すると、予約可能な職員を指定できるようになります。それ以外の場合は、どの職員でも予約可能となります。</font>
</font></td>
</tr>
<tr height="22">
<td nowrap align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備管理者設定</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="has_admin_flg" value="t"<? if ($has_admin_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="has_admin_flg" value="f"<? if ($has_admin_flg != "t") {echo(" checked");} ?>>しない
<br><font color="red">※管理者は、どの職員が登録した予約でも更新・削除可能となります。通常は自分の登録した予約しか更新・削除できません。</font>
</font></td>
</tr>
<tr height="22">
<td nowrap align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約開始日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="start_date1"><? show_select_years_future(11,$start_date1,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="start_date2"><? show_select_months($start_date2,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="start_date3"><? show_select_days($start_date3,true); ?></select>
</font></td>
</tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約期限日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="end_date_flg" value="1"<? if ($end_date_flg == "1") {echo(" checked");} ?>>日付指定
<input type="radio" name="end_date_flg" value="2"<? if ($end_date_flg == "2") {echo(" checked");} ?>>期間指定<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block_in">
<tr>
<td>
<select name="end_date1"><? show_select_years_future(11,$end_date1,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="end_date2"><? show_select_months($end_date2,true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="end_date3"><? show_select_days($end_date3,true); ?></select></font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="end_month">
<?
$fix = $end_month;
// 1〜24のオプション
echo("<option value=\"-\">\n");

for ($i = 1; $i <= 24; $i++) {
    $val = sprintf("%02d", $i);
    echo("<option value=\"$val\"");
    if ($i == $fix) {
        echo(" selected");
    }
    echo(">$i</option>\n");
}

?>
</select>月後まで
</font>
</td>
</tr>
</table>

</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別を表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="show_type_flg" value="t"<? if ($show_type_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="show_type_flg" value="f"<? if ($show_type_flg != "t") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰り返しを表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="show_repeat_flg" value="t"<? if ($show_repeat_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="show_repeat_flg" value="f"<? if ($show_repeat_flg != "t") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「指定しない」を表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="show_timeless_flg" value="t"<? if ($show_timeless_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="show_timeless_flg" value="f"<? if ($show_timeless_flg != "t") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr>
<td rowspan="2" width="160"  align="right" bgcolor="#f6f9ff">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容形式&nbsp;
<input type="radio" name="content_type" value="1"<? if ($content_type == "1") {echo(" checked");} ?> onclick="document.facility.referbtn.disabled = true;<?
if ($can_regist_flg == false) {
    echo("document.facility.update.disabled = false;");
}
?>">テキスト&nbsp;
<input type="radio" name="content_type" value="2"<? if ($content_type == "2") {echo(" checked");} ?> onclick="document.facility.referbtn.disabled = false;<?
if ($can_regist_flg == false) {
    echo("document.facility.update.disabled = true;");
}
?>">テンプレート（PHP）&nbsp;&nbsp;
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">

</font>
</td>
</tr>
<tr>
<td>
<textarea name="tmpl_content" rows="10" cols="70"><?=$tmpl_content?></textarea>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細入力</font></td>
<td><input type="checkbox" name="detail" value="on"<? if ($detail == "on") {echo(" checked");} ?> onclick="showDetail(this.checked);"></td>
</tr>
</table>
<table id="id_detail" width="700" border="0" cellspacing="0" cellpadding="2" class="list" style="display:none;position:relative;top:-1px;">
<tr height="22">
<td width="160"align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プレゼン設備</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="presentation" value="t"<? if ($presentation == "t") {echo(" checked");} ?>>有
<input type="radio" name="presentation" value="f"<? if ($presentation != "t") {echo(" checked");} ?>>無
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">収容人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="capacity" value="<? echo($capacity); ?>" size="4" maxlength="4" style="ime-mode: inactive;">名</font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">
<input type="submit" name="update" value="更新">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="facility_id" value="<? echo($facility_id); ?>">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="back" value="t">
<input type="hidden" name="old_cate_id" value="<? if ($back != "t") {echo($cate_id);} else {echo($old_cate_id);} ?>">
</form>
</td>
</tr>
</table>
<script type="text/javascript">
<?
// テキスト時はボタンを無効化
if ($content_type == "1") {
?>
document.facility.referbtn.disabled = true;
<?
}
?>
<?
// プレビュー押下時
if ($preview_flg == "1") {

    if (!is_dir("fcl")) {
        mkdir("fcl", 0755);
    }
    if (!is_dir("fcl/tmp")) {
        mkdir("fcl/tmp", 0755);
    }
    // content 保存
    $savefilename = "fcl/tmp/{$session}_d.php";

    // 内容書き込み
    if ($tmpl_content == "") {
        $tmpl_content = " ";
    }

    $fp = fopen($savefilename, "w");
    if (!$fp) {
        echo("alert('一時ファイルがオープンできません。再度、プレビューしてください。$savefilename');");
        echo("history.back();</script>");
        exit;
    } else {

        if(!fwrite($fp, $tmpl_content, 2000000)) {
            fclose($fp);
            echo("alert('一時ファイルに書込みできません。再度、プレビューしてください。');");
            echo("history.back();</script>");
            exit;
        }
    }

?>
    var url = 'facility_master_preview.php'
        + '?session=<? echo($session); ?>'
        + '&cate_id=<? echo($cate_id); ?>'
        + '&old_cate_id=<? echo($old_cate_id); ?>'
        + '&facility_id=<? echo($facility_id); ?>'
        + '&facility_name=<? echo(urlencode($facility_name)); ?>'
        + '&content_type=<? echo($content_type); ?>'
        + '&show_type_flg=<? echo($show_type_flg); ?>'
        + '&show_repeat_flg=<? echo($show_repeat_flg); ?>'
        + '&show_timeless_flg=<? echo($show_timeless_flg); ?>'
        + '&mode=update'
        ;
    show_preview_window(url);
<?
}
?>

<?
if ($can_regist_flg == false && $content_type == "2") {
?>
document.facility.update.disabled = true;
<? } ?>
</script>
</body>
<? pg_close($con); ?>
</html>
