<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('Cmx/Model/SystemConfig.php');
require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_header_class.php");

//require_once("hiyari_wf_class.php");
require_once("smarty_setting.ini");

require_once("hiyari_wf_utils.php");
//require_once("hiyari_db_columns.php");

require_once("hiyari_report_class.php");
require_once("show_class_name.ini");
require_once("smarty_setting.ini");

//ページ名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	showLoginPage();
	exit;
}

// 権限チェック
$hiyari = check_authority($session, 47, $fname);
if ($hiyari == "0") {
	showLoginPage();
	exit;
}

// リスクマネージャ権限を取得
$rm_auth = check_authority($session, 48, $fname);

//DBコネクション
$con = connect2db($fname);
if($con == "0"){
	showErrorPage();
	exit;
}

//デザイン（1=旧デザイン、2=新デザイン）
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

$emp_id = get_emp_id($con, $session, $fname);

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);

// 環境設定からマスタ変更情報を取得
$sql = "select user_belong_change, user_job_change from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$user_belong_change = pg_fetch_result($sel, 0, "user_belong_change");
$user_job_change = pg_fetch_result($sel, 0, "user_job_change");


if ( $_POST['ok'] != '' ){

	$_POST['emp_id'] = $emp_id;
	register_profile( $con, $_POST, $fname, $user_belong_change, $user_job_change);
	redirectPage("hiyari_profile_register.php?session=$session");

}
else {
	$profile = get_profile($con, $emp_id, $fname);
	$exp_list = get_exp_list($con,$fname);

	$cls_list = get_cls_list($con,$fname);
	$atrb_list = get_atrb_list($con,$fname);
	$dept_list = get_dept_list($con,$fname);
	
	list($emp_cls,$emp_atrb,$emp_dept,$emp_room) = get_emp_info($con, $emp_id, $fname);
	
	
	
	if ($arr_class_name["class_cnt"] == 4) 
	{
		$room_list = get_room_list($con,$fname);
	}
	
	
	if ($profile["exp_year"] > 0 && $profile["exp_month"] > 0) {
		$exp_year = $profile["exp_year"];
        $exp_month = $profile["exp_month"];
	} else {
		$exp_year = 0;
        $exp_month = 0;
	}
    $exp_year_list = get_select_years(41, $exp_year);

	if ($profile["dept_year"] > 0 && $profile["dept_month"] > 0) {
		$dept_year = $profile["dept_year"];
        $dept_month = $profile["dept_month"];
	} else {
		$dept_year = 0;
        $dept_month = 0;
	}
    $dept_year_list = get_select_years(41, $dept_year);

	$rep_obj = new hiyari_report_class($con, $fname);

	//項目要素非表示情報
	$item_element_no_disp = $rep_obj->get_item_element_no_disp();

	//職種リスト項目マージ処理
	$exp_list_temp = array();
	foreach($exp_list as $exp)
	{
		if(!in_array($exp["easy_code"], $item_element_no_disp["3050"]["30"]) || $exp["easy_code"] == $profile["exp_code"])
		{
			array_push($exp_list_temp, $exp);
		}
	}	
	$exp_list = $exp_list_temp;
    
    //==============================
    //表示
    //==============================
	$smarty = new Cmx_View();

	$smarty->assign("session", $session);
	$smarty->assign("fname", $fname);

    //ヘッダ用データ
    $header = new hiyari_header_class($session, $fname, $con);
    $smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
    $smarty->assign("inci_auth_name", $header->get_inci_auth_name());
    $smarty->assign("is_kanrisha", $header->is_kanrisha());
    $smarty->assign("tab_info", $header->get_user_tab_info());

    //資格
	$smarty->assign("qualification", $profile["qualification"]);

    //職種
	$smarty->assign("exp_code", $profile["exp_code"]);
	$smarty->assign("exp_list", $exp_list);

    //職種・部署開始年月
    $smarty->assign("exp_year", $exp_year);
    $smarty->assign("exp_month", $exp_month);
    $smarty->assign("dept_year", $dept_year);
    $smarty->assign("dept_month", $dept_month);
    $smarty->assign("exp_year_list", $exp_year_list);
    $smarty->assign("dept_year_list", $dept_year_list);
    $smarty->assign("month_list", get_select_months());

    //所属部署
	$smarty->assign("cls_list", $cls_list);
	$smarty->assign("atrb_list", $atrb_list);
	$smarty->assign("dept_list", $dept_list);
	$smarty->assign("room_list", $room_list);
	
	$smarty->assign("belong_num", $arr_class_name["class_cnt"]);

	$smarty->assign("emp_cls", $emp_cls);
	$smarty->assign("emp_atrb", $emp_atrb);
	$smarty->assign("emp_dept", $emp_dept);
	$smarty->assign("emp_room", $emp_room);
	
	$smarty->assign("belong1", $arr_class_name[0]);
	$smarty->assign("belong2", $arr_class_name[1]);
	$smarty->assign("belong3", $arr_class_name[2]);
	$smarty->assign("belong4", $arr_class_name[3]);
	
	$smarty->assign("can_belong_change", $user_belong_change=="t");
		
    if ($design_mode == 1){
        $smarty->display("hiyari_profile_register1.tpl");
    }
    else{
        $smarty->display("hiyari_profile_register2.tpl");
    }
}

//DB切断
pg_close($con);

// 実行終了
exit;

// プロフィールを登録
function register_profile($con, $inputs, $fname, $user_belong_change, $user_job_change) {

	// トランザクションを開始
	pg_query($con, "begin");

	// DBの既存の登録を削除
	$sql = "delete from inci_profile";
	$cond = "where emp_id = '{$inputs["emp_id"]}'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}

	// DBに登録
	$sql = "insert into inci_profile(emp_id, qualification, exp_year, exp_month, dept_year, dept_month, exp_code) values (";
	$content = array($inputs["emp_id"], pg_escape_string($inputs["qualification"]), $inputs["exp_Year"], $inputs["exp_Month"], $inputs["dept_Year"], $inputs["dept_Month"], $inputs["exp_code"]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	
	
	
	if($user_belong_change == "t")  
	{
		//→環境設定「一般ユーザによる所属変更」設定が制御している
		// 組織名の取得
		$arr_class_name = get_class_name_array($con, $fname);
		//所属の更新
		if ($arr_class_name["class_cnt"] == 4) 
		{
			if($inputs["room"] == "0")
			{
				//第四階層が未設定
				$set = array("emp_class", "emp_attribute", "emp_dept");
				$setvalue = array($inputs["cls"], $inputs["atrb"], $inputs["dept"]);
				
			}
			else
			{
				//第四階層あり
				$set = array("emp_class", "emp_attribute", "emp_dept", "emp_room");
				$setvalue = array($inputs["cls"], $inputs["atrb"], $inputs["dept"], $inputs["room"]);
			}
		}
		else
		{
			//第四階層なし
			$set = array("emp_class", "emp_attribute", "emp_dept");
			$setvalue = array($inputs["cls"], $inputs["atrb"], $inputs["dept"]);
		}
	}
	

	$up_skip = "f";	
	if($user_job_change == "t")
	{
		
		//変更した当事者職種名とCoMedixマスタの職種名が同じであったら、CoMedixの個人職種も変更する
		//→環境設定「一般ユーザによる職種変更」設定が制御している
		
		$sql = "select job_id, job_nm from jobmst inner join (select easy_code,easy_name from inci_report_materials where grp_code = 3050 and easy_item_code = 30 and easy_flag = '1' and easy_code='".$inputs["exp_code"]."') t_job on  jobmst.job_nm = t_job.easy_name";
		$cond = "where job_del_flg = 'f'";
		$sel = select_from_table($con,$sql,$cond,$fname);
		if($sel == 0){
			pg_query($con, "rollback");
			pg_close($con);
			showErrorPage();
			exit;
		}
		
		if ( pg_numrows( $sel ) > 0 )
		{
			//件数があった（当事者職種と同じマスタ職種があった）ので、個人職種も変更する
			
			if(is_array($set))
			{
				array_push($set, "emp_job");
			}
			else
			{
				$set = array("emp_job");
			}
			
			if(is_array($setvalue))
			{
				array_push($setvalue, pg_fetch_result($sel, 0, "job_id"));
			}
			else
			{
				$setvalue = array(pg_fetch_result($sel, 0, "job_id"));
			}
		}
		else
		{
			//職種の個人情報をスキップするフラグ
			$up_skip = "t";	
		}
	}
		
	if($user_belong_change == 't' || ($user_job_change == 't' && $up_skip == "f"))
	{
		
		//職員情報の更新
		$sql = "update empmst set";
		$cond = "where emp_id = '".$inputs["emp_id"]."'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

//職種の一覧を取得
function get_exp_list($con,$fname)
{
	$sql = "select easy_code,easy_name from inci_report_materials";
	$cond = "where grp_code = 3050 and easy_item_code = 30 and easy_flag = '1' order by easy_num";
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$array = pg_fetch_all($sel);
	return $array;
}

function get_cls_list($con,$fname)
{
	// 部門一覧を取得
	$sql = "select * from classmst";
	$cond = "where class_del_flg = 'f' order by order_no";
	$sel_class = select_from_table($con, $sql, $cond, $fname);
	if ($sel_class == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$array = pg_fetch_all($sel_class);
	return $array;
}

function get_atrb_list($con,$fname)
{
	// 課一覧を取得
	$sql = "select * from atrbmst";
	$cond = "where atrb_del_flg = 'f' order by order_no";
	$sel_atrb = select_from_table($con, $sql, $cond, $fname);
	if ($sel_atrb == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	
	$array = pg_fetch_all($sel_atrb);
	return $array;
}

function get_dept_list($con,$fname)
{
	// 科一覧を取得
	$sql = "select * from deptmst";
	$cond = "where dept_del_flg = 'f' order by order_no";
	$sel_dept = select_from_table($con, $sql, $cond, $fname);
	if ($sel_dept == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	
	$array = pg_fetch_all($sel_dept);
	return $array;
}

function get_room_list($con,$fname)
{
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	
	$array = pg_fetch_all($sel_room);
	return $array;
}


function get_emp_info($con, $emp_id, $fname)
{
	$sql = "select emp_class,emp_attribute,emp_dept,emp_room from empmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	
	//$array = pg_fetch_all($sel_room);
	return array(pg_fetch_result($sel, 0, "emp_class"),pg_fetch_result($sel, 0, "emp_attribute"),pg_fetch_result($sel, 0, "emp_dept"),pg_fetch_result($sel, 0, "emp_room"));
}

//========================================================================
// 年オプション
//========================================================================
function get_select_years($num, $fix) {
    $year_list = array();

    $now = date("Y");

    if ($fix > 0) {
        $num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
    }

    for ($i = 0; $i < $num; $i++) {
        $year_list[] = $now - $i;
    }

    return $year_list;
}

//========================================================================
// 月オプション
//========================================================================
function get_select_months() {
    $month_list = array();

    for ($i = 1; $i <= 12; $i++) {
        $month_list[] = sprintf("%02d", $i);
    }

    return $month_list;
}
