<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

$target_array = explode("|", $target);

// データベースに接続
$con = connect2db($fname);

switch ($target_array[0]) {

// 日付分
case "d":
	$date = $target_array[1];

	$sql = "select note from bed_innote";
	$cond = q("where type = 'd' and mode = '%s' and date = '%s'", $mode, $date);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}
	$note = pg_fetch_result($sel, 0, "note");

	break;

// 患者分
case "p":
	$ptif_id = $target_array[1];
	$in_dttm = $target_array[2];
	$back_flg = $target_array[3];

	$sql = "select note from bed_innote";
	$cond = q("where type = 'p' and mode = '%s' and ptif_id = '%s' and back_flg = '%s'", $mode, $ptif_id, $back_flg);
	$cond .= ($in_dttm == "") ? " and in_dttm is null" : q(" and in_dttm = '%s'", $in_dttm);
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		js_error_exit();
	}
	$note = pg_fetch_result($sel, 0, "note");

	break;
}
?>
<title>CoMedix 病床管理 | 入退院カレンダー | 入院予定表 | 備考更新</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>備考更新</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="bed_menu_schedule_in_update.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="3" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right" width="20%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
<td><input type="text" name="note" value="<? eh($note); ?>" size="70" maxlength="100" style="ime-mode:active;"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="mode" value="<? eh($mode); ?>">
<input type="hidden" name="target" value="<? eh($target); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
