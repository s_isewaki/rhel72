<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ | 登録者選択</title>
<?
require_once("about_comedix.php");
require_once("news_common.ini");
require_once("show_news_member_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\"showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 46, $fname);
if ($checkauth == "0") {
	$checkauth = check_authority($session, 24, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// イニシャルのデフォルトは「あ行」とする
if ($initial == "") {$initial = "1";}

// 職種一覧を取得
$job_list = get_job_list($con, $fname);

// 職種未指定の場合、ログインユーザの職種をデフォルトとする
if ($job_id == "") {
	$sql = "select emp_job from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\"showErrorPage(window);</script>");
		exit;
	}
	$job_id = pg_fetch_result($sel, 0, "emp_job");
}
?>
<script type="text/javascript">
function changeJob(jobID) {
	location.href = 'news_member_list.php?session=<? echo($session); ?>&from_page_id=<? echo($from_page_id); ?>&job_id=' + jobID;
}

function selectEmp(empID, empName) {
	opener.document.mainform.emp_name.value = empName;
	opener.document.mainform.srch_emp_id.value = empID;
	self.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録者選択</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><select name="job_id" onchange="changeJob(this.value);">
<option value="0">すべて
<? show_options($job_list, $job_id); ?>
</select></td>
</tr>
<tr height="22">
<td><? show_news_initial_list($job_id, $initial, $from_page_id, $session); ?></td>
</tr>
<tr height="22">
<td><? show_news_member_list($con, $job_id, $initial, $session, $fname); ?></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
