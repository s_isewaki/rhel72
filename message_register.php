<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 伝言メモ | 伝言メモ作成</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 伝言メモ権限のチェック
$checkauth = check_authority($session, 4, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// デフォルト値の設定
if ($back != "t") {
    $message_date1 = date("Y");
    $message_date2 = date("m");
    $message_date3 = date("d");
    $message_date4 = date("H");
    $message_date5 = date("i");
    $message_service = "1";
    $message_towhich_device = "0";
}

// データベースに接続
$con = connect2db($fname);

// Eメール送受信設定を取得
$sql = "select use_email from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$use_email = pg_fetch_result($sel, 0, "use_email");
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="message_menu.php?session=<? echo($session); ?>"><img src="img/icon/b05.gif" width="32" height="32" border="0" alt="伝言メモ"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="message_menu.php?session=<? echo($session); ?>"><b>伝言メモ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./message_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="./message_left.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >送信履歴</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="message_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>伝言メモ作成</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="message" action="message_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日時：</font></td>
<td width="480"><select name="message_date1"><? show_select_years_future(2, $message_date1); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="message_date2"><? show_select_months($message_date2); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="message_date3"><? show_select_days($message_date3); ?></select>&nbsp;<select name="message_date4"><? show_select_hrs($message_date4); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font><select name="message_date5"><? show_select_min($message_date5); ?></select></td>
</tr>
<tr>
<td height="22" align="right"><a href="javascript:void(0);" onclick="window.open('message_address.php?session=<? echo($session); ?>', 'newwin2', 'width=640,height=480,scrollbars=yes')"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">宛先</font></a><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">：</font></td>
<td><input type="text" name="message_towhich_name" value="<? echo($hid_message_towhich_name); ?>" size="25" maxlength="250" disabled></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">相手先所属名：</font></td>
<td><input type="text" name="message_company" value="<? echo($message_company); ?>" size="50" maxlength="50" style="ime-mode: active"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">相手先氏名：</font></td>
<td><input type="text" name="message_name" value="<? echo($message_name); ?>" size="25" maxlength="250"style="ime-mode: active"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号：</font></td>
<td><input type="text" name="message_tel1" value="<? echo($message_tel1); ?>" size="6" maxlength="6" style="ime-mode: inactive">-<input type="text" name="message_tel2" value="<? echo($message_tel2); ?>" size="6" maxlength="6" style="ime-mode: inactive">-<input type="text" name="message_tel3" value="<? echo($message_tel3); ?>" size="6" maxlength="6" style="ime-mode: inactive"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">用件：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="message_service" value="1"<? if ($message_service == "1") {echo(" checked");} ?>>お電話がありました<br>
<input type="radio" name="message_service" value="2"<? if ($message_service == "2") {echo(" checked");} ?>>訪問されました<br>
<input type="radio" name="message_service" value="3"<? if ($message_service == "3") {echo(" checked");} ?>>ファックスがありました<br>
<input type="radio" name="message_service" value="4"<? if ($message_service == "4") {echo(" checked");} ?>>折り返しお電話ください<br>
<input type="radio" name="message_service" value="5"<? if ($message_service == "5") {echo(" checked");} ?>>その他<br>
<br>
&nbsp;また、以下の伝言がありました。<br>
<textarea name="message" rows="5" cols="40" style="ime-mode: active;"><? echo($message); ?></textarea>
</font></td>
</tr>
<? if ($use_email == "t") { ?>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メールの送信先：</font></td>
<td><input type="radio" name="message_towhich_device" value="1"<? if ($message_towhich_device == "1") {echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Eメール</font>&nbsp;<input type="radio" name="message_towhich_device" value="2"<? if ($message_towhich_device == "2") {echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">携帯</font>&nbsp;<input type="radio" name="message_towhich_device" value="0"<? if ($message_towhich_device == "0") {echo(" checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">送らない</font></td>
</tr>
<? } ?>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<? if ($use_email != "t") { ?>
<input type="hidden" name="message_towhich_device" value="0">
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="hid_message_towhich_name" value="<? echo($hid_message_towhich_name); ?>">
<input type="hidden" name="message_towhich_id" value="<? echo($message_towhich_id); ?>">
<input type="hidden" name="message_towhich_email" value="<? echo($message_towhich_email); ?>">
<input type="hidden" name="message_towhich_email_mobile" value="<? echo($message_towhich_email_mobile); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
