<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cas_application_approve_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="apply_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apv_stat" value="<?echo($apv_stat)?>">

</form>

<?

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;

}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$count = count($approve_chk);
for ($i=0; $i<$count; $i++)
{
	// パラメータ取得
	$app_values = $approve_chk[$i];
	$arr_app = split(",", $app_values);

	$apply_id       = $arr_app["0"];
	$apv_order      = $arr_app["1"];
	$apv_sub_order  = $arr_app["2"];

	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$wkfw_appr = $arr_apply_wkfwmst[0]["wkfw_appr"];
	$short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];

	$arr_applyapv_per_hierarchy = $obj->get_applyapv_per_hierarchy($apply_id, $apv_order);
	$next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

	// 承認処理
	$obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, "", $next_notice_div, $session, "LIST", "", $short_wkfw_name);

}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);


// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");

?>
</body>