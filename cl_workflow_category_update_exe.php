<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="cl_workflow_category_update.php">
<input type="hidden" name="session" value="<?=$session?>">

<input type="hidden" name="cate_id" value="<?=$cate_id?>">
<input type="hidden" name="folder_id" value="<?=$folder_id?>">

<input type="hidden" name="src_cate_id" value="<?=$src_cate_id?>">
<input type="hidden" name="src_folder_id" value="<?=$src_folder_id?>">
<input type="hidden" name="dest_cate_id" value="<?=$dest_cate_id?>">
<input type="hidden" name="dest_folder_id" value="<?=$dest_folder_id?>">

<input type="hidden" name="category_name" value="<?=$category_name?>">
<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">

<?
if (is_array($hid_ref_dept)) {
	foreach ($hid_ref_dept as $tmp_dept_id) {
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
} else {
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st)) {
	foreach ($ref_st as $tmp_st_id) {
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
} else {
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<?
if ($target_id_list1 != "") {
	$hid_ref_emp = split(",", $target_id_list1);
} else {
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="back" value="t">

</form>
<?
require("about_session.php");
require("about_authority.php");
require("cl_application_workflow_common_class.php");
require_once("cl_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($category_name == "") {
	echo("<script type=\"text/javascript\">alert('フォルダ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (strlen($category_name) > 30) {
	echo("<script type=\"text/javascript\">alert('フォルダ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 利用権限
if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0) {
	echo("<script type=\"text/javascript\">alert('利用可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}


// 登録内容の編集
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}

// データベースに接続
$con = connect2db($fname);

$obj = new cl_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// フォルダ名・利用権限更新
if($cate_id != "" && $folder_id == "")
{
	// カテゴリ更新
	update_wkfwcatemst($con, $fname, $cate_id, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);

	// カテゴリ用アクセス権（科）登録
	$obj->delete_regist_wkfwcate_refdept($cate_id, $hid_ref_dept, "UPD");

	// カテゴリ用アクセス権（役職）登録
	$obj->delete_regist_wkfwcate_refst($cate_id, $ref_st, "UPD");

	// カテゴリ用アクセス権（職員）
	$obj->delete_regist_wkfwcate_refemp($cate_id, $hid_ref_emp, "UPD");

	
}
else if($cate_id != "" && $folder_id != "")
{
	// フォルダ更新
	update_wkfwfolder($con, $fname, $folder_id, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);

	// フォルダ用アクセス権（科）登録
	$obj->delete_regist_wkfwfolder_refdept($folder_id, $hid_ref_dept, "UPD");

	// フォルダ用アクセス権（役職）登録
	$obj->delete_regist_wkfwfolder_refst($folder_id, $ref_st, "UPD");

	// フォルダ用アクセス権（職員）
	$obj->delete_regist_wkfwfolder_refemp($folder_id, $hid_ref_emp, "UPD");
}

// カテゴリ・フォルダ移動

if($src_cate_id == $dest_cate_id && $src_folder_id == $dest_folder_id)
{
	// 何もしない	
}
else
{
	// カテゴリ→ルート
	if($cate_id != "" && $folder_id == "" && $dest_cate_id == "" && $dest_folder_id == "")
	{
		// 何もしない
	}
	// カテゴリ→カテゴリ
	else if($cate_id != "" && $folder_id == "" && $dest_cate_id != "" && $dest_folder_id == "")
	{
		$obj->move_category($cate_id, $dest_cate_id, $dest_folder_id);
	}
	// カテゴリ→フォルダ
	else if($cate_id != "" && $folder_id == "" && $dest_cate_id != "" && $dest_folder_id != "")
	{
		$obj->move_category($cate_id, $dest_cate_id, $dest_folder_id);
	}
	// フォルダ→ルート
	else if($cate_id != "" && $folder_id != "" && $dest_cate_id == "" && $dest_folder_id == "")
	{
		$obj->move_folder($cate_id, $folder_id, $dest_cate_id, $dest_folder_id, true);
	}
	// フォルダ→カテゴリ
	else if($cate_id != "" && $folder_id != "" && $dest_cate_id != "" && $dest_folder_id == "")
	{
		$obj->move_folder($cate_id, $folder_id, $dest_cate_id, $dest_folder_id, false);
	}
	// フォルダ→フォルダ
	else if($cate_id != "" && $folder_id != "" && $dest_cate_id != "" && $dest_folder_id != "")
	{
		$obj->move_folder($cate_id, $folder_id, $dest_cate_id, $dest_folder_id, false);
	}
}	

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'cl_workflow_menu.php?session=$session&selected_cate=$dest_cate_id&selected_folder=$dest_folder_id';</script>");

function update_wkfwcatemst($con, $fname, $wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
{
	// カテゴリ名称を更新
	$sql = "update cl_wkfwcatemst set";
	$set = array("wkfw_nm", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg");
	$setvalue = array(pg_escape_string($category_name), $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
	$cond = "where wkfw_type = $wkfw_type";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

function update_wkfwfolder($con, $fname, $wkfw_folder_id, $wkfw_folder_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
{
	// フォルダ名称を更新
	$sql = "update cl_wkfwfolder set";
	$set = array("wkfw_folder_name", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg");
	$setvalue = array(pg_escape_string($wkfw_folder_name), $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
	$cond = "where wkfw_folder_id = $wkfw_folder_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
</body>