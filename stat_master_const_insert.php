<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 52, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($name == "") {
	echo("<script type=\"text/javascript\">alert('定数の名前が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($name) > 80) {
	echo("<script type=\"text/javascript\">alert('定数の名前が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($value == "") {
	echo("<script type=\"text/javascript\">alert('値が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($value) > 10) {
	echo("<script type=\"text/javascript\">alert('値が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (!preg_match("/^-?[0-9.]+$/", $value)) {
	echo("<script type=\"text/javascript\">alert('値には数値を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// 定数IDを採番
$sql = "select max(statconst_id) from statconst";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 定数名を登録
$sql = "insert into statconst (statconst_id, statconst_name, statconst_value) values (";
$content = array($id, $name, $value);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 定数一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'stat_master_const_list.php?session=$session'</script>");
?>
