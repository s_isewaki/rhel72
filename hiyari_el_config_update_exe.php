<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
if (check_authority($session, 48, $fname) == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//DB更新
//==============================

// デリート
$sql = "delete from el_config";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// インサート
$sql = "insert into el_config (count_flg, dragdrop_flg, real_name_flg) values (";
$content = array($count_flg, $dragdrop_flg, $real_name_flg);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);


//==============================
//画面遷移
//==============================

// オプション画面を再表示
echo("<script type=\"text/javascript\">location.href = 'hiyari_el_config.php?session=$session';</script>");
?>
