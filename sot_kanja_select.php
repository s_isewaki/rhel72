<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者検索</title>
<link rel="stylesheet" type="text/css" href="css/main_summary.css" />
<script language="javascript">
	function adjustScrollListSize(){
		var w = (document.getElementById("listtable").clientWidth  || document.getElementById("listtable").offsetWidth);
		document.getElementById("listhead").style.width = w + "px";
		document.getElementById("list_div").style.height = "100px";
		var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
		h = Math.max(h, document.body.scrollHeight);
		h = h - 170;
		h = Math.max(h, 200);
		if (h > 0) document.getElementById("list_div").style.height = h + "px";
	}
	function trClicked(ptif_id, ptif_name){
		if (!window.opener) return;
		if (window.opener.closed) return;
<? if (!@$_REQUEST["js_callback"]){?>
		alert("値をセットできません。");
		return;
<? } ?>
		if (!window.opener.<?= @$_REQUEST["js_callback"] ?>){
			return;
		}
		window.opener.<?= @$_REQUEST["js_callback"] ?>(ptif_id, ptif_name);
		window.close();
	}
</script>

<?
$fname = $_SERVER["PHP_SELF"];

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("summary_common.ini");
require_once("./get_values.ini");
require_once("sot_util.php");

//==============================================================================
// 初期処理
//==============================================================================
$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	//権限チェック
	$summary_check = check_authority($session,57,$fname);
	if(!$summary_check) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

$sel_ptif_id = trim(@$_REQUEST["sel_ptif_id"]);
$sel_sort_by_byoto = trim(@$_REQUEST["sel_sort_by_byoto"]);

// 初回検索時でなければ
if (@$_REQUEST["searching"]){
	$sel_bldg_cd  = (int)trim(@$_REQUEST["sel_bldg_cd"]);
	$sel_ward_cd  = (int)trim(@$_REQUEST["sel_ward_cd"]);
	$sel_room_no  = (int)trim(@$_REQUEST["sel_room_no"]);
	$sel_ptif_name_list = explode(" ", trim(@$_REQUEST["sel_ptif_name"]));
	$sel_dr_key   = trim(@$_REQUEST["sel_dr_key"]);
	$sel_sect_key = trim(@$_REQUEST["sel_sect_key"]);
	if ($sel_dr_key) {
		list($sel_enti_id, $sel_sect_id, $sel_dr_id) = explode("_", $sel_dr_key."__"); // 担当医をバラす
	}
	else if ($sel_sect_key) {
		$sel_dr_id = "";
		list($sel_enti_id, $sel_sect_id) = explode("_", $sel_sect_key."_"); // 診療科をバラす
	}
	else {
		$sel_dr_id = "";
		$sel_enti_id = "";
		$sel_sect_id = "";
	}
}


// 初回検索時の処理
else if (!@$_REQUEST["searching"]){
	$_REQUEST["is_nyuinnomi"] = 1;

	// ログイン者がドクターとみなして、ドクターの診療科を取得
	$emp_id = get_emp_id($con, $session, $fname);
	$sql =
		" select dr.enti_id, dr.sect_id, dr.dr_id from drmst dr".
		" inner join sectmst se on (se.enti_id = dr.enti_id and se.sect_id = dr.sect_id and se.sect_del_flg = 'f')".
		" where dr.dr_emp_id = '".pg_escape_string($emp_id)."' and dr.dr_del_flg = 'f'".
		" limit 1";
	$sel = $c_sot_util->select_from_table($sql);
	$rs_drmst = pg_fetch_array($sel);


	// ログイン者が看護師とみなして、看護師の病棟担当の取得
	$sql =
		" select swer.bldg_cd, swer.ward_cd from sot_ward_empclass_relation swer".
		" inner join empmst emp on (".
		"   emp.emp_class = swer.class_id".
		"   and emp.emp_attribute = swer.atrb_id".
		"   and emp.emp_dept = swer.dept_id".
		"   and emp.emp_id = '".pg_escape_string($emp_id)."'".
		" )";
	$sel = @$c_sot_util->select_from_table($sql);
	$rs_sot_ward_empclass_relation = pg_fetch_array($sel);


	// 入院患者が指定されたとみなして、入院患者の病棟を取得
	$nyuin_info = $c_sot_util->get_nyuin_info($con, $fname, $sel_ptif_id, date("Ymd"));

	$sel_ptif_name_list = array();
	$sel_sort_by_byoto = "1";

	// ログイン者がDr.の場合
	if ($rs_drmst["dr_id"]){
		$sel_bldg_cd  = 0;
		$sel_ward_cd  = 0;
		$sel_room_no  = 0;
		$sel_enti_id  = $rs_drmst["enti_id"];
		$sel_sect_id  = $rs_drmst["sect_id"];
		$sel_dr_id    = $rs_drmst["dr_id"];
		$sel_dr_key   = $sel_enti_id."_".$sel_sect_id."_".$sel_dr_id;
		$sel_sect_key = $sel_enti_id."_".$sel_sect_id;
	}

	// 入院患者が指定されている場合
	else if (@$nyuin_info["ptif_id"]){
		$sel_bldg_cd  = (int)@$nyuin_info["bldg_cd"];
		$sel_ward_cd  = (int)@$nyuin_info["ward_cd"];
		$sel_room_no  = "";
		$sel_enti_id  = "";
		$sel_sect_id  = "";
		$sel_dr_id    = "";
		$sel_dr_key   = "";
		$sel_sect_key = "";
	}

	// 看護師に担当病棟がある場合、またはそれ以外
	else {
		$sel_bldg_cd  = (int)@$rs_sot_ward_empclass_relation["bldg_cd"];
		$sel_ward_cd  = (int)@$rs_sot_ward_empclass_relation["ward_cd"];
		$sel_room_no  = "";
		$sel_enti_id  = "";
		$sel_sect_id  = "";
		$sel_dr_id    = "";
		$sel_dr_key   = "";
		$sel_sect_key = "";
	}
}





if ($sel_dr_key) list($sel_enti_id, $sel_sect_id, $sel_dr_id) = explode("_", $sel_dr_key."__"); // もし担当医が指定されていれば、バラす。
$sel_sect_key = $sel_enti_id."_".$sel_sect_id; //診療科コードを再作成





//==============================================================================
// ドロップダウンの作成
//==============================================================================
$EMPTY_OPTION = '<option value="">　　　　</option>';

// 主治医選択肢
$sql =
	" select enti_id, sect_id, dr_id, dr_nm from drmst".
	" where dr_del_flg = 'f'".
	" and enti_id = " . (int)$sel_enti_id.
	" and sect_id = " . (int)$sel_sect_id.
	" order by enti_id ,sect_id";
$sel = $c_sot_util->select_from_table($sql);
$dr_data_options = array();
$dr_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$key = $row["enti_id"]."_".$row["sect_id"]."_".$row["dr_id"];
	$selected = ($key == $sel_dr_key ? " selected" : "");
	$dr_data_options[] = '<option value="'.$key.'"'.$selected.'>'.$row["dr_nm"].'</option>';
}

// 診療科選択肢
$sql =
	" select enti_id, sect_id, sect_nm, sort_order from (".
	"   select *, case when sect_sort_order = '' then null else sect_sort_order end as sort_order from sectmst".
	" ) d".
	" where sect_del_flg = 'f'".
	" and (sect_hidden_flg is null or sect_hidden_flg <> 1) ".
	" order by sort_order, sect_id";
$sel = $c_sot_util->select_from_table($sql);
$sect_data_options = array();
$sect_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$key = $row["enti_id"]."_".$row["sect_id"];
	$selected = ($key == $sel_sect_key ? " selected" : "");
	$sect_data_options[] = '<option value="'.$key.'"'.$selected.'>'.$row["sect_nm"].'</option>';
}


// 事業所選択肢
$sel = $c_sot_util->select_from_table("select bldg_cd, bldg_name from bldgmst where bldg_del_flg = 'f' order by bldg_cd");
$bldg_data_options = array();
$bldg_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$selected = ($row["bldg_cd"] == $sel_bldg_cd ? " selected" : "");
	$bldg_data_options[] = '<option value="'.$row["bldg_cd"].'"'.$selected.'>'.$row["bldg_name"].'</option>';
}

// 病棟選択肢
$sql =
	" select ward_cd, ward_name from wdmst".
	" where ward_del_flg = 'f'".
	" and bldg_cd = ".$sel_bldg_cd.
	" order by ward_cd";
$sel = $c_sot_util->select_from_table($sql);
$ward_data_options = array();
$ward_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$selected = ($row["ward_cd"] == $sel_ward_cd ? " selected" : "");
	$ward_data_options[] = '<option value="'.$row["ward_cd"].'"'.$selected.'>'.$row["ward_name"].'</option>';
}

// 病室選択肢
$sql =
	" select ptrm_room_no, ptrm_name from ptrmmst".
	" where ptrm_del_flg = 'f'".
	" and bldg_cd = ".$sel_bldg_cd.
	" and ward_cd = ".$sel_ward_cd.
	" order by ptrm_room_no";
$sel = $c_sot_util->select_from_table($sql);
$room_data_options = array();
$room_data_options[] = $EMPTY_OPTION;
while($row = pg_fetch_array($sel)){
	$selected = ($row["ptrm_room_no"] == $sel_room_no ? " selected" : "");
	$room_data_options[] = '<option value="'.$row["ptrm_room_no"].'"'.$selected.'>'.$row["ptrm_name"].'</option>';
}


//==============================================================================
// 患者一覧の検索
//==============================================================================
$caution_message = "";


$sql = array();
$sql[] = "select * from (";
$sql[] = "select pt.ptif_id, inpt.ptif_id as inpt_ptif_id, hist_counts.ptif_id as hist_ptif_id, hist_counts.hist_count";
$sql[] = ",inpt.bldg_cd, inpt.ward_cd, inpt.ptrm_room_no, inpt.inpt_bed_no, inpt.inpt_in_dt";
$sql[] = ",case";
$sql[] = "  when inpt.ptif_id is not null then ward1.ward_name";
$sql[] = "  when hist_counts.hist_count=1 then ward2.ward_name";
$sql[] = "  when hist_counts.hist_count>1 then '(複数あり)'";
$sql[] = " end as ward_name";
$sql[] = ",case";
$sql[] = "  when inpt.ptif_id is not null then ptrm1.ptrm_name";
$sql[] = "  when hist_counts.hist_count=1 then ptrm2.ptrm_name";
$sql[] = "  when hist_counts.hist_count>1 then '(複数あり)'";
$sql[] = " end as ptrm_name";

$sql[] = ",pt.ptif_lt_kaj_nm, pt.ptif_ft_kaj_nm";
$sql[] = "from ptifmst pt";
$sql[] = "left outer join inptmst inpt on (";
$sql[] = "  inpt.ptif_id = pt.ptif_id";
if ($sel_bldg_cd) $sql[] = "and inpt.bldg_cd = ".$sel_bldg_cd;
if ($sel_ward_cd) $sql[] = "and inpt.ward_cd = ".$sel_ward_cd;
if ($sel_room_no) $sql[] = "and inpt.ptrm_room_no = ".$sel_room_no;
if ($sel_enti_id) $sql[] = "and inpt.inpt_enti_id = ".$sel_enti_id;
if ($sel_sect_id) $sql[] = "and inpt.inpt_sect_id = ".$sel_sect_id;
if ($sel_dr_id)   $sql[] = "and inpt.dr_id = ".$sel_dr_id;
$sql[] = ")";

$sql[] = "left outer join (";
$sql[] = " select ptif_id, count(ptif_id) as hist_count from inpthist where 1 = 1";
if ($sel_bldg_cd) $sql[] = "and bldg_cd = ".$sel_bldg_cd;
if ($sel_ward_cd) $sql[] = "and ward_cd = ".$sel_ward_cd;
if ($sel_room_no) $sql[] = "and ptrm_room_no = ".$sel_room_no;
if ($sel_enti_id) $sql[] = "and inpt_enti_id = ".$sel_enti_id;
if ($sel_sect_id) $sql[] = "and inpt_sect_id = ".$sel_sect_id;
if ($sel_dr_id)   $sql[] = "and dr_id = ".$sel_dr_id;
$sql[] = " group by ptif_id";
$sql[] = ") hist_counts on (hist_counts.ptif_id = pt.ptif_id)";

$sql[] = "left outer join (";
$sql[] = " select hist.ptif_id, hist.bldg_cd, hist.ward_cd, hist.ptrm_room_no, 1 as dummy_count from inpthist hist where 1 = 1";
if ($sel_bldg_cd) $sql[] = "and hist.bldg_cd = ".$sel_bldg_cd;
if ($sel_ward_cd) $sql[] = "and hist.ward_cd = ".$sel_ward_cd;
if ($sel_room_no) $sql[] = "and hist.ptrm_room_no = ".$sel_room_no;
if ($sel_enti_id) $sql[] = "and hist.inpt_enti_id = ".$sel_enti_id;
if ($sel_sect_id) $sql[] = "and hist.inpt_sect_id = ".$sel_sect_id;
if ($sel_dr_id)   $sql[] = "and hist.dr_id = ".$sel_dr_id;
$sql[] = " group by hist.ptif_id, hist.bldg_cd, hist.ward_cd, hist.ptrm_room_no";
$sql[] = ") hist on (hist.ptif_id = hist_counts.ptif_id and hist_counts.hist_count = hist.dummy_count)";

$sql[] = "left outer join bldgmst bldg1 on (bldg1.bldg_cd = inpt.bldg_cd)";
$sql[] = "left outer join wdmst ward1 on (ward1.bldg_cd = inpt.bldg_cd and ward1.ward_cd = inpt.ward_cd)";
$sql[] = "left outer join ptrmmst ptrm1 on (ptrm1.bldg_cd = inpt.bldg_cd and ptrm1.ward_cd = inpt.ward_cd and ptrm1.ptrm_room_no = inpt.ptrm_room_no)";
$sql[] = "left outer join bldgmst bldg2 on (bldg2.bldg_cd = hist.bldg_cd)";
$sql[] = "left outer join wdmst ward2 on (ward2.bldg_cd = hist.bldg_cd and ward2.ward_cd = hist.ward_cd)";
$sql[] = "left outer join ptrmmst ptrm2 on (ptrm2.bldg_cd = hist.bldg_cd and ptrm2.ward_cd = hist.ward_cd and ptrm2.ptrm_room_no = hist.ptrm_room_no)";

$sql[] = "where 1=1";
$sql[] = "and pt.ptif_del_flg = 'f'";
if (@$_REQUEST["is_nyuinnomi"]) $sql[] = "and inpt.ptif_id is not null";

if ($sel_bldg_cd || $sel_ward_cd || $sel_room_no || $sel_enti_id || $sel_sect_id || $sel_dr_id) {
	$sql[] = "and (hist_counts.ptif_id is not null or inpt.ptif_id is not null)";
}


// もし患者名も事業所も選択していなければ、検索しない。
if (trim(@$_REQUEST["sel_ptif_name"])=="" && !$sel_bldg_cd && !$sel_sect_id){
	$sql[] = "and false";
	$caution_message = "検索条件を指定してください。";
}

foreach ($sel_ptif_name_list as $idx => $ptif_name){
	$ptif_name = pg_escape_string(trim($ptif_name));
	if ($ptif_name=="") continue;
	$sql[] = "and(";
	$sql[] = "  pt.ptif_id = '".$ptif_name."'";
	$sql[] = "  or pt.ptif_lt_kaj_nm like '%".$ptif_name."%'";
	$sql[] = "  or pt.ptif_ft_kaj_nm like '%".$ptif_name."%'";
	$sql[] = "  or pt.ptif_lt_kaj_nm || pt.ptif_ft_kaj_nm like '%".$ptif_name."%'";
	$sql[] = ")";
}

$sql[] = ") d";

if ($sel_sort_by_byoto){
	$sql[] = "order by bldg_cd, ward_cd, ptrm_name, ptrm_room_no, inpt_bed_no, ptif_id, inpt_in_dt";
} else {
	$sql[] = "order by ptif_lt_kaj_nm, ptif_ft_kaj_nm";
}
//echo implode("\n",$sql);
$sel = $c_sot_util->select_from_table(implode("\n",$sql));


?>
</head>
<body onload="adjustScrollListSize(); document.getElementById('sel_ptif_name').focus();" onresize="adjustScrollListSize();">
<center>
<div style="width:600px; margin-top:4px">



<!-- ヘッダエリア -->
<?= summary_common_show_dialog_header("患者検索"); ?>



<!-- 検索条件エリア -->
<form name="search" action="sot_kanja_select.php" method="get">
<input type="hidden" name="searching" value="1">
<input type="hidden" name="js_callback" value="<?=$_REQUEST["js_callback"] ?>">
<input type="hidden" name="session" value="<?= $session ?>">
<input type="hidden" name="sel_sort_by_byoto" id="sel_sort_by_byoto" value="@$_REQUEST["sel_sort_by_byoto"]" />
<input type="submit" value="dummy" style="display:none"/>
<div class="dialog_searchfield">
	<table style="width:100%">
		<tr>
			<td style="width:10%">患者名</td>
			<td>
				<div style="float:left">
					<input type="text" id="sel_ptif_name" name="sel_ptif_name" style="width:150px; ime-mode:active;" value="<?=@$_REQUEST["sel_ptif_name"]?>"/>
				</div>
				<div style="clear:both"></div>
			</td>
		</tr>
		<tr>
			<td style="white-space:nowrap">
				<div style="margin-top:4px;">事業所(棟)</div>
			</td>
			<td>
				<div style="float:left; margin-top:2px">
					<select onchange="document.search.submit();" name="sel_bldg_cd"><?=implode("\n", $bldg_data_options)?></select>
				</div>
				<div style="float:left; margin-top:4px; margin-left:4px">病棟</div>
				<div style="float:left; margin-top:2px; margin-left:4px">
					<select onchange="document.search.submit();" name="sel_ward_cd"><?=implode("\n", $ward_data_options)?></select>
				</div>
				<div style="float:left; margin-top:4px; margin-left:4px">病室</div>
				<div style="float:left; margin-top:2px; margin-left:4px">
					<select onchange="document.search.submit();" name="sel_room_no"><?=implode("\n", $room_data_options)?></select>
				</div>
				<div style="float:left; margin-top:2px; margin-left:4px">
					<label style="cursor:pointer"><input type="checkbox" name="is_nyuinnomi" value="1" onchange="document.search.submit();" <?=@$_REQUEST["is_nyuinnomi"] ? "checked='checked'":""?> />入院のみ</label>
				</div>
				<div style="clear:both"></div>
			</td>
		</tr>
		<tr>
			<td>診療科</td>
			<td>
				<div style="float:left; margin-top:2px">
					<select onchange="document.getElementById('sel_dr_key').selectedIndex=0; document.search.submit();" name="sel_sect_key"><?=implode("\n", $sect_data_options)?></select>
				</div>
				<div style="float:left; margin-top:4px; margin-left:4px">主治医</div>
				<div style="float:left; margin-top:2px; margin-left:4px">
					<select onchange="document.search.submit();" id="sel_dr_key" name="sel_dr_key"><?=implode("\n", $dr_data_options)?></select>
				</div>
				<div style="float:right; margin-right:4px">
					<input type="button" onclick="document.search.submit();" value="検索"/>
				</div>
				<div style="clear:both"></div>
			</td>
		</tr>
	</table>
</div>
</form>



<!-- 一覧表ヘッダエリア -->
<div style="margin-top:8px; text-align:left">
<table class="list_table" id="listhead" style="width:600px;" cellspacing="1">
	<tr>
		<th width="40">状態</th>
		<th width="80"><a href="" class="always_blue" onclick="document.getElementById('sel_sort_by_byoto').value='1'; document.search.submit(); return false;">病室</a></th>
		<th width="100">患者ID</th>
		<th width="220"><a href="" class="always_blue" onclick="document.getElementById('sel_sort_by_byoto').value=''; document.search.submit(); return false;">患者名</a></th>
		<th>病棟</th>
	</tr>
</table>
</div>



<!-- 一覧表データ・スクロールエリア -->
<div>
<div id="list_div" style="height:310px; overflow-x:hidden; overflow-y:scroll; background-color:#f4f4f4; border-bottom:1px solid #d4d4d4; text-align:left">
<? if ($caution_message) {?><div style="color:#aaa; margin-top:20px; text-align:center"><?=$caution_message?></div><?}?>
<table class="list_table" id="listtable" style="cursor:pointer" cellspacing="1">
	<? $count = 0; ?>
	<? while ($row = pg_fetch_array($sel)){ ?>
	<?   $count++ ?>
	<?   $tr_color = @$tr_color=="#fff" ? "#f1f7fd": "#fff"; ?>
	<?   $dummy = @$dummy=="disabled" ? "inactive": "disabled"; ?>
	<tr onmouseover="this.style.backgroundColor='#fefc8f';" onmouseout="this.style.backgroundColor='<?=$tr_color?>';" onclick="trClicked('<?=$row["ptif_id"]?>','<?=str_replace('"','\"',trim($row["ptif_lt_kaj_nm"]."&nbsp;".$row["ptif_ft_kaj_nm"]))?>');"
		style="background-color:<?=$tr_color?>; ime-mode:<?=$dummy?>">
		<? // 入院の場合 ?>
		<? if ($row["inpt_ptif_id"]) { ?>
			<td width="40">入院</td>
		<? // 退院の場合 ?>
		<? } else if ($row["hist_ptif_id"]){ ?>
			<td width="40" style="color:#f00">退院</td>
		<? // その他、入院も退院も無い場合 ?>
		<? } else { ?>
			<td width="40"></td>
		<? } ?>
		<td width="80"><?=$row["ptrm_name"]?></td>
		<td width="100"><?= $row["ptif_id"] ?></td>
		<td width="220"><?= trim($row["ptif_lt_kaj_nm"]."&nbsp;".$row["ptif_ft_kaj_nm"]) ?></td>
		<td><?= $row["ward_name"] ?></td>
	</tr>
	<? } ?>
</table>
</div>



</div>
</center>
</body>
</html>
