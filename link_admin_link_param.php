<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix リンクライブラリ | リンクパラメータ設定</title>
<?
/*************************************************************************
リンクライブラリ管理
  リンクパラメータ設定・画面
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// リンクライブラリ管理権限の取得(リンクライブラリ管理[61])
$link_admin_auth = check_authority($session, 61, $fname);

// データベースに接続
$con = connect2db($fname);

// リンク情報を取得
$sql = "SELECT * FROM link";
$cond = "WHERE link_id={$link_id}";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$link = pg_fetch_assoc($sel);

// パラメータ一覧配列を初期化
define("DEFAULT_ROW_COUNT", 1);
$params = array();

// 初期表示時
if ($mode == "") {
	
	// リンク一覧を取得
	$sql = "SELECT *, 1 AS exist FROM link_param";
	$cond = "WHERE link_id=$link_id ORDER BY param_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	pg_close($con);
	
	// 取得した情報を配列に格納
	if (pg_num_rows($sel) > 0) {
		$params = pg_fetch_all($sel);
	}
	
	// 最低表示行数に満たない場合は行を追加
	while (count($params) < DEFAULT_ROW_COUNT) {
		$params[] = add_row();
	}
	
	// 画面再表示時
} else {
	for ($i = 0, $j = count($param_name); $i < $j; $i++) {
		if ($mode == "DELETE_ROW" && $i == $del_id) {
			continue;
		}
		
		$params[] = add_row(
				$param_id[$i],
				$param_type[$i],
				$param_name[$i],
				$param_value[$i],
				$exist[$i]
				);
	}
	
	// 「行を追加」ボタン押下時
	if ($mode == "ADD_ROW") {
		$params[] = add_row();
	}
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
// 行追加
function addRow() {
	document.mainform.action = 'link_admin_link_param.php';
	document.mainform.mode.value = 'ADD_ROW';
	document.mainform.submit();
}

// 行削除
function deleteRow(del_id) {
	document.mainform.action = 'link_admin_link_param.php';
	document.mainform.mode.value = 'DELETE_ROW';
	document.mainform.del_id.value = del_id;
	document.mainform.submit();
}

// Submit
function updateParams() {
	document.mainform.action = 'link_admin_link_param_exe.php';
	return true;
}

// 固定値以外は固定値フォームを無効にする
function valueDisabled(self, row_id) {
	if ( self.value == 1 ) {
		document.getElementById('param_value_'+row_id).disabled = false;
		document.getElementById('param_value_'+row_id).focus();
	} else {
		document.getElementById('param_value_'+row_id).disabled = true;
	}
}
</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="560" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="480" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>パラメータ設定</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<br>

<form name="mainform" method="post" onsubmit="return updateParams();">
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パラメータ名と値の区切り文字：</font></td>
<td><input type="text" name="value_sep" value="<? eh($link["value_sep"]); ?>" size="3"></td>
</tr>
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パラメータの区切り文字：</font></td>
<td><input type="text" name="param_sep" value="<? eh($link["param_sep"]); ?>" size="3"></td>
</tr>
<tr height="22">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">URL・プログラムとパラメータの区切り文字：</font></td>
<td><input type="text" name="query_sep" value="<? eh($link["query_sep"]); ?>" size="3"></td>
</tr>
</table>
<hr size="1" width="560">

<table width="560" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
※編集内容は更新ボタンを押すまで確定されません。<br>
</font></td>
</tr>
<tr height="22">
<td align="right"><input type="button" value="行を追加" onclick="addRow();"></td>
</tr>
</table>

<table width="560" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff" align="center">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">パラメータ名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">値選択</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">固定値</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
</tr>
<? write_rows($params); ?>
</table>
<table width="560" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<? if ($exe == 1) {?>
<hr size="1" width="560">
<button onclick="window.close()">閉じる</button>
<? } ?>
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="link_id" value="<? eh($link_id); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="del_id" value="">
<input type="hidden" name="param_id" value="">
</form>
</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);

// 行追加
function add_row(
	$param_id="", 
	$param_type=1, 
	$param_name="", 
	$param_value="", 
	$exist=0
	) {
	return array(
			"param_id"		=> $param_id,
			"param_type"	=> $param_type,
			"param_name"	=> $param_name,
			"param_value"	=> $param_value,
			"exist"			=> $exist
			);
}

// 一覧行・表示
function write_rows($params) {
	foreach ($params as $i => $param) {
		?>
		<tr height="22">
		<td>
		<input type="text" name="param_name[<? eh($i);?>]" value="<? eh($param["param_name"]);?>" size="30"><br>
		</td>
		<td>
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<select name="param_type[<? eh($i);?>]" onchange="valueDisabled(this,'<? eh($i);?>');">
		<option value="1" <? if($param["param_type"]==1) e('selected'); ?>>固定値</option>
		<option value="2" <? if($param["param_type"]==2) e('selected'); ?>>職員ID</option>
		<option value="3" <? if($param["param_type"]==3) e('selected'); ?>>ログインID</option>
		<option value="4" <? if($param["param_type"]==4) e('selected'); ?>>パスワード</option>
		<option value="5" <? if($param["param_type"]==5) e('selected'); ?>>メールID</option>
		<option value="6" <? if($param["param_type"]==6) e('selected'); ?>>E-Mailアドレス</option>
		</select>
		</font>
		</td>
		<td>
		<input id="param_value_<? eh($i);?>" type="text" name="param_value[<? eh($i);?>]" value="<? eh($param["param_value"]);?>" size="50" <? if($param["param_type"]!=1) e('disabled'); ?>><br>
		</td>
		<td align="center">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<input type="button" value="削除" onclick="deleteRow('<? eh($i);?>');">
		</font>
		</td>
		</tr>
		<input type="hidden" name="param_id[<? eh($i);?>]" value="<? eh($param["param_id"]);?>">
		<input type="hidden" name="exist[<? eh($i);?>]" value="<? eh($param["exist"]);?>">
		<?
	}
}