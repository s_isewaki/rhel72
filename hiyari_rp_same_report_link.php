<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_reporting_common.ini");
require_once("hiyari_yui_calendar.ini");
require_once("hiyari_report_class.php");
require_once("show_class_name.ini");
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('hiyari_item_time_setting_models.php');

//==============================
//画面名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//エントリー情報追加
//==============================
$arr_add_report_id_info = array();      // ドラッグ元のレポートＩＤ配列（追加エントリーするレポート情報配列）
$arr_target_report_id_info = array();   // ドラッグ先のレポートＩＤまたは事案番号クリックのレポート情報配列
$arr_entry_report_id_info = array();    // エントリー中のレポート情報配列

if($add_report_id != "") {
    $arr_add_report_id_info = get_report_id_info($con, $fname, $add_report_id, "child_call");
}

// 親画面から遷移時
if($is_postback != true) {
    // ドラッグ先レポートＩＤ または、事案番号クリック時のレポートＩＤ
    if($target_report_id != "") {
        $arr_target_report_id_info = get_report_id_info($con, $fname, $target_report_id, "parent_call");

        foreach($arr_add_report_id_info as $add_report) {
            array_push($arr_target_report_id_info, $add_report);
        }
        $arr_add_report_id_info = $arr_target_report_id_info;

        $color_target_report_id = $target_report_id;
    }

    // ドラッグ元レポートＩＤ
    if($add_report_id != "") {
        $color_add_report_id = $add_report_id;
    }
}

if($entry_report_id != "") {
    $arr_entry_report_id_info = get_entry_report_id_info($con, $fname, split(",", $entry_report_id));

    // 重複レポートＩＤ除去処理
    $tmp_add_report = array();
    foreach($arr_add_report_id_info as $add_report) {

        $dpl_chk = false;
        foreach($arr_entry_report_id_info as $entry_report) {
            if($add_report["report_id"] == $entry_report["report_id"]) {
                $dpl_chk = true;
            }
        }
        if(!$dpl_chk) {
            array_push($tmp_add_report, $add_report);
        }
    }
    // エントリー一覧に追加
    foreach($tmp_add_report as $add_report) {
        array_push($arr_entry_report_id_info, $add_report);
    }

} else {
    $arr_entry_report_id_info = $arr_add_report_id_info;
}

$entry_report_id = "";
foreach($arr_entry_report_id_info as $list_data) {

    if($entry_report_id != "") {
        $entry_report_id .= ",";
    }
    $entry_report_id .= $list_data["report_id"];
}


//==============================
//エントリー情報削除
//==============================
if($del_report_id != "") {

    $tmp_entry_report_info = array();
    foreach($arr_entry_report_id_info as $entry_report) {
        if($del_report_id != $entry_report["report_id"]){
            array_push($tmp_entry_report_info, $entry_report);
        }
    }
    $arr_entry_report_id_info = $tmp_entry_report_info;

    $entry_report_id_list = split(",", $entry_report_id);
    $tmp_report_id = "";
    foreach($entry_report_id_list as $report_id) {
        if($del_report_id != $report_id) {

            if($tmp_report_id != "") {
                $tmp_report_id .= ",";
            }
            $tmp_report_id .= $report_id;
        }
    }
    $entry_report_id = $tmp_report_id;
}

$is_main_report_id_exist = false;
foreach($arr_entry_report_id_info as $report) {
    if($report["report_id"] == $main_report_id) {
        $is_main_report_id_exist = true;
        break;
    }
}
if(count($arr_entry_report_id_info) > 0 && !$is_main_report_id_exist) {
    $main_report_id = $arr_entry_report_id_info[0]["report_id"];
}


// 登録処理
if($mode == "regist") {
    $arr_entry_report_id = array();
    if($entry_report_id != "" ) {
        $arr_entry_report_id = split(",", $entry_report_id);
    }
    allot_entry_report($con, $fname, $arr_entry_report_id, $target_report_id, $main_report_id);
}

$rep_obj = new hiyari_report_class($con, $fname); //20141008

//==============================
//追加エントリーレポート一覧取得
//==============================
if($is_postback != true) { // 親画面から遷移時
	if ($design_mode == 1){
	    // 本日日付から１ヶ月前
	    $before_1_month = date("Y/m/d", strtotime("-1 month"));
	    // １ヶ月前の日付の１日後
	    $start_date = date("Y/m/d", strtotime("+1 day", strtotime($before_1_month)));
	}else{
	    // レポート項目情報を取得 20141008
	    $report_info = $rep_obj->get_report($target_report_id);
	    
	    //#1694 y-yamamoto
	    $date_of_incidence = $report_info["content"]["input_items"]["100"]["5"][0]; // 発生年月日
	    if (!empty($date_of_incidence)) {
	    	$search_incident_date_start = $date_of_incidence;
	    	$search_incident_date_end = $date_of_incidence;
	    }
	    
	    $hour_of_incidence = $report_info["content"]["input_items"]["100"]["40"][0]; // 発生時間帯
	    if (!empty($hour_of_incidence)) {
	    	$search_incident_hour = $hour_of_incidence;
	    }
	    
	    $place_of_incidence = $report_info["content"]["input_items"]["110"]["60"][0]; // 発生場所
	    if (!empty($place_of_incidence)) {
	    	$search_incident_place = $place_of_incidence;
	    }
	    
	    $summary_of_incidence = $report_info["content"]["input_items"]["900"]["10"][0]; // 概要
	    if (!empty($summary_of_incidence)) {
	    	$search_super_item_2010 = $summary_of_incidence;
	    }
	}
}

// 検索条件を配列化
$serch_params = compact(
        'search_emp_name',
        'start_date',
        'end_date',
        'search_emp_class',
        'search_emp_attribute',
        'search_emp_dept',
        'search_emp_room',
        'entry_report_id',
        'search_incident_date_start', // 発生日(開始)
        'search_incident_date_end',   // 発生日(終了)
        'search_incident_hour',       // 発生時間帯
        'search_incident_place',      // 発生場所
        'search_super_item_2010',     // 概要
        'search_incident_keyword',    // 内容へのキーワード
        'search_keyword_type'         // 内容へのキーワードAND/OR
);
$arr_report_list = get_report_list($con, $fname, $serch_params);

//==============================
//ページングに関する情報
//==============================
//pate省略時は先頭ページを対象とする。
if($page == ""){
    $page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($arr_report_list);
if($rec_count == 1 && $arr_report_list[0] == ""){
    $rec_count = 0;
}

//最大ページ数
if($rec_count == 0){
    $page_max  = 1;
}
else{
    $page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================
$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++){
    if($i >= $rec_count){
        break;
    }
    $list_data_array_work[] = $arr_report_list[$i];
}
$arr_report_list = $list_data_array_work;

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);
$smarty->assign("title", '事案番号関連付け');

$smarty->assign("entry_report_id", $entry_report_id);
$smarty->assign("target_report_id", $target_report_id);
$smarty->assign("main_report_id", $main_report_id);
$smarty->assign("color_target_report_id", $color_target_report_id);
$smarty->assign("color_add_report_id", $color_add_report_id);
$smarty->assign("search_emp_name", h($search_emp_name));
$smarty->assign("start_date", $start_date);
$smarty->assign("end_date", $end_date);

//------------------------------
//部署
//------------------------------
$smarty->assign("search_emp_class", $search_emp_class);
$smarty->assign("search_emp_attribute", $search_emp_attribute);
$smarty->assign("search_emp_dept", $search_emp_dept);
$smarty->assign("search_emp_room", $search_emp_room);

// セクション名(部門・課・科・室)
$arr_class_name = get_class_name_array($con, $fname);
$smarty->assign("arr_class_name", $arr_class_name);

// 部門一覧取得
$sel_class = get_class_mst($con, $fname);
$class_list = array();
while ($row = pg_fetch_array($sel_class)) {
    $class_list[ $row["class_id"] ] = $row["class_nm"];
}
$smarty->assign("class_list", $class_list);

// 課一覧
$sel_atrb = get_atrb_mst($con, $fname);
$atrb_list = array();
while ($row = pg_fetch_array($sel_atrb)) {
    $atrb_list[ $row["class_id"] ][ $row["atrb_id"] ] = $row["atrb_nm"];
}
$smarty->assign("atrb_list", $atrb_list);

// 科一覧
$sel_dept = get_dept_mst($con, $fname);
$dept_list = array();
while ($row = pg_fetch_array($sel_dept)) {
    $dept_list[ $row["atrb_id"] ][ $row["dept_id"] ] = $row["dept_nm"];
}
$smarty->assign("dept_list", $dept_list);

// 室一覧
if ($arr_class_name["class_cnt"] == 4){
    $sel_room = get_room_mst($con, $fname);

    $room_list = array();
    while ($row = pg_fetch_array($sel_room)) {
        $room_list[ $row["dept_id"] ][ $row["room_id"] ] = $row["room_nm"];
    }
    $smarty->assign("room_list", $room_list);
}

//------------------------------
//発生日
//------------------------------
$smarty->assign("search_incident_date_start", $search_incident_date_start);
$smarty->assign("search_incident_date_end", $search_incident_date_end);

//------------------------------
//発生時間帯
//------------------------------
$smarty->assign("search_incident_hour", $search_incident_hour);
$incident_hour_list = get_incident_hour_list($con, $fname);
$smarty->assign("incident_hour_list", $incident_hour_list);

//------------------------------
//概要
//------------------------------
$smarty->assign("search_super_item_2010", $search_super_item_2010);
// $rep_obj = new hiyari_report_class($con, $fname);
$super_item_2010_list = $rep_obj->get_all_super_item_2010();
$smarty->assign("super_item_2010_list", $super_item_2010_list);

//------------------------------
//発生場所
//------------------------------
$smarty->assign("search_incident_place", $search_incident_place);
$incident_place_list = get_incident_place_list($con, $fname);
$smarty->assign("incident_place_list", $incident_place_list);

//------------------------------
//内容へのキーワード
//------------------------------
$smarty->assign("search_incident_keyword", $search_incident_keyword);
$search_keyword_type = $search_keyword_type ? $search_keyword_type : "and";
$smarty->assign("search_keyword_type", $search_keyword_type);

//------------------------------
//関連付けレポート
//------------------------------
$linked_report_info = array();
foreach ($arr_entry_report_id_info as $entry_report) {
    $info = $entry_report;

    $info['is_target_report'] = ($entry_report["report_id"] == $color_target_report_id);

    $info['is_add_report'] = false;
    if($color_add_report_id != "") {
        if($entry_report["report_id"] == $color_add_report_id) {
            $info['is_add_report'] = true;
        }
    }

    $linked_report_info[] = $info;
}
$smarty->assign("linked_report_info", $linked_report_info);

//------------------------------
//追加候補レポート
//------------------------------
$add_report_info = array();
foreach ($arr_report_list as $list_data) {
    $info = $list_data;

    if ($design_mode == 1){
        $info['little_report_img'] = get_little_report_img($list_data["report_link_status"]);
    }
    else{
        $info['report_link_status'] = $list_data["report_link_status"];
    }

    if ($list_data["anonymous_report_flg"] == "t"){
        if ($design_mode == 1){
            $info['create_emp_name'] = h(get_anonymous_name($con,$fname));
        }
        else{
            $info['create_emp_name'] = '匿名';
        }
    }
    else{
        $info['create_emp_name'] =  h($list_data["registrant_name"]);
    }

    $info['is_target_report'] = ($list_data["report_id"] == $color_target_report_id);

    $info['is_add_report'] = false;
    if($color_add_report_id != "") {
        if($list_data["report_id"] == $color_add_report_id) {
            $info['is_add_report'] = true;
        }
    }

    $add_report_info[] = $info;
}
$smarty->assign("add_report_info", $add_report_info);

//ページング
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
if ($page_max > 1){
    $page_list = array();
    $page_stt = max(1, $page - 3);
    $page_end = min($page_stt + 6, $page_max);
    $page_stt = max(1, $page_end - 6);
    for ($i=$page_stt; $i<=$page_end; $i++){
        $page_list[] = $i;
    }
    $smarty->assign("page_list", $page_list);
}

if ($design_mode == 1){
    $smarty->display("hiyari_rp_same_report_link1.tpl");
}
else{
    $smarty->display("hiyari_rp_same_report_link2.tpl");
}

//==============================================================================
// 内部関数
//==============================================================================
//==============================
//レポート一覧取得
//==============================
function get_report_list($con, $fname, $serch_params)
{
    // 検索条件を変数化(以下の変数に展開)
    // $search_emp_name, $start_date, $end_date, $search_emp_class, $search_emp_attribute,
    // $search_emp_dept, $search_emp_room, $entry_report_id, $search_incident_date_start, $search_incident_date_end,
    // $search_incident_hour, $search_incident_place, $search_super_item_2010, $search_incident_keyword, $search_keyword_type
    extract($serch_params);
    
    $where .= "where not del_flag = 't' and not shitagaki_flg = 't' ";

    if($entry_report_id != "") {
        $where .= "and rpt.report_id not in ($entry_report_id) ";
    }

    $serch_emp_target = "report";
    // 部署
    if($search_emp_class != "")
    {
        $where .= " and registrant_class = $search_emp_class ";
    }
    if($search_emp_attribute != "")
    {
        $where .= " and registrant_attribute = $search_emp_attribute ";
    }
    if($search_emp_dept != "")
    {
        $where .= " and registrant_dept = $search_emp_dept ";
    }
    if($search_emp_room != "")
    {
        $where .= " and registrant_room = $search_emp_room ";
    }

    // 報告者名
    if($search_emp_name != "")
    {
        $search_emp_name2 = str_replace(" ", "", $search_emp_name);
        $search_emp_name2 = str_replace("　", "", $search_emp_name2);
        $search_emp_name2 = pg_escape_string($search_emp_name2);
        $where .= " and ";
        $where .= " ( ";
        $where .= "   ( ";
        $where .= "     anonymous_report_flg ";
        $where .= "     and ";
        $where .= "     '".get_anonymous_name($con,$fname)."' like '%$search_emp_name2%' ";
        $where .= "   ) ";
        $where .= "   or ";
        $where .= "   ( ";
        $where .= "     not anonymous_report_flg ";
        $where .= "     and  ";
        $where .= "       ( ";
        $where .= "            ".$serch_emp_target."_emp_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or ".$serch_emp_target."_emp_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or ".$serch_emp_target."_emp_kn_lt_nm like '%$search_emp_name2%' ";
        $where .= "         or ".$serch_emp_target."_emp_kn_ft_nm like '%$search_emp_name2%' ";
        $where .= "         or (".$serch_emp_target."_emp_lt_nm || ".$serch_emp_target."_emp_ft_nm) like '%$search_emp_name2%' ";
        $where .= "         or (".$serch_emp_target."_emp_kn_lt_nm || ".$serch_emp_target."_emp_kn_ft_nm) like '%$search_emp_name2%' ";
        $where .= "       ) ";
        $where .= "   ) ";
        $where .= " ) ";
    }

    // 報告日
    if ($start_date != "") {
        $where .= "and registration_date >= '$start_date'";
    }
    if ($end_date != "") {
        $where .= "and registration_date <= '$end_date'";
    }
    
    // 発生日
    if ($search_incident_date_start != "") {
        $where .= " and date.input_item >= '$search_incident_date_start'";
    }
    if ($search_incident_date_end != "") {
        $where .= " and date.input_item <= '$search_incident_date_end'";
    }

    // 発生時間帯
    if ($search_incident_hour != "") {
        $two_hour_time_divide = getTimeSetting($con, $fname);

        // 2時間単位選択表示の場合
        if ($two_hour_time_divide) {
            $where .= " and hour.input_item = '$search_incident_hour'";

        // 時間・分選択表示の場合
        } else {
            if ($search_incident_hour <= 12) {
                $hour = ($search_incident_hour - 1) * 2;
                $where .= " and hour.input_item in ('$hour', ";
                $hour++;
                $where .= "'$hour')";
            } else {
                $where .= " and hour.input_item = '$search_incident_hour'";  // ありえない
            }
        }
    }
    
    // 発生場所
    if ($search_incident_place != "") {
        $where .= " and place.input_item = '$search_incident_place'";
    }
    
    // 概要
    if ($search_super_item_2010 != "") {
        $where .= " and super_item_2010.input_item = '$search_super_item_2010'";
    }
    
    // 内容へのキーワード
    // 全角スペースを半角スペースに変換して、半角スペースで分割
    $keywords = explode(' ', mb_convert_kana($search_incident_keyword, "s", "EUC-JP"));
    $keywords = array_filter($keywords, "strlen");    // サイズゼロの文字列を除く
    if (count($keywords) != 0) {
        $keyword_cond = "";
        foreach ($keywords as $keyword) {
            if ($keyword_cond) {
                $keyword_cond .= " $search_keyword_type";
            }
            $keyword_cond .= " keyword.input_item like '%$keyword%'";
        }
        
        if (count($keywords) > 1) {
            $where .= " and ($keyword_cond)";
        } else {
            $where .= " and $keyword_cond";
        }
    }

    $order = "order by main_report_id desc, report_link_status, report_id desc";

    // 取得対象フィールド
    $fields = array(
                'report_id',
                'report_title',
    //          'job_id',
    //          'registrant_id',
                'registrant_name',
                'registration_date',
    //          'eis_id',
                'eid_id',
                'del_flag',
                'report_no',
                'shitagaki_flg',
    //          'report_comment',
    //          'edit_lock_flg',
                'anonymous_report_flg',
                'registrant_class',
                'registrant_attribute',
                'registrant_dept',
                'registrant_room',

    //          'report_emp_class',
    //          'report_emp_attribute',
    //          'report_emp_dept',
    //          'report_emp_room',
    //          'report_emp_job',
                'report_emp_lt_nm',
                'report_emp_ft_nm',
                'report_emp_kn_lt_nm',
                'report_emp_kn_ft_nm',

    //          '[auth]_update_emp_id',
    //          '[auth]_start_flg',
    //          '[auth]_end_flg',
    //          '[auth]_start_date',
    //          '[auth]_end_date',
    //          '[auth]_use_flg',

                'report_link_id',
                'report_link_status',
                'main_report_id',
                'incident_date',      // 発生日
                'incident_hour',      // 発生時間帯
                'incident_place',     // 発生場所
                'super_item_2010',    // 概要
                'incident_keyword'    // 内容へのキーワード
        );

    $arr_report_list = get_report_db_list($con, $fname, $where, $order, $fields);

    return $arr_report_list;
}


//==============================
// エントリーレポートＩＤ情報取得
//==============================
function get_report_id_info($con, $fname, $report_id, $div) {

    $sql = "select b.report_id, b.report_title, b.report_no ";
    $sql .= "from ( ";

    if($div == "child_call") { // 子画面から呼ばれた場合

        $sql .= "select * from inci_report_link_view where report_link_id in (select report_link_id from inci_report_link_view where report_id = $report_id and report_link_status = 1) ";
        $sql .= "union all ";
        $sql .= "select * from inci_report_link_view where report_id in (select report_id from inci_report_link_view where report_id = $report_id and report_link_status <> 1) ";
    } else if($div == "parent_call"){ // 親画面から呼ばれた場合

        $sql .= "select * from inci_report_link_view where report_link_id in (select report_link_id from inci_report_link_view where report_id = $report_id and report_link_status in (1, 2)) ";
        $sql .= "union all ";
        $sql .= "select * from inci_report_link_view where report_id in (select report_id from inci_report_link_view where report_id = $report_id and report_link_status = 0) ";
    }

    $sql .= ") a inner join inci_report b on a.report_id = b.report_id ";
    $sql .= "order by report_link_status asc, report_id desc";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $array = pg_fetch_all($sel);
    return $array;
}

//==============================
// エントリー中レポートID情報取得
//==============================
function get_entry_report_id_info($con, $fname, $arr_report_id) {

    for($i=0; $i<count($arr_report_id); $i++)
    {
        if($i > 0)
        {
            $report_id_cond .= ',';
        }
        $report_id_cond .= $arr_report_id[$i];
    }

    $sql = "select report_id, report_title, report_no from inci_report";
    $cond = "where report_id in ($report_id_cond)";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $array = pg_fetch_all($sel);
    return $array;
}

//==============================
// レポートＩＤ割付処理
//==============================
function allot_entry_report($con, $fname, $arr_entry_report_id, $target_report_id, $main_report_id) {

    // トランザクションの開始
    pg_query($con, "begin transaction");

    // 【target_report_id】 に紐づくリンク情報削除
    delete_entry_report($con, $fname, $target_report_id, "report_link_id_del");

    // エントリーID 削除
    for($i=0; $i<count($arr_entry_report_id); $i++) {

        $report_cnt = get_report_cnt($con, $fname, $arr_entry_report_id[$i]);
        if($report_cnt <= 2) {
            delete_entry_report($con, $fname, $arr_entry_report_id[$i], "report_link_id_del");
        } else {
            delete_entry_report($con, $fname, $arr_entry_report_id[$i], "report_id_del");
        }
    }

    // エントリーするレポートが２報告以上の場合、レポート割付を行う
    if(count($arr_entry_report_id) >= 2) {

        // 新規採番
        $report_link_id = get_max_report_link_id($con, $fname) + 1;

        for($i=0; $i<count($arr_entry_report_id); $i++) {

            $main_flg = 'f';
            if($arr_entry_report_id[$i] == $main_report_id) {
                $main_flg = 't';
            }
            regist_report($con, $fname, $report_link_id, $arr_entry_report_id[$i], $main_flg);
        }
    }

    // トランザクションをコミット
    pg_query($con, "commit");

    echo("<script type=\"text/javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
    echo("<script type=\"text/javascript\">self.close();</script>");
}

//==============================
// レポートＩＤ削除処理
//==============================
function delete_entry_report($con, $fname, $report_id, $div) {

    $sql = "delete from inci_report_link";

    if($div == "report_link_id_del") {

        $cond = "where report_link_id in (select report_link_id from inci_report_link where report_id = $report_id)";

    } else {

        $cond = "where report_id = $report_id";

    }

    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

//==============================
// レポートリンク数取得
//==============================
function get_report_cnt($con, $fname, $report_id) {

    $sql = "select count(report_link_id) as cnt from inci_report_link";
    $cond = "where report_link_id in (select report_link_id from inci_report_link where report_id = $report_id)";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $cnt = pg_result($sel,0,"cnt");
    return $cnt;
}

//==============================
// レポートＩＤ・ＭＡＸ値取得
//==============================
function get_max_report_link_id($con, $fname) {

    $sql = "select max(report_link_id) as max from inci_report_link";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $max = pg_result($sel,0,"max");
    return $max;
}

//==============================
// レポートリンク登録処理
//==============================
function regist_report($con, $fname, $report_link_id, $report_id, $main_flg) {

    $sql = "insert into inci_report_link (report_link_id, report_id, main_flg) values (";
    $content = array($report_link_id, $report_id, $main_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_exec($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

}

//=================================
// 部門オプションを出力
//=================================
function show_class_options($sel_class, $search_emp_class) {
    echo("<option value=\"\">----------\n");
    while ($row = pg_fetch_array($sel_class)) {
        $id = $row["class_id"];
        $name = $row["class_nm"];
        echo("<option value=\"$id\"");

        if($search_emp_class == $id) {
            echo(" selected");
        }
        echo(">$name\n");
    }
}

//=================================
// 発生時間帯取得
//=================================
function get_incident_hour_list($con, $fname) {
    $sql = "select easy_name, easy_code from inci_report_materials";
    $cond = "where grp_code = 100 and easy_item_code = 40 order by to_number(easy_code, '99')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $incident_hour_list = pg_fetch_all($sel);
    return $incident_hour_list;
}

//=================================
// 発生場所取得
//=================================
function get_incident_place_list($con, $fname) {
    // 取得した発生場所項目を取得
    $sql = "select easy_name, easy_code from inci_report_materials";
    $cond = "where grp_code = 110 and easy_item_code = 60 order by to_number(easy_code, '99')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $data = pg_fetch_all($sel);
    
    // 非表示項目を取得
    global $rep_obj;
    $item_element_no_disp = $rep_obj->get_item_element_no_disp();
    
    // 取得した発生項目から、表示項目のみ抽出
    $incident_place_list = array();
    if (is_array($item_element_no_disp[110][60])) {
        foreach ($data as $value) {
            if (in_array($value["easy_code"], $item_element_no_disp[110][60])) {
                // 非表示項目ならスキップ
                continue;
            }
            $incident_place_list[] = $value;
        }
    } else {
        $incident_place_list = $data;
    }
    
    return $incident_place_list;
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);