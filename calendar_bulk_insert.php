<?
require("about_session.php");
require("about_authority.php");
require("holiday.php");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 登録対象日を配列で取得
$start_ym = "$year_from$month_from";
$end_ym = "$year_to$month_to";
$start_ymd = "{$start_ym}01";
$end_y = substr($end_ym, 0, 4);
$end_m = substr($end_ym, 4, 2);
$end_d = sprintf("%02d", days_in_month($end_y, $end_m));
$end_ymd = "$end_ym$end_d";
$arr_date = array();
switch ($tgtday) {
case "1":  // 日付指定
	$arr_date = get_target_date1($start_ymd, $end_ymd, $tgtday1_a);
	break;
case "2":  // 繰り返しタイプ1
	$arr_date = get_target_date2($start_ymd, $end_ymd, $tgtday2_a, $tgtday2_b);
	break;
case "3":  // 繰り返しタイプ2
	$arr_date = get_target_date3($start_ymd, $end_ymd, $tgtday3_a, $tgtday3_b, $tgtday3_c);
	break;
case "4":  // 祝日
	$arr_date = get_target_date4($start_ymd, $end_ymd);
	break;
}
if (count($arr_date) == 0) {
	echo("<script type=\"text/javascript\">alert('登録対象日がありません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 登録対象日をループ
foreach ($arr_date as $tmp_date) {

	// 対象日付のレコードを削除
	$sql = "delete from calendar";
	$cond = "where date = '$tmp_date'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$wk_type = $type;
	// 振替休日
	if ($tgtday == "4" && $type == "6") {
		$holiday_name = get_holiday_name($tmp_date);
		if ($holiday_name == "振替休日") {
			$wk_type = "4";
		}
	}

	// 対象日付のレコードを作成
	$sql = "insert into calendar (date, type) values (";
	$content = array("$tmp_date", $wk_type);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 年間カレンダー画面に遷移
echo("<script type=\"text/javascript\">location.href = 'calendar_year.php?session=$session';</script>");

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 登録対象日付を配列で返す（日付指定）
function get_target_date1($start_ymd, $end_ymd, $day) {
	$arr_date = array();
	if ($day == "all") {  // 1〜末日
		$tmp_ymd = $start_ymd;
		while ($tmp_ymd <= $end_ymd) {
			array_push($arr_date, $tmp_ymd);
			$tmp_ymd = next_date($tmp_ymd);
		}
	} else if ($day == "last") {  // 末日
		$start_ym = substr($start_ymd, 0, 6);
		$end_ym = substr($end_ymd, 0, 6);
		$tmp_ym = $start_ym;
		while ($tmp_ym <= $end_ym) {
			$tmp_y = substr($tmp_ym, 0, 4);
			$tmp_m = substr($tmp_ym, 4, 2);
			$tmp_d = sprintf("%02d", days_in_month($tmp_y, $tmp_m));
			array_push($arr_date, "$tmp_ym$tmp_d");
			if ($tmp_m == "12") {
				$tmp_y++;
				$tmp_m = "01";
			} else {
				$tmp_m = sprintf("%02d", $tmp_m + 1);
			}
			$tmp_ym = "$tmp_y$tmp_m";
		}
	} else {  // 日指定
		$start_ym = substr($start_ymd, 0, 6);
		$end_ym = substr($end_ymd, 0, 6);
		$tmp_ym = $start_ym;
		while ($tmp_ym <= $end_ym) {
			$tmp_y = substr($tmp_ym, 0, 4);
			$tmp_m = substr($tmp_ym, 4, 2);
			if (checkdate($tmp_m, $day, $tmp_y)) {
				array_push($arr_date, "$tmp_ym$day");
			}
			if ($tmp_m == "12") {
				$tmp_y++;
				$tmp_m = "01";
			} else {
				$tmp_m = sprintf("%02d", $tmp_m + 1);
			}
			$tmp_ym = "$tmp_y$tmp_m";
		}
	}
	return $arr_date;
}

// 登録対象日付を配列で返す（繰り返しタイプ1）
function get_target_date2($start_ymd, $end_ymd, $cond1, $cond2) {

	// 戻り値となる配列を宣言
	$arr = array();

	// パス回数の上限をセット
	switch ($cond1) {
	case "1":  // 毎週
		$pass_count = 0;
		break;
	case "2":  // 隔週
		$pass_count = 1;
		break;
	}

	// 期間ループ内で使用する変数を初期化
	$tmp_pass_count = 0;
	$pass_flag = false;
	$tmp_ymd = $start_ymd;

	// 期間をループ
	while ($tmp_ymd <= $end_ymd) {

		// 処理日付の曜日を取得
		$tmp_weekday = date("w", to_timestamp($tmp_ymd));

		// 登録対象日かどうか、および特殊日かどうかをチェック
		switch ($cond2) {
		case "1":  // 日：日曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 0);
			$special_flag = $target_flag;
			break;
		case "2":  // 月：月曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 1);
			$special_flag = $target_flag;
			break;
		case "3":  // 火：火曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 2);
			$special_flag = $target_flag;
			break;
		case "4":  // 水：水曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 3);
			$special_flag = $target_flag;
			break;
		case "5":  // 木：木曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 4);
			$special_flag = $target_flag;
			break;
		case "6":  // 金：金曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 5);
			$special_flag = $target_flag;
			break;
		case "7":  // 土：土曜であれば登録、登録日なら特殊日
			$target_flag = ($tmp_weekday == 6);
			$special_flag = $target_flag;
			break;
		case "8":  // 月、水、金：月・水・金のいずれかであれば登録、金なら特殊日
			$target_flag = ($tmp_weekday == 1 || $tmp_weekday == 3 || $tmp_weekday == 5);
			$special_flag = ($tmp_weekday == 5);
			break;
		case "9":  // 火、木：火・木のいずれかであれば登録、木なら特殊日
			$target_flag = ($tmp_weekday == 2 || $tmp_weekday == 4);
			$special_flag = ($tmp_weekday == 4);
			break;
		case "10":  // 月〜金：土・日以外であれば登録、金なら特殊日
			$target_flag = ($tmp_weekday != 6 && $tmp_weekday != 0);
			$special_flag = ($tmp_weekday == 5);
			break;
		case "11":  // 土、日：土・日のいずれかであれば登録、日なら特殊日
			$target_flag = ($tmp_weekday == 6 || $tmp_weekday == 0);
			$special_flag = ($tmp_weekday == 0);
			break;
		}

		// 登録対象日かつパスフラグが立っていなければ配列に追加
		if ($target_flag && !$pass_flag) {
			array_push($arr, $tmp_ymd);
		}

		// 特殊日の場合
		if ($special_flag) {

			// パスフラグを立てる
			$pass_flag = true;

			// 次回のパス回数が上限を超える場合、パスをキャンセル
			if (++$tmp_pass_count > $pass_count) {
				$pass_flag = false;
				$tmp_pass_count = 0;
			}
		}

		// 日付を進める
		$tmp_ymd = next_date($tmp_ymd);
	}

	return $arr;

}

// 登録対象日付を配列で返す（繰り返しタイプ2）
function get_target_date3($start_ymd, $end_ymd, $cond1, $cond2, $cond3) {

	// 戻り値となる配列を宣言
	$arr = array();

	// 処理対象曜日を変数にセット
	switch ($cond3) {
	case "1":  // 日
		$weekday = 0;
		break;
	case "2":  // 月
		$weekday = 1;
		break;
	case "3":  // 火
		$weekday = 2;
		break;
	case "4":  // 水
		$weekday = 3;
		break;
	case "5":  // 木
		$weekday = 4;
		break;
	case "6":  // 金
		$weekday = 5;
		break;
	case "7":  // 土
		$weekday = 6;
		break;
	}

	// スタート日付の月の第1Ｘ曜日を求める
	$y = substr($start_ymd, 0, 4);
	$m = substr($start_ymd, 4, 2);
	for ($d = 1; $d <= 7; $d++) {
		if (date("w", mktime(0, 0, 0, $m, $d, $y)) == $weekday) {
			break;
		}
	}
	$tmp_date = "{$y}{$m}0{$d}";

// スタート月からエンド月までの日付情報を月ごとにtmp配列に格納
	$arr_tmp = array();
	$tmp_ym = substr($tmp_date, 0, 6);
	$end_ym = substr($end_ymd, 0, 6);
	while ($tmp_ym <= $end_ym) {

		// 年月が配列のキーとして存在しなければ追加（配列で初期化）
		if (!array_key_exists($tmp_ym, $arr_tmp)) {
			$arr_tmp[$tmp_ym] = array();
		}

		// 下層の配列に処理日付を追加
		array_push($arr_tmp[$tmp_ym], $tmp_date);

		// 日付を翌週に進める
		$tmp_date = next_week($tmp_date);

		// 処理日付の年月を変数にセット
		$tmp_ym = substr($tmp_date, 0, 6);
	}

	// 日付情報のうち指定週のもののみをtmp配列にセット
	$arr_tmp2 = array();
	foreach ($arr_tmp as $val) {
		switch ($cond2) {
		case "1":  // 第1
			array_push($arr_tmp2, $val[0]);
			break;
		case "2":  // 第2
			array_push($arr_tmp2, $val[1]);
			break;
		case "3":  // 第3
			array_push($arr_tmp2, $val[2]);
			break;
		case "4":  // 第4
			array_push($arr_tmp2, $val[3]);
			break;
		case "5":  // 最終
			array_push($arr_tmp2, $val[count($val) - 1]);
			break;
		}
	}

	// 対象日付がなければリターン
	if (count($arr_tmp2) == 0) {
		return $arr_tmp2;
	}

	// 指定期間から外れる分を削除
	while ($arr_tmp2[0] < $start_ymd) {
		array_shift($arr_tmp2);
		if (count($arr_tmp2) == 0) {
			return $arr_tmp2;
		}
	}
	while ($arr_tmp2[count($arr_tmp2) - 1] > $end_ymd) {
		array_pop($arr_tmp2);
		if (count($arr_tmp2) == 0) {
			return $arr_tmp2;
		}
	}

	// 「毎月／毎隔月／毎3ヶ月ごと」を考慮して戻り値の配列にセット
	switch ($cond1) {
	case "1":  // 毎月
		$step = 1;
		break;
	case "2":  // 毎隔月
		$step = 2;
		break;
	case "3":  // 毎3ヶ月ごと
		$step = 3;
		break;
	}
	for ($i = 0; $i < count($arr_tmp2); $i += $step) {
		array_push($arr, $arr_tmp2[$i]);
	}

	return $arr;
}

// 登録対象日付を配列で返す（祝日）
function get_target_date4($start_ymd, $end_ymd) {
	$arr_date = array();
	$tmp_ymd = $start_ymd;
	while ($tmp_ymd <= $end_ymd) {
		if (get_holiday_name($tmp_ymd) != "") {
			array_push($arr_date, $tmp_ymd);
		}
		$tmp_ymd = next_date($tmp_ymd);
	}
	return $arr_date;
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
	return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 翌週日付をyyyymmdd形式で取得
function next_week($yyyymmdd) {
	return date("Ymd", strtotime("+7 days", to_timestamp($yyyymmdd)));
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}

// 指定年月の日数を求める
function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}
?>
