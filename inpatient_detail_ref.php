<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理｜入退院登録−入院情報参照</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("get_values.ini");
require("conf/sql.inf");
require("inpatient_list_common.php");

$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// チェック情報を取得
$sql = "select * from bedcheck";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 46; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}

// 患者情報を取得
$sql = "select ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_sex, ptif_birth from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel,0,"ptif_ft_kaj_nm");
$sex = get_sex_string(pg_fetch_result($sel, 0, "ptif_sex"));
$age = get_age(pg_fetch_result($sel, 0, "ptif_birth"));

// 入院情報の取得
$sql = "select * from inpthist";
$cond = "where ptif_id = '$pt_id' and inpt_in_dt = '$in_dt' and inpt_in_tm = '$in_tm'";
$sel = select_from_table($con,$sql,$cond,$fname);
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

//-------------------事業所名取得----------------
	$enti_id =  pg_fetch_result($sel,0,"inpt_enti_id");
	$cond_enti = "where enti_id = '$enti_id'";
	$sel_enti = select_from_table($con,$SQL39,$cond_enti,$fname);		//entimstから取得

	if($sel_enti == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$enti_nm =  pg_fetch_result($sel_enti,0,"enti_nm");

//---------------------------------------
$bldg_cd = pg_fetch_result($sel,0,"bldg_cd");
	$cond = "where bldg_cd = '$bldg_cd'";
	$sel_bldg = select_from_table($con,$SQL92,$cond,$fname);		//bldgmstから取得
	if($sel_bldg == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$bldg_nm = pg_fetch_result($sel_bldg,0,"bldg_name");
//---------------------------------------
$ward_cd = pg_fetch_result($sel,0,"ward_cd");
	$cond = "where bldg_cd = '$bldg_cd' and ward_cd = '$ward_cd'";
	$sel_wd = select_from_table($con,$SQL67,$cond,$fname);		//wardmstから取得
	if($sel_wd == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$ward_nm = pg_fetch_result($sel_wd,0,"ward_name");
//---------------------------------------
$ptrm = pg_fetch_result($sel,0,"ptrm_room_no");
	$cond = "where bldg_cd='$bldg_cd' and ward_cd='$ward_cd' and ptrm_room_no = '$ptrm'";
	$sel_rm = select_from_table($con,$SQL94,$cond,$fname);		//ptrmmstから取得
	if($sel_rm == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$ptrm_nm = pg_fetch_result($sel_rm,0,"ptrm_name");
	$bed_chg = pg_fetch_result($sel_rm, 0, "ptrm_bed_chg");
	if ($bed_chg > 0) {
		$ptrm_nm .= "（差額" . number_format($bed_chg) . "円）";
	}
	$ptrm_nm .= "&nbsp;<input type='button' value='設備詳細' onclick=\"window.open('room_equipment_detail.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd&rm_no=$ptrm', 'newwin', 'width=640,height=480,scrollbars=yes');\">";
//---------------------------------------
$in_dt = pg_fetch_result($sel,0,"inpt_in_dt");
$in_yr = substr($in_dt,0,4);
$in_mn = substr($in_dt,4,2);
$in_dy = substr($in_dt,6,2);
$in_tm = pg_fetch_result($sel,0,"inpt_in_tm");
$in_hr = substr($in_tm,0,2);
$in_min = substr($in_tm,2,2);
$in_dttm = $in_yr."/".$in_mn."/".$in_dy." ".$in_hr.":".$in_min;

$arrival = pg_fetch_result($sel, 0, "inpt_arrival");
if (strlen($arrival)) {
	$arv_f_h = substr($arrival, 0, 2);
	$arv_f_m = substr($arrival, 2, 2);
	$arv_t_h = substr($arrival, 4, 2);
	$arv_t_m = substr($arrival, 6, 2);
	$arrival = "{$arv_f_h}:{$arv_f_m} 〜 {$arv_t_h}:{$arv_t_m}";
}

//---------------------------------------
$enti_id = pg_fetch_result($sel,0,"inpt_enti_id");
$sect_id = pg_fetch_result($sel,0,"inpt_sect_id");
	$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id'";
	$sel_sect = select_from_table($con,$SQL109,$cond,$fname);		//sectmstから取得
	if($sel_sect == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$sect_nm = pg_fetch_result($sel_sect,0,"sect_nm");
//---------------------------------------
$dr_id = pg_fetch_result($sel,0,"dr_id");
if ($dr_id == 0) {
	$dr_nm = "";
} else {
	$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and dr_id = '$dr_id'";
	$sel_doc = select_from_table($con,$SQL96,$cond,$fname);		//drmstから取得
	if($sel_doc == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$dr_nm = pg_fetch_result($sel_doc,0,"dr_nm");
}
//---------------------------------------
$nurse_id = pg_fetch_result($sel,0,"nurse_id");
if ($nurse_id == 0) {
	$nurse_nm = "";
} else {
	$cond = "where enti_id = '$enti_id' and sect_id = '$sect_id' and nurse_id = '$nurse_id'";
	$sel_nu = select_from_table($con,$SQL107,$cond,$fname);		//numstから取得
	if($sel_nu == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$nurse_nm = pg_fetch_result($sel_nu,0,"nurse_nm");
}
//---------------------------------------
$blood = pg_fetch_result($sel,0,"inpt_sample_blood");
switch ($blood) {
case "t":
	$blood = "済";
	break;
case "f":
	$blood = "未済";
	break;
default:
	$blood = "";
}
$meet = pg_fetch_result($sel,0,"inpt_meet_flg");
switch ($meet) {
case "0":
	$meet = "可能";
	break;
case "1":
	$meet = "家族のみ";
	break;
case "2":
	$meet = "面会謝絶";
	break;
default:
	$meet = "";
}
$observe = pg_fetch_result($sel,0,"inpt_observe");
switch ($observe) {
case "1":
	$observe = "Ａ";
	break;
case "2":
	$observe = "Ｂ";
	break;
case "3":
	$observe = "Ｃ";
	break;
case "4":
	$observe = "Ｄ";
	break;
case "5":
	$observe = "Ｅ";
	break;
default:
	$observe = "";
}

$dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
switch ($dcb_cls) {
case "1":
	$dcb_cls = "脳血管疾患等";
	break;
case "2":
	$dcb_cls = "高次脳機能障害等";
	break;
case "3":
	$dcb_cls = "大腿骨等";
	break;
case "4":
	$dcb_cls = "外科手術又は肺炎等";
	break;
case "5":
	$dcb_cls = "大腿骨の神経等";
	break;
default:
	$dcb_cls = "";
}

$dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
if (strlen($dcb_bas) == 8) {
	$dcb_bas = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $dcb_bas);
} else {
	$dcb_bas = "";
}
$dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
if (strlen($dcb_exp) == 8) {
	$dcb_exp = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $dcb_exp);
} else {
	$dcb_exp = "";
}

$rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
switch ($rhb_cls) {
case "1":
	$rhb_cls = "心大血管疾患リハビリテーション";
	break;
case "2":
	$rhb_cls = "脳血管疾患等リハビリテーション";
	break;
case "3":
	$rhb_cls = "運動器リハビリテーション";
	break;
case "4":
	$rhb_cls = "呼吸器リハビリテーション";
	break;
default:
	$rhb_cls = "";
}

$rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
if (strlen($rhb_bas) == 8) {
	$rhb_bas = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $rhb_bas);
} else {
	$rhb_bas = "";
}
$rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
if (strlen($rhb_exp) == 8) {
	$rhb_exp = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $rhb_exp);
} else {
	$rhb_exp = "";
}

$purpose_rireki = intval(pg_fetch_result($sel, 0, "inpt_purpose_rireki"));
$purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");

// 入院目的名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A306' and rireki_index = $purpose_rireki and item_cd = '$purpose_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$purpose = pg_fetch_result($sel2, 0, "item_name");

$short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
$outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
$reduced = pg_fetch_result($sel, 0, "inpt_reduced");
$impaired = pg_fetch_result($sel, 0, "inpt_impaired");
$exemption = pg_fetch_result($sel, 0, "inpt_exemption");
$insured = pg_fetch_result($sel, 0, "inpt_insured");

// 手術予定日
$ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
if ($ope_dt_flg == "t") {
	$ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
	$ope_y = substr($ope_dt, 0, 4);
	$ope_m = substr($ope_dt, 4, 2);
	$ope_d = substr($ope_dt, 6, 2);
	$ope_dt = $ope_y . "/" . $ope_m . "/" . $ope_d;
} else if ($ope_dt_flg == "f") {
	$ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
	$ope_y = substr($ope_ym, 0, 4);
	$ope_m = substr($ope_ym, 4, 2);

	$ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
	switch ($ope_td) {
	case "1":
		$ope_td = "/上";
		break;
	case "2":
		$ope_td = "/中";
		break;
	case "3":
		$ope_td = "/下";
		break;
	default:
		$ope_td = "";
		break;
	}

	$ope_dt = $ope_y . "/" . $ope_m . $ope_td;
} else {
	$ope_dt = "";
}

$diagnosis = pg_fetch_result($sel,0,"inpt_diagnosis");
$nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$free = pg_fetch_result($sel, 0, "inpt_free");
switch ($free) {
case "1":
	$free = "��";
	break;
case "2":
	$free = "��";
	break;
case "3":
	$free = "��";
	break;
case "4":
	$free = "��";
	break;
}
$special = pg_fetch_result($sel,0,"inpt_special");
$bed = pg_fetch_result($sel,0,"inpt_bed_no");
$disease = pg_fetch_result($sel,0,"inpt_disease");

$patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
if (strlen($patho_from)) {
	$patho_from = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $patho_from);
}
$patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
if (strlen($patho_to)) {
	$patho_to = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $patho_to);
}
if (strlen($patho_from) || strlen($patho_to)) {
	$patho = "$patho_from 〜 $patho_to";
}

$emergency = pg_fetch_result($sel,0,"inpt_emergency");
switch ($emergency) {
case "1":
	$emergency = "即時入院";
	break;
case "2":
	$emergency = "至急";
	break;
case "3":
	$emergency = "なるべく早く";
	break;
case "4":
	$emergency = "予約順";
	break;
case "5":
	$emergency = "時期指定";
	break;
case "6":
	$emergency = "入院日時決定済み";
	break;
case "7":
	$emergency = "その他";
	break;
default:
	$emergency = "";
}

$period = pg_fetch_result($sel,0,"inpt_in_plan_period");
switch ($period) {
case "A":
	$period = "日帰り";
	break;
case "B":
	$period = "1日";
	break;
case "C":
	$period = "2日";
	break;
case "D":
	$period = "3日";
	break;
case "1":
	$period = "短期";
	break;
case "2":
	$period = "7日以内";
	break;
case "3":
	$period = "14日以内";
	break;
case "E":
	$period = "19日以内";
	break;
case "4":
	$period = "20日以上";
	break;
case "5":
	$period = "その他";
	break;
default:
	$period = "";
}

$way = pg_fetch_result($sel,0,"inpt_in_way");
switch ($way) {
case "1":
	$way = "独歩";
	break;
case "2":
	$way = "車椅子";
	break;
case "3":
	$way = "ストレッチャー";
	break;
case "4":
	$way = "抱っこ";
	break;
default:
	$way = "";
}

$nursing = pg_fetch_result($sel,0,"inpt_nursing");
switch ($nursing) {
case "1":
	$nursing = "常時観察を必要とする";
	break;
case "2":
	$nursing = "継続的に観察を必要とする";
	break;
case "3":
	$nursing = "継続的観察は特に必要としない";
	break;
default:
	$nursing = "";
}

$staple_fd = pg_fetch_result($sel,0,"inpt_staple_fd");
switch ($staple_fd) {
case "1":
	$staple_fd = "米";
	break;
case "2":
	$staple_fd = "軟";
	break;
case "3":
	$staple_fd = "全粥";
	break;
case "4":
	$staple_fd = "7分粥";
	break;
case "5":
	$staple_fd = "5分粥";
	break;
case "6":
	$staple_fd = "3分粥";
	break;
case "7":
	$staple_fd = "絶食";
	break;
default:
	$staple_fd = "";
}

$fd_type = pg_fetch_result($sel,0,"inpt_fd_type");
switch ($fd_type) {
case "1":
	$fd_type = "常食";
	break;
case "2":
	$fd_type = "特別食";
	break;
default:
	$fd_type = "";
}

$fd_dtl = pg_fetch_result($sel,0,"inpt_fd_dtl");

$wish_rm_rireki = intval(pg_fetch_result($sel, 0, "inpt_wish_rm_rireki"));
$wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");

// 希望病室名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A305' and rireki_index = $wish_rm_rireki and item_cd = '$wish_rm_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wish_rm = pg_fetch_result($sel2, 0, "item_name");

$fd_start = pg_fetch_result($sel,0,"inpt_fd_start");
switch ($fd_start) {
case "1":
	$fd_start = "朝";
	break;
case "2":
	$fd_start = "昼";
	break;
case "3":
	$fd_start = "夕";
	break;
default:
	$fd_start = "";
}

$insurance = pg_fetch_result($sel,0,"inpt_insurance");
switch ($insurance) {
case "1":
	$insurance = "一般";
	break;
case "9":
	$insurance = "後期高齢";
	break;
case "2":
	$insurance = "老人";
	break;
case "3":
	$insurance = "労災";
	break;
case "4":
	$insurance = "自賠責";
	break;
case "5":
	$insurance = "介護";
	break;
case "6":
	$insurance = "生保";
	break;
case "7":
	$insurance = "自費";
	break;
case "8":
	$insurance = "公傷";
	break;
default:
	$insurance = "";
}

$insu_rate_rireki = intval(pg_fetch_result($sel, 0, "inpt_insu_rate_rireki"));
$insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");

// 医療保険負担割合名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A301' and rireki_index = $insu_rate_rireki and item_cd = '$insu_rate_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$insu_rate = pg_fetch_result($sel2, 0, "item_name");

$privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
switch ($privacy_flg) {
case "1":
	$privacy_flg = "要望有り";
	break;
case "2":
	$privacy_flg = "要望無し";
	break;
default:
	$privacy_flg = "";
}

$privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");

$pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
switch ($pre_div) {
case "1":
	$pre_div = "自宅";
	break;
case "2":
	$pre_div = "医療機関他";
	break;
default:
	$pre_div = "";
}

$care_no = pg_fetch_result($sel,0,"inpt_care_no");

$care_grd_rireki = intval(pg_fetch_result($sel, 0, "inpt_care_grd_rireki"));
$care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");

$care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
if (strlen($care_apv) == 8) {
	$care_apv = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $care_apv);
} else {
	$care_apv = "";
}

// 要介護度名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A302' and rireki_index = $care_grd_rireki and item_cd = '$care_grd_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$care_grd = pg_fetch_result($sel2, 0, "item_name");

$care_from = pg_fetch_result($sel, 0, "inpt_care_from");
if (strlen($care_from)) {
	$care_from = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $care_from);
}
$care_to = pg_fetch_result($sel, 0, "inpt_care_to");
if (strlen($care_to)) {
	$care_to = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $care_to);
}
if (strlen($care_from) || strlen($care_to)) {
	$care = "$care_from 〜 $care_to";
}

$dis_grd_rireki = intval(pg_fetch_result($sel, 0, "inpt_dis_grd_rireki"));
$dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");

// 身体障害者手帳等級名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A303' and rireki_index = $dis_grd_rireki and item_cd = '$dis_grd_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dis_grd = pg_fetch_result($sel2, 0, "item_name");

$dis_type_rireki = intval(pg_fetch_result($sel, 0, "inpt_dis_type_rireki"));
$dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");

// 障害種別名称を取得
$sql = "select item_name from tmplitemrireki";
$cond = "where mst_cd = 'A304' and rireki_index = $dis_type_rireki and item_cd = '$dis_type_cd'";
$sel2 = select_from_table($con, $sql, $cond, $fname);
if ($sel2 == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dis_type = pg_fetch_result($sel2, 0, "item_name");

$spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");

$spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
if (strlen($spec_from)) {
	$spec_from = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $spec_from);
}
$spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
if (strlen($spec_to)) {
	$spec_to = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $spec_to);
}
if (strlen($spec_from) || strlen($spec_to)) {
	$spec = "$spec_from 〜 $spec_to";
}

// 病棟一覧を取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 紹介元医療機関名を取得
$intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
if ($intro_inst_cd != "") {
	$sql = "select mst_name from institemmst";
	$cond = "where mst_cd = '$intro_inst_cd'";
	$sel2 = select_from_table($con, $sql, $cond, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$intro_inst_name = pg_fetch_result($sel2, 0, "mst_name");

	// 診療科名・前医名を取得
	$intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
	$intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
	$intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
	if ($intro_sect_rireki != "") {
		$sql = "select * from institemrireki";
		$cond = "where mst_cd = '$intro_inst_cd' and rireki_index = '$intro_sect_rireki' and item_cd = '$intro_sect_cd'";
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$intro_sect_name = pg_fetch_result($sel2, 0, "item_name");
		if ($intro_doctor_no != "") {
			$intro_doctor_name = pg_fetch_result($sel2, 0, "doctor_name$intro_doctor_no");
		}
	}
}

$hotline = (pg_fetch_result($sel, 0, "inpt_hotline") == "t") ? "チェックあり" : "";

// 担当者情報を取得
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, jobmst.job_nm from inptophist inner join empmst on empmst.emp_id = inptophist.emp_id inner join jobmst on jobmst.job_id = empmst.emp_job";
$cond = "where inptophist.ptif_id = '$pt_id' and inptophist.inpt_in_dt = '$in_dt' and inptophist.inpt_in_tm = '$in_tm' order by inptophist.order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$operators = array();
while ($row = pg_fetch_array($sel)) {
	$operators[] = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}（{$row["job_nm"]}）";
}
$operators = join(", ", $operators);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	setListDateDisabled();
}

<? show_inpatient_list_common_javascript(); ?>
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}

.list2 {border-collapse:collapse;}
.list2 td {border:#0e9c55 solid 1px;}

.list3 {border-collapse:collapse;}
.list3 td {border:#b8860b solid 1px;}

.list4 {border-collapse:collapse;}
.list4 td {border:#c71585 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入退院登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="70%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入院予定〜退院登録</b></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>検索条件設定</b></font></td>
</tr>
<tr>
<td valign="middle">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="center"><form action="inpatient_vacant_search.php" method="get"><input type="submit" value="空床検索"><input type="hidden" name="session" value="<? echo($session); ?>"></form><br><form action="inpatient_waiting_list.php" method="get"><input type="submit" value="待機患者"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_reserve_register_list.php" method="get"><input type="submit" value="入院予定"><input type="hidden" name="session" value="<? echo($session); ?>"><input type="hidden" name="mode" value="search"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_register_list.php" method="get"><input type="submit" value="入院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_go_out_register_list.php" method="get"><input type="submit" value="外出・外泊"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="inpatient_move_register_list.php" method="get"><input type="submit" value="転棟"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_reserve_register_list.php" method="get">
<input type="submit" value="退院予定"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
<td align="center"><img src="img/right.gif" alt="" width="17" height="17"></td>
<td align="center"><form action="out_inpatient_register_list.php" method="get"><input type="submit" value="退院"><input type="hidden" name="session" value="<? echo($session); ?>"></form></td>
</tr>
</table>
</td>
<td valign="middle">
<? show_inpatient_list_form($con, $sel_ward, "", "", "", "", "", "", "", "", "", $session, $fname); ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_sub.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_contact.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="50" align="center" bgcolor="#bdd1e7"><a href="inpatient_patient_claim.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="inpatient_detail_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>入院情報</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="inpatient_check_list_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チェックリスト</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="inpatient_nutrition_ref.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">栄養問診表</font></a></td>
<td width="5">&nbsp;</td>
<td width="170" align="center" bgcolor="#bdd1e7"><a href="inpatient_exception.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&path=6&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均在院日数除外指定</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="outpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&in_dt=<? echo($in_dt) ?>&in_tm=<? echo($in_tm) ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退院情報</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者ID</font></td>
<td width="18%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者氏名</font></td>
<td width="18%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
<td width="8%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td width="9%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sex); ?></font></td>
<td width="8%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td width="9%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($age); ?>歳</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="24%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td width="28%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$bldg_nm$ward_nm"); ?></font></td>
<td width="15%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室</font></td>
<td width="33%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ptrm_nm); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット番号</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($bed); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($in_dttm); ?></font></td>
</tr>
<? if ($display19 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">到着時間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arrival); ?></font></td>
</tr>
<? } ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($sect_nm); ?></font></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主治医</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dr_nm); ?></font></td>
</tr>
<? if ($display1 == "t" || $display2 == "t") { ?>
<tr height="22">
<? if ($display1 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当看護師</font></td>
<td<? if ($display2 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($nurse_nm); ?></font></td>
<? } ?>
<? if ($display2 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診断名(主病名)</font></td>
<td<? if ($display1 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($disease); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display22 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($operators); ?></font></td>
</tr>
<? } ?>
<? if ($display21 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発症年月日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($patho); ?></font></td>
</tr>
<? } ?>
<? if ($display3 == "t" || $display23 == "t" || $display4 == "t" || $display20 == "t") { ?>
<tr height="22">
<? if ($display3 == "t" || $display23 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院目的</font></td>
<td<? if ($display4 != "t" && $display20 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($display3 == "t") { ?>
<span style="margin-right:5px;"><? echo($purpose); ?></span>
<? } ?>
<? if ($display23 == "t" && $short_stay == "t") { ?>
<nobr>短期入所</nobr>
<? } ?>
<? if ($display3 == "t") { ?>
<? if ($display23 == "t" || $display4 == "t" || $display20 == "t") {echo("<br>");} ?>
その他：<? echo($purpose_content); ?>
<? } ?>
</font></td>
<? } ?>
<? if ($display4 == "t" || $display20 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院前検査</font></td>
<td<? if ($display3 != "t" && $display23 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($display4 == "t") { ?>
<span style="margin-right:5px;">採血：<? echo($blood); ?></span>
<? } ?>
<? if ($display20 == "t" && $outpt_test == "t") { ?>
<nobr>外来検査</nobr>
<? } ?>
<? if ($display4 == "t") { ?>
<? if ($display20 == "t" || $display3 == "t" || $display23 == "t") {echo("<br>");} ?>
その他：<? echo($diagnosis); ?>
<? } ?>
</font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display5 == "t" || $display6 == "t") { ?>
<tr height="22">
<? if ($display5 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">緊急度</font></td>
<td<? if ($display6 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emergency); ?></font></td>
<? } ?>
<? if ($display6 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院予定期間</font></td>
<td<? if ($display5 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($period); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display7 == "t" || $display8 == "t") { ?>
<tr height="22">
<? if ($display7 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入院方法</font></td>
<td<? if ($display8 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($way); ?></font></td>
<? } ?>
<? if ($display8 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護度</font></td>
<td<? if ($display7 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($nursing); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display9 == "t" || $display10 == "t") { ?>
<tr height="22">
<? if ($display9 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">主食</font></td>
<td<? if ($display10 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($staple_fd); ?></font></td>
<? } ?>
<? if ($display10 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事内容</font></td>
<td<? if ($display9 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo($fd_type); ?><? if ($display9 == "t") {echo("<br>");} else {echo("&nbsp;");} ?>
詳細：<? echo($fd_dtl); ?>
</font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display11 == "t" || $display12 == "t") { ?>
<tr height="22">
<? if ($display11 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">希望病室</font></td>
<td<? if ($display12 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($wish_rm); ?></font></td>
<? } ?>
<? if ($display12 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">食事開始帯</font></td>
<td<? if ($display11 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($fd_start); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display13 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($insurance); ?></font></td>
</tr>
<? } ?>
<? if ($display24 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">医療保険負担割合</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($insu_rate); ?></font></td>
</tr>
<? } ?>
<? if ($display25 == "t" || $display26 == "t") { ?>
<tr height="22">
<? if ($display25 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">減額認定有り</font></td>
<td<? if ($display26 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($reduced == "t") {echo("有り");} ?></font></td>
<? } ?>
<? if ($display26 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マル障有り</font></td>
<td<? if ($display25 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($impaired == "t") {echo("有り");} ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display46 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">差額室料免除（減免）</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? if ($exemption == "t") {echo("有り");} ?></font></td>
</tr>
<? } ?>
<? if ($display27 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険者名称</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($insured); ?></font></td>
</tr>
<? } ?>
<? if ($display18 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">個人情報に関する要望</font></td>
<td colspan="3">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($privacy_flg); ?></td>
<td style="padding-left:2px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $privacy_text)); ?></td>
</tr>
</table>
</td>
</tr>
<? } ?>
<? if ($display28 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前入院先区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pre_div); ?></font></td>
</tr>
<? } ?>
<? if ($display14 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手術予定日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($ope_dt); ?></font></td>
</tr>
<? } ?>
<? if ($display15 == "t" || $display16 == "t") { ?>
<tr height="22">
<? if ($display15 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">観察</font></td>
<td<? if ($display16 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($observe); ?></font></td>
<? } ?>
<? if ($display16 == "t") { ?>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">面会</font></td>
<td<? if ($display15 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($meet); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display45 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">看護度分類</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
看護観察の程度：<? echo($nrs_obsv); ?>
<span style="margin-left:10px;">生活の自由度：<? echo($free); ?></span>
</font></td>
</tr>
<? } ?>
<? if ($display17 == "t") { ?>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特記事項</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $special)); ?></font></td>
</tr>
<? } ?>
</table>

<? if ($display29 == "t" || $display30 == "t" || $display31 == "t" || $display32 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜紹介情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list2">
<tr>
<td width="24%" style="border-style:none;"></td>
<td width="28%" style="border-style:none;"></td>
<td width="15%" style="border-style:none;"></td>
<td width="33%" style="border-style:none;"></td>
</tr>
<? if ($display29 == "t" || $display30 == "t") { ?>
<tr height="22">
<? if ($display29 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">紹介元医療機関</font></td>
<td<? if ($display30 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intro_inst_name); ?></font></td>
<? } ?>
<? if ($display30 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td<? if ($display29 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intro_sect_name); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display31 == "t" || $display32 == "t") { ?>
<tr height="22">
<? if ($display31 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">前医</font></td>
<td<? if ($display32 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intro_sect_name); ?></font></td>
<? } ?>
<? if ($display32 == "t") { ?>
<td bgcolor="#ceefc2" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ホットライン</font></td>
<td<? if ($display31 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($hotline); ?></font></td>
<? } ?>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display33 == "t" || $display34 == "t" || $display35 == "t" || $display36 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜リハビリ情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list3">
<tr>
<td width="24%" style="border-style:none;"></td>
<td width="28%" style="border-style:none;"></td>
<td width="15%" style="border-style:none;"></td>
<td width="33%" style="border-style:none;"></td>
</tr>
<? if ($display33 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定期限分類</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dcb_cls); ?></font></td>
</tr>
<? } ?>
<? if ($display34 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定基準日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dcb_bas); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">回復期リハ算定期限日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dcb_exp); ?></font></td>
</tr>
<? } ?>
<? if ($display35 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定期限分類</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($rhb_cls); ?></font></td>
</tr>
<? } ?>
<? if ($display36 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定基準日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($rhb_bas); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リハビリ算定期限日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($rhb_exp); ?></font></td>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display37 == "t" || $display38 == "t" || $display39 == "t" || $display40 == "t" || $display41 == "t" || $display42 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜社会資源情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list3">
<tr>
<td width="24%" style="border-style:none;"></td>
<td width="28%" style="border-style:none;"></td>
<td width="15%" style="border-style:none;"></td>
<td width="33%" style="border-style:none;"></td>
</tr>
<? if ($display37 == "t" || $display38 == "t") { ?>
<tr height="22">
<? if ($display37 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">介護保険被保険者番号</font></td>
<td<? if ($display38 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($care_no); ?></font></td>
<? } ?>
<? if ($display38 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度</font></td>
<td<? if ($display37 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($care_grd); ?></font></td>
<? } ?>
</tr>
<? } ?>
<? if ($display39 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度認定年月日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($care_apv); ?></font></td>
</tr>
<? } ?>
<? if ($display40 == "t") { ?>
<tr height="22">
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">要介護度の有効期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($care); ?></font></td>
</tr>
<? } ?>
<? if ($display41 == "t" || $display42 == "t") { ?>
<tr height="22">
<? if ($display41 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">身体障害者手帳等級</font></td>
<td<? if ($display42 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dis_grd); ?></font></td>
<? } ?>
<? if ($display42 == "t") { ?>
<td bgcolor="#fffacd" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">障害種別</font></td>
<td<? if ($display41 != "t") {echo(' colspan="3"');} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($dis_type); ?></font></td>
<? } ?>
</tr>
<? } ?>
</table>
<? } ?>

<? if ($display43 == "t" || $display44 == "t") { ?>
<table width="700" border="0" cellspacing="0" cellpadding="2" style="margin-top:6px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜特定疾患情報＞</b></font></td>
</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="2" class="list4">
<tr>
<td width="24%" style="border-style:none;"></td>
<td width="28%" style="border-style:none;"></td>
<td width="15%" style="border-style:none;"></td>
<td width="33%" style="border-style:none;"></td>
</tr>
<? if ($display43 == "t") { ?>
<tr height="22">
<td bgcolor="#ffe4e1" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特定疾患の疾患名</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($spec_name); ?></font></td>
</tr>
<? } ?>
<? if ($display44 == "t") { ?>
<tr height="22">
<td bgcolor="#ffe4e1" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">特定疾患の有効期間</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($spec); ?></font></td>
</tr>
<? } ?>
</table>
<? } ?>

</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function get_sex_string($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不明";
	}
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
