<?php

require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $_SERVER['PHP_SELF'];

///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
// 入力内容チェック
///-----------------------------------------------------------------------------
// 集計名
foreach($_POST['name'] as $id => $r) {
    if (mb_strwidth($r, 'EUC-JP') > 20) {
        echo("<script type=\"text/javascript\">alert('集計名「$r」が20字を超えています');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}
// 集計値
foreach($_POST['value'] as $v) {
    foreach($v as $id => $r) {
        if (preg_match("/[^0-9\-\.]/", $r)) {
            echo("<script type=\"text/javascript\">alert('集計値に数字以外が含まれています');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }
    }
}

///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
pg_query($con, "begin transaction");

///-----------------------------------------------------------------------------
// 変更スイッチ
///-----------------------------------------------------------------------------
$pattern_id = $_POST['pattern_id'];
$sum_change_sw = ($_POST['change_switch'] === '') ? 0 : 2;

$pg_pattern_id = p($pattern_id);
$sql_sw = "SELECT sum_change_sw FROM duty_shift_entity_sum_change_sw WHERE group_id = {$pg_pattern_id}";
$sel_sw = select_from_table($con, $sql_sw, "", $fname);
if ($sel_sw == 0) {
    pg_close($con);
    js_error_exit();
}
$if_change_switch = pg_num_rows($sel_sw);
$set = array(
    'group_id' => $pg_pattern_id,
    'sum_change_sw' => $sum_change_sw,
);

if ($if_change_switch) {
    $sql_sw2 = "UPDATE duty_shift_entity_sum_change_sw SET";
    $cond = "WHERE group_id = '{$pg_pattern_id}'";
    $upd_sw = update_set_table($con, $sql_sw2, array_keys($set), array_values($set), $cond, $fname);
    if ($upd_sw == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }
}
else {
    $sql_sw3 = "INSERT INTO duty_shift_entity_sum_change_sw (group_id,sum_change_sw) VALUES (";
    $ins_sw = insert_into_table($con, $sql_sw3, array_values($set), $fname);
    if ($ins_sw == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }
}

///-----------------------------------------------------------------------------
// 集計
///-----------------------------------------------------------------------------
$name = array();
$value = array();

// DELETE FROM duty_shift_sign_count_name
$sql_nm = "DELETE FROM duty_shift_sign_count_name WHERE group_id = {$pg_pattern_id}";
$del_nm = delete_from_table($con, $sql_nm, "", $fname);
if ($del_nm == 0) {
	pg_query($con, "rollback");
	pg_close($con);
    js_error_exit();
}

// DELETE FROM duty_shift_sign_count_value
$sql_va = "DELETE FROM duty_shift_sign_count_value WHERE group_id = {$pg_pattern_id}";
$del_va = delete_from_table($con, $sql_va, "", $fname);
if ($del_nm == 0) {
	pg_query($con, "rollback");
	pg_close($con);
    js_error_exit();
}

for ($i = 1; $i <= count($_POST["name"]); $i++) {
    $count_job = array();
    foreach ($_POST["count_job"][$i] as $job) {
        if (!$job) {
            continue;
        }
        $count_job[$job] = 1;
    }

    $name = array(
        'group_id'      => $pg_pattern_id,
        'count_id'      => $i,
        'count_name'    => p($_POST['name'][$i]),
        'count_row_flg' => $_POST["count_row"][$i] ? 't' : 'f',
        'count_col_flg' => $_POST["count_col"][$i] ? 't' : 'f',
        'job_filter'    => p(serialize($count_job)),
        'order_no'      => $i,
    );

    $sql_nm = "INSERT INTO duty_shift_sign_count_name (" . implode(',', array_keys($name)) . ") VALUES (";
    $ins_nm = insert_into_table($con, $sql_nm, array_values($name), $fname);
    if ($ins_nm == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        js_error_exit();
    }

    foreach ($_POST['value'] as $ptn => $values) {
        if ($_POST["value"][$ptn][$i] === '0' or !$_POST["value"][$ptn][$i]) {
            continue;
        }
        $value = array(
            'group_id'    => $pg_pattern_id,
            'count_id'    => $i,
            'pattern'     => p(substr($ptn, 1)),
            'count_value' => p($values[$i]),
        );

        $sql_va = "INSERT INTO duty_shift_sign_count_value (" . implode(',', array_keys($value)) . ") VALUES (";
        $ins_va = insert_into_table($con, $sql_va, array_values($value), $fname);
        if ($ins_va == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
    }
}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
pg_query("commit");

///-----------------------------------------------------------------------------
// データベース接続を閉じる
///-----------------------------------------------------------------------------
pg_close($con);

?>
<script type="text/javascript">
    location.href = 'duty_shift_entity_sign_count.php?session=<?=h($_POST['session'])?>&pattern_id=<?=h($pattern_id)?>';
</script>