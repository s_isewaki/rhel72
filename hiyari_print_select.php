<?php
//==============================
//本処理
//==============================
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ( $summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ( $con == "0" ) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//情報取得
//==============================
$PAGE_TITLE = "印刷選択";

//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
function show_report_print() {
<?php
$print_setting = get_print_setting($con,$fname);
$patient_non_print_flg = $print_setting["patient_non_print_flg"] == "t";
$patient_non_print_select_flg = $print_setting["patient_non_print_select_flg"] == "t";

if ( $patient_non_print_flg ) {
?>
    var patient_non_print_flg = true;
<?php } else { ?>
    var patient_non_print_flg = false;
<?php } ?>

<?php if($patient_non_print_select_flg) { ?>
    patient_non_print_flg = !confirm("患者ID、氏名の印刷を有効にしますか？");
<?php } ?>	
    var param_patient_non_print = "false";
    if ( patient_non_print_flg ) {
        param_patient_non_print = "true";
    }

    var url = "hiyari_report_print.php?session=<?=$session?>&report_id=<?=$report_id?>&eis_id=<?=$eis_id?>&patient_non_print=" + param_patient_non_print;
    window.open(url);
    close();
}
function show_mail_print() {
    var url = "hiyari_mail_print.php?session=<?=$session?>&mail_id=<?=$mail_id?>";
    window.open(url);
    close();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#35B341 solid 0px;}
table.block_in td td {border-width:1;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?php show_hiyari_header_for_sub_window($PAGE_TITLE); ?>
</table>
<!-- ヘッダー END -->

</td></tr><tr><td>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0" width="250">
	<tr>
		<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
		<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
	</tr>
	<tr>
		<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
		<td bgcolor="#F5FFE5">

			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td align="center" bgcolor="#FFFFFF">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						<input type="button" value="メール印刷" onclick="show_mail_print()">
						</font>
					</td>
					<td align="center" bgcolor="#FFFFFF">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
						<input type="button" value="報告書印刷" onclick="show_report_print()">
						</font>
					</td>
				</tr>
			</table>

		</td>
		<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
	</tr>
	<tr>
		<td><img src="img/r_3.gif" width="10" height="10"></td>
		<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
		<td><img src="img/r_4.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- 本体 END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?php pg_close($con); //DBコネクション終了 ?>

