<?
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// 説明文の内容をDELETE〜INSERT
$sql = "delete from wkfwexplanation";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == "0") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$content_list = $_POST["content"];

//　説明文の内容を登録
if (!empty($content_list)){
	$no = 1;
	foreach ($content_list as $content){
		$sql = "insert into wkfwexplanation (id, content) values (";
		$content = array($no, pg_escape_string($content));
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == "0") {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$no++;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'workflow_explanation_setting.php?session=$session&show_mode=setting';</script>");
