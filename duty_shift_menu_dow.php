<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜曜日別集計表</title>

<?php
//************************************************************************
// 勤務シフト作成　曜日別集計表・画面
//************************************************************************
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_auto_bunsan_common_class.php");
require_once("duty_shift_auto_hindo_class.php");
ob_end_clean();

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;

///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);

///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];

///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;

///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm); //20140210
$data_wktmgrp = $obj->get_wktmgrp_array();

///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
$group_array = $obj->get_valid_group_array($group_array, $emp_id, "", "");
if (count($group_array) > 0) {
	if ($group_id == "") {
		//画面初期表示時
		$group_id = $group_array[0]["group_id"];
		$group_name = $group_array[0]["group_name"];
		$pattern_id = $group_array[0]["pattern_id"];
		$start_month_flg1 = $group_array[0]["start_month_flg1"];
		$start_day1 = $group_array[0]["start_day1"];
		$month_flg1 = $group_array[0]["month_flg1"];
		$end_day1 = $group_array[0]["end_day1"];
		$start_day2 = $group_array[0]["start_day2"];
		$month_flg2 = $group_array[0]["month_flg2"];
		$end_day2 = $group_array[0]["end_day2"];
		$print_title = $group_array[0]["print_title"];
		$reason_setting_flg = $group_array[0]["reason_setting_flg"];
	} else {
		//病棟名リストボックス選択時
		for ($k=0; $k < count($group_array); $k++) {
			if ($group_array[$k]["group_id"] == $group_id) {
				$group_name = $group_array[$k]["group_name"];
				$pattern_id = $group_array[$k]["pattern_id"];
				$start_month_flg1 = $group_array[$k]["start_month_flg1"];
				$start_day1 = $group_array[$k]["start_day1"];
				$month_flg1 = $group_array[$k]["month_flg1"];
				$end_day1 = $group_array[$k]["end_day1"];
				$start_day2 = $group_array[$k]["start_day2"];
				$month_flg2 = $group_array[$k]["month_flg2"];
				$end_day2 = $group_array[$k]["end_day2"];
				$print_title = $group_array[$k]["print_title"];
				$reason_setting_flg = $group_array[$k]["reason_setting_flg"];
			}
		}
	}
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
if ($start_day2 == "" || $start_day2 == "0") {	$start_day2 = "0";}
if ($month_flg2 == "" || $end_day2 == "") {	$month_flg2 = "1";}
if ($end_day2 == "" || $end_day2 == "0") {	$end_day2 = "99";}

// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

$start_date = $arr_date[0];
$end_date = $arr_date[1];

$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$day_cnt=count($calendar_array);

///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$tmp_date = $start_date;
for ($k=1; $k<=$day_cnt; $k++) {
	$tmp_date = strtotime($tmp_date);
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	$tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}

///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
//応援追加されている出勤パターングループ確認 
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
}
//応援なしの場合、同じパターングループのデータを設定
else {
    $data_atdptn_all = $data_atdptn;
    $data_pattern_all = $data_pattern;
}
//20140220 end

///-----------------------------------------------------------------------------
//遷移元のデータに変更
///-----------------------------------------------------------------------------
$set_data = array();
$plan_array = $obj->get_duty_shift_plan_array(
		$group_id, $pattern_id,
		$duty_yyyy, $duty_mm, $day_cnt,
		$individual_flg,
		$hope_get_flg,
		$show_data_flg,
		$set_data, $week_array, $calendar_array,
		$data_atdptn, $data_pattern_all,
		$data_st, $data_job, $data_emp);

///*****************************************************************************
//頻度分布クラスにある頻度集計テーブルを使う
///*****************************************************************************
///-----------------------------------------------------------------------------
//グループデータ(分散クラス仕様)
///-----------------------------------------------------------------------------
$obj_bunsan = new duty_shift_auto_bunsan_common_class($group_id, $con, $fname);
$group_table = $obj_bunsan->duty_shift_group_table();

///-----------------------------------------------------------------------------
// 集計開始日〜終了日
///-----------------------------------------------------------------------------
//開始日
$wk_minus_m = 0;
if ($start_month_flg == "0") {
	$wk_minus_m = -1;
}
$wk_minus_m -= (int)$group_table["auto_check_months"];
$wk_start_date = date("Ym", strtotime("$wk_minus_m month", $obj->to_timestamp(sprintf("%04d%02d%02d", $duty_yyyy, $duty_mm, 1)))). substr($start_date, 6, 2);
//終了日(表示日の前日)
$wk_end_date = date("Ymd", strtotime("-1 day", $obj->to_timestamp($start_date)));

///-----------------------------------------------------------------------------
//職員データ(分散クラス仕様)
///-----------------------------------------------------------------------------
$staff_table = $obj_bunsan->duty_shift_staff_table($plan_array, $duty_yyyy, $duty_mm);

///-----------------------------------------------------------------------------
//パターンデータ(分散クラス仕様)
///-----------------------------------------------------------------------------
$atdptn_table = $obj_bunsan->duty_shift_pattern_table();
$ptn_count = count($atdptn_table);
$ptn_colspan = $ptn_count * 9 ;

///-----------------------------------------------------------------------------
//頻度集計テーブル
///-----------------------------------------------------------------------------
$hindo_obj = new duty_shift_auto_hindo_class(
	$group_id,
	$wk_start_date,
	$wk_end_date,
	$staff_table,
	$atdptn_table,
	$con,
	$fname
	);
$hindo_table = $hindo_obj->get_hindo_table();

///-----------------------------------------------------------------------------
//HTML
///-----------------------------------------------------------------------------
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	//曜日別集計表
	function makeExcelDow() {
		document.mainform.action = "duty_shift_menu_excel_dow.php";
		document.mainform.target = "";
		document.mainform.submit();
	}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

img.close {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/minus.gif");
	vertical-align:middle;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? // 全画面ではない
if ($fullscreen_flg != "1") {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
		<?
			show_user_title($session, $section_admin_auth); //duty_shift_common.ini
		?>
		</tr></table>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
		<?
			$arr_option = "&group_id=" . $group_id . "&duty_yyyy=" . $duty_yyyy ."&duty_mm=" . $duty_mm;
			show_user_tab_menuitem($session, $fname, $arr_option); //duty_shift_user_tab_common.ini
		?>
		</tr></table>

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
		</table>
		<img src="img/spacer.gif" width="1" height="5" alt=""><br>
	</td></tr></table>
<? // 全画面時
} else {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr bgcolor="#5279a5">
	<td width="100%" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>曜日別集計表 <?=$group_name?></b></font></td>
	<td width="32" bgcolor="#dddddd" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
<?
}
?>

	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
	
		<br>
		<table border="1" cellspacing="0" cellpadding="5">
		<tr>
			<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シフトグループ（病棟）名</font></td>
			<td><select name="group_id" onchange="this.form.action = '<? echo($fname); ?>'; this.form.submit();">
<?
for ($k=0; $k<count($group_array); $k++) {
	$wk_id = $group_array[$k]["group_id"];
	$wk_name = $group_array[$k]["group_name"];
	echo("<option  value=\"$wk_id\"");
	if ($group_id == $wk_id) {
		echo(" selected");
	}
	echo(">$wk_name  \n");
}
			?>
			</select></td>
		</tr>
		<tr>
			<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計期間</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo preg_replace("/(\d\d\d\d)(\d\d)(\d\d)/","$1/$2/$3",$wk_start_date);?> 〜 <? echo preg_replace("/(\d\d\d\d)(\d\d)(\d\d)/","$1/$2/$3",$wk_end_date);?></font></td>
		</tr>
		<tr>
			<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Excel出力</font></td>
			<td><input type="button" value="Excel出力" onclick="makeExcelDow();"></td>
		</tr>
		</table>
		<br>


<?
$html = <<<_HTML_END_
<table id="header" border="1" cellspacing="0" cellpadding="1">
<tr height="22">
<td width="20"  align="center" rowspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">番号</font></td>
<td width="40"  align="center" rowspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">チーム</font></td>
<td width="100" align="center" rowspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
<td width="40"  align="center" rowspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
<td width="40"  align="center" rowspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
_HTML_END_;

//曜日
foreach( array('月','火','水','木','金','土','日','祝日','祝日前日') as $dow ) {
	$html .= sprintf('<td align="center" colspan="%s"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">%s</font></td>', $ptn_count, $dow);
}

$html .= <<<_HTML_END_
</tr>
<tr height="22">
_HTML_END_;

//パターン
for( $i=0; $i<9; $i++ ) {
	foreach( $atdptn_table as $ptn ) {
		$size = ( mb_strwidth($ptn["atdptn_nm"]) > 4 ) ? 1 : 2;
		$html .= sprintf('<td width="20" align="center"><font size="%s" face="ＭＳ Ｐゴシック, Osaka" class="j12">%s</font></td>', $size, $ptn["atdptn_nm"]);
	}
}

$html .= '</tr>';

//職員
foreach( $plan_array as $i => $staff ) {
	$color = ( $staff["sex"] == 1 ) ? 'blue' : 'black';
	$html .= <<<_HTML_END_
	<tr height="22">
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$staff["no"]}</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$staff["team_name"]}</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="{$color}">{$staff["staff_name"]}</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$staff["job_name"]}</font></td>
<td align="center" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">{$staff["status"]}</font></td>
_HTML_END_;

	//パターン
	foreach( array(1,2,3,4,5,6,0,7,8) as $week ) {
		foreach( $atdptn_table as $ptn_id => $ptn ) {
			$color = 'black';
			if ( $hindo_table[$week][$ptn_id][$staff["staff_id"]] == 0 ) {
				$color = 'silver';
			}
			elseif ( $hindo_table[$week][$ptn_id][$staff["staff_id"]] >= 10 ) {
				$color = 'blue';
			}
			$html .= sprintf('<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="%s">%d</font></td>', $color, $hindo_table[$week][$ptn_id][$staff["staff_id"]]);
		}
	}
	
	$html .= '</tr>';
}
$html .= '</table>';
?>
<? echo $html; ?>

	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="duty_yyyy" value="<? echo($duty_yyyy); ?>">
	<input type="hidden" name="duty_mm" value="<? echo($duty_mm); ?>">
	</form>

</td></tr></table>
</body>
</html>
<? pg_close($con); ?>