<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="workflow_category_register.php">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="category_name" value="<?=$category_name?>">


<input type="hidden" name="src_cate_id" value="<?=$src_cate_id?>">
<input type="hidden" name="src_folder_id" value="<?=$src_folder_id?>">

<input type="hidden" name="dest_cate_id" value="<?=$dest_cate_id?>">
<input type="hidden" name="dest_folder_id" value="<?=$dest_folder_id?>">

<input type="hidden" name="ref_dept_st_flg" value="<? echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<? echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<? echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<? echo($ref_atrb_src); ?>">
<?
if (is_array($hid_ref_dept)) {
	foreach ($hid_ref_dept as $tmp_dept_id) {
		echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
	}
} else {
	$hid_ref_dept = array();
}
?>
<input type="hidden" name="ref_st_flg" value="<? echo($ref_st_flg); ?>">
<?
if (is_array($ref_st)) {
	foreach ($ref_st as $tmp_st_id) {
		echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
	}
} else {
	$ref_st = array();
}
?>
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">
<?
if ($target_id_list1 != "") {
	$hid_ref_emp = split(",", $target_id_list1);
} else {
	$hid_ref_emp = array();
}
?>
<input type="hidden" name="ref_toggle_mode" value="<? echo($ref_toggle_mode); ?>">




</form>
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("application_workflow_common_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$wkfw = check_authority($session,3,$fname);
if($wkfw == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// DBコネクション作成
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);


// フォルダ名未入力
if($category_name == ""){
	echo("<script language=\"javascript\">alert(\"フォルダ名が入力されていません。\");</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
// フォルダ名文字数
if(strlen($category_name) > 30){
	echo("<script language=\"javascript\">alert(\"フォルダ名が長すぎます。\");</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
// 利用権限
if ((($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0) {
	echo("<script type=\"text/javascript\">alert('利用可能範囲が不正です。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}


// 登録内容の編集
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}


$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");


if($dest_cate_id == "")
{
	// カテゴリ取得(wkfw_typeの最大値取得)
	$max = get_max_wkfw_type($con, $fname);
	if($max == "")
	{
		$wkfw_type = 1;
	}
	else
	{
		$wkfw_type = $max+1;
	}

	// カテゴリ登録
	insert_wkfwcatemst($con, $fname, $wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);

	// カテゴリ用アクセス権（科）登録
	$obj->delete_regist_wkfwcate_refdept($wkfw_type, $hid_ref_dept, "");

	// カテゴリ用アクセス権（役職）登録
	$obj->delete_regist_wkfwcate_refst($wkfw_type, $ref_st, "");

	// カテゴリ用アクセス権（職員）
	$obj->delete_regist_wkfwcate_refemp($wkfw_type, $hid_ref_emp, "");

}
else if($dest_cate_id != "")
{

	// フォルダ取得(wkfw_folder_idの最大値取得)
	$max = get_max_wkfw_folder_id($con, $fname);
	if($max == "")
	{
		$wkfw_folder_id = 1;
	}
	else
	{
		$wkfw_folder_id = $max+1;
	}

	// フォルダ登録
	insert_wkfwfolder($con, $fname, $wkfw_folder_id, $dest_cate_id, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);

	// フォルダ用アクセス権（科）登録
	$obj->delete_regist_wkfwfolder_refdept($wkfw_folder_id, $hid_ref_dept, "");

	// フォルダ用アクセス権（役職）登録
	$obj->delete_regist_wkfwfolder_refst($wkfw_folder_id, $ref_st, "");

	// フォルダ用アクセス権（職員）
	$obj->delete_regist_wkfwfolder_refemp($wkfw_folder_id, $hid_ref_emp, "");

	if($dest_folder_id != "")
	{
		// ツリー登録
		insert_wkfwtree($con, $fname, $dest_folder_id, $wkfw_folder_id);
	}
}


// トランザクションをコミット
pg_query($con, "commit");



echo("<script language='javascript'>location.href='workflow_menu.php?session=$session';</script>");



// カテゴリ取得(wkfw_typeの最大値取得)
function get_max_wkfw_type($con, $fname)
{
	$sql = "select max(wkfw_type) from wkfwcatemst";
	$sel = select_from_table($con, $sql, "", $fname);
	if($sel == 0)
	{
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}	

	$max = pg_result($sel, 0, "max");
	return $max;
}

// カテゴリ登録
function insert_wkfwcatemst($con, $fname, $wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
{
	$sql = "insert into wkfwcatemst (wkfw_type, wkfw_nm, wkfwcate_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
	$content = array($wkfw_type, pg_escape_string($category_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
	$result = insert_into_table($con, $sql, $content, $fname);
	if ($result == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// フォルダ取得(wkfw_folder_idの最大値取得)
function get_max_wkfw_folder_id($con, $fname)
{
	$sql = "select max(wkfw_folder_id) from wkfwfolder";
	$sel = select_from_table($con, $sql, "", $fname);
	if($sel == 0)
	{
		pg_exec($con,"rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}	

	$max = pg_result($sel, 0, "max");
	return $max;
}

// フォルダ登録
function insert_wkfwfolder($con, $fname, $wkfw_folder_id, $wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
{
	$sql = "insert into wkfwfolder (wkfw_folder_id, wkfw_type, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
	$content = array($wkfw_folder_id, $wkfw_type, pg_escape_string($category_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
	$result = insert_into_table($con, $sql, $content, $fname);
	if ($result == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}

// ツリー登録
function insert_wkfwtree($con, $fname, $wkfw_parent_id, $wkfw_child_id)
{
	$sql = "insert into wkfwtree (wkfw_parent_id, wkfw_child_id) values (";
	$content = array($wkfw_parent_id, $wkfw_child_id);
	$result = insert_into_table($con, $sql, $content, $fname);
	if ($result == 0)
	{
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

pg_close($con);
?>
</body>