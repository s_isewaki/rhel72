<?php
// ファントルくんグラフ
// 使用ライブラリ：pChart 1.27d
// http://pchart.sourceforge.net/
// このプログラムは UTF-8 で書かれています。

set_include_path(get_include_path() . PATH_SEPARATOR . "./PEAR");
include("pChart/pData.class");
include("pChart/pChart.class");
require("PEAR/Cache/Lite.php");
require_once("about_comedix.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $_SERVER["PHP_SELF"];

//セッションのチェック
$session = qualify_session($_GET["session"],$fname);
if($session == "0"){
    js_login_exit();
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
    js_login_exit();
}

// フォント
if (file_exists('/usr/share/fonts/ipa-gothic/ipag.ttf')) {
    define("DRAW_FONT", "/usr/share/fonts/ipa-gothic/ipag.ttf");
}
else if (file_exists('/usr/share/fonts/ipa-pgothic/ipagp.ttf')) {
    define("DRAW_FONT", "/usr/share/fonts/ipa-pgothic/ipagp.ttf");
}
else if (file_exists('/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf')) {
    define("DRAW_FONT", "/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf");
}
else if (file_exists('/usr/share/fonts/vlgothic/VL-Gothic-Regular.ttf')) {
    define("DRAW_FONT", "/usr/share/fonts/vlgothic/VL-Gothic-Regular.ttf");
} 

//==============================
//パラメータ
//==============================
$type   = !empty($_GET["type"])  ? $_GET["type"]  : 1;
$cache  = !empty($_GET["cache"]) ? $_GET["cache"] : 0;
$hanrei = !empty($_COOKIE["_hiyari_graph_hanrei"]) ? $_COOKIE["_hiyari_graph_hanrei"] : 1;
$h_size = !empty($_COOKIE["_hiyari_graph_h_size"]) ? $_COOKIE["_hiyari_graph_h_size"] : 9;
$z_size = !empty($_COOKIE["_hiyari_graph_z_size"]) ? $_COOKIE["_hiyari_graph_z_size"] : 9;

//==============================
//専用セッションを使用する。
//==============================
session_name("hiyari_stats_sid");
session_start();

//==============================
//集計結果情報の取得
//==============================

//集計結果データ
$stats  = $_SESSION['stats_result_info']['report_stats_data'];
$counts = $stats["stats_data_list"]["cells"];
$v_sums = $stats["stats_data_list"]["vertical_sums"];
$h_sums = $stats["stats_data_list"]["horizontal_sums"];

//軸条件表示
$v_label = $_SESSION['stats_result_info']['vertical_label'];
$h_label = $_SESSION['stats_result_info']['horizontal_label'];

//非表示項目
$v_no_disp = $_SESSION['stats_result_info']['vertical_no_disp_code_list'];
$h_no_disp = $_SESSION['stats_result_info']['horizontal_no_disp_code_list'];


//==============================
// キャッシュ
//==============================
$cache_id = session_id()  . $type;
$cachelite = new Cache_Lite(array(
    'cacheDir' => '/tmp/',
    'lifeTime' => 3600,
    'automaticSerialization' => true,
    'automaticCleaningFactor' => 100,
));
// キャッシュのクリア
if (!$cache) {
    $cachelite->clean('hiyari_rm_stats_chart','ingroup');
}

//==============================
//データ
//==============================
$cache_data = $cachelite->get($cache_id, 'hiyari_rm_stats_chart');
$data  = $cache_data["data"];
$v_sum = $cache_data["v_sum"];
$v_max = $cache_data["v_max"];

//20120201
//座標軸名表示
$str_dips_xy = "横軸：".mb_convert_encoding(mb_convert_encoding($_SESSION["stats_result_info"]["horizontal_label"],"sjis-win","eucJP-win"),"UTF-8","sjis-win")."　縦軸：".mb_convert_encoding(mb_convert_encoding($_SESSION["stats_result_info"]["vertical_label"],"sjis-win","eucJP-win"),"UTF-8","sjis-win");
$str_dips_y_type2 = "表示項目：".mb_convert_encoding(mb_convert_encoding($_SESSION["stats_result_info"]["vertical_label"],"sjis-win","eucJP-win"),"UTF-8","sjis-win");


// キャッシュがなければデータ生成
if ($cache_data === false) {
    $data = new pData;

    // 横軸ラベル
    $label = array();
    foreach ($stats["horizontal_options"] as $h) {
        if (in_array($h['easy_code'], $h_no_disp)) {
            continue;
        }
        $label[] = mb_convert_encoding(mb_convert_encoding($h['easy_name'],"sjis-win","eucJP-win"),"UTF-8","sjis-win");
    }

    //データ
    $v_index = -1;
    $v_sum = 0;
    $v_max = 0;
    $pie = array();
    $pielabel = array();
    foreach ($stats["vertical_options"] as $v) {
        $v_index++;
        if (in_array($v['easy_code'],$v_no_disp)) {
            continue;
        }

        // 縦軸ラベル
        $vlabel = mb_convert_encoding(mb_convert_encoding($v['easy_name'],"sjis-win","eucJP-win"),"UTF-8","sjis-win");

        // 棒グラフ、折れ線グラフ
        if ($type != 2) {
            $data->SetSerieName($vlabel, "Serie".$v_index);

            $h_index = -1;
            $point = array();
            foreach ($stats["horizontal_options"] as $h) {
                $h_index++;
                if (in_array($h['easy_code'],$h_no_disp)) {
                    continue;
                }
                $point[] = $counts[$v_index][$h_index];

                if ($v_max < $counts[$v_index][$h_index]) {
                    $v_max = $counts[$v_index][$h_index];
                }
                if ($v_sum < $h_sums[$h_index]) {
                    $v_sum = $h_sums[$h_index];
                }
            }
            $data->AddPoint($point, "Serie".$v_index);
        }

        // 円グラフ
        else if ($v_sums[$v_index] > 0) {
            $pie[] = $v_sums[$v_index];
            $pielabel[] = $vlabel."({$v_sums[$v_index]})";
        }
    }
    if ($type != 2) {
        $data->AddPoint($label, "Label");
        $data->SetAbsciseLabelSerie("Label");
    }
    else {
        $data->AddPoint($pie, "Pie");
        $data->AddPoint($pielabel, "Label");
        $data->SetAbsciseLabelSerie("Label");
    }

    $data->AddAllSeries();
    $cachelite->save(array("data" => $data, "v_sum" => $v_sum, "v_max" => $v_max), $cache_id, 'hiyari_rm_stats_chart');
}

//==============================
//描画
//==============================
$graph = new pChart(1030,550);

// 凡例の色
$graph->setColorPalette( 0,  0,  0,255); // #0000ff
$graph->setColorPalette( 1,  0,255,  0); // #00ff00
$graph->setColorPalette( 2,255,  0,  0); // #ff0000
$graph->setColorPalette( 3,  0,255,255); // #00ffff
$graph->setColorPalette( 4,255,255,  0); // #ffff00
$graph->setColorPalette( 5,128,255,255); // #80ffff
$graph->setColorPalette( 6,255,128,255); // #ff80ff
$graph->setColorPalette( 7,255,255,128); // #ffff80
$graph->setColorPalette( 8,128,  0,  0); // #800000
$graph->setColorPalette( 9,  0,128,  0); // #008000
$graph->setColorPalette(10,  0,  0,128); // #000080
$graph->setColorPalette(11,255,128,  0); // #ff8000
$graph->setColorPalette(12,128,255,  0); // #80ff00
$graph->setColorPalette(13,128,  0,255); // #8000ff
$graph->setColorPalette(14,255,  0,128); // #ff0080
$graph->setColorPalette(15,  0,255,128); // #00ff80
$graph->setColorPalette(16,  0,128,255); // #0080ff
$graph->setColorPalette(17,255,128,128); // #ff8080
$graph->setColorPalette(18,128,255,128); // #80ff80
$graph->setColorPalette(19,128,128,255); // #8080ff
$graph->setColorPalette(20,  0,128,128); // #008080
$graph->setColorPalette(21,128,  0,128); // #800080
$graph->setColorPalette(22,128,128,  0); // #808000
$graph->setColorPalette(23,128,128,128); // #808080
$graph->setColorPalette(24, 64,  0,  0); // #400000
$graph->setColorPalette(25,  0, 64,  0); // #004000
$graph->setColorPalette(26,  0,  0, 64); // #000040
$graph->setColorPalette(27,192,  0,  0); // #c00000
$graph->setColorPalette(28,  0,192,  0); // #00c000
$graph->setColorPalette(29,  0,  0,192); // #0000c0
$graph->setColorPalette(30,  0,255, 64); // #00ff40
$graph->setColorPalette(31,255, 64,  0); // #ff4000
$graph->setColorPalette(32, 64,  0,255); // #4000ff

// 凡例を表示する
if ($hanrei == 1) {
    $garea_x = 850;
}
// 凡例を表示しない
else {
    $garea_x = 1000;
}

/*
********グラフ表示の際のグラフ縦軸の上限値と目盛り刻み数の対応表*********

件数の最大値    縦軸上限    目盛り表示単位  表示される目盛りの個数
1～10           12          2               6
11～15          16          2               8
16～20          25          5               5
21～30          35          5               7
31～40          45          5               9
41～50          60          10              6
51～60          70          10              7
61～70          80          10              8
71～80          90          10              9
81～90          100         10              10
91～100         120         20              6
101～120        140         20              7
121～140        160         20              8
141～160        180         20              9
161～180        200         20              10
181～220        250         50              5
221～280        300         50              6
281～330        350         50              7
331～380        400         50              8
381～420        450         50              9
421～470        500         50              10
471～570        600         100             6
571～660        700         100             7
661～760        800         100             8
761～850        900         100             9
851～950        1000        100             10
951～1100       1200        200             6
1101～1300      1400        200             7
1301～1500      1600        200             8
1501～1700      1800        200             9
1701～1900      2000        200             10
1901～2400      2500        500             5
2401～2900      3000        500             6
2901～3400      3500        500             7
3401～3900      4000        500             8
3901～4300      4500        500             9
4301～4800      5000        500             10
4801～5800      6000        1000            6
5801～6700      7000        1000            7
6701～7700      8000        1000            8
7701～8600      9000        1000            9
8601～9600      10000       1000            10
9601～          最大数      2000            ？？？



*/


$arr_border_line = array();
$arr_border_line = array (
"12" => "6",
"16" => "8",
"25" => "5",
"35" => "7",
"45" => "9",
"60" => "6",
"70" => "7",
"80" => "8",
"90" => "9",
"100" => "10",
"120" => "6",
"140" => "7",
"160" => "8",
"180" => "9",
"200" => "10",
"250" => "5",
"300" => "6",
"350" => "7",
"400" => "8",
"450" => "9",
"500" => "10",
"600" => "6",
"700" => "7",
"800" => "8",
"900" => "9",
"1000" => "10",
"1200" => "6",
"1400" => "7",
"1600" => "8",
"1800" => "9",
"2000" => "10",
"2500" => "5",
"3000" => "6",
"3500" => "7",
"4000" => "8",
"4500" => "9",
"5000" => "10",
"6000" => "6",
"7000" => "7",
"8000" => "8",
"9000" => "9",
"10000" => "10");



switch ($type) {
    // 積み上げ棒グラフ
    case 1:
        $graph->setGraphArea(50,10,$garea_x,450);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$z_size);
        //$graph->setFixedScale(0,$v_sum,$v_sum);



        //件数からグラフの最大値と目盛りの刻み数を取得する
        $border_key = get_border_lline($arr_border_line,$v_sum);

        //test
        //$www_v_sum = 3541;
        //$border_key = get_border_lline($arr_border_line,$www_v_sum);

        if($border_key =="OVER")
        {
            //設定の上限値を超えた場合は、件数を500で割った数値をメモリ数とする
            $set_div = ($v_sum/500);
            $border_key = $v_sum;
            //$set_div = ($www_v_sum/500);
            //$border_key = $www_v_sum;
        }
        else
        {
            //配列の設定値から目盛りの分割数を取得する
            $set_div = $arr_border_line[$border_key];
        }

        $graph->setFixedScale(0,$border_key,$set_div);


        $graph->drawScale($data->GetData(),$data->GetDataDescription(),SCALE_ADDALLSTART0,0,0,0,true,0,0,true);

        $graph->drawGrid(1,true,220,220,220,50);

        $graph->drawStackedBarGraph($data->GetData(), $data->GetDataDescription());

        if ($hanrei == 1) {
            $graph->setFontProperties(DRAW_FONT,$h_size);
            $data->removeSerie("Label");
            $graph->drawLegend(860,10,$data->GetDataDescription(),245,255,229,-1,-1,-1,0,0,0,false);
        }

        //縦軸横軸の項目名を表示する20120201
        $graph->drawTextBox(10,500,700,530,$str_dips_xy,0,0,0,0,ALIGN_LEFT,FALSE,245,255,229,100);


        break;

    // 折れ線グラフ
    case 3:
        $graph->setGraphArea(50,10,$garea_x,450);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$z_size);



        //件数からグラフの最大値と目盛りの刻み数を取得する
        $border_key = get_border_lline($arr_border_line,$v_max);

        //test
        //$www_v_sum = 2541;
        //$border_key = get_border_lline($arr_border_line,$www_v_sum);

        if($border_key =="OVER")
        {
            //設定の上限値を超えた場合は、件数を500で割った数値をメモリ数とする
            $set_div = ($v_max/500);
            $border_key = $v_max;
            //$set_div = ($www_v_sum/500);
            //$border_key = $www_v_sum;
        }
        else
        {
            //配列の設定値から目盛りの分割数を取得する
            $set_div = $arr_border_line[$border_key];
        }

        $graph->setFixedScale(0,$border_key,$set_div);
        //$graph->setFixedScale(0,$v_max,$v_max);



        $graph->drawScale($data->GetData(),$data->GetDataDescription(),SCALE_START0,0,0,0,true,0,0,false);

        $graph->drawGrid(1,true,220,220,220,50);

        $graph->drawLineGraph($data->GetData(), $data->GetDataDescription());

        if ($hanrei == 1) {
            $graph->setFontProperties(DRAW_FONT,$h_size);
            $data->removeSerie("Label");
            $graph->drawLegend(860,10,$data->GetDataDescription(),245,255,229,-1,-1,-1,0,0,0,false);
        }

        //横軸縦軸の項目名を表示する20120201
        $graph->drawTextBox(10,500,700,530,$str_dips_xy,0,0,0,0,ALIGN_LEFT,FALSE,245,255,229,100);

        break;

    // 円グラフ
    case 2:
        $graph->setGraphArea(0,0,1030,550);
        $graph->drawFilledRectangle(0,0,1030,550,245,255,229,false,100);// #F5FFE5
        $graph->drawGraphArea(245,255,229,false);// #F5FFE5

        $graph->setFontProperties(DRAW_FONT,$h_size);
	//y-yamamoto modify 20141114
        if($data->GetData() != 0){
		$graph->drawFlatPieGraph($data->GetData(), $data->GetDataDescription(),515,275,200,PIE_PERCENTAGE_LABEL,0,1);
	}else{
		$error_str="結果が0件ですので円グラフを表示できません";
		$graph->drawTextBox(10,10,70,53, $error_str,0,0,0,0,ALIGN_LEFT,FALSE,245,255,229,100);
	}
        //縦軸の項目名を表示する20120201
        $graph->drawTextBox(10,500,700,530,$str_dips_y_type2,0,0,0,0,ALIGN_LEFT,FALSE,245,255,229,100);

        break;
}

ob_clean();
$graph->Stroke();

function get_border_lline($arr_border_line,$v_sum)
{

//test
//$v_sum = 1301;

    foreach( $arr_border_line as $key => $value )
    {

        if($key > $v_sum)
        {
            return $key;
        }
    }

    //あらかじめの設定の上限値(5000)を超えた場合は件数を上限値として設定する
    return "OVER";

}


