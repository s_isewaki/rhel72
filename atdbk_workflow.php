<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 承認者設定</title>
<?
require("about_session.php");
require("about_authority.php");
require_once("atdbk_menu_common.ini");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限の取得
$work_admin_auth = check_authority($session, 42, $fname);

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

// 承認者一覧を取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id in (select emp_id from tmcdaprv) order by emp_id";
$sel_aprv = select_from_table($con, $sql, $cond, $fname);
if ($sel_aprv == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認者情報を取得
$sql = "select overtime_id, modify_id, return_id from tmcdwkfw";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_wkfw = select_from_table($con, $sql, $cond, $fname);
if ($sel_wkfw == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel_wkfw) > 0) {
	$overtime_id = pg_fetch_result($sel_wkfw, 0, "overtime_id");
	$modify_id = pg_fetch_result($sel_wkfw, 0, "modify_id");
	$return_id = pg_fetch_result($sel_wkfw, 0, "return_id");
} else {
	$overtime_id = "";
	$modify_id = "";
	$return_id = "";
}

// 承認階層情報取得
if ($back != "t") {
	// 承認者ワークフロー情報取得
	$sql = "select * from atdbk_wkfw";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$ovtm_wkfw_div = pg_fetch_result($sel, 0, "ovtm_wkfw_div");
	$ovtm_wkfw_approve_num = pg_fetch_result($sel, 0, "ovtmcfm");

	$time_wkfw_div = pg_fetch_result($sel, 0, "time_wkfw_div");
	$time_wkfw_approve_num = pg_fetch_result($sel, 0, "timecfm");

	$rtn_wkfw_div = pg_fetch_result($sel, 0, "rtn_wkfw_div");
	$rtn_wkfw_approve_num = pg_fetch_result($sel, 0, "rtncfm");


	//////////////////////////////////////////////////////////////////
	// 残業申請ワークフロー設定取得
	//////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 1 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ovtm_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $ovtm_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "ovtm_apv_div0_flg$i";
		$apv_div1_flg = "ovtm_apv_div1_flg$i";
		$apv_div4_flg = "ovtm_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");


		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "1", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "1", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "1", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "ovtm_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "ovtm_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "ovtm_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "ovtm_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "ovtm_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "ovtm_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "ovtm_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "ovtm_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "ovtm_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "ovtm_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "ovtm_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "ovtm_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "ovtm_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));
	}


	//////////////////////////////////////////////////////////////////
	// 勤務時間修正申請ワークフロー設定取得
	//////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 2 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$time_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $time_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "time_apv_div0_flg$i";
		$apv_div1_flg = "time_apv_div1_flg$i";
		$apv_div4_flg = "time_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");


		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "2", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "2", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "2", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "time_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "time_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "time_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "time_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "time_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "time_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "time_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "time_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "time_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "time_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "time_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "time_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "time_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

	}


	//////////////////////////////////////////////////////////////////
	// 退勤後復帰申請ワークフロー設定取得
	//////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 3 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rtn_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $rtn_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "rtn_apv_div0_flg$i";
		$apv_div1_flg = "rtn_apv_div1_flg$i";
		$apv_div4_flg = "rtn_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");


		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "3", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "3", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "3", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "rtn_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "rtn_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "rtn_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "rtn_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "rtn_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "rtn_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "rtn_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "rtn_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "rtn_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "rtn_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "rtn_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "rtn_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "rtn_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

	}



}

// 各申請の承認者登録可否フラグを設定
$sql = "select ovtmscr_tm, modify_flg, return_flg, over_time_apply_type from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$ovtmscr_tm = pg_fetch_result($sel, 0, "ovtmscr_tm");
	$modify_flg = pg_fetch_result($sel, 0, "modify_flg");
	$return_flg = pg_fetch_result($sel, 0, "return_flg");
	$over_time_apply_type = pg_fetch_result($sel, 0, "over_time_apply_type");
}
//残業申請の承認者の有効フラグ、分指定がある、または、事後申請
$overtime_flg = (($ovtmscr_tm > 0 && $over_time_apply_type == "1") || $over_time_apply_type == "2");
$modify_flg = ($modify_flg != "f");
$return_flg = ($return_flg == "t");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
<? if ($work_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
// メニュー表示
show_atdbk_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="atdbk_workflow_update_exe.php" method="post">
<table width="820" border="0" cellspacing="0" cellpadding="2" class="list">


<!-- 残業申請ワークフロー start -->
<tr height="22">
<?
if($ovtm_approve_num != "")
{
$ovtm_rowspan = "rowspan=\"2\"";
}
?>
<td <?=$ovtm_rowspan?> width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業申請の承認者</font></td>

<?
if($ovtm_approve_num != "")
{
?>
<td>
<table id="wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if (!$overtime_flg) {echo(" disabled");} ?>>
<!-- 承認階層数 -->
<?
$ovtm_approve = array($ovtm_approve1, $ovtm_approve2, $ovtm_approve3, $ovtm_approve4, $ovtm_approve5, $ovtm_approve6, $ovtm_approve7, $ovtm_approve8, $ovtm_approve9, $ovtm_approve10, $ovtm_approve11, $ovtm_approve12, $ovtm_approve13, $ovtm_approve14, $ovtm_approve15, $ovtm_approve16, $ovtm_approve17, $ovtm_approve18, $ovtm_approve19, $ovtm_approve20);

$ovtm_apv_div0_flg = array($ovtm_apv_div0_flg1, $ovtm_apv_div0_flg2, $ovtm_apv_div0_flg3, $ovtm_apv_div0_flg4, $ovtm_apv_div0_flg5, $ovtm_apv_div0_flg6, $ovtm_apv_div0_flg7, $ovtm_apv_div0_flg8, $ovtm_apv_div0_flg9, $ovtm_apv_div0_flg10, $ovtm_apv_div0_flg11, $ovtm_apv_div0_flg12, $ovtm_apv_div0_flg13, $ovtm_apv_div0_flg14, $ovtm_apv_div0_flg15, $ovtm_apv_div0_flg16, $ovtm_apv_div0_flg17, $ovtm_apv_div0_flg18, $ovtm_apv_div0_flg19, $ovtm_apv_div0_flg20);
$ovtm_apv_div1_flg = array($ovtm_apv_div1_flg1, $ovtm_apv_div1_flg2, $ovtm_apv_div1_flg3, $ovtm_apv_div1_flg4, $ovtm_apv_div1_flg5, $ovtm_apv_div1_flg6, $ovtm_apv_div1_flg7, $ovtm_apv_div1_flg8, $ovtm_apv_div1_flg9, $ovtm_apv_div1_flg10, $ovtm_apv_div1_flg11, $ovtm_apv_div1_flg12, $ovtm_apv_div1_flg13, $ovtm_apv_div1_flg14, $ovtm_apv_div1_flg15, $ovtm_apv_div1_flg16, $ovtm_apv_div1_flg17, $ovtm_apv_div1_flg18, $ovtm_apv_div1_flg19, $ovtm_apv_div1_flg20);
$ovtm_apv_div4_flg = array($ovtm_apv_div4_flg1, $ovtm_apv_div4_flg2, $ovtm_apv_div4_flg3, $ovtm_apv_div4_flg4, $ovtm_apv_div4_flg5, $ovtm_apv_div4_flg6, $ovtm_apv_div4_flg7, $ovtm_apv_div4_flg8, $ovtm_apv_div4_flg9, $ovtm_apv_div4_flg10, $ovtm_apv_div4_flg11, $ovtm_apv_div4_flg12, $ovtm_apv_div4_flg13, $ovtm_apv_div4_flg14, $ovtm_apv_div4_flg15, $ovtm_apv_div4_flg16, $ovtm_apv_div4_flg17, $ovtm_apv_div4_flg18, $ovtm_apv_div4_flg19, $ovtm_apv_div4_flg20);

$ovtm_target_class_div = array($ovtm_target_class_div1, $ovtm_target_class_div2, $ovtm_target_class_div3, $ovtm_target_class_div4, $ovtm_target_class_div5, $ovtm_target_class_div6, $ovtm_target_class_div7, $ovtm_target_class_div8, $ovtm_target_class_div9, $ovtm_target_class_div10, $ovtm_target_class_div11, $ovtm_target_class_div12, $ovtm_target_class_div13, $ovtm_target_class_div14, $ovtm_target_class_div15, $ovtm_target_class_div16, $ovtm_target_class_div17, $ovtm_target_class_div18, $ovtm_target_class_div19, $ovtm_target_class_div20);
$ovtm_st_id = array($ovtm_st_id1, $ovtm_st_id2, $ovtm_st_id3, $ovtm_st_id4, $ovtm_st_id5, $ovtm_st_id6, $ovtm_st_id7, $ovtm_st_id8, $ovtm_st_id9, $ovtm_st_id10, $ovtm_st_id11, $ovtm_st_id12, $ovtm_st_id13, $ovtm_st_id14, $ovtm_st_id15, $ovtm_st_id16, $ovtm_st_id17, $ovtm_st_id18, $ovtm_st_id19, $ovtm_st_id20);
$ovtm_emp_id = array($ovtm_emp_id1, $ovtm_emp_id2, $ovtm_emp_id3, $ovtm_emp_id4, $ovtm_emp_id5, $ovtm_emp_id6, $ovtm_emp_id7, $ovtm_emp_id8, $ovtm_emp_id9, $ovtm_emp_id10, $ovtm_emp_id11, $ovtm_emp_id12, $ovtm_emp_id13, $ovtm_emp_id14, $ovtm_emp_id15, $ovtm_emp_id16, $ovtm_emp_id17, $ovtm_emp_id18, $ovtm_emp_id19, $ovtm_emp_id20);
$ovtm_emp_nm = array($ovtm_emp_nm1, $ovtm_emp_nm2, $ovtm_emp_nm3, $ovtm_emp_nm4, $ovtm_emp_nm5, $ovtm_emp_nm6, $ovtm_emp_nm7, $ovtm_emp_nm8, $ovtm_emp_nm9, $ovtm_emp_nm10, $ovtm_emp_nm11, $ovtm_emp_nm12, $ovtm_emp_nm13, $ovtm_emp_nm14, $ovtm_emp_nm15, $ovtm_emp_nm16, $ovtm_emp_nm17, $ovtm_emp_nm18, $ovtm_emp_nm19, $ovtm_emp_nm20);

$ovtm_class_sect_id = array($ovtm_class_sect_id1, $ovtm_class_sect_id2, $ovtm_class_sect_id3, $ovtm_class_sect_id4, $ovtm_class_sect_id5, $ovtm_class_sect_id6, $ovtm_class_sect_id7, $ovtm_class_sect_id8, $ovtm_class_sect_id9, $ovtm_class_sect_id10, $ovtm_class_sect_id11, $ovtm_class_sect_id12, $ovtm_class_sect_id13, $ovtm_class_sect_id14, $ovtm_class_sect_id15, $ovtm_class_sect_id16, $ovtm_class_sect_id17, $ovtm_class_sect_id18, $ovtm_class_sect_id19, $ovtm_class_sect_id20);
$ovtm_atrb_sect_id = array($ovtm_atrb_sect_id1, $ovtm_atrb_sect_id2, $ovtm_atrb_sect_id3, $ovtm_atrb_sect_id4, $ovtm_atrb_sect_id5, $ovtm_atrb_sect_id6, $ovtm_atrb_sect_id7, $ovtm_atrb_sect_id8, $ovtm_atrb_sect_id9, $ovtm_atrb_sect_id10, $ovtm_atrb_sect_id11, $ovtm_atrb_sect_id12, $ovtm_atrb_sect_id13, $ovtm_atrb_sect_id14, $ovtm_atrb_sect_id15, $ovtm_atrb_sect_id16, $ovtm_atrb_sect_id17, $ovtm_atrb_sect_id18, $ovtm_atrb_sect_id19, $ovtm_atrb_sect_id20);
$ovtm_dept_sect_id = array($ovtm_dept_sect_id1, $ovtm_dept_sect_id2, $ovtm_dept_sect_id3, $ovtm_dept_sect_id4, $ovtm_dept_sect_id5, $ovtm_dept_sect_id6, $ovtm_dept_sect_id7, $ovtm_dept_sect_id8, $ovtm_dept_sect_id9, $ovtm_dept_sect_id10, $ovtm_dept_sect_id11, $ovtm_dept_sect_id12, $ovtm_dept_sect_id13, $ovtm_dept_sect_id14, $ovtm_dept_sect_id15, $ovtm_dept_sect_id16, $ovtm_dept_sect_id17, $ovtm_dept_sect_id18, $ovtm_dept_sect_id19, $ovtm_dept_sect_id20);
$ovtm_room_sect_id = array($ovtm_room_sect_id1, $ovtm_room_sect_id2, $ovtm_room_sect_id3, $ovtm_room_sect_id4, $ovtm_room_sect_id5, $ovtm_room_sect_id6, $ovtm_room_sect_id7, $ovtm_room_sect_id8, $ovtm_room_sect_id9, $ovtm_room_sect_id10, $ovtm_room_sect_id11, $ovtm_room_sect_id12, $ovtm_room_sect_id13, $ovtm_room_sect_id14, $ovtm_room_sect_id15, $ovtm_room_sect_id16, $ovtm_room_sect_id17, $ovtm_room_sect_id18, $ovtm_room_sect_id19, $ovtm_room_sect_id20);

$ovtm_st_sect_id = array($ovtm_st_sect_id1, $ovtm_st_sect_id2, $ovtm_st_sect_id3, $ovtm_st_sect_id4, $ovtm_st_sect_id5, $ovtm_st_sect_id6, $ovtm_st_sect_id7, $ovtm_st_sect_id8, $ovtm_st_sect_id9, $ovtm_st_sect_id10, $ovtm_st_sect_id11, $ovtm_st_sect_id12, $ovtm_st_sect_id13, $ovtm_st_sect_id14, $ovtm_st_sect_id15, $ovtm_st_sect_id16, $ovtm_st_sect_id17, $ovtm_st_sect_id18, $ovtm_st_sect_id19, $ovtm_st_sect_id20);

$ovtm_multi_apv_flg = array($ovtm_multi_apv_flg1, $ovtm_multi_apv_flg2, $ovtm_multi_apv_flg3, $ovtm_multi_apv_flg4, $ovtm_multi_apv_flg5, $ovtm_multi_apv_flg6, $ovtm_multi_apv_flg7, $ovtm_multi_apv_flg8, $ovtm_multi_apv_flg9, $ovtm_multi_apv_flg10, $ovtm_multi_apv_flg11, $ovtm_multi_apv_flg12, $ovtm_multi_apv_flg13, $ovtm_multi_apv_flg14, $ovtm_multi_apv_flg15, $ovtm_multi_apv_flg16, $ovtm_multi_apv_flg17, $ovtm_multi_apv_flg18, $ovtm_multi_apv_flg19, $ovtm_multi_apv_flg20);
$ovtm_next_notice_div = array($ovtm_next_notice_div1, $ovtm_next_notice_div2, $ovtm_next_notice_div3, $ovtm_next_notice_div4, $ovtm_next_notice_div5, $ovtm_next_notice_div6, $ovtm_next_notice_div7, $ovtm_next_notice_div8, $ovtm_next_notice_div9, $ovtm_next_notice_div10, $ovtm_next_notice_div11, $ovtm_next_notice_div12, $ovtm_next_notice_div13, $ovtm_next_notice_div14, $ovtm_next_notice_div15, $ovtm_next_notice_div16, $ovtm_next_notice_div17, $ovtm_next_notice_div18, $ovtm_next_notice_div19, $ovtm_next_notice_div20);

$ovtm_apv_num = array($ovtm_apv_num1, $ovtm_apv_num2, $ovtm_apv_num3, $ovtm_apv_num4, $ovtm_apv_num5, $ovtm_apv_num6, $ovtm_apv_num7, $ovtm_apv_num8, $ovtm_apv_num9, $ovtm_apv_num10, $ovtm_apv_num11, $ovtm_apv_num12, $ovtm_apv_num13, $ovtm_apv_num14, $ovtm_apv_num15, $ovtm_apv_num16, $ovtm_apv_num17, $ovtm_apv_num18, $ovtm_apv_num19, $ovtm_apv_num20);


for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;
	if(!$overtime_flg){
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	else{
		$fontcolor = "";
		$approve_content_color = "#0000ff";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><font id=\"ovtm_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"ovtm_approve_content". $j ."\"></span></font>");

	$ovtm_target_class_div[$i] = ($ovtm_target_class_div[$i] == "") ? "1" : $ovtm_target_class_div[$i];
	$ovtm_multi_apv_flg[$i] = ($ovtm_multi_apv_flg[$i] == "") ? "f" : $ovtm_multi_apv_flg[$i];
	$ovtm_next_notice_div[$i] = ($ovtm_next_notice_div[$i] == "") ? "2" : $ovtm_next_notice_div[$i];
	$ovtm_apv_num[$i] = ($ovtm_apv_num[$i] == "") ? "1" : $ovtm_apv_num[$i];

	echo("<input type=\"hidden\" name=\"ovtm_approve" . $j . "\" value=\"" . $ovtm_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_apv_div0_flg" . $j . "\" value=\"" . $ovtm_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_apv_div1_flg" . $j . "\" value=\"" . $ovtm_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_apv_div4_flg" . $j . "\" value=\"" . $ovtm_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_target_class_div" . $j . "\" value=\"" . $ovtm_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_st_id" . $j . "\" value=\"" . $ovtm_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"" . $ovtm_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"" . $ovtm_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_class_sect_id" . $j . "\" value=\"" . $ovtm_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_atrb_sect_id" . $j . "\" value=\"" . $ovtm_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_dept_sect_id" . $j . "\" value=\"" . $ovtm_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_room_sect_id" . $j . "\" value=\"" . $ovtm_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_st_sect_id" . $j . "\" value=\"" . $ovtm_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_multi_apv_flg" . $j . "\" value=\"" . $ovtm_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"" . $ovtm_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_apv_num" . $j . "\" value=\"" . $ovtm_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>

<!-- 残業申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="ovtm_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if(!$overtime_flg){echo("color = \"#a9a9a9\"");} ?>>残業申請の確定</font></td>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if(!$overtime_flg){echo("color = \"#a9a9a9\"");}else{echo("color = \"#0000ff\"");} ?>><?= $ovtm_wkfw_approve_num ?></font>
</td>
</tr>
</table>
</td>
</tr>


<tr>
<?
}
?>

<td>
<select name="overtime_id"<? if(!$overtime_flg) {echo(" disabled");} ?>>
<option value="">
<?
while ($row = pg_fetch_array($sel_aprv)) {
	$tmp_emp_id = $row["emp_id"];
	$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

	echo("<option value=\"$tmp_emp_id\"");
	if ($tmp_emp_id == $overtime_id) {echo(" selected");}
	echo(">$tmp_emp_nm\n");
}
?>
</select>

<?
if(!$overtime_flg)
{
?>
<input type="hidden" name="overtime_id" value="<?=$overtime_id?>">
<?
}
?>

</td>
</tr>
<!-- 残業申請ワークフロー end -->


<!-- 勤務時間修正申請ワークフロー start -->
<tr height="22">
<?
if($time_approve_num != "")
{
$time_rowspan = "rowspan=\"2\"";
}
?>
<td <?=$time_rowspan?> align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間修正申請の承認者</font></td>

<?
if($time_approve_num != "")
{
?>
<td>
<table id="wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if ($modify_flg) {echo(" disabled");} ?>>
<!-- 承認階層数 -->
<?
$time_approve = array($time_approve1, $time_approve2, $time_approve3, $time_approve4, $time_approve5, $time_approve6, $time_approve7, $time_approve8, $time_approve9, $time_approve10, $time_approve11, $time_approve12, $time_approve13, $time_approve14, $time_approve15, $time_approve16, $time_approve17, $time_approve18, $time_approve19, $time_approve20);

$time_apv_div0_flg = array($time_apv_div0_flg1, $time_apv_div0_flg2, $time_apv_div0_flg3, $time_apv_div0_flg4, $time_apv_div0_flg5, $time_apv_div0_flg6, $time_apv_div0_flg7, $time_apv_div0_flg8, $time_apv_div0_flg9, $time_apv_div0_flg10, $time_apv_div0_flg11, $time_apv_div0_flg12, $time_apv_div0_flg13, $time_apv_div0_flg14, $time_apv_div0_flg15, $time_apv_div0_flg16, $time_apv_div0_flg17, $time_apv_div0_flg18, $time_apv_div0_flg19, $time_apv_div0_flg20);
$time_apv_div1_flg = array($time_apv_div1_flg1, $time_apv_div1_flg2, $time_apv_div1_flg3, $time_apv_div1_flg4, $time_apv_div1_flg5, $time_apv_div1_flg6, $time_apv_div1_flg7, $time_apv_div1_flg8, $time_apv_div1_flg9, $time_apv_div1_flg10, $time_apv_div1_flg11, $time_apv_div1_flg12, $time_apv_div1_flg13, $time_apv_div1_flg14, $time_apv_div1_flg15, $time_apv_div1_flg16, $time_apv_div1_flg17, $time_apv_div1_flg18, $time_apv_div1_flg19, $time_apv_div1_flg20);
$time_apv_div4_flg = array($time_apv_div4_flg1, $time_apv_div4_flg2, $time_apv_div4_flg3, $time_apv_div4_flg4, $time_apv_div4_flg5, $time_apv_div4_flg6, $time_apv_div4_flg7, $time_apv_div4_flg8, $time_apv_div4_flg9, $time_apv_div4_flg10, $time_apv_div4_flg11, $time_apv_div4_flg12, $time_apv_div4_flg13, $time_apv_div4_flg14, $time_apv_div4_flg15, $time_apv_div4_flg16, $time_apv_div4_flg17, $time_apv_div4_flg18, $time_apv_div4_flg19, $time_apv_div4_flg20);

$time_target_class_div = array($time_target_class_div1, $time_target_class_div2, $time_target_class_div3, $time_target_class_div4, $time_target_class_div5, $time_target_class_div6, $time_target_class_div7, $time_target_class_div8, $time_target_class_div9, $time_target_class_div10, $time_target_class_div11, $time_target_class_div12, $time_target_class_div13, $time_target_class_div14, $time_target_class_div15, $time_target_class_div16, $time_target_class_div17, $time_target_class_div18, $time_target_class_div19, $time_target_class_div20);
$time_st_id = array($time_st_id1, $time_st_id2, $time_st_id3, $time_st_id4, $time_st_id5, $time_st_id6, $time_st_id7, $time_st_id8, $time_st_id9, $time_st_id10, $time_st_id11, $time_st_id12, $time_st_id13, $time_st_id14, $time_st_id15, $time_st_id16, $time_st_id17, $time_st_id18, $time_st_id19, $time_st_id20);
$time_emp_id = array($time_emp_id1, $time_emp_id2, $time_emp_id3, $time_emp_id4, $time_emp_id5, $time_emp_id6, $time_emp_id7, $time_emp_id8, $time_emp_id9, $time_emp_id10, $time_emp_id11, $time_emp_id12, $time_emp_id13, $time_emp_id14, $time_emp_id15, $time_emp_id16, $time_emp_id17, $time_emp_id18, $time_emp_id19, $time_emp_id20);
$time_emp_nm = array($time_emp_nm1, $time_emp_nm2, $time_emp_nm3, $time_emp_nm4, $time_emp_nm5, $time_emp_nm6, $time_emp_nm7, $time_emp_nm8, $time_emp_nm9, $time_emp_nm10, $time_emp_nm11, $time_emp_nm12, $time_emp_nm13, $time_emp_nm14, $time_emp_nm15, $time_emp_nm16, $time_emp_nm17, $time_emp_nm18, $time_emp_nm19, $time_emp_nm20);

$time_class_sect_id = array($time_class_sect_id1, $time_class_sect_id2, $time_class_sect_id3, $time_class_sect_id4, $time_class_sect_id5, $time_class_sect_id6, $time_class_sect_id7, $time_class_sect_id8, $time_class_sect_id9, $time_class_sect_id10, $time_class_sect_id11, $time_class_sect_id12, $time_class_sect_id13, $time_class_sect_id14, $time_class_sect_id15, $time_class_sect_id16, $time_class_sect_id17, $time_class_sect_id18, $time_class_sect_id19, $time_class_sect_id20);
$time_atrb_sect_id = array($time_atrb_sect_id1, $time_atrb_sect_id2, $time_atrb_sect_id3, $time_atrb_sect_id4, $time_atrb_sect_id5, $time_atrb_sect_id6, $time_atrb_sect_id7, $time_atrb_sect_id8, $time_atrb_sect_id9, $time_atrb_sect_id10, $time_atrb_sect_id11, $time_atrb_sect_id12, $time_atrb_sect_id13, $time_atrb_sect_id14, $time_atrb_sect_id15, $time_atrb_sect_id16, $time_atrb_sect_id17, $time_atrb_sect_id18, $time_atrb_sect_id19, $time_atrb_sect_id20);
$time_dept_sect_id = array($time_dept_sect_id1, $time_dept_sect_id2, $time_dept_sect_id3, $time_dept_sect_id4, $time_dept_sect_id5, $time_dept_sect_id6, $time_dept_sect_id7, $time_dept_sect_id8, $time_dept_sect_id9, $time_dept_sect_id10, $time_dept_sect_id11, $time_dept_sect_id12, $time_dept_sect_id13, $time_dept_sect_id14, $time_dept_sect_id15, $time_dept_sect_id16, $time_dept_sect_id17, $time_dept_sect_id18, $time_dept_sect_id19, $time_dept_sect_id20);
$time_room_sect_id = array($time_room_sect_id1, $time_room_sect_id2, $time_room_sect_id3, $time_room_sect_id4, $time_room_sect_id5, $time_room_sect_id6, $time_room_sect_id7, $time_room_sect_id8, $time_room_sect_id9, $time_room_sect_id10, $time_room_sect_id11, $time_room_sect_id12, $time_room_sect_id13, $time_room_sect_id14, $time_room_sect_id15, $time_room_sect_id16, $time_room_sect_id17, $time_room_sect_id18, $time_room_sect_id19, $time_room_sect_id20);

$time_st_sect_id = array($time_st_sect_id1, $time_st_sect_id2, $time_st_sect_id3, $time_st_sect_id4, $time_st_sect_id5, $time_st_sect_id6, $time_st_sect_id7, $time_st_sect_id8, $time_st_sect_id9, $time_st_sect_id10, $time_st_sect_id11, $time_st_sect_id12, $time_st_sect_id13, $time_st_sect_id14, $time_st_sect_id15, $time_st_sect_id16, $time_st_sect_id17, $time_st_sect_id18, $time_st_sect_id19, $time_st_sect_id20);

$time_multi_apv_flg = array($time_multi_apv_flg1, $time_multi_apv_flg2, $time_multi_apv_flg3, $time_multi_apv_flg4, $time_multi_apv_flg5, $time_multi_apv_flg6, $time_multi_apv_flg7, $time_multi_apv_flg8, $time_multi_apv_flg9, $time_multi_apv_flg10, $time_multi_apv_flg11, $time_multi_apv_flg12, $time_multi_apv_flg13, $time_multi_apv_flg14, $time_multi_apv_flg15, $time_multi_apv_flg16, $time_multi_apv_flg17, $time_multi_apv_flg18, $time_multi_apv_flg19, $time_multi_apv_flg20);
$time_next_notice_div = array($time_next_notice_div1, $time_next_notice_div2, $time_next_notice_div3, $time_next_notice_div4, $time_next_notice_div5, $time_next_notice_div6, $time_next_notice_div7, $time_next_notice_div8, $time_next_notice_div9, $time_next_notice_div10, $time_next_notice_div11, $time_next_notice_div12, $time_next_notice_div13, $time_next_notice_div14, $time_next_notice_div15, $time_next_notice_div16, $time_next_notice_div17, $time_next_notice_div18, $time_next_notice_div19, $time_next_notice_div20);

$time_apv_num = array($time_apv_num1, $time_apv_num2, $time_apv_num3, $time_apv_num4, $time_apv_num5, $time_apv_num6, $time_apv_num7, $time_apv_num8, $time_apv_num9, $time_apv_num10, $time_apv_num11, $time_apv_num12, $time_apv_num13, $time_apv_num14, $time_apv_num15, $time_apv_num16, $time_apv_num17, $time_apv_num18, $time_apv_num19, $time_apv_num20);


for ($i = 0; $i < $time_approve_num; $i++) {
	$j = $i + 1;
	if($modify_flg){
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	else{
		$fontcolor = "";
		$approve_content_color = "#0000ff";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><font id=\"time_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"time_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"time_approve_content". $j ."\"></span></font>");

	$time_target_class_div[$i] = ($time_target_class_div[$i] == "") ? "1" : $time_target_class_div[$i];
	$time_multi_apv_flg[$i] = ($time_multi_apv_flg[$i] == "") ? "f" : $time_multi_apv_flg[$i];
	$time_next_notice_div[$i] = ($time_next_notice_div[$i] == "") ? "2" : $time_next_notice_div[$i];
	$time_apv_num[$i] = ($time_apv_num[$i] == "") ? "1" : $time_apv_num[$i];

	echo("<input type=\"hidden\" name=\"time_approve" . $j . "\" value=\"" . $time_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_apv_div0_flg" . $j . "\" value=\"" . $time_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_apv_div1_flg" . $j . "\" value=\"" . $time_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_apv_div4_flg" . $j . "\" value=\"" . $time_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_target_class_div" . $j . "\" value=\"" . $time_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_st_id" . $j . "\" value=\"" . $time_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_emp_id" . $j . "\" value=\"" . $time_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_emp_nm" . $j . "\" value=\"" . $time_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_class_sect_id" . $j . "\" value=\"" . $time_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_atrb_sect_id" . $j . "\" value=\"" . $time_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_dept_sect_id" . $j . "\" value=\"" . $time_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_room_sect_id" . $j . "\" value=\"" . $time_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_st_sect_id" . $j . "\" value=\"" . $time_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_multi_apv_flg" . $j . "\" value=\"" . $time_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_next_notice_div" . $j . "\" value=\"" . $time_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_apv_num" . $j . "\" value=\"" . $time_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>

<!-- 勤務時間修正申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="time_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($modify_flg){echo("color = \"#a9a9a9\"");} ?>>勤務時間修正申請の確定</font></td>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($modify_flg){echo("color = \"#a9a9a9\"");}else{echo("color = \"#0000ff\"");} ?>><?= $time_wkfw_approve_num ?></font>
</td>
</tr>
</table>
</td>
</tr>


<tr>
<?
}
?>

<td>
<select name="modify_id"<? if ($modify_flg) {echo(" disabled");} ?>>
<option value="">
<?
pg_result_seek($sel_aprv, 0);
while ($row = pg_fetch_array($sel_aprv)) {
	$tmp_emp_id = $row["emp_id"];
	$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

	echo("<option value=\"$tmp_emp_id\"");
	if ($tmp_emp_id == $modify_id) {echo(" selected");}
	echo(">$tmp_emp_nm\n");
}
?>

<?
if($modify_flg)
{
?>
<input type="hidden" name="modify_id" value="<?=$modify_id?>">
<?
}
?>
</select>
</td>
</tr>
<!-- 勤務時間修正申請ワークフロー end -->



<!-- 退勤後復帰申請ワークフロー start -->
<tr height="22">
<?
if($rtn_wkfw_approve_num != "")
{
	$time_rowspan = "rowspan=\"2\"";
}
?>
<td <?=$time_rowspan?> align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?>申請の承認者</font></td>

<? /* if($rtn_wkfw_div != "1"){ */ //常に表示するように変更 位置を下へ移動 20100928 ?>


<? /*}else*/ if($rtn_wkfw_div == "1"){ ?>
<td>

<table id="wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if (!$return_flg) {echo(" disabled");} ?>>
<!-- 承認階層数 -->
<?
$rtn_approve = array($rtn_approve1, $rtn_approve2, $rtn_approve3, $rtn_approve4, $rtn_approve5, $rtn_approve6, $rtn_approve7, $rtn_approve8, $rtn_approve9, $rtn_approve10, $rtn_approve11, $rtn_approve12, $rtn_approve13, $rtn_approve14, $rtn_approve15, $rtn_approve16, $rtn_approve17, $rtn_approve18, $rtn_approve19, $rtn_approve20);

$rtn_apv_div0_flg = array($rtn_apv_div0_flg1, $rtn_apv_div0_flg2, $rtn_apv_div0_flg3, $rtn_apv_div0_flg4, $rtn_apv_div0_flg5, $rtn_apv_div0_flg6, $rtn_apv_div0_flg7, $rtn_apv_div0_flg8, $rtn_apv_div0_flg9, $rtn_apv_div0_flg10, $rtn_apv_div0_flg11, $rtn_apv_div0_flg12, $rtn_apv_div0_flg13, $rtn_apv_div0_flg14, $rtn_apv_div0_flg15, $rtn_apv_div0_flg16, $rtn_apv_div0_flg17, $rtn_apv_div0_flg18, $rtn_apv_div0_flg19, $rtn_apv_div0_flg20);
$rtn_apv_div1_flg = array($rtn_apv_div1_flg1, $rtn_apv_div1_flg2, $rtn_apv_div1_flg3, $rtn_apv_div1_flg4, $rtn_apv_div1_flg5, $rtn_apv_div1_flg6, $rtn_apv_div1_flg7, $rtn_apv_div1_flg8, $rtn_apv_div1_flg9, $rtn_apv_div1_flg10, $rtn_apv_div1_flg11, $rtn_apv_div1_flg12, $rtn_apv_div1_flg13, $rtn_apv_div1_flg14, $rtn_apv_div1_flg15, $rtn_apv_div1_flg16, $rtn_apv_div1_flg17, $rtn_apv_div1_flg18, $rtn_apv_div1_flg19, $rtn_apv_div1_flg20);
$rtn_apv_div4_flg = array($rtn_apv_div4_flg1, $rtn_apv_div4_flg2, $rtn_apv_div4_flg3, $rtn_apv_div4_flg4, $rtn_apv_div4_flg5, $rtn_apv_div4_flg6, $rtn_apv_div4_flg7, $rtn_apv_div4_flg8, $rtn_apv_div4_flg9, $rtn_apv_div4_flg10, $rtn_apv_div4_flg11, $rtn_apv_div4_flg12, $rtn_apv_div4_flg13, $rtn_apv_div4_flg14, $rtn_apv_div4_flg15, $rtn_apv_div4_flg16, $rtn_apv_div4_flg17, $rtn_apv_div4_flg18, $rtn_apv_div4_flg19, $rtn_apv_div4_flg20);

$rtn_target_class_div = array($rtn_target_class_div1, $rtn_target_class_div2, $rtn_target_class_div3, $rtn_target_class_div4, $rtn_target_class_div5, $rtn_target_class_div6, $rtn_target_class_div7, $rtn_target_class_div8, $rtn_target_class_div9, $rtn_target_class_div10, $rtn_target_class_div11, $rtn_target_class_div12, $rtn_target_class_div13, $rtn_target_class_div14, $rtn_target_class_div15, $rtn_target_class_div16, $rtn_target_class_div17, $rtn_target_class_div18, $rtn_target_class_div19, $rtn_target_class_div20);
$rtn_st_id = array($rtn_st_id1, $rtn_st_id2, $rtn_st_id3, $rtn_st_id4, $rtn_st_id5, $rtn_st_id6, $rtn_st_id7, $rtn_st_id8, $rtn_st_id9, $rtn_st_id10, $rtn_st_id11, $rtn_st_id12, $rtn_st_id13, $rtn_st_id14, $rtn_st_id15, $rtn_st_id16, $rtn_st_id17, $rtn_st_id18, $rtn_st_id19, $rtn_st_id20);
$rtn_emp_id = array($rtn_emp_id1, $rtn_emp_id2, $rtn_emp_id3, $rtn_emp_id4, $rtn_emp_id5, $rtn_emp_id6, $rtn_emp_id7, $rtn_emp_id8, $rtn_emp_id9, $rtn_emp_id10, $rtn_emp_id11, $rtn_emp_id12, $rtn_emp_id13, $rtn_emp_id14, $rtn_emp_id15, $rtn_emp_id16, $rtn_emp_id17, $rtn_emp_id18, $rtn_emp_id19, $rtn_emp_id20);
$rtn_emp_nm = array($rtn_emp_nm1, $rtn_emp_nm2, $rtn_emp_nm3, $rtn_emp_nm4, $rtn_emp_nm5, $rtn_emp_nm6, $rtn_emp_nm7, $rtn_emp_nm8, $rtn_emp_nm9, $rtn_emp_nm10, $rtn_emp_nm11, $rtn_emp_nm12, $rtn_emp_nm13, $rtn_emp_nm14, $rtn_emp_nm15, $rtn_emp_nm16, $rtn_emp_nm17, $rtn_emp_nm18, $rtn_emp_nm19, $rtn_emp_nm20);

$rtn_class_sect_id = array($rtn_class_sect_id1, $rtn_class_sect_id2, $rtn_class_sect_id3, $rtn_class_sect_id4, $rtn_class_sect_id5, $rtn_class_sect_id6, $rtn_class_sect_id7, $rtn_class_sect_id8, $rtn_class_sect_id9, $rtn_class_sect_id10, $rtn_class_sect_id11, $rtn_class_sect_id12, $rtn_class_sect_id13, $rtn_class_sect_id14, $rtn_class_sect_id15, $rtn_class_sect_id16, $rtn_class_sect_id17, $rtn_class_sect_id18, $rtn_class_sect_id19, $rtn_class_sect_id20);
$rtn_atrb_sect_id = array($rtn_atrb_sect_id1, $rtn_atrb_sect_id2, $rtn_atrb_sect_id3, $rtn_atrb_sect_id4, $rtn_atrb_sect_id5, $rtn_atrb_sect_id6, $rtn_atrb_sect_id7, $rtn_atrb_sect_id8, $rtn_atrb_sect_id9, $rtn_atrb_sect_id10, $rtn_atrb_sect_id11, $rtn_atrb_sect_id12, $rtn_atrb_sect_id13, $rtn_atrb_sect_id14, $rtn_atrb_sect_id15, $rtn_atrb_sect_id16, $rtn_atrb_sect_id17, $rtn_atrb_sect_id18, $rtn_atrb_sect_id19, $rtn_atrb_sect_id20);
$rtn_dept_sect_id = array($rtn_dept_sect_id1, $rtn_dept_sect_id2, $rtn_dept_sect_id3, $rtn_dept_sect_id4, $rtn_dept_sect_id5, $rtn_dept_sect_id6, $rtn_dept_sect_id7, $rtn_dept_sect_id8, $rtn_dept_sect_id9, $rtn_dept_sect_id10, $rtn_dept_sect_id11, $rtn_dept_sect_id12, $rtn_dept_sect_id13, $rtn_dept_sect_id14, $rtn_dept_sect_id15, $rtn_dept_sect_id16, $rtn_dept_sect_id17, $rtn_dept_sect_id18, $rtn_dept_sect_id19, $rtn_dept_sect_id20);
$rtn_room_sect_id = array($rtn_room_sect_id1, $rtn_room_sect_id2, $rtn_room_sect_id3, $rtn_room_sect_id4, $rtn_room_sect_id5, $rtn_room_sect_id6, $rtn_room_sect_id7, $rtn_room_sect_id8, $rtn_room_sect_id9, $rtn_room_sect_id10, $rtn_room_sect_id11, $rtn_room_sect_id12, $rtn_room_sect_id13, $rtn_room_sect_id14, $rtn_room_sect_id15, $rtn_room_sect_id16, $rtn_room_sect_id17, $rtn_room_sect_id18, $rtn_room_sect_id19, $rtn_room_sect_id20);

$rtn_st_sect_id = array($rtn_st_sect_id1, $rtn_st_sect_id2, $rtn_st_sect_id3, $rtn_st_sect_id4, $rtn_st_sect_id5, $rtn_st_sect_id6, $rtn_st_sect_id7, $rtn_st_sect_id8, $rtn_st_sect_id9, $rtn_st_sect_id10, $rtn_st_sect_id11, $rtn_st_sect_id12, $rtn_st_sect_id13, $rtn_st_sect_id14, $rtn_st_sect_id15, $rtn_st_sect_id16, $rtn_st_sect_id17, $rtn_st_sect_id18, $rtn_st_sect_id19, $rtn_st_sect_id20);

$rtn_multi_apv_flg = array($rtn_multi_apv_flg1, $rtn_multi_apv_flg2, $rtn_multi_apv_flg3, $rtn_multi_apv_flg4, $rtn_multi_apv_flg5, $rtn_multi_apv_flg6, $rtn_multi_apv_flg7, $rtn_multi_apv_flg8, $rtn_multi_apv_flg9, $rtn_multi_apv_flg10, $rtn_multi_apv_flg11, $rtn_multi_apv_flg12, $rtn_multi_apv_flg13, $rtn_multi_apv_flg14, $rtn_multi_apv_flg15, $rtn_multi_apv_flg16, $rtn_multi_apv_flg17, $rtn_multi_apv_flg18, $rtn_multi_apv_flg19, $rtn_multi_apv_flg20);
$rtn_next_notice_div = array($rtn_next_notice_div1, $rtn_next_notice_div2, $rtn_next_notice_div3, $rtn_next_notice_div4, $rtn_next_notice_div5, $rtn_next_notice_div6, $rtn_next_notice_div7, $rtn_next_notice_div8, $rtn_next_notice_div9, $rtn_next_notice_div10, $rtn_next_notice_div11, $rtn_next_notice_div12, $rtn_next_notice_div13, $rtn_next_notice_div14, $rtn_next_notice_div15, $rtn_next_notice_div16, $rtn_next_notice_div17, $rtn_next_notice_div18, $rtn_next_notice_div19, $rtn_next_notice_div20);

$rtn_apv_num = array($rtn_apv_num1, $rtn_apv_num2, $rtn_apv_num3, $rtn_apv_num4, $rtn_apv_num5, $rtn_apv_num6, $rtn_apv_num7, $rtn_apv_num8, $rtn_apv_num9, $rtn_apv_num10, $rtn_apv_num11, $rtn_apv_num12, $rtn_apv_num13, $rtn_apv_num14, $rtn_apv_num15, $rtn_apv_num16, $rtn_apv_num17, $rtn_apv_num18, $rtn_apv_num19, $rtn_apv_num20);


for ($i = 0; $i < $rtn_approve_num; $i++) {
	$j = $i + 1;
	if(!$return_flg){
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	else{
		$fontcolor = "";
		$approve_content_color = "#0000ff";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><font id=\"rtn_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"rtn_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"rtn_approve_content". $j ."\"></span></font>");

	$rtn_target_class_div[$i] = ($rtn_target_class_div[$i] == "") ? "1" : $rtn_target_class_div[$i];
	$rtn_multi_apv_flg[$i] = ($rtn_multi_apv_flg[$i] == "") ? "f" : $rtn_multi_apv_flg[$i];
	$rtn_next_notice_div[$i] = ($rtn_next_notice_div[$i] == "") ? "2" : $rtn_next_notice_div[$i];
	$rtn_apv_num[$i] = ($rtn_apv_num[$i] == "") ? "1" : $rtn_apv_num[$i];

	echo("<input type=\"hidden\" name=\"rtn_approve" . $j . "\" value=\"" . $rtn_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_apv_div0_flg" . $j . "\" value=\"" . $rtn_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_apv_div1_flg" . $j . "\" value=\"" . $rtn_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_apv_div4_flg" . $j . "\" value=\"" . $rtn_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_target_class_div" . $j . "\" value=\"" . $rtn_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_st_id" . $j . "\" value=\"" . $rtn_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_emp_id" . $j . "\" value=\"" . $rtn_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_emp_nm" . $j . "\" value=\"" . $rtn_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_class_sect_id" . $j . "\" value=\"" . $rtn_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_atrb_sect_id" . $j . "\" value=\"" . $rtn_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_dept_sect_id" . $j . "\" value=\"" . $rtn_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_room_sect_id" . $j . "\" value=\"" . $rtn_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_st_sect_id" . $j . "\" value=\"" . $rtn_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_multi_apv_flg" . $j . "\" value=\"" . $rtn_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_next_notice_div" . $j . "\" value=\"" . $rtn_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_apv_num" . $j . "\" value=\"" . $rtn_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>

<!-- 退勤後復帰申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="time_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if(!$return_flg){echo("color = \"#a9a9a9\"");} ?>><?=$ret_str?>申請の確定</font></td>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if(!$return_flg){echo("color = \"#a9a9a9\"");}else{echo("color = \"#0000ff\"");} ?>><?= $rtn_wkfw_approve_num ?></font>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<? } ?>
<td>
<? /* if($rtn_wkfw_div != "1"){ */ //常に表示するように変更20100928 ?>

<select name="return_id"<? if(!$return_flg) {echo(" disabled");} ?>>
<option value="">
<?
pg_result_seek($sel_aprv, 0);
while ($row = pg_fetch_array($sel_aprv)) {
	$tmp_emp_id = $row["emp_id"];
	$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];

	echo("<option value=\"$tmp_emp_id\"");
	if ($tmp_emp_id == $return_id) {echo(" selected");}
	echo(">$tmp_emp_nm\n");
}

?>
</select>

<?
if(!$return_flg)
{
?>
<input type="hidden" name="return_id" value="<?=$return_id?>">
	<?
}
?>
</td>
</tr>
<!-- 退勤後復帰申請ワークフロー start -->


</table>
<table width="820" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>

<script type="text/javascript">
// 承認階層
<?
if($ovtm_wkfw_div == "1")
{
for ($i = 0; $i < $ovtm_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('ovtm_approve_content<?=$j?>').innerHTML = '<?=$ovtm_approve[$i]?>';
<?
}
}
?>


<?
if($time_wkfw_div == "1")
{
for ($i = 0; $i < $time_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('time_approve_content<?=$j?>').innerHTML = '<?=$time_approve[$i]?>';
<?
}
}
?>

<?
if($rtn_wkfw_div == "1")
{
for ($i = 0; $i < $rtn_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('rtn_approve_content<?=$j?>').innerHTML = '<?=$rtn_approve[$i]?>';
<?
}
}
?>



</script>

</body>
<? pg_close($con); ?>
</html>
<?
// ワークロー・承認者詳細情報取得
function search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, $wkfw_div, &$ret_arr) {

	$sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg ";
	$sql .= "from atdbk_apvdtl a ";
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "inner join authmst c on a.emp_id = c.emp_id ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order order by a.apv_sub_order asc";
    $cond = "";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$idx = 0;
	while($row = pg_fetch_array($sel))
	{
		if($idx > 0)
		{
			$emp_id .= ",";
			$approve .= ", ";
			$emp_del_flg .= ",";
		}
		$emp_id .= $row["emp_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$emp_del_flg .= $row["emp_del_flg"];

		$emp_full_nm = $emp_lt_nm ." ".$emp_ft_nm;
		$approve .= $emp_full_nm;
		$idx++;
	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => $emp_id,
							    "emp_nm" => $approve,
							    "emp_del_flg" => $emp_del_flg,
   							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// ワークフロー・部署役職情報取得
function search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, $wkfw_div, &$ret_arr) {
	global $_label_by_profile;
	global $profile_type;

	$sql  = "select ";
	$sql .= "a.target_class_div, ";
	$sql .= "case a.target_class_div ";
	$sql .= "when '1' then c.class_nm ";
	$sql .= "when '2' then c.atrb_nm ";
	$sql .= "when '3' then c.dept_nm ";
	$sql .= "when '4' then c.room_nm ";
	$sql .= "else '' ";
	$sql .= "end as target_class_name, ";
	$sql .= "b.st_id,";
	$sql .= "b.st_nm ";
	$sql .= "from atdbk_apvmng a left outer join ";
	$sql .= "(select a.apv_order, a.st_id, b.st_nm ";

	$sql .= "from atdbk_apvpstdtl a inner join stmst b on a.st_id = b.st_id where wkfw_div = $wkfw_div and st_div = 0) b on ";
	$sql .= "a.apv_order = b.apv_order, ";
	$sql .= "(select * from classname) c ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order ";

    $cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);

	$approve = "";
	$target_class_div = "";
	$st_id = "";
	$st_nm = "";

	for ($i=0; $i<$num; $i++) {

		if($i == 0) {
		  	$target_class_div = pg_fetch_result($sel, $i, "target_class_div");
		  	$target_class_name = pg_fetch_result($sel, $i, "target_class_name");
     		$st_id = pg_fetch_result($sel, $i, "st_id");
			$st_nm = pg_fetch_result($sel, $i, "st_nm");

			if($target_class_div != 0) {
				$approve .= "申請者の所属する【";
    	 		$approve .=	$target_class_name;
     			$approve .=	"】の";
			} else {
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$approve .=	"【".$in_hospital2_title."】の";
				}
			}

			$approve .=	$st_nm;

		} else {
			$st_id .=",";
     		$st_id .= pg_fetch_result($sel, $i, "st_id");

			$st_nm = pg_fetch_result($sel, $i, "st_nm");
			$approve .=	", ";
			$approve .=	$st_nm;
		}

	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => $target_class_div,
								"st_id" => $st_id,
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 部署役職(部署所属)取得
function search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, $wkfw_div, &$ret_arr)
{

	// 部署取得
	$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
	$sql .= "from atdbk_apvsectdtl a ";

	$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
	$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
	$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
	$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order ";

	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class_sect_id = pg_fetch_result($sel, 0, "class_id");
	$atrb_sect_id = pg_fetch_result($sel, 0, "atrb_id");
	$dept_sect_id = pg_fetch_result($sel, 0, "dept_id");
	$room_sect_id = pg_fetch_result($sel, 0, "room_id");

	$class_nm = pg_fetch_result($sel, 0, "class_nm");
	$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
	$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
	$room_nm = pg_fetch_result($sel, 0, "room_nm");

	if($class_nm != "")
	{
		$class_sect_nm .= $class_nm;

		if($atrb_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $atrb_nm;
		}

		if($dept_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $dept_nm;
		}

		if($room_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $room_nm;
		}
	}

	// 役職
	if($class_sect_nm != "")
	{
		$sql  = "select a.*, b.st_nm ";
		$sql .= "from atdbk_apvpstdtl a ";
		$sql .= "left join stmst b on a.st_id = b.st_id and not st_del_flg ";
		$sql .= "where a.apv_order = $apv_order ";
		$sql .= "and a.st_div = 1 and a.wkfw_div = $wkfw_div";

		$sel  = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$all_st_sect_id = "";
		$all_st_sect_nm = "";
		while($row = pg_fetch_array($sel))
		{
			if($all_st_sect_id != "")
			{
				$all_st_sect_id .= ",";
				$all_st_sect_nm .= ", ";
			}

			$all_st_sect_id .= $row["st_id"];
			$all_st_sect_nm .= $row["st_nm"];
		}

	}
	$approve = "";
	if($class_sect_nm != "" && $all_st_sect_nm != "")
	{
		$approve = $class_sect_nm."の".$all_st_sect_nm;
	}

	array_push($ret_arr, array("approve" => $approve,
	 						    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "class_sect_id" => $class_sect_id,
							    "atrb_sect_id" => $atrb_sect_id,
							    "dept_sect_id" => $dept_sect_id,
							    "room_sect_id" => $room_sect_id,
							    "st_sect_id" => $all_st_sect_id));
}

?>