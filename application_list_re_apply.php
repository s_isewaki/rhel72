<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="application_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="approve_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apply_stat" value="<?echo($apply_stat)?>">
</form>


<?
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("application_workflow_common_class.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("Cmx.php");
require_once("aclg_set.php");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}


// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//勤怠申請機能を使用するかファイルで確認 
$kintai_flag = file_exists("kintai/common/mdb.php");
// 総合届け使用フラグ
$general_flag = file_exists("opt/general_flag");
// ----- メディアテック Start -----
if (phpversion() >= '5.1' && $kintai_flag) {
    require_once 'kintai/common/mdb_wrapper.php';
    require_once 'kintai/common/mdb.php';
    MDBWrapper::init();
    $mdb = new MDB();
    $mdb->beginTransaction(); // トランザクション開始
}
// ----- メディアテック End -----

$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

$arr_next_appry_id = array();
$must_send_mail_applys = array();
for ($list_re_apply_i = 0, $list_re_apply_count = count($re_apply_chk); $list_re_apply_i < $list_re_apply_count; $list_re_apply_i++) {
    if ($re_apply_chk[$list_re_apply_i] != "") {

        $apply_id = $re_apply_chk[$list_re_apply_i];

        // 再申請チェック
        $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
        $apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
        if($apply_stat != "3") {
            echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }

        // 新規申請(apply_id)採番
        $sql = "select max(apply_id) from apply";
        $cond = "";
        $apply_max_sel = select_from_table($con,$sql,$cond,$fname);
        $max = pg_result($apply_max_sel,0,"max");
        if($max == ""){
            $new_apply_id = "1";
        }else{
            $new_apply_id = $max + 1;
        }

        // 新規申請番号(apply_no)採番
        $date = date("Ymd");
        $year = substr($date, 0, 4);
        $md   = substr($date, 4, 4);

        if($md >= "0101" and $md <= "0331") {
            $year = $year - 1;
        }
        $max_cnt = $obj->get_apply_cnt_per_year($year);
        $apply_no = $max_cnt + 1;

        //テンプレートの場合XML形式のテキスト$contentを作成
        if ($arr_apply_wkfwmst[0]["wkfw_content_type"] == "2") {
            $apply_title = $arr_apply_wkfwmst[0]["apply_title"];
            $wkfw_id = $arr_apply_wkfwmst[0]["wkfw_id"];
            $ext = ".php";
            $savexmlfilename = "workflow/tmp/{$session}_x{$ext}";
            $arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $arr_apply_wkfwmst[0]["wkfw_history_no"]);
            $wkfw_content = $arr_wkfw_template_history["wkfw_content"];
            $wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
            $fp = fopen($savexmlfilename, "w");
            if (!$fp) {
                echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
                echo("<script language='javascript'>history.back();</script>");
            }
            if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
                fclose($fp);
                echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
                echo("<script language='javascript'>history.back();</script>");
            }
            fclose($fp);
            include( $savexmlfilename );
        }

        // 申請登録
        $obj->regist_re_apply($new_apply_id, $apply_id, "", "", "LIST");

        // ----- メディアテック Start -----
        if (phpversion() >= '5.1' && $kintai_flag) {
            // テンプレートタイプを取得
            require_once 'kintai/common/workflow_common_util.php';
            $template_type = WorkflowCommonUtil::get_template_type($apply_id); 
            
            $class = null;
            switch ($template_type) {
                case "ovtm": // 時間外勤務命令の場合
                    if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                        $class = WORKFLOW_OVTM_CLASS;
                    }
                    break;
                case "hol": // 休暇申請の場合
                    //総合届け
                    if ($general_flag) {
                        if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                            $class = WORKFLOW_HOL_CLASS;
                        }
                    }
                    //休暇申請
                    else {
                        if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                            $class = WORKFLOW_HOL2_CLASS;
                        }
                    }
                    break;
                case "travel": // 旅行命令
                    if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                        $class = WORKFLOW_TRVL_CLASS;
                    }
                    break;
                case "late": // 遅刻早退の場合
                    if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
                        $class = WORKFLOW_LATE_CLASS;
                    }
                    break;
            }
            if (!empty($class)) {
                $module = new $class;
                $module->regist_re_apply($new_apply_id, $apply_id);
                // 申請日を更新
                require_once 'kintai/common/workflow_common_util.php';
                WorkflowCommonUtil::apply_date_update($con, $new_apply_id);
            }
        }
        // ----- メディアテック End -----
        
        // 承認登録
        $obj->regist_re_applyapv($new_apply_id, $apply_id);

        // 承認者候補登録
        $obj->regist_re_applyapvemp($new_apply_id, $apply_id);

        // 添付ファイル登録
        $obj->regist_re_applyfile($new_apply_id, $apply_id);

        // 非同期・同期受信登録
        $obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);

        // 申請結果通知登録
        $obj->regist_re_applynotice($new_apply_id, $apply_id);

        // 前提とする申請書(申請用)登録
        $obj->regist_re_applyprecond($new_apply_id, $apply_id);

        // 元の申請書に再申請ＩＤを更新
        $obj->update_re_apply_id($apply_id, $new_apply_id);

        array_push($arr_next_appry_id, $new_apply_id);

        // メール送信不要であれば次の申請書へ
        $wkfw_id = get_wkfw_id_by_apply_id($con, $new_apply_id, $fname);
        $wkfw_send_mail_flg = get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
        if ($wkfw_send_mail_flg != "t") {
            continue;
        }

        // メール送信に必要な情報を配列に格納
        $sql = "select e.emp_email2, a.apv_order from applyapv a inner join empmst e on e.emp_id = a.emp_id";
        $cond = "where a.apply_id = $new_apply_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $to_addresses = array();
        while ($row = pg_fetch_array($sel)) {
            if (must_send_mail($wkfw_send_mail_flg, $row["emp_email2"], $arr_apply_wkfwmst[0]["wkfw_appr"], $row["apv_order"])) {
                $to_addresses[] = $row["emp_email2"];
            }
        }

        // 送信先がなければ次の申請書へ
        if (count($to_addresses) == 0) {
            continue;
        }

        // メール送信に必要な情報を配列に格納
        $must_send_mail_applys[] = array(
            "to_addresses" => $to_addresses,
            "wkfw_content_type" => $arr_apply_wkfwmst[0]["wkfw_content_type"],
            "content" => $arr_apply_wkfwmst[0]["apply_content"],
            "apply_title" => $arr_apply_wkfwmst[0]["apply_title"],
            "approve_label" => ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認"
        );
    }
}



// 添付ファイルをコピー
// 添付ファイルの確認
if (!is_dir("apply")) {
    mkdir("apply", 0755);
}
for ($i = 0; $i < $list_re_apply_count; $i++) {
    if ($re_apply_chk[$i] != "") {

        $apply_id = $re_apply_chk[$i];
        $sql = "select * from applyfile";
        $cond = "where apply_id = $apply_id order by apply_id asc";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if($sel==0){
            pg_exec($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {

            $fine_no = $row["applyfile_no"];
            $file_name = $row["applyfile_name"];
            $ext = strrchr($file_name, ".");

            $tmp_filename = "apply/{$apply_id}_{$fine_no}{$ext}";
            copy($tmp_filename, "apply/{$arr_next_appry_id[$i]}_{$fine_no}{$ext}");
        }
    }
}

// メール送信準備
if (count($must_send_mail_applys) > 0) {
    $emp_id = get_emp_id($con, $session, $fname);
    $emp_detail = $obj->get_empmst_detail($emp_id);
    $emp_nm = format_emp_nm($emp_detail[0]);
    $emp_mail_header = format_emp_mail_header($emp_detail[0]);
    $emp_mail = format_emp_mail($emp_detail[0]);
    $emp_pos = format_emp_pos($emp_detail[0]);
}

// ----- メディアテック Start -----
if (phpversion() >= '5.1' && $kintai_flag) {
    // MDBトランザクションをコミット
    $mdb->endTransaction();
}
// ----- メディアテック End -----
// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($must_send_mail_applys) > 0) {
	$conf = new Cmx_SystemConfig();
	$title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");
	
	foreach ($must_send_mail_applys as $data) {
		$mail_subject = "[CoMedix] {$data["approve_label"]}依頼のお知らせ";
		$mail_content = format_mail_content($con, $data["wkfw_content_type"], $data["content"], $fname);
		$mail_separator = str_repeat("-", 60) . "\n";
		$additional_headers = "From: {$emp_mail_header}";
		$additional_parameter = "-f{$emp_mail}";
		
		foreach ($data["to_addresses"] as $to_address) {
			$mail_body = "以下の{$data["approve_label"]}依頼がありました。\n\n";
			$mail_body .= "申請者：{$emp_nm}\n";
			$mail_body .= "所属：{$emp_pos}\n";
			$mail_body .= "{$title_label}：{$data["apply_title"]}\n";
			if ($mail_content != "") {
				$mail_body .= $mail_separator;
				$mail_body .= "{$mail_content}\n";
			}
			
			mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
		}
	}
}

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");

// ----- メディアテック Start -----
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
// ----- メディアテック end -----
?>
</body>
