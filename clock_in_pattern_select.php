<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 勤務パターン選択</title>
<?
require_once("about_postgres.php");
require_once("html_utils.php");
require_once("date_utils.php");

$fname = $PHP_SELF;

//グループIDが渡されてない場合エラー
if (empty($group_id)) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

//
$today = date("Ymd");
$sql =  "SELECT type FROM calendar WHERE date = '$today'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$type = $sel["type"];

$type = ($type != "2" && $type != "3") ? "1" : $type;

//勤務パターンｘ勤務開始時刻取得SQL 表示順、非表示対応 20101007
$sql =  "SELECT ".
    "atdptn.atdptn_id, ".
    "atdptn.atdptn_nm, ".
    "officehours.officehours2_start, ".
    "officehours.officehours2_end ".
    "FROM ".
    "atdptn ".
    "LEFT JOIN officehours ON (officehours.tmcd_group_id = $group_id AND CAST(atdptn.atdptn_id AS varchar) = officehours.pattern AND officehours.officehours_id = '".$type."') ".
	"WHERE ".
	"atdptn.group_id = ".$group_id." and atdptn.display_flag = 't' ".
	"ORDER BY ".
	"atdptn.atdptn_order_no, atdptn.atdptn_id";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$now_time = date("Hi");
$pattern_hours_array = array();
$earliest_atdptn_id = "";
$earliest_time = "";
$select_atdptn_id = "";
$nearest_time_difference = "";
while ($row = pg_fetch_array($sel)) {

	$pattern_hours_array[] = $row;

	//コロンを消す
	$work_start_time = str_replace(":", "", $row["officehours2_start"]);

	if (empty($work_start_time) == false){

		//最も近い時刻のパターンIDと差分を保持
		if ($now_time < $work_start_time && (empty($nearest_time_difference) || $work_start_time - $now_time < $nearest_time_difference)){
			$select_atdptn_id           = $row["atdptn_id"];
			$nearest_time_difference    = $work_start_time - $now_time;
		}

		//出勤時間がすべて経過していた場合、最も早い出勤時間を設定するためにid保持
		if ($earliest_time > $work_start_time || empty($earliest_time)){
			$earliest_atdptn_id = $row["atdptn_id"];
			$earliest_time      = $work_start_time;
		}
	}

}

//出勤時間がすべて経過していた場合、最も早い出勤時間を設定
if (empty($select_atdptn_id)){
	$select_atdptn_id = $earliest_atdptn_id;
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<SCRIPT type="text/javascript">

	function open_clock_in_insert(){

		opener.document.loginform.id.value        = '<?=$id ?>';
		opener.document.loginform.pass.value      = '<?=$pass ?>';
		opener.document.loginform.wherefrom.value = '<?=$wherefrom ?>';

		opener.document.loginform.select_group_id.value  = '<?=$group_id ?>';
		opener.document.loginform.focus_flg.value  = 'f';
		opener.document.loginform.hol_reg_flg.value  = '1';
		opener.document.loginform.conf_ok_flg.value  = '<?=$conf_ok_flg ?>';
		var len = document.select_atdptn.select_atdptn_id.length;
		selected_atdptn_id = null;
		for(i=0;i<len;i++) {
			if (document.select_atdptn.select_atdptn_id[i].checked == true){
				selected_atdptn_id = document.select_atdptn.select_atdptn_id[i].value;
			}
		}
		opener.document.loginform.select_atdptn_id.value = selected_atdptn_id;
		opener.regPatternSelectTimecard();
		self.close();

	}

</SCRIPT>


<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>

<form name="select_atdptn" action="" method="post" target="newwin">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td align="right">
			<input type="button" value="確認" onclick="open_clock_in_insert()">
		</td>
	</tr>
<?
	$pattern_count = 0;
	foreach ($pattern_hours_array as $pattern_array) {
		echo("<tr><td>");
		html_utils::show_radio("select_atdptn_id", $select_atdptn_id, $pattern_array["atdptn_id"], $pattern_array["atdptn_nm"], $pattern_count);
//		echo($pattern_array["officehours2_start"]);
		echo("</td></tr>");
		$pattern_count++;
	}
?>

	<tr>
		<td align="right">
			<input type="button" value="確認" onclick="open_clock_in_insert()">
		</td>
	</tr>
</table>
<input type="hidden" name="conf_ok_flg" value="<?=$conf_ok_flg?>">
</form>

</center>
</body>
<? pg_close($con); ?>
</html>
