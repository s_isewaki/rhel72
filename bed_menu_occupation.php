<?
session_start();
$_SESSION['bed_in_ret_url'] = "bed_menu_occupation.php?bldg_cd=$bldg_cd&ward_cd=$ward_cd&action=" . urlencode($action) . "&session=$session";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | 病床利用状況</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_occupation_detail.ini");
require_once("kango_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$checkauth = check_authority($session, 14, $fname);
if($checkauth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 棟一覧を取得
$sql = "select bldg_cd, bldg_name from bldgmst";
$cond = "where bldg_del_flg = 'f' order by bldg_cd";
$sel_bldg = select_from_table($con, $sql, $cond, $fname);
if ($sel_bldg == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_count = pg_num_rows($sel_bldg);

// 病棟一覧の取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $sql, $cond, $fname);
if ($sel_ward == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

if ($action == "表示") {
	set_user_selected_bldg($con, $fname, $emp_id, $bldg_cd, $ward_cd);
} else {
	$user_selected_bldg = get_user_selected_bldg($con, $fname, $emp_id);
	if (!is_null($user_selected_bldg)) {
		$bldg_cd = $user_selected_bldg["bldg_cd"];
		$ward_cd = $user_selected_bldg["ward_cd"];
		$action = "表示";
	}
}

if ($action == "表示") {

	// 画面更新間隔の値を取得
	$sql = "select refresh_sec from roomlayout";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$refresh_sec = pg_fetch_result($sel, 0, "refresh_sec");
	} else {
		$refresh_sec = 300;
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
function initPage() {
<? if ($action == "表示") { ?>
	setTimeout(function(){location.reload();}, <? echo($refresh_sec * 1000); ?>);
<? } ?>

	resetWardOptions('<? echo($ward_cd); ?>');

	YUI().use('dd-drag', 'dd-drop', 'dd-scroll', function(Y){
		var divs = document.getElementsByTagName('div');
		for (var i = 0, j = divs.length; i < j; i++) {
			switch (divs[i].className) {
			case 'used':
				new Y.DD.Drop({node: '#' + divs[i].id});

				var bed = new Y.DD.Drag({node: '#' + divs[i].id}).plug(Y.Plugin.DDWinScroll);
				bed.set('data', bed.get('node').getXY());
				bed.on('drag:start', function (e){
					var dragNode = this.get('node');
					dragNode.setStyle('backgroundColor', '#fefe83');
					dragNode.setStyle('zIndex', 999);
				});
				bed.on('drag:drophit', function(e){
					var pt_id = e.drag.get('node').get('id').split('_').slice(5).join('');
					var bed_info = e.drop.get('node').get('id').split('_');
					window.open('inpatient_move_reserve_register.php?session=<? echo($session); ?>&pt_id='.concat(pt_id).concat('&ward=').concat(bed_info[1]).concat('-').concat(bed_info[2]).concat('&ptrm=').concat(bed_info[3]).concat('&bed=').concat(bed_info[4]).concat('&direct=t'), 'newwin', 'width=900,height=640,scrollbars=yes');
				});
				bed.on('drag:end', function(e){
					var dragNode = this.get('node');
					dragNode.setXY(this.get('data'));
					dragNode.setStyle('backgroundColor', '');
					dragNode.setStyle('zIndex', 0);
				});
				break;

			case 'vacant':
				new Y.DD.Drop({node: '#' + divs[i].id});
				break;
			}
		}
	});
}

function resetWardOptions(default_ward_cd) {
	var bldg_cd = document.mainform.bldg_cd.value;

	deleteAllOptions(document.mainform.ward_cd);

	addOption(document.mainform.ward_cd, '', '　　　　　', default_ward_cd);
	addOption(document.mainform.ward_cd, '0', 'すべて', default_ward_cd);

	switch (bldg_cd) {
<?
$pre_bldg_cd = "";
while ($row = pg_fetch_array($sel_ward)) {
	$cur_bldg_cd = $row["bldg_cd"];

	if ($cur_bldg_cd != $pre_bldg_cd) {
		if ($pre_bldg_cd != "") {
			echo ("\t\tbreak;\n");
		}
		echo ("\tcase '$cur_bldg_cd':\n");
		$pre_bldg_cd = $cur_bldg_cd;
	}

	echo("\t\taddOption(document.mainform.ward_cd, '{$row["ward_cd"]}', '{$row["ward_name"]}', default_ward_cd);\n");
}
?>
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function popupPatientDetail(pt_info1, pt_info13, pt_info2, pt_info3, pt_info4, pt_info5, pt_info6, pt_info7, pt_info8, pt_info22, pt_info23, pt_info9, pt_info10, pt_info11, pt_info14, pt_info15, pt_info20, pt_info21, pt_info12, pt_info16, pt_info19, pt_info24, pt_info18, pt_info17, e) {
	popupDetailBlue(
		new Array(
			'患者氏名', pt_info1,
			'患者ID', pt_info13,
			'保険', pt_info2,
			'性別', pt_info3,
			'年齢', pt_info4,
			'診療科', pt_info5,
			'主治医', pt_info6,
			'移動', pt_info7,
			'観察', pt_info8,
			'面会', pt_info9,
			'外泊', pt_info10,
			'外出', pt_info11,
			'入院日', pt_info14,
			'在院日数', pt_info15,
			'回復期リハビリ算定期限分類', pt_info22,
			'回復期リハビリ算定期限日', pt_info23,
			'リハビリ算定期限分類', pt_info20,
			'リハビリ算定期限日', pt_info21,
			'退院予定日', pt_info12,
			'医療区分・ADL区分', pt_info16,
			'看護度分類', pt_info19,
			'差額室料免除',pt_info24,
			'個人情報に関する要望', pt_info18,
			'特記事項', pt_info17
		), 350, 100, e
	);
}

function printPage() {
<? $tmp_ward_cd = ($ward_cd == "0") ? "" : $ward_cd; ?>
	window.open('bed_menu_occupation_print.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($tmp_ward_cd); ?>', 'occp', 'width=800,height=600,scrollbars=yes');
}

function showDetail() {
	if (document.mainform.ward_cd.value == '') {
		return;
	}

	document.mainform.action.value = '表示';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.bed {border-collapse:collapse;}
table.bed td {border:solid #5279a5 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height ="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu.php?session=<? echo($session); ?>">お知らせ</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>">入院患者検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用状況</font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="mainform" action="bed_menu_occupation.php" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事業所（棟）：<select name="bldg_cd" onchange="resetWardOptions();">
<?
while ($row = pg_fetch_array($sel_bldg)) {
	echo("<option value=\"{$row["bldg_cd"]}\"");
	if ($row["bldg_cd"] == $bldg_cd) {
		echo(" selected");
	}
	echo(">{$row["bldg_name"]}\n");
}
?></select>
病棟：<select name="ward_cd" onchange="showDetail();">
</select>
</font></td>
<? if ($bldg_count > 0 && $action == "表示") { ?>
<? //if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") === false) { ?>
<td align="center">
<a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">←</a>
<img src="img/spacer.gif" alt="" width="5" height="1">
<a href="javascript:void(0);" onclick="top.scrollBy(0, 99999);">↓</a>
<img src="img/spacer.gif" alt="" width="5" height="1">
<a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">→</a>
</td>
<? //} ?>
<td align="right">
<input type="button" value="全画面" onclick="window.open('bed_menu_occupation_sub.php?session=<? echo($session); ?>&bldg_cd=<? echo($bldg_cd); ?>&ward_cd=<? echo($ward_cd); ?>', 'ocpall', 'width=' + (screen.width - 50) + ',height=' + (screen.height - 100) + ',scrollbars=yes,resizable=yes');">
<input type="button" value="EXCEL出力" onclick="document.excelform.submit();">
<input type="button" value="印刷" onclick="printPage();">
</td>
<? } ?>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<?
if ($bldg_count == 0) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">棟が登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
} else if ($action == "表示") {
	if ($ward_cd == "0") {
		show_bldg_report($con, $bldg_cd, $fname);
		$ward_cd = "";
	}

	show_ward_layout($con, $bldg_cd, $ward_cd, $session, $fname);

	if ($ward_cd != "") {
		show_ward_report($con, $bldg_cd, $ward_cd, $fname);
	}
}
?>
<input type="hidden" name="session" value="<? echo $session ?>">
<input type="hidden" name="action" value="">
</form>
<? if ($bldg_count > 0 && $action == "表示") { ?>
<? //if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") === false) { ?>
<img src="img/spacer.gif" alt="" width="1" height="20"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td align="center">
<a href="javascript:void(0);" onclick="window.scrollBy(-99999, 0);">←</a>
<img src="img/spacer.gif" alt="" width="5" height="1">
<a href="javascript:void(0);" onclick="top.scrollBy(0, -99999);">↑</a>
<img src="img/spacer.gif" alt="" width="5" height="1">
<a href="javascript:void(0);" onclick="window.scrollBy(99999, 0);">→</a>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="20"><br>
<? //} ?>
<? } ?>
</td>
</tr>
</table>
<iframe name="excelframe" width="0" height="0" frameborder="0"></iframe>
<form name="excelform" action="bed_menu_occupation_excel.php" method="get" target="excelframe">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
