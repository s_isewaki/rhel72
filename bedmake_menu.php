<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜ベットメイクオーダー</title>
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("about_postgres.php"); ?>
<? require("./conf/sql.inf"); ?>
<? require("show_bedmake_menu.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$ward = check_authority($session,14,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

//DBコネクションの作成
$con = connect2db($fname);

// 病棟一覧の取得
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel_ward = select_from_table($con, $SQL67, $cond, $fname);
if ($sel_ward == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_ward = array();
$arr_wardptrm = array();

// 全病棟をループ
while ($row = pg_fetch_array($sel_ward)) {
	$tmp_bldg_cd = $row["bldg_cd"];
	$tmp_ward_cd = $row["ward_cd"];
	$tmp_bldg_wd = "$tmp_bldg_cd-$tmp_ward_cd";
	$tmp_ward_nm = $row["ward_name"];
	array_push($arr_ward, array("bldgwd" => $tmp_bldg_wd, "ward_nm" => $tmp_ward_nm));

	// 病室一覧の取得
	$cond = "where bldg_cd = '$tmp_bldg_cd' and ward_cd = '$tmp_ward_cd' and ptrm_del_flg = 'f' order by ptrm_room_no";
	$sel_ptrm = select_from_table($con, $SQL72, $cond, $fname);
	if ($sel_ptrm == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	$arr_ptrm = array();

	// 全病室をループ
	while ($row = pg_fetch_array($sel_ptrm)) {
		$ptrm_no = $row["ptrm_room_no"];
		$ptrm_nm = $row["ptrm_name"];
		$bed_chg = $row["ptrm_bed_chg"];
		if ($bed_chg > 0) {
			$ptrm_nm .= "（差額" . number_format($bed_chg) . "円）";
		}
		array_push($arr_ptrm, array("ptrm_no" => $ptrm_no, "ptrm_nm" => $ptrm_nm));
	}

	array_push($arr_wardptrm, array("bldgwd" => $tmp_bldg_wd, "ptrms" => $arr_ptrm));
}

list($bldg_cd, $ward_cd) = split("-", $bldg_wd);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="javascript">
function initPage() {

<? if ($bldg_wd != "") { ?>
	document.search.bldg_wd.value = '<? echo($bldg_wd); ?>';
<? } ?>

	wardOnChange('<? echo $ptrm; ?>');

}

function wardOnChange(ptrm) {

	var bldgwd = document.search.bldg_wd.value;

	// 病室セレクトボックスのオプションを全削除
	deleteAllOptions(document.search.ptrm);

	// 病室セレクトボックスのオプションを作成
<? foreach ($arr_wardptrm as $wardptrm) { ?>
	if (bldgwd == '<? echo $wardptrm["bldgwd"]; ?>') {
	<? foreach($wardptrm["ptrms"] as $ptrmitem) { ?>
		addOption(document.search.ptrm, '<? echo $ptrmitem["ptrm_no"]; ?>', '<? echo $ptrmitem["ptrm_nm"]; ?>', ptrm);
	<? } ?>
	}
<? } ?>

	// オプションが1件も作成されなかった場合
	if (document.search.ptrm.options.length == 0) {
		addOption(document.search.ptrm, '0', '（未登録）', ptrm);
	}

	// 病室が未登録の場合、ボタンを押下不可にする
	var btnDisabled = (document.search.ptrm.value == '0');
	document.search.btnDetail.disabled = btnDisabled;
	document.search.btnSearch.disabled = btnDisabled;

}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}

}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';

}

function openEquipWin() {

	var arr_bldgwd = document.search.bldg_wd.value.split('-');

	var url = 'room_equipment_detail.php';
	url += '?session=<? echo $session; ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&rm_no=' + document.search.ptrm.value;

	window.open(url, 'newwin', 'width=640,height=480,scrollbars=yes');

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.bed {border-collapse:collapse;}
table.bed td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ベットメイク</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="25%" height="22" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイクオーダー</font></td>
<td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bedmake_operation.php?session=<? echo($session); ?>">実施入力</a></font></td>
<td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
<td width="25%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"></font></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- 検索 -->
<form name="search" action="bedmake_menu.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td width="350" height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟：</font><select name="bldg_wd" onchange="wardOnChange();">
<?
foreach ($arr_ward as $warditem) {
	$tmp_bldgwd = $warditem["bldgwd"];
	$tmp_ward_nm = $warditem["ward_nm"];
	echo "<option value='$tmp_bldgwd'>$tmp_ward_nm\n";
}
?>
</select>&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病室：</font><select name="ptrm"></select>&nbsp;<input type="button" name="btnDetail" value="詳細" onclick="openEquipWin();"></td>
<td width="100" align="center"><input name="btnSearch" type="submit" value="検索"></td>
<td>&nbsp;</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo $session ?>">
</form>
<? show_bed_list($con, $session, $bldg_wd, $ptrm, $bed, $fname); ?>
<? if ($bed != "") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- ベットメイク依頼 -->
<form action="bedmake_insert.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="22" colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベットメイク依頼</b></font></td>
</tr>
<tr bgcolor="#f6f9ff">
<td width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">依頼内容：</font></td>
<td width="480"><input type="checkbox" name="sheet_chg_flg" value="t"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">シーツ交換</font>&nbsp;<input type="checkbox" name="mattress_st_flg" value="t"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マットレス消毒</font>&nbsp;<input type="checkbox" name="bed_st_flg" value="t"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベット消毒</font>&nbsp;<input type="submit" value="登録"></td>
<td>&nbsp;</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="bldg_wd" value="<? echo($bldg_wd); ?>">
<input type="hidden" name="bldg_cd" value="<? echo($bldg_cd); ?>">
<input type="hidden" name="ward_cd" value="<? echo($ward_cd); ?>">
<input type="hidden" name="room_no" value="<? echo($ptrm); ?>">
<input type="hidden" name="bed_no" value="<? echo($bed); ?>">
<input type="hidden" name="wherefrom" value="1">
</form>
<!-- ベットメイク履歴 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="22" colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ベットメイク履歴</b></font></td>
</tr>
<tr bgcolor="#f6f9ff">
<td width="280"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">依頼内容</font></td>
<td width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">完了日時</font></td>
<td width="160"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">依頼日時</font></td>
<td>&nbsp;</td>
</tr>
<? show_bedmake_list($con, $bldg_cd, $ward_cd, $ptrm, $bed, $fname) ?>
</table>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
