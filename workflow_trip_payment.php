<?php
// 出張旅費
// 支給基準マスタ・一覧
require_once("about_comedix.php");
require_once("workflow_common.ini");
require_once("referer_common.ini");

$fname = $_SERVER["PHP_SELF"];
$workflow = @$_REQUEST["workflow"];

// セッションと権限のチェック
$session = qualify_session($_REQUEST["session"], $fname);
$checkauth = check_authority($session, 3, $fname);
if ($session == "0" or $checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "") {
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
} else {
	$referer = get_referer($con, $session, "workflow", $fname);
}

// type
$type = @$_REQUEST["type"] ? $_REQUEST["type"] : 1;

// オプション設定の取得
$sql = "SELECT * FROM trip_option";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
$option = pg_fetch_assoc($sel);
if ($type == 1) {
	$payment_date = $option["payment1_date"];
}
else {
	$payment_date = $option["payment2_date"];
}

// 支給基準マスタの取得
$sql = "SELECT p.*,s.st_id as s_st_id,s.st_nm FROM (SELECT * FROM trip_payment WHERE type={$type}) p RIGHT JOIN stmst s USING (st_id) WHERE st_del_flg='f' ORDER BY order_no";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	js_error_exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 出張旅費 | 支給基準マスタ・一覧</title>
<script type="text/javascript" src="js/yui_2.8.1/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/animation/animation-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/element/element-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/build/calendar/calendar-min.js"></script>
<script type="text/javascript" src="js/yui_2.8.1/DDTableRow.js"></script>
<script type="text/javascript" src="js/workflow/trip_payment.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
<link rel="stylesheet" type="text/css" href="css/workflow/trip.css">
<link type="text/css" rel="stylesheet" href="js/yui_2.8.1/build/calendar/assets/calendar.css">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<? if ($referer == "2") { ?>
<table id="header">
<tr>
<td class="icon"><a href="workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><a href="workflow_menu.php?session=<?=$session?>"><b>ワークフロー</b></a></td>
</tr>
</table>
<? } else { ?>
<table id="header">
<tr>
<td class="icon"><a href="application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
<td>
	<a href="application_menu.php?session=<? echo($session); ?>"><b>決裁・申請</b></a> &gt;
	<a href="workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt;
	<a href="workflow_trip_seisan.php?session=<? echo($session); ?>"><b>出張旅費</b></a>
</td>
<td class="kanri"><a href="application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></td>
</tr>
</table>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="120"><?show_wkfw_sidemenu($session);?></td>
<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top">

<?
show_wkfw_menuitem($session, $fname, "");
?>
<ul id="menu2_tab">
	<li><a href="?type=1&session=<?=$session?>">支給基準1</a></li>
	<li><a href="?type=2&session=<?=$session?>">支給基準2</a></li>
</ul>

<form id="mstform" action="workflow_trip_payment_submit.php" method="post">

<div class="button">
<button type="submit">更新</button>
</div>

<p>施行日：<input type="text" id="date" name="payment_date" value="<?eh(!empty($payment_date) ? str_replace("-","/",$payment_date) : "");?>" size="12" readonly onclick="YAHOO.cal.cal1.show();" /> から有効</p>
<div id="calContainer"></div>

<table id="msttable">
<thead>
<tr>
	<td rowspan="2">役職</td>
	<td colspan="2">宿泊料</td>
	<td rowspan="2">宿泊日当</td>
	<td rowspan="2">日帰日当</td>
	<td rowspan="2">表示</td>
</tr>
<tr>
	<td>一般</td>
	<td>東京23区</td>
</tr>
</thead>
<tbody>
<?
while ($r = pg_fetch_assoc($sel)) {
?>
<tr>
	<td class="id"><?eh($r["st_nm"]);?><input name="st_id[]" type="hidden" value="<?eh($r["s_st_id"]);?>"/></td>
	<td class="number"><input type="number" name="hotel[<?eh($r["s_st_id"]);?>]" value="<?eh($r["hotel"]);?>" size="10" style="ime-mode:disabled;" />円</td>
	<td class="number"><input type="number" name="hotel_tokyo[<?eh($r["s_st_id"]);?>]" value="<?eh($r["hotel_tokyo"]);?>" size="10" style="ime-mode:disabled;" />円</td>
	<td class="number"><input type="number" name="stay[<?eh($r["s_st_id"]);?>]" value="<?eh($r["stay"]);?>" size="10" style="ime-mode:disabled;" />円</td>
	<td class="number"><input type="number" name="daytrip[<?eh($r["s_st_id"]);?>]" value="<?eh($r["daytrip"]);?>" size="10" style="ime-mode:disabled;" />円</td>
	<td class="check"><input name="view[<?eh($r["s_st_id"]);?>]" type="checkbox" value="t" <?echo (is_null($r["view_flg"]) or $r["view_flg"]=='t') ? 'checked' : '';?>/></td>
</tr>
<?}?>
</tbody>
</table>

<div class="button">
<button type="submit">更新</button>
</div>

<input type="hidden" name="session" value="<?=$session?>" />
<input type="hidden" name="type" value="<?=$type?>" />
</form>

</td>
</tr>
</table>
</body>
</html>
<?php
pg_close($con);