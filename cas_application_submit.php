<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="cas_application_menu.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">

<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


//for ($i = 1; $i <= $approve_num; $i++) {
//	$varname = "approve_name$i";
//	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
//}

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_type" value="<? echo($wkfw_type); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="apply_title_disp_flg" value="<?=$apply_title_disp_flg?>">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
<input type="hidden" name="wkfwfile_history_no" value="<?=$wkfwfile_history_no?>">
<input type="hidden" name="short_wkfw_name" value="<?=$short_wkfw_name?>">
</form>
<?

$fname=$PHP_SELF;

require_once("./about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, $CAS_MENU_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}




// 添付ファイルの確認
if (!is_dir("cas_apply")) {
	mkdir("cas_apply", 0755);
}
if (!is_dir("cas_apply/tmp")) {
	mkdir("cas_apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "cas_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

$obj = new cas_application_workflow_common_class($con, $fname);


// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];
$emp_class     = $arr_empmst[0]["emp_class"];
$emp_attribute = $arr_empmst[0]["emp_attribute"];
$emp_dept      = $arr_empmst[0]["emp_dept"];
$emp_room      = $arr_empmst[0]["emp_room"];

// 申請の場合、未指定の承認者を許可する
if ($draft != "on") {
	$tmp_arr_apv = array();
	$tmp_apv_order = 0;
	$tmp_pre_apv_order = 0;
	for ($i = 1; $i <= $approve_num; $i++) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";

		if ($$tmp_regist_emp_id_var != "") {
			if ($$tmp_apv_order_var != $tmp_pre_apv_order) {
				$tmp_apv_order++;
				$tmp_apv_sub_order = 1;
				$tmp_pre_apv_order = $$tmp_apv_order_var;
			} else {
				$tmp_apv_sub_order += 1;
			}

			if ($tmp_apv_sub_order == 2) {
				$tmp_arr_apv[count($tmp_arr_apv) - 1]["apv_sub_order"] = 1;
			}

			$tmp_arr_apv[] = array(
				"regist_emp_id" => $$tmp_regist_emp_id_var,
				"st_div" => $$tmp_st_div_var,
				"parent_pjt_id" => $$tmp_parent_pjt_id_var,
				"child_pjt_id" => $$tmp_child_pjt_id_var,
				"apv_order" => $tmp_apv_order,
				"apv_sub_order" => (($tmp_apv_sub_order == 1) ? null : $tmp_apv_sub_order),
				"multi_apv_flg" => $$tmp_multi_apv_flg_var,
				"next_notice_div" => $$tmp_next_notice_div_var
			);
		}

		unset($$tmp_regist_emp_id_var);
		unset($$tmp_st_div_var);
		unset($$tmp_parent_pjt_id_var);
		unset($$tmp_child_pjt_id_var);
		unset($$tmp_apv_order_var);
		unset($$tmp_apv_sub_order_var);
		unset($$tmp_multi_apv_flg_var);
		unset($$tmp_next_notice_div_var);
	}
	$approve_num = count($tmp_arr_apv);
	$i = 1;
	foreach ($tmp_arr_apv as $tmp_apv) {
		$tmp_regist_emp_id_var = "regist_emp_id$i";
		$tmp_st_div_var = "st_div$i";
		$tmp_parent_pjt_id_var = "parent_pjt_id$i";
		$tmp_child_pjt_id_var = "child_pjt_id$i";
		$tmp_apv_order_var = "apv_order$i";
		$tmp_apv_sub_order_var = "apv_sub_order$i";
		$tmp_multi_apv_flg_var = "multi_apv_flg$i";
		$tmp_next_notice_div_var = "next_notice_div$i";

		$$tmp_regist_emp_id_var = $tmp_apv["regist_emp_id"];
		$$tmp_st_div_var = $tmp_apv["st_div"];
		$$tmp_parent_pjt_id_var = $tmp_apv["parent_pjt_id"];
		$$tmp_child_pjt_id_var = $tmp_apv["child_pjt_id"];
		$$tmp_apv_order_var = $tmp_apv["apv_order"];
		$$tmp_apv_sub_order_var = $tmp_apv["apv_sub_order"];
		$$tmp_multi_apv_flg_var = $tmp_apv["multi_apv_flg"];
		$$tmp_next_notice_div_var = $tmp_apv["next_notice_div"];

		$i++;
	}
	unset($tmp_arr_apv);
}

// トランザクション開始
pg_query($con, "begin");

// 新規申請(apply_id)採番
$sql = "select max(apply_id) from cas_apply";
$cond = "";
$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
$max = pg_result($apply_max_sel,0,"max");
if($max == ""){
	$apply_id = "1";
}else{
	$apply_id = $max + 1;
}

// テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "cas_workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//20070918
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、申請してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、申請してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );


}

$apply_no = null;
if($draft != "on")
{
	$date = date("Ymd");
	$year = substr($date, 0, 4);
	$md   = substr($date, 4, 4);

	if($md >= "0101" and $md <= "0331")
	{
		$year = $year - 1;
	}
	$max_cnt = $obj->get_apply_cnt_per_year($year);
	$apply_no = $max_cnt + 1;
}



// 申請登録
$arr = array(
				"apply_id" => $apply_id,
				"wkfw_id" => $wkfw_id,
				"apply_content" => $content,
				"emp_id" => $emp_id,
				"apply_stat" => "0",
				"apply_date" => date("YmdHi"),
				"delete_flg" => "f",
				"apply_title" => $apply_title,
				"re_apply_id" => null,
				"apv_fix_show_flg" => "t",
				"apv_bak_show_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"apv_ng_show_flg" => "t",
				"emp_room" => $emp_room,
				"draft_flg" => ($draft == "on") ? "t" : "f",
				"wkfw_appr" => $wkfw_appr,
				"wkfw_content_type" => $wkfw_content_type,
				"apply_title_disp_flg" => $apply_title_disp_flg,
				"apply_no" => $apply_no,
				"notice_sel_flg" => $rslt_ntc_div2_flg,
				"wkfw_history_no" => ($wkfw_history_no == "") ? null : $wkfw_history_no,
				"wkfwfile_history_no" => ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
			 );

$obj->regist_apply($arr);


// 承認登録
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$emp_id = ($$varname == "") ? null : $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;


	// 所属、役職も登録する
	$infos = get_empmst($con, $emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];

	$arr = array(
				"wkfw_id" => $wkfw_id,
				"apply_id" => $apply_id,
				"apv_order" => $apv_order,
				"emp_id" => $emp_id,
				"apv_stat" => "0",
				"apv_date" => "",
				"delete_flg" => "f",
				"apv_comment" => "",
				"st_div" => $st_div,
				"deci_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"emp_st" => $emp_st,
				"apv_fix_show_flg" => "t",
				"emp_room" => $emp_room,
				"apv_sub_order" => $apv_sub_order,
				"multi_apv_flg" => $multi_apv_flg,
				"next_notice_div" => $next_notice_div,
				"parent_pjt_id" => $parent_pjt_id,
				"child_pjt_id" => $child_pjt_id,
				"other_apv_flg" =>  "f"
				);

	$obj->regist_applyapv($arr);

}

// 承認者候補登録
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "pst_approve_num$apv_order";
	$pst_approve_num = $$varname;

	for($j=1; $j<=$pst_approve_num; $j++)
	{
		$varname = "pst_emp_id$apv_order";
		$varname .= "_$j";
		$pst_emp_id = $$varname;

		$varname = "pst_st_div$apv_order";
		$varname .= "_$j";
		$pst_st_div = $$varname;

		$varname = "pst_parent_pjt_id$apv_order";
		$varname .= "_$j";
	    $pst_parent_pjt_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_child_pjt_id$apv_order";
		$varname .= "_$j";
    	$pst_child_pjt_id = ($$varname == "") ? null : $$varname;

		$obj->regist_applyapvemp($apply_id, $apv_order, $j, $pst_emp_id, $pst_st_div, $pst_parent_pjt_id, $pst_child_pjt_id);
	}
}





// 添付ファイル登録
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 非同期・同期受信登録
if($wkfw_appr == "2")
{
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	for($i=1; $i <= $approve_num; $i++)
	{
		$varname = "apv_order$i";
		$apv_order = $$varname;

		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;

		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
		}
		$arr_apv_sub_order[] = $apv_sub_order;
		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];

		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$obj->regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order);
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$obj->regist_applyasyncrecv($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order);
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}
}

// 申請結果通知登録
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// 前提とする申請書(申請用)登録
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->regist_applyprecond($apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

if($draft != "on")
{
	// 評価申請の場合
	if($obj->is_eval_template($short_wkfw_name))
	{
        $arr_eval_apply_id = $obj->create_arr_eval_apply_id($application_book_apply_id, $apply_id, $short_wkfw_name, "ADD");
	    $obj->update_cas_recognize_schedule_for_apply_id($arr_eval_apply_id);
	}

  	// レベルアップ申請の場合
	if($obj->is_levelup_apply_template($short_wkfw_name))
	{
        $obj->update_levelup_apply_id($levelup_application_book_id, $apply_id);
	}
	// 病棟評価表
	if($obj->is_ward_template($short_wkfw_name))
	{
		$obj->update_ward_grade_data($apply_id, $_POST);
	}
}

// トランザクションをコミット
pg_query($con, "commit");


// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "cas_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "cas_apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("cas_apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


if($draft == "on")
{
	echo("<script type=\"text/javascript\">location.href='cas_application_menu.php?session=$session&apply_id=$apply_id';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href='cas_application_apply_list.php?session=$session&mode=search&category=$wkfw_type&workflow=$wkfw_id';</script>");
}

?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
?>