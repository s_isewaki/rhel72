<?
require_once('Cmx.php');

if($ptid == ""){
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
</html>
<?
    exit;
}

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_mail.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_report_auth_models.php");
require_once("hiyari_db_class.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("hiyari_report_classification.php");

//==============================
//画面名
//==============================
$fname = $PHP_SELF;

$rep_obj = new hiyari_report_class($con, $fname);
//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0"){
    exit;
}
//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
    exit;
}

//==============================
//ユーザーID取得
//==============================
$emp_id = get_emp_id($con,$session,$fname);

// ログインID、パスワード取得
$sql = "select emp_login_id, emp_login_pass from login";
$cond = "where emp_id = '${emp_id}'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    exit;
}
if (pg_num_rows($sel) == 0) {
    exit;
}
$emp_login_id = pg_fetch_result($sel, 0, "emp_login_id");
$pass = pg_fetch_result($sel, 0, "emp_login_pass");

//データ連携設定情報
$arr_data_connect = $rep_obj->get_inci_data_connect();
$emr_path = $arr_data_connect["emr_path"];
$emr_exe_path = "\"${emr_path}\\CSEmrCtrl.exe\" -user ${emp_login_id} -password ${pass} -ptid ${ptid}";
$emr_exe_path = str_replace('\\', '\\\\', $emr_exe_path);
      
//画面名
$PAGE_TITLE = "電子カルテ実行";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>
</head>
<body>
<script language="JavaScript">
<!--
// 実行モジュール起動
function cmd(cmd) {
    try {
    var obj = new ActiveXObject("WScript.Shell");
    obj.Run(cmd);
    } catch(e) {
        alert(e);
    }
    window.location.replace="<?=$PHP_SELF?>";
}

window.onload = function() {
    cmd('<?=$emr_exe_path?>');
};
-->
</script>
</body>
</html>