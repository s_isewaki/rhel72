<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//入力チェック
if($trashbox==""){
	echo("<script language='javascript'>alert(\"チェックボックスをオンにしてください。\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

$count=count($trashbox);

$con = connect2db($fname);

// テンプレート登録チェック
$check_flg = true;
$message_filename = "";

$sql = "select distinct(tmpl_file) as tmpl_file from tmplmst";
$cond = "";		//SQLの条件

$sel = select_from_table($con,$sql,$cond,$fname);		//テンプレート名一覧を取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$arr_tmplfile = array();
$num = pg_numrows($sel);

for($i=0;$i<$num;$i++){
	$arr_tmplfile[$i] = pg_result($sel,$i,"tmpl_file");
}

for($i=0;$i<$count;$i++){
	for ($j=0;$j<$num;$j++) {
		if($trashbox[$i] == $arr_tmplfile[$j]){
			$check_flg = false;
			if ($message_filename != "") {
				$message_filename .= ",";
			}
			$message_filename .= $trashbox[$i];
		}
	}
}

if ($check_flg == false) {
	pg_close($con);
	$message = $message_filename . "はテンプレート登録されているため削除できません。";
	echo("<script language='javascript'>alert(\"$message\");</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

// 削除処理
for($i=0;$i<$count;$i++){
	if($trashbox[$i]!=""){
		$tmpl_filename = $trashbox[$i];
		$fp1 = fopen($tmpl_filename, "r");
		if ($fp1){
			$tmpl_content = fread($fp1, filesize($tmpl_filename));
			fclose($fp1);
		}

		$pos = strpos($tmpl_content, "xml_creator");
		$pos = strrpos($tmpl_content, "<", $pos);
		$pos1 = strpos($tmpl_content, "create_", $pos);
		$pos2 = strpos($tmpl_content, "\"", $pos1);
		$create_filename = substr($tmpl_content, $pos1, $pos2 - $pos1);

		unlink($tmpl_filename);
		unlink($create_filename);

	}
}

//削除成功時は一覧画面を再表示する。
echo("<script language='javascript'>location.href=\"./sinryo_tmpl_refer.php?session=$session\";</script>");

pg_close($con);
?>