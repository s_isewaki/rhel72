<?
require_once("about_comedix.php");

// テンプレートの場合、
$summary_naiyo = str_replace("&#160;", " ", @$_REQUEST["summary_naiyo_escaped_amp"]);
$summary_naiyo = str_replace("&nbsp;", " ", $summary_naiyo);
$summary_naiyo = str_replace("&amp;", "&", $summary_naiyo);
$summary_naiyo = h($summary_naiyo);
// フリー記載の場合はそのまま
if (!trim(@$_REQUEST["summary_naiyo_escaped_amp"])){
    $summary_naiyo = @$_REQUEST["summary_naiyo"];
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="summary_new.php" method="post">

<?
//========================================
//ファイルの読み込み
//========================================
// メドレポート
require("about_validation.php");
require_once("summary_common.ini"); // ｴﾗｰ時ログ用


$fname = $PHP_SELF;


//========================================
//セッションチェック
//========================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//========================================
//権限チェック
//========================================
$dept = check_authority($session,57,$fname);
if($dept == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//========================================
//ＤＢのコネクション作成
//========================================
$con = connect2db($fname);

$cre_date = $sel_yr.$sel_mon.$sel_day;

$my_div_id = (int)$_REQUEST["sel_diag"];
$sql = "select diag_div from divmst where div_id = $my_div_id";
$sel = select_from_table($con,$sql,"",$fname);
$tmp = pg_fetch_result($sel, 0, 0);
if ($tmp) { $_REQUEST["sel_diag"] = $tmp; $sel_diag = $tmp; }

?>

<? // メドレポート ?>
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="date" value="<?=$cre_date?>"><?//summary_new用?>
<input type="hidden" name="summary_problem" value="<?=$summary_problem?>">
<input type="hidden" name="diag" value="<?=$sel_diag?>">
<input type="hidden" name="priority" value="<?=$priority?>">
<input type="hidden" name="pt_id" value="<?=$pt_id?>"><?//summary_new用?>
<input type="hidden" name="xml_file" value="<?=$xml_file?>">
<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
<input type="hidden" name="problem_id" value="<?=$problem_id?>">
<input type="hidden" name="summary_naiyo" value="<?=h($summary_naiyo)?>">
<input type="hidden" name="sect_id" value="<?=$sect_id?>">
<input type="hidden" name="outreg" value="<?=$outreg?>">
<input type="hidden" name="seiki_emp_id" value="<?=$seiki_emp_id?>">
<input type="hidden" name="dairi_emp_id" value="<?=$dairi_emp_id?>">
<input type="hidden" name="direct_registration" value="<?=@$direct_registration?>">
<input type="hidden" name="copy_summary_id" value="<?=$_REQUEST["copy_summary_id"]?>">

</form>
<?

if($summary_naiyo =="" && !@$direct_registration){
    echo("<script language=\"javascript\">alert(\"登録内容を入力してください\");</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

//========================================
// メドレポート ＤＢ登録データ作成
//========================================
$summary_naiyo = str_replace("\r\n", "\n", $summary_naiyo);
//$cre_date = $sel_yr.$sel_mon.$sel_day;
if($problem_id == "") $problem_id = "0";


if($summary_naiyo =="" && @$direct_registration){
    require_once("summary_tmpl_util.ini");
    $summary_naiyo = get_tmpl_naiyo_html_default($con, $fname, $xml_file);
}

//========================================
//ＤＢ登録（ログ）
//========================================
//require_once("summary_common.ini");
//summary_common_write_operate_log("modify", $con, $fname, $session, $login_emp_id, "", $pt_id);


//========================================
//ＤＢ登録（一時保存用テーブル）
//========================================

// ワークデータを取得する
$sql_wk = "select * from sum_xml";
$cond_wk = " where xml_file_name = '".$xml_file."'";
$sel_wk = select_from_table($con, $sql_wk, $cond_wk, $fname);
$res_wk = pg_fetch_all($sel_wk);
if ($res_wk) {
    $cnt_wk = count($res_wk);
}
else {
    echo("<script language=\"javascript\">alert(\"ワークデータがありません\");</script>\n");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}







// 既にデータがあるかどうかチェック
$sql_chk = " select emp_id, ptif_id from sum_draft";
$cond_chk = "where emp_id='".pg_escape_string($emp_id)."' and ptif_id='".pg_escape_string($pt_id)."'";
$sel_chk = select_from_table($con, $sql_chk, $cond_chk, $fname);
$res_array = pg_fetch_all($sel_chk);
if (!$res_array) {
    $cnt_chk = 0;
}
else {
    $cnt_chk = count($res_array);
}

if ($sel_chk != 0 && $cnt_chk > 0) {    // データが存在する
    // 更新
    $sql =
        " update sum_draft set".
        " saved_at = '".date("YmdHis")."'".
        ", tmpl_id = ".((int)$tmpl_id ? (int)$tmpl_id : "null").
        ", content = '".$res_wk[0]['smry_xml']."'".
        ", div_id = $my_div_id".
        ", copy_summary_id = ".(int)$_REQUEST["copy_summary_id"].
        " where emp_id = '".pg_escape_string($emp_id)."' and ptif_id='".pg_escape_string($pt_id)."'";
}
else {
    // 新規追加
    $sql =
        " insert into sum_draft (".
        " emp_id, ptif_id, saved_at, tmpl_id, content, div_id, copy_summary_id".
        " ) values (".
        "'".pg_escape_string($emp_id)."'".
        ",'".pg_escape_string($pt_id)."'".
        ",'".date("YmdHis")."'".
        ",".((int)$tmpl_id ? (int)$tmpl_id : "null").
        ",'".$res_wk[0]['smry_xml']."'".
        ",$my_div_id".
        ",".(int)$_REQUEST["copy_summary_id"].
        " )";
}



pg_query($con, "begin");

$ret = update_set_table($con, $sql, array(), null, "", $fname);

if ($ret == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        summary_common_show_error_page_and_die();
}


//========================================
// ＤＢコミット
//========================================
pg_query($con, "commit");
pg_close($con);


//========================================
//処理成功時のJavaScript
//========================================


/// 一時保存読み込みボタンを表示させる。

    echo("<script language=\"javascript\">parent.hidari.location.href=\"./summary_rireki_table.php?session=$session&pt_id=$pt_id&st_date=$cre_date\";</script>\n");
    echo("<script language=\"javascript\">location.href=\"./summary_new.php?session=".$session."&p_sect_id=".$sect_id."&pt_id=".$pt_id."&diag=".urlencode($sel_diag)."\";</script>\n");
//}



?>
