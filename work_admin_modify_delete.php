<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

switch ($wherefrom) {
    case "7":  // 勤務シフト作成の実績入力画面より
    case "8":
        $checkauth = check_authority($session, 69, $fname);
        if ($checkauth == "0") {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }
        break;
    default:
        // 勤務管理権限チェック
        $checkauth = check_authority($session, 42, $fname);
        if ($checkauth == "0") {
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showLoginPage(window);</script>");
            exit;
        }
        break;
}

// データベースに接続
$con = connect2db($fname);


// トランザクションを開始
pg_query($con, "begin");

// 申請情報を取得
$sql = "select * from tmmdapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_status = pg_fetch_result($sel, 0, "apply_status");

// 否認済みでない場合
if ($apply_status != "2") {
	$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
	$target_date = pg_fetch_result($sel, 0, "target_date");

    $b_pattern  = pg_fetch_result($sel, 0, "b_pattern");
    $b_reason   = pg_fetch_result($sel, 0, "b_reason");
    $b_night_duty = pg_fetch_result($sel, 0, "b_night_duty");
    $b_allow_id = pg_fetch_result($sel, 0, "b_allow_id");

	$b_start_time = pg_fetch_result($sel, 0, "b_start_time");
	$b_out_time = pg_fetch_result($sel, 0, "b_out_time");
	$b_ret_time = pg_fetch_result($sel, 0, "b_ret_time");
	$b_end_time = pg_fetch_result($sel, 0, "b_end_time");
	$a_end_time = pg_fetch_result($sel, 0, "a_end_time");
	for ($i = 1; $i <= 10; $i++) {
		$b_start_time_ver = "b_o_start_time$i";
		$b_end_time_ver = "b_o_end_time$i";
		$$b_start_time_ver = pg_fetch_result($sel, 0, "$b_start_time_ver");
		$$b_end_time_ver = pg_fetch_result($sel, 0, "$b_end_time_ver");
		$a_start_time_ver = "a_o_start_time$i";
		$$a_start_time_ver = pg_fetch_result($sel, 0, "$a_start_time_ver");
	}

	$b_tmcd_group_id  = pg_fetch_result($sel, 0, "b_tmcd_group_id");
	$b_meeting_time  = pg_fetch_result($sel, 0, "b_meeting_time");
	$b_previous_day_flag  = pg_fetch_result($sel, 0, "b_previous_day_flag");
	$b_next_day_flag  = pg_fetch_result($sel, 0, "b_next_day_flag");

	$b_meeting_start_time = pg_fetch_result($sel, 0, "b_meeting_start_time");
	$b_meeting_end_time = pg_fetch_result($sel, 0, "b_meeting_end_time");
	
	//前日フラグが未設定、出勤時刻が入力されていない場合、前日フラグを初期化(0にする)
	if ($b_start_time == "" || $b_start_time == null || $b_previous_day_flag == "" || $b_previous_day_flag == null){
		$b_previous_day_flag = 0;
	}

	//翌日フラグが未設定、退勤時刻が入力されていない場合、前日フラグを初期化(0にする)
	if ($b_end_time == "" || $b_end_time == null || $b_next_day_flag == "" || $b_next_day_flag == null){
		$b_next_day_flag = 0;
	}

	// 勤務実績を申請前の状態に戻す
	$sql = "update atdbkrslt set";
	$set = array("pattern", "reason", "night_duty", "allow_id", "start_time", "out_time", "ret_time", "end_time", "o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3", "o_start_time4", "o_end_time4", "o_start_time5", "o_end_time5", "o_start_time6", "o_end_time6", "o_start_time7", "o_end_time7", "o_start_time8", "o_end_time8", "o_start_time9", "o_end_time9", "o_start_time10", "o_end_time10", "tmcd_group_id", "meeting_time", "previous_day_flag", "next_day_flag", "meeting_start_time", "meeting_end_time");
	$setvalue = array($b_pattern, $b_reason, $b_night_duty, $b_allow_id, $b_start_time, $b_out_time, $b_ret_time, $b_end_time, $b_o_start_time1, $b_o_end_time1, $b_o_start_time2, $b_o_end_time2, $b_o_start_time3, $b_o_end_time3, $b_o_start_time4, $b_o_end_time4, $b_o_start_time5, $b_o_end_time5, $b_o_start_time6, $b_o_end_time6, $b_o_start_time7, $b_o_end_time7, $b_o_start_time8, $b_o_end_time8, $b_o_start_time9, $b_o_end_time9, $b_o_start_time10, $b_o_end_time10, $b_tmcd_group_id, $b_meeting_time, $b_previous_day_flag, $b_next_day_flag, $b_meeting_start_time, $b_meeting_end_time);
	$cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 退勤時刻が変わる場合は同日の残業申請を削除
	if ($b_end_time != $a_end_time) {

		$sql = "select apply_id from ovtmapply ";
		$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
            $ovtm_apply_id = $row["apply_id"];
	
	        // 残業申請承認論理削除
			$sql = "update ovtmaprv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $ovtm_apply_id";
	
		    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
	
	        // 残業申請論理削除
			$sql = "update ovtmapply set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $ovtm_apply_id";
		    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
	
			// 承認候補を論理削除
			$sql = "update ovtmaprvemp set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $ovtm_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			// 同期・非同期情報を論理削除
			$sql = "update ovtm_async_recv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $ovtm_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
	    }
	}

	// 退勤後復帰時刻が変わる場合は同日の退勤後復帰申請を削除
	if ($b_o_start_time1 != $a_o_start_time1 || $b_o_start_time2 != $a_o_start_time2 || $b_o_start_time3 != $a_o_start_time3 || $b_o_start_time4 != $a_o_start_time4 || $b_o_start_time5 != $a_o_start_time5 || $b_o_start_time6 != $a_o_start_time6 || $b_o_start_time7 != $a_o_start_time7 || $b_o_start_time8 != $a_o_start_time8 || $b_o_start_time9 != $a_o_start_time9 || $b_o_start_time10 != $a_o_start_time10) {

		$sql = "select apply_id from rtnapply ";
		$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
		    $rtn_apply_id = $row["apply_id"];
		
			// 退勤後復帰申請承認論理削除
			$sql = "update rtnaprv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			// 退勤後復帰申請論理削除
			$sql = "update rtnapply set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			// 承認候補を論理削除
			$sql = "update rtnaprvemp set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			// 同期・非同期情報を論理削除
			$sql = "update rtn_async_recv set";
			$set = array("delete_flg");
			$setvalue = array("t");
			$cond = "where apply_id = $rtn_apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
}



$sql = "select apply_id from tmmdapply ";
$cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


while($row = pg_fetch_array($sel))
{
    $tmmd_apply_id = $row["apply_id"];

	// 勤務時間修正申請承認論理削除
	$sql = "update tmmdaprv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $tmmd_apply_id";
	
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 勤務時間修正申請論理削除
	$sql = "update tmmdapply set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $tmmd_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 承認候補を論理削除
	$sql = "update tmmdaprvemp set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $tmmd_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	// 同期・非同期情報を論理削除
	$sql = "update tmmd_async_recv set";
	$set = array("delete_flg");
	$setvalue = array("t");
	$cond = "where apply_id = $tmmd_apply_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリフレッシュして自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");

?>