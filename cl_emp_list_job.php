<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

//======================================
// セッションのチェック
//======================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	showLoginPage();
	exit;
}

//======================================
// データベースに接続
//======================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);
// 呼び元画面が「病棟評価表」画面の場合
if($call_screen == "WARD_GRADE")
{
    $arr_empmst = $obj->get_empmst($session);
    $login_emp_id = $arr_empmst[0]["emp_id"];

    $employee_div = "";

    // 該当職員が「評価者」どうか判断する
    if($obj->is_grader($login_emp_id))
    {
        $employee_div = "GRADER";
    }
    // 該当職員が「師長」どうか判断する
    if($obj->is_nursing_commander($login_emp_id))
    {
        $employee_div = "NURSING_COMMANDER";
    }
    // 該当職員が「特定職員」どうか判断する
    if($obj->is_particular_employee($login_emp_id))
    {
        $employee_div = "PARTICULAR_EMPLOYEE";
    }
}


//======================================
// 職種情報取得
//======================================
$sql = "select site_id from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
// サイトIDを取得
$site_id = pg_fetch_result($sel, 0, "site_id");

if ($site_id == "") {
	$mail_column = "login.emp_login_id";
} else {
	$mail_column = "login.emp_login_mail || '_$site_id'";
}

$employees = array();
$sql = "select job_id, job_nm from jobmst ";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row_job = pg_fetch_array($sel_job)) {
	$jobs[$row_job["job_id"]] = array("name" => $row_job["job_nm"]);
// 選択されたアイコンのIDと同じならば以下の情報を読み込む
	if ($selected_id != $row_job["job_id"]) {
		continue;
	}
    $sql = " select varchar(1) '1', empmst.emp_id, empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as name, $mail_column as email from login inner join empmst on empmst.emp_id = login.emp_id where exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f' $approve_cond) and empmst.emp_job = " . $row_job['job_id'] ;
    $sql .= $obj->get_sql_for_ward_grade($employee_div, $login_emp_id);
    $sql .= " order by emp_kn_lt_nm, emp_kn_ft_nm, 1";
    $cond = "";
    $sel_emp = select_from_table($con, $sql, $cond, $fname);

    if ($sel_emp == 0) {
      pg_close($con);
      echo("<script type='text/javascript' src='./js/showpage.js'></script>");
      echo("<script language='javascript'>showErrorPage(window);</script>");
      exit;
    }
    while ($row_emp = pg_fetch_array($sel_emp)) {
      $tmp_arr = array();
      $tmp_arr['name'] = $row_emp['name'];
      $tmp_arr['email'] = $row_emp['email'];
      $tmp_arr['emp_id'] = $row_emp[1];

      $employees[$row_job['job_id']][] = $tmp_arr;
    }
}

$cl_title = cl_title_name();

//==============================
// HTML
//==============================
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?> | 職員検索</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

function changeDisplay(class_id) {
	document.mainform.selected_id.value = class_id;
	var img = document.getElementById('class'.concat(class_id));
	if (img.className == 'close') {
		img.className = 'open';
		row_display = '';
	} else {
		img.className = 'close';
		row_display = 'none';
	}

	var rows = document.getElementsByTagName('tr');
	var row_class = 'children_of_'.concat(class_id);
	for (var i = 0, j = rows.length; i < j; i++) {
		if (rows[i].className == '') {
			continue;
		}

		if (rows[i].className == row_class) {
			rows[i].style.display = row_display;
			continue;
		}

		if (rows[i].className.indexOf(row_class.concat('-')) >= 0) {
			if (img.className == 'close') {
				rows[i].style.display = row_display;
				continue;
			}

			if (img.className == 'open') {
				var parent_img_id = 'class'.concat(rows[i].className.replace('children_of_', ''));
				var parent_img = document.getElementById(parent_img_id);
				if (parent_img.className == 'close') {
					rows[i].style.display = 'none';
					continue;
				} else if (parent_img.className == 'open') {
					rows[i].style.display = '';
					continue;
				}
			}
		}
	}
}

function readEmpData(class_id) {
	document.mainform.mode.value = 'disp';
	document.mainform.selected_id.value = class_id;
	document.mainform.action = "cl_emp_list_job.php?session=<?=$session?>&call_screen=<?=$call_screen?>";
	document.mainform.submit();
}

function copyEmpData(data_id, col_name)
{
	document.mainform.mode.value = 'copy';
	document.mainform.data_id.value = data_id;
	document.mainform.col_name.value = col_name;
	document.mainform.action = "cl_emp_list_job.php?session=<?=$session?>&call_screen=<?=$call_screen?>";
	document.mainform.submit();
}

// コピー処理
function copy_emp(emp_id, emp_nm)
{
    if(window.opener && !window.opener.closed && window.opener.add_target_list)
    {
		if (emp_id == "")
		{
		    alert('職員が存在しません');
			return;
		}

	    set_wm_counter(emp_id);
        window.opener.add_target_list(' <?=$input_div?>',emp_id, emp_nm);
    }
}

// ajax 「よく使う人」カウントアップ処理
function set_wm_counter(ids)
{
	
	var url = 'cl_wm_counter.php';
	var params = $H({'session':'<?=$session?>','used_emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url, 
		{
			method: 'post',
			postBody: params
		});
}

<?
// コピーボタン押下時、対象を追加する関数を呼出す。
if ($mode == "copy") {
	$emp_info = get_post_emp_ids($con, $data_id, $col_name, $fname, $site_id, $employee_div, $login_emp_id, $obj);
?>
function copy_target() {	

	copy_emp('<?=$emp_info["emp_id"]?>', '<?=$emp_info["emp_name"]?>');

<?
// 最後にクリックされたアイコンの位置を展開する。
	if ($selected_id != "") {
		echo ("changeDisplay('$selected_id');");
	}
?>

}
<?
}
?>

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

img.close {
	background-image:url("img/icon/plus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("img/icon/minus.gif");
	vertical-align:middle;
}

// -->

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"

<?
// 展開用アイコン押下時
if ($mode == "disp") {
	echo (" onload=\"changeDisplay('$selected_id');\"");
}

// コピーボタン押下時
if ($mode == "copy") {
	echo (" onload=\"copy_target();\"");
}
?>
>

<form name="mainform" action="#" method="post">
<input type="hidden" name="selected_id" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="data_id" value="">
<input type="hidden" name="col_name" value="">
<input type="hidden" name="input_div" value="<?=$input_div?>">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#35B341">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員検索</b></font></td>
<td>&nbsp</td>
<td width="10">&nbsp</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" width="10" height="10" alt=""><br>

<?
// タブ表示
show_emp_list_header_tab($session, $fname, $input_div, $call_screen);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td>

<!-- ここから -->

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="26" bgcolor="#DFFFDC">
<td width="100">&nbsp;</td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font></td>
</tr>
<? // 職種
foreach ($jobs as $tmp_job_id => $tmp_jobs) {
?>
<tr height="26">
<td align="center">
<input type=button value="コピー" onclick="copyEmpData('<? echo ($tmp_job_id);?>', 'emp_job');">
</td>
<?
if ($selected_id != $tmp_job_id) {
	$func_name = "readEmpData";
} else {
	$func_name = "changeDisplay";
}
?>
<td style="padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<img id="class<? echo($tmp_job_id); ?>" src="img/spacer.gif" alt="" width="20" height="20" class="close" style="margin-right:3px;" onclick="<? echo("$func_name") ?>('<? echo($tmp_job_id); ?>');"><? echo($tmp_jobs["name"]); ?>
</font>
</td>
</tr>
<? // 職種ごとの職員
foreach ($employees[$tmp_job_id] as $employee ) {
?>
<tr class="children_of_<? echo("{$tmp_job_id}"); ?>" height="26" style="display:none;">
<td align="center">
<input type=button value="コピー" onclick="copy_emp('<? echo $employee['emp_id']; ?>', '<? echo $employee['name']; ?>');">
</td><td style="padding-left:50px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? print $employee['name'] . " &lt;$employee[email]&gt;"; ?></font></td>
</tr>
<?
}
?>

<?
}
?>
</table>


<!-- ここまで -->


</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>

</form>	
</body>
</html>




<?
//-------------------------------------------
// 職種ごとに職員を一括取得する。
//-------------------------------------------
function get_post_emp_ids($con, $mst_id, $col_name, $fname, $site_id, $employee_div, $login_emp_id, $obj){


    if ($site_id == "") {
    	$mail_column = "login.emp_login_id";
  	} else {
    	$mail_column = "login.emp_login_mail || '_$site_id'";
  	}

 	$sql = "select empmst.emp_id, empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as name, $mail_column as email from login inner join empmst on empmst.emp_id = login.emp_id where exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f' $approve_cond) and empmst.$col_name = " . $mst_id;
    $sql .= $obj->get_sql_for_ward_grade($employee_div, $login_emp_id);
 	$cond = "order by emp_kn_lt_nm, emp_kn_ft_nm";

	 $sel = select_from_table($con, $sql, $cond, $fname);
  	if ($sel == 0) {
    	pg_close($con);
    	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    	exit;
  	}

  	$ids = "";
	$names = "";

  	while($row = pg_fetch_array($sel))
  	{
		if($ids != "")
		{
			$ids   .= ",";
			$names .= ",";
		}
		$ids   .= $row["emp_id"];
		$names .= $row["name"];
  	}

	$arr = array("emp_id" => $ids, "emp_name" => $names);

  	return $arr;
}

//==============================
//データベース接続を閉じる
//==============================
pg_close($con);
?>
