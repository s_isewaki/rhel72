<?
/*
 * 呼出元：報告書登録画面
 * 概要：postされた日付を元に、月、曜日、曜日区分を取得してjosonデータとして返す
 */
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("about_comedix.php");
require_once("holiday.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// インシデント権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

//------------------------------------------------------------------------------
//与えられた日付から月、曜日、曜日区分を取得して返す
//------------------------------------------------------------------------------
//月
$time = strtotime($ymd);
$m = date("n", $time);

//曜日
$w = date("w", $time);
if ($w == 0){
    $w = 7;
}

//曜日区分
$date = str_replace('/', '', $ymd);
$sql = "SELECT type FROM calendar WHERE date = '{$date}'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$result = pg_fetch_array($sel);
if($result['type'] > 0 && $result['type'] <= 3) {
    $w_type = 1;
} else if($result['type'] >= 4) {
    $w_type = 2;
} else {
    $w_type = "";
}

//DB切断
pg_close($con);

//取得した値を返す
echo cmx_json_encode(
    array(
        'month' => $m,
        'weekday' => $w,
        'weekday_type' => $w_type
    )
);
