<?php
require_once("about_comedix.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $_SERVER['PHP_SELF'];

///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if ($con == "0") {
    js_login_exit();
}

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
    pg_close($con);
    js_login_exit();
}
//ユーザ画面用
$section_user_auth = $obj->check_authority_user($session, $fname);

///-----------------------------------------------------------------------------
// 勤務パターン
///-----------------------------------------------------------------------------
$pattern_id = $_REQUEST['pattern_id'];
$pgroup_lists = $obj->get_wktmgrp_array();
if (empty($pgroup_lists)) {
    $err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
}
if (empty($pattern_id)) {
    $pattern_id = $pgroup_lists[0]["id"];
}

///-----------------------------------------------------------------------------
// 職種
///-----------------------------------------------------------------------------
$sql_job = "SELECT *, job_id as id, job_nm as name FROM jobmst WHERE NOT job_del_flg ORDER BY order_no, job_id";
$sel_job = select_from_table($con, $sql_job, "", $fname);
if ($sel_job == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_job = array();
while ($row = pg_fetch_assoc($sel_job)) {
    $emp_job[] = $row;
}

///-----------------------------------------------------------------------------
// 勤務シフトパターン
// Cmx/Model/Shift/Pattern.php CMX_Shift_Pattern->lists()
///-----------------------------------------------------------------------------
$pg_pattern_id = p($pattern_id);
$sql_ptn = "
    SELECT
        *,
        a.atdptn_id as atdptn_id,
        a.reason as atd_reason,
        r.reason_id as ptn_reason,
        a.display_flag as a_display_flag,
        r.display_flag as r_display_flag
    FROM atdptn a
        LEFT JOIN atdbk_reason_mst r ON (a.atdptn_id=10 AND r.display_flag)
        LEFT JOIN duty_shift_pattern d ON (d.pattern_id=a.group_id AND d.atdptn_ptn_id=a.atdptn_id AND (d.reason=r.reason_id OR r.reason_id IS NULL))
    WHERE a.group_id= {$pg_pattern_id}
    ORDER BY d.order_no, a.atdptn_order_no, r.sequence
";
$sel_ptn = select_from_table($con, $sql_ptn, "", $fname);
if ($sel_ptn == 0) {
    pg_close($con);
    js_error_exit();
}
$pattern_list = array();
$pattern_name = array();
$pattern_1034 = true;
while ($row = pg_fetch_assoc($sel_ptn)) {
    $row['ptn_id'] = sprintf("%02d%02d", $row["atdptn_id"], $row["ptn_reason"]);

    if ($row["atdptn_id"] !== "10") {
        $row['ptn_name'] = $row["atdptn_nm"];
    }
    else if (!empty($row["display_name"])) {
        $row['ptn_name'] = '(休暇)' . $row["display_name"];
    }
    else {
        $row['ptn_name'] = '(休暇)' . $row["default_name"];
    }

    $pattern_name[$row['ptn_id']] = $row['ptn_name'];

    if ($row["atdptn_id"] !== "10") {
        if ($row['a_display_flag'] === 't') {
            $pattern_list[] = $row;
        }
    }
    else {
        $pattern_list[] = $row;
        if ($row['a_display_flag'] !== 't') {
            $pattern_1034 = false;
        }
    }
}
// 1034がatdbk_reason_mstに入っていないので
// わざわざ別に登録する必要がある。
if ($pattern_1034) {
    $pattern_list[] = array(
        'ptn_id'              => '1034',
        'ptn_name'            => '(休暇)',
        'atdptn_id'           => '10',
        'atd_reason'          => '34',
        'ptn_reason'          => '34',
    );
    $pattern_name['1034'] = '(休暇)';
}

///-----------------------------------------------------------------------------
// 「集計変更を有効にする」スイッチを取得
///-----------------------------------------------------------------------------
$sql_sw = "SELECT sum_change_sw FROM duty_shift_entity_sum_change_sw WHERE group_id = {$pg_pattern_id}";
$sel_sw = select_from_table($con, $sql_sw, "", $fname);
if ($sel_sw == 0) {
    pg_close($con);
    js_error_exit();
}
$change_switch = pg_fetch_result($sel_sw, 0, "sum_change_sw");

///-----------------------------------------------------------------------------
// 勤務表集計: 集計値
// Cmx/Model/Shift/PatternCount.php Cmx_Shift_PatternCount->values()
///-----------------------------------------------------------------------------
$sql_val = "SELECT * FROM duty_shift_sign_count_value WHERE group_id = {$pg_pattern_id}";
$sel_val = select_from_table($con, $sql_val, "", $fname);
if ($sel_val == 0) {
    pg_close($con);
    js_error_exit();
}
$count_value = array();
while ($row = pg_fetch_assoc($sel_val)) {
    $count_value[$row['pattern']][$row['count_id']] = $row['count_value'];
}

///-----------------------------------------------------------------------------
// 勤務表集計: 集計名リスト
// Cmx/Model/Shift/PatternCount.php Cmx_Shift_PatternCount->name_lists()
///-----------------------------------------------------------------------------
$sql_nam = "SELECT * FROM duty_shift_sign_count_name WHERE group_id = {$pg_pattern_id} ORDER BY order_no, count_id";
$sel_nam = select_from_table($con, $sql_nam, "", $fname);
if ($sel_nam == 0) {
    pg_close($con);
    js_error_exit();
}
$count_name_list = array();
while ($row = pg_fetch_assoc($sel_nam)) {
    if (!empty($row['job_filter'])) {
        $row['jobs'] = array_keys(unserialize($row['job_filter']));
    }
    else {
        $row['jobs'] = array();
    }
    $count_name_list[] = $row;
}

///-----------------------------------------------------------------------------
// 勤務表集計: 集計パターンリスト
// Cmx/Model/Shift/PatternCount.php Cmx_Shift_PatternCount->pattern_lists()
///-----------------------------------------------------------------------------
$sql_cpt = "SELECT DISTINCT pattern FROM duty_shift_sign_count_value WHERE group_id = {$pg_pattern_id} ORDER BY pattern";
$sel_cpt = select_from_table($con, $sql_cpt, "", $fname);
if ($sel_cpt == 0) {
    pg_close($con);
    js_error_exit();
}
$count_pattern_list = array();
while ($row = pg_fetch_assoc($sel_cpt)) {
    $row['name'] = $pattern_name[$row['pattern']];
    $count_pattern_list[] = $row;
}

///-----------------------------------------------------------------------------
// DB閉じる
///-----------------------------------------------------------------------------
pg_close($con);

///-----------------------------------------------------------------------------
// job_pulldown
///-----------------------------------------------------------------------------
function job_pulldown($id = null) {
    global $emp_job;
    $output = '';
    foreach ($emp_job as $row) {
        $selected = "";
        if (!empty($id) and $row['id'] == $id) {
            $selected = 'selected';
        }
        $output.= sprintf('<option value="%s" %s>%s</option>', h($row['id']), $selected, h($row['name']));
    }
    return $output;
}

///-----------------------------------------------------------------------------
// pattern_pulldown
///-----------------------------------------------------------------------------
function pattern_pulldown($ptn_id = null) {
    global $pattern_list;
    $output = '';

    foreach ($pattern_list as $row) {
        $selected = "";
        if (!empty($ptn_id) and $row['ptn_id'] === $ptn_id) {
            $selected = 'selected';
        }
        $output.= sprintf('<option value="%s" %s>%s</option>', h($row['ptn_id']), $selected, h($row['ptn_name']));
    }
    return $output;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix 勤務表集計設定</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/shift/entity_sign_count.css">
        <script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript" src="js/duty_shift/entity_sign_count.js"></script>
        <script type="text/javascript">
            var pulldown_job = '<?= job_pulldown(); ?>';
            var cols = [];
<? foreach ($count_name_list as $row) { ?>
            cols.push('<?= h($row['count_id']) ?>');
<? } ?>
            var rows = [];
<? foreach ($count_pattern_list as $ptn) { ?>
            rows.push('<?= h($ptn['pattern']) ?>');
<? } ?>
        </script>
    </head>

    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"><? show_manage_title($session, $section_user_auth);?></table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0"><? show_manage_tab_menuitem($session, $fname, $arr_option);?></table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr></table><img src="img/spacer.gif" width="1" height="5" alt=""><br>

<?
if (!empty($err_msg_1)) {
    echo($err_msg_1);
} else {
?>
        <div style="padding:5px; font-size:14px;">
            [<a href="?session=<?=h($session)?>">集計設定A</a>]
            &nbsp;&nbsp;
            [<a href="duty_shift_entity_sum_unit.php?session=<?=h($session)?>">集計設定B</a>]
        </div>
        <hr>

        <div class="pulldown">
            <form name="sign" action="duty_shift_entity_sign_count.php" method="post">
                <input type="hidden" name="session" value="<?= h($session) ?>">
                <b>勤務パターングループ</b>:
                <select name="pattern_id" onchange="this.form.submit();">
<? foreach ($pgroup_lists as $row) { ?>
                        <option value="<?= h($row['id']) ?>" <? if ($row['id'] == $pattern_id) echo 'selected'; ?>><?= h($row['name']) ?></option>
<? } ?>
                </select>
            </form>
        </div>
        <div class="clear"></div>


        <form id="signform" action="duty_shift_entity_sign_count_update_exe.php" method="post">
            <input type="hidden" name="session" value="<?= h($session) ?>">
            <input type="hidden" name="pattern_id" value="<?= h($pattern_id) ?>" />
            <input type="hidden" name="mode" value="confirm" />

            <div class="change_switch">
                <label><input type="checkbox" name="change_switch" value="ON" <? if ($change_switch === "2") echo 'checked'; ?>>集計設定Aを有効にする</label>
            </div>

            <div>
                <button type="submit">登録する</button>
            </div>

            <table class="list" id="count">
                <thead>
                    <tr>
                        <td>集計名&gt;&gt;</td>
<? foreach ($count_name_list as $row) { ?>
                            <td><input type="text" name="name[<?= h($row['count_id']) ?>]" value="<?= h($row['count_name']) ?>" size="10" class="jpn" /></td>
<? } ?>
                        <td class="ptn"><button type="button" onclick="add_col();">列の追加</button></td>
                    </tr>
                </thead>

                <tbody>
                    <tr id="count_row">
                        <td class="center">行集計 (職員別)</td>
<? foreach ($count_name_list as $row) { ?>
                            <td class="center"><input type="checkbox" name="count_row[<?= h($row['count_id']) ?>]" value="1" <? if ($row['count_row_flg'] === 't') echo 'checked="checked"' ?> /></td>
<? } ?>
                        <td class="ptn">-</td>
                    </tr>
                    <tr id="count_col">
                        <td class="center">列集計 (日別)</td>
<? foreach ($count_name_list as $row) { ?>
                            <td class="center"><input type="checkbox" name="count_col[<?= h($row['count_id']) ?>]" value="1" <? if ($row['count_col_flg'] === 't') echo 'checked="checked"' ?> /></td>
<? } ?>
                        <td class="ptn">-</td>
                    </tr>
                    <tr id="count_job">
                        <td class="center">集計対象職種<br />(列集計のみ有効)</td>
<? foreach ($count_name_list as $row) { ?>
                            <td class="center">
<? foreach ($row['jobs'] as $job) { ?>
                                    <div>
                                        <select class="job" name="count_job[<?= h($row['count_id']) ?>][]">
                                            <option value="">-</option>
<?= job_pulldown($job); ?>
                                        </select>
                                        <a href="javascript:void(0);" onclick="remove_job(this);">[X]</a>
                                    </div>
<? } ?>
                                <div>
                                    <select class="job" name="count_job[<?= h($row['count_id']) ?>][]">
                                        <option value="">-</option>
<?= job_pulldown(); ?>
                                    </select>
                                    <a href="javascript:void(0);" onclick="remove_job(this);">[X]</a>
                                </div>
                                <button type="button" onclick="add_job(this, '<?= h($row['count_id']) ?>');">職種追加</button>
                            </td>
<? } ?>
                        <td class="ptn">-</td>
                    </tr>
<? foreach ($count_pattern_list as $ptn) { ?>
                        <tr id="<?= h($ptn['pattern']) ?>" class="count">
                            <td class="center"><?= h($ptn['name']) ?></td>
<? foreach ($count_name_list as $row) { ?>
                                <td class="input center"><input class="num" type="text" name="value[p<?= h($ptn['pattern']) ?>][<?= h($row['count_id']) ?>]" value="<?= h($count_value[$ptn['pattern']][$row['count_id']]); ?>" size="6" /></td>
<? } ?>
                            <td class="ptn"><button type="button" onclick="remove_row(this)">&lt;&lt;削除</button></td>
                        </tr>
<? } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td class="center">
                            <select id="row_pattern"><?= pattern_pulldown(); ?></select><br />
                            <button type="button" onclick="add_row();">行の追加</button>
                        </td>
<? foreach ($count_name_list as $row) { ?>
                            <td class="center"><button type="button" onclick="remove_col(this)">列の削除</button></td>
<? } ?>
                        <td class="ptn">-</td>
                    </tr>
                </tfoot>
            </table>

            <div>
                <button type="submit">登録する</button>

            </div>

        </form>
<? } ?>
    </body>
</html>