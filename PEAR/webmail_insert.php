<meta http-equiv="Content-Type" content="text/html; charset=shift_JIS">
<? require("about_postgres.php"); ?>
<?// require("about_session.php"); ?>
<?// require("about_authority.php"); ?>
<?
$fname = $PHP_SELF;
/*
$session = qualify_session($session,$fname);
if($session == "0"){
echo("<script language=\"javascript\">location.href=\"./login.php\";</script>\n");
exit;
}

$ward = check_authority($session,0,$fname);
if($ward == "0"){
echo("<script language=\"javascript\">location.href=\"./login.php\";</script>\n");
exit;
}
*/?>
<?
$pop3=trim($pop3);
$smtp=trim($smtp);
$account_name=trim($account_name);
$password=trim($password);
$port_smtp=trim($port_smtp);
$port_pop3=trim($port_pop3);

if(!$pop3){
echo("<script language='javascript'>alert(\"受信メール（POP3）を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}
if(strlen($pop3)>50){
echo("<script language='javascript'>alert(\"受信メール（POP3）を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(!$webmail_title){
echo("<script language='javascript'>alert(\"件名を入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($webmail_title)>50){
echo("<script language='javascript'>alert(\"件名を25文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

if(strlen($webmail) > 500){
echo("<script language='javascript'>alert(\"本文を250文字以内で入力してください。\");</script>");
echo("<script language='javascript'>history.back();</script>");
exit;
}

$con=connect2db($fname);
if($con=="0"){
echo("<script language='javascript'>location.href=\"./error.php\";</script>");
exit;
}

$select_id="select emp_id from login where emp_id in(select emp_id from session where session_id='$session')";
//echo($select_id);
$result_select=pg_exec($con, $select_id);
if($result_select==false){
echo("<script language='javascript'>location.replace('./error.php');</script>");
exit;
}
$emp_id=pg_result($result_select,0,"emp_id");

$select_name="select emp_lt_nm,emp_ft_nm from empmst where emp_id='$emp_id'";
$result_select_name=pg_exec($con,$select_name);
if($result_select_name==false){
echo("<script language='javascript'>location.replace('./error.php');</script>");
exit;
}

$date=date(YmdHi);

$select_max="select max(webmail_id) from webmail";
$result_max=pg_exec($con,$select_max);
$count=pg_numrows($result_max);
if($count>0){
$webmail_id=pg_result($result_max,0,"max");
$webmail_id=$webmail_id+1;
}else{
$webmail_id = "1";
}

begin;

$insert_webmail="insert into webmail(emp_id,webmail_id,date,webmail_towhich_email,webmail_towhich_id,webmail_title,webmail,webmail_status)values('$emp_id','$webmail_id','$date','$webmail_towhich_email','$webmail_towhich_id','$webmail_title','$webmail','0')";
$result_insert=pg_exec($con,$insert_webmail);
if($result_insert==false){
rollback;
echo("<script language='javascript'>location.href=\"./error.php\";</script>");
exit;
}

if (phpversion()>"4.1.0"){ 
extract($_POST); 
extract($_SERVER); 
$upfile_a=$_FILES["upfile"]["tmp_name"]; 
$upfile_a_name=$_FILES["upfile"]["name"]; 
$upfile_a_type=$_FILES["upfile"]["type"]; 
$upfile_b=$_FILES["upfile"]["tmp_name"]; 
$upfile_b_name=$_FILES["upfile"]["name"]; 
$upfile_b_type=$_FILES["upfile"]["type"]; 
}

function attachfile($upf,$upf_name,$filetype){
global $tmp,$body,$boundary;
$fp=fopen($tmp.$upf_name,"r") or die("error");
$content=fread($fp,filesize($tmp.$upf_name));
fclose($fp); 
$fencoded=chunk_split(base64_encode($content));
$body.="\n\n--$boundary\n";
$body.="Content-Type: ".$filetype.";\n"; 
if(ereg("^[[:alnum:]@!#\._-]+$",$upf_name)){
$f_name=$upf_name; 
}else{ 
$f_name=mime_head($upf_name);
} 
$body.="\tname=\"$f_name\"\n"; 
$body.="Content-Transfer-Encoding: base64\n"; 
$body.="Content-Disposition: attachment;\n"; 
$body.="\tfilename=\"$f_name\"\n\n"; 
$body.="$fencoded\n"; 

//unlink($tmp.$upf_name);

return $body;
}

function sjis2jis($sjis){   
$jis=''; 
$ascii=true; 
$b=unpack("C*",$sjis); 
for ($i=1;$i<=count($b);$i++){ 
if($b[$i] >= 0x80){ 
if ($ascii) { 
$ascii = false; 
$jis .= chr(0x1B).'$B'; 
} 
$b[$i] <<= 1; 
if ($b[$i+1] < 0x9F) { 
$b[$i]   -= ($b[$i] < 0x13F) ? 0xE1 : 0x61; 
$b[$i+1] -= ($b[$i+1] > 0x7E) ? 0x20 : 0x1F; 
} else { 
$b[$i] -= ($b[$i] < 0x13F) ? 0xE0 : 0x60; 
$b[$i+1] -= 0x7E; 
} 
$b[$i] = $b[$i] & 0xff; 
$jis .= pack("CC", $b[$i], $b[$i+1]); 
$i++; 
} else { 
if (!$ascii) { 
$ascii = true; 
$jis .= chr(0x1B).'(B'; 
} 
$jis .= pack("C", $b[$i]); 
} 
} 
if (!$ascii) $jis .= chr(0x1B).'(B'; 

return $jis; 
} 

function mime_head($usr_str){
  if(get_magic_quotes_gpc()) $usr_str = stripslashes($usr_str);
  $enc = sjis2jis($usr_str);
  return "=?iso-2022-jp?B?" . base64_encode($enc) . "?=";
}

$boundary = md5(uniqid(rand()));
$tmp = "./data/";
$myhost = "medi-system-vps.com"; 
$logfile = "mailog.txt";
$maxbyte = "200000"; 

$webmail=i18n_convert($webmail,"SJIS");
$webmail_title=i18n_convert($webmail_title,"SJIS");
$subject = '=?ISO-2022-JP?B?'.base64_encode($webmail_title).'?=';

$year=substr($date,0,4);
$month=substr($date,4,2);
$day=substr($date,6,2);
$hour=substr($date,8,2);
$minute=substr($date,10,2);
$date=$year."/".$month."/".$day."　".$hour."：".$minute;

$body="$webmail
[送信日時] $date
";
$filename='webmail.txt';
if(is_writable($filename)) {
if(!$handle = fopen($filename, 'a')) {
pg_exec($con,"rollback");
echo("ファイルを開けません。");
exit;
}
if(!fwrite($handle, $body)) {
pg_exec($con,"rollback");
echo("ファイルに書き込めません。");
exit;
}
$fp = popen("echo $body | qkc -j webmail.txt","r");
pclose($fp);
fclose($handle);
}else{
pg_exec($con,"rollback");
echo("ファイルに書き込めません。");
}

$body = ""; 
$headers  = "From: $emp_id\n";
$headers .= "Reply-To: $webmail_towhich_email\n";
if($cc) $headers .= "Cc: $cc\n";
if($bcc) $heady .= "Bcc: $bcc\n";
$headers .= "X-Mailer: PHP/".phpversion()."\n"; 
$headers .= "MIME-version: 1.0\n";
if(($usrfile_a!="")||($usrfile_b!="")){ 
	$headers .= "Content-Type: multipart/mixed;\n"; 
   $headers .= "\tboundary=\"$boundary\"\n"; 
	$body .= "This is a multi-part message in MIME format.\n\n"; 
	$body .= "--$boundary\n"; 
	$body .= "Content-Type: text/plain; charset=ISO-2022-JP\n"; 
	$body .= "Content-Transfer-Encoding: 7bit\n\n"; 
   $flag = true; 
}else{ 
   $headers .= "Content-Type: text/plain; charset=ISO-2022-JP\n"; 
   $headers .= "Content-Transfer-Encoding: 7bit\n"; 
} 

if($usrfile_a!="") attachfile($usrfile_a,$usrfile_a_name,$usrfile_a_type); 
if($usrfile_b!="") attachfile($usrfile_b,$usrfile_b_name,$usrfile_b_type); 
if($flag) $body .= "--$boundary--"; 

if(mail($to, $subject, $body, $heady)){ 
	echo("<script language='javascript'>alert('送信しました。');</script>");
	echo("<script language='javascript'>self.close();</script>");
	echo("<script language='javascript'>window.opener.location.reload();</script>");
	echo("<script language='javascript'>location.replace('./webmail_left.php?session=$session');</script>");
}else{ 
	echo("<script language='javascript'>alert('メール送信に失敗しました。');</script>");
	echo("<script language='javascript'>location.replace('./webmail_register.php?session=$session');</script>");
} 
exit; 
/*
$sender = "自動配信メール";
$sender = '=?ISO-2022-JP?B?'.base64_encode($sender).'?=';

$headers  = "X-Mailer: PHP/".phpversion()."\n"; 
$headers  = "Mime-Version: 1.0\r\n";
if($usrfile_a!=""||$usrfile_b!=""){ 
$headers .= "Content-Type: multipart/mixed;\n";
$headers .= "\tboundary=\"$boundary\"\n";
}else{
$headers .= "Content-Type: text/plain; charset=iso-2022-jp\r\n";
$headers .= "Content-Transfer-Encoding: 7bit\r\n";
}
$headers .= "From: $sender <info@opus-medi.co.jp>\r\n";
$headers="Reply-To: $webmail_towhich_email\n";
if($cc)$headers.="Cc: $cc\r\n";
if($bcc)$headers.="Bcc: $bcc\r\n";

if($usrfile_a!=""||$usrfile_b!=""){ 
$content.="This is a multi-part message in MIME format.\n\n"; 
$content.="--$boundary\n"; 
$content.="Content-Type: text/plain; charset=ISO-2022-JP\n"; 
$content.="Content-Transfer-Encoding: 7bit\n\n"; 
$flag = true;
}else{
$headers.="Content-Type: text/plain; charset=ISO-2022-JP\n"; 
$headers.="Content-Transfer-Encoding: 7bit\n"; 
} 

if($flag)$content.="--$boundary--"; 

if(mail($webmail_towhich_email,$webmail_title,$content,$headers)){
echo("<script language='javascript'>alert('送信しました。');</script>");
echo("<script language='javascript'>self.close();</script>");
echo("<script language='javascript'>window.opener.location.reload();</script>");
echo("<script language='javascript'>location.replace('./webmail_left.php?session=$session');</script>");
exit;
}else{ 
echo("<script language='javascript'>alert('メール送信に失敗しました。');</script>");
echo("<script language='javascript'>location.replace('./webmail_register.php?session=$session');</script>");
exit;
} */
?>
