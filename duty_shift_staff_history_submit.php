<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力内容のチェック
if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d \d\d:\d\d:\d\d$/", $_POST["histdate"])) {
    echo("<script type=\"text/javascript\">alert('日付が正しく入力されていません。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}
//システム日付よりあとの日の場合はエラーとする
$wk_hist_ymd = substr($_POST["histdate"] , 0, 4).substr($_POST["histdate"] , 5, 2).substr($_POST["histdate"] , 8, 2);
$wk_sys_ymd = date("Ymd");
if ($wk_hist_ymd > $wk_sys_ymd) {
    echo("<script type=\"text/javascript\">alert('日付は当日以前を入力してください。');</script>\n");
    echo("<script type=\"text/javascript\">history.back();</script>\n");
    exit;
}

// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// UPDATE
if (!empty($_POST["duty_shift_staff_history_id"])) {
    $sql = "UPDATE duty_shift_staff_history SET ";
    $cond = "WHERE duty_shift_staff_history_id={$_POST["duty_shift_staff_history_id"]}";
    $upd = update_set_table($con, $sql, array('emp_id', 'group_id', 'histdate'), array($_POST["emp_id"], $_POST["group_id"], $_POST["histdate"]), $cond, $fname);
    if ($upd == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// INSERT
else {
    $sql = "insert into duty_shift_staff_history (emp_id, group_id, histdate) values(";
    $content = array($_POST["emp_id"], $_POST["group_id"], $_POST["histdate"]);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'duty_shift_staff_history_list.php?session=<? echo($session); ?>&emp_id=<? echo($emp_id); ?>';
</script>
