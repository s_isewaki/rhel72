<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("cl_workflow_common.ini");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

$cl_title = cl_title_name();

/**
 * アプリケーションメニューHTML取得
 */
$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベース接続オブジェクト取得
 */
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

/**
 * ログインユーザ情報取得
 */
require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$arr_login = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_login["emp_id"];

/**
 * 審議会
 */
require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_setting_council_model.php");
$council_model = new cl_mst_setting_council_model($mdb2, $login_emp_id);

if ($_POST['regist_flg'] == 'true') {

	/**
	 * トランザクション開始
	 */
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	/**
	 * 審議会削除
	 */
	$council_model->physical_delete();

	if ($_POST['emp_id'] != '') {
		// 選択されている顧客分ループ
		$arr_emp_id = explode(',' ,$_POST['emp_id']);
		foreach($arr_emp_id as $emp_id) {
			// 審議会登録
			$council_model->insert($emp_id);
		}
	}

	/**
	 * コミット処理
	 */
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * 登録フラグを初期化
 */
$regist_flg = '';

/**
 * 審議会取得
 */
$data = $council_model->getList();

// DB切断
$mdb2->disconnect();

/**
 * 削除アンカーを付加
 */
$row_count = count($data);

if ($row_count > 0) {
	// 先頭行
	$emp_ids = $data[0]['emp_id'];
	$emp_nms = $data[0]['emp_lt_nm'].' '.$data[0]['emp_ft_nm'];
	$emp_nms_view = write_emp_delete($data[0]['emp_id'], $data[0]['emp_lt_nm'], $data[0]['emp_ft_nm']);

	// 先頭行以外
	for($i = 1; $i < $row_count; $i++){
		$emp_ids .= ','.$data[$i]['emp_id'];
		$emp_nms .= ','.$data[$i]['emp_lt_nm'].' '.$data[$i]['emp_ft_nm'];
		$emp_nms_view .= ",";
		$emp_nms_view .= write_emp_delete($data[$i]['emp_id'], $data[$i]['emp_lt_nm'], $data[$i]['emp_ft_nm']);
	}
}

/**
 * テンプレートマッピング
 */
$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

$smarty->assign( 'cl_title'				, $cl_title				);
$smarty->assign( 'session'				, $session				);
$smarty->assign( 'regist_flg'			, $regist_flg			);
$smarty->assign( 'wkfwctn_mnitm_str'	, $wkfwctn_mnitm_str	);
$smarty->assign( 'js_str'				, $js_str				);

$smarty->assign( 'emp_nms_view'			, $emp_nms_view			);
$smarty->assign( 'emp_ids'				, $emp_ids				);
$smarty->assign( 'emp_nms'				, $emp_nms				);

$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート出力開始",__FILE__,__LINE__);

$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

$log->debug("テンプレート出力終了",__FILE__,__LINE__);

/**
 * アプリケーションメニューHTML取得
 */
function get_wkfw_menuitem($session,$fname)
{
	ob_start();
	show_wkfw_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}
/**
 * 削除アンカー生成
 */
function write_emp_delete($emp_id, $emp_lt_nm, $emp_ft_nm){

	$build  = "<a href=\"javascript:delete_emp('";
	$build .= $emp_id;
	$build .= "', '";
	$build .= $emp_lt_nm;
	$build .= ' ';
	$build .= $emp_ft_nm;
	$build .= "');\">";
	$build .= $emp_lt_nm;
	$build .= ' ';
	$build .= $emp_ft_nm;
	$build .= "</a>";
	return $build;
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
