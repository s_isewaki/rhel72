<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("sum_application_workflow_common_class.php");
require_once("summary_common.ini");

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// ログ
//==============================
require_once("summary_common.ini");
summary_common_write_operate_log("access", $con, $fname, $session, "", "", "");

$emp_id = get_emp_id($con, $session, $fname);

$obj = new application_workflow_common_class($con, $fname);


//==============================
// 承認などのデータ更新処理
//==============================
if (@$_REQUEST["approve_update"]){

	$approve_chk = (array)$_REQUEST["approve_chk"];
	foreach($approve_chk as $apply_info){
		list($c_apply_id, $c_apv_order, $c_apv_sub_order) = explode(",", $apply_info);
		$c_apply_id = (int)$c_apply_id;
		$c_apv_order = (int)$c_apv_order;
		$c_apv_sub_order = (int)$c_apv_sub_order;

		$sql = "select * from sum_apply where apply_id = ".$c_apply_id;
		$sel_apply = select_from_table($con, $sql, "", $fname);
		$row_apply = pg_fetch_array($sel_apply);

		$sql = " select * from sum_wkfwmst where wkfw_id = ".$row_apply["wkfw_id"];
		$sel_wkfwmst = select_from_table($con, $sql, "", $fname);
		$row_wkfwmst = pg_fetch_array($sel_wkfwmst);

		$sql = " select * from sum_applyapv where apply_id = ".$c_apply_id;
		$sel_applyapv = select_from_table($con, $sql, "", $fname);
		$row_applyapv = pg_fetch_array($sel_applyapv);

		//==============================
		// 申請結果通知削除・登録
		//==============================
		$sql =
			" update sum_applynotice set".
			" confirmed_flg = 't'".
			",send_emp_id = recv_emp_id".
			",send_date = ". date("YmdHm").
			" where apply_id = ".$c_apply_id.
			" and recv_emp_id = '".$emp_id."'".
			" and delete_flg = 'f'";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		//==============================
		// 受信ステータスの更新
		//==============================
		$sql =
			" update sum_applyapv set".
			" apv_stat_accepted = '1'".
			",apv_date_accepted = " . date("YmdHi").
			" where apply_id = ". $c_apply_id.
			" and apv_order = ". $c_apv_order;
		if ($c_apv_sub_order) $sql .= " and apv_sub_order = ". $c_apv_sub_order;
		else $sql .= " and apv_sub_order is null";
		$upd = update_set_table($con, $sql, array(), null, "", $fname);

		//==============================
		// 閲覧可能階層状態を変更
		//==============================
		$obj->approve_application($c_apply_id, $c_apv_order, $row_applyapv["next_notice_div"], $row_applyapv["next_notice_recv_div"], $session, (int)@$row_wkfwmst["wkfw_appr"]);
	}
}

//====================================
// 初期化処理
//====================================
$arr_wkfwcatemst = array();
$sel_wkfwcate = select_from_table($con,"select * from wkfwcatemst where wkfwcate_del_flg = 'f' order by wkfw_type","",$fname);

while($row_cate = pg_fetch_array($sel_wkfwcate)) {
  $tmp_wkfw_type = $row_cate["wkfw_type"];
  $tmp_wkfw_nm = $row_cate["wkfw_nm"];

  $arr_wkfwmst_tmp = array();
  $sql = "select * from wkfwmst where wkfw_type='".$tmp_wkfw_type."' and wkfw_del_flg = 'f' order by wkfw_id asc";
  $sel_wkfwmst = select_from_table($con,$sql,"",$fname);
  while($row_wkfwmst = pg_fetch_array($sel_wkfwmst)) {
    $tmp_wkfw_id = $row_wkfwmst["wkfw_id"];
    $wkfw_title = $row_wkfwmst["wkfw_title"];
    $arr_wkfwmst_tmp[$tmp_wkfw_id] = $wkfw_title;
  }

  $arr_wkfwcatemst[$tmp_wkfw_type] = array("name" => $tmp_wkfw_nm, "wkfw" => $arr_wkfwmst_tmp);
}

// 送信日セット(本日日付から過去２週間分)
// 送信タブをクリックした場合のみ。
if(@$apply_date_default == "on" || @$apply_date_default == "on2"){
  $today = date("Y/m/d", strtotime("today"));

  $arr_today = split("/", $today);
  $date_y2 = $arr_today[0];
  $date_m2 = $arr_today[1];
  $date_d2 = $arr_today[2];

// ２週間前の日付取得
//	$two_weeks_ago = date("Y/m/d",strtotime("-2 week" ,strtotime($today)));
  $two_weeks_ago = date("Y/m/d",strtotime("-1 day" ,strtotime($today)));
  $arr_two_weeks_ago = split("/", $two_weeks_ago);
  $date_y1 = $arr_two_weeks_ago[0] ;
  $date_m1 = $arr_two_weeks_ago[1] ;
  $date_d1 = $arr_two_weeks_ago[2] ;
}


?>
<?
// 外部ファイルを読み込む
write_yui_calendar_use_file_read_0_12_2();

// カレンダー作成、関数出力
write_yui_calendar_script2(2);
?>
<script type="text/javascript">
var isAllChecked = false;
function listCheckClicked(btn){
	isAllChecked = !isAllChecked;
	btn.value = "全て" + (isAllChecked ? "OFF" : "ON");

	for (var i = 0, j = document.approve.elements.length; i < j; i++) {
		if (document.approve.elements[i].type == 'checkbox') {
			if (document.approve.elements[i].disabled) continue;
			if (document.approve.elements[i].name=="sel_sasimodosare") continue;
			document.approve.elements[i].checked = isAllChecked;
		}
	}
}


function reload_page() {
  document.getElementById("refresh_wait_message").style.display = "";
  location.reload(true);
}

function initPage() {
}

function approve_search() {
  document.approve.action="sum_application_approve_list.php";
  document.approve.mode.value="search";
  document.approve.submit();
}

function show_sub_window(url) {
  var h = '700';
  var w = '1024';
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
  window.open(url, 'approvewin',option);
}
</script>
</head>
<body onload="initPage();initcal();">

<? summary_common_show_sinryo_top_header($session, $fname, "受信一覧"); ?>

<? summary_common_show_main_tab_menu($con, $fname, "受信一覧", 1); ?>

<form name="frm_refresh" action="sum_application_approve_list.php" method="get">
	<input type="hidden" name="session" value="<?=$session?>" />
	<input type="hidden" name="sel_diag_div" value="<?=@$sel_diag_div?>" />
	<input type="hidden" name="sel_ptif_id" value="<?=h(@$sel_ptif_id)?>" />
	<input type="hidden" name="sel_ptif_name" value="<?=h(@$sel_ptif_name)?>" />
	<input type="hidden" name="apply_emp_nm" value="<?=h(@$apply_emp_nm)?>" />
	<input type="hidden" name="date_y1" value="<?=@$date_y1?>" />
	<input type="hidden" name="date_m1" value="<?=@$date_m1?>" />
	<input type="hidden" name="date_d1" value="<?=@$date_d1?>" />
	<input type="hidden" name="date_y2" value="<?=@$date_y2?>" />
	<input type="hidden" name="date_m2" value="<?=@$date_m2?>" />
	<input type="hidden" name="date_d2" value="<?=@$date_d2?>" />
	<input type="hidden" name="apv_stat_accepted" value="<?=@$apv_stat_accepted?>" />
	<input type="hidden" name="sel_tmpl_id" value="<?=@$sel_tmpl_id?>" />
	<input type="hidden" name="sel_sasimodosare" value="<?=@$sel_sasimodosare?>" />
	<input type="hidden" name="cmb_reload_timer" value="0" />
</form>

<form name="approve" action="#" method="get">
<input type="submit" name="dummy" style="display:none" />
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="approve_update" value="">
<input type="hidden" name="approve" value="">
<table class="list_table" cellspacing="1">
<tr>
	<th width="10%" style="white-space:nowrap">診療記録区分</th>
	<td width="23%">
		<select name="sel_diag_div" onchange="resetOffsetTime();">
			<option value="">すべて</option>
			<? $sel = select_from_table($con, "select * from divmst where div_del_flg = 'f' order by div_id", "", $fname);?>
			<? while ($row = @pg_fetch_array($sel)){ ?>
			<?   $selected = ($row["diag_div"] == @$sel_diag_div ? "selected" : "");?>
			<?   $str = h($row["diag_div"]); ?>
			<option value="<?=$str?>"<?=$selected?>><?=$str?></option>
			<? } ?>
		</select>
	</td>
	<th width="10%" bgcolor="#fefcdf" style="white-space:nowrap">テンプレート名</th>
	<td width="23%">
		<select name="sel_tmpl_id" onchange="resetOffsetTime();">
			<option value="">すべて</option>
			<? $sel = select_from_table($con, "select * from tmplmst order by tmpl_id", "", $fname); ?>
			<? while($row = @pg_fetch_array($sel)){ ?>
			<?   $selected = ($row["tmpl_id"] == @$sel_tmpl_id ? "selected" : ""); ?>
			<option value="<?=$row["tmpl_id"]?>"<?=$selected?>><?=$row["tmpl_name"]?></option>
			<? } ?>
		</select>
	</td>
	<th width="10%" bgcolor="#fefcdf">送信者</th>
	<td width="23%" colspan="3"><input type="text" size="30" maxlength="30" name="apply_emp_nm" value="<?=h(@$apply_emp_nm)?>" onkeydown="resetOffsetTime();"></td>
</tr>
<tr>
	<th bgcolor="#fefcdf">患者ID</th>
	<td><input type="text" size="20" maxlength="20" name="sel_ptif_id" value="<?=h(@$sel_ptif_id)?>" onkeydown="resetOffsetTime();"></td>
	<th bgcolor="#fefcdf" style="white-space:nowrap;">患者氏名</th>
	<td><input type="text" size="30" maxlength="40" name="sel_ptif_name" value="<?=h(@$sel_ptif_name)?>" onkeydown="resetOffsetTime();"></td>
	<th bgcolor="#fefcdf">差戻通知</th>
	<td colspan="3"><label><input type="checkbox" name="sel_sasimodosare" value="1" <?=@$sel_sasimodosare ? "checked": ""?>" onclick="resetOffsetTime();">差戻し通知ありのみ</label></td>
</tr>
<tr>
	<th bgcolor="#fefcdf">送信日</th>
	<td colspan="3">
		<table>
		<tr>
			<td>
				<select id="date_y1" name="date_y1" onchange="resetOffsetTime();"><? show_select_years(10, $date_y1, true); ?></select>/<select id="date_m1" name="date_m1" onchange="resetOffsetTime();"><? show_select_months($date_m1, true); ?></select>/<select id="date_d1" name="date_d1" onchange="resetOffsetTime();"><? show_select_days($date_d1, true); ?></select>
			</td>
			<td>
			  <img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/><br>
			    <div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
			</td>
			<td>
				&nbsp;〜<select id="date_y2" name="date_y2" onchange="resetOffsetTime();"><? show_select_years(10, $date_y2, true); ?></select>/<select id="date_m2" name="date_m2" onchange="resetOffsetTime();"><? show_select_months($date_m2, true); ?></select>/<select id="date_d2" name="date_d2" onchange="resetOffsetTime();"><? show_select_days($date_d2, true); ?></select>
			</td>
			<td>
		  	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2()"/><br>
					<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div>
			</td>
		</tr>
		</table>
	</td>
	<th bgcolor="#fefcdf">受信状況</th>
	<td>
		<? $arr_nm = array('未受付','受付済','否認','差戻し'); ?>
		<? $arr_id = array('0','1','2','3'); ?>
		<? $apv_stat_options = array(); ?>
		<? $apv_stat_options[] = "<option value=\"-\">すべて</option>"; ?>
		<? for($i=0;$i<count($arr_nm);$i++) { ?>
		<?   $apv_stat_options[] = '<option value="'.$arr_id[$i].'" '.(@$apv_stat_accepted==$arr_id[$i] ? "selected":"").'>'.$arr_nm[$i].'</option>'; ?>
		<? } ?>
		<select name="apv_stat_accepted" onchange="resetOffsetTime();"><?= implode("\n",$apv_stat_options) ?></select>
	</td>
	<th bgcolor="#fefcdf">実施状況</th>
	<td>
		<? $arr_nm = array('未実施','実施済','否認','差戻し'); ?>
		<? $arr_id = array('0','1','2','3'); ?>
		<? $apv_stat_options = array(); ?>
		<? $apv_stat_options[] = "<option value=\"-\">すべて</option>"; ?>
		<? for($i=0;$i<count($arr_nm);$i++) { ?>
		<?   $apv_stat_options[] = '<option value="'.$arr_id[$i].'" '.(@$apv_stat_operated==$arr_id[$i] ? "selected":"").'>'.$arr_nm[$i].'</option>'; ?>
		<? } ?>
		<select name="apv_stat_operated" onchange="resetOffsetTime();"><?= implode("\n",$apv_stat_options) ?></select>
	</td>
</tr>
</table>

<div class="search_button_div" style="border-bottom:1px solid #fdfdfd">
	<input type="button" onclick="approve_search();" value="検索"/>
</div>

<div style="background-color:#F4F4F4; padding-top:5px; padding-bottom:5px; border-top:1px solid #d8d8d8; text-align:right">
	<select name="cmb_reload_timer" id="cmb_reload_timer" onchange="resetOffsetTime(); reloadTimer();">
		<option value="0">画面の自動リフレッシュ　オフ</option>
		<option value="30000"<? if(@$cmb_reload_timer == "30000"){?> selected<? } ?>>30秒おきに画面の自動リフレッシュ</option>
		<option value="60000"<? if(@$cmb_reload_timer == "60000"){?> selected<? } ?>>60秒おきに画面の自動リフレッシュ</option>
	</select>
	<input type="text" value="" id="reload_counter" style="border:0; width:25px; color:#aaa; background-color:transparent" readonly />
	<input type="button" onclick="approve_regist();" value="受信"/>
	<script type="text/javascript">
		var offsetTime = new Date().getTime();
		function resetOffsetTime(){
			offsetTime = new Date().getTime();
		}
		function reloadTimer(){
			var t = document.approve.cmb_reload_timer.value;
			if (!parseInt(t)) {
				document.approve.cmb_reload_timer.selectedIndex = 0;
				document.getElementById("reload_counter").value = "";
				return;
			}
			var currentTime = new Date().getTime();
			var diff = offsetTime*1 + t*1 - currentTime;
			if (diff < 0) {
				document.frm_refresh.cmb_reload_timer.value = t;
				document.frm_refresh.submit();
				return;
			}
			document.getElementById("reload_counter").value = parseInt(Math.ceil(diff/1000));
			setTimeout("reloadTimer()", 300);
		}
		reloadTimer();
	</script>
</div>


<?
$list_sql =
	" select".
	" *".
	",ap.apply_id".
	",su.ptif_id".
	",sast.sasimodosare_count".
	",wkm.next_notice_div as nnd".
	",apv.apv_order as apply_apv_order".
	" from sum_apply ap".
	" inner join sum_applyapv apv on (apv.apply_id = ap.apply_id and apv.emp_id = '".$emp_id."' and apv.apv_order <= ap.visibility_apv_order)".
	" inner join sum_wkfwmst wk on (wk.wkfw_id = ap.wkfw_id and wk.is_hide_approve_list = 0)".
	" inner join summary su on (su.summary_id = ap.summary_id and su.ptif_id = ap.ptif_id and su.emp_id = ap.emp_id)".
	" left join ptifmst pt on (pt.ptif_id = su.ptif_id)".
	" left join tmplmst tm on (tm.tmpl_id = wk.tmpl_id)".
	" left join empmst emp2 on (emp2.emp_id = ap.emp_id)".
	" left join sum_apply_hinin_sasimodosi sahs on (".
	"   sahs.apply_id = apv.apply_id".
	"   and sahs.from_apv_order = apv.apv_order".
//	"   and sahs.from_emp_id = apv.emp_id".
	"   and sahs.update_ymdhm is null".
	" )".
	" left join sum_wkfwapvmng wkm on (".
	"       wkm.wkfw_id   = apv.wkfw_id".
	"   and wkm.apv_order = coalesce(apv.wkfw_apv_order, apv.apv_order)".
	" )".
	" left join (".
	"   select".
	"   apply_id".
	"  ,to_apv_order".
	"  ,count(apply_id) as sasimodosare_count".
	"   from sum_apply_sasimodosi_target".
	"   where to_emp_id = '".$emp_id."'".
	"   and update_ymdhm is null".
	"   group by".
	"   apply_id".
	"  ,to_apv_order".
	" ) sast on (sast.apply_id = apv.apply_id and sast.to_apv_order = apv.apv_order)".
	" where ap.apply_stat = '1'".
	" and ap.delete_flg = 'f'";

if (trim(@$sel_diag_div) != "") { //診療記録区分
  $list_sql .= " and su.diag_div = '". pg_escape_string(trim($sel_diag_div))."'";
}
if ((int)@$sel_tmpl_id > 0) { //テンプレート名
  $list_sql .= " and wk.tmpl_id = ". (int)$sel_tmpl_id;
}
if(trim(@$sel_ptif_id) != "") {// 患者ID
  $list_sql .= " and su.ptif_id = '" . pg_escape_string(trim($sel_ptif_id))."'";
}
if(trim(@$sel_ptif_name) != "") {// 患者名
  $list_sql .=
    " and (".
    " pt.ptif_lt_kaj_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
	" or pt.ptif_ft_kaj_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
	" or pt.ptif_lt_kana_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
	" or pt.ptif_ft_kana_nm like '%" . pg_escape_string(trim($sel_ptif_name))."%'".
	" )";
}

//if(trim(@$apply_emp_nm) != "") { // 受信者
//  $sql_emp_nm = pg_escape_string($apply_emp_nm);
//  $list_sql .=
//    " and ap.emp_id in (".
//    "   select emp_id from empmst".
//    "   where (emp_lt_nm || emp_ft_nm) like '%$sql_emp_nm%'".
//    "   or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$sql_emp_nm%'".
//    "   or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$sql_emp_nm%'".
//    " )";
//}

if(trim(@$apply_emp_nm) != "") { // 受信者
	$sql_emp_nm = pg_escape_string($apply_emp_nm);
	$list_sql .=
    " and exists (".
    "   select empmst.emp_id from empmst".
    "   where ((emp_lt_nm || emp_ft_nm) like '%$sql_emp_nm%'".
    "   or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$sql_emp_nm%'".
    "   or (emp_kn_lt_nm || emp_kn_ft_nm) like '%$sql_emp_nm%')".
    "   and (ap.emp_id = empmst.emp_id)".
		" )";
}

if (preg_match("/^(\d*)/", @$date_y1.@$date_m1.@$date_d1, $matches) == 1) { // 送信日(from)
	$list_sql .= " and ap.apply_date >= '".$matches[1]."'";
}
if (preg_match("/^(\d*)/", @$date_y2.@$date_m2.@$date_d2, $matches) == 1) { // 送信日(to)
	$nextday = "0".((int)@$date_d2+1);
	$nextday = substr($nextday, strlen($nextday)-2, strlen($nextday));
	$list_sql .= " and ap.apply_date < '".@$date_y2.@$date_m2.@$nextday."'";
}
if(@$apv_stat_accepted == "0") { // 未受付
	$list_sql .= " and (apv.apv_stat_accepted is null or apv.apv_stat_accepted = '0')";
	$list_sql .= " and (apv.apv_stat_accepted_by_other is null or apv.apv_stat_accepted_by_other = '0')";
	$list_sql .= " and sahs.from_emp_id is null";
}
if(@$apv_stat_accepted == "1") {// 受付済
	$list_sql .= " and (apv.apv_stat_accepted = '1' or apv.apv_stat_accepted_by_other = '1')";
	$list_sql .= " and sahs.from_emp_id is null";
}
if(@$apv_stat_operated == "0") {// 未実施
	$list_sql .= " and wkm.next_notice_recv_div = 2";
	$list_sql .= " and (apv.apv_stat_operated is null or apv.apv_stat_operated = '0')";
	$list_sql .= " and (apv.apv_stat_operated_by_other is null or apv.apv_stat_operated_by_other = '0')";
	$list_sql .= " and sahs.from_emp_id is null";
}
if(@$apv_stat_operated == "1") {// 実施済
	$list_sql .= " and wkm.next_notice_recv_div = 2";
	$list_sql .= " and (apv.apv_stat_operated = '1' or apv.apv_stat_operated_by_other = '1')";
	$list_sql .= " and sahs.from_emp_id is null";
}
if(@$apv_stat_accepted == "2") {// 受付否認
	$list_sql .= " and sahs.hinin_or_sasimodosi = 'hinin'";
	$list_sql .= " and sahs.from_apply_stat_accepted = '1'";
}
if(@$apv_stat_operated == "2") {// 実施否認
	$list_sql .= " and sahs.hinin_or_sasimodosi = 'hinin'";
	$list_sql .= " and sahs.from_apply_stat_operated = '1'";
}
if(@$apv_stat_accepted == "3") {// 受付否認
	$list_sql .= " and sahs.hinin_or_sasimodosi = 'sasimodosi'";
	$list_sql .= " and sahs.from_apply_stat_accepted = '1'";
}
if(@$apv_stat_operated == "3") {// 実施差戻し
	$list_sql .= " and sahs.hinin_or_sasimodosi = 'sasimodosi'";
	$list_sql .= " and sahs.from_apply_stat_operated = '1'";
}
if (@$sel_sasimodosare){
	$list_sql .= " and sast.sasimodosare_count > 0";
}
$page = @$_REQUEST["page"];

// 件数取得
$sel = select_from_table($con, "select count(*) as cnt from ( " . $list_sql . ") d", "", $fname);
if ($sel == 0) {
  pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}

$approve_list_count = pg_fetch_result($sel, 0, "cnt");

if ($page == "") $page = 1;

//一画面内の最大表示件数
$disp_max_page = 50;
$offset = ($page - 1) * $disp_max_page;

$page_max  = 1;
if($approve_list_count != 0) $page_max  = floor( ($approve_list_count-1) / $disp_max_page ) + 1;

// 一覧データ取得
if($approve_list_count > 0) {

	show_page_area($page_max,$page);
  
?>



<table class="listp_table" cellspacing="1">
<tr>
<th width="4%" style="text-align:center; white-space:nowrap; padding:2px"><input type="button" value="全てON" onclick="listCheckClicked(this)"/></th>
<th width="10%" style="text-align:center; white-space:nowrap; padding:2px">オーダ番号<br>(管理ＣＤ-連番)</th>
<th width="8%" style="text-align:center; white-space:nowrap; padding:2px">診療<br/>記録区分</th>
<th width="10%" style="text-align:center; white-space:nowrap; padding:2px">テンプレート名</th>
<th width="7%" style="text-align:center; white-space:nowrap; padding:2px">患者ID</th>
<th width="9%" style="text-align:center; white-space:nowrap; padding:2px">患者氏名</th>
<th width="9%" style="text-align:center; white-space:nowrap; padding:2px">生年月日</th>
<th width="3%" style="text-align:center; white-space:nowrap; padding:2px">性別</th>
<th width="10%" style="text-align:center; white-space:nowrap; padding:2px">送信者</th>
<th width="11%" style="text-align:center; white-space:nowrap; padding:2px">送信日</th>
<th width="5%" style="text-align:center; white-space:nowrap; padding:2px">受信<BR>状況</th>
<th width="5%" style="text-align:center; white-space:nowrap; padding:2px">実施<BR>状況</th>
<th width="4%" style="text-align:center; white-space:nowrap; padding:2px">差戻<br/>通知</th>
</tr>

<?

  $sel = select_from_table($con, $list_sql . " order by ap.apply_id desc, apv.apv_order offset $offset limit $disp_max_page", "", $fname);
  if ($sel == 0) {
    pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
  }
  $approve_list = pg_fetch_all($sel);
  
  $count = 0;
  foreach($approve_list as $apv) {

	// オーダ番号
	$order_bangou = $obj->generate_apply_no_for_display($apv["short_wkfw_name"], $apv["apply_date"], $apv["apply_no"]);

	$apv_stat_accepted_nm = "";
	$apv_stat_operateded_nm = "";
	$onclick_event = "";

	$apv_stat_accepted_nm = "未受付";
	if ($apv["apv_stat_accepted_by_other"]=="1" && $apv["nnd"] == "3") $apv_stat_accepted_nm = "他者済";
	if ($apv["apv_stat_accepted"]=="1") $apv_stat_accepted_nm = "受付済";
	if ($apv["from_emp_id"]==$emp_id && $apv["from_apply_stat_accepted"] && $apv["hinin_or_sasimodosi"]=="sasimodosi") $apv_stat_accepted_nm = "差戻し";
	if ($apv["from_emp_id"]==$emp_id && $apv["from_apply_stat_accepted"] && $apv["hinin_or_sasimodosi"]=="hinin"     ) $apv_stat_accepted_nm = "否認"  ;
	if (($apv_stat_accepted_nm == "未受付") && ($apv["nnd"] == "3"))
	{
		$hos = getHininOrSasimodosi($con, $fname, $apv["ptif_id"], $apv["apply_id"], $apv["apply_apv_order"], $apv["emp_id"]) ;
		if ($hos) $apv_stat_accepted_nm = $hos ;
	}
/*    
	$apv_stat_operated_nm = "未実施";
	if ($apv["apv_stat_operated_by_other"]=="1") $apv_stat_operated_nm = "他者済";
	if ($apv["apv_stat_operated"]=="1") $apv_stat_operated_nm = "実施済";
	if ($apv["from_emp_id"]==$emp_id && $apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="sasimodosi") $apv_stat_operated_nm = "差戻し";
	if ($apv["from_emp_id"]==$emp_id && $apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="hinin") $apv_stat_operated_nm = "否認";
	if ($apv["from_emp_id"]==$emp_id && $apv["next_notice_recv_div"] <> "2") $apv_stat_operated_nm = '-';
*/
	$apv_stat_operated_nm = "未実施";
	if ($apv["apv_stat_operated_by_other"]=="1") $apv_stat_operated_nm = "他者済";
	if ($apv["apv_stat_operated"]=="1") $apv_stat_operated_nm = "実施済";
	if ($apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="sasimodosi") $apv_stat_operated_nm = "差戻し";
	if ($apv["from_apply_stat_operated"] && $apv["hinin_or_sasimodosi"]=="hinin") $apv_stat_operated_nm = "否認";
    if ($apv["next_notice_recv_div"] <> "2") $apv_stat_operated_nm = '-';
    

	$approve_chk_state = " disabled";
	$bgcolor = "#fff";

	// 受信完結、確認済みは薄い黄色
	if ($apv["is_approve_complete"]){
		if ($apv["approve_complete_check_ymdhms"]) $bgcolor = "#ffffd0";
	}
	// 操作が残っている場合は赤
	if($apv_stat_accepted_nm == "受付済" && $apv_stat_operated_nm == "未実施") {
		$approve_chk_state = "";
		$bgcolor = "#ffd9ec";
	}
	// 操作が残っている場合は赤
	if($apv_stat_accepted_nm == "未受付" && ($apv_stat_operated_nm == "未実施" || $apv_stat_operated_nm == "-")) {
		$approve_chk_state = "";
		$bgcolor = "#ffd9ec";
	}
	// 差し戻しが含まれる場合は緑
	if($apv_stat_accepted_nm == "差戻し" || $apv_stat_operated_nm == "差戻し") {
//       $bgcolor = "#b1f8b7";
		}
	// 否認が含まれる場合はオレンジ
	if($apv_stat_accepted_nm == "否認" || $apv_stat_operated_nm == "否認") {
		$bgcolor = "#fcc6a5";
	}
	// 差し戻されが含まれる場合は緑
	$is_sasimodosare = "";
	if ($apv["sasimodosare_count"] > 0){
	$bgcolor = "#b1f8b7";
		$is_sasimodosare = "あり";
	}

	// 文字の色をつける
	if ($apv_stat_accepted_nm == "差戻し") $apv_stat_accepted_nm = '<span style="color:#f05">差戻し</span>';
	if ($apv_stat_operated_nm == "差戻し") $apv_stat_operated_nm = '<span style="color:#f05">差戻し</span>';
	if ($apv_stat_accepted_nm == "否認") $apv_stat_accepted_nm = '<span style="color:#f05">否認</span>';
	if ($apv_stat_operated_nm == "否認") $apv_stat_operated_nm = '<span style="color:#f05">否認</span>';
	if ($apv_stat_accepted_nm == "受付済") $apv_stat_accepted_nm = '<span style="color:#880">受付済</span>';
	if ($apv_stat_operated_nm == "実施済") $apv_stat_operated_nm = '<span style="color:#880">実施済</span>';
	if ($apv_stat_accepted_nm == "他者済") $apv_stat_accepted_nm = '<span style="color:#ba16be">他者済</span>';
	if ($apv_stat_operated_nm == "他者済") $apv_stat_operated_nm = '<span style="color:#ba16be">他者済</span>';


	$ptif_sex = "不明";
	if ($apv["ptif_sex"] == "1") $ptif_sex = "男性";
	if ($apv["ptif_sex"] == "2") $ptif_sex = "女性";

	$ptif_birth = "";
	$birth = $apv["ptif_birth"];
	if (strlen($birth) == 8){
		$ptif_birth = substr($birth,0,4) . "年". substr($birth,4,2) . "月" . substr($birth,6,2) . "日";
	}

    $onclick = " onclick=\"show_sub_window('sum_application_approve_detail.php?session=$session&apply_id=".$apv["apply_id"]."&apv_order_fix=".$apv["apply_apv_order"]."&apv_sub_order_fix=".$apv["apv_sub_order"]."');\"";
    $count++;
    $classtag = " class=\"approve_list_".$count."\"";
    $align = " align=\"center\"";

//    $apply_date = preg_replace("/^(\d{4})(\d{2})(\d{2}).*$/", "$1/$2/$3", $apv["apply_date"]);
    $apply_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2}).*$/", "$1/$2/$3 $4:$5", $apv["apply_date"]);
    echo('<tr style="background-color:'.$bgcolor."; cursor:pointer\" onmouseover=\"this.style.backgroundColor='#fefc8f';\" onmouseout=\"this.style.backgroundColor='".$bgcolor."';\">\n");
    echo("<td".$classtag." onclick=\"resetOffsetTime();\" style=\"padding:1px; text-align:center\"><input type=\"checkbox\" name=\"approve_chk[]\" value=\"".$apv["apply_id"].",".$apv["apply_apv_order"].",".$apv["apv_sub_order"]."\"".$approve_chk_state."></td>\n");//選択
    echo("<td".$classtag.$onclick.">".h($order_bangou)."</td>\n"); //オーダ番号
    echo("<td".$classtag.$onclick.">".h($apv["diag_div"])."</td>\n"); //診療記録区分
    echo("<td".$classtag.$onclick.">".h($apv["tmpl_name"])."</td>\n"); //テンプレート名
    echo("<td".$classtag.$onclick.">".h($apv["ptif_id"])."</td>\n"); //患者ID
    echo("<td".$classtag.$onclick.">".h($apv["ptif_lt_kaj_nm"].$apv["ptif_ft_kaj_nm"])."</td>\n"); //患者氏名
    echo("<td".$classtag.$onclick.">".$ptif_birth."</td>\n"); //生年月日
    echo("<td".$classtag.$onclick.">".$ptif_sex."</td>\n"); //性別
    echo("<td".$align.$classtag.$onclick.">".h($apv["emp_lt_nm"]." ".$apv["emp_ft_nm"])."</td>\n");//送信者
    echo("<td".$align.$classtag.$onclick.">".$apply_date."</td>\n");// 送信日
    echo("<td".$align.$classtag.$onclick.">".$apv_stat_accepted_nm."</td>\n");//受信状況
    echo("<td".$align.$classtag.$onclick.">".$apv_stat_operated_nm."</td>\n");//実施状況
    echo("<td".$align.$classtag.$onclick." style='color:#f05'>".$is_sasimodosare."</td>\n");//差戻しあり
    echo("</tr>\n");
  }
  echo ("</table>");

  //最大ページ数
  show_page_area($page_max,$page);
}
?>
<input type="hidden" name="page" value="">

</form>

<table id="refresh_wait_message" style="position:absolute; width:100%; top:150px; display:none"><tr><td style="text-align:center">
	<span style="padding:20px; background-color:#f48d02; color:#fff; border:2px solid #eaef07; font-size:14px">
		画面を最新の状態に更新します。しばらくお待ちください。
	</span>
</td></tr></table>

<script type="text/javascript">
function approve_regist() {
  if (document.approve.elements['approve_chk[]'] == undefined) {
    alert('「受信」するデータを選択してくだい。'); return;
  }

  if (document.approve.elements['approve_chk[]'].length == undefined) {
    if (!document.approve.elements['approve_chk[]'].checked) {
      alert('「受信」するデータを選択してくだい。'); return;
    }
  } else {
    var checked = false;
    for (var i = 0, j = document.approve.elements['approve_chk[]'].length; i < j; i++) {
      if (document.approve.elements['approve_chk[]'][i].checked) {
        checked = true; break;
      }
    }
    if (!checked) {
      alert('「受信」するデータを選択してくだい。'); return;
    }
  }

  if (confirm('選択された送信書の「受信」をします。よろしいですか？')) {
    document.approve.action="sum_application_approve_list.php";
    document.approve.approve_update.value="1";
    document.approve.approve.value="1";
    document.approve.page.value="<?=$page?>";
    document.approve.submit();
  }
}
</script>

</body>
<? pg_close($con); ?>
</html>


<? function show_page_area($page_max,$page) { ?>
	<? if($page_max < 2) return; ?>
  <script type="text/javascript">
	  function page_change(page) { document.approve.page.value = page; document.approve.submit(); }
  </script>
  <table width="100%" style="margin-top:4px;"><tr><td>
    <table class="td_pad2px" style="color:#c0c0c0; white-space:nowrap"><tr>
	    <td><? if($page!=1){ ?><a href="javascript:page_change(1);"><? } ?>先頭へ<? if($page!=1){ ?></a><? } ?></td>
	    <td><? if($page!=1) { ?><a href="javascript:page_change(<?=$page-1?>);"><? } ?>前へ<? if($page!=1){ ?></a><? } ?></td>
	    <td>
				<? for($i=1;$i<=$page_max;$i++) { ?>
					<? if($i!=$page) { ?><a href="javascript:page_change(<?=$i?>);"><? } ?>[<?=$i?>]<? if($i!=$page) { ?></a><? } ?>
				<? } ?>
	    </td>
	    <td><? if($page!=$page_max){ ?><a href="javascript:page_change(<?=$page+1?>);"><? } ?>次へ<? if($page!=$page_max){ ?></a><? } ?></td>
    </tr></table>
  </td></tr></table>
<? } ?>

<?
function getHininOrSasimodosi($con, $fname, $pt_id, $apply_id, $apv_order, $emp_id)
{
  $sql2 =
    " select" .
    "   hinin_or_sasimodosi" .
    " from" .
    "   sum_apply_hinin_sasimodosi" .
    " where" .
    "       ptif_id = '" . $pt_id . "'" .
    "   and apply_id = " . $apply_id .
    "   and from_apv_order = " . $apv_order;
//    "   and from_apv_order = " . $apv_order . 
//    "   and from_emp_id <> '" . $emp_id . "'" ;
  
  $sel2 = @select_from_table($con, $sql2, "" , $fname) ;
  $stat2 = @pg_fetch_array($sel2) ;
  $hos = @$stat2["hinin_or_sasimodosi"] ;
  
  if ($hos) return "他者済" ;
  return "" ;
}
?>
