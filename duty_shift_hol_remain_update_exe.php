<?php
//duty_shift_hol_remain_update_exe.php
//繰越公休残日数表示調整更新処理
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("duty_shift_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$arr_staff_id = array();
$arr_hol_remain = array();
// 入力チェック
for ($i=0; $i<$data_cnt; $i++) {
    $staff_id_var = "staff_id_".$i;
    $staff_id = $$staff_id_var;
    $arr_staff_id[$i] = $staff_id;
    $var_name = "remain_".$i;
    $wk = $$var_name;
    if ($wk != "") {
        if (preg_match("/^-?\d$|^-?\d+\.?\d+$/", $wk) == 0) {
            $emp_name = get_emp_kanji_name($con, $staff_id, $fname);
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('{$emp_name}さんの繰越公休残には半角数字で入力してください。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
    }
    $arr_hol_remain[] = $wk;
}


// トランザクションを開始
pg_query($con, "begin transaction");

$emp_id_str = "";
for ($i=0; $i<$data_cnt; $i++) {
	if ($i>0) {
		$emp_id_str .= ",";
	}
    $emp_id_str .= "'$arr_staff_id[$i]'";
}
$sql = "select * from duty_shift_hol_remain ";
$cond = "where emp_id in ($emp_id_str) and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_days = array();
$num = pg_num_rows($sel);
for ($i=0; $i<$num; $i++) {
	$emp_id = pg_fetch_result($sel, $i, "emp_id");
    $arr_days["$emp_id"]["remain"] = pg_fetch_result($sel, $i, "remain");
}

//ログインユーザID
$login_emp_id = get_emp_id($con,$session,$fname);
$update_time = date("YmdHmi");
for ($i=0; $i<$data_cnt; $i++) {
	
    //応援追加は更新しない
    $var_name = "other_flg_".$i;
    if ($$var_name == "1") {
        continue;   
    }
	
	$var_name1 = "remain_".$i;
	$wk_emp_id = $arr_staff_id[$i];
	if ($$var_name1 != $arr_days["$wk_emp_id"]["remain"]) {
		//削除
		$sql = "delete from duty_shift_hol_remain ";
        $cond = "where emp_id = '$wk_emp_id' and duty_yyyy = $duty_yyyy and duty_mm = $duty_mm ";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		//追加
		if ($$var_name1 != "") {
            $sql = "insert into duty_shift_hol_remain (emp_id, duty_yyyy, duty_mm, remain, update_emp_id, update_time ) values (";
			$remain = $$var_name1;
            if ($remain != "") { //null以外で00を0とする
                if ($remain == 0) {
                    $remain = "0";
                }
            }
            $content = array($wk_emp_id, $duty_yyyy, $duty_mm, $remain, $login_emp_id, $update_time);
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

$org_yyyy=$duty_yyyy;
$org_mm=$duty_mm;

if ($warning_flg == "1") {
    $duty_mm = $duty_mm + 1;
    if ($duty_mm > 12) {
        $duty_yyyy = $duty_yyyy + 1;
    }
    ;
    //画面を更新する場合、コメントをはずす
//echo("<script type=\"text/javascript\">opener.location.href = 'duty_shift_results.php?session=$session&group_id=$group_id&duty_yyyy=$duty_yyyy&duty_mm=$duty_mm&warning_flg=$warning_flg';</script>");
}
else {
    //同じ月の場合、親画面側の公休残日数表示を更新する
    echo("<script type=\"text/javascript\">");
    echo("if (parent.opener) {\n");
    echo("var duty_yyyy = parent.opener.document.getElementById('cause_duty_yyyy').value;\n");
    echo("var duty_mm = parent.opener.document.getElementById('cause_duty_mm').value;\n");
    echo("if (duty_yyyy == '$duty_yyyy' && duty_mm=='$duty_mm') {");
    for ($i=0;$i<$data_cnt; $i++) {
        $cel_id = "hol_remain".(($i+1)*2);
        $wk = $arr_hol_remain[$i];
        if ($wk != "") { //null以外で00を0とする
            if ($wk == 0) {
                $wk = "0";
            }
        }
        
        echo("parent.opener.document.getElementById('$cel_id').innerHTML='<font face=\"ＭＳ Ｐゴシック, Osaka\">$wk</font>';\n");
    }
    echo("}\n");
    echo("");
    echo("}\n");
    
    echo("</script>");
}

echo("<script type=\"text/javascript\">location.href = 'duty_shift_hol_remain.php?session=$session&duty_yyyy=$org_yyyy&duty_mm=$org_mm&group_id=$group_id&start_date=$start_date&end_date=$end_date&prev_flg=$prev_flg&hol_cnt=$hol_cnt';</script>");

?>