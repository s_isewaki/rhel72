<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require_once("label_by_profile_type.ini");
require_once("show_sinryo_top_header.ini");
require_once("summary_common.ini");

//==============================
//ヘッダー処理
//==============================

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 権限チェック
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$is_postback = isset($_POST['is_postback']) ? $_POST['is_postback'] : '';

//==============================
//ポストバック時(検索時)
//==============================
$problem_name = '';
if($is_postback == 'true') {
    if ($mode == 'search') {    // 検索
        $problem_name = isset($_POST['problem_name']) ? trim($_POST['problem_name']) : '';
    }
} else {
    $problem_id = isset($_GET['problem_id']) ? intval($_GET['problem_id']) : -1;
    if ($problem_id > 0) {
        // プロブレムIDが0以外の場合、プロブレム名を検索条件とする為、プロブレム名を取得して$problem_nameにセット
        $ptif_id = isset($_GET['ptif_id']) ? $_GET['ptif_id'] : '';
        $problem_name = getProblemName($con, $ptif_id, $problem_id, $fname);
        $problem_name = ($problem_name !== false) ? $problem_name : '';
    }
}

//==============================
//標準計画一覧を取得
//==============================
$cond = "where usable = true";
if (strlen($problem_name) != 0) {
    $cond .= " and problem_name like '%{$problem_name}%'";
}

// 標準計画ライブラリデータリストを取得する
$data_list = getStandardPlanLibraryDataList($con, $cond, true, $fname);
pg_close($con);    // DB切断

/**
 * 標準計画ライブラリデータリストを取得する
 *
 * @param resource $con   データベース接続リソース
 * @param string   $cond  条件
 * @param string   $fname ページ名
 *
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getStandardPlanLibraryDataList($con, $cond, $fname)
{
    $sql = "select * from sum_med_std_wkfwmst";
    $cond .= " order by med_wkfw_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $data_list = pg_fetch_all($sel);

    return $data_list;
}

/**
 * プロブレム名を取得する
 *
 * @param resource $con        データベース接続リソース
 * @param string   $ptif_id    患者ID
 * @param integer  $problem_id プロブレムID
 * @param string   $fname      ページ名
 *
 * @return mixed 成功時:プロブレム名/失敗時:false
 */
function getProblemName($con, $ptif_id, $problem_id, $fname)
{
    $sql = "select problem from summary_problem_list where ptif_id = '{$ptif_id}' and problem_id = {$problem_id}";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        return false;
    }

    $problem_name = pg_fetch_result($sel, 0, 'problem');
    return $problem_name;
}

/**
 * インラインフレームを生成
 *
 * @param string $session   セッション
 * @param array  $data_list 標準計画ライブラリデータリスト
 *
 * @return void
 */
function createInlineFrame($session, $data_list)
{
    $param = "session={$session}&mode=detail";
    if (!empty($data_list)) {
        $param .= "&med_wkfw_id={$data_list[0]['med_wkfw_id']}";
    }
    echo '<iframe src="summary_stdplan_library.php?' . $param . '" frameborder="no" name="stdplan_detail" id="stdplan_detail" width="615" height="700">';
    echo '</iframe>';
}
?>
<title>CoMedix 標準看護計画</title>
<script language="JavaScript" type="text/JavaScript">
/****************************************************************
 * reload_page()
 * リロード
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function reload_page()
{
  location.href = "summary_stdplan_library_reference_main.php?session=<? echo $session; ?>";
}

/****************************************************************
 * show_stdplan_detail()
 * 標準看護計画詳細表示
 *
 * 引　数：med_wkfw_id ワークフローID
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function show_stdplan_detail(med_wkfw_id)
{
    document.getElementById('stdplan_detail').contentWindow.reload_page(med_wkfw_id);
}

/****************************************************************
 * search_stdplan_library()
 * 標準看護計画検索
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function search_stdplan_library()
{
    document.mainform.action = 'summary_stdplan_library_reference_main.php?mode=search';
    document.mainform.submit();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/main_summary.css"/>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 th {border:#ffffff solid 1px; background-color:#4682b4; color:#ffffff}
.list2_in2 td {border:#ffffff solid 1px;}
div.left_block {
  float: left;
  width: 300px;
  margin-left: 25px;
}
.container {
  width: 955px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? echo summary_common_show_dialog_header('標準看護計画'); ?>
<form name="mainform" action="" method="post">
<div class="container">
<!-- left start -->
<div class="left_block">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
      <img src="img/spacer.gif" alt="" width="1" height="3"><br>
      <div style="margin-top: 4px;">
         プロブレム<br>
        <input type="text" id="problem_name" name="problem_name" style="width: 280px; ime-mode: active;" value="<? echo $problem_name; ?>"  maxlength="120">
      </div>
      <div style="margin-top: 4px; width:280px; text-align: right;"><input type="button" value="検索" onclick="search_stdplan_library();"></div>
      <br style="clear:both">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" valign="top">
<!-- theme list start -->
            <input type="hidden" name="session" value="<? echo($session); ?>">
            <input type="hidden" name="is_postback" value="true">

プロブレム一覧
            <table width="300" border="0" cellspacing="0" cellpadding="2">
<?
$total_record = $data_list ? count($data_list) : 0;
for ($i = 0; $i < $total_record; $i++) {
    if (!$data_list[$i]) {
        break;
    }
?>
              <tr>
                <td>
                  <a href="javascript:show_stdplan_detail('<? echo $data_list[$i]['med_wkfw_id']; ?>')">
<? echo h($data_list[$i]['problem_name']); ?>
                  </a>
                </td>
              </tr>
<?
}
?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>
<!-- left end -->
<!-- right start -->
<div class="right_block">
<? createInlineFrame($session, $data_list); ?>
</div>
<!-- right end -->
</div>
</form>
</body>
</html>
