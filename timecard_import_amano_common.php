<?php
//勤務実績インポート（アマノ）用共通
//timecard_import_amano_common.php 

/**
 * 変換設定データ取得
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $no 空白以外：指定データ 空白""：全て
 * @return array 変換設定の配列
 *
 */
function get_cnvdata_amano($con, $fname, $no) {
    //display_nameのnull対応 20131111
    $sql = "select ";
    $sql .= " a.* ";
    $sql .= ",b.group_name as tmcd_group_name";
    $sql .= ",c.atdptn_nm, c.previous_day_possible_flag, c.previous_day_flag ";
    $sql .= ",case when d.display_name = '' or d.display_name is null then d.default_name else d.display_name end as reason_name";
    $sql .= " from timecard_import_set_amano a ";
    $sql .= " left join wktmgrp b on b.group_id = a.tmcd_group_id ";
    $sql .= " left join atdptn c on c.group_id = a.tmcd_group_id and to_char(c.atdptn_id,'FM999999999999') = a.atdptn_ptn_id ";
    $sql .= " left join atdbk_reason_mst d on d.reason_id = a.reason ";
    
    if ($no == "") {
        $cond = " order by a.no ";
    } else {
        $cond = " where a.no = $no ";
    }
    
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $num = pg_numrows($sel);
    if ($num == 0) {
        return array();
    }
    return pg_fetch_all($sel);
    
}

/**
 * 変換設定データ削除
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $no 対象番号
 * @return なし
 *
 */
function deleteImportSetDataAmano($con, $fname, $no) {
    
    pg_query($con, "begin transaction");
    
    $sql = "delete from timecard_import_set_amano";
    $cond = "where no = $no";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    
    $sql = "select max(no) from timecard_import_set_amano ";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $wk_no = intval(pg_fetch_result($sel, 0, 0));
    //番号を詰める
    if ($no < $wk_no) {
        $sql = "update timecard_import_set_amano set no = no - 1 ";
        $set = array();
        $setvalue = array();
        $cond = "where no > $no";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_exec($con,"rollback");
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
    }
    
    pg_query($con, "commit");
    
}

?>