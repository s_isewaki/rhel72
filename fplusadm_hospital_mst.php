<?
$fname = $PHP_SELF;
ob_start();
require_once("about_comedix.php");
require_once("show_fplusadm_lib_list.ini");
require_once("fplusadm_common.ini");
require_once("show_class_name.ini");
require_once("show_emp_next.ini");
require_once("show_emp_list.ini");
require_once("yui_calendar_util.ini");

ob_end_clean();

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$checkauth = check_authority($session, 79, $fname);
$con = @connect2db($fname);
if(!$session || !$checkauth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

// 並び順のデフォルトは職員IDの昇順
$emp_o = "1";

// 表示条件のデフォルトは「利用中の職員を表示」
$view = "1";

// 組織階層情報を取得
$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧を取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧を取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 科一覧を取得
$sql = "select * from deptmst";
$cond = "where dept_del_flg = 'f' order by order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 室一覧を取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select * from classroom";
	$cond = "where room_del_flg = 'f' order by order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
}


//入院基本料施設基準（一般病床）取得
$sql = "select nursing_standard_id, nursing_standard from fplus_nursing_standard where del_flg = 'f'";
$cond = "order by nursing_standard_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$nursing_standard_list = pg_fetch_all($sel);


//==============================
// 病院一覧を取得
//==============================
if(@$hospital_id == ""){
	$sql = "select hospital_id, hospital_name from fplus_hospital_master where del_flg = 'f'";
	$cond = "order by hospital_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_list = pg_fetch_all($sel);

	//病院IDが指定されていない場合は先頭の病院を指定
	if($hospital_list != "")
	{
		$hospital_id = $hospital_list[0]["hospital_id"];
	}
}



//==============================
//ポストバック時
//==============================
if(@$postback == "true")
{
	if($postback_mode == "history_delete")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		$delete_target_list = "";
		foreach($hospital_history_ids as $history_id)
		{
			if($delete_target_list != "")
			{
				$delete_target_list .= ",";
			}
			$delete_target_list .= "'".pg_escape_string($history_id)."'";
		}

		$sql = "update fplus_hospital_history set";
		$set = array("update_time", "del_flg");
		$setvalue = array(date("Y-m-d H:i:s"), "t");
		$cond = "where hospital_history_id in($delete_target_list)";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// トランザクションのコミット
		pg_query($con, "commit");
	}
	elseif($postback_mode == "history_update")
	{
		// トランザクションの開始
		pg_query($con, "begin");

		if($closing_flg == "on"){
			$hospital_closing_flg = "t";
		}else{
			$hospital_closing_flg = "f";
		}

		//履歴データ更新
		$sql = "update fplus_hospital_history set";
		$set = array("hospital_bed", "nursing_standard", "update_time", "closing_flg");
		$setvalue = array($hospital_bed, $nursing_standard, date("Y-m-d H:i:s"), $hospital_closing_flg);
		$cond = "where hospital_history_id ='$hospital_history_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// トランザクションのコミット
		pg_query($con, "commit");
	}
}

//==============================
//病院基本情報を取得
//==============================
if($hospital_id != "")
{
	$sql = " SELECT ".
		" 	hospital_id ".
		" 	, hospital_name ".
		" 	, hospital_directer ".
		" 	, class_id ".
		" 	, attribute_id ".
		" 	, dept_id ".
		" 	, room_id ".
		" FROM ".
		" 	fplus_hospital_master ";
	$cond = "where hospital_id = '$hospital_id' AND del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_contents = pg_fetch_all($sel);
}

	$hospital_name = $hospital_contents[0]["hospital_name"];
	$hospital_directer = $hospital_contents[0]["hospital_directer"];
	$class_id = $hospital_contents[0]["class_id"];
	$attribute_id = $hospital_contents[0]["attribute_id"];
	$dept_id = $hospital_contents[0]["dept_id"];
	$room_id = $hospital_contents[0]["room_id"];


//==============================
// 更新履歴情報を取得
//==============================
if($hospital_id != "")
{
	$sql = "SELECT ".
		" 	hospital_history_id ".
		" 	, hospital_id ".
		" 	, fiscal_year ".
		" 	, hospital_bed ".
		" 	, nursing_standard ".
		" 	, closing_flg ".
		" FROM ".
		" 	fplus_hospital_history ".
		" WHERE ".
		" 	hospital_id = '$hospital_id' ".
		" 	AND del_flg = 'f' ";
	if($hospital_history_id != ""){
		$sql .= " 	AND hospital_history_id = '$hospital_history_id' ";
	}
	$cond = "order by fiscal_year";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_history_list = pg_fetch_all($sel);

}

	$hospital_history_id = $hospital_history_list[0]["hospital_history_id"];
	$fiscal_year = $hospital_history_list[0]["fiscal_year"];
	$hospital_bed = $hospital_history_list[0]["hospital_bed"];
	$nursing_standard_id = $hospital_history_list[0]["nursing_standard"];
	$hospital_closing_flg = $hospital_history_list[0]["closing_flg"];


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | マスタ管理</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
	// 新規病院追加
	function registHospital() {
		window.open('fplusadm_hospital_mst_registration.php?session=<?=$session?>', 'newwin', 'width=600,height=400,scrollbars=yes');
	}

	// 新規履歴追加
	function registHistory() {
		window.open('fplusadm_hospital_history_registration.php?session=<?=$session?>&hospital_id=<?=$hospital_id?>', 'newwin', 'width=600,height=400,scrollbars=yes');
	}

	// 病院削除
	function deleteHospital() {
		var checked_ids = getCheckedIds(document.delhosp.elements['hospital_ids[]']);
		if (checked_ids.length == 0) {
			alert('病院が選択されていません。');
			return;
		}

		if (!confirm('選択された病院を削除します。よろしいですか？')) {
			return;
		}

		document.delhosp.submit();
	}

	function updateHospital() {
		var hospital_name = document.getElementById("hospital_name");
		var hospital_directer = document.getElementById("hospital_directer");

		if(hospital_name.value == ""){
			alert("病院名を入力してください。");
			hospital_name.focus();
			return;
		}

		if(hospital_directer.value == ""){
			alert("病院長名を入力してください。");
			hospital_directer.focus();
			return;
		}

		if (!confirm('更新します。よろしいですか？')) {
			return;
		}
		document.updhosp.submit();
	}

	function getCheckedIds(boxes) {
		var checked_ids = new Array();
		if (boxes) {
			if (!boxes.length) {
				if (boxes.checked) {
					checked_ids.push(boxes.value);
				}
			} else {
				for (var i = 0, j = boxes.length; i < j; i++) {
					if (boxes[i].checked) {
						checked_ids.push(boxes[i].value);
					}
				}
			}
		}
		return checked_ids;
	}

	function deleteHistory() {
		var is_selected = false;
		var objs = document.getElementsByName("hospital_history_ids[]");
		for (var i = 0; i < objs.length; i++) {
			if (objs[i].checked) {
				is_selected = true;
				break;
			}
		}

		if (!is_selected) {
			alert("選択されていません。");
			return;
		}


		if (!confirm('選択された項目を削除します。よろしいですか？')) {
			return;
		}


		document.history_frm.postback_mode.value = "history_delete";
		document.history_frm.submit();
	}
	function updateHistory() {
		var hospital_bed = document.getElementById("hospital_bed");
		var nursing_standard = document.getElementById("nursing_standard");

		if (hospital_bed.value == "") {
			alert("病床数を入力してください。");
			hospital_bed.focus();
			return;
		} else {
			if (!hospital_bed.value.match(/^[0-9]+$/)) {
				alert("病床数は数値を入力してください。");
				hospital_bed.focus();
				return;
			}
		}

		if (nursing_standard.value == "") {
			alert("入院基本料施設基準（一般病床）を選択してください。");
			nursing_standard.focus();
			return;
		}

		if (!confirm('更新します。よろしいですか？')) {
			return;
		}

		document.history_contents.postback_mode.value = "history_update";
		document.history_contents.submit();
	}

	function reload_page() {
		location.href = "fplusadm_hospital_mst.php?session=<?=$session?>&hosapital_id=<?=$hosapital_id?>";
	}

	function setAtrbOptions() {

		clearOptions(document.updhosp.attribute_id);

		addOption(document.updhosp.attribute_id, '', ' ');

		var class_id = document.updhosp.class_id.value;

		<? while ($row = pg_fetch_array($sel_atrb)) { ?>
			if (class_id == '<? echo $row["class_id"]; ?>') {
				addOption(document.updhosp.attribute_id, '<? echo $row["atrb_id"]; ?>', '<? echo $row["atrb_nm"]; ?>');
			}
		<? } ?>

		setDeptOptions();
	}

	function setDeptOptions() {

		clearOptions(document.updhosp.dept_id);

		addOption(document.updhosp.dept_id, '', ' ');

		var atrb_id = document.updhosp.attribute_id.value;

		<? while ($row = pg_fetch_array($sel_dept)) { ?>
			if (atrb_id == '<? echo $row["atrb_id"]; ?>') {
				addOption(document.updhosp.dept_id, '<? echo $row["dept_id"]; ?>', '<? echo $row["dept_nm"]; ?>');
			}
		<? } ?>
		<? if ($arr_class_name["class_cnt"] == 4) { ?>
			setRoomOptions();
		<? } ?>
	}

	<? if ($arr_class_name["class_cnt"] == 4) { ?>
		function setRoomOptions() {

			clearOptions(document.updhosp.room_id);

			addOption(document.updhosp.room_id, '', ' ');

			var dept_id = document.updhosp.dept_id.value;

			<? while ($row = pg_fetch_array($sel_room)) { ?>
				if (dept_id == '<? echo $row["dept_id"]; ?>') {
					addOption(document.updhosp.room_id, '<? echo $row["room_id"]; ?>', '<? echo $row["room_nm"]; ?>');
				}
			<? } ?>
		}
	<? } ?>

	function clearOptions(box) {
		for (var i = box.length - 1; i >= 0; i--) {
			box.options[i] = null;
		}
	}

	function addOption(box, value, text, selected) {
		var opt = document.createElement("option");
		opt.value = value;
		opt.text = text;
		if (selected == value) {
			opt.selected = true;
		}
		box.options[box.length] = opt;
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.list td td {border-width:0;}

.list2 {border-collapse:collapse;}
.list2 td {border:#E6B3D4 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 td {border:#E6B3D4 solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#E6B3D4 solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a> &gt; <a href="fplusadm_mst.php?session=<? echo($session); ?>"><b>マスタ管理</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>


<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">

	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr>
			<td width="3" align="right"><img src="img/menu-fplus-left.gif" alt="" width="3" height="22"></td>
			<td width="120" align="center" bgcolor="#FFDFFF"><a href="fplusadm_mst.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr>マスタ作成</nobr></font></a></td>
			<td width="3" align="left"><img src="img/menu-fplus-right.gif" alt="" width="3" height="22"></td>
			<td width="10">&nbsp;</td>
			<td width="3" align="right"><img src="img/menu-fplus-left-on.gif" alt="" width="3" height="22"></td>
			<td width="120" align="center" bgcolor="#FF99CC"><a href="fplusadm_hospital_mst.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><nobr>病院属性</nobr></font></a></td>
			<td width="3" align="left"><img src="img/menu-fplus-right-on.gif" alt="" width="3" height="22"></td>
			<td>&nbsp;</td>
		</tr>
	</table>



	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:6px">
		<tr>
			<td width="30%" valign="top">


				<!-- theme list start -->
				<form name="delhosp" action="fplusadm_hospital_mst_delete.php" method="post">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="1">
									<tr bgcolor="#feeae9">
										<td class="spacing" height="24"><?=$font?>マスタ一覧</font></td>
										<td align="right"><?=$font?>
											<input type="button" value="作成" onclick="registHospital();" />
											<input type="button" value="削除" onclick="deleteHospital();" /></font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr height="400">
							<td valign="top">
								<?// 病院一覧表示?>
								<? show_hospital_mst_list($con, $hospital_id, $session, $fname); ?>
							</td>
						</tr>
					</table>

					<input type="hidden" name="session" value="<? echo($session); ?>" />
					<input type="hidden" name="hosapital_id" value="<? echo($hosapital_id); ?>" />
				</form>
				<!-- theme list end -->

			</td>


			<td><img src="img/spacer.gif" alt="" width="2" height="1"></td>

			<td width="70%" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2">
					<tr>
						<td width="100%" bgcolor="#feeae9"><?=$font?>病院情報</font></td>
					</tr>
					<tr>
						<td width="100%" style="border-bottom:0;">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2">
								<tr>
									<td width="100%" bgcolor="#feeae9"><?=$font?>病院基本情報</font></td>
								</tr>
								<tr>
									<td width="100%">
										<!-- theme detail start -->
										<form name="updhosp" action="fplusadm_hospital_mst_update_exe.php" method="post">
											<table width="100%" border="0" cellspacing="0" cellpadding="2">
												<tr>
													<td width="5%" style="border:0;"></td>
													<td width="20%" align="left" style="border:0;"><?=$font?>病院名</font></td>
													<td width="50%" style="border:0;"><?=$font?><input type="text" id="hospital_name" name="hospital_name" style="width:100%;" value="<?=$hospital_name?>" /></font></td>
													<td width="25%" style="border:0;"></td>
												</tr>
												<tr>
													<td width="5%" style="border:0;"></td>
													<td width="20%" align="left" style="border:0;"><?=$font?>病院長名</font></td>
													<td width="50%" style="border:0;"><?=$font?><input type="text" id="hospital_directer" name="hospital_directer"  style="width:100%;" value="<?=$hospital_directer?>" /></font></td>
													<td width="25%" style="border:0;"></td>
												</tr>
												<tr>
													<td width="100%" colspan="4" style="border:0;">
													</td>
												</tr>
												<tr>
													<td width="100%" colspan="4" style="border:0;">
														<table width="100%" border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;"><?=$font?>組織階層</font></td>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="2">
																		<tr>
																			<td width="5%" style="border:0;"></td>
																			<td width="5%" style="border:0;"><?=$font?><? echo($arr_class_name["class_nm"]); ?></font></td>
																			<td width="90%" style="border:0;">
																				<div style="float:left"><?=$font?>
																					<select id="class_id" name="class_id" onchange="setAtrbOptions();">
																					<option value="">
																					<?
																					while ($row = pg_fetch_array($sel_class)) {
																						echo("<option value=\"{$row["class_id"]}\"");
																						if ($row["class_id"] == $class_id) {echo(" selected");}
																						echo(">{$row["class_nm"]}\n");
																					}
																					?>
																					</select>
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td width="5%" style="border:0;"></td>
																			<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["atrb_nm"]); ?></font></td>
																			<td width="85%" style="border:0;">
																				<div style="float:left"><?=$font?>
																				<select id="attribute_id" name="attribute_id" onchange="setDeptOptions();">
																				<option value="">
																				<?
																				pg_result_seek($sel_atrb, 0);
																				while ($row = pg_fetch_array($sel_atrb)) {
																					if ($row["class_id"] == $class_id) {
																						echo("<option value=\"{$row["atrb_id"]}\"");
																						if ($row["atrb_id"] == $attribute_id) {echo(" selected");}
																						echo(">{$row["atrb_nm"]}\n");
																					}
																				}
																				?>
																				</select>
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td width="5%" style="border:0;"></td>
																			<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["dept_nm"]); ?></font></td>
																			<td width="85%" style="border:0;">
																				<div style="float:left"><?=$font?>
																				<select id="dept_id" name="dept_id"<? if ($arr_class_name["class_cnt"] == 4) {echo(" onchange=\"setRoomOptions();\"");} ?>>
																				<option value="">
																				<?
																				pg_result_seek($sel_dept, 0);
																				while ($row = pg_fetch_array($sel_dept)) {
																					if ($row["atrb_id"] == $attribute_id) {
																						echo("<option value=\"{$row["dept_id"]}\"");
																						if ($row["dept_id"] == $dept_id) {echo(" selected");}
																						echo(">{$row["dept_nm"]}\n");
																					}
																				}
																				?>
																				</select>
																				</div>
																			</td>
																		</tr>
																		<? if ($arr_class_name["class_cnt"] == 4) { ?>
																		<tr>
																			<td width="5%" style="border:0;"></td>
																			<td width="10%" style="border:0;"><?=$font?><? echo($arr_class_name["room_nm"]); ?></font></td>
																			<td width="85%" style="border:0;">
																				<div style="float:left"><?=$font?>
																					<select id="room_id" name="room_id">
																					<option value="">
																					<?
																					pg_result_seek($sel_room, 0);
																					while ($row = pg_fetch_array($sel_room)) {
																						if ($row["dept_id"] == $dept_id) {
																							echo("<option value=\"{$row["room_id"]}\"");
																							if ($row["room_id"] == $room_id) {echo(" selected");}
																							echo(">{$row["room_nm"]}\n");
																						}
																					}
																					?>
																					</select>
																				</div>
																			</td>
																		</tr>
																		<? } ?>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td width="5%" style="border:0;"></td>
													<td width="95%" colspan="3" align="left" style="border:0;"><?=$font?><input type="button" value="更新" onclick="updateHospital();" <?=($hospital_id==""?"disabled=\"disabled\"":"") ?> /></font></td>
												</tr>
												<input type="hidden" name="session" value="<? echo($session); ?>" />
												<input type="hidden" name="hospital_id" value="<? echo($hospital_id); ?>" />
											</table>
										</form>
										<!-- theme detail end -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" style="border-bottom-width:0; border-top-width:0;"></td>
					</tr>
					<tr>
						<td width="100%" style="border-top-width:0;">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="40%" style="border:0;" valign="top">
										<form name="history_frm" action="fplusadm_hospital_mst.php" method="post">
											<input type="hidden" name="session" value="<? echo($session); ?>"/>
											<input type="hidden" name="hospital_id" value="<? echo($hospital_id); ?>"/>
											<input type="hidden" name="postback" value="true"/>
											<input type="hidden" name="postback_mode" value=""/>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr bgcolor="#feeae9" style="height:22px;">
													<td align="left" width="50%" style=" border-right:0;"><?=$font?>年度</font>
													</td>
													<td width="50%" style=" border-left:0;">
														<div style="float:right"><?=$font?>
															<input type="button" value="作成" onclick="registHistory();" />
															<input type="button" value="削除" onclick="deleteHistory();" /></font>
														</div>
													</td>
												</tr>
												<tr>
													<td width="100%" colspan="2">
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td valign="top" style="border:0;">
																	<?// 履歴一覧表示?>
																	<? show_hospital_history_list($con, $hospital_id, $hospital_history_id, $session, $fname); ?>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</form>
									</td>
									<td width="60%" style="border:0;" valign="top">
										<form name="history_contents" action="fplusadm_hospital_mst.php" method="post">
											<input type="hidden" name="session" value="<? echo($session); ?>"/>
											<input type="hidden" name="hospital_id" value="<? echo($hospital_id); ?>"/>
											<input type="hidden" name="hospital_history_id" value="<? echo($hospital_history_id); ?>"/>
											<input type="hidden" name="postback" value="true"/>
											<input type="hidden" name="postback_mode" value=""/>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr bgcolor="#feeae9" style="height:22px;">
													<td align="left" width="100%" style="height:22px;"><?=$font?>病院属性履歴</font></td>
												</tr>
												<tr>
													<td width="100%">
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<? if($fiscal_year=="0000"){ ?>
																	<td width="5%" style="border:0;"></td>
																	<td width="95%" style="border:0;" colspan="2"><?=$font?>初期登録時</font></td>
																<? }else{ ?>
																	<td width="5%" style="border:0;"></td>
																	<td width="95%" style="border:0;"><?=$font?>
																		<input type="text" id="fiscal_year" name="fiscal_year" style="background-color:#D3D3D3" maxlength="4" size="6" value="<?=($fiscal_year=="0000"?"":$fiscal_year)?>" readonly="readonly" style="text-align:right;" />
																			&nbsp;年度
																	</font></td>
																<? } ?>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;"><?=$font?>病床数
																	<input type="text" id="hospital_bed" name="hospital_bed" value="<?=$hospital_bed?>" maxlength="4" size="6" style="ime-mode: disabled;text-align:right;" /></font>
																</td>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;" colspan="2"><?=$font?>入院基本料施設基準（一般病床）</font></td>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;" colspan="2">
																	<?=$font?>
																	<select id="nursing_standard" name="nursing_standard"><option></option>
																		<? foreach($nursing_standard_list as $idx => $row){ ?>
																			<option value="<?=$row["nursing_standard_id"]?>" <?= ($row["nursing_standard_id"]==$nursing_standard_id ? 'selected="selected"': "")?> ><?=$row["nursing_standard"]?></option>
																		<? } ?>
																	</select>
																	</font>
																</td>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;" colspan="2" align="left">
																	<?=$font?><input type="checkbox" id="closing_flg" name="closing_flg" <?= ($hospital_closing_flg=="t" ? 'checked="checked"': "") ?> <?=($fiscal_year=="0000" ? "disabled" : "")?> />閉鎖</font>
																</td>
															</tr>
															<tr>
																<td width="5%" style="border:0;"></td>
																<td width="95%" style="border:0;" colspan="2">
																	<?=$font?><input type="button" value="更新" onclick="updateHistory();" <?=($hospital_history_id==""?"disabled=\"disabled\"":"") ?>/></font>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>

<?

// 病院一覧を表示
function show_hospital_mst_list($con, $hospital_id, $session, $fname) {

	$ret_hospital_id = "";

	// 病院一覧を取得
	$sql = "select hospital_id, hospital_name from fplus_hospital_master where del_flg = 'f'";
	$cond = "order by hospital_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td class=\"spacing\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録済みの病院はありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return $ret_mst_cd;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");

	while ($row = pg_fetch_array($sel)) {

		$tmp_hospital_id = $row["hospital_id"];
		$tmp_hospital_name = $row["hospital_name"];

		if ($hospital_id == "") {
			$hospital_id = $tmp_hospital_id;
		}

		$cbox = "<input type=\"checkbox\" id=\"hospital_ids[]\" name=\"hospital_ids[]\" value=\"$tmp_hospital_id\">";
		$label = ($tmp_hospital_id == $hospital_id) ? $tmp_hospital_name : "<a href=\"fplusadm_hospital_mst.php?session=$session&hospital_id=$tmp_hospital_id\">$tmp_hospital_name</a>";
		echo("<tr height=\"22\">\n");
		echo("<td>\n");
		echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		if ($tmp_hospital_id == $hospital_id) {
			echo("<td><img src=\"img/right_red.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		} else {
			echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		}
		echo("<td valign=\"top\">$cbox</td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$label</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");

		if ($tmp_hospital_id == $hospital_id) {
			$ret_hospital_id = $tmp_hospital_id;
		}
	}
	echo("</table>\n");

	//return $ret_hospital_id;
}

// 履歴一覧を表示
function show_hospital_history_list($con, $hospital_id, $hospital_history_id, $session, $fname) {

	$ret_hospital_history_id = "";

	
	if($hospital_id != ""){
		// 履歴一覧を取得
		$sql = "SELECT ".
			" 	hospital_history_id ".
			" 	, hospital_id ".
			" 	, fiscal_year ".
			" FROM ".
			" 	fplus_hospital_history ".
			" WHERE ".
			" 	hospital_id = '$hospital_id' ".
			" 	AND del_flg = 'f' ";
		$cond = "order by fiscal_year";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td class=\"spacing\" style=\"border:0;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録済みの履歴はありません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return $ret_mst_cd;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">\n");

	while ($row = pg_fetch_array($sel)) {

		$tmp_hospital_history_id = $row["hospital_history_id"];
		if($row["fiscal_year"]=="0000"){
			$tmp_fiscal_year = "初期登録時";
			$chk_disable = "disabled=\"disabled\"";
		}else{
			$tmp_fiscal_year = $row["fiscal_year"]."年度";
			$chk_disable = "";
		}

		if ($hospital_history_id == "") {
			$hospital_history_id = $tmp_hospital_history_id;
		}

		$cbox = "<input type=\"checkbox\" id=\"hospital_history_ids[]\" name=\"hospital_history_ids[]\" value=\"$tmp_hospital_history_id\" $chk_disable />";
		$label = ($tmp_hospital_history_id == $hospital_history_id) ? $tmp_fiscal_year : "<a href=\"fplusadm_hospital_mst.php?session=$session&hospital_id=$hospital_id&hospital_history_id=$tmp_hospital_history_id\">$tmp_fiscal_year</a>";
		echo("<tr height=\"22\">\n");
		echo("<td style=\"border:0;\">\n");
		echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		echo("<tr>\n");
		if ($tmp_hospital_history_id == $hospital_history_id) {
			echo("<td style=\"border:0;\"><img src=\"img/right_red.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		} else {
			echo("<td style=\"border:0;\"><img src=\"img/spacer.gif\" alt=\"\" width=\"17\" height=\"17\"></td>\n");
		}
		echo("<td valign=\"top\" style=\"border:0;\">$cbox</td>\n");
		echo("<td style=\"border:0;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$label</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");

		if ($tmp_hospital_history_id == $hospital_history_id) {
			$ret_hospital_history_id = $tmp_hospital_history_id;
		}
	}
	echo("</table>\n");

	return $ret_hospital_history_id;
}

?>
