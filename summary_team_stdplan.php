<?

//==============================
// チーム計画
//==============================
// 当初の仕様では、
// ・チーム実践では共用、看護、介護、リハビリの4職種
// ・評価では看護、介護、リハビリの3職種
// など職種別に入力ができるようになっていましたが当面は職種別分けをしないことになりました
// チーム実践では共用のみを、評価では看護のみを使用、それ以外の職種の表示関係をコメントアウトしました
// 職種別を使用する際には表示関係のコメントを外し、入力サブウィンドウsummary_team_stdsub.phpも修正してください 2012.8.1
// DB処理は生かしたままになっています

require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require("summary_tmpl_util.ini");
require_once("summary_common.ini");

//==============================
//ページ名
//==============================
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// DB処理開始
//=================================
// ログインユーザー氏名と職名を得る
//=================================
$sql = "SELECT E.emp_lt_nm||E.emp_ft_nm AS name, J.job_nm AS job FROM empmst AS E INNER JOIN jobmst AS J ON E.emp_job=J.job_id WHERE emp_id = '$emp_id' ";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$login_user_name = pg_fetch_result($sel, 0, "name");
$login_user_job = pg_fetch_result($sel, 0, "job");

//==============================
// 患者氏名を得る
//==============================
$sql = "SELECT ptif_lt_kaj_nm,ptif_ft_kaj_nm FROM ptifmst WHERE ptif_id = '$pt_id' ";
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
if( pg_num_rows($sel) == 0 ){
	$kname = "";
}else{
	$ptif_lt_name = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
	$ptif_ft_name = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
	$kname = $ptif_lt_name . " " . $ptif_ft_name;
}

//==================================================================
// プロブレムリストを作る
// ・解決済でないプロブレムであること。
// ・標準チーム計画テーブルに登録されているプロブレムIDも取得。
//==================================================================
$sql = <<<_SQL_END_
SELECT
P.problem_id,
P.problem_list_no,
P.problem,
T.problem_id AS team_problem_id
FROM summary_problem_list AS P
LEFT JOIN (
SELECT
problem_id
FROM sum_med_std_team
WHERE ptif_id = '{$pt_id}'
GROUP BY problem_id
) AS T
ON P.problem_id = T.problem_id
WHERE P.ptif_id = '{$pt_id}' AND P.end_date = ''
ORDER BY P.problem_list_no
_SQL_END_;
$sel = select_from_table($con,$sql,"",$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$problem_list=array();
for($i=0; $i<pg_num_rows($sel); $i++){
	$problem_list[$i]["id"] = pg_fetch_result($sel, $i, "problem_id");
	$problem_list[$i]["disp_id"] = pg_fetch_result($sel, $i, "problem_list_no");
	$problem_list[$i]["name"] = pg_fetch_result($sel, $i, "problem");
	$problem_list[$i]["team_problem_id"] = pg_fetch_result($sel, $i, "team_problem_id");
}
$disp_problem_list="";

// 表示形式に直す
for($i=0; $i<count($problem_list); $i++){
	$wkproid=$problem_list[$i]["id"]; // DBのプロブレムID
	$wkdisp_proid=$problem_list[$i]["disp_id"]; // 表示用のプロブレム番号
	$wkpname=$problem_list[$i]["name"];
	$bgcolor   = "#ffffff";    // デフォルト背景色:白
	$fontcolor = "#000000";    // デフォルト文字色:黒
	if ( $problem_list[$i]["id"]==$problem_id ){ // いま選ばれているプロブレム
	    if ($problem_list[$i]["team_problem_id"]) {    // 標準チーム計画に対象プロブレムのデータがある場合
	        $bgcolor   = "#63bafe";    // 背景:薄いブルー
	        $fontcolor = "#ffffff";    // 文字色:白
	    }
	    $disp_problem_list .= '<SPAN STYLE="background-color:' . $bgcolor. '; color:' . $fontcolor . ';">' . "#" . $wkdisp_proid.$wkpname . '</SPAN>&nbsp;&nbsp;';
	    $leftdeftitle = $problem_list[$i]["name"];
	}else{
	    if ($problem_list[$i]["team_problem_id"]) {    // 標準チーム計画に対象プロブレムのデータがある場合
	        $bgcolor = "#ffff00";    // 背景:薄い黄色
	    }
	    $disp_problem_list .= '<SPAN STYLE="background-color:' . $bgcolor . ';"><A HREF="javascript:problemSelect('.$wkproid.');">#'.$wkdisp_proid.$wkpname.'</A></SPAN>&nbsp;&nbsp;';
	}
}
//==============================
// 各初期値
// ヘッダデータ
//==============================

if($nextdate != "") {
    $select_date = $nextdate;
    $create_date = $nextdate; // mode="new"のとき
} elseif ($sel_other_problem) {    // プロブレム変更時
    $select_date = '';
}

$creater_name="";
$creater_job="";
// 左側データ
$leftdef_title="";
$deftdef_value="";
$leftS="";
$leftE="";
$leftO="";
$leftCreater="";
$leftCreater_emp_id="";
// 中側データ
$centerCreater	= array();
$centerCreaterId= array();
$centerLogDate	= array();
$centerLogger	= array();
$centerLoggerId = array();
$leftObservationList="";
// 右側データ
$rightdate = array();
$rightname = array();
$rightvalu = array();
$rightemp_id = array();
$rightbutt = array();
$righttime = array();

$rightCount=array();
$rightCount[1]=1;
$rightCount[2]=1;
$rightCount[3]=1;

// ロック関係
$tbl_name=array("sum_med_priactice0","sum_med_priactice1","sum_med_priactice2","sum_med_priactice3","sum_med_std_team");
$col_nm_lockflg=array("c0_lock_flag","c1_lock_flag","c2_lock_flag","c3_lock_flag","left_lock_flag");
$col_nm_lockemp=array("c0_lock_emp_id","c1_lock_emp_id","c2_lock_emp_id","c3_lock_emp_id","lef_lock_emp_id");
$col_nm_locktim=array("c0_lock_time","c1_lock_time","c2_lock_time","c3_lock_time","lef_lock_time");

//==============================
// チーム計画マスターを読み込む
//==============================
$sql = <<<_SQL_END_
SELECT 
M.team_id,
problem_id,
ptif_id,
create_date,
emp_name,
M.emp_job,
def_value,
s_detail,
e_detail,
risk_detail,
outcome,
left_lock_flag,
lef_lock_time,
lef_lock_emp_id,
kyoyo_op,
kyoyo_tp,
kyoyo_ep,
c0_lock_flag,
c0_lock_time,
c0_lock_emp_id,
kango_op,
kango_tp,
kango_ep,
c1_lock_flag,
c1_lock_time,
c1_lock_emp_id,
kaigo_op,
kaigo_tp,
kaigo_ep,
c2_lock_flag,
c2_lock_time,
c2_lock_emp_id,
rihabiri_op,
rihabiri_tp,
rihabiri_ep,
c3_lock_flag,
c3_lock_time,
c3_lock_emp_id,
 left_jobname||' '||left_emp_name AS left_creater_name,
kyoyo_jobname||' '||kyoyo_emp_name AS kyoyo_creater_name,
kango_jobname||' '||kango_emp_name AS kango_creater_name,
kaigo_jobname||' '||kaigo_emp_name AS kaigo_creater_name,
rihabiri_jobname||' '||rihabiri_emp_name AS rihabiri_creater_name
FROM sum_med_std_team AS M
LEFT OUTER JOIN sum_med_priactice0 AS P0 ON M.team_id=P0.team_id 
LEFT OUTER JOIN sum_med_priactice1 AS P1 ON M.team_id=P1.team_id 
LEFT OUTER JOIN sum_med_priactice2 AS P2 ON M.team_id=P2.team_id 
LEFT OUTER JOIN sum_med_priactice3 AS P3 ON M.team_id=P3.team_id 
_SQL_END_;
$cond  = " WHERE M.ptif_id='$pt_id' AND M.problem_id=$problem_id  ";
$cond .= " ORDER BY create_date DESC ";
$sel = select_from_table($con,$sql,$cond,$fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}

if ( pg_num_rows($sel) != 0 && $mode != "new" ){ // チーム計画が存在する&&新規での再起動でもないとき
	// ヘッダデータ
	//=====================================
	// 同じプロブレム内で違う日付のマスター
	//=====================================
	$fetchPoint=0;
	$create_date		= date("Y-m-d",strtotime(pg_fetch_result($sel, $fetchPoint, "create_date"))); // 時刻の部分を捨てる
	$create_date_list = "<SELECT NAME='select_date' ONCHANGE='javascript:dateSelect();'>";
	for($i=0; $i<pg_num_rows($sel); $i++){
		$selected="";
		// 日付処理。時刻の部分を捨てる。d_dateは表示形式、v_dateは格納形式
		$wkdate = strtotime(pg_fetch_result($sel, $i, "create_date"));
		$d_date = date("Y年m月d日",$wkdate);
		$v_date = date("Y-m-d",$wkdate);
		if ($select_date == $v_date){ // $select_dateは日付を変更して再呼び出しされる際の変数。患者レポート画面からは受け継がない
			$selected="SELECTED";
			$fetchPoint=$i;
			$check_date = $d_date; // 日付指定呼び出しの場合の$create_dateはこっち
		}
		$create_date_list .= "<OPTION VALUE='".$v_date."' ".$selected.">".$d_date."</OPTION>";
	}
	
	$team_id 		= pg_fetch_result($sel, $fetchPoint, "team_id");
	$create_date_list .= "</SELECT>";
	$creater_name		= pg_fetch_result($sel, $fetchPoint, "emp_name");
	$creater_job		= pg_fetch_result($sel, $fetchPoint, "emp_job");
	// 左側データ
	$leftdef_title		= pg_fetch_result($sel, $fetchPoint, "def_name");
	$deftdef_value		= pg_fetch_result($sel, $fetchPoint, "def_value");
	$leftS			= pg_fetch_result($sel, $fetchPoint, "s_detail");
	$leftE			= pg_fetch_result($sel, $fetchPoint, "e_detail");
	$leftRisk		= pg_fetch_result($sel, $fetchPoint, "risk_detail");
	$leftO			= pg_fetch_result($sel, $fetchPoint, "outcome");
	$leftCreater		= pg_fetch_result($sel, $fetchPoint, "left_creater_name");
	// 中央データ
	// 0共用 1看護 2介護 3リハビリ
	$centerCreater[0]	= pg_fetch_result($sel, $fetchPoint, "kyoyo_creater_name");
	$centerOP[0]		= pg_fetch_result($sel, $fetchPoint, "kyoyo_op");
	$centerTP[0]		= pg_fetch_result($sel, $fetchPoint, "kyoyo_tp");
	$centerEP[0]		= pg_fetch_result($sel, $fetchPoint, "kyoyo_ep");
	
	$centerCreater[1]	= pg_fetch_result($sel, $fetchPoint, "kango_creater_name");
	$centerOP[1]		= pg_fetch_result($sel, $fetchPoint, "kango_op");
	$centerTP[1]		= pg_fetch_result($sel, $fetchPoint, "kango_tp");
	$centerEP[1]		= pg_fetch_result($sel, $fetchPoint, "kango_ep");
	
	$centerCreater[2]	= pg_fetch_result($sel, $fetchPoint, "kaigo_creater_name");
	$centerOP[2]		= pg_fetch_result($sel, $fetchPoint, "kaigo_op");
	$centerTP[2]		= pg_fetch_result($sel, $fetchPoint, "kaigo_tp");
	$centerEP[2]		= pg_fetch_result($sel, $fetchPoint, "kaigo_ep");
	
	$centerCreater[3]	= pg_fetch_result($sel, $fetchPoint, "rihabiri_creater_name");
	$centerOP[3]		= pg_fetch_result($sel, $fetchPoint, "rihabiri_op");
	$centerTP[3]		= pg_fetch_result($sel, $fetchPoint, "rihabiri_tp");
	$centerEP[3]		= pg_fetch_result($sel, $fetchPoint, "rihabiri_ep");
	
	// 患者の問題及びアウトカムの更新記録リストを得る
	$sql  = "SELECT update ,left_jobname||' '||left_emp_name AS name FROM sum_med_rec_left ";
	$sql .= " WHERE team_id=$team_id ORDER BY update DESC ";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$wk = pg_num_rows($sel);
	for($i=0; $i<$wk; $i++){
		$leftUpdaterId[$i]=pg_fetch_result($sel, $i , "emp_id");
		$leftUpdatDate[$i]=pg_fetch_result($sel, $i , "update");
		$leftUpdaterName[$i]=pg_fetch_result($sel, $i , "name");
	}
	
	// 表示形式に直す
	$dispLeftRec="";
	for($i=0; $i<count($leftUpdatDate); $i++){
		$wk=strtotime($leftUpdatDate[$i]);
		$dispLeftRec .= "更新日:".date("Y/m/d H:i",date(strtotime($leftUpdatDate[$i])))."<BR>";
		$dispLeftRec .= "更新者:".$leftUpdaterName[$i]."<BR>";
	}
	
	// 観察項目リスト
	$sql  = "SELECT MST.item_cd , TMP.item_name AS name FROM sum_med_std_obse AS MST ";
	$sql .= "INNER JOIN tmplitem AS TMP ON TMP.item_cd=MST.item_cd AND TMP.mst_cd='A263' ";
	$sql .= " WHERE team_id=$team_id ORDER BY MST.update ";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$wk = pg_num_rows($sel);
	$get_obs_items = pg_fetch_all($sel);
	// 表示形式に直す
	$observation_items = "";
	foreach($get_obs_items as $key=>$val){
		$obs_id=$val["item_cd"];
		$obs_nm=$val["name"];
		$observation_items .= '<div id="' . $obs_id. '_div">' . '<input type="checkbox" class="observation_item" id="' . $obs_id . '" name="' . $obs_id . '">' . $obs_nm . '</div>';
	}
	
	// チーム実践の更新記録
	
	// ロックがかかっているときは更新ロックを外す
	if( $mode == "unlock" ){
		$jtype = $updatetype;
		if ( $jtype == 10 ){ $jtype = 4; }
		switch($jtype){
		  case 0:
		  case 1:
		  case 2:
		  case 3:
		  case 4:
		  $sql  = "UPDATE ".$tbl_name[$jtype]." SET ";
		  $cond = "WHERE team_id = $team_id";
		  $set = array($col_nm_lockflg[$jtype],$col_nm_lockemp[$jtype]);
		  $setvalue =  array("f","");
		  $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		  if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			summary_common_show_error_page_and_die();
		  }
		  break;
		}
	}
	// 表示形式用
	$centerRec=array("","","",""); // 0:共用 1:看護 2:介護 3:リハビリ
	
	// 共用
	$sql = "SELECT update_c0 AS update ,jobname||' '||emp_name AS name FROM sum_med_recorder0 ";
	$sql .= " WHERE team_id=$team_id ORDER BY update_c0 DESC";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$center_rec_all0 = pg_fetch_all($sel);
	// 表示形式に直す
	foreach($center_rec_all0 as $reckey){
		$wk=date("Y/m/d H:i",strtotime($reckey["update"]));
		$centerRec[0] .= "更新日:".$wk."<BR>"."更新者:".$reckey["name"]."<BR>";
	}
	
	// 看護
	$sql = "SELECT update_c1 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder1 AS M ";
	$sql .= " WHERE team_id=$team_id ORDER BY update_c1 DESC";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$center_rec_all1 = pg_fetch_all($sel);
	// 表示形式に直す
	foreach($center_rec_all1 as $reckey){
		$wk=date("Y/m/d H:i",strtotime($reckey["update"]));
		$centerRec[1] .= "更新日:".$wk."<BR>"."更新者:".$reckey["name"]."<BR>";
	}
	
	// 介護
	$sql = "SELECT update_c2 AS update,jobname||' '||emp_name AS name  FROM sum_med_recorder2 AS M ";
	$sql .= " WHERE team_id=$team_id ORDER BY update_c2 DESC";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$center_rec_all2 = pg_fetch_all($sel);
	// 表示形式に直す
	foreach($center_rec_all2 as $reckey){
		$wk=date("Y/m/d H:i",strtotime($reckey["update"]));
		$centerRec[2] .= "更新日:".$wk."<BR>"."更新者:".$reckey["name"]."<BR>";
	}
	
	// リハビリ
	$sql = "SELECT update_c3 AS update,jobname||' '||emp_name AS name FROM sum_med_recorder3 AS M ";
	$sql .= " WHERE team_id=$team_id ORDER BY update_c3 DESC";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$center_rec_all3 = pg_fetch_all($sel);
	// 表示形式に直す
	foreach($center_rec_all3 as $reckey){
		$wk=date("Y/m/d H:i",strtotime($reckey["update"]));
		$centerRec[3] .= "更新日:".$wk."<BR>"."更新者:".$reckey["name"]."<BR>";
	}
	
	// 右側データ・評価コメントを得る
	$sql = "SELECT team_type,emp_id,update,comment,jobname||' '||emp_name AS name FROM sum_med_std_asses ";
	$sql .= " WHERE team_id=$team_id ORDER BY team_type , update DESC";
	$sel = select_from_table($con,$sql,"",$fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$wk = pg_num_rows($sel);
	$ass_list = pg_fetch_all($sel);
	// 表示形式に直す
	// team_type 1:看護 2:介護 3:リハビリ
	$ass_disp_list=array("","","",""); // 0:未使用 1:看護 2:介護 3:リハビリ
	foreach($ass_list as $num=>$value){
		$type     = $value["team_type"];
		$update   = $value["update"];
		$emp_name = $value["name"];
		$wk_emp_id = $value["emp_id"];
		$comment  = $value["comment"];
		$displayDate = date( "Y年m月d日",strtotime($update) );
		$btype=$type + 4;
		$ass_disp_list[$type] .= "評価日:$displayDate";
		if( $emp_id == $wk_emp_id ){
			$ass_disp_list[$type] .= "<INPUT TYPE='BUTTON' VALUE='削除' ONCLICK='javascript:deleteRight($type,\"$update\");'><BR>";
		}else{
			$ass_disp_list[$type] .= "<BR>";
		}
		$ass_disp_list[$type] .= "評価者:$emp_name<BR>";
		$ass_disp_list[$type] .= "評価:$comment<BR>";
		$rightCount[$type]++;
	}
}
else{
//==================================================================
// マスターが無いときは新規。またはmode="new"のときは新規
// 新規のときの日付は呼び出し元からもらう($create_date)
// 日付を選んで全削除した場合は$create_dateが無くなるので当日の日付
//==================================================================
	$team_id = 0;
	if ( $create_date == "" ){
		$create_date = date("Y-m-d");
	}
	$wkdate = strtotime($create_date);
	$d_date = date("Y年m月d日",$wkdate);
	$v_date = date("Y-m-d", $wkdate);
	$create_date_list  = "<SELECT NAME='select_date' ONCHANGE='javascript:dateSelect();'>";	
	$create_date_list .= "<OPTION VALUE='".$v_date."'>".$d_date."</OPTION></SELECT>";
}

// 他の職員が更新しようとしているときに削除するとき
if ( $mode == "deleteall" ){
	$lock_name=array();
	$lock_mode = 0;
	$lock_disp = "";
	for($type=0; $type<4; $type++){
		$sql= "SELECT E.emp_lt_nm||E.emp_ft_nm AS name FROM ".$tbl_name[$type]." AS M ";
		$sql .= " LEFT OUTER JOIN empmst AS E ON E.emp_id=".$col_nm_lockemp[$type]." ";
		$cond = "WHERE team_id = $team_id AND ".$col_nm_lockflg[$type]."='t'";
		$sel = select_from_table($con,$sql,$cond,$fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			require_once("summary_common.ini");
			summary_common_show_error_page_and_die();
		}
		if ( pg_num_rows($sel) != 0 ){
			$lock_name[$lock_mode]=pg_fetch_result($sel, 0, "name");
			$lock_mode ++;
		}
	}
}

// DB読み込みおわり

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML XMLNS="http://www.w3.org/1999/xhtml">
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=EUC-JP">
<LINK REL="STYLEsheet" TYPE="text/css" HREF="css/main_summary.css"/>
<style type="text/css">
</style>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>

<TITLE>CoMedix メドレポート｜標準チーム計画</TITLE>
<SCRIPT TYPE="text/javascript">

// グローバル変数
var login_user_id="<? echo $emp_id;?>";
var problem_id="<? echo $problem_id;?>";
var team_id="<? echo $team_id;?>";

// グローバル変数・子画面
var subwin_input;
var subwin_soap;
var subwin_observ;
var subwin_library;

// 現在日時
function now(tp){
	var now=new Date();
	var yy=now.getFullYear();
	var mo=now.getMonth()+1;
	var dd=now.getDate();
	var hh="0"+now.getHours();
	var mm="0"+now.getMinutes();
	var ss="0"+now.getSeconds();
	var dy=yy+"/"+mo+"/"+dd;
	var ti=hh.substr(hh.length-2)+":"+mm.substr(mm.length-2);
	var ret="";
	switch (tp){
	 case 0:ret = yy+"年"+mo+"月"+dd+"日"; break;
	 case 1:ret = ti+":"+ss ; break;
	 case 2:ret = yy+"-"+mo+"-"+dd+" "+ti+":"+ss ; break;
	}
	return ret;
}

// 職員氏名を返す、子画面で使う
function getEmpName(){
	return "<? echo $login_user_name;?>";
}

// 違うプロブレムを選択したとき
function problemSelect(pid){
	document.standardform.problem_id.value=pid;
	document.standardform.sel_other_problem.value = 'true';    // 別プロブレム選択フラグ
	document.standardform.action = 'summary_team_stdplan.php';
	document.standardform.submit();
}
// 違う日付を選択したとき。日付はFORMのNAME=select_dateのvalue
function dateSelect(){
	document.standardform.problem_id.value=problem_id;
	document.standardform.action = 'summary_team_stdplan.php';
	document.standardform.submit();
}
// プロブレムIDと日付を指定して再起動(summary_stdplan_libcopy_list.phpとsummary_stdplan_libcopy_exe.phpから呼ばれる)
function teamSelect(pid,date,mode){
	document.standardform.problem_id.value=pid;
	document.standardform.nextdate.value=date; // select_dateに上書きが出来ないためnextdateを別途用意する
	document.standardform.mode.value=mode;
	document.standardform.action = 'summary_team_stdplan.php';
	document.standardform.submit();
}

function unLock(type){
	document.standardform.problem_id.value=problem_id;
	document.standardform.updatetype.value=type;
	document.standardform.mode.value="unlock";
	document.standardform.action = 'summary_team_stdplan.php';
	document.standardform.submit();

}
// 再表示 DEBUG用
// function reStart(){
//	document.standardform.problem_id.value=problem_id;
//	document.standardform.action = 'summary_team_stdplan.php';
//	document.standardform.submit();
//}

// 観察項目を削除
function deleteObservationItem() {
    // 観察項目を取得
    var elements = document.getElementsByClassName('observation_item');
    var observation = document.getElementById('left_observation');
    var length = elements.length;
    var innerHTMLNew = '';
    for (var i = 0; i < length; i++) {
        var id = elements[i].id;
        var check = document.getElementById(id);
        if (check.checked) {
            continue;
        }
        innerHTMLNew += '<div id="' + id + '_div">' + document.getElementById(id + '_div').innerHTML + '</div>';
    }
    observation.innerHTML = innerHTMLNew;
    submitUpd(41);
}

// 評価コメントを削除して再表示 typeは職種、updateは登録日時(表示日時とは異なる)
function deleteRight(type,update){
	if( confirm("評価を削除しても良いですか？") ){
		document.standardform.updatemode.value="delete";
		document.standardform.rightdate.value=update;
		// DB更新して再表示
		var btype=type+4;
		submitUpd(btype);
	}
}

// データ入力・更新子画面を開く。複数不可
// 必要なデータは子画面からここへ取りに来るので引数へ付けない
function subWin( type ){
	var arg="";
	if ( !subwin_input || subwin_input.closed ){
	 switch (type){
	 case 5:
	 case 6:
	 case 7:
	 case 10:
		var xsize = 400;
		var ysize = 200;
	 	break;
	 default:
		var xsize = 400;
		var ysize = 750;
	 }
	 var url = "summary_team_stdsub.php?session=<? echo $session;?>&type="+type+"&team_id=" + team_id + "&emp_id=" + '<? echo $emp_id; ?>';
	 var arg = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,width=" + xsize + ",height=" + ysize ;
	 subwin_input = window.open ( url,"",arg );
	}
}

// SOAP参照子画面を開く。複数可
function openSOAP(){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=950,height=750";
	var url = "summary_team_flowsheet.php?session=<? echo $session;?>&pt_id=<? echo $pt_id;?>&page_num=0"+"&problem_id=" + problem_id ;
	subwin_soap = window.open(url, 'summary_team_frowsheet_window',option);
}

// 観察項目一覧子画面を開く。複数不可
function popupObservationList(org_id) {
	if ( !subwin_observ || subwin_observ.closed ){
		var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=380,height=300";
		subwin_observ = window.open('summary_observation_item_list.php?session=<? echo $session; ?>&mode=master&org_id='+org_id, 'observationWin', option);
	}
}

// 新規ボタン、ライブラリを開く
function newLibrary(){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=550,height=400";
	subwin_library = window.open("summary_stdplan_libcopy_list.php?session=<? echo $session; ?>&mode=master&problem_id="+problem_id+"&pt_id=<? echo $pt_id; ?>&emp_id=<? echo $emp_id;?>","library",option);
}

// DB更新(子画面からも呼ばれる)
function submitUpd(type){
	var elementIdCR = new Array("create0","create1","create2","create3");
	// 更新する項目
	document.standardform.updatetype.value=type;
	document.standardform.problem_id.value=problem_id;
	document.standardform.team_id.value=team_id;
	switch (type){
	case 4: // 左側
		document.standardform.leftdefval.value=document.getElementById("leftdefval").innerHTML;
		document.standardform.leftS.value=document.getElementById("leftS").innerHTML;
		document.standardform.leftE.value=document.getElementById("leftE").innerHTML;
		document.standardform.leftRisk.value=document.getElementById("leftRisk").innerHTML;
		document.standardform.leftO.value=document.getElementById("leftO").innerHTML;
		if ( document.getElementById("leftRcreater").innerHTML == "" ){ // 新規登録時は記載者を登録する
			document.standardform.creater.value=login_user_id;
		}else{
			document.standardform.creater.value="";
		}
		document.standardform.upd_date.value=now(2);
		break;
	case 41:// 左側観察項目
		// 観察一覧の項目を取得
		var elements = document.getElementsByClassName('observation_item');
		var idlist = new Array();
		for (var i = 0; i < elements.length; i++) {
			idlist[i]=elements[i].id;
		}
		document.standardform.leftObservation.value=idlist.toString();
		break;
	case 0: // 中央 共用
	case 1: // 中央 看護
	case 2: // 中央 介護
	case 3: // 中央 リハビリ
		if ( document.getElementById(elementIdCR[type]).innerHTML == "" ){ // 新規登録時は記載者を登録する
			document.standardform.centercreater.value=login_user_id;
		}else{
			document.standardform.centercreater.value="";
		}
		document.standardform.centerupd_date.value=now(2);
		break;
//	case 5:	// 右側 看護、FORMデータ設定済み
//	case 6:	// 右側 介護、FORMデータ設定済み
//	case 7:	// 右側 リハビリ、FORMデータ設定済み
//		break;
//	case 10:// ヘッダ、FORMデータ設定済み
//		break;
//	case 99: // 削除 設定するデータ無し
//		break;
	}
	document.standardform.action="summary_team_updexe.php";
	document.standardform.submit();
}

function deleteAll(){
	if( team_id == 0 ){
		alert("削除するチーム計画データはありません");
	}else{
		if(confirm("チーム計画を削除します")==true){
			document.standardform.problem_id.value=problem_id;
			document.standardform.mode.value="deleteall";
			document.standardform.action = 'summary_team_stdplan.php';
			document.standardform.submit();
		}
	}
}
function createExcel(){
    location.href = 'summary_team_stdplan_print.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&team_id=<? echo $team_id; ?>&type=excel';
}

function print(type) {
    var option = "directories=no,location=no,menubar=1,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=1,left=0,top=0,width=730,height=800";
    window.open('summary_team_stdplan_print.php?session=<? echo $session; ?>&pt_id=<? echo $pt_id; ?>&team_id=<? echo $team_id; ?>&type=' + type, 'stdPlanLibraryWin', option);
}

function main(){
	var mode = "<? echo $mode; ?>";
	if ( mode == "already" ){
		var a_date="<? echo $check_date; ?>";
		alert ( a_date + "は、すでにチーム計画が存在するか、他のユーザーにより登録されました\nプロブレムと日付を確認してください\nすでに登録されているチーム計画を表示します");
	}
	if ( mode == "noexsit" ){
		alert ( "このチーム計画は他の利用者により削除されました");
		window.close();
	}
	if ( mode == "deleteall" ){
		var lock_name = new Array();
<?
for( $i=0; $i<count($lock_name); $i++ ){
	echo "lock_name[".$i."]='".$lock_name[$i]."';";
}
?>

		var lock_cnt = <? echo count($lock_name); ?>;
		if ( lock_cnt == 0 ){
			submitUpd(99);
		}else{
			var mess="";
			for(var i=0; i<lock_cnt; i++){
				mess = mess+lock_name[i] +"さん\n";
			}
			mess = mess + "が使用中です。\n削除しますか";
			if ( confirm( mess ) ){
				submitUpd(99);
			}
		}
	}
}

</SCRIPT>
</HEAD>

<BODY OnLoad="javascript:main();">

<DIV STYLE="padding:4px">

<!-- ヘッダエリア -->
<? echo  summary_common_show_dialog_header("チーム計画"); ?>

<TABLE WIDTH="100%" CLASS="list_table" STYLE="margin-top:4px"><TR><TD><!-- 外枠 -->

<FORM NAME="standardform" METHOD="POST" ACCEPT-CHARSET="euc-jp,us-ascii">

<!-- 冒頭部 -->
 <TABLE WIDTH="100%" BORDER="0"> 
  <TR><TD><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>患者ID:<? echo $pt_id; ?>&nbsp&nbsp&nbsp患者氏名:&nbsp<? echo $kname; ?>&nbsp</B></font></TD></TR>
  <TR><TD STYLE="height:5px;"></TD></TR>
  <TR><TD><B>プロブレム</B></font></TD></TR>
  <TR><TD><? echo $disp_problem_list; ?></TD></TR>
  <TR><TD STYLE="height:5px;"></TD></TR>
 </TABLE>
<!--INPUT TYPE="BUTTON" VALUE="再表示" ONCLICK="javascript:reStart();"-->
<TABLE WIDTH="100%" BORDER="0"><TR>

<TD WIDTH="25%">
立案年月日&nbsp<? echo $create_date_list;?>
</TD>
<TD WIDTH="40%" ONCLICK="javascript:subWin( 10 );">
&nbsp&nbsp
&nbsp責任者&nbsp
&nbsp職種<INPUT TYPE="TEXT" NAME="creater_job" SIZE="8" MAXLENGTH="20" value="<? echo $creater_job; ?>" READONLY>
&nbsp氏名<INPUT TYPE="TEXT" NAME="creater_name" SIZE="20" MAXLENGTH="40" value="<? echo $creater_name; ?>" READONLY>
</TD>
<TD><INPUT TYPE="BUTTON" VALUE="削除" ONCLICK="javascript:deleteAll();"></TD>
<TD ALIGN="RIGHT"><INPUT TYPE="BUTTON" VALUE="新規" OnClick="javascript:newLibrary();"></TD>
<TD><INPUT TYPE="BUTTON" VALUE="エクセル出力" ONCLICK="javascript:createExcel();"></TD>
<TD><INPUT TYPE="BUTTON" VALUE="印刷" ONCLICK="javascript:print('pdf');"></TD>
</TR>
</TABLE>

<TABLE WIDTH="90%" CLASS="list_table" STYLE="margin-top:4px">
<TR><TH WIDTH="38%" ALIGN="CENTER">患者の問題及びアウトカム</TH><TH WIDTH="38%" ALIGN="CENTER">チーム実践</TH><TH WIDTH="24%" ALIGN="CENTER">評価日／解決・継続／サイン</TH></TR>

<!-- 左 -->

<TR><TD VALIGN="TOP" BGCOLOR="#efefef">
<TABLE><TR><TD BGCOLOR="#efefef">コメント:</TD><TD ID="leftdeftitle" BGCOLOR="#efefef"><? echo $leftdeftitle;?></TD><TD BGCOLOR="#efefef"><INPUT TYPE="BUTTON" VALUE="修正" ONCLICK="javascript:subWin( 4 );"></TD></TR></TABLE>
<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="leftdefval" BGCOLOR="#fafafa"><? echo $deftdef_value;?></TD></TR></TABLE><BR>
S:症状・兆候	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="leftS" BGCOLOR="#fafafa"><? echo $leftS;?></TD></TR></TABLE><BR>
E:原因・関連因子<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="leftE" BGCOLOR="#fafafa"><? echo $leftE;?></TD></TR></TABLE><BR>
危険因子	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="leftRisk" BGCOLOR="#fafafa"><? echo $leftRisk;?></TD></TR></TABLE><BR>
アウトカム	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="leftO" BGCOLOR="#fafafa"><? echo $leftO;?></TD></TR></TABLE><BR>
観察項目&nbsp<INPUT TYPE="BUTTON" VALUE="行追加" ONCLICK="popupObservationList();">&nbsp<INPUT TYPE="BUTTON" VALUE="削除" ONCLICK="javascrpit:deleteObservationItem()">
<TABLE CLASS="list_table" STYLE="margin-top:4px">
 <TR><TD ID="left_observation" BGCOLOR="#fafafa"><? echo $observation_items;?></TD></TR>
</TABLE>
<!--記載者--><TABLE><TR><TD BGCOLOR="#efefef">記載者:</TD><TD ID="leftRcreater" BGCOLOR="#efefef"><? echo $leftCreater;?></TD></TR></TABLE>
<!--更新者--><TABLE><TR><TD ID="leftRupdater" BGCOLOR="#efefef"><? echo $dispLeftRec;?></TD></TR></TABLE>
<BR><BR>
</TD>

<!-- 中 -->
<?
// 当初の仕様では、共用、看護、介護、リハビリの4職種についての項目設定でしたが
// 当面は職種別分けをしないことになりました
// これらを復活させる場合コメントを外し、＜共用＞の文字列を復活させてください
// 入力サブウィンドウsummary_team_stdsub.phpも修正してください 2012.8.1
?>


<TD VALIGN="TOP" BGCOLOR="#ffdfdf">
<TABLE WIDTH="100%" HIEGHT="100%" BORDER="0"><TR><TD BGCOLOR="#ffdfdf">
<!--＜共用＞&nbsp--><INPUT TYPE="BUTTON" VALUE="修正" ONCLICK="javascript:subWin( 0 );"><BR>
O-P:観察計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="op0" BGCOLOR="#ffefef"><? echo $centerOP[0];?></TD></TR></TABLE><BR>
T-P:実践計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="tp0" BGCOLOR="#ffefef"><? echo $centerTP[0];?></TD></TR></TABLE><BR>
E-P:教育・指導計画	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="ep0" BGCOLOR="#ffefef"><? echo $centerEP[0];?></TD></TR></TABLE>
<!--記載者-->		<TABLE><TR><TD BGCOLOR="#ffdfdf">記載者:<TD ID="create0" BGCOLOR="#ffdfdf"><? echo $centerCreater[0];?></TD></TR></TABLE>
<!--更新者-->		<TABLE><TR><TD ID="upd0" BGCOLOR="#ffdfdf"><? echo $centerRec[0];?></TD></TR></TABLE>
<BR>
</TD></TR></TABLE>

<!--- ここから下は将来機能▼ >
<--
<TABLE WIDTH="100%" BORDER="0"><TR><TD BGCOLOR="#dfffdf">
＜看護＞<INPUT TYPE="BUTTON" VALUE="修正" ONCLICK="javascript:subWin( 1 );"><BR>
O-P:観察計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="op1" BGCOLOR="#efffef"><? echo $centerOP[1];?></TD></TR></TABLE><BR>
T-P:実践計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="tp1" BGCOLOR="#efffef"><? echo $centerTP[1];?></TD></TR></TABLE><BR>
E-P:教育・指導計画	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="ep1" BGCOLOR="#efffef"><? echo $centerEP[1];?></TD></TR></TABLE>
<--記載者>		<TABLE><TR><TD BGCOLOR="#dfffdf">記載者:<TD ID="create1" BGCOLOR="#dfffdf"><? echo $centerCreater[1];?></TD></TR></TABLE>
<--更新者>		<TABLE><TR><TD ID="upd1" BGCOLOR="#dfffdf"><? echo $centerRec[1];?></TD></TR></TABLE>
<BR>
</TD></TR></TABLE>

<TABLE WIDTH="100%" BORDER="0"><TR><TD BGCOLOR="#dfdfff">
＜介護＞<INPUT TYPE="BUTTON" VALUE="修正" ONCLICK="javascript:subWin( 2 );"><BR>
O-P:観察計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="op2" BGCOLOR="#efefff"><? echo $centerOP[2];?></TD></TR></TABLE><BR>
T-P:実践計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="tp2" BGCOLOR="#efefff"><? echo $centerTP[2];?></TD></TR></TABLE><BR>
E-P:教育・指導計画	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="ep2" BGCOLOR="#efefff"><? echo $centerEP[2];?></TD></TR></TABLE>
<--記載者>		<TABLE><TR><TD BGCOLOR="#dfdfff">記載者:</TD><TD ID="create2" BGCOLOR="#dfdfff"><? echo $centerCreater[2];?></TD></TR></TABLE>
<--更新者>		<TABLE><TR><TD ID="upd2" BGCOLOR="#dfdfff"><? echo $centerRec[2];?></TD></TR></TABLE>
<BR>
</TD></TR></TABLE>

<TABLE WIDTH="100%" BORDER="0"><TR><TD BGCOLOR="#ffffdf">
＜リハビリ＞		<INPUT TYPE="BUTTON" VALUE="修正" ONCLICK="javascript:subWin( 3 );"><BR>
O-P:観察計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="op3" BGCOLOR="#ffffef"><? echo $centerOP[3];?></TD></TR></TABLE><BR>
T-P:実践計画		<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="tp3" BGCOLOR="#ffffef"><? echo $centerTP[3];?></TD></TR></TABLE><BR>
E-P:教育・指導計画	<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD ID="ep3" BGCOLOR="#ffffef"><? echo $centerEP[3];?></TD></TR></TABLE>
<--記載者>		<TABLE><TR><TD BGCOLOR="#ffffdf">記載者:</TD><TD ID="create3" BGCOLOR="#ffffdf"><? echo $centerCreater[3];?></TD></TR></TABLE>
<--更新者>		<TABLE><TR><TD ID="upd3" BGCOLOR="#ffffdf"><? echo $centerRec[3];?></TD></TR></TABLE>
<BR>
</TD></TR></TABLE>

<-- 将来機能ここまで▲-->

</TD>

<!-- 右 -->
<?
// 当初の仕様では、看護、介護、リハビリの3職種についての項目設定でしたが
// 当面は職種別をしないことになりました
// これらを復活させる場合コメントを外し、＜看護＞の文字列を復活させてください
// 入力サブウィンドウsummary_team_stdsub.phpも修正してください 2012.8.1
?>

<TD VALIGN="TOP" BGCOLOR="#efefef">
<!--＜看護＞<BR-->
<INPUT TYPE="BUTTON" VALUE="行追加" ONCLICK="javascript:subWin(5);">&nbsp<INPUT TYPE="BUTTON" VALUE="ＳＯＡＰ参照" ONCLICK="javascript:openSOAP();">
<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD BGCOLOR="#efffef"><? echo $ass_disp_list[1];?></TD></TR></TABLE><BR>

<!--- ここから下は将来機能▼ >

＜介護＞<BR>
<INPUT TYPE="BUTTON" VALUE="行追加" ONCLICK="javascript:subWin(6);">&nbsp<INPUT TYPE="BUTTON" VALUE="ＳＯＡＰ参照" ONCLICK="javascript:openSOAP();">
<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD BGCOLOR="#dfdfff"><? echo $ass_disp_list[2];?></TD></TR></TABLE><BR>

＜リハビリ＞<BR>
<INPUT TYPE="BUTTON" VALUE="行追加" ONCLICK="javascript:subWin(7);">&nbsp<INPUT TYPE="BUTTON" VALUE="ＳＯＡＰ参照" ONCLICK="javascript:openSOAP();">
<TABLE CLASS="list_table" STYLE="margin-top:4px"><TR><TD BGCOLOR="#ffffdf"><? echo $ass_disp_list[3];?></TD></TR></TABLE><BR>
<-- 将来機能ここまで▲-->

</TD></TR></TABLE>

<!--全体-->
<INPUT TYPE="hidden" NAME="session" VALUE="<? echo $session;?>">
<INPUT TYPE="hidden" NAME="emp_id" VALUE="<? echo $emp_id;?>">
<INPUT TYPE="hidden" NAME="updatetype" VALUE="">
<INPUT TYPE="hidden" NAME="updatemode" VALUE="">
<INPUT TYPE="hidden" NAME="problem_id" VALUE="">
<INPUT TYPE="hidden" NAME="nextdate" VALUE="">
<INPUT TYPE="hidden" NAME="mode" VALUE="">
<INPUT TYPE="hidden" NAME="sel_other_problem" VALUE="">
<INPUT TYPE="hidden" NAME="createdate" VALUE="">
<INPUT TYPE="hidden" NAME="pt_id" VALUE="<? echo $pt_id;?>"><!--患者ID-->
<INPUT TYPE="hidden" NAME="team_id" VALUE=""><!--チーム計画マスターのID-->
<!--左側-->
<INPUT TYPE="hidden" NAME="leftdeftitle">
<INPUT TYPE="hidden" NAME="leftdefval">
<INPUT TYPE="hidden" NAME="leftS">
<INPUT TYPE="hidden" NAME="leftE">
<INPUT TYPE="hidden" NAME="leftRisk">
<INPUT TYPE="hidden" NAME="leftO">
<INPUT TYPE="hidden" NAME="leftObservation">
<INPUT TYPE="hidden" NAME="creater">
<INPUT TYPE="hidden" NAME="emplist">
<INPUT TYPE="hidden" NAME="upd_date">
<!--中央-->
<INPUT TYPE="hidden" NAME="OP">
<INPUT TYPE="hidden" NAME="TP">
<INPUT TYPE="hidden" NAME="EP">
<INPUT TYPE="hidden" NAME="centercreater">
<INPUT TYPE="hidden" NAME="centeremplist">
<INPUT TYPE="hidden" NAME="centerupd_date">
<!--右側-->
<INPUT TYPE="hidden" NAME="rightdate">
<INPUT TYPE="hidden" NAME="rightvalu">

</FORM>
<SPAN ID="debug"></SPAN><? echo $DEBUG;?>
</BODY>
</HTML>
<?
pg_close($con);
?>
