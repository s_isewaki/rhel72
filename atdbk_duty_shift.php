<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 勤務シフト希望／参照</title>

<?
//atdbk_duty_shift.php
require_once("about_comedix.php");

require_once("atdbk_menu_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_menu_common_class.php");
require_once("get_menu_label.ini");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_menu = new duty_shift_menu_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
$checkauth = check_authority($session, 5, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// 勤務管理権限の取得
///-----------------------------------------------------------------------------
$work_admin_auth = check_authority($session, 42, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//現在日付の取得
	///-----------------------------------------------------------------------------
	$date = getdate();
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	$now_yyyymm = sprintf("%04d%02d", $now_yyyy, $now_mm);
	///-----------------------------------------------------------------------------
	//翌月
	///-----------------------------------------------------------------------------
	if ((int)$now_mm == 12){
		$next_yyyy = (int)$now_yyyy + 1;
		$next_mm = 1;
	} else {
		$next_yyyy = $now_yyyy;
		$next_mm = (int)$now_mm + 1;
	}
	$next_dd = $now_dd;
	$next_yyyymm = sprintf("%04d%02d", $next_yyyy, $next_mm);


	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	$data_st = $obj->get_stmst_array();
	$data_job = $obj->get_jobmst_array();
	$data_wktmgrp = $obj->get_wktmgrp_array();
//20140210
$err_msg_1 = "";
$err_msg_2 = "";
$err_msg_3 = "";

///-----------------------------------------------------------------------------
	// 勤務シフトスタッフ情報を取得（ログインユーザの存在するグループ）
	///-----------------------------------------------------------------------------
        $group_id = $obj->get_group_id_from_duty_shift_staff($emp_id);
		if ($group_id == ""){
			$err_msg_1 = "{$shift_menu_label}の職員設定で未登録職員のため利用できません。管理者に連絡してください。";
		}
    
	///-----------------------------------------------------------------------------
	// 勤務シフトグループ情報を取得
	// ログインユーザの存在するグループIDを取得
	///-----------------------------------------------------------------------------
	$pattern_id = "";
	if ($group_id != ""){
		//勤務シフトグループ情報ＤＢより情報取得
		$group_array = $obj->get_duty_shift_group_array($group_id, "", $data_wktmgrp);
		if (count($group_array) <= 0) {
			$err_msg_2 = "勤務シフトグループ情報が未設定です。管理者に連絡してください。";
		} else {
			$pattern_id = $group_array[0]["pattern_id"];
			$group_close_day = $group_array[0]["close_day"];
			$start_month_flg1 = $group_array[0]["start_month_flg1"];
			$start_day1 = $group_array[0]["start_day1"];
			$month_flg1 = $group_array[0]["month_flg1"];
			$end_day1 = $group_array[0]["end_day1"];
			$paid_hol_disp_flg = $group_array[0]["paid_hol_disp_flg"];    //有給休暇残日数表示フラグ
            $unit_disp_flg = $group_array[0]["unit_disp_flg"];
        }
	}
	if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
	if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
	if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
	if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
	//初期表示を翌月にする。締切り後の場合は翌々月
	$add_mm = 1;
	if ($group_close_day != "") {
		if ((int)$now_dd > (int)$group_close_day) {
			$add_mm = 2;
		}
	}
    $hope_flg = false;
///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
// 参照の場合
if ($refer_flg == "1") {
    //初期表示時、当日が範囲内に入る月度とする 20140723
    if ($duty_yyyy == "" && $duty_mm == "") {
        $duty_yyyy = $now_yyyy; 
        $duty_mm = $now_mm; 
        $arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
        $today = date("Ymd");
        //当日が範囲より前の場合は、前月とする
        if ($today < $arr_date[0]) {
            $duty_mm--;
            if ($duty_mm < 1) {
                $duty_yyyy--;
                $duty_mm = 12;
            }
        } else if ($today > $arr_date[1]) {
            //当日が範囲より後の場合は、翌月とする
            $duty_mm++;
            if ($duty_mm > 12) {
                $duty_yyyy++;
                $duty_mm = 1;
            }
        }
    }    
} else {
    // 希望登録の場合、翌月、翌々月にする
    if ($duty_yyyy == "") {
        $hope_flg = true;
        $duty_yyyy = $now_yyyy;
        $duty_mm = $now_mm + $add_mm;
        if ($duty_mm >= 13) {
            $duty_yyyy = $duty_yyyy + 1;
            $duty_mm = $duty_mm - 12;
        }
    }
}

	$duty_yyyymm = sprintf("%04d%02d", $duty_yyyy, $duty_mm);
    
//当月以前の場合、duty_shift_plan_staff年月毎のシフト一覧に登録されているか確認 20140210
$curr_yyyymm = date("Ym");
if ($err_msg_1 == "" && ($duty_yyyymm <= $curr_yyyymm)) {
    $cnt = $obj->check_duty_shift_plan_staff($group_id, $emp_id, $duty_yyyy, $duty_mm);
    if ($cnt == 0) {
        $err_msg_1 = "指定されたシフトグループにあなたの予実績データはありません。";
    }
} 

	///-----------------------------------------------------------------------------
	//各変数値の初期値設定
	///-----------------------------------------------------------------------------
	$staff_del_flg = "";		//スタッフ削除フラグ（１：削除）
	if ($refer_flg == "1") {	//参照画面の場合
		$highlight_flg = "";	//強調表示フラグ（１：強調表示）
	} else {
		$highlight_flg = "1";	//強調表示フラグ（１：強調表示）
	}
	$check_flg = "";			//警告行表示フラフ（１：表示）
	$group_show_flg = "";		//シフトグループ表示フラグ（１：表示）
	$individual_flg = "";		//希望ＤＢフラグ（１：希望）
	$hope_get_flg = "";			//希望取り込みフラグ（１：取り込み）

	if ($data_cnt == "") { $data_cnt = 0; }
	//入力形式で表示する日（開始／終了）
	if ($edit_start_day == "") { $edit_start_day = 0; }
	if ($edit_end_day == "") { $edit_end_day = 0; }

	$show_data_flg = "";		//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直）

	//表示行数
	$show_gyo_cnt = 1;
	if ($plan_results_flg == "2") {
		$show_gyo_cnt = 2;
		$show_data_flg = "1";
	}
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
	///-----------------------------------------------------------------------------
	// カレンダー(calendar)情報を取得
	///-----------------------------------------------------------------------------
	$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
	$start_date = $arr_date[0];
	$end_date = $arr_date[1];
	$calendar_array = $obj->get_calendar_array($start_date, $end_date);
	//日数
	$day_cnt = count($calendar_array);
	///-----------------------------------------------------------------------------
	//指定月の全曜日を設定
	///-----------------------------------------------------------------------------
	$week_array = array();
	for ($k=1; $k<=$day_cnt; $k++) {
		$wk_date = $calendar_array[$k-1]["date"];
		$wk_yyyy = substr($wk_date, 0, 4);
		$wk_mm = substr($wk_date, 4, 2);
		$wk_dd = substr($wk_date, 6, 2);
		$tmp_date = mktime(0, 0, 0, $wk_mm, $wk_dd, $wk_yyyy);
		$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
		if (($week_array[$k]["name"] != "土") &&
			($week_array[$k]["name"] != "日")) {
			if (($calendar_array[$k - 1]["type"] == "4") ||
				($calendar_array[$k - 1]["type"] == "5") ||
				($calendar_array[$k - 1]["type"] == "6")) {
				$week_array[$k]["type"] = "6";
			}
		}
	}
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	// 勤務シフト希望／勤務シフト参照　判定
	///-----------------------------------------------------------------------------
	$create_flg = "1";
	if ($refer_flg == "1") {
		$create_flg = "";
	}

	///-----------------------------------------------------------------------------
	//ＤＢ(atdptn)より出勤パターン情報取得
	//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
	///-----------------------------------------------------------------------------
	$data_atdptn = $obj->get_atdptn_array($pattern_id);
    //20140220 start
	$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
    //応援追加されている出勤パターングループ確認 
    $arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, array(), $pattern_id);
    $pattern_id_cnt = count($arr_pattern_id);
    if ($pattern_id_cnt > 0) {
        $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
        $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
        $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
        $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
    }
    //応援なしの場合、同じパターングループのデータを設定
    else {
	    $data_atdptn_all = $data_atdptn;
	    $data_pattern_all = $data_pattern;
    }
    //20140220 end
	///-----------------------------------------------------------------------------
	// 締切チェック用
	///-----------------------------------------------------------------------------
	$close_ng_flg = "";
	if ((int)$now_yyyymm >= (int)$duty_yyyymm) {
		$close_ng_flg = "1";
	} else {
		if ($group_close_day != "") {
		if (((int)$next_yyyymm == (int)$duty_yyyymm) &&
			((int)$now_dd > (int)$group_close_day)) {
			$close_ng_flg = "1";
		}
		}
	}
	///-----------------------------------------------------------------------------
	// 勤務シフト情報（希望）を取得
	///-----------------------------------------------------------------------------
	//参照時は希望でなく勤務シフトを表示
	if ($refer_flg == "") {
		$individual_flg = "1";	//１：勤務シフトＤＢ（希望）
	}
	///-----------------------------------------------------------------------------
	// 勤務シフト情報を取得
	///-----------------------------------------------------------------------------
	$plan_individual_array = array();
	$set_data = array();
	$wk_flg = "";
	if ($group_id != ""){
		///-----------------------------------------------------------------------------
		// 勤務シフト情報を取得判定（新規表示、再表示）
		///-----------------------------------------------------------------------------
		if (($data_cnt > 0) &&
			($group_id == $cause_group_id) &&
			($duty_yyyy == $cause_duty_yyyy) &&
			($duty_mm == $cause_duty_mm)) {
			$wk_flg = "1";
		} else {
			$edit_start_day = 0;
			$edit_end_day = 0;
		}
		///-----------------------------------------------------------------------------
		//再表示時
		///-----------------------------------------------------------------------------
		if ($wk_flg == "1") {
			for($i=0;$i<$data_cnt;$i++) {
				//応援追加情報
				$assist_str = "assist_group_$i";
				$arr_assist_group = split(",", $$assist_str);
				///-----------------------------------------------------------------------------
				// 再表示時、データ再設定
				///-----------------------------------------------------------------------------
				for($k=1;$k<=$day_cnt;$k++) {
					$set_data[$i]["plan_rslt_flg"] = "1";				//１：実績は再取得

					$wk2 = "pattern_id_$i" . "_" . $k;
					$wk3 = "atdptn_ptn_id_$i" . "_" . $k;
					$wk6 = "reason_2_$i" . "_" . $k;

					$set_data[$i]["staff_id"] = $staff_id[$i];					//職員ＩＤ
					$set_data[$i]["pattern_id_$k"] = $$wk2;						//出勤グループＩＤ

					if ((int)substr($$wk3,0,2) > 0) {
						$set_data[$i]["atdptn_ptn_id_$k"] = (int)substr($$wk3,0,2);	//出勤パターンＩＤ
					}
					if ((int)substr($$wk3,2,2) > 0) {
						$set_data[$i]["reason_$k"] = (int)substr($$wk3,2,2);	//事由
					}
					$set_data[$i]["reason_2_$k"] = $$wk6;	//事由（午前有給／午後有給）
					//応援情報
					$set_data[$i]["assist_group_$k"] = $arr_assist_group[$k - 1];
				}
			}
		}
		///-----------------------------------------------------------------------------
		// 勤務シフト情報を取得
		///-----------------------------------------------------------------------------
		$plan_individual_array = $obj->get_duty_shift_plan_array(
											$group_id, $pattern_id,
											$duty_yyyy, $duty_mm, $day_cnt,
											$individual_flg,
											$hope_get_flg,
											$show_data_flg,
											$set_data, $week_array, $calendar_array,
											$data_atdptn, $data_pattern_all,
											$data_st, $data_job, $data_emp,
											$emp_id);
		$data_cnt = count($plan_individual_array);
	}
	///-----------------------------------------------------------------------------
	// 勤務実績情報(atdbkrslt)を取得
	///-----------------------------------------------------------------------------
	$rslt_array = array();
	$rslt_cnt = 0;
	if ($group_id != ""){
		//ＳＱＬ
		$sql = "select * from atdbkrslt";
		$cond = "where date >= '$start_date' and date <= '$end_date' ";
		$cond .= "and emp_id = '$emp_id' ";
		$cond .= "order by emp_id, date";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		$today = Date("Ymd");
		//データ取得
		for ($i=0;$i<$num;$i++) {
			$wk_pattern = pg_result($sel,$i,"pattern");
			$wk_start_time = pg_result($sel,$i,"start_time");
			$wk_date = pg_result($sel,$i,"date");
			// 休暇以外
			if (trim($wk_pattern) != "10") {
				// 出勤時刻がある場合に日別の勤務実績ありとする
				if ($wk_start_time != "") {
					$rslt_array[$rslt_cnt]["date"] = $wk_date;
					$rslt_cnt++;
				}
			} else {
			// 休暇の場合
				// 当日分までを実績ありとする
				if ($wk_date <= $today) {
					$rslt_array[$rslt_cnt]["date"] = $wk_date;
					$rslt_cnt++;
				}
			}
		}
	}
	///-----------------------------------------------------------------------------
	// 行タイトル（行ごとの集計する勤務パターン）を取得
	// 列タイトル（列ごとの集計する勤務パターン）を取得
	///-----------------------------------------------------------------------------
	$title_gyo_array = array();
	$title_retu_array = array();
	if ($group_id != ""){
		$title_gyo_array = $obj->get_total_title_array($pattern_id,	"1", $data_pattern);
		$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
		if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
			$err_msg_3 = "勤務シフト記号情報が未設定です。管理者に連絡してください。";
		}
	}
	///-----------------------------------------------------------------------------
	//個人日数合計（行）を算出設定
	//個人日数合計（列）を算出設定
	///-----------------------------------------------------------------------------
	//2段目が実績の場合、システム日以降は、行を集計しない。
	if ($show_data_flg == "1") {
		$wk_rs_flg = "2";
	} else {
		$wk_rs_flg = "";
	}
	$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
							"1", $wk_rs_flg, $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
	$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
							"2", "", $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

	$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
//休暇事由情報
	$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);

// 登録子画面の指定情報保存
	$obj->update_last_ptn($emp_id, $last_ptn_id, $last_reason_2, $last_copy_cnt);

//希望情報取得、予定背景色変更用 20120403
if ($refer_flg == "1") {
    $plan_individual = $obj->get_plan_individual($group_id, $pattern_id, $start_date, $end_date);
}
else {
    $plan_individual = array();
}

//-------------------------------------------------------------------------------
// 有給休暇残日数を取得
//-------------------------------------------------------------------------------
$paid_hol_all_array = array();
if ($paid_hol_disp_flg === "t") {
    // 希望ＤＢフラグにより、対象となるシフトデータ種別を決定("plan":シフト予定 "hope":シフト希望)
    $plan_type = ($individual_flg == "1") ? "hope" : "plan";
    $paid_hol_all_array = $obj->get_paid_hol_rest_array($plan_individual_array, $calendar_array, $duty_yyyy, $duty_mm, $plan_type);
}

//スタンプ対応 20140225 start
// 勤務記号の初期値
$def_ptn_info=$obj->get_auto_default_ptn_info($pattern_id);
if ($def_ptn_info["reason"] == " ") {
	$def_ptn_info["reason"] = "";
}//JavaScriptで使用するpatern_id
$wk_pattern_id = ($pattern_id == "") ? "0" : $pattern_id;
//スタンプ対応 20140225 end

///-----------------------------------------------------------------------------
// ユニット表示仕変 20140110
//$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------
;
// ユニット（３階層名）表示のため追加
//$arr_class_name = get_class_name_array($con, $fname);
//$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

//スタンプ対応 20140205 start
//勤務シフト希望
if ($refer_flg == "") {
	//初期時
	if ($stamp_flg == "") {
		//締切前
		if ($close_ng_flg == "") {
			$stamp_flg = "1"; //スタンプ表示
		}
	}
	//登録後再表示
	elseif ($stamp_flg == "2") {
		$stamp_flg = ""; //スタンプ非表示
	}
}
//勤務シフト参照
else {
	$stamp_flg = "";
}
//スタンプ対応 20140205 end
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<?php
include_js("js/fontsize.js");
include_js("js/jquery/jquery-1.7.2.min.js");
include_js("js/browser-support.js");
include_js("js/duty_shift/atdbk_duty_shift.js"); //results.js
//勤務パターン数によってフローティングウインドウ用の画像サイズを調整
$floatWinCnt=0;
$floatWinRow=1;
for ($k=0;$k<count($data_pattern_all);$k++) {
	if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&	$data_pattern_all[$k]["font_name"] != "" &&
			$data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20130219
		$floatWinCnt++;
	}
}

//スタンプ用ウインドウサイズ
//件数に応じた大きさにする 20130219 start
//ウィンドウの高さ＝上下の余白＋行数＊高さ22、件数が少ない場合でも最低限3行分とする、$floatWinCntへの+1は未設定分
$wk_row = ceil(($floatWinCnt+1) / 15);
if ($wk_row < 3) {
    $wk_row = 3;
}
$floatWinHeiStp = 44 + ($wk_row * 22);

if($floatWinCnt > 7){
    $floatWinWidStp=800;
}else{
	$floatWinWidStp=500;
}
//件数に応じた大きさにする 20130219 end

//スタンプ用ウインドウ位置
if ($stamp_flg != "1"){
	$floatWinTopStp=-1000;
	$floatWinLeftStp=-1000;
}else{
	$floatWinTopStp=230;
	$floatWinLeftStp=350;
	$floatWinDisStp='';
}

?>
<style type="text/css">
#floatWindowStp{
	display:<?=$floatWinDisStp?>;
	position:absolute;
	top:<?=$floatWinTopStp?>px;
	left:<?=$floatWinLeftStp?>px;
	width:<?=$floatWinWidStp?>px;
	height:<?=$floatWinHeiStp?>px;
	/*filter:alpha(opacity=90);*/ /*IE*/
    /*-moz-opacity:0.9;*/ /*FF*/
    /*opacity:0.9;*/ /*OTHER*/
}
#floatWindowStp dl{
	width:<?=$floatWinWidStp?>px;
	height:<?=$floatWinHeiStp?>px;
	background:url("./images/floatWindow_g.gif");
	margin:1;
}
</style>

<script type="text/javascript">
var change_switch = <?=$obj->GetSumCustomSwith($pattern_id);?>;
var count_row = <? eh("0"); // 勤務実績入力に合わせるため ?>;
var childwin_rslt = null;

	///-----------------------------------------------------------------------------
	//登録
	///-----------------------------------------------------------------------------
	function editData() {
		//登録
		if (confirm('「登録」します。よろしいですか？')) {
			document.mainform.action="atdbk_duty_shift_update_exe.php";
			document.mainform.submit();
		}
	}
	///-----------------------------------------------------------------------------
	//予定／実績表示
	///-----------------------------------------------------------------------------
	function showPlanResults(flg) {
		//予定／実績表示
		document.mainform.edit_start_day.value = 0;
		document.mainform.edit_end_day.value = 0;
		document.mainform.plan_results_flg.value = flg;
		if (document.mainform.stamp_flg.value == "") { // スタンプ非表示 20140214
			document.mainform.stamp_flg.value = "2";
		}
		document.mainform.action="atdbk_duty_shift.php?session=<?=$session?>";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	//印刷
	///-----------------------------------------------------------------------------
	function dataPrint(total_print_flg, pdf_print_flg) {
		//印刷ＷＩＮＤＯＷをＯＰＥＮ
		dx = screen.width;
		dy = screen.top;
		base_left = 0;
		base = 0;
		wx = 1100;
		wy = 600;
		childwin_print = window.open('', 'printFormChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		document.printForm.action = "atdbk_duty_shift_print.php";
		document.printForm.target = "printFormChild";
		document.printForm.total_print_flg.value = total_print_flg;
		document.printForm.pdf_print_flg.value = pdf_print_flg;
		document.printForm.submit();
	}

	///-----------------------------------------------------------------------------
	//印刷選択子画面
	///-----------------------------------------------------------------------------
	function printSelect() {
		wx = 300;
		wy = 165;
		dx = screen.width / 2 - (wx / 2);
		dy = screen.top;
		base = screen.height / 2 - (wy / 2);
		var url = 'duty_shift_menu_print_select.php';
		url += '?session=<?=$session?>';
		url += '&atdbk_flag=TRUE';
		childwin_print_select = window.open(url, 'printSelectPopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_print_select.focus();
	}
	///-----------------------------------------------------------------------------
	//指定日された選択入力可能にする
	///-----------------------------------------------------------------------------
	function showEdit(start_day, end_day) {
		try {
			var day_cnt = document.mainform.day_cnt.value;
			///-----------------------------------------------------------------------------
			//参照表示時
			///-----------------------------------------------------------------------------
			if (document.mainform.refer_flg.value == "1") {
				return;
			}
			///-----------------------------------------------------------------------------
			//チェック（締切日までは、翌月以降を登録できる。）
			///-----------------------------------------------------------------------------
			var close_ng_flg = document.mainform.close_ng_flg.value;
			if (close_ng_flg == "1") {
				alert('締切り日を過ぎているため、変更はできません。');
				return;
			}
			///-----------------------------------------------------------------------------
			//実績が存在する場合、入力不可
			///-----------------------------------------------------------------------------
			//日別チェック
			if ((start_day == end_day) && (start_day > 0) && (end_day > 0)) {
				i = start_day - 1;
				var wk = document.mainform.elements['rslt_flg[]'][i].value;
				if (wk == "1") {
					alert('既に指定日には実績入力されているため、入力出来ません。');
					return;
				}
			}
			//週入力チェック
			if ((start_day != end_day) && (start_day > 0) && (end_day > 0)) {
				for (k=1; k<=day_cnt; k++) {
					i = k - 1;
					var wk = document.mainform.elements['rslt_flg[]'][i].value;
					if ((k >= start_day) && (k <= end_day) && (wk == "1")) {
						alert('指定週に実績入力された日が存在するため週入力はできません。日別入力を行ってください。');
						return;
					}
				}
			}
			///-----------------------------------------------------------------------------
			//指定日された選択入力可能にする
			///-----------------------------------------------------------------------------
			if ((document.mainform.edit_start_day.value == start_day) &&
				(document.mainform.edit_end_day.value == end_day)) {
				//表示のみ
				document.mainform.edit_start_day.value = 0;
				document.mainform.edit_end_day.value = 0;
			} else {
				//選択入力可能
				document.mainform.edit_start_day.value = start_day;
				document.mainform.edit_end_day.value = end_day;
			}
			document.mainform.action="atdbk_duty_shift.php?session=<?=$session?>";
			document.mainform.submit();
		}
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	// 勤務予定入力画面（ＯＰＥＮ）
	///-----------------------------------------------------------------------------
	function openEditWindow(emp_id) {
		dx = screen.width;
		dy = screen.top;
//		base_left = (dx-wx) / 2
//		base_left = (dx-wx);
		base_left = 0;
		base = 0;
		wx = 1100;
		wy = 600;

		var url = './atdbk_duty_shift_edit.php';
		url += '?session=<?=$session?>';
		url += '&emp_id='+emp_id;
		url += '&duty_shift_flg=1';
		childwin_rslt = window.open(url, 'esltEditWindow', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_rslt.focus();

	}
	///-----------------------------------------------------------------------------
	// 勤務予定入力画面（ＣＬＯＳＥ）
	///-----------------------------------------------------------------------------
	function closeEditWindow() {
		if (childwin_rslt != null && !childwin_rslt.closed) {
			childwin_rslt.close();
		}
		childwin_rslt = null;
	}
	///-----------------------------------------------------------------------------
	// 勤務予定入力画面よりの再表示指示
	///-----------------------------------------------------------------------------
	function show_refresh() {
		document.mainform.data_cnt.value = 0;
		document.mainform.action="duty_shift_results.php?session=<?=$session?>";
		document.mainform.submit();
	}
	///-----------------------------------------------------------------------------
	//希望取り込みデータ変更チェック（ダミー）
	///-----------------------------------------------------------------------------
	function chk_individual(gyo, retu) {
		return;
	}

	///-----------------------------------------------------------------------------
	//行／列ハイライト
	///-----------------------------------------------------------------------------
	function highlightCells(class_name, gyo, retu) {
		changeCellsColor(class_name, '#ffff66',gyo,retu);
	}
	function dehighlightCells(class_name, gyo, retu) {
		changeCellsColor(class_name, '',gyo,retu);
	}
	function changeCellsColor(class_name, color, gyo, retu) {
		try {
			var data = document.getElementById("data");
			var data_cnt = data.rows.length;
			var summary = document.getElementById("summary");
			var summary_cnt = summary.rows.length;
			var day_cnt = document.mainform.day_cnt.value;
			var title_gyo_cnt = document.mainform.title_gyo_cnt.value;
			var show_gyo_cnt = document.mainform.show_gyo_cnt.value;
			var td_name = '';
			var p = 1;
			///-----------------------------------------------------------------------------
			//予実表示時、予定時のみ表示
			///-----------------------------------------------------------------------------
			if (show_gyo_cnt == "2") {
				p = 2;

				var wk = parseInt(gyo) / 2;
				var wk1 = "" + wk;
				//予定か判定
			    if (wk1.match(/[^0-9]/g)) {
				} else {
					return;
				}
			}
			///-----------------------------------------------------------------------------
			//カーソル表示
			///-----------------------------------------------------------------------------
			if ((parseInt(gyo) > 0) && (parseInt(retu) > 0)) {
				for (k=1;k<=day_cnt;k++){
					td_name = 'data' +  parseInt(gyo) + '_' + k;
					document.getElementById(td_name).style.backgroundColor = color;
				}
				for (i=1;i<=data_cnt;i++){
					td_name = 'data' + i + '_' + parseInt(retu);
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
			if (parseInt(gyo) > 0) {
				td_name = 'no' + parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'name' + parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'job' +  parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'st' +  parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;

				for (h=0;h<title_gyo_cnt;h++){
					td_name = 'sum_gyo' + parseInt(gyo) + '_' + h;
					document.getElementById(td_name).style.backgroundColor = color;
				}
				td_name = 'name' + parseInt(gyo) + '_2';
				document.getElementById(td_name).style.backgroundColor = color;

			}
			if (parseInt(retu) > 0) {
				td_name = 'day' + parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'day_of_week' +  parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;

				for (i=1;i<=summary_cnt;i++) {
					td_name = 'summary' + i + '_' + parseInt(retu);
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
		}
		catch(err){
		}

	}

	///-----------------------------------------------------------------------------
	//十字ハイライト
	///-----------------------------------------------------------------------------
<? $obj_menu->createCrossHighlightJavaScript($show_data_flg); ?>

	///-----------------------------------------------------------------------------
	//メッセージ画面（表示／非表示）
	///-----------------------------------------------------------------------------
	function showMemo(day, memo) {
		try {
			if (memo == "") {
				return;
			}

		    //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
			var msg1  = '<table bgcolor="#dddddd" border="1">\n';
				msg1 += '<tr><td>'+memo+'</td></tr>\n';
				msg1 += '</table>\n';
			showMsg(msg1);
		}
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	//勤務パターン登録子画面
	///-----------------------------------------------------------------------------
	function ptnUpdate(plan_hope_flg, emp_idx, atdptn_ptn_id, reason_2, day) {
//		closeChildWin();
		///-----------------------------------------------------------------------------
		//参照表示時
		///-----------------------------------------------------------------------------
		if (document.mainform.refer_flg.value == "1") {
			return;
		}
		///-----------------------------------------------------------------------------
		//チェック（締切日までは、翌月以降を登録できる。）
		///-----------------------------------------------------------------------------
		var close_ng_flg = document.mainform.close_ng_flg.value;
		if (close_ng_flg == "1") {
			alert('締切り日を過ぎているため、変更はできません。');
			return;
		}
		//スタンプ 20140205
		if (document.mainform.stamp_flg.value == "1") {
			setStamp(emp_idx, day, plan_hope_flg);
			return;
		}

		wx = 380;
		wy = 270;
//ウィンドウ位置を調整
		if (navigator.userAgent.search("Firefox") != -1 ) {

			if (day < 16) {
				dx = 280 + (day * 20);
			} else {
				dx = 280 + (day * 20) - wx - 30;
			}
<?
			if ($fullscreen_flg != "1") {
				echo("			wtop = 250;\n");
			} else {
				echo("			wtop = 130;\n");
			}
?>
			base = wtop + (emp_idx * 20);
			if (base > screen.height - wy) {
				base = screen.height - wy;
			}
		} else {

			midx = screen.width / 2;

			wleft = 0;
			wtop = 0;
			if (navigator.userAgent.search("MSIE") != -1 ) {
				wleft = window.screenLeft;
				wtop = window.screenTop;
			}
			dx =  wleft + getMouseX(event);

			if (day < 16) {
				dx = dx + 30;
			} else {
				dx = dx - wx - 30;
			}

			base = wtop + getMouseY(event);
			base = base - 20;

			wheight = screen.availHeight;
			if (base > wheight - wy) {
				base = wheight - wy;
			}

		}
		wk_id = 'staff_id['+emp_idx+']';
		emp_id = document.mainform.elements[wk_id].value;
		var url = 'duty_shift_menu_ptn_update.php';
		url += '?session=<?=$session?>';
		url += '&atdbk_duty_shift_flg=1';
		url += '&plan_hope_flg='+plan_hope_flg;
		url += '&group_id=<?=$group_id?>';
		url += '&pattern_id=<?=$pattern_id?>';
		url += '&emp_idx='+emp_idx;
		url += '&emp_id='+emp_id;
		url += '&atdptn_ptn_id='+atdptn_ptn_id;
		url += '&reason_2='+reason_2;
		url += '&duty_yyyy=<?=$duty_yyyy?>';
		url += '&duty_mm=<?=$duty_mm?>';
		url += '&day='+day;
		url += '&start_date=<?=$start_date?>';
		url += '&end_date=<?=$end_date?>';
		childwin_ptn_update = window.open(url, 'ptnUpdatePopup', 'left='+dx+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
		childwin_ptn_update.focus();
	}

//組み合わせ指定
var arr_today = new Array();
var arr_nextday = new Array();
<?
//組み合わせによる自動設定
$arr_seq_pattern = $obj->get_duty_shift_group_seq_pattern_array($group_id);
for ($i=0; $i<count($arr_seq_pattern); $i++) {

	$wk_today = sprintf("%02d%02d", $arr_seq_pattern[$i]["today_atdptn_ptn_id"], $arr_seq_pattern[$i]["today_reason"]);
	$wk_nextday = sprintf("%02d%02d", $arr_seq_pattern[$i]["nextday_atdptn_ptn_id"], $arr_seq_pattern[$i]["nextday_reason"]);
	echo("arr_today[$i] = '$wk_today';\n");
	echo("arr_nextday[$i] = '$wk_nextday';\n");
}

$end_day = $day_cnt;
?>
		var end_day = <?=$end_day?>;
		var wk_pattern_id = <?=$wk_pattern_id?>;

	//子画面で更新ボタン押下
	function callbackPtnUpdate(plan_hope_flg, emp_idx, day, atdptn_ptn_id, reason_2, copy_cnt ) {

		// 指定情報を保存
		document.mainform.elements['last_ptn_id'].value = atdptn_ptn_id;
		document.mainform.elements['last_reason_2'].value = reason_2;
		document.mainform.elements['last_copy_cnt'].value = copy_cnt;

        //対象日
		wk_day = day;
		//連続回数分繰返し
        for (cnt = 0; cnt < copy_cnt; cnt++) {

            //1日目
		    //勤務パターンを設定
		    if (plan_hope_flg == '0') {
			    var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
			    var wk2 = 'reason_2_'+emp_idx+'_'+wk_day;
		    } else {
			    var wk1 = 'rslt_id_'+emp_idx+'_'+wk_day;
			    var wk2 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
		    }

		    document.mainform.elements[wk1].value = atdptn_ptn_id;
		    document.mainform.elements[wk2].value = reason_2;

            //組合せ設定
		    var auto_set_flg = false;
		    var auto_set_flg2 = false;
		    var auto_set_flg3 = false;
            //2日目
		    wk_day++;
		    if (wk_day > end_day) {//終了日確認
		        break;
            }
		    if (wk_day <= end_day) {
			    if (plan_hope_flg == '0') {
				    var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
				    var wk2 = 'reason_2_'+emp_idx+'_'+wk_day;
			    } else {
				    var wk1 = 'rslt_id_'+emp_idx+'_'+wk_day;
				    var wk2 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
			    }
    			//組合せの確認
			    for (var i=0; i<arr_today.length; i++) {
				    if (arr_today[i] == atdptn_ptn_id) {
					    document.mainform.elements[wk1].value = arr_nextday[i];
					    document.mainform.elements[wk2].value = '';
					    var flg = (plan_hope_flg == '2') ? 2 : 1;
					    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
					    auto_set_flg = true;
					    break;
				    }
			    }
			    if (auto_set_flg) {//組合せ設定できたら対象日を進める
		            wk_day++;
		            if (wk_day > end_day) {
		                break;
                    }
                }
		    }
            //3日目
		    if (auto_set_flg == true && wk_day <= end_day) {
			    if (plan_hope_flg == '0') {
				    var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				    var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
				    var wk4 = 'reason_2_'+emp_idx+'_'+wk_day;
			    } else {
				    var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				    var wk3 = 'rslt_id_'+emp_idx+'_'+wk_day;
				    var wk4 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
			    }
			    var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

    			//組合せの確認
			    for (var i=0; i<arr_today.length; i++) {
				    if (arr_today[i] == yokuyoku_ptn_id) {
					    document.mainform.elements[wk3].value = arr_nextday[i];
					    document.mainform.elements[wk4].value = '';
					    var flg = (plan_hope_flg == '2') ? 2 : 1;
					    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
					    auto_set_flg2 = true;
					    break;
				    }
			    }
		        if (auto_set_flg2) {//組合せ設定できたら対象日を進める
    	            wk_day++;
		            if (wk_day > end_day) {
		                break;
                    }
                }
		    }
            //4日目
		    if (auto_set_flg2 == true && wk_day <= end_day) {
			    if (plan_hope_flg == '0') {
				    var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				    var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
				    var wk4 = 'reason_2_'+emp_idx+'_'+wk_day;
			    } else {
				    var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				    var wk3 = 'rslt_id_'+emp_idx+'_'+wk_day;
				    var wk4 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
			    }
			    var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;
    			//組合せの確認
			    for (var i=0; i<arr_today.length; i++) {
				    if (arr_today[i] == yokuyokuyoku_ptn_id) {
					    document.mainform.elements[wk3].value = arr_nextday[i];
					    document.mainform.elements[wk4].value = '';
					    var flg = (plan_hope_flg == '2') ? 2 : 1;
					    setReason(emp_idx, wk_day, wk_pattern_id, arr_nextday[i], flg);
					    auto_set_flg3 = true;
					    break;
				    }
			    }
		        if (auto_set_flg3) {//組合せ設定できたら対象日を進める
		            wk_day++;
		            if (wk_day > end_day) {
		                break;
                    }
                }
		    }

		}
		//document.mainform.emp_idx.value = emp_idx;
		//再表示
		showEdit(document.mainform.edit_start_day.value, document.mainform.edit_end_day.value);
    }

var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $obj->get_pattern_reason_array();
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
	foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
		if ($reason != "" && !($wk_atdptn_id == "10" && $reason == "24")) { //20140206
			echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$reason';\n");
		}
	}
}
?>
	function setReason(pos, day, tmcd_group_id, atdptn_id, flg) {

		if (flg == 1) {
			item_name = 'reason_2_'+pos+'_'+day;
		} else {
			item_name = 'rslt_reason_2_'+pos+'_'+day;
		}
		if (atdptn_id == '0000') {
			document.mainform[item_name].value = '';
			return '';
		}
		wk_atdptn_id = parseInt(atdptn_id.substring(0, 2),10);
		wk_id = tmcd_group_id+'_'+wk_atdptn_id;
		if (arr_atdptn_reason[wk_id] == undefined) {
			document.mainform[item_name].value = '';
			return '';
		}
		document.mainform[item_name].value = arr_atdptn_reason[wk_id];
		return arr_atdptn_reason[wk_id];
	}
	//スタンプ対応 20140205 start
	//スタンプ有効無効設定
	function setStampFlg() {
		if (!document.mainform.data_exist_flg) {
			alert('データがそろっていないため処理できません。表示を待ってから処理してください。');
			return false;
		}
		var flg = (document.mainform.stamp_flg.value == '') ? '1' : '';
		document.mainform.stamp_flg.value = flg;

		var btn = document.getElementById('stamp_img');
		if (flg == '') {
			btn.src = 'img/ico-illust-stamp.gif';
			//フローティング(スタンプ用)非表示
			floatWindowHide(0,'Stp');
		} else {
			btn.src = 'img/ico-illust-stamp-off.gif';
			//フローティング(スタンプ用)表示
			floatWindowHide(1,'Stp');
		}

		return;
	}

	var stamp_ptn = '<? $wk_stamp_ptn = ($stamp_ptn != "") ? $stamp_ptn :$def_ptn_info["atdptn_ptn_id"]; echo($wk_stamp_ptn); ?>';
	var stamp_reason = '<? $wk_stamp_reason = ($stamp_reason != "") ? $stamp_reason :$def_ptn_info["reason"]; echo($wk_stamp_reason); ?>';
	// スタンプパターン設定。スタンプの基準を設定する。
	// ptn 勤務パターン
	// reason 事由
	function setStampPtn(ptn, reason, targetid) {
		if (stamp_ptn != '') {
			wk_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;
			cel = document.getElementById("stamp_"+wk_id);
		}
		wk_id = (ptn != '10') ? ptn : ptn+'_'+reason;

		cel = document.getElementById("stamp_"+wk_id);

		stamp_ptn = ptn;
		stamp_reason = reason;

		//cell = document.getElementById("stamp_cel");
		//cell2 = document.getElementById("stamp_cel2");
		//未設定
		if (stamp_ptn == '') {
			//cell.style.backgroundColor = '#ffffff';
			//cell2.style.backgroundColor = '#ffffff';
			var wk_back_color = '#ffffff';
			var wk_ptn_name = '未設定';
			var wk_font_color = '#000000';
		} else {
			wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;
			//cell.style.backgroundColor = arr_bg_color[wk_ptn_id];
			//cell2.style.backgroundColor = arr_bg_color[wk_ptn_id];
			var wk_back_color = arr_bg_color[wk_ptn_id];
			var wk_ptn_name = arr_ptn_name[wk_ptn_id];
			var wk_font_color = arr_font_color[wk_ptn_id];
		}

		//------------------------------------------------------------------------------------------------------
		//スタンプ時
		//------------------------------------------------------------------------------------------------------
        //前回クリックされた勤務シフトは元の表示に戻す
        cell = document.getElementById('stmpPosition');
        var stmpPositionID=cell.value;
        //今回クリック情報を保持
        cell.value = targetid;

        //前回クリック元の表示に戻す
		if (stmpPositionID!=''){
			cell = document.getElementById(stmpPositionID);
			cell.style.fontWeight = 'normal';
			cell.style.fontStyle = 'normal';
		}

		//今回クリックされた箇所は太字
		cell = document.getElementById(targetid);
		cell.style.fontWeight = 'bold';
		cell.style.fontStyle = 'italic';

		//連続を1に設定
		document.getElementById("continuous_cnt").value = "1";
		cell = document.getElementById('stamp_cel');
		cell.style.backgroundColor = wk_back_color;
        cell.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="'+wk_font_color+'"><b>'+wk_ptn_name+'</b></font>';
		//下側スタンプへの設定
		//cell2.innerHTML = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		//document.getElementById("continuous_cnt2").value = "1";
	}
	//連続変更時設定
	function setCnt(flg) {
		if (flg == 1) {
			document.getElementById("continuous_cnt2").value = document.getElementById("continuous_cnt").value;
		} else {
			document.getElementById("continuous_cnt").value = document.getElementById("continuous_cnt2").value;
		}
	}

var arr_ptn_name = new Array();
var arr_font_color = new Array();
var arr_bg_color = new Array();
var arr_reason_name = new Array();
<?
for ($k=0;$k<count($data_pattern_all);$k++) {
	if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
			$data_pattern_all[$k]["font_name"] != "") {

		$wk_atdptn_ptn_id = $data_pattern_all[$k]["atdptn_ptn_id"];
		$wk_reason = $data_pattern_all[$k]["reason"];
		$wk_key = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wk_reason;

		//シングルクォーテーションがある場合、Javascriptエラーになるためエスケープ 20110817
		$wk_font_name = str_replace("'", "\'", h($data_pattern_all[$k]["font_name"]));
		echo("arr_ptn_name['".$wk_key."'] = '".$wk_font_name."';\n");
		echo("arr_font_color['".$wk_key."'] = '".$data_pattern_all[$k]["font_color_id"]."';\n");
		echo("arr_bg_color['".$wk_key."'] = '".$data_pattern_all[$k]["back_color_id"]."';\n");

	}
}

$reason_2_array = $obj->get_reason_2("");
for ($m=0; $m<count($reason_2_array); $m++) {
	$wk_key = $reason_2_array[$m]["id"];
	echo("arr_reason_name['".$wk_key."'] = '".$reason_2_array[$m]["font_name"]."';\n");
}
?>
    //デフォルト背景色を取得する
    function getDefaultBackColor(day) {
        //曜日欄
        var cell = document.getElementById('day_of_week' + day);

        //曜日欄の現設定背景色情報を取得
        var bgcolor;
        if (navigator.userAgent.indexOf("MSIE") >= 0) {
           bgcolor = cell.currentStyle.backgroundColor;
        }
        else {
           bgcolor = document.defaultView.getComputedStyle(cell,null).getPropertyValue("background-color");
        }
        return bgcolor;
    }

	// スタンプ設定。日毎のセルへ設定する。
	// emp_idx 職員の行位置
	// day 日の位置
	// plan_hope_flg 希望フラグ（実績も同じフラグ）0:予定のみ 2:希望等2段目あり
	function setStamp(emp_idx, day, plan_hope_flg) {

		if (document.mainform.stamp_flg.value != "1") {
			showEdit(day, day);
			return;
		}

		var copy_cnt = document.mainform.elements['continuous_cnt'].value;
		var wk_day = parseInt(day, 10);

		//連続回数分繰返し
        for (cnt = 0; cnt < copy_cnt; cnt++) {

		if (plan_hope_flg == '0') {
			if (document.mainform.elements['show_data_flg'].value != '') {
				var data_idx = (parseInt(emp_idx) * 2) + 1;
			} else {
				var data_idx = parseInt(emp_idx) + 1;
			}
			var wk0 = 'data' + data_idx + '_' + wk_day;
			var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+wk_day;
			var wk2 = 'reason_2_'+emp_idx+'_'+wk_day;
			//setUpdFlg(emp_idx);
		} else {
			var data_idx = (parseInt(emp_idx) + 1) * 2;
			var wk0 = 'data' + data_idx + '_' + wk_day;
			var wk1 = 'rslt_id_'+emp_idx+'_'+wk_day;
			var wk2 = 'rslt_reason_2_'+emp_idx+'_'+wk_day;
			//setRsltUpdFlg(emp_idx);
		}
		var cell = document.getElementById(wk0);
		//未設定にする
		if (stamp_ptn == '') {
			document.mainform.elements[wk1].value = '0000';
			document.mainform.elements[wk2].value = '';

            //日付欄
			cell2 = document.getElementById('day' + wk_day);

			//日付欄の背景色をセット
	        cell.style.backgroundColor = getDefaultBackColor(wk_day);
            cell.innerHTML = '&nbsp;';
            changeCell(wk0, emp_idx, wk_day+cnt, plan_hope_flg);

			wk_day++;
			if (wk_day > end_day) {//終了日確認
				break;
			}
			continue;
		}
		//組合せ確認
		var wk_reason = (stamp_reason != '' && stamp_reason != ' ') ? stamp_reason : '00';
		atdptn_ptn_id = (stamp_ptn < 10) ? '0' + stamp_ptn : stamp_ptn;
		atdptn_ptn_id = atdptn_ptn_id + wk_reason;

		document.mainform.elements[wk1].value = atdptn_ptn_id;

		//事由設定
		var flg = (plan_hope_flg == '2') ? 2 : 1;
		var wk_reason_2 = setReason(emp_idx, day, <?=$wk_pattern_id?>, atdptn_ptn_id, flg);
		wk_ptn_id = (stamp_ptn != '10') ? stamp_ptn : stamp_ptn+'_'+stamp_reason;

		//背景色[白:#FFFFFF]以外は適用
		if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
            cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

		} else {
			//背景色[白:#FFFFFF]の場合
			//日付欄の背景色をセット
			cell.style.backgroundColor = getDefaultBackColor(wk_day);

		}
		var wk_ptn_name = arr_ptn_name[wk_ptn_id];

<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>

		if (wk_reason_2 != '' && wk_reason_2 != undefined) { //20140204
			wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
		}
<? } ?>
		var wk_font_color = arr_font_color[wk_ptn_id];

		fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

		//組合せ設定
		var auto_set_flg = false;
		var auto_set_flg2 = false;
		var auto_set_flg3 = false;
		//2日目
	    wk_day++;
	    if (wk_day > end_day) {//終了日確認
	        break;
        }
		if ((wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk1 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk2 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk1 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk2 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}

            for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == atdptn_ptn_id) {
					document.mainform.elements[wk1].value = arr_nextday[i];
					document.mainform.elements[wk2].value = '';
					auto_set_flg = true;
					break;
				}
			}
			if (auto_set_flg == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != '10') ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '' && wk_reason_2 != undefined) { //20140204
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];
				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}
		//3日目
		if (auto_set_flg == true && (wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}
			var yokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					auto_set_flg2 = true;
					break;
				}
			}
			if (auto_set_flg2 == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '' && wk_reason_2 != undefined) { //20140204
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];

				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);
				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}
        //4日目
		if (auto_set_flg2 == true && (wk_day) <= end_day) {
			var wk0 = 'data' + data_idx + '_' + (wk_day);
			if (plan_hope_flg == '0') {
				var wk2 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'atdptn_ptn_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'reason_2_'+emp_idx+'_'+(wk_day);
			} else {
				var wk2 = 'rslt_id_'+emp_idx+'_'+(wk_day-1);
				var wk3 = 'rslt_id_'+emp_idx+'_'+(wk_day);
				var wk4 = 'rslt_reason_2_'+emp_idx+'_'+(wk_day);
			}
			var yokuyokuyoku_ptn_id = document.mainform.elements[wk2].value;

			for (var i=0; i<arr_today.length; i++) {
				if (arr_today[i] == yokuyokuyoku_ptn_id) {
					document.mainform.elements[wk3].value = arr_nextday[i];
					document.mainform.elements[wk4].value = '';
					auto_set_flg3 = true;
					break;
				}
			}
			if (auto_set_flg3 == true) {
				var cell = document.getElementById(wk0);
				wk_atdptn_ptn_id = parseInt(arr_nextday[i].substring(0, 2),10);
				wk_reason = parseInt(arr_nextday[i].substring(2, 4),10);
				wk_ptn_id = (wk_atdptn_ptn_id != 10) ? wk_atdptn_ptn_id : wk_atdptn_ptn_id+'_'+wk_reason;
				wk_reason_2 = setReason(emp_idx, wk_day, <?=$wk_pattern_id?>, arr_nextday[i], flg);

				//背景色[白:#FFFFFF]以外は適用
				if (arr_bg_color[wk_ptn_id].toUpperCase() != "#FFFFFF") {
	                cell.style.backgroundColor = arr_bg_color[wk_ptn_id];

				} else {
					//背景色[白:#FFFFFF]の場合
					//日付欄の背景色をセット
					cell.style.backgroundColor = getDefaultBackColor(wk_day);

				}
				var wk_ptn_name = arr_ptn_name[wk_ptn_id];
<? //事由設定フラグ確認
if ($reason_setting_flg == "t") {
?>
				if (wk_reason_2 != '' && wk_reason_2 != undefined) { //20140204
					wk_ptn_name = wk_ptn_name + arr_reason_name[wk_reason_2];
				}
<? } ?>
				var wk_font_color = arr_font_color[wk_ptn_id];

				fsize = (wk_ptn_name.length > 1) ? 'j10' : 'j12';
		        cell.innerHTML = '<font face="ＭＳ Ｐゴシック, Osaka" color="'+wk_font_color+'">'+wk_ptn_name+'</font>';
		        changeCell(wk0, emp_idx, wk_day, plan_hope_flg);

				wk_day++;//組合せ設定できたら対象日を進める
				if (wk_day > end_day) {//終了日確認
					break;
				}
			}
		}
		}
	}
	//スタンプ対応 20140205 end
	</script>
<!-- ************************************************************************ -->
<!-- JavaScript（メッセージ子画面表示処理） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	//(注1)

	//--HTML出力
	function outputLAYER(layName,html){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
	  document.getElementById(layName).innerHTML=html;
	} else if(document.all){            //e4用
	  document.all(layName).innerHTML=html;
	} else if(document.layers) {        //n4用
	   with(document.layers[layName].document){
	     open();
	     write(html);
	     close();
	  }
	}
	}

	//--マウス追跡
	/*==================================================================
	followingLAYER()

	Syntax :
	 追跡レイヤー名 = new followingLAYER('レイヤー名'
	                            ,右方向位置,下方向位置,動作間隔,html)

	 レイヤー名 マウスを追跡させるレイヤー名
	 右方向位置 マウスから右方向へ何ピクセル離すか
	 下方向位置 マウスから下方向へ何ピクセル離すか
	 動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
	 html       マウスを追跡するHTML

	------------------------------------------------------------------*/
	/*--/////////////ここから下は触らなくても動きます/////////////--*/

	//--追跡オブジェクト
	//e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
	function followingLAYER(layName,ofx,ofy,delay,html){
	this.layName = layName;   //マウスを追跡させるレイヤー名
	this.ofx     = ofx;       //マウスから右方向へ何ピクセル離すか
	this.ofy     = ofy;       //マウスから下方向へ何ピクセル離すか
	this.delay   = delay;     //マウスを追跡するタイミング
	if(document.layers)
	  this.div='<layer name="'+layName+'" left="-100" top="-100">\n'	          + html + '</layer>\n';
	else
	  this.div='<div id="'+layName+'"\n'	          +'style="position:absolute;left:-100px;top:-100px">\n'	          + html + '</div>\n';
	document.write(this.div);
	}

	//--メソッドmoveLAYER()を追加する
	followingLAYER.prototype.moveLAYER = moveLAYER; //メソッドを追加する
	function moveLAYER(layName,x,y){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
	    document.getElementById(layName).style.left = x;
	    document.getElementById(layName).style.top  = y;
	} else if(document.all){            //e4用
	    document.all(layName).style.pixelLeft = x;
	    document.all(layName).style.pixelTop  = y;
	} else if(document.layers)          //n4用
	    document.layers[layName].moveTo(x,y);
	}

	//--Eventをセットする(マウスを動かすとdofollow()を実行します)
	document.onmousemove = dofollow;
	//--n4マウスムーブイベント走査開始
	if(document.layers)document.captureEvents(Event.MOUSEMOVE);
	//--oの全画面のEventを拾えないことへの対策
	if(window.opera){
	op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'	          +'                     left:100%;top:100%"></div> ';
	document.write(op_dmydoc);
	}

	//--イベント発生時にマウス追跡実行
	function dofollow(e){
	for(var i=0 ; i < a.length ; i++ )
	  setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"	         +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"	    ,a[i].delay);
	}

	//--マウスX座標get
	function getMouseX(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientX;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollLeft+event.clientX;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageX;
	}

	//--マウスY座標get
	function getMouseY(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientY;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollTop+event.clientY;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageY;
	}

	/*////////////////////////////// マウスを追跡するレイヤーここまで */

	//テンプレートを作成しoutputLAYERへ渡す
	function showMsg(msg1){
		outputLAYER('test0',msg1);
	}

	//レイヤーの中身を消す
	function hideMsg(){
		var msg1 ='';
		outputLAYER('test0',msg1);
	}

	//レイヤーの数だけa[i]=…部分を増減して使ってください
	var a = new Array();
	a[0]  = new followingLAYER('test0',20,10,100,'');
//	a[1]  = new followingLAYER('test1',20,10,200,'');
//	a[2]  = new followingLAYER('test2',20,10,300,'');
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/font.css">
<link rel="stylesheet" type="text/css" href="css/shift/shift.css">
<link rel="stylesheet" type="text/css" href="css/shift/results.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

img.close {
	background-image:url("images/minus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}

</style>
</head>

<? //echo("emp_id=$emp_id <br>"); ?>
<? //echo("duty_yyyymm=$duty_yyyymm <br>"); ?>
<? //echo("next_yyyymm=$next_yyyymm <br>"); ?>
<? //echo("now_dd=$now_dd <br>"); ?>
<? //echo("group_close_day=$group_close_day <br>"); ?>
<? //echo("closing_day=$closing_day <br>"); ?>
<? //echo("start_day=$start_day <br>"); ?>
<? //echo("end_day=$end_day <br>"); ?>
<? //echo("start_date=$start_date <br>"); ?>
<? //echo("end_date=$end_date <br>"); ?>
<? //print_r($all_group_array); ?>
<? //print_r($plan_staff_array); ?>
<? //print_r($other_group_array); ?>
<? //print_r($calendar_array); ?>
<? //print_r($data_pattern_all); ?>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
		<!-- 画面遷移 -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr bgcolor="#f6f9ff">
		<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a></font></td>
		<? if ($work_admin_auth == "1") { ?>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>&work=1"><b>管理画面へ</b></a></font></td>
		<? } ?>
		</tr></table>
		<!-- タブ -->
		<?
			$arr_option = "&refer_flg=$refer_flg";
			show_atdbk_menuitem($session, $fname, $arr_option);
		?>
		<!-- 下線 -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr></table><img src="img/spacer.gif" alt="" width="1" height="2"><br>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
		if (($err_msg_1 != "") || ($err_msg_2 != "") || ($err_msg_3 != "")) {
			echo($err_msg_1);
			if ($err_msg_1 != "") { echo("<br>\n"); }
			echo($err_msg_2);
			if ($err_msg_2 != "") { echo("<br>\n"); }
			echo($err_msg_3);
			echo("<br>\n");
			echo("<form name=\"mainform\" method=\"post\">\n");
			echo("<table id=\"header\" class=\"list\"></table>\n");
			echo("<div id=\"region\">\n");
			echo("<table id=\"data\" class=\"list\"></table>\n");
			echo("</div>\n");
			echo("<table id=\"error\" class=\"list\"></table>\n");
			echo("<table id=\"summary\" class=\"list\"></table>\n");
			echo("</form>");
		} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 勤務表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<?
			echo($obj_menu->showHidden_1($data_cnt,
										$day_cnt,
										$edit_start_day,
										$edit_end_day,
										count($title_gyo_array)+2));

			echo("<input type=\"hidden\" name=\"show_gyo_cnt\" value=\"$show_gyo_cnt\">\n");	//表示行数
		?>
		<!-- ------------------------------------------------------------------------ -->
		<!--  シフトグループ（病棟）名 、年、月 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?
			$url = "atdbk_duty_shift.php";
			$url_option = "&refer_flg=$refer_flg";
			$group_show_flg = "1";				//シフトグループ表示フラグ（１：表示）
			$wk_group_array = $group_array;

/* シフトグループプルダウンを表示とする 20140210
			if ($refer_flg == "") {
				//他病棟と重複しているスタッフの場合
				if (count($other_group_array) > 0) {
					$wk_group_array = $other_group_array;
					$group_show_flg = "";
				}
			}
            */
			echo($obj_menu->showHead($session, $fname, $group_show_flg, $group_id,
									$wk_group_array,
									$duty_yyyy, $duty_mm, $url, $url_option));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 各種ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<? echo($obj_menu->showButtonAtdbk($create_flg, $plan_results_flg, $stamp_flg)); // 20140205 ?>
		</table>

<?//■スタンプエリアの生成開始 20140205 start ?>
<img src="./images/imgm.gif" id="flagimg" name="img" style="position:absolute;display:none;" width="15" height="15" />
<input type="hidden" name="listPosition" id="listPosition">
<input type="hidden" name="listPositionPlanHope" id="listPositionPlanHope">

<div id="floatWindowStp" style="position:absolute;">
<a href="" class="close" onclick="floatWindowHide(0,'Stp');setStampFlg();"><img src="./images/close.gif" alt="閉じる" /></a>
<dl id="dl_floatWindowStp">
<dt></dt>
<dd>
<table border="0" cellspacing="0" cellpadding="0" frame="void">
<tr>
<td>
	<?
	//スタンプの指定無しの場合、シフト記号登録の先頭のパターンを取得
	for ($k=0;$k<count($data_pattern_all);$k++) {
		if ($pattern_id == $data_pattern_all[$k]["pattern_id"] &&
				(($stamp_ptn != "" && $data_pattern_all[$k]["atdptn_ptn_id"] == $stamp_ptn) ||
					($stamp_ptn == "" && $data_pattern_all[$k]["font_name"] != "")) &&
				$data_pattern_all[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306
			$wk_ptn_name = $data_pattern_all[$k]["font_name"];
			$wk_font_color = $data_pattern_all[$k]["font_color_id"];
			$wk_back_color = $data_pattern_all[$k]["back_color_id"];
			break;
		}
	}
?>
        <? $stamp_tbl_disp = ($stamp_flg == "1") ? "" : "none"; ?>
          <table border="0" cellspacing="0" cellpadding="1" class="list">
            <tr>
              <input type="hidden" name="stmpPosition" value="" id="stmpPosition">
              <td width="50" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連続</font>
              </td>
            </tr>
            <tr>
<!--              <td align="center" style="background-color:#ffffff;">-->
              <td align="center" style="background-color:#ffffff;">
                <select name="continuous_cnt" id="continuous_cnt">
	<?
	for ($c_idx=1; $c_idx<=31; $c_idx++) {
		echo("<option value=\"$c_idx\">$c_idx</option>\n");
	}
        ?>
                </select>
              </td>
            </tr>


            <tr>
              <td id="stamp_cel" bgcolor="<?echo($wk_back_color);?>" align="center">
		        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="<?echo($wk_font_color);?>"><?echo($wk_ptn_name);?></font>
		      </td>
            </tr>


        </table>
      </td>

      <td width="5">
        <table>
        </table>
      </td>

      <td>
        <table border="0" cellspacing="0" cellpadding="1" class="list" style="margin-left : auto ; margin-right : auto ; text-align : left ;">
          <tr valign="middle">
	<?

	$wdpa = $data_pattern_all;
	$data_ptn_cnt = count($data_pattern_all);
	$wdpa[$data_ptn_cnt]["pattern_id"] = $pattern_id;
	$wdpa[$data_ptn_cnt]["atdptn_ptn_id"] = "";
	$wdpa[$data_ptn_cnt]["reason"] = "";
	$wdpa[$data_ptn_cnt]["font_name"] = "未設";
	$wdpa[$data_ptn_cnt]["atdptn_ptn_name"] = "未設定";

	$col_max = 15;
	$wk_cnt = 0;
	for ($k=0;$k<count($wdpa);$k++) {
		if ($pattern_id == $wdpa[$k]["pattern_id"] &&
				$wdpa[$k]["font_name"] != "" &&
				$wdpa[$k]["stamp_disp_flg"] != "f") { //スタンプ非表示フラグ確認 20120306

			if ($wk_cnt % $col_max == 0 && $wk_cnt > 0) {
				echo("</tr><tr>\n");
			}
			$wk_atdptn_ptn_id = $wdpa[$k]["atdptn_ptn_id"];
			$wk_id = ($wk_atdptn_ptn_id != "10") ? $wk_atdptn_ptn_id : $wk_atdptn_ptn_id."_".$wdpa[$k]["reason"];

			if (!$wdpa[$k]["back_color_id"]){
				$style = "";
			}else{
				$style = " style=\"background-color:".$wdpa[$k]["back_color_id"]."\"";
			}
			echo("<td class=\"td03\" id=\"stamp_$wk_id\"".$style." onClick=\"setStampPtn('");
			echo($wdpa[$k]["atdptn_ptn_id"]);
			echo("','");
			echo($wdpa[$k]["reason"]);
			echo("','");
			echo("stamp_$wk_id");
			echo("');\">");

			if (!$wdpa[$k]["reason_name"]){
				echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] . "\">");
			}else{
				echo("<div style=\"color: ".$wdpa[$k]["font_color_id"]."\" title=\"" . $wdpa[$k]["atdptn_ptn_name"] ."/".$wdpa[$k]["reason_name"]. "\">");
			}
			$wk_font_name = h($wdpa[$k]["font_name"]);
			if (strpos($wk_font_name, "&") === false) {
				$wk_font_name = mb_substr($wk_font_name,0,3);
			}
			echo($wk_font_name);
			echo("</div>");
			echo("</td>\n");

			$wk_cnt++;
		}
	}

?>
        </table>
       </td>
      </tr>
     </table>
</dd>
</dl>
</div>

		<!-- ------------------------------------------------------------------------ -->
		<!-- 見出し -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="non_list">
		<?
			//週 20140204
			//if ($refer_flg == "") {
		//echo($obj_menu->showTitleSub1($duty_yyyy, $duty_mm,
		//			$day_cnt, $edit_start_day, $edit_end_day,
		//			$week_array, $title_gyo_array, $check_flg, "", $group_array));
		//	}
		?>
		</table>
<?//■スタンプエリアの生成開始 20140205 end ?>

		<table width="250" id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
        <?
		// ユニット（３階層名）表示のため追加
        $unit_disp_level = $obj->get_emp_unitlevel($group_array[0]['group_id']);
		$arr_class_name = get_class_name_array($con, $fname);
		$unit_name = $arr_class_name[intval($unit_disp_level) - 1];
		$group_array[0]['unit_disp_flg'] = $unit_disp_flg;

        //日・曜日
        echo($obj_menu->showTitleSub2(
                $duty_yyyy,
                $duty_mm,
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $week_array,
                &$title_gyo_array, //行合計タイトル情報 集計列幅対応 20140502
                $check_flg,
                $calendar_array,
                "",
                "",
                "",
                array(),
                "",
                "",
                $paid_hol_disp_flg,
                $group_array,
                "create_shift",
                0,
                $unit_name,				//ユニット（３階層）名 20140110
                $gyo_array, //行合計情報 集計列幅対応 20140502
                "1"         //勤務シフト希望フラグ（１:勤務シフト希望）
        ));
        ?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトデータ（職員（列）年月日（行） -->
		<!-- ------------------------------------------------------------------------ -->
		<?php // スタンプ対応 20140205 cursor:pointer;除外 ?>
		<div id="region" style="overflow-x:hidden; overflow-y:scroll; overflow:auto; border:#5279a5 solid 0px;">
		<table width="250" id="data" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
        <?
        $warning_gyo_array = array();
        $obj_menu->showList(
                $plan_individual_array,
                $data_pattern,
                $warning_gyo_array,
                $plan_results_flg,
                $plan_hope_flg,
                $plan_duty_flg,
                $plan_comment_flg,
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $week_array,
                $title_gyo_array,
                $gyo_array,
                $check_flg,
                $emp_id,
                $staff_del_flg,
                0,
                $data_cnt,
                $highlight_flg,
                $group_id,
                $pattern_id,
                "1",
                $create_flg,
                "",
                0,
                $calendar_array,
                "",
                $title_hol_array,
                $hol_cnt_array,
                $paid_hol_all_array,
                $reason_setting_flg,
                "",
                $group_array,
                $plan_individual,
                $paid_hol_disp_flg
        );
        ?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 列計 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" id="summary" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
        <?
        $paid_hol_array[] = $paid_hol_all_array["plan"];
        if ($show_data_flg == "1") {    //2行目に勤務実績を表示する場合
            $paid_hol_array[] = $paid_hol_all_array["result"];
        }
        echo($obj_menu->showTitleRetu(
                $day_cnt,
                $edit_start_day,
                $edit_end_day,
                $week_array,
                $title_gyo_array,
                $gyo_array,
                $title_retu_array,
                $retu_array,
                $check_flg,
                $data_cnt,
                0,
                $show_gyo_cnt,
                "",
                "",
                $title_hol_array,
                $hol_cnt_array,
                "",
                $group_array,
                $calendar_array,
                $paid_hol_disp_flg,
                $paid_hol_array
        ));
        ?>
		</table>
		</div>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
	<?
	echo("<input type=\"hidden\" name=\"up_staff_id\" value=\"$emp_id\"> \n");			//登録対象職員ＩＤ
	echo("<input type=\"hidden\" name=\"refer_flg\" value=\"$refer_flg\"> \n");			//画面（希望／参照）フラグ（１：参照）
	echo("<input type=\"hidden\" name=\"close_ng_flg\" value=\"$close_ng_flg\"> \n");	//締切チェックフラグ（１：締切を過ぎている）
	///-----------------------------------------------------------------------------
	// 共通ＨＩＤＤＥＮ
	///-----------------------------------------------------------------------------
	$wk_rslt_flg = "";
	echo($obj_menu->showHidden_2($wk_rslt_flg,
				$plan_individual_array, $calendar_array,
				$session,
				$day_cnt,
				$plan_results_flg,
				$plan_hope_flg,
				$plan_duty_flg,
				$plan_comment_flg,
				$group_id,
				$duty_yyyy, $duty_mm,
				$edit_start_day, $edit_end_day,
				$add_staff_cnt, $add_staff_id, $del_staff_id,
				$pattern_id,
				$draft_flg,
				$check_flg,
				"",
				$reason_setting_flg));
	///-----------------------------------------------------------------------------
	// 入力時、指定職員以外のデータ（HIDDEN）
	///-----------------------------------------------------------------------------
	for ($i=0; $i<count($plan_individual_array); $i++) {
		for ($k=1; $k<=$day_cnt; $k++) {
			if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
				if ($emp_id != $plan_individual_array[$i]["staff_id"]) {
					$wk2 = sprintf("%02d%02d", $plan_individual_array[$i]["atdptn_ptn_id_$k"], $plan_individual_array[$i]["reason_$k"]);;
					$wk1 = "atdptn_ptn_id_$i" . "_" . $k;
					echo("<input type=\"hidden\" name=\"$wk1\" value=$wk2 >\n");
				}
			}
		}
	}
	///-----------------------------------------------------------------------------
	//シフトグループ表示フラグ（１：表示）
	///-----------------------------------------------------------------------------
	if ($group_show_flg == "1") {	//シフトグループ表示フラグ（１：表示）
		echo("<input type=\"hidden\" name=\"group_id\" value=$group_id>\n");
	}
	///-----------------------------------------------------------------------------
	// 実績データ
	///-----------------------------------------------------------------------------
	for ($k=1; $k<=$day_cnt; $k++) {
		$rslt_flg[$k] = "";
	}
	for ($i=0;$i<$rslt_cnt;$i++) {
		for ($k=1; $k<=$day_cnt; $k++) {
			$m = $k - 1;
			if($calendar_array[$m]["date"] == $rslt_array[$i]["date"]) {
				$rslt_flg[$k] = "1";
			}
		}
	}
	for ($k=1; $k<=$day_cnt; $k++) {
		$wk = $rslt_flg[$k];
		echo("<input type=\"hidden\" name=\"rslt_flg[]\" value=\"$wk\"> \n");
	}
	//登録子画面の指定情報
	echo("<input type=\"hidden\" name=\"last_ptn_id\" value=\"\">\n");
	echo("<input type=\"hidden\" name=\"last_reason_2\" value=\"\">\n");
	echo("<input type=\"hidden\" name=\"last_copy_cnt\" value=\"\">\n");
	//スタンプ対応 20140205 start
	//スタンプ有効 1:有効
	echo("<input type=\"hidden\" name=\"stamp_flg\" value=\"$stamp_flg\">\n");
	echo("<input type=\"hidden\" name=\"stamp_ptn\" value=\"$stamp_ptn\">\n");
	echo("<input type=\"hidden\" name=\"stamp_reason\" value=\"$stamp_reason\">\n");
	//２行目表示データフラグを出力（"":予定のみ １：勤務実績）
	echo("<input type=\"hidden\" name=\"show_data_flg\" value=\"$show_data_flg\">\n");
	//登録時、データがそろっていることの確認用
	echo("<input type=\"hidden\" name=\"data_exist_flg\" value=\"1\">\n");
	//スタンプ対応 20140205 end
	?>
	</form>
	<!-- ------------------------------------------------------------------------ -->
	<!-- ＨＩＤＤＥＮ（印刷用） -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="printForm" method="post" action="atdbk_duty_shift_print.php" target="printFormChild">
		<?
			echo("<input type=\"hidden\" name=\"refer_flg\" value=\"$refer_flg\"> \n");			//画面（希望／参照）フラグ（１：参照）
			///-----------------------------------------------------------------------------
			// 共通ＨＩＤＤＥＮ
			///-----------------------------------------------------------------------------
			$wk_rslt_flg = "";
			echo($obj_menu->showHidden_1($data_cnt,
										$day_cnt,
										$edit_start_day,
										$edit_end_day,
										count($title_gyo_array)));
			echo($obj_menu->showHidden_2($wk_rslt_flg,
										$plan_individual_array, $calendar_array,
										$session,
										$day_cnt,
										$plan_results_flg,
										$plan_hope_flg,
										$plan_duty_flg,
										$plan_comment_flg,
										$group_id,
										$duty_yyyy, $duty_mm,
										$edit_start_day, $edit_end_day,
										$add_staff_cnt, $add_staff_id, $del_staff_id,
										$pattern_id,
										$draft_flg,
										$check_flg));
			///-----------------------------------------------------------------------------
			// 入力時、指定職員以外のデータ（HIDDEN）
			///-----------------------------------------------------------------------------
			for ($i=0; $i<count($plan_individual_array); $i++) {
			for ($k=1; $k<=$day_cnt; $k++) {
			if (($edit_start_day <= $k) && ($edit_end_day >= $k)) {
				if ($emp_id != $plan_individual_array[$i]["staff_id"]) {
					$wk2 = sprintf("%02d%02d", $plan_individual_array[$i]["atdptn_ptn_id_$k"], $plan_individual_array[$i]["reason_$k"]);;
					$wk1 = "atdptn_ptn_id_$i" . "_" . $k;
					echo("<input type=\"hidden\" name=\"$wk1\" value=$wk2 > \n");
				}
			}
			}
			}
			//集計行列の印刷 0:しない 1:する
			echo("<input type=\"hidden\" name=\"total_print_flg\" value=\"\">\n");
			//PDF印刷 0:WEBページ 1:PDF
			echo("<input type=\"hidden\" name=\"pdf_print_flg\" value=\"\">\n");
			//印刷用に２行目表示データフラグを出力（"":予定のみ １：勤務実績）
			echo("<input type=\"hidden\" name=\"show_data_flg\" value=\"$show_data_flg\">\n");
		?>
	</form>

<?
	}
?>

</body>

<? pg_close($con); ?>

</html>

<!-- ************************************************************************ -->
<!-- JavaScript（１週間単位での表示／非表示） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	var data = document.getElementById("data");
	var header = document.getElementById("header");
	///-----------------------------------------------------------------------------
	// スクロールの位置調整
	///-----------------------------------------------------------------------------
//	document.getElementById("region").style.width = data.offsetWidth + 17;
	document.getElementById("region").style.width = data.offsetWidth + 20;
	///-----------------------------------------------------------------------------
	// スクロールの縦サイズ調整
	///-----------------------------------------------------------------------------
	var data_row = data.rows.length;
	if(data_row > 20) {
		var max = 20;
		var scroll_size = 0;
		for(i=0; i<max; i++) {
			scroll_size += data.rows[i].offsetHeight;
		}
		document.getElementById('region').style.height = scroll_size;
        <? /* フレームの高さ指定、scroll_sizeの後ろの数字は見出しの高さ分 20120329 */ ?>
        if (parent.document.getElementById('floatingpage')) {
            parent.document.getElementById('floatingpage').style.height = (scroll_size + 180) + 'px';
        }
	}
</script>
<script language="JavaScript">
<?
//シフトグループ変更後と表示中に無効化、表示後有効化 20111221
if ($group_show_flg == "") {	//シフトグループ表示フラグ（１：表示）
?>
	var group_id = document.getElementById('group_id');
    if (group_id) group_id.disabled = false;
    <?
}
?>
	var duty_yyyy = document.getElementById('duty_yyyy');
    if (duty_yyyy) duty_yyyy.disabled = false;
</script>
