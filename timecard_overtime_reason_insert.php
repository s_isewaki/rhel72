<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($reason == "") {
	echo("<script type=\"text/javascript\">alert('残業理由が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($reason) > 200) {
	echo("<script type=\"text/javascript\">alert('残業理由が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 残業理由IDを採番
$sql = "select max(reason_id) from ovtmrsn";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$reason_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 残業理由を登録
// ----- メディアテック Start -----
$sql = "insert into ovtmrsn (reason_id, reason, over_no_apply_flag, tmcd_group_id) values (";
if (empty($wktmgrp)) $wktmgrp = NULL;
$content = array($reason_id, pg_escape_string($reason), $over_no_apply_flag, $wktmgrp);
$ins = insert_into_table($con, $sql, $content, $fname);
// ----- メディアテック End -----
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_overtime_reason.php?session=$session';</script>");
?>
