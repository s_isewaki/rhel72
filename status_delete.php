<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 27, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// 入力チェック
if ($trashbox == "") {
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 選択された役職をループ
foreach ($trashbox as $tmp_st_id) {

	// 職員存在チェック
	$sql = "select count(*) from empmst";
	$cond = "where ((emp_st = $tmp_st_id) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_st = $tmp_st_id))) and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		$sql = "select st_nm from stmst";
		$cond = "where st_id = $tmp_st_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_st_nm = pg_fetch_result($sel, 0, "st_nm");

		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('「{$tmp_st_nm}」は職員情報が存在するため削除できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	// 役職を論理削除
	$sql = "update stmst set";
	$set = array("st_del_flg");
	$setvalue = array("t");
	$cond = "where st_id = $tmp_st_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 役職一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'status_menu.php?session=$session';</script>");
?>
