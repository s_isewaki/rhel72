<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$date = date("Ymd", $date);

// データベースに接続
$con = connect2db($fname);

// 施設名を取得
$sql = "select name from statfcl";
$cond = "where statfcl_id = $fcl_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$fcl_name = pg_fetch_result($sel, 0, "name");

// 入力対象となる管理項目・補助項目の一覧を取得
// 　入力対象の補助項目
// 　　項目タイプ、管理項目ID、管理項目名、補助項目ID、補助項目名
// 　補助項目のない管理項目
// 　　項目タイプ、管理項目ID、管理項目名、null、null
$sql = "select 'assist' as type, statitem.statitem_id, statitem.name, statassist.statassist_id, statassist.statassist_name from statassist inner join statitem on statassist.statitem_id = statitem.statitem_id where statassist.assist_type = '1' and statassist.del_flg = 'f' union select 'item', statitem_id, name, null, null from statitem where del_flg = 'f' and not exists (select * from statassist where statassist.statitem_id = statitem.statitem_id and statassist.del_flg = 'f')";
$cond = "order by 2, 4";
$sel_item = select_from_table($con, $sql, $cond, $fname);
if ($sel_item == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 統計値を取得
$sql = "select statitem_id, statassist_id, stat_val from stat";
$cond = "where stat_date = '$date' and statfcl_id = $fcl_id";
$sel_stat = select_from_table($con, $sql, $cond, $fname);
if ($sel_stat == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// イントラメニュー情報を取得
$sql = "select menu1 from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu1 = pg_fetch_result($sel, 0, "menu1");
?>
<title><? echo($menu1); ?> | 登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	location.href = 'intra_stat.php?session=<? echo($session); ?>&date=' + dt;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>登録</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="intra_stat_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="3" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" width="160" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">日付</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date)); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">施設</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? echo($fcl_name); ?></font></td>
</tr>
<?
$stats = array();
while ($row = pg_fetch_array($sel_stat)) {
	$tmp_item_id = $row["statitem_id"];
	$tmp_assist_id = $row["statassist_id"];
	$tmp_val = $row["stat_val"];

	if ($tmp_assist_id == "") {
		$tmp_id = $tmp_item_id;
	} else {
		$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
	}

	$stats[$tmp_id] = $tmp_val;
}

while ($row = pg_fetch_array($sel_item)) {
	$tmp_item_id = $row["statitem_id"];
	$tmp_item_name = $row["name"];
	$tmp_assist_id = $row["statassist_id"];
	$tmp_assist_name = $row["statassist_name"];

	if ($row["type"] == "item") {
		$tmp_id = $tmp_item_id;
		$tmp_name = $tmp_item_name;
	} else {
		$tmp_id = $tmp_item_id . "_" . $tmp_assist_id;
		$tmp_name = $tmp_item_name . "<br>&gt; " . $tmp_assist_name;
	}

	$tmp_val = $stats[$tmp_id];

	echo("<tr height=\"22\">\n");
	echo("<td bgcolor=\"#f6f9ff\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">$tmp_name</td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\"><input type=\"text\" name=\"stats[]\" value=\"$tmp_val\" size=\"20\" maxlength=\"20\" style=\"ime-mode:inactive;font-size:1.6em;font-family:'Times New Roman';\"><input type=\"hidden\" name=\"ids[]\" value=\"$tmp_id\"></td>\n");
	echo("</tr>\n");
}
?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="fcl_id" value="<? echo($fcl_id); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
