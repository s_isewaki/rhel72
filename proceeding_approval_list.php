<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("mygroup_common.php");
require_once("label_by_profile_type.ini");
require_once("application_workflow_select_box.ini");


//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$wkfw=check_authority($session,42,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}



// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $emp_id);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);



?>

<?
//====================================
// HTML
//====================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ワークフロー｜承認者選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script src="js/prototype/dist/prototype.js"></script>
<script language="javascript">

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=6';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function openProjectList()
{
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './workflow_project_list.php';
	url += '?session=<?=$session?>';
	childwin = window.open(url, 'popup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');
	childwin.focus();
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");
	var target_length = document.wkfw.target.options.length;

	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.wkfw.target.options.length; k < l; k++) {
			if (document.wkfw.target[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.wkfw.target, emp_id, emp_name);
		}
	}
}

function getSelectedValue(sel) {
	return sel.options[sel.selectedIndex].value;
}

function deleteAllOptions(box) {

	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

// 追加ボタン
function addEmp() {

	for (var i = 0, j = document.wkfw.emplist.options.length; i < j; i++)
	{
		if (document.wkfw.emplist.options[i].selected)
		{
			var emp_id = document.wkfw.emplist.options[i].value;
			var emp_name = document.wkfw.emplist.options[i].text;
	
			var in_group = false;
			for (var k = 0, l = document.wkfw.target.options.length; k < l; k++)
			{
				if (document.wkfw.target.options[k].value == emp_id)
				{
					in_group = true;
					break;
				}
			}
	
			if (!in_group)
			{
				set_wm_counter(emp_id);
				addOption(document.wkfw.target, emp_id, emp_name);
			}
		}
	}
}

// 削除ボタン
function deleteEmp() {

	for (var i = document.wkfw.target.options.length - 1; i >= 0; i--) {
		if (document.wkfw.target.options[i].selected) {
			document.wkfw.target.options[i] = null;
		}
	}
}




// 職員指定制御
function set_st_div1(st_div_flg)
{
	st_staff_flg = true;
	if(st_div_flg == 't')
	{
		st_staff_flg = false;
	}

	// 職員
    document.wkfw.target.disabled = st_staff_flg;
    document.wkfw.emplist.disabled = st_staff_flg;
    document.wkfw.add_emp.disabled = st_staff_flg;
    document.wkfw.delete_emp.disabled = st_staff_flg;
    document.wkfw.mygroup.disabled = st_staff_flg;
    document.wkfw.emplist_btn.disabled = st_staff_flg;

}


function initPage() {

	groupOnChange();


		var emp_id = window.opener.document.updform.<?=$wkfw_nm?>_emp_id<? echo($approve); ?>.value;
		var emp_nm = window.opener.document.updform.<?=$wkfw_nm?>_emp_nm<? echo($approve); ?>.value;
		if(emp_id != "") {
			var obj_target = document.wkfw.target;
			deleteAllOptions(obj_target);

			arr_emp_id = emp_id.split(",");
			arr_emp_nm = emp_nm.split(", ");

			for(i=0; i<arr_emp_id.length; i++)
			{
				addOption(obj_target, arr_emp_id[i], arr_emp_nm[i], '');
			}
		}
}


function setApprove()
{

	var approve_content = "";
	var target_class_div = "0";
	var target_class_nm = "";
	var st_id = "";
	var emp_id = "";
	var emp_nm = "";

	var class_sect_id = "";
	var atrb_sect_id = "";
	var dept_sect_id = "";
	var room_sect_id = "";

	var st_sect_id = "";


	// 職員指定
		if(approve_content != '')
		{
			approve_content += "<BR>";
		}
        var target_length = document.wkfw.target.options.length;
        if(target_length == 0)
        {
            alert('職員を選択してください。');
            return false;
        }

		for(i=0; i<target_length; i++)
		{
			if(i > 0)
			{
				emp_id += ",";
				emp_nm += ", ";
			}
	        emp_id += document.wkfw.target.options[i].value;
    	    emp_nm += document.wkfw.target.options[i].text;
		}
		approve_content += emp_nm;

	window.opener.document.getElementById('<?=$wkfw_nm?>_approve_content<?=$approve?>').innerHTML = approve_content;
	confirm("ch1");

	confirm("emp_id"+emp_id);
	window.opener.document.prcd.<?=$wkfw_nm?>_emp_id<? echo($approve); ?>.value = emp_id;

	confirm("emp_nm"+emp_nm);
	window.opener.document.prcd.<?=$wkfw_nm?>_emp_nm<? echo($approve); ?>.value = emp_nm;

	closeEmployeeList();

	self.close();
}

function groupOnChange() {
	var group_id = getSelectedValue(document.wkfw.mygroup);

	// 職員一覧セレクトボックスのオプションを全削除
	deleteAllOptions(document.wkfw.emplist);

	// 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
	if (group_id == '<? echo $tmp_group_id; ?>') {
	<? foreach($arr as $tmp_emp) { ?>
		addOption(document.wkfw.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
	<? } ?>
	}
<? } ?>
}

function set_wm_counter(ids)
{
	
	var url = 'emplist_address_counter.php';
	var params = $H({'session':'<?=$session?>','emp_ids':ids}).toQueryString();
	var myAjax = new Ajax.Request(
		url, 
		{
			method: 'post',
			postBody: params
		});
}

function changeNextNotice(val)
{
	if(val == 't')
	{
		notice_flg = false;
	}
	else
	{
		notice_flg = true;
	}

	for (var i = 0; i < document.wkfw.next_notice_div.length; i++) {
		document.wkfw.next_notice_div[i].disabled = notice_flg;
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}
table.block_in td td {border-width:1;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3" onload="initPage();">
<form name="wkfw" action="timecard_approval_list.php" method="post">
<input type="hidden" name="approve" value="<? echo($approve); ?>">


<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#5279a5">
		<td class="spacing">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff">
				<b>承認者選択</b>
			</font>
		</td>
		<td width="32" align="center">
			<a href="javascript:void(0);" onclick="closeEmployeeList();window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
		</td>
	</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<!-- 職員指定 -->
	<tr>
		<td width="20%" bgcolor="#f6f9ff">
			<table>
				<tr>
					<td>
						<input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
					</td>
				</tr>
			</table>
		</td>
		<td colspan="2" bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="3" cellpadding="0">
				<tr>
					<td width="160" height="22" valign="bottom">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象者</font>
					</td>
					<td width="30"></td>
					<td width="*">
						<select name="mygroup" onchange="groupOnChange();">
							<?
							 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
							?>
							<option value="<?=$tmp_mygroup_id?>"
							<?
							if($mygroup == $tmp_mygroup_id) {
							 echo(" selected");
							}
							?>
							><?=$tmp_mygroup?></option>
							<?
							}
							?>
						</selelct>
					</td>
				</tr>
				<tr>
					<td>
						<select name="target" size="10" multiple style="width:150px;height:150px;"></select>
					</td>
					<td>
						<input type="button" name="add_emp" value="&nbsp;&lt;&nbsp;追加" onclick="addEmp();">
						<br><br>
						<input type="button" name="delete_emp" value="削除&nbsp;&gt;&nbsp;" onclick="deleteEmp();">
					</td>
					<td>
						<select name="emplist" size="10" style="width:150px;height:150px;" multiple></select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td align="right">
			<input type="button" value="設定" onclick="setApprove();">
		</td>
	</tr>
</table>


</form>
</body>
</html>

<?
pg_close($con);
?>