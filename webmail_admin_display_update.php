<?
require_once("about_comedix.php");
require_once("webmail_quota_functions.php");
require_once("webmail_alias_functions.php");

ini_set("max_execution_time", 0);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$check_auth = check_authority($session, 66, $fname);
if ($check_auth == "0") {
	js_login_exit();
}

// 入力チェック
if ($webmail_use_quota == "t") {
	if (!preg_match("/[1-9][0-9]{0,2}\z/", $webmail_default_quota)) {
		js_alert_exit("容量は1〜999MBの間で入力してください。");
	}
} else {
	$webmail_default_quota = null;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を更新
$sql = "update config set";
$set = array("webmail_download_link", "webmail_message_id", "webmail_destination_mygroup_myself", "webmail_init_disp_address");
$setvalue = array($webmail_download_link, $webmail_message_id, $webmail_destination_mygroup_myself, $webmail_init_disp_address);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 機密機能
$conf = new Cmx_SystemConfig();
$conf->set('webmail.use_secret_checkbox', $webmail_use_secret_checkbox);

// 転送機能
if ($webmail_alias_enabled == "t") {
	webmail_alias_enable();
} else if ($webmail_alias_enabled == "f") {
	$current_alias_enabled = webmail_alias_enabled();
	if ($current_alias_enabled) {
		if (!webmail_alias_truncate($con, $fname)) {
			pg_query($con, "rollback");
			pg_close($con);
			js_error_exit();
		}
	}
	webmail_alias_disable();
}

// クォータ
$current_default_quota = webmail_quota_get_default();
webmail_quota_set_default($webmail_default_quota);
if ($current_default_quota != $webmail_default_quota) {
	change_quota($con, $current_default_quota, $webmail_default_quota, $fname);
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'webmail_admin_display.php?session=$session';</script>");

function change_quota($con, $current_default_quota, $webmail_default_quota, $fname) {
	ob_end_flush();
	ob_implicit_flush(true);
?>
<p>メールボックスを更新中です...</p>
<p>進行度：<span id="progress">0</span>%</p>
<script type="text/javascript">
function setProgress(progress) {
	document.getElementById('progress').innerHTML = progress;
}
</script>
<?
	$dir = getcwd();
	if ($webmail_default_quota > 0) {
		$command = "setquota";
		$quota = $webmail_default_quota;
	} else {
		$command = "unsetquota";
		$quota = "";
	}

	$sql = "select get_mail_login_id(emp_id) as mail_id from empmst";
	$cond = "where emp_id in (select emp_id from option where webmail_quota is null)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
	$count = pg_num_rows($sel);

	for ($i = 0; $i < $count; $i++) {
		$account = pg_fetch_result($sel, $i, "mail_id");
		webmail_quota_change($account, $quota);
		echo("<script type=\"text/javascript\">setProgress('" . floor(($i + 1) / $count * 100) . "');</script>\n");
	}
}
