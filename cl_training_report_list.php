<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("cl_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


$obj = new cl_application_workflow_common_class($con, $fname);

// ログインユーザの職員IDを取得
$arr_empmst = $obj->get_empmst($session);
$emp_id = $arr_empmst[0]["emp_id"];

$arr_trainig_report = $obj->get_trainig_report_list($emp_id);

$cl_title = cl_title_name();
//====================================
// 初期処理
//====================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜受講報告一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


function set_training_content(apply_id)
{
    for(i=1; i<=7; i++)
    {
        var date_y = opener.document.getElementById('date_y' + i).value;
        var date_m = opener.document.getElementById('date_m' + i).value;
        var date_d = opener.document.getElementById('date_d' + i).value;
        var training_name = opener.document.getElementById('training_name' + i).value;
        var hour = opener.document.getElementById('hour' + i).value;

        if(date_y == "-" && date_m == "-" && date_d == "-" && training_name == "" && hour == "")
        {
            opener.document.getElementById('date_y' + i).value = document.getElementById( apply_id + '_date_y2').value;
            opener.document.getElementById('date_m' + i).value = document.getElementById( apply_id + '_date_m2').value;
            opener.document.getElementById('date_d' + i).value = document.getElementById( apply_id + '_date_d2').value;
            opener.document.getElementById('training_name' + i).value = document.getElementById( apply_id + '_course_theme').value;
            opener.document.getElementById('hour' + i).value = document.getElementById( apply_id + '_course_times').value;
            break;
        }
    }
}

</script>


</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>受講報告一覧</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<?
if(count($arr_trainig_report) == 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受講報告はございません。</font>
</td>
</tr>
</table>
<?
}
else
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#E5F6CD" align="center">
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修コード</font></td>
<td width="*"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">研修名</font></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日程</font></td>
</tr>

<?
foreach($arr_trainig_report as $trainig_report)
{
    $apply_id = $trainig_report["apply_id"];
    $apply_content = $trainig_report["apply_content"];
    $arr_content = $obj->get_training_content($apply_content);

    $course_code = $arr_content[0]["course_code"];
    $course_theme = $arr_content[0]["course_theme"];
    $purpose = $arr_content[0]["purpose"];
    $outline = $arr_content[0]["outline"];

    $date_y2 = $arr_content[0]["date_y2"];
    $date_m2 = $arr_content[0]["date_m2"];
    $date_d2 = $arr_content[0]["date_d2"];

    $course_days = $arr_content[0]["course_days"];
    $course_times = $arr_content[0]["course_times"];

    $start_ymd = "";
    if($date_y2 != "" && $date_m2 != "" && $date_d2 != "")
    {
        $start_ymd = $date_y2."/".$date_m2."/".$date_d2;
    }
?>
<tr>
<td align="center"><input type="button" value="選択" onclick="set_training_content('<?=$apply_id?>')"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$course_code?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($course_theme)?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$start_ymd?></font></td>
</tr>

<input type="hidden" id="<?=$apply_id?>_course_theme" value="<?=$course_theme?>">

<input type="hidden" id="<?=$apply_id?>_date_y2" value="<?=$date_y2?>">
<input type="hidden" id="<?=$apply_id?>_date_m2" value="<?=$date_m2?>">
<input type="hidden" id="<?=$apply_id?>_date_d2" value="<?=$date_d2?>">

<input type="hidden" id="<?=$apply_id?>_course_times" value="<?=$course_times?>">
<?
}
?>
</table>
<?
}
?>

<center>
</body>
</form>
</html>


<?

pg_close($con);
?>