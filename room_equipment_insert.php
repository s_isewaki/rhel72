<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("./conf/sql.inf");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$wardreg = check_authority($session,21,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

//正規表現チェック
if($eqp_nm ==""){
	echo("<script language=\"javascript\">alert(\"設備名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

/*
$check = special_char_check($eqp_nm);
if($check == 0){
	echo("<script language=\"javascript\">alert(\"特殊な文字は使用しないでください\");</script>\n");
	echo("<script language=\"javascript\">location.href=\"./status_menu.php?session=$session\";</script>\n");
	exit;
}
*/

//ＤＢへ設備名を登録する

$cond = "";
$sel = select_from_table($con,$SQL73,$cond,$fname);	//設備ＩＤの最大値を取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$max = pg_result($sel,0,"max");
if($max == 0){
	$next_id = "1";
}else{
	$next_id = $max + 1;
}

$content = array("$next_id","$eqp_nm","f");

$in = insert_into_table($con,$SQL74,$content,$fname);	//役職情報を挿入
pg_close($con);											//ＤＢとのコネクションを閉じる

if($in == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}else{
	echo("<script language=\"javascript\">location.href = 'room_equipment_register.php?session=$session&bldg_cd=$bldg_cd&ward_cd=$ward_cd';</script>\n");
	exit;
}

?>