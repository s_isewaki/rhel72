<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_select_values.ini");
require_once("atdbk_common_class.php");
require_once("work_admin_paid_holiday_common.php");
require_once("show_timecard_common.ini");


// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}

// DBコネクション作成
$con = connect2db($fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

// 締め日を取得
$sql = "select closing, closing_parttime, closing_paid_holiday, criteria_months from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}
//基準月数
$criteria_months = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "criteria_months") : "2";
$wk_month = ($criteria_months == "1") ? "3" : "6";


//年設定
//付与年月日入力の場合
if ($select_add_yr != "-" && $select_add_yr != "") {
	$year = $select_add_yr;
} elseif ($select_close_yr != "") {
	//
	$cur_year = $select_close_yr;
	$md = $select_close_mon.$select_close_day;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
	//指定年
	$year = $cur_year;
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
else {
	$cur_year = date("Y");
	$md = date("md");;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
}


$arr_class_name = get_class_name_array($con, $fname);

// 組織情報を取得し、配列に格納
$arr_cls = array();
$arr_clsatrb = array();
$arr_atrbdept = array();
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));
	
	$sql2 = "select atrb_id, atrb_nm from atrbmst";
    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' order by order_no";
	$sel2 = select_from_table($con, $sql2, $cond2, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_atrb = array();
	while ($row2 = pg_fetch_array($sel2)) {
		$tmp_atrb_id = $row2["atrb_id"];
		$tmp_atrb_nm = $row2["atrb_nm"];
		array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));
		
		$sql3 = "select dept_id, dept_nm from deptmst";
        $cond3 = "where atrb_id = '$tmp_atrb_id' and dept_del_flg = 'f' order by order_no";
		$sel3 = select_from_table($con, $sql3, $cond3, $fname);
		if ($sel3 == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_dept = array();
		while ($row3 = pg_fetch_array($sel3)) {
			$tmp_dept_id = $row3["dept_id"];
			$tmp_dept_nm = $row3["dept_nm"];
			array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
		}
		array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
	}
	array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
}

// 該当する職員を検索
$sel_emp = false;
$arr_data = array();
if ($srch_flg == "1") {
	
	$select_close_date = $select_close_yr.$select_close_mon.$select_close_day;
    $cond = get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_date, $srch_id);
	
	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}
	//sql文取得
	$sql = get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_yr, $select_close_mon, $select_close_day, $sort);
	
	switch ($sort) {
		case "1":
			$orderby = "empmst.emp_personal_id ";
			break;
		case "2":
			$orderby = "empmst.emp_personal_id desc ";
			break;
		case "3":
			$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
			break;
		case "4":
			$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
			break;
		case "5":
            $orderby = "c.order_no, d.order_no, e.order_no, empmst.emp_personal_id ";
            break;
		case "6":
            $orderby = "c.order_no desc, d.order_no desc, e.order_no desc, empmst.emp_personal_id ";
            break;
	}
	$cond .= " order by $orderby ";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	
}
$sql = "select closing_paid_holiday from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
while($row = pg_fetch_array($sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=8; $i++) {
		$varname = "add_day".$i;
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120329
        $wk_days = $row["$varname"];
        if ($i == 8 && $wk_days == "") {
            $wk_days = $arr_paid_hol[$paid_hol_tbl_id][7];
        }
        if ($wk_days == "") {
            $wk_days = "0";
        }
        $arr_paid_hol[$paid_hol_tbl_id][$i] = $wk_days;
	}
}

$title_year_str = ($closing_paid_holiday == "1") ? "年度" : "年";
?>
<title>CoMedix 勤務管理｜有休日数一覧（<? echo($year); echo($title_year_str); ?>）</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="self.print();self.close();">

<?
	//年度、一覧表示
	$select_close_date = $select_close_yr.$select_close_mon.$select_close_day;
	$select_add_date = $select_add_yr.$select_add_mon.$select_add_day;
?>
<? get_emp_list_data($con, $fname, $sel_emp, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, "3"); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="check_all" value="1">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="disp_year" value="<? echo($year); ?>">
<input type="hidden" name="start_year" value="<? echo($start_year); ?>">
<input type="hidden" name="update_flg" value="">
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
