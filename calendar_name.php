<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | カレンダー | 勤務日</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<?
require_once("about_comedix.php");
require_once("maintenance_menu_list.ini");
require_once("show_select_values.ini");
require_once("calendar_name_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 勤務日設定値を取得
$sql = "select * from calendarname";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) > 0) {
	$day1_time = pg_fetch_result($sel, 0, "day1_time");
	$day2 = pg_fetch_result($sel, 0, "day2");
	$day3 = pg_fetch_result($sel, 0, "day3");
	$day4 = pg_fetch_result($sel, 0, "day4");
	$day4_time = pg_fetch_result($sel, 0, "day4_time");
	$day5 = pg_fetch_result($sel, 0, "day5");
	$day5_time = pg_fetch_result($sel, 0, "day5_time");
	$am_time = pg_fetch_result($sel, 0, "am_time");
	$pm_time = pg_fetch_result($sel, 0, "pm_time");
	
	$day1_hour = substr($day1_time, 0, 2);
	$day1_min = substr($day1_time, 2, 2);
	$day4_hour = substr($day4_time, 0, 2);
	$day4_min = substr($day4_time, 2, 2);
	$day5_hour = substr($day5_time, 0, 2);
	$day5_min = substr($day5_time, 2, 2);
	$am_hour = substr($am_time, 0, 2);
	$am_min = substr($am_time, 2, 2);
	$pm_hour = substr($pm_time, 0, 2);
	$pm_min = substr($pm_time, 2, 2);
}
// 所定労働時間変更履歴を取得
$calendar_name_class = new calendar_name_class($con, $fname);
$arr_timehist = $calendar_name_class->get_history("desc");

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
/* 所定労働時間 履歴 */
function time_history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.timehist.action = 'calendar_name_time_history_delete.php';
        document.timehist.calendarname_time_history_id.value = id;
        document.timehist.submit();
    }
}
function time_history_edit(id) {
    if (id) {
        document.timehist.calendarname_time_history_id.value = id;
    }
    document.timehist.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.hist {
    border-collapse:collapse;
    margin: 10px 0 0 0;
}
table.hist thead td {
    background-color: #f6f9ff;
    font-family: ＭＳ Ｐゴシック, Osaka;
    padding: 3px 5px;
    text-align: center;
    border: 1px solid #000;
}
table.hist tbody td {
    font-family: ＭＳ Ｐゴシック, Osaka;
    padding: 3px 5px;
    border: 1px solid #000;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="./calendar_name.php?session=<? echo($session); ?>"><b>カレンダー</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="calendar_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>勤務日</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="calendar_year.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年間カレンダー</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="calendar_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form action="./calendar_name_update_exe.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>通常勤務日</b></font></td>
</tr>
<tr>
<td height="22" width="140" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働時間：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="day1_hour"><? show_hour_options_0_23($day1_hour); ?></select>時間<select name="day1_min"><? show_min_options_00_59($day1_min); ?></select>分</font></td>
</tr>
<tr>
<td height="22" width="140" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午前所定時間：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="am_hour"><? show_hour_options_0_23($am_hour); ?></select>時間<select name="am_min"><? show_min_options_00_59($am_min); ?></select>分</font></td>
</tr>
<tr>
<td height="22" width="140" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午後所定時間：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="pm_hour"><? show_hour_options_0_23($pm_hour); ?></select>時間<select name="pm_min"><? show_min_options_00_59($pm_min); ?></select>分</font></td>
</tr>
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>法定休日</b></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称：</font></td>
<td><input type="text" name="day2" value="<? echo($day2); ?>" size="20" maxlength="12" style="ime-mode:active;"></td>
</tr>
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>所定休日</b></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称：</font></td>
<td><input type="text" name="day3" value="<? echo($day3); ?>" size="20" maxlength="12" style="ime-mode:active;"></td>
</tr>
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務日1</b></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称：</font></td>
<td><input type="text" name="day4" value="<? echo($day4); ?>" size="20" maxlength="12" style="ime-mode:active;"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">労働時間：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="day4_hour"><? show_hour_options_0_23($day4_hour); ?></select>時間<select name="day4_min"><? show_min_options_15($day4_min); ?></select>分</font></td>
</tr>
<tr>
<td height="22" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>勤務日2</b></font></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称：</font></td>
<td><input type="text" name="day5" value="<? echo($day5); ?>" size="20" maxlength="12" style="ime-mode:active;"></td>
</tr>
<tr>
<td height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">労働時間：</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="day5_hour"><? show_hour_options_0_23($day5_hour); ?></select>時間<select name="day5_min"><? show_min_options_15($day5_min); ?></select>分</font></td>
</tr>
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>

<?
//履歴タブが増える場合は、職員登録employee_detail.phpを参考にして変更する
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="140" height="22" align="center" bgcolor="#5279a5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>所定労働時間履歴</b></font></td>
<td width="5">&nbsp;</td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<div class="hist" id="timehist">
<form name="timehist" action="calendar_name_time_history_form.php" method="post">
<button type="button" onclick="time_history_edit();">所定労働時間の履歴を手動で追加する</button>
<? if (count($arr_timehist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">所定労働時間</td>
    <td class="j12">半日午前所定時間</td>
    <td class="j12">半日午後所定時間</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? foreach ($arr_timehist as $r) {?>
<tr>
    <td class="j12" align="center"><?php
        if ($r["day1_time"] != "") {
            eh(intval(substr($r["day1_time"],0,2)).":".substr($r["day1_time"],2,2));
        } ?></td>
    <td class="j12" align="center"><?php
        if ($r["am_time"] != "") {
            eh(intval(substr($r["am_time"],0,2)).":".substr($r["am_time"],2,2));
        } ?></td>
    <td class="j12" align="center"><?php
        if ($r["pm_time"] != "") {
            eh(intval(substr($r["pm_time"],0,2)).":".substr($r["pm_time"],2,2));
        } ?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="time_history_edit(<?php eh($r["calendarname_time_history_id"]);?>);">修正</button></td>
    <td><button type="button" onclick="time_history_delete(<?php eh($r["calendarname_time_history_id"]);?>);">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="calendarname_time_history_id" value="">
</form>
</div>

</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
