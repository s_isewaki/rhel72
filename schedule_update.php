<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_schedule_common.ini");
require_once("show_facility.ini");
require_once("schedule_repeat_common.php");
require_once("mygroup_common.php");
require_once("show_class_name.ini");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 2, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// スケジュール情報を取得
$table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
$sql = "select $table_name.*, empmst.emp_lt_nm, empmst.emp_ft_nm from $table_name inner join empmst on empmst.emp_id = $table_name.schd_reg_id";
$cond = "where schd_id = '$schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
// 削除済みの場合
if (pg_num_rows($sel) == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('この予約は既に削除されています。');window.opener.location.reload();self.close();</script>");
    exit;
}
$schd = pg_fetch_assoc($sel);
$schd_emp_id = pg_fetch_result($sel, 0, "emp_id");
$schd_title = pg_fetch_result($sel, 0, "schd_title");
$marker = pg_fetch_result($sel, 0, "marker");
$place_id = pg_fetch_result($sel, 0, "schd_place_id");
$schd_plc = pg_fetch_result($sel, 0, "schd_plc");
$schd_imprt = pg_fetch_result($sel, 0, "schd_imprt");
$schd_start_date = pg_fetch_result($sel, 0, "schd_start_date");
list($year, $month, $day) = split("-", $schd_start_date);
$schd_start_time_v = pg_fetch_result($sel, 0, "schd_start_time_v");
$schd_start_hrs = substr($schd_start_time_v, 0, 2);
$schd_start_min = substr($schd_start_time_v, 2, 2);
$schd_dur_v = pg_fetch_result($sel, 0, "schd_dur_v");
$schd_dur_hrs = substr($schd_dur_v, 0, 2);
$schd_dur_min = substr($schd_dur_v, 2, 2);

$schd_type = pg_fetch_result($sel, 0, "schd_type");
$schd_detail = pg_fetch_result($sel, 0, "schd_detail");
$reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
$reg_time = pg_fetch_result($sel, 0, "reg_time");
$reg_emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$schd_status = pg_fetch_result($sel, 0, "schd_status");
$repeat_flg = (pg_fetch_result($sel, 0, "repeat_flg") == "t");
$pub_limit = pg_fetch_result($sel, 0, "pub_limit");

$alarm_h = intval($schd["alarm"] / 60);
$alarm_m = $schd["alarm"] % 60;

$schedule_dt = "$schd_start_date $schd_start_time_v";
$now_dt = date('Y-m-d H:i:s');

// 設備予約からの登録かどうかチェック
$table_name = ($timeless != "t") ? "reservation" : "reservation2";
$sql = "select count(*) from $table_name";
$cond = "where emp_id = '$reg_emp_id' and reg_time = '$reg_time' and date = '$year$month$day' and reservation_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$from_facility_flg = (pg_fetch_result($sel, 0, 0) > 0);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");

if ($login_emp_id == $reg_emp_id) {
    $reg_emp_flg = "t";
}
else {
    $reg_emp_flg = "f";
}

// 繰返し登録でも1件のみになっている場合はオプションを表示しない
if ($repeat_flg) {
    $repeat_schedules = get_repeated_schedules($con, $schd_id, $timeless, 2, $fname, true);
    if (count($repeat_schedules) == 1) {
        $repeat_flg = false;
    }
}

// 予定ステータスの設定
switch ($schd_status) {
    case "2":
        $status_name = "未承認";
        break;
    case "3":
        $status_name = "承認";
        break;
    default:
        $status_name = "";
        break;
}
if ($from_facility_flg) {
    if ($status_name == "") {
        $status_name = "設備予約と連動";
    }
    else {
        $status_name .= "（設備予約と連動）";
    }
}

if ($login_emp_id == $schd_emp_id || $login_emp_id == $reg_emp_id) {
    $doc_title = "更新";
}
else {
    $doc_title = "詳細";
}


// 対象職員情報を配列に格納
$arr_target_id = get_emp_id_from_schedules($con, $schd_id, $timeless, $fname);
$arr_target = array();
for ($i = 0; $i < count($arr_target_id); $i++) {
    $tmp_emp_id = $arr_target_id[$i];
    $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
    $cond = "where emp_id = '$tmp_emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    }
    $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
    if ($tmp_emp_id == $schd_emp_id) {
        $schd_emp_name = $tmp_emp_name;
    }
    array_push($arr_target, array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
}

// マイグループと職員名リスト取得
list($mygroups, $employees) = get_mygroups_employees($con, $fname, $login_emp_id);

// 公開範囲の現在値を取得
if ($pub_limit == "") {
    $pub_limit_label = "全て";
}
else {
    $pub_limit_type = substr($pub_limit, 0, 1);
    $pub_limit_id = substr($pub_limit, 1);
    switch ($pub_limit_type) {
        case "d":
            switch ($pub_limit_id) {
                case "1":
                    $pub_limit_label = $arr_class_name["class_nm"];
                    break;
                case "2":
                    $pub_limit_label = $arr_class_name["atrb_nm"];
                    break;
                case "3":
                    $pub_limit_label = $arr_class_name["dept_nm"];
                    break;
                case "4":
                    $pub_limit_label = $arr_class_name["room_nm"];
                    break;
            }
            break;
        case "c":
            $sql = "select group_nm from comgroupmst";
            $cond = "where group_id = $pub_limit_id";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $pub_limit_label = pg_fetch_result($sel, 0, "group_nm");
            break;
    }
}

// 公開範囲用のグループ一覧を作成
$pub_limit_group = array();
$sql = "select m.group_id, m.group_nm from comgroupmst m";
$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$login_emp_id') order by m.group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $pub_limit_group["c" . $row["group_id"]] = $row["group_nm"];
}

if ($pub_limit != "") {
    switch ($pub_limit_type) {
        case "d":
            $pub_limit_dept = $pub_limit_id;
            $pub_limit = "d";
            break;
        case "c":
            if (isset($pub_limit_group[$pub_limit])) {
                $pub_limit_group_id = $pub_limit;
                $pub_limit = "g";
            }
            break;
    }
}

// アラーム機能
$conf = new Cmx_SystemConfig();
$schedule_alarm = $conf->get('schedule.alarm');
$schedule_alarm_start = $conf->get('schedule.alarm.start');
$schedule_alarm_end = $conf->get('schedule.alarm.end');

$salarm_start = '';
if ($schedule_alarm_start) {
    $salarm_start_h = intval($schedule_alarm_start / 60);
    $salarm_start_m = $schedule_alarm_start % 60;
    if ($salarm_start_h) {
        $salarm_start .= $salarm_start_h . '時間';
    }
    if ($salarm_start_m) {
        $salarm_start .= $salarm_start_m . '分';
    }
    $salarm_start .= '前';
}

$salarm_end = '';
if ($schedule_alarm_end) {
    $salarm_end_h = intval($schedule_alarm_end / 60);
    $salarm_end_m = $schedule_alarm_end % 60;
    if ($salarm_end_h) {
        $salarm_end .= $salarm_end_h . '時間';
    }
    if ($salarm_end_m) {
        $salarm_end .= $salarm_end_m . '分';
    }
    $salarm_end .= '前';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | <? echo($doc_title); ?></title>
<?
require_once("schedule_registration_javascript.php");
insert_javascript();
?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function initPage() {
    setDayOptions('<? echo($day); ?>');

    setTimeDisabled();
    //登録対象者を設定する。
<?
if ($reg_emp_flg == "t") {
    echo("  update_target_html();\n");
} else {
    echo("  show_target_html();\n");
}
?>
    document.mainform.old_target_id_list.value = document.mainform.target_id_list.value;
    setMailDisabled();
    groupOnChange();
}

function setDayOptions(selectedDay) {
    var dayPulldown = document.mainform.schd_start_day;
    if (!selectedDay) {
        selectedDay = dayPulldown.value;
    }
    deleteAllOptions(dayPulldown);

    var year = parseInt(document.mainform.schd_start_yrs.value, 10);
    var month = parseInt(document.mainform.schd_start_mth.value, 10);
    var daysInMonth = new Date(year, month, 0).getDate();

    for (var d = 1; d <= daysInMonth; d++) {
        var tmpDayValue = d.toString();
        if (tmpDayValue.length == 1) {
            tmpDayValue = '0'.concat(tmpDayValue);
        }

        var tmpDayText = d.toString();

        var tmpDate = new Date(year, month - 1, d);
        var tmpWeekDay = tmpDate.getDay();
        switch (tmpWeekDay) {
        case 0:
            tmpDayText = tmpDayText.concat('（日）');
            break;
        case 1:
            tmpDayText = tmpDayText.concat('（月）');
            break;
        case 2:
            tmpDayText = tmpDayText.concat('（火）');
            break;
        case 3:
            tmpDayText = tmpDayText.concat('（水）');
            break;
        case 4:
            tmpDayText = tmpDayText.concat('（木）');
            break;
        case 5:
            tmpDayText = tmpDayText.concat('（金）');
            break;
        case 6:
            tmpDayText = tmpDayText.concat('（土）');
            break;
        }

        addOption(dayPulldown, tmpDayValue, tmpDayText);
    }

    while (parseInt(selectedDay, 10) > daysInMonth) {
        selectedDay = (parseInt(selectedDay, 10) - 1).toString();
    }
    dayPulldown.value = selectedDay;
}

// 登録対象者表示
function show_target_html()
{
    //登録対象者HTMLを更新
    _show_target_html2(document.getElementById("target_disp_area"),document.getElementById("target_id_list"),document.getElementById("target_name_list"));
}

function _show_target_html2(disp_obj,target_hidden_id,target_hidden_name)
{
    if(m_target_list["ID"].length == 0)
    {
        disp_obj.innerHTML = "";
        target_hidden_id.value = "";
        target_hidden_name.value = "";
    }
    else
    {
        var disp_obj_html = "";
        var target_hidden_id_value = "";
        var target_hidden_name_value = "";
        for(var i=0;i<m_target_list["ID"].length;i++)
        {
            var emp_id   = m_target_list["ID"][i].emp_id;
            var emp_name = m_target_list["ID"][i].emp_name;
            if(i!=0)
            {
                disp_obj_html = disp_obj_html + ",";
                target_hidden_id_value = target_hidden_id_value + ",";
                target_hidden_name_value = target_hidden_name_value + ",";
            }

            disp_obj_html = disp_obj_html + emp_name;

            target_hidden_id_value = target_hidden_id_value + emp_id;
            target_hidden_name_value = target_hidden_name_value + emp_name;
        }
        disp_obj.innerHTML = disp_obj_html;
        target_hidden_id.value = target_hidden_id_value;
        target_hidden_name.value = target_hidden_name_value;
    }
}

function setDateDisabled(disabled) {
    document.mainform.schd_start_yrs.disabled = disabled;
    document.mainform.schd_start_mth.disabled = disabled;
    document.mainform.schd_start_day.disabled = disabled;
}

function setTimeDisabled() {
    var disabled = document.mainform.timeless.checked;
    document.mainform.schd_start_hrs.disabled = disabled;
    document.mainform.schd_start_min.disabled = disabled;
    document.mainform.schd_dur_hrs.disabled = disabled;
    document.mainform.schd_dur_min.disabled = disabled;
}

function deleteSchedule() {
    if (confirm('削除します。よろしいですか？')) {
        var rptopt = '';
        if (document.mainform.rptopt) {
            for (var i = 0, j = document.mainform.rptopt.length; i < j; i++) {
                if (document.mainform.rptopt[i].checked) {
                    rptopt = i + 1;
                    break;
                }
            }
        }
        location.href = 'schedule_delete_checker.php?reg_emp_flg=<? echo($reg_emp_flg); ?>&session=<? echo($session); ?>&schd_id=<? echo($schd_id); ?>&timeless=<? echo($timeless); ?>&rptopt='.concat(rptopt);
    }
}

function alarm_confirm() {
    $.ajax({
        type: "POST",
        url: 'schedule_alarm_confirm.php',
        data: {
            session: '<?php eh($session);?>',
            schd_id: '<?php eh($schd_id); ?>',
            timeless: '<?php eh($timeless); ?>'
        },
        success: function(){
            location.reload();
        },
        error: function(){
            location.href = 'error.php';
        }
    });
}

function checkEndMinute() {
    if (document.mainform.schd_dur_hrs.value == '24' &&
        document.mainform.schd_dur_min.value != '00') {
        document.mainform.schd_dur_min.value = '00';
        alert('終了時刻が24時の場合には00分しか選べません。');
    }
}
var childwin = null;
function openEmployeeList() {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './schedule_address_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$login_emp_id?>';
    url += '&mode=1';
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    $script = "m_target_list['ID'] = new Array(";
    $is_first = true;
    foreach($arr_target as $row)
    {
        if($is_first)
        {
            $is_first = false;
        }
        else
        {
            $script .= ",";
        }
        $emp_id = $row["id"];
        $emp_name = $row["name"];
        $script .= "new user_info('$emp_id','$emp_name')";
    }
    $script .= ");\n";
    print $script;
?>

function groupOnChange() {
    var group_id = getSelectedValue(document.mainform.mygroup);

    // 職員一覧セレクトボックスのオプションを全削除
    deleteAllOptions(document.mainform.emplist);

    // 職員一覧セレクトボックスのオプションを作成
<? foreach ($employees as $tmp_group_id => $arr) { ?>
    if (group_id == '<? echo $tmp_group_id; ?>') {
    <? foreach($arr as $tmp_emp) { ?>
        addOption(document.mainform.emplist, '<? echo $tmp_emp["emp_id"]; ?>', '<? echo $tmp_emp["name"]; ?>');
    <? } ?>
    }
<? } ?>
}

function setTargetList() {
    document.mainform.target_id_list.value = '';
    for (var i = 0, j = document.mainform.target.options.length; i < j; i++) {
        if (i > 0) {
            document.mainform.target_id_list.value += ',';
        }
        document.mainform.target_id_list.value += document.mainform.target.options[i].value;
    }
    setMailDisabled();
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
}

function getSelectedValue(sel) {
    return sel.options[sel.selectedIndex].value;
}

function setMailDisabled() {
    var disabled = (
        document.mainform.target_id_list.value == '<? echo($login_emp_id); ?>' ||
        document.mainform.target_id_list.value == '' ||
        document.mainform.target_id_list.value == document.mainform.old_target_id_list.value
    );
    document.mainform.sendmail.disabled = disabled;
}

function addEmp() {
    var emp_id_str = "";
    var emp_name_str = "";
    var first_flg = true;
    for (var i = 0, j = document.mainform.emplist.options.length; i < j; i++) {
        if (document.mainform.emplist.options[i].selected) {
            var emp_id = document.mainform.emplist.options[i].value;
            var emp_name = document.mainform.emplist.options[i].text;
            if (first_flg == true) {
                first_flg = false;
            } else {
                emp_id_str += ", ";
                emp_name_str += ", ";
            }
            emp_id_str += emp_id;
            emp_name_str += emp_name;
        }
    }
    if (emp_id_str != "") {
        set_wm_counter(emp_id_str);
        add_target_list(emp_id_str, emp_name_str);
    }
}

</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list, .list td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<form method="post" name="mainform" action="schedule_update_checker.php">
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo($doc_title); ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td><input type="text" name="schd_title" maxlength="100" size="50" value="<? eh($schd_title); ?>" style="ime-mode:active;"></td>
<td width="110" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ラインマーカー</font>
<td><select name="marker">
<option value="0" style="background-color:white;color:black;"<? if ($marker == 0) {echo(" selected");} ?>>なし
<option value="1" style="background-color:red;color:white;"<? if ($marker == 1) {echo(" selected");} ?>>赤
<option value="2" style="background-color:aqua;color:blue;"<? if ($marker == 2) {echo(" selected");} ?>>青
<option value="3" style="background-color:yellow;color:blue;"<? if ($marker == 3) {echo(" selected");} ?>>黄
<option value="4" style="background-color:lime;color:blue;"<? if ($marker == 4) {echo(" selected");} ?>>緑
<option value="5" style="background-color:fuchsia;color:white;"<? if ($marker == 5) {echo(" selected");} ?>>ピンク
</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行先</font></td>
<td colspan="3"><select name="place_id"><option value=""<? if ($place_id == "") {echo(" selected");} ?>></option><? show_schedule_place_selected($con, $place_id, $fname); ?><option value="0"<? if ($place_id == "0") {echo(" selected");} ?>>その他</option></select>&nbsp;<input type="text" name="schd_plc" maxlength="100" size="35" value="<? echo($schd_plc); ?>" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定の公開</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="schd_imprt" value="1"<? if ($schd_imprt == "1") {echo(" checked");} ?>>私用 (自分以外に予定は公開されません)
<br>
<input type="radio" name="schd_imprt" value="2"<? if ($schd_imprt == "2") {echo(" checked");} ?>>公開 （<?
if ($schd_imprt == "2") {
    echo("<input type=\"radio\" name=\"pub_limit\" value=\"nc\" checked onclick=\"this.form.schd_imprt[1].checked = true;\">変更しない（" . $pub_limit_label . "）\n");
}

echo("<input type=\"radio\" name=\"pub_limit\" value=\"\"");
if ($schd_imprt != "2") {
    echo(" checked");
}
echo(" onclick=\"this.form.schd_imprt[1].checked = true;\">全て\n");

echo("<input type=\"radio\" name=\"pub_limit\" value=\"d\" onclick=\"this.form.schd_imprt[1].checked = true;\">所属 ");
echo("<select name=\"pub_limit_dept\" onchange=\"this.form.schd_imprt[1].checked = true; this.form.pub_limit[2].checked = true;\">\n");
    echo("<option value=\"1\"");
    if ($pub_limit_dept == "1") echo(" selected");
    echo(">" . $arr_class_name["class_nm"] . "\n");

    echo("<option value=\"2\"");
    if ($pub_limit_dept == "2") echo(" selected");
    echo(">" . $arr_class_name["atrb_nm"] . "\n");

    echo("<option value=\"3\"");
    if ($pub_limit_dept == "3") echo(" selected");
    echo(">" . $arr_class_name["dept_nm"] . "\n");

    if ($arr_class_name["class_cnt"] == 4) {
        echo("<option value=\"4\"");
        if ($pub_limit_dept == "4") echo(" selected");
        echo(">" . $arr_class_name["room_nm"] . "\n");
    }
echo("</select>\n");

if (count($pub_limit_group) > 0) {
    echo("<input type=\"radio\" name=\"pub_limit\" value=\"g\" onclick=\"this.form.schd_imprt[1].checked = true;\">");
    echo("<select name=\"pub_limit_group_id\" onchange=\"this.form.schd_imprt[1].checked = true; this.form.pub_limit[3].checked = true;\">\n");
    foreach ($pub_limit_group as $tmp_pub_limit_group_id => $tmp_pub_limit_group_nm) {
        echo("<option value=\"$tmp_pub_limit_group_id\"");
        if ($pub_limit_group_id == $tmp_pub_limit_group_id) echo(" selected");
        echo(">" . $tmp_pub_limit_group_nm . "\n");
    }
    echo("</select>\n");
}
?>）
<br>
<input type="radio" name="schd_imprt" value="3"<? if ($schd_imprt == "3") {echo(" checked");} ?>>自分以外には「予定あり」とのみ表示
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="schd_start_yrs" onchange="setDayOptions();"><? show_select_years_span(min($year, 2000), max($year, date("Y") + 3), $year); ?></select>/<select name="schd_start_mth" onchange="setDayOptions();"><? show_select_months($month); ?></select>/<select name="schd_start_day"></select></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="schd_start_hrs"><? show_hour_options_0_23($schd_start_hrs, false); ?></select>：<select name="schd_start_min"><? show_min_options_5($schd_start_min, false); ?></select>〜<select name="schd_dur_hrs" onchange="checkEndMinute();"><? show_select_hrs_by_args(0, 24, $schd_dur_hrs, false); ?></select>：<select name="schd_dur_min" onchange="checkEndMinute();"><? show_min_options_5($schd_dur_min, false); ?></select><br>
<input type="checkbox" name="timeless" value="t"<? if ($timeless == "t") {echo(" checked");} ?> onclick="setTimeDisabled();">指定しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_schd_type_radio($con, $schd_type, $fname); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">詳細</font></td>
<td colspan="3"><textarea name="schd_detail" rows="5" cols="40" style="ime-mode:active;"><? eh($schd_detail); ?></textarea></td>
</tr>
<?
// 登録者の場合に対象者を表示
$display_target = ($login_emp_id != $reg_emp_id) ? "none" : "";
?>
<tr height="150">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録対象者</font><br>
<span style="display:<?=$display_target?>">
<input type="button" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList();"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('<?=$schd_emp_id?>','<?=$schd_emp_name?>');"><br></span></td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area"></span>
</font>

</td>
<td style="display:<?=$display_target?>">
<input type="button" value="&lt;&nbsp;追加" style="margin-left:1em;width=4.5em;" onclick="addEmp();">
</td>
<td width="10">
</td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="1" class="non_in_list">
<tr style="display:<?=$display_target?>">
<td height="45%" align="center" valign="bottom">

<select name="mygroup" onchange="groupOnChange();">
<?
 foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup) {
?>
<option value="<?=$tmp_mygroup_id?>"
<?
        if ($mygroup == $tmp_mygroup_id) {
            echo(" selected");
        }
?>
><?=$tmp_mygroup?></option>
<?
}
?>
</selelct>
</td></tr>
<tr style="display:<?=$display_target?>"><td>
<select name="emplist" size="10" multiple style="width:150px;">
</select>

</td>
</tr>
<tr style="display:<?=$display_target?>">
<td height="45%" valign="top"><input type="button" value="全て選択" onclick="selectAllEmp();"></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr style="display:<?=$display_target?>">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録通知メール</font>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="sendmail" value="t"<? if ($sendmail == "t") {echo(" checked");} ?>>送信する<font color="gray">（自分以外の追加された登録対象者にウェブメールを送信します。）</font></font></td>
</tr>

<?php if ($schedule_alarm) {?>
<tr>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アラーム</font></td>
<td colspan="3" class="j12">
<?php if ($schedule_alarm === "1") {?>
<input type="number" name="alarm_h" size="4" value="<?php eh($alarm_h);?>" class="numeric" />時間 <input type="number" name="alarm_m" size="4" value="<?php eh($alarm_m);?>" class="numeric" />分前に通知する<br />
<?php
if ($salarm_start or $salarm_end) {
    print "（設定可能範囲: {$salarm_start} 〜 {$salarm_end}）";
}
?>
<?php } else if ($schedule_alarm === "2") {?>
<label><input type="checkbox" name="alarm_m" value="1" <?php if ($alarm_h or $alarm_m) { echo 'checked'; }?>/>通知する</label>
<?php }?>
</td>
</tr>
<?php }?>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reg_emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予定ステータス</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($status_name); ?></font></td>
</tr>
<? if ($repeat_flg) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰返しオプション</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="rptopt" value="1" checked onclick="setDateDisabled(false);">1件のみ
<input type="radio" name="rptopt" value="2" onclick="setDateDisabled(true);">全て
<input type="radio" name="rptopt" value="3" onclick="setDateDisabled(true);">将来の予定のみ<br>
<font color="red">※この予定は繰返し登録されています。承認／更新／削除時にはこのオプションが有効になります。</font>
</font></td>
</tr>
<? } ?>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<? if ($login_emp_id == $schd_emp_id && $schd_status == "2") { ?>
<input type="submit" name="submit" value="承認">
<? } ?>
<? if ($login_emp_id == $schd_emp_id || $login_emp_id == $reg_emp_id) { ?>
<input type="submit" name="submit" value="更新"<? if ($from_facility_flg) {echo(" disabled");} ?>>
<input type="button" value="削除" onclick="deleteSchedule();"<? if ($from_facility_flg) {echo(" disabled");} ?>>
<? } ?>
<?php if ($login_emp_id == $schd_emp_id and $schedule_dt >= $now_dt and $schedule_alarm and $schd["alarm"] ) {?>
<input type="button" value="アラーム確認" onclick="alarm_confirm();" <? if ($schd["alarm_confirm"]) {echo(" disabled");} ?>/>
<?php }?>
</td>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="schd_id" value="<? echo($schd_id); ?>">
<input type="hidden" name="original_timeless" value="<? echo($timeless); ?>">
<input type="hidden" id="target_id_list" name="target_id_list" value="">
<input type="hidden" id="old_target_id_list" name="old_target_id_list" value="">
<input type="hidden" id="target_name_list" name="target_name_list" value="">
<input type="hidden" name="reg_emp_flg" value="<? echo($reg_emp_flg); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
