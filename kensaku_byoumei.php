<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require_once("medis_version.ini");

//ページ名
$fname = $PHP_SELF;


//==============================
//パラメータ精査
//==============================
if($search_pattern != "zen" && $search_pattern != "kou")
{
	$search_pattern = "bubun";
}

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 65, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
//検索処理
//==============================

//検索文字解析
if($search_input != "")
{
	//検索文言整理
	$search_input_sql = $search_input;
	$search_input_sql = mb_convert_kana($search_input_sql,"rn");//r:全角英字→半角,n:全角数字→半角
	$search_input_sql = strtoupper($search_input_sql);//小文字→大文字
	$search_input_sql = pg_escape_string($search_input_sql);

	//複数ワード指定(スペースで分割する)
	$search_input_sql = mb_ereg_replace("　"," ",$search_input_sql);//全角スペース除去
	$search_input_sql = trim($search_input_sql);//前後スペース除去
	$search_input_sql = preg_replace("/\s+/", " ", $search_input_sql);//連続スペース除去
	$search_input_sql_list = mb_split(" ",$search_input_sql);//スペース区切り

	//print_r($search_input_sql_list);
}
else
{
	$search_input_sql_list = array();//空配列
}

//検索実行
$list_cnt = count($search_input_sql_list);
if($list_cnt > 0)
{
	//一致条件作成
	$mae_p = "%";
	$ato_p = "%";
	if($search_pattern == "zen")
	{
		$mae_p = "";
	}
	elseif($search_pattern == "kou")
	{
		$ato_p = "";
	}

	//SQL作成
	$sql = "";
	$sql .= " select ";
	$sql .= "   medis_byoumei.byoumei,";
	$sql .= "   medis_byoumei.icd10,";
	$sql .= "   medis_byoumei.byoumei_code,";
	$sql .= "   medis_byoumei.rece_code,";
	$sql .= "   medis_byoumei.byoumei_id,";
	$sql .= "   index_tbl.synonym_flg";
	$sql .= " from medis_byoumei";
	$sql .= " inner join ";
	$sql .= " (";
	$sql .= "   select ";
	$sql .= "   medis_byoumei_index_org.link_code,";
	$sql .= "   (";
	$sql .= "     case ";
	$sql .= "     WHEN ( min(medis_byoumei_index_org.synonym_class) = '2' or min(medis_byoumei_index_org.synonym_class) = '9') then false ";
	$sql .= "     else true ";
	$sql .= "     end ";
	$sql .= "   ) as synonym_flg";
	$sql .= "   from medis_byoumei_index_org";
	$sql .= "   where";
	$sql .= "   (";

	for($i=0;$i<$list_cnt;$i++)
	{
		if($i != 0)
		{
			$sql .= " and ";
		}

		$sql .= " (medis_byoumei_index_org.index_str like '".$mae_p.$search_input_sql_list[$i].$ato_p."' ";
		$tmp_kana = mb_convert_kana($search_input_sql_list[$i],"C");//C:全角ひらがな→全角カタカナ
		if ($tmp_kana !== $search_input_sql_list[$i]) {
			$sql .= " or medis_byoumei_index_org.index_str like '".$mae_p.$tmp_kana.$ato_p."' ";
		}
		$sql .= " ) ";
	}

	$sql .= "   )";
	$sql .= "   group by link_code";
	$sql .= " ) index_tbl";
	$sql .= " on medis_byoumei.byoumei_code = index_tbl.link_code where medis_byoumei.update_class<>'1'";
	$sql .= " order by byoumei_kana";
	$cond =  "";

	//SQL実行
	$sel = select_from_table($con,$sql,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sel_num = pg_numrows($sel);
}
else
{
	$sel = 0;
	$sel_num = 0;
}

//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title>CoMedix メドレポート｜病名選択</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">

function load_action()
{
	document.main_form.search_input.focus();
}

//病名が選択されたときの処理を行います。
function select_byoumei(byoumei,byoumei_code,icd10)
{
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_summary_byoumei_select)
	{
		window.opener.call_back_summary_byoumei_select(byoumei,byoumei_code,icd10);
	}

	//自画面を終了
	window.close();
}





function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}
function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}
function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action();">
<?


//==============================
//ヘッダー 出力
//==============================
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="31" height="31" class="spacing"><a href="kensaku_byoumei.php?session=<?=$session?>"><img src="img/icon/b41.gif" width="31" height="31" border="0" alt="検索ちゃん"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="kensaku_byoumei.php?session=<?=$session?>"><b>検索ちゃん</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#FF33FF"><a href="kensaku_byoumei.php?session=<?=$session?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病名検索</b></font></a></td>
<td width="">&nbsp;</td>
<td width="" align="center"></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#FF33FF"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" width="4" height="4" alt=""><br>

<?


//==============================
//入力フィールド 出力
//==============================
?>
<form name="main_form" action="kensaku_byoumei.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<table width="100%" border="0" cellspacing="2" cellpadding="0">
<tr><td>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="460" align="light">
		<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr>
		<td bgcolor="#FFCCFF" align="right" width="50">
			<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></nobr>
		</td>
		<td align="left" width="300">
			<nobr>
			<input type="text" name="search_input" value="<?=h($search_input)?>" style="width:350px;ime-mode: active">
			<input type="submit" value="検索">
			</nobr>
		</td>
		</tr>
		</table>
	</td>
	<td align="left" valign="bottom">
		<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">MEDIS標準病名マスターV<?=$MEDIS_VERSION?>対応</font></nobr>
	</td>
	<td width="250" align="right">
		<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr>
		<td bgcolor="#FFCCFF" align="right" width="100">
			<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索方法</font></nobr>
		</td>
		<td align="left" width="100">
			<select name="search_pattern" style="width:100">
				<option value="zen"   <? if($search_pattern == "zen"  ){print "selected";} ?> >前方一致
				<option value="bubun" <? if($search_pattern == "bubun"){print "selected";} ?> >部分一致
				<option value="kou"   <? if($search_pattern == "kou"  ){print "selected";} ?> >後方一致
			</select>
		</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>

</td></tr>
<tr><td>
	<img src="img/spacer.gif" width="1" height="5" alt=""><br>
</td></tr>
<tr><td align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<!-- 一覧HEADER START -->
	<tr>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">同義語</font></nobr></td>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></nobr></td>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ICD10</font></nobr></td>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交換用コード</font></nobr></td>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">傷病名コード</font></nobr></td>
	<td bgcolor="#FFCCFF" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理番号</font></nobr></td>
	</tr>
	<!-- 一覧HEADER END -->
	<!-- 一覧DATA START -->
	<?
	$is_disp_over = false;
	for($i = 0; $i < $sel_num; $i++)
	{
		if($i >= 200)
		{
			$is_disp_over = true;
			break;
		}

		$synonym_flg  = pg_fetch_result($sel,$i,"synonym_flg");
		if($synonym_flg == 't')
		{
			$synonym_flg = "○";
		}
		else
		{
			$synonym_flg = "×";
		}
		$byoumei      = pg_fetch_result($sel,$i,"byoumei");
		$icd10        = pg_fetch_result($sel,$i,"icd10");
		$byoumei_code = pg_fetch_result($sel,$i,"byoumei_code");
		$rece_code    = pg_fetch_result($sel,$i,"rece_code");
		$byoumei_id   = pg_fetch_result($sel,$i,"byoumei_id");

		$byoumei_js      = str_replace("'","''",$byoumei);
		$icd10_js        = str_replace("'","''",$icd10);
		$byoumei_code_js = str_replace("'","''",$byoumei_code);
		$line_script = "";
		$line_script .= " class=\"list_line".$i."\"";
		$line_script .= " onmouseover=\"highlightCells(this.className);\"";
		$line_script .= " onmouseout=\"dehighlightCells(this.className);\"";

//		$line_script .= " onmouseover=\"highlightCells(this.className);this.style.cursor = 'pointer';\"";
//		$line_script .= " onmouseout=\"dehighlightCells(this.className);this.style.cursor = '';\"";
//		$line_script .= " onclick=\"select_byoumei('".$byoumei_js."' , '".$byoumei_code_js."' , '".$icd10_js."')\"";
	?>
	<tr>
	<td <?=$line_script?> width="50"  align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($synonym_flg)?></font></nobr></td>
	<td <?=$line_script?>             align="left"  ><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($byoumei)?></font></nobr></td>
	<td <?=$line_script?> width="50"  align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($icd10)?></font></nobr></td>
	<td <?=$line_script?> width="100" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($byoumei_code)?></font></nobr></td>
	<td <?=$line_script?> width="100" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($rece_code)?></font></nobr></td>
	<td <?=$line_script?> width="100" align="center"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h($byoumei_id)?></font></nobr></td>
	</tr>
	<?
	}

	if($is_disp_over)
	{
		?>
		<tr><td colspan="6">
			<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">200件以上データがあります。</font></nobr>
		</td></tr>
		<?
	}

	?>
	<!-- 一覧DATA END -->
	</table>
</td></tr>
</table>
</form>
<?


//==============================
//終了HTML 出力
//==============================
?>
</body>
</html>
<?

pg_close($con);
?>
