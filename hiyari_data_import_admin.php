<?php

require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_db_class.php");

require_once("get_values.php");
require_once("show_class_name.ini");

require_once("Cmx.php");
require_once("class/Cmx/Model/SystemConfig.php");

// ==================================================
// 画面名
// ==================================================
$fname = $PHP_SELF;

// ==================================================
// セッションのチェック
// ==================================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 権限チェック
// ==================================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') {
	showLoginPage();
	exit;
}

// ==================================================
// DBコネクション取得
// ==================================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// インシデント権限取得
// ==================================================
$arr_inci = get_inci_auth_case($con, $fname);

// システムコンフィグクラスのインスタンス化
$conf = new Cmx_SystemConfig();

// ==================================================
// 更新処理
// ==================================================
if ($mode == "update") {
	// 利用フラグ
	$conf->set("fantol_xml_import_use_flg", $_POST["xml_import_use_flg"]);
	
	// 権限別利用フラグ
	foreach ($arr_inci as $arr) {
		$index_str = strtolower($arr["auth"]);
		
		$conf->set("fantol_xml_import_" . $index_str . "_flg", $_POST["xml_import_" . $index_str . "_flg"]);
	}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?= $INCIDENT_TITLE ?> | XMLインポート権限設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	function update() {
		document.mainform.mode.value="update";
		document.mainform.submit();
	}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="hiyari_data_import_admin.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		
		<!-- ヘッダー -->
		<?
		show_hiyari_header($session, $fname, true);
		?>
		<!-- ヘッダー -->
		
		<img src="img/spacer.gif" width="1" height="5" alt=""><br>
		
		<table width="800" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
				<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
				<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
			</tr>
			<tr>
				<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
				<td valign="top" align="left" bgcolor="#F5FFE5">
					<!-- XMLインポート機能の利用 START //-->
					<table border="0" cellspacing="1" cellpadding="0" class="list">
						<tr>
							<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">XMLインポート</font>
							</td>
							<?
							if ($conf->get("fantol_xml_import_use_flg") == "1") {
								$true_checked_str = "checked";
								$false_checked_str = "";
							} else {
								$true_checked_str = "";
								$false_checked_str = "checked";
							}
							?>
							<td width="160" bgcolor="#FFFFFF" align="center" colspan="3" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									<input type="radio" id="xml_import_use_flg_false" name="xml_import_use_flg" value="0" <?= $false_checked_str ?>>
									<label for="xml_import_use_flg_false">利用しない</label>
								</font>
							</td>
							<td width="160" bgcolor="#FFFFFF" align="center" colspan="3" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									<input type="radio" id="xml_import_use_flg_true" name="xml_import_use_flg" value="1" <?= $true_checked_str ?>>
									<label for="xml_import_use_flg_true">利用する</labe>
								</font>
							</td>
						</tr>
					</table>
					<!-- XMLインポート機能の利用 END //-->
					
					<img src="img/spacer.gif" width="1" height="5" alt=""><br>
					
					<!-- 参照 START -->
					<table border="0" cellspacing="1" cellpadding="0" class="list">
						<tr>
							<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
							</td>
							<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可</font>
							</td>
							<td width="80" bgcolor="#DFFFDC" align="center" nowrap>
								<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">不可</font>
							</td>
						</tr>
						<?
						foreach ($arr_inci as $arr) {
							$index_str = strtolower($arr["auth"]);
							
							if ($conf->get("fantol_xml_import_" . $index_str . "_flg") == "1") {
								$true_checked_str = "checked";
								$false_checked_str = "";
							} else {
								$true_checked_str = "";
								$false_checked_str = "checked";
							}
							?>
							<tr>
								<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?= $arr["auth_name"] ?></font>
								</td>
								<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									<input type="radio" name="xml_import_<?= $index_str ?>_flg" value="1" <?= $true_checked_str ?>>
									</font>
								</td>
								<td width="80" bgcolor="#FFFFFF" align="center" nowrap>
									<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
									<input type="radio" name="xml_import_<?= $index_str ?>_flg" value="0" <?= $false_checked_str ?>>
									</font>
								</td>
							</tr>
						<?
						}
						?>
					</table>
					<!-- 参照 END -->
					
					<table width="100%" border="0" cellspacing="1" cellpadding="0">
						<tr>
							<td align="right">
								<input type="button" value="登録" onclick="update();">
							</td>
						</tr>
					</table>
				</td>
				<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
			</tr>
			<tr>
				<td><img src="img/r_3.gif" width="10" height="10"></td>
				<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
				<td><img src="img/r_4.gif" width="10" height="10"></td>
			</tr>
		</table>
		
		<!-- フッター START -->
		<?
		show_hiyari_footer($session,$fname,true);
		?>
		<!-- フッター END -->
		
		</td>
	</tr>
</table>

</form>
</body>

<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

</html>

<?
/**
 * XMLインポート利用権限一覧取得
 * 
 * @param  onject $con            接続コネクション
 * @param  string $fname          画面名
 * @return array                  権限データ配列
 */
function get_inci_auth_case($con, $fname)
{
	$sql  = "select";
		$sql .=" a.*";
		$sql .=", b.auth_rank";
		$sql .=", case when a.auth = 'GU' then '一般ユーザー' else b.auth_name end";
	$sql .= " from";
		$sql .=" inci_auth_case a";
		$sql .=" left join";
		$sql .=" inci_auth_mst b";
		$sql .=" on";
		$sql .=" a.auth = b.auth";
	
	$cond = " order by";
		$cond .= " auth_rank is not null";
		$cond .= ", auth_rank asc";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}
?>
