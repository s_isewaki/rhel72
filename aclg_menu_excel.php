<?php

ob_start();

require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("aclg_tools_array.php");
require_once("sot_util.php");
require_once("get_menu_label.ini");

//require_once("jnl_application_common.ini");
//require("jnl_application_imprint_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,92,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

$date_ym = ($date_y*100)+$date_m;
$day_max = date("d", strtotime($date_y."-".($date_m<12?$date_m+1:1)."-1 -1 day"));

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
}
else {
	$encoding = mb_http_output();   // Firefox
}

if($regstat == "2")
{
	//管理画面用のファイル名を設定
	$fileName = "AccessLogCountAdmin_".$date_ym.".xls";
}
else
{
	//ユーザ画面用のファイル名を設定	
	$fileName = "AccessLogCountUser_".$date_ym.".xls";
}	

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding(
		$fileName,
		$encoding,
		mb_internal_encoding());

$fileName = mb_convert_encoding($fileName, $encoding, mb_internal_encoding());
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$download_data = setData($fname,$con,$date_ym,$day_max,$regstat,$tools);

echo mb_convert_encoding(
		nl2br($download_data),
		'sjis',
		mb_internal_encoding()
		);

ob_end_flush();

pg_close($con); 



/**
 * /エクセル出力用データセット
 *
 * @param mixed $fname This is a description
 * @param mixed $con This is a description
 * @param mixed $date_ym This is a description
 * @param mixed $day_max This is a description
 * @param mixed $regstat This is a description
 * @return mixed This is the return value description
 *
 */
function setData($fname,$con,$date_ym,$day_max,$regstat,$tools)
{
	if($regstat == "2")
	{
		//管理画面用のdetail_idを設定
		$detail_id = '2';
	}
	else
	{
		//ユーザ画面用のdetail_idを設定	
		$detail_id = '1';
	}	
	
	$max_count = count($tools);

	for($i=1; $i<=$max_count; $i++) 
	{

/*
		if($i > 0)
		{
			//ログイン以外のカウント
			$sql = "select substr(wdate,7,2) as setDate, setCount from
			(
			select substr(ac_date,0,9) as wdate, count(substr(ac_date,0,9)) as setCount 
			from aclg_data where cate_id='0006' and detail_id='$detail_id' and status1='".$tools[$i][1]."' 
					and substr(ac_date,0,7) ='".$date_ym."' 
					group by substr(ac_date,0,9)
					) subTable";
		}
		else
		{
			//ログインのカウント
			$sql = "select substr(wdate,7,2) as setDate, setCount from
						(
						select substr(ac_date,0,9) as wdate, count(substr(ac_date,0,9)) as setCount 
						from aclg_data where cate_id='0001' and detail_id='1'
						and substr(ac_date,0,7) ='".$date_ym."' 
						group by substr(ac_date,0,9)
						) subTable2";
		}
*/		

		$sql = "select substr(wdate,7,2) as setDate, setCount from
				(
				select substr(ac_date,0,9) as wdate, count(substr(ac_date,0,9)) as setCount 
				from aclg_data where cate_id='0006' and detail_id='$detail_id' and status1='".$tools[$i][1]."' 
				and substr(ac_date,0,7) ='".$date_ym."' 
				group by substr(ac_date,0,9)
				) subTable
				union 	
				select substr(wdate,7,2) as setDate, setCount from
				(
				select substr(ac_date,0,9) as wdate, count(substr(ac_date,0,9)) as setCount 
				from aclg_data where cate_id='0001' and detail_id='1' and '".$tools[$i][1]."' = '0001'
				and substr(ac_date,0,7) ='".$date_ym."' 
				group by substr(ac_date,0,9)
				) subTable2	";
		
		$cond = "";

		$sel = select_from_table($con, $sql,$cond,$fname);
		$data = array();
		while($row = pg_fetch_array($sel)) 
		{

			for($j = 1; $j <= $day_max; $j++) 
			{
				
				$chg_day = sprintf("%02d", $j);

				if($row["setdate"] == $chg_day)
				{
					//echo($row["setcount"]);
					$tools[$i]["coutData"][$j] = $row["setcount"];
				}
			}
		}
	}


	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	$data .= "<table border=1 >";
	$data .=  "<tr style=\"background-color:#fefcdf\">";
	$data .=  "<th style=\"text-align:left; background-color:#fefcdf; white-space:nowrap\">
	<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">基本機能</font></th>";
	for($i=1; $i<=$day_max; $i++) {
		$data .=  "<td>";
		$data .=  $i>0 ? $i : "";
		$data .=   "</td>";
	}
	$data .=  "</tr>";

	for($i=1; $i<=16; $i++) 
	{
		if($regstat == '2' )
		{
			if($i == 1 || $i == 6 || $i == 7 || $i == 16)
			{
				//管理画面のＣＳＶ出力の場合は「ログイン」、「タスク」、「伝言メモ」、「パスワード変更」は表示しない
				continue;
			}
		}
		$data .= "<tr>";
		$data .= "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">
		<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$tools[$i][0]."</font></td>";
		for($j = 1; $j <= $day_max; $j++) 
		{
			$data .= "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
		}
		$data .= "</tr>";
	}
	$data .= "</table>";

	$data .= "<br>";

	$data .= "<table border=1 >";
	$data .= "<tr style=\"background-color:#fefcdf\">";
	$data .= "<th style=\"text-align:left; background-color:#fefcdf; white-space:nowrap\">
	<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">業務機能</font></th>";
	for($i=1; $i<=$day_max; $i++) {
		$data .= "<td>";
		$data .= $i>0 ? $i : "";
		$data .= "</td>";
	}
	$data .= "</tr>";
	for($i=17; $i<=17; $i++) {
		$data .= "<tr>";
		$data .= "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">
		<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$tools[$i][0]."</font></td>";
		for($j = 1; $j <= $day_max; $j++) {
			$data .= "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
		}
		$data .= "</tr>";
	}
	$data .= "</table>";

	if($regstat != "2")
	{
		
		$data .= "<br>";
		
		$data .= "<table border=1 >";
		$data .= "<tr style=\"background-color:#fefcdf\">";
		$data .= "<th style=\"text-align:left; background-color:#fefcdf; white-space:nowrap\">
				<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">管理機能</font></th>";
		for($i=1; $i<=$day_max; $i++) {
			$data .= "<td>";
			$data .= $i>0 ? $i : "";
			$data .= "</td>";
		}
		$data .= "</tr>";
		for($i=18; $i<=21; $i++) {
			$data .= "<tr>";
			$data .= "<td style=\"text-align:left; background-color:#f6f9ff; white-space:nowrap\">
					<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$tools[$i][0]."</font></td>";
			for($j = 1; $j <= $day_max; $j++) {
				$data .= "<td width=\"3%\">".@$tools[$i]["coutData"][$j]."</td>";
			}
			$data .= "</tr>";
		}
		$data .= "</table>";
	}

	return $data;	


	
}

?>
