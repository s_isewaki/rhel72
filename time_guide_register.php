<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("referer_common.ini");
require("show_date_navigation_month.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 年月の設定
if ($hid_date != "") {
	$date = $hid_date;
} else if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$last_month = get_last_month($date);
$next_month = get_next_month($date);
$year = date("Y", $date);
$month = date("m", $date);
$days = days_in_month($year, $month);

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "event", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_1 = pg_fetch_result($sel, 0, "menu2_1");

// 施設一覧を取得
$sql = "select fcl_id, name from timegdfcl";
$cond = "order by fcl_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 登録行数のデフォルトは5件
if ($rows == "") {
	$rows = 5;
}
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 院内行事登録/法人内行事登録
$event_reg_title =  $_label_by_profile["EVENT_REG"][$profile_type];
?>
<title>CoMedix <? echo($event_reg_title); ?> | 行事登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.mainform.hid_date.value = dt;
	document.mainform.action = 'time_guide_register.php';
	document.mainform.submit();
}

function setTimeDisabled(name) {
	if (!name) {
		for (var i = 0, j = document.mainform.elements.length; i < j; i++) {
			if (document.mainform.elements[i].type != 'checkbox') {
				continue;
			}
			var name = document.mainform.elements[i].name;
			var row_no = name.replace('time_flg', '');
			var disabled = document.mainform.elements[name].checked;
			document.mainform.elements['start_hrs' + row_no].disabled = disabled;
			document.mainform.elements['start_min' + row_no].disabled = disabled;
			document.mainform.elements['dur_hrs' + row_no].disabled = disabled;
			document.mainform.elements['dur_min' + row_no].disabled = disabled;
		}
	} else {
		var row_no = name.replace('time_flg', '');
		var disabled = document.mainform.elements[name].checked;
		document.mainform.elements['start_hrs' + row_no].disabled = disabled;
		document.mainform.elements['start_min' + row_no].disabled = disabled;
		document.mainform.elements['dur_hrs' + row_no].disabled = disabled;
		document.mainform.elements['dur_min' + row_no].disabled = disabled;
	}
}

function addRow() {
	for (var i = 0, j = document.mainform.elements.length; i < j; i++) {
		if (document.mainform.elements[i].type != 'checkbox') {
			continue;
		}
		if (!document.mainform.elements[i].checked) {
			document.mainform.elements[i].value = 't';
			document.mainform.elements[i].checked = true;
		}
	}

	document.mainform.rows.value = Number(document.mainform.rows.value) + 1;
	document.mainform.action = 'time_guide_register.php';
	document.mainform.submit();
}

function registEvent() {
	document.mainform.action = 'time_guide_insert.php';
	document.mainform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setTimeDisabled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="time_guide_list.php?session=<? echo($session); ?>"><img src="img/icon/b22.gif" width="32" height="32" border="0" alt="<? echo($event_reg_title); ?>"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="time_guide_list.php?session=<? echo($session); ?>"><b><? echo($event_reg_title); ?></b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_event.php?session=<? echo($session); ?>"><b><? echo($menu2_1); ?></b></a> &gt; <a href="time_guide_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_info_event.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">行事一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="time_guide_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>行事登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="time_guide_class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="time_guide_fcl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設登録</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>1. 施設・年月を選択してください</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設：<select name="fcl_id" tabindex="1">
<option value="-">全体
<?
while ($row = pg_fetch_array($sel)) {
	$tmp_fcl_id = $row["fcl_id"];
	$tmp_name = $row["name"];
	echo("<option value=\"$tmp_fcl_id\"");
	if ($tmp_fcl_id == $fcl_id) {echo(" selected");}
	echo(">$tmp_name\n");
}
?>
</select>
<span style="margin-left:20px;"><a href="javascript:void(0);" onclick="changeDate('<? echo($last_month); ?>');return false;" tabindex="2">&lt;前月</a>&nbsp;<select name="date" onchange="changeDate(this.value);" tabindex="3"><? show_date_options_m($date); ?></select>&nbsp;<a href="javascript:void(0);" onclick="changeDate('<? echo($next_month); ?>');return false;" tabindex="4">翌月&gt;</a></span>
</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>2. 行事内容を入力してください</b><font color="gray">（施設詳細と連絡先は特になければ入力不要です）</font></font></td>
</tr>
</table>
<?
for ($i = 0; $i < $rows; $i++) {
	$tabindex_offset = ($i + 1) * 10;

	echo("<table width=\"800\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\" style=\"margin-bottom:5px;\">\n");
	echo("<tr height=\"22\">\n");
	echo("<td width=\"50\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">日付</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo(intval($year) . "年" . intval($month) . "月");
	echo("<select name=\"day[]\" tabindex=\"" . strval($tabindex_offset + 1) . "\">\n");
	show_select_days_ex($day[$i], $days);
	echo("</select>日\n");
	echo("</font></td>\n");
	echo("<td width=\"55\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">行事名</font></td>\n");
	echo("<td><input type=\"text\" name=\"event_name[]\" value=\"{$event_name[$i]}\" size=\"30\" maxlength=\"50\" style=\"ime-mode:active;\" tabindex=\"" . strval($tabindex_offset + 7) . "\"></td>\n");
	echo("<td width=\"65\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">施設詳細</font></td>\n");
	echo("<td><input type=\"text\" name=\"fcl_detail[]\" value=\"{$fcl_detail[$i]}\" size=\"21\" maxlength=\"50\" style=\"ime-mode:active;\" tabindex=\"" . strval($tabindex_offset + 9) . "\"></td>\n");
	echo("</tr>\n");
	echo("<tr height=\"22\">\n");
	echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">時刻</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	echo("<select name=\"start_hrs$i\" tabindex=\"" . strval($tabindex_offset + 2) . "\">\n");
	show_hour_options_0_23(${"start_hrs$i"}, true);
	echo("</select>：<select name=\"start_min$i\" tabindex=\"" . strval($tabindex_offset + 3) . "\">\n");
    show_min_options_5(${"start_min$i"});
	echo("</select>〜<select name=\"dur_hrs$i\" tabindex=\"" . strval($tabindex_offset + 4) . "\">\n");
	show_hour_options_0_23(${"dur_hrs$i"}, true);
	echo("</select>：<select name=\"dur_min$i\" tabindex=\"" . strval($tabindex_offset + 5) . "\">\n");
    show_min_options_5(${"dur_min$i"});
	echo("</select><br>\n");
	echo("<input type=\"checkbox\" name=\"time_flg$i\" value=\"f\"");
	if (${"time_flg$i"} == "f" || ${"time_flg$i"} == "") {
		echo(" checked");
	}
	echo(" onclick=\"setTimeDisabled(this.name);\" tabindex=\"" . strval($tabindex_offset + 6) . "\">指定しない\n");
	echo("</font></td>\n");
	echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">詳細</font></td>\n");
	echo("<td><textarea name=\"event_content[]\" cols=\"30\" rows=\"4\" style=\"ime-mode:active;\" tabindex=\"" . strval($tabindex_offset + 8) . "\">{$event_content[$i]}</textarea></td>\n");
	echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">連絡先</font></td>\n");
	echo("<td><textarea name=\"contact[]\" cols=\"14\" rows=\"3\" style=\"ime-mode:active;\" tabindex=\"" . strval($tabindex_offset + 10) . "\">{$contact[$i]}</textarea></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="button" value="入力欄を追加" onclick="addRow();" tabindex="<? echo(($rows + 1) * 10 + 1); ?>">
<input type="button" value="登録" onclick="registEvent();" tabindex="<? echo(($rows + 1) * 10 + 2); ?>">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="hid_date" value="">
<input type="hidden" name="rows" value="<? echo($rows); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_select_days_ex($fix, $days) {
	echo("<option value=\"-\">\n");

	for ($i = 1; $i <= $days; $i++) {
		$val = sprintf("%02d", $i);
		echo("<option value=\"$val\"");
		if ($i == $fix) {
			echo(" selected");
		}
		echo(">$i</option>\n");
	}
}

function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}
?>
