<?
$fname = $_SERVER["PHP_SELF"];
$session = @$_REQUEST["session"];

$chart_chart_seq = (int)@$_REQUEST["chart_chart_seq"];
$cliplist_chart_seq = (int)@$_REQUEST["cliplist_chart_seq"];

ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);


$error = "";
$chart_data = "";
$cliplist_data = "";

for (;;){
	if($session == "0") { $error = "01 / ". $session; break; }
	if($summary_check == "0") { $error = "02"; break; }
	if($con == "0") { $error = "03"; break; }

	if (!$chart_chart_seq && !$cliplist_chart_seq) { $error = "11"; break; }

	// 既存データを検索
	if ($chart_chart_seq){
		$sel = select_from_table($con, "select chart_data from inci_analysis_rca_chart where chart_seq = " .$chart_chart_seq, "", $fname);
		if (!$sel) { pg_close($con); $error = "21"; break; }
		$chart_data = @pg_fetch_result($sel, 0, 0);
	}

	if ($cliplist_chart_seq){
		$sel = select_from_table($con, "select cliplist_data from inci_analysis_rca_chart where chart_seq = " .$cliplist_chart_seq, "", $fname);
		if (!$sel) { pg_close($con); $error = "22"; break; }
		$cliplist_data = @pg_fetch_result($sel, 0, 0);
	}
	break;
}

function escape_crlf($str){
	return str_replace("\r", "", str_replace("\n", "\\n", trim($str)));
}

if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{
<? if ($chart_chart_seq){?> "chart_data" : {<?=escape_crlf($chart_data)?>}, <? } ?>
<? if ($cliplist_chart_seq){?> "cliplist_data" : {<?=escape_crlf($cliplist_data)?>}, <? } ?>
"chart_chart_seq":"<?=$chart_chart_seq?>","cliplist_chart_seq":"<?=$cliplist_chart_seq?>","error":"<?=$error?>"}