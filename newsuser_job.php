<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>お知らせ | 職種</title>
<?
require("about_authority.php");
require("about_session.php");
require("show_class_name.ini");
require("show_newsuser_job.ini");
require("news_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 46, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// お知らせ管理権限の取得
$news_admin_auth = check_authority($session, 24, $fname);

// ページ番号の設定
if ($job_page == "") {$job_page = 1;}

// データベースに接続
$con = connect2db($fname);

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// ログインユーザの所属する職種を取得
$sql = "select emp_job from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$job_id = pg_fetch_result($sel, 0, "emp_job");

// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);

// カテゴリを取得
$category_list = get_category_list_from_db($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a></font></td>
<? if ($news_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>&news=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#5279a5"><a href="newsuser_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<? if ($everyone_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><a href="newsuser_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$category_list["1"]?></font></a></td>
<td style="padding-right:30px;"><a href="newsuser_class.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$category_list["2"]?></font></a></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><?=$category_list["3"]?></b></font></td>
<td style="padding-right:30px;"><a href="newsuser_circular.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$category_list["10000"]?></font></a></td>
<? if ($everyone_flg == "t") { ?>
<td style="padding-right:30px;"><a href="newsuser_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自己登録分</font></a></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td width="110"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td width="90"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認</font></td>
<td width="210"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間</font></td>
<td width="95"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧</font></td>
</tr>
<? show_news($con, $job_id, $job_page, $session, $fname); ?>
</table>
<? show_news_page_list($con, $job_id, $job_page, $session, $fname); ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
