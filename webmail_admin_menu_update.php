<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="webmail_admin_menu.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="use_email" value="<? echo($use_email); ?>">
<input type="hidden" name="use_cyrus" value="<? echo($use_cyrus); ?>">
<input type="hidden" name="webmail_from_order1" value="<? echo($webmail_from_order1); ?>">
<input type="hidden" name="webmail_from_order2" value="<? echo($webmail_from_order2); ?>">
<input type="hidden" name="webmail_from_order3" value="<? echo($webmail_from_order3); ?>">
<input type="hidden" name="smtp_server_address" value="<? echo($smtp_server_address); ?>">
<input type="hidden" name="smtp_port" value="<? echo($smtp_port); ?>">
<input type="hidden" name="back" value="t">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 66, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($webmail_from_order2 == $webmail_from_order1) {
	echo("<script type=\"text/javascript\">alert('差出人表示順2を正しく選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($webmail_from_order3 == $webmail_from_order1 || ($webmail_from_order3 != "" && $webmail_from_order3 == $webmail_from_order2)) {
	echo("<script type=\"text/javascript\">alert('差出人表示順3を正しく選択してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($smtp_server_address == "") {
	echo("<script type=\"text/javascript\">alert('SMTPサーバ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($smtp_port == "") {
	echo("<script type=\"text/javascript\">alert('ポート番号が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (preg_match("/^\d{1,5}$/", $smtp_port) == 0) {
	echo("<script type=\"text/javascript\">alert('ポート番号を数字で入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($smtp_port > 65535) {
	echo("<script type=\"text/javascript\">alert('ポート番号は0〜65535の範囲で入力してください。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// 入力値の編集
if ($webmail_from_order2 == "") {$webmail_from_order2 = null;}
if ($webmail_from_order3 == "") {$webmail_from_order3 = null;}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を更新
$sql = "update config set";
$set = array("use_email", "webmail_from_order1", "webmail_from_order2", "webmail_from_order3", "smtp_server_address", "smtp_port", "use_cyrus");
$setvalue = array($use_email, $webmail_from_order1, $webmail_from_order2, $webmail_from_order3, $smtp_server_address, $smtp_port, $use_cyrus);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'webmail_admin_menu.php?session=$session';</script>");
?>
</body>
