	<?
//	ini_set( 'display_errors', 1 );
	//==================================
	//ラダー評価マスタPHP
	//==================================
	require_once("about_session.php");
	require_once("about_authority.php");
	require_once("cl_workflow_common.ini");
	require_once("cl_title_name.ini");
	require_once("cl_common_log_class.inc");
	require_once("cl_smarty_setting.ini");
	require_once("Cmx.php");
	require_once("MDB2.php");

	$log = new cl_common_log_class(basename(__FILE__));

	$log->info(basename(__FILE__)." START");

	$smarty = cl_get_smarty_obj();
	$smarty->template_dir = dirname(__FILE__) . "/cl/view";

	//====================================
	// レベルフラグ切り替え
	//====================================
	if($hdnlevel == ''){
		$target_level = 1;
	}else{
		$target_level = $_POST["hdnlevel"];
	}

	//====================================
	// マスタ切り替え
	//====================================
	if($hdnmst == ''){
		$target_mst = evaluation;
	}else{
		$target_mst = $_POST["hdnmst"];
	}

	//====================================
	//画面名
	//====================================
	$fname = $PHP_SELF;

	//====================================
	//セッションのチェック
	//====================================
	$session = qualify_session($session,$fname);
	if($session == "0"){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	//====================================
	//権限チェック
	//====================================
	$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
	if($wkfw=="0"){
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showLoginPage(window);</script>");
		exit;
	}
	// ワークフロー権限の取得
	$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

	$cl_title = cl_title_name();

	//====================================
	// アプリケーションメニューHTML取得
	//====================================
	$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

	//====================================
	// Javascriptコード生成（動的オプション項目変更用）
	//====================================
	//$js_str = get_js_str($arr_wkfwcatemst,$workflow);

	//====================================
	// データベース接続オブジェクト取得
	//====================================
	$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
	$mdb2 = MDB2::connect(CMX_DB_DSN);
	if (PEAR::isError($mdb2)) {
		$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
	}
	$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

	//====================================
	// ログインユーザ情報取得
	//====================================
	require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
	$empmst_model = new empmst_model($mdb2);
	$arr_login = $empmst_model->select_login_emp($session);
	$login_emp_id = $arr_login["emp_id"];

	//====================================
	// SQL用PHP設定
	//====================================
		// 共通
		require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_ludder_eval_model.php");
		$cl_mst_ludder_evaluation = new cl_mst_ludder_eval_model($mdb2,$login_emp_id);

		// マスタ種類コンボ情報取得
		// 2012/06/08 Yamagawa upd(s)
		//$get_mst_tpl = $cl_mst_ludder_evaluation->select_get_evaluation();
		$get_mst_tpl = $cl_mst_ludder_evaluation->select_get_evaluation($target_level);
		// 2012/06/08 Yamagawa upd(e)
		$cmb_mst=get_mst_tpl($get_mst_tpl,$_POST["cmb_mst"],$mode);

		// マスタ切り替え時には未検索状態とする
		if ($mode != "MstLoad"){
			// カテゴリコンボ情報取得
			$get_cate_tpl = $cl_mst_ludder_evaluation->select_get_category($target_level,$_POST["cmb_mst"]);
			$cmb_cate=get_cate_tpl($get_cate_tpl,$_POST["cmb_cate"],$mode);
		}


	// マスタ種類
	if(($target_mst == 'evaluation') || ($target_mst == ''))
	{
		require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_evaluation_model.php");
		$cl_mst_ludder_eval = new cl_mst_evaluation_model($mdb2,$login_emp_id);

		//====================================
		// テンプレートコンボ情報取得
		//====================================
		$get_tpl_arr = $cl_mst_ludder_evaluation->select_get_tpl($target_level);
		$data_tpl    =get_tlp_options($get_tpl_arr,$_POST["$data[cell].evaluation_wkfw_title"]);

	// カテゴリ
	}else if ($target_mst == 'evaluation_category'){

		require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_evaluation_category_model.php");
		$cl_mst_ludder_eval = new cl_mst_evaluation_category_model($mdb2,$login_emp_id);


	// 評価基準
	}else if ($target_mst == 'evaluation_group'){

		require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_evaluation_group_model.php");
		$cl_mst_ludder_eval = new cl_mst_evaluation_group_model($mdb2,$login_emp_id);
	}

	//====================================
	// IdSequence取得(マスタ種類)
	//====================================
	if ($target_mst == 'evaluation') {
		require_once("cl/model/sequence/cl_evaluation_id_seq_model.php");
		$id_model = new cl_evaluation_id_seq_model($mdb2,$login_emp_id);
	} else if ($target_mst == 'evaluation_category') {
		require_once("cl/model/sequence/cl_evaluation_category_id_seq_model.php");
		$id_model = new cl_evaluation_category_id_seq_model($mdb2,$login_emp_id);
	} else if ($target_mst == 'evaluation_group') {
		require_once("cl/model/sequence/cl_evaluation_group_id_seq_model.php");
		$id_model = new cl_evaluation_group_id_seq_model($mdb2,$login_emp_id);
	}

	//========================================================================
	//Submit処理（Submit処理で分岐）
	//========================================================================

		// 更新ボタン押下時
		if ($mode == 'UPDATA'){

			if ($target_mst == 'evaluation') {

				for($i=0; $i<$dataCount; $i++) {
					$txt = $i."_TXT";
					$rank = $i."_RANK";
					$id = $i."_ID";
					$chk = $i."_CHK";
					$cmb = $i."_CMB";

					// 存在チェック
					$result = $cl_mst_ludder_eval->getRecord($$id);
					if (count($result) == 0){
						$setid = $id_model->getSeqId();
						$param=array();
						$param["evaluation_id"]=(int)substr($setid,-8);
						$param["level"]=$target_level;
						$param["evaluation_name"]=$$txt;
						$param["disp_order"]=$$rank;
						$param["evaluation_short_wkfw_name"]=$$cmb;
						$param["passing_mark"]=0;
						$result = $cl_mst_ludder_eval->insert($param);

					}else{
						$param=array();
						$param["evaluation_id"]=$$id;
						$param["level"]=$target_level;
						$param["evaluation_name"]=$$txt;
						$param["disp_order"]=$$rank;
						$param["evaluation_short_wkfw_name"]=$$cmb;
						$param["passing_mark"]=0;
						$result = $cl_mst_ludder_eval->update($param);
					}
				}

			} else if ($target_mst == 'evaluation_category') {

				for($i=0; $i<$dataCount; $i++) {

					$txt = $i."_TXT";
					$rank = $i."_RANK";
					$id = $i."_ID";
					$chk = $i."_CHK";

					// 存在チェック
					$result = $cl_mst_ludder_eval->getRecord($$id);

					if (count($result) == 0){
						$setid = $id_model->getSeqId();
						$param=array();
						$param["category_id"]=(int)substr($setid,-8);
						$param["level"]=$target_level;
						$param["evaluation_id"]=$_POST["cmb_mst"];
						$param["category_name"]=$$txt;
						$param["disp_order"]=$$rank;
						//2012/05/07 K.Fujii upd(s)
						//if ($$chk == '1')
						//{
						if ($$chk == '0'){
						//2012/05/07 K.Fujii upd(e)
							$param["disp_flg"]=$$chk;
						} else {
							//2012/05/07 K.Fujii upd(s)
							//$param["disp_flg"]='0';
							$param["disp_flg"]=1;
							//2012/05/07 K.Fujii upd(e)
						}
						$result = $cl_mst_ludder_eval->insert($param);
					}else{
						$param=array();
						$param["category_id"]=$$id;
						$param["level"]=$target_level;
						$param["evaluation_id"]=$_POST["cmb_mst"];
						$param["category_name"]=$$txt;
						$param["disp_order"]=$$rank;
						//2012/05/07 K.Fujii upd(s)
						//if ($$chk == '1')
						//{
						if ($$chk == '0'){
						//2012/05/07 K.Fujii upd(e)
							$param["disp_flg"]=$$chk;
						} else {
							//2012/05/07 K.Fujii upd(s)
							//$param["disp_flg"]='0';
							$param["disp_flg"]=1;
							//2012/05/07 K.Fujii upd(e)
						}
						$result = $cl_mst_ludder_eval->update($param);
					}
				}

			} else if ($target_mst == 'evaluation_group') {

				for($i=0; $i<$dataCount; $i++) {

					$seq = $i."_SEQ";
					$txt = $i."_TXT";
					$rank = $i."_RANK";
					$id = $i."_ID";
					$chk = $i."_CHK";

					// 存在チェック
					$result = $cl_mst_ludder_eval->getRecord($$id);


					if (count($result) == 0){
						$setid = $id_model->getSeqId();
						$param["group_id"]=(int)substr($setid,-8);
						$param["level"]=$target_level;
						$param["evaluation_id"]=$_POST["cmb_mst"];
						$param["category_id"]=$_POST["cmb_cate"];
						$param["group_name"]=$$txt;
						$param["group_no"]=$$seq;
						$param["disp_order"]=$$rank;
						//2012/05/07 K.Fujii upd(s)
						//if ($$chk == '1')
						//{
						if ($$chk == '0'){
						//2012/05/07 K.Fujii upd(e)
							$param["disp_flg"]=$$chk;
						} else {
							//2012/05/07 K.Fujii upd(s)
							//$param["disp_flg"]='0';
							$param["disp_flg"]=1;
							//2012/05/07 K.Fujii upd(e)
						}

						$result = $cl_mst_ludder_eval->insert($param);
					}else{
						$param["group_id"]=$$id;
						$param["level"]=$target_level;
						$param["evaluation_id"]=$_POST["cmb_mst"];
						$param["category_id"]=$_POST["cmb_cate"];
						$param["group_name"]=$$txt;
						$param["group_no"]=$$seq;
						$param["disp_order"]=$$rank;
						//2012/05/07 K.Fujii upd(s)
						//if ($$chk == '1')
						//{
						if ($$chk == '0'){
							$param["disp_flg"]=$$chk;
						} else {
							//2012/05/07 K.Fujii upd(s)
							//$param["disp_flg"]='0';
							$param["disp_flg"]=1;
							//2012/05/07 K.Fujii upd(e)
						}

						$result = $cl_mst_ludder_eval->update($param);
					}
				}
			}
		}

		// 画面単位で検索条件を分岐（カテゴリ・評価基準に関してはコンボ処理を含む）
		if (($target_mst == 'evaluation') || ($target_mst == '')){

			// マスタ種類情報取得
			$arr_stmst = $cl_mst_ludder_evaluation->select_mstKind($target_level);

			// テンプレートコンボ生成
			for ($i = 0; $i < count($arr_stmst); $i++){
				$arr_stmst[$i]['evaluation_wkfw_title'] = get_tlp_options($get_tpl_arr,$arr_stmst[$i]['evaluation_short_wkfw_name']);
				$arr_stmst[$i]['exe'] = '1';
			}

		} else if ($target_mst == 'evaluation_category'){

			// コンボ選択時＆更新ボタン押下時
		//	if (($mode == 'CatSel') || ($mode == 'UPDATA'))
		//	{
				// カテゴリ情報取得
				$arr_stmst = $cl_mst_ludder_evaluation->select_mst_cate($target_level,$_POST["cmb_mst"]);

				// 存在チェック用フラグ
				for ($i = 0; $i < count($arr_stmst); $i++){
					$arr_stmst[$i]['exe'] = '1';
				}
		//	}

		} else if ($target_mst == 'evaluation_group') {

			// カテゴリコンボ選択時
		//	if($mode == 'GrpSel'){

				// 評価基準情報取得
			//	$arr_stmst = $cl_mst_ludder_evaluation->select_mst_grp($target_level,$_POST["cmb_mst"],$_POST["cmb_cate"]);

				// 更新ボタン押下時
		//	} else if ($mode == 'UPDATA') {

				// 評価基準情報取得
				$arr_stmst = $cl_mst_ludder_evaluation->select_mst_grp($target_level,$_POST["cmb_mst"],$_POST["cmb_cate"]);
		//	}

			// 存在チェック用フラグ
			for ($i = 0; $i < count($arr_stmst); $i++){
				$arr_stmst[$i]['exe'] = '1';
			}
		}

		// 表示順押下時
		if($mode == 'rank_change')
		{
			$date = array();

			for($i=0; $i<$dataCount; $i++) {
				$seq = $i."_SEQ";
				$txt = $i."_TXT";
				$rank = $i."_RANK";
				$id = $i."_ID";
				$cmd = $i."_CMB";
				$chk = $i."_CHK";
				$exe = $i."_EXE";

				if (($target_mst == 'evaluation') || ($target_mst == '')){

					$arr_stmst[$$rank] = array('evaluation_name' => $$txt, 'disp_order' => $$rank, 'evaluation_id' => $$id,'evaluation_wkfw_title' => get_tlp_options($get_tpl_arr,$_POST["$cmd"]));

				} else if ($target_mst == 'evaluation_category'){

					if ($$chk == '1')
					{
						$arr_stmst[$$rank] = array('category_name' => $$txt, 'disp_order' => $$rank, 'category_id' => $$id, 'disp_flg' => $$chk);
					} else {
						$arr_stmst[$$rank] = array('category_name' => $$txt, 'disp_order' => $$rank, 'category_id' => $$id, 'disp_flg' => '0');
					}

				} else if ($target_mst == 'evaluation_group') {

					if ($$chk == '1')
					{
						$arr_stmst[$$rank] = array('group_name' => $$txt, 'disp_order' => $$rank, 'group_id' => $$id, 'disp_flg' => $$chk, 'group_no' => $$seq, 'exe' => $$exe);
					} else {
						$arr_stmst[$$rank] = array('group_name' => $$txt, 'disp_order' => $$rank, 'group_id' => $$id, 'disp_flg' => '0', 'group_no' => $$seq, 'exe' => $$exe);
					}
				}
			}

		// 行追加ボタン押下時
		}else if ($mode == 'ADD_LINE'){
			$j=0;
			$arr_stmst = array();

			for($i=0; $i<$dataCount; $i++) {
				$seq = $i."_SEQ";
				$txt = $i."_TXT";
				$rank = $i."_RANK";
				$id = $i."_ID";
				$cmd = $i."_CMB";
				$chk = $i."_CHK";
				$exe = $i."_EXE";

					if (($target_mst == 'evaluation') || ($target_mst == '')){

						$arr_stmst[$j] = array('evaluation_name' => $$txt, 'disp_order' => $$rank, 'evaluation_id' => $$id,'evaluation_wkfw_title' => get_tlp_options($get_tpl_arr,$_POST["$cmd"]));

					} else if ($target_mst == 'evaluation_category'){

						$arr_stmst[$j] = array('category_name' => $$txt, 'disp_order' => $$rank, 'category_id' => $$id, 'disp_flg'=>$$chk);

					} else if ($target_mst == 'evaluation_group') {

						$arr_stmst[$j] = array('group_name' => $$txt, 'disp_order' => $$rank, 'group_id' => $$id, 'disp_flg'=>$$chk, 'group_no'=>$$seq, 'exe' => $$exe);

					}
				$j++;

				// 追加行
				if($i == $hdnrec)
				{
					$setid = $id_model->getSeqId();

					if (($target_mst == 'evaluation') || ($target_mst == '')){
						$arr_stmst[$j] = array('evaluation_name' => "", 'disp_order' => "", 'evaluation_id' => (int)substr($setid,-8),'evaluation_wkfw_title' => get_tlp_options($get_tpl_arr,''));

					} else if ($target_mst == 'evaluation_category'){

						$arr_stmst[$j] = array('category_name' => "", 'disp_order' => "", 'evaluation_id' => (int)substr($setid,-8), 'disp_flg'=> "");

					} else if ($target_mst == 'evaluation_group') {

						$arr_stmst[$j] = array('group_name' => "", 'disp_order' => "", 'group_id' => (int)substr($setid,-8), 'disp_flg'=>"", 'group_no'=>"", 'exe' => '');

					}

					$j++;
				}
			}


		// 行削除ボタン押下時
		}else if ($mode == 'DEL_LINE'){
			$j=0;
			$arr_stmst = array();
			for($i=0; $i<$dataCount; $i++) {

				$seq = $i."_SEQ";
				$txt = $i."_TXT";
				$rank = $i."_RANK";
				$id = $i."_ID";
				$cmd = $i."_CMB";
				$chk = $i."_CHK";
				$exe = $i."_EXE";

				if($i == $hdnrec)
				{
					// 論理削除
					$result = $cl_mst_ludder_eval->logical_delete($$id);
					continue;
				}
					if (($target_mst == 'evaluation') || ($target_mst == '')){
						$arr_stmst[$j] = array('evaluation_name' => $$txt, 'disp_order' => $$rank, 'evaluation_id' => $$id,'evaluation_wkfw_title' => get_tlp_options($get_tpl_arr,$_POST["$cmd"]));

					} else if ($target_mst == 'evaluation_category'){

						$arr_stmst[$j] = array('category_name' => $$txt, 'disp_order' => $$rank, 'evaluation_id' => $$id,'disp_flg'=>$$chk);

					} else if ($target_mst == 'evaluation_group') {

						$arr_stmst[$j] = array('group_name' => $$txt, 'disp_order' => $$rank, 'group_id' => $$id, 'disp_flg'=>$$chk, 'group_no'=>$$seq, 'exe' => $$exe);

					}
				$j++;
			}
		}

		// 空ﾚｺｰﾄﾞ表示(検索結果が０件or初期表示)
		if (count($arr_stmst) == 0){
				$arr_stmst = array();
				$setid = $id_model->getSeqId();
				if (($target_mst == 'evaluation') || ($target_mst == '')){

					$arr_stmst[0] = array('evaluation_name' => "", 'disp_order' => "", 'evaluation_id' => (int)substr($setid,-8), 'evaluation_wkfw_title' => get_tlp_options($get_tpl_arr,''));

				} else if ($target_mst == 'evaluation_category'){

					$arr_stmst[0] = array('category_name' => "", 'disp_order' => "", 'evaluation_id' => $$id,'disp_flg'=> "");

				} else if ($target_mst == 'evaluation_group') {

					$arr_stmst[0] = array('group_name' => "", 'disp_order' => "", 'group_id' => "", 'disp_flg'=> "", 'group_no'=> "", 'exe' => '');

				}
		}


	// DB切断
	$mdb2->disconnect();

	//========================================================================
	// テンプレート定義
	//========================================================================
	//====================================
	// テンプレートマッピング
	//====================================
	$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

		$smarty->assign( 'hdnlevel'				, $target_level					);
		$smarty->assign( 'hdnmst'				, $hdnmst						);

		$smarty->assign( 'hdnmstchg'			, $_POST["cmb_mst"]				);
		$smarty->assign( 'hdncatchg'			, $_POST["cmb_cate"]			);

		$smarty->assign( 'cl_title'				, $cl_title						);
		$smarty->assign( 'session'				, $session						);
		$smarty->assign( 'regist_flg'			, $regist_flg					);
		$smarty->assign( 'wkfwctn_mnitm_str'	, $wkfwctn_mnitm_str			);

		$smarty->assign( 'data'					, $arr_stmst					);
		$smarty->assign( 'data_tpl'				, $data_tpl						);

		$smarty->assign( 'cmb_cate'				, $cmb_cate						);
		$smarty->assign( 'cmb_mst'				, $cmb_mst						);
		$smarty->assign( 'cmb_ctg'				, $cmb_ctg						);

		$smarty->assign( 'get_tpl_arr'			, $get_tpl_arr					);
		$smarty->assign( 'get_tpl'				, $get_tpl						);

	$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

	//====================================
	// テンプレート出力
	//====================================
	$log->debug("テンプレート出力開始",__FILE__,__LINE__);

		// マスタ種類
		if(($target_mst == 'evaluation') || ($target_mst == ''))
		{
			$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

		// カテゴリ
		}else if ($target_mst == 'evaluation_category'){

			$smarty->display(basename($_SERVER['PHP_SELF'],'.php')."_category.tpl");

		// 評価基準
		}else if ($target_mst == 'evaluation_group'){

			$smarty->display(basename($_SERVER['PHP_SELF'],'.php')."_group.tpl");

		}

	$log->debug("テンプレート出力終了",__FILE__,__LINE__);

	//====================================
	// アプリケーションメニューHTML取得
	//====================================
	function get_wkfw_menuitem($session,$fname)
	{
		ob_start();
		show_wkfw_menuitem($session,$fname,"");
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}


	//========================================================================
	// コンボ生成
	//========================================================================
	//====================================
	// マスタ種類HTML取得
	//====================================
	function get_mst_tpl($arr,$mst,$mode)
	{
		ob_start();
		show_get_mst_tpl($arr,$mst,$mode);
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}

	//====================================
	// マスタ種類を出力
	//====================================
	function show_get_mst_tpl($arr_mst,$mst,$mode) {
		echo("<option value=\"\">");

		foreach ($arr_mst as $index => $arr) {

			$evaluation_name = $arr["evaluation_name"];
			$evaluation_id = $arr["evaluation_id"];

			echo("<option value=\"$evaluation_id\"");

			if($mst != "") {
				if (($mst == $evaluation_id) && ($mode != "MstLoad")) {
					echo(" selected");
				}
			}
			echo(">$evaluation_name\n");
		}
	}

	//====================================
	// カテゴリ種類HTML取得
	//====================================
	function get_cate_tpl($arr,$cate,$mode)
	{
		ob_start();
		show_get_cate_tpl($arr,$cate,$mode);
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}

	//====================================
	// カテゴリを出力
	//====================================
	function show_get_cate_tpl($arr_cate,$cate,$mode) {
		echo("<option value=\"\">");

		foreach ($arr_cate as $index => $arr) {

			$category_name = $arr["category_name"];
			$category_id = $arr["category_id"];

			echo("<option value=\"$category_id\"");
			if($cate != "") {
				if (($cate == $category_id) && ($mode != "MstLoad")) {
					echo(" selected");
				}
			}
			echo(">$category_name\n");
		}
	}

	//====================================
	// テンプレートHTML取得
	//====================================
	function get_tlp_options($arr,$tel)
	{
		ob_start();
		show_tel_options($arr,$tel);
		$str_buff=ob_get_contents();
		ob_end_clean();
		return $str_buff;
	}

	//====================================
	// テンプレートを出力
	//====================================
	function show_tel_options($arr_cate,$tel) {

		echo("<option value=\"\">");

		foreach ($arr_cate as $index => $arr) {

			$evaluation_wkfw_title = $arr["evaluation_wkfw_title"];
			$evaluation_short_wkfw_name = $arr["evaluation_short_wkfw_name"];

			echo("<option value=\"$evaluation_short_wkfw_name\"");

			if($tel != "") {
				if ($tel == $evaluation_short_wkfw_name) {
					echo(" selected");
				}
			}
			echo(">$evaluation_wkfw_title\n");
		}

	}

	//====================================
	// Javascript文字列取得
	//====================================
	//function get_js_str($arr_wkfwcatemst,$workflow)
	//{
	//	$str_buff[]  = "";
	//	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
	//		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";
	//
	//
	//		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
	//			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
	//			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
	//			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);
	//
	//			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";
	//
	//		}
	//
	//		$str_buff[] .= "\t\t}\n";
	//	}
	//
	//	return implode('', $str_buff);
	//}

	$log->info(basename(__FILE__)." END");
	$log->shutdown();
