<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理 | 診断名リスト</title>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require("label_by_profile_type.ini"); ?>
<?
// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 患者管理各権限の取得
$ptreg = check_authority($session, 22, $fname);
$ptif = check_authority($session, 15, $fname);
$outreg = check_authority($session, 35, $fname);
$inout = check_authority($session, 33, $fname);
$disreg = check_authority($session, 36, $fname);
$dishist = check_authority($session, 34, $fname);

// 権限のチェック
if ($dishist == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

//--------------------------------------------------------------------

//DBへのコネクション作成
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

//----------患者情報----------

$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f' order by ptif_id";
$sel = select_from_table($con,$SQL85,$cond,$fname);		//ptifmstから取得
if($sel == 0){
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$lt_kn_nm = pg_result($sel,0,"ptif_lt_kana_nm");
$ft_kn_nm = pg_result($sel,0,"ptif_ft_kana_nm");
$lt_kj_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
$ft_kj_nm = pg_result($sel,0,"ptif_ft_kaj_nm");
$birth    = pg_result($sel,0,"ptif_birth");

$yr = substr($birth,0,4);
$mn = substr($birth,4,2);
$dy = substr($birth,6,2);
$birth = "$yr"."年"."$mn"."月"."$dy"."日";

$sex      = pg_result($sel,0,"ptif_sex");
if($sex == 1){
	$sex = "男性";
}else{
	$sex = "女性";
}

//----------診断名情報----------

$sql = "select *, (select sect_nm from sectmst where sectmst.enti_id = dishist.enti_id and sectmst.sect_id = dishist.sect_id and sectmst.sect_del_flg = 'f') as sect_nm from dishist";
$cond = "where ptif_id = '$pt_id' order by dishist_strt_dt desc";
$sel_dishist = select_from_table($con, $sql, $cond, $fname);
if ($sel_dishist == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

// 検索フォームからの引継ぎ値をurlencode
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function openDetail(pt_id, reg_emp, reg_date, reg_time) {
	window.open('disease_register.php?session=<? echo($session); ?>&pt_id=' + pt_id + '&r_emp=' + reg_emp + '&r_date=' + reg_date + '&r_time=' + reg_time, 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteDetail(pt_id, reg_emp, reg_date, reg_time) {
	if (confirm('削除してよろしいですか？')) {
		location.href = 'disease_delete.php?session=<? echo($session); ?>&pt_id=' + pt_id + '&r_emp=' + reg_emp + '&r_date=' + reg_date + '&r_time=' + reg_time;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="./patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&page=<? echo($page); ?>&key1=<? echo($key1); ?>&key2=<? echo($ekey2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基本</font></a></td>
<td width="5">&nbsp;</td>
<td width="85" align="center" bgcolor="#bdd1e7"><a href="patient_insurance.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">健康保険</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_genogram.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族構成図</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_sub_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="patient_contact_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_claim_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<? } ?>
<? if ($inout == 1) { ?>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inoutpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["IN_OUT"][$profile_type]); ?></font></a></td>
<? } ?>
<? if ($dishist == 1) { ?>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#5279a5"><a href="disease_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>プロブレムリスト</b></font></a></td>
<? } ?>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="135" align="center" bgcolor="#bdd1e7"><a href="patient_karte.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?></font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pt_id); ?></font></td>
<td width="20%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_NAME"][$profile_type]); ?></font></td>
<td width="30%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>診断名一覧</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病名</font></td>
<td width="80"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">診療科</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日</font></td>
<td width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">転帰日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保険</font></td>
<? if ($disreg == 1) { ?>
<td width="100" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新／削除</font></td>
<? } ?>
</tr>
<?
while ($row = pg_fetch_array($sel_dishist)) {
	$disease = $row["dishist_disease"];
	switch ($row["dishist_result"]) {
	case "1":
		$result = "治癒";
		break;
	case "2":
		$result = "中止";
		break;
	case "3":
		$result = "継続";
		break;
	case "4":
		$result = "死亡";
		break;
	default:
		$result = "";
		break;
	}
	$sect_nm = $row["sect_nm"];
	$strt_dt = format_date($row["dishist_strt_dt"]);
	$end_dt = format_date($row["dishist_end_dt"]);
	$insurance = $row["dishist_insurance"];
	$reg_emp_id = $row["dishist_emp_id"];
	$reg_date = $row["dishist_reg_dt"];
	$reg_time = $row["dishist_reg_tm"];
?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $disease; ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $result; ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $sect_nm; ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $strt_dt; ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $end_dt; ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo $insurance; ?></font></td>
<? if ($disreg == 1) { ?>
<td align="center">
<input type="button" value="更新" onclick="openDetail('<? echo($pt_id); ?>', '<? echo($reg_emp_id); ?>', '<? echo($reg_date); ?>', '<? echo($reg_time); ?>');">
<input type="button" value="削除" onclick="deleteDetail('<? echo($pt_id); ?>', '<? echo($reg_emp_id); ?>', '<? echo($reg_date); ?>', '<? echo($reg_time); ?>');">
</td>
<? } ?>
</tr>
<?
}
?>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
//------------------------------------------------------------------------------
// Title     :  書式フォーマット済み日付取得
// Name      :  format_date
// Arguments :  dt - 日付（「YYYYMMDD」形式）
// Return    :  日付（「YYYY/MM/DD」形式）
//------------------------------------------------------------------------------
function format_date($dt) {
	$pattern = '/^(\d{4})(\d{2})(\d{2})$/';
	$replacement = '$1/$2/$3';
	return preg_replace($pattern, $replacement, $dt);
}
?>
