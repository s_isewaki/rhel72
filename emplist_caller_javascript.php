<?php
function insert_javascript() {
    ?>
    <script type="text/javascript"><!--

var m_target_list = new Array();
m_target_list["1"] = new Array();
m_target_list["2"] = new Array();
m_target_list["3"] = new Array();
m_target_list["4"] = new Array();

//--------------------------------------------------
//登録対象者情報
//--------------------------------------------------
function user_info(emp_id,emp_name)
{
	this.emp_id = emp_id;
	this.emp_name = emp_name;
}

//--------------------------------------------------
//登録対象者追加 引数：", "で区切られたID、名前
//--------------------------------------------------
//※この関数は他の画面からも呼ばれます。
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");

	//追加
	for(i=0;i<emp_ids.length;i++){
		m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_ids[i],emp_names[i]));

	}
	//登録対象者一覧の重複除去
	single_target_list(item_id);
	//HTML反映処理
	update_target_html(item_id);

<?
// delete_other_target_listの3番目の引数の数字を増やす場合は
// 呼出し元のhiddenや<span id="target_disp_area3"></span>を
// 実際に使用しなくてもダミーで追加する。
//	delete_other_target_list(item_id, emp_id, 2);
?>
}

//--------------------------------------------------
//登録対象者を削除します。
//--------------------------------------------------
function delete_target(item_id, emp_id,emp_name)
{
	if(confirm("「" + emp_name + "」を登録対象者から削除します。よろしいですか？"))
	{
		delete_target_list(item_id, emp_id);
		
		//HTML反映処理
		update_target_html(item_id);
	}
}

//--------------------------------------------------
//m_target_listより登録対象者を削除します。
//--------------------------------------------------
//※ここではHTML反映は行わないため、update_target_html()を実行する必要あり。
function delete_target_list(item_id, emp_id)
{
	var new_array = new Array();
	for(var i=0;i<m_target_list[item_id].length;i++)
	{
		if(emp_id != m_target_list[item_id][i].emp_id)
		{
			new_array = array_add(new_array,m_target_list[item_id][i]);
		}
	}
	m_target_list[item_id] = new_array;
}

//--------------------------------------------------
//item_id以外のリストから登録対象者を削除します。複数ID指定可
//--------------------------------------------------
function delete_other_target_list(item_id, emp_id, max_other_id)
{
	var emp_ids = emp_id.split(", ");
	var new_array;
	var exist_flg = false;
	for (var other_id=1; other_id<=max_other_id; other_id++) {

		if (other_id == item_id) continue;

		new_array = new Array();

		for(var i=0; i<m_target_list[other_id].length; i++)
		{
			exist_flg = false;
			for (var j=0; j<emp_ids.length; j++) {
				if(emp_ids[j] == m_target_list[other_id][i].emp_id)
				{
					exist_flg = true;
					break;
				}
			}
			if (exist_flg == false) {
				new_array = array_add(new_array,m_target_list[other_id][i]);
			}
		}
		m_target_list[other_id] = new_array;
		update_target_html(other_id);
	}
}
//--------------------------------------------------
//m_target_listの登録対象者の重複をなくします。
//--------------------------------------------------
function single_target_list(item_id)
{
	var new_target_list = new Array();
	new_target_list[item_id] = new Array();
	var emp_id_csv = "";
	for(var i = 0; i < m_target_list[item_id].length; i++)
	{
		var emp_id = m_target_list[item_id][i].emp_id;
		if(emp_id_csv.indexOf(emp_id) == -1)
		{
			//重複していない。
			emp_id_csv = emp_id_csv + "," + emp_id;
			new_target_list[item_id] = array_add(new_target_list[item_id],m_target_list[item_id][i]);
		}
	}
	m_target_list[item_id] = new_target_list[item_id];
}

//--------------------------------------------------
//m_target_listより登録対象者のHTMLを再生成します。
//--------------------------------------------------
function update_target_html(item_id)
{
	//登録対象者HTMLを更新
	_update_target_html2(item_id,document.getElementById("target_disp_area"+item_id),document.getElementById("target_id_list"+item_id),document.getElementById("target_name_list"+item_id));
}

function _update_target_html2(item_id,disp_obj,target_hidden_id,target_hidden_name)
{
	if(m_target_list[item_id].length == 0)
	{
		disp_obj.innerHTML = "";
		target_hidden_id.value = "";
		target_hidden_name.value = "";
	}
	else
	{
		var disp_obj_html = "";
		var target_hidden_id_value = "";
		var target_hidden_name_value = "";
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			var emp_id   = m_target_list[item_id][i].emp_id;
			var emp_name = m_target_list[item_id][i].emp_name;
			if(i!=0)
			{
				disp_obj_html = disp_obj_html + ",";
				target_hidden_id_value = target_hidden_id_value + ",";
				target_hidden_name_value = target_hidden_name_value + ",";
			}
			
			disp_obj_html = disp_obj_html + "<a href=\"javascript:void(0);\" onClick=\"delete_target('" + item_id + "','" + emp_id + "','" + emp_name + "'); return false;\">" + emp_name + "</a>";
			
			target_hidden_id_value = target_hidden_id_value + emp_id;
			target_hidden_name_value = target_hidden_name_value + emp_name;
		}
		disp_obj.innerHTML = disp_obj_html;
		target_hidden_id.value = target_hidden_id_value;
		target_hidden_name.value = target_hidden_name_value;
	}
}


//--------------------------------------------------
//配列追加
//--------------------------------------------------
function array_add(array_obj,add_obj)
{
	return array_obj.concat( new Array( add_obj ) );
}

//--------------------------------------------------
//m_target_list形式データをデバッグします。
//--------------------------------------------------
function debug_target_list(item_id,target_list)
{
	var str = "\n";
	if(target_list[item_id].length != 0)
	{
		for(var i=0;i<target_list[item_id].length;i++)
		{
			str = str + target_list[item_id][i].emp_id + "(" + target_list[item_id][i].emp_name + ")" + "\n";
		}
	}
	alert(str);
}


// --></script>
<?php
} /* End of included JavaScript */

?>
