<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理｜対象者検索</title>
<?
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_attendance_pattern.ini");
require_once("date_utils.php");
require_once("show_select_values.ini");
require_once("atdbk_common_class.php");
require_once("work_admin_paid_holiday_common.php");
require_once("show_timecard_common.ini");
require_once("atdbk_menu_common.ini");


// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}
//1ページあたり件数
$limit = 50;
$url_srch_name = urlencode($srch_name);

// DBコネクション作成
$con = connect2db($fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

// 締め日を取得
$sql = "select closing, closing_parttime, closing_paid_holiday, criteria_months from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$closing = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing") : "";
$closing_parttime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_parttime") : "";
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";
//非常勤が未設定時は常勤を使用する 20091214
if ($closing_parttime == "") {
	$closing_parttime = $closing;
}
//基準月数
$criteria_months = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "criteria_months") : "2";
$wk_month = ($criteria_months == "1") ? "3" : "6";

//年設定
//$default_start_year = 2006;


//付与年月日入力の場合
if ($select_add_yr != "-" && $select_add_yr != "") {
	$year = $select_add_yr;
} elseif ($select_close_yr != "") {
	//
	$cur_year = $select_close_yr;
	$md = $select_close_mon.$select_close_day;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
	//指定年
	$year = $cur_year;
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
else {
	$cur_year = date("Y");
	$md = date("md");;
	//年度にする
	if ($closing_paid_holiday == "1") {
		if($md >= "0101" and $md <= "0331") {
			$cur_year--;
		}
	}
}

//年一覧開始年
if ($start_year == "") {
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
$end_year = $start_year + 6;

$arr_class_name = get_class_name_array($con, $fname);

// 組織情報を取得し、配列に格納
$arr_cls = array();
$arr_clsatrb = array();
$arr_atrbdept = array();
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));

	$sql2 = "select atrb_id, atrb_nm from atrbmst";
    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' order by order_no";
	$sel2 = select_from_table($con, $sql2, $cond2, $fname);
	if ($sel2 == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_atrb = array();
	while ($row2 = pg_fetch_array($sel2)) {
		$tmp_atrb_id = $row2["atrb_id"];
		$tmp_atrb_nm = $row2["atrb_nm"];
		array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));

		$sql3 = "select dept_id, dept_nm from deptmst";
        $cond3 = "where atrb_id = '$tmp_atrb_id' and dept_del_flg = 'f' order by order_no";
		$sel3 = select_from_table($con, $sql3, $cond3, $fname);
		if ($sel3 == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_dept = array();
		while ($row3 = pg_fetch_array($sel3)) {
			$tmp_dept_id = $row3["dept_id"];
			$tmp_dept_nm = $row3["dept_nm"];
			array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
		}
		array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
	}
	array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
}

// 該当する職員を検索
$sel_emp = false;
$arr_data = array();
if ($srch_flg == "1") {
	$sql = "select count(*) from empmst";

	//付与日指定時
	if ($select_add_yr != "-") {
		$search_mmdd = "";
		if ($select_add_mon != "-" && $select_add_day != "-") {
			$search_mmdd = $select_add_mon.$select_add_day;
			$compare_mmdd = "mmdd";
			$compare_len = 4;
		} elseif ($select_add_mon != "-") {
			$search_mmdd = $select_add_mon;
			$compare_mmdd = "mm";
			$compare_len = 2;
		}
		//2件有り得るので1件に絞り込む 20110404
		$sql .= " left join (select distinct on (b.emp_id) b.* from emppaid b  ";
		$sql .= " where  ";
		$sql .= " CAST(b.year AS varchar) = '$select_add_yr' and substr(b.paid_hol_add_mmdd, 1, $compare_len) = '$search_mmdd' order by b.emp_id, b.year desc, b.paid_hol_add_mmdd desc ";
		$sql .= " ) emppaid on emppaid.emp_id = empmst.emp_id ";
	}
	//有休残締め日指定時
	elseif ($select_close_yr != "-") {
		$start_ymd = ($select_close_yr-1).$select_close_mon.$select_close_day;
		$end_ymd = $select_close_yr.$select_close_mon.$select_close_day;

		//2件有り得るので1件に絞り込む
		$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
		$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
		$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
		$sql .= " where j.emp_id = b.emp_id  and  ";
		$sql .= " (((CAST(b.year AS varchar)||b.paid_hol_add_mmdd > '$start_ymd' and ";
		$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd <= '$end_ymd')  ";
		$sql .= " ))) order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
		$sql .= " ) emppaid on emppaid.emp_id = empmst.emp_id ";
	}

	$sql .= " left join empcond f on f.emp_id = empmst.emp_id ";
	//付与日優先
	if ($select_add_yr != "-") {

		$wk_year_before = ($select_add_yr - 1).$select_add_mon;
		$select_add_ymd = $select_add_yr.$select_add_mon;
		if ($select_add_day != "-" && $select_add_day != "") {
			$wk_year_before .= $select_add_day;
			$select_add_ymd .= $select_add_day;
		} else {
			$wk_year_before .= "01";
			$select_add_ymd .= "01";
		}
		$sql .= " left join (select distinct on (j.emp_id) j.* from emppaid j where (CAST(j.year AS varchar)||j.paid_hol_add_mmdd >= '$wk_year_before' and ";
		$sql .= " CAST(j.year AS varchar)||j.paid_hol_add_mmdd < '$select_add_ymd')  order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
		$sql .= " ) g on g.emp_id = empmst.emp_id ";

	}
	//有休残締め日指定時
	elseif ($select_close_yr != "-") {
		//1:4/1, 2:1/1
		if ($closing_paid_holiday != "3") {
			// 2年前まで、2件有り得るので1件に絞り込む 20110325
			$wk_mmdd = ($closing_paid_holiday == "1") ? "0401" : "0101";
			$start_ymd = ($year-2).$wk_mmdd;
			$end_ymd = $year.$wk_mmdd;
			$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
			$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
			$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
			$sql .= " where j.emp_id = b.emp_id  and  ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd >= '$start_ymd' and ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd < '$end_ymd') order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
			$sql .= " ) g on g.emp_id = empmst.emp_id ";
		}
		//3:採用日
		else {
			$start_ymd = ($select_close_yr-3).$select_close_mon.$select_close_day;
			$end_ymd = ($select_close_yr-1).$selcet_close_mon.$select_close_day;
			//2件有り得るので1件に絞り込む
			$sql .= " left join (select distinct on (j.emp_id, j.year, j.paid_hol_add_mmdd) j.* from emppaid j  ";
			$sql .= " where CAST(j.year AS varchar)||j.paid_hol_add_mmdd =  ";
			$sql .= " (select max(CAST(b.year AS varchar)||b.paid_hol_add_mmdd ) from emppaid b  ";
			$sql .= " where j.emp_id = b.emp_id  and  ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd > '$start_ymd' and ";
			$sql .= " CAST(b.year AS varchar)||b.paid_hol_add_mmdd <= '$end_ymd') order by j.emp_id, j.year desc, j.paid_hol_add_mmdd desc ";
			$sql .= " ) g on g.emp_id = empmst.emp_id ";
		}
	}
	$select_close_date = $select_close_yr.$select_close_mon.$select_close_day;
    $cond = get_paid_srch_cond($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_date, $srch_id);

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_cnt = pg_fetch_result($sel, 0, 0);

	//付与年月日未設定時、プルダウンの"-"を""にする
	if ($select_add_yr == "-") {
		$select_add_yr = "";
	}
	if ($select_add_mon == "-") {
		$select_add_mon = "";
	}
	if ($select_add_day == "-") {
		$select_add_day = "";
	}
	//sql文取得
	$sql = get_paid_srch_sql($srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day, $year, $criteria_months, $select_add_yr, $closing_paid_holiday, $select_close_yr, $select_close_mon, $select_close_day, $sort);

	$offset = $page * $limit;
	//ソート順
	if ($sort == "") {
		$sort = "1";
	}
	switch ($sort) {
		case "1":
			$orderby = "empmst.emp_personal_id ";
			break;
		case "2":
			$orderby = "empmst.emp_personal_id desc ";
			break;
		case "3":
			$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
			break;
		case "4":
			$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
			break;
		case "5":
			$orderby = "c.order_no, d.order_no, e.order_no, empmst.emp_personal_id ";
			break;
		case "6":
			$orderby = "c.order_no desc, d.order_no desc, e.order_no desc, empmst.emp_personal_id ";
			break;
	}
	$cond .= " order by $orderby offset $offset limit $limit";
	$sel_emp = select_from_table($con, $sql, $cond, $fname);
	if ($sel_emp == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

}
$sql = "select closing_paid_holiday from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$closing_paid_holiday = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "closing_paid_holiday") : "1";

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by order_no";
$cond = "";
$paid_sel = select_from_table($con, $sql, $cond, $fname);
if ($paid_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_paid_hol = array();
while($row = pg_fetch_array($paid_sel)){
	$paid_hol_tbl_id      = $row["paid_hol_tbl_id"];
	for ($i=1; $i<=8; $i++) {
		$varname = "add_day".$i;
        //未設定時の対応、""は0とする、8番目が""の時は7番目の数値を使用 20120329
        $wk_days = $row["$varname"];
        if ($i == 8 && $wk_days == "") {
            $wk_days = $arr_paid_hol[$paid_hol_tbl_id][7];
        }
        if ($wk_days == "") {
            $wk_days = "0";
        }
        $arr_paid_hol[$paid_hol_tbl_id][$i] = $wk_days;
	}
}

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function initPage() {
	classOnChange('<? echo($atrb); ?>', '<? echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<? foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<? echo $tmp_clsatrb["class_id"]; ?>') {
	<? foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<? echo $tmp_atrb["id"]; ?>', '<? echo $tmp_atrb["name"]; ?>', atrb_id);
	<? } ?>
	}
<? } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<? foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<? echo $tmp_atrbdept["atrb_id"]; ?>') {
	<? foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<? echo $tmp_dept["id"]; ?>', '<? echo $tmp_dept["name"]; ?>', dept_id);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}


function changeDay(pos) {
	days1 = document.getElementById("days1_"+pos).value;
	days2 = document.getElementById("days2_"+pos).value;
	adjust = document.getElementById("adjust_"+pos).value;
	if (days1 == "") days1 = "0";
	if (days2 == "") days2 = "0";
	if (adjust == "") adjust = "0";
	if (!days1.match(/^\d$|^\d+\.?\d+$/g)) {
		alert('当年繰越には半角数字を入力してください。');
		return false;
	}
	if (!days2.match(/^\d$|^\d+\.?\d+$/g)) {
		alert('当年付与には半角数字を入力してください。');
		return false;
	}
	if (!adjust.match(/^-?\d$|^-?\d+\.?\d+$/g)) {
		alert('調整日数には半角数字を入力してください。');
		return false;
	}
	days3 = eval(days1) + eval(days2) + eval(adjust);
	if (days3 == 0 || isNaN(days3)) days3 = "";
	document.getElementById("days3_"+pos).value = days3;

	curr_use = document.getElementById("curr_use_"+pos).value;
	if (curr_use == "") curr_use = "0";
	curr_zan = days3 - eval(curr_use);
	document.getElementById("curr_zan_"+pos).value = curr_zan;

}

//検索時のパラメータチェック
function chkPara() {
	if ((document.mainform.select_add_yr.selectedIndex == 0 &&
		document.mainform.select_add_mon.selectedIndex == 0 &&
		document.mainform.select_add_day.selectedIndex == 0) ||
		(document.mainform.select_add_yr.selectedIndex != 0 &&
		document.mainform.select_add_mon.selectedIndex != 0)
		) {
		;
	} else {
		alert('付与年月日には、年月日または年月を指定してください');
		return false;
	}
	if ((document.mainform.select_close_yr.selectedIndex == 0 &&
		document.mainform.select_close_mon.selectedIndex == 0 &&
		document.mainform.select_close_day.selectedIndex == 0) ||
		(document.mainform.select_close_yr.selectedIndex != 0 &&
		document.mainform.select_close_mon.selectedIndex != 0 &&
		document.mainform.select_close_day.selectedIndex != 0)
		) {
		;
	} else {
		alert('有休残締め日には、年月日を指定するか、または、すべて空白にしてください');
		return false;
	}
	document.mainform.page.value = 0
	return true;
}

//年休付与日数表、表番号と勤続年数をキーとする2次元配列、indexは1から始まる
var arr_paid_hol = new Array();
<?
for ($i=1; $i<=8; $i++) {
	$wk_arr_str = "arr_paid_hol[$i] = Array(0";	//先頭の0はダミー
	for ($j=1; $j<=8; $j++) {
        $wk_arr_str .= ",".$arr_paid_hol[$i][$j];
	}
	$wk_arr_str .= ");\n";
	echo($wk_arr_str);
}

?>
//表変更
function changeTblId(pos, tbl_id) {
	var kinzoku_id = document.getElementById("kinzoku_id_"+pos).value;
	var fuyo_su = arr_paid_hol[tbl_id][kinzoku_id];
	document.getElementById("days2_"+pos).value = fuyo_su;
	changeDay(pos);
}


//印刷ＷＩＮＤＯＷをＯＰＥＮ
function openPrintPage() {
	dx = screen.width;
	dy = screen.top;
	base_left = 0;
	base = 0;
	wx = 720;
	wy = 700;

	childwin_print = window.open('' , 'printFormChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	document.printForm.action = 'work_admin_paid_holiday_print.php';
	document.printForm.target = "printFormChild";

	document.printForm.srch_id.value = document.mainform.srch_id.value;
	document.printForm.srch_name.value = document.mainform.srch_name.value;
	document.printForm.cls.value = document.mainform.cls.value;
	document.printForm.atrb.value = document.mainform.atrb.value;
	document.printForm.dept.value = document.mainform.dept.value;
	document.printForm.group_id.value = document.mainform.group_id.value;
	document.printForm.duty_form_jokin.value = (document.mainform.duty_form_jokin.checked) ? "checked" : "";
	document.printForm.duty_form_hijokin.value = (document.mainform.duty_form_hijokin.checked) ? "checked" : "";
	document.printForm.select_add_yr.value = document.mainform.select_add_yr.value;
	document.printForm.select_add_mon.value = document.mainform.select_add_mon.value;
	document.printForm.select_add_day.value = document.mainform.select_add_day.value;
	document.printForm.select_close_yr.value = document.mainform.select_close_yr.value;
	document.printForm.select_close_mon.value = document.mainform.select_close_mon.value;
	document.printForm.select_close_day.value = document.mainform.select_close_day.value;
	document.printForm.sort.value = document.mainform.sort.value;
	document.printForm.submit();
}

//ダウンロード
function downloadExcel() {

<? /*
印刷が先に作成されたため、フォームを共有する
*/ ?>

	document.printForm.srch_id.value = document.mainform.srch_id.value;
	document.printForm.srch_name.value = document.mainform.srch_name.value;
	document.printForm.cls.value = document.mainform.cls.value;
	document.printForm.atrb.value = document.mainform.atrb.value;
	document.printForm.dept.value = document.mainform.dept.value;
	document.printForm.group_id.value = document.mainform.group_id.value;
	document.printForm.duty_form_jokin.value = (document.mainform.duty_form_jokin.checked) ? "checked" : "";
	document.printForm.duty_form_hijokin.value = (document.mainform.duty_form_hijokin.checked) ? "checked" : "";
	document.printForm.select_add_yr.value = document.mainform.select_add_yr.value;
	document.printForm.select_add_mon.value = document.mainform.select_add_mon.value;
	document.printForm.select_add_day.value = document.mainform.select_add_day.value;
	document.printForm.select_close_yr.value = document.mainform.select_close_yr.value;
	document.printForm.select_close_mon.value = document.mainform.select_close_mon.value;
	document.printForm.select_close_day.value = document.mainform.select_close_day.value;

	document.printForm.action = 'work_admin_paid_holiday_excel.php';
	document.printForm.target = 'download';
	document.printForm.submit();

}

//ソート
function set_sort(id) {

	sort_key = document.mainform.sort.value;

	if (id == "EMP_ID") {
		sort_key = (sort_key == "1") ? "2" : "1";
	} else if (id == "EMP_NAME") {
		sort_key = (sort_key == "3") ? "4" : "3";
	} else if (id == "DEPT") {
		sort_key = (sort_key == "5") ? "6" : "5";
	}
	document.mainform.sort.value = sort_key;
	document.mainform.page.value = 0;
	document.mainform.submit();
}

//修正画面
function openEdit(emp_id, paid_hol_tbl_id, paid_hol_add_date, service_year, service_mon) {

	base_left = 20;
	base = 20;
	wx = 900;
	wy = 600;

	//有休残締め日
	if (document.mainform.select_add_yr.value == '-' && document.mainform.select_add_mon.value == '-') {
		select_close_date = ''+document.mainform.select_close_yr.value+document.mainform.select_close_mon.value+document.mainform.select_close_day.value;
	} else {
		select_close_date = '';
	}

	url = 'work_admin_paid_edit.php?session=<? echo($session); ?>&emp_id='+emp_id+'&arg_tbl_id='+paid_hol_tbl_id+'&arg_add_date='+paid_hol_add_date+'&service_year='+service_year+'&service_mon='+service_mon+'&select_close_date='+select_close_date;

	childwin = window.open(url , 'EditChild', 'left='+base_left+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

}
//有休付与レコード一括作成
function bulkData() {

	if ((document.mainform.select_add_yr.selectedIndex != 0 &&
		document.mainform.select_add_mon.selectedIndex != 0)
		) {
		;
	} else {
		alert('付与年月日または年月を指定してください');
		return false;
	}
	if (confirm('指定された付与年月日の有休付与情報がない場合に新規作成します。よろしいですか？')) {
		document.mainform.action = "work_admin_paid_holiday_bulk.php";
		document.mainform.update_flg.value = '1';
		document.mainform.submit();
	} else {
		return false;
	}
}

function changeYmd() {

	if (!document.mainform.printBtn1) {
		return;
	}
	add_yr = document.mainform.select_add_yr.value;
	add_mon = document.mainform.select_add_mon.value;

	close_yr = document.mainform.select_close_yr.value;
	close_mon = document.mainform.select_close_mon.value;
	close_day = document.mainform.select_close_day.value;
	//alert(close_yr);

	if ((close_yr != "-" && close_mon != "-" && close_day != "-") ||
		(add_yr != "-" && add_mon != "-")) {
		document.mainform.printBtn1.disabled = false;
		document.mainform.printBtn2.disabled = false;
	} else {
		document.mainform.printBtn1.disabled = true;
		document.mainform.printBtn2.disabled = true;
	}
}
//クリア
function clearAddYmd() {
	document.mainform.select_add_yr.value = "-";
	document.mainform.select_add_mon.value = "-";
	document.mainform.select_add_day.value = "-";

	changeYmd();
}
//当日設定
function setTodayAddYmd() {
	today = new Date();
	yy = today.getFullYear();
	mm = today.getMonth() + 1;
	dd = today.getDate();
	if (mm < 10) { mm = "0" + mm; }
	if (dd < 10) { dd = "0" + dd; }

	document.mainform.select_add_yr.value = yy;
	document.mainform.select_add_mon.value = mm;
	document.mainform.select_add_day.value = dd;

	changeYmd();
}

//クリア
function clearCloseYmd() {
	document.mainform.select_close_yr.value = "-";
	document.mainform.select_close_mon.value = "-";
	document.mainform.select_close_day.value = "-";

	changeYmd();
}
//当日設定
function setTodayCloseYmd() {
	today = new Date();
	yy = today.getFullYear();
	mm = today.getMonth() + 1;
	dd = today.getDate();
	if (mm < 10) { mm = "0" + mm; }
	if (dd < 10) { dd = "0" + dd; }

	document.mainform.select_close_yr.value = yy;
	document.mainform.select_close_mon.value = mm;
	document.mainform.select_close_day.value = dd;

	changeYmd();
}

//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<?
//サブメニュー表示
show_work_admin_submenu($session,$fname,$arr_option);

	if (($closing == "") || ($closing_parttime == "")) {  // 締め日未登録の場合
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード締め日が登録されていません。出退勤マスターメンテナンスの締め日画面で登録してください。</font></td>
</tr>
</table>
<? } else { ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="work_admin_paid_holiday.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
		<td><input type="text" name="srch_id" value="<? echo($srch_id); ?>" size="15" maxlength="12" style="ime-mode:inactive;"></td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
		<td><input type="text" name="srch_name" value="<? echo($srch_name); ?>" size="25" maxlength="50" style="ime-mode:active;"></td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">属性</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="cls" onchange="classOnChange();">
				<option value="-">----------
<?
foreach ($arr_cls as $tmp_cls) {
	$tmp_id = $tmp_cls["id"];
	$tmp_name = $tmp_cls["name"];
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $cls) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
				</select><? echo($arr_class_name[0]); ?><br>
			<select name="atrb" onchange="atrbOnChange();">
			</select><? echo($arr_class_name[1]); ?><br>
			<select name="dept">
			</select><? echo($arr_class_name[2]); ?>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;グループ
<select name="group_id">
<option value="-">----------</option>
<?
foreach ($group_names as $tmp_group_id => $group_name) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $group_id) {
		echo(" selected");
	}
	echo(">$group_name\n");
}
?>
</select></font>
</td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">雇用・勤務形態</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<input type="checkbox" name="duty_form_jokin" value="checked" <?=$duty_form_jokin?>>常勤　
			<input type="checkbox" name="duty_form_hijokin" value="checked" <?=$duty_form_hijokin?>>非常勤
			</font>
		</td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">付与年月日</font></td>
		<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="select_add_yr" onchange="changeYmd();">
			<? show_select_years(2, $select_add_yr, true, true); ?>
			</select>
年
			<select name="select_add_mon" onchange="changeYmd();">
			<? show_select_months($select_add_mon, true); ?>
			</select>
月
			<select name="select_add_day" onchange="changeYmd();">
			<? show_select_days($select_add_day, true); ?>
			</select>
日
			</font>
			<input type="button" value="クリア" onclick="clearAddYmd();";>
			<input type="button" value="当日" onclick="setTodayAddYmd();";>
		</td>
	</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">有休残締め日</font></td>
		<td>
	<?
	if ($select_close_yr == "") {
		$select_close_yr = date("Y");
	}
	if ($select_close_mon == "") {
		$select_close_mon = date("m");
	}
	if ($select_close_day == "") {
		$select_close_day = date("d");
	}
	?>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="select_close_yr" onchange="changeYmd();">
			<? show_select_years(2, $select_close_yr, true, true); ?>
			</select>
年
			<select name="select_close_mon" onchange="changeYmd();">
			<? show_select_months($select_close_mon, true); ?>
			</select>
月
			<select name="select_close_day" onchange="changeYmd();">
			<? show_select_days($select_close_day, true); ?>
			</select>
日
			</font>
			<input type="button" value="クリア" onclick="clearCloseYmd();";>
			<input type="button" value="当日" onclick="setTodayCloseYmd();";>
		</td>
	</tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2" align="right"><input type="submit" value="検索" onclick="return chkPara();"></td>
</tr>
</table>

	<?
	//年度、一覧表示
	$select_close_date = $select_close_yr.$select_close_mon.$select_close_day;
	$select_add_date = $select_add_yr.$select_add_mon.$select_add_day;
?>
<? show_navi($emp_cnt, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort, $closing_paid_holiday, $select_close_date, $select_add_date, 1); ?>
<?
	// 一覧表示
get_emp_list_data($con, $fname, $sel_emp, $closing_paid_holiday, $arr_paid_hol, $year, $criteria_months, $atdbk_common_class, $select_close_date, $sort, $select_add_date, "1");
?>

<? show_navi($emp_cnt, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort, $closing_paid_holiday, $select_close_date, $select_add_date, 2); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="check_all" value="1">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="disp_year" value="<? echo($year); ?>">
<input type="hidden" name="start_year" value="<? echo($start_year); ?>">
<input type="hidden" name="update_flg" value="">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
</form>
</td>
</tr>
</table>

<form name="printForm" action="work_admin_paid_holiday_print.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="srch_id" value="">
<input type="hidden" name="srch_name" value="">
<input type="hidden" name="cls" value="">
<input type="hidden" name="atrb" value="">
<input type="hidden" name="dept" value="">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="duty_form_jokin" value="">
<input type="hidden" name="duty_form_hijokin" value="">
<input type="hidden" name="select_add_yr" value="">
<input type="hidden" name="select_add_mon" value="">
<input type="hidden" name="select_add_day" value="">
<input type="hidden" name="select_close_yr" value="">
<input type="hidden" name="select_close_mon" value="">
<input type="hidden" name="select_close_day" value="">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="sort" value="">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>

<? } ?>
</body>
<? pg_close($con); ?>
</html>
<?

/**
 * ページ切り替えリンクを表示
 *
 * @param string $num 職員数
 * @param string $limit 1ページあたり件数
 * @param string $page ページ数
 * @param string $year 処理年度
 * @param string $start_year リンク開始年
 * @param string $default_start_year デフォルト開始年
 * @param string $fname プログラム名
 * @param string $sort ソート順
 * @param string $closing_paid_holiday 有給休暇の付与日
 * @return なし
 *
 */
function show_navi($num, $limit, $page, $year, $start_year, $default_start_year, $fname, $sort, $closing_paid_holiday, $select_close_date, $select_add_date, $id) {
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");

    global $session, $url_srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_yr, $select_add_mon, $select_add_day, $select_close_yr, $select_close_mon, $select_close_day, $srch_id;

	$url_search_cond = "&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&group_id=$group_id&select_add_yr=$select_add_yr&select_add_mon=$select_add_mon&select_add_day=$select_add_day&select_close_yr=$select_close_yr&select_close_mon=$select_close_mon&select_close_day=$select_close_day&sort=$sort&srch_id=$srch_id";

	$total_page = ceil($num/$limit);
	$check_next = $num % $limit;
	$check_last = $total_page - 1;

	echo("<table width=\"1040\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	//年
	$cnt_per_page = 7;
	echo("<td width=\"260\">\n");
	echo("&nbsp;</td>\n");
	if ($num > 0 ) {
		echo("<td align=\"right\" width=\"400\">\n");
		echo("<input type=\"button\" value=\"有休付与レコード一括作成\" onclick=\"bulkData();\">\n");

		//日付指定がない場合
		if ($select_add_date == "" && $select_close_date == "---") {
			$disabled = " disabled";
		} else {
			$disabled = "";
		}

		echo("<input type=\"button\" name=\"excelBtn\" id=\"excelBtn\" value=\"EXCEL出力\" onclick=\"downloadExcel();\"> ");
		echo("<input type=\"button\" name=\"printBtn{$id}\" id=\"printBtn{$id}\" value=\"印刷\" onclick=\"openPrintPage();\" $disabled>\n");
		echo("&nbsp;");
		echo("</td>\n");
	}

	echo("<td align=\"left\" width=\"380\">\n");
	if ($total_page >= 2) {
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("ページ");
		echo("&nbsp;");
		echo("</font>");

		if ($page > 0) {
			$back = $page - 1;
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session$url_search_cond&page=$back&srch_flg=1&start_year={$start_year}&year={$year}\">←</a></font>");
		}

		for ($i = 0; $i < $total_page; $i++) {
			$j = $i + 1;
			if ($page != $i) {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> <a href=\"$fname?session=$session$url_search_cond&page=$i&srch_flg=1&start_year={$start_year}&year={$year}\">$j</a> </font>");
			} else {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("［{$j}］");
				echo("</font>");
			}
		}

		if ($check_last != $page) {
			$next = $page + 1;
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session$url_search_cond&page=$next&srch_flg=1&start_year={$start_year}&year={$year}\">→</a></font>");
		}

	}
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}


?>
