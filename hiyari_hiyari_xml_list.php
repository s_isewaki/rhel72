<?
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_hiyari_xml_util.ini");
require_once("hiyari_header_class.php");
require_once("hiyari_report_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
//オブジェクト生成
//==============================
$hiyari_obj = new hiyari_hiyari_xml_util($con, $fname);

//==================================================
//レポートクラス
//==================================================

$rep_obj = new hiyari_report_class($con, $fname);

//==============================
//年情報
//==============================
$this_year = date('Y');	
$this_month = date('m');
$start_year = 2004;//統計分析と同じ。



//==============================
//パラメータ精査
//==============================
if($hiyari_year == "")
{
	$hiyari_year = $this_year;
}
if($list_tab_mode == "")
{
	$list_tab_mode = "target_list";
}

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	switch ($mode)
	{
		case "folder_delete":
	
			//==============================
			//分類フォルダの削除
			//==============================
			if(!empty($folder_select))
			{
				$select_count=count($folder_select);
				for($i=0;$i<$select_count;$i++)
				{
					//フォルダID
					$del_folder_id = $folder_select[$i];
					
					//フォルダの物理削除
					delete_hiyari_folder($con,$fname,$del_folder_id);
					
					//選択中の分類フォルダを削除した場合は先頭のフォルダにする。
					if($del_folder_id == $folder_id)
					{
						$folder_id = "";//後で特定
						$list_tab_mode = "target_list";
					}
				}
				
			}
		
			break;
		case "change_output_target":
			//==============================
			//提出対象／提出除外
			//==============================
			if(!empty($list_select))
			{
				$list_ids_csv = join(',',$list_select);
				$output_target_flg = ($change_output_target_in_out == 'in') ? 't':'f';
				$auto_output_target_flg = 'f';//手動に更新
				
				$sql = "update inci_hiyari_xml_folder_report set";
				$set = array("output_target_flg","auto_output_target_flg");
				$setvalue = array($output_target_flg,$auto_output_target_flg);
				$cond = "where folder_id = $folder_id and report_id in($list_ids_csv)";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		
			break;
		case "do_check":
			//==============================
			//チェック処理
			//==============================
			//ここでは行わない。自動分類の後で行う。
			break;
		default:
			break;
	}
	
}


//==============================
//自動分類処理
//==============================

//年全件ではなく指定フォルダのみに変更。そのため初期選択無し＋件数表示無し。
//$hiyari_obj->set_hiyari_folder_auto_classification($hiyari_year);
if($folder_id != "")
{
	$hiyari_obj->set_hiyari_folder_auto_classification_single($folder_id);
}

//==============================
//年に対するフォルダ一覧情報の取得
//==============================
//inci_hiyari_xml_folderのフィールド+report_count。
$sql  = " select";
$sql .= " a.*,";
$sql .= " case when b.report_count isnull then 0 else b.report_count end as report_count";
$sql .= " from inci_hiyari_xml_folder a";
$sql .= " natural left join";
$sql .= " (";
$sql .= " select folder_id,count(*) as report_count from inci_hiyari_xml_folder_report group by folder_id";
$sql .= " ) b";
$sql .= " where hiyari_year = '$hiyari_year' order by folder_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0){
	pg_query($con,"rollback");
	pg_close($con);
	showErrorPage();
	exit;
}
$all_folder_info = pg_fetch_all($sel);
$all_folder_info_count = pg_num_rows($sel);


//==============================
//フォルダを新規で開いた場合のフォルダID特定
//==============================
//※新規時、年変更時、選択フォルダの削除時、フォルダ0件からのフォルダ作成時。
if($folder_id == "")
{
	if($all_folder_info_count == 0)
	{
		$page = 1;
		$folder_id = "";
	}
	else
	{
		$page = 1;
//		$folder_id = $all_folder_info[0]["folder_id"];
		$folder_id = "";
	}
}

//==============================
//フォルダ情報
//==============================
$hiyari_data_type = "";
if($folder_id != "")
{
	foreach($all_folder_info as $all_info)
	{
		if($folder_id == $all_info["folder_id"])
		{
			$hiyari_data_type = $all_info["hiyari_data_type"];
			break;
		}
	}	
}

//==============================
//NGチェック
//==============================
if($mode == "do_check")
{
	if($folder_id != "")
	{
		$hiyari_obj->check_ng_report($folder_id);
	}
}

//==============================
//報告書一覧情報
//==============================
if($folder_id != "")
{
	if($list_tab_mode == "target_list" || $list_tab_mode == "target_list_ng")
	{
		$output_target_not_sql = "";
	}
	elseif($list_tab_mode == "target_out_list")
	{
		$output_target_not_sql = "not";
	}
	
	$check_ng_flg_sql = "";
	if($list_tab_mode == "target_list_ng")
	{
		$check_ng_flg_sql = "and check_ng_flg";
	}
	
	
	$sql  = " select * from";
	$sql .= " (";
	$sql .= " select";
	$sql .= " report_id";
	$sql .= " from inci_hiyari_xml_folder_report where folder_id = $folder_id and $output_target_not_sql output_target_flg $check_ng_flg_sql";
	$sql .= " ) fr";

	$sql .= " natural inner join";
	$sql .= " (";
	$sql .= " select";
	$sql .= " report_id,";
	$sql .= " eid_id,";
	$sql .= " report_no,";
	$sql .= " report_title,";
	$sql .= " registrant_class,";
	$sql .= " registrant_attribute,";
	$sql .= " registrant_dept,";
	$sql .= " registrant_room,";
	$sql .= " registrant_id,";
	$sql .= " registrant_name,";
	$sql .= " anonymous_report_flg";
	$sql .= " from inci_report where not shitagaki_flg and not del_flag";
	$sql .= " ) rp";

	$sql .= " natural left join";
	$sql .= " (";
	$sql .= " select";
	$sql .= " eid_id,";
	$sql .= " input_item as input_item_100_5";
	$sql .= " from inci_easyinput_data where grp_code = 100 and easy_item_code = 5";
	$sql .= " ) dt";
	
	$sql .= " natural left join";
	$sql .= " (";
	$sql .= " select";
	$sql .= " emp_id as registrant_id,";
	$sql .= " emp_kn_lt_nm as report_emp_kn_lt_nm,";
	$sql .= " emp_kn_ft_nm as report_emp_kn_ft_nm";
	$sql .= " from empmst";
	$sql .= " ) re";

	if($sort_div == "0")
	{
		$sort = "asc";
	}
	else
	{
		$sort = "desc";
	}	
	$order = "order by report_id desc";
	switch($sort_item)
	{
		case "REPORT_NO":
			$order = "order by report_no $sort";
			break;
		case "REPORT_TITLE":
			$order = "order by report_title $sort, report_id desc";
			break;
		case "CLASS":
			$order = "order by registrant_class $sort, registrant_attribute $sort , registrant_dept $sort , registrant_room, report_id desc";
			break;
		case "REGIST_NAME":
			$order = "order by anonymous_report_flg $sort, report_emp_kn_lt_nm || report_emp_kn_ft_nm $sort, report_id desc";
			break;
		case "INCIDENT_DATE":
			$order = "order by input_item_100_5 $sort, report_id desc";
			break;
		default:
			//何もしない。
			break;
	}
	$sql .= " $order";

	$sel = select_from_table($con, $sql, "", $fname);
	if ($sel == 0){
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	$list_data_array = pg_fetch_all($sel);
}



//==============================
//ページングに関する情報
//==============================

//pate省略時は先頭ページを対象とする。
if($page == "")
{
	$page = 1;
}

//一画面内の最大表示件数
$disp_count_max_in_page = 20;

//表示開始/終了インデックス
$disp_start_index = ($page - 1)*$disp_count_max_in_page;
$disp_end_index   = $disp_start_index + $disp_count_max_in_page - 1;

//全データ件数
$rec_count = count($list_data_array);
if($rec_count == 1 && $list_data_array[0] == "")
{
	$rec_count = 0;
}

//最大ページ数
if($rec_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($rec_count-1) / $disp_count_max_in_page ) + 1;
}

//==============================
//ページングによる絞込み
//==============================

$list_data_array_work = array();
for($i=$disp_start_index;$i<=$disp_end_index;$i++)
{
	if($i >= $rec_count)
	{
		break;
	}
	$list_data_array_work[] = $list_data_array[$i];
}
$list_data_array = $list_data_array_work;

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());

//------------------------------
//ヘッダ用データ
//------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

$smarty->assign("hiyari_year", $hiyari_year);
$smarty->assign("hiyari_data_type", $hiyari_data_type);

$smarty->assign("list_tab_mode", $list_tab_mode);
$smarty->assign("page", $page);
$smarty->assign("sort_item", $sort_item);
$smarty->assign("sort_div", $sort_div);

//------------------------------
//年
//------------------------------
$year_list = array();
for($i=$this_year;$i>=$start_year;$i--){
    $year_list[] = $i;
}
$smarty->assign("year_list", $year_list);

//------------------------------
//フォルダ
//------------------------------
$smarty->assign("folder_id", $folder_id);
$smarty->assign("all_folder_info_count", $all_folder_info_count);

$folder_info = array();
foreach($all_folder_info as $all_info)
{
    $is_selected_this_folder = ($folder_id == $all_info["folder_id"]);

    $info = array();

    $info['folder_id'] = $all_info["folder_id"];
    $info['folder_name'] = h($all_info["folder_name"]);
    $info['report_count'] = $all_info["report_count"];
    $info['folder_style'] = get_folder_style($is_selected_this_folder);
    $info['folder_image'] = get_folder_image($is_selected_this_folder, $all_info["hiyari_data_type"]);
    $info['td_bgcolor'] = get_folder_td_bgcolor($is_selected_this_folder);

    $folder_info[] = $info;
}
$smarty->assign("folder_info", $folder_info);

//------------------------------
//一覧
//------------------------------
$profile_use = get_inci_profile($fname, $con);
$anonymous_name = get_anonymous_name($con,$fname);
$mouseover_popup_flg = is_anonymous_readable($session, $con, $fname);
$smarty->assign("is_mouseover_popup", $mouseover_popup_flg);

$list = array();
foreach($list_data_array as $list_data)
{
    $row = array();
    
	$row['report_id'] = $list_data["report_id"];
    $row['mail_id'] = $rep_obj->get_newest_mail_id($list_data["report_id"]);
	$row['report_no'] = $list_data["report_no"];
	$row['title'] = h($list_data["report_title"]);
	$row['incident_date'] =  $list_data["input_item_100_5"];

    $class_nm = "";
	if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != "")
	{
		$class_nm .= get_class_nm($con,$list_data["registrant_class"],$fname);
	}
	if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != "")
	{
		$class_nm .= get_atrb_nm($con,$list_data["registrant_attribute"],$fname);
	}
	if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != "")
	{
		$class_nm .= get_dept_nm($con,$list_data["registrant_dept"],$fname);
	}
	if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != "")
	{
		$class_nm .= get_room_nm($con,$list_data["registrant_room"],$fname);
	}
    $row['class_nm'] = $class_nm;

    //報告者
	$create_emp_name =  $list_data["registrant_name"];
    $row['real_name'] = h($create_emp_name);

    //表示名
    if($list_data["anonymous_report_flg"] == "t"){
        $row['is_anonymous_report'] = true;
        $disp_emp_name =  $anonymous_name;
    }
    else{
        $row['is_anonymous_report'] = false;
        $disp_emp_name =  $create_emp_name;
    }
    $row['disp_emp_name'] = h($disp_emp_name);

    $list[] = $row;
}
$smarty->assign("list", $list);

//------------------------------
//ページ
//------------------------------
$smarty->assign("page_max", $page_max);
$smarty->assign("page", $page);
if ($page_max > 1){
    $smarty->assign("page", $page);
    $page_list = array();
    $page_stt = max(1, $page - 3);
    $page_end = min($page_stt + 6, $page_max);
    $page_stt = max(1, $page_end - 6);
    for ($i=$page_stt; $i<=$page_end; $i++){
        $page_list[] = $i;
    }
    $smarty->assign("page_list", $page_list);
}

//------------------------------
//表示
//------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_hiyari_xml_list1.tpl");
}
else{
    $smarty->display("hiyari_hiyari_xml_list2.tpl");
}

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
function get_folder_style($is_selected)
{
    if($is_selected)
    {
    return 'style="color:#000000;font-weight:bold"';
    }
    else
    {
    return '';
    }
}
function get_folder_image($is_selected, $data_type)
{
    if($is_selected)
    {
    if($data_type == 'jiko') return 'img/icon/hiyari_lt_red_folder_open.gif';

    // 事故で無ければ普通の黄フォルダ
    return 'img/icon/hiyari_lt_folder_open.gif';
    }
    else
    {
    if($data_type == 'jiko') return 'img/icon/hiyari_lt_red_folder.gif';

    return 'img/icon/hiyari_lt_folder.gif';
    }
}
function get_folder_td_bgcolor($is_selected)
{
    if($is_selected)
    {
    return 'bgcolor="#FFDDFD"';
    }
    else
    {
    return '';
    }
}

/**
 * ヒヤリ・ハット分類フォルダを削除します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $folder_id 削除するフォルダID
 */
function delete_hiyari_folder($con,$fname,$folder_id)
{
	//フォルダ削除
	$sql  = "delete from inci_hiyari_xml_folder";
	$cond = "where folder_id = $folder_id";
	$result = delete_from_table($con, $sql, $cond, $fname);
	if ($result == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//レポートの所属情報削除
	$sql  = "delete from inci_hiyari_xml_folder_report";
	$cond = "where folder_id = $folder_id";
	$result = delete_from_table($con, $sql, $cond, $fname);
	if ($result == 0) {
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
