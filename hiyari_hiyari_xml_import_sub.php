<?php

// セッションの開始
session_start();

// ==================================================
// ファイルの読込み
// ==================================================
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("hiyari_common.ini");
require_once("hiyari_post_select_box.ini");
require_once("hiyari_report_class.php");
require_once("hiyari_wf_utils.php");

require_once("get_values.php");

// ==================================================
// 初期処理
// ==================================================

// 画面名
$fname = $PHP_SELF;

// ==================================================
// セッションのチェック
// qualify_session() from about_session.php
// ==================================================
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// 権限チェック
// check_authority() from about_authority.php
// ==================================================
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

// ==================================================
// DBコネクション取得
// connect2db() from about_postgres.php
// ==================================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ==================================================
// HTML出力 START
// ==================================================
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?= $INCIDENT_TITLE ?> | ヒヤリハット提出</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<link rel="stylesheet" type="text/css" href="css/incident.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.list_2 {border-collapse:collapse;}
.list_2 td {border:#999999 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}

</style>

<script language="javascript" type="text/javascript">
	function start_auto_session_update()
	{
	    //1分(60*1000ms)後に開始
	    setTimeout(auto_sesson_update, 60000);
	}
	
	function auto_sesson_update()
	{
	    //セッション更新
	    document.session_update_form.submit();
		
	    //1分(60*1000ms)後に最呼び出し
	    setTimeout(auto_sesson_update, 60000);
	}
	
	function select_target(index)
	{
		var element, cnt, i;
		
		cnt = document.import_sub_form.repetition_count.value;
		
		for (i = 0; i < cnt; i++) {
			var element = document.getElementById("repetition_tr_" + i);
			
			if (i == index) {
				element.style.backgroundColor = "#ffffaa";
			} else {
				element.style.backgroundColor = "#ffffff";
			}
		}
		
		document.import_sub_form.target_db_index.value = index;
		
		document.import_sub_form.action = "hiyari_hiyari_xml_import_sub_iframe.php?session=<?= $session ?>";
		document.import_sub_form.target = "iframe_compare";
		document.import_sub_form.submit();
	}
	
	function replace_data() {
		if (document.import_sub_form.target_db_index.value == "") {
			alert("重複データを選択してください");
			return false;
		}
		
		parent_win = window.opener;
		parent_win.document.import_form.mode.value = "duplication";
		parent_win.document.import_form.target_xml_index.value = document.import_sub_form.target_xml_index.value;
		parent_win.document.import_form.target_db_index.value = document.import_sub_form.target_db_index.value;
		parent_win.document.import_form.submit();
		
		window.close();
	}
	
</script>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->

<!-- セッションタイムアウト防止 START -->
<form name="session_update_form" action="hiyari_session_update.php" method="post" target="session_update_frame">
<input type="hidden" name="session" value="<?= $session ?>">
</form>
<iframe name="session_update_frame" width="0" height="0" frameborder="0"></iframe>
<!-- セッションタイムアウト防止 END -->

<!-- ヘッダー START -->
<?
// ==================================================
// ヘッダーの表示
// show_hiyari_header() from hiyari_common.ini
// ==================================================
show_hiyari_header_for_sub_window("重複データ置換対象選択");
?>
<!-- ヘッダー END -->

<?
$target_xml_index = $_GET["index"];

// 配列の初期化
$fix_arr = array();

// ==================================================
// 固定データ配列を取得
// get_fix_xml_data_array() from self
// ==================================================
$fix_arr = get_fix_xml_data_array();
?>

<form name="import_sub_form" method="post" action="hiyari_hiyari_xml_import_sub.php?session=<?= $session ?>">

<input type="hidden" name="mode" value="">
<input type="hidden" name="target_xml_index" value="<?= $target_xml_index ?>">
<input type="hidden" name="target_db_index" value="">
<input type="hidden" name="repetition_count" value="<?= count($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][$target_xml_index]) ?>">

<!-- 本体 START -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" style="padding-left: 10px; padding-right: 10px">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" valign="top">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
								<td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
							</tr>
							<tr>
								<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
								<td bgcolor="#F5FFE5">
									<table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
										<tr>
											<td>
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>XMLデータ内容</b></font>
												<table border="0" cellspacing="0" cellpadding="3" class="list">
													<tr height="25">
														<td align="center" bgcolor="#DFFFDC" width="60">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SEQ</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="90">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="90">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間帯</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="110">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="110">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font>
														</td>
													</tr>
													<?
													// XMLデータの表示：最後のインデックスは親画面で選択した行番号
													$repetition_report = $_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_REPORT"][$target_xml_index];
													?>
													<tr height="25">
														<td align="center" bgcolor="#FFFFFF" class="j12">
															<?= $repetition_report["_attr"]["SEQ"] ?>
														</td>
														<td align="center" bgcolor="#FFFFFF" class="j12">
															<?= $repetition_report["DATYEAR"][0]["_value"] . "年" . $repetition_report["DATMONTH"][0]["_attr"]["CODE"] . "月" ?>
														</td>
														<td align="center" bgcolor="#FFFFFF" class="j12">
															<?= $fix_arr["DATTIMEZONE"][(string)$repetition_report["DATTIMEZONE"][0]["_attr"]["CODE"]] ?>
														</td>
														<td align="center" bgcolor="#FFFFFF" class="j12">
															<?
															$flg = 0;
															foreach ($repetition_report["LSTSITE"][0]["DATSITE"] as $datsite) {
																if ($flg == 1) {
																	echo "、";
																}
																echo $fix_arr["DATSITE"][(string)$datsite["_attr"]["CODE"]];
																
																$flg = 1;
															}
															?>
														</td>
														<td align="center" bgcolor="#FFFFFF" class="j12">
															<?= $fix_arr["DATSUMMARY"][(string)$repetition_report["DATSUMMARY"][0]["_attr"]["CODE"]] ?>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
							</tr>
							<tr>
								<td><img src="img/r_3.gif" width="10" height="10"></td>
								<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td><img src="img/r_4.gif" width="10" height="10"></td>
							</tr>
						</table>
					</td>
					<td width="5"></td>
					<td valign="top">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
								<td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
							</tr>
							<tr>
								<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
								<td bgcolor="#F5FFE5">
									<table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
										<tr>
											<td>
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><b>重複データ内容</b></font>
												<table border="0" cellspacing="0" cellpadding="3" class="list">
													<tr height="25">
														<td align="center" bgcolor="#DFFFDC">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">比較</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="60">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SEQ</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="90">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生年月</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="90">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間帯</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="110">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">発生場所</font>
														</td>
														<td align="center" bgcolor="#DFFFDC" width="110">
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font>
														</td>
													</tr>
													<?
													// SQL生成
													$sql = "select";
														$sql .= " a.report_id";
														$sql .= ", a.seq";
														$sql .= ", (";
															$sql .= "select";
																$sql .= " input_item";
															$sql .= " from";
																$sql .= " inci_easyinput_data b";
															$sql .= " where";
																$sql .= " a.eid_id = b.eid_id";
																$sql .= " and b.grp_code = 100";
																$sql .= " and b.easy_item_code = 5";
														$sql .= ") as date";
														$sql .= ", (";
															$sql .= "select";
																$sql .= " input_item";
															$sql .= " from";
																$sql .= " inci_easyinput_data c";
															$sql .= " wher";
																$sql .= "e a.eid_id = c.eid_id";
																$sql .= " and c.grp_code = 100";
																$sql .= " and c.easy_item_code = 40";
														$sql .= ") as timezone";
														$sql .= ", (";
															$sql .= "select";
																$sql .= " input_item";
															$sql .= " from";
																$sql .= " inci_easyinput_data d";
															$sql .= " where";
																$sql .= " a.eid_id = d.eid_id";
																$sql .= " and d.grp_code = 110";
																$sql .= " and d.easy_item_code = 60";
														$sql .= ") as site";
														$sql .= ", (";
															$sql .= "select";
																$sql .= " input_item";
															$sql .= " from";
																$sql .= " inci_easyinput_data e";
															$sql .= " where";
																$sql .= " a.eid_id = e.eid_id";
																$sql .= " and e.grp_code = 900";
																$sql .= " and e.easy_item_code = 10";
														$sql .= ") as summary";
													$sql .= " from";
														$sql .= " inci_report a";
													
													$cond = " where";
														$cond .= " a.report_id in (";
													
													$flg = 0;
													
													// 重複データの表示：最後のインデックスは親画面で選択した行番号
													foreach ($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][$target_xml_index] as $id_arr) {
														$tmp_report_id = $id_arr["report_id"];
														
														if ($flg == 1) {
															$cond .= ", ";
														}
														
														$cond .= $tmp_report_id;
														
														$flg = 1;
													}
													
													$cond .= ")";
													
													//SQL実行
													
													// ==================================================
													// SQL実行
													// select_from_table() from about_postgres.php
													// ==================================================
													$sel = select_from_table($con, $sql, $cond, $fname);
													if ($sel == 0) {
														pg_query($con, "rollback");
														pg_close($con);
														showErrorPage();
														exit;
													}
													
													$result = pg_fetch_all($sel);
													
													$i = 0;
													
													// 重複データの表示：最後のインデックスは親画面で選択した行番号
													foreach ($_SESSION["HIYARI_HIYARI_XML_IMPORT"]["XML_REPETITION_ID_ARRAY"][$target_xml_index] as $id_arr) {
														$tmp_report_id = $id_arr["report_id"];
														
														foreach ($result as $row) {
															if ($tmp_report_id == $row["report_id"]) {
															?>
																<tr id="repetition_tr_<?= $i ?>" height="25" bgcolor="#ffffff">
																	<td align="center" class="j12" style="padding: 0px; padding-left: 2px; padding-right: 2px">
																		<input type="button" value="表に反映" onclick="select_target('<?= $i ?>')">
																	</td>
																	<td align="center" class="j12">
																		<?= $row["seq"] ?>
																	</td>
																	<td align="center" class="j12">
																		<?= substr($row["date"], 0, 4) . "年" . substr($row["date"], 5, 2) . "月" ?>
																	</td>
																	<td align="center" class="j12">
																		<?= $fix_arr["DATTIMEZONE"][substr("00" . (string)$row["timezone"], -2)] ?>
																	</td>
																	<td align="center" class="j12">
																		<?
																		// ==================================================
																		// DEFデータの取得
																		// get_def_array() from self
																		// ==================================================
																		$def_arr = get_def_array($con, $fname, 110, 60);
																		
																		$site_arr = explode("\t", $row["site"]);
																		
																		$flg = 0;
																		foreach ($site_arr as $site) {
																			if ($flg == 1) {
																				echo "、";
																			}
																			foreach ($def_arr as $def) {
																				// XMLデータとデータベースのDEFコードを比較
																				if ($site == $def["easy_code"]) {
																					echo $fix_arr["DATSITE"][(string)trim($def["def_code"])];
																					break;
																				}
																			}
																			
																			$flg = 1;
																		}
																		?>
																	</td>
																	<td align="center" class="j12">
																		<?= $fix_arr["DATSUMMARY"][convert_super_item_id_to_code($con, $fname, $row["summary"])] ?>
																	</td>
																</tr>
																<?
																$i++;
																
																break;
															}
														}
													}
													?>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
							</tr>
							<tr>
								<td><img src="img/r_3.gif" width="10" height="10"></td>
								<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
								<td><img src="img/r_4.gif" width="10" height="10"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td height="100%" colspan="3">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" class="list">
										<tr height="35">
											<td colspan="2" align="center" width="602" bgcolor="#DFFFDC" width="60">
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">XMLデータ</font>
											</td>
											<td colspan="2" align="center" width="600" bgcolor="#DFFFDC" width="60">
												<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">重複データ</font>
											</td>
										</tr>
									</table>
									<iframe src="hiyari_hiyari_xml_import_sub_iframe.php?session=<?= $session ?>&target_xml_index=<?= $target_xml_index ?>" width="1220" height="500" name="iframe_compare" frameborder="0" style="border: solid 1px #bbbbbb">
									</iframe>
								</td>
							</tr>
							<tr>
								<td align="right">
									<img src="img/spacer.gif" width="1" height="5" alt=""><br>
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
											<td background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
											<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
										</tr>
										<tr>
											<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
											<td bgcolor="#F5FFE5">
												<table border="0" cellspaing="0" cellpadding="0" style="border-collapse: collapse; border: #ffffff">
													<tr>
														<td>
															<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
																XMLデータ(表：左)で、現在選択中の重複データ(表：右)を置換します
																<input type="button" value="実行" style="width: 80px" onclick="replace_data()">
															</font>
														</td>
													</tr>
												</table>
											</td>
											<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
										</tr>
										<tr>
											<td><img src="img/r_3.gif" width="10" height="10"></td>
											<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
											<td><img src="img/r_4.gif" width="10" height="10"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

<!-- 本体 END -->

<?
// ==================================================
// フッターの表示
// show_hiyari_footer() from hiyari_common.ini
// ==================================================
show_hiyari_footer($session, $fname, false);
?>

<!-- BODY全体 END -->

</form>

</body>

<?
// DB接続の切断
pg_close($con);
?>

</html>

<?
// ==================================================
// HTML出力 END
// ==================================================

/**
 * XMLの固定内容データを連想配列に格納
 *
 * @return array 固定内容データ配列
 */
function get_fix_xml_data_array()
{
	$ret = array();
	
	$ret = array(
		"DATMONTH" => array(
			"01" => "１月"
			, "02" => "２月"
			, "03" => "３月"
			, "04" => "４月"
			, "05" => "５月"
			, "06" => "６月"
			, "07" => "７月"
			, "08" => "８月"
			, "09" => "９月"
			, "10" => "１０月"
			, "11" => "１１月"
			, "12" => "１２月"
		)
		, "DATDAY" => array(
			"01" => "日曜日"
			, "02" => "月曜日"
			, "03" => "火曜日"
			, "04" => "水曜日"
			, "05" => "木曜日"
			, "06" => "金曜日"
			, "07" => "土曜日"
		)
		, "DATHOLIDAY" => array(
			"01" => "平日"
			, "02" => "休日・祝日"
		)
		, "DATTIMEZONE" => array(
			"01" => "0:00〜1:59"
			, "02" => "2:00〜3:59"
			, "03" => "4:00〜5:59"
			, "04" => "6:00〜7:59"
			, "05" => "8:00〜9:59"
			, "06" => "10:00〜11:59"
			, "07" => "12:00〜13:59"
			, "08" => "14:00〜15:59"
			, "09" => "16:00〜17:59"
			, "10" => "18:00〜19:59"
			, "11" => "20:00〜21:59"
			, "12" => "22:00〜23:59"
			, "90" => "不明"
		)
		, "DATEXECUTION" => array(
			"01" => "実施あり"
			, "02" => "実施なし"
		)
		, "DATLEVEL" => array(
			"01" => "濃厚な治療"
			, "02" => "軽微な治療"
			, "03" => "治療なし"
			, "90" => "不明"
		)
		, "DATSEQUELAE" => array(
			"01" => "死亡"
			, "02" => "障害残存の可能性がある(高い)"
			, "03" => "障害残存の可能性がある(低い)"
			, "04" => "障害残存の可能性なし"
			, "05" => "障害なし"
			, "90" => "不明"
		)
		, "DATINFLUENCE" => array(
			"01" => "死亡もしくは重篤な状況に至ったと考えられる"
			, "02" => "濃厚な処置・治療が必要であると考えられる"
			, "03" => "軽微な処置・治療が必要もしくは処置・治療が不要と考えられる"
		)
		, "DATSITE" => array(
			"01" => "外来診察室"
			, "02" => "外来処置室"
			, "03" => "外来待合室"
			, "04" => "救急外来"
			, "05" => "救命救急センター"
			, "06" => "病室"
			, "07" => "病棟処置室"
			, "08" => "手術室"
			, "09" => "ICU"
			, "10" => "CCU"
			, "11" => "NICU"
			, "12" => "検査室"
			, "13" => "カテーテル検査室"
			, "14" => "放射線治療室"
			, "15" => "放射線撮影室"
			, "16" => "核医学検査室"
			, "17" => "透析室"
			, "18" => "分娩室"
			, "19" => "機能訓練室"
			, "20" => "トイレ"
			, "21" => "廊下"
			, "22" => "浴室"
			, "23" => "階段"
			, "24" => "薬局(調剤所)"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATSUMMARY" => array(
			"02" => "薬剤"
			, "03" => "輸血"
			, "04" => "治療・処置"
			, "10" => "医療機器等"
			, "06" => "ドレーン・チューブ"
			, "08" => "検査"
			, "09" => "療養上の世話"
			, "99" => "その他"
		)
		, "DATSPECIALLY" => array(
			"090105" => "汚染された薬剤・材料・生体由来材料等の使用による事故"
			, "090107" => "院内感染による死亡や障害"
			, "090201" => "患者の自殺又は自殺企図"
			, "090202" => "入院患者の失踪"
			, "090204" => "患者の熱傷"
			, "090205" => "患者の感電"
			, "090206" => "医療施設内の火災による患者の死亡や障害"
			, "090207" => "間違った保護者の許への新生児の引渡し"
			, "999999" => "本事例は選択肢には該当しない"
		)
		, "DATDEPARTMENT" => array(
			"01" => "内科"
			, "02" => "麻酔科"
			, "03" => "循環器内科"
			, "04" => "神経科"
			, "05" => "呼吸器内科"
			, "06" => "消化器科"
			, "07" => "血液内科"
			, "08" => "循環器外科"
			, "09" => "アレルギー科"
			, "10" => "リウマチ科"
			, "11" => "小児科"
			, "12" => "外科"
			, "13" => "整形外科"
			, "14" => "形成外科"
			, "15" => "美容外科"
			, "16" => "脳神経外科"
			, "17" => "呼吸器外科"
			, "18" => "心臓血管外科"
			, "19" => "小児外科"
			, "20" => "ペインクリニック"
			, "21" => "皮膚科"
			, "22" => "泌尿器科"
			, "23" => "性病科"
			, "24" => "肛門科"
			, "25" => "産婦人科"
			, "26" => "産科"
			, "27" => "婦人科"
			, "28" => "眼科"
			, "29" => "耳鼻咽喉科"
			, "30" => "心療内科"
			, "31" => "精神科"
			, "32" => "リハビリテーション科"
			, "33" => "放射線科"
			, "34" => "歯科"
			, "35" => "矯正歯科"
			, "36" => "小児歯科"
			, "37" => "歯科口腔外科"
			, "90" => "不明"
			, "99" => "その他"
		)
		, "DATPATIENT" => array(
			"1" => "１人"
			, "2" => "複数人"
		)
		, "DATPATIENTAGE" => array(
			"years" => "歳"
			, "months" => "ヶ月"
		)
		, "DATPATIENTSEX" => array(
			"01" => "男性"
			, "02" => "女性"
		)
		, "DATPATIENTDIV" => array(
			"01" => "入院"
			, "04" => "外来"
		)
		, "DATDISEASE" => array(
			"disease0" => "事故(事例)に直接関連する疾患名"
			, "disease1" => "関連する疾患名１"
			, "disease2" => "関連する疾患名２"
			, "disease3" => "関連する疾患名３"
		)
		, "DATPATIENTSTATE" => array(
			"01" => "意識障害"
			, "02" => "視覚障害"
			, "03" => "聴覚障害"
			, "04" => "構音障害"
			, "05" => "精神障害"
			, "06" => "認知症・健忘"
			, "07" => "上肢障害"
			, "08" => "下肢障害"
			, "09" => "歩行障害"
			, "10" => "床上安静"
			, "11" => "睡眠中"
			, "13" => "薬剤の影響下"
			, "14" => "麻酔中・麻酔前後"
			, "99" => "その他特記する心身状態あり"
		)
		, "DATDISCOVERER" => array(
			"01" => "当事者本人"
			, "02" => "同職種者"
			, "03" => "他職種者"
			, "04" => "患者本人"
			, "05" => "家族・付き添い"
			, "06" => "他患者"
			, "99" => "その他"
		)
		, "LSTCONCERNED" => array(
			"01" => "１人"
			, "02" => "２人"
			, "03" => "３人"
			, "04" => "４人"
			, "05" => "５人"
			, "06" => "６人"
			, "07" => "７人"
			, "08" => "８人"
			, "09" => "９人"
			, "10" => "１０人"
			, "90" => "１１人以上"
		)
		, "DATCONCERNED" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATEXPERIENCE" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATLENGTH" => array(
			"years" => "年"
			, "months" => "ヶ月"
		)
		, "DATDUTY" => array(
			"00" => "０回"
			, "01" => "１回"
			, "02" => "２回"
			, "03" => "３回"
			, "04" => "４回"
			, "05" => "５回"
			, "06" => "６回"
			, "07" => "７回"
			, "90" => "不明"
		)
		, "DATSHIFT" => array(
			"02" => "２交替"
			, "03" => "３交替"
			, "04" => "４交替"
			, "08" => "交替勤務なし"
			, "99" => "その他"
		)
		, "DATRELCATEGORY" => array(
			"01" => "医師"
			, "02" => "歯科医師"
			, "03" => "看護師"
			, "04" => "准看護師"
			, "05" => "薬剤師"
			, "06" => "臨床工学技士"
			, "07" => "助産師"
			, "08" => "看護助手"
			, "09" => "診療放射線技師"
			, "10" => "臨床検査技師"
			, "12" => "管理栄養士"
			, "13" => "栄養士"
			, "14" => "調理師・調理従事者"
			, "11" => "理学療法士(PT)"
			, "16" => "作業療法士(OT)"
			, "15" => "言語聴覚士(ST)"
			, "17" => "衛生検査技師"
			, "18" => "歯科衛生士"
			, "19" => "歯科技工士"
			, "99" => "その他"
		)
		, "DATTYPE" => array(
			// 薬剤
			"02" => array(
				"020401" => "血液製剤"
				, "020402" => "麻薬"
				, "020403" => "抗腫瘍剤"
				, "020404" => "循環器用薬"
				, "020405" => "抗糖尿病薬"
				, "020406" => "抗不安剤"
				, "020407" => "睡眠導入剤"
				, "020408" => "抗凝固剤"
				, "020499" => "その他"
			)
			// 治療・処置
			, "04" => array(
				"040101" => "開頭"
				, "040102" => "開胸"
				, "040103" => "開心"
				, "040104" => "開腹"
				, "040105" => "四肢"
				, "040106" => "鏡視下手術"
				, "040199" => "その他の手術"
				, "040201" => "全身麻酔(吸入麻酔+静脈麻酔)"
				, "040202" => "局所麻酔"
				, "040203" => "吸引麻酔"
				, "040204" => "静脈麻酔"
				, "040205" => "脊椎・硬膜外麻酔"
				, "040299" => "その他の麻酔"
				, "040301" => "経膣分娩"
				, "040302" => "帝王切開"
				, "040303" => "人工妊娠中絶"
				, "040399" => "その他の分娩・人工妊娠中絶等"
				, "040401" => "血液浄化療法(血液透析含む)"
				, "040402" => "IVR(血液カテーテル治療等)"
				, "040403" => "放射線治療"
				, "040404" => "ペインクリニック"
				, "040405" => "リハビリテーション"
				, "040406" => "観血的歯科治療"
				, "040407" => "非観血的歯科治療"
				, "040408" => "内視鏡的治療"
				, "040499" => "その他の治療"
				, "040501" => "中心静脈ライン"
				, "040502" => "末梢静脈ライン"
				, "040503" => "動脈ライン"
				, "040504" => "血液浄化用カテーテル"
				, "040505" => "栄養チューブ(NG・ED)"
				, "040506" => "尿道カテーテル"
				, "040509" => "ドレーンに関する処置"
				, "040508" => "創傷処置"
				, "040599" => "その他の一般的処置"
				, "040601" => "気管挿管"
				, "040602" => "気管切開"
				, "040603" => "心臓マッサージ"
				, "040604" => "酸素療法"
				, "040605" => "血管確保"
				, "040699" => "その他の救急措置"
			)
			// 医療機器等
			, "10" => array(
				"050101" => "人工呼吸器"
				, "050102" => "酸素療法機器"
				, "050103" => "麻酔器"
				, "050104" => "人工心肺"
				, "050105" => "除細動器"
				, "050106" => "IABP"
				, "050107" => "ペースメーカー"
				, "050108" => "輸血・輸注ポンプ"
				, "050109" => "血液浄化用機器"
				, "050110" => "保育器"
				, "050111" => "内視鏡"
				, "050112" => "低圧持続吸引器"
				, "050113" => "心電図・血液モニター"
				, "050114" => "パルスオキシメーター"
				, "050115" => "高気圧酸素療法装置"
				, "050116" => "歯科用回転研削器具"
				, "050117" => "歯科用根管治療用器具"
				, "050118" => "歯科補綴物・充填物"
				, "050199" => "その他の医療機器等"
			)
			// ドレーン・チューブ
			, "06" => array(
				"060101" => "中心静脈ライン"
				, "060102" => "末梢静脈ライン"
				, "060103" => "動脈ライン"
				, "060104" => "気管チューブ"
				, "060105" => "気管カニューレ"
				, "060106" => "栄養チューブ(NG・ED)"
				, "060107" => "尿道カテーテル"
				, "060108" => "胸腔ドレーン"
				, "060109" => "腹腔ドレーン"
				, "060110" => "脳室・脳槽ドレーン"
				, "060111" => "皮下持続吸引ドレーン"
				, "060112" => "硬膜外カテーテル"
				, "060113" => "血液浄化用カテーテル・回路"
				, "060114" => "三方活栓"
				, "060199" => "その他のドレーン・チューブ類"
			)
			// 検査
			, "08" => array(
				"080101" => "採血"
				, "080102" => "採尿"
				, "080103" => "採便"
				, "080104" => "採痰"
				, "080105" => "穿刺液"
				, "080199" => "その他の検体採取"
				, "080201" => "超音波検査"
				, "080202" => "心電図検査"
				, "080203" => "トレッドミル検査"
				, "080204" => "ホルター負荷心電図"
				, "080205" => "脳波検査"
				, "080206" => "筋電図検査"
				, "080207" => "肺機能検査"
				, "080299" => "その他の生理検査"
				, "080301" => "一般撮影"
				, "080302" => "ポータブル撮影"
				, "080303" => "CT"
				, "080304" => "MRI"
				, "080305" => "血管カテーテル撮影"
				, "080306" => "上部消化管撮影"
				, "080307" => "下部消化管撮影"
				, "080399" => "その他の画像検査"
				, "080401" => "上部消化管"
				, "080402" => "下部消化管"
				, "080403" => "気管支鏡"
				, "080499" => "その他の内視鏡検査"
				, "080501" => "耳鼻科検査"
				, "080502" => "眼科検査"
				, "080503" => "歯科検査"
				, "080505" => "検体検査"
				, "080506" => "血糖測定(病棟で実施したもの)"
				, "080507" => "病理検査"
				, "080508" => "核医学検査"
				, "080599" => "その他の機能検査"
			)
			// 療養上の世話
			, "09" => array(
				"090101" => "気管内・口腔内吸引"
				, "090102" => "体位交換"
				, "090103" => "清拭"
				, "090104" => "更衣介助"
				, "090106" => "入浴介助"
				, "090107" => "排泄介助"
				, "090109" => "移動介助"
				, "090110" => "搬送・移送"
				, "090115" => "罨法"
				, "090114" => "患者観察"
				, "090112" => "患者周辺物品管理"
				, "090111" => "体温管理"
				, "090113" => "配膳"
				, "090199" => "その他の療養上の世話"
				, "090201" => "経口摂取"
				, "090202" => "経管栄養"
				, "090203" => "食事介助"
				, "090301" => "散歩中"
				, "090302" => "移動中"
				, "090303" => "外出・外泊中"
				, "090304" => "食事中"
				, "090305" => "入浴中"
				, "090306" => "着替え中"
				, "090307" => "排泄中"
				, "090308" => "就寝中"
				, "090399" => "その他の療養上の場面"
			)
		)
		, "DATSCENE" => array(
			// 薬剤
			"02" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "020401" => "内服薬調剤"
				, "020402" => "注射薬調剤"
				, "020403" => "血液製剤調剤"
				, "020404" => "外用薬調剤"
				, "020499" => "その他の調剤に関する場面"
				, "020301" => "内服薬製剤管理"
				, "020302" => "注射薬製剤管理"
				, "020303" => "血液製剤管理"
				, "020304" => "外用薬製剤管理"
				, "020399" => "その他の製剤管理に関する場面"
				, "020101" => "与薬準備"
				, "020201" => "皮下・筋肉注射"
				, "020202" => "静脈注射"
				, "020203" => "動脈注射"
				, "020204" => "抹消静脈点滴"
				, "020205" => "中心静脈注射"
				, "020206" => "内服"
				, "020207" => "外用"
				, "020208" => "坐剤"
				, "020209" => "吸入"
				, "020210" => "点鼻・点耳・点眼"
				, "020299" => "その他の与薬に関する場面"
			)
			// 輸血
			, "03" => array(
				"010201" => "手書きによる処方箋の作成"
				, "010202" => "オーダリングによる処方箋の作成"
				, "010203" => "口頭による処方指示"
				, "010204" => "手書きによる処方の変更"
				, "010205" => "オーダリングによる処方の変更"
				, "010206" => "口頭による処方の変更"
				, "010299" => "その他の処方に関する場面"
				, "030101" => "準備"
				, "030102" => "実施"
				, "030199" => "その他の輸血検査に関する場面"
				, "030201" => "準備"
				, "030202" => "実施"
				, "030299" => "その他の放射線照射に関する場面"
				, "030301" => "製剤の交付"
				, "030399" => "その他の輸血準備に関する場面"
				, "030401" => "実施"
				, "030499" => "その他の輸血実施に関する場面"
			)
			// 治療・処置
			, "04" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040199" => "その他の管理に関する場面"
				, "040201" => "準備"
				, "040299" => "その他の準備に関する場面"
				, "040301" => "実施"
				, "040399" => "その他の治療・処置に関する場面"
			)
			// 医療機器等
			, "10" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 検査
			, "08" => array(
				"010102" => "手書きによる指示の作成"
				, "010103" => "オーダリングによる指示の作成"
				, "010104" => "口頭による指示"
				, "010105" => "手書きによる指示の変更"
				, "010106" => "オーダリングによる指示の変更"
				, "010107" => "口頭による指示の変更"
				, "010199" => "その他の指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "使用中"
			)
			// 療養上の世話
			, "09" => array(
				"010301" => "手書きによる計画又は指示の作成"
				, "010302" => "オーダリングによる計画又は指示の作成"
				, "010303" => "口頭による計画又は指示"
				, "010304" => "手書きによる計画又は指示の変更"
				, "010305" => "オーダリングによる計画又は指示の変更"
				, "010306" => "口頭による計画又は指示の変更"
				, "010399" => "その他の計画又は指示に関する場面"
				, "040101" => "管理"
				, "040201" => "準備"
				, "040301" => "実施中"
			)
		)
		, "DATDETAIL" => array(
			// 薬剤
			"02" => array(
				"010201" => "処方忘れ"
				, "010202" => "処方遅延"
				, "010203" => "処方量間違い"
				, "010204" => "重複処方"
				, "010205" => "禁忌薬剤の処方"
				, "010206" => "対象患者処方間違い"
				, "010207" => "処方薬剤間違い"
				, "010208" => "処方単位間違い"
				, "010209" => "投与方法処方間違い"
				, "010299" => "その他の処方に関する内容"
				
				, "020212" => "調剤忘れ"
				, "020201" => "処方箋・注射箋鑑査間違い"
				, "020202" => "秤量間違い調剤"
				, "020203" => "数量間違い"
				, "020204" => "分包間違い"
				, "020205" => "規格間違い調剤"
				, "020206" => "単位間違い調剤"
				, "020207" => "薬剤取り違え調剤"
				, "020208" => "説明文書の取り違え"
				, "020209" => "交付患者間違い"
				, "020210" => "薬剤・製剤の取り違え交付"
				, "020211" => "期限切れ製剤の交付"
				, "020299" => "その他の調剤に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の製剤管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020117" => "混合間違い"
				, "020199" => "その他の与薬準備に関する内容"
				
				, "020414" => "過剰投与"
				, "020415" => "過少投与"
				, "020404" => "投与時間・日付間違い"
				, "020405" => "重複投与"
				, "020416" => "禁忌薬剤の投与"
				, "020407" => "投与速度速すぎ"
				, "020408" => "投与速度遅すぎ"
				, "020409" => "患者間違い"
				, "020410" => "薬剤間違い"
				, "020411" => "単位間違い"
				, "020412" => "投与方法間違い"
				, "020413" => "無投薬"
				, "020499" => "その他の与薬に関する内容"
			)
			// 輸血
			, "03" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010110" => "指示量間違い"
				, "010111" => "重複指示"
				, "010112" => "禁忌薬剤の指示"
				, "010113" => "対象患者指示間違い"
				, "010114" => "指示薬剤間違い"
				, "010115" => "指示単位間違い"
				, "010116" => "投与方法指示間違い"
				, "010199" => "その他の指示出しに関する内容"
				
				, "030101" => "未実施"
				, "030102" => "検体取り違え"
				, "030105" => "判定間違い"
				, "030104" => "結果記入・入力間違い"
				, "030199" => "その他の輸血検査に関する内容"
				
				, "030201" => "未実施"
				, "030202" => "過剰照射"
				, "030205" => "過少照射"
				, "030203" => "患者間違い"
				, "030204" => "製剤間違い"
				, "030299" => "その他の放射線照射に関する内容"
				
				, "020302" => "薬袋・ボトルの記載間違い"
				, "020305" => "異物混入"
				, "020306" => "細菌汚染"
				, "020307" => "期限切れ製剤"
				, "020399" => "その他の輸血管理に関する内容"
				
				, "020114" => "過剰与薬準備"
				, "020115" => "過少与薬準備"
				, "020104" => "与薬時間・日付間違い"
				, "020105" => "重複与薬"
				, "020116" => "禁忌薬剤の与薬"
				, "020107" => "投与速度速すぎ"
				, "020108" => "投与速度遅すぎ"
				, "020109" => "患者間違い"
				, "020110" => "薬剤間違い"
				, "020111" => "単位間違い"
				, "020112" => "投与方法間違い"
				, "020113" => "無投薬"
				, "020199" => "その他の輸血準備に関する内容"
				
				, "020214" => "過剰投与"
				, "020215" => "過少投与"
				, "020204" => "投与時間・日付間違い"
				, "020205" => "重複投与"
				, "020216" => "禁忌薬剤の投与"
				, "020207" => "投与速度速すぎ"
				, "020208" => "投与速度遅すぎ"
				, "020209" => "患者間違い"
				, "020210" => "薬剤間違い"
				, "020211" => "単位間違い"
				, "020212" => "投与方法間違い"
				, "020213" => "無投薬"
				, "020299" => "その他の輸血実施に関する内容"
			)
			// 治療・処置
			, "04" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010116" => "治療・処置指示間違い"
				, "010117" => "日程間違い"
				, "010118" => "時間間違い"
				, "010199" => "その他の治療・処置の指示に関する内容"
				
				, "040301" => "治療・処置の管理"
				, "040399" => "その他の治療・処置の管理に関する内容"
				
				, "040201" => "医療材料取り違え"
				, "040202" => "患者体位の誤り"
				, "040203" => "消毒・清潔操作の誤り"
				, "040299" => "その他の治療・処置の準備に関する内容"
				
				, "040101" => "患者間違い"
				, "040102" => "部位取違え"
				, "040105" => "方法(手技)の誤り"
				, "040106" => "未実施・忘れ"
				, "040107" => "中止・延期"
				, "040108" => "日程・時間の誤り"
				, "040109" => "順番の誤り"
				, "040110" => "不必要行為の実施"
				, "040114" => "誤嚥"
				, "040115" => "誤飲"
				, "040116" => "異物の体内残存"
				, "040104" => "診察・治療・処置等その他の取違え"
				, "040199" => "その他の治療・処置の実施に関する内容"
			)
			// 医療機器等
			, "10" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他の医療機器等・医療材料の使用に関する内容"
				
				, "050301" => "保守・点検不良"
				, "050302" => "保守・点検忘れ"
				, "050303" => "使用中の点検・管理ミス"
				, "050304" => "破損"
				, "050399" => "その他の医療機器等・医療材料の管理に関する内容"
				
				, "050201" => "組み立て"
				, "050202" => "設定条件間違い"
				, "050203" => "設定忘れ"
				, "050204" => "電源入れ忘れ"
				, "050205" => "警報設定忘れ"
				, "050206" => "警報設定範囲間違い"
				, "050207" => "便宜上の警報解除後の再設定忘れ"
				, "050208" => "消毒・清潔操作の誤り"
				, "050209" => "使用前の点検・管理ミス"
				, "050210" => "破損"
				, "050299" => "その他の医療機器等・医療材料の準備に関する内容"
				
				, "050105" => "医療機器等・医療材料の不適切使用"
				, "050104" => "誤作動"
				, "050106" => "故障"
				, "050116" => "破損"
				, "050199" => "その他の医療機器等・医療材料の使用に関する内容"
			)
			// ドレーン・チューブ
			, "06" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "使用方法指示間違い"
				, "010199" => "その他のドレーン・チューブ類の使用・管理の指示に関する内容"
				
				, "060301" => "点検忘れ"
				, "060302" => "点検不良"
				, "060303" => "使用中の点検・管理ミス"
				, "060304" => "破損"
				, "060399" => "その他のドレーン・チューブ類の管理に関する内容"
				
				, "060201" => "組み立て"
				, "060202" => "設定条件間違い"
				, "060203" => "設定忘れ"
				, "060204" => "消毒・清潔操作の誤り"
				, "060205" => "使用前の点検・管理ミス"
				, "060299" => "その他のドレーン・チュ−ブ類の準備に関する内容"
				
				, "060101" => "点滴漏れ"
				, "060102" => "自己抜去"
				, "060103" => "自然抜去"
				, "060104" => "接続はずれ"
				, "060105" => "未接続"
				, "060106" => "閉塞"
				, "060107" => "切断・破損"
				, "060108" => "接続間違い"
				, "060109" => "三方活栓操作間違い"
				, "060110" => "ルートクランプエラー"
				, "060111" => "空気混入"
				, "060112" => "誤作動"
				, "060113" => "故障"
				, "060114" => "ドレーン・チューブ類の不適切使用"
				, "060199" => "その他のドレーン・チューブ類の使用に関する内容"
			)
			// 検査
			, "08" => array(
				"010101" => "指示出し忘れ"
				, "010102" => "指示遅延"
				, "010113" => "対象患者指示間違い"
				, "010119" => "指示検査の間違い"
				, "010199" => "その他の検査の指示に関する内容"
				
				, "070301" => "分析機器・器具管理"
				, "070302" => "試薬管理"
				, "070303" => "データ紛失"
				, "070304" => "計算・入力・暗記"
				, "070399" => "その他の検査の管理に関する内容"
				
				, "070201" => "患者取違え"
				, "070204" => "検体取違え"
				, "070205" => "検体紛失"
				, "070210" => "検査機器・器具の準備"
				, "070206" => "検体破損"
				, "070299" => "その他の検査の準備に関する内容"
				
				, "070101" => "患者取違え"
				, "070104" => "検体取違え"
				, "070115" => "試薬の間違い"
				, "070105" => "検体紛失"
				, "070102" => "検査の手技・判定技術の間違い"
				, "070103" => "検体採取時のミス"
				, "070106" => "検体破損"
				, "070107" => "検体のコンタミネーション"
				, "070111" => "データ取違え"
				, "070114" => "結果報告"
				, "070199" => "その他の検査の実施に関する内容"
			)
			// 療養上の世話
			, "09" => array(
				"010101" => "計画忘れ又は指示出し忘れ"
				, "010102" => "計画又は指示の遅延"
				, "010111" => "計画又は指示の対象患者間違い"
				, "010119" => "計画又は指示内容間違い"
				, "010199" => "その他の療養上の世話の計画又は指示に関する内容"
				
				, "080104" => "拘束・抑制"
				, "080505" => "給食の内容の間違い"
				, "080109" => "安静指示"
				, "080110" => "禁食指示"
				, "080306" => "外出・外泊許可"
				, "080405" => "異物混入"
				, "080101" => "転倒"
				, "080102" => "転落"
				, "080103" => "衝突"
				, "080106" => "誤嚥"
				, "080107" => "誤飲"
				, "080108" => "誤配膳"
				, "080202" => "遅延"
				, "080203" => "実施忘れ"
				, "080204" => "搬送先間違い"
				, "080205" => "患者間違い"
				, "080404" => "延食忘れ"
				, "080403" => "中止の忘れ"
				, "080301" => "自己管理薬飲み忘れ・注射忘れ"
				, "080305" => "自己管理薬注入忘れ"
				, "080303" => "自己管理薬取違え摂取"
				, "080406" => "不必要行為の実施"
				, "089999" => "その他の療養上の世話の管理・準備・実施に関する内容"
			)
		)
		, "DATFACTOR" => array(
			"010101" => "確認を怠った"
			, "010102" => "観察を怠った"
			, "010103" => "報告が遅れた(怠った)"
			, "010104" => "記録などに不備があった"
			, "010105" => "連携ができていなかった"
			, "010106" => "患者への説明が不十分であった(怠った)"
			, "010107" => "判断を誤った"
			, "010201" => "知識が不足していた"
			, "010202" => "技術・手技が未熟だった"
			, "010203" => "勤務状況が繁忙だった"
			, "010204" => "通常とは異なる身体的条件下にあった"
			, "010205" => "通常とは異なる心理的条件下にあった"
			, "010299" => "その他"
			, "010301" => "コンピュータシステム"
			, "010302" => "医薬品"
			, "010303" => "医療機器"
			, "010304" => "施設・設備"
			, "010305" => "諸物品"
			, "010306" => "患者側"
			, "010399" => "その他"
			, "010401" => "教育・訓練"
			, "010402" => "仕組み"
			, "010403" => "ルールの不備"
			, "010499" => "その他"
		)
		, "DATCOMMITTEE" => array(
			"05" => "既設の医療安全に関する委員会等で対応"
			, "07" => "内部調査委員会設置(予定も含む)"
			, "08" => "外部調査委員会設置(予定も含む)"
			, "06" => "現在顕著宇宙で対応は未定"
			, "99" => "その他"
		)
	);
	
	return $ret;
}

/**
 * DEFデータの取得
 * 
 * @param  onject $con            接続コネクション
 * @param  string $fname          画面名
 * @param  int    $grp_code       グループコード
 * @param  int    $easy_item_code 項目コード
 * @return array                  DEFデータ配列
 */
function get_def_array($con, $fname, $grp_code, $easy_item_code)
{
	// データ取得
	$sql = "select";
		$sql .= " easy_code";
		$sql .= ", def_code";
	$sql .= " from";
		$sql .= " inci_report_materials";
	$cond = " where";
		$cond .= " grp_code = " . $grp_code;
		$cond .= " and easy_item_code = " . $easy_item_code;
	$cond .= " order by";
		$cond .= " def_code";
		$cond .= ", easy_code";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	return pg_fetch_all($sel);
}

/**
 * CODEデータの取得(概要)
 * 
 * @param  onject $con     接続コネクション
 * @param  string $fname   画面名
 * @param  string $db_code データベースコード
 * @return string          CODE
 */
function convert_super_item_id_to_code($con, $fname, $db_code)
{
	// db_codeが指定されない場合は空文字を返す
	if ($db_code == "") {
		return "";
	}
	
	// 職種データ取得
	$sql = "select";
		$sql .= " code";
	$sql .= " from";
		$sql .= " inci_super_item_2010";
	
	$cond = " where";
		$cond .= " super_item_id = '" . $db_code . "'";
	
	// ==================================================
	// SQL実行
	// select_from_table() from about_postgres.php
	// ==================================================
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	$row = pg_fetch_row($sel);
	
	return trim($row[0]);
}

?>