<?
chdir(dirname(__FILE__));

ini_set("max_execution_time", 0);

require_once("about_comedix.php");
require_once("webmail_quota_functions.php");

$fname = 'employee_add_mail.php';

// データベースに接続
$con = connect2db($fname);

// 職員一覧を取得
$sql = "select get_mail_login_id(emp_id) as mail_id, emp_login_pass from login";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	js_error_exit();
}

// 職員データを配列化
$employees = array();
while ($row = pg_fetch_array($sel)) {
	$employees[] = array(
		"mail_id" => $row["mail_id"],
		"pass" => $row["emp_login_pass"]
	);
}

// データベース処理を閉じる
pg_close($con);

// メールアカウントの作成
require_once("webmail/config/config.php");
$count = count($employees);
$dir = dirname(__FILE__);
echo("------------------------------\n");
for ($i = 0; $i < $count; $i++) {
	$num = $i + 1;
	echo("creating $num/$count ({$employees[$i]["mail_id"]})\n");
	make_mail_account($employees[$i], $dir, $imapServerAddress);
}

// 成功画面を表示
echo("done.\n");

// メールアカウントを作成
function make_mail_account($employee_data, $dir, $imapServerAddress) {
	$account = $employee_data["mail_id"];
	$pass = $employee_data["pass"];

	if (preg_match("/[a-z]/", $account) > 0) {
		if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
			exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $pass 2>&1 1> /dev/null", $output, $exit_status);
			if ($exit_status !== 0) {
				exec("sudo -u postgres /usr/bin/expect -f $dir/expect/addmailbox.exp $account $pass");
			}
		} else {
			exec("sudo -u postgres /usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $pass $imapServerAddress $dir");
		}
	}

	webmail_quota_change_to_default($account);
}
