<?php
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("about_comedix.php");
require_once("news_common.ini");
require_once("referer_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "news", $fname);

// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);

// 初期カテゴリを取得
$default_category = get_default_category($con, $fname);

// カテゴリ一覧を取得
$category_list = get_category_list_from_db($con, $fname);

// 「確認者を記録する」の初期値を取得
$record_flg = get_default_record_flg($con, $fname);

// 一覧表示の件数を取得
$rows_per_page = get_rows_per_page($con, $fname);

// 期間指定の初期値を取得
$default_term = get_default_term($con, $fname);

// マイページの未読・既読選択を取得
$mypage_read_flg = get_mypage_read_flg($con, $fname);

// お知らせ表示条件の初期値を取得
$default_read_status = get_default_read_status($con, $fname);

// 職員IDのカラム表示条件の初期値を取得
$default_emp_id_chk = get_emp_id_read_status($con, $fname);

// 職員組織のカラム表示条件の初期値を取得
$default_emp_class_chk = get_emp_class_read_status($con, $fname);

// 職員職種のカラム表示条件の初期値を取得
$default_emp_job_chk = get_emp_job_read_status($con, $fname);

// 職員役職のカラム表示条件の初期値を取得
//20130218 中嶌追記
$default_emp_st_chk = get_emp_st_read_status($con, $fname);

// 回答入力欄のカラム表示条件の初期値を取得
$default_emp_comment_chk = get_emp_comment_read_status($con, $fname);

// 参照者一覧の表示件数を取得
$default_refer_rows_per_page = get_refer_rows_per_page($con, $fname);

// 職員一覧を表示する対象カテゴリ設定を取得（部署）
$setting_emplist_class = get_setting_emplist_class($con, $fname);

// 職員一覧を表示する対象カテゴリ（職種）
$setting_emplist_job = get_setting_emplist_job($con, $fname);


// お知らせ返信機能設定を取得
$conf = new Cmx_SystemConfig();
$setting_henshin = $conf->get('setting_henshin');

// お知らせ集計機能設定を取得
$setting_aggregate = $conf->get('setting_aggregate');

// 通達区分ポップアップ
$notice_popup = $conf->get('news.notice_popup');

//オプションから引用した設定が"t"ではなかった場合に、"f"を設定する。
if ($setting_henshin != "t") {
    $setting_henshin = "f";
}
if ($setting_aggregate != "t") {
    $setting_aggregate = "f";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | オプション</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function setDefaultCategoryDisabled() {
    document.mainform.default_category.disabled = document.mainform.everyone_flg[1].checked;
}
</script>

<link rel="stylesheet" type="text/css" href="css/main.css">

<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>

</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setDefaultCategoryDisabled();">

<?php if ($referer == "2") { ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="news_menu.php?session=<?php eh($session); ?>"><img src="img/icon/b21.gif" width="32" height="32" border="0" alt="お知らせ・回覧板管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<?php eh($session); ?>"><b>お知らせ・回覧板管理</b></a></font></td>
</tr>
</table>

<?php } else { ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<?php eh($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<?php eh($session); ?>"><b>お知らせ・回覧板</b></a> &gt; <a href="news_menu.php?session=<?php eh($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="newsuser_menu.php?session=<?php eh($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>

<?php } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="90" height="22" align="center" bgcolor="#bdd1e7"><a href="news_menu.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_register.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_list.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice_register.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_list.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_notice2_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_list.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="news_cate_register.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="news_trash.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="news_option.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>オプション</b></font></a></td>
<td width="5">&nbsp;</td>
<?php //オプションで集計機能に許可するにチェックを入れることでタブに集計を表示する
 if($setting_aggregate == 't'){?>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="news_aggregate.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計表</font></a></td>
<?php } ?>
<td width="">&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5">

<form name="mainform" action="news_option_update_exe.php" method="post">
<p><b>■お知らせ設定</b></p>

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一般ユーザのお知らせ登録</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="everyone_flg" value="t"<?php if ($everyone_flg == "t") {echo(" checked");} ?> onclick="setDefaultCategoryDisabled();">許可する
<input type="radio" name="everyone_flg" value="f"<?php if ($everyone_flg == "f") {echo(" checked");} ?> onclick="setDefaultCategoryDisabled();">許可しない
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期対象カテゴリ</font></td>
<td>
<select name="default_category">
<?php
foreach ($category_list as $category_id => $category) {
    echo "<option value=\"{$category_id}\"";
    echo ($default_category == $category_id) ? " selected>" : ">";
    eh($category["newscate_name"]);
}
?>
</select>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">「確認者を記録する」の初期値</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="record_flg" value="t"<?php if ($record_flg == "t") {echo(" checked");} ?>>する
<input type="radio" name="record_flg" value="f"<?php if ($record_flg == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧表示の件数</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="rows_per_page" value="20"<?php if ($rows_per_page == "20") {echo(" checked");} ?>>20件
<input type="radio" name="rows_per_page" value="30"<?php if ($rows_per_page == "30") {echo(" checked");} ?>>30件
<input type="radio" name="rows_per_page" value="40"<?php if ($rows_per_page == "40") {echo(" checked");} ?>>40件
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">掲載期間指定の初期値</font></td>
<td colspan="3"><select name="default_term">
<option value="1d"<?php if ($default_term == "1d") {echo(" selected");} ?>>1日
<option value="2d"<?php if ($default_term == "2d") {echo(" selected");} ?>>2日
<option value="3d"<?php if ($default_term == "3d") {echo(" selected");} ?>>3日
<option value="1w"<?php if ($default_term == "1w") {echo(" selected");} ?>>1週間
<option value="2w"<?php if ($default_term == "2w") {echo(" selected");} ?>>2週間
<option value="3w"<?php if ($default_term == "3w") {echo(" selected");} ?>>3週間
<option value="1m"<?php if ($default_term == "1m") {echo(" selected");} ?>>1ヶ月
<option value="2m"<?php if ($default_term == "2m") {echo(" selected");} ?>>2ヶ月
<option value="3m"<?php if ($default_term == "3m") {echo(" selected");} ?>>3ヶ月
<option value="4m"<?php if ($default_term == "4m") {echo(" selected");} ?>>4ヶ月
<option value="5m"<?php if ($default_term == "5m") {echo(" selected");} ?>>5ヶ月
<option value="6m"<?php if ($default_term == "6m") {echo(" selected");} ?>>6ヶ月
<option value="7m"<?php if ($default_term == "7m") {echo(" selected");} ?>>7ヶ月
<option value="8m"<?php if ($default_term == "8m") {echo(" selected");} ?>>8ヶ月
<option value="9m"<?php if ($default_term == "9m") {echo(" selected");} ?>>9ヶ月
<option value="10m"<?php if ($default_term == "10m") {echo(" selected");} ?>>10ヶ月
<option value="11m"<?php if ($default_term == "11m") {echo(" selected");} ?>>11ヶ月
<option value="1y"<?php if ($default_term == "1y") {echo(" selected");} ?>>1年
</select></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページの未読・既読選択</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="mypage_read_flg" value="t" <?php if ($mypage_read_flg == "t") {echo("checked");} ?>>する
<input type="radio" name="mypage_read_flg" value="f" <?php if ($mypage_read_flg == "f") {echo("checked");} ?>>しない
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧表示項目</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="emp_id_chk" value="t" <?php if ($default_emp_id_chk            == "t") {echo("checked");} ?>>職員ID
<input type="checkbox" name="emp_class_chk" value="t" <?php if ($default_emp_class_chk      == "t") {echo("checked");} ?>>所属
<input type="checkbox" name="emp_job_chk" value="t" <?php if ($default_emp_job_chk          == "t") {echo("checked");} ?>>職種
<input type="checkbox" name="emp_st_chk" value="t" <?php if ($default_emp_st_chk            == "t") {echo("checked");} ?>>役職
<input type="checkbox" name="emp_comment_chk" value="t" <?php if ($default_emp_comment_chk  == "t") {echo("checked");} ?>>回答入力欄
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照者一覧の表示件数</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="refer_rows_per_page" value="20" <?php if (trim($default_refer_rows_per_page) == "20") {echo("checked");} ?>>20件
<input type="radio" name="refer_rows_per_page" value="FULL" <?php if ($default_refer_rows_per_page     == "FULL") {echo("checked");} ?>>全件
</font></td>
</tr>
    <tr height="22">
        <td align="right" bgcolor="#f6f9ff">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ返信機能</font>
        </td>
        <td colspan="3">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="radio" name="setting_henshin" value="t" <?php if ($setting_henshin == "t") {echo("checked");} ?>>使用する
                <input type="radio" name="setting_henshin" value="f" <?php if ($setting_henshin == "f") {echo("checked");} ?>>使用しない
            </font>
        </td>
    </tr>

    <tr height="22">
        <td align="right" bgcolor="#f6f9ff">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計機能</font>
        </td>
        <td colspan="3">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="radio" name="setting_aggregate" value="t" <?php if ($setting_aggregate == "t") {echo("checked");} ?>>使用する
                <input type="radio" name="setting_aggregate" value="f" <?php if ($setting_aggregate == "f") {echo("checked");} ?>>使用しない
            </font>
        </td>
    </tr>
    <tr height="22">
        <td align="right" bgcolor="#f6f9ff">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">送信対象者を表示</font>
        </td>
        <td colspan="3">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="checkbox" name="setting_emplist_class" value="t" <?php if ($setting_emplist_class == "t") {echo("checked");} ?>><?php echo $category_list["2"]["newscate_name"] ?>カテゴリ
                <input type="checkbox" name="setting_emplist_job" value="t" <?php if ($setting_emplist_job == "t") {echo("checked");} ?>><?php echo $category_list["3"]["newscate_name"] ?>カテゴリ
            </font>
        </td>
    </tr>
    <tr height="22">
        <td align="right" bgcolor="#f6f9ff">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分ポップアップ機能</font>
        </td>
        <td colspan="3">
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="radio" name="notice_popup" value="1" <?php if ($notice_popup === "1") {echo("checked");} ?>>使用する
                <input type="radio" name="notice_popup" value="0" <?php if ($notice_popup !== "1") {echo("checked");} ?>>使用しない
            </font>
        </td>
    </tr>

</table>

<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="default_read_status" value="<?php eh($default_read_status); ?>">
<input type="hidden" name="session" value="<?php eh($session); ?>">
</form>

<hr align="left" width="600">

<p>■<b>マイページの表示条件、一括変更</b></p>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
※全職員のお知らせの表示条件を一括で変更できます。（変更後に、各職員が各々変更可能です。）<br>
</font>
<form action="news_option_read_status_update_exe.php" method="post">

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff" width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">マイページの表示条件、一括変更</font></td>
<td><?php disp_sele_stat($default_read_status,$mypage_read_flg,0);?></td>
</tr>
</table>

<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>

<input type="hidden" name="session" value="<?php eh($session); ?>">
</form>

</body>
</html>
<?php pg_close($con);