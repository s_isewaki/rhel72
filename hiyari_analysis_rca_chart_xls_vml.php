<?
//****************************************************************************//
//                                                                            //
//          このファイルはShift-JIS、CRLFで保存してください。                 //
//                                                                            //
//****************************************************************************//

define("HORZ_L_UPPER", 16);
define("HORZ_R_UPPER", 32);
define("HORZ_L_UNDER", 64);
define("HORZ_R_UNDER", 128);
define("VERT_UPPER", 256);
define("VERT_CENTER", 512);
define("VERT_UNDER", 1024);
define("ARROW_RIGHT", 2048);
define("ARROW_BOTTOM", 4096);


ob_start();
require_once("about_session.php");
require_once("about_authority.php");
ob_end_clean();
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
$chart_data = chartdata_json_to_array(mb_convert_encoding($_REQUEST["print_chart_data"], "shift_jis", "euc-jp"));
//$chart_data = chartdata_json_to_array($_REQUEST["print_chart_data"]);
$line_data  = linedata_json_to_array($_REQUEST["print_line_data"]);
$row_heights_data = explode(",", $_REQUEST["print_row_heights_data"]);
$col_widths_data = explode(",", $_REQUEST["print_col_widths_data"]);
$print_title = $_REQUEST["print_title"];
$print_analysis_no = $_REQUEST["print_analysis_no"];

//print_r($line_data);
//print_r($chart_data);
//die;
//$data_col_count = 0;
//$data_row_count = 0;
$col_max_width = array();
$row_max_height =array();
$total_width = array();
$total_height = array();


$col = 0;
foreach($col_widths_data as $w){
	$col++;
	$col_max_width[$col]["px"] = $w;
	$col_max_width[$col]["pt"] = $col_max_width[$col]["px"] / 4 * 3;
	$col_max_width[$col]["alt"] = $col_max_width[$col]["px"] * 32;
	$total_width["px"] = @$total_width["px"] + $col_max_width[$col]["px"];
	$total_width["pt"] = @$total_width["pt"] + $col_max_width[$col]["pt"];
	$total_width["alt"] = @$total_width["alt"] + $col_max_width[$col]["alt"];
}

$row = 0;
foreach($row_heights_data as $h){
	$row++;
	$row_max_height[$row]["px"] =$h;
	$row_max_height[$row]["pt"] = $row_max_height[$row]["px"] / 4 * 3;
	$row_max_height[$row]["alt"] = $row_max_height[$row]["px"] * 32;
	$total_height["px"] = @$total_height["px"] + $row_max_height[$row]["px"];
	$total_height["pt"] = @$total_height["pt"] + $row_max_height[$row]["pt"];
	$total_height["alt"] = @$total_height["alt"] + $row_max_height[$row]["alt"];
}






function escape_crlf($str){
	return str_replace("\r", "", str_replace("\n", "\\n", trim($str)));
}

function chartdata_json_to_array($str){
	$boxcolor = array("工程"=>"#ffff77","結果"=>"#d1d1d1","質問"=>"#7eb5ff","原因"=>"#ffa7d3","根本原因"=>"#ff9080","パス"=>"#fff");
	$out = array();
	$tmp = array();
	$startpos = 0;
	$safe = 0;
	for (;;){
		$safe++;
		if ($safe > 1000) break;
		$strcnt = 0;
		$nextpos1 = strpos($str, ']},"', $startpos);
		$nextpos2 = strpos($str, '"},"',  $startpos);
		if ($nextpos1 > 0 && $nextpos1!==FALSE && ($nextpos1 < $nextpos2 || $nextpos2===FALSE)){ // destがあって、次の要素がある
			$strcnt = $nextpos1 + 3 - $startpos;
			$tmp[] = substr($str, $startpos, $strcnt-1);
		}
		else if ($nextpos2 > 0 && $nextpos2!==FALSE && ($nextpos2 < $nextpos1 || $nextpos1===FALSE)){ // destがなくて、次の要素がある
			$strcnt = $nextpos2 + 3 - $startpos;
			$tmp[] = substr($str, $startpos, $strcnt-1);
		}
		else {
			$tmp[] = substr($str, $startpos);
			break;
		}
		$startpos += $strcnt;
	}
	foreach ($tmp as $row){
		$colonpos = strpos($row, '":{"');
		$rcaid = substr($row, 1, $colonpos-1);
		$rowdata = substr($row, $colonpos + 3, strlen($row)-$colonpos-4);

		$colonpos = strpos($rowdata, '"w":"');
		$endpos = strpos($rowdata, '"', $colonpos+5);
		$rca_w = substr($rowdata, $colonpos+5, $endpos-$colonpos-5);

		$colonpos = strpos($rowdata, '"h":"');
		$endpos = strpos($rowdata, '"', $colonpos+5);
		$rca_h = substr($rowdata, $colonpos+5, $endpos-$colonpos-5);

		$colonpos = strpos($rowdata, '"boxtype":"');
		$endpos = strpos($rowdata, '"', $colonpos+11);
		$rca_boxtype = substr($rowdata, $colonpos+11, $endpos-$colonpos-11);

		$colonpos_dest = strpos($rowdata, '"dest":[');
		$rca_dest = "";
		if ($colonpos_dest!==FALSE) {
			$endpos = strpos($rowdata, ']', $colonpos_dest+7);
			$rca_dest = substr($rowdata, $colonpos_dest+8, $endpos-$colonpos_dest-8);
		}

		$colonpos = strpos($rowdata, '"text":"');
		$endpos = ($colonpos_dest!==FALSE ? $colonpos_dest-4-$colonpos-6 : strlen($rowdata)-$colonpos-8-1);
		$rca_text = substr($rowdata, $colonpos+8, $endpos);
		if ($rca_text=="") $rca_text = "<br/>";

		$out[$rcaid] = array(
			"w_px" => $rca_w,
			"h_px" => $rca_h,
			"w_pt" => $rca_w / 4 * 3,
			"h_pt" => $rca_h / 4 * 3,
			"w_alt" => $rca_w * 32,
			"h_alt" => $rca_h * 32,
			"boxtype" => $rca_boxtype,
			"boxcolor" => $boxcolor[$rca_boxtype],
			"text" => $rca_text,
			"dest" => ($rca_dest!="" ? explode(",", str_replace('"','',$rca_dest)) : array())
		);
	}
	return $out;
}


function linedata_json_to_array($str){
	$tmp = explode(" ", $str);
	$out = array();
	$startpos = 0;
	$safe = 0;
	foreach ($tmp as $idx => $row){
		$params = explode(",", $row);
		$tmp = explode("_", $params[0]);
		if ($tmp[0]=="imgr") { $out[$tmp[1]."_".$tmp[2]."_".$tmp[3]."_arrow"] = "r"; continue; }
		if ($tmp[0]=="imgb") { $out[$tmp[1]."_".$tmp[2]."_".$tmp[3]."_arrow"] = "b"; continue; }
		$out[$tmp[1]."_".$tmp[2]."_".$tmp[3]][] = array (
			"type" =>     $tmp[0],
			"zy" =>       $tmp[1],
			"zx" =>       $tmp[2],
			"position" => $tmp[3],
			"linebit" => $tmp[4],
			"zl" => $params[1],
			"zt" => $params[2],
			"zw" => $params[3],
			"zh" => $params[4]
		);
	}
	return $out;
}


//====================================================================
// ヘッダ設定
//====================================================================
$fname = "fplus.xls";

header("content-type: application/vnd.ms-excel");
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Disposition: attachment; filename=".$fname.";");
header('Content-Type: application/vnd.ms-excel');
header("Content-Transfer-Encoding: binary");

?>

<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=shift_jis">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel=File-List href="print_excel.files/filelist.xml">
<link rel=Edit-Time-Data href="print_excel.files/editdata.mso">
<link rel=OLE-Object-Data href="print_excel.files/oledata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Medi-System</o:Author>
  <o:LastAuthor>Medi-System</o:LastAuthor>
  <o:Created>2010-06-01T00:26:32Z</o:Created>
  <o:LastSaved>2010-06-01T00:35:59Z</o:LastSaved>
  <o:Company>（株）メディシステムソリューション</o:Company>
  <o:Version>11.9999</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.98in .79in .98in .79in;
	mso-header-margin:.51in;
	mso-footer-margin:.51in;}
tr
	{mso-height-source:auto;
	mso-ruby-visibility:none;}
col
	{mso-width-source:auto;
	mso-ruby-visibility:none;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	border:none;
	mso-protection:locked visible;
	mso-style-name:標準;
	mso-style-id:0;}
.font0
	{color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;}
.font7
	{color:windowtext;
	font-size:14.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	mso-number-format:General;
	text-align:general;
	vertical-align:middle;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl_boxY
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FFFFCC;
	mso-pattern:auto none;
	white-space:normal;}
.xl25
	{mso-style-parent:style0;
	text-align:center;}
.xl_boxG
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:silver;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxB
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#CCFFFF;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxR
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FF99CC;
	mso-pattern:auto none;
	white-space:normal;}
.xl_boxV
	{mso-style-parent:style0;
	text-align:center;
	border:.5pt solid windowtext;
	background:#FFCC99;
	mso-pattern:auto none;
	white-space:normal;}
ruby
	{ruby-align:left;}
rt
	{color:windowtext;
	font-size:6.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"ＭＳ Ｐゴシック", monospace;
	mso-font-charset:128;
	mso-char-type:katakana;
	display:none;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>RCA分析チャート</x:Name>
    <x:WorksheetOptions>
     <x:DefaultRowHeight>270</x:DefaultRowHeight>
     <x:Zoom>100</x:Zoom>
     <x:Selected/>
     <x:DoNotDisplayGridlines/>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>9615</x:WindowHeight>
  <x:WindowWidth>15075</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>30</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1043"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body link=blue vlink=purple class=xl25>

<table x:str border=0 cellpadding=0 cellspacing=0 width=<?=$total_width["px"]?> style='border-collapse:
 collapse;table-layout:fixed;width:<?=$total_width["px"]?>px'>



<? //======================================================?>
<? // 列の定義                                             ?>
<? //======================================================?>

 <? // 一列目はカラ列30px ?>
 <col class=xl25 width=30 style='mso-width-source:userset;mso-width-alt:960; width:23pt'>
<? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
	<? // データ行 ?>
 <col class=xl25 width=<?=$col_max_width[$col]["px"]?> style='mso-width-source:userset;mso-width-alt:<?=$col_max_width[$col]["alt"]?>;
 width:<?=$col_max_width[$col]["px"]?>px'>
 <? // ライン行は固定幅30px ?>
 <col class=xl25 width=30 style='mso-width-source:userset;mso-width-alt:960;
 width:23pt'>
<? } ?>


<? //======================================================?>
<? // マージン用の行・ヘッダ行                             ?>
<? //======================================================?>
 <tr height=18 style='height:13.5pt'>
  <td height=18 class=xl25 width=30 style='height:13.5pt;width:23pt'></td>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
  <td class=xl25 width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["px"]?>px'></td>
  <td class=xl25 width=30 style='width:30px'></td>
  <? } ?>
 </tr>
 <tr height=18 style='height:13.5pt'>
 	<td height=18 class=xl25 width=30 style='height:13.5pt;width:23pt'></td>
  <td height=18 colspan=<?=count($col_widths_data)*2?> class=xl25 style='height:13.5pt;mso-ignore:colspan'>
<!--[if gte vml 1]><v:rect id="_x0000_s1146"
   style='position:absolute;margin-left:2.25pt;margin-top:.75pt;width:561pt;
   height:27.75pt;mso-wrap-style:tight' filled="f" fillcolor="window [65]"
   stroked="f" strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:left;padding-top:5.25pt'><font class="font7"><?=$print_title?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><v:rect id="_x0000_s1147" style='position:absolute;margin-left:623.25pt;
   margin-top:1.5pt;width:74.25pt;height:18pt;z-index:123;mso-wrap-style:tight'
   fillcolor="#cfc [42]" strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:center;padding-top:1.875pt'><font class="font0">分析番号</font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><v:rect id="_x0000_s1148" style='position:absolute;margin-left:623.25pt;
   margin-top:19.5pt;width:74.25pt;height:18pt;z-index:124' fillcolor="window [65]"
   strokecolor="windowText [64]" o:insetmode="auto">
   <v:textbox style='mso-direction-alt:auto'>
    <div style='text-align:center;padding-top:1.875pt'><font class="font0"><?=$print_analysis_no?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
   </v:rect><![endif]--><![if !vml]>
  </td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>
 <tr height=18 style='height:13.5pt'>
  <td height=18 colspan=<?=count($col_widths_data)*2+1?> class=xl25 style='height:13.5pt;mso-ignore:colspan'></td>
 </tr>


<? for ($row = 1; $row <= count($row_heights_data); $row++){ ?>

	<? //======================================================?>
	<? // データ行開始                                         ?>
	<? //======================================================?>
 <tr height=<?=$row_max_height[$row]["px"]?> style='mso-height-source:userset;height:<?=$row_max_height[$row]["px"]?>px'>

	<? // １列目A列はカラ列 ?>
  <td height=<?=$row_max_height[$row]["px"]?> class=xl25 style='height:<?=$row_max_height[$row]["px"]?>px'></td>

	<? //======================================================?>
	<? // ２列目以降のデータ行                                 ?>
	<? //======================================================?>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>

  <td width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["px"]?>px; vertical-align:top'
    align=left valign=top>

<? if (@$chart_data[$row."_".$col]["boxtype"]) { ?>
 <!--[if gte vml 1]><v:rect
   style='position:absolute; margin-left:0pt; margin-top:0pt; z-index:5000; mso-wrap-style:tight;
   width:<?=@$chart_data[$row."_".$col]["w_px"]?>px;
   height:<?=@$chart_data[$row."_".$col]["h_px"]?>px;'
   fillcolor="<?=@$chart_data[$row."_".$col]["boxcolor"]?>" strokecolor="#000">
   <v:textbox style='mso-direction-alt:auto;' inset="1.5mm,0.8mm,1.5mm,0.8mm">
    <div style='text-align:center;'><font class="font0"><?=str_replace("\n","<br>\n",@$chart_data[$row."_".$col]["text"]) ?></font></div>
   </v:textbox>
   <x:ClientData ObjectType="Rect">
    <x:TextHAlign>Center</x:TextHAlign>
    <x:TextVAlign>Center</x:TextVAlign>
   </x:ClientData>
  </v:rect><![endif]-->
<? } ?>


<? if (@$line_data[$row."_".$col."_PosBox"]) { ?>
<?   foreach ($line_data[$row."_".$col."_PosBox"] as $idx => $ld) {?>
  <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
    from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
    coordsize="21600,21600" strokecolor="windowText [64]" o:insetmode="auto" /><![endif]-->
<?   } ?>
<? } ?>



</td>


	<? // ライン用 ?>
  <td class=xl25 style='mso-ignore:colspan;'>
    <? if (@$line_data[$row."_".$col."_PosR"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosR"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600" strokecolor="windowText [64]" o:insetmode="auto">
        <? if (@$line_data[$row."_".$col."_PosR_arrow"]=="r"){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        </v:line><![endif]-->
      <? } ?>
    <? } ?>
  </td>

	<? } ?>


 </tr>


	<? //======================================================?>
	<? // ライン行開始                                         ?>
	<? //======================================================?>
 <tr height=30 style='mso-height-source:userset;height:22.5pt'>

	<? // １列目A列はカラ列 ?>
  <td height=30 class=xl25 style='height:22.5pt'></td>


  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>


  <td class=xl25 style='mso-ignore:colspan'>
    <? if (@$line_data[$row."_".$col."_PosB"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosB"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line
        style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600"
        strokecolor="windowText [64]" o:insetmode="auto">
        <? $line_bit = @$ld["linebit"]; ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow"]=="r" && $line_bit == ($line_bit | HORZ_R_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow"]=="r" && $line_bit == ($line_bit | HORZ_L_UPPER)){ ?>
        <v:stroke startarrow="block"/>
        <? } ?>
        <? if (@$line_data[$row."_".$col."_PosB_arrow"]=="b" && $line_bit == ($line_bit | VERT_UNDER)){ ?>
        <v:stroke endarrow="block"/>
        <? } ?>
        </v:line><![endif]-->
      <? } ?>
    <? } ?>
  </td>


  <td class=xl25 style='mso-ignore:colspan'>
    <? if (@$line_data[$row."_".$col."_PosRB"]) { ?>
      <? foreach ($line_data[$row."_".$col."_PosRB"] as $idx => $ld) {?>
        <!--[if gte vml 1]><v:line style='position:absolute; z-index:1'
        from="<?=$ld["zl"]?>px,<?=$ld["zt"]?>px" to="<?=$ld["zl"]+$ld["zw"]?>px,<?=$ld["zt"]+$ld["zh"]?>px"
        coordsize="21600,21600"
        strokecolor="windowText [64]" o:insetmode="auto" /><![endif]-->
      <? } ?>
    <? } ?>
  </td>

<? } ?>
<? } ?>

 </tr>


 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <? for ($col = 1; $col <= count($col_widths_data); $col++){ ?>
  <td width=30 style='width:23pt'></td>
  <td width=<?=$col_max_width[$col]["px"]?> style='width:<?=$col_max_width[$col]["pt"]?>pt'></td>
  <? } ?>
  <td width=30 style='width:23pt'></td>
 </tr>







 <![endif]>
</table>

</body>

</html>
