<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | 当直予定表作成</title>

<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//現在日付の取得
	///-----------------------------------------------------------------------------
	$date = getdate();
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	///-----------------------------------------------------------------------------
	//表示年／月設定
	///-----------------------------------------------------------------------------
	if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
	if ($duty_mm == "") { $duty_mm = $now_mm; }
	///-----------------------------------------------------------------------------
	//日数
	///-----------------------------------------------------------------------------
//	$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
//	$start_day = 1;
//	$end_day = $day_cnt;
	///-----------------------------------------------------------------------------
	//各変数値の初期値設定
	///-----------------------------------------------------------------------------
	$highlight_flg = "1";		//強調表示フラグ（１：強調表示）
	$draft_flg = "";			//下書きフラグ（１：下書き）
	if ($data_cnt == "") { $data_cnt = 0; }
	//入力形式で表示する日（開始／終了）
	if ($edit_start_day == "") { $edit_start_day = 0; }
	if ($edit_end_day == "") { $edit_end_day = 0; }
	///-----------------------------------------------------------------------------
	// カレンダー(calendar)情報を取得
	///-----------------------------------------------------------------------------
//	$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, "01");
//	$end_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $day_cnt);
//	$calendar_array = $obj->get_calendar_array($start_date, $end_date);
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
	///-----------------------------------------------------------------------------
	//ＤＢ(stmst)より役職情報取得
	//ＤＢ(jobmst)より職種情報取得
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務グループ情報を取得
	///-----------------------------------------------------------------------------
	$data_st = $obj->get_stmst_array();
	$data_job = $obj->get_jobmst_array();
//	$data_emp = $obj->get_empmst_array("");
// 有効なグループの職員を取得
//	$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm); // 20140210
	$data_wktmgrp = $obj->get_wktmgrp_array();
///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", "", $data_wktmgrp);
if (count($group_array) <= 0) {
	$err_msg_1 = "勤務シフトグループ情報が未設定です。管理画面で登録してください。";
} else {
    // 更新権限あるグループ取得 20140210 //職員設定、応援追加されていても含めない
    $group_array = $obj->get_valid_group_array($group_array, $emp_id, $duty_yyyy, $duty_mm);
    // 1件もない場合
    if (count($group_array) <= 0) {
        $err_msg_1 = "シフト管理者として登録されたシフトグループがありません。管理画面＞シフトグループ一覧＞シフトグループ登録画面で登録してください。";
    }
    else {
        if ($group_id == ""){
            $group_id = $group_array[0]["group_id"];
        } else {
            //職員設定から移動してきた場合の対応 20100827
            $group_id_flg = false;
            
            //権限、職員設定されているか確認
            for ($i=0; $i<count($group_array); $i++) {
                if ($group_id == $group_array[$i]["group_id"]) {
                    $group_id_flg = true;
                    break;
                }
            }
            //権限がないグループの場合、先頭のグループにする
            if (!$group_id_flg) {
                $group_id = $group_array[0]["group_id"];
            }
        }
    }
}
	for ($i=0;$i<count($group_array);$i++) {
	if ($group_id == $group_array[$i]["group_id"]) {
		$start_month_flg1 = $group_array[$i]["start_month_flg1"];
		$start_day1 = $group_array[$i]["start_day1"];
		$month_flg1 = $group_array[$i]["month_flg1"];
		$end_day1 = $group_array[$i]["end_day1"];
		break;
	}
	}
	if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
	if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
	if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
	if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}

// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);

	///-----------------------------------------------------------------------------
	// カレンダー(calendar)情報を取得
	///-----------------------------------------------------------------------------
	$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);
	$start_date = $arr_date[0];
	$end_date = $arr_date[1];
	$calendar_array = $obj->get_calendar_array($start_date, $end_date);
	$day_cnt=count($calendar_array);
	///-----------------------------------------------------------------------------
	//指定日の週を設定
	///-----------------------------------------------------------------------------
	$week_array = array();
	for ($k=1; $k<=$day_cnt; $k++) {
//		$tmp_date = mktime(0, 0, 0, $duty_mm, $k, $duty_yyyy);
		$wk_date = $calendar_array[$k-1]["date"];
		$wk_yyyy = substr($wk_date, 0, 4);
		$wk_mm = substr($wk_date, 4, 2);
		$wk_dd = substr($wk_date, 6, 2);
		$tmp_date = mktime(0, 0, 0, $wk_mm, $wk_dd, $wk_yyyy);
		$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	}
	///-----------------------------------------------------------------------------
	// 当直管理者情報を取得
	///-----------------------------------------------------------------------------
	$person_array = $obj_duty->get_duty_shift_duty_person_array($data_st, $data_job, $data_emp);
	//権限チェック
	$create_flg = "";
	for ($i=0;$i<count($person_array);$i++) {
	if ($emp_id == $person_array[$i]["id"]) {
		$create_flg = "1";
		break;
	}
	}
	///-----------------------------------------------------------------------------
	// 勤務当直情報を取得判定（新規表示／再表示）
	///-----------------------------------------------------------------------------
	$duty_array = array();
	$set_data = array();
	$wk_flg = "";
	if (($data_cnt > 0) &&
		($group_id == $cause_group_id) &&
		($duty_yyyy == $cause_duty_yyyy) &&
		($duty_mm == $cause_duty_mm)) {
		$wk_flg = "1";
	} else {
		$edit_start_day = 0;
		$edit_end_day = 0;
	}
	///-----------------------------------------------------------------------------
	//再表示時
	///-----------------------------------------------------------------------------
	// 当直のみ表示の場合の条件追加？and/or ?
	if ($wk_flg == "1" || $duty_only_disp_flg != "") {
		for($i=0;$i<$data_cnt;$i++) {
			///-----------------------------------------------------------------------------
			//既存データの追加
			///-----------------------------------------------------------------------------
			for($k=1;$k<=$day_cnt;$k++) {
				$wk = "night_duty_$i" . "_" . $k;
				$set_data[$i]["staff_id"] = $staff_id[$i];
				$set_data[$i]["night_duty_$k"] = $$wk;
			}
			///-----------------------------------------------------------------------------
			//実績
			///-----------------------------------------------------------------------------
//			$wk_array = split(",", $rslt_night_duty[$i]);
//			$p = 1;
//			foreach ($wk_array as $wk_rslt_night_duty) {
//				$set_data[$i]["rslt_night_duty_$p"] = $wk_rslt_night_duty;
//				$p++;
//			}
		}
	} else {
		$edit_start_day = 0;
		$edit_end_day = 0;
	}
	///-----------------------------------------------------------------------------
	// ＤＢ読み込み時
	///-----------------------------------------------------------------------------
	// 当直のみ表示の場合、取得時のグループIDを""とする
	if ($duty_only_disp_flg != "") {
		$tmp_group_id = "";
	} else {
		$tmp_group_id = $group_id;
	}
	$duty_array = $obj_duty->get_duty_shift_duty_array($tmp_group_id,
													$duty_yyyy, $duty_mm, $day_cnt,
													$set_data, $data_week, $calendar_array, $duty_only_disp_flg,
									$data_st, $data_job, $data_emp);
	$data_cnt = count($duty_array);
	///-----------------------------------------------------------------------------
	// 勤務シフト情報の下書きデータ有無
	///-----------------------------------------------------------------------------
	//検索条件（職員）
	$cond_add = "";
	for ($i=0;$i<count($duty_array);$i++) {
		$wk_id = $duty_array[$i]["staff_id"];
		if ($cond_add == "") {
			$cond_add .= "and (emp_id = '$wk_id' ";
		} else {
			$cond_add .= "or emp_id = '$wk_id' ";
		}
	}
	if ($cond_add != "") {
		$cond_add .= ") ";
	}
	//検索
	$sql = "select count(*) as cnt from duty_shift_duty_draft";
	$cond = "where 1 = 1 ";
	$cond .= "and duty_date >= '$start_date'and duty_date <= '$end_date' ";
	$cond .= $cond_add;
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num_1 = pg_fetch_result($sel, 0, "cnt");
	if ($num_1 > 0) {
		$finish_flg = "[下書き：有り]";
	}
?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	//下書き保存／登録
	///-----------------------------------------------------------------------------
	function editData(flg) {
		//下書き保存／登録
		if (flg == 1) {
			if (confirm('「下書き保存」します。よろしいですか？')) {
				document.mainform.draft_flg.value = "1";
				document.mainform.action="duty_shift_duty_update_exe.php";
				document.mainform.submit();
			}
		} else {
			if (confirm('「登録」します。よろしいですか？')) {
				document.mainform.draft_flg.value = "";
				document.mainform.action="duty_shift_duty_update_exe.php";
				document.mainform.submit();
			}
		}
	}
	///-----------------------------------------------------------------------------
	//予定／実績表示
	///-----------------------------------------------------------------------------
	function showPlanResults(flg) {
		//予定／実績表示
		document.mainform.edit_start_day.value = 0;
		document.mainform.edit_end_day.value = 0;
		document.mainform.plan_results_flg.value = flg;
		document.mainform.action="duty_shift_duty.php";
		document.mainform.submit();
	}

	///-----------------------------------------------------------------------------
	//指定日された選択入力可能にする
	///-----------------------------------------------------------------------------
	function showEdit(start_day, end_day) {
		try {
			//登録権限があるかチェック
			if (document.mainform.create_flg.value == "") {
				return;
			}

			//指定日された選択入力可能にする
			if ((document.mainform.edit_start_day.value == start_day) &&
				(document.mainform.edit_end_day.value == end_day)) {
				//表示のみ
				document.mainform.edit_start_day.value = 0;
				document.mainform.edit_end_day.value = 0;
			} else {
				//選択入力可能
				document.mainform.edit_start_day.value = start_day;
				document.mainform.edit_end_day.value = end_day;
			}
			document.mainform.action="duty_shift_duty.php";
			document.mainform.submit();
		} 
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	//行／列ハイライト
	///-----------------------------------------------------------------------------
	function highlightCells(class_name, gyo, retu) {
		changeCellsColor(class_name, '#ffff66',gyo,retu);
	}
	function dehighlightCells(class_name, gyo, retu) {
		changeCellsColor(class_name, '',gyo,retu);
	}
	function changeCellsColor(class_name, color, gyo, retu) {
		try {
			var data = document.getElementById("data");
			var data_cnt = data.rows.length;
			var summary = document.getElementById("summary");
			var summary_cnt = summary.rows.length;
			var day_cnt = document.mainform.day_cnt.value;
			var plan_results_flg = document.mainform.plan_results_flg.value;
			var td_name = '';
			var p = 1;
			///-----------------------------------------------------------------------------
			//予実表示時、予定時のみ表示
			///-----------------------------------------------------------------------------
			if (plan_results_flg == "2") {
				p = 2;

				var wk = parseInt(gyo) / 2;
				var wk1 = "" + wk;
				//予定か判定
			    if (wk1.match(/[^0-9]/g)) {
				} else {
					return;
				}
			}
			///-----------------------------------------------------------------------------
			//カーソル表示
			///-----------------------------------------------------------------------------
			if ((parseInt(gyo) > 0) && (parseInt(retu) > 0)) {
				for (k=1;k<=day_cnt;k++){
					td_name = 'data' +  parseInt(gyo) + '_' + k;
					document.getElementById(td_name).style.backgroundColor = color;
				}
				for (i=1;i<=data_cnt;i++){
					td_name = 'data' + i + '_' + parseInt(retu);
					document.getElementById(td_name).style.backgroundColor = color;
				}
			}
			if (parseInt(gyo) > 0) {
				td_name = 'no' + parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				if (document.mainform.duty_only_disp_flg.value != '') {
					td_name = 'group' + parseInt(gyo);
					document.getElementById(td_name).style.backgroundColor = color;
				}
				td_name = 'name' + parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'job' +  parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'sum_gyo' + parseInt(gyo);
				document.getElementById(td_name).style.backgroundColor = color;
			}
			if (parseInt(retu) > 0) {
				td_name = 'day' + parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'day_of_week' +  parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;
				td_name = 'summary' + parseInt(retu);
				document.getElementById(td_name).style.backgroundColor = color;
			}
		} 
		catch(err){
		}
	}

	///-----------------------------------------------------------------------------
	//メッセージ画面（表示／非表示）
	///-----------------------------------------------------------------------------
	function showMemo(day, memo) {
		try {
			if (memo == "") {
				return;
			}

		    //--このHTMLテンプレートを書き換えればヘルプのデザインが変わります。
			var msg1  = '<table bgcolor="#dddddd" border="1">\n';
				msg1 += '<tr><td>'+memo+'</td></tr>\n';
				msg1 += '</table>\n';
			showMsg(msg1);
		} 
		catch(err){
		}
	}
	//当直のみ表示
	function showDutyOnly() {
		document.mainform.duty_only_disp_flg.value = "1";
		document.mainform.action="duty_shift_duty.php";
		document.mainform.submit();
	}

</script>
<!-- ************************************************************************ -->
<!-- JavaScript（メッセージ子画面表示処理） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	//(注1)

	//--HTML出力
	function outputLAYER(layName,html){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o7,s1用
	  document.getElementById(layName).innerHTML=html;
	} else if(document.all){            //e4用
	  document.all(layName).innerHTML=html;
	} else if(document.layers) {        //n4用
	   with(document.layers[layName].document){
	     open();
	     write(html);
	     close();
	  }
	}
	}

	//--マウス追跡
	/*==================================================================
	followingLAYER()

	Syntax :
	 追跡レイヤー名 = new followingLAYER('レイヤー名'
	                            ,右方向位置,下方向位置,動作間隔,html)

	 レイヤー名 マウスを追跡させるレイヤー名
	 右方向位置 マウスから右方向へ何ピクセル離すか
	 下方向位置 マウスから下方向へ何ピクセル離すか
	 動作間隔   マウスを追跡する間隔(1/1000秒単位 何秒後に動くか?)
	 html       マウスを追跡するHTML

	------------------------------------------------------------------*/
	/*--/////////////ここから下は触らなくても動きます/////////////--*/

	//--追跡オブジェクト
	//e4,e5,e6,n4,n6,n7,m1,o6,o7,s1用
	function followingLAYER(layName,ofx,ofy,delay,html){
	this.layName = layName;   //マウスを追跡させるレイヤー名
	this.ofx     = ofx;       //マウスから右方向へ何ピクセル離すか
	this.ofy     = ofy;       //マウスから下方向へ何ピクセル離すか
	this.delay   = delay;     //マウスを追跡するタイミング
	if(document.layers)
	  this.div='<layer name="'+layName+'" left="-100" top="-100">\n'	          + html + '</layer>\n';
	else
	  this.div='<div id="'+layName+'"\n'	          +'style="position:absolute;left:-100px;top:-100px">\n'	          + html + '</div>\n';
	document.write(this.div);
	}

	//--メソッドmoveLAYER()を追加する
	followingLAYER.prototype.moveLAYER = moveLAYER; //メソッドを追加する
	function moveLAYER(layName,x,y){
	if(document.getElementById){        //e5,e6,n6,n7,m1,o6,o7,s1用
	    document.getElementById(layName).style.left = x;
	    document.getElementById(layName).style.top  = y;
	} else if(document.all){            //e4用
	    document.all(layName).style.pixelLeft = x;
	    document.all(layName).style.pixelTop  = y;
	} else if(document.layers)          //n4用
	    document.layers[layName].moveTo(x,y);
	}

	//--Eventをセットする(マウスを動かすとdofollow()を実行します)
	document.onmousemove = dofollow;
	//--n4マウスムーブイベント走査開始
	if(document.layers)document.captureEvents(Event.MOUSEMOVE);
	//--oの全画面のEventを拾えないことへの対策
	if(window.opera){
	op_dmydoc ='<div id="dmy" style="position:absolute;z-index:0'	          +'                     left:100%;top:100%"></div> ';
	document.write(op_dmydoc);
	}

	//--イベント発生時にマウス追跡実行
	function dofollow(e){
	for(var i=0 ; i < a.length ; i++ )
	  setTimeout("a["+i+"].moveLAYER(a["+i+"].layName,"	         +(getMouseX(e)+a[i].ofx)+","+(getMouseY(e)+a[i].ofy)+")"	    ,a[i].delay);
	}

	//--マウスX座標get
	function getMouseX(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientX;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollLeft+event.clientX;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageX;
	}

	//--マウスY座標get
	function getMouseY(e){
	if(navigator.userAgent.search(
	         "Opera(\ |\/)6") != -1 )   //o6用
	    return e.clientY;
	else if(document.all)               //e4,e5,e6用
	    return document.body.scrollTop+event.clientY;
	else if(document.layers ||
	        document.getElementById)    //n4,n6,n7,m1,o7,s1用
	    return e.pageY;
	}

	/*////////////////////////////// マウスを追跡するレイヤーここまで */

	//テンプレートを作成しoutputLAYERへ渡す
	function showMsg(msg1){
		outputLAYER('test0',msg1);
	}

	//レイヤーの中身を消す
	function hideMsg(){
		var msg1 ='';
		outputLAYER('test0',msg1);
	}

	//レイヤーの数だけa[i]=…部分を増減して使ってください
	var a = new Array();
	a[0]  = new followingLAYER('test0',20,10,100,'');
	a[1]  = new followingLAYER('test1',20,10,200,'');
	a[2]  = new followingLAYER('test2',20,10,300,'');
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.non_list {border-collapse:collapse;}
.non_list td {border:#FFFFFF solid 1px;}

img.close {
	background-image:url("images/minus.gif");
	vertical-align:middle;
}
img.open {
	background-image:url("images/plus.gif");
	vertical-align:middle;
}

</style>
</head>

<? //echo("<br>group_id=$group_id"); ?>
<? //echo("<br>pattern_id=$pattern_id"); ?>
<? //echo("<br>emp_id=$emp_id"); ?>
<? //echo("<br>"); ?>
<? //print_r($set_data); ?>
<? //print_r($duty_array); ?>
<? //print_r($calendar_array); ?>
<?
/**
for ($i=0;$i<count($duty_array); $i++) {
	$wk = sprintf("%02d",$i+1);
	$wk1 = $duty_array[$i]["staff_id"];
	$wk2 = $duty_array[$i]["staff_name"];
	echo("($wk)staff_id=$wk1 $wk2<br>");
	for ($k=1;$k<=$day_cnt; $k++) {
		$wk1 = $duty_array[$i]["pattern_id_$k"];
		echo("($wk)pattern_id_$k=$wk1<br>");
	}
}
**/
//echo("<br>");
?>
	<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
	<?
		// 画面遷移
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
		echo("</table>\n");

		// タブ
		$arr_option = "&group_id=" . $group_id;
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
		echo("</table>\n");

		// 下線
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
	<?
		if ($err_msg_1 != "") {
			echo($err_msg_1);
			echo("<br>\n");
			echo("<form name=\"mainform\" method=\"post\">\n");
			echo("<table id=\"header\" class=\"list\"></table>\n");
			echo("<div id=\"region\">\n");
			echo("<table id=\"data\" class=\"list\"></table>\n");
			echo("</div>\n");
			echo("<table id=\"error\" class=\"list\"></table>\n");
			echo("<table id=\"summary\" class=\"list\"></table>\n");
			echo("</form>");
		} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 当直表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<?
			echo($obj_duty->showHidden_1($data_cnt,
										$day_cnt,
										$plan_results_flg,
										$edit_start_day,
										$edit_end_day));
		?>
		<!-- ------------------------------------------------------------------------ -->
		<!--  シフトグループ（病棟）名、年、月 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?
			$url = "duty_shift_duty.php";
			$url_option = "";
			$wk_group_show_flg = "";
			echo($obj_duty->showHead($session, $fname,
									$wk_group_show_flg, $group_id, $group_array,
									$duty_yyyy, $duty_mm, $url, $url_option));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 各種ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<?
			echo($obj_duty->showButton($create_flg, $plan_results_flg, $finish_flg));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 見出し -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="non_list">
		<?
			//週
			echo($obj_duty->showTitleSub1($duty_yyyy, $duty_mm,
										$day_cnt, $edit_start_day, $edit_end_day,
										$week_array, $refer_flg,
										$duty_only_disp_flg));
		?>
		</table>
		<table width="250" id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
			//日・曜日
			echo($obj_duty->showTitleSub2($duty_yyyy, $duty_mm,
										$day_cnt, $edit_start_day, $edit_end_day,
										$week_array, $calendar_array, $refer_flg,
										$duty_only_disp_flg));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務シフトデータ（職員（列）年月日（行） -->
		<!-- ------------------------------------------------------------------------ -->
		<div id="region" style="cursor:pointer; overflow-x:hidden; overflow-y:scroll; overflow:auto; border:#5279a5 solid 0px;">
		<table width="250" id="data" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
			echo($obj_duty->showList($duty_array,
									$plan_results_flg,
									$day_cnt,
									$edit_start_day,
									$edit_end_day,
									$week_array,
									0,
									$data_cnt,
									$highlight_flg,
									$refer_flg,
									$duty_only_disp_flg,
									$group_array));
		?>
		</table>
		</div>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 列計 -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" id="summary" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
		<?
			echo($obj_duty->showTitleRetu($duty_array, $day_cnt,
									$edit_start_day, $edit_end_day, $week_array,
									$data_cnt, $refer_flg,
									$duty_only_disp_flg));
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<?
			echo($obj_duty->showHidden_2(
										$duty_array,
										$calendar_array,
										$session,
										$day_cnt,
										$group_id,
										$duty_yyyy,
										$duty_mm,
										$edit_start_day,
										$edit_end_day,
										$draft_flg,
										$refer_flg));

			echo("<input type=\"hidden\" name=\"create_flg\" value=\"$create_flg\">\n");	//登録可能フラグ（１：可能）
			if ($duty_only_disp_flg == "1") {
				$duty_only_disp_flg = "2";
			}
			echo("<input type=\"hidden\" name=\"duty_only_disp_flg\" value=\"$duty_only_disp_flg\">\n");	//当直のみ表示フラグ（"":当直のみ表示以外 1:初回 2:2回目以降）
		?>

	</form>

	<?
	}
	?>

</body>

<? pg_close($con); ?>

</html>

<!-- ************************************************************************ -->
<!-- JavaScript（スクロールの位置調整） -->
<!-- ************************************************************************ -->
<script language="JavaScript">
	var data = document.getElementById("data");
	var header = document.getElementById("header");

	///-----------------------------------------------------------------------------
	// スクロールの位置調整
	///-----------------------------------------------------------------------------
//	document.getElementById("region").style.width = data.offsetWidth + 17;
	document.getElementById("region").style.width = data.offsetWidth + 20;
	///-----------------------------------------------------------------------------
	// スクロールの縦サイズ調整
	///-----------------------------------------------------------------------------
	var data_row = data.rows.length;
	if(data_row > 20) {
		var max = 20;
		var scroll_size = 0;
		for(i=0; i<max; i++) {
			scroll_size += data.rows[i].offsetHeight;
		}
		document.getElementById('region').style.height = scroll_size;
	}
</script>


