<?php
$fname = $_SERVER["PHP_SELF"];

require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("show_schedule_chart.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

// スケジュール管理権限を取得
$schd_place_auth = check_authority($session, 13, $fname);

// 日付の設定
if ($date == "") {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}
$day = date("d", $date);
$month = date("m", $date);
$year = date("Y", $date);

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "SELECT emp_id FROM session WHERE session_id='{$session}'";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    pg_close($con);
    js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// スケジュール機能の初期画面情報を取得
$default_view = get_default_view($con, $emp_id, $fname);

// マイグループ一覧を配列に格納
$mygroups = array();
$sql = "select mygroup_id, mygroup_nm from mygroupmst";
$cond = "where owner_id = '$emp_id' order by mygroup_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $mygroups[$row["mygroup_id"]] = $row["mygroup_nm"];
}

// 初期表示時に表示するグループを設定
if ($mygroup_id == "" && count($mygroups) > 0) {
    $mygroup_id = key($mygroups);
}
if ($mygroup_id == "") {
    $mygroup_id = 0;
}

// マイグループ情報を配列に格納
$sql = "select mygroup.member_id, empmst.emp_lt_nm, empmst.emp_ft_nm from mygroup inner join empmst on empmst.emp_id = mygroup.member_id inner join authmst on authmst.emp_id = mygroup.member_id";
$cond = "where mygroup.owner_id = '$emp_id' and mygroup_id = $mygroup_id and authmst.emp_del_flg = 'f' order by mygroup.order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
}
$arr_mygroup = array();
while ($row = pg_fetch_array($sel)) {
    array_push(
        $arr_mygroup,
        array(
            "id" => $row["member_id"],
            "name" => $row["emp_lt_nm"] . " " . $row["emp_ft_nm"]
        )
    );
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | マイグループ</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
var childwin = null;
function openEmployeeList(item_id) {
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode=5';
    url += '&item_id='+item_id;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

function add_target_list(item_id, emp_id,emp_name)
{
    var emp_ids = emp_id.split(", ");
    var emp_names = emp_name.split(", ");

    //追加
    for(i=0;i<emp_ids.length;i++){
        var in_group = false;
        emp_id = emp_ids[i];
        emp_name = emp_names[i];
        for (var k = 0, l = document.mainform.mygroup.options.length; k < l; k++) {
            if (document.mainform.mygroup.options[k].value == emp_id) {
                in_group = true;
                break;
            }
        }

        if (!in_group && emp_id != '<?=$emp_id?>') {
            addOption(document.mainform.mygroup, emp_id, emp_name);
        }
    }
    setMyGroupList();
}


function initPage() {
}

function deleteEmp() {

    for (var i = document.mainform.mygroup.options.length - 1; i >= 0; i--) {
        if (document.mainform.mygroup.options[i].selected) {
            document.mainform.mygroup.options[i] = null;
        }
    }
    setMyGroupList();

}

function deleteAllEmp() {

    deleteAllOptions(document.mainform.mygroup);
    setMyGroupList();

}

function setMyGroupList() {

    document.mainform.mygroup_list.value = '';
    for (var i = 0, j = document.mainform.mygroup.options.length; i < j; i++) {
        if (i > 0) {
            document.mainform.mygroup_list.value += ',';
        }
        document.mainform.mygroup_list.value += document.mainform.mygroup.options[i].value;
    }

}

function deleteAllOptions(box) {

    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }

}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    box.options[box.length] = opt;
    try {box.style.fontSize = 'auto';} catch (e) {}
    box.style.overflow = 'auto';
    if (selected == value) {
        box.selectedIndex = box.options.length -1;
        return;
    }
}

function getSelectedValue(sel) {

    return sel.options[sel.selectedIndex].value;

}

function raiseEmployee() {
    var myGroup = document.mainform.mygroup;
    var selectedIndex = myGroup.selectedIndex;
    if (selectedIndex < 1) {
        return;
    }

    selectedValue = myGroup.options[selectedIndex].value;
    selectedText  = myGroup.options[selectedIndex].text;

    upperValue = myGroup.options[selectedIndex - 1].value;
    upperText  = myGroup.options[selectedIndex - 1].text;

    myGroup.options[selectedIndex - 1].value    = selectedValue;
    myGroup.options[selectedIndex - 1].text     = selectedText;
    myGroup.options[selectedIndex - 1].selected = true;

    myGroup.options[selectedIndex].value    = upperValue;
    myGroup.options[selectedIndex].text     = upperText;
    myGroup.options[selectedIndex].selected = false;

    setMyGroupList();
}

function dropEmployee() {
    var myGroup = document.mainform.mygroup;
    var selectedIndex = myGroup.selectedIndex;
    if (selectedIndex < 0 || selectedIndex >= myGroup.options.length - 1) {
        return;
    }

    selectedValue = myGroup.options[selectedIndex].value;
    selectedText  = myGroup.options[selectedIndex].text;

    underValue = myGroup.options[selectedIndex + 1].value;
    underText  = myGroup.options[selectedIndex + 1].text;

    myGroup.options[selectedIndex + 1].value    = selectedValue;
    myGroup.options[selectedIndex + 1].text     = selectedText;
    myGroup.options[selectedIndex + 1].selected = true;

    myGroup.options[selectedIndex].value    = underValue;
    myGroup.options[selectedIndex].text     = underText;
    myGroup.options[selectedIndex].selected = false;

    setMyGroupList();
}

function updateMyGroupName() {
    var mygroup_id = '';
    if (document.subform.elements['mygroup_ids[]']) {
        if (document.subform.elements['mygroup_ids[]'].length) {
            for (var i = 0, j = document.subform.elements['mygroup_ids[]'].length; i < j; i++) {
                if (document.subform.elements['mygroup_ids[]'][i].checked) {
                    mygroup_id = document.subform.elements['mygroup_ids[]'][i].value;
                    break;
                }
            }
        } else if (document.subform.elements['mygroup_ids[]'].checked) {
            mygroup_id = document.subform.elements['mygroup_ids[]'].value;
        }
    }

    if (mygroup_id == '') {
        alert('編集対象が選択されていません。');
        return;
    }

    window.open('schedule_mygroup_name_update.php?session=<?php eh($session); ?>&mygroup_id='.concat(mygroup_id), 'newwin', 'width=640,height=480,scrollbars=yes');
}

function registerMyGroupName() {
    window.open('schedule_mygroup_name_register.php?session=<?php eh($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteMyGroupName() {
    var selected = false;
    if (document.subform.elements['mygroup_ids[]']) {
        if (document.subform.elements['mygroup_ids[]'].length) {
            for (var i = 0, j = document.subform.elements['mygroup_ids[]'].length; i < j; i++) {
                if (document.subform.elements['mygroup_ids[]'][i].checked) {
                    selected = true;
                    break;
                }
            }
        } else if (document.subform.elements['mygroup_ids[]'].checked) {
            selected = true;
        }
    }

    if (!selected) {
        alert('削除対象が選択されていません。');
        return;
    }

    if (confirm('削除します。よろしいですか？')) {
        document.subform.submit();
    }
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><img src="img/icon/b02.gif" width="32" height="32" border="0" alt="スケジュール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php eh($default_view); ?>?session=<?php eh($session); ?>"><b>スケジュール</b></a></font></td>
<? if ($schd_place_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="schedule_place_list.php?session=<?php eh($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #5279a5; margin-bottom:5px;">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_week_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_month_menu.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5"></td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="schedule_year_menu.php?session=<?php eh($session); ?>&year=<?php eh($year); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_list_upcoming.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5"></td>
<td width="160" align="center" bgcolor="#bdd1e7"><a href="schedule_search.php?session=<?php eh($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開スケジュール検索</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#5279a5"><a href="schedule_mygroup.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>マイグループ</b></font></a></td>
<td width="5"></td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="schedule_ical.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カレンダー連携</font></a></td>
<td width="5"></td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="schedule_option.php?session=<?php eh($session); ?>&date=<?php eh($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td>
<form name="subform" action="schedule_mygroup_name_delete.php" method="post">
<table width="150" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
マイグループ名<br>
<input type="button" value="編集" onclick="updateMyGroupName();">
<input type="button" value="作成" onclick="registerMyGroupName();">
<input type="button" value="削除" onclick="deleteMyGroupName();">
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<?
if ($mygroup_id == 0) {
    echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成されていません。</font>");
} else {
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
    foreach ($mygroups as $tmp_mygroup_id => $tmp_mygruop_nm) {
        echo("<tr height=\"22\" valign=\"middle\">\n");
        echo("<td width=\"1\"><input type=\"checkbox\" name=\"mygroup_ids[]\" value=\"$tmp_mygroup_id\"></td>\n");
        echo("");
        if ($tmp_mygroup_id != $mygroup_id) {
            echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"schedule_mygroup.php?session=$session&mygroup_id=$tmp_mygroup_id\">$tmp_mygruop_nm</a></font></td>\n");
        } else {
            echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_mygruop_nm</font></td>\n");
        }
        echo("</tr>\n");
    }
    echo("</table>\n");
}
?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
</form>
</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>
<form name="mainform" action="schedule_mygroup_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録対象職員</font></td>
<td width="480">
<table width="480" border="0" cellspacing="0" cellpadding="0">
<tr height="2">
<td colspan="4"></td>
</tr>
<tr>
<td width="30"></td>
<td width="150" valign="bottom"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象</font></td>
<td width="80"></td>
<td width="220">
</td>
</tr>
<tr>
<td align="center"><input type="button" value="↑" onclick="raiseEmployee();"><br><br><input type="button" value="↓" onclick="dropEmployee();"></td>
<td>
<select name="mygroup" size="10" style="width:150px;">
<? show_mygroup_options($arr_mygroup); ?>
</select>
</td>
<td align="center">
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');">
<br><br>
<input type="button" value="削除 &gt;" style="margin-left:2em;width=5.5em;" onclick="deleteEmp();">
</td>
<td>
</td>
</tr>
<tr height="22">
<td></td>
<td><input type="button" value="全て消去" onclick="deleteAllEmp();"></td>
<td></td>
</tr>
<tr height="2">
<td colspan="4"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新" onclick="closeEmployeeList();" <? if ($mygroup_id == 0) {echo(" disabled");} ?>></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="mygroup_list" value="<? show_mygroup_list($arr_mygroup); ?>">
<input type="hidden" name="mygroup_id" value="<?php eh($mygroup_id); ?>">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_mygroup_options($arr_mygroup) {
    foreach ($arr_mygroup as $row) {
        echo("<option value=\"{$row["id"]}\">{$row["name"]}\n");
    }
}

function show_mygroup_list($arr_mygroup) {
    for ($i = 0; $i < count($arr_mygroup); $i++) {
        if ($i > 0) {
            echo(",");
        }
        echo($arr_mygroup[$i]["id"]);
    }
}