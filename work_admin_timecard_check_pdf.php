<?php

ob_start(); 
require_once("work_admin_timecard_check_class.php");
require_once("timecard_common_class.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_timecard_common.ini");
require_once("show_select_values.ini");
require_once('fpdf153/mbfpdf.php');
ob_end_clean();

$title_name = $title . "一覧";
// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$fname = $PHP_SELF;
// データベースに接続
$con = connect2db($fname);

// 20131210
if($shift_flg == "1"){
	$shift_groups = array();
	$arr_emp_id = explode("," , $emp_id_list);
	$tmp_emp_id = implode("','", $arr_emp_id);
	
	$sql = "select s.emp_id, g.group_id, g.group_name from duty_shift_staff s ";
	$sql .= "inner join duty_shift_group g on s.group_id = g.group_id";
	$cond = "where s.emp_id in ('$tmp_emp_id')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo ("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)){
		$tmp_id = $row["emp_id"];
		$tmp_group_id = $row["group_id"];
		$tmp_group_name = $row["group_name"];
		$shift_groups[$tmp_id] = array("id"=>$tmp_group_id, "name"=>$tmp_group_name);
	}
}


// 組織階層情報取得
$arr_org_level = array ();
$level_cnt = 0;
$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname ";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo ("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo ("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$arr_org_level[0] = pg_result($sel, 0, "class_nm");
$arr_org_level[1] = pg_result($sel, 0, "atrb_nm");
$arr_org_level[2] = pg_result($sel, 0, "dept_nm");
$arr_org_level[3] = pg_result($sel, 0, "room_nm");

//チェック項目の情報を取得する
$cls_timecard = new check_timecard();
$arr_sortkey = array($sortkey1, $sortkey2, $sortkey3,$sortkey4,$sortkey5);
$list_info = $cls_timecard->get_show_list($con, $fname, $emp_id_list, $start_date, $end_date, $menuname, $arr_sortkey);

//日付並び順にする
foreach ($list_info as $key=>$item) {
	ksort($list_info[$key]["date_err"]);
}

//データを加工する
if ($err_msg) {
	foreach ($list_info as $key=>$item) {
		foreach ($item["date_err"] as $err_key=>$err){
			foreach ($err as $msg_key=>$msg){
				if ($msg_key != $err_msg){
					unset($list_info[$key]["date_err"][$err_key][$msg_key]);
				}
			}

			if (empty($list_info[$key]["date_err"][$err_key])) {
				unset($list_info[$key]["date_err"][$err_key]);
			}
		}
	}
}

$arr_width = array();
$arr_list = array();
//タイトル
if ($shift_flg == 1){
	$arr_list[] = array("id"=>"職員ID", "name"=>"職員氏名", "dep"=>"部署",  "shift"=>"シフトグループ", "date"=>"日付", "msg"=>"エラー内容");
	$arr_width = array("id"=>25, "name"=>30, "dep"=>90, "shift"=>50, "date"=>17, "msg"=>65);
}else{
	$arr_list[] = array("id"=>"職員ID", "name"=>"職員氏名", "dep"=>"部署", "date"=>"日付", "msg"=>"エラー内容");
	$arr_width = array("id"=>35, "name"=>40, "dep"=>100, "date"=>22, "msg"=>80);
}

//内容
foreach ($list_info as $key=>$item) {
	$emp_id 		= $item["emp_personal_id"];
	$emp_name 		= $item["emp_name"];
	$emp_department = $item["emp_department"];
	$shift_name = ($shift_groups[$key]) ? $shift_groups[$key]["name"] : "";

	foreach ($item["date_err"] as $err_key=>$err){
		$error_msg = implode("、", $err); //エラー内容
		$tmp_date = date("m月d日",strtotime($err_key));
		
		if ($shift_flg == "1"){
			$arr_list[] = array("id"=>$emp_id, "name"=>$emp_name, "dep"=>$emp_department,  "shift"=>$shift_name, "date"=>$tmp_date, "msg"=>$error_msg);
		}else{
			$arr_list[] = array("id"=>$emp_id, "name"=>$emp_name, "dep"=>$emp_department, "date"=>$tmp_date, "msg"=>$error_msg);
		}
	}
}

//A4横
$pdf = new MBFPDF('L', 'mm', 'A4');
$pdf->SetAutoPageBreak(true, 5);
$pdf->AddMBFont(MINCHO,'EUC-JP');
$pdf->AddMBFont(GOTHIC,'EUC-JP');
$pdf->Open();

output_pdf($pdf, $title_name, $arr_list, $arr_width);

function output_pdf(&$pdf, $title, $arr_list, $arr_width){
	
	$left_margin = 9;
	
	$pdf->AddPage();
	$pdf->setY(10);
	$pdf->SetFont(GOTHIC, '', 12);
	
	$pdf->Cell(0, 2.0, $title, '', 0, 'C');
	$pdf->SetFont(GOTHIC, '', 9);
	
	$pdf->setXY($left_margin,16);
	
	// Cell(幅、高さ、文字列、境界線、次の位置）
	foreach ($arr_list as $list){
		$pdf->setX($left_margin);
		
		foreach ($list as $key=>$item){
			$wd = $arr_width[$key];
			$next_flg = ($key == "msg") ? 1 : 0;
			$pdf->cell($wd, 5.0, $item, '1', $next_flg, 'L');
		}
	}
	
	$pdf->setXY($left_margin, 285);
}


pg_close($con); 

$pdf->Output();

die;
?>

