<?
require_once("about_comedix.php");
require_once("fplus_common_class.php");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}



// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 承認状況チェック
$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert('他の受付状況が変更されたため、報告取消ができません。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 申請論理削除
$obj->update_delflg_all_apply($apply_id, "t");

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

?>