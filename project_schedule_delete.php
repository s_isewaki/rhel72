<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
$fname = $PHP_SELF;

require_once("Cmx.php");
require_once("aclg_set.php");
require("about_postgres.php");

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

// トランザクションを開始
pg_query($con, "begin");

// スケジュール情報を削除
$sql = "delete from proschd";
$cond = " where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "delete from proschd2";
$cond = " where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 議事録情報を削除
$sql = "delete from proceeding";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 添付ファイル情報を削除
$sql = "delete from prcdfile";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認情報を削除
$sql = "delete from prcdaprv";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 出席者情報を削除
$sql = "delete from prcdatnd";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 議事録添付ファイルがあれば削除
if (!is_dir("proceeding")) {
	mkdir("proceeding", 0755);
}
foreach (glob("proceeding/{$pjt_schd_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 画面遷移
switch ($path) {
case "1":
	$url = "project_schedule_list_all.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";
	break;
case "2":
	$url = "project_schedule_list_upcoming.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";
	break;
case "3":
	$url = "project_schedule_list_past.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";
	break;
case "4":
	$url = "project_schedule_week_menu.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";
	break;
case "5":
	$url = "schedule_week_menu.php?session=$session&date=$date";
	break;
case "6":
	$url = "schedule_menu.php?session=$session&date=$date";
	break;
case "7":
	$url = "project_schedule_menu.php?session=$session&pjt_id=$pjt_id&date=$date&adm_flg=$adm_flg";
	break;
default:
	$url = $ret_url;
	break;
}
echo("<script type=\"text/javascript\">location.href = '$url';</script>");
?>
