<?
require("about_session.php");
require("about_authority.php");
require("employee_auth_common.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// トランザクションの開始
pg_query("begin");
//XX mysql_query("start transaction", $con_mysql);

// 権限グループを更新
//XX auth_change_group_of_employee($con, $con_mysql, $emp_id, $group_id, $fname);
auth_change_group_of_employee($con, $emp_id, $group_id, $fname);

// 追加権限情報を配列化
$emp_auth = array();
auth_set_overwrite($webml,      "emp_webml_flg",     $emp_auth);
auth_set_overwrite($bbs,        "emp_bbs_flg",       $emp_auth);
auth_set_overwrite($attditem,   "emp_attditem_flg",  $emp_auth);
auth_set_overwrite($schd,       "emp_schd_flg",      $emp_auth);
auth_set_overwrite($wkflw,      "emp_wkflw_flg",     $emp_auth);
auth_set_overwrite($phone,      "emp_phone_flg",     $emp_auth);
auth_set_overwrite($attdcd,     "emp_attdcd_flg",    $emp_auth);
auth_set_overwrite($qa,         "emp_qa_flg",        $emp_auth);
auth_set_overwrite($aprv,       "emp_aprv_flg",      $emp_auth);
auth_set_overwrite($pass,       "emp_pass_flg",      $emp_auth);
auth_set_overwrite($adbk,       "emp_adbk_flg",      $emp_auth);
auth_set_overwrite($empif,      "emp_empif_flg",     $emp_auth);
auth_set_overwrite($reg,        "emp_reg_flg",       $emp_auth);
auth_set_overwrite($conf,       "emp_conf_flg",      $emp_auth);
auth_set_overwrite($resv,       "emp_resv_flg",      $emp_auth);
auth_set_overwrite($tmgd,       "emp_tmgd_flg",      $emp_auth);
auth_set_overwrite($schdplc,    "emp_schdplc_flg",   $emp_auth);
auth_set_overwrite($ward,       "emp_ward_flg",      $emp_auth);
auth_set_overwrite($ward_reg,   "emp_ward_reg_flg",  $emp_auth);
auth_set_overwrite($ptreg,      "emp_ptreg_flg",     $emp_auth);
auth_set_overwrite($ptif,       "emp_ptif_flg",      $emp_auth);
auth_set_overwrite($dept,       "emp_dept_flg",      $emp_auth);
auth_set_overwrite($news,       "emp_news_flg",      $emp_auth);
auth_set_overwrite($master,     "emp_master_flg",    $emp_auth);
auth_set_overwrite($job,        "emp_job_flg",       $emp_auth);
auth_set_overwrite($status,     "emp_status_flg",    $emp_auth);
auth_set_overwrite($entity,     "emp_entity_flg",    $emp_auth);
auth_set_overwrite($work,       "emp_work_flg",      $emp_auth);
auth_set_overwrite($pjt,        "emp_pjt_flg",       $emp_auth);
auth_set_overwrite($lib,        "emp_lib_flg",       $emp_auth);
auth_set_overwrite($config,     "emp_config_flg",    $emp_auth);
auth_set_overwrite($biz,        "emp_biz_flg",       $emp_auth);
auth_set_overwrite($inout,      "emp_inout_flg",     $emp_auth);
auth_set_overwrite($dishist,    "emp_dishist_flg",   $emp_auth);
auth_set_overwrite($outreg,     "emp_outreg_flg",    $emp_auth);
auth_set_overwrite($disreg,     "emp_disreg_flg",    $emp_auth);
auth_set_overwrite($hspprf,     "emp_hspprf_flg",    $emp_auth);
auth_set_overwrite($lcs,        "emp_lcs_flg",       $emp_auth);
auth_set_overwrite($fcl,        "emp_fcl_flg",       $emp_auth);
auth_set_overwrite($wkadm,      "emp_wkadm_flg",     $emp_auth);
auth_set_overwrite($cmt,        "emp_cmt_flg",       $emp_auth);
auth_set_overwrite($cmtadm,     "emp_cmtadm_flg",    $emp_auth);
auth_set_overwrite($memo,       "emp_memo_flg",      $emp_auth);
auth_set_overwrite($newsuser,   "emp_newsuser_flg",  $emp_auth);
auth_set_overwrite($inci,       "emp_inci_flg",      $emp_auth);
auth_set_overwrite($rm,         "emp_rm_flg",        $emp_auth);
auth_set_overwrite($fplus,      "emp_fplus_flg",     $emp_auth);
auth_set_overwrite($fplusadm,   "emp_fplusadm_flg",  $emp_auth);
auth_set_overwrite($ext,        "emp_ext_flg",       $emp_auth);
auth_set_overwrite($extadm,     "emp_extadm_flg",    $emp_auth);
auth_set_overwrite($intram,     "emp_intram_flg",    $emp_auth);
auth_set_overwrite($stat,       "emp_stat_flg",      $emp_auth);
auth_set_overwrite($allot,      "emp_allot_flg",     $emp_auth);
auth_set_overwrite($life,       "emp_life_flg",      $emp_auth);
auth_set_overwrite($intra,      "emp_intra_flg",     $emp_auth);
auth_set_overwrite($ymstat,     "emp_ymstat_flg",    $emp_auth);
auth_set_overwrite($med,        "emp_med_flg",       $emp_auth);
auth_set_overwrite($qaadm,      "emp_qaadm_flg",     $emp_auth);
auth_set_overwrite($medadm,     "emp_medadm_flg",    $emp_auth);
auth_set_overwrite($link,       "emp_link_flg",      $emp_auth);
auth_set_overwrite($linkadm,    "emp_linkadm_flg",   $emp_auth);
auth_set_overwrite($bbsadm,     "emp_bbsadm_flg",    $emp_auth);
auth_set_overwrite($libadm,     "emp_libadm_flg",    $emp_auth);
auth_set_overwrite($adbkadm,    "emp_adbkadm_flg",   $emp_auth);
auth_set_overwrite($search,     "emp_search_flg",    $emp_auth);
auth_set_overwrite($webmladm,   "emp_webmladm_flg",  $emp_auth);
auth_set_overwrite($shift,      "emp_shift_flg",     $emp_auth);
auth_set_overwrite($shiftadm,   "emp_shiftadm_flg",  $emp_auth);
auth_set_overwrite($cas,        "emp_cas_flg",       $emp_auth);
auth_set_overwrite($casadm,     "emp_casadm_flg",    $emp_auth);
auth_set_overwrite($jnl,        "emp_jnl_flg",       $emp_auth);
auth_set_overwrite($jnladm,     "emp_jnladm_flg",    $emp_auth);
auth_set_overwrite($nlcs,       "emp_nlcs_flg",      $emp_auth);
auth_set_overwrite($nlcsadm,    "emp_nlcsadm_flg",   $emp_auth);
auth_set_overwrite($manabu,     "emp_manabu_flg",    $emp_auth);
auth_set_overwrite($manabuadm,  "emp_manabuadm_flg", $emp_auth);
auth_set_overwrite($kview,      "emp_kview_flg",     $emp_auth);
auth_set_overwrite($kviewadm,   "emp_kviewadm_flg",  $emp_auth);
auth_set_overwrite($ptadm,      "emp_ptadm_flg",     $emp_auth);
auth_set_overwrite($pjtadm,     "emp_pjtadm_flg",    $emp_auth);
auth_set_overwrite($amb,        "emp_amb_flg",       $emp_auth);
auth_set_overwrite($ambadm,     "emp_ambadm_flg",    $emp_auth);
auth_set_overwrite($jinji,      "emp_jinji_flg",     $emp_auth);
auth_set_overwrite($jinjiadm,   "emp_jinjiadm_flg",  $emp_auth);
auth_set_overwrite($ladder,     "emp_ladder_flg",    $emp_auth);
auth_set_overwrite($ladderadm,  "emp_ladderadm_flg", $emp_auth);
auth_set_overwrite($shift2,     "emp_shift2_flg",    $emp_auth);
auth_set_overwrite($shift2adm,  "emp_shift2adm_flg", $emp_auth);
auth_set_overwrite($cauth,      "emp_cauth_flg",     $emp_auth);
auth_set_overwrite($accesslog,  "emp_accesslog_flg", $emp_auth);
auth_set_overwrite($career,     "emp_career_flg",    $emp_auth);
auth_set_overwrite($careeradm,  "emp_careeradm_flg", $emp_auth);
auth_set_overwrite($ccusr1,     "emp_ccusr1_flg",    $emp_auth);
auth_set_overwrite($ccadm1,     "emp_ccadm1_flg",    $emp_auth);

// 追加権限情報を更新
//XX auth_change_extra_of_employee($con, $con_mysql, $emp_id, $emp_auth, $fname);
auth_change_extra_of_employee($con, $emp_id, $emp_auth, $fname);

// 整合性のチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname);
auth_check_consistency($con, $session, $fname);

// トランザクションのコミット
pg_query($con, "commit");
//XX mysql_query("commit", $con_mysql);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 権限登録画面を再表示
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);
echo("<script type=\"text/javascript\">location.href = 'employee_auth_setting.php?session=$session&emp_id=$emp_id&key1=$url_key1&key2=$url_key2&class_cond=$class_cond&atrb_cond=$atrb_cond&dept_cond=$dept_cond&room_cond=$room_cond&job_cond=$job_cond&page=$page&view=$view&emp_o=$emp_o';</script>");
