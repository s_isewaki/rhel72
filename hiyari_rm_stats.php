<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("about_comedix.php");
require_once("hiyari_report_class.php");
require_once("smarty_setting.ini");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_rm_stats_util.ini");
require_once("get_values.php");
require_once("hiyari_inci_item_2010_models.php");
require_once("hiyari_item_time_setting_models.php");
require_once("hiyari_item_timelag_setting_models.php");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("conf/conf.inf");
require_once("hiyari_report_classification.php");
$fname = $PHP_SELF;

//==================================================
// セッションのチェック
//==================================================
$session = qualify_session($session, $fname);
if ($session == "0") {
    showLoginPage();
    exit;
}

//==================================================
// 権限のチェック
//==================================================
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    showLoginPage();
    exit;
}

//==================================================
// データベースに接続
//==================================================
$con = connect2db($fname);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

$sub_report_use_flg_tmp = $sysConf->get('fantol.aggregate.subreport');
//==================================================
// 専用セッションを使用する。(破棄は明示的にせず、タイムアウトに従う。)
// ※報告書登録系でも別セッションを使用している。
//==================================================
session_cache_limiter('');
session_name("hiyari_stats_sid");
session_start();

//==================================================
//ログインユーザーID
//==================================================
$emp_id = get_emp_id($con,$session,$fname);

//==================================================
//入力方法 時間帯 or 時刻
//==================================================
// 設定情報を取得
$time_setting_flg = getTimeSetting($con, $PHP_SELF);

//==================================================
//軸情報作成
//==================================================

// 統計対象レポート閲覧制限区分取得
$stats_read_div = get_stats_read_div($session,$fname,$con);

// 組織階層情報取得
$arr_class_name = get_class_name_array($con, $fname);
$class_cnt = $arr_class_name["class_cnt"];

// 報告書マスタの内容を取得
$rep_obj = new hiyari_report_class($con, $fname);
$grps = $rep_obj->get_all_easy_items();

//縦軸横軸の表示フラグ
$axises_array = $rep_obj->get_axises();

// $eis_idが空なら取得する 20090910
if($session) $emp_id = get_emp_id( $con, $session, $fname );
if($emp_id) $sel_job = get_own_job( $con, $emp_id, $fname );
if($sel_job) $sel_eis_no = $rep_obj->get_default_eis_no_from_job($sel_job);
if($sel_eis_no) $eis_id = $rep_obj->get_eis_id_from_eis_no($sel_eis_no);

// 様式ごとにタイトルを変更する 20090910
if(!empty($eis_id)) {
    $sql  = "select * from inci_item_title ";
    $cond = "where eis_id = " . pg_escape_string($eis_id);
    $result = select_from_table( $con, $sql, $cond, $fname );
    if ($result == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        showErrorPage();
        exit;
    }

    $tmp_eis_item_title = pg_fetch_all($result);

    // 文言変更データを修正する
    foreach($tmp_eis_item_title as $val) {
        $eis_item_title[$val['eis_column']] = $val['eis_title'];
    }
}

//自分の部門しか参照できない場合を除いて部門の集計を可能にする。
$priority_auth = get_emp_priority_auth($session,$fname,$con);
$is_emp_auth_system_all = is_emp_auth_system_all($emp_id, $priority_auth, $con, $fname);

// 20090910
unset($item_title);


//復活

if(!empty($eis_id) && $eis_item_title[120]) {
    $summary_title = $eis_item_title[120];
}else {
    $summary_title = $grps[120]["easy_item_list"][80]["easy_item_name"];
}

if(!empty($eis_id) && $eis_item_title[125]) {
    $hiyari_summary_title = $eis_item_title[125];
}else {
    $hiyari_summary_title = $grps[125]["easy_item_list"][85]["easy_item_name"];
}

if(!empty($eis_id) && $eis_item_title[240]) {
    $patient_division_2_title = $eis_item_title[240];
}else {
    $patient_division_2_title = $grps[240]["easy_item_list"][70]["easy_item_name"];
}

if(!empty($eis_id) && $eis_item_title[900]) {
    $scene_content_2010_title = $eis_item_title[900];
}else {
    $scene_content_2010_title = "概要2010年版";
}

// タイムラグの表示フラグ取得
$disp_taimelag_flg = getDispTimelagSetting($con, $fname);

$vertical_axises = array();
foreach ($axises_array as $title_array) {
    if($title_array["axises_flg"] == 't'){
       // $title_key = $title_array["key"];
        if(strstr($title_array["key"],'time')){
            // 時間の入力方法が時間帯（2時間毎）のとき
            if ( $time_setting_flg === true ) {
                if($title_array["key"] == "time" ||$title_array["key"] == "time3"|| $title_array["key"] == "timelag_class" ){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"]);
                    }else{
                        if( $title_array["key"] == "time" ){
                            $vertical_axises += array($title_array["key"] => $grps[100]["easy_item_list"][40]["easy_item_name"]);

                        }else if( $title_array["key"] == "time3" ){
                            $vertical_axises += array($title_array["key"] => $grps[105]["easy_item_list"][40]["easy_item_name"]);
                        }else if($title_array["key"] == "timelag_class"){
                            if ($disp_taimelag_flg == true) {
                                $vertical_axises += array($title_array["key"] =>'発見から報告までの時間');
                            }
                        }
                    }
                }
            }else{
                if($title_array["key"] == "time" ){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"].":2時間毎");
                    }else{
                        $vertical_axises += array($title_array["key"] => $grps[100]["easy_item_list"][40]["easy_item_name"].":2時間毎");
                    }
                }else  if($title_array["key"] == "time3"){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"].":2時間毎");
                    }else{
                        $vertical_axises += array($title_array["key"] => $grps[105]["easy_item_list"][40]["easy_item_name"].":2時間毎");
                    }
                }else  if($title_array["key"] == "time2"){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"].":1時間毎");
                    }else{
                        $vertical_axises += array($title_array["key"] => $grps[100]["easy_item_list"][40]["easy_item_name"].":1時間毎");
                    }
                }else  if($title_array["key"] == "time4"){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"].":1時間毎");
                    }else{
                        $vertical_axises += array($title_array["key"] => $grps[105]["easy_item_list"][40]["easy_item_name"].":1時間毎");
                    }
                }else  if($title_array["key"] == "timelag_class"){
                    if($title_array["title"]){
                        $vertical_axises += array($title_array["key"] => $title_array["title"]);
                    }else{
                        $vertical_axises += array($title_array["key"] =>'発見から報告までの時間');
                    }
                }
            }

        }else{
            if($title_array["title"]){
                $vertical_axises += array($title_array["key"] => $title_array["title"]);
            }else{
                if( $title_array["key"] == "level" ){
                    $vertical_axises += array($title_array["key"] => $grps[90]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "place"){
                    $vertical_axises += array($title_array["key"] => $grps[110]["easy_item_list"][60]["easy_item_name"]);
                }else if($title_array["key"] == "generating_post"){
                    $vertical_axises += array($title_array["key"] => $grps[116]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "month"){
                    $vertical_axises += array($title_array["key"] => $grps[100]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "weekday"){
                    $vertical_axises += array($title_array["key"] => $grps[100]["easy_item_list"][20]["easy_item_name"]);
                }else if($title_array["key"] == "report_month"){
                    $vertical_axises += array($title_array["key"] => "報告月");
                }else if($title_array["key"] == "registrant_job"){
                    $vertical_axises += array($title_array["key"] => "報告者職種");
                }else if($title_array["key"] == "d_job"){
                    $vertical_axises += array($title_array["key"] => $grps[3020]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "d_job_year"){
                    $vertical_axises += array($title_array["key"] => $grps[3036]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "d_class_year"){
                    $vertical_axises += array($title_array["key"] => $grps[3038]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "t_job"){
                    $vertical_axises += array($title_array["key"] => $grps[3050]["easy_item_list"][30]["easy_item_name"]);
                }else if($title_array["key"] == "job_year"){
                    $vertical_axises += array($title_array["key"] => $grps[3250]["easy_item_list"][70]["easy_item_name"]);
                }else if($title_array["key"] == "t_night_count"){
                    $vertical_axises += array($title_array["key"] => $grps[3350]["easy_item_list"][110]["easy_item_name"]);
                }else if($title_array["key"] == "class_year"){
                    $vertical_axises += array($title_array["key"] => $grps[3300]["easy_item_list"][90]["easy_item_name"]);
                }else if($title_array["key"] == "patient_sex"){
                    $vertical_axises += array($title_array["key"] => $grps[210]["easy_item_list"][30]["easy_item_name"]);
                }else if($title_array["key"] == "patient_year"){
                    $vertical_axises += array($title_array["key"] => $grps[210]["easy_item_list"][40]["easy_item_name"]);
                }else if($title_array["key"] == "patient_division_2"){
                    $vertical_axises += array($title_array["key"] => $patient_division_2_title);
                }else if($title_array["key"] == "assessment_score"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][3]["easy_item_name"]);
                }else if($title_array["key"] == "dangerous"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_6"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][6]["easy_item_name"]);
                }else if($title_array["key"] == "restrict"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][7]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_8"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][8]["easy_item_name"]);
                }else if($title_array["key"] == "risyou_sensor"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][5]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_20"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][20]["easy_item_name"]);
                }else if($title_array["key"] == "life_system"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][30]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_40"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][40]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_50"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][50]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_60"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][60]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_70"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][70]["easy_item_name"]);
                }else if($title_array["key"] == "asses_150_80"){
                    $vertical_axises += array($title_array["key"] => $grps[150]["easy_item_list"][80]["easy_item_name"]);
                }else if($title_array["key"] == "ic"){
                    $vertical_axises += array($title_array["key"] => '影響と対応');
                }else if($title_array["key"] == "patient_state"){
                    $vertical_axises += array($title_array["key"] => $grps[290]["easy_item_list"][120]["easy_item_name"]);
                }else if($title_array["key"] == "summary"){
                    $vertical_axises += array($title_array["key"] => $summary_title);
                }else if($title_array["key"] == "hiyari_summary"){
                    $vertical_axises += array($title_array["key"] => $hiyari_summary_title);
                }else if($title_array["key"] == "scene_content"){
                    $vertical_axises += array($title_array["key"] => "発生場面と内容");
                }else if($title_array["key"] == "scene_content_2010"){
                    $vertical_axises += array($title_array["key"] => $scene_content_2010_title);
                }else if($title_array["key"] == "factor"){
                    $vertical_axises += array($title_array["key"] => $grps[600]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "clinic"){
                    $vertical_axises += array($title_array["key"] => $grps[130]["easy_item_list"][90]["easy_item_name"]);
                }else if($title_array["key"] == "risk_juudai"){
                    $vertical_axises += array($title_array["key"] =>$grps[1500]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "risk_kinkyu"){
                    $vertical_axises += array($title_array["key"] =>$grps[1502]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "risk_hindo"){
                    $vertical_axises += array($title_array["key"] =>$grps[1504]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "risk_yosoku"){
                    $vertical_axises += array($title_array["key"] =>$grps[1510]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "system_kaizen"){
                    $vertical_axises += array($title_array["key"] =>$grps[1520]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "kyouiku_kenshu"){
                    $vertical_axises += array($title_array["key"] =>$grps[1530]["easy_item_list"][10]["easy_item_name"]);
                }else if($title_array["key"] == "post"){
                    if ( ( $stats_read_div == 0 )
                        || ( $stats_read_div >= 1 && $stats_read_div  <= 4 && $is_emp_auth_system_all )) {
                        $vertical_axises += array($title_array["key"] => $arr_class_name[0]);
                    }
                }else if($title_array["key"] == "post_dept"){
                    if ( ( $stats_read_div == 0 )
                        || ( $stats_read_div >= 1 && $stats_read_div  <= 4 && $is_emp_auth_system_all )) {

                        $vertical_axises += array($title_array["key"] =>'組織階層3');
                    }
                    
                }else if(strpos($title_array["key"], 'user_item_')===0){
                    $vertical_axises += array($title_array["key"] => $title_array["default_title"]);
                }
            }
        }
    }
}


//}// 復活ここまで

//復活
// *** 軸項目名 ***
/*
// 時間の入力方法が時間帯（2時間毎）のとき
if ( $time_setting_flg === true ) {
    $vertical_axises = array(
        "level"                 => $grps[90]["easy_item_list"][10]["easy_item_name"],
        "place"                 => $grps[110]["easy_item_list"][60]["easy_item_name"],
        "generating_post"                  => $grps[116]["easy_item_list"][10]["easy_item_name"],
        "month"                 => $grps[100]["easy_item_list"][10]["easy_item_name"],
        "weekday"               => $grps[100]["easy_item_list"][20]["easy_item_name"],
        "time"                  => $grps[100]["easy_item_list"][40]["easy_item_name"],
        "time3"                 => $grps[105]["easy_item_list"][40]["easy_item_name"],
        "report_month"          => "報告月",
        "registrant_job"        => "報告者職種",

        "d_job"                 => $grps[3020]["easy_item_list"][10]["easy_item_name"],
        "d_job_year"            => $grps[3036]["easy_item_list"][10]["easy_item_name"],
        "d_class_year"          => $grps[3038]["easy_item_list"][10]["easy_item_name"],

        "t_job"                 => $grps[3050]["easy_item_list"][30]["easy_item_name"],
        "job_year"              => $grps[3250]["easy_item_list"][70]["easy_item_name"],
        "t_night_count"         => $grps[3350]["easy_item_list"][110]["easy_item_name"],
        "class_year"            => $grps[3300]["easy_item_list"][90]["easy_item_name"],

        "patient_sex"           => $grps[210]["easy_item_list"][30]["easy_item_name"],
        "patient_year"          => $grps[210]["easy_item_list"][40]["easy_item_name"],
        "patient_division_2"    => $patient_division_2_title,

        "assessment_score"      => $grps[150]["easy_item_list"][3]["easy_item_name"],
        "dangerous"             => $grps[150]["easy_item_list"][10]["easy_item_name"],
        "asses_150_6"           => $grps[150]["easy_item_list"][6]["easy_item_name"],
        "restrict"              => $grps[150]["easy_item_list"][7]["easy_item_name"],
        "asses_150_8"           => $grps[150]["easy_item_list"][8]["easy_item_name"],
        "risyou_sensor"         => $grps[150]["easy_item_list"][5]["easy_item_name"],
        "asses_150_20"          => $grps[150]["easy_item_list"][20]["easy_item_name"],
        "life_system"           => $grps[150]["easy_item_list"][30]["easy_item_name"],
        "asses_150_40"          => $grps[150]["easy_item_list"][40]["easy_item_name"],
        "asses_150_50"          => $grps[150]["easy_item_list"][50]["easy_item_name"],
        "asses_150_60"          => $grps[150]["easy_item_list"][60]["easy_item_name"],
        "asses_150_70"          => $grps[150]["easy_item_list"][70]["easy_item_name"],
        "asses_150_80"          => $grps[150]["easy_item_list"][80]["easy_item_name"],

        "ic"                    => "影響と対応",

        "patient_state"         => $grps[290]["easy_item_list"][120]["easy_item_name"],
        "summary"               => $summary_title,
        "hiyari_summary"        => $hiyari_summary_title,
        "scene_content"         => "発生場面と内容",
        "scene_content_2010"    => $scene_content_2010_title,
        "factor"                => $grps[600]["easy_item_list"][10]["easy_item_name"],
        "clinic"                => $grps[130]["easy_item_list"][90]["easy_item_name"],

        "risk_juudai"           => $grps[1500]["easy_item_list"][10]["easy_item_name"], // リスクの評価：重大性
        "risk_kinkyu"           => $grps[1502]["easy_item_list"][10]["easy_item_name"], // リスクの評価：緊急性
        "risk_hindo"            => $grps[1504]["easy_item_list"][10]["easy_item_name"], // リスクの評価：頻度
        "risk_yosoku"           => $grps[1510]["easy_item_list"][10]["easy_item_name"], // リスクの予測
        "system_kaizen"         => $grps[1520]["easy_item_list"][10]["easy_item_name"], // システム改善の必要性
        "kyouiku_kenshu"        => $grps[1530]["easy_item_list"][10]["easy_item_name"], // 教育研修への活用
    );
}

// 時間の入力方法が時刻（時・分）のとき
else {
    $vertical_axises = array(
        "level"                 => $grps[90]["easy_item_list"][10]["easy_item_name"],
        "place"                 => $grps[110]["easy_item_list"][60]["easy_item_name"],
        "generating_post"                  => $grps[116]["easy_item_list"][10]["easy_item_name"],
        "month"                 => $grps[100]["easy_item_list"][10]["easy_item_name"],
        "weekday"               => $grps[100]["easy_item_list"][20]["easy_item_name"],
        "time"                  => "発生時間帯：2時間毎",
        "time2"                 => "発生時間帯：1時間毎",
        "time3"                 => "発見時間帯：2時間毎",
        "time4"                 => "発見時間帯：1時間毎",
        "report_month"          => "報告月",
        "registrant_job"        => "報告者職種",

        "d_job"                 => $grps[3020]["easy_item_list"][10]["easy_item_name"],
        "d_job_year"            => $grps[3036]["easy_item_list"][10]["easy_item_name"],
        "d_class_year"          => $grps[3038]["easy_item_list"][10]["easy_item_name"],

        "t_job"                 => $grps[3050]["easy_item_list"][30]["easy_item_name"],
        "job_year"              => $grps[3250]["easy_item_list"][70]["easy_item_name"],
        "t_night_count"         => $grps[3350]["easy_item_list"][110]["easy_item_name"],
        "class_year"            => $grps[3300]["easy_item_list"][90]["easy_item_name"],

        "patient_sex"           => $grps[210]["easy_item_list"][30]["easy_item_name"],
        "patient_year"          => $grps[210]["easy_item_list"][40]["easy_item_name"],
        "patient_division_2"    => $patient_division_2_title,

        "assessment_score"      => $grps[150]["easy_item_list"][3]["easy_item_name"],
        "dangerous"             => $grps[150]["easy_item_list"][10]["easy_item_name"],
        "asses_150_6"           => $grps[150]["easy_item_list"][6]["easy_item_name"],
        "restrict"              => $grps[150]["easy_item_list"][7]["easy_item_name"],
        "asses_150_8"           => $grps[150]["easy_item_list"][8]["easy_item_name"],
        "risyou_sensor"         => $grps[150]["easy_item_list"][5]["easy_item_name"],
        "asses_150_20"          => $grps[150]["easy_item_list"][20]["easy_item_name"],
        "life_system"           => $grps[150]["easy_item_list"][30]["easy_item_name"],
        "asses_150_40"          => $grps[150]["easy_item_list"][40]["easy_item_name"],
        "asses_150_50"          => $grps[150]["easy_item_list"][50]["easy_item_name"],
        "asses_150_60"          => $grps[150]["easy_item_list"][60]["easy_item_name"],
        "asses_150_70"          => $grps[150]["easy_item_list"][70]["easy_item_name"],
        "asses_150_80"          => $grps[150]["easy_item_list"][80]["easy_item_name"],

        "ic"                    => "影響と対応",

        "patient_state"         => $grps[290]["easy_item_list"][120]["easy_item_name"],
        "summary"               => $summary_title,
        "hiyari_summary"        => $hiyari_summary_title,
        "scene_content"         => "発生場面と内容",
        "scene_content_2010"    => $scene_content_2010_title,
        "factor"                => $grps[600]["easy_item_list"][10]["easy_item_name"],
        "clinic"                => $grps[130]["easy_item_list"][90]["easy_item_name"],

        "risk_juudai"           => $grps[1500]["easy_item_list"][10]["easy_item_name"], // リスクの評価：重大性
        "risk_kinkyu"           => $grps[1502]["easy_item_list"][10]["easy_item_name"], // リスクの評価：緊急性
        "risk_hindo"            => $grps[1504]["easy_item_list"][10]["easy_item_name"], // リスクの評価：頻度
        "risk_yosoku"           => $grps[1510]["easy_item_list"][10]["easy_item_name"], // リスクの予測
        "system_kaizen"         => $grps[1520]["easy_item_list"][10]["easy_item_name"], // システム改善の必要性
        "kyouiku_kenshu"        => $grps[1530]["easy_item_list"][10]["easy_item_name"], // 教育研修への活用
    );
}

// タイムラグの表示フラグ取得
$disp_taimelag_flg = getDispTimelagSetting($con, $fname);

// タイムラグを表示する場合は、集計項目を追加
if ($disp_taimelag_flg == true) {
    $vertical_axises['timelag_class'] = '発見から報告までの時間';
}
*/



/*
if ( !(( $stats_read_div == 0 )
  || ( $stats_read_div >= 1 && $stats_read_div  <= 4 && $is_emp_auth_system_all ))
) {
    unset($vertical_axises["post_dept"]);

    $vertical_axises["post"] = $arr_class_name[0];
    $vertical_axises["post_dept"] = '組織階層3';
}
*/
$horizontal_axises = $vertical_axises;


//==================================================
//パラメータ変換
//==================================================

// 今年を取得 20100127
$this_year = date('Y');
$this_month = date('m');

//パラメータを変形
$year = ($Date_Year != "") ? $Date_Year : $this_year;
$month = ($Date_Month != "") ? $Date_Month : $this_month;
//$sub_report_use_flg = ($sub_report_use == "true");
if($postback_mode == ""){
    if($sub_report_use_flg_tmp == "t"){
         $sub_report_use_flg = 't';
    }
}else{
    if($sub_report_use == "true"){
        $sub_report_use_flg = 't';
    }else{
        $sub_report_use_flg = '';
    }
}

$Date_Term = (empty($Date_Term)) ? "01" : $Date_Term;

$stats_util = new hiyari_rm_stats_util($con, $fname);
$vertical_no_disp_code_list         = $stats_util->conv_no_disp_code_list_csv_to_arr($vertical_no_disp_code_list_csv);
$horizontal_no_disp_code_list       = $stats_util->conv_no_disp_code_list_csv_to_arr($horizontal_no_disp_code_list_csv);
$vertical_no_disp_code_list_old     = $stats_util->conv_no_disp_code_list_csv_to_arr($vertical_no_disp_code_list_old_csv);
$horizontal_no_disp_code_list_old   = $stats_util->conv_no_disp_code_list_csv_to_arr($horizontal_no_disp_code_list_old_csv);

//ラベル分類フォルダー取得 20150310
$folders = $stats_util->get_folder_info($emp_id);
$styles = $stats_util->get_style_info();

//集計結果情報のセッション初期化
if ($postback_mode == "") {
    $_SESSION['stats_result_info'] = null;
}

//項目絞り込み情報の取得とセッション初期化
if ($postback_mode == "" || $is_item_shibori_clear == "true"  || $shibori_info_clear == "true") {
    //ファーストアクセスの場合、もしくはクリア指示がある場合は項目絞り込み情報をクリア
    $_SESSION['item_shibori_info'] = null;
}

if($postback_mode == 'load_stats_condition') {
    $_SESSION['item_shibori_info'] = array();

    $sql = "SELECT * FROM inci_stats_condition WHERE condition_id = {$_POST['get_stats_condition']}";
    $result = select_from_table($con, $sql, '', $fname);
    if($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    $stats_condition = pg_fetch_assoc($result);

    // 縦軸、横軸をセット
    $vertical_axis   = $stats_condition['v_axis'];
    $horizontal_axis = $stats_condition['h_axis'];

    $arr_condition_sub_id = explode("\t", $stats_condition['condition_sub_id']);
    $i = 0;
    if(is_array($arr_condition_sub_id)) {
        foreach($arr_condition_sub_id as $condition_sub_id) {
            if($condition_sub_id) {
                $sql = "SELECT * FROM inci_stats_sub_condition WHERE condition_sub_id = {$condition_sub_id}";
                $result = select_from_table($con, $sql, '', $fname);
                if($result == 0) {
                    pg_close($con);
                    showErrorPage();
                    exit;
                }
                $stats_sub_condition = pg_fetch_assoc($result);

                $sql = "SELECT summary_item, post_class, post_attribute, post_dept FROM inci_stats_item_detail_info WHERE item_detail_info_id = {$stats_sub_condition['item_detail_info_id']}";
                $result = select_from_table($con, $sql, '', $fname);
                if($result == 0) {
                    pg_close($con);
                    showErrorPage();
                    exit;
                }
                $item_detail_info = pg_fetch_assoc($result);

                // 縦軸＞項目、横軸＞項目
                if ($vertical_axis == $stats_sub_condition["item"]){
                    $vertical_axises_detail = $stats_sub_condition["item_detail"];
                    $vertical_axises_summary_item = $item_detail_info["summary_item"];
                }
                if ($horizontal_axis == $stats_sub_condition["item"]){
                    $horizontal_axises_detail = $stats_sub_condition["item_detail"];
                    $horizontal_axises_summary_item = $item_detail_info["summary_item"];
                }



                $code_list = array();
                $arr_code_list_id = explode("\t", $stats_sub_condition['code_list_id']);
                foreach($arr_code_list_id as $code_list_id) {
                    $sql = "SELECT code, code_name FROM inci_stats_code_list WHERE code_list_id = {$code_list_id} ORDER BY code_list_id";
                    $result = select_from_table($con, $sql, '', $fname);
                    if($result == 0) {
                        pg_close($con);
                        showErrorPage();
                        exit;
                    }
                    $code_list[] = pg_fetch_assoc($result);
                }

                $_SESSION['item_shibori_info'][$i]['item'] = $stats_sub_condition['item'];
                $_SESSION['item_shibori_info'][$i]['item_name'] = $stats_sub_condition['item_name'];
                $_SESSION['item_shibori_info'][$i]['item_detail'] = $stats_sub_condition['item_detail'];
                $_SESSION['item_shibori_info'][$i]['item_detail_info'] = $item_detail_info;
                $_SESSION['item_shibori_info'][$i]['code_list'] = $code_list;

                //報告書様式
                if ($stats_sub_condition['item'] == "style") {
                    foreach ($code_list as $tmp_code) {
                        $style_id[$tmp_code["code"]] = $tmp_code["code"];
                    }
                }

                $i++;
            } else {
                $_SESSION['item_shibori_info'] = "";
            }
        }
    }
    else {
        $_SESSION['item_shibori_info'] = "";
    }
}

$item_shibori_info = $_SESSION['item_shibori_info'];

//絞込条件追加 20150310
$item_folder_info = array();
$folder_tmp = array();

$item_style_info = array();
$style_tmp = array();

if ($boxflag == "true") {
    
    $folder_id = array();
    foreach ($folder_select as $folder_item) {
        $folder_tmp = explode("_", $folder_item);
        $folder_key = $folder_tmp[0];
        $folder_id[$folder_key] = $folder_key;
    }

    $style_id = array();
    foreach ($item_shibori_info as $tmp_key=>$tmp_shibori) {
        if ($tmp_shibori["item"] == "style") {
            unset($item_shibori_info[$tmp_key]);
        }
    }
    
    
    foreach ($style_select as $style_item) {
        $style_tmp = explode("_", $style_item);
        $style_key = $style_tmp[0];
        $style_id[$style_key] = $style_key;
    }
    
}
if ($postback_mode == "" || $is_item_shibori_clear == "true") {
    $folder_id = null;
    $style_id = null;
}

// ラベル
$all_folder_info = array();
foreach ($folders as $tmp_item) {
    $folder_key = $tmp_item["folder_id"];
    
    $all_folder_info[$folder_key]["folder_id"] = $tmp_item["folder_id"];
    $all_folder_info[$folder_key]["folder_name"] = $tmp_item["folder_name"];
    
    if (in_array($folder_key, $folder_id)){
        $all_folder_info[$folder_key]["checked"] = "checked";
        $item_folder_info[] = $tmp_item["folder_name"];
    }else{
        $all_folder_info[$folder_key]["checked"] = "";
    }
}

// 報告書様式
if ($postback_mode == "item_shibori_change") {
    // 集計データ設定の場合
    if ($vertical_axis == "style" || $horizontal_axis == "style") {
        $style_id = array();
    }
    
    if ($vertical_axis == "style") {
        $vertical_style_id = explode(",", $vertical_item_shibori_change_csv);
        foreach ($vertical_style_id as $tmp_item) {
            $style_id[$tmp_item] = $tmp_item;
        }
         $vertical_item_shibori_change_csv = "";
    }
    if ($horizontal_axis == "style") {
        $horizontal_style_id = explode(",", $horizontal_item_shibori_change_csv);
        foreach ($horizontal_style_id as $tmp_item) {
            $style_id[$tmp_item] = $tmp_item;
        }
         $horizontal_item_shibori_change_csv = "";
    }
}

$all_style_info = array();
foreach ($styles as $tmp_item) {
    $style_key = $tmp_item["eis_id"];
    
    $all_style_info[$style_key]["eis_id"] = $tmp_item["eis_id"];
    $all_style_info[$style_key]["eis_name"] = $tmp_item["eis_name"];
    
    if (in_array($style_key, $style_id)){
        $all_style_info[$style_key]["checked"] = "checked";
        $item_style_info[] = $tmp_item["eis_name"];
    }else{
        $all_style_info[$style_key]["checked"] = "";
    }
}

$style_flag = false;
if(!$_POST['is_stats_condition_set'] && !empty($style_id)) {
    if ($vertical_axis == "style" || $boxflag) {
        if ($boxflag == "true") {
            $vertical_axis = "style";
            $style_flag = true;
        }
        $vertical_item_shibori_change_csv = (empty($vertical_item_shibori_change_csv)) ? implode(",", $style_id) : "," . implode(",", $style_id);
    }
}

//==================================================
//ポストバックした場合
//==================================================
//$postback_mode == "" ･･･ 画面起動時
//$postback_mode == "count" ･･･ 集計を実行
//$postback_mode == "drill_down" ･･･ ドリルダウンを実行
//$postback_mode == "drill_down_return" ･･･ ドリルダウン戻しを実行
//$postback_mode == "no_disp" ･･･ 非表示化を実行
//$postback_mode == "all_disp" ･･･ 全表示化を実行
//$postback_mode == "item_shibori_change" ･･･ 項目絞り込み条件変更(次回の検索から有効。)
//$postback_mode == "load_stats_condition" ･･･ 集計条件読込(↑に記述)
if ($postback_mode != "") {
    if($vertical_axis == "summary") {
    //縦軸を戻す場合 20090306
        if ($vertical_axises_detail == "content") {
            //元画面が自己抜去詳細の場合
            if ($vertical_axises_detail_old == "summary_selfremove") {
                $vertical_axises_summary_item = "6";
            }
        }
    }
    if($horizontal_axis == "summary") {
    //横軸を戻す場合 20090306
        if ($horizontal_axises_detail == "content") {
            //元画面が自己抜去詳細の場合
            if ($horizontal_axises_detail_old == "summary_selfremove") {
                $horizontal_axises_summary_item = "6";
            }
        }
    }
    //==================================================
    //集計結果を取得
    //==================================================

    $arr_post = array();
    foreach($search_emp_class as $key => $value) {
        if($search_emp_class[$key] != "") {
            $arr_post[$key][0] = $search_emp_class[$key];
        } else {
            // 所属が空の場合、全表示
            unset($arr_post);
            break;
        }
        if($search_emp_attribute[$key] != "") {
            $arr_post[$key][1] = $search_emp_attribute[$key];
        }
        if($search_emp_dept[$key] != "") {
            $arr_post[$key][2] = $search_emp_dept[$key];
        }
        if($search_emp_room[$key] != "") {
            $arr_post[$key][3] = $search_emp_room[$key];
        }
    }

    $axis_detail_info = "";
    $axis_detail_info["vertical_axises_detail"] = $vertical_axises_detail;
    $axis_detail_info["horizontal_axises_detail"] = $horizontal_axises_detail;
    $axis_detail_info["vertical_axises_summary_item"] = $vertical_axises_summary_item;
    $axis_detail_info["horizontal_axises_summary_item"] = $horizontal_axises_summary_item;
    $axis_detail_info["vertical_axises_post_class"] = $vertical_axises_post_class;
    $axis_detail_info["vertical_axises_post_attribute"] = $vertical_axises_post_attribute;
    $axis_detail_info["vertical_axises_post_dept"] = $vertical_axises_post_dept;
    $axis_detail_info["horizontal_axises_post_class"] = $horizontal_axises_post_class;
    $axis_detail_info["horizontal_axises_post_attribute"] = $horizontal_axises_post_attribute;
    $axis_detail_info["horizontal_axises_post_dept"] = $horizontal_axises_post_dept;

    //前回の全表示時の集計結果が使用できない場合
    if(in_array($postback_mode,array("count","drill_down","drill_down_return","load_stats_condition")))
    {
        if($postback_mode == 'drill_down_return' && !in_array($vertical_axises_detail_old, array('score', 'danger', 'kousoku', 'mitten', 'sensor')))$drill_down_return = true;

        $arr_report_stats_data = $stats_util->get_report_stats_data($grps, $vertical_axis, $horizontal_axis, $year, $month, $arr_post, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info,$item_shibori_info,$drill_down_return,$easy_code, $Date_Term, $folder_id, $style_id);

        //非表示項目がある場合
        if($vertical_no_disp_code_list != "" || $horizontal_no_disp_code_list != "")
        {
            //再集計を行う。
            $vertical_options          = $arr_report_stats_data["vertical_options"];
            $horizontal_options        = $arr_report_stats_data["horizontal_options"];
            $cells_report_id_list_full = $arr_report_stats_data["cells_report_id_list_full"];
            $arr_report_stats_data = $stats_util->get_report_stats_data2($cells_report_id_list_full,$vertical_options,$horizontal_options,$vertical_no_disp_code_list,$horizontal_no_disp_code_list);
        }

        //集計を行った場合のみ、集計に使用した項目絞込み情報を文字列化する。(集計を行わなかった場合は前回情報を維持)
        //※$_SESSION['item_shibori_info']は前回の集計情報を維持しないケースがあるため、前回の集計情報としてこれが必要。
        $item_shibori_info_string = $stats_util->item_shibori_info_to_string($item_shibori_info);
    }
    //前回の全表示時の集計結果が使用できる場合
    else//"no_disp"/"all_disp"/"item_shibori_change"
    {
        $vertical_axis_stats_info = $stats_util->get_axis_stats_info('vertical',$grps,$vertical_axis,$axis_detail_info,$count_date_mode,false,$year,$month);
        $vertical_options = $vertical_axis_stats_info["axis_options"];

        $horizontal_axis_stats_info = $stats_util->get_axis_stats_info('horizontal',$grps,$horizontal_axis,$axis_detail_info,$count_date_mode,false,$year,$month);
        $horizontal_options = $horizontal_axis_stats_info["axis_options"];

        $cells_report_id_list_full = $stats_util->cells_report_id_list_full_from_string($cells_report_id_list_full_string);

        $arr_report_stats_data = $stats_util->get_report_stats_data2($cells_report_id_list_full,$vertical_options,$horizontal_options,$vertical_no_disp_code_list,$horizontal_no_disp_code_list);
    }

    $vertical_options          = $arr_report_stats_data["vertical_options"];
    $horizontal_options        = $arr_report_stats_data["horizontal_options"];
    $stats_data_list           = $arr_report_stats_data["stats_data_list"];
    $report_id_list            = $arr_report_stats_data["report_id_list"];
    $cells_report_id_list_full = $arr_report_stats_data["cells_report_id_list_full"];

    $counts                    = $stats_data_list["cells"];
    $vertical_sums             = $stats_data_list["vertical_sums"];
    $horizontal_sums           = $stats_data_list["horizontal_sums"];
    $sums_sum                  = $stats_data_list["sums_sum"];

    $cells_report_id_list_full_string = $stats_util->cells_report_id_list_full_to_string($cells_report_id_list_full);
}
//==================================================
//初回アクセスの場合
//==================================================
else
{
    //==================================================
    //初期表示値を設定
    //==================================================

    //軸
    $vertical_axis = "level";
    $horizontal_axis = "report_month";

    //部署
    $arr_login_user_post = get_login_user_post($con, $fname, $session);
    $search_emp_class     = $arr_login_user_post[0]["emp_class"];
    $search_emp_attribute = $arr_login_user_post[0]["emp_attribute"];
    $search_emp_dept      = $arr_login_user_post[0]["emp_dept"];
    $search_emp_room      = $arr_login_user_post[0]["emp_room"];

    //集計結果(空)
    $vertical_options = array();
    $horizontal_options = array();
    $vertical_sums = array();
    $horizontal_sums = array();
    $sums_sum = 0;
    $report_id_list = array();
    $counts = array();
}

//==================================================
//軸名
//==================================================
$vertical_label   = $vertical_axises[$vertical_axis]     . label_detail('v', $vertical_axis,   $vertical_axises_detail,   $vertical_axises_summary_item,   $rep_obj, $con, $fname);
$horizontal_label = $horizontal_axises[$horizontal_axis] . label_detail('h', $horizontal_axis, $horizontal_axises_detail, $horizontal_axises_summary_item, $rep_obj, $con, $fname);

//==================================================
//詳細リンク情報
//==================================================
//縦軸詳細リンク
list($vertical_axises_detail_link, $vertical_axises_detail_link_return, $vertical_axises_summary_item) = detail_link(
    $vertical_axis,
    $vertical_axises_detail,
    $vertical_axises_detail_old,
    $vertical_axises_summary_item,
    $class_cnt
);
list($horizontal_axises_detail_link, $horizontal_axises_detail_link_return, $horizontal_axises_summary_item) = detail_link(
    $horizontal_axis,
    $horizontal_axises_detail,
    $horizontal_axises_detail_old,
    $horizontal_axises_summary_item,
    $class_cnt
);

//==================================================
//項目絞り込み条件変更の場合
//==================================================
if($postback_mode == "item_shibori_change" || $style_flag)
{
    //項目絞り込み条件変更
    $item_shibori_info = $stats_util->change_item_shibori_info(
            $item_shibori_info,
            $vertical_item_shibori_change_csv,
            $horizontal_item_shibori_change_csv,
            $vertical_axis,
            $vertical_label,
            $vertical_options,
            $horizontal_axis,
            $horizontal_label,
            $horizontal_options,
            $axis_detail_info
            );
    //セッション更新
    $_SESSION['item_shibori_info'] = $item_shibori_info;
}


//==================================================
//項目絞り込み条件保存の場合
//==================================================
if($_POST['is_stats_condition_set'] == "true") {
    $sql = "SELECT count(*) FROM inci_stats_condition WHERE stats_name = '".pg_escape_string($stats_condition)."' and available = 't'";
    $result = select_from_table($con, $sql, '', $fname);
    if($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    $count = pg_fetch_result($result, 0, 0);

    if ( empty($stats_condition) ) {
        echo "<script>alert('集計条件保存名が記入されていません')</script>";
    } else if($count > 0) {
        echo "<script>alert('集計条件保存名に同じ名称が登録されています')</script>";
    } else {
        $arr_condition_sub_id = array();
        foreach($item_shibori_info as $val) {
            $arr_code_list_id = array();
            if(count($val['code_list']) > 0) {
                foreach($val['code_list'] as $value) {
                    $sql = "SELECT max(code_list_id) FROM inci_stats_code_list";
                    $result = select_from_table($con, $sql, '', $fname);
                    if ($result == 0) {
                        pg_close($con);
                        showErrorPage();
                        exit;
                    }
                    $max_code_list_id = pg_fetch_result($result, 0, 0) + 1;

                    $sql = "INSERT INTO inci_stats_code_list(code_list_id, code, code_name) VALUES(";
                    $content = array($max_code_list_id, "{$value['code']}", "{$value['code_name']}");
                    $ins = insert_into_table($con, $sql, $content, $fname);
                    if ($ins == 0) {
                        pg_close($con);
                        showErrorPage();
                        exit;
                    }

                    $arr_code_list_id[] = $max_code_list_id;
                }
            }

            $sql = "SELECT max(item_detail_info_id) FROM inci_stats_item_detail_info";
            $result = select_from_table($con, $sql, '', $fname);
            if($result == 0) {
                pg_close($con);
                showErrorPage();
                exit;
            }
            $max_item_detail_info_id = pg_fetch_result($result, 0, 0) + 1;

            $sql = "INSERT INTO inci_stats_item_detail_info(item_detail_info_id, summary_item, post_class, post_attribute, post_dept) VALUES(";
            $content = array($max_item_detail_info_id, "{$val['item_detail_info']['summary_item']}", "{$val['item_detail_info']['post_class']}", "{$val['item_detail_info']['post_attribute']}", "{$val['item_detail_info']['post_dept']}");
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_close($con);
                showErrorPage();
                exit;
            }

            $sql = "SELECT max(condition_sub_id) FROM inci_stats_sub_condition";
            $result = select_from_table($con, $sql, '', $fname);
            if($result == 0) {
                pg_close($con);
                showErrorPage();
                exit;
            }
            $max_condition_sub_id = pg_fetch_result($result, 0, 0) + 1;

            $tab_code_list_id = implode("\t", $arr_code_list_id);
            $sql = "INSERT INTO inci_stats_sub_condition(condition_sub_id, item, item_name, item_detail, item_detail_info_id, code_list_id) VALUES(";
            $content = array($max_condition_sub_id, "{$val['item']}", "{$val['item_name']}", "{$val['item_detail']}", $max_item_detail_info_id, "$tab_code_list_id");
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_close($con);
                showErrorPage();
                exit;
            }

            $arr_condition_sub_id[] = $max_condition_sub_id;
        }

        $sql = "SELECT max(condition_id) FROM inci_stats_condition";
        $result = select_from_table($con, $sql, '', $fname);
        if($result == 0) {
            pg_close($con);
            showErrorPage();
            exit;
        }
        $max_condition_id = pg_fetch_result($result, 0, 0) + 1;

        $tab_condition_sub_id = implode("\t", $arr_condition_sub_id);
        $sql = "INSERT INTO inci_stats_condition(condition_id, condition_sub_id, stats_name, v_axis, h_axis, available) VALUES(";
        $content = array($max_condition_id, "{$tab_condition_sub_id}", pg_escape_string($stats_condition), "{$vertical_axis}", "{$horizontal_axis}", "t");
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_close($con);
            showErrorPage();
            exit;
        }
    }
}


//==================================================
//集計結果情報を生成
//==================================================
//集計結果がある場合のみ生成。
//※この機能により、作り直せばcells_report_id_list_full_stringなどの画面間インターフェースが不要となる可能性あり。
if(count($counts) > 0)
{
    //集計結果データ
    $_SESSION['stats_result_info']['report_stats_data'] = $arr_report_stats_data;
    //軸条件表示
    $_SESSION['stats_result_info']['vertical_label'] = $vertical_label;
    $_SESSION['stats_result_info']['horizontal_label'] = $horizontal_label;
    //非表示項目
    $_SESSION['stats_result_info']['vertical_no_disp_code_list'] = $vertical_no_disp_code_list;
    $_SESSION['stats_result_info']['horizontal_no_disp_code_list'] = $horizontal_no_disp_code_list;
}

//======================================================================================================================================================
//表示
//======================================================================================================================================================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());

//------------------------------------------------------------------------------
//ヘッダ用データ
//------------------------------------------------------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------------------------------------------------------
//MENU:集計ダウンロード
//------------------------------------------------------------------------------
$stats_download_mode = defined("STATS_DOWNLOAD_MODE");
$smarty->assign("stats_download_mode", $stats_download_mode);

//------------------------------------------------------------------------------
//集計条件：集計モード
//------------------------------------------------------------------------------
$smarty->assign("count_date_mode", $count_date_mode);
$smarty->assign("is_report_date_mode", $count_date_mode == "report_date");
$smarty->assign("is_incident_date_mode", $count_date_mode == "incident_date");

//------------------------------------------------------------------------------
//集計条件：集計年月
//------------------------------------------------------------------------------
$smarty->assign("year_list", array_reverse(range(2004,$this_year)));
$smarty->assign("year", $year);
$smarty->assign("month_list", range(1,12));
$smarty->assign("month", $month);
$smarty->assign("Date_Term", $Date_Term);

//------------------------------------------------------------------------------
//集計条件：副報告
//------------------------------------------------------------------------------
$smarty->assign("sub_report_use_flg", $sub_report_use_flg);
$smarty->assign("sub_report_use", $sub_report_use);

//------------------------------------------------------------------------------
//集計条件：部署
//------------------------------------------------------------------------------
//-----------------
//部署階層の名前
//-----------------
$smarty->assign("arr_class_name", $arr_class_name);

//-----------------
//部署の選択肢
//-----------------
$stats_read_auth_flg = get_stats_read_auth_flg($session,$fname,$con);
$post_select_data = get_post_select_data($con, $fname, $emp_id, $stats_read_div, $priority_auth, $stats_read_auth_flg);
$smarty->assign("post_select_data", $post_select_data);

//-----------------
//部署条件数と選択値
//-----------------
//部署条件数
if (!isset($count_search)){
    $count_search = 1;
}

//行追加
if($postback_mode == "add_search")
{
    $count_search++;
}
//行削除
elseif($postback_mode == "delete_search")
{
    $tmp_class = array();
    $tmp_attribute = array();
    $tmp_dept = array();
    $tmp_room = array();

    for($i=0; $i<$count_search; $i++) {
        if($delete_line_no != $i) {
            $tmp_class[] = $search_emp_class[$i];
            $tmp_attribute[] = $search_emp_attribute[$i];
            $tmp_dept[] = $search_emp_dept[$i];
            if ($search_emp_room != null){
                $tmp_room[] = $search_emp_room[$i];
            }
        }
    }

    $count_search--;

    $search_emp_class = $tmp_class;
    $search_emp_attribute = $tmp_attribute;
    $search_emp_dept = $tmp_dept;
    if ($search_emp_room != null){
        $search_emp_room = $tmp_room;
    }
}

$smarty->assign("count_search", $count_search);
$smarty->assign("search_emp_class", $search_emp_class);
$smarty->assign("search_emp_attribute", $search_emp_attribute);
$smarty->assign("search_emp_dept", $search_emp_dept);
if ($search_emp_room != null){
    $smarty->assign("search_emp_room", $search_emp_room);
}

//------------------------------------------------------------------------------
//集計条件：集計データ
//------------------------------------------------------------------------------
$smarty->assign("item_shibori_info", $item_shibori_info);
$smarty->assign("all_folder_info", $all_folder_info);
$smarty->assign("all_style_info", $all_style_info);

$smarty->assign("item_folder_info", $item_folder_info);
$smarty->assign("folder_id", $folder_id);

$smarty->assign("item_style_info", $item_style_info);
$smarty->assign("style_id", $style_id);

//------------------------------------------------------------------------------
//集計条件：軸
//------------------------------------------------------------------------------
$smarty->assign("vertical_axises", $vertical_axises);
$smarty->assign("vertical_axis", $vertical_axis);
$smarty->assign("horizontal_axises", $horizontal_axises);
$smarty->assign("horizontal_axis", $horizontal_axis);

//------------------------------------------------------------------------------
//集計結果
//------------------------------------------------------------------------------
$smarty->assign("has_result", count($counts) > 0);
$smarty->assign("col_width", intval(80 / (count($horizontal_sums)+1)));

//軸ラベル
$smarty->assign("vertical_label", $vertical_label);
$smarty->assign("horizontal_label", $horizontal_label);

//除外項目
$smarty->assign("vertical_no_disp_code_list", $vertical_no_disp_code_list);
$smarty->assign("horizontal_no_disp_code_list", $horizontal_no_disp_code_list);

//EXCEL出力ボタン表示フラグ
$smarty->assign("excel_disp_flg", get_inci_btn_show_flg($con, $fname, $session));

//非表示項目
$smarty->assign("vertical_no_disp_code_list_csv", $vertical_no_disp_code_list_csv);
$smarty->assign("horizontal_no_disp_code_list_csv", $horizontal_no_disp_code_list_csv);

//絞り込み項目
$smarty->assign("item_shibori_info_string", $item_shibori_info_string);

//軸のドリルダウン情報
$smarty->assign("vertical_axises_detail", $vertical_axises_detail);
$smarty->assign("horizontal_axises_detail", $horizontal_axises_detail);
$smarty->assign("vertical_axises_summary_item", $vertical_axises_summary_item);
$smarty->assign("horizontal_axises_summary_item", $horizontal_axises_summary_item);
$smarty->assign("vertical_axises_post_class", $vertical_axises_post_class);
$smarty->assign("vertical_axises_post_attribute", $vertical_axises_post_attribute);
$smarty->assign("vertical_axises_post_dept", $vertical_axises_post_dept);
$smarty->assign("horizontal_axises_post_class", $horizontal_axises_post_class);
$smarty->assign("horizontal_axises_post_attribute", $horizontal_axises_post_attribute);
$smarty->assign("horizontal_axises_post_dept", $horizontal_axises_post_dept);

//集計結果キャッシュ
$smarty->assign("cells_report_id_list_full_string", $cells_report_id_list_full_string);

$smarty->assign("horizontal_axises_detail_link", $horizontal_axises_detail_link);
$smarty->assign("horizontal_axises_detail_link_return", $horizontal_axises_detail_link_return);
$smarty->assign("vertical_axises_detail_link", $vertical_axises_detail_link);
$smarty->assign("vertical_axises_detail_link_return", $vertical_axises_detail_link_return);

//------------------------------------------------------------------------------
//集計結果：タイトル行
//------------------------------------------------------------------------------
$title_row = array();
foreach($horizontal_options as $option){
    $easy_code = $option['easy_code'];
    if(in_array($easy_code, $horizontal_no_disp_code_list)){
        continue;
    }

    $cell = array();
    $cell['easy_code'] = $option['easy_code'];
    $cell['easy_name'] = $option['easy_name'];
    $cell['checked'] = in_array($easy_code, $horizontal_no_disp_code_list_old) ? 'checked' : '';

    $horizontal_link_flg = false;
    if((($horizontal_axis != "summary" && $horizontal_axis != "scene_content" && $horizontal_axis != "scene_content_2010" ) ||
        ($horizontal_axis == "summary" && $horizontal_axises_detail == "content" &&
        (($horizontal_axises_summary_item == "6" && $easy_code == "2") ||
        ($horizontal_axises_summary_item == "9" && ($easy_code == "1" || $easy_code == "2")))) ||
        ($horizontal_axis == "summary" && $horizontal_axises_detail == "summary_selfremove" &&
        ($horizontal_axises_summary_item == "2" && $easy_code == "20"))
        ) &&
        $horizontal_axises_detail_link != "" && $easy_code != ""){
        $horizontal_link_flg = true;
    }
    $cell['horizontal_link_flg'] = $horizontal_link_flg;

    $cell['btn_list'] = get_button_info($con, $fname, $horizontal_axis, $horizontal_axises_detail, $horizontal_axises_detail_link, $easy_code);

    $title_row[] = $cell;
}
$smarty->assign("title_row", $title_row);

//------------------------------------------------------------------------------
//集計結果：詳細行
//------------------------------------------------------------------------------
$detail_rows = array();
foreach ($vertical_options as $row_idx => $v_option){
    $easy_code = $v_option['easy_code'];
    if(in_array($easy_code,$vertical_no_disp_code_list)){
        continue;
    }

    $row = array();
    $row['index'] = $row_idx;

    //行タイトル
    $row['easy_code'] = $v_option['easy_code'];
    $row['easy_name'] = $v_option['easy_name'];

    $row['checked'] = in_array($easy_code, $vertical_no_disp_code_list_old) ? 'checked' : '';

    $row['vertical_link_flg'] = false;
    if ((($vertical_axis != "summary" && $vertical_axis != "scene_content" && $vertical_axis != "scene_content_2010" ) ||
         ($vertical_axis == "summary" && $vertical_axises_detail == "content" &&
         (($vertical_axises_summary_item == "6" && $easy_code == "2") ||
          ($vertical_axises_summary_item == "9" && ($easy_code == "1" || $easy_code == "2")))) ||
         ($vertical_axis == "summary" && $vertical_axises_detail == "summary_selfremove" &&
          ($vertical_axises_summary_item == "2" && $easy_code == "20"))) &&
        $vertical_axises_detail_link != "" && $easy_code != "")
    {
        $row['vertical_link_flg'] = true;
    }

    $row['btn_list'] = get_button_info($con, $fname, $vertical_axis, $vertical_axises_detail, $vertical_axises_detail_link, $easy_code);

    //件数
    foreach($horizontal_options as $col_idx => $h_option){
        $easy_code = $h_option['easy_code'];
        if(in_array($easy_code, $horizontal_no_disp_code_list)){
            continue;
        }

        $cell = array();
        $cell['index'] = $col_idx;
        $cell['count'] = $counts[$row_idx][$col_idx];

        $id_list = "";
        foreach($report_id_list['cells'][$row_idx][$col_idx] as $key => $report_id){
            if($key > 0){
                $id_list .= ',';
            }
            $id_list .= $report_id;
        }
        $cell['id_list'] = $id_list;

        $row['cells'][] = $cell;
    }

    //行合計
    $row['sum'] = $vertical_sums[$row_idx];
    $id_list = "";
    foreach ($report_id_list['vertical_sums'][$row_idx] as $key => $report_id){
        if ($key > 0){
            $id_list .= ',';
        }
        $id_list .= $report_id;
    }
    $row['id_list'] = $id_list;

    $detail_rows[] = $row;
}
$smarty->assign("detail_rows", $detail_rows);

//------------------------------------------------------------------------------
//集計結果：合計行
//------------------------------------------------------------------------------
//列合計
$sum_row = array();
foreach ($horizontal_options as $index => $option){
    if(in_array($option['easy_code'], $horizontal_no_disp_code_list)){
        continue;
    }

    $cell = array();

    $cell['index'] = $index;
    $cell['sum'] = $horizontal_sums[$index];

    $id_list = "";
    foreach ($report_id_list['horizontal_sums'][$index] as $key => $report_id){
        if ($key > 0){
            $id_list .=  ',';
        }
        $id_list .= $report_id;
    }
    $cell['id_list'] = $id_list;

    $sum_row[] = $cell;
}
$smarty->assign("sum_row", $sum_row);

//総合計
$smarty->assign("sums_sum", $sums_sum);
$sum_id_list = "";
foreach($report_id_list['sums_sum'] as $key => $report_id)
{
    if($key > 0){
        $sum_id_list .= ',';
    }
    $sum_id_list .= $report_id;
}
$smarty->assign("sum_id_list", $sum_id_list);

//------------------------------------------------------------------------------
//子画面用データ
//------------------------------------------------------------------------------
$v_name_list = array();
foreach($vertical_options as $option)
{
    $v_name_list[] = str_replace("'", "\\'", $option['easy_name']);
}
$smarty->assign("v_name_list", $v_name_list);

$h_name_list = array();
foreach($horizontal_options as  $option)
{
    $h_name_list[] = str_replace("'", "\\'", $option['easy_name']);
}
$smarty->assign("h_name_list", $h_name_list);

//------------------------------------------------------------------------------
//表示
//------------------------------------------------------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_rm_stats1.tpl");
}
else{
    $smarty->display("hiyari_rm_stats2.tpl");
}

pg_close($con);

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================

// ログインユーザの部署情報取得
function get_login_user_post($con, $fname, $session)
{
    $sql  = "select emp_class, emp_attribute, emp_dept, emp_room from empmst";
    $cond = "where emp_id in (select emp_id from session where session_id = '$session')";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_all($result);
}

// 患者への影響と対応の非表示設定
function disp_ic_item($con, $fname, $code, $easy_code) {
    //echo $easy_code;
    $sql  = "select count(*) from inci_ic_item";
    $cond = "where item_code = '{$code}' and super_item_id = {$easy_code} and disp_flg = 't' and available = 't'";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_result($result, 0, 0);
}
// 患者への影響と対応の非表示設定
function disp_ic_sub_item($con, $fname, $easy_code) {
    //echo $easy_code;
    $sql  = "select count(*) from inci_ic_sub_item";
    $cond = "where item_id = {$easy_code} and disp_flg = 't' and available = 't'";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_result($result, 0, 0);
}
// インシデント直前の患者の状態
function disp_incident_patient_state($con, $fname, $easy_code) {
    $sql  = "select count(*) from inci_report_materials";
    $cond = "where grp_code = 295 and easy_item_code = {$easy_code} and easy_flag = '1'";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_result($result, 0, 0);
}
// 発生場所
function disp_incident_place($con, $fname, $easy_code) {
    $sql  = "select count(*) from inci_report_materials";
    $cond = "where grp_code = 115 and easy_item_code = {$easy_code} and easy_flag = '1'";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_result($result, 0, 0);
}

// 縦軸横軸ラベル名の詳細部分
function label_detail($vh, $axis, $axises_detail, $axises_summary_item, $rep_obj, $con, $fname) {
    $label_detail = "";

    $summary_items = array(
        "1"  => "指示出し",
        "2"  => "薬剤",
        "3"  => "輸血",
        "4"  => "治療・処置",
        "5"  => "医療用具",
        "6"  => "ドレーン・チューブ",
        "7"  => "歯科医療用具",
        "8"  => "検査",
        "9"  => "療養上の世話",
        "10" => "その他",
    );

    switch ($axis) {
        // 発生場面と内容
        case 'scene_content':
            if(!is_numeric($axises_summary_item)) {
                break;
            }

            $scene_content_items = $rep_obj->get_super_item_by_id($axises_summary_item);
            $label_detail .= get_label_detail($axises_detail, $scene_content_items['super_item_name']);
            break;

        // 影響と対応
        case 'ic':
            if(!is_numeric($axises_summary_item)) {
                break;
            }

            if (in_array($axises_detail, array('influence', 'mental', 'correspondence'))) {
                $ic_items = $rep_obj->get_ic_super_item_by_id($axises_summary_item);
                $ic_name = $ic_items['super_item_name'];
            }
            else if ($axises_detail == 'detail_correspondence') {
                $ic_items = $rep_obj->get_ic_item_by_id($axises_summary_item, 'correspondence');
                $ic_name  = $ic_items['item_name'];
            }

            $label_detail .= get_label_detail($axises_detail, $ic_name);
            break;

        // 概要2010年版
        case 'scene_content_2010':
            if(!is_numeric($axises_summary_item)) {
                break;
            }

            if (in_array($axises_detail, array('scene', 'kind', 'content'))) {
                $scene_content_items = $rep_obj->get_super_item_by_id_2010($axises_summary_item, $axises_detail);
                $scene_content_name = $scene_content_items['super_item_name'];
            }
            else if(in_array($axises_detail, array('detail_scene', 'detail_kind', 'detail_content'))) {

                switch ($axises_detail) {
                    case 'detail_scene' :
                        $scene_content_items = $rep_obj->get_item_by_id_2010($axises_summary_item, 'scene');
                        break;
                    case 'detail_kind' :
                        $scene_content_items = $rep_obj->get_item_by_id_2010($axises_summary_item, 'kind');
                        break;
                    case 'detail_content' :
                        $scene_content_items = $rep_obj->get_item_by_id_2010($axises_summary_item, 'content');
                        break;
                }
                $scene_content_name = $scene_content_items['item_name'];
            }
            else {
                $scene_content_items = $rep_obj->get_super_item_by_id_2010($axises_summary_item);
                $scene_content_name = $scene_content_items['super_item_name'];
            }

            $label_detail .= get_label_detail($axises_detail, $scene_content_name);
            break;

        // summary
        case 'summary':
            $label_detail .= get_label_detail($axises_detail, $summary_items[$axises_summary_item], $axises_summary_item);
            break;

        // post
        case 'post_attribute':
        case 'post_dept':
        case 'post_room':
            if ($vh == 'v') {
                $class = $_POST["vertical_axises_post_class"];
                $atrb  = $_POST["vertical_axises_post_attribute"];
                $dept  = $_POST["vertical_axises_post_dept"];
            }
            else {
                $class = $_POST["horizontal_axises_post_class"];
                $atrb  = $_POST["horizontal_axises_post_attribute"];
                $dept  = $_POST["horizontal_axises_post_dept"];
            }
            if (axises_detail == 'post_attribute') {
                $label_detail .= "＞".get_class_nm($con,$class,$fname);
            }
            else if (axises_detail == 'post_dept') {
                $label_detail .= "＞".get_class_nm($con,$class,$fname);
                $label_detail .= "＞".get_atrb_nm($con,$atrb,$fname);
            }
            else if (axises_detail == 'post_room') {
                $label_detail .= "＞".get_class_nm($con,$class,$fname);
                $label_detail .= "＞".get_atrb_nm($con,$atrb,$fname);
                $label_detail .= "＞".get_dept_nm($con,$dept,$fname);
            }
            break;
    }
    return $label_detail;
}
function get_label_detail($axis, $name, $item=null) {
    switch ($axis) {
        case 'scene':
        case 'detail_scene':
            return "＞発生場面({$name})";

        case 'kind':
        case 'detail_kind':
            return "＞種類({$name})";

        case 'content':
        case 'detail_content':
            return "＞内容({$name})";

        case 'score':
            return "＞スコア(転倒・転落)";

        case 'danger':
            return "＞危険度(転倒・転落)";

        case 'sensor':
            return "＞センサー(転倒・転落)";

        case 'kousoku':
            return "＞拘束用具(自己抜去)";

        case 'mitten':
            return "＞ミトン(自己抜去)";

        case 'influence':
            return "＞影響({$name})";

        case 'mental':
            return "＞精神的影響({$name})";

        case 'correspondence':
            return "＞対応({$name})";

        case 'detail_correspondence':
            return "＞詳細({$name})";

        case 'summary_selfremove':
            return "＞内容(ドレーン・チューブ)＞自己抜去";

        case 'summary_mitten':
            return "＞内容(ドレーン・チューブ)＞自己抜去＞ミトンあり";

        case 'summary_stumble':
            if ($item == "1") {
                return "＞内容(療養上の世話)＞転倒";
            }
            else {
                return "＞内容(療養上の世話)＞転落";
            }
    }
}

// リンクと戻りの部分
function detail_link($axis, $detail, $detail_old, $item, $class_cnt) {
    $link = "";
    $return = "";

    switch ($axis) {
        case 'post':
            switch ($detail) {
                case 'post_attribute':
                    $link = "post_dept";
                    break;

                case 'post_dept':
                    if ($class_cnt == 4) {
                        $link = "post_room";
                        $return = "post_attribute";
                    }
                    break;

                case 'post_room':
                    $return = "post_dept";
                    break;

                default:
                    $link = "post_attribute";
                    break;
            }
            break;

        case 'summary':
            switch ($detail) {
                case 'content':
                    //元画面が自己抜去詳細の場合
                    if ($detail_old == "summary_selfremove") {
                        $item = "6";
                    }
                    //ドレーン・チューブ
                    if ($item == "6") {
                        $link = 'summary_selfremove';
                    }
                    //療養上の世話
                    else if ($item == "9") {
                        $link = 'summary_stumble';
                    }
                    break;

                //自己抜去
                case 'summary_selfremove':
                    if ($item == "2") {
                        $link = 'summary_mitten';
                    }
                    $return = 'content';
                    break;

                //ミトンあり
                case 'summary_mitten':
                    $return = "summary_selfremove";
                    $item = "2";
                    break;

                //転倒・転落
                case 'summary_stumble':
                case 'score':   //スコア
                case 'danger':  //危険度
                case 'sensor':  //センサー
                    $return = "content";
                    $item = "9";
                    break;

                case 'kousoku': //拘束用具
                case 'mitten':  //ミトン
                    $return = "content";
                    $item = "6";
                    break;
            }
            break;

        case 'scene_content':
            switch ($detail) {
                case 'content':
                    //元画面が自己抜去詳細の場合
                    if ($detail_old == "scene_content_selfremove") {
                        $item = "7";
                    }
                    //ドレーン・チューブ
                    if ($item == "7") {
                        $link = 'scene_content_selfremove';
                    }
                    //療養上の世話
                    else if ($item == "10") {
                        $link = 'scene_content_stumble';
                    }
                    break;

                case 'score':   //スコア
                case 'danger':  //危険度
                case 'sensor':  //センサー
                    $return = "content";
                    $item = "10";
                    break;

                case 'kousoku': //拘束用具
                case 'mitten':  //ミトン
                    $return = "content";
                    $item = "7";
                    break;
            }
            break;

        case 'scene_content_2010':
            switch ($detail) {
                case 'detail_content':
                    //元画面が自己抜去詳細の場合
                    if ($detail_old == "scene_content_selfremove") {
                        $item = "68";
                    }
                    //ドレーン・チューブ
                    if ($item == "68") {
                        $link = 'scene_content_selfremove';
                    }
                    //療養上の世話
                    else if ($item == "74") {
                        $link = 'scene_content_stumble';
                    }
                    break;

                case 'detail_kind':
                    $return = 'kind';
                    break;

                case 'detail_scene':
                    $return = 'scene';
                    break;

                case 'score':   //スコア
                case 'danger':  //危険度
                case 'sensor':  //センサー
                    $return = "detail_content";
                    $item = "74";
                    break;

                case 'kousoku': //拘束用具
                case 'mitten':  //ミトン
                    $return = "detail_content";
                    $item = "68";
                    break;
            }
            break;
    }
    return array($link, $return, $item);
}

function get_button_info($con, $fname, $axis, $axises_detail, $axises_detail_link, $easy_code)
{
    $btn_list = array();

    $btn['disabled'] = false;

    switch($axis){
    //拘束用具
    case "restrict":
        if ($axises_detail == "" && $easy_code == 10){
            $btn['value'] = '詳細';
            $btn['detail'] = 'asses_150_7_detail';
            $btn_list[] = $btn;
        }
        break;

    //リスク回避用具
    case "asses_150_6":
        if ($axises_detail == "" && $easy_code == 10){
            $btn['value'] = '詳細';
            $btn['detail'] = 'asses_150_6_detail';
            $btn_list[] = $btn;
        }
        break;

    //身体拘束行為
    case "asses_150_8":
        if ($axises_detail == "" && $easy_code == 10){
            $btn['value'] = '詳細';
            $btn['detail'] = 'asses_150_8_detail';
            $btn_list[] = $btn;
        }
        break;

    //ルート関連（自己抜去）
    case "asses_150_20":
        if ($axises_detail == "" && $easy_code == 20){
            $btn['value'] = '詳細';
            $btn['detail'] = 'asses_150_20_detail';
            $btn_list[] = $btn;
        }
        break;

    //発生場所
    case "place":
        if ($axises_detail == "" && $easy_code != ""){
            if (disp_incident_place($con, $fname, $easy_code)){
                $btn['value'] = '詳細';
                $btn['detail'] = 'place_detail';
                $btn_list[] = $btn;
            }
        }
        break;

    //インシデント直前の患者の状態
    case "patient_state":
        if ($axises_detail == "" && $easy_code != ""){
            if (disp_incident_patient_state($con, $fname, $easy_code)){
                $btn['value'] = '詳細';
                $btn['detail'] = 'patient_state_detail';
                $btn_list[] = $btn;
            }
        }
        break;

    //インシデントの概要
    case "summary":
        if ($axises_detail == "" && $easy_code != ""){
            $btn['value'] = '場面';
            $btn['detail'] = 'scene';
            $btn_list[] = $btn;

            $btn['value'] = '内容';
            $btn['detail'] = 'content';
            $btn_list[] = $btn;
        }

        if ($axises_detail_link == "summary_stumble" && ($easy_code == "1" || $easy_code == "2")){
            $btn['value'] = 'スコア';
            $btn['detail'] = 'score';
            $btn_list[] = $btn;

            $btn['value'] = '危険度';
            $btn['detail'] = 'danger';
            $btn_list[] = $btn;

            $btn['value'] = 'センサー';
            $btn['detail'] = 'sensor';
            $btn_list[] = $btn;
        }

        if ($axises_detail_link == "summary_selfremove" && $easy_code == "2"){
            $btn['value'] = '拘束用具';
            $btn['detail'] = 'kousoku';
            $btn_list[] = $btn;

            $btn['value'] = 'ミトン';
            $btn['detail'] = 'mitten';
            $btn_list[] = $btn;
        }
        break;

    //発生場面と内容
    case "scene_content":
        if ($axises_detail == "" && $easy_code != ""){
            $btn['value'] = '場面';
            $btn['detail'] = 'scene';
            $btn_list[] = $btn;

            $btn['value'] = '内容';
            $btn['detail'] = 'content';
            $btn_list[] = $btn;

            if ($easy_code == 2 || $easy_code == 3){
                $btn['value'] = '薬剤';
                $btn['detail'] = 'kind';
                $btn_list[] = $btn;
            }
        }

        if ($axises_detail_link == "scene_content_stumble" && ($easy_code == "293" || $easy_code == "294")){
            $btn['value'] = 'スコア';
            $btn['detail'] = 'score';
            $btn_list[] = $btn;

            $btn['value'] = '危険度';
            $btn['detail'] = 'danger';
            $btn_list[] = $btn;

            $btn['value'] = 'センサー';
            $btn['detail'] = 'sensor';
            $btn_list[] = $btn;
        }

        if ($axises_detail_link == "scene_content_selfremove" && $easy_code == "197"){
            $btn['value'] = '拘束用具';
            $btn['detail'] = 'kousoku';
            $btn_list[] = $btn;

            $btn['value'] = 'ミトン';
            $btn['detail'] = 'mitten';
            $btn_list[] = $btn;
        }
        break;

    //概要2010年版
    case 'scene_content_2010':
        if ($axises_detail == "" && $easy_code != ""){
            $item_code_flg_list = getItemCodeFlgList($con, $easy_code);
            $btn['value'] = '種類';
            $btn['detail'] = 'kind';
            $btn['disabled'] = $item_code_flg_list['kind']    == true ? '' : 'disabled';
            $btn_list[] = $btn;

            $btn['value'] = '場面';
            $btn['detail'] = 'scene';
            $btn['disabled'] = $item_code_flg_list['scene']   == true ? '' : 'disabled';
            $btn_list[] = $btn;

            $btn['value'] = '内容';
            $btn['detail'] = 'content';
            $btn['disabled'] = $item_code_flg_list['content'] == true ? '' : 'disabled';
            $btn_list[] = $btn;
        }

        if (($axises_detail == "content" || $axises_detail == 'kind' || $axises_detail == 'scene') && $easy_code != ""){
            $btn['value'] = '詳細';
            $btn['detail'] = 'detail_' . $axises_detail;
            $btn_list[] = $btn;
        }

        if ($axises_detail_link == "scene_content_stumble" && ($easy_code == "473" || $easy_code == "474")){
            $btn['value'] = 'スコア';
            $btn['detail'] = 'score';
            $btn_list[] = $btn;

            $btn['value'] = '危険度';
            $btn['detail'] = 'danger';
            $btn_list[] = $btn;

            $btn['value'] = 'センサー';
            $btn['detail'] = 'sensor';
            $btn_list[] = $btn;
        }

        if ($axises_detail_link == "scene_content_selfremove" && $easy_code == "421"){
            $btn['value'] = '拘束用具';
            $btn['detail'] = 'kousoku';
            $btn_list[] = $btn;

            $btn['value'] = 'ミトン';
            $btn['detail'] = 'mitten';
            $btn_list[] = $btn;
        }
        break;

    //発生要因詳細
    case "factor":
        if ($axises_detail == "" && $easy_code != ""){
            $btn['value'] = '詳 細';
            $btn['detail'] = 'detail_factor';
            $btn_list[] = $btn;
        }
        break;

    //影響と対応
    case "ic":
        if ($axises_detail == "" && $easy_code != ""){
            if (disp_ic_item($con, $fname, 'influence', $easy_code)){
                $btn['value'] = '影響';
                $btn['detail'] = 'influence';
                $btn_list[] = $btn;
            }
            if (disp_ic_item($con, $fname, 'mental', $easy_code)){
                $btn['value'] = '精神的影響';
                $btn['detail'] = 'mental';
                $btn_list[] = $btn;
            }
            if (disp_ic_item($con, $fname, 'correspondence', $easy_code)){
                $btn['value'] = '対応';
                $btn['detail'] = 'correspondence';
                $btn_list[] = $btn;
            }
        }

        if (($axises_detail == 'correspondence') && $easy_code != ""){
            if (disp_ic_sub_item($con, $fname, $easy_code)){
                $btn['value'] = '詳細';
                $btn['detail'] = 'detail_' . $axises_detail;
                $btn_list[] = $btn;
            }
        }
        break;
    }

    return $btn_list;
}
