<?
require("about_session.php");
require("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// トランザクションを開始
pg_query($con, "begin");
//XX mysql_query("start transaction", $con_mysql);

// チェックされた権限グループをループ
foreach ($group_ids as $tmp_group_id) {

	// 当該権限グループが設定されている職員一覧を取得する
	$sql = "select emp_id from authmst";
	$cond = "where group_id = $tmp_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query("rollback");
		pg_close($con);
//XX 		mysql_query("rollback", $con_mysql);
//XX 		mysql_close($con_mysql);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 該当職員の権限グループを更新
	while ($row = pg_fetch_array($sel)) {
//XX		auth_change_group_of_employee($con, $con_mysql, $row["emp_id"], "", $fname);
		auth_change_group_of_employee($con, $row["emp_id"], "", $fname);
	}

	// 権限グループ情報を削除
	$sql = "delete from authgroup";
	$cond = "where group_id = $tmp_group_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if($tmp_group_id == $default_group_id)
	{
		//権限グループ初期設定の内容もクリアする

		// system_config
		$conf = new Cmx_SystemConfig();

		$ret_val = $conf->set("default_set.group_id","");


	}

}

// 整合性のチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname);
auth_check_consistency($con, $session, $fname);

// トランザクションをコミット
pg_query($con, "commit");
//XX mysql_query("commit", $con_mysql);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 権限グループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'employee_auth_group.php?session=$session';</script>");
?>
