<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 51, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($menu1 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー1を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($menu1) > 60) {
	echo("<script type=\"text/javascript\">alert('メニュー1が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($desc1 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー1の説明を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($menu2 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー2を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($menu2) > 60) {
	echo("<script type=\"text/javascript\">alert('メニュー2が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($desc2 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー2の説明を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($menu3 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー3を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($menu3) > 60) {
	echo("<script type=\"text/javascript\">alert('メニュー3が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if ($desc3 == "") {
	echo("<script type=\"text/javascript\">alert('メニュー3の説明を入力してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// メニュー情報を更新
$sql = "update intramenu set";
$set = array("menu1", "desc1", "menu2", "desc2", "menu3", "desc3");
$setvalue = array($menu1, $desc1, $menu2, $desc2, $menu3, $desc3);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// メニュー設定画面を再表示
echo("<script type=\"text/javascript\">location.href = 'intra_master_menu.php?session=$session';</script>");
?>
