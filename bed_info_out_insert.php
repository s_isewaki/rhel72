<?
require_once("about_comedix.php");
require_once("inpatient_common.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="<? eh($back_url); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="out_yr" value="<? eh($out_yr); ?>">
<input type="hidden" name="out_mon" value="<? eh($out_mon); ?>">
<input type="hidden" name="out_day" value="<? eh($out_day); ?>">
<input type="hidden" name="out_hr" value="<? eh($out_hr); ?>">
<input type="hidden" name="out_min" value="<? eh($out_min); ?>">
<input type="hidden" name="result" value="<? eh($result); ?>">
<input type="hidden" name="result_dtl" value="<? eh($result_dtl); ?>">
<input type="hidden" name="out_rsn_cd" value="<? eh($out_rsn_cd); ?>">
<input type="hidden" name="out_pos_cd" value="<? eh($out_pos_cd); ?>">
<input type="hidden" name="out_pos_dtl" value="<? eh($out_pos_dtl); ?>">
<input type="hidden" name="out_inst_cd" value="<? eh($out_inst_cd); ?>">
<input type="hidden" name="out_sect_cd" value="<? eh($out_sect_cd); ?>">
<input type="hidden" name="out_doctor_no" value="<? eh($out_doctor_no); ?>">
<input type="hidden" name="out_city" value="<? eh($out_city); ?>">
<input type="hidden" name="pr_inst_cd" value="<? eh($pr_inst_cd); ?>">
<input type="hidden" name="pr_sect_cd" value="<? eh($pr_sect_cd); ?>">
<input type="hidden" name="pr_doctor_no" value="<? eh($pr_doctor_no); ?>">
<input type="hidden" name="fd_end" value="<? eh($fd_end); ?>">
<input type="hidden" name="sheet_chg_flg" value="<? eh($sheet_chg_flg); ?>">
<input type="hidden" name="mattress_st_flg" value="<? eh($mattress_st_flg); ?>">
<input type="hidden" name="bed_st_flg" value="<? eh($bed_st_flg); ?>">
<input type="hidden" name="comment" value="<? eh($comment); ?>">
<input type="hidden" name="ward" value="<? eh($ward); ?>">
<input type="hidden" name="ptrm" value="<? eh($ptrm); ?>">
<input type="hidden" name="bed" value="<? eh($bed); ?>">
<input type="hidden" name="bedundec1" value="<? eh($bedundec1); ?>">
<input type="hidden" name="bedundec2" value="<? eh($bedundec2); ?>">
<input type="hidden" name="bedundec3" value="<? eh($bedundec3); ?>">
<input type="hidden" name="back_in_yr" value="<? eh($back_in_yr); ?>">
<input type="hidden" name="back_in_mon" value="<? eh($back_in_mon); ?>">
<input type="hidden" name="back_in_day" value="<? eh($back_in_day); ?>">
<input type="hidden" name="back_in_hr" value="<? eh($back_in_hr); ?>">
<input type="hidden" name="back_in_min" value="<? eh($back_in_min); ?>">
<input type="hidden" name="back_sect" value="<? eh($back_sect); ?>">
<input type="hidden" name="back_doc" value="<? eh($back_doc); ?>">
<input type="hidden" name="back_change_purpose" value="<? eh($back_change_purpose); ?>">
<input type="hidden" name="session" value="<? eh($session); ?>">
<input type="hidden" name="pt_id" value="<? eh($pt_id); ?>">
</form>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェック情報を取得
$sql = "select * from bedcheckout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
for ($i = 1; $i <= 9; $i++) {
	$varname = "required$i";
	$$varname = pg_fetch_result($sel, 0, "required$i");
}

// 入力チェック
if (!checkdate($out_mon, $out_day, $out_yr)) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院日が不正です。", "items");
}
$date = "$out_yr$out_mon$out_day";
if ($date > date("Ymd")) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院日には未来の日付を指定できません。", "items");
}
if ($required1 == "t" && $result == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("転帰を選択してください。", "items");
}
if (strlen($result_dtl) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("転帰詳細が長すぎます。", "items");
}
if ($required2 == "t" && $out_rsn_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院理由を選択してください。", "items");
}
if ($required3 == "t" && $out_pos_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院先区分を選択してください。", "items");
}
if (strlen($out_pos_dtl) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院先区分の詳細が長すぎます。", "items");
}
if ($required4 == "t" && $out_inst_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院先施設名を選択してください。", "items");
}
if ($required5 == "t" && $out_sect_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("診療科を選択してください。", "items");
}
if ($required6 == "t" && $out_doctor_no == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("担当医を選択してください。", "items");
}
if (strlen($out_city) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院先市区町村が長すぎます。", "items");
}
if ($required7 == "t" && $out_city == "") {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("退院先市区町村を入力してください。", "items");
}
if ($required9 == "t") {
	if ($pr_inst_cd == "") {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("かかりつけ医の医療機関名を選択してください。", "items");
	}
	if ($pr_sect_cd == "") {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("かかりつけ医の診療科を選択してください。", "items");
	}
	if ($pr_doctor_no == "") {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("かかりつけ医の担当医を選択してください。", "items");
	}
}
if (strlen($comment) > 200) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("コメントが長すぎます。", "items");
}
if ($back_in_yr == "") {$back_in_yr = "-";}
if ($back_in_mon == "") {$back_in_mon = "-";}
if ($back_in_day == "") {$back_in_day = "-";}
if ($back_in_hr == "") {$back_in_hr = "--";}
if ($back_in_min == "") {$back_in_min = "--";}
if (($back_in_yr == "-" && ($back_in_mon != "-" || $back_in_day != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_mon == "-" && ($back_in_yr != "-" || $back_in_day != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_day == "-" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_hr == "--" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_day != "-" || $back_in_min != "--")) || ($back_in_min == "--" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_day != "-" || $back_in_hr != "--"))) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("入院日時が不正です。", "items");
}
if ($back_in_yr != "-") {
	if (!checkdate($back_in_mon, $back_in_day, $back_in_yr)) {
		pg_query($con, "rollback");
		pg_close($con);
		js_alert_exit("入院日が不正です。", "items");
	}
}
if (strlen($back_change_purpose) > 200) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("転院目的が長すぎます。", "items");
}

// 入力値の編集
$time = sprintf("%02d%02d", $out_hr, $out_min);
$sheet_chg_flg = ($sheet_chg_flg != "t") ? "f" : "t";
$mattress_st_flg = ($mattress_st_flg != "t") ? "f" : "t";
$bed_st_flg = ($bed_st_flg != "t") ? "f" : "t";

if ($ward == 0) {
	$bldg_cd = null;
	$ward_cd = null;
} else {
	list($bldg_cd, $ward_cd) = explode("-", $ward);
}
if ($ptrm == 0) {$ptrm = null;}
if ($bed == 0) {$bed = null;}
if ($back_in_yr == "-") {
	$in_date = null;
	$in_time = null;
} else {
	$in_date = sprintf("%04d%02d%02d", $back_in_yr, $back_in_mon, $back_in_day);
	$in_time = sprintf("%02d%02d", $back_in_hr, $back_in_min);
}
if ($back_sect == 0) {$back_sect = null;}
if ($back_doc == 0) {$back_doc = null;}

// 入院状況レコードを登録
update_inpatient_condition($con, $pt_id, $date, $time, "2", $session, $fname, true);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = q("where session_id = '%s'", $session);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 入院患者情報を更新
if ($out_sect_rireki == "") {$out_sect_rireki = null;}
if ($out_doctor_no == "") {$out_doctor_no = null;}
if ($pr_sect_rireki == "") {$pr_sect_rireki = null;}
if ($pr_doctor_no == "") {$pr_doctor_no = null;}
$sql = "update inptmst set";
$set = array("inpt_in_flg", "inpt_out_dt", "inpt_out_flg", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_out_tm", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_out_rsn_rireki", "inpt_out_rsn_cd", "inpt_pr_inst_cd", "inpt_pr_sect_rireki", "inpt_pr_sect_cd", "inpt_pr_doctor_no");
$setvalue = array("f", $date, "t", date("Ymd"), date("Hi"), $emp_id, $time, $result, $result_dtl, $out_pos_rireki, $out_pos_cd, $out_pos_dtl, $fd_end, $comment, $out_inst_cd, $out_sect_rireki, $out_sect_cd, $out_doctor_no, $out_city, $bldg_cd, $ward_cd, $ptrm, $bed, $in_date, $in_time, $back_sect, $back_doc, $back_change_purpose, $out_rsn_rireki, $out_rsn_cd, $pr_inst_cd, $pr_sect_rireki, $pr_sect_cd, $pr_doctor_no);
$cond = q("where ptif_id = '%s'", $pt_id);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院履歴レコードを作成
$sql = "select * from inptmst";
$cond = q("where ptif_id = '%s'", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$inpt_lt_kn_nm = pg_fetch_result($sel, 0, "inpt_lt_kn_nm");
$inpt_ft_kn_nm = pg_fetch_result($sel, 0, "inpt_ft_kn_nm");
$inpt_lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$inpt_ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$inpt_keywd = pg_fetch_result($sel, 0, "inpt_keywd");
$inpt_in_dt = pg_fetch_result($sel, 0, "inpt_in_dt");
$inpt_out_dt = pg_fetch_result($sel, 0, "inpt_out_dt");
$inpt_arrival = pg_fetch_result($sel, 0, "inpt_arrival");
$inpt_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$inpt_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$dr_id = pg_fetch_result($sel, 0, "dr_id");
$nurse_id = pg_fetch_result($sel, 0, "nurse_id");
$inpt_meet_flg = pg_fetch_result($sel, 0, "inpt_meet_flg");
$inpt_dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
$inpt_dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
$inpt_dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
$inpt_rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
$inpt_rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
$inpt_rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
$inpt_ecg_flg = pg_fetch_result($sel, 0, "inpt_ecg_flg");
$inpt_sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
$inpt_observe = pg_fetch_result($sel, 0, "inpt_observe");
$inpt_purpose_rireki = pg_fetch_result($sel, 0, "inpt_purpose_rireki");
if ($inpt_purpose_rireki == "") {$inpt_purpose_rireki = null;}
$inpt_purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$inpt_purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");
$inpt_short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
if ($inpt_short_stay == "") {$inpt_short_stay = null;}
$inpt_outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
if ($inpt_outpt_test == "") {$inpt_outpt_test = null;}
$inpt_diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
$inpt_nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$inpt_free = pg_fetch_result($sel, 0, "inpt_free");
$inpt_special = pg_fetch_result($sel, 0, "inpt_special");
$inpt_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$inpt_disease = pg_fetch_result($sel, 0, "inpt_disease");
$inpt_patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
$inpt_patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
$inpt_up_dt = pg_fetch_result($sel, 0, "inpt_up_dt");
$inpt_up_tm = pg_fetch_result($sel, 0, "inpt_up_tm");
$inpt_op_no = pg_fetch_result($sel, 0, "inpt_op_no");
$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
$inpt_out_tm = pg_fetch_result($sel, 0, "inpt_out_tm");
$inpt_out_res_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");
$inpt_out_res_tm = pg_fetch_result($sel, 0, "inpt_out_res_tm");
$inpt_in_res_dt_flg = pg_fetch_result($sel, 0, "inpt_in_res_dt_flg");
$inpt_in_res_dt = pg_fetch_result($sel, 0, "inpt_in_res_dt");
$inpt_in_res_tm = pg_fetch_result($sel, 0, "inpt_in_res_tm");
$inpt_in_res_ym = pg_fetch_result($sel, 0, "inpt_in_res_ym");
$inpt_in_res_td = pg_fetch_result($sel, 0, "inpt_in_res_td");
$inpt_ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
$inpt_ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
$inpt_ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
$inpt_ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
$inpt_emergency = pg_fetch_result($sel, 0, "inpt_emergency");
$inpt_in_plan_period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
$inpt_in_way = pg_fetch_result($sel, 0, "inpt_in_way");
$inpt_nursing = pg_fetch_result($sel, 0, "inpt_nursing");
$inpt_staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
$inpt_fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
$inpt_fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
$inpt_wish_rm_rireki = pg_fetch_result($sel, 0, "inpt_wish_rm_rireki");
if ($inpt_wish_rm_rireki == "") {$inpt_wish_rm_rireki = null;}
$inpt_wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
$inpt_fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
$inpt_result = pg_fetch_result($sel, 0, "inpt_result");
$inpt_result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
$inpt_out_rsn_rireki = pg_fetch_result($sel, 0, "inpt_out_rsn_rireki");
if ($inpt_out_rsn_rireki == "") {$inpt_out_rsn_rireki = null;}
$inpt_out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");
$inpt_out_pos_rireki = pg_fetch_result($sel, 0, "inpt_out_pos_rireki");
if ($inpt_out_pos_rireki == "") {$inpt_out_pos_rireki = null;}
$inpt_out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");
$inpt_out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
$inpt_fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
$inpt_out_comment = pg_fetch_result($sel, 0, "inpt_out_comment");
$inpt_res_chg_psn = pg_fetch_result($sel, 0, "inpt_res_chg_psn");
$inpt_res_chg_psn_dtl = pg_fetch_result($sel, 0, "inpt_res_chg_psn_dtl");
$inpt_res_chg_rsn = pg_fetch_result($sel, 0, "inpt_res_chg_rsn");
$inpt_res_chg_ctt = pg_fetch_result($sel, 0, "inpt_res_chg_ctt");
$inpt_except_flg = pg_fetch_result($sel, 0, "inpt_except_flg");
$inpt_except_from_date1 = pg_fetch_result($sel, 0, "inpt_except_from_date1");
$inpt_except_to_date1 = pg_fetch_result($sel, 0, "inpt_except_to_date1");
$inpt_except_from_date2 = pg_fetch_result($sel, 0, "inpt_except_from_date2");
$inpt_except_to_date2 = pg_fetch_result($sel, 0, "inpt_except_to_date2");
$inpt_except_from_date3 = pg_fetch_result($sel, 0, "inpt_except_from_date3");
$inpt_except_to_date3 = pg_fetch_result($sel, 0, "inpt_except_to_date3");
$inpt_except_from_date4 = pg_fetch_result($sel, 0, "inpt_except_from_date4");
$inpt_except_to_date4 = pg_fetch_result($sel, 0, "inpt_except_to_date4");
$inpt_except_from_date5 = pg_fetch_result($sel, 0, "inpt_except_from_date5");
$inpt_except_to_date5 = pg_fetch_result($sel, 0, "inpt_except_to_date5");
$inpt_insurance = pg_fetch_result($sel, 0, "inpt_insurance");
$inpt_insu_rate_rireki = pg_fetch_result($sel, 0, "inpt_insu_rate_rireki");
if ($inpt_insu_rate_rireki == "") {$inpt_insu_rate_rireki = null;}
$inpt_insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
$inpt_reduced = pg_fetch_result($sel, 0, "inpt_reduced");
if ($inpt_reduced == "") {$inpt_reduced = null;}
$inpt_impaired = pg_fetch_result($sel, 0, "inpt_impaired");
if ($inpt_impaired == "") {$inpt_impaired = null;}
$inpt_insured = pg_fetch_result($sel, 0, "inpt_insured");
$inpt_privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
$inpt_privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
$inpt_pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
$inpt_except_reason_id1 = pg_fetch_result($sel, 0, "inpt_except_reason_id1");
if ($inpt_except_reason_id1 == "") {$inpt_except_reason_id1 = null;}
$inpt_except_reason_id2 = pg_fetch_result($sel, 0, "inpt_except_reason_id2");
if ($inpt_except_reason_id2 == "") {$inpt_except_reason_id2 = null;}
$inpt_except_reason_id3 = pg_fetch_result($sel, 0, "inpt_except_reason_id3");
if ($inpt_except_reason_id3 == "") {$inpt_except_reason_id3 = null;}
$inpt_except_reason_id4 = pg_fetch_result($sel, 0, "inpt_except_reason_id4");
if ($inpt_except_reason_id4 == "") {$inpt_except_reason_id4 = null;}
$inpt_except_reason_id5 = pg_fetch_result($sel, 0, "inpt_except_reason_id5");
if ($inpt_except_reason_id5 == "") {$inpt_except_reason_id5 = null;}
$inpt_out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
$inpt_out_sect_rireki = pg_fetch_result($sel, 0, "inpt_out_sect_rireki");
if ($inpt_out_sect_rireki == "") {$inpt_out_sect_rireki = null;}
$inpt_out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
$inpt_out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
if ($inpt_out_doctor_no == "") {$inpt_out_doctor_no = null;}
$inpt_out_city = pg_fetch_result($sel, 0, "inpt_out_city");
$inpt_back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
if ($inpt_back_bldg_cd == "") {$inpt_back_bldg_cd = null;}
$inpt_back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
if ($inpt_back_ward_cd == "") {$inpt_back_ward_cd = null;}
$inpt_back_ptrm_room_no = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
if ($inpt_back_ptrm_room_no == "") {$inpt_back_ptrm_room_no = null;}
$inpt_back_bed_no = pg_fetch_result($sel, 0, "inpt_back_bed_no");
if ($inpt_back_bed_no == "") {$inpt_back_bed_no = null;}
$inpt_back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
if ($inpt_back_in_dt == "") {$inpt_back_in_dt = null;}
$inpt_back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
if ($inpt_back_in_tm == "") {$inpt_back_in_tm = null;}
$inpt_back_sect_id = pg_fetch_result($sel, 0, "inpt_back_sect_id");
if ($inpt_back_sect_id == "") {$inpt_back_sect_id = null;}
$inpt_back_dr_id = pg_fetch_result($sel, 0, "inpt_back_dr_id");
if ($inpt_back_dr_id == "") {$inpt_back_dr_id = null;}
$inpt_back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");
$inpt_care_no = pg_fetch_result($sel, 0, "inpt_care_no");
$inpt_care_grd_rireki = pg_fetch_result($sel, 0, "inpt_care_grd_rireki");
if ($inpt_care_grd_rireki == "") {$inpt_care_grd_rireki = null;}
$inpt_care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");
$inpt_care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
$inpt_care_from = pg_fetch_result($sel, 0, "inpt_care_from");
$inpt_care_to = pg_fetch_result($sel, 0, "inpt_care_to");
$inpt_dis_grd_rireki = pg_fetch_result($sel, 0, "inpt_dis_grd_rireki");
if ($inpt_dis_grd_rireki == "") {$inpt_dis_grd_rireki = null;}
$inpt_dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
$inpt_dis_type_rireki = pg_fetch_result($sel, 0, "inpt_dis_type_rireki");
if ($inpt_dis_type_rireki == "") {$inpt_dis_type_rireki = null;}
$inpt_dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
$inpt_spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");
$inpt_spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
$inpt_spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
$inpt_intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
$inpt_intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
if ($inpt_intro_sect_rireki == "") {$inpt_intro_sect_rireki = null;}
$inpt_intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
$inpt_intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
if ($inpt_intro_doctor_no == "") {$inpt_intro_doctor_no = null;}
$inpt_hotline = pg_fetch_result($sel, 0, "inpt_hotline");
if ($inpt_hotline == "") {$inpt_hotline = null;}
$inpt_pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
$inpt_pr_sect_rireki = pg_fetch_result($sel, 0, "inpt_pr_sect_rireki");
if ($inpt_pr_sect_rireki == "") {$inpt_pr_sect_rireki = null;}
$inpt_pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
$inpt_pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
if ($inpt_pr_doctor_no == "") {$inpt_pr_doctor_no = null;}
$in_updated = pg_fetch_result($sel, 0, "in_updated");
$back_res_updated = pg_fetch_result($sel, 0, "back_res_updated");
$inpt_exemption = pg_fetch_result($sel, 0, "inpt_exemption");
if ($inpt_exemption == "") {$inpt_exemption = null;}

$sql = "insert into inpthist (ptif_id, bldg_cd, ward_cd, ptrm_room_no, inpt_lt_kn_nm, inpt_ft_kn_nm, inpt_lt_kj_nm, inpt_ft_kj_nm, inpt_keywd, inpt_in_dt, inpt_out_dt, inpt_enti_id, inpt_sect_id, dr_id, nurse_id, inpt_meet_flg, inpt_ecg_flg, inpt_sample_blood, inpt_observe, inpt_purpose_rireki, inpt_purpose_cd, inpt_purpose_content, inpt_diagnosis, inpt_special, inpt_bed_no, inpt_disease, inpt_up_dt, inpt_up_tm, inpt_op_no, inpt_in_tm, inpt_out_tm, inpt_out_res_dt, inpt_out_res_tm, inpt_in_res_dt_flg, inpt_in_res_dt, inpt_in_res_tm, inpt_in_res_ym, inpt_in_res_td, inpt_ope_dt_flg, inpt_ope_dt, inpt_ope_ym, inpt_ope_td, inpt_emergency, inpt_in_plan_period, inpt_in_way, inpt_nursing, inpt_staple_fd, inpt_fd_type, inpt_fd_dtl, inpt_wish_rm_rireki, inpt_wish_rm_cd, inpt_fd_start, inpt_result, inpt_result_dtl, inpt_out_pos_rireki, inpt_out_pos_cd, inpt_out_pos_dtl, inpt_fd_end, inpt_out_comment, inpt_res_chg_psn, inpt_res_chg_psn_dtl, inpt_res_chg_rsn, inpt_res_chg_ctt, inpt_except_flg, inpt_insurance, inpt_except_from_date1, inpt_except_to_date1, inpt_except_from_date2, inpt_except_to_date2, inpt_except_from_date3, inpt_except_to_date3, inpt_except_from_date4, inpt_except_to_date4, inpt_except_from_date5, inpt_except_to_date5, inpt_privacy_flg, inpt_privacy_text, inpt_except_reason_id1, inpt_except_reason_id2, inpt_except_reason_id3, inpt_except_reason_id4, inpt_except_reason_id5, inpt_out_inst_cd, inpt_out_sect_rireki, inpt_out_sect_cd, inpt_out_doctor_no, inpt_out_city, inpt_back_bldg_cd, inpt_back_ward_cd, inpt_back_ptrm_room_no, inpt_back_bed_no, inpt_back_in_dt, inpt_back_in_tm, inpt_back_sect_id, inpt_back_dr_id, inpt_back_change_purpose, inpt_short_stay, inpt_outpt_test, inpt_insu_rate_rireki, inpt_insu_rate_cd, inpt_reduced, inpt_impaired, inpt_insured, inpt_arrival, inpt_patho_from, inpt_patho_to, inpt_out_rsn_rireki, inpt_out_rsn_cd, inpt_dcb_cls, inpt_dcb_bas, inpt_dcb_exp, inpt_rhb_cls, inpt_rhb_bas, inpt_rhb_exp, inpt_care_no, inpt_care_grd_rireki, inpt_care_grd_cd, inpt_care_apv, inpt_care_from, inpt_care_to, inpt_dis_grd_rireki, inpt_dis_grd_cd, inpt_dis_type_rireki, inpt_dis_type_cd, inpt_spec_name, inpt_spec_from, inpt_spec_to, inpt_pre_div, inpt_intro_inst_cd, inpt_intro_sect_rireki, inpt_intro_sect_cd, inpt_intro_doctor_no, inpt_hotline, inpt_nrs_obsv, inpt_free, inpt_pr_inst_cd, inpt_pr_sect_rireki, inpt_pr_sect_cd, inpt_pr_doctor_no, in_updated, out_updated, back_res_updated, inpt_exemption) values (";
$content = array($pt_id, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_lt_kn_nm, $inpt_ft_nm, $inpt_lt_kj_nm, $inpt_ft_kj_nm, $inpt_keywd, $inpt_in_dt, $inpt_out_dt, $inpt_enti_id, $inpt_sect_id, $dr_id, $nurse_id, $inpt_meet_flg, $inpt_ecg_flg, $inpt_sample_blood, $inpt_observe, $inpt_purpose_rireki, $inpt_purpose_cd, $inpt_purpose_content, $inpt_diagnosis, $inpt_special, $inpt_bed_no, $inpt_disease, $inpt_up_dt, $inpt_up_tm, $inpt_op_no, $inpt_in_tm, $inpt_out_tm, $inpt_out_res_dt, $inpt_out_res_tm, $inpt_in_res_dt_flg, $inpt_in_res_dt, $inpt_in_res_tm, $inpt_in_res_ym, $inpt_in_res_td, $inpt_ope_dt_flg, $inpt_ope_dt, $inpt_ope_ym, $inpt_ope_td, $inpt_emergency, $inpt_in_plan_period, $inpt_in_way, $inpt_nursing, $inpt_staple_fd, $inpt_fd_type, $inpt_fd_dtl, $inpt_wish_rm_rireki, $inpt_wish_rm_cd, $inpt_fd_start, $inpt_result, $inpt_result_dtl, $inpt_out_pos_rireki, $inpt_out_pos_cd, $inpt_out_pos_dtl, $inpt_fd_end, $inpt_out_comment, $inpt_res_chg_psn, $inpt_res_chg_psn_dtl, $inpt_res_chg_rsn, $inpt_res_chg_ctt, $inpt_except_flg, $inpt_insurance, $inpt_except_from_date1, $inpt_except_to_date1, $inpt_except_from_date2, $inpt_except_to_date2, $inpt_except_from_date3, $inpt_except_to_date3, $inpt_except_from_date4, $inpt_except_to_date4, $inpt_except_from_date5, $inpt_except_to_date5, $inpt_privacy_flg, $inpt_privacy_text, $inpt_except_reason_id1, $inpt_except_reason_id2, $inpt_except_reason_id3, $inpt_except_reason_id4, $inpt_except_reason_id5, $inpt_out_inst_cd, $inpt_out_sect_rireki, $inpt_out_sect_cd, $inpt_out_doctor_no, $inpt_out_city, $inpt_back_bldg_cd, $inpt_back_ward_cd, $inpt_back_ptrm_room_no, $inpt_back_bed_no, $inpt_back_in_dt, $inpt_back_in_tm, $inpt_back_sect_id, $inpt_back_dr_id, $inpt_back_change_purpose, $inpt_short_stay, $inpt_outpt_test, $inpt_insu_rate_rireki, $inpt_insu_rate_cd, $inpt_reduced, $inpt_impaired, $inpt_insured, $inpt_arrival, $inpt_patho_from, $inpt_patho_to, $inpt_out_rsn_rireki, $inpt_out_rsn_cd, $inpt_dcb_cls, $inpt_dcb_bas, $inpt_dcb_exp, $inpt_rhb_cls, $inpt_rhb_bas, $inpt_rhb_exp, $inpt_care_no, $inpt_care_grd_rireki, $inpt_care_grd_cd, $inpt_care_apv, $inpt_care_from, $inpt_care_to, $inpt_dis_grd_rireki, $inpt_dis_grd_cd, $inpt_dis_type_rireki, $inpt_dis_type_cd, $inpt_spec_name, $inpt_spec_from, $inpt_spec_to, $inpt_pre_div, $inpt_intro_inst_cd, $inpt_intro_sect_rireki, $inpt_intro_sect_cd, $inpt_intro_doctor_no, $inpt_hotline, $inpt_nrs_obsv, $inpt_free, $inpt_pr_inst_cd, $inpt_pr_sect_rireki, $inpt_pr_sect_cd, $inpt_pr_doctor_no, $in_updated, date("YmdHis"), $back_res_updated, $inpt_exemption);
$ins = insert_into_table($con, $sql, q($content), $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 病床レコードを更新して空床にする
remove_inpatient_from_bed($con, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no, $fname);

// チェックリストの履歴を作成
$sql = "select * from inptq";
$cond = q("where ptif_id = '%s'", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (pg_num_rows($sel) > 0) {
	$inpt_q1 = pg_fetch_result($sel, 0, "inpt_q1");
	$inpt_q2 = pg_fetch_result($sel, 0, "inpt_q2");
	$inpt_q3 = pg_fetch_result($sel, 0, "inpt_q3");
	$inpt_q4_1 = pg_fetch_result($sel, 0, "inpt_q4_1");
	$inpt_q4_2 = pg_fetch_result($sel, 0, "inpt_q4_2");
	$inpt_q5 = pg_fetch_result($sel, 0, "inpt_q5");
	$inpt_q6_1 = pg_fetch_result($sel, 0, "inpt_q6_1");
	$inpt_q6_2 = pg_fetch_result($sel, 0, "inpt_q6_2");
	$inpt_q7 = pg_fetch_result($sel, 0, "inpt_q7");
	$inpt_q7_com = pg_fetch_result($sel, 0, "inpt_q7_com");
	$inpt_q8_1 = pg_fetch_result($sel, 0, "inpt_q8_1");
	$inpt_q8_2 = pg_fetch_result($sel, 0, "inpt_q8_2");

	$sql = "insert into inptqhist (ptif_id, inpt_in_dt, inpt_in_tm, inpt_q1, inpt_q2, inpt_q3, inpt_q4_1, inpt_q4_2, inpt_q5, inpt_q6_1, inpt_q6_2, inpt_q7, inpt_q7_com, inpt_q8_1, inpt_q8_2) values (";
	$content = array($pt_id, $inpt_in_dt, $inpt_in_tm, $inpt_q1, $inpt_q2, $inpt_q3, $inpt_q4_1, $inpt_q4_2, $inpt_q5, $inpt_q6_1, $inpt_q6_2, $inpt_q7, $inpt_q7_com, $inpt_q8_1, $inpt_q8_2);
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}

	$sql = "delete from inptq";
	$cond = q("where ptif_id = '%s'", $pt_id);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 栄養問診表の履歴を作成
$sql = "select * from inptnut";
$cond = q("where ptif_id = '%s'", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (pg_num_rows($sel) > 0) {
	$nut_q1 = pg_fetch_result($sel, 0, "nut_q1");
	$nut_q1_1 = pg_fetch_result($sel, 0, "nut_q1_1");
	$nut_q1_2 = pg_fetch_result($sel, 0, "nut_q1_2");
	$nut_q1_3 = pg_fetch_result($sel, 0, "nut_q1_3");
	$nut_q1_4 = pg_fetch_result($sel, 0, "nut_q1_4");
	$nut_q1_5_com = pg_fetch_result($sel, 0, "nut_q1_5_com");
	$nut_q1_6_com = pg_fetch_result($sel, 0, "nut_q1_6_com");
	$nut_q2_1 = pg_fetch_result($sel, 0, "nut_q2_1");
	$nut_q2_2 = pg_fetch_result($sel, 0, "nut_q2_2");
	$nut_q2_3 = pg_fetch_result($sel, 0, "nut_q2_3");
	$nut_q2_4 = pg_fetch_result($sel, 0, "nut_q2_4");
	$nut_q2_5 = pg_fetch_result($sel, 0, "nut_q2_5");
	$nut_q2_6 = pg_fetch_result($sel, 0, "nut_q2_6");
	$nut_q2_7 = pg_fetch_result($sel, 0, "nut_q2_7");
	$nut_q2_8 = pg_fetch_result($sel, 0, "nut_q2_8");
	$nut_q3 = pg_fetch_result($sel, 0, "nut_q3");
	$nut_q4 = pg_fetch_result($sel, 0, "nut_q4");
	$nut_q5_1 = pg_fetch_result($sel, 0, "nut_q5_1");
	$nut_q5_2 = pg_fetch_result($sel, 0, "nut_q5_2");

	$sql = "insert into inptnuthist (ptif_id, inpt_in_dt, inpt_in_tm, nut_q1, nut_q1_1, nut_q1_2, nut_q1_3, nut_q1_4, nut_q1_5_com, nut_q1_6_com, nut_q2_1, nut_q2_2, nut_q2_3, nut_q2_4, nut_q2_5, nut_q2_6, nut_q2_7, nut_q2_8, nut_q3, nut_q4, nut_q5_1, nut_q5_2) values (";
	$content = array($pt_id, $inpt_in_dt, $inpt_in_tm, $nut_q1, $nut_q1_1, $nut_q1_2, $nut_q1_3, $nut_q1_4, $nut_q1_5_com, $nut_q1_6_com, $nut_q2_1, $nut_q2_2, $nut_q2_3, $nut_q2_4, $nut_q2_5, $nut_q2_6, $nut_q2_7, $nut_q2_8, $nut_q3, $nut_q4, $nut_q5_1, $nut_q5_2);
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}

	$sql = "delete from inptnut";
	$cond = q("where ptif_id = '%s'", $pt_id);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 入院状況履歴の作成
$sql = "update inptcond set";
$set = array("hist_flg");
$setvalue = array("t");
$cond = q("where ptif_id = '%s' and hist_flg = 'f'", $pt_id);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// ベットメイクが必要ならレコードを作成
if ($sheet_chg_flg == "t" || $mattress_st_flg == "t" || $bed_st_flg == "t") {
	$sql = "select max(bdmk_id) from bedmake";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
	$bdmk_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

	$sql = "insert into bedmake (bdmk_id, bldg_cd, ward_cd, room_no, bed_no, sheet_chg_flg, mattress_st_flg, bed_st_flg, order_dt, order_tm) values (";
	$content = array($bdmk_id, $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no, $sheet_chg_flg, $mattress_st_flg, $bed_st_flg, date("Ymd"), date("Hi"));
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 入院分の担当者情報を退避
$sql = "select emp_id, order_no from inptop";
$cond = q("where ptif_id = '%s' order by order_no", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院分の担当者情報を削除
$sql = "delete from inptop";
$cond = q("where ptif_id = '%s'", $pt_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院履歴分の担当者情報を作成
while ($row = pg_fetch_array($sel)) {
	$sql = "insert into inptophist (ptif_id, inpt_in_dt, inpt_in_tm, emp_id, order_no) values (";
	$content = array($pt_id, $inpt_in_dt, $inpt_in_tm, $row["emp_id"], $row["order_no"]);
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 退院予定表の備考をコピー
$sql = "select note from bed_outnote";
$cond = q("where type = 'p' and mode = 'r' and ptif_id = '%s'", $pt_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (pg_num_rows($sel) == 0) {
	$note = "";
} else {
	$note = pg_fetch_result($sel, 0, "note");
	$sql = "delete from bed_outnote";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}
$sql = "delete from bed_outnote";
$cond = q("where type = 'p' and mode = 'o' and ptif_id = '%s' and in_dttm = '%s%s'", $pt_id, $inpt_in_dt, $inpt_in_tm);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$sql = "insert into bed_outnote (type, mode, ptif_id, in_dttm, note) values (";
$content = array("p", "o", $pt_id, "$inpt_in_dt$inpt_in_tm", $note);
$ins = insert_into_table($con, $sql, q($content), $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 戻り入院の備考を引き継ぐ
$sql = "update bed_innote set";
$set = array("in_dttm");
$setvalue = array("$inpt_in_dt$inpt_in_tm");
$cond = q("where type = 'p' and mode = 'r' and ptif_id = '%s' and back_flg = 't' and in_dttm is null", $pt_id);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);
?>
<script type="text/javascript">
opener.location.reload();
self.close();
</script>
</body>
