<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 54, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// ライフステージ一覧を取得
$sql = "select lifestage_id, name from lifestage";
$cond = "order by order_no";
$sel_stage = select_from_table($con, $sql, $cond, $fname);
if ($sel_stage == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// イントラメニュー名を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");
$intra_menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
?>
<title>マスターメンテナンス | <? echo($intra_menu3); ?>設定 | ライフステージ一覧</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteStage() {
	if (confirm('削除してよろしいですか？')) {
		document.delform.submit();
	}

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($intra_menu3); ?></b></a> &gt; <a href="life_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="intra_life.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="life_master_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#5279a5"><a href="life_master_stage_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ライフステージ一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="life_master_stage_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライフステージ登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteStage();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="delform" action="life_master_stage_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff">
<td width="30" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライフステージ</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel_stage)) {
	$stage_id = $row["lifestage_id"];
	$name = $row["name"];

	echo("<tr>\n");
	echo("<td align=\"center\"><input type=\"checkbox\" name=\"stage_ids[]\" value=\"$stage_id\"></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"life_master_stage_update.php?session=$session&stage_id=$stage_id\">$name</a></font></td>\n");
	echo("</tr>\n");
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
