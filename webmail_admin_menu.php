<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ウェブメール | インターネット接続</title>
<?
require_once("Cmx.php");
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 66, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

if ($back != "t") {
	// 環境設定情報を取得
	$sql = "select * from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$use_email = pg_fetch_result($sel, 0, "use_email");
	$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");
	$webmail_from_order1 = pg_fetch_result($sel, 0, "webmail_from_order1");
	$webmail_from_order2 = pg_fetch_result($sel, 0, "webmail_from_order2");
	$webmail_from_order3 = pg_fetch_result($sel, 0, "webmail_from_order3");
	$smtp_server_address = pg_fetch_result($sel, 0, "smtp_server_address");
	$smtp_port = pg_fetch_result($sel, 0, "smtp_port");
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkImap() {
	if (document.mainform.use_cyrus[0].checked || document.mainform.use_cyrus[0].disabled) {
		return true;
	}
	return confirm('一度「CoMedix以外のIMAPサーバを使用する」を選択すると、\n今後「CoMedixのCyrusを使用する」に戻せなくなります。\n更新してよろしいですか？');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="webmail_menu.php?session=<? echo($session); ?>"><img src="img/icon/b01.gif" width="32" height="32" border="0" alt="ウェブメール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_menu.php?session=<? echo($session); ?>"><b>ウェブメール</b></a> &gt; <a href="webmail_admin_menu.php?session=<? echo($session); ?>&sort=<? echo($sort); ?>&show_content=<? echo($show_content); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="webmail_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="150" align="center" bgcolor="#5279a5"><a href="webmail_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>インターネット接続</b></font></a></td>
<td width="5"></td>
<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_display.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション設定</font></a></td>
<td width="5"></td>
<td width="150" align="center" bgcolor="#bdd1e7"><a href="webmail_admin_shoot.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">トラブルシュート</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form name="mainform" action="webmail_admin_menu_update.php" method="post" onsubmit="return checkImap();">
<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">Eメール送受信</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="use_email" type="radio" value="t"<? if ($use_email == "t") {echo(" checked");} ?>>する
<input name="use_email" type="radio" value="f"<? if ($use_email == "f") {echo(" checked");} ?>>しない
</font></td>
</tr>
<tr height="30">
<td width="30%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">IMAPサーバ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="use_cyrus" type="radio" value="t"<? if ($use_cyrus == "t") {echo(" checked");} else {echo(" disabled");} ?>>CoMedixのCyrusを使用する
<input name="use_cyrus" type="radio" value="f"<? if ($use_cyrus == "f") {echo(" checked");} ?>>CoMedix以外のIMAPサーバを使用する
</font></td>
</tr>
<tr height="30">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウェブメール差出人表示順</font></td>
<td style="padding:4px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
1. <select name="webmail_from_order1"><option value="1"<? if ($webmail_from_order1 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order1 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order1 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス</select><br>
2. <select name="webmail_from_order2"><option value="1"<? if ($webmail_from_order2 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order2 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order2 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス<option value=""<? if ($webmail_from_order2 == "") {echo(" selected");} ?>>表示しない</select><br>
3. <select name="webmail_from_order3"><option value="1"<? if ($webmail_from_order3 == "1") {echo(" selected");} ?>>院内メールアドレス<option value="2"<? if ($webmail_from_order3 == "2") {echo(" selected");} ?>>Eメールアドレス<option value="3"<? if ($webmail_from_order3 == "3") {echo(" selected");} ?>>ウェブメールオプションのメールアドレス<option value=""<? if ($webmail_from_order3 == "") {echo(" selected");} ?>>表示しない</select><br>
</font></td>
</tr>
<tr height="30">
<td width="23%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">SMTPサーバ名</font></td>
<td><input type="text" name="smtp_server_address" value="<?=$smtp_server_address?>" size="40" maxlength="100"></td>
</tr>
<tr height="30">
<td width="23%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ポート番号</font></td>
<td><input type="text" name="smtp_port" value="<?=$smtp_port?>" size="5" maxlength="5"></td>
</tr>
</table>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr height="30">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
