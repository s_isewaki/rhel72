<?
/*
 * GETでsession, 開始日(Ymd or 0), 終了日（Ymd or 0）
 * , プロジェクト表示の有無（on or off）, 院内行事表示の有無（on or off）
 * を渡している。
 * 
 */
ob_start();
require_once("about_session.php");
require_once("about_authority.php");

/*
 * schdArrayクラス
 * @auther murakami
 * @version $Revision: 1.00 $
 */
class schdArray {
    // 各要素が入る多次元連想配列
    var $list = array();
    // SQL文（前半）が入る
    var $sql;
    // SQL文（後半）が入る
    var $cond;
/*
 * SQL文から配列を作る
 * 
 * CSV出力用の配列を作成する。
 * $typeは、配列の最後に入れる文字列の変数
 * タイトル、日、開始時間、終了時間、設備カテゴリ、設備名、タイプ
 */
    function makeArray($con, $fname, $type) {
        $sel = select_from_table($con, $this->sql, $this->cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 配列に格納
        while ($row = pg_fetch_array($sel)){
            $this->list[] = array(
                "schd_title"        => $row["schd_title"],
                "schd_start_date"   => $row["schd_start_date"],
                "schd_start_time_v" => $row["schd_start_time_v"],
                "schd_dur_v"        => $row["schd_dur_v"],
                "fcalcate_name"     => $row["fclcate_name"],
                "facility_name"     => $row["facility_name"],
                "type"              => $type
            );
        }
    }
    
/*
 * date型の時間をvchar型に変換する
 * 
 * 院内行事の開始時間、終了時間がdate型のため、
 * vchar4桁（Hi）の形式に変換する。
 *  例）06:00 → 0600, 23:30 → 2330
 */
    function timeToVar(){
        foreach ($this->list as $key => $row) {
            if(isset($row["schd_start_time_v"])){
                $this->list[$key]["schd_start_time_v"] = date("Hi",strtotime($row["schd_start_time_v"]));
                $this->list[$key]["schd_dur_v"] = date("Hi",strtotime($row["schd_dur_v"]));
            }
        }
    }
}


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session === "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
}

// 権限のチェック
$checkauth = check_authority($session, 13, $fname);
if ($checkauth === "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
}

// データベースに接続
$con = connect2db($fname);

// 職員IDを取得する。
$sql = "SELECT empmst.emp_id FROM empmst";
$cond = " JOIN session ON empmst.emp_id = session.emp_id WHERE session.session_id = '$session'";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
// 職員ID取得ここまで

// 各列の名前
$fields=array(
    "schd_title",
    "schd_start_date",
    "schd_start_time_v",
    "schd_dur_v",
    "fcalcate_name",
    "facility_name",
    "type"
);

// 表示範囲の指定
if( $start_date!=="0" || $end_date!=="0"){
    $start_date=pg_escape_string($start_date);
    $end_date=pg_escape_string($end_date);
    if($start_date==="0"){
        $between = " < '{$end_date}'";
    }
    else if($end_date==="0"){
        $between = " >= '{$start_date}'";
    }
    else if($start_date === $end_date){
        $between = " = '{$start_date}'";
    }
    else{
        $between = " BETWEEN '{$start_date}' AND '{$end_date}'";
    }
}
else{
    $between="";
}

// CSVに出力する要素を格納する
$csv_items = array();

// 委員会・WGにチェックが入っている場合
if( $pjt_f === "on"){
    $pjtschd = new schdArray();
    $pjtschd->sql = get_project_schedule( $emp_id, $between );
    $pjtschd->makeArray($con, $fname,"委員会・WG");
    $csv_items=  array_merge_recursive($csv_items, $pjtschd->list);
}

// 院内行事にチェックが入っている場合
if( $time_f === "on"){
    $timegd = new schdArray();
    $timegd->sql = get_event_schedule( $between );
    $timegd->makeArray($con, $fname,"院内行事");
    $timegd->timeToVar();
    $csv_items = array_merge_recursive($csv_items, $timegd->list);
}

// スケジュールの配列を取得
$schd = new schdArray();
$schd->sql = get_schedule( $emp_id, $between);
$schd->makeArray($con, $fname,"スケジュール");
$csv_items=array_merge_recursive($schd->list,$csv_items);
// 出力するCSVを日付、開始時間、終了時間の降順でソート
foreach ($csv_items as $key => $row) {
    $tmp1[$key]  = $row['schd_start_date'];
    $tmp2[$key] = $row['schd_start_time_v'];
    $tmp3[$key] = $row['schd_dur_v'];
}
array_multisort($tmp1, SORT_ASC, $tmp2, SORT_ASC, $tmp3, SORT_ASC, $csv_items);

// 実際に出力する文字列
$csv = "タイトル,日付,開始時間,終了時間,設備,施設,機能\r\n";

$new_fields = array();
$i=0;
foreach($csv_items as $key => $csv_item){
    foreach ($fields as $item) {
        $value = str_replace('"', '""', $csv_items[$key][$item]);
        if( $item==="schd_start_time_v" || $item==="schd_dur_v" ){
            $value = preg_replace("/(\d{2})(\d{2})/", "$1:$2", $value);
        }
        if (preg_match('/[,"\s]/', $value)) {
            $value = '"' . $value . '"';
        }
        $new_fields[$key][$item] = $value;
    }
    $csv .= implode(',', $new_fields[$key]) . "\n";
}
$csv = mb_convert_encoding($csv, "Shift_JIS", "EUC-JP");

// データベース接続を閉じる
pg_close($con);
// CSVを出力
$file_name = "schedule_list.csv";

ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

/*
 *  院内行事のスケジュール取得するSQL文を作るだけ
 */
function get_event_schedule( $between ){
    if( $between !== ""){
        $between = " AND event_date".$between;
    }
// SQL作成
$sql = <<<__SQL_END__
SELECT
    event_name AS schd_title,
    event_date AS schd_start_date,
    CASE WHEN time_flg = 't' THEN event_time
        ELSE null END AS schd_start_time_v,
    CASE WHEN time_flg = 't' THEN event_dur
        ELSE null END AS schd_dur_v,
    null AS fcalcate_name,
    null AS facility_name
FROM timegd
WHERE TRUE
$between;
__SQL_END__;
    return $sql;
}

/*
 *  スケジュール取得するSQL文を作るだけ
 */
function get_schedule( $emp_id, $between ){
    $emp_id=pg_escape_string($emp_id);
    if( $between !== ""){
        $between = " AND schd_start_date".$between;
    }
// 時間指定ありスケジュール
$sql = <<<__SQL_END__
SELECT
    s.schd_title,
    s.schd_start_date,
    s.schd_start_time_v,
    s.schd_dur_v,
    r.fclcate_name,
    r.facility_name
FROM schdmst s
    LEFT JOIN (
        SELECT
            r.reg_time,
            c.fclcate_name,
            f.facility_name
        FROM reservation r
            LEFT JOIN fclcate c USING(fclcate_id)
            LEFT JOIN facility f ON f.facility_id = r.facility_id
        WHERE NOT c.fclcate_del_flg
        AND NOT f.facility_del_flg
    ) r USING (reg_time)
WHERE s.emp_id = '{$emp_id}'
{$between}
__SQL_END__;
// 時間指定無しスケジュール
$sql2 = <<<__SQL_END__
 UNION
SELECT
    s.schd_title,
    schd_start_date,
    null AS schd_start_time_v,
    null AS schd_dur_v,
    r.fclcate_name,
    r.facility_name
FROM schdmst2 s
    LEFT JOIN (
        SELECT
            r.reg_time,
            c.fclcate_name,
            f.facility_name
        FROM reservation2 r
            LEFT JOIN fclcate c USING(fclcate_id)
            LEFT JOIN facility f ON f.facility_id = r.facility_id
        WHERE NOT c.fclcate_del_flg
        AND NOT f.facility_del_flg
    ) r USING (reg_time)
WHERE s.emp_id = '{$emp_id}'
{$between}
ORDER BY schd_start_date, schd_start_time_v, schd_dur_v;
__SQL_END__;
    return $sql.$sql2;
}

// 委員会・WGのスケジュールのSQLを作るだけ
function get_project_schedule( $emp_id, $between ){
    $emp_id=pg_escape_string($emp_id);
    if( $between!=="" ){
        $between = " AND pjt_schd_start_date".$between;
    }
// 時間指定ありスケジュール
$sql = <<<__SQL_END__
SELECT
    s.pjt_schd_title AS schd_title,
    s.pjt_schd_start_date AS schd_start_date,
    s.pjt_schd_start_time_v AS schd_start_time_v,
    s.pjt_schd_dur_v AS schd_dur_v,
    r.fclcate_name,
    r.facility_name
FROM proschd s
    LEFT JOIN project p
        USING(pjt_id)
    LEFT JOIN (
        SELECT
            r.reg_time,
            c.fclcate_name,
            f.facility_name
        FROM reservation r
            LEFT JOIN fclcate c USING(fclcate_id)
            LEFT JOIN facility f ON f.facility_id = r.facility_id
        WHERE NOT c.fclcate_del_flg
        AND NOT f.facility_del_flg
    ) r USING (reg_time)
WHERE s.emp_id = '{$emp_id}'
AND NOT pjt_delete_flag
AND pjt_id IN (
    SELECT pjt_id
    FROM schdproject
    WHERE emp_id = '{$emp_id}'
    AND ownother = '1')
{$between}
__SQL_END__;
// 時間指定無しスケジュール
$sql2 = <<<__SQL_END__
 UNION
SELECT
    s.pjt_schd_title AS schd_title,
    s.pjt_schd_start_date AS schd_start_date,
    null AS schd_start_time_v,
    null AS schd_dur_v,
    r.fclcate_name,
    r.facility_name
FROM proschd2 s
    LEFT JOIN project p
        USING(pjt_id)
    LEFT JOIN (
        SELECT
            r.reg_time,
            c.fclcate_name,
            f.facility_name
        FROM reservation2 r
            LEFT JOIN fclcate c USING(fclcate_id)
            LEFT JOIN facility f ON f.facility_id = r.facility_id
        WHERE NOT c.fclcate_del_flg
        AND NOT f.facility_del_flg
    ) r USING (reg_time)
WHERE s.emp_id = '{$emp_id}'
AND NOT pjt_delete_flag
AND pjt_id IN (
    SELECT pjt_id
    FROM schdproject
    WHERE emp_id = '{$emp_id}'
    AND ownother = '1')
{$between}
ORDER BY schd_start_date, schd_start_time_v, schd_dur_v;
__SQL_END__;
    return $sql.$sql2;
}
?>
