<?php
require_once('jnl_indicator_display.php');
class IndicatorFacilityExcel extends IndicatorDisplay
{
    /**
     * コンストラクタ
     */
    function IndicatorFacilityExcel($arr)
    {
        parent::IndicatorDisplay($arr);
    }

    /**
     * No.74 [病院] 病院別月間報告書患者数総括 エクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelSummaryOfNumberOfMonthlyReportPatientsAccordingToHospital($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr = $this->getExcelMeta();

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();
        $fontTag = $this->getFontTag();

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_8  = array('name' => 'rowspan', 'value' =>  8);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_10 = array('name' => 'rowspan', 'value' => 10);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_14 = array('name' => 'rowspan', 'value' => 14);
        $rowspan_19 = array('name' => 'rowspan', 'value' => 19);
        $rowspan_24 = array('name' => 'rowspan', 'value' => 24);
        $rowspan_28 = array('name' => 'rowspan', 'value' => 28);
        $rowspan_65 = array('name' => 'rowspan', 'value' => 65);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_5  = array('name' => 'colspan', 'value' =>  5);

        # style
        $bgColor_blue            = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center        = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right         = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap       = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_parcent = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
        $msoNumberFormat_comma   = $this->getMsoNumberFormat('comma');
        $msoNumberFormat_unit    = $this->getMsoNumberFormat('unit');
        $msoNumberFormat_parcent1 = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0     = array('name' => 'mso-number-format', 'value' => '0.0');

        $facilityId   = $this->getArrValueByKey('facility_id', $dateArr);
        $targetYear   = $this->getArrValueByKey('year',        $dateArr);
        $displayMonth = $this->getDisplayMonth();
        $startMonth   = $this->getFiscalYearStartMonth();

        $leftSide = array();
        $mainData = array();

        $yearMonthArr[] = array('data' => '&nbsp;', 'attr' => array($colspan_5));
        for ($m=0; $m < 12; $m++) {
            $month = ($startMonth + $m < 13)? $startMonth + $m : $startMonth + $m - 12;
            $yearMonthArr[] = array('data' => $month.'月');
        }
        $yearMonthArr[] = array('data' => '合計');

        $rtnStr .= sprintf('<table border="1" ><tr><td>%s</td></tr></table>', sprintf($fontTag, $targetYear.'年度'));
        $rtnStr .= '<table class="list" width="100%" border="1" ><tr>';
        $rtnStr .= $this->convertCellData($yearMonthArr, $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center)).'</tr>';

        # 外来
        $leftSide[1] = array(0 => array('data' => '外来', 'attr' => array($rowspan_2), 'style' => array('textAlign' => $textAlign_center))
                           , 1 => array('data' => '外来合計', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[2] = array(0 => array('data' => '1日平均', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));

        # 入院
        $leftSide[3] = array(0 => array('data' => '入院', 'attr' => array($rowspan_65), 'style' => array('textAlign' => $textAlign_center))
                           , 1 => array('data' => '全病棟', 'attr' => array($rowspan_6), 'style' => array('textAlign' => $textAlign_center))
                           , 2 => array('data' => '入院合計', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[4] = array(0 => array('data' => '1日平均', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[5] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[6] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[7] = array(0 => array('data' => '稼働病床稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[8] = array(0 => array('data' => '許可病床稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 一般病棟
        $leftSide[9] = array(0 => array('data' => '一般病棟', 'attr' => array($rowspan_13), 'style' => array('textAlign' => $textAlign_center))
                           , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[10] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[11] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[12] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[13] = array(0 => array('data' => '平均在院日数(直近3ヶ月)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[14] = array(0 => array('data' => '新入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[15] = array(0 => array('data' => 'DPC対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[16] = array(0 => array('data' => '重症度・看護必要度割合', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[17] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[18] = array(0 => array('data' => '15日〜30日延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[19] = array(0 => array('data' => '救急医療管理加算算定延べ数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[20] = array(0 => array('data' => '90日超延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[21] = array(0 => array('data' => '上記内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 障害者病棟
        $leftSide[22] = array(0 => array('data' => '障害者病棟', 'attr' => array($rowspan_8), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[23] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[24] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[25] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[26] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[27] = array(0 => array('data' => '15日〜30日延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[28] = array(0 => array('data' => '90日超延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[29] = array(0 => array('data' => '上記の内特定入院対象延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 精神一般病棟
        $leftSide[30] = array(0 => array('data' => '精神一般病棟', 'attr' => array($rowspan_4), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[31] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[32] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[33] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 医療
        $leftSide[34] = array(0 => array('data' => '療養病棟', 'attr' => array($rowspan_28), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '医療', 'attr' => array($rowspan_14), 'style' => array('textAlign' => $textAlign_center))
                            , 2 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[35] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[36] = array(0 => array('data' => '1日平均', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[37] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[38] = array(0 => array('data' => '入院基本料', 'attr' => array($rowspan_9), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => 'A', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[39] = array(0 => array('data' => 'B', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[40] = array(0 => array('data' => 'C', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[41] = array(0 => array('data' => 'D', 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[42] = array(0 => array('data' => 'E', 'style' => array('t extAlign' => $textAlign_center)));
        $leftSide[43] = array(0 => array('data' => 'F', 'style' => array('t extAlign' => $textAlign_center)));
        $leftSide[44] = array(0 => array('data' => 'G', 'style' => array('t extAlign' => $textAlign_center)));
        $leftSide[45] = array(0 => array('data' => 'H', 'style' => array('t extAlign' => $textAlign_center)));
        $leftSide[46] = array(0 => array('data' => 'I', 'style' => array('t extAlign' => $textAlign_center)));
        $leftSide[47] = array(0 => array('data' => '1日〜14日延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病棟 介護
        $leftSide[48] = array(0 => array('data' => '介護', 'attr' => array($rowspan_10), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[49] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[50] = array(0 => array('data' => '1日平均', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[51] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[52] = array(0 => array('data' => '要介護5', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[53] = array(0 => array('data' => '要介護4', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[54] = array(0 => array('data' => '要介護3', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[55] = array(0 => array('data' => '要介護2', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[56] = array(0 => array('data' => '要介護1', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[57] = array(0 => array('data' => '平均介護度', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 療養病床 精神療養
        $leftSide[58] = array(0 => array('data' => '精神療養', 'attr' => array($rowspan_4), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[59] = array(0 => array('data' => '定床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[60] = array(0 => array('data' => '空床数', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[61] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 入院 特定病床
        $leftSide[62] = array(0 => array('data' => '特定病棟', 'attr' => array($rowspan_6), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '延べ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[63] = array(0 => array('data' => '定床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[64] = array(0 => array('data' => '空床数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[65] = array(0 => array('data' => '稼働率', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[66] = array(0 => array('data' => '回復期リハ', 'attr' => array($rowspan_2), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '重症患者の割合', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center))
        );
        $leftSide[67] = array(0 => array('data' => '在宅復帰率', 'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)));

        # 重点項目
        $leftSide[68] = array(0 => array('data' => '重点項目', 'attr' => array($rowspan_19), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(1),  'style' => array('textAlign' => $textAlign_center))
                            , 2 => array('data' => '新患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[69] = array(0 => array('data' => $indicatorData->sc(2),    'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '初診患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[70] = array(0 => array('data' => $indicatorData->sc(3),    'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '紹介患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[71] = array(0 => array('data' => $indicatorData->sc(4),      'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '救急依頼件数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[72] = array(0 => array('data' => $indicatorData->sc(5), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(4).'の内、救急受入れ患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[73] = array(0 => array('data' => $indicatorData->sc(6), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(5).'の内、救急入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[74] = array(0 => array('data' => $indicatorData->sc(7), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => '診療時間外患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[75] = array(0 => array('data' => $indicatorData->sc(8), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => $indicatorData->sc(7).'の内、時間外入院患者数', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)),
        );
        $leftSide[76] = array(0 => array('data' => '紹介率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[77] = array(0 => array('data' => '救急受入率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[78] = array(0 => array('data' => '救急入院率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[79] = array(0 => array('data' => '時間外入院率', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[80] = array(0 => array('data' => $indicatorData->sc(9), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => 'リハビリ平均提供単位数(一般)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[81] = array(0 => array('data' => $indicatorData->sc(10), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => 'リハビリ平均提供単位数(療養・精神)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[82] = array(0 => array('data' => $indicatorData->sc(11), 'style' => array('textAlign' => $textAlign_center))
                            , 1 => array('data' => 'リハビリ平均提供単位数(回復期リハ)', 'attr' => array($colspan_3), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[83] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[84] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[85] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));
        $leftSide[86] = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4), 'style' => array('textAlign' => $textAlign_center)));

        $rowCount = 86;

        $dailyData   = $this->getArrValueByKey('daily_data',   $dataArr);
        $monthlyData = $this->getArrValueByKey('monthly_data', $dataArr);

        $startRowNum     = 3;
        $startColumnNum  = 6;
        $totalColNum     = 18;

        $columnAlphaName = $this->columnAlphaNameArr;

        for ($m=0; $m < 12; $m++) {
            $rowNum = $startRowNum - 1;
            $colNum = $startColumnNum + $m;

            $month = ($startMonth + $m < 13)? $startMonth + $m : $startMonth + $m - 12;
            $year  = ($month < $startMonth)?  $targetYear + 1  : $targetYear;

			//表示月が現在月より過去の場合はMAXの日曜・祝日日数を設定する
			//表示月と現在月が同じ場合は現在日までの日曜・祝日日数を設定する
			$holi_cnt = $indicatorData->getHolidayCount($year,$month);
			
			
            $gokeiGa      = null;
            $dayCount     = null;
            $gokeiIp      = null;
            $gokeiShog    = null;
            $gokeiSeip    = null;
            $gokeiIr      = null;
            $gokeiKaig    = null;
            $gokeiKaif    = null;
            $gokeiAk      = null;
            $gokeiKan     = null;
            $gokeiIcu     = null;
            $gokeiShoni   = null;
            $gokeiSeir    = null;
            $gokeiTok     = null;
            $gokeiNin     = null;
            $actCount     = null;
            $pmtCount     = null;
            $ipAct        = null;
            $shogAct      = null;
            $seipAct      = null;
            $irAct        = null;
            $kaigAct      = null;
            $kaifAct      = null;
            $akAct        = null;
            $kanAct       = null;
            $icuAct       = null;
            $shoniAct     = null;
            $seirAct      = null;
            $tokAct       = null;
            $ninAct       = null;
            $sumGokei     = null;
            $gokeiTokutei = null;
            $tokuteiAct   = null;

            $dispNum = array();

            # このへんでデータ格納
            if (is_array($dailyData)) {
                foreach ($dailyData as $dailyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $dailyVal) && sprintf('%02d', $month) == $this->getArrValueByKey('month', $dailyVal)) {
                        $gokeiGa      = $this->getArrValueByKey('gokei_ga',    $dailyVal);
                        $dayCount     = $this->getArrValueByKey('day_count',   $dailyVal);
                        $gokeiIp      = $this->getArrValueByKey('gokei_ip',    $dailyVal);
                        $gokeiShog    = $this->getArrValueByKey('gokei_shog',  $dailyVal);
                        $gokeiSeip    = $this->getArrValueByKey('gokei_seip',  $dailyVal);
                        $gokeiIr      = $this->getArrValueByKey('gokei_ir',    $dailyVal);
                        $gokeiKaig    = $this->getArrValueByKey('gokei_kaig',  $dailyVal);
                        $gokeiKaif    = $this->getArrValueByKey('gokei_kaif',  $dailyVal);
                        $gokeiAk      = $this->getArrValueByKey('gokei_ak',    $dailyVal);
                        $gokeiKan     = $this->getArrValueByKey('gokei_kan',   $dailyVal);
                        $gokeiIcu     = $this->getArrValueByKey('gokei_icu',   $dailyVal);
                        $gokeiShoni   = $this->getArrValueByKey('gokei_shoni', $dailyVal);
                        $gokeiSeir    = $this->getArrValueByKey('gokei_seir',  $dailyVal);
                        $gokeiTok     = $this->getArrValueByKey('gokei_tok',   $dailyVal);
                        $gokeiNin     = $this->getArrValueByKey('gokei_nin',   $dailyVal);
                        $actCount     = $this->getArrValueByKey('act_count',   $dailyVal);
                        $pmtCount     = $this->getArrValueByKey('pmt_count',   $dailyVal);
                        $ipAct        = $this->getArrValueByKey('ip_act',      $dailyVal);
                        $shogAct      = $this->getArrValueByKey('shog_act',    $dailyVal);
                        $seipAct      = $this->getArrValueByKey('seip_act',    $dailyVal);
                        $irAct        = $this->getArrValueByKey('ir_act',      $dailyVal);
                        $kaigAct      = $this->getArrValueByKey('kaig_act',    $dailyVal);
                        $kaifAct      = $this->getArrValueByKey('kaif_act',    $dailyVal);
                        $akAct        = $this->getArrValueByKey('ak_act',      $dailyVal);
                        $kanAct       = $this->getArrValueByKey('kan_act',     $dailyVal);
                        $icuAct       = $this->getArrValueByKey('icu_act',     $dailyVal);
                        $shoniAct     = $this->getArrValueByKey('shoni_act',   $dailyVal);
                        $seirAct      = $this->getArrValueByKey('seir_act',    $dailyVal);
                        $tokAct       = $this->getArrValueByKey('tok_act',     $dailyVal);
                        $ninAct       = $this->getArrValueByKey('nin_act',     $dailyVal);
                        $sumGokei     = $gokeiIp + $gokeiShog + $gokeiSeip + $gokeiIr + $gokeiKaig + $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiSeir + $gokeiTok + $gokeiNin;
                        $gokeiTokutei = $gokeiKaif + $gokeiAk + $gokeiKan + $gokeiIcu + $gokeiShoni + $gokeiTok + $gokeiNin;
                        $tokuteiAct   = $kaifAct + $akAct + $kanAct + $icuAct + $shoniAct + $tokAct + $ninAct;
                    }
                }
            }

            if (is_array($monthlyData)) {
                foreach ($monthlyData as $monthlyVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $monthlyVal) && $month == $this->getArrValueByKey('month', $monthlyVal)) {
                        for ($i=297; $i <= 341; $i++) { $dispNum[$i] = $monthlyVal['dispnum_'.$i]; }
                    }
                }
            }

            # 外来
            $mainData[1][$m] = array('data' => $gokeiGa);
            //$mainData[2][$m] = array('data' => sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%d, %d)))', $this->coord($columnAlphaName[$colNum], $rowNum+1), $dayCount, $this->coord($columnAlphaName[$colNum], $rowNum+1), $dayCount, 1), 'day_count' => $dayCount);
			$mainData[2][$m] = array('data' => sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%d, %d)))', $this->coord($columnAlphaName[$colNum], $rowNum+1), $dayCount, $this->coord($columnAlphaName[$colNum], $rowNum+1), ($dayCount - $holi_cnt), 1), 'day_count' => $dayCount);

            # 入院 全病棟
            $mainData[3][$m] = array('data' => $sumGokei);
            $mainData[4][$m] = array('data' => sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%d, %d)))', $this->coord($columnAlphaName[$colNum], $rowNum+3), $dayCount, $this->coord($columnAlphaName[$colNum], $rowNum+3), $dayCount, 1), 'day_count' => $dayCount);
            $mainData[5][$m] = array('data' => $actCount);
            $mainData[6][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+5), $this->coord($columnAlphaName[$colNum], $rowNum+3), 1));
            $mainData[7][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+3), $this->coord($columnAlphaName[$colNum], $rowNum+5), 3));
            $mainData[8][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+3), $pmtCount, 3), 'pmt_count' => $pmtCount);

            # 入院 一般病棟
            $mainData[9][$m]  = array('data' => $gokeiIp);
            $mainData[10][$m] = array('data' => $ipAct);
            $mainData[11][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+10), $this->coord($columnAlphaName[$colNum], $rowNum+9), 1));
            $mainData[12][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+9), $this->coord($columnAlphaName[$colNum], $rowNum+10), 3));

			$mainData[13][$m] = array('data' => $dispNum['297'], 'disp_type' => 'float');
			
			$mainData[14][$m] = array('data' => $dispNum['298']);
            $mainData[15][$m] = array('data' => $dispNum['299']);

			//エクセルでパーセンテージ表示するので数値を１００分の１しておかないと桁が合わない
//			$mainData[16][$m] = array('data' => $dispNum['300'], 'disp_type' => 'float');
//			$mainData[16][$m] = array('data' => $dispNum['300']);
			$mainData[16][$m] = array('data' => $this->getExcelDivData(($dispNum['300'] /100), 1));
			
			$mainData[17][$m] = array('data' => $dispNum['301']);
            $mainData[18][$m] = array('data' => $dispNum['302']);
            $mainData[19][$m] = array('data' => $dispNum['303']);
            $mainData[20][$m] = array('data' => $dispNum['304']);
            $mainData[21][$m] = array('data' => $dispNum['305']);

            # 入院 障害者病棟
            $mainData[22][$m] = array('data' => $gokeiShog);
            $mainData[23][$m] = array('data' => $shogAct);
            $mainData[24][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+23), $this->coord($columnAlphaName[$colNum], $rowNum+22), 1));
            $mainData[25][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+22), $this->coord($columnAlphaName[$colNum], $rowNum+23), 3));
            $mainData[26][$m] = array('data' => $dispNum['306']);
            $mainData[27][$m] = array('data' => $dispNum['307']);
            $mainData[28][$m] = array('data' => $dispNum['308']);
            $mainData[29][$m] = array('data' => $dispNum['309']);

            # 入院 精神一般
            $mainData[30][$m] = array('data' => $gokeiSeip);
            $mainData[31][$m] = array('data' => $seipAct);
            $mainData[32][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+31), $this->coord($columnAlphaName[$colNum], $rowNum+30), 1));
            $mainData[33][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+30), $this->coord($columnAlphaName[$colNum], $rowNum+31), 3));

            # 入院 療養病棟 医療適用
            $mainData[34][$m] = array('data' => $gokeiIr);
            $mainData[35][$m] = array('data' => $irAct);
            $mainData[36][$m] = array('data' =>sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%d, %d)))', $this->coord($columnAlphaName[$colNum], $rowNum+34), $dayCount, $this->coord($columnAlphaName[$colNum], $rowNum+34), $dayCount, 1), 'day_count' => $dayCount);
            $mainData[37][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+34), $this->coord($columnAlphaName[$colNum], $rowNum+35), 3));
            $mainData[38][$m] = array('data' => $dispNum['312']);
            $mainData[39][$m] = array('data' => $dispNum['313']);
            $mainData[40][$m] = array('data' => $dispNum['314']);
            $mainData[41][$m] = array('data' => $dispNum['315']);
            $mainData[42][$m] = array('data' => $dispNum['316']);
            $mainData[43][$m] = array('data' => $dispNum['317']);
            $mainData[44][$m] = array('data' => $dispNum['318']);
            $mainData[45][$m] = array('data' => $dispNum['319']);
            $mainData[46][$m] = array('data' => $dispNum['320']);
            $mainData[47][$m] = array('data' => $dispNum['321']);

            # 入院 療養病棟 介護
            $mainData[48][$m] = array('data' => $gokeiKaig);
            $mainData[49][$m] = array('data' => $kaigAct);
            $mainData[50][$m] = array('data' => empty($dayCount)? '' : sprintf('=ROUND(%s/%s, %d)', $this->coord($columnAlphaName[$colNum], $rowNum+48), $dayCount, 1), 'day_count' => $dayCount);
            $mainData[51][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+48), $this->coord($columnAlphaName[$colNum], $rowNum+49), 3));
            $mainData[52][$m] = array('data' => $dispNum['322']);
            $mainData[53][$m] = array('data' => $dispNum['323']);
            $mainData[54][$m] = array('data' => $dispNum['324']);
            $mainData[55][$m] = array('data' => $dispNum['325']);
            $mainData[56][$m] = array('data' => $dispNum['326']);
            $tmpRowData57 = $dispNum['322']+$dispNum['323']+$dispNum['324']+$dispNum['325']+$dispNum['326'];
            $mainData[57][$m] = array(
                'data' => sprintf(
                    '=IF((%s+%s+%s+%s+%s)=0, "", ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1)/(%s+%s+%s+%s+%s)), %d))',
                    $this->coord($columnAlphaName[$colNum], $rowNum+52), $this->coord($columnAlphaName[$colNum], $rowNum+53), $this->coord($columnAlphaName[$colNum], $rowNum+54), $this->coord($columnAlphaName[$colNum], $rowNum+55), $this->coord($columnAlphaName[$colNum], $rowNum+56),
                    $this->coord($columnAlphaName[$colNum], $rowNum+52), $this->coord($columnAlphaName[$colNum], $rowNum+53), $this->coord($columnAlphaName[$colNum], $rowNum+54), $this->coord($columnAlphaName[$colNum], $rowNum+55), $this->coord($columnAlphaName[$colNum], $rowNum+56),
                    $this->coord($columnAlphaName[$colNum], $rowNum+52), $this->coord($columnAlphaName[$colNum], $rowNum+53), $this->coord($columnAlphaName[$colNum], $rowNum+54), $this->coord($columnAlphaName[$colNum], $rowNum+55), $this->coord($columnAlphaName[$colNum], $rowNum+56),
                    1
                ),
            );

            # 入院 療養病棟 精神療養
            $mainData[58][$m] = array('data' => $gokeiSeir);
            $mainData[59][$m] = array('data' => $seirAct);
            $mainData[60][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+59), $this->coord($columnAlphaName[$colNum], $rowNum+58), 1));
            $mainData[61][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+58), $this->coord($columnAlphaName[$colNum], $rowNum+59), 3));

            # 入院 特定病棟
            $mainData[62][$m] = array('data' => $gokeiTokutei);
            $mainData[63][$m] = array('data' => $tokuteiAct);
            $mainData[64][$m] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$colNum], $rowNum+63), $this->coord($columnAlphaName[$colNum], $rowNum+62), 1));
            $mainData[65][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+62), $this->coord($columnAlphaName[$colNum], $rowNum+63), 3));

            # 重点項目
			$mainData[66][$m] = array('data' => $dispNum['310'], 'disp_type' => 'float');
			$mainData[67][$m] = array('data' => $dispNum['311'], 'disp_type' => 'float');
            $mainData[68][$m] = array('data' => $dispNum['327']);
            $mainData[69][$m] = array('data' => $dispNum['328']);
            $mainData[70][$m] = array('data' => $dispNum['329']);
            $mainData[71][$m] = array('data' => $dispNum['330']);
            $mainData[72][$m] = array('data' => $dispNum['331']);
            $mainData[73][$m] = array('data' => $dispNum['332']);
            $mainData[74][$m] = array('data' => $dispNum['333']);
            $mainData[75][$m] = array('data' => $dispNum['334']);
            $mainData[76][$m] = array('data' => $this->getExcelDivData(sprintf('IF(%s+%s=0, "", (%s+%s))', $this->coord($columnAlphaName[$colNum], $rowNum+70), $this->coord($columnAlphaName[$colNum], $rowNum+72), $this->coord($columnAlphaName[$colNum], $rowNum+70), $this->coord($columnAlphaName[$colNum], $rowNum+72)), $this->coord($columnAlphaName[$colNum], $rowNum+69), 3));
            $mainData[77][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+72), $this->coord($columnAlphaName[$colNum], $rowNum+71), 3));
            $mainData[78][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+73), $this->coord($columnAlphaName[$colNum], $rowNum+72), 3));
            $mainData[79][$m] = array('data' => $this->getExcelDivData($this->coord($columnAlphaName[$colNum], $rowNum+75), $this->coord($columnAlphaName[$colNum], $rowNum+74), 3));
			$mainData[80][$m] = array('data' => $dispNum['335'], 'disp_type' => 'exception');
			$mainData[81][$m] = array('data' => $dispNum['336'], 'disp_type' => 'exception');
			$mainData[82][$m] = array('data' => $dispNum['337'], 'disp_type' => 'exception');
            $mainData[83][$m] = array('data' => $dispNum['338']);
            $mainData[84][$m] = array('data' => $dispNum['339']);
            $mainData[85][$m] = array('data' => $dispNum['340']);
            $mainData[86][$m] = array('data' => $dispNum['341']);

            for ($t=1; $t <= $rowCount; $t++) {
                $totalData[$t][0]['status']  = $this->checkCellDataStatus('comp', array($totalData[$t][0]['status'], $mainData[$t][$m]['status']));
                switch ($t) {
                    case '2': # 外来 1日平均
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
//                        $totalData[$t][0]['data'] = sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%s, 1)))', $this->coord($columnAlphaName[$totalColNum], $rowNum+1), $totalData[$t][0]['day_count'], $this->coord($columnAlphaName[$totalColNum], $rowNum+1), $totalData[$t][0]['day_count'], 1);
						
						//$totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColNum-1], $rowNum+$t);

						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+2,$columnAlphaName[$totalColNum-1],$rowNum+2,
								count($monthlyData),
								1
								);
						
						break;

                    case '4': # 入院 全病棟 1日平均
//                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
//                        $totalData[$t][0]['data'] = sprintf('=IF(%s="", "", IF(%d=0, "", ROUND(%s/%s, 1)))', $this->coord($columnAlphaName[$totalColNum], $rowNum+3), $totalData[$t][0]['day_count'], $this->coord($columnAlphaName[$totalColNum], $rowNum+3), $totalData[$t][0]['day_count'], 1);
						//$totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColNum-1], $rowNum+$t);

						//平均値の合計数を月数で割った平均値を表示するように変更します。2011/05/16
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+4,$columnAlphaName[$totalColNum-1],$rowNum+4,
								count($monthlyData),
								1
								);
						
						break;

                    case '6': # 入院 全病棟 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+5), $this->coord($columnAlphaName[$totalColNum], $rowNum+3), 1);
                        break;

                    case '7': # 入院 全病棟 稼働病床稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+3), $this->coord($columnAlphaName[$totalColNum], $rowNum+5), 3);
                        break;

                    case '8': # 入院 全病棟 許可病床稼働率
                        $totalData[$t][0]['pmt_count'] = $totalData[$t][0]['pmt_count'] + $mainData[$t][$m]['pmt_count'];
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+3), $totalData[$t][0]['pmt_count'], 3);
                        break;

                    case '11': # 入院 一般病棟 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+10), $this->coord($columnAlphaName[$totalColNum], $rowNum+9), 1);
                        break;

                    case '12': # 入院 一般病棟 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+9), $this->coord($columnAlphaName[$totalColNum], $rowNum+10), 3);
                        break;


					case '13': # 入院 一般病棟 平均在院日数（直近3カ月）
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+13,$columnAlphaName[$totalColNum-1],$rowNum+13,
								count($monthlyData),
								1
								);
						break;
					

					case '16': # 入院 一般病棟 「重症度、看護必要度割合」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+16,$columnAlphaName[$totalColNum-1],$rowNum+16,
								count($monthlyData),
								3
								);
						break;
					

                    case '24': # 入院 障害者病棟 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+23), $this->coord($columnAlphaName[$totalColNum], $rowNum+22), 1);
                        break;

                    case '25': # 入院 障害者病棟 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+22), $this->coord($columnAlphaName[$totalColNum], $rowNum+23), 3);
                        break;

                    case '32': # 入院 精神一般病棟 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+31), $this->coord($columnAlphaName[$totalColNum], $rowNum+30), 1);
                        break;

                    case '33': # 入院 精神一般病棟 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+30), $this->coord($columnAlphaName[$totalColNum], $rowNum+31), 3);
                        break;

                    case '36': # 入院 療養病棟 医療 1日平均
                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
                        //$totalData[$t][0]['data'] = sprintf('=IF(%d=0, "", ROUND(%s/%s, 1))', $totalData[$t][0]['day_count'], $this->coord($columnAlphaName[$totalColNum], $rowNum+34), $totalData[$t][0]['day_count'], 1);

						//$totalData[$t][0]['data'] = sprintf('=ROUND((ROUND(%s/%s, 1)/%s), %d)',$this->coord($columnAlphaName[$totalColNum], $rowNum+34), $totalData[$t][0]['day_count'],count($monthlyData),1);
						
						//平均値の合計数を月数で割った平均値を表示するように変更します。2011/05/16
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+36,$columnAlphaName[$totalColNum-1],$rowNum+36,
								count($monthlyData),
								1
								);
						

                        break;

                    case '37': # 入院 療養病棟 医療 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+34), $this->coord($columnAlphaName[$totalColNum], $rowNum+35), 3);
                        break;

                    case '50': # 入院 療養病棟 介護適用 1日平均
                        $totalData[$t][0]['day_count'] = $totalData[$t][0]['day_count'] + $mainData[$t][$m]['day_count'];
                        //$totalData[$t][0]['data'] = sprintf('=IF(%d=0, "", ROUND(%s/%s, 1))', $totalData[$t][0]['day_count'], $this->coord($columnAlphaName[$totalColNum], $rowNum+48), $totalData[$t][0]['day_count'], 1);

						//$totalData[$t][0]['data'] = sprintf('=ROUND((ROUND(%s/%s, 1)/%s), %d)',$this->coord($columnAlphaName[$totalColNum], $rowNum+48), $totalData[$t][0]['day_count'],count($monthlyData),1);


						//平均値の合計数を月数で割った平均値を表示するように変更します。2011/05/16
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+50,$columnAlphaName[$totalColNum-1],$rowNum+50,
								count($monthlyData),
								1
								);
						
						break;

                    case '51': # 入院 療養病棟 介護適用 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+48), $this->coord($columnAlphaName[$totalColNum], $rowNum+49), 3);
                        break;

                    case '57': # 入院 療養病棟 介護適用 平均介護度

/*
                        $totalData[$t][0]['data'] = empty($tmpRowData57)? '' : sprintf(
                            '=ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1)/(%s+%s+%s+%s+%s)), %d)',
                            $this->coord($columnAlphaName[$totalColNum], $rowNum+52), $this->coord($columnAlphaName[$totalColNum], $rowNum+53), $this->coord($columnAlphaName[$totalColNum], $rowNum+54), $this->coord($columnAlphaName[$totalColNum], $rowNum+55), $this->coord($columnAlphaName[$totalColNum], $rowNum+56),
                            $this->coord($columnAlphaName[$totalColNum], $rowNum+52), $this->coord($columnAlphaName[$totalColNum], $rowNum+53), $this->coord($columnAlphaName[$totalColNum], $rowNum+54), $this->coord($columnAlphaName[$totalColNum], $rowNum+55), $this->coord($columnAlphaName[$totalColNum], $rowNum+56),
                            1
                        );
						
*/
						if(!empty($tmpRowData57))
						{
							$totalData[$t][0]['data'] = sprintf(
									'=ROUND(((%s*5+%s*4+%s*3+%s*2+%s*1)/(%s+%s+%s+%s+%s)), %d)',
									$this->coord($columnAlphaName[$totalColNum], $rowNum+52), $this->coord($columnAlphaName[$totalColNum], $rowNum+53), $this->coord($columnAlphaName[$totalColNum], $rowNum+54), $this->coord($columnAlphaName[$totalColNum], $rowNum+55), $this->coord($columnAlphaName[$totalColNum], $rowNum+56),
									$this->coord($columnAlphaName[$totalColNum], $rowNum+52), $this->coord($columnAlphaName[$totalColNum], $rowNum+53), $this->coord($columnAlphaName[$totalColNum], $rowNum+54), $this->coord($columnAlphaName[$totalColNum], $rowNum+55), $this->coord($columnAlphaName[$totalColNum], $rowNum+56),
									1
									);
						}

                        break;

                    case '60': # 入院 療養病棟 精神療養 空床数
                        $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+59), $this->coord($columnAlphaName[$totalColNum], $rowNum+58), 1);
                        break;

                    case '61': # 入院 療養病棟 精神療養 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+58), $this->coord($columnAlphaName[$totalColNum], $rowNum+59), 3);
                        break;

                    case '64': # 入院 特定病棟 空床数
                          $totalData[$t][0]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$totalColNum], $rowNum+63), $this->coord($columnAlphaName[$totalColNum], $rowNum+62), 1);
                        break;

                    case '65': # 入院 特定病棟 稼働率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+62), $this->coord($columnAlphaName[$totalColNum], $rowNum+63), 3);
                        break;

					case '66': # 入院 特定病棟 「重症患者の割合」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+66,$columnAlphaName[$totalColNum-1],$rowNum+66,
								count($monthlyData),
								3
								);
						break;
					
					case '67': # 入院 特定病棟 「在宅復帰率」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+67,$columnAlphaName[$totalColNum-1],$rowNum+67,
								count($monthlyData),
								3
								);
						break;

                    case '76': # その他 紹介率
                        $totalData[$t][0]['data'] = $this->getExcelDivData(sprintf('(%s+%s)', $this->coord($columnAlphaName[$totalColNum], $rowNum+70), $this->coord($columnAlphaName[$totalColNum], $rowNum+72)), $this->coord($columnAlphaName[$totalColNum], $rowNum+69), 3);
                        break;

                    case '77': # その他 救急受入率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+72), $this->coord($columnAlphaName[$totalColNum], $rowNum+71), 3);
                        break;

                    case '78': # その他 救急入院率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+73), $this->coord($columnAlphaName[$totalColNum], $rowNum+72), 3);
                        break;

                    case '79': # その他 時間外入院率
                        $totalData[$t][0]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$totalColNum], $rowNum+75), $this->coord($columnAlphaName[$totalColNum], $rowNum+74), 3);
                        break;

					case '80': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（一般）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+80,$columnAlphaName[$totalColNum-1],$rowNum+80,
								count($monthlyData),
								3
								);
						break;
					
					case '81': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（療養、精神）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+81,$columnAlphaName[$totalColNum-1],$rowNum+81,
								count($monthlyData),
								3
								);
						break;

					case '82': # その他　「ﾘﾊﾋﾞﾘ平均提供単位数（回復期リハ）」
						$totalData[$t][0]['data'] = sprintf(
								'=ROUND((SUM(%s%s:%s%s)/%s), %d)',$columnAlphaName[$startColumnNum], $rowNum+82,$columnAlphaName[$totalColNum-1],$rowNum+82,
								count($monthlyData),
								3
								);
						break;
					
                    default:
                        $totalData[$t][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColNum-1], $rowNum+$t);
                        break;
                }
            }
        }


        for ($i=1; $i <= $rowCount; $i++) {
            switch ($i) {
                default:
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                              .'</tr>';
                    break;

				case '2': case '4': case '13': case '36': case '50': case '57': case '66': case '67':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0))
                              .'</tr>';
                    break;

				case '7': case '8': case '12':  case '16': case '25': case '33': case '37': case '51': case '61': case '65': case '76': case '77': case '78': case '79':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_parcent))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_parcent))
                              .'</tr>';
                    break;
				
				case '80': case '81': case '82':
                    $rtnStr .= '<tr>'
                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_unit))
                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_unit))
                              .'</tr>';
//                    $rtnStr .= '<tr>'
//                                  .$this->convertCellData($leftSide[$i],  $fontTag, array('bgColor'   => $bgColor_blue, 'whiteSpace' => $whiteSpace_nowrap))
//                                  .$this->convertCellData($mainData[$i],  $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '#0.0\0022単位\0022')))
//                                  .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '#0.0\0022単位\0022')))
//                              .'</tr>';
                    break;
            }
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.75 [病院] 病院月間報告書(行為件数)のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelHospitalMonthlyReport($dataArr, $plantArr, $arr, $dateArr)
    {
        $rtnStr     = $this->getExcelMeta();
        $fontTag    = $this->getFontTag();
        $cYear      = $this->getArrValueByKey('year', $dateArr);
        $startMonth = $this->getFiscalYearStartMonth();

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_9  = array('name' => 'rowspan', 'value' =>  9);
        $rowspan_17 = array('name' => 'rowspan', 'value' => 17);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);

        # style
        $bgColor_weekday     = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday    = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday     = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $bgColor_blue        = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');
        $msoNumberFormat_0_0 = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_0_0 = array('name' => 'mso-number-format', 'value' => '0.0');
        $writingMode = array('name' => 'writing-mode', 'value' => 'tb-rl');

        $rtnStr .= sprintf('<table border="1" ><tr><td>%04d年度</td></tr></table>', $cYear);
        $rtnStr .= '<table class="list" width="100%" border="1" >';

        $leftSide  = array();
        $mainData  = array();
        $totalData = array();
        $plantNameArr = array();
        $plantNameAnderDisplay = array();

//        $leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($rowspan_2, $colspan_2)));
		$leftSide[0]  = array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)));
		$leftSide[2]  = array(0 => array('data' => '手術件数', 'attr' => array($rowspan_5))
                            , 1 => array('data' => '全身麻酔による手術')
        );
        $leftSide[3]  = array(0 => array('data' => '硬膜外・脊椎麻酔による手術'));
        $leftSide[4]  = array(0 => array('data' => '上・下肢伝達麻酔による手術'));
        $leftSide[5]  = array(0 => array('data' => '内視鏡的手術'));
        $leftSide[6]  = array(0 => array('data' => '白内障手術'));
        $leftSide[7]  = array(0 => array('data' => '内視鏡', 'attr' => array($rowspan_3))
                            , 1 => array('data' => '食道・胃・十二指腸ファイバー')
        );
        $leftSide[8]  = array(0 => array('data' => '大腸・直腸ファイバー'));
        $leftSide[9]  = array(0 => array('data' => 'その他'));
        $leftSide[10] = array(0 => array('data' => '検査', 'attr' => array($rowspan_9))
                            , 1 => array('data' => '心電図')
        );
        $leftSide[11] = array(0 => array('data' => 'ホルター型心電図'));
        $leftSide[12] = array(0 => array('data' => 'トレッドミル'));
        $leftSide[13] = array(0 => array('data' => '心臓超音波'));
        $leftSide[14] = array(0 => array('data' => '胸腹部超音波'));
        $leftSide[15] = array(0 => array('data' => 'その他超音波(頭頸部・四肢等)'));
        $leftSide[16] = array(0 => array('data' => '肺機能'));
        $leftSide[17] = array(0 => array('data' => '骨塩定量'));
        $leftSide[18] = array(0 => array('data' => '血液ガス分析'));
        $leftSide[19] = array(0 => array('data' => '放<br>射<br>線', 'attr' => array($rowspan_17))
                            , 1 => array('data' => '一般撮影')
        );
        $leftSide[20] = array(0 => array('data' => 'マンモグラフィー'));
        $leftSide[21] = array(0 => array('data' => 'MDL(UGI)'));
        $leftSide[22] = array(0 => array('data' => 'BEM'));
        $leftSide[23] = array(0 => array('data' => 'DIP'));
        $leftSide[24] = array(0 => array('data' => 'DIC'));
        $leftSide[25] = array(0 => array('data' => '特殊'));
        $leftSide[26] = array(0 => array('data' => 'その他'));
        $leftSide[27] = array(0 => array('data' => 'CT(BRAIN)'));
        $leftSide[28] = array(0 => array('data' => 'CT(BODY)'));
        $leftSide[29] = array(0 => array('data' => 'CT(その他)'));
        $leftSide[30] = array(0 => array('data' => 'CT造影剤加算'));
        $leftSide[31] = array(0 => array('data' => 'MRI(BRAIN)'));
        $leftSide[32] = array(0 => array('data' => 'MRI(BODY)'));
        $leftSide[33] = array(0 => array('data' => 'MRI(その他)'));
        $leftSide[34] = array(0 => array('data' => 'MRI造影剤加算'));
        $leftSide[35] = array(0 => array('data' => 'Angio'));
        $leftSide[36] = array(0 => array('data' => "重点項目", 'attr' => array($rowspan_17), 'style' => array($writingMode))
                            , 1 => array('data' => '超重症児(者)入院診療加算(イ・ロ)')
        );
        $leftSide[37] = array(0 => array('data' => '栄養サポートチーム加算'));
        $leftSide[38] = array(0 => array('data' => '呼吸ケアチーム加算'));
        $leftSide[39] = array(0 => array('data' => '医療安全対策加算(1・2)'));
        $leftSide[40] = array(0 => array('data' => '感染防止対策加算'));
        $leftSide[41] = array(0 => array('data' => '医療機器安全管理料(1・2)'));
        $leftSide[42] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_1').')'));
        $leftSide[43] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_2').')'));
        $leftSide[44] = array(0 => array('data' => '薬剤管理指導料('.$indicatorData->sc('roman_3').')'));
        $leftSide[45] = array(0 => array('data' => '医薬品安全性情報等管理体制加算'));
        $leftSide[46] = array(0 => array('data' => '&nbsp;'));
        $leftSide[47] = array(0 => array('data' => '&nbsp;'));
        $leftSide[48] = array(0 => array('data' => '&nbsp;'));
        $leftSide[49] = array(0 => array('data' => '&nbsp;'));
        $leftSide[50] = array(0 => array('data' => '&nbsp;'));
        $leftSide[51] = array(0 => array('data' => '&nbsp;'));
        $leftSide[52] = array(0 => array('data' => '&nbsp;'));

        $rowCnt = count($leftSide);

//		$startRowNum = 4;
		$startRowNum = 3;
        $startColNum = 3;
        $totalColNum = $startColNum + 12;

        $colAlphaName = $this->columnAlphaNameArr;

        for ($m=0; $m < 12; $m++) {
            $year = ($startMonth+$m>12)? $cYear +1 : $cYear;
            $month = ($startMonth+$m>12)? $startMonth+$m-12 : $startMonth+$m;

            $plantNameArr[$m] = array('data' => sprintf('%d月', $month), 'style' => array('bgColor' => $bgColor_blue));
//            $plantNameAnderDisplay[$m] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

            $dispNum = array();
            for ($i=342; $i <= 392; $i++) { $dispNum[$i] = null; }

            foreach ($dataArr as $dataKey => $dataVal) {
                if ($year == $dataVal['year'] && sprintf('%02d', $month) == $dataVal['month']) {
                    for ($i=342; $i <= 392; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                }
            }

            $startNum = 2;
            for ($i=342; $i <= 392; $i++) {
                $mainData[$startNum][$m] = array('data' => $dispNum[$i]);
                $startNum++;
            }

            for ($i = 2; $i <= $rowCnt; $i++) {
                $rowNum = $startRowNum - 2 + $i;
                $totalData[$i][0] = array('data' => $this->getExcelSideSumData($colAlphaName[$startColNum], $colAlphaName[$totalColNum-1], $rowNum));
            }
        }

        $plantNameArr[] = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue));
//        $plantNameAnderDisplay[] = array('data' => '医療', 'style' => array('bgColor' => $bgColor_blue));

        $mainData[0] = $plantNameArr;
        $mainData[1] = $plantNameAnderDisplay;

        $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSide[0], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($mainData[0], $fontTag, array('textAlign' => $textAlign_center))
                  .'</tr>';
//                  .'<tr>'
//                      .$this->convertCellData($mainData[1], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_0_0))
//                  .'</tr>';

        for ($i = 2; $i <= $rowCnt; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($leftSide[$i], $fontTag, array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue))
                          .$this->convertCellData($mainData[$i], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_0_0))
                          .$this->convertCellData($totalData[$i], $fontTag, array('textAlign' => $textAlign_center, 'msoNumberFormat' => $msoNumberFormat_0_0))
                      .'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.76 [病院] 外来診療行為別明細のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $dateArr
     * @return string $rtnStr
     */
    function outputExcelOutpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_header    = array('name' => 'background-color', 'value' => '#ffc8e9');

        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');

        $topHeaderArr = array(
             0 => array('data' => '診療料'),  1 => array('data' => '件数'),    2 => array('data' => '日数'),      3 => array('data' => '初診料')
           , 4 => array('data' => '再診料'),  5 => array('data' => '指導料'),  6 => array('data' => '在宅医療'),  7 => array('data' => '院内処方')
           , 8 => array('data' => '投薬料'),  9 => array('data' => '注射料'), 10 => array('data' => '処置料'),   11 => array('data' => '手術・麻酔科')
          , 12 => array('data' => '検査料'), 13 => array('data' => 'X線科'),  14 => array('data' => 'リハビリ'), 15 => array('data' => '処方箋料')
          , 16 => array('data' => 'その他'), 17 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '内科')),        1 => array(0 => array('data' => '神経内科')),  2 => array(0 => array('data' => '呼吸器科'))
           , 3 => array(0 => array('data' => '消化器科')),    4 => array(0 => array('data' => '循環器科')),  5 => array(0 => array('data' => '小児科'))
           , 6 => array(0 => array('data' => '外科')),        7 => array(0 => array('data' => '整形外科')),  8 => array(0 => array('data' => '形成(美)外科'))
           , 9 => array(0 => array('data' => '脳神経外科')), 10 => array(0 => array('data' => '皮膚科')),   11 => array(0 => array('data' => '泌尿器科'))
          , 12 => array(0 => array('data' => '産婦人科')),   13 => array(0 => array('data' => '眼科')),     14 => array(0 => array('data' => '耳鼻咽喉科'))
          , 15 => array(0 => array('data' => '透析科')),     16 => array(0 => array('data' => '精神科')),   17 => array(0 => array('data' => '歯科'))
          , 18 => array(0 => array('data' => '放射線科')),   19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '総合計', 'style' => array($textAlign_center)))
        );

        $cellNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  4 => 'e',  5 => 'f',  6 => 'g',  7 => 'h',  8 => 'i',  9 => 'j'
          , 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p', 16 => 'q', 17 => 'r', 18 => 's', 19 => 't'
        );

        $dispData = array();
        for ($r=0; $r<count($leftSideHeaderArr); $r++) {
            for ($c=0; $c<=15; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr     = array();
        $totalRowDataArr = array();
        $totalColDataArr = array();

        # エクセル座標計算用変数
        $startRowNum = 3;
        $startColNum = 2;
        $endColNum   = $startColNum + 15;
        $colAlphaName = $this->columnAlphaNameArr;

        for ($r=0; $r<=count($leftSideHeaderArr); $r++) {
            $rowNum = $startRowNum + $r;

            for ($c=0; $c<=15; $c++) {
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_account'];
                        break;

                    case '1':
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_days'];
                        break;

                    default:
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_'.($c-1)];
                        break;
                }
            }
            # 診療科合計
            $totalColDataArr[$r][0]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum + 2], $colAlphaName[$endColNum], $rowNum);
        }

        # 総合計行
        for ($c=0; $c<=15; $c++) {
            $totalRowDataArr[$c]['data'] = $this->getExcelLengthSumData($colAlphaName[$startColNum + $c], $startRowNum, count($leftSideHeaderArr)+1);
        }

        $rtnStr .= '<table width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => $bgColor_header)).'</tr>';

        # メインデータ行
        for ($i=0; $i<=19; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .$this->convertCellData($totalColDataArr[20],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.77 [病院] 外来日当点のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputExcelOutpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center      = array('name' => 'text-align',        'value' => 'center');
        $textAlign_left        = array('name' => 'text-align',        'value' => 'left');
        $textAlign_right       = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap     = array('name' => 'white-space',       'value' => 'nowrap');
        $bgColor_header        = array('name' => 'background-color',  'value' => '#ffc8e9');
        $msoNumberFormat_int   = $this->getMsoNumberFormat('comma');
        $msoNumberFormat_float = $this->getMsoNumberFormat('decimal');
//        $msoNumberFormat_int   = array('name' => 'mso-number-format', 'value' => '0');
//        $msoNumberFormat_float = array('name' => 'mso-number-format', 'value' => '0.0');

        $topHeaderArr = array(
             0 => array('data' => '診療料'),  1 => array('data' => '件数'),    2 => array('data' => '日数'),       3 => array('data' => '初診料')
           , 4 => array('data' => '再診料'),  5 => array('data' => '指導料'),  6 => array('data' => '在宅医療'),  7 => array('data' => '院内処方')
           , 8 => array('data' => '投薬料'),  9 => array('data' => '注射料'), 10 => array('data' => '処置料'),   11 => array('data' => '手術・麻酔科')
          , 12 => array('data' => '検査料'), 13 => array('data' => 'X線科'),  14 => array('data' => 'リハビリ'), 15 => array('data' => '処方箋料')
          , 16 => array('data' => 'その他'), 17 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '内科')),        1 => array(0 => array('data' => '神経内科')),  2 => array(0 => array('data' => '呼吸器科'))
           , 3 => array(0 => array('data' => '消化器科')),    4 => array(0 => array('data' => '循環器科')),  5 => array(0 => array('data' => '小児科'))
           , 6 => array(0 => array('data' => '外科')),        7 => array(0 => array('data' => '整形外科')),  8 => array(0 => array('data' => '形成(美)外科'))
           , 9 => array(0 => array('data' => '脳神経外科')), 10 => array(0 => array('data' => '皮膚科')),   11 => array(0 => array('data' => '泌尿器科'))
          , 12 => array(0 => array('data' => '産婦人科')),   13 => array(0 => array('data' => '眼科')),     14 => array(0 => array('data' => '耳鼻咽喉科'))
          , 15 => array(0 => array('data' => '透析科')),     16 => array(0 => array('data' => '精神科')),   17 => array(0 => array('data' => '歯科'))
          , 18 => array(0 => array('data' => '放射線科')),   19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '総合計', 'style' => array($textAlign_center)))
        );

        $cellNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  4 => 'e',  5 => 'f',  6 => 'g',  7 => 'h',  8 => 'i',  9 => 'j'
          , 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p', 16 => 'q', 17 => 'r', 18 => 's', 19 => 't'
        );

        $dispData = array();
        for ($r=0; $r<=19; $r++) {
            for ($c=0; $c<=15; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr     = array();
        $totalRowDataArr = array();
        $totalColDataArr = array();

        # エクセル座標計算用変数
        $startRowNum  = 3;
        $startColNum  = 2;
        $colAlphaName = $this->columnAlphaNameArr;

        for ($r=0; $r<=19; $r++) {
            $rowNum = $startRowNum + $r;
            for ($c=0; $c<=15; $c++) {
                $dayCnt      = $dispData[$cellNameArr[$r].'_days'];
                $colNum      = $startColNum + $c;
                $dayCntCoord = $this->coord($colAlphaName[$startColNum+1], $rowNum);
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c] = array('data'  => $dispData[$cellNameArr[$r].'_account'], 'style' => array('msoNumberFormat' => $msoNumberFormat_int));
                        $totalRowDataArr[$c] = array(
                            'data'  => $this->getExcelLengthSumData($colAlphaName[$startColNum], $startRowNum, count($leftSideHeaderArr)+1)
                          , 'style' => array('msoNumberFormat' => $msoNumberFormat_int)
                        );
                        break;

                    case '1':
                        $mainDataArr[$r][$c] = array('data'  => $dayCnt, 'style' => array('msoNumberFormat' => $msoNumberFormat_int));
                        $totalRowDataArr[$c] = array(
                            'data'  => $this->getExcelLengthSumData($colAlphaName[$startColNum+1], $startRowNum, count($leftSideHeaderArr)+1)
                          , 'style' => array('msoNumberFormat' => $msoNumberFormat_int)
                        );
                        break;

                    default:
                        $mainDataArr[$r][$c] = array(
                            'data'      => $this->getExcelDivData($dispData[$cellNameArr[$r].'_'.($c-1)], $dayCntCoord, 1)
                          , 'numerator' => $dispData[$cellNameArr[$r].'_'.($c-1)]
                        );

                        # 合計列
                        $totalColDataArr[$r][0]['numerator'] = $totalColDataArr[$r][0]['numerator'] + $mainDataArr[$r][$c]['numerator'];

                        # 総合計行
                        $totalRowDataArr[$c]['numerator'] = $totalRowDataArr[$c]['numerator'] + $mainDataArr[$r][$c]['numerator'];
                        $totalRowDataArr[16]['numerator'] = $totalRowDataArr[16]['numerator'] + $mainDataArr[$r][$c]['numerator'];
                        break;
                }
                $totalColDataArr[$r][0]['data'] = $this->getExcelDivData($totalColDataArr[$r][0]['numerator'], $dayCntCoord, 1);
            }
        }

        $numerator = 0;
        $dayCntCoord = $this->coord($colAlphaName[$startColNum+1], $startRowNum+20);
        for ($c=0; $c<=16; $c++) {
            $colNum = $startColNum + $c;
            switch ($c) {
                case '0': case '1':
                    break;

                case '16':
                    $totalRowDataArr[$c]['data'] = $this->getExcelDivData($totalRowDataArr[16]['numerator'], $dayCntCoord, 1);
                    break;

                default:
                    $totalRowDataArr[$c]['data'] = $this->getExcelDivData($totalRowDataArr[$c]['numerator'], $dayCntCoord, 1);
                    break;
            }
        }

        $rtnStr .= '<table width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => $bgColor_header)).'</tr>';

        # メインデータ行
        for ($i=0; $i<=19; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_header))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .$this->convertCellData($totalColDataArr[20],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .'</tr>';

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.78 [病院] 入院診療行為別明細のエクセル出力表示
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputExcelInpatientCareActDetails($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $colspan_2         = array('name' => 'colspan',          'value' => 2);
        $rowspan_20        = array('name' => 'rowspan',          'value' => 20);

        # style
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $msoNumberForamat_comma = $this->getMsoNumberFormat('comma');

        $topHeaderArr = array(
              0 => array('data' => '病床種別/診療科', 'attr' => array($colspan_2)),     1 => array('data' => '件数'),    2 => array('data' => '日数')
			, 3 => array('data' => '初診料'),    4 => array('data' => '指導料'),        5 => array('data' => '投薬料'),  6 => array('data' => '注射料')
            , 7 => array('data' => '処置料'),    8 => array('data' => '手術・麻酔料'),  9 => array('data' => '検査料'), 10 => array('data' => 'X線料')
           , 11 => array('data' => 'リハビリ'), 12 => array('data' => 'その他'),       13 => array('data' => '入院料'), 14 => array('data' => 'DPC包括評価')
           , 15 => array('data' => '食事療養'), 16 => array('data' => '合計')
        );
        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '一般病棟', 'attr' => array($rowspan_20)), 1 => array('data' => '内科')), 1 => array(0 => array('data' => '神経内科'))
           , 2 => array(0 => array('data' => '呼吸器科')),     3 => array(0 => array('data' => '消化器科')),           4 => array(0 => array('data' => '循環器科'))
           , 5 => array(0 => array('data' => '小児科')),       6 => array(0 => array('data' => '外科')),               7 => array(0 => array('data' => '整形外科'))
           , 8 => array(0 => array('data' => '形成(美)外科')), 9 => array(0 => array('data' => '脳神経外科')),        10 => array(0 => array('data' => '皮膚科'))
          , 11 => array(0 => array('data' => '泌尿器科')),    12 => array(0 => array('data' => '産婦人科')),          13 => array(0 => array('data' => '眼科'))
          , 14 => array(0 => array('data' => '耳鼻咽喉科')),  15 => array(0 => array('data' => '透析科')),            16 => array(0 => array('data' => '精神科'))
          , 17 => array(0 => array('data' => '歯科')),        18 => array(0 => array('data' => '放射線科')),          19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '一般病棟合計',     'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center)))
          , 21 => array(0 => array('data' => '障害者病棟',       'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#cccccc'))))
          , 22 => array(0 => array('data' => '回復期リハ病床',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffa4b8'))))
          , 23 => array(0 => array('data' => '亜急性期病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 24 => array(0 => array('data' => 'ICU及びハイケア',  'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe8b7'))))
          , 25 => array(0 => array('data' => '小児・入院管理料', 'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#d5ffc8'))))
          , 26 => array(0 => array('data' => '療養病棟(医療)',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#aaddaa'))))
          , 27 => array(0 => array('data' => '介護病棟',         'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#b7c7ff'))))
          , 28 => array(0 => array('data' => '精神一般病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8ddff'))))
          , 29 => array(0 => array('data' => '精神療養病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#a4e9ff'))))
          , 30 => array(0 => array('data' => '特殊疾患病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#dbb7ff'))))
          , 31 => array(0 => array('data' => '認知症治療病棟',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 32 => array(0 => array('data' => '緩和ケア病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffddc8'))))
          , 33 => array(0 => array('data' => '総合計',           'attr' => array($colspan_2), 'style' => array('textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#ccddbb'))))
        );

        $cellNameArr = array(
             0 =>  'a',  1 =>  'b',  2 =>  'c',  3 =>  'd',  4 =>  'e',  5 =>  'f',  6 =>  'g',  7 =>  'h',  8 =>  'i',  9 =>  'j', 10 =>  'k'
          , 11 =>  'l', 12 =>  'm', 13 =>  'n', 14 =>  'o', 15 =>  'p', 16 =>  'q', 17 =>  'r', 18 =>  's', 19 =>  't', 20 =>  'u', 21 =>  'v'
          , 22 =>  'w', 23 =>  'x', 24 =>  'y', 25 =>  'z', 26 => 'ab', 27 => 'ac', 28 => 'ad', 29 => 'ae', 30 => 'af', 31 => 'ag'
        );

        $dispData = array();
        for ($r=0; $r<=32; $r++) {
            for ($c=0; $c<=14; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr                = array();
        $generalWardTotalRowDataArr = array();
        $totalRowDataArr            = array();
        $totalColDataArr            = array();

        $startRowNum = 3;
        $startColNum = 3;
        $colAlphaName = $this->columnAlphaNameArr;

        for ($r=0; $r<=31; $r++) {
            $rowNum = $startRowNum + $r;
            $rowNum = ($r>=20)? $rowNum+1 : $rowNum;

            for ($c=0; $c<=14; $c++) {
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_account'];
                        break;

                    case '1':
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_days'];
                        break;

                    default:
                        # DPC包括評価は途中からデータ無し
                        if ($r >= 20 && $c == 13) { $mainDataArr[$r][$c]['data'] = null; }
                        else { $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_'.($c-1)]; }

                        $totalColDataArr[$r][0]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum+2], $colAlphaName[$startColNum+14], $rowNum);
                        break;
                }
            }
        }

        for ($c=0; $c<=14; $c++) {
            $colNum = $startColNum + $c;
            $generalWardTotalRowDataArr[$c]['data'] = $this->getExcelLengthSumData($colAlphaName[$colNum], $startRowNum, 22);
            $totalRowDataArr[$c]['data']            = $this->getExcelLengthSumData($colAlphaName[$colNum], 23, 35);
        }
        $generalWardTotalRowDataArr[15]['data'] = $this->getExcelSideSumData($colAlphaName[$startColNum+2], $colAlphaName[$startColNum+14], 23);
        $totalColDataArr[33][0]['data']         = $this->getExcelSideSumData($colAlphaName[$startColNum+2], $colAlphaName[$startColNum+14], 36);

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8'))).'</tr>';

        # メインデータ行
        for ($i=0; $i<20; $i++) {

            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .'</tr>';

        }
        $rtnStr .= '<tr>'
                  .$this->convertCellData($leftSideHeaderArr[20],      $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                  .$this->convertCellData($generalWardTotalRowDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                  .'</tr>';

        for ($i=21; $i<33; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left))
                      .$this->convertCellData($mainDataArr[$i-1],     $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .$this->convertCellData($totalColDataArr[$i-1], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[33], $fontTag, array('whiteSpace' => $whiteSpace_nowrap))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .$this->convertCellData($totalColDataArr[33],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberForamat_comma))
                      .'</tr>';
        $rtnStr .= '</table>';
        return $rtnStr;
    }


    /**
     * No.79 [病院] 入院日当点のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputInpatientDailyAllowancePoint($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $colspan_2             = array('name' => 'colspan',           'value' => 2);
        $rowspan_20            = array('name' => 'rowspan',           'value' => 20);

        # style
        $textAlign_center      = array('name' => 'text-align',        'value' => 'center');
        $textAlign_left        = array('name' => 'text-align',        'value' => 'left');
        $textAlign_right       = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap     = array('name' => 'white-space',       'value' => 'nowrap');
        $bgColor_header        = array('name' => 'background-color',  'value' => '#8effa8');
        $msoNumberFormat_float = $this->getMsoNumberFormat('decimal');

        $topHeaderArr = array(
              0 => array('data' => '病床種別/診療科', 'attr' => array($colspan_2)),     1 => array('data' => '件数'),    2 => array('data' => '日数')
            , 3 => array('data' => '初診料'),    4 => array('data' => '指導料'),        5 => array('data' => '投薬料'),  6 => array('data' => '注射料')
            , 7 => array('data' => '処置料'),    8 => array('data' => '手術・麻酔料'),  9 => array('data' => '検査料'), 10 => array('data' => 'X線料')
           , 11 => array('data' => 'リハビリ'), 12 => array('data' => 'その他'),       13 => array('data' => '入院料'), 14 => array('data' => 'DPC包括評価')
           , 15 => array('data' => '食事療養'), 16 => array('data' => '合計')
        );

        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '一般病棟', 'attr' => array($rowspan_20)), 1 => array('data' => '内科')), 1 => array(0 => array('data' => '神経内科'))
           , 2 => array(0 => array('data' => '呼吸器科')),     3 => array(0 => array('data' => '消化器科')),           4 => array(0 => array('data' => '循環器科'))
           , 5 => array(0 => array('data' => '小児科')),       6 => array(0 => array('data' => '外科')),               7 => array(0 => array('data' => '整形外科'))
           , 8 => array(0 => array('data' => '形成(美)外科')), 9 => array(0 => array('data' => '脳神経外科')),        10 => array(0 => array('data' => '皮膚科'))
          , 11 => array(0 => array('data' => '泌尿器科')),    12 => array(0 => array('data' => '産婦人科')),          13 => array(0 => array('data' => '眼科'))
          , 14 => array(0 => array('data' => '耳鼻咽喉科')),  15 => array(0 => array('data' => '透析科')),            16 => array(0 => array('data' => '精神科'))
          , 17 => array(0 => array('data' => '歯科')),        18 => array(0 => array('data' => '放射線科')),          19 => array(0 => array('data' => '麻酔科'))
          , 20 => array(0 => array('data' => '一般病棟合計',     'attr' => array($colspan_2), 'style' => array($textAlign_center)))
          , 21 => array(0 => array('data' => '障害者病棟',       'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#cccccc'))))
          , 22 => array(0 => array('data' => '回復期リハ病床',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffa4b8'))))
          , 23 => array(0 => array('data' => '亜急性期病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 24 => array(0 => array('data' => 'ICU及びハイケア',  'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffe8b7'))))
          , 25 => array(0 => array('data' => '小児・入院管理料', 'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#d5ffc8'))))
          , 26 => array(0 => array('data' => '療養病棟(医療)',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#aaddaa'))))
          , 27 => array(0 => array('data' => '介護病棟',         'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#b7c7ff'))))
          , 28 => array(0 => array('data' => '精神一般病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#c8ddff'))))
          , 29 => array(0 => array('data' => '精神療養病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#a4e9ff'))))
          , 30 => array(0 => array('data' => '特殊疾患病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#dbb7ff'))))
          , 31 => array(0 => array('data' => '認知症治療病棟',   'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffc8e9'))))
          , 32 => array(0 => array('data' => '緩和ケア病棟',     'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ffddc8'))))
          , 33 => array(0 => array('data' => '総合計',           'attr' => array($colspan_2), 'style' => array('bgColor' => array('name' => 'background-color', 'value' => '#ccddbb'), 'textAlign' => $textAlign_center)))
        );

        $cellNameArr = array(
             0 =>  'a',  1 =>  'b',  2 =>  'c',  3 =>  'd',  4 =>  'e',  5 =>  'f',  6 =>  'g',  7 =>  'h',  8 =>  'i',  9 =>  'j', 10 =>  'k'
          , 11 =>  'l', 12 =>  'm', 13 =>  'n', 14 =>  'o', 15 =>  'p', 16 =>  'q', 17 =>  'r', 18 =>  's', 19 =>  't', 20 =>  'u', 21 =>  'v'
          , 22 =>  'w', 23 =>  'x', 24 =>  'y', 25 =>  'z', 26 => 'ab', 27 => 'ac', 28 => 'ad', 29 => 'ae', 30 => 'af', 31 => 'ag'
        );

        $dispData = array();
        for ($r=0; $r<=32; $r++) {
            for ($c=0; $c<=14; $c++) {
                switch ($c) {
                    case '0':
                        $dispData[$cellNameArr[$r].'_account'] = 0;
                        break;

                    case '1':
                        $dispData[$cellNameArr[$r].'_days'] = 0;
                        break;

                    default:
                        $dispData[$cellNameArr[$r].'_'.($c-1)] = 0;
                        break;
                }
            }
        }

        # このあたりで、データの格納
        if (is_array($dataArr)) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $dispData[$dataArrKey] = $dataArrVal; } }

        $mainDataArr                = array();
        $generalWardTotalRowDataArr = array();
        $totalRowDataArr            = array();
        $totalColDataArr            = array();

        $startRowNum  = 3;
        $startColNum  = 3;
        $colAlphaName = $this->columnAlphaNameArr;

        for ($r=0; $r<=32; $r++) {
            $rowNum = $startRowNum + $r;
            $rowNum = ($r>=20)? $rowNum + 1 : $rowNum;
            for ($c=0; $c<=14; $c++) {
                $dayCnt = $dispData[$cellNameArr[$r].'_days'];
                $dayCntCoord = $this->coord($colAlphaName[$startColNum+1], $rowNum);
                switch ($c) {
                    case '0':
                        $mainDataArr[$r][$c]['data'] = $dispData[$cellNameArr[$r].'_account'];
                        break;

                    case '1':
                        $mainDataArr[$r][$c]['data'] = $dayCnt;
                        break;

                    default:
                        # DPC包括評価は途中からデータ無し
                        if ($r >= 20 && $c == 13) {
                            $mainDataArr[$r][$c]['data']        = null;
                            $totalRowDataArr[$c]['numerator']   = $totalRowDataArr[$c]['numerator']   + null;
                            $totalRowDataArr[$c]['denominator'] = $totalRowDataArr[$c]['denominator'] + null;
                        }
                        else {
                            $mainDataArr[$r][$c]['numerator']   = $mainDataArr[$r][$c]['numerator']   + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $mainDataArr[$r][$c]['denominator'] = $dayCnt;
                            $mainDataArr[$r][$c]['data']        = sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $dayCntCoord, $mainDataArr[$r][$c]['numerator'], $dayCntCoord);
                            $mainDataArr[$r][$c]['style']       = array($msoNumberFormat_float);

                            $totalRowDataArr[$c]['numerator']   = $totalRowDataArr[$c]['numerator']   + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $totalRowDataArr[$c]['denominator'] = $totalRowDataArr[$c]['denominator'] + $dayCnt;
                        }

                        if ($r < 20) {
                            $generalWardTotalRowDataArr[$c]['numerator']   = $generalWardTotalRowDataArr[$c]['numerator'] + $dispData[$cellNameArr[$r].'_'.($c-1)];
                            $generalWardTotalRowDataArr[$c]['denominator'] = $generalWardTotalRowDataArr[$c]['denominator'] + $dayCnt;
                        }
                        $totalColDataArr[$r][0]['numerator']   = $totalColDataArr[$r][0]['numerator']   + $dispData[$cellNameArr[$r].'_'.($c-1)];
                        $totalColDataArr[$r][0]['denominator'] = $dayCnt;
                        $totalColDataArr[$r][0]['data']        = sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $dayCntCoord, $totalColDataArr[$r][0]['numerator'], $dayCntCoord);
                        $totalColDataArr[$r][0]['style']       = array($msoNumberFormat_float);
                        break;
                }
            }
        }

        $generalWardNumerator   = 0;
        $generalWardDenominator = 0;
        $generalWardTotal       = 0;
        $numerator              = 0;
        $denominator            = 0;
        $total                  = 0;

        $sideTotalDenominator = 0;
        $sideTotalNumerator   = 0;
        $totalDenominator     = 0;
        $totalNumerator       = 0;

        for ($c=0; $c<=14; $c++) {
            $colNum = $startColNum + $c;
            switch ($c) {
                case '0': case '1':
                    $generalWardTotalRowDataArr[$c]['data'] = $this->getExcelLengthSumData($colAlphaName[$colNum], $startRowNum, 22);
                    $totalRowDataArr[$c]['data']            = $this->getExcelLengthSumData($colAlphaName[$colNum], 23, 35);
                    break;

                case '13':
                    $sideTotalDenominatorCoord = $this->coord($colAlphaName[$startColNum+1], 23);
                    $sideTotalNumerator   = $sideTotalNumerator   +  $generalWardTotalRowDataArr[$c]['numerator'];
                    $generalWardTotalRowDataArr[$c] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $sideTotalDenominatorCoord, $generalWardTotalRowDataArr[$c]['numerator'], $sideTotalDenominatorCoord), 'style' => array('msoNumberFormat' => $msoNumberFormat_float));

                    $totalDenominatorCoord = $this->coord($colAlphaName[$startColNum+1], 36);
                    $totalDenominator = $totalDenominator + $totalRowDataArr[$c]['denominator'];
                    $totalNumerator   = $totalNumerator   + $totalRowDataArr[$c]['numerator'];
                    $totalRowDataArr[$c] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $sideTotalDenominatorCoord, $totalRowDataArr[$c]['numerator'], $sideTotalDenominatorCoord), 'style' => array('msoNumberFormat' => $msoNumberFormat_float));
                    break;

                default:
                    $sideTotalDenominatorCoord = $this->coord($colAlphaName[$startColNum+1], 23);
                    $sideTotalNumerator   = $sideTotalNumerator   +  $generalWardTotalRowDataArr[$c]['numerator'];
                    $generalWardTotalRowDataArr[$c] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $sideTotalDenominatorCoord, $generalWardTotalRowDataArr[$c]['numerator'], $sideTotalDenominatorCoord), 'style' => array('msoNumberFormat' => $msoNumberFormat_float));

                    $totalDenominatorCoord = $this->coord($colAlphaName[$startColNum+1], 36);
                    $totalDenominator = $totalDenominator + $totalRowDataArr[$c]['denominator'];
                    $totalNumerator   = $totalNumerator   + $totalRowDataArr[$c]['numerator'];
                    $totalRowDataArr[$c] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $totalDenominatorCoord, $totalRowDataArr[$c]['numerator'], $totalDenominatorCoord), 'style' => array('msoNumberFormat' => $msoNumberFormat_float));
                    break;
            }
        }

        $generalWardTotalRowDataArr[15] = array(
            'data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $sideTotalDenominatorCoord, $sideTotalNumerator, $sideTotalDenominatorCoord)
          , 'style' => array('msoNumberFormat' => $msoNumberFormat_float)
        );
        $totalColDataArr[33][0] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $totalDenominatorCoord, $totalNumerator, $totalDenominatorCoord), 'style' => array('msoNumberFormat' => $msoNumberFormat_float));

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="1" >'
                      .'<tr>'
                          .$this->convertCellData(array(
                              0 => array('data' => $facilityName)
                            , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          ),
                          $fontTag,
                          array('whiteSpace' => $whiteSpace_nowrap)
                      )
                      .'</tr>'
                  .'</table>';

        $rtnStr .= '<table class="list" width="100%"border="1" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_center, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8'))).'</tr>';

        # メインデータ行
        for ($i=0; $i<20; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .$this->convertCellData($totalColDataArr[$i],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .'</tr>';
        }
         $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[20],      $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left, 'bgColor' => array('name' => 'background-color', 'value' => '#8effa8')))
                      .$this->convertCellData($generalWardTotalRowDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right))
                      .'</tr>';
        for ($i=21; $i<33; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_left))
                      .$this->convertCellData($mainDataArr[$i-1],     $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .$this->convertCellData($totalColDataArr[$i-1], $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .'</tr>';
        }

        # 総合計行
        $rtnStr .=    '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[33], $fontTag, array('whiteSpace' => $whiteSpace_nowrap))
                      .$this->convertCellData($totalRowDataArr,       $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .$this->convertCellData($totalColDataArr[33],   $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'textAlign' => $textAlign_right, 'msoNumberFormat' => $msoNumberFormat_float))
                      .'</tr>';
        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.80 [病院] 診療報酬総括表[医科]
     * No.81 [病院] 診療報酬総括表[歯科]のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArrs
     * @return string $rtnStr
     */
    function outputExcelMedicalTreatmentFeeGeneralizationTable($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();

        # parameter
        $fontTag      = $this->getFontTag();
        $facilityName = '病院名&nbsp;'.$plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # style
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $textAlign_left      = array('name' => 'text-align',       'value' => 'left');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');
        $bgColor_salmon      = array('name' => 'background-color', 'value' => $this->getRgbColor('salmon'));
        $bgColor_cadetBlue   = array('name' => 'background-color', 'value' => $this->getRgbColor('cadet_blue'));
        $bgColor_orange      = array('name' => 'background-color', 'value' => $this->getRgbColor('orange'));
        $bgColor_purple      = array('name' => 'background-color', 'value' => $this->getRgbColor('purple'));
        $bgColor_lightGreen  = array('name' => 'background-color', 'value' => $this->getRgbColor('light_green'));
        $bgColor_cadetGreen  = array('name' => 'background-color', 'value' => $this->getRgbColor('cadet_green'));
        $bgColor_lightBrown  = array('name' => 'background-color', 'value' => $this->getRgbColor('light_brown'));
        $bgColor_pink        = array('name' => 'background-color', 'value' => $this->getRgbColor('pink'));
        $bgColor_yellow      = array('name' => 'background-color', 'value' => "#FFF975");
        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');

        # attr
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_15 = array('name' => 'colspan', 'value' => 15);
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_12 = array('name' => 'rowspan', 'value' => 12);

        # top header
        $topHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_3, $rowspan_2))
                     , 1 => array('data' => '入院',   'attr' => array($colspan_4))
                     , 2 => array('data' => '外来',   'attr' => array($colspan_4))
                     , 3 => array('data' => '合計',   'attr' => array($colspan_4))
            )
          , 1 => array(
                 0 => array('data' => '件数')
               , 1 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
               , 2 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
               , 3 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
               , 4 => array('data' => '件数')
               , 5 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
               , 6 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
               , 7 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
               , 8 => array('data' => '件数')
               , 9 => array('data' => '日数', 'style' => array('bgColor' => $bgColor_orange))
              , 10 => array('data' => '点数', 'style' => array('bgColor' => $bgColor_purple))
              , 11 => array('data' => '金額', 'style' => array('bgColor' => $bgColor_lightGreen))
            )
        );

        # side header
        $leftSideHeaderArr = array(
             0 => array(0 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 1 => array('data' => '基金', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '単独')
             )
           , 1 => array(0 => array('data' => '生保'))
           , 2 => array(0 => array('data' => '(旧)27老人'))
           , 3 => array(0 => array('data' => '公費併用'))
           , 4 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
           , 5 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '基金', 'attr' => array($rowspan_6))
                      , 2 => array('data' => '一般')
             )
           , 6 => array(0 => array('data' => '退職'))
           , 7 => array(0 => array('data' => '39長寿'))
           , 8 => array(0 => array('data' => '(旧)27老人'))
           , 9 => array(0 => array('data' => '公費併用'))
          , 10 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 11 => array(0 => array('data' => '労災', 'attr' => array($colspan_2))
                      , 1 => array('data' => '小計3')
            )
          , 12 => array(0 => array('data' => '自賠', 'attr' => array($colspan_2))
                      , 1 => array('data' => '小計4')
            )
          , 13 => array(0 => array('data' => '合計 A', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 14 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 15 => array(0 => array('data' => "返戻<br />再請求分", 'attr' => array($rowspan_12))
                      , 1 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '単独')
            )
          , 16 => array(0 => array('data' => '生保'))
          , 17 => array(0 => array('data' => '(旧)27老人'))
          , 18 => array(0 => array('data' => '公費併用'))
          , 19 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
          , 20 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '一般')
            )
          , 21 => array(0 => array('data' => '退職'))
          , 22 => array(0 => array('data' => '39長寿'))
          , 23 => array(0 => array('data' => '(旧)27老人'))
          , 24 => array(0 => array('data' => '公費併用'))
          , 25 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 26 => array(0 => array('data' => '労災')
                      , 1 => array('data' => '小計3')
            )
          , 27 => array(0 => array('data' => '合計 B', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 28 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 29 => array(0 => array('data' => "査定･過誤<br />再審査請求分", 'attr' => array($rowspan_12))
                      , 1 => array('data' => '社保', 'attr' => array($rowspan_5))
                      , 2 => array('data' => '一般')
            )
          , 30 => array(0 => array('data' => '生保'))
          , 31 => array(0 => array('data' => '(旧)27老人'))
          , 32 => array(0 => array('data' => '公費併用'))
          , 33 => array(0 => array('data' => '小計1', 'style' => array($bgColor_yellow)))
          , 34 => array(0 => array('data' => '国保', 'attr' => array($rowspan_6))
                      , 1 => array('data' => '一般')
            )
          , 35 => array(0 => array('data' => '退職'))
          , 36 => array(0 => array('data' => '39長寿'))
          , 37 => array(0 => array('data' => '(旧)27老人'))
          , 38 => array(0 => array('data' => '公費併用'))
          , 39 => array(0 => array('data' => '小計2', 'style' => array($bgColor_yellow)))
          , 40 => array(0 => array('data' => '労災')
                      , 1 => array('data' => '小計3')
            )
          , 41 => array(0 => array('data' => '合計 C', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          , 42 => array(0 => array('data' => '当月請求 A - (B + C)', 'attr' => array($colspan_3), 'style' => array($bgColor_yellow)))
          # 空白行
          , 43 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_15)))
          , 44 => array(0 => array('data' => '前年分', 'attr' => array($rowspan_3))
                      , 1 => array('data' => '当月請求分', 'attr' => array($colspan_2))
            )
          , 45 => array(0 => array('data' => '前年同月請求分', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => '差額', 'attr' => array($colspan_2), 'style' => array($bgColor_yellow)))
        );


        $cellDataNameArr = array(
             0 => 'a',  1 => 'b',  2 => 'c',  3 => 'd',  5 => 'e',  6 =>  'f',  7 =>  'g',  8 => 'h',  9 => 'i', 15 => 'j', 16 => 'k', 17 => 'l', 18 =>  'm', 20 =>  'n'
          , 21 => 'o', 22 => 'p', 23 => 'q', 24 => 'r', 29 => 's', 30 =>  't', 31 =>  'u', 32 => 'v', 34 => 'w', 35 => 'x', 36 => 'y', 37 => 'z', 38 => 'ab', 39 => 'ac'
        );

        $mainData    = array();
        $mainDataArr = array();

        foreach ($cellDataNameArr as $key => $val) {
            for ($l=1; $l<=8; $l++) {
                $mainData[$val.'_'.$l] = null;
                if ($l < 5) { $mainData[$val.'_'.$l.'_'.($l + 4)] = null; }
            }
        }
        $kanaArr = array(
            0 =>  'a', 1 =>   'i',  2 =>  'u',  3 =>  'e',  4 => 'ka',  5 =>  'ki',  6 => 'ku',  7 => 'ke'
         ,  8 => 'sa', 9 => 'shi', 10 => 'su', 11 => 'se', 12 => 'ta', 13 => 'chi', 14 => 'te', 15 => 'to'
        );
        for ($k=0; $k<count($kanaArr); $k++) {
            $mainData['kana_'.$kanaArr[$k].'_1']   = null;
            $mainData['kana_'.$kanaArr[$k].'_2']   = null;
            $mainData['kana_'.$kanaArr[$k].'_1_2'] = null;
        }

        # データを $mainData に入力する
        if (is_array($dataArr) && count($dataArr)>0) { foreach ($dataArr[0] as $dataArrKey => $dataArrVal) { $mainData[$dataArrKey] = $dataArrVal; } }

        $startRowNum  = 4;
        $startColNum  = 4;
        $colAlphaName = $this->columnAlphaNameArr;

        # main data
        for ($i=0; $i<count($leftSideHeaderArr); $i++) {
            $mainDataArr[$i] = array();
            $tmpArr          = array();

            $colNum = $startColNum - 1;
            $rowNum = $startRowNum + $i;
            if ($i >= 15 && $i < 29)  { $rowNum = $startRowNum + $i + 2; }
            if ($i >= 29 && $i < 44)  { $rowNum = $startRowNum + $i + 4; }
            if ($i >= 44 && $i <= 46) { $rowNum = $startRowNum + $i + 6; }

            switch ($i) {
                case '0': case '1': case '2': case '5': case '6': case '7': case '8': case '15': case '16': case '17': case '20': case '21': case '22': case '23':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)]);
                        $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)]);
                        $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                case '3': case '9': case '18': case '24': case '29': case '30': case '31': case '32': case '34': case '35': case '36': case '37': case '38':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)], 'style' => array($bgColor_salmon));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)], 'style' => array($bgColor_salmon));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_salmon));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)]);
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)]);
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 1
                case '4': case '19':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 2
                case '10': case '25':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-5), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-5), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-5), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-5), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 3
                case '11':
                    $kanaAArr = array(0 => 'a', 1 => 'i', 2 => 'u', 3 => 'e');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]    = array('data' => $mainData['kana_'.$kanaAArr[$l].'_1']);
                        $tmpArr[$l+4]  = array('data' => $mainData['kana_'.$kanaAArr[$l].'_2']);
                        $tmpArr[$l+8]  = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 4
                case '12':
                    $kanaKaArr = array(0 => 'ka', 1 => 'ki', 2 => 'ku', 3 => 'ke');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaKaArr[$l].'_1']);
                        $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaKaArr[$l].'_2']);
                        $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 合計 A
                case '13':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => sprintf('=%s+%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-9), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-3), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                        $tmpArr[$l+4] = array('data' => sprintf('=%s+%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-9), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-3), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                        $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',       $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 3
                case '26':
                    $kanaSaArr = array(0 => 'sa', 1 => 'shi', 2 => 'su', 3 => 'se');
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaSaArr[$l].'_1']);
                        $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaSaArr[$l].'_2']);
                        $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 合計 B
                case '27':
                    for ($l=0; $l<4; $l++) {
                        $tmpArr[$l]   = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                        $tmpArr[$l+4] = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                        $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',    $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 1
                case '33':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-4), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)),   'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 2
                case '39':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)], 'style' => array($bgColor_salmon), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)], 'style' => array($bgColor_salmon), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+1)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => $mainData[$cellDataNameArr[$i].'_'.($l+5)], 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 小計 3
                case '40':
                    $kanaTaArr = array(0 => 'ta', 1 => 'chi', 2 => 'te', 3 => 'to');
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1'], 'style' => array($bgColor_salmon));
                                $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_2'], 'style' => array($bgColor_salmon));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_salmon));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_1']);
                                $tmpArr[$l+4] = array('data' => $mainData['kana_'.$kanaTaArr[$l].'_2']);
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 合計 C
                case '41':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default:
                                $tmpArr[$l]   = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',    $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;

                            case '3':
                                $tmpArr[$l]   = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=%s+%s+%s', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-8), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',    $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),   $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 当月請求 A - ( B + C )
                case '42':
                    for ($l=0; $l<4; $l++) {
                        switch ($l) {
                            default: # A - B
                                $tmpArr[$l]   = array('data' => sprintf('=%s-%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-33), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-17)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+4] = array('data' => sprintf('=%s-%s', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-33), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-17)), 'style' => array($bgColor_yellow));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;

                            case '3': # A - (B + C)
                                $tmpArr[$l]   = array('data' => sprintf('=%s-(%s+%s)', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-33), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-17), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array('bgColor' => $bgColor_yellow, 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '0')));
                                $tmpArr[$l+4] = array('data' => sprintf('=%s-(%s+%s)', $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-33), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-17), $this->coord($colAlphaName[$colNum+($l+5)], $rowNum-1)), 'style' => array('bgColor' => $bgColor_yellow, 'msoNumberFormat' => array('name' => 'mso-number-format', 'value' => '0')));
                                $tmpArr[$l+8] = array('data' => sprintf('=%s+%s',      $this->coord($colAlphaName[$colNum+($l+1)], $rowNum),    $this->coord($colAlphaName[$colNum+($l+5)], $rowNum)), 'style' => array($bgColor_yellow));
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                case '44': #  前年分 当月請求分
                    for ($l=0; $l<12; $l++) { $tmpArr[$l] = array('data' => sprintf('=%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-4))); }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 前年分 前年度分
                case '45':
                    for ($l=0; $l<12; $l++) {
                        switch ($l) {
                            case '8': case '9': case '10': case '11':
                                $tmpArr[$l] = array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$colNum+($l-7)], $rowNum), $this->coord($colAlphaName[$colNum+($l-3)], $rowNum)));
                                break;

                            default:
                                $tmpArr[$l] = array('data' => $mainData['claim_prev_year_'.($l+1)]);
                                break;
                        }
                    }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 前年分 差額
                case '46':
                    for ($l=0; $l<12; $l++) { $tmpArr[$l] = array('data' => sprintf('=%s-%s', $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-2), $this->coord($colAlphaName[$colNum+($l+1)], $rowNum-1)), 'style' => array($bgColor_yellow)); }
                    $mainDataArr[$i] = $tmpArr;
                    break;

                # 空白行は何もしない
                case '14': case '28': case '43':
                    break;

                default:
                    for ($l=0; $l<12; $l++) { $tmpArr[$l] = array('data' => '&nbsp;'); }
                    $mainDataArr[$i] = $tmpArr;
                    break;
            }
        }

        # ここから表示データ
        $rtnStr .= '<table width="100%" border="1" >'
                      .'<tr>'
                      .$this->convertCellData(
                          array(0 => array('data' => $facilityName, 'style' => array('textAlign' => $textAlign_left))
                              , 1 => array('data' => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array($textAlign_right))
                          )
                        , $fontTag
                        , array()
                      )
                      .'</tr>';
        $rtnStr .= '</table>';

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                      .'<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_cadetBlue, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap)).'</tr>';

        for ($i=0; $i <= 14; $i++) {
            switch ($i) {
                case '14':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_cadetBlue, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_cadetGreen, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap, $msoNumberFormat_comma)).'</tr>';

        for ($i=15; $i <= 28; $i++) {
            switch ($i) {
                case '28':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_cadetGreen, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_lightBrown, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap, $msoNumberFormat_comma)).'</tr>';

        for ($i=29; $i <= 43; $i++) {
            switch ($i) {
                case '43':
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array($textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;

                default:
                    $rtnStr .= '<tr>'
                              .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_lightBrown, $textAlign_center, $whiteSpace_nowrap))
                              .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                              .'</tr>';
                    break;
            }
        }

        $rtnStr .=     '<tr>'.$this->convertCellData($topHeaderArr[0], $fontTag, array('bgColor' => $bgColor_pink, $textAlign_center, $whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($topHeaderArr[1], $fontTag, array($textAlign_center, $whiteSpace_nowrap, $msoNumberFormat_comma)).'</tr>';

        for ($i=44; $i <= 46; $i++) {
            $rtnStr .= '<tr>'
                      .$this->convertCellData($leftSideHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_pink, $textAlign_center, $whiteSpace_nowrap))
                      .$this->convertCellData($mainDataArr[$i],       $fontTag, array($textAlign_right,  $whiteSpace_nowrap, $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.103 [老健] 患者月報(日報・日毎)のエクセル出力
     * @access protected
     * @param  array     $dataArr
     * @param  array     $plantArr
     * @param  array     $arr
     * @param  array     $optionArr
     * @return string    $rtnStr    [HTML table]
     */
    function outputExcelMonthlyPerDate($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $displayYear  = $this->getArrValueByKey('year',  $optionArr);
        $displayMonth = $this->getArrValueByKey('month', $optionArr);

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_8  = array('name' => 'colspan', 'value' =>  8);
        $colspan_12 = array('name' => 'colspan', 'value' => 12);
        $colspan_13 = array('name' => 'colspan', 'value' => 13);
        $colspan_14 = array('name' => 'colspan', 'value' => 14);
        $colspan_15 = array('name' => 'colspan', 'value' => 15);
        $colspan_16 = array('name' => 'colspan', 'value' => 16);

        # style
        $bgColor_blue               = array('name' => 'background-color',  'value' => $this->getRgbColor('blue'));
        $textAlign_center           = array('name' => 'text-align',        'value' => 'center');
        $textAlign_right            = array('name' => 'text-align',        'value' => 'right');
        $whiteSpace_nowrap          = array('name' => 'white-space',       'value' => 'nowrap');
        $msoNumberFormat_parcent0_0 = $this->getMsoNumberFormat('percent');
        $msoNumberFormat_0_0        = $this->getMsoNumberFormat('comma');
//        $msoNumberFormat_parcent    = array('name' => 'mso-number-format', 'value' => '0%');
//        $msoNumberFormat_parcent0_0 = array('name' => 'mso-number-format', 'value' => '0.0%');
//        $msoNumberFormat_0_0        = array('name' => 'mso-number-format', 'value' => '0.0');

        $rtnStr .= '<table class="list" width="100%" border="1" >'
                     .'<tr>'
                        .$this->convertCellData(
                            array(
                                0 => array('data' => '&nbsp;', 'attr' => array($rowspan_3))
                              , 1 => array('data' => '通所', 'attr' => array($colspan_12))
                              , 2 => array('data' => '入所', 'attr' => array($colspan_15))
                              , 3 => array('data' => '訪問リハビリ<br />(単位)', 'attr' => array($colspan_3, $rowspan_2))
                              , 4 => array('data' => '判定件数<br />(本入所のみ)', 'attr' => array($colspan_2, $rowspan_2))
                              , 5 => array('data' => '営業件数<br />(訪問件数のみ)', 'attr' => array($rowspan_3))
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>'
                     .'<tr>'
                        .$this->convertCellData(
                            array(
                                 0 => array('data' => '定員数', 'attr' => array($rowspan_2))
                               , 1 => array('data' => '通所リハビリ', 'attr' => array($colspan_8))
                               , 2 => array('data' => '予防通所リハビリ', 'attr' => array($rowspan_2))
                               , 3 => array('data' => '合計', 'attr' => array($rowspan_2))
                               , 4 => array('data' => '稼働率', 'attr' => array($rowspan_2))
                               , 5 => array('data' => '定員数', 'attr' => array($rowspan_2))
                               , 6 => array('data' => '一般', 'attr' => array($rowspan_2))
                               , 7 => array('data' => '一般<br />ショートステイ', 'attr' => array($rowspan_2))
                               , 8 => array('data' => '一般<br />予防ショート', 'attr' => array($rowspan_2))
                               , 9 => array('data' => '認知', 'attr' => array($rowspan_2))
                              , 10 => array('data' => '認知<br />ショートステイ', 'attr' => array($rowspan_2))
                              , 11 => array('data' => '認知<br />予防ショート', 'attr' => array($rowspan_2))
                              , 12 => array('data' => '小計', 'attr' => array($rowspan_2))
                              , 13 => array('data' => '入所数', 'attr' => array($rowspan_2))
                              , 14 => array('data' => '退所数', 'attr' => array($rowspan_2))
                              , 15 => array('data' => 'ショート<br />予防ショート<br />入所数', 'attr' => array($rowspan_2))
                              , 16 => array('data' => 'ショート<br />予防ショート<br />退所数', 'attr' => array($rowspan_2))
                              , 17 => array('data' => '空床数', 'attr' => array($rowspan_2))
                              , 18 => array('data' => '稼働率', 'attr' => array($rowspan_2, $colspan_2))
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>'
                     .'<tr>'
                        .$this->convertCellData(
                            array(
                                 0 => array('data' => '1時間以上<br />2時間未満')
                               , 1 => array('data' => '2時間以上<br />3時間未満')
                               , 2 => array('data' => '3時間以上<br />4時間未満')
                               , 3 => array('data' => '4時間以上<br />6時間未満')
                               , 4 => array('data' => '6時間以上<br />8時間未満')
                               , 5 => array('data' => '8時間以上<br />9時間未満')
                               , 6 => array('data' => '9時間以上<br />10時間未満')
                               , 7 => array('data' => '小計')
                               , 8 => array('data' => '訪問リハビリ')
                               , 9 => array('data' => '介護予防<br />訪問リハビリ')
                              , 10 => array('data' => '合計')
                              , 11 => array('data' => '可')
                              , 12 => array('data' => '不可')
                            ),
                            $fontTag,
                            array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap)
                        )
                     .'</tr>';

        $startRowNum     = 5;
        $startColumnNum  = 1;

        $columnAlphaName = $this->columnAlphaNameArr;

        $totalData = array();
        if (is_array($plantArr)) {
            foreach ($plantArr as $plantKey => $plantVal) {
                $facilityId   = $this->getArrValueByKey('facility_id',   $plantVal);
                $facilityName = $this->getArrValueByKey('facility_name', $plantVal);

                $lastDay = $this->getLastDay($displayYear, $displayMonth);
                $totalRowNum     = $startRowNum + 1 + $lastDay;
                for ($d=1; $d <= $lastDay; $d++) {
                    $rowNum = $startRowNum + $d;

                    $displayDate = sprintf('%02d月%02d日(%s)', $displayMonth, $d, $this->getDayOfTheWeekForDate($displayYear, $displayMonth, $d));
                    $mainData     = array();
                    $dailyLimit   = null;
                    $dispNum_7    = null;
                    $dispNum_14   = null;
                    $dispNum_21   = null;
                    $dispNum_28   = null;
                    $dispNum_35   = null;
                    $dispNum_42   = null;
                    $dispNum_49   = null;
                    $dispNum_53   = null;
                    $enterLimit   = null;
                    $dispNum_60   = null;
                    $dispNum_74   = null;
                    $dispNum_85   = null;
                    $dispNum_67   = null;
                    $dispNum_81   = null;
                    $dispNum_89   = null;
                    $dispNum_101  = null;
                    $dispNum_102  = null;
                    $dispNum_104  = null;
                    $dispNum_105  = null;
                    $dispNum_96   = null;
                    $dispNum_100  = null;
                    $dispNum_106  = null;
                    $dispNum_107  = null;
                    $dispNum_108  = null;

                    if (is_array($dataArr)) {
                        foreach ($dataArr as $dataVal) {
                            if ($d == $this->getArrValueByKey('day', $dataVal)) {
                                $dailyLimit   = $this->getArrValueByKey('daily_limit', $dataVal);
                                $dispNum_7    = $this->getArrValueByKey('dispnum_7',   $dataVal);
                                $dispNum_14   = $this->getArrValueByKey('dispnum_14',  $dataVal);
                                $dispNum_21   = $this->getArrValueByKey('dispnum_21',  $dataVal);
                                $dispNum_28   = $this->getArrValueByKey('dispnum_28',  $dataVal);
                                $dispNum_35   = $this->getArrValueByKey('dispnum_35',  $dataVal);
                                $dispNum_42   = $this->getArrValueByKey('dispnum_42',  $dataVal);
                                $dispNum_49   = $this->getArrValueByKey('dispnum_49',  $dataVal);
                                $dispNum_53   = $this->getArrValueByKey('dispnum_53',  $dataVal);
                                $enterLimit   = $this->getArrValueByKey('enter_limit', $dataVal);
                                $dispNum_60   = $this->getArrValueByKey('dispnum_60',  $dataVal);
                                $dispNum_74   = $this->getArrValueByKey('dispnum_74',  $dataVal);
                                $dispNum_85   = $this->getArrValueByKey('dispnum_85',  $dataVal);
                                $dispNum_67   = $this->getArrValueByKey('dispnum_67',  $dataVal);
                                $dispNum_81   = $this->getArrValueByKey('dispnum_81',  $dataVal);
                                $dispNum_89   = $this->getArrValueByKey('dispnum_89',  $dataVal);
                                $dispNum_101  = $this->getArrValueByKey('dispnum_101', $dataVal);
                                $dispNum_102  = $this->getArrValueByKey('dispnum_102', $dataVal);
                                $dispNum_104  = $this->getArrValueByKey('dispnum_104', $dataVal);
                                $dispNum_105  = $this->getArrValueByKey('dispnum_105', $dataVal);
                                $dispNum_96   = $this->getArrValueByKey('dispnum_96',  $dataVal);
                                $dispNum_100  = $this->getArrValueByKey('dispnum_100', $dataVal);
                                $dispNum_106  = $this->getArrValueByKey('dispnum_106', $dataVal);
                                $dispNum_107  = $this->getArrValueByKey('dispnum_107', $dataVal);
                                $dispNum_108  = $this->getArrValueByKey('dispnum_108', $dataVal);
                            }
                        }
                    }

                    # 通所
                    $mainData[0]  = array('data' => $displayDate, 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                    $mainData[1]  = array('data' => $dailyLimit);
                    $mainData[2]  = array('data' => $dispNum_7);
                    $mainData[3]  = array('data' => $dispNum_14);
                    $mainData[4]  = array('data' => $dispNum_21);
                    $mainData[5]  = array('data' => $dispNum_28);
                    $mainData[6]  = array('data' => $dispNum_35);
                    $mainData[7]  = array('data' => $dispNum_42);
                    $mainData[8]  = array('data' => $dispNum_49);
                    $mainData[9]  = array(
                        'data' => sprintf(
                             '=%s+%s+%s+%s+%s+%s+%s'
                           , $this->coord($columnAlphaName[$startColumnNum+2], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+3], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+4], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+5], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+6], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+7], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+8], $rowNum)
                        ),
                    );
                    $mainData[10] = array('data' => $dispNum_53);
                    $mainData[11] = array('data' => sprintf('=%s+%s', $this->coord($columnAlphaName[$startColumnNum+9], $rowNum), $this->coord($columnAlphaName[$startColumnNum+10], $rowNum)));
                    $mainData[12] = array(
                        'data' => $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+11], $rowNum), $this->coord($columnAlphaName[$startColumnNum+1], $rowNum), 3),
                        'style' => array('msoNumberFormat' => $msoNumberFormat_parcent0_0)
                    );

                    # 入所
                    $mainData[13] = array('data' => $enterLimit);
                    $mainData[14] = array('data' => $dispNum_60);
                    $mainData[15] = array('data' => $dispNum_74);
                    $mainData[16] = array('data' => $dispNum_85);
                    $mainData[17] = array('data' => $dispNum_67);
                    $mainData[18] = array('data' => $dispNum_81);
                    $mainData[19] = array('data' => $dispNum_89);
                    $mainData[20] = array(
                        'data' => sprintf(
                             '=%s+%s+%s+%s+%s+%s'
                           , $this->coord($columnAlphaName[$startColumnNum+14], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+15], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+16], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+17], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+18], $rowNum)
                           , $this->coord($columnAlphaName[$startColumnNum+19], $rowNum)
                        )
                    );
                    $mainData[21] = array('data' => $dispNum_101);
                    $mainData[22] = array('data' => $dispNum_102);
                    $mainData[23] = array('data' => $dispNum_104);
                    $mainData[24] = array('data' => $dispNum_105);
                    $mainData[25] = array('data' => $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+13], $rowNum), $this->coord($columnAlphaName[$startColumnNum+20], $rowNum)));
                    $mainData[27] = array(
                        'data'  => $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+20], $rowNum), $this->coord($columnAlphaName[$startColumnNum+13], $rowNum), 3)
                      , 'style' => array('msoNumberFormat' => $msoNumberFormat_parcent0_0)
                    );

                    $mainData[26] = array('data' => $this->getExcelUtilizationRatesTrigona($columnAlphaName[$startColumnNum+27], $rowNum, 95));

                    # 訪問リハビリ
                    $mainData[28] = array('data' => $dispNum_96);
                    $mainData[29] = array('data' => $dispNum_100);
                    $mainData[30] = array('data' => $dispNum_96 + $dispNum_100);

                    # 判定件数
                    $mainData[31] = array('data' => $dispNum_106);
                    $mainData[32] = array('data' => $dispNum_107);

                    # 営業件数
                    $mainData[33] = array('data' => $dispNum_108);

                    for ($i=0; $i <= 33; $i++) {
                        switch ($i) {
                            case '0':
                                $totalData[$i] = array('data' => '合計', 'style' => array('textAlign' => $textAlign_center, 'bgColor' => $bgColor_blue));
                                break;

                            # 小計
                            case '9':
                                $totalData[$i]['data'] = sprintf(
                                     '=%s+%s+%s+%s+%s+%s+%s'
                                   , $this->coord($columnAlphaName[$startColumnNum+2], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+3], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+4], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+5], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+6], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+7], $totalRowNum)
                                   , $this->coord($columnAlphaName[$startColumnNum+8], $totalRowNum)
                                );
                                break;

                            # 合計
                            case '11':
                                $totalData[$i]['data'] = sprintf('=%s+%s', $this->coord($columnAlphaName[$startColumnNum+9], $totalRowNum), $this->coord($columnAlphaName[$startColumnNum+10], $totalRowNum));
                                break;

                            case '26':
                                break;

                            # 稼働率
                            case '12':
                            case '27':
                                $leftKey  = ($i==12)? 11 : 20;
                                $rightKey = ($i==12)?  1 : 13;
                                $totalData[$i]['data'] = $this->getExcelDivData($this->coord($columnAlphaName[$startColumnNum+$leftKey], $totalRowNum), $this->coord($columnAlphaName[$startColumnNum+$rightKey], $totalRowNum));
                                $totalData[$i]['style'] = array('msoNumberFormat' => $msoNumberFormat_parcent0_0);
                                if ($i == 27) { $totalData[$i-1]['data'] = $this->getExcelUtilizationRatesTrigona($columnAlphaName[$startColumnNum+$i], $totalRowNum, 95); }
                                break;

                            case '25':
                                $totalData[$i]['data'] = $this->getExcelSubData($this->coord($columnAlphaName[$startColumnNum+13], $totalRowNum), $this->coord($columnAlphaName[$startColumnNum+20], $totalRowNum));
                                break;

                            default:
                                $totalData[$i]['data'] = sprintf('=SUM(%s:%s)', $this->coord($columnAlphaName[$startColumnNum+$i], $startRowNum+1), $this->coord($columnAlphaName[$startColumnNum+$i], $totalRowNum-1));
                                break;
                        }
                    }

                    $rtnStr .= '<tr>'.$this->convertCellData($mainData , $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0)).'</tr>';
                }
            }

            $rtnStr .= '<tr>'.$this->convertCellData($totalData, $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0)).'</tr>';
        }

        $rtnStr .= '</table>';
        return $rtnStr;
    }

    /**
     * No.124 [老健] 老健月間加算項目・算定実績一覧のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputExcelCalculationResults($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr  = $this->getExcelMeta();
        $fontTag = $this->getFontTag();

        $facilityId  = $this->getArrValueByKey('facility_id', $optionArr);
        $targetYear  = $this->getArrValueByKey('year', $optionArr);

        # 特殊文字
        $roman1 = $this->sc('roman_1');
        $roman2 = $this->sc('roman_2');
        $roman3 = $this->sc('roman_3');

        /* @var $indicatorData IndicatorData */
        $indicatorData = $this->getIndicatorData();

        # style
        $bgColor_blue      = array('name' => 'background-color', 'value' => $this->getRgbColor('blue'));
        $bgColor_weekday   = array('name' => 'background-color', 'value' => $this->getRgbColor('weekday'));
        $bgColor_saturday  = array('name' => 'background-color', 'value' => $this->getRgbColor('saturday'));
        $bgColor_holiday   = array('name' => 'background-color', 'value' => $this->getRgbColor('holiday'));
        $textAlign_center  = array('name' => 'text-align',       'value' => 'center');
        $textAlign_right   = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left    = array('name' => 'text-align',       'value' => 'left');
        $whiteSpace_nowrap = array('name' => 'white-space',      'value' => 'nowrap');
        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');

        # 入所のヘッダー
        $enterHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.夜間職員配置加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位')),
             2 => array(0 => array('data' => '2.短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
             3 => array(0 => array('data' => '3.認知症短期集中ﾘﾊﾋﾞﾘﾃｰｼｮﾝ実施加算(週3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '240単位')),
             4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位')),
             5 => array(0 => array('data' => '5.若年性認知症入所者受入加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位')),
             6 => array(0 => array('data' => '6.ターミナル加算(1)死亡日以前15日以上30日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
             7 => array(0 => array('data' => '7.ターミナル加算(1)死亡日以前15日以上31日以下', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '315単位')),
             8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位')),
             9 => array(0 => array('data' => '9.初期加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位')),
            10 => array(0 => array('data' => '10.退所前後訪問指導加算(入所中1回又は2回、退所後1回を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '460単位')),
            11 => array(0 => array('data' => '11.退所時指導加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '400単位')),
            12 => array(0 => array('data' => '12.退所時情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            13 => array(0 => array('data' => '13.退所前連携加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            14 => array(0 => array('data' => '14.老人訪問看護指示加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '300単位')),
            15 => array(0 => array('data' => '15.栄養マネジメント加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '14単位')),
            16 => array(0 => array('data' => '16.経口移行加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位')),
            17 => array(0 => array('data' => '17.経口維持加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '28単位')),
            18 => array(0 => array('data' => '18.経口維持加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位')),
            19 => array(0 => array('data' => '19.口腔機能維持管理加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '30単位')),
            20 => array(0 => array('data' => '20.療養食加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位')),
            21 => array(0 => array('data' => '21.在宅復帰支援機能加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '15単位')),
            22 => array(0 => array('data' => '22.在宅復帰支援機能加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '5単位')),
            23 => array(0 => array('data' => '23.緊急時施設療養費(1)緊急時治療管理(1月に1回3日を限度)', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            24 => array(0 => array('data' => '24.認知症専門ケア加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '3単位')),
            25 => array(0 => array('data' => '25.認知症専門ケア加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '4単位')),
            26 => array(0 => array('data' => '26.認知症情報提供加算', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '350単位')),
            27 => array(0 => array('data' => '27.サービス提供体制強化加算('.$roman1.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位')),
            28 => array(0 => array('data' => '28.サービス提供体制強化加算('.$roman2.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
            29 => array(0 => array('data' => '29.サービス提供体制強化加算('.$roman3.')', 'style' => array('textAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
        );

        # ショートのヘッダー
        $shortHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.夜勤職員配置加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '24単位')),
             2 => array(0 => array('data' => '2.リハビリ機能強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '30単位')),
             3 => array(0 => array('data' => '3.個別リハビリテーション実施加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
             4 => array(0 => array('data' => '4.認知症ケア加算(ユニットは算定不可)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '76単位')),
             5 => array(0 => array('data' => '5.認知症行動・心理症状緊急対応加算(7日間を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
             6 => array(0 => array('data' => '6.若年性認知症入所者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '120単位')),
             7 => array(0 => array('data' => '7.送迎加算(片道につき)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '184単位')),
             8 => array(0 => array('data' => '8.療養体制維持特別加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '27単位')),
             9 => array(0 => array('data' => '9.療養食加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '23単位')),
            10 => array(0 => array('data' => '10.緊急短期入所ネットワーク加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位')),
            11 => array(0 => array('data' => '11.緊急時施設療養費(１)緊急時治療管理(1月に1回3日を限度)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '500単位')),
            12 => array(0 => array('data' => '12.サービス提供体制強化加算('.$roman1.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '12単位')),
            13 => array(0 => array('data' => '13.サービス提供体制強化加算('.$roman2.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
            14 => array(0 => array('data' => '14.サービス提供体制強化加算('.$roman3.')', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '6単位')),
        );

        # 通所のヘッダー
        $communteHeaderArr = array(
             0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
             1 => array(0 => array('data' => '1.連続介護加算(ｱ･8h-9h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '50単位')),
             2 => array(0 => array('data' => '2.連続介護加算(ｲ･9ｈ-10h)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '100単位')),
             3 => array(0 => array('data' => '3.入浴介助加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '50単位')),
             4 => array(0 => array('data' => '4.リハ計画作成加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '550単位')),
             5 => array(0 => array('data' => '5.リハビリテーションマネジメント加算（月1回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '230単位')),
             6 => array(0 => array('data' => '6.短期集中リハ実施加算（１月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '280単位')),
             7 => array(0 => array('data' => '7.短期集中リハ実施加算（１月超３月以内）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '140単位')),
             8 => array(0 => array('data' => '8.個別リハ実施加算（３月超）（月13回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '80単位')),
             9 => array(0 => array('data' => '9.認知症短期集中リハ実施加算（週2日）を限度', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '240単位')),
            10 => array(0 => array('data' => '10.若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '60単位')),
            11 => array(0 => array('data' => '11.栄養改善加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位')),
            12 => array(0 => array('data' => '12.口腔機能向上加算（月2回限度）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '150単位')),
            13 => array(0 => array('data' => '13.サービス提供体制加算（'.$roman1.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '12単位')),
            14 => array(0 => array('data' => '14.サービス提供体制加算（'.$roman2.'）', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        # 介護予防通所のヘッダー
        $communtePreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1．若年性認知症利用者受入加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '240単位')),
            2 => array(0 => array('data' => '2.運動機能加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '225単位')),
            3 => array(0 => array('data' => '3.栄養改善加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位')),
            4 => array(0 => array('data' => '4.口腔機能向上加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '150単位')),
            5 => array(0 => array('data' => '5.事業所評価加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '100単位')),
            6 => array(0 => array('data' => '6.サービス提供強化加算（1）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位')),
            7 => array(0 => array('data' => '7.サービス提供強化加算（1）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '96単位')),
            8 => array(0 => array('data' => '8.サービス提供強化加算（2）要支援１', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '24単位')),
            9 => array(0 => array('data' => '9.サービス提供強化加算（2）要支援２', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1月につき'), 2 => array('data' => '48単位')),
        );

        # 訪問リハのヘッダー
        $visitHeaderArr = array(
            0 => array(0 => array('data' => '加算項目', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(1月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '340単位')),
            2 => array(0 => array('data' => '2.短期集中リハビリテーション実施加算(1月超3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
            3 => array(0 => array('data' => '3.サービス提供強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        # 介護予防訪問リハのヘッダー
        $visitPreHeaderArr = array(
            0 => array(0 => array('data' => '加算項目'), 1 => array('data' => '&nbsp;'), 2 => array('data' => '単位数')),
            1 => array(0 => array('data' => '1.短期集中リハビリテーション実施加算(3月以内)', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1日につき'), 2 => array('data' => '200単位')),
            2 => array(0 => array('data' => '2.サービス提供強化加算', 'style' => array('tyextAlign' => $textAlign_left)), 1 => array('data' => '1回につき'), 2 => array('data' => '6単位')),
        );

        $plantCnt = count($plantArr);

        $enterMainArr       = array();
        $shortMainArr       = array();
        $communteMainArr    = array();
        $communtePreMainArr = array();
        $visitMainArr       = array();
        $visitPreMainArr    = array();

        $enterTotalArr       = array();
        $shortTotalArr       = array();
        $communteTotalArr    = array();
        $communtePreTotalArr = array();
        $visitTotalArr       = array();
        $visitPreTotalArr    = array();

        $totalStatus = 3;

        $startMonth = $this->getFiscalYearStartMonth();

        # エクセル座標算出用
        $enterStartRowNum       =  4 - 1;
        $shortStartRowNum       = 35 - 1;
        $communteStartRowNum    = 51 - 1;
        $communtePreStartRowNum = 67 - 1;
        $visitStartRowNum       = 78 - 1;
        $visitPreStartRowNum    = 83 - 1;
        $startColumnNum         = 4;
        $totalColumnNum         = $startColumnNum + 12 - 1;
        $columnAlphaName        = $this->columnAlphaNameArr;

        for ($m=0; $m<=11; $m++) {
            $month       = $startMonth + $m;
            $currentYear = ($month>=13)? $targetYear + 1 : $targetYear;
            $month       = ($month>=13)? $month-12 : $month;
            $monthName   = $month.'月';

            # 施設名
            $enterMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $shortMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communteMainArr[0][]    = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $communtePreMainArr[0][] = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitMainArr[0][]       = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
            $visitPreMainArr[0][]    = array('data' => $monthName, 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

            $status = $this->checkCellDataStatus('calculation_results', array('year' => $currentYear, 'month' => $month));

            # 初期化
            for ($i=109; $i<=179; $i++) { $dispNum[$i] = 0; }

            if (is_array($dataArr) && count($dataArr)!=0) {
                foreach ($dataArr as $dataVal) {
                    if ($facilityId==$this->getArrValueByKey('jnl_facility_id', $dataVal) && $currentYear == $this->getArrValueByKey('year', $dataVal) && sprintf('%02d', $month) == $this->getArrValueByKey('month', $dataVal)) {
                        for ($i=109; $i<=179; $i++) { $dispNum[$i] = $this->getArrValueByKey('dispnum_'.$i, $dataVal); }
                        $status = $this->checkCellDataStatus('calculation_results', $dataVal);
                    }
                }
            }

            for ($i=1; $i<=29; $i++) {
                $enterMainArr[$i][] = array('data' => $dispNum[108+$i]);
                $enterTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $enterStartRowNum+$i);

                if ($i<=14) {
                    $shortMainArr[$i][] = array('data' => $dispNum[137+$i]);
                    $shortTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $shortStartRowNum+$i);
                    $communteMainArr[$i][] = array('data' => $dispNum[151+$i]);
                    $communteTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $communteStartRowNum+$i);

                    if ($i<=9) {
                        $communtePreMainArr[$i][] = array('data' => $dispNum[165+$i]);
                        $communtePreTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $communtePreStartRowNum+$i);

                        if ($i<=3) {
                            $visitMainArr[$i][] = array('data' => $dispNum[174+$i]);
                            $visitTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $visitStartRowNum+$i);

                            if ($i<=2) {
                                $visitPreMainArr[$i][] = array('data' => $dispNum[177+$i]);
                                $visitPreTotalArr[$i][0]['data'] = $this->getExcelSideSumData($columnAlphaName[$startColumnNum], $columnAlphaName[$totalColumnNum], $visitPreStartRowNum+$i);
                            }
                        }
                    }
                }
            }
        }

        $enterMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $shortMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communteMainArr[0][12]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $communtePreMainArr[0][12] = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitMainArr[0][12]       = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));
        $visitPreMainArr[0][12]    = array('data' => '合計', 'style' => array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center));

        # 出力データ表示
        $rtnStr .= '<table width="100%" >'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data' => sprintf('%04d年度', $targetYear))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '入所', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=29; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($enterHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($enterMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($enterTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .= '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> 'ショート(介護・予防を含む)', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($shortHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($shortMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($shortTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=              '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '通所リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=14; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communteHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communteMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($communteTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '介護予防通所リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=9; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($communtePreHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($communtePreMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($communtePreTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '訪問リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=3; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($visitTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                      .'<tr>'
                          .$this->convertCellData(array(0 => array('data'=> '介護予防訪問リハビリテーション', 'style' => array($textAlign_left))), $fontTag, array())
                      .'</tr>'
                      .'<tr>'
                          .'<td>'
                              .'<table class="list" width="100%" border="1" >';
        for ($i=0; $i<=2; $i++) {
            $rtnStr .= '<tr>'
                          .$this->convertCellData($visitPreHeaderArr[$i], $fontTag, array('bgColor' => $bgColor_blue, 'textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($visitPreMainArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($visitPreTotalArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .'</tr>';
        }
        $rtnStr .=             '</table>'
                          .'</td>'
                      .'</tr>'
                  .'</table>';
        return $rtnStr;
    }

    /**
     * No.125 [老健] 療養費明細書のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputExcelMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr = $this->getExcelMeta();
        $fontArr = $this->getFont();
        $fontArr['color'] = 'red';

        $fontTag      = $this->getFontTag();
        $fontTag_red  = $this->getFontTag($fontArr);
        $facilityName = $plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_24 = array('name' => 'rowspan', 'value' => 24);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_10 = array('name' => 'colspan', 'value' => 10);

        # style
        $bgColor_gray        = array('name' => 'background-color', 'value' => '#c0c0c0');
        $bgColor_white       = array('name' => 'background-color', 'value' => '#ffffff');
        $bgColor_lightBlue   = array('name' => 'background-color', 'value' => '#b7dbff');
        $bgColor_lightRed    = array('name' => 'background-color', 'value' => '#ffb7b7');
        $bgColor_lightGreen  = array('name' => 'background-color', 'value' => '#a4ffa4');
        $bgColor_lightYellow = array('name' => 'background-color', 'value' => '#ffe8b7');
        $bgColor_lightOrange = array('name' => 'background-color', 'value' => '#ffc6a4');
        $bgColor_purple      = array('name' => 'background-color', 'value' => '#dbb7ff');
        $bgColor_blue        = array('name' => 'background-color', 'value' => '#75a9ff');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left      = array('name' => 'text-align',       'value' => 'left');
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');
        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');
		$msoNumberFormat_0_0     = $this->getMsoNumberFormat('decimal');
		
        # 以下、表示データ
        $rtnStr .= '<table width="100%" border="1" ><tr>'
                       .$this->convertCellData(
                           array(0 => array('data'  => '施設名：'.$facilityName)
                               , 1 => array('data'  => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array('textAlign' => $textAlign_right))
								,2 => array('data'  => '療養費明細書')
                           )
                         , $fontTag
                         , array()
                       )
                       .'</tr>'
                   .'</table>';

        ### header ###
        $firstTypeHeaderArr = array(
            3 => array('data' => '保険種別'),    4 => array('data' => '公費法別'),    5 => array('data' => '件数(人数)')
          , 6 => array('data' => '日数'),        7 => array('data' => '単位'),        8 => array('data' => '保険請求額')
          , 9 => array('data' => '公費請求額'), 10 => array('data' => '利用者負担'), 11 => array('data' => '費用合計額')
        );

        $firstHeaderArr = array(
             0 => array(0 => array('data' => '保険請求分', 'attr' => array($rowspan_2, $colspan_7), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '総請求額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow))
             )
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array($bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 26 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 28 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 29 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 32 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 33 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 34 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 35 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 38 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 39 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 40 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 41 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 44 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 45 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 47 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 50 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 51 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 52 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 53 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 56 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 57 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 58 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 59 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 62 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 63 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 64 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 65 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 68 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 69 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 70 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 71 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 74 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_24)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 75 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 76 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 77 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 80 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 81 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 82 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 83 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 86 => array_merge(array(0 => array('data' => '入所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 87 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 88 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 89 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 92 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 93 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 94 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 95 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $secondLeftHeaderArr = array(
             0 => array(0 => array('data' => '特定入所者介護サービス費等', 'attr' => array($rowspan_2, $colspan_10), 'style' => array('textAlign' => $textAlign_left)))
           , 2 => array(0 => array('data' => '保険請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
          , 11 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 12 => array(0 => array('data' => '食費'))
          , 13 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 14 => array(0 => array('data' => '食費'))
          , 15 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 16 => array(0 => array('data' => '食費'))
          , 17 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 18 => array(0 => array('data' => '食費'))
          , 19 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 20 => array(0 => array('data' => '食費'))
          , 21 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 22 => array(0 => array('data' => '食費'))
          , 23 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 24 => array(0 => array('data' => '食費'))
          , 25 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 26 => array(0 => array('data' => '食費'))
        );
        $secondRightHeaderArr = array(
             0 => array(1 => array('data' => '特定入所者分請求額', 'attr' => array($rowspan_2, $colspan_2), 'style' => array($bgColor_lightYellow)))
           , 2 => array(0 => array('data' => '公費請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
          , 11 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 12 => array(0 => array('data' => '食費'))
          , 13 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 14 => array(0 => array('data' => '食費'))
          , 15 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 16 => array(0 => array('data' => '食費'))
          , 17 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 18 => array(0 => array('data' => '食費'))
          , 19 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 20 => array(0 => array('data' => '食費'))
          , 21 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_6)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 22 => array(0 => array('data' => '食費'))
          , 23 => array(0 => array('data' => '入所', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 24 => array(0 => array('data' => '食費'))
          , 25 => array(0 => array('data' => '合計', 'attr' => array($rowspan_2)), 1 => array('data' => '居住費'))
          , 26 => array(0 => array('data' => '食費'))
        );

        $thirdHeaderArr = array(
            0 => array(0 => array('data' => '居宅介護支援(ケアプラン作成料)', 'attr' => array($colspan_4), 'style' => array($textAlign_left)))
          , 1 => array(0 => array('data' => '&nbsp;'), 1 => array('data' => '件数'), 2 => array('data' => '単位'), 3 => array('data' => '金額'))
          , 2 => array(0 => array('data' => '合計'))
          , 3 => array(0 => array('data' => '当月'))
          , 4 => array(0 => array('data' => '月遅れ'))
          , 5 => array(0 => array('data' => '返戻再請求'))
        );

        $fourthHeaderArr = array(
            0 => array(0 => array('data' => '負担限度額認定者比率(日数比率)', 'attr' => array($colspan_6), 'style' => array($textAlign_left)))
          , 1 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)), 1 => array('data' => '低所得者件数'), 2 => array('data' => '低所得者日数')
                     , 3 => array('data' => '入所者全日数'), 4 => array('data' => '比率'))
          , 2 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_3)), 1 => array('data' => '合計'))
          , 3 => array(0 => array('data' => '入所'))
          , 4 => array(0 => array('data' => 'ショート'))
        );

        $fifthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)), 1 => array('data' => '件数'), 2 => array('data' => '日数'), 3 => array('data' => '金額'))
          , 1 => array(0 => array('data' => '通所'), 1 => array('data' => '食費', 'attr' => array($colspan_3)))
          , 2 => array(0 => array('data' => 'ショート', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)', 'attr' => array($colspan_2)))
          , 3 => array(0 => array('data' => '滞在費(多床室)', 'attr' => array($colspan_2)))
          , 4 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)', 'attr' => array($colspan_2)))
          , 5 => array(0 => array('data' => '滞在費(低所得 者負担金あり 個室)', 'attr' => array($colspan_2)))
          , 6 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)', 'attr' => array($colspan_2)))
          , 7 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)', 'attr' => array($colspan_2)))
          , 8 => array(0 => array('data' => '食費(朝)', 'attr' => array($colspan_2)))
          , 9 => array(0 => array('data' => '食費(昼)', 'attr' => array($colspan_2)))
         , 10 => array(0 => array('data' => '食費(夕)', 'attr' => array($colspan_2)))
         , 11 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 12 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 13 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 14 => array(0 => array('data' => '合計', 'attr' => array($colspan_3)))
         , 15 => array(0 => array('data' => '入所', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)', 'attr' => array($colspan_2)))
         , 16 => array(0 => array('data' => '滞在費(多床室)', 'attr' => array($colspan_2)))
         , 17 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)', 'attr' => array($colspan_2)))
         , 18 => array(0 => array('data' => '滞在費(低所得者負担金あり 個室)', 'attr' => array($colspan_2)))
         , 19 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)', 'attr' => array($colspan_2)))
         , 20 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)', 'attr' => array($colspan_2)))
         , 21 => array(0 => array('data' => '食費(朝)', 'attr' => array($colspan_2)))
         , 22 => array(0 => array('data' => '食費(昼)', 'attr' => array($colspan_2)))
         , 23 => array(0 => array('data' => '食費(夕)', 'attr' => array($colspan_2)))
         , 24 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 25 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 26 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_3)))
         , 28 => array(0 => array('data' => '特別な室料', 'attr' => array($colspan_4)))
         , 29 => array(0 => array('data' => 'その他の自費', 'attr' => array($colspan_4)))
         , 30 => array(0 => array('data' => '総合計', 'attr' => array($colspan_4)))
        );

        $sixthHeaderArr = array(
             0 => array(0 => array('data' => '訪問リハビリ', 'attr' => array($rowspan_2, $colspan_6), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '保険分請求総額', 'attr' => array($rowspan_2, $colspan_2), 'style' => array($bgColor_lightYellow))
             )
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $seventhHeaderArr = array(0 => array('data' => '請求額'));

        ### data ###
        $dispData = array();
        for ($i=1; $i<=570; $i++) {
            $dispData['orange_'.$i] = null;
            if ($i<=359) {
                $dispData['red_'.$i] = null;
                if ($i<=80) { $dispData['green_'.$i] = null; }
            }
        }

        # ここでデータを配列に･･･
        if (is_array($dataArr) && count($dataArr)>0 ) { foreach ($dataArr[0] as $key => $val) { $dispData[$key] = $val; } }

        $firstOrangeNumArr = array(
              3 =>   1,   9 =>  32,  15 =>  63,  27 =>  94,  33 => 125,  39 => 156,  51 => 187,  57 => 218,  63 => 249
        );
        $firstRedNumArr    = array(
              0 =>   1,   1 =>   2,  21 =>   3,  45 =>  34,  69 =>  65,  75 =>  96,  81 => 127,  87 => 158,  93 => 189
        );
        $firstGreenNumArr  = array(
              3 =>   1,   9 =>   5,  15 =>   9,  21 =>  13,  27 =>  17,  33 =>  21,  39 =>  25,  45 =>  29,  51 =>  33
          ,  57 =>  37,  63 =>  41,  69 =>  45,  75 =>  49,  81 =>  53,  87 =>  57,  93 =>  61
        );

        $colAlphaName = $this->columnAlphaNameArr;

        $firstMainDataArr = array();
        $firstStartRowNum = 4;
        $firstStartColNum = 5;

        for ($i = 0; $i <= 97; $i++) {
            $oNum = null;
            $rNum = null;
            $gNum = null;

            switch ($i) {
                case '0':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[8], 95), $this->coord($colAlphaName[9], 95), $this->coord($colAlphaName[13], 101)), 'attr' => array($colspan_2)));
                    break;

                case '1':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => sprintf('=SUM(%s, %s)', $this->coord($colAlphaName[8], 95), $this->coord($colAlphaName[9], 95)), 'attr' => array($colspan_2)));
                    break;

                case  '3': case  '9': case '15': case '27': case '33': case '39'; case '51': case '57': case '63':
                    $oNum = $firstOrangeNumArr[$i];
                    $gNum = $firstGreenNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $firstMainDataArr[$i][$l]['data'] = $dispData['orange_'.($oNum + $l)];
                            $firstCoord[$i][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+2);
                        }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+ 7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+13)];
                            $firstCoord[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+3);
                            $firstCoord[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+4);
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+12)];
                            $firstCoord[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+3);
                            $firstCoord[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+4);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+23)];
                            $firstCoord[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+5);
                            $firstCoord[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+6);
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+22)];
                            $firstCoord[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+5);
                            $firstCoord[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+6);
                        }
                    }
                    break;

                case '21': case '45': case '69': case '93':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-18][$l], $firstCoord[$i-12][$l], $firstCoord[$i-6][$l]); }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-17][$l], $firstCoord[$i-11][$l], $firstCoord[$i-5][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-16][$l], $firstCoord[$i-10][$l], $firstCoord[$i-4][$l]);
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-17][$l], $firstCoord[$i-11][$l], $firstCoord[$i-5][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-16][$l], $firstCoord[$i-10][$l], $firstCoord[$i-4][$l]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-15][$l], $firstCoord[$i-9][$l], $firstCoord[$i-3][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-14][$l], $firstCoord[$i-8][$l], $firstCoord[$i-2][$l]);
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-15][$l], $firstCoord[$i-9][$l], $firstCoord[$i-3][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-14][$l], $firstCoord[$i-8][$l], $firstCoord[$i-2][$l]);
                        }
                    }
                    break;

                # 合計
                case '75': case '81': case '87':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $firstMainDataArr[$i][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-72][$l], $firstCoord[$i-48][$l], $firstCoord[$i-24][$l]);
                            $firstCoord[$i][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+2);
                        }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-71][$l], $firstCoord[$i-47][$l], $firstCoord[$i-23][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-70][$l], $firstCoord[$i-46][$l], $firstCoord[$i-22][$l]);
                            $firstCoord[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+3);
                            $firstCoord[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+4);
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-71][$l], $firstCoord[$i-47][$l], $firstCoord[$i-23][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-70][$l], $firstCoord[$i-46][$l], $firstCoord[$i-22][$l]);
                            $firstCoord[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+3);
                            $firstCoord[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $i+4);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)], 'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-69][$l], $firstCoord[$i-45][$l], $firstCoord[$i-21][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-68][$l], $firstCoord[$i-44][$l], $firstCoord[$i-20][$l]);
                            $firstCoord[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+5);
                            $firstCoord[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+6);
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-69][$l], $firstCoord[$i-45][$l], $firstCoord[$i-21][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s, %s)', $firstCoord[$i-68][$l], $firstCoord[$i-44][$l], $firstCoord[$i-20][$l]);
                            $firstCoord[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+5);
                            $firstCoord[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $i+6);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $secondRightOrangeArr = array(3 => 281, 9 => 297, 15 => 313);
        $secondLeftOrangeArr  = array(3 => 329, 9 => 345, 15 => 361);
        $secondRightRedArr    = array(3 => 221, 9 => 229, 15 => 237, 21 => 245);
        $secondLeftRedArr     = array(3 => 269, 9 => 277, 15 => 285, 21 => 293);
        $secondStartRowNum    = $firstStartRowNum + 97;
        $secondStartColNum    = 4;
        $secondRightCoordArr  = array();
        $secondLeftCoordArr   = array();

        for ($i=0; $i<=26; $i++) {
            $orNum = null;
            $olNum = null;
            $rrNum = null;
            $rlNum = null;

            switch ($i) {
                case '0':
                    $secondRightMainData[$i][0] = array('data' => sprintf('=SUM(%s, %s, %s, %s)', $this->coord($colAlphaName[7], 126), $this->coord($colAlphaName[7], 127), $this->coord($colAlphaName[14], 126), $this->coord($colAlphaName[14], 127)), 'attr' => array($rowspan_2, $colspan_2));
                    break;

                case '3': case '9': case '15':
                    $orNum = $secondRightOrangeArr[$i];
                    $olNum = $secondLeftOrangeArr[$i];
                    $rrNum = $secondRightRedArr[$i];
                    $rlNum = $secondLeftRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        $secondLeftMainData[$i][$l]   = array('data' => $dispData['orange_'.($orNum+$l)]);
                        $secondLeftMainData[$i+1][$l] = array('data' => $dispData['orange_'.($orNum+$l+4)]);
                        $secondLeftMainData[$i+2][$l] = array('data' => $dispData['orange_'.($orNum+$l+8)]);
                        $secondLeftMainData[$i+3][$l] = array('data' => $dispData['orange_'.($orNum+$l+12)]);
                        $secondLeftMainData[$i+4][$l] = array('data' => sprintf('=SUM(%s,%s)', $this->coord($colAlphaName[$secondStartColNum+$l], $secondStartRowNum+$i),   $this->coord($colAlphaName[$secondStartColNum+$l], $secondStartRowNum+$i+2)));
                        $secondLeftMainData[$i+5][$l] = array('data' => sprintf('=SUM(%s,%s)', $this->coord($colAlphaName[$secondStartColNum+$l], $secondStartRowNum+$i+1), $this->coord($colAlphaName[$secondStartColNum+$l], $secondStartRowNum+$i+3)));

                        $secondRightMainData[$i][$l]   = array('data' => $dispData['orange_'.($olNum+$l)]);
                        $secondRightMainData[$i+1][$l] = array('data' => $dispData['orange_'.($olNum+$l+4)]);
                        $secondRightMainData[$i+2][$l] = array('data' => $dispData['orange_'.($olNum+$l+8)]);
                        $secondRightMainData[$i+3][$l] = array('data' => $dispData['orange_'.($olNum+$l+12)]);
                        $secondRightMainData[$i+4][$l] = array('data' => sprintf('=SUM(%s,%s)', $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i),   $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+2)));
                        $secondRightMainData[$i+5][$l] = array('data' => sprintf('=SUM(%s,%s)', $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+1), $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+3)));

                        $secondLeftCoordArr[$i][$l]    = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i);
                        $secondLeftCoordArr[$i+1][$l]  = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i+1);
                        $secondLeftCoordArr[$i+2][$l]  = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i+2);
                        $secondLeftCoordArr[$i+3][$l]  = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i+3);
                        $secondLeftCoordArr[$i+4][$l]  = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i+4);
                        $secondLeftCoordArr[$i+5][$l]  = $this->coord($colAlphaName[$secondStartColNum+$l],   $secondStartRowNum+$i+5);
                        $secondRightCoordArr[$i][$l]   = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i);
                        $secondRightCoordArr[$i+1][$l] = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+1);
                        $secondRightCoordArr[$i+2][$l] = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+2);
                        $secondRightCoordArr[$i+3][$l] = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+3);
                        $secondRightCoordArr[$i+4][$l] = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+4);
                        $secondRightCoordArr[$i+5][$l] = $this->coord($colAlphaName[$secondStartColNum+$l+7], $secondStartRowNum+$i+5);
                    }
                    break;

                case '21':
                    $orNum = $secondRightOrangeArr[$i];
                    $olNum = $secondLeftOrangeArr[$i];
                    $rrNum = $secondRightRedArr[$i];
                    $rlNum = $secondLeftRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        for ($r=0; $r<=5; $r++) {
                            $secondLeftMainData[$i+$r][$l]  = array('data' => sprintf('=SUM(%s,%s,%s)', $secondLeftCoordArr[$i-18+$r][$l],  $secondLeftCoordArr[$i-12+$r][$l],  $secondLeftCoordArr[$i-6+$r][$l]));
                            $secondRightMainData[$i+$r][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $secondRightCoordArr[$i-18+$r][$l], $secondRightCoordArr[$i-12+$r][$l], $secondRightCoordArr[$i-6+$r][$l]));
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $thirdRedArr      = array(1 => 317);
        $thirdOrangeArr   = array(3 => 377, 4 => 380, 5 => 383);
        $thirdStartRowNum = $secondStartRowNum + 29;
        $thirdStartColNum = 2;


        for ($i=2; $i<=5; $i++) {
            $oNum = $thirdOrangeArr[$i];
            if ($i==2) {
                for ($l=0; $l<=2; $l++) {
                   $thirdMainDataArr[$i][$l] = array(
                       'data' => sprintf('=SUM(%s,%s,%s)', $this->coord($colAlphaName[$thirdStartColNum+$l], $thirdStartRowNum+2), $this->coord($colAlphaName[$thirdStartColNum+$l], $thirdStartRowNum+3), $this->coord($colAlphaName[$thirdStartColNum+$l], $thirdStartRowNum+4))
                   );
                }
            }
            else { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); } }
        }

        $fourthOrangeArr   = array(2 => 386, 3 => 389, 4 => 392);
        $foruthStartRowNum = $thirdStartRowNum + 6;
        $foruthStartColNum = 3;

        for ($i=2; $i<=4; $i++) {
            $oNum = $fourthOrangeArr[$i];
            $rNum = $fourthRedArr[$i];
            for ($l=0; $l<=3; $l++) {
                if ($l!=3) { $fourthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                else {
                    $tmpLeft  = $this->coord($colAlphaName[$foruthStartColNum+1], $foruthStartRowNum+$i);
                    $tmpRight = $this->coord($colAlphaName[$foruthStartColNum+2], $foruthStartRowNum+$i);
                    $fourthMainDataArr[$i][$l] = array('data' => sprintf('=IF(%s=0, 0, ROUND(%s/%s, 1))', $tmpRight, $tmpLeft, $tmpRight));
                }
            }
        }

        $fifthOrangeArr = array(
             1 => 395,  2 => 398,  3 => 401,  4 => 404,  5 => 407,  6 => 410,  7 => 413,  8 => 416,  9 => 419, 10 => 422
          , 11 => 425, 12 => 428, 13 => 431, 15 => 434, 16 => 437, 17 => 440, 18 => 443, 19 => 446, 20 => 449, 21 => 452
          , 22 => 455, 23 => 458, 24 => 461, 25 => 464, 26 => 467, 28 => 470, 29 => 471
        );
        $fifthRedArr    = array(14 => 323, 27 => 324, 30 => 325);

        $fifthStartRowNum = $foruthStartRowNum + 6;
        $fifthStartColNum = 7;

        for ($i=1; $i<=30; $i++) {
            $oNum = $fifthOrangeArr[$i];
            $rNum = $fifthRedArr[$i];
            switch ($i) {
                default:
                    for ($l=0; $l<=2; $l++) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                    break;

                case '14':
                    for ($l=0; $l<=2; $l++) {
                        if ($l==0) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($colspan_2), 'style' => array($bgColor_white)); }
                        else if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+3), $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+14))); }
                    }
                    break;

                case '27':
                    for ($l=0; $l<=2; $l++) {
                        if ($i==27 && $l==0) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4, $colspan_2), 'style' => array($bgColor_white)); }
                        else if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+16), $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+27))); }
                    }
                    break;

                case '28': case '29':
                    for ($l=0; $l<=2; $l++) { if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum)]); } }
                    break;

                case '30':
                    for ($l=0; $l<=2; $l++) {
                        if ($i==27 && $l==0) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4, $colspan_2), 'style' => array($bgColor_white)); }
                        else if ($l==2) {
                            $fifthMainDataArr[$i][$l] = array(
                                'data' => sprintf('=%s+%s+SUM(%s:%s)', $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+2), $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+15), $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+28), $this->coord($colAlphaName[$fifthStartColNum], $fifthStartRowNum+30))
                            );
                        }
                    }
                    break;
            }
        }

        $sixthOrangeArr   = array( 3 => 472, 9 => 505, 15 => 538);
        $sixthGreenArr    = array( 3 =>  65, 9 =>  69, 15 =>  73,  21 => 77);
        $sixthRedArr      = array(21 => 327);
        $sixthStartRowNum = $fifthStartRowNum+33;
        $sixthStartColNum = 4;

        for ($i=0; $i<=25; $i++) {
            $oNum = null;
            $gNum = null;

            switch ($i) {
                case '0':
                    $sixthMainDataArr[$i] = array(0 => array('data' => sprintf('=SUM(%s, %s)', $this->coord($colAlphaName[7], $sixthStartRowNum+21), $this->coord($colAlphaName[8], $sixthStartRowNum+21)), 'attr' => array($rowspan_2, $colspan_2), 'style' => array($textAlign_right)));
                    break;

                case '3': case '9': case '15':
                    $oNum = $sixthOrangeArr[$i];
                    $gNum = $sixthGreenArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l]   = array('data' => $dispData['orange_'.($oNum+$l)]); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)]);
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['orange_'.($oNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['orange_'.($oNum+$l+24)]);
                        }
                    }
                    break;

                case '21':
                    $gNum = $sixthGreenArr[$i];
                    $rNum = $sixthRedArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $sixthMainDataArr[$i][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-18), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-12), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-6))
                            );
                        }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-17), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-11), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-5))
                            );
                            $sixthMainDataArr[$i+2][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-16), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-10), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-4))
                            );
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-17), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-11), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-5))
                            );
                            $sixthMainDataArr[$i+2][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-16), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-10), $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i-4))
                            );
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)]);
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-15), $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-9), $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-3))
                            );
                            $sixthMainDataArr[$i+4][$l] = array(
                                'data' => sprintf('=SUM(%s, %s, %s)', $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-14), $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-8), $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i-2))
                            );
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $sevenMainDataArr = array(0 => array('data' => $dispData['orange_280']));

        ### first table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i <= 97; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 19) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 25) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i <= 43) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 49) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i <= 67) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 73) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i <= 91) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 97) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
            else {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### second table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i <= 26; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                           .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                           .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                           .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 2) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightOrange))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 6) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 8) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i <= 12) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 14) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i <= 18) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 20) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i <= 24) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 26) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
            else {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
        }
        $rtnStr .= '</table><br>';

        ### third table ###
        $rtnStr .=             '<table class="list" border="1" >';
        for ($i=0; $i <= 5; $i++) {
            switch ($i) {
                case '0':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '1':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '2':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '3':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '4':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '5':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                              .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;
            }
        }
        $rtnStr .=             '</table><br />';

        ### fourth table ###
        $rtnStr .=             '<table class="list" border="1" >';
        for ($i=0; $i <= 4; $i++) {
			
/*			
            if ($i==0) {
                $rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
			elseif($i==4) {
				$rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
					.$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_0_0)).'</tr>';
            }
            else {
                $rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                          .$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
*/
			

			if ($i==0) {
				$rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
					.$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
					
			}
			elseif($i==1) {
                $rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                          .$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
			else 
			{
				$rtnStr .= '<tr>';
				$rtnStr .= $this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple));
				
				for ($j=0; $j <= 3; $j++) 
				{
					
					if($j==3)
					{
						$rtnStr .= "<td  style=\" text-align:right; white-space:nowrap;  mso-number-format:\\#\\,\\#\\#0\\.0;\" >".$fourthMainDataArr[$i][$j]["data"]."</td>";
					}
					else
					{
						$rtnStr .= "<td  style=\" text-align:right;  mso-number-format:\#\,\#\#0;\" >".$fourthMainDataArr[$i][$j]["data"]."</td>";
					}
				}
				$rtnStr .= '</tr>';
				
			}
			

			

        }
        $rtnStr .= '</table><br />';

        ### fifth table ###
        $rtnStr .= '<table class="list" border="1" >'
                      .'<tr><td colspan="7">'.sprintf($fontTag, '自己負担分明細書').'</td></tr>';
        for ($i=0; $i <=30; $i++) {
            $rtnStr .= '<tr>'.$this->convertCellData($fifthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($fifthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
        }
        $rtnStr .= '</table><br />';

        ### sixth table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i < 26; $i++) {
            if ($i <= 1) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 7) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 13) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 19) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i <= 25) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### seventh table ###
        $rtnStr .= '<table border="1" >'
                      .'<tr>'.$this->convertCellData(array(0 => array('data' => '介護職員処遇改善交付金', 'attr' => array($colspan_2))), $fontTag_red, array($whiteSpace_nowrap)).'</tr>'
                      .'<tr>'.$this->convertCellData($seventhHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple, 'msoNumberFormat' => $msoNumberFormat_comma))
                      .$this->convertCellData($sevenMainDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap)).'</tr>'
                  .'</table>';

        return $rtnStr;
    }

    /**
     * No.126 [老健] 予防給付療養費明細書のエクセル出力
     * @param  array  $dataArr
     * @param  array  $plantArr
     * @param  array  $arr
     * @param  array  $optionArr
     * @return string $rtnStr
     */
    function outputExcelPreventiveSupplyMedicalExpenseDetail($dataArr, $plantArr, $arr, $optionArr)
    {
        $rtnStr       = $this->getExcelMeta();
        $fontTag      = $this->getFontTag();
        $facilityName = $plantArr[0]['facility_name'];
        $displayYear  = $optionArr['year'];
        $displayMonth = $optionArr['month'];

        # attr
        $rowspan_2  = array('name' => 'rowspan', 'value' =>  2);
        $rowspan_3  = array('name' => 'rowspan', 'value' =>  3);
        $rowspan_4  = array('name' => 'rowspan', 'value' =>  4);
        $rowspan_5  = array('name' => 'rowspan', 'value' =>  5);
        $rowspan_6  = array('name' => 'rowspan', 'value' =>  6);
        $rowspan_13 = array('name' => 'rowspan', 'value' => 13);
        $rowspan_18 = array('name' => 'rowspan', 'value' => 18);
        $rowspan_24 = array('name' => 'rowspan', 'value' => 24);
        $colspan_2  = array('name' => 'colspan', 'value' =>  2);
        $colspan_3  = array('name' => 'colspan', 'value' =>  3);
        $colspan_4  = array('name' => 'colspan', 'value' =>  4);
        $colspan_6  = array('name' => 'colspan', 'value' =>  6);
        $colspan_7  = array('name' => 'colspan', 'value' =>  7);
        $colspan_10 = array('name' => 'colspan', 'value' => 10);

        # style
        $bgColor_gray        = array('name' => 'background-color', 'value' => '#c0c0c0');
        $bgColor_white       = array('name' => 'background-color', 'value' => '#ffffff');
        $bgColor_lightBlue   = array('name' => 'background-color', 'value' => '#b7dbff');
        $bgColor_lightRed    = array('name' => 'background-color', 'value' => '#ffb7b7');
        $bgColor_lightGreen  = array('name' => 'background-color', 'value' => '#a4ffa4');
        $bgColor_lightYellow = array('name' => 'background-color', 'value' => '#ffe8b7');
        $bgColor_lightOrange = array('name' => 'background-color', 'value' => '#ffc6a4');
        $bgColor_purple      = array('name' => 'background-color', 'value' => '#dbb7ff');
        $bgColor_blue        = array('name' => 'background-color', 'value' => '#75a9ff');
        $textAlign_right     = array('name' => 'text-align',       'value' => 'right');
        $textAlign_left      = array('name' => 'text-align',       'value' => 'left');
        $textAlign_center    = array('name' => 'text-align',       'value' => 'center');
        $whiteSpace_nowrap   = array('name' => 'white-space',      'value' => 'nowrap');

        $msoNumberFormat_comma = $this->getMsoNumberFormat('comma');

        # 以下、表示データ
        $rtnStr .= '<table width="100%" border="1" ><tr>'
                       .$this->convertCellData(
                           array(0 => array('data'  => '施設名：'.$facilityName)
                               , 1 => array('data'  => sprintf('%04d年%02d月', $displayYear, $displayMonth), 'style' => array('textAlign' => $textAlign_right))
								,2 => array('data'  => '予防給付療養費明細書')
							)
                         , $fontTag
                         , array()
                       )
                       .'</tr>'
                   .'</table>';

        ### header ###
        $firstTypeHeaderArr = array(
            3 => array('data' => '保険種別'),    4 => array('data' => '公費法別'),    5 => array('data' => '件数(人数)')
          , 6 => array('data' => '日数'),        7 => array('data' => '単位'),        8 => array('data' => '保険請求額')
          , 9 => array('data' => '公費請求額'), 10 => array('data' => '利用者負担'), 11 => array('data' => '費用合計額')
        );

        $firstHeaderArr = array(
             0 => array(0 => array('data' => '保険請求分', 'attr' => array($rowspan_2, $colspan_7), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '総請求額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow))
             )
           , 1 => array(0 => array('data' => '保険分請求総額', 'attr' => array($colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow)))
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11=> array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 26 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 27 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 28 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 29 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 32 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 33 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 34 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 35 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 38 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 39 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 40 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 41 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 44 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 45 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 46 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 47 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 50 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 51 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 52 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 53 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 56 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_18)), 1 => array('data' => '通所', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 57 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 58 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 59 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 62 => array_merge(array(0 => array('data' => 'ショート', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 63 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 64 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 65 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 68 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 69 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 70 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 71 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $secondLeftHeaderArr = array(
             0 => array(0 => array('data' => '特定入所者介護サービス費等', 'attr' => array($rowspan_2, $colspan_10), 'style' => array('textAlign' => $textAlign_left)))
           , 2 => array(0 => array('data' => '保険請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_2)), 1 => array('data' => '合計',     'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
        );
        $secondRightHeaderArr = array(
             0 => array(1 => array('data' => '特定入所者分請求額', 'attr' => array($rowspan_2, $colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow)))
           , 2 => array(0 => array('data' => '公費請求分', 'attr' => array($colspan_3)), 1 => array('data' => '件数'), 2 => array('data' => '日数')
                      , 3 => array('data' => '費用合計'), 4 => array('data' => '保険請求')
           )
           , 3 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 4 => array(0 => array('data' => '食費'))
           , 5 => array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 6 => array(0 => array('data' => '食費'))
           , 7 => array(0 => array('data' => '再請求', 'attr' => array($rowspan_2)), 1 => array('data' => 'ショート', 'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
           , 8 => array(0 => array('data' => '食費'))
           , 9 => array(0 => array('data' => '総請求', 'attr' => array($rowspan_2)), 1 => array('data' => '合計',     'attr' => array($rowspan_2)), 2 => array('data' => '居住費'))
          , 10 => array(0 => array('data' => '食費'))
        );

        $thirdHeaderArr = array(
            0 => array(0 => array('data' => '居宅介護支援(ケアプラン作成料)', 'attr' => array($colspan_4)))
          , 1 => array(0 => array('data' => '&nbsp;'), 1 => array('data' => '件数'), 2 => array('data' => '単位'), 3 => array('data' => '金額'))
          , 2 => array(0 => array('data' => '合計'))
          , 3 => array(0 => array('data' => '当月'))
          , 4 => array(0 => array('data' => '月遅れ'))
          , 5 => array(0 => array('data' => '返戻再請求'))
        );

        $fourthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_2)), 1 => array('data' => '低所得者件数'), 2 => array('data' => '低所得者日数')
                     , 3 => array('data' => '入所者全日数'), 4 => array('data' => '比率'))
          , 1 => array(0 => array('data' => '当月分', 'attr' => array($rowspan_2)), 1 => array('data' => '合計'))
          , 2 => array(0 => array('data' => 'ショート'))
        );

        $fifthHeaderArr = array(
            0 => array(0 => array('data' => '&nbsp;', 'attr' => array($colspan_4)), 1 => array('data' => '件数'), 2 => array('data' => '日数'), 3 => array('data' => '金額'))
          , 1 => array(0 => array('data' => '通所'), 1 => array('data' => '食費', 'attr' => array($colspan_3)))
          , 2 => array(0 => array('data' => 'ショート', 'attr' => array($rowspan_13)), 1 => array('data' => '4段階', 'attr' => array($rowspan_2)), 2 => array('data' => '滞在費(個室室)', 'attr' => array($colspan_2)))
          , 3 => array(0 => array('data' => '滞在費(多床室)', 'attr' => array($colspan_2)))
          , 4 => array(0 => array('data' => '2･3段階', 'attr' => array($rowspan_2)), 1 => array('data' => '滞在費(低所得者負担金あり 多床室)', 'attr' => array($colspan_2)))
          , 5 => array(0 => array('data' => '滞在費(低所得 者負担金あり 個室)', 'attr' => array($colspan_2)))
          , 6 => array(0 => array('data' => '1段階'), 1 => array('data' => '滞在費(低所得者負担金なし)', 'attr' => array($colspan_2)))
          , 7 => array(0 => array('data' => '4段階', 'attr' => array($rowspan_4)), 1 => array('data' => '食費(1日)', 'attr' => array($colspan_2)))
          , 8 => array(0 => array('data' => '食費(朝)', 'attr' => array($colspan_2)))
          , 9 => array(0 => array('data' => '食費(昼)', 'attr' => array($colspan_2)))
         , 10 => array(0 => array('data' => '食費(夕)', 'attr' => array($colspan_2)))
         , 11 => array(0 => array('data' => '3段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 12 => array(0 => array('data' => '2段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 13 => array(0 => array('data' => '1段階'), 1 => array('data' => '食費', 'attr' => array($colspan_2)))
         , 14 => array(0 => array('data' => '合計', 'attr' => array($colspan_3)))
         , 15 => array(0 => array('data' => '特別な室料', 'attr' => array($colspan_4)))
         , 16 => array(0 => array('data' => 'その他の自費', 'attr' => array($colspan_4)))
         , 17 => array(0 => array('data' => '総合計', 'attr' => array($colspan_4)))
        );

        $sixthHeaderArr = array(
             0 => array(0 => array('data' => '訪問リハビリ', 'attr' => array($rowspan_2, $colspan_6), 'style' => array('textAlign' => $textAlign_left))
                      , 1 => array('data' => '保険分請求総額', 'attr' => array($rowspan_2, $colspan_2), 'style' => array('bgColor' => $bgColor_lightYellow))
           )
           , 2 => array_merge(array(0 => array('data' => '当月分', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 3 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
           , 4 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 5 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
           , 8 => array_merge(array(0 => array('data' => '月遅れ', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
           , 9 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 10 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 11 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 14 => array_merge(array(0 => array('data' => '再請求', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 15 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 16 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 17 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 20 => array_merge(array(0 => array('data' => '合計', 'attr' => array($rowspan_6))), $firstTypeHeaderArr)
          , 21 => array(0 => array('data' => '合計', 'attr' => array($colspan_2)))
          , 22 => array(0 => array('data' => 12, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
          , 23 => array(0 => array('data' => 49, 'style' => array($textAlign_right)), 1 => array('data' => 12, 'style' => array($textAlign_right)))
        );

        $seventhHeaderArr = array(0 => array('data' => '請求額'));

        ### data ###
        $dispData = array();
        for($i=81; $i<=144; $i++) { $dispData['green_'.$i] = null; }
        for ($i=360; $i<=960; $i++) {
            if ($i<=605) { $dispData['red_'.$i] = null; }
            if ($i>=571) { $dispData['orange_'.$i] = null; }
        }

        # ここでデータを配列に･･･
        if (is_array($dataArr) && count($dataArr) > 0 ) { foreach ($dataArr[0] as $key => $val) { $dispData[$key] = $val; } }

        $colAlphaName = $this->columnAlphaNameArr;

        $firstMainDataArr  = array();
        $firstOrangeNumArr = array(3 => 571,   9 => 602,  21 => 633,  27 => 664,  39 => 695,  45 => 726);
        $firstRedNumArr    = array(0 => 360,   1 => 361,  15 => 362,  33 => 393,  51 => 424,  57 => 455,  63 => 486,  69 => 517);
        $firstGreenNumArr  = array(3 =>  81,   9 =>  85,  15 =>  89,  21 =>  93,  27 =>  97,  33 => 101,  39 => 105,  45 => 109,  51 => 113,  57 => 117,  63 => 121,  69 => 125);
        $firstStartRowNum  = 2;
        $firstStartColNum  = 5;
        $firstCoordArr     = array();

        for ($i = 0; $i <= 73; $i++) {
            $oNum = null;
            $rNum = null;
            $gNum = null;

            switch ($i) {
                case '0':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => sprintf('=SUM(%s,%s,%s)', $this->coord($colAlphaName[8], 71), $this->coord($colAlphaName[9], 71), $this->coord($colAlphaName[13], 77)), 'attr' => array($colspan_2)));
                    break;

                case '1':
                    $rNum = $firstRedNumArr[$i];
                    $firstMainDataArr[$i] = array(0 => array('data' => sprintf('=SUM(%s,%s)', $this->coord($colAlphaName[8], 71), $this->coord($colAlphaName[9], 71)), 'attr' => array($colspan_2)));
                    break;

                case '3': case '9': case '21': case '27': case '39': case '45';
                    $oNum = $firstOrangeNumArr[$i];
                    $gNum = $firstGreenNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $firstCoordArr[$i][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i);
                            $firstMainDataArr[$i][$l]['data'] = $dispData['orange_'.($oNum + $l)];
                        }
                        # 2, 3
                        if ($l<=2) {
                            $firstCoordArr[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+1);
                            $firstCoordArr[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+2);
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+ 7)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+13)];
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstCoordArr[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+1);
                            $firstCoordArr[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+2);
                            $firstMainDataArr[$i+1][$l]['data'] = $dispData['orange_'.($oNum+$l+6)];
                            $firstMainDataArr[$i+2][$l]['data'] = $dispData['orange_'.($oNum+$l+12)];
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstCoordArr[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+3);
                            $firstCoordArr[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+4);
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+17)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+23)];                        }
                        else if ($l==5) {}
                        else {
                            $firstCoordArr[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+3);
                            $firstCoordArr[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+4);
                            $firstMainDataArr[$i+3][$l]['data'] = $dispData['orange_'.($oNum+$l+16)];
                            $firstMainDataArr[$i+4][$l]['data'] = $dispData['orange_'.($oNum+$l+22)];
                        }
                    }
                    break;

                case '15': case '33': case '51': case '69':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $firstMainDataArr[$i][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-12][$l], $firstCoordArr[$i-6][$l]); }
                        # 2, 3
                        if ($l<=2) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-11][$l], $firstCoordArr[$i-5][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-10][$l], $firstCoordArr[$i-4][$l]);
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-11][$l], $firstCoordArr[$i-5][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-10][$l], $firstCoordArr[$i-4][$l]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-9][$l], $firstCoordArr[$i-3][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-8][$l], $firstCoordArr[$i-2][$l]);
                        }
                        else if ($l==5) {}
                        else {
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-9][$l], $firstCoordArr[$i-3][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s, %s)', $firstCoordArr[$i-8][$l], $firstCoordArr[$i-2][$l]);
                        }
                    }
                    break;

                case '57': case '63':
                    $gNum = $firstGreenNumArr[$i];
                    $rNum = $firstRedNumArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $firstCoordArr[$i][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i);
                            $firstMainDataArr[$i][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-54][$l], $firstCoordArr[$i-36][$l], $firstCoordArr[$i-18][$l]);
                        }
                        # 2, 3
                        if ($l<=2) {
                            $firstCoordArr[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+1);
                            $firstCoordArr[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+2);
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-53][$l], $firstCoordArr[$i-35][$l], $firstCoordArr[$i-17][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-52][$l], $firstCoordArr[$i-34][$l], $firstCoordArr[$i-16][$l]);
                        }
                        else if ($l==3) { $firstMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_4), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $firstCoordArr[$i+1][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+1);
                            $firstCoordArr[$i+2][$l] = $this->coord($colAlphaName[$firstStartColNum+$l], $firstStartRowNum+$i+2);
                            $firstMainDataArr[$i+1][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-53][$l], $firstCoordArr[$i-35][$l], $firstCoordArr[$i-17][$l]);
                            $firstMainDataArr[$i+2][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-52][$l], $firstCoordArr[$i-34][$l], $firstCoordArr[$i-16][$l]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $firstMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $firstMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else if ($l<=4) {
                            $firstCoordArr[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+3);
                            $firstCoordArr[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+4);
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-51][$l], $firstCoordArr[$i-33][$l], $firstCoordArr[$i-15][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-50][$l], $firstCoordArr[$i-32][$l], $firstCoordArr[$i-14][$l]);
                        }
                        else if ($l==5) {}
                        else {
                            $firstCoordArr[$i+3][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+3);
                            $firstCoordArr[$i+4][$l] = $this->coord($colAlphaName[$firstStartColNum+$l-2], $firstStartRowNum+$i+4);
                            $firstMainDataArr[$i+3][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-51][$l], $firstCoordArr[$i-33][$l], $firstCoordArr[$i-15][$l]);
                            $firstMainDataArr[$i+4][$l]['data'] = sprintf('=SUM(%s,%s,%s)', $firstCoordArr[$i-50][$l], $firstCoordArr[$i-32][$l], $firstCoordArr[$i-14][$l]);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $secondLeftMainData   = array();
        $secondRightMainData  = array();
        $secondLeftOrangeArr  = array(3 => 758, 5 => 766, 7 => 774);
        $secondRightOrangeArr = array(3 => 782, 5 => 790, 7 => 798);
        $secondLeftRedArr     = array(9 => 549);
        $secondRightRedArr    = array(9 => 557);

        $secondStartRowNum      = $firstStartRowNum + 75; # 77
        $secondLeftStartColNum  =  4;
        $secondRightStartColNum = 11;
        $secondLeftCoordArr     = array();
        $secondRightCoordArr    = array();

        for ($i=0; $i<=10; $i++) {
            $orNum = null;
            $olNum = null;
            $rrNum = null;
            $rlNum = null;

            switch ($i) {
                case '0':
                    $secondRightMainData[$i][0] = array('data' => sprintf('=%s+%s+%s+%s', $this->coord($colAlphaName[$secondLeftStartColNum+3], $secondStartRowNum+9), $this->coord($colAlphaName[$secondLeftStartColNum+3], $secondStartRowNum+10), $this->coord($colAlphaName[$secondRightStartColNum+3], $secondStartRowNum+9), $this->coord($colAlphaName[$secondRightStartColNum+3], $secondStartRowNum+10)), 'attr' => array($rowspan_2, $colspan_2));
                    break;

                case '3': case '5': case '7':
                    $olNum = $secondLeftOrangeArr[$i];
                    $orNum = $secondRightOrangeArr[$i];
                    $rlNum = $secondLeftRedArr[$i];
                    $rrNum = $secondRightRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        $secondLeftMainData[$i][$l]    = array('data' => $dispData['orange_'.($olNum+$l)]);
                        $secondLeftMainData[$i+1][$l]  = array('data' => $dispData['orange_'.($olNum+$l+4)]);
                        $secondRightMainData[$i][$l]   = array('data' => $dispData['orange_'.($orNum+$l)]);
                        $secondRightMainData[$i+1][$l] = array('data' => $dispData['orange_'.($orNum+$l+4)]);

                        $secondLeftCoordArr[$i][$l]    = $this->coord($colAlphaName[$secondLeftStartColNum+$l], $secondStartRowNum+$i);
                        $secondLeftCoordArr[$i+1][$l]  = $this->coord($colAlphaName[$secondLeftStartColNum+$l], $secondStartRowNum+$i+1);
                        $secondRightCoordArr[$i][$l]   = $this->coord($colAlphaName[$secondRightStartColNum+$l], $secondStartRowNum+$i);
                        $secondRightCoordArr[$i+1][$l] = $this->coord($colAlphaName[$secondRightStartColNum+$l], $secondStartRowNum+$i+1);
                    }
                    break;

                case '9':
                    $rlNum = $secondLeftRedArr[$i];
                    $rrNum = $secondRightRedArr[$i];

                    for ($l=0; $l<=3; $l++) {
                        for ($r=0; $r<=5; $r++) {
                            $secondLeftMainData[$i+$r][$l]  = array('data' => sprintf('=SUM(%s,%s,%s)', $secondLeftCoordArr[$i+$r-6][$l], $secondLeftCoordArr[$i+$r-4][$l], $secondLeftCoordArr[$i+$r-2][$l]));
                            $secondRightMainData[$i+$r][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $secondRightCoordArr[$i+$r-6][$l], $secondRightCoordArr[$i+$r-4][$l], $secondRightCoordArr[$i+$r-2][$l]));
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $thirdMainDataArr = array();
        $thirdRedArr      = array(2 => 565);
        $thirdOrangeArr   = array(3 => 806, 4 => 809, 5 => 812);
        $thirdStartRowNum = $secondStartRowNum + 12; # 89
        $thirdStartColNum = 2;

        for ($i=2; $i<=5; $i++) {
            $rNum = $thirdRedArr[$i];
            $oNum = $thirdOrangeArr[$i];
            if ($i==2) {
                for ($l=0; $l<=2; $l++) {
                    $thirdMainDataArr[$i][$l] = array('data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$thirdStartColNum+$l], $thirdStartRowNum+3), $this->coord($colAlphaName[$thirdStartColNum+$l], $thirdStartRowNum+5)));
                }
            }
            else { for ($l=0; $l<=2; $l++) { $thirdMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); } }
        }

        $fourthOrangeArr = array(1 => 815, 2 => 818);
        $fourthRedArr    = array(1 => 568, 2 => 569);

        $fourthStartRowNum = $thirdStartRowNum + 7; # 96
        $fourthStartColNum = 3;

        for ($i=1; $i<=2; $i++) {
            $oNum = $fourthOrangeArr[$i];
            $rNum = $fourthRedArr[$i];
            for ($l=0; $l<=3; $l++) {
                if ($l!=3) { $fourthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                else {
                    $fourthMainDataArr[$i][$l] = array('data' => sprintf('=ROUND(%s/%s,1)', $this->coord($colAlphaName[$fourthStartColNum+1], $fourthStartRowNum+$i+1), $this->coord($colAlphaName[$fourthStartColNum+2], $fourthStartRowNum+$i+1)));
                }
            }
        }

        $fifthMainDataArr = array();
        $fifthOrangeArr   = array( 1 => 821,  2 => 824,  3 => 827,  4 => 830,  5 => 833,  6 => 836,  7 => 839,  8 => 842,  9 => 845, 10 => 848, 11 => 851, 12 => 854, 13 => 857, 15 => 860, 16 => 861);
        $fifthRedArr      = array(14 => 570, 17 => 571);
        $fifthStartRowNum = $fourthStartRowNum + 5; # 101
        $fifthStartColNum = 5;

        for ($i=1; $i<=17; $i++) {
            $oNum = $fifthOrangeArr[$i];
            $rNum = $fifthRedArr[$i];
            switch ($i) {
                default:
                    for ($l=0; $l<=2; $l++) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]); }
                    break;

                case '14':
                    for ($l=0; $l<=2; $l++) {
                        if ($l==0 && $i==14) { $fifthMainDataArr[$i][$l] = array('data' => '&nbsp;', 'attr' => array($colspan_2, $rowspan_4), 'style' => array($bgColor_white)); }
                        else if($l==2) {
                            $fifthMainDataArr[$i][$l] = array(
                                'data' => sprintf('=SUM(%s:%s)', $this->coord($colAlphaName[$fifthStartColNum+2], $fifthStartRowNum+3), $this->coord($colAlphaName[$fifthStartColNum+2], $fifthStartRowNum+14))
                            );
                        }
                    }
                    break;

                case '17':
                    for ($l=0; $l<=2; $l++) {
                        if($l==2) {
                            $fifthMainDataArr[$i][$l] = array(
                                'data' => sprintf('=%s+SUM(%s:%s)', $this->coord($colAlphaName[$fifthStartColNum+2], $fifthStartRowNum+2), $this->coord($colAlphaName[$fifthStartColNum+2], $fifthStartRowNum+15), $this->coord($colAlphaName[$fifthStartColNum+2], $fifthStartRowNum+17))
                            );
                        }
                    }
                    break;

                case '15': case '16':
                    for ($l=0; $l<=2; $l++) { if ($l==2) { $fifthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum)]); } }
                    break;
            }
        }

        $sixthOrangeArr   = array( 3 => 862, 9 => 895, 15 => 928);
        $sixthGreenArr    = array( 3 => 129, 9 => 133, 15 => 137,  21 => 141);
        $sixthRedArr      = array(21 => 573);
        $sixthStartRowNum = $fifthStartRowNum + 20; # 121
        $sixthStartColNum = 4;
        $sixthCoordArr    = array();

        for ($i=0; $i<=25; $i++) {
            $oNum = null;
            $gNum = null;
            switch ($i) {
                case '0':
                    $sixthMainDataArr[$i] = array(0 => array('data' => sprintf('=%s+%s', $this->coord($colAlphaName[$sixthStartColNum+3], $sixthStartRowNum+21), $this->coord($colAlphaName[$sixthStartColNum+4], $sixthStartRowNum+21)), 'attr' => array($rowspan_2, $colspan_2), 'style' => array($textAlign_right)));
                    break;

                case '3': case '9': case '15':
                    $oNum = $sixthOrangeArr[$i];
                    $gNum = $sixthGreenArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) {
                            $sixthCoordArr[$i][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i);
                            $sixthMainDataArr[$i][$l] = array('data' => $dispData['orange_'.($oNum+$l)]);
                        }
                        # 2, 3
                        if ($l<=2) {
                            $sixthCoordArr[$i+1][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i+1);
                            $sixthCoordArr[$i+2][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i+2);
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+7)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+13)]);
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthCoordArr[$i+1][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i+1);
                            $sixthCoordArr[$i+2][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l], $sixthStartRowNum+$i+2);
                            $sixthMainDataArr[$i+1][$l] = array('data' => $dispData['orange_'.($oNum+$l+6)]);
                            $sixthMainDataArr[$i+2][$l] = array('data' => $dispData['orange_'.($oNum+$l+12)]);
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthCoordArr[$i+3][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i+3);
                            $sixthCoordArr[$i+4][$l] = $this->coord($colAlphaName[$sixthStartColNum+$l-2], $sixthStartRowNum+$i+4);
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['orange_'.($oNum+$l+17)]);
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['orange_'.($oNum+$l+24)]);
                        }
                    }
                    break;

                case '21':
                    $gNum = $sixthGreenArr[$i];
                    $rNum = $sixthRedArr[$i];
                    for ($l=0; $l<=8; $l++) {
                        # 1
                        if ($l<=6) { $sixthMainDataArr[$i][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-18][$l], $sixthCoordArr[$i-12][$l], $sixthCoordArr[$i-6][$l])); }
                        # 2, 3
                        if ($l<=2) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-17][$l], $sixthCoordArr[$i-11][$l], $sixthCoordArr[$i-5][$l]));
                            $sixthMainDataArr[$i+2][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-16][$l], $sixthCoordArr[$i-10][$l], $sixthCoordArr[$i-4][$l]));
                        }
                        else if ($l==3) { $sixthMainDataArr[$i+1][$l] = array('data' => '&nbsp;', 'attr' => array($rowspan_2), 'style' => array($bgColor_white)); }
                        else if ($l<=6) {
                            $sixthMainDataArr[$i+1][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-17][$l], $sixthCoordArr[$i-11][$l], $sixthCoordArr[$i-5][$l]));
                            $sixthMainDataArr[$i+2][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-16][$l], $sixthCoordArr[$i-10][$l], $sixthCoordArr[$i-4][$l]));
                        }
                        # 4, 5
                        if ($l<=1) {
                            $sixthMainDataArr[$i+3][$l] = array('data' => $dispData['green_'.($gNum+$l)],   'style' => array($bgColor_white));
                            $sixthMainDataArr[$i+4][$l] = array('data' => $dispData['green_'.($gNum+$l+2)], 'style' => array($bgColor_white));
                        }
                        else {
                            $sixthMainDataArr[$i+3][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-15][$l], $sixthCoordArr[$i-9][$l], $sixthCoordArr[$i-3][$l]));
                            $sixthMainDataArr[$i+4][$l] = array('data' => sprintf('=SUM(%s,%s,%s)', $sixthCoordArr[$i-14][$l], $sixthCoordArr[$i-8][$l], $sixthCoordArr[$i-2][$l]));
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        $sevenMainDataArr = array(0 => array('data' => $dispData['orange_757'], 'style' => array($textAlign_right)));

        ### first table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i <= 73; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=13) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=19) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightBlue)).'</tr>';
            }
            else if ($i<=31) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=37) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightRed)).'</tr>';
            }
            else if ($i<=49) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=55) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightGreen)).'</tr>';
            }
            else if ($i<=67) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=73) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $firstHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($firstMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
        }
        $rtnStr .= '</table><br />';

        ### second table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i <= 10; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=2) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightOrange))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=4) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=6) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=8) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=10) {
                $rtnStr .= '<tr>'.$this->convertCellData($this->getArrValueByKey($i, $secondLeftHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondLeftMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($this->getArrValueByKey($i, $secondRightHeaderArr), $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($secondRightMainData[$i], $fontTag, array('textAlign' => $textAlign_right, 'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }
        }
        $rtnStr .= '</table><br>';

        ### third table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i <= 5; $i++) {
            switch ($i) {
                case '0':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_left, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '1':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '2':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_gray))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '3':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '4':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;

                case '5':
                    $rtnStr .= '<tr>'.$this->convertCellData($thirdHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($thirdMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
                    break;
            }
        }
        $rtnStr .= '</table><br />';

        ### fourth table ###
        $rtnStr .= '<table class="list" border="1" ><tr>'.$this->convertCellData(array(0 => array('data' => '負担限度額認定者比率(日数比率)', 'attr' => array($colspan_6))), $fontTag, array());
        for ($i=0; $i <= 2; $i++) {
			if($i == 0)
			{
				$rtnStr .= '<tr>'.$this->convertCellData($fourthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
					.$this->convertCellData($fourthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
			}
			else
			{
				$rtnStr .= '<tr>';
				$rtnStr .= $this->convertCellData($fourthHeaderArr[$i],   $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple));
				
				for ($j=0; $j <= 3; $j++) 
				{
					
					if($j==3)
					{
						$rtnStr .= "<td  style=\" text-align:right; white-space:nowrap;  mso-number-format:\\#\\,\\#\\#0\\.0;\" >".$fourthMainDataArr[$i][$j]["data"]."</td>";
					}
					else
					{
						$rtnStr .= "<td  style=\" text-align:right;  mso-number-format:\#\,\#\#0;\" >".$fourthMainDataArr[$i][$j]["data"]."</td>";
					}
				}
				$rtnStr .= '</tr>';
				
				
			}
        }
        $rtnStr .= '</table><br />';

        ### fifth table ###
        $rtnStr .= '<table class="list" border="1" ><tr>'.$this->convertCellData(array(0 => array('data' => '自己負担分明細書', 'attr' => array($colspan_7))), $fontTag, array()).'</tr>';
        for ($i=0; $i <=17; $i++) {
            $rtnStr .= '<tr>'.$this->convertCellData($fifthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_blue))
                      .$this->convertCellData($fifthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
        }
        $rtnStr .= '</table><br />';

        ### sixth table ###
        $rtnStr .= '<table class="list" border="1" >';
        for ($i=0; $i < 26; $i++) {
            if ($i<=1) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=7) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightBlue))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=13) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightRed))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=19) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightGreen))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>';
            }
            else if ($i<=25) {
                $rtnStr .= '<tr>'.$this->convertCellData($sixthHeaderArr[$i], $fontTag, array('textAlign' => $textAlign_center, 'whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_lightYellow))
                          .$this->convertCellData($sixthMainDataArr[$i], $fontTag, array('textAlign' => $textAlign_right,  'whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma, 'bgColor' => $bgColor_lightYellow)).'</tr>';
            }

        }
        $rtnStr .= '</table><br />';

        $fontArr = $this->getFont();
        $fontArr['color'] = 'red';
        $fontTag_red = $this->getFontTag($fontArr);

        ### seventh table ###
        $rtnStr .= '<table class="list" border="1" >'
                      .'<tr>'.$this->convertCellData(array(0 => array('data' => '介護職員処遇改善交付金', 'attr' => array($colspan_2))), $fontTag_red, array()).'</td></tr>'
                      .'<tr>'.$this->convertCellData($seventhHeaderArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'bgColor' => $bgColor_purple))
                      .$this->convertCellData($sevenMainDataArr, $fontTag, array('whiteSpace' => $whiteSpace_nowrap, 'msoNumberFormat' => $msoNumberFormat_comma)).'</tr>'
                  .'</table>';
        return $rtnStr;
    }
}