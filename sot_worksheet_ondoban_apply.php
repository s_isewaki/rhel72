<?
$error = "";

// 画面パラメータ取得
$session = @$_REQUEST["session"];
$nikkin_yakin = @$_REQUEST["nikkin_yakin"];
$kango_kaigo = @$_REQUEST["kango_kaigo"];
$is_delete = @$_REQUEST["is_delete"];
$ondoban_date = (int)@$_REQUEST["ondoban_date"];
$ptif_id = @$_REQUEST["ptif_id"];
$emp_id = "";
$emp_name = "";
$is_succeed = "";

$fname = $_SERVER["PHP_SELF"];

for (;;){
	// リクワイア   リクワイア先でのecho/print文を除去
	ob_start();
	require_once("about_postgres.php");
	require_once("about_session.php");
	require_once("about_authority.php");
	ob_end_clean();

	// DB接続
	$con = connect2db($fname);
	if($con == "0") { $error = "03"; break; }

	//セッションのチェック
	$session = qualify_session(@$session,$fname);
	if($session == "0") { $error = "01 / ". $session; break; }

	//権限チェック
	$summary_check=check_authority($session,57,$fname);
	if($summary_check == "0") { $error = "02"; break; }

	//職員情報取得
	$sql = "select * from empmst where emp_id in (select emp_id from session where session_id='".$session."') limit 1";
	$sel = select_from_table($con, $sql, "", $fname);
	if (!$sel) { $error = "04"; break; }
	$emp = pg_fetch_array($sel);
	if (!$emp || !count($emp)) { $error = "05"; break; }
	$emp_id  = $emp["emp_id"];
	$emp_name = trim($emp["emp_lt_nm"] . " " . $emp["emp_ft_nm"]);
	if (!$emp_id) { $error = "06"; break; }

	// 画面パラメータチェック
	if ($is_delete=="") { $error = "11"; break; }
	if ($nikkin_yakin != "nikkin" && $nikkin_yakin != "yakin") { $error = "12"; break; }
	if ($kango_kaigo != "kango" && $kango_kaigo != "kaigo") { $error = "15"; break; }
	if ($ondoban_date < 1) { $error = "13"; break; }
	if ($ptif_id=="") { $error = "14"; break; }

	// WHERE句の準備
	$where = " where ondoban_date = ". $ondoban_date . " and ptif_id = '".pg_escape_string($ptif_id)."' and kango_or_kaigo = '".$kango_kaigo."'";

	// 既存データを検索する。
	$sel = select_from_table($con, "select * from sot_ondoban_apply ".$where." limit 1", "", $fname);
	if (!$sel) { pg_close($con); $error = "21"; break; }
	$accepted_emp_id = @pg_fetch_result($sel, 0, $nikkin_yakin."_accepted_emp_id");

	// データがなければ仮登録する。
	if (!$is_delete){
		if (!@pg_fetch_result($sel, 0, "ondoban_date")){
			$sql = "insert into sot_ondoban_apply (ondoban_date, ptif_id, kango_or_kaigo) values (".$ondoban_date.",'".pg_escape_string($ptif_id)."', '".$kango_kaigo."')";
			if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "22"; break; }
			$is_succeed = 1;
		}
	}

	// 更新
	$sql =
		" update sot_ondoban_apply set".
		" ".$nikkin_yakin."_accepted_emp_id = ". ($is_delete ? "null" : "'".$emp_id."'").
		",".$nikkin_yakin."_accepted_systime = current_timestamp".
		",kango_or_kaigo = '".$kango_kaigo."'".
		" ".$where;
	if (!update_set_table($con, $sql, array(), null, "", $fname)) { $error = "31"; break; }
	$is_succeed = 1;
	break;
}
if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{"ondoban_date":"<?=$ondoban_date?>","nikkin_yakin":"<?=$nikkin_yakin?>","kango_kaigo":"<?=$kango_kaigo?>",
"emp_id":"<?=$emp_id?>","emp_name":"<?=$emp_name?>","error":"<?=$error?>",
"is_succeed":"<?=$is_succeed?>","is_delete":"<?=$is_delete?>"}
