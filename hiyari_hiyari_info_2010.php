<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_report_class.php");
require_once("smarty_setting.ini");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_rm_stats_util.ini");
require_once("hiyari_header_class.php");
require_once("get_values.php");
require_once('class/Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

//==================================================
// セッションのチェック
//==================================================
$session = qualify_session($session, $fname);
if ($session == "0") {
    showLoginPage();
    exit;
}

//==================================================
// 権限のチェック
//==================================================
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
    showLoginPage();
    exit;
}

//==================================================
// データベースに接続
//==================================================
$con = connect2db($fname);


//==================================================
// 専用セッションを使用する。(破棄は明示的にせず、タイムアウトに従う。)
// ※報告書登録系でも別セッションを使用している。
//==================================================
session_name("hiyari_stats_sid");
session_start();

//==================================================
//ログインユーザーID
//==================================================
$emp_id = get_emp_id($con,$session,$fname);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==================================================
//軸情報作成
//==================================================
// 報告書マスタの内容を取得
$rep_obj = new hiyari_report_class($con, $fname);
$grps = $rep_obj->get_all_easy_items();

if(empty($mode)) {
    $year = date("Y");
    $month = 1;
    $mode = 'all';
}

$stats_util = new hiyari_rm_stats_util($con, $fname);

$arr_report_stats_data = $stats_util->get_report_stats_data_2010($grps, 'intelligence_gathering', 'influence', $year, $month, $mode, $arr_post, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info,$item_shibori_info,$drill_down_return,$easy_code);
$arr_report_stats_data_2 = $stats_util->get_report_stats_data_2010($grps, 'show', 'influence', $year, $month, $mode, $arr_post, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info,$item_shibori_info,$drill_down_return,$easy_code);

$vertical_options          = $arr_report_stats_data["vertical_options"];
$horizontal_options        = $arr_report_stats_data["horizontal_options"];
$stats_data_list           = $arr_report_stats_data["stats_data_list"];
$report_id_list            = $arr_report_stats_data["report_id_list"];

$counts                    = $stats_data_list["cells"];
$vertical_sums             = $stats_data_list["vertical_sums"];
$horizontal_sums           = $stats_data_list["horizontal_sums"];
$sums_sum                  = $stats_data_list["sums_sum"];


$vertical_options_2          = $arr_report_stats_data_2["vertical_options"];
$horizontal_options_2        = $arr_report_stats_data_2["horizontal_options"];
$stats_data_list_2           = $arr_report_stats_data_2["stats_data_list"];
$report_id_list_2            = $arr_report_stats_data_2["report_id_list"];

$counts_2                    = $stats_data_list_2["cells"];
$vertical_sums_2             = $stats_data_list_2["vertical_sums"];

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);
$smarty->assign("session_name", session_name());
$smarty->assign("session_id", session_id());

//------------------------------
//ヘッダ用データ
//------------------------------
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//------------------------------
//年月絞り込み用データ
//------------------------------
$years=array();
for($i=2010; $i<=date("Y"); $i++) {
    $years[]=$i;
}
$smarty->assign("years", $years);
$smarty->assign("year", $year);
$smarty->assign("month", $month);
$smarty->assign("mode", $mode);
$smarty->assign("cur_year", date("Y"));

//------------------------------
//列タイトル
//------------------------------
$smarty->assign("col_width", intval(80 / (count($horizontal_sums)+1)));
$col_title = array();
foreach($horizontal_options as $option){
    $easy_code = $option['easy_code'];
    $easy_name = $option['easy_name'];
    if(in_array($easy_code,$horizontal_no_disp_code_list))
    {
        continue;
    }
    if($easy_name == '実施あり') continue;
    $col_title[] = h($easy_name);
}
$smarty->assign("col_title", $col_title);

//------------------------------
//集計結果（概要別）
//------------------------------
$data1 = array();
foreach($vertical_options as $v => $option){
    $easy_code = $option['easy_code'];
    $easy_name = $option['easy_name'];
    if(in_array($easy_code,$vertical_no_disp_code_list)) continue;
    
    //行タイトル
    $data1[$v]['row_title'] = "(" . ($v+1) . ")" . h($easy_name);

    //件数
    $items = array();
    foreach($horizontal_options as $h => $option_h)
    {
        if(in_array($option_h['easy_code'],$horizontal_no_disp_code_list)) continue;

        $report_ids = "";
        foreach($report_id_list['cells'][$v][$h] as $r => $report_id){
            if($r > 0) {
                $report_ids = $report_ids.",";
            }
            $report_ids = $report_ids.$report_id;
        }

        $items[$h]['report_ids'] = $report_ids;
        $items[$h]['count'] = $counts[$v][$h];
        $total_item_count[] = $counts[$v][$h];
    }
    $data1[$v]['items'] = $items;

    //行合計
    $report_ids = "";
    foreach($report_id_list['vertical_sums'][$v] as $r => $report_id){
        if($r > 0) {
            $report_ids = $report_ids.",";
        }
        $report_ids = $report_ids.$report_id;
    }
    $data1[$v]['report_ids'] = $report_ids;
    $data1[$v]['sum'] = $vertical_sums[$v];
    $total_vertical_sum_count[] = $vertical_sums[$v];
}
$smarty->assign("data1", $data1);

//列合計
$data1_sums = array();
foreach($horizontal_options as $h => $option_h){
    if(in_array($option_h['easy_code'],$horizontal_no_disp_code_list)) continue;
    
    $report_ids = "";
    foreach($report_id_list['horizontal_sums'][$h] as $r => $report_id){
        if($r > 0) {
            $report_ids = $report_ids.",";
        }
        $report_ids = $report_ids.$report_id;
    }
    $data1_sums[$h]['report_ids'] = $report_ids;
    $data1_sums[$h]['sum'] = $horizontal_sums[$h];
    $total_horizontal_sum_count[] = $horizontal_sums[$h];
}
$smarty->assign("data1_sums", $data1_sums);

//総合計
$report_ids = "";
foreach($report_id_list['sums_sum'] as $r => $report_id){
    if($r > 0) {
        $report_ids = $report_ids.",";
    }
    $report_ids = $report_ids.$report_id;
}
$smarty->assign("report_ids",$report_ids);
$smarty->assign("sums_sum",$sums_sum);
$total_sum_count[] = $sums_sum;

//------------------------------
//集計結果（再掲別）
//------------------------------
$data2 = array();
foreach($vertical_options_2 as $v => $option){
    $easy_code = $option['easy_code'];
    $easy_name = $option['easy_name'];
    if(in_array($easy_code,$vertical_no_disp_code_list)) continue;

    //行タイトル
    $data2[$v]['row_title'] = h($easy_name);

    //件数
    $items = array();
    foreach($horizontal_options_2 as $h => $option_h)
    {
        if(in_array($option_h['easy_code'],$horizontal_no_disp_code_list)) continue;

        $report_ids = "";
        foreach($report_id_list_2['cells'][$v][$h] as $r => $report_id){
            if($r > 0) {
                $report_ids = $report_ids.",";
            }
            $report_ids = $report_ids.$report_id;
        }

        $items[$h]['report_ids'] = $report_ids;
        $items[$h]['count'] = $counts_2[$v][$h];
        $total_show_count[] = $counts_2[$v][$h];
    }
    $data2[$v]['items'] = $items;

    //行合計
    $report_ids = "";
    foreach($report_id_list_2['vertical_sums'][$v] as $r => $report_id){
        if($r > 0) {
            $report_ids = $report_ids.",";
        }
        $report_ids = $report_ids.$report_id;
    }
    $data2[$v]['report_ids'] = $report_ids;
    $data2[$v]['sum'] = $vertical_sums_2[$v];
    $total_vertical2_sum_count[] = $vertical_sums_2[$v];

}
$smarty->assign("data2", $data2);

//------------------------------
//子画面送信フォーム用データ
//------------------------------
//縦軸・横軸の項目名データ
$v_names=array();
foreach ($vertical_options as $i => $option){
    if(in_array($option['easy_code'],$vertical_no_disp_code_list)) continue;
    $v_names[$i]['v_name'] = str_replace("'","\\'",$option['easy_name']);
}
$smarty->assign("v_names", $v_names);

$h_names=array();
foreach ($horizontal_options as $i => $option){
    if(in_array($option['easy_code'],$horizontal_no_disp_code_list)) continue;
    $h_names[$i]['h_name'] = str_replace("'","\\'",$option['easy_name']);
}
$smarty->assign("h_names", $h_names);

//------------------------------
//Excel用フォームデータ
//------------------------------
$smarty->assign("total_item_count_list", implode(',', $total_item_count));
$smarty->assign("total_vertical_sum_count_list", implode(',', $total_vertical_sum_count));
$smarty->assign("total_horizontal_sum_count_list", implode(',', $total_horizontal_sum_count));
$smarty->assign("total_sum_count_list", implode(',', $total_sum_count));
$smarty->assign("total_show_count_list", implode(',', $total_show_count));
$smarty->assign("total_vertical2_sum_count_list", implode(',', $total_vertical2_sum_count));

//------------------------------
//表示
//------------------------------
if ($design_mode == 1){
    $smarty->display("hiyari_hiyari_info_2010_1.tpl");
}
else{
    $smarty->display("hiyari_hiyari_info_2010_2.tpl");
}

//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
// ログインユーザの部署情報取得
function get_login_user_post($con, $fname, $session){
    $sql  = "select emp_class, emp_attribute, emp_dept, emp_room from empmst";
    $cond = "where emp_id in (select emp_id from session where session_id = '$session')";
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0) {
        pg_close($con);
        showErrorPage();
        exit;
    }
    return pg_fetch_all($result);
}

// データベース接続を閉じる
pg_close($con);


