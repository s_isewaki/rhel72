<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_postgres.php");
require("about_authority.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu3 = pg_fetch_result($sel, 0, "menu3");
$menu3_1 = pg_fetch_result($sel, 0, "menu3_1");
$menu3_2 = pg_fetch_result($sel, 0, "menu3_2");
$menu3_3 = pg_fetch_result($sel, 0, "menu3_3");
?>
<title>イントラネット | <? echo($menu3); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><? echo($menu3); ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="4"><br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_life_coupon_from_login.php"><img src="img/icon/intra_life_coupon.gif" alt="<? echo($menu3_1); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3_1); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_life_welfare_from_login.php"><img src="img/icon/intra_life_welfare.gif" alt="<? echo($menu3_2); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu3_2); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
