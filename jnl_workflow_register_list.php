<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー | 承認依頼通知</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("jnl_workflow_common.ini");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 81, $fname);
if ($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($workflow != "")
{
	set_referer($con, $session, "workflow", $workflow, $fname);
	$referer = $workflow;
}
else
{
	$referer = get_referer($con, $session, "workflow", $fname);
}



// 日報・月報の日付
if ($regist_yr == "")
{
	$regist_yr = date('Y');
}
if ($regist_mon == "")
{
	$regist_mon = date('m');
}
if ($regist_day == "")
{
	$regist_day = date('d');
}

//指定日付作成
$date = $regist_yr.$regist_mon.$regist_day;

//施設・病院に関する詳細内容の登録実行
if($update_type == "1")
{
	//病院・施設、入力者を設定する

	//病院・施設名を更新する
	update_jnl_facility_name($con,$fname,$facility_id,$group_nm);

	//職員情報の登録
	insert_jnl_input_member($con,$fname,$facility_id,$target_list);

}
elseif($update_type == "2")
{
	//登録データ作成
	$total = array();
	$total[0] = $facility_id;
	$total[1] = $date;

	//カウント初期値設定
	$total_cnt = 2;

	for ($i=1; $i<14; $i++)
	{
		//ループ変数を仕様して入力項目のname属性を取得する(許可病床)
		$wrk = "bed_pmt".$i;

		$hh = $bed_pmt1;

		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$total[$total_cnt] = null;
		}
		else
		{
			$total[$total_cnt] = $$wrk;
		}
		$total_cnt++;


		//ループ変数を仕様して入力項目のname属性を取得する(稼動病床)
		$wrk = "bed_act".$i;

		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$total[$total_cnt] = null;
		}
		else
		{
			$total[$total_cnt] = $$wrk;
		}
		$total_cnt++;
	}

	//病床数の登録
	insert_update_jnl_amount_bed($con,$fname,$facility_id,$date,$total);
}
elseif($update_type == "3")
{

	//指定日付作成
	$date = $regist_yr.$regist_mon.$regist_day;

	//登録データ作成
	$total = array();
	$total[0] = $facility_id;
	$total[1] = $date;

	//integerの属性なので空文字をnullに変換する
	if($wk_limit == "")
	{
		$total[2] = null;
	}
	else
	{
		$total[2] = $wk_limit;
	}

	if($st_limit == "")
	{
		$total[3] = null;
	}
	else
	{
		$total[3] = $st_limit;
	}

	if($sn_limit == "")
	{
		$total[4] = null;
	}
	else
	{
		$total[4] = $sn_limit;
	}

	if($enter_limit == "")
	{
		$total[5] = null;
	}
	else
	{
		$total[5] = $enter_limit;
	}


	//「平日（月曜〜金曜）」の業務曜日
	$total[6] = $cfg_wk_flg;

	//「土曜日」の業務曜日
	$total[7] = $cfg_sat_flg;

	//「日曜日」の業務曜日
	$total[8] = $cfg_sun_flg;


	//「日曜日」の業務曜日
	$total[9] = $cfg_ann_flg;

	//通所数の登録
	insert_update_jnl_nursehome_limit($con,$fname,$facility_id,$date,$total);

}


//初期表示用データ作成（取得）

//日報・月報の入力対象者を保持するリスト
$arr_input_member_list = array();


//施設種別を取得（無ければ1(病院)を設定）
if($facility_type == "")
{
	$facility_type = "1";
}

// 施設一覧を配列に格納
$sql = "select jnl_facility_id,jnl_facility_name from jnl_facility_master";
$cond = "where jnl_facility_type='".$facility_type."' order by jnl_facility_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$facilities = array();

while ($row = pg_fetch_array($sel))
{
	$facilities[$row["jnl_facility_id"]] = $row["jnl_facility_name"];
}

//「施設種類選択」のリストボックスの初期表示用の変数
$selected_hos = "";
$selected_old = "";
$selected_ret = "";

// 報告書登録メンバーを表示する場合
if ($facility_type != "")
{
	//「施設種類選択」のリストボックスの初期表示用に変数に"selected"を代入する
	if($facility_type == "2")
	{
		$selected_old = "selected";
	}
	else
	{
		$selected_hos = "selected";
	}
}


if ($facility_id == "")
{
	$facility_id = key($facilities);
}

//$facilities が無かった場合はNULLになる
if ($facility_id != NULL)
{
	//画面初期表示用のデータを取得する

	$sql = "select a.jnl_facility_id,a.emp_id, b.emp_lt_nm,b.emp_ft_nm from jnl_facility_register a, empmst b ";
	$cond = "where a.emp_id=b.emp_id and jnl_facility_id='".$facility_id."' order by a.sort_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$num1 = pg_numrows($sel);

	for ($i=0; $i<$num1; $i++)
	{
		$arr_input_member_list[$i]["id"] =  pg_result($sel,$i,"emp_id");
		$arr_input_member_list[$i]["name"] =  pg_result($sel,$i,"emp_lt_nm").pg_result($sel,$i,"emp_ft_nm");
	}




	//病床数or通所数を表示する
	if($facility_type == "2")
	{
		//老人保健施設

		$sql = "select 	jnl_facility_id, assigned_date ,wk_limit, st_limit, sn_limit, enter_limit, wk_regard, st_regard, sn_regard, ann_regard from jnl_nurse_home_bed ";
		$cond = "where jnl_facility_id='".$facility_id."' order by assigned_date desc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//初期表示用に取得したデータの最新日付のものを配列にセット
		$nhome_bed = array();

		$nhome_bed['wk_limit'] = pg_result($sel,0,"wk_limit");
		$nhome_bed['st_limit'] = pg_result($sel,0,"st_limit");
		$nhome_bed['sn_limit'] = pg_result($sel,0,"sn_limit");

		$nhome_bed['enter_limit'] = pg_result($sel,0,"enter_limit");

		$nhome_bed['cfg_wk_flg'] = pg_result($sel,0,"wk_regard");
		$nhome_bed['cfg_sat_flg'] = pg_result($sel,0,"st_regard");
		$nhome_bed['cfg_sun_flg'] = pg_result($sel,0,"sn_regard");
		$nhome_bed['cfg_ann_flg'] = pg_result($sel,0,"ann_regard");

		$limit_set_record = array();


		$num1 = pg_numrows($sel);

		for ($i=0; $i<$num1; $i++)
		{
			//表示名称（日付）作成
			$record_date = pg_result($sel,$i,"assigned_date");
			$record_date = substr($record_date,0,4)."年".substr($record_date,4,2)."月".substr($record_date,6,2)."日";

			$limit_set_record[$i]['id'] =pg_result($sel,$i,"jnl_facility_id");
			$limit_set_record[$i]['date'] =pg_result($sel,$i,"assigned_date");
			$limit_set_record[$i]['name'] = $record_date;

		}
	}
	else
	{
		//上記以外は病院と判断する

		$sql = "select 	jnl_facility_id, assigned_date ,
				ip_pmt  ,ip_act  ,
				shog_pmt  ,	shog_act  ,
				seip_pmt  ,	seip_act  ,
				ir_pmt  ,ir_act  ,
				kaig_pmt  ,	kaig_act  ,
				kaif_pmt  ,	kaif_act  ,
				ak_pmt  ,	ak_act  ,
				kan_pmt  ,	kan_act  ,
				icu_pmt  ,	icu_act  ,
				shoni_pmt  ,shoni_act  ,
				seir_pmt  ,	seir_act  ,
				tok_pmt  ,	tok_act  ,
				nin_pmt  ,	nin_act  from jnl_hospital_bed ";
		$cond = "where jnl_facility_id='".$facility_id."' order by assigned_date desc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//初期表示用に取得したデータの最新日付のものを配列にセット
		$hos_bed = array();

		$hos_bed['ip_pmt'] = pg_result($sel,0,"ip_pmt");
		$hos_bed['ip_act'] = pg_result($sel,0,"ip_act");
		$hos_bed['shog_pmt'] = pg_result($sel,0,"shog_pmt");
		$hos_bed['shog_act'] = pg_result($sel,0,"shog_act");
		$hos_bed['seip_pmt'] = pg_result($sel,0,"seip_pmt");
		$hos_bed['seip_act'] = pg_result($sel,0,"seip_act");
		$hos_bed['ir_pmt'] = pg_result($sel,0,"ir_pmt");
		$hos_bed['ir_act'] = pg_result($sel,0,"ir_act");
		$hos_bed['kaig_pmt'] = pg_result($sel,0,"kaig_pmt");
		$hos_bed['kaig_act'] = pg_result($sel,0,"kaig_act");
		$hos_bed['kaif_pmt'] = pg_result($sel,0,"kaif_pmt");
		$hos_bed['kaif_act'] = pg_result($sel,0,"kaif_act");
		$hos_bed['ak_pmt'] = pg_result($sel,0,"ak_pmt");
		$hos_bed['ak_act'] = pg_result($sel,0,"ak_act");
		$hos_bed['kan_pmt'] = pg_result($sel,0,"kan_pmt");
		$hos_bed['kan_act'] = pg_result($sel,0,"kan_act");
		$hos_bed['icu_pmt'] = pg_result($sel,0,"icu_pmt");
		$hos_bed['icu_act'] = pg_result($sel,0,"icu_act");
		$hos_bed['shoni_pmt'] = pg_result($sel,0,"shoni_pmt");
		$hos_bed['shoni_act'] = pg_result($sel,0,"shoni_act");
		$hos_bed['seir_pmt'] = pg_result($sel,0,"seir_pmt");
		$hos_bed['seir_act'] = pg_result($sel,0,"seir_act");
		$hos_bed['tok_pmt'] = pg_result($sel,0,"tok_pmt");
		$hos_bed['tok_act'] = pg_result($sel,0,"tok_act");
		$hos_bed['nin_pmt'] = pg_result($sel,0,"nin_pmt");
		$hos_bed['nin_act'] = pg_result($sel,0,"nin_act");

		$bed_set_record = array();

		$num1 = pg_numrows($sel);

		for ($i=0; $i<$num1; $i++)
		{
			//表示名称（日付）作成
			$record_date = pg_result($sel,$i,"assigned_date");
			$record_date = substr($record_date,0,4)."年".substr($record_date,4,2)."月".substr($record_date,6,2)."日";

			$bed_set_record[$i]['id'] =pg_result($sel,$i,"jnl_facility_id");
			$bed_set_record[$i]['date'] =pg_result($sel,$i,"assigned_date");
			$bed_set_record[$i]['name'] = $record_date;

		}
	}
}




//職員情報の登録
/**
 * insert_jnl_input_member 日報・月報の入力対象職員を登録する
 *
 * @param mixed $con 接続情報
 * @param mixed $fname PHPファイル名
 * @param mixed $facility_id 病院・施設ID
 * @param mixed $target_list 入力対象職員リスト
 * @return なし
 *
 */
function insert_jnl_input_member($con,$fname,$facility_id,$target_list)
{

	//指定されている施設・病院の登録されているメンバーを一旦全削除する
	$sql = "delete from jnl_facility_register ";
	$cond = "where jnl_facility_id='".$facility_id."'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	//登録職員ID
	$arr_target_list = split(",", $target_list);

	$lp_cnt = count($arr_target_list);

	if(1 == count($arr_target_list))
	{
		//データチェック
		if("" == $arr_target_list[0])
		{
			//入力無しでも1件となるのでデータが無ければ登録しない
			$lp_cnt = 0;
		}
	}

	for ($i=0; $i<$lp_cnt; $i++)
	{
		$sql = "insert into jnl_facility_register (sort_id, jnl_facility_id, emp_id";
		$sql .= ") values (";
		$content = array($i+1,$facility_id ,$arr_target_list[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

//病床数の登録
function insert_update_jnl_amount_bed($con,$fname,$facility_id,$date,$total)
{

	// 指定病院・日付の病床数を配列に格納
	$sql = "select
			ip_pmt  ,ip_act  ,
			shog_pmt  ,	shog_act  ,
			seip_pmt  ,	seip_act  ,
			ir_pmt  ,ir_act  ,
			kaig_pmt  ,	kaig_act  ,
			kaif_pmt  ,	kaif_act  ,
			ak_pmt  ,	ak_act  ,
			kan_pmt  ,	kan_act  ,
			icu_pmt  ,	icu_act  ,
			shoni_pmt  ,	shoni_act  ,
			seir_pmt  ,	seir_act  ,
			tok_pmt  ,	tok_act  ,
			nin_pmt  ,	nin_act
			from jnl_hospital_bed ";
	$cond = "where jnl_facility_id='".$facility_id."' and assigned_date='".$date."'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$count = pg_numrows($sel);

	if($count > 0)
	{
		//指定の報告書はすでに登録されている状態なので更新処理

		$sql = "update jnl_hospital_bed set";
		$set = array("jnl_facility_id","assigned_date","ip_pmt","ip_act","shog_pmt","shog_act","seip_pmt","seip_act","ir_pmt","ir_act","kaig_pmt","kaig_act","kaif_pmt","kaif_act","ak_pmt","ak_act","kan_pmt","kan_act","icu_pmt","icu_act","shoni_pmt","shoni_act","seir_pmt","seir_act","tok_pmt","tok_act","nin_pmt","nin_act");
		$setvalue = $total;
		$cond = "where jnl_facility_id='".$facility_id."' and assigned_date='".$date."'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	else
	{
		//指定の報告書はまだ登録されていない状態なので登録処理

		$sql = "insert into jnl_hospital_bed (jnl_facility_id, assigned_date,
			ip_pmt ,ip_act  ,shog_pmt  ,	shog_act  ,	seip_pmt  ,	seip_act  ,	ir_pmt  ,	ir_act  ,
			kaig_pmt  ,	kaig_act  ,	kaif_pmt  ,	kaif_act  ,	ak_pmt  ,	ak_act  ,	kan_pmt  ,	kan_act  ,
			icu_pmt  ,	icu_act  ,	shoni_pmt  ,shoni_act  ,seir_pmt  ,	seir_act  ,	tok_pmt  ,	tok_act  ,
			nin_pmt  ,	nin_act";
		$sql .= ") values (";
		$content = $total;
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}


//病院・施設名を登録・更新する
function update_jnl_facility_name($con,$fname,$facility_id,$group_nm)
{
	$sql = "update jnl_facility_master set";
	$set = array("jnl_facility_id","jnl_facility_name");
	$setvalue = array($facility_id,$group_nm);
	$cond = "where jnl_facility_id='".$facility_id."'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}

//通所数を登録・更新する
function insert_update_jnl_nursehome_limit($con,$fname,$facility_id,$date,$total)
{

	// 指定老人保健施設・日付の通所数を配列に格納
	$sql = "select 	jnl_facility_id, assigned_date from jnl_nurse_home_bed ";
	$cond = "where jnl_facility_id='".$facility_id."' and assigned_date='".$date."'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}


	$count = pg_numrows($sel);


	if($count > 0)
	{
		//指定の報告書はすでに登録されている状態なので更新処理

		$sql = "update jnl_nurse_home_bed set";
		$set = array("jnl_facility_id","assigned_date","wk_limit", "st_limit", "sn_limit","enter_limit","wk_regard","st_regard","sn_regard","ann_regard");
		$setvalue = $total;
		$cond = "where jnl_facility_id='".$facility_id."' and assigned_date='".$date."'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	else
	{
		//指定の報告書はまだ登録されていない状態なので登録処理

		$sql = "insert into jnl_nurse_home_bed (jnl_facility_id, assigned_date, wk_limit, st_limit, sn_limit, enter_limit, wk_regard, st_regard, sn_regard, ann_regard";
		$sql .= ") values (";
		$content = $total;
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

/**
 * show_options 日報・月報の入力対象職員をセレクトリストのリスト欄に出力する項目を表示する
 *
 * @param mixed $arr_input_member_list 入力対象職員リスト
 * @return なし
 *
 */
function show_options($arr_input_member_list)
{

	for ($i=0; $i<count($arr_input_member_list); $i++)
	{
		$id = $arr_input_member_list[$i]["id"];
		$nm = $arr_input_member_list[$i]["name"];
		echo("<option value=\"$id\"");
		echo(">$nm</option>\n");
	}

}

?>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registerFacility()
{
	wtype = document.delform.facility_type.value;
	window.open('jnl_workflow_facility_register.php?session=<? echo($session); ?>&facility_type=' + wtype, 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteFacility()
{
	var selected = false;
	if (document.delform.elements['group_ids[]']) {
		if (document.delform.elements['group_ids[]'].length) {
			for (var i = 0, j = document.delform.elements['group_ids[]'].length; i < j; i++) {
				if (document.delform.elements['group_ids[]'][i].checked) {
					selected = true;
					break;
				}
			}
		} else if (document.delform.elements['group_ids[]'].checked) {
			selected = true;
		}
	}

	if (!selected) {
		alert('削除対象が選択されていません。');
		return;
	}



	if (confirm('削除します。よろしいですか？')) {
		document.delform.action = "jnl_workflow_facility_delete.php";
		document.delform.submit();
	}
}

function updateDisplay()
{
		document.delform.action = "jnl_workflow_register_list.php";
		document.delform.submit();
}

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=9';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

// 職員指定画面から呼ばれる関数
function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");

	var target_length = document.employee.target.options.length;

	//追加
	for(i=0;i<emp_ids.length;i++)
	{
		var in_group = false;
		emp_id = emp_ids[i];
		emp_name = emp_names[i];
		for (var k = 0, l = document.employee.target.options.length; k < l; k++) {
			if (document.employee.target[k].value == emp_id) {
				in_group = true;
				break;
			}
		}

		if (!in_group) {
			addOption(document.employee.target, emp_id, emp_name);
		}
	}
}

function addOption(box, value, text, selected) {

	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}
// 削除ボタン
function deleteEmp() {

	for (var i = document.employee.target.options.length - 1; i >= 0; i--) {
		if (document.employee.target.options[i].selected) {
			document.employee.target.options[i] = null;
		}
	}
}


/* updateSetting 指定されたデータを登録するためにhiddenエリアにデータを設定し、submitする
**/
function updateSetting(type)
{


	//登録者設定
	document.employee.target_list.value = '';
	for (var i = 0, j = document.employee.target.options.length; i < j; i++)
	{
		if (i > 0)
		{
			document.employee.target_list.value += ',';
		}
		document.employee.target_list.value += document.employee.target.options[i].value;
	}

	//病院・施設の選択リストの状態を保持する
	document.employee.facility_type.value = document.delform.facility_type.value;


	//登録（更新）内容の種類を保持する
	document.employee.update_type.value = type;


	if(type == 2)
	{
		//数値チェック
		ret = check_number_hos();

		//上記数値チェックでエラーだった場合は画面遷移しない
		if(ret == 1)
		{
			return false;
		}
	}
	else if(type == 3)
	{

		//数値チェック
		ret = check_number_nhome();

		//上記数値チェックでエラーだった場合は画面遷移しない
		if(ret == 1)
		{
			return false;
		}
	}

	//document.employee.action="jnl_workflow_register_list.php";
	document.employee.submit();


}
//合計値を算出して入力する
function check_number_hos()
{

	//チェックフラグ
	var ret = 0;

	for(var wcnt2 = 1;wcnt2 <= 13;wcnt2++)
	{
		//それぞれのセルの値を取得する

		//入力された数値（文字）を1桁ずつ数値チェックする

		//許可病床
		word = 'bed_pmt'+wcnt2;
		obj = document.getElementById(word);

		//許可病床
		ck_array_char = obj.value.split("");

		//許可病床数値チェック↓
		for(wi = 0;wi < ck_array_char.length; wi++)
		{
			ck_char = ck_array_char[wi];
			ck_num = ck_char.match(/[0-9]/);
			if (ck_num != null)
			{
				//先頭の数値がゼロの場合はエラーとして再入力させます
				if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char.length != 1))
				{
					obj.style.color = "red";

					if(ret != 1)
					{
						//2回目以降は出さない
						alert("無効の数値です。");
					}
				    ret = 1;
				    break;
				}
				else
				{
					obj.style.color = "black";
			    }
			}
			else
			{
				//空文字（未入力）以外であればエラーをかえす
				if(obj.value != "")
				{
					obj.style.color = "red";

					if(ret != 1)
					{
						//2回目以降は出さない
						alert("項目は半角数値のみを入力してください。");
					}
					ret = 1;
				}
				else
				{
					obj.style.color = "black";
				}
			}
		}
		//許可病床数値チェック↑


		//稼動病床
		word2 = 'bed_act'+wcnt2;
		obj2 = document.getElementById(word2);

		//稼動病床
		ck_array_char2 = obj2.value.split("");

		//稼動病床数値チェック↓
		for(wi = 0;wi < ck_array_char2.length; wi++)
		{
			ck_char = ck_array_char2[wi];
			ck_num = ck_char.match(/[0-9]/);
			if (ck_num != null)
			{
				//先頭の数値がゼロの場合はエラーとして再入力させます
				if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char2.length != 1))
				{
					obj2.style.color = "red";
					if(ret != 1)
					{
						alert("無効の数値です。");
					}
				    ret = 1;
				    break;
				}
				else
				{
					obj2.style.color = "black";
			    }
			}
			else
			{
				//空文字（未入力）以外であればエラーをかえす
				if(obj2.value != "")
				{
					obj2.style.color = "red";
					if(ret != 1)
					{
						alert("項目は半角数値のみを入力してください。");
					}
					ret = 1;
				}
				else
				{
					obj2.style.color = "black";
				}
			}
		}
		//稼動病床数値チェック↑
	}
	return ret;
}

//通所数数値チェック
function check_number_nhome()
{

	//チェックフラグ
	var ret = 0;

	//それぞれのセルの値を取得する

	//入力された数値（文字）を1桁ずつ数値チェックする

	//平日通所数
	word = 'wk_limit';
	obj = document.getElementById(word);

	//バイト単位に分割
	ck_array_char = obj.value.split("");

	//平日通所数数値チェック↓
	for(wi = 0;wi < ck_array_char.length; wi++)
	{
		ck_char = ck_array_char[wi];
		ck_num = ck_char.match(/[0-9]/);
		if (ck_num != null)
		{
			//先頭の数値がゼロの場合はエラーとして再入力させます
			if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char.length != 1))
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("無効の数値です。");
				}
			    ret = 1;
			    break;
			}
			else
			{
				obj.style.color = "black";
		    }
		}
		else
		{
			//空文字（未入力）以外であればエラーをかえす
			if(obj.value != "")
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("項目は半角数値のみを入力してください。");
				}
				ret = 1;
			}
			else
			{
				obj.style.color = "black";
			}
		}
	}
	//平日通所数数値チェック↑



	//土曜通所数
	word = 'st_limit';
	obj = document.getElementById(word);

	//バイト単位に分割
	ck_array_char = obj.value.split("");

	//土曜通所数数値チェック↓
	for(wi = 0;wi < ck_array_char.length; wi++)
	{
		ck_char = ck_array_char[wi];
		ck_num = ck_char.match(/[0-9]/);
		if (ck_num != null)
		{
			//先頭の数値がゼロの場合はエラーとして再入力させます
			if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char.length != 1))
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("無効の数値です。");
				}
			    ret = 1;
			    break;
			}
			else
			{
				obj.style.color = "black";
		    }
		}
		else
		{
			//空文字（未入力）以外であればエラーをかえす
			if(obj.value != "")
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("項目は半角数値のみを入力してください。");
				}
				ret = 1;
			}
			else
			{
				obj.style.color = "black";
			}
		}
	}
	//土曜通所数数値チェック↑


	//日曜通所数
	word = 'sn_limit';
	obj = document.getElementById(word);

	//バイト単位に分割
	ck_array_char = obj.value.split("");

	//日曜通所数数値チェック↓
	for(wi = 0;wi < ck_array_char.length; wi++)
	{
		ck_char = ck_array_char[wi];
		ck_num = ck_char.match(/[0-9]/);
		if (ck_num != null)
		{
			//先頭の数値がゼロの場合はエラーとして再入力させます
			if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char.length != 1))
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("無効の数値です。");
				}
			    ret = 1;
			    break;
			}
			else
			{
				obj.style.color = "black";
		    }
		}
		else
		{
			//空文字（未入力）以外であればエラーをかえす
			if(obj.value != "")
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("項目は半角数値のみを入力してください。");
				}
				ret = 1;
			}
			else
			{
				obj.style.color = "black";
			}
		}
	}
	//日曜通所数数値チェック↑

	//入所設定数
	word = 'enter_limit';
	obj = document.getElementById(word);

	//バイト単位に分割
	ck_array_char = obj.value.split("");

	//入所設定数値チェック↓
	for(wi = 0;wi < ck_array_char.length; wi++)
	{
		ck_char = ck_array_char[wi];
		ck_num = ck_char.match(/[0-9]/);
		if (ck_num != null)
		{
			//先頭の数値がゼロの場合はエラーとして再入力させます
			if((wi == 0)&&(ck_char[wi] == 0)&&(ck_array_char.length != 1))
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("無効の数値です。");
				}
			    ret = 1;
			    break;
			}
			else
			{
				obj.style.color = "black";
		    }
		}
		else
		{
			//空文字（未入力）以外であればエラーをかえす
			if(obj.value != "")
			{
				obj.style.color = "red";

				if(ret != 1)
				{
					//2回目以降は出さない
					alert("項目は半角数値のみを入力してください。");
				}
				ret = 1;
			}
			else
			{
				obj.style.color = "black";
			}
		}
	}
	//入所設定数値チェック↑

	for ( i = 0; i < document.employee.cfg_wk_flg.length; i++ )
	{
		if ( document.employee.cfg_wk_flg[i].checked )
		{
			break;
		}
	}

	if(i > 2)
	{
		alert("「平日（月曜〜金曜）」の業務曜日を設定してください。");
		ret = 1;
	}

	for ( i = 0; i < document.employee.cfg_sat_flg.length; i++ )
	{
		if ( document.employee.cfg_sat_flg[i].checked )
		{
			break;
		}
	}

	if(i > 2)
	{
		alert("「土曜日」の業務曜日を設定してください。");
		ret = 1;
	}


	for ( i = 0; i < document.employee.cfg_sun_flg.length; i++ )
	{
		if ( document.employee.cfg_sun_flg[i].checked )
		{
			break;
		}
	}

	if(i > 2)
	{
		alert("「日曜日」の業務曜日を設定してください。");
		ret = 1;
	}



	for ( i = 0; i < document.employee.cfg_ann_flg.length; i++ )
	{
		if ( document.employee.cfg_ann_flg[i].checked )
		{
			break;
		}
	}

	if(i > 2)
	{
		alert("「祝日」の業務曜日を設定してください。");
		ret = 1;
	}



	return ret;

}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>


</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
<?
if ($referer == "2")
{
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a>
					</td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font>
					</td>
				</tr>
			</table>
<?
} else {
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr bgcolor="#f6f9ff">
					<td width="32" height="32" class="spacing">
						<a href="jnl_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/b48.gif" width="32" height="32" border="0" alt="日報・月報"></a>
					</td>
					<td>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>日報・月報</b></a> &gt; <a href="jnl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font>
					</td>
					<td align="right" style="padding-right:6px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="jnl_application_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font>
					</td>
				</tr>
			</table>
<?
}
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
<? show_wkfw_menuitem($session, $fname, ""); ?>
					<td></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
				<tr>
					<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
				</tr>
			</table>
			<img src="img/spacer.gif" width="1" height="5" alt=""><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr valign="top">
					<td width="15%">
						<form name="delform">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
								<tr height="22">
									<td  bgcolor="C8D5FF">
										<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>施設種類選択</b><br>
											<select id="facility_type" name="facility_type">
												<option value="1" <?=$selected_hos?>>病院</option>
												<option value="2" <?=$selected_old?>>老健</option>
											</select><input type="button" value="表示" onclick="updateDisplay();"><br>
										</font>
										<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
											<input type="button" value="作成" onclick="registerFacility();">
											<input type="button" value="削除" onclick="deleteFacility();">
										</font>

									</td>
								</tr>
								<tr height="100" valign="top">
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
foreach ($facilities as $tmp_facility_id => $tmp_facility_nm)
{
?>
											<tr height="22" valign="middle">
												<td width="1"><input type="checkbox" name="group_ids[]" value="<?=$tmp_facility_id?>"></td>
<?
	if ($tmp_facility_id == $facility_id)
	{
?>
												<td style="padding-left:3px;background-color:#ffff66;">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$tmp_facility_nm?></font>
												</td>
<?
	} else {
?>
												<td style="padding-left:3px;">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
														<a href="jnl_workflow_register_list.php?session=<?=$session?>&facility_id=<?=$tmp_facility_id?>&facility_type=<?=$facility_type?>"><?=$tmp_facility_nm?></a>
													</font>
												</td>
<?
	}
?>
											</tr>
<?
}
?>
										</table>
									</td>
								</tr>
							</table>
							<input type="hidden" name="session" value="<? echo($session); ?>">
						</form>
					</td>
					<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
					<td>
<?
if($facility_id != "")
{
?>
						<form name="employee" action="jnl_workflow_register_list.php">
							<table border="0" cellspacing="0" cellpadding="2" class="list">
								<tr height="22">

									<td align="left" rowspan="3"><input type="button" value="登録" onclick="updateSetting(1);"></td>

									<td bgcolor="C8D5FF" align="right">
										<table border="0" cellspacing="0" cellpadding="2" class="list">
											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病院・施設名</b></font>
												</td>
												<td>
													<input type="text" name="group_nm" value="<? echo($facilities[$facility_id]); ?>" size="30" style="ime-mode:active;">
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr height="22">
									<td>
										<img src="img/spacer.gif" alt="" width="1" height="10"><br>
									</td>
								</tr>


								<tr height="22">
									<td>
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td colspan="3" align="center" bgcolor="C8D5FF">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>日報・月報入出力対象者</b></font>
												</td>
											</tr>
											<tr>
												<td>
													<input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
												</td>
												<td>
													<select name="target" size="10" multiple style="width:150px;">
														<?show_options($arr_input_member_list);?>
													</select>
												</td>
												<td>
													<input type="button" name="delete_emp" value="削除" onclick="deleteEmp();">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<img src="img/spacer.gif" alt="" width="1" height="10"><br>

<?
	if($facility_type == "1")
	{
		//「病院」の場合は日付を指定する
?>

							<table cellspacing="0" cellpadding="2">
								<tr height="22">
									<td align="left">
										<table border="0" cellspacing="0" cellpadding="2" class="list">
											<tr height="22">
												<td align="left" rowspan="15"><input type="button" value="登録" onclick="updateSetting(2);"></td>
												<td bgcolor="C8D5FF" align="center" colspan="3">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病床数設定</b></font>
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="center" colspan="3">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定日付：
														<select id='regist_yr' name='regist_yr'>
															<? show_select_years(2, $regist_yr, true, true); ?>
														</select>年
														<select id='regist_mon' name='regist_mon'>
															<?show_select_months($regist_mon, true);?>
														</select>月
														<select id='regist_day' name='regist_day'>
															<?show_select_days($regist_day, true);?>
														</select>日
													</font>
												</td>
											</tr>

											<tr>
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>一般病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt1" id="bed_pmt1" style="text-align:right;" value="<?= $hos_bed['ip_pmt']?>" size="6"  maxlength=5>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act1" id="bed_act1" style="text-align:right;" value="<?= $hos_bed['ip_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>障害者病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt2" id="bed_pmt2" style="text-align:right;" value="<?= $hos_bed['shog_pmt']?>" size="6"  maxlength=5>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act2" id="bed_act2" style="text-align:right;" value="<?= $hos_bed['shog_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>精神一般病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt3" id="bed_pmt3" style="text-align:right;" value="<?= $hos_bed['seip_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act3" id="bed_act3" style="text-align:right;" value="<?= $hos_bed['seip_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>医療療養病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt4" id="bed_pmt4" style="text-align:right;" value="<?= $hos_bed['ir_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act4" id="bed_act4" style="text-align:right;" value="<?= $hos_bed['ir_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>介護療養病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt5" id="bed_pmt5" style="text-align:right;" value="<?= $hos_bed['kaig_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act5" id="bed_act5" style="text-align:right;" value="<?= $hos_bed['kaig_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>回復期リハ病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt6" id="bed_pmt6" style="text-align:right;" value="<?= $hos_bed['kaif_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act6" id="bed_act6" style="text-align:right;" value="<?= $hos_bed['kaif_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>


											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>亜急性期病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt7" id="bed_pmt7" style="text-align:right;" value="<?= $hos_bed['ak_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act7" id="bed_act7" style="text-align:right;" value="<?= $hos_bed['ak_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>緩和ケア病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt8" id="bed_pmt8" style="text-align:right;" value="<?= $hos_bed['kan_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act8" id="bed_act8" style="text-align:right;" value="<?= $hos_bed['kan_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>ICU及びハイケアユニット</b></font>
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt9" id="bed_pmt9" style="text-align:right;" value="<?= $hos_bed['icu_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act9" id="bed_act9" style="text-align:right;" value="<?= $hos_bed['icu_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>小児管理料</b></font>
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt10" id="bed_pmt10" style="text-align:right;" value="<?= $hos_bed['shoni_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act10" id="bed_act10" style="text-align:right;" value="<?= $hos_bed['shoni_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>精神医療病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt11" id="bed_pmt11" style="text-align:right;" value="<?= $hos_bed['seir_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff"  align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act11" id="bed_act11" style="text-align:right;" value="<?= $hos_bed['seir_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>


											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>特殊疾患(2)</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt12" id="bed_pmt12" style="text-align:right;" value="<?= $hos_bed['tok_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act12" id="bed_act12" style="text-align:right;" value="<?= $hos_bed['tok_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>

											<tr height="22">
												<td bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>認知症病棟</b></font>
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">定床数（許可病床数）：</font>
													<input type="text" name="bed_pmt13" id="bed_pmt13" style="text-align:right;" value="<?= $hos_bed['nin_pmt']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="#f6f9ff" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動病床数：</font>
													<input type="text" name="bed_act13" id="bed_act13" style="text-align:right;" value="<?= $hos_bed['nin_act']?>" size="6"  maxlength=5 >
												</td>
											</tr>
										</table>
									</td>

									<td align="left" valign="top">
										<table border="0" cellspacing="0" cellpadding="2" class="list">
											<tr height="22">
												<td bgcolor="C8D5FF" align="center">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>更新履歴</b></font>
												</td>
											</tr>
											<tr height="22">
												<td valign="top">
<?
		for ($i=0; $i<count($bed_set_record); $i++)
		{
?>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
														<a href="javascript:void(0);" onclick="window.open('jnl_workflow_register_bed_date_popup.php?session=<?=$session?>&type=1&fc_id=<?=$bed_set_record[$i]['id']?>&date=<?=$bed_set_record[$i]['date']?>', 'newwin2', 'width=600,height=500,scrollbars=yes')"><?=$bed_set_record[$i]['name']?></a><br>
													</font>
<?
		}
?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
<?
	}
	elseif($facility_type == "2")
	{
		//老人保健施設の場合は平日：土曜：日曜・祝日に入力を分ける
?>
							<table cellspacing="0" cellpadding="2">
								<tr height="22">
									<td align="left">
										<table border="0" cellspacing="0" cellpadding="2" class="list">
											<tr height="22">
												<td align="left" rowspan="9"><input type="button" value="登録" onclick="updateSetting(3);"></td>

												<td bgcolor="C8D5FF" align="center" colspan="4">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">指定日付：
														<select id='regist_yr' name='regist_yr'>
															<? show_select_years(2, $regist_yr, true, true); ?>
														</select>年
														<select id='regist_mon' name='regist_mon'>
															<?show_select_months($regist_mon, true);?>
														</select>月
														<select id='regist_day' name='regist_day'>
															<?show_select_days($regist_day, true);?>
														</select>日
													</font>
												</td>
											</tr>


											<tr height="22">
												<td  bgcolor="C8D5FF" align="center" colspan="4">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>定数設定</b></font>
												</td>
											</tr>


											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>通所定数</b></font>
												</td>
												<td bgcolor="FFE9C8" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日：</font>
													<input type="text" name="wk_limit" id="wk_limit" style="text-align:right;" value="<?= $nhome_bed['wk_limit']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="C8FCFF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜：</font>
													<input type="text" name="st_limit" id="st_limit" style="text-align:right;" value="<?= $nhome_bed['st_limit']?>" size="6"  maxlength=5 >
												</td>
												<td bgcolor="FFD7D7" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜：</font>
													<input type="text" name="sn_limit" id="sn_limit" style="text-align:right;" value="<?= $nhome_bed['sn_limit']?>" size="6"  maxlength=5 >
												</td>
											</tr>


											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>入所定数</b></font>
												</td>
												<td bgcolor="B9FF8E" align="right">
													<input type="text" name="enter_limit" id="enter_limit" style="text-align:right;" value="<?= $nhome_bed['enter_limit']?>" size="12"  maxlength=5 >
												</td>
												<td colspan="2">
												</td>
											</tr>




											<tr height="22">
												<td  bgcolor="C8D5FF" align="center" colspan="4">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>業務曜日設定</b></font>
												</td>
											</tr>

											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平日（月曜〜金曜）</b></font>
												</td>
												<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="0"<? if ($nhome_bed['cfg_wk_flg'] == "0") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
												</td>
												<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="1"<? if ($nhome_bed['cfg_wk_flg'] == "1") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
												</td>
												<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_wk_flg" id="cfg_wk_flg" value="2"<? if ($nhome_bed['cfg_wk_flg'] == "2") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
												</td>
											</tr>

											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>土曜日</b></font>
												</td>
												<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="0"<? if ($nhome_bed['cfg_sat_flg'] == "0") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
												</td>
												<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="1"<? if ($nhome_bed['cfg_sat_flg'] == "1") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
												</td>
												<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_sat_flg" id="cfg_sat_flg" value="2"<? if ($nhome_bed['cfg_sat_flg'] == "2") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
												</td>
											</tr>


											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>日曜日</b></font>
												</td>
												<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="0"<? if ($nhome_bed['cfg_sun_flg'] == "0") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
												</td>
												<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="1"<? if ($nhome_bed['cfg_sun_flg'] == "1") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
												</td>
												<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_sun_flg" id="cfg_sun_flg" value="2"<? if ($nhome_bed['cfg_sun_flg'] == "2") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
												</td>
											</tr>

											<tr>
												<td  bgcolor="C8D5FF" align="right">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>祝日</b></font>
												</td>
												<td bgcolor="FFE9C8" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="0"<? if ($nhome_bed['cfg_ann_flg'] == "0") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平日扱い</font>
												</td>
												<td bgcolor="C8FCFF" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="1"<? if ($nhome_bed['cfg_ann_flg'] == "1") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">土曜扱い</font>
												</td>
												<td bgcolor="FFD7D7" align="right">
<input type="radio" name="cfg_ann_flg" id="cfg_ann_flg" value="2"<? if ($nhome_bed['cfg_ann_flg'] == "2") {echo(" checked");} ?>>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日曜扱い</font>
												</td>
											</tr>

										</table>
									</td>

									<td align="left" valign="top">
										<table border="0" cellspacing="0" cellpadding="2" class="list">
											<tr height="22">
												<td bgcolor="C8D5FF" align="center">
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>更新履歴</b></font>
												</td>
											</tr>
											<tr height="22">
												<td valign="top">
<?
		for ($i=0; $i<count($limit_set_record); $i++)
		{
?>
													<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
														<a href="javascript:void(0);" onclick="window.open('jnl_workflow_register_bed_date_popup.php?session=<?=$session?>&type=2&fc_id=<?=$limit_set_record[$i]['id']?>&date=<?=$limit_set_record[$i]['date']?>', 'newwin2', 'width=600,height=500,scrollbars=yes')"><?=$limit_set_record[$i]['name']?></a><br>
													</font>
<?
		}
?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
<?
	}
?>
							<input type="hidden" name="session" id="session" value="<?= $session?>">
							<input type="hidden" name="target_list" id="target_list" value="">
							<input type="hidden" name="facility_id" id="facility_id" value="<?= $facility_id?>">
							<input type="hidden" name="facility_type" id="facility_type" value="<?= $facility_type?>">
							<input type="hidden" name="update_type">
						</form>
<?
}
?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
