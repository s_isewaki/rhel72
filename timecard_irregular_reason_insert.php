<?
require_once("about_comedix.php");

//-----------------------------------------------------------------------------------------------------
// 新規作成　遅刻早退理由登録
// timecard_overtime_reason_insert.php より作成
//-----------------------------------------------------------------------------------------------------

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($reason == "") {
	echo("<script type=\"text/javascript\">alert('遅刻早退理由が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($reason) > 200) {
	echo("<script type=\"text/javascript\">alert('遅刻早退理由が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遅刻早退理由IDを採番
$sql = "select max(reason_id) from iregrsn";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$reason_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

// 遅刻早退理由を登録
$sql = "insert into iregrsn (reason_id, reason, ireg_no_deduction_flg, tmcd_group_id) values (";
if (empty($wktmgrp)) $wktmgrp = NULL;
$content = array($reason_id, pg_escape_string($reason), $ireg_no_deduction_flg, $wktmgrp);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_irregular_reason.php?session=$session';</script>");
?>
