<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | WG詳細</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

if ($schd_emp_id == "") {
	$schd_emp_id = $emp_id;
}

// 委員会情報を配列に格納
$arr_parent = array();
$sql = "select pjt_id, pjt_name from project";
$cond = "where (pjt_parent_id is null) and pjt_delete_flag = 'f' order by pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
}
while ($row = pg_fetch_array($sel)) {
	$arr_parent[$row["pjt_id"]] = $row["pjt_name"];
}

// 委員会・WG情報を取得
$sql = "select project.*, rspemp.emp_lt_nm as rsp_lt_nm, rspemp.emp_ft_nm as rsp_ft_nm, regemp.emp_lt_nm as reg_lt_nm, regemp.emp_ft_nm as reg_ft_nm from project inner join empmst rspemp on project.pjt_response = rspemp.emp_id inner join empmst regemp on project.pjt_reg_emp_id = regemp.emp_id";
$cond = "where pjt_id = '$pjt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$org_rsp_emp_id = pg_fetch_result($sel, 0, "pjt_response");
$reg_emp_id = pg_fetch_result($sel, 0, "pjt_reg_emp_id");
$reg_emp_nm = pg_fetch_result($sel, 0, "reg_lt_nm") . " " . pg_fetch_result($sel, 0, "reg_ft_nm");

// 初期表示時
if ($back != "t") {
	$project_parent_id = pg_fetch_result($sel, 0, "pjt_parent_id");
	$project_name = pg_fetch_result($sel, 0, "pjt_name");
	$project_name_abbrev = pg_fetch_result($sel, 0, "pjt_name_abbrev");
	$s_date = pg_fetch_result($sel, 0, "pjt_start_date");
	$e_date = pg_fetch_result($sel, 0, "pjt_end_date");
	$real_e_date = pg_fetch_result($sel, 0, "pjt_real_end_date");
	$incharge_id = pg_fetch_result($sel, 0, "pjt_response");
	$incharge = pg_fetch_result($sel, 0, "rsp_lt_nm") . " " . pg_fetch_result($sel, 0, "rsp_ft_nm");
	$public_flag = pg_fetch_result($sel, 0, "pjt_public_flag");
	$project_content = pg_fetch_result($sel, 0, "pjt_content");

	//ADD 20100910 A.Tsuzuki
	$force_aprv_flag = pg_fetch_result($sel, 0, "pjt_force_aprv_flg");

	// 承認者の強制入力処理を入れる前のデータの承認者の強制入力は「しない」デフォルト設定
	if ($force_aprv_flag == "") {
		$force_aprv_flag = "f";
	}

	$s_dt1 = substr($s_date, 0, 4);
	$s_dt2 = substr($s_date, 5, 2);
	$s_dt3 = substr($s_date, 8, 2);
	$e_dt1 = substr($e_date, 0, 4);
	$e_dt2 = substr($e_date, 5, 2);
	$e_dt3 = substr($e_date, 8, 2);
	$real_e_dt1 = substr($real_e_date, 0, 4);
	$real_e_dt2 = substr($real_e_date, 4, 2);
	$real_e_dt3 = substr($real_e_date, 6, 2);

	// メンバー情報を取得〜配列に格納
	$sql = "select promember.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, promember.member_kind from promember inner join empmst on promember.emp_id = empmst.emp_id";
	$cond = "where pjt_id = '$pjt_id' order by pjt_member_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_target['1'] = array();
	$arr_target['2'] = array();
	// 事務局フラグ、データ更新可否の確認用
	$secretaria_flg = "f";
	while ($row = pg_fetch_array($sel)) {
		$tmp_emp_id = $row["emp_id"];
		$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		if ($tmp_emp_id == $schd_emp_id) {
			$schd_emp_name = $tmp_emp_nm;
		}
		$tmp_member_kind = $row["member_kind"];
		if ($tmp_member_kind == "1") {
			array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_nm));
			if ($emp_id == $tmp_emp_id) {
				$secretaria_flg = "t";
			}
		} else {
			array_push($arr_target['2'], array("id" => $tmp_emp_id, "name" => $tmp_emp_nm));
		}
	}


	// 承認者情報(設定部分)を取得〜配列に格納
	$sql = "select * from prcdaprv_mng_default ";
	$cond = "where pjt_id = '$pjt_id' order by prcdaprv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_def_mng_ap = array();
	$cnt = 0;
	while ($row = pg_fetch_array($sel))
	{
		$arr_def_mng_ap[$cnt]["notice"] = $row["next_notice_div"];
		$arr_def_mng_ap[$cnt]["order"] =  $row["prcdaprv_order"];
		$cnt++;

	}

	// 承認者情報を取得〜配列に格納
	$sql = "select prcdaprv_default.prcdaprv_order, prcdaprv_default.prcdaprv_no, prcdaprv_default.prcdaprv_emp_id,
			empmst.emp_lt_nm, empmst.emp_ft_nm from prcdaprv_default inner join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id";
	$cond = "where pjt_id = '$pjt_id' order by prcdaprv_default.prcdaprv_order, prcdaprv_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_def_ap_user = array();
	$cnt = 0;
	$level_cnt_forward = "";
	while ($row = pg_fetch_array($sel))
	{
		$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";

		$level_cnt = ($row["prcdaprv_order"] - 1);

		if($cnt == 0)
		{
			//初期値設定
			$level_cnt_forward = $level_cnt;
		}
		else
		{
			if($level_cnt_forward != $level_cnt)
			{
				//階層が変わったので配列を変えます
				$level_cnt_forward = $level_cnt;
				$cnt = 0;
			}
		}

		$arr_def_ap_user[$level_cnt][$cnt]["id"] =$row["prcdaprv_emp_id"];
		$arr_def_ap_user[$level_cnt][$cnt]["order"] = $row["prcdaprv_order"];
		$arr_def_ap_user[$level_cnt][$cnt]["name"] = $tmp_emp_nm;
		//$arr_def_mng_ap[$cnt]["no"] =$row["prcdaprv_no"];

		$cnt++;

	}


// ポストバック時
} else {

	// メンバー情報を配列に格納
	$arr_target['1'] = array();
	if ($target_id_list1 != "") {
		$arr_target_id = split(",", $target_id_list1);
		for ($i = 0; $i < count($arr_target_id); $i++) {
			$tmp_emp_id = $arr_target_id[$i];
			$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
			$cond = "where emp_id = '$tmp_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
			}
			$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
			if ($tmp_emp_id == $schd_emp_id) {
				$schd_emp_name = $tmp_emp_name;
			}
			array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
		}
	}
	$arr_target['2'] = array();
	if ($target_id_list2 != "") {
		$arr_target_id = split(",", $target_id_list2);
		for ($i = 0; $i < count($arr_target_id); $i++) {
			$tmp_emp_id = $arr_target_id[$i];
			$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
			$cond = "where emp_id = '$tmp_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
			}
			$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
			if ($tmp_emp_id == $schd_emp_id) {
				$schd_emp_name = $tmp_emp_name;
			}
			array_push($arr_target['2'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
		}
	}



}

// ログインユーザの職員名を取得
if ($schd_emp_name == "") {
	$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
	$cond = "where emp_id = '$schd_emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
	}
	$schd_emp_name = pg_fetch_result($sel, 0, "emp_name");
}

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<?
require("project_emplist_caller_javascript.php");
insert_javascript();
?>
<script language="javascript">
<!--
function initPage() {
	//登録対象者を設定する。
	update_target_html("1");
	update_target_html("2");
}

var childwin = null;
function openEmployeeList() {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './project_emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=1';
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function openEmployeeList_aprv(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode='+item_id;
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function closeEmployeeList()
{

	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;

	document.pjt.submit();

}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
	for ($i=1; $i<=2; $i++) {
		$script = "m_target_list['$i'] = new Array(";
		$is_first = true;
		foreach($arr_target["$i"] as $row)
		{
			if($is_first)
			{
				$is_first = false;
			}
			else
			{
				$script .= ",";
			}
			$tmp_emp_id = $row["id"];
			$tmp_emp_name = $row["name"];
			$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
		}
		$script .= ");\n";
		print $script;
	}
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}


function checkApprove()
{
	document.pjt.action = "project_wg_update.php";
	document.pjt.back.value = "t";
	document.pjt.submit();
}


//-->
</script>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.schdreg {border-collapse:collapse;}
.schdreg td {border:#5279a5 solid 1px;}
.inner td {border-style:none;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a NAME="P1" href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>


<? if ($pjt_admin_auth == "1") { ?>
	<? if($adm_flg=="t") {?>
	<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? } else {?>
	<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? }?>
<? }else{ ?>
	<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>


</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? }else{ ?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>


<td width="5">&nbsp;</td>
<? if($adm_flg=="t") {?>
	<td width="120" align="center" bgcolor="#5279a5"><a href="project_wg_update.php?session=<? echo($session); ?>&adm_flg=<? echo($adm_flg); ?>&pjt_id=<? echo($pjt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>WG詳細</b></font></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会登録</font></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_wg_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG登録</font></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_member_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG名簿</font></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
	<td width="5">&nbsp;</td>
	<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_bulk_set.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
	<td width="">&nbsp;</td>
<? }else{ ?>
	<td width="120" align="center" bgcolor="#5279a5"><a href="project_wg_update.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>WG詳細</b></font></a></td>
	<td width="5">&nbsp;</td>
	<td width="">&nbsp;</td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<form name="pjt" action="project_wg_update_confirm.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="schdreg">
	<tr>
		<td bgcolor="#ccffcc" width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会</font></td>
		<td bgcolor="FFF1E3" width="480" colspan="2"><select name="project_parent_id"><? show_parent_options($arr_parent, $project_parent_id); ?></select></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" width="120" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG名</font></td>
		<td bgcolor="FFF1E3" width="480" colspan="2"><input name="project_name" type="text" size="50" maxlength="50" value="<? echo($project_name); ?>" style="ime-mode: active;"></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">略称</font></td>
		<td bgcolor="FFF1E3" colspan="2"><input name="project_name_abbrev" type="text" size="8" maxlength="8" value="<? echo($project_name_abbrev); ?>" style="ime-mode: active;"></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイプ</font></td>
		<td bgcolor="FFF1E3" colspan="2"><input type="radio" name="public_flag" value="t" <? if($public_flag==t){echo("checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開</font>&nbsp;<input type="radio" name="public_flag" value="f" <? if($public_flag==f){echo("checked");} ?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非公開</font></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日</font></td>
		<td bgcolor="FFF1E3" colspan="2"><select name="s_dt1"><? show_select_years_span(2000, date("Y") + 9, $s_dt1); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="s_dt2"><? show_select_months($s_dt2); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="s_dt3"><? show_select_days($s_dt3); ?></select></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了予定日</font></td>
		<td bgcolor="FFF1E3" colspan="2"><select name="e_dt1"><? show_update_years($e_dt1, 10, true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="e_dt2"><? show_select_months($e_dt2, true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="e_dt3"><? show_select_days($e_dt3, true); ?></select></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了日</font></td>
		<td bgcolor="FFF1E3" colspan="2"><select name="real_e_dt1"><? show_select_years(10, $real_e_dt1, true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="real_e_dt2"><? show_select_months($real_e_dt2, true); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="real_e_dt3"><? show_select_days($real_e_dt3, true); ?></select></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">責任者</font></td>
		<td bgcolor="FFF1E3"><input name="incharge_name" type="text" size="30" maxlength="50" value="<? echo($incharge); ?>" disabled><input type="hidden" name="incharge" value="<? echo($incharge); ?>"><input type="hidden" name="incharge_id" value="<? echo($incharge_id); ?>"></td><td bgcolor="#ccffcc" width="220"><input type="button" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList();"></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事務局</font><br>
			<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','<?=$schd_emp_id?>','<?=$schd_emp_name?>');"><br>
		</td>
		<td bgcolor="FFF1E3" colspan="2">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
				<tr>
					<td bgcolor="white" width="350" height="48" style="border:#5279a5 solid 1px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
							<span id="target_disp_area1"></span>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メンバー</font><br>
			<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('2','<?=$schd_emp_id?>','<?=$schd_emp_name?>');"><br>
		</td>
		<td bgcolor="FFF1E3" colspan="2">
			<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
				<tr>
					<td bgcolor="white" width="350" height="48" style="border:#5279a5 solid 1px;">
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
							<span id="target_disp_area2"></span>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<?

if (($back != "t") && ($ovtm_approve_num ==""))
{
	//初期表示の場合
	$ovtm_approve_num = count($arr_def_mng_ap);
}

?>


<!-- 承認階層数 -->
	<tr height="22">
		<td width="160" align="right" bgcolor="#ccffcc"><font id="ovtm_kaisofont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >承認階層数</font></td>
		<td colspan="2"><select name="ovtm_approve_num" id="ovtm_approve_num" onChange="checkApprove();" ><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $ovtm_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
<?
//追加部分↓
/*
$ovtm_approve = array($ovtm_approve1, $ovtm_approve2, $ovtm_approve3, $ovtm_approve4, $ovtm_approve5, $ovtm_approve6, $ovtm_approve7, $ovtm_approve8, $ovtm_approve9, $ovtm_approve10, $ovtm_approve11, $ovtm_approve12, $ovtm_approve13, $ovtm_approve14, $ovtm_approve15, $ovtm_approve16, $ovtm_approve17, $ovtm_approve18, $ovtm_approve19, $ovtm_approve20);

$ovtm_emp_id = array($ovtm_emp_id1, $ovtm_emp_id2, $ovtm_emp_id3, $ovtm_emp_id4, $ovtm_emp_id5, $ovtm_emp_id6, $ovtm_emp_id7, $ovtm_emp_id8, $ovtm_emp_id9, $ovtm_emp_id10, $ovtm_emp_id11, $ovtm_emp_id12, $ovtm_emp_id13, $ovtm_emp_id14, $ovtm_emp_id15, $ovtm_emp_id16, $ovtm_emp_id17, $ovtm_emp_id18, $ovtm_emp_id19, $ovtm_emp_id20);
$ovtm_emp_nm = array($ovtm_emp_nm1, $ovtm_emp_nm2, $ovtm_emp_nm3, $ovtm_emp_nm4, $ovtm_emp_nm5, $ovtm_emp_nm6, $ovtm_emp_nm7, $ovtm_emp_nm8, $ovtm_emp_nm9, $ovtm_emp_nm10, $ovtm_emp_nm11, $ovtm_emp_nm12, $ovtm_emp_nm13, $ovtm_emp_nm14, $ovtm_emp_nm15, $ovtm_emp_nm16, $ovtm_emp_nm17, $ovtm_emp_nm18, $ovtm_emp_nm19, $ovtm_emp_nm20);

$ovtm_multi_apv_flg = array($ovtm_multi_apv_flg1, $ovtm_multi_apv_flg2, $ovtm_multi_apv_flg3, $ovtm_multi_apv_flg4, $ovtm_multi_apv_flg5, $ovtm_multi_apv_flg6, $ovtm_multi_apv_flg7, $ovtm_multi_apv_flg8, $ovtm_multi_apv_flg9, $ovtm_multi_apv_flg10, $ovtm_multi_apv_flg11, $ovtm_multi_apv_flg12, $ovtm_multi_apv_flg13, $ovtm_multi_apv_flg14, $ovtm_multi_apv_flg15, $ovtm_multi_apv_flg16, $ovtm_multi_apv_flg17, $ovtm_multi_apv_flg18, $ovtm_multi_apv_flg19, $ovtm_multi_apv_flg20);
$ovtm_next_notice_div = array($ovtm_next_notice_div1, $ovtm_next_notice_div2, $ovtm_next_notice_div3, $ovtm_next_notice_div4, $ovtm_next_notice_div5, $ovtm_next_notice_div6, $ovtm_next_notice_div7, $ovtm_next_notice_div8, $ovtm_next_notice_div9, $ovtm_next_notice_div10, $ovtm_next_notice_div11, $ovtm_next_notice_div12, $ovtm_next_notice_div13, $ovtm_next_notice_div14, $ovtm_next_notice_div15, $ovtm_next_notice_div16, $ovtm_next_notice_div17, $ovtm_next_notice_div18, $ovtm_next_notice_div19, $ovtm_next_notice_div20);

$ovtm_apv_num = array($ovtm_apv_num1, $ovtm_apv_num2, $ovtm_apv_num3, $ovtm_apv_num4, $ovtm_apv_num5, $ovtm_apv_num6, $ovtm_apv_num7, $ovtm_apv_num8, $ovtm_apv_num9, $ovtm_apv_num10, $ovtm_apv_num11, $ovtm_apv_num12, $ovtm_apv_num13, $ovtm_apv_num14, $ovtm_apv_num15, $ovtm_apv_num16, $ovtm_apv_num17, $ovtm_apv_num18, $ovtm_apv_num19, $ovtm_apv_num20);
*/
for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;

	if ($back == "t")
	{
		//画面遷移のエラーチェックで戻ってきた場合

		$str_emp_idW = "ovtm_emp_id".$j;
		$str_emp_id = $$str_emp_idW;

		$str_emp_nmW = "ovtm_emp_nm".$j;
		$str_emp_nm = $$str_emp_nmW;

		//$str_contentW = "ovtm_approve_content".$j;
		//$str_content = $$str_contentW;

		$str_noticeW = "ovtm_next_notice_div".$j;
		$str_notice = $$str_noticeW;

	}
	else
	{
		//初期表示の場合

		if($arr_def_ap_user[$i][0]["name"] != "" )
		{

			for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
			{
				if($wcnt1 == 0)
				{
					$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
					$str_emp_id = $arr_def_ap_user[$i][$wcnt1]["id"];
				}
				else
				{
					$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
					$str_emp_id = $str_emp_id.",".$arr_def_ap_user[$i][$wcnt1]["id"];
				}
			}
		}
		else
		{
			$str_emp_nm = "";
			$str_emp_id = "";
		}

		$str_notice = $arr_def_mng_ap[$i]["notice"];

	}



	//	if($ovtm_wkfw_div == "1"){
	$linktarget = "window.open('project_approval_list.php?session=$session&approve=$j&maxCnt=$ovtm_approve_num&wkfw_nm=ovtm', 'newwin2', 'width=640,height=700,scrollbars=yes')";
	$fontcolor = "";
	$approve_content_color = "green";
	//	}
	//	else{
	//		$linktarget = "";
	//		$fontcolor = "#a9a9a9";
	//		$approve_content_color = "#a9a9a9";
	//	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#ccffcc\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"ovtm_kaisou". $j ."\" href=\"javascript:void(0);\" onclick=\"" . $linktarget . "\"><font id=\"ovtm_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></a></font></td>\n");

	echo("<td colspan=\"2\">");
	echo("<font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\">");
	echo("<span id=\"ovtm_approve_content". $j ."\"></span></font>\n");

	//	echo("<td colspan=\"2\"><font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"target_disp_area3\"></span></font>");

	/*
		$ovtm_multi_apv_flg[$i] = ($ovtm_multi_apv_flg[$i] == "") ? "f" : $ovtm_multi_apv_flg[$i];
		$ovtm_next_notice_div[$i] = ($ovtm_next_notice_div[$i] == "") ? "1" : $ovtm_next_notice_div[$i];
		$ovtm_apv_num[$i] = ($ovtm_apv_num[$i] == "") ? "1" : $ovtm_apv_num[$i];
	*/
	//echo("<input type=\"hidden\" name=\"ovtm_approve" . $j . "\" value=\"" . $ovtm_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"".$str_emp_id."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"".$str_emp_nm."\">\n");

	//echo("<input type=\"hidden\" name=\"ovtm_multi_apv_flg" . $j . "\" value=\"" . $ovtm_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"".$str_notice."\">\n");

	//echo("<input type=\"hidden\" name=\"ovtm_apv_num" . $j . "\" value=\"" . $ovtm_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");

}
//追加部分↑

?>

	<tr>
		<td bgcolor="#ccffcc" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
		<td bgcolor="FFF1E3" colspan="2"><textarea name="project_content" rows="5" cols="40" style="ime-mode: active;"><? echo($project_content); ?></textarea></td>
	</tr>

	<tr>
		<td bgcolor="#ccffcc" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">作成者</font></td>
		<td bgcolor="FFF1E3" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reg_emp_nm); ?></font></td>
	</tr>


</table>

<? if (($emp_id == $org_rsp_emp_id || $emp_id == $reg_emp_id || $secretaria_flg == "t") || ($adm_flg=="t") ){ ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" colspan="2" align="right"><input type="button" value="更新" onclick="closeEmployeeList();"></td>
</tr>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="schd_emp_id" value="<? echo($schd_emp_id); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
<input type="hidden" id="target_name_list2" name="target_name_list2" value="">
<input type="hidden" name="secretaria_flg" value="<? echo($secretaria_flg); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="prev_project_parent_id" value="<? echo($project_parent_id); ?>">
</form>
</td>
</tr>
</table>

<script type="text/javascript">
// 承認階層
<?
for ($i = 0; $i < $ovtm_approve_num; $i++)
{
	$j = $i+1;



	if ($back == "t")
	{
		$str_emp_nmW = "ovtm_emp_nm".$j;
		$str_emp_nm = $$str_emp_nmW;
	}
	else
	{
		//初期表示の場合


		if($arr_def_ap_user[$i][0]["name"] != "" )
		{

			for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
			{
				if($wcnt1 == 0)
				{
					$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
				}
				else
				{
					$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
				}
			}
		}
		else
		{
			$str_emp_nm = "";
			$str_emp_id = "";
		}
	}


?>
document.getElementById('ovtm_approve_content<?=$j?>').innerHTML = '<?=$str_emp_nm?>';
	<?
}
?>
</script>



</body>
<? pg_close($con); ?>
</html>
<?
// 委員会オプションを出力
function show_parent_options($arr_parent, $parent_id) {
	echo("<option value=\"-\">--------------------");
	foreach ($arr_parent as $tmp_parent_id => $tmp_parent_name) {
		echo("<option value=\"$tmp_parent_id\"");
		if ($parent_id == $tmp_parent_id) {
			echo(" selected");
		}
		echo(">$tmp_parent_name\n");
	}
}
?>
