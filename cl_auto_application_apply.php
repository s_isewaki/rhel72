<?php
//TODO このモジュールは恐らく使用されていません。要廃止検討。


//ini_set( 'display_errors', 1 );

require_once("cl_common_log_class.inc");
$log = new cl_common_log_class(basename(__FILE__));
$log->info(basename(__FILE__)." START");

require_once('MDB2.php');
require_once(dirname(__FILE__) . "/Cmx.php");

require_once(dirname(__FILE__) . "/cl/model/search/cl_auto_application_apply_model.php");
require_once(dirname(__FILE__) . "/cl/model/sequence/cl_apply_id_seq_model.php");
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_apply_model.php");
require_once(dirname(__FILE__) . "/cl/model/workflow/cl_applyapv_model.php");

function auto_application_apply($obj, $mdb2, $short_wkfw_name, $apply_emp_id, $apv_emp_id){

	global $log;
	$log->debug('ワークフローマスタ取得',__FILE__,__LINE__);
	$auto_apply_model = new cl_auto_application_apply_model($mdb2, $apply_emp_id);
	$wkfw_data = $auto_apply_model->getwkfw($short_wkfw_name);

	$log->debug('ワークフロー履歴No取得',__FILE__,__LINE__);
	$wkfw_history_no = $auto_apply_model->get_max_wkfw_history_no($wkfw_data['wkfw_id']);

	$log->debug('フォーマットファイル情報取得',__FILE__,__LINE__);
	$wkfw_file = $auto_apply_model->get_wkfwfile($wkfw_data['wkfw_id']);

	if (count($wkfw_file) > 0) {
		$log->debug('ワークファイル履歴No取得',__FILE__,__LINE__);
		$wkfwfile_history_no = $auto_apply_model->get_max_wkfwfile_history_no($wkfw_data['wkfw_id']);
	}

	$log->debug('申請者情報取得',__FILE__,__LINE__);
	$apply_emp_data = $auto_apply_model->getemp($apply_emp_id);

	$log->debug('新規申請取得',__FILE__,__LINE__);
	$cl_apply_id_model = new cl_apply_id_seq_model($mdb2,$apply_emp_id);
	$apply_id=$cl_apply_id_model->getApplyId();

	$log->debug('申請番号取得',__FILE__,__LINE__);
	$apply_no = $obj->get_apply_no();
//	$date = date("Ymd");
//	$year = substr($date, 0, 4);
//	$md   = substr($date, 4, 4);
//	if($md >= "0101" and $md <= "0331")
//	{
//		$year = $year - 1;
//	}
//	$max_cnt = $obj->get_apply_cnt_per_year($year);
//	$apply_no = $max_cnt + 1;
	

	$log->debug('申請登録',__FILE__,__LINE__);
	$arr = array(
		"apply_id"				=> $apply_id,
		"wkfw_id"				=> $wkfw_data['wkfw_id'],
		"apply_content"			=> null,
		"emp_id"				=> $apply_emp_id,
		"apply_stat"			=> "0",
		"apply_date"			=> date("YmdHi"),
		"apply_title"			=> $wkfw_data['wkfw_title'],
		"re_apply_id"			=> null,
		"apv_fix_show_flg"		=> "t",
		"apv_bak_show_flg"		=> "t",
		"emp_class"				=> $apply_emp_data['emp_class'],
		"emp_attribute"			=> $apply_emp_data['emp_attribute'],
		"emp_dept"				=> $apply_emp_data['emp_dept'],
		"apv_ng_show_flg"		=> "t",
		"emp_room"				=> $apply_emp_data['emp_room'],
		"draft_flg"				=> "f",
		"wkfw_appr"				=> $wkfw_data['wkfw_appr'],
		"wkfw_content_type"		=> $wkfw_data['wkfw_content_type'],
		"apply_title_disp_flg"	=> $wkfw_data['apply_title_disp_flg'],
		"apply_no"				=> $apply_no,
		"notice_sel_flg"		=> "f",
		"wkfw_history_no"		=> ($wkfw_history_no == "") ? null : $wkfw_history_no,
		"wkfwfile_history_no"	=> ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
	);

	$cl_apply_model = new cl_apply_model($mdb2,$apply_emp_id);
	$cl_apply_model->insert($arr);

	$log->debug('承認者情報取得',__FILE__,__LINE__);
	$apv_emp_data = $auto_apply_model->getemp($apv_emp_id);

	$log->debug('承認登録',__FILE__,__LINE__);
	$arr = array(
		"wkfw_id"			 =>	$wkfw_data['wkfw_id'],
		"apply_id"			 =>	$apply_id,
		"apv_order"			 =>	1,
		"emp_id"			 =>	$apv_emp_id,
		"apv_stat"			 =>	"0",
		"apv_date"			 =>	"",
		"apv_comment"		 =>	"",
		"st_div"			 =>	"2",
		"deci_flg"			 =>	"t",
		"emp_class"			 =>	null,
		"emp_attribute"		 =>	null,
		"emp_dept"			 =>	null,
		"emp_st"			 =>	$apv_emp_data['emp_st'],
		"apv_fix_show_flg"	 =>	"t",
		"emp_room"			 =>	null,
		"apv_sub_order"		 =>	1,
		"multi_apv_flg"		 =>	"f",
		"next_notice_div"	 =>	null,
		"parent_pjt_id"		 =>	null,
		"child_pjt_id"		 =>	null,
		"other_apv_flg"		 =>	"f",
		"draft_flg"			 =>	"f",
		"eval_content"		 =>	null
	);

	$cl_applyapv_model = new cl_applyapv_model($mdb2,$apply_emp_id);
	$cl_applyapv_model->insert($arr);

}

?>