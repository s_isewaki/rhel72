<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">

<?php
// 管理帳票
// duty_shift_mprint.php

ob_start();

ini_set("max_execution_time", 0);

require_once("about_comedix.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_user_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_mprint_common_class.php");
require_once("get_menu_label.ini");
require_once("get_values.ini");
require_once("work_admin_csv_download_common.php");

require_once("atdbk_common_class.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("timecard_bean.php");
require_once("calendar_name_class.php");

ob_clean();

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$emp_id = get_emp_id($con,$session,$fname);
// 勤務シフト作成
$shift_menu_label = get_shift_menu_label($con, $fname);
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_mprint = new duty_shift_mprint_common_class($con, $fname);
//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//カレンダー名、所定労働時間履歴用
$calendar_name_class = new calendar_name_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//ユーザ画面用
$chk_flg = $obj->check_authority_user($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//管理画面用
$section_admin_auth = $obj->check_authority_Management($session, $fname);


//全てを集計できる権限確認
$is_mprint_staff = $obj_mprint->is_duty_shift_mprint_staff($emp_id); 
//権限のあるシフトグループ確認
$group_array = $obj->get_duty_shift_group_array("", "", $data_wktmgrp);
// 更新権限あり、職員設定、応援追加されているグループ取得
$group_array = $obj->get_valid_group_array($group_array, $emp_id, $duty_yyyy, $duty_mm);

//権限有無確認
if (count($group_array) <= 0 && !$is_mprint_staff) {
    $err_msg_1 = "勤務シフトグループ情報が未設定です。管理画面で登録してください。";
} else {
    $err_msg_1 = "";
}
if ($shift_group_id == "" &&
    count($group_array) > 0) {
    $shift_group_id = $group_array[0]["group_id"];
}

//帳票名称
$arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst();


if ($print_flg == "1") {
    //期間
    $start_date = $select_start_yr.$select_start_mon.$select_start_day;
    $end_date = $select_end_yr.$select_end_mon.$select_end_day;
    $start_date_parttime = $start_date;
    $end_date_parttime = $end_date;
    
    $arr_config = get_timecard_csv_config($con, $fname, $csv_layout_id, "2");
    $header_flg = $arr_config["csv_header_flg"];
    $csv_ext_name = $arr_config["csv_ext_name"];
    
    if ($page == "") {
        $page = 1;
    }
    if ($limit == "") {
        $limit = 30;
    }
    if ($total_flg == "") {
        $total_flg = "1";
    }
    
    //全体の件数取得
    $wk_cnt = get_arr_emp_id_for_mprint($con, $fname, $shift_group_id, $arr_config, 0, -1, $start_date, $end_date, $total_flg);
    $max_page = ceil($wk_cnt / $limit);
    
    $arr_emp_id_layout = get_arr_emp_id_for_mprint($con, $fname, $shift_group_id, $arr_config, $page, $limit, $start_date, $end_date, $total_flg);
    
    $arr_csv_list = get_csv_list($con, $fname, $csv_layout_id, "2");
    
    //CSV休日出勤回数出力職種ID取得
    $arr_job_id_list = get_job_id_list($con, $fname, $csv_layout_id, "2");
    //
    $header_flg = "t";
    $closing = $timecard_bean->closing;
    $closing_month_flg = $timecard_bean->closing_month_flg;
    $arr_dat = get_attendance_book_csv($con, $emp_id_list,
            $start_date, $end_date,
            $start_date_parttime, $end_date_parttime,
            $c_yyyymm, $ovtmscr_tm, $fname, $arr_reason_id, $arr_disp_flg,
            $group_id, $atdbk_common_class, $arr_csv_list, $header_flg, $arr_job_id_list, $closing, $closing_month_flg, $arr_emp_id_layout, $timecard_bean, $shift_group_id, $csv_layout_id, $obj_hol_hour, $calendar_name_class, "2", "0", $total_flg);
}

?>
<title>CoMedix <? echo($shift_menu_label); ?>｜管理帳票</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>

<script type="text/javascript">

//終了日月末設定
function set_last_day() {
	day = document.mainform.select_end_day.value;
	day = parseInt(day,10);
	if (day >= 28) {
		year = document.mainform.select_end_yr.value;
		month = document.mainform.select_end_mon.value;
		last_day = days_in_month(year, month);
		if (last_day != day) {
			day = last_day;
			dd = (day < 10) ? '0'+day : day;
			document.mainform.select_end_day.value = dd;
		}
	}

}
//月末取得
function days_in_month(year, month) {
	//月別日数配列
	lastDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31,29);
	//閏年
	wk_year = parseInt(year,10);
	wk_mon = parseInt(month,10);
	if ((wk_mon == 2) &&
		((wk_year % 4) == 0 && ((wk_year % 100) != 0 || (wk_year % 400)))) {
		wk_mon = 13;
	}

	days = lastDay[wk_mon-1];
	return days;
}
//一覧表示
function print(layout_id) {
    //帳票未指定
    if (layout_id == '') {
        return;
    }
    //別条件で表示中
    if (document.mainform._shift_group_id.disabled) {
        return;
    }
    //期間チェック
    chk_year = parseInt(document.mainform.select_start_yr.value, 10)+1;
    chk_date = chk_year+document.mainform.select_start_mon.value+document.mainform.select_start_day.value;
    end_date = document.mainform.select_end_yr.value+document.mainform.select_end_mon.value+document.mainform.select_end_day.value;
    if (chk_date <= end_date) {
        if (!confirm('期間が1年を超えています。続けてもよろしいですか？')) {
            return false;
        }
    }
    <? /* shift_group_id:画面遷移引継ぎ用hidden _shift_group_id:プルダウン用（disabledにするため別の名前にする） */ ?>
    document.mainform.shift_group_id.value = document.mainform._shift_group_id.value;
    document.mainform._shift_group_id.disabled = true;
	var dataoutbtn = document.getElementById('dataoutbtn');
    if (dataoutbtn) {
        dataoutbtn.disabled = true;
    }
	var excelbtn = document.getElementById('excelbtn');
    if (excelbtn) {
        excelbtn.disabled = true;
    }
    
    document.mainform.print_flg.value = '1';
    document.mainform.csv_layout_id.value = layout_id;
    document.mainform.submit();
}
//ダウンロード
function downloadCSV() {

	if (document.mainform._shift_group_id) {
		document.csv.shift_group_id.value = document.mainform._shift_group_id.value;
	}
	document.csv.csv_layout_id.value = document.mainform.csv_layout_id.value;
	//document.csv.yyyymm.value = document.mainform._yyyymm2.value;
	//期間指定
	document.csv.select_start_yr.value = document.mainform.select_start_yr.value;
	document.csv.select_start_mon.value = document.mainform.select_start_mon.value;
	document.csv.select_start_day.value = document.mainform.select_start_day.value;
	document.csv.select_end_yr.value = document.mainform.select_end_yr.value;
	document.csv.select_end_mon.value = document.mainform.select_end_mon.value;
	document.csv.select_end_day.value = document.mainform.select_end_day.value;
	document.csv.total_flg.value = (document.mainform.total_flg.checked) ? 2 : 1;

	document.csv.action = 'work_admin_csv_download.php';
	document.csv.target = 'download';
	document.csv.submit();

}
///-----------------------------------------------------------------------------
//Excel出力
///-----------------------------------------------------------------------------
function makeExcel() {
	if (document.mainform.shift_group_id) {
		document.csv.shift_group_id.value = document.mainform._shift_group_id.value;
	}
	document.csv.csv_layout_id.value = document.mainform.csv_layout_id.value;
	//期間指定
	document.csv.select_start_yr.value = document.mainform.select_start_yr.value;
	document.csv.select_start_mon.value = document.mainform.select_start_mon.value;
	document.csv.select_start_day.value = document.mainform.select_start_day.value;
	document.csv.select_end_yr.value = document.mainform.select_end_yr.value;
	document.csv.select_end_mon.value = document.mainform.select_end_mon.value;
	document.csv.select_end_day.value = document.mainform.select_end_day.value;
	document.csv.total_flg.value = (document.mainform.total_flg.checked) ? 2 : 1;

	document.csv.action = 'duty_shift_mprint_excel.php';
	document.csv.target = 'download';
	document.csv.submit();

}
//改頁
function pagechg(flg) {
    //flg:-1 prev
    //flg:1 next
    document.mainform.page.value = parseInt(document.mainform.page.value) + flg;
    print(document.mainform.csv_layout_id.value);
}
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
<?
// 画面遷移
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_user_title($session, $section_admin_auth);			//duty_shift_common.ini
echo("</table>\n");

// タブ
$arr_option = "&group_id=" . $group_id;
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_user_tab_menuitem($session, $fname, $arr_option);	//duty_shift_user_tab_common.ini
echo("</table>\n");

// 下線
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
	</td></tr></table>
	<?
		if ($err_msg_1 != "") {
			echo($err_msg_1);
			echo("<br>\n");
			echo("<form name=\"mainform\" method=\"post\">\n");
			echo("</form>");
} else {
	?>	<!-- ------------------------------------------------------------------------ -->
	<!-- 表 -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="mainform" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- （見出し） -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="500">

    <?
    echo("<tr height=\"22\"> \n");
    echo("<td width=\"100\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">シフトグループ</font> \n");
    echo("</td> \n");
    echo("<td> \n");
    
    echo("<select name=\"_shift_group_id\" id=\"_shift_group_id\" onchange=\"document.mainform.page.value = 1;print(document.mainform.csv_layout_id.value);\">");
    if ($is_mprint_staff) {
        echo("<option value=\"-\">全て");
        echo("</option> \n");
    }
    for ($i=0; $i<count($group_array); $i++) {
        
        $selected = ($shift_group_id == $group_array[$i]["group_id"]) ? " selected" : "";
        
        echo("<option value=\"{$group_array[$i]["group_id"]}\" $selected>{$group_array[$i]["group_name"]}");
        echo("</option> \n");
    }
    echo("</select> \n");
    echo("</td> \n");
    echo("</tr> \n");
    
    //初期時
    if ($print_flg != "1") {
        
        //当月
        $start_year = date("Y");
        $start_month = date("m");
        $start_day = 1;
        $end_year = $start_year;
        $end_month = $start_month;
        $end_day = date("t", mktime(0,0,0, $start_month, 1, $start_year));
        if ($total_flg == "") {
            $total_flg = 1;
        }
    }
    //帳票指定で再表示
    else {
        $start_year = $select_start_yr;
        $start_month = $select_start_mon;
        $start_day = $select_start_day;
        $end_year = $select_end_yr;
        $end_month = $select_end_mon;
        $end_day = $select_end_day;
    }
    
?>
<tr>
<td align="right" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">期間</font>
</td>
<td>
				<select name="select_start_yr">
				<? show_select_years(10, $start_year, false, true); ?>
				</select>/<select name="select_start_mon">
				<? show_select_months($start_month, false); ?>
				</select>/<select name="select_start_day">
				<? show_select_days($start_day, false); ?>
				</select> 〜 <select name="select_end_yr" onchange="set_last_day();">
				<? show_select_years(10, $end_year, false, true); ?>
				</select>/<select name="select_end_mon" onchange="set_last_day();">
				<? show_select_months($end_month, false); ?>
				</select>/<select name="select_end_day">
				<? show_select_days($end_day, false); ?>
				</select>
</td>
</tr>
<tr>
<td align="right" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計単位</font>
</td>
<td>
	<label><input type="checkbox" name="total_flg" value="2"<? if ($total_flg == 2) {e(" checked ");} ?>>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">応援をシフトグループ毎に分割する</font></label>&nbsp;

</td>
</tr>

    <?
    echo("</table> \n");
    
    echo("<table class=\"\" bgcolor=\"#dbeef4\"> \n");
    echo("<tr> \n");
    
    $col_max = 6;
    $layout_name = "";
    for($i=0; $i<count($arr_layout_mst); $i++) {
        if ($arr_layout_mst[$i]['layout_id'] == $csv_layout_id) {
            $layout_name = $arr_layout_mst[$i]['layout_name'];
        }
        if ($i % $col_max == 0) {
            echo("</tr> \n");
            echo("<tr> \n");
        }
        
        // 帳票名称
        echo("<td width=\"160\" align=\"left\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        //echo("<a href=\"javascript:void(0);\" onclick=\"document.mainform.page.value = 1;print({$arr_layout_mst[$i]['layout_id']});\">");
        //echo($arr_layout_mst[$i]['layout_name']);
        //echo("</a>");
        //リンクからボタンに変更 20130820
        echo("<input type=\"button\" value=\"{$arr_layout_mst[$i]['layout_name']}\" onclick=\"document.mainform.page.value = 1;print({$arr_layout_mst[$i]['layout_id']});\">");
        echo("</font></td>\n");
        
    }
    //空白を表示
    $cnt = count($arr_layout_mst);
    if ($cnt % $col_max == 0) {
        $cnt += $col_max;
    }
    
    $max = ceil($cnt / $col_max) * $col_max;
    
    for (;$i<$max; $i++) {
        if ($i % $col_max == 0) {
            echo("</tr> \n");
            echo("<tr> \n");
        }
        echo("<td width=\"160\"><br></td>");
    }
    
    echo("</tr> \n");
 		?>
		</table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- ＨＩＤＤＥＮ -->
	<!-- ------------------------------------------------------------------------ -->
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="print_flg" value="">
	<input type="hidden" name="csv_layout_id" value="<? echo($csv_layout_id); ?>">
    <input type="hidden" name="shift_group_id" value="">
    <input type="hidden" name="page" value="<? echo($page); ?>">

    <?
    //<!-- ------------------------------------------------------------------------ -->
    //<!-- リンククリック、シフトグループ変更時、一覧表示 -->
    //<!-- ------------------------------------------------------------------------ -->
    
    if ($print_flg == "1") {
        
        echo("<table width=\"100%\">");
        echo("<tr height=\"26\">\n");
        echo("<td width=\"280\">\n");
        echo("&nbsp;&nbsp;<b>");
        echo($layout_name);
        echo("</b>");
        echo("</td>\n");
        
        echo("<td width=\"240\" align=\"left\">\n");
        echo("&nbsp;&nbsp;<input type=\"button\" name=\"dataoutbtn\" id=\"dataoutbtn\" value=\"データ出力\" onclick=\"downloadCSV();\">");
        if (phpversion() >= "5.1.6") {
            echo("&nbsp;&nbsp;<input type=\"button\" name=\"excelbtn\" id=\"excelbtn\" value=\"EXCEL出力\" onclick=\"makeExcel();\">");
        }
        echo("</td>\n");
        echo("<td width=\"120\" align=\"left\">\n");
        
        $prev_disabled = ($page == 0) ? " disabled" : "";
        $next_disabled = ($page == ($max_page-1)) ? " disabled" : "";
        echo("&nbsp;<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">1頁");
        $limit_array = array(20, 30, 50, 100);
        echo("<select name=\"limit\" id=\"limit\" onchange=\"document.mainform.page.value = 1;print(document.mainform.csv_layout_id.value);\">");
        for ($i=0; $i<count($limit_array); $i++) {
            
            $selected = ($limit == $limit_array[$i]) ? " selected" : "";
            
            echo("<option value=\"{$limit_array[$i]}\" $selected>{$limit_array[$i]}");
            echo("</option> \n");
        }
        echo("</select>");
        echo("件");
        
        echo("&nbsp;&nbsp;");
        echo("</td>\n");
        echo("<td align=\"left\">\n");
        show_page_area($max_page, $page);
        echo("</td>\n");
        echo("<tr>\n");
        echo("</table>\n");
        
        echo("<table  width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" class=\"list\">");
        for ($i=0; $i<count($arr_dat); $i++) {
            
            if ($i == 0) {
                $wk_bgcolor = "#4f81bd";
                $wk_color = "#ffffff";
                $wk_height = "32";
            }
            else {
                $wk_bgcolor = ($i % 2) ? "#d0d8e8" : "#e9edf4";
                $wk_color = "#000000";
                $wk_height = "22";
            }
            echo("<tr height=\"$wk_height\" bgcolor=\"$wk_bgcolor\" >");
            
            for ($j=0; $j<count($arr_csv_list); $j++) {
                //職員ID、氏名、所属は左詰めとする
                if ($i != 0 && (($arr_csv_list[$j] >= "1_0" && $arr_csv_list[$j] <= "1_4") || $arr_csv_list[$j] == "1_7")) {
                    $wk_align = "left";
                }
                else {
                    $wk_align = "center";
                }
                echo("<td align=\"{$wk_align}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\">");
                if ($i == 0) {
                    echo("<b>");
                }
                echo($arr_dat[$i][$j]);
                if ($i == 0) {
                    echo("</b>");
                }
                echo("</font></td>");
            }
            echo("</tr>\n");
        }
        echo("</table>");
    }
    
?>
</form>
<form name="csv" method="post" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id_list" value="">
<input type="hidden" name="yyyymm" value="<? echo($c_yyyymm); ?>">
<input type="hidden" name="group_id" value="">
<input type="hidden" name="csv_layout_id" value="">

<input type="hidden" name="shift_group_id" value="">
<input type="hidden" name="select_start_yr" value="">
<input type="hidden" name="select_start_mon" value="">
<input type="hidden" name="select_start_day" value="">
<input type="hidden" name="select_end_yr" value="">
<input type="hidden" name="select_end_mon" value="">
<input type="hidden" name="select_end_day" value="">
<input type="hidden" name="total_flg" value="">
<input type="hidden" name="from_flg" value="2">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<? } ?>

</td>
</tr>
</table>

<script language="JavaScript">
<? //シフトグループ変更後と表示中に無効化、表示後有効化 ?>
	var shift_group_id = document.getElementById('_shift_group_id');
    if (shift_group_id) {
        shift_group_id.disabled = false;
    }
	var dataoutbtn = document.getElementById('dataoutbtn');
    if (dataoutbtn) {
        dataoutbtn.disabled = false;
    }
	var excelbtn = document.getElementById('excelbtn');
    if (excelbtn) {
        excelbtn.disabled = false;
    }

</script>

</body>
<? pg_close($con); ?>
</html>
<?

/**
 * 頁表示
 *
 * @param mixed $page_max 最大頁数
 * @param mixed $page 頁
 * @return なし
 *
 */
function show_page_area($page_max,$page)
{
	?>
	<script type="text/javascript">

	//ページ遷移します。
	function page_change(page)
	{
		document.mainform.page.value=page;
    print(document.mainform.csv_layout_id.value);
    //document.mainform.submit();
	}

	</script>

	<?
	if($page_max > 1)
	{
	?>
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td>
        <table border="0" cellpadding="0" cellspacing="1">
        <tr>
		  
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?

		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?


		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
        </tr>
        </table>
        		</td>
        		</tr>
        </table>
	<?
	}
	?>
	<?
}

?>
