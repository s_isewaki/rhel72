<?
require_once("about_comedix.php");
?>
<form name="items" action="facility_reservation_update.php" method="post">
<body>
<input type="hidden" name="back" value="t">
<input type="hidden" name="cate_id" value="<? if ($syncopt == "1") {echo($cate_id);} else {echo($cate_id_old);} ?>">
<input type="hidden" name="facility_id" value="<? if ($syncopt == "1") {echo($facility_id);} else {echo($facility_id_old);} ?>">
<input type="hidden" name="year" value="<? if ($rptopt == "1") {echo($year);} else {echo($year_old);} ?>">
<input type="hidden" name="month" value="<? if ($rptopt == "1") {echo($month);} else {echo($month_old);} ?>">
<input type="hidden" name="day" value="<? if ($rptopt == "1") {echo($day);} else {echo($day_old);} ?>">
<input type="hidden" name="s_hour" value="<? echo($s_hour); ?>">
<input type="hidden" name="s_minute" value="<? echo($s_minute); ?>">
<input type="hidden" name="e_hour" value="<? echo($e_hour); ?>">
<input type="hidden" name="e_minute" value="<? echo($e_minute); ?>">
<input type="hidden" name="title" value="<? eh($title); ?>">
<input type="hidden" name="marker" value="<? echo($marker); ?>">
<input type="hidden" name="content" value="<? eh($content); ?>">
<input type="hidden" name="users" value="<? echo($users); ?>">
<input type="hidden" name="path" value="<? echo($path); ?>">
<input type="hidden" name="range" value="<? echo($range); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="rsv_id" value="<? echo($rsv_id); ?>">
<input type="hidden" name="pt_ids" value="<? echo($pt_ids); ?>">
<input type="hidden" name="pt_nms" value="<? echo($pt_nms); ?>">
<input type="hidden" name="rptopt" value="<? echo($rptopt); ?>">
<input type="hidden" name="syncopt" value="<? echo($syncopt); ?>">
<input type="hidden" name="schdopt" value="<? echo($schdopt); ?>">
<input type="hidden" name="repeat_flg" value="<? echo($repeat_flg); ?>">
<input type="hidden" name="sync_flg" value="<? echo($sync_flg); ?>">
<input type="hidden" name="reg_time" value="<? echo($reg_time); ?>">
<input type="hidden" name="reg_time" value="<? echo($reg_time); ?>">
<input type="hidden" name="original_timeless" value="<? echo($timeless); ?>">
<input type="hidden" id="target_id_list" name="target_id_list" value="<? echo($target_id_list); ?>">
<input type="hidden" id="old_target_id_list" name="old_target_id_list" value="<? echo($old_target_id_list); ?>">
<input type="hidden" name="reg_emp_flg" value="<? echo($reg_emp_flg); ?>">
<input type="hidden" name="emp_ext" value="<? echo($emp_ext); ?>">
<?
/*
<input type="hidden" name="schd_flg" value="<? echo($schd_flg); ?>">
*/
foreach ($type_ids as $type_id) {
    echo("<input type=\"hidden\" name=\"type_ids[]\" value=\"$type_id\">\n");
}
?>
</form>
<?
require_once("facility_common.ini");
require_once("schedule_repeat_common.php");
require_once("schedule_common.ini");
define('SM_PATH','webmail/');
require_once("webmail/config/config.php");
require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("aclg_set.php");

// ファイル名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック

//// 日付の妥当性チェック
if ($rptopt == "1") {
    if (!checkdate($month, $day, $year)) {
        echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>");
//      echo("<script type=\"text/javascript\">document.items.submit();</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

//// 時刻の妥当性チェック
$s_time = "$s_hour$s_minute";
$e_time = "$e_hour$e_minute";
if ($timeless != "t") {
    if ($s_time >= $e_time) {
        echo("<script type=\"text/javascript\">alert('終了時刻は開始時刻より後にしてください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

//// タイトル未入力チェック
if ($title == "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

//// タイトルの長さチェック
if (strlen($title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

//if ($target_id_list == "") {
//  echo("<script type=\"text/javascript\">alert('登録対象者が選択されていません。');</script>");
//  echo("<script type=\"text/javascript\">history.back();</script>");
//  exit;
//}

//// 利用人数のチェック
if ($users != "") {
    if (preg_match("/^[0-9]{1,4}$/", $users) == 0) {
        echo("<script type=\"text/javascript\">alert('利用人数は半角数字1〜4桁で入力してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

// スケジュール同時登録フラグ設定
$schd_flg = ($target_id_list != "") ? "t" : "f" ;

if ($timeless != "t") {
    $timeless = "f";
}
if ($original_timeless != "t") {
    $original_timeless = "f";
}

$conf = new Cmx_SystemConfig();
//承認依頼有無フラグ
$aprv_ctrl = $conf->get("schedule.aprv_ctrl");	

// ログインユーザの職員IDを取得
$sql = "select emp_id, emp_lt_nm, emp_ft_nm, get_mail_login_id(emp_id) from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query("rollback");
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_login_id = pg_fetch_result($sel, 0, 3);

// 予約可能期間の確認
// 繰返しなし
if ($repeat_flg == "f" || $rptopt == "1") {
    $arr_date = array($year.$month.$day);
} else {
    $tmp_arr_date = get_date_from_reservations($con, $rsv_id, $login_emp_id, $reg_time, $original_timeless, $rptopt, $repeat_flg, $year_old, $month_old, $day_old, $cate_id_old, $facility_id_old, $fname);
    $arr_date = array();
    foreach ($tmp_arr_date as $tmp_date) {
        $arr_date[] = str_replace("-", "", $tmp_date);
    }
}
// 同時予約なし
if ($sync_flg == "f" || $syncopt == "1") {
    list($ret, $term) = check_term($con, $fname, $facility_id, $arr_date, $login_emp_id);
    if (!$ret) {
        echo("<script type=\"text/javascript\">alert('予約可能期間内($term)の日付を指定してください。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
} else {
// 同時予約あり
    $arr_facility_id = get_facilities_from_reservations($con, $rsv_id, $emp_id, $reg_time, $original_timeless, $year_old, $month_old, $day_old, $fname);
    foreach ($arr_facility_id as $tmp_facility_id) {
        list($ret, $term) = check_term($con, $fname, $tmp_facility_id, $arr_date, $login_emp_id);
        if (!$ret) {
            echo("<script type=\"text/javascript\">alert('予約可能期間内($term)の日付を指定してください。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
            exit;
        }
    }
}
// テンプレートの場合XML形式のテキスト$contentを作成
if ($content_type == "2") {
    $fclhist_info = get_fclhist($con, $fname, $facility_id, $fclhist_id);
    $ext = ".php";
    $savexmlfilename = "fcl/tmp/{$session}_x{$ext}";

    // ワークフロー情報取得
    $tmpl_content = $fclhist_info["fclhist_content"];
    $tmpl_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $tmpl_content);
    $fp = fopen($savexmlfilename, "w");
    if (!$fp) {
        echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、登録してください。$savexmlfilename');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
    if(!fwrite($fp, $tmpl_content_xml, 2000000)) {
        fclose($fp);
        echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、登録してください。');</script>");
        echo("<script language='javascript'>history.back();</script>");
        exit;
    }
    fclose($fp);

    include( $savexmlfilename );

    // 個人スケジュール用内容編集
    $schd_content = get_text_from_xml($content);
} else {
// テキストの場合
    $schd_content = $content;
}

// トランザクションの開始
pg_query($con, "begin transaction");

$date = "$year$month$day";

//重複許可フラグ取得 "t" or "f"
$double_flg = get_double_flg($con, $fname, $cate_id);

// 重複許可フラグが"しない"の場合のみ、重複チェック
// SELECTがヒットしたら重複
if ($timeless != "t" and $double_flg == "f") {
    $sql = "SELECT COUNT(reservation_id) FROM reservation r";

    // 繰返しあり(1件のみ以外)、同時予約なし(or 当施設のみ)
    // ※ 日付(date)の変更はない
    if ($repeat_flg == "t" and $rptopt != "1" and ($sync_flg == "f" or $syncopt == "1")) {
        $cond = "WHERE reservation_del_flg='f'";
        $cond .= " AND reg_time='$reg_time'";                               // 同時予約した予約のみ
        $cond .= " AND fclcate_id=$cate_id_old";                            // 該当する施設 (変更前の施設)
        $cond .= " AND facility_id=$facility_id_old";                       // 該当する施設 (変更前の施設)

        // 繰返しオプション 将来の予定のみ
        if ($rptopt == "3") {
            $cond .= " AND date>='$year_old$month_old$day_old'";
        }

        // 重複予約サブクエリ
        $cond .= " AND EXISTS (";
        $cond .= "SELECT reservation_id FROM reservation r2 ";
        $cond .= "WHERE reservation_del_flg='f' ";
        $cond .= " AND fclcate_id=$cate_id";                                // 該当する施設 (変更された施設)
        $cond .= " AND facility_id=$facility_id";                           // 該当する施設 (変更された施設)
        $cond .= " AND r2.date=r.date";                                     // 該当する日 (親クエリと同じ日)
        $cond .= " AND NOT (start_time>='$e_time' OR end_time<='$s_time')"; // 該当する時間 (変更された時間)
        $cond .= " AND r2.reservation_id<>r.reservation_id";                // 変更元の予約以外
        $cond .= ")";
    }

    // 繰返しなし(or 1件のみ)、同時予約あり(全て)
    // ※ 施設(fclcate_id,facility_id)の変更はない
    elseif (($repeat_flg == "f" or $rptopt == "1") and $sync_flg == "t" and $syncopt != "1") {
        $cond = "WHERE reservation_del_flg='f'";
        $cond .= " AND reg_time='$reg_time'";                               // 同時予約した予約のみ
        $cond .= " AND date='$year_old$month_old$day_old'";                   // 該当する日 (変更前の日)

        // 重複予約サブクエリ
        $cond .= " AND EXISTS (";
        $cond .= "SELECT reservation_id FROM reservation r2 ";
        $cond .= "WHERE reservation_del_flg='f' ";
        $cond .= " AND r2.fclcate_id=r.fclcate_id";                         // 該当する施設 (親クエリと同じ施設)
        $cond .= " AND r2.facility_id=r.facility_id";                       // 該当する施設 (親クエリと同じ施設)
        $cond .= " AND r2.date='$date'";                                      // 該当する日 (変更された日)
        $cond .= " AND NOT (start_time>='$e_time' OR end_time<='$s_time')"; // 該当する時間 (変更された時間)
        $cond .= " AND r2.reservation_id<>r.reservation_id";                // 変更元の予約以外
        $cond .= ")";
    }

    // 繰返しあり(1件のみ以外)、同時予約あり(全て)
    // ※ 日付(date)の変更はない
    // ※ 施設(fclcate_id,facility_id)の変更はない
    elseif ($repeat_flg == "t" and $rptopt != "1" and $sync_flg == "t" and $syncopt != "1") {
        $cond = "WHERE reservation_del_flg='f'";
        $cond .= " AND reg_time='$reg_time'";                               // 同時予約した予約のみ

        // 繰返しオプション 将来の予定のみ
        if ($rptopt == "3") {
            $cond .= " AND date>='$year_old$month_old$day_old'";
        }

        // 重複予約サブクエリ
        $cond .= " AND EXISTS (";
        $cond .= "SELECT reservation_id FROM reservation r2 ";
        $cond .= "WHERE reservation_del_flg='f' ";
        $cond .= " AND r2.fclcate_id=r.fclcate_id";                         // 該当する施設 (親クエリと同じ施設)
        $cond .= " AND r2.facility_id=r.facility_id";                       // 該当する施設 (親クエリと同じ施設)
        $cond .= " AND r2.date=r.date";                                     // 該当する日 (親クエリと同じ日)
        $cond .= " AND NOT (start_time>='$e_time' OR end_time<='$s_time')"; // 該当する時間 (変更された時間)
        $cond .= " AND r2.reservation_id<>r.reservation_id";                // 変更元の予約以外
        $cond .= ")";
    }

    // 繰返しなし、同時予約なし
    else {
        $cond = "WHERE reservation_del_flg='f'";
        $cond .= " AND fclcate_id=$cate_id AND facility_id=$facility_id";   // 該当する施設 (変更された施設)
        $cond .= " AND date='$date'";                                         // 該当する日 (変更された日)
        $cond .= " AND NOT (start_time>='$e_time' OR end_time<='$s_time')"; // 該当する時間 (変更された時間)
        $cond .= " AND reservation_id<>$rsv_id";                            // 変更元の予約以外
    }

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query("rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $dup_count = pg_fetch_result($sel, 0, 0);

    // 重複する予約が存在する場合はエラーとする
    if ($dup_count > 0) {
        pg_query("rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('指定された施設・設備は既に予約されています。');</script>");
        echo("<script type=\"text/javascript\">history.back();</script>");
        exit;
    }
}

$facility_name = get_facility_name($con, $facility_id, $fname);

// スケジュール情報を更新
if ($schdopt == "1") {
    $new_emp_id = array();
    $old_emp_id = array();
    // 同時予約したスケジュールIDを取得
    $schd_id = get_sync_schd_id($con, $fname, $rsv_id, $original_timeless);
    // スケジュールデータがある場合
    if ($schd_id != "") {
        // 更新対象スケジュール一覧を取得
        $upd_schedules = get_repeated_schedules($con, $schd_id, $original_timeless, $rptopt, $fname, true);
        // 更新前の職員ID
        $old_emp_id = get_emp_id_from_schedules($con, $schd_id, $original_timeless, $fname);
    }

    // 登録対象職員を配列化
    if ($target_id_list != "") {
        $new_emp_id = array_merge($new_emp_id, split(",", $target_id_list));
    }

    // 職員IDの変更の確認
    $emp_change_flg = false;
    if (count($old_emp_id) == count($new_emp_id)) {
        sort($new_emp_id);
        for ($i=0; $i<count($old_emp_id); $i++) {
            if ($old_emp_id[$i] != $new_emp_id[$i]) {
                $emp_change_flg = true;
                break;
            }
        }
    } else {
        $emp_change_flg = true;
    }

    // 職員IDの変更がない場合
    if ($emp_change_flg == false) {
        // スケジュールを更新
        foreach ($upd_schedules as $tmp_schedule) {
            $tmp_schd_id = $tmp_schedule["id"];
            $tmp_timeless = $tmp_schedule["timeless"];
            $tmp_emp_id = $tmp_schedule["emp_id"];
            $tmp_date = $tmp_schedule["date"];
            $tmp_time = $tmp_schedule["time"];
            $tmp_schd_status = $tmp_schedule["approve_status"];
            // 時間指定の有無が変わらない場合
            if ($tmp_timeless == $timeless) {

                // スケジュール情報を更新
                $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
                $sql = "update $table_name set";
                $cond = "where schd_id = '$tmp_schd_id'";
                $set = array("schd_title", "schd_detail", "marker", "schd_plc");
                $setvalue = array(p($title), p($schd_content), $marker, $facility_name);
                if ($date != "") {
                    array_push($set, "schd_start_date");
                    array_push($setvalue, $date);
                }
                if ($tmp_timeless != "t") {
                    $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
                    $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
                    array_push($set, "schd_start_time_v", "schd_dur_v");
                    array_push($setvalue, "$schd_start_time_v", "$schd_dur_v");
                }
                if ($login_emp_id != $tmp_emp_id) {
                    array_push($set, "schd_status");
                    if ($timeless == "f") {
                        $new_time = "{$schd_start_hrs}:{$schd_start_min}〜{$schd_dur_hrs}:{$schd_dur_min}";
                    } else {
                        $new_time = "時刻指定なし";
                    }
                    // 日時変更あり
                    if (($date != "" && $date != str_replace("/", "", $tmp_date)) ||
                        $new_time != $tmp_time) {
							
						if($aprv_ctrl == "2")
						{
							//承認依頼しない(管理画面のその他タグで指定)
							array_push($setvalue, "3");
						}
						else
						{
							array_push($setvalue, "2");
						}
						
                    } else {
                    // 日時変更なし
                        array_push($setvalue, $tmp_schd_status);
                    }
                }
                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                if ($upd == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

            // 時間指定の有無が変わる場合
            } else {

                // 更新元のスケジュール情報を取得
                $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
                $sql = "select emp_id, schd_reg_id, schd_status, repeat_flg, reg_time, schd_start_date, schd_place_id from $table_name";
                $cond = "where schd_id = '$tmp_schd_id'";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $emp_id = pg_fetch_result($sel, 0, "emp_id");
                $reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
                $schd_status = pg_fetch_result($sel, 0, "schd_status");
                $tmp_repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
                $reg_time = pg_fetch_result($sel, 0, "reg_time");
                $schd_start_date = pg_fetch_result($sel, 0, "schd_start_date");
                $schd_place_id = pg_fetch_result($sel, 0, "schd_place_id");

                // 更新元のスケジュール情報を削除
                $sql = "delete from $table_name";
                $cond = "where schd_id = '$tmp_schd_id'";
                $del = delete_from_table($con, $sql, $cond, $fname);
                if ($del == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // スケジュールIDを採番
                $new_schd_id = get_schd_id($con, $fname, $timeless);

                // スケジュール情報を登録
                $tmp_date = ($date == "") ? $schd_start_date : $date;
                $new_table_name = ($timeless != "t") ? "schdmst" : "schdmst2";
                $columns = array("schd_id", "emp_id", "schd_title", "schd_plc", "schd_imprt", "schd_start_date", "schd_detail", "schd_type", "schd_place_id", "marker", "schd_reg_id", "repeat_flg", "reg_time");
                $setvalue = array($new_schd_id, $emp_id, p($title), $facility_name, "2", $tmp_date, p($schd_content), "1", $schd_place_id, $marker, $reg_emp_id, $tmp_repeat_flg, $reg_time);
                if ($timeless != "t") {
                    $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
                    $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
                    // 24:00対応
                    $schd_dur = $schd_e_time;
                    if ($schd_dur == "24:00") {
                        $schd_dur = "23:59";
                    }
                    array_push($columns, "schd_start_time", "schd_dur", "schd_start_time_v", "schd_dur_v");
                    array_push($setvalue, "$schd_s_time", "$schd_dur", "$schd_start_time_v", "$schd_dur_v");
                }
                if ($login_emp_id != $tmp_emp_id) {
                    array_push($columns, "schd_status");

					if($aprv_ctrl == "2")
					{
						//承認依頼しない(管理画面のその他タグで指定)
						array_push($setvalue, "3");
					}
					else
					{
						array_push($setvalue, "2");
					}

                } else {
                    array_push($columns, "schd_status");
                    array_push($setvalue, $schd_status);
                }
                $sql = "insert into $new_table_name (" . join(", ", $columns) . ") values (";
                $ins = insert_into_table($con, $sql, $setvalue, $fname);
                if ($ins == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
            }

        }
    } else {
    // 職員IDの変更がある場合
        // スケジュール情報がある場合
        if ($schd_id != "") {
            // 更新元のスケジュール情報を取得
            $table_name = ($original_timeless != "t") ? "schdmst" : "schdmst2";
            $sql = "select emp_id, schd_reg_id, schd_status, repeat_flg, reg_time, schd_start_date ";
            if ($original_timeless != "t") {
                $sql .= " , schd_start_time_v, schd_dur_v ";
            }
            $sql .= " from $table_name";
            $cond = "where schd_id = '$schd_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $reg_emp_id = pg_fetch_result($sel, 0, "schd_reg_id");
            $schd_status = pg_fetch_result($sel, 0, "schd_status");
            $tmp_repeat_flg = pg_fetch_result($sel, 0, "repeat_flg");
            $reg_time = pg_fetch_result($sel, 0, "reg_time");
            $schd_start_date = pg_fetch_result($sel, 0, "schd_start_date");
            if ($original_timeless != "t") {
                $schd_start_time_v = pg_fetch_result($sel, 0, "schd_start_time_v");
                $schd_dur_v = pg_fetch_result($sel, 0, "schd_dur_v");
                $schd_time = "$schd_start_time_v$schd_dur_v";
            }
        } else {
        // スケジュール情報がない場合
            $reg_emp_id = $login_emp_id;
            $tmp_repeat_flg = ($date != "") ? "f" : "t" ;
//          $reg_time = ""; // hiddenから
            $schd_start_date = "";
            if ($timeless != "t") {
                $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
                $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
                $schd_time = "$schd_start_time_v$schd_dur_v";
            }
        }

        // 日付変更
        if ($date != "") {
            if (!in_array($date, $arr_date)) {
                $arr_date[] = $date;
            }
        } else {
            // 設備情報から日付配列を取得
            $arr_date = get_date_from_reservations($con, $rsv_id, $login_emp_id, $reg_time, $original_timeless, $rptopt, $repeat_flg, $year_old, $month_old, $day_old, $cate_id_old, $facility_id_old, $fname);
        }

        $arr_schd_id = array();
        // 時刻指定有無
        $arr_timeless = array();
        // 承認情報
        $arr_schd_status = array();
        foreach ($upd_schedules as $tmp_schedule) {
            $tmp_key = str_replace("/", "-", $tmp_schedule["date"])."_".$tmp_schedule["emp_id"];
            $arr_schd_id["$tmp_key"] = $tmp_schedule["id"];
            $arr_timeless["$tmp_key"] = $tmp_schedule["timeless"];
            $arr_schd_status["$tmp_key"] = $tmp_schedule["approve_status"];
        }

        $new_table_name = ($timeless != "t") ? "schdmst" : "schdmst2";

        // 登録メール用
        $arr_add_emp_id = array();

        // 登録・更新処理
        foreach($arr_date as $tmp_date) {
            foreach($new_emp_id as $tmp_emp_id) {
                if ($date != "") {
                    // 元の日付の情報を取得する
                    $tmp_key = $schd_start_date."_".$tmp_emp_id;
                } else {
                    $tmp_key = $tmp_date."_".$tmp_emp_id;
                }
                $tmp_schd_id = $arr_schd_id["$tmp_key"];
                $tmp_schd_status = $arr_schd_status["$tmp_key"];
                $tmp_timeless = $arr_timeless["$tmp_key"];

                // 時刻指定有無が有り、かつ、変わらない場合は更新する
                if ($tmp_timeless != "" && $tmp_timeless == $timeless) {
                    $table_name = ($tmp_timeless != "t") ? "schdmst" : "schdmst2";
                    $sql = "update $table_name set";
                    $cond = "where schd_id = '$tmp_schd_id'";
                    $set = array("schd_title", "schd_plc", "schd_imprt", "schd_detail", "schd_place_id", "marker");
                    $setvalue = array(p($title), $facility_name, "2", p($schd_content), $place_id, $marker);
                    if ($date != "") {
                        array_push($set, "schd_start_date");
                        array_push($setvalue, $tmp_date);
                    }
                    if ($tmp_timeless != "t") {
                        $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
                        $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
                        array_push($set, "schd_start_time_v", "schd_dur_v");
                        array_push($setvalue, "$schd_start_time_v", "$schd_dur_v");
                        $tmp_time = "$schd_start_time_v$schd_dur_v";
                    }
                    if ($login_emp_id != $tmp_emp_id) {
                        // 日時が変更された場合、未承認にする
                        if (($date != "" && $date != $schd_start_date) || ($tmp_timeless != "t" && $tmp_time != $schd_time)) {
                            array_push($set, "schd_status");
							
							if($aprv_ctrl == "2")
							{
								//承認依頼しない(管理画面のその他タグで指定)
								array_push($setvalue, "3");
							}
							else
							{
								array_push($setvalue, "2");
							}
							
                        }
                    }
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                } else { // 追加登録
                    // スケジュールIDを採番
                    $new_schd_id = get_schd_id($con, $fname, $timeless);

                    $columns = array("schd_id", "emp_id", "schd_title", "schd_plc", "schd_imprt", "schd_start_date", "schd_detail", "schd_type", "schd_place_id", "marker", "schd_reg_id", "repeat_flg", "reg_time");
                    $setvalue = array($new_schd_id, $tmp_emp_id, p($title), $facility_name, "2", $tmp_date, p($schd_content), "1", $place_id, $marker, $reg_emp_id, $tmp_repeat_flg, $reg_time);
                    if ($timeless != "t") {
                        $schd_start_time_v = substr($schd_s_time, 0, 2).substr($schd_s_time, 3, 2);
                        $schd_dur_v = substr($schd_e_time, 0, 2).substr($schd_e_time, 3, 2);
                        // 24:00対応
                        $schd_dur = "$schd_e_time";
                        if ($schd_dur == "24:00") {
                            $schd_dur = "23:59";
                        }
                        array_push($columns, "schd_start_time", "schd_dur", "schd_start_time_v", "schd_dur_v");
                        array_push($setvalue, "$schd_s_time", "$schd_dur", "$schd_start_time_v", "$schd_dur_v");
                    }

                    if ($login_emp_id == $tmp_emp_id) {
                        array_push($columns, "schd_status");
                        array_push($setvalue, "1");
                    } else {
                        array_push($columns, "schd_status");
						
						if($aprv_ctrl == "2")
						{
							//承認依頼しない(管理画面のその他タグで指定)
							array_push($setvalue, "3");
						}
						else
						{
							array_push($setvalue, "2"); // 未承認
						}
						
                    }

                    $sql = "insert into $new_table_name (" . join(", ", $columns) . ") values (";
                    $ins = insert_into_table($con, $sql, $setvalue, $fname);
                    if ($ins == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                    // 登録メール用
                    $arr_add_emp_id[] = $tmp_emp_id;
                }
            }
        }

        $schedules_for_del_mail = array();
        // 更新元のスケジュール情報を削除
        foreach ($upd_schedules as $tmp_schedule) {
            // 登録対象の職員以外を削除
            $tmp_emp_id = $tmp_schedule["emp_id"];
            if (in_array($tmp_emp_id, $new_emp_id) && ($tmp_schedule["timeless"] == $timeless)) { // 時刻指定が変更されていないことを条件に追加
                continue;
            }
            $tmp_table_name = ($tmp_schedule["timeless"] == "f") ? "schdmst" : "schdmst2";
            $tmp_schd_id = $tmp_schedule["id"];

            $sql = "delete from $tmp_table_name";
            $cond = "where schd_id = '$tmp_schd_id'";
            $del = delete_from_table($con, $sql, $cond, $fname);
            if ($del == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            // 削除メール用
            if ($tmp_schedule["approve_status"] == "3") {
                $schedules_for_del_mail[] = $tmp_schedule;
            }
        }

    }

}

// 更新用条件
$table_name = ($original_timeless != "t") ? "reservation" : "reservation2";
// 繰返しなし、同時予約なし
if ($repeat_flg == "f" && $sync_flg == "f") {
    $cond = "where reservation_id = $rsv_id";
} else {
    $cond = "where emp_id = (select emp_id from $table_name where reservation_id = '$rsv_id') ";
    $cond .= " and reservation_del_flg = 'f' ";
    $cond .= " and reg_time = '$reg_time' ";
    // 繰返しオプション
    if ($repeat_flg == "f" || $rptopt == "1") {
        // 1件のみ
        $cond .= " and date = '$year_old$month_old$day_old' ";
    } else if ($rptopt == "2") {
        // 全て
        ;
    } else if ($rptopt == "3") {
        // 将来の予定のみ
        $cond .= " and date >= '$year_old$month_old$day_old' ";
    }

    // 同時予約オプション 当施設・設備のみ
    if ($sync_flg == "f" || $syncopt == "1") {
        $cond .= " and fclcate_id = $cate_id_old and facility_id = $facility_id_old ";
    }

}

$table_name2 = ($original_timeless != "t") ? "rsvpt" : "rsvpt2";
// 予約患者情報をDELETE
$sql = "delete from $table_name2";
$cond_del = "where reservation_id in (select reservation_id from $table_name " .$cond .")";
$del = delete_from_table($con, $sql, $cond_del, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 日付、設備分のreservation_id取得、患者情報更新用
$arr_rsv = array();
$sql = "select reservation_id from $table_name";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query("rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$arr_rsv = pg_fetch_all($sel);

// 種別を更新
if ($show_type_flg == "t") {
    $type1 = isset($type_ids[0]) ? $type_ids[0] : null;
    $type2 = isset($type_ids[1]) ? $type_ids[1] : null;
    $type3 = isset($type_ids[2]) ? $type_ids[2] : null;
} else {
    $type1 = null;
    $type2 = null;
    $type3 = null;
}
$arr_new_rsv = array();
// 設備予約更新
foreach($arr_rsv as $tmp_rsv) {

    $tmp_rsv_id = $tmp_rsv["reservation_id"];
    // 時間指定の有無が変わらない場合
    if ($original_timeless == $timeless) {

        // 情報を更新
        $table_name = ($timeless != "t") ? "reservation" : "reservation2";
        $sql = "update $table_name set";
        $cond = "where reservation_id = $tmp_rsv_id";
        $set = array("title", "content", "users", "type1", "type2", "type3", "upd_emp_id", "marker", "pt_name", "schd_flg", "emp_ext");
        $setvalue = array(p($title), p($content), $users, $type1, $type2, $type3, $login_emp_id, $marker, $pt_nms, $schd_flg, $emp_ext);
        // 繰返しなしの場合のみ日付を更新
        if ($repeat_flg == "f" || $rptopt == "1") {
            array_push($set, "date");
            array_push($setvalue, $date);
        }
        // 時刻指定ありの場合のみ開始、終了時刻を更新
        if ($timeless != "t") {
            array_push($set, "start_time");
            array_push($setvalue, $s_time);
            array_push($set, "end_time");
            array_push($setvalue, $e_time);
        }

        // 同時予約なしの場合のみ設備を更新
        if ($sync_flg == "f" || $syncopt == "1") {
            array_push($set, "fclcate_id");
            array_push($setvalue, $cate_id);
            array_push($set, "facility_id");
            array_push($setvalue, $facility_id);
        }

        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

    } else {
    // 時間指定の有無が変わる場合

        $table_name = ($original_timeless != "t") ? "reservation" : "reservation2";
        // 元の情報を取得
        $sql = "select emp_id, date, facility_id from $table_name";
        $cond = "where reservation_id = $tmp_rsv_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $reg_emp_id = pg_fetch_result($sel, 0, "emp_id");
        if ($rptopt == "1") {
            $date = "$year$month$day";
        } else {
            $date = pg_fetch_result($sel, 0, "date");
        }
        // 同時予約の場合のみ設備を取得
        if ($syncopt == "2") {
            $facility_id = pg_fetch_result($sel, 0, "facility_id");
        }
        // 元の情報を削除
        $sql = "delete from $table_name";
        $cond = "where reservation_id = '$tmp_rsv_id'";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        if ($fclhist_id == "") {
            $fclhist_id = 0;
        }
        // 予約情報を登録
        if ($timeless != "t") {
            $reservation_id = get_reservation_id($con, $fname);
            $sql = "insert into reservation (reservation_id, fclcate_id, facility_id, emp_id, date, start_time, end_time, title, content, users, type1, type2, type3, upd_emp_id, marker, repeat_flg, schd_flg, sync_flg, reg_time, fclhist_id, pt_name) values (";
            $contents = array($reservation_id, $cate_id, $facility_id, $reg_emp_id, $date, $s_time, $e_time, p($title), p($content), $users, $type1, $type2, $type3, $login_emp_id, $marker, $repeat_flg, $schd_flg, $sync_flg, $reg_time, $fclhist_id, $pt_nms);
        } else {
            $reservation_id = get_reservation_id2($con, $fname);
            $sql = "insert into reservation2 (reservation_id, fclcate_id, facility_id, emp_id, date, title, content, users, type1, type2, type3, upd_emp_id, marker, repeat_flg, schd_flg, sync_flg, reg_time, fclhist_id, pt_name) values (";
            $contents = array($reservation_id, $cate_id, $facility_id, $reg_emp_id, $date, p($title), p($content), $users, $type1, $type2, $type3, $login_emp_id, $marker, $repeat_flg, $schd_flg, $sync_flg, $reg_time, $fclhist_id, $pt_nms);
        }
        $ins = insert_into_table($con, $sql, $contents, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        // 新IDを配列に保存
        $tmp_rsv["reservation_id"] = $reservation_id;
        array_push($arr_new_rsv, $tmp_rsv);

    }

}

// 予約患者情報をINSERT
$pt_ids = ($pt_ids != "") ? split(",", $pt_ids) : array();

// 時刻指定有無の変更時は、$arr_rsvを別テーブル用新IDにする
if ($original_timeless != $timeless) {
    $arr_rsv = $arr_new_rsv;
}

$table_name2 = ($timeless != "t") ? "rsvpt" : "rsvpt2";

// 日付・設備数分のreservation_id、繰返し
if (count($pt_ids) > 0) {
    foreach($arr_rsv as $tmp_rsv){
        $tmp_rsv_id = $tmp_rsv["reservation_id"];

        $order_no = 1;
        foreach ($pt_ids as $tmp_pt_id) {
            $sql = "insert into $table_name2 (reservation_id, ptif_id, order_no) values (";
            $contents = array($tmp_rsv_id, $tmp_pt_id, $order_no);
            $ins = insert_into_table($con, $sql, $contents, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $order_no++;
        }
    }
}

// トランザクションのコミット
pg_query($con, "commit");

// 行先一覧を配列に格納
if (($sendmail == "t" && count($arr_add_emp_id) > 0) ||
    (count($schedules_for_del_mail) > 0)) {
    $places = get_places($con, $fname);
}

if ($sendmail == "t") {
// 登録メール
    if (count($arr_add_emp_id) > 0) {
        $arr_add_emp_id = array_unique($arr_add_emp_id);
        $to_login_ids = array();
        foreach ($arr_add_emp_id as $tmp_emp_id) {
            // 自分以外の場合
            if ($tmp_emp_id != $login_emp_id) {
                $to_login_ids[] = get_mail_addr($con, $fname, $tmp_emp_id, $domain);
            }
        }
        $mail_schd_date_times = array();
        for ($i = 0, $j = count($arr_date); $i < $j; $i++) {
            $tmp_date = $arr_date[$i];
            $mail_schd_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $tmp_date);
            if ($i == 0) {
                $mail_schd_time = ($timeless == "t") ? "時刻指定なし" : "{$s_hour}:{$s_minute}〜{$e_hour}:{$e_minute}";
            } else {
                $mail_schd_time = "　〃";
            }
            $mail_schd_date_times[] = "$mail_schd_date $mail_schd_time";
        }
        $mail_schd_date_time = implode("\n　　　　　", $mail_schd_date_times);
        $mail_schd_detail = str_replace("\n", "\n　　　　　", $schd_detail);
        $subject = "[CoMedix] スケジュール登録のお知らせ";

        $message = "下記のスケジュールが登録されました。\n\n";
        $message .= "登録者　：{$emp_nm}\n";
        $message .= "タイトル：{$title}\n";
        $message .= "行先　　：{$facility_name}\n";
        $message .= "日時　　：{$mail_schd_date_time}\n";
        $message .= "詳細　　：{$schd_content}\n";
        $message = mb_convert_kana($message, "KV"); // 半角カナ変換
        $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";

        foreach ($to_login_ids as $to_login_id) {
            mb_send_mail($to_login_id, $subject, $message, $additional_headers);
        }

    }
}
// 削除メール
if (count($schedules_for_del_mail) > 0) {
    usort($schedules_for_del_mail, "sort_by_emp_id_datetime");

    $subject = "[CoMedix] スケジュール削除のお知らせ";
    $additional_headers = "From: ".mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$emp_login_id@$domain>";
    $message = "下記の承認済みスケジュールが削除されました。\n\n";
    $message .= "登録者　：{$emp_nm}\n";

    // 職員IDをキーとして処理
    $old_emp_id = "";

    foreach ($schedules_for_del_mail as $tmp_schedule) {

        $tmp_emp_id = $tmp_schedule["emp_id"];
        // 初回のみキー設定
        if ($old_emp_id == "") {
            $old_emp_id = $tmp_emp_id;
        }

        // 職員が変わった場合
        if ($old_emp_id != $tmp_emp_id) {
            // メールアドレス取得
            $to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
            // メール送信
            mb_send_mail($to_addr, $subject, $message, $additional_headers);
            // キー設定
            $old_emp_id = $tmp_emp_id;
            // 初期化
            $message = "下記の承認済みスケジュールが削除されました。\n\n";
            $message .= "登録者　：{$emp_nm}\n";
        }

        $mail_schd_detail = str_replace("\n", "\n　　　　　", $tmp_schedule["detail"]);
        $message .= "-----------------------------------------------------\n";
        $message .= "タイトル：{$tmp_schedule["title"]}\n";
        $message .= "行先　　：{$facility_name}\n";
        $message .= "日時　　：{$tmp_schedule["date"]} {$tmp_schedule["time"]}\n";
        $message .= "詳細　　：{$schd_content}\n";
        $message = mb_convert_kana($message, "KV"); // 半角カナ変換
    }

    // メールアドレス取得
    $to_addr = get_mail_addr($con, $fname, $old_emp_id, $domain);
    // メール送信
    mb_send_mail($to_addr, $subject, $message, $additional_headers);
}

// データベース接続を閉じる
pg_close($con);

// 親画面を更新
if ($rptopt >= "2") {
    $year = "$year_old";
    $month = "$month_old";
    $day = "$day_old";
}
$date = mktime(0, 0, 0, $month, $day, $year);

if ($syncopt == "2") {
    $cate_id = $cate_id_old;
    $facility_id = $facility_id_old;
}

switch ($path) {
case "1":
    $file = "facility_menu.php?session=$session&range=$range&date=$date";
    break;
case "2":
    $file = "facility_week.php?session=$session&range=$range&date=$date";
    break;
case "3":
    $file = "facility_month.php?session=$session&range=$range&date=$date";
    break;
case "4":
    $file = "facility_year.php?session=$session&range=$range&date=$date";
    break;
case "5":
    $file = "facility_week_chart.php?session=$session&range=$range&date=$date&cate_id=$cate_id&fcl_id=$facility_id";
    break;
case "6":
    $file = "left.php?session=$session";
    break;
}
echo("<script type=\"text/javascript\">opener.location.href = '$file';self.close();</script>");
?>
</body>
<?
function get_mail_addr($con, $fname, $emp_id, $domain) {
    $sql = "select get_mail_login_id(emp_id) as mail_login_id, emp_lt_nm, emp_ft_nm from empmst";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $mail_addr = pg_fetch_result($sel, 0, "mail_login_id");
    $emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . pg_fetch_result($sel, 0, "emp_ft_nm");
    $to_addr = mb_encode_mimeheader (mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <$mail_addr@$domain>";
    return $to_addr;
}

// 同時予約された日付を取得する
//20110930 不具合修正
//function get_date_from_reservations($con, $rsv_id, $emp_id, $reg_time, $timeless, $repeat_type, $repeat_flg, $year_old, $month_old, $day_old, $cate_id_old, $facility_id_old, $fname) {
function get_date_from_reservations($con, $rsv_id, $emp_id, $reg_time, $timeless, $rptopt, $repeat_flg, $year_old, $month_old, $day_old, $cate_id_old, $facility_id_old, $fname) {

    $table_name = ($timeless != "t") ? "reservation" : "reservation2";
    $sql = "select date from $table_name";

    // 登録時の繰返し状態
    if ($repear_flg == "f") {
        $cond = "where reservation_id = $rsv_id";
    } else {
        $cond = "where emp_id = (select emp_id from $table_name where reservation_id = '$rsv_id') ";
        $cond .= " and reservation_del_flg = 'f' ";
        $cond .= " and reg_time = '$reg_time' ";
        // 繰返しオプション
        if ($repeat_flg == "f" || $rptopt == "1") {
            // 1件のみ
            $cond .= " and date = '$year_old$month_old$day_old' ";
        } else if ($rptopt == "2") {
            // 全て
            ;
        } else if ($rptopt == "3") {
            // 将来の予定のみ
            $cond .= " and date >= '$year_old$month_old$day_old' ";
        }
    }

    $cond .= " and fclcate_id = $cate_id_old and facility_id = $facility_id_old ";
    $cond .= " order by date ";
//echo("sql=$sql cond=$cond");
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $tmp_date = $row["date"];
        $arr_date[] = substr($tmp_date, 0, 4)."-".substr($tmp_date, 4, 2)."-".substr($tmp_date, 6, 2);
    }

    return $arr_date;
}

// 同時予約された設備を取得する
function get_facilities_from_reservations($con, $rsv_id, $emp_id, $reg_time, $timeless, $year_old, $month_old, $day_old, $fname) {
    $arr_facilities = array();

    $table_name = ($timeless != "t") ? "reservation" : "reservation2";
    $sql = "select r.facility_id from $table_name r inner join facility f on r.facility_id = f.facility_id";
    $cond = "where r.emp_id = (select emp_id from $table_name where reservation_id = '$rsv_id') ";
    $cond .= " and r.reservation_del_flg = 'f' ";
    $cond .= " and r.reg_time = '$reg_time' ";
    $cond .= " and r.date = '$year_old$month_old$day_old' ";
    $cond .= " order by f.order_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $arr_facilities[] = $row["facility_id"];
    }

    return $arr_facilities;

}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
    $y = substr($yyyymmdd, 0, 4);
    $m = substr($yyyymmdd, 4, 2);
    $d = substr($yyyymmdd, 6, 2);
    return mktime(0, 0, 0, $m, $d, $y);
}
