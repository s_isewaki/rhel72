<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cas_application_workflow_common_class.php");
require_once("cas_application_workflow_select_box.ini");
require_once("cas_application_common.ini");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CAS_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);
//====================================
// 初期処理
//====================================
if($level == "")
{
    $level = "2";
}

//一画面内の最大表示件数
$disp_max_page = 15;

if($page == "")
{
	$page = 1;
}

$arr_empmst = $obj->get_empmst($session);
$login_emp_id = $arr_empmst[0]["emp_id"];
$is_operator_employee = $obj->is_operator_employee($login_emp_id);

$arr_condition = array(
                        "emp_nm"    => $search_apply_emp_nm,
                        "level"     => $level,
                        "class"     => $search_emp_class,
                        "attribute" => $search_emp_attribute,
                        "dept"      => $search_emp_dept,
                        "room"      => $search_emp_room,
						"page"      => $page,
						"max_page"  => $disp_max_page,
						"login_emp_id" => ($is_operator_employee ? "" : $login_emp_id)
                       );

// 認定スケジュール情報一覧件数取得
$recognize_schedule_list_count = $obj->get_cas_recognize_schedule_list_count($arr_condition);

// 認定スケジュール情報取得
$arr_recognize_schedule = $obj->get_cas_recognize_schedule_list($arr_condition);


$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cas_title?> | 認定スケジュール</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function search_schedule()
{
	document.apply.action="cas_recognize_schedule_list.php?session=<?=$session?>";
	document.apply.submit();
}


function highlightCells(class_name) {
	changeCellsColor(class_name, '#ffff66');
}

function dehighlightCells(class_name) {
	changeCellsColor(class_name, '');
}

function changeCellsColor(class_name, color) {
	var cells = document.getElementsByTagName('td');
	for (var i = 0, j = cells.length; i < j; i++) {
		if (cells[i].className != class_name) {
			continue;
		}
		cells[i].style.backgroundColor = color;
	}
}

function show_sub_window(url) {
	var h = '600';
	var w = '880';
	var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	window.open(url, 'approvewin',option);
}

// この関数は別画面から呼び出されます。
function reload_page()
{
	document.apply.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cas_application_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cas_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_application_menu.php?session=<? echo($session); ?>"><b><? echo($cas_title); ?></b></a> &gt; <a href="cas_recognize_schedule_list.php?session=<? echo($session); ?>"><b>認定スケジュール</b></a></font></td>
<? if ($workflow_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cas_workflow_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>

<?
show_applycation_menuitem($session,$fname,"");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">

<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<table width="100%" border="0" cellspacing="1" cellpadding="1" class="list">
<tr>
<td>

<table width="100%" border="0" cellspacing="1" cellpadding="2" class="list">
<tr>

<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベル</font></td>
<td><select name="level"><? show_level_options($level); ?></select></td>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者名</font></td>
<td><input type="text" size="40" maxlength="40" name="search_apply_emp_nm" value="<?=htmlspecialchars($search_apply_emp_nm)?>"></td>
</tr>
<tr>
<td bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></td>
<td colspan="3">
<?
show_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj, $login_emp_id, $is_operator_employee);
?>

</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block_in">
<tr align="right"><td>
<input type="button" value="検索" onclick="search_schedule();">
</td></tr>
</table>



</td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">



<tr bgcolor="#E5F6CD">
<td width="110" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者名</font></td>
<td width="70" rowspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願日</font></td>
<td width="140" colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
<td width="280" colspan="4"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育/自己学習能力</font></td>
<td width="70" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font></td>
</tr>

<tr bgcolor="#E5F6CD">
<?if($level == "2") {?>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.1</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.3</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.5</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.6</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価3<br>No.8</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価4<br>No.9</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.10</font></td>
<?}else if($level == "3"){?>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.2</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.4</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.5</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価2<br>No.7</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価3<br>No.8</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価4<br>No.9</font></td>
<td width="70"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価1<br>No.11</font></td>
<?}?>
</tr>
<?
if($recognize_schedule_list_count > 0)
{
	$count = "1";
	foreach($arr_recognize_schedule as $recognize_schedule)
	{
		$apply_id  = $recognize_schedule["apply_id"];
		$apply_date  = $recognize_schedule["apply_date"];

		$emp_lt_nm = $recognize_schedule["emp_lt_nm"];
		$emp_ft_nm = $recognize_schedule["emp_ft_nm"];
		$emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;

		$emp_id    = $recognize_schedule["emp_id"];
		$level     = $recognize_schedule["level"];
		$no1_date  = $recognize_schedule["no1_date"];
		$no2_date  = $recognize_schedule["no2_date"];
		$no3_date  = $recognize_schedule["no3_date"];
		$no4_date  = $recognize_schedule["no4_date"];
		$no5_date  = $recognize_schedule["no5_date"];
		$no6_date  = $recognize_schedule["no6_date"];
		$no7_date  = $recognize_schedule["no7_date"];
		$no8_date  = $recognize_schedule["no8_date"];
		$no9_date  = $recognize_schedule["no9_date"];
		$no10_date = $recognize_schedule["no10_date"];
		$no11_date = $recognize_schedule["no11_date"];
		$no12_date = $recognize_schedule["no12_date"];

        $apply_date = ($apply_date == "") ? "" : substr($apply_date, 0, 4)."/".substr($apply_date, 4, 2)."/".substr($apply_date, 6, 2);
		$no1_date = ($no1_date == "") ? "" : substr($no1_date, 0, 4)."/".substr($no1_date, 4, 2)."/".substr($no1_date, 6, 2);
		$no2_date = ($no2_date == "") ? "" : substr($no2_date, 0, 4)."/".substr($no2_date, 4, 2)."/".substr($no2_date, 6, 2);
		$no3_date = ($no3_date == "") ? "" : substr($no3_date, 0, 4)."/".substr($no3_date, 4, 2)."/".substr($no3_date, 6, 2);
		$no4_date = ($no4_date == "") ? "" : substr($no4_date, 0, 4)."/".substr($no4_date, 4, 2)."/".substr($no4_date, 6, 2);
		$no5_date = ($no5_date == "") ? "" : substr($no5_date, 0, 4)."/".substr($no5_date, 4, 2)."/".substr($no5_date, 6, 2);
		$no6_date = ($no6_date == "") ? "" : substr($no6_date, 0, 4)."/".substr($no6_date, 4, 2)."/".substr($no6_date, 6, 2);
		$no7_date = ($no7_date == "") ? "" : substr($no7_date, 0, 4)."/".substr($no7_date, 4, 2)."/".substr($no7_date, 6, 2);
		$no8_date = ($no8_date == "") ? "" : substr($no8_date, 0, 4)."/".substr($no8_date, 4, 2)."/".substr($no8_date, 6, 2);
		$no9_date = ($no9_date == "") ? "" : substr($no9_date, 0, 4)."/".substr($no9_date, 4, 2)."/".substr($no9_date, 6, 2);
		$no10_date = ($no10_date == "") ? "" : substr($no10_date, 0, 4)."/".substr($no10_date, 4, 2)."/".substr($no10_date, 6, 2);
		$no11_date = ($no11_date == "") ? "" : substr($no11_date, 0, 4)."/".substr($no11_date, 4, 2)."/".substr($no11_date, 6, 2);
		$no12_date = ($no12_date == "") ? "" : substr($no12_date, 0, 4)."/".substr($no12_date, 4, 2)."/".substr($no12_date, 6, 2);


		$onclick_event = "onclick=\"show_sub_window('cas_recognize_schedule_regist.php?session=$session&apply_id=$apply_id');\"";
?>
<tr>
<td width="110" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=htmlspecialchars($emp_full_nm)?></font></td>
<td width="70" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font></td>

<?
if($level == "2")
{
?>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no1_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no3_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no5_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no6_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no8_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no9_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no10_date?></font></td>
<?
} else if($level == "3"){
?>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no2_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no4_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no5_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no7_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no8_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no9_date?></font></td>
<td width="70" align="center" class="schedule_list_<?=$count?>" onmouseover="highlightCells(this.className);this.style.cursor = 'pointer';" onmouseout="dehighlightCells(this.className);this.style.cursor = '';" <?=$onclick_event?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$no11_date?></font></td>
<?
}
?>

</tr>
<?
	$count++;
	}
}
?>

</table>
<?
//最大ページ数
if($recognize_schedule_list_count == 0)
{
	$page_max  = 1;
}
else
{
	$page_max  = floor( ($recognize_schedule_list_count-1) / $disp_max_page ) + 1;
}

show_page_area($page_max,$page);

?>
</form>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
</body>

</html>


<?
function show_level_options($level)
{
	$arr_level_nm = array("レベルII", "レベルIII");
	$arr_level_id = array("2","3");

	for($i=0;$i<count($arr_level_nm);$i++) {

		echo("<option value=\"$arr_level_id[$i]\"");
		if($level == $arr_level_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_level_nm[$i]\n");
	}
}

function show_page_area($page_max,$page)
{
	?>
	<input type="hidden" name="page" value="">
	<script type="text/javascript">

	//ページ遷移します。
	function page_change(page)
	{
		document.apply.page.value=page;
		document.apply.submit();
	}

	</script>

	<?
	if($page_max != 1)
	{
	?>
		<table width="100%">
		<tr>
		<td>
		  <table>
		  <tr>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    先頭へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(1);">
		    先頭へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		<?


		if($page == 1)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    前へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page-1?>);">
		    前へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  <td>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		<?

		for($i=1;$i<=$page_max;$i++)
		{
			if($i == $page)
			{
		?>
		    [<?=$i?>]
		<?
			}
			else
			{
		?>
		    <a href="javascript:page_change(<?=$i?>);">[<?=$i?>]</a>
		<?
			}
		}
		?>
		    </font>
		  </td>
		  <td>
		<?


		if($page == $page_max)
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <span style="color:silver">
		    次へ
		    </span>
		    </font>
		    </nobr>
		<?
		}
		else
		{
		?>
		    <nobr>
		    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
		    <a href="javascript:page_change(<?=$page+1?>);">
		    次へ
		    </a>
		    </font>
		    </nobr>
		<?
		}


		?>
		  </td>
		  </tr>
		  </table>
		</td>
		</tr>
		</table>
	<?
	}
	?>
	<?
}


pg_close($con);
?>


