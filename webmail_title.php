<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>ウェブメール</title>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ウェブメール権限のチェック
$checkauth = check_authority($session, 0, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ウェブメール管理権限の取得
$webml_admin_auth = check_authority($session, 66, $fname);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" class="spacing"><a href="webmail_menu.php?session=<? echo($session); ?>" target="floatingpage"><img src="img/icon/b01.gif" width="32" height="32" border="0" alt="ウェブメール"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_menu.php?session=<? echo($session); ?>" target="floatingpage"><b>ウェブメール</b></a></font></td>
<? if ($webml_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="webmail_admin_menu.php?session=<? echo($session); ?>" target="floatingpage"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="mainform">
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
</html>
