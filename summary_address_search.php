<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
//require_once("./show_address.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);");
	echo("if (window.opener && !window.opener.closed) {");
	echo("	window.opener.close();");
	echo("}");
	echo("</script>");
	exit;
}

// 権限のチェック
/* 別機能でも使用できるようチェックなしにした
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
*/

// 「あ行」をデフォルトとする
//if ($in_id == "") {$in_id = 0;}

// データベースに接続
$con = connect2db($fname);

/*
// 職種一覧を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by job_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["job_id"]] = $row["job_nm"];
}

// 職種のデフォルト値を設定
if ($job == "") {
	if ($job_name == "") {
		$job_name = "医師";
	}
	$job = array_search($job_name, $jobs);
}
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>アドレス帳一覧</title>
<script type="text/javascript">
function setAddrName(addr_nm) {
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_summary_addr_search)
	{
		var result = new Object();
		result.addr_nm = addr_nm;
		window.opener.call_back_summary_addr_search('<?=$caller?>',result);
	}
	
	//自画面を終了
	window.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>アドレス帳一覧</b></font></td>
	<td>&nbsp</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
</table>

<form name="search" action="summary_address_search.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="search_shared_flg" onchange="sharedFlgOnchange();">
<option value="1"<? if ($search_shared_flg == "1") {echo(" selected");} ?>>全て(個人・共有)</option>
<option value="2"<? if ($search_shared_flg == "2") {echo(" selected");} ?>>個人アドレス帳</option>
<option value="3"<? if ($search_shared_flg == "3") {echo(" selected");} ?>>共有アドレス帳</option>
<? /*
<option value="4"<? if ($search_shared_flg == "4") {echo(" selected");} ?>>職員連絡先</option>
*/ ?>
</select>
&nbsp;
<select name="search_item_flg" <? if ($search_shared_flg == "4") {echo(" disabled");} ?>>
<option value="1"<? if ($search_item_flg == "1") {echo(" selected");} ?>>氏名</option>
<option value="2"<? if ($search_item_flg == "2") {echo(" selected");} ?>>会社名</option>
<option value="3"<? if ($search_item_flg == "3") {echo(" selected");} ?>>部門名</option>
</select>
<input type="text" name="search_name" value="<? echo($search_name); ?>" style="ime-mode: active;" size="40">&nbsp;<input type="submit" name="search_submit" value="検索">&nbsp;&nbsp;&nbsp;
<?
//<input type="submit" name="search_submit" value="全表示">
?>
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="caller" value="<? echo($caller); ?>">
</form>
<?
// 初期表示時は一覧表示しない
// if ($search_shared_flg != "") { ?>
<div align="right"><? showNext($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg, $caller); ?></div>
<? show_address($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg, $caller); ?>
<div align="right"><? showNext($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg, $caller); ?></div>
</form>
<? //} ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>

<?
function show_address($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg){
require_once("./about_postgres.php");

$con=connect2db($fname);
if($con=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$login_emp_id = pg_fetch_result($sel, 0, "emp_id");

$limit="20";
//$limit="2";

if ($page == "") {
	$page = 0;
}

$offset = $limit * $page;

$select_address = get_sql_address($session, $search_shared_flg, $search_submit, $admin_flg, $search_name, $search_item_flg, $offset, $limit, 0);
$result_select=pg_exec($con,$select_address);
if($result_select==false){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$num=pg_numrows($result_select);

$url_name = urlencode($search_name);
$url_submit = urlencode($search_submit);

echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">");

echo("<tr>");
//echo("<td width=\"30\" height=\"22\" align=\"center\" bgcolor=\"#f6f9ff\">");
//echo("</td>");
echo("<td width=\"\" align=\"center\" bgcolor=\"#f6f9ff\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("名前");
echo("</font>");
echo("</td>");
echo("<td width=\"250\" align=\"center\" bgcolor=\"#f6f9ff\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
$company_title = ($search_shared_flg == "4") ? "所属" : "会社名" ;
echo($company_title);
echo("</font>");
echo("</td>");
// 個人/共有
echo("<td width=\"120\" align=\"center\" bgcolor=\"#f6f9ff\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("登録先");
echo("</font>");
echo("</td>");
echo("<td width=\"100\" align=\"center\" bgcolor=\"#f6f9ff\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("電話");
echo("</font>");
echo("</td>");
echo("<td width=\"100\" align=\"center\" bgcolor=\"#f6f9ff\">");
echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("携帯");
echo("</font>");
echo("</td>");
echo("</tr>");

for($i=0;$i<$num;$i++){

	if ($search_shared_flg != "4") {
		$address_id=pg_result($result_select,$i,address_id);
		$name1=pg_result($result_select,$i,name1);
		$name2=pg_result($result_select,$i,name2);
		$company=pg_result($result_select,$i,company);
		$tel1=pg_result($result_select,$i,tel1);
		$tel2=pg_result($result_select,$i,tel2);
		$tel3=pg_result($result_select,$i,tel3);
		$mobile1=pg_result($result_select,$i,mobile1);
		$mobile2=pg_result($result_select,$i,mobile2);
		$mobile3=pg_result($result_select,$i,mobile3);
		$shared_flg=pg_result($result_select,$i,shared_flg);
		$emp_id=pg_result($result_select,$i,emp_id);
		$emp_name=pg_result($result_select,$i,emp_lt_nm)." ".pg_result($result_select,$i,emp_ft_nm);
	} else {
		$name1=pg_result($result_select,$i,emp_lt_nm);
		$name2=pg_result($result_select,$i,emp_ft_nm);
		$class_nm=pg_result($result_select,$i,class_nm);
		$atrb_nm=pg_result($result_select,$i,atrb_nm);
		$dept_nm=pg_result($result_select,$i,dept_nm);
		$room_nm=pg_result($result_select,$i,room_nm);
		$company=$class_nm." &gt; ".$atrb_nm." &gt; ".$dept_nm;
		if ($room_nm != "") {
			$company.= " &gt; ".$room_nm;
		}
		$tel1=pg_result($result_select,$i,emp_tel1);
		$tel2=pg_result($result_select,$i,emp_tel2);
		$tel3=pg_result($result_select,$i,emp_tel3);
		$mobile1=pg_result($result_select,$i,emp_mobile1);
		$mobile2=pg_result($result_select,$i,emp_mobile2);
		$mobile3=pg_result($result_select,$i,emp_mobile3);
		$emp_id=pg_result($result_select,$i,emp_id);
	}

	echo("<tr>");
/*	echo("<td width=\"\" height=\"22\" align=\"center\">");
	if ($search_shared_flg != "4") {
		// 他ユーザの共有アドレスは管理者以外は削除不可
		$disabled = ($login_emp_id != $emp_id && $shared_flg == "t" && $admin_flg != "t") ? " disabled" : "" ;
	} else {
		// 職員連絡先は削除できない
		$disabled = " disabled";
	}
	echo("<input name=\"trashbox[]\" type=\"checkbox\" value=\"$address_id\" $disabled>");
	echo("</td>");
*/	echo("<td width=\"\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
// call_back
//	if ($search_shared_flg != "4") {
//		echo("<a href=\"address_update.php?session=$session&address_id=$address_id&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg\">");
		echo("<a href=\"javascript:void(0);\" onclick=\"setAddrName('$name1 $name2');\">");
//	} else {
		// 職員連絡先の場合
//		echo("<a href=\"address_contact_update.php?session=$session&emp_id=$emp_id&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$page&admin_flg=$admin_flg\">");
//	}
	echo($name1);
	echo("&nbsp;");
	echo($name2);
	echo("</a>");
	echo("</font>");
	echo("</td>");
// 会社名
	echo("<td width=\"\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
// != ""
	echo("<a href=\"javascript:void(0);\" onclick=\"setAddrName('$company');\">");
	echo($company);
	echo("</a>");
	echo("</font>");
	echo("</td>");
// 個人/共有
	echo("<td width=\"\" align=\"left\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	if ($search_shared_flg != "4") {
		if ($shared_flg == "t") {
//			$kubun = "共有";
			$kubun = "共有(".$emp_name.")";
		} else {
			if ($admin_flg == "t") {
				$kubun = "個人(".$emp_name.")";
			} else {
				$kubun = "個人";
			}
		}
	} else {
		$kubun = "職員名簿";
	}
	echo($kubun);
	echo("</font>");
	echo("</td>");
// 電話
	echo("<td width=\"\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//	echo("（電話）");
if($tel1!=""&&$tel2!=""&&$tel3!=""){
	echo($tel1);
	echo("-");
	echo($tel2);
	echo("-");
	echo($tel3);
}
	echo("</font>");
	echo("</td>");
// 携帯
	echo("<td width=\"\">");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//	echo("（携帯）");
if($mobile1!=""&&$mobile2!=""&&$mobile3!=""){
	echo($mobile1);
	echo("-");
	echo($mobile2);
	echo("-");
	echo($mobile3);
}
	echo("</font>");
	echo("</td>");
	echo("</tr>");
}
echo("</table>");
}

function showNext($session,$page,$search_shared_flg,$search_name,$search_submit, $admin_flg, $search_item_flg, $caller){
require_once("./about_postgres.php");
$con=connect2db($fname);
if($con=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$here=$page+1;
$limit="20";
//$limit="2";

$select_address = get_sql_address($session, $search_shared_flg, $search_submit, $admin_flg, $search_name, $search_item_flg, 0, $limit, 1);

$result_select=pg_exec($con,$select_address);
if($result_select==false){
echo("SQLの実行に失敗しました");
exit;
}
  
$check=pg_numrows($result_select);
if($check == 0){
return;
}

$url_name = urlencode($search_name);
$url_submit = urlencode($search_submit);

$rows1=pg_numrows($result_select);
$limita=$limit +1;
if($rows1<$limita){
}else{
	$unit=$page*$limit;
//   $sql2="select * from address limit $unit";
	$sql2 = $select_address . " limit $unit";
   $result2=pg_exec($con, $sql2);
	if($result2==false){
	echo("SQLの実行に失敗しました");
	exit;
	}

	// 管理権限の場合URLのPHPを変更
//	if ($admin_flg == "t") {
//		$page_url = "address_admin_menu.php";
//	} else {
		$page_url = "summary_address_search.php";
//	}

	$rows2=pg_numrows($result2);
	
	$surplus=$rows1-$rows2;		    
	if($page>0){
		$pre=$page-1;
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<a href='./$page_url?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$pre&caller=$caller'>\n");
		echo("←");
		echo("</a>");
		echo("</font> ");
	 }
	$j=0;
	$h=1;
	
	for($i=0;$i<$rows1;$i++){
		if($i%$limit==0){	
  			if($here==$h){
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color='#FF6600'>");
		      echo("[");
		      echo($h);
		      echo("]");
		      echo("</font>");
		      $j++;
		      $h++;
			}else{
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		   	echo("<a href='./$page_url?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$j&caller=$caller'>$h</a>");
		      echo("</font> ");
		      $j++;
		      $h++;
		   }		    	
		}
	}
		
	if($surplus>$limit){
		$next=$page+1;
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("<a href='./$page_url?session=$session&search_shared_flg=$search_shared_flg&search_name=$url_name&search_submit=$url_submit&page=$next&caller=$caller'>\n");
		echo("→");
		echo("</a>"); 
		echo("</font> ");
	}
}
}

// アドレス用SQLを取得
// $proc_flg(処理フラグ)
//  1:order by、offset、limitをつけない文字列を返す
//  2:where部分のみ文字列を返す(CSV出力用）、addressテーブルの別名をaとすること
//  0:全ての文字列を返す
function get_sql_address($session, $search_shared_flg, $search_submit, $admin_flg, $search_name, $search_item_flg, $offset, $limit, $proc_flg) {

	if ($search_shared_flg != "4") {
		$select_address = "select a.*, b.emp_lt_nm, b.emp_ft_nm from address a inner join empmst b on a.emp_id = b.emp_id ";
		$cond = "where address_del_flg = 'f' ";

		if ($search_submit == "全表示" || $search_shared_flg == "1") {  // 全表示＝個人＋共有
			// 管理権限がない場合個人を指定、管理権限がある場合は個人アドレス全て表示
			if ($admin_flg == "f" ) {
				$cond .= " and (a.emp_id = (select emp_id from session where session_id = '$session') or shared_flg = 't') ";
			}
		} else if ($search_shared_flg == "2") {  // 個人
			$cond .= " and shared_flg = 'f' ";
			if ($admin_flg == "f" ) {
				$cond .= " and a.emp_id = (select emp_id from session where session_id = '$session') ";
			}
		} else if ($search_shared_flg == "3") {  // 共有
			$cond .= " and shared_flg = 't' ";
		}
	} else {
		// 職員連絡先の場合
		$select_address = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, a.emp_tel1, a.emp_tel2, a.emp_tel3, a.emp_mobile1, a.emp_mobile2, a.emp_mobile3, b.class_nm , c.atrb_nm, d.dept_nm, e.room_nm from empmst a inner join classmst b on b.class_id = a.emp_class inner join atrbmst c on c.atrb_id = a.emp_attribute inner join deptmst d on d.dept_id = a.emp_dept left outer join classroom e on e.room_id = a.emp_room inner join authmst f on a.emp_id = f.emp_id ";
		$cond = "where f.emp_del_flg = 'f' ";

	}

	if ($search_submit == "検索") {
		// 氏名の場合は空白を必ず削除
		if ($search_item_flg == "1") {
			$trimed_name = str_replace(" ", "", $search_name);
			$trimed_name = str_replace("　", "", $trimed_name);
		} else {
		// 会社名・部門名の場合は、先頭、最後の空白を削除
			$trimed_name = ereg_replace("^ ", "", $search_name);
			$trimed_name = ereg_replace(" $", "", $trimed_name);
			$trimed_name = mbereg_replace("^　", "", $trimed_name);
			$trimed_name = mbereg_replace("　$", "", $trimed_name);
		}
		if ($trimed_name != "") {
			// アドレス帳
			if ($search_shared_flg != "4") {
				switch($search_item_flg) {
				case "1": // 氏名
					$cond .= " and (name1 || name2 like '%$trimed_name%' or name_kana1 || name_kana2 like '%$trimed_name%') ";
					break;
				case "2": // 会社名
					$cond .= " and (company like '%$trimed_name%') ";
					break;
				case "3": // 部門名
					$cond .= " and (department1 like '%$trimed_name%' or department2 like '%$trimed_name%') ";
					break;
				}
			} else {
			// 職員名簿
				$cond .= " and (a.emp_lt_nm || a.emp_ft_nm like '%$trimed_name%' or a.emp_kn_lt_nm || a.emp_kn_ft_nm like '%$trimed_name%') ";
			}
		}
	}

	$select_address .= $cond;
	// order by以降を設定。1の場合は未設定、頁表示用
	if ($proc_flg != 1) {
		if ($search_shared_flg != "4") {
			// 表示順をaddress_idからカナに修正
			$select_address .= " order by name_kana1 asc, name_kana2 asc offset $offset limit $limit";
		} else {
			$select_address .= " order by a.emp_kn_lt_nm asc, a.emp_kn_ft_nm asc offset $offset limit $limit";
		}
	}
// echo ($select_address);
	if ($proc_flg <= 1) {
		return $select_address;
	} else {
	// CSV出力用に条件のみ返す
		return $cond;
	}
}
?>