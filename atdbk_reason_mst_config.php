<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 休暇種別</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");
require_once("settings_common.php");
require_once("timecard_hol_menu_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

//常に表示する 20131127
//$opt_display_flag = file_exists("opt/flag");
$opt_display_flag = true;
//時間有休表示フラグ、休暇申請テンプレートで使用する場合"t" opt/settings.phpで設定
//$paid_hol_hour_flg = get_settings_value("kintai_paid_hol_hour_flg", "f");
$paid_hol_hour_flg = "t"; //常に表示に変更 20141217
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
<!--
/*
* チェックボックスをチェック/非チェックする。
*/
function fncAllCheck(flg){
var chkTF;
var inputs;
var chkName;

    if (flg == 1) {
	    if(update.chkAll.checked==true){
		    chkTF = true;
	    }else{
		    chkTF = false;
	    }
        chkName = 'display_flag_';
    }
    else if (flg == 2){
	    if(update.chkAll2.checked==true){
		    chkTF = true;
	    }else{
		    chkTF = false;
	    }
        chkName = 'time_hol_flag';
    }
    else {
	    if(update.chkAll3.checked==true){
		    chkTF = true;
	    }else{
		    chkTF = false;
	    }
        chkName = 'half_hol_flag';
    }

	inputs = update.getElementsByTagName('input');

	for(var i=0; i < inputs.length; i++){
		var input = inputs[i];
		if(input.type == "checkbox"){
            if (input.name.substr(0, 13) == chkName){
		        input.checked = chkTF;
            }
		}
	}
}

// 細目を編集する 20140620
$(document).ready(function() {

	// 行を追加する
	$(document).on("click", ".addList", function() {
	    $(".details-tfoot > tr").eq(0).clone(true).insertAfter(
	      $(this).parent().parent()
	    );
  	});

	// 行を削除する
	$(document).on("click", ".removeList", function() {
		$(this).parent().parent().empty();
	});

	// 行を一つ上に移動させる
	$(document).on("click", ".upList", function() {
		var t = $(this).parent().parent();
		if($(t).prev("tr")) {
			$(t).insertBefore($(t).prev("tr")[0]);
		}
	});

	// 行を一つ下に移動させる
	$(document).on("click", ".downList", function() {
		var t = $(this).parent().parent();
		if($(t).next("tr")) {
			$(t).insertAfter($(t).next("tr")[0]);
		}
	});


});

//細目伸縮処理
function show_detail(obj,id){
	if (obj.className == "open") {
		$('#img' + id).attr('src','images/minus.gif');
		obj.className = 'close';
		$('#detail' + id).show();
	}else{
		$('#img' + id).attr('src','images/plus.gif');
		obj.className = 'open';
		$('#detail' + id).hide();
	}
}

function add_item(id) {
	var item = $(".details-tfoot > tr").eq(0).clone(true);
	$('#tbody' + id).append(item);
}


function update_reason_mst() {
	var check_id = new Array();
	var notice_msg = new Array();

	//登録する細目情報をチェックする
	$("[id^=target_reason_]").each(function(){
		var tmp_id = $(this).val();

		$("#tbody" + tmp_id + " tr td input:text").each(function(no){
			var tmp_value = $(this).val();
			var tmp_name = $(this).attr("class");

			// 細目名称
			if (tmp_name == "detail_name") {
				if (tmp_value.replace(/(^\s+)|(\s+$)/g, "") == "") {
					check_id[tmp_id] = tmp_id;
					notice_msg[0] = "細目名称を入力してください。"
				}
			}
			// 常勤限度日数
			if (tmp_name == "full_day") {
				if (tmp_value.match(/[^0-9]+/)) {
					check_id[tmp_id] = tmp_id;
					notice_msg[1] = "常勤限度日数には半角数字のみで入力してください。"
				}
			}
			// 非常勤限度日数
			if (tmp_name == "part_day") {
				if (tmp_value.match(/[^0-9]+/)) {
					check_id[tmp_id] = tmp_id;
					notice_msg[2] = "非常勤限度日数には半角数字のみで入力してください。"
				}
				no++;
			}
		});
		$("#check_" + tmp_id).css('background-color', '');
	});

	for (var id in check_id){
		$("#check_" + id).css('background-color','#ff7575');
	}

	// 警告メッセージ表示で処理を中止する。
	if (notice_msg.length > 0){
		var msg = "";
		for (var j = 0; j < notice_msg.length; j++){
			if (notice_msg[j]) msg += notice_msg[j] + "\n";
		}

		alert(msg);
		return false;
	}

	$("[id^=target_reason_]").each(function(){
		var i = 0;
		var reason_id = $(this).val();

		$("#tbody" + reason_id + " tr td input:text").each(function(no){
			var item_value = $(this).val();
			var class_name = $(this).attr("class");

			if (class_name == "detail_name"){
				var details_info = '<input type="hidden" name="details_info[' + reason_id + '][' + i + '][detail_name]" value=' + item_value + '>';
				$(details_info).insertAfter("#tbody" + reason_id);
			}

			if (class_name == "full_day"){
				var details_info = '<input type="hidden" name="details_info[' + reason_id + '][' + i + '][full_day]" value=' + item_value + '>';
				$(details_info).insertAfter("#tbody" + reason_id);
			}

			if (class_name == "part_day"){
				var details_info = '<input type="hidden" name="details_info[' + reason_id + '][' + i + '][part_day]" value=' + item_value + '>';
				$(details_info).insertAfter("#tbody" + reason_id);
				i++;
			}
		});

	});

	document.update.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#bdd1e7 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block th {background: #bdd1e7; border:#5279a5 solid 1px; font:"ＭＳ Ｐゴシック"; }

table.details-table{
	font-size:14px;
	border-collapse:collapse;
}
table.details-table th,table.details-table td{
	border:1px solid #aaa;
	padding:5px 8px;
	text-align:center;
}
table.details-table th{
	background-color:#BEBEBE;
	color:#fff;
}
table.details-table img{
	cursor:pointer;
	vertical-align:text-bottom;
	height:18px;
}
table.details-table input[type="button"]{
	width:80px;
	cursor:pointer;
	background-color:#f0f0f0;
	border:1px solid #aaa;
	border-radius:3px;
	box-shadow:0 1px 2px #999;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");

// 細目情報を取得する 20140620
$sql = "select d.* from atdbk_reason_detail_mst d inner join atdbk_reason_mst r on d.reason_id = r.reason_id ";
$sql .= "order by d.reason_id, d.reason_detail_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0){
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$detail_list = array();
while($row = pg_fetch_array($sel)){
	$reason_id      = $row["reason_id"];
	$detail_id      = $row["reason_detail_id"];
	$detail_name    = $row["reason_detail_name"];
	$full_day       = $row["reason_fulltime_day"];
	$part_day       = $row["reason_parttime_day"];

	$detail_list[$reason_id][$detail_id]["detail_name"] = $detail_name;
	$detail_list[$reason_id][$detail_id]["full_day"] = $full_day;
	$detail_list[$reason_id][$detail_id]["part_day"] = $part_day;
}

// 休暇の場合はその連動になる事由項目を取得する 20130129
$sql = "select reason from atdptn";
$cond = "where atdptn_id = '10'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$vacation_reason = pg_fetch_result($sel, 0, "reason");
$vacation_reason = ($vacation_reason == 24) ? " checked" : "" ;

//事由の読み込み
$sql = "select * from atdbk_reason_mst order by sequence";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//休暇種別メニュータブ表示
show_timecard_hol_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<form name="update" action="atdbk_reason_mst_update.php" method="post">

<table>
	<tr>
		<td>
			<table width=885 cellpadding="1" >
				<tr>
					<!-- 休暇の場合は自動的に事由項目が公休となる 20130128 -->
					<td nowrap align="left"  class="j12">
						<label><input type="checkbox" name="chkVacation" value="chkVacation" <?=$vacation_reason?>>
						勤務実績に「休暇」を選択する場合は事由に「公休」を設定する</label>
					</td>

					<td nowrap align="right"><input type="submit" value="登録" onclick="update_reason_mst();return false;"></td>
				</tr>
			</table>
		<td>
	</tr>
	<tr>

		<td>
			<table width="660" border="0" cellspacing="0" cellpadding="1"  class="list">
				<tr>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由標準設定</font></td>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">支給換算日数</font></td>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休暇日数</font></td>
					<td nowrap bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種類</font></td>
					<td nowrap bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示変更</font></td>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半休/半休
                    (<label><input type="checkbox" name="chkAll3" value="all" onclick="fncAllCheck(3)"/>All</label>)</font></td>
<? if ($paid_hol_hour_flg == "t") { ?>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休
                    (<label><input type="checkbox" name="chkAll2" value="all" onclick="fncAllCheck(2)"/>All</label>)</font></td>
<? } ?>
					<td nowrap bgcolor="#f6f9ff" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">非表示
                    (<label><input type="checkbox" name="chkAll" value="all" onclick="fncAllCheck(1)"/>All</label>)</font></td>
					<td nowrap bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">備考</font></td>
				</tr>
<?

while($row = pg_fetch_array($sel)){
	$reason_id      = $row["reason_id"];
	$sequence       = $row["sequence"];
	$dafault_name   = $row["default_name"];
	$display_name   = $row["display_name"];
	$display_flag   = $row["display_flag"];
	$recital        = $row["recital"];
	$day_count      = $row["day_count"];
	$holiday_count  = $row["holiday_count"];
	$kind_flag      = $row["kind_flag"];
    $time_hol_flag  = $row["time_hol_flag"];
    $half_hol_flag  = $row["half_hol_flag"];

	$display_flag_checked = "";
	if ($display_flag != 't'){
		$display_flag_checked = " checked ";
	}
    $time_hol_flag_checked = "";
    if ($time_hol_flag == 't'){
        $time_hol_flag_checked = " checked ";
    }
    $half_hol_flag_checked = "";
    if ($half_hol_flag == 't'){
        $half_hol_flag_checked = " checked ";
    }

    $img_flag = "open";
?>
				<tr id="check_<?=$reason_id?>">
					<td nowrap>
						<img src="<?=($img_flag == "open")? "images/plus.gif":"images/minus.gif"?>" width="11" height="11" id="img<?=$reason_id?>" style="cursor:pointer;" class="<?=$img_flag?>" onclick="show_detail(this,'<?=$reason_id?>');">
						<input type="hidden" name="reason_id[]" value="<?=$reason_id?>">
						<font size="2" face="ＭＳ Ｐゴシック, Osaka"><?=$dafault_name?></font>
					</td>
					<td nowrap align="center">
						<select name="day_count[]">
						<option value="0.0" <? if ($day_count == 0.0) echo("selected"); ?>>0</option>
						<option value="0.5" <? if ($day_count == 0.5) echo("selected"); ?>>0.5</option>
						<option value="1.0" <? if ($day_count == 1.0) echo("selected"); ?>>1</option>
						</select>
					</td>

					<td nowrap align="center">
						<select name="holiday_count[]">
						<option value=""     <? if ($holiday_count == "")  echo("selected"); ?>></option>
						<option value="1.0"  <? if ($holiday_count == 1.0) echo("selected"); ?>>1</option>
						<option value="1.5"  <? if ($holiday_count == 1.5) echo("selected"); ?>>1.5</option>
						<option value="2.0"  <? if ($holiday_count == 2.0) echo("selected"); ?>>2</option>
						<option value="0.5"  <? if ($holiday_count == 0.5) echo("selected"); ?>>0.5</option>
						<option value="0.25" <? if ($holiday_count == 0.25) echo("selected"); ?>>0.25</option>
						</select>
					</td>

					<td nowrap align="center">
						<select name="kind_flag[]">
						<option value=""  <? if ($kind_flag == "") echo("selected"); ?>></option>
						<option value="1" <? if ($kind_flag == 1)  echo("selected"); ?>>有給</option>
						<option value="4" <? if ($kind_flag == 4)  echo("selected"); ?>>公休</option>
						<option value="2" <? if ($kind_flag == 2)  echo("selected"); ?>>週休</option>
						<option value="3" <? if ($kind_flag == 3)  echo("selected"); ?>>夏休</option>
						<option value="5" <? if ($kind_flag == 5)  echo("selected"); ?>>特別</option>
						<option value="6" <? if ($kind_flag == 6)  echo("selected"); ?>>病欠</option>
<? if ($opt_display_flag) { ?>
						<option value="99" <? if ($kind_flag == 99)  echo("selected"); ?>>その他</option> <!-- メディアテック追加 -->
<? } ?>
						</select>
					</td>

					<td align="center"><input type="text"     name="display_name[]" value="<?=$display_name?>" style="ime-mode:active;" size="32" maxlength="15"></td>
					<td align="center"><input type="checkbox" name="half_hol_flag_<?=$reason_id?>" <?=$half_hol_flag_checked?>></td>
<? if ($paid_hol_hour_flg == "t") { ?>
					<td align="center"><input type="checkbox" name="time_hol_flag_<?=$reason_id?>" <?=$time_hol_flag_checked?>></td>
<? } ?>
					<td align="center"><input type="checkbox" name="display_flag_<?=$reason_id?>" <?=$display_flag_checked?>></td>
					<td align="center"><input type="text"     name="recital[]"      value="<?=$recital?>" style="ime-mode:active;" size="60" maxlength="25"></td>
				</tr>
				<tr id="detail<?=$reason_id?>" style="display:none;">
					<td></td>
					<td colspan=8 style="padding:10px 20px">
						<table class="details-table" width="100%">
							<thead>
								<tr>
								  <th width="25%">細目名称</th>
								  <th width="18%">常勤限度日数</th>
								  <th width="18%">非常勤限度日数</th>
								  <th width="15%">表示順</th>
								  <th width="24%">
								  	<span>追加 / 削除</span>&nbsp;
								  	<input type="hidden" id="target_reason_<?=$reason_id?>" value="<?=$reason_id?>" />
								  	<input id="add<?=$reason_id?>" value="追加" type="button"  style="width:50px" onclick="add_item('<?=$reason_id?>');" />
								  </th>
								</tr>
							</thead>
							<tfoot class="details-tfoot" style="display:none;">
								<tr>
								  <td nowrap>
								    <input type="text" class="detail_name" style="ime-mode:active;" size="30" maxlength="30" value="" />
								  </td>
								  <td nowrap>
								    <input type="text" class="full_day" style="ime-mode:inactive;" maxlength="3" value="" />
								  </td>
								  <td nowrap>
								    <input type="text" class="part_day" style="ime-mode:inactive;" maxlength="3" value="" />
								  </td>
								  <td nowrap>
								    <img src="images/up.gif" alt="↑" class="upList" />上へ
								    <img src="images/down.gif" alt="↓" class="downList" />下へ
								  </td>
								  <td nowrap>
								    <input value="追加" type="button" class="addList" />
								    <input value="削除" type="button" class="removeList" />
								  </td>
								</tr>
							</tfoot>
							<tbody id="tbody<?=$reason_id?>" class="details-tbody">
								<?php
									if ($detail_list[$reason_id]){
										foreach ($detail_list[$reason_id] as $item) {
								?>
											<tr>
											  <td nowrap>
											    <input type="text" style="ime-mode:active;" class="detail_name" size="30" maxlength="30" value="<?php echo($item["detail_name"]); ?>" />
											  </td>
											  <td nowrap>
											    <input type="text" class="full_day" style="ime-mode:inactive;" maxlength="3" value="<?php echo($item["full_day"]); ?>" />
											  </td>
											  <td nowrap>
											    <input type="text" class="part_day" style="ime-mode:inactive;" maxlength="3" value="<?php echo($item["part_day"]); ?>" />
											  </td>
											  <td nowrap>
											    <img src="images/up.gif" alt="↑" class="upList" />上へ
											    <img src="images/down.gif" alt="↓" class="downList" />下へ
											  </td>
											  <td nowrap>
											    <input value="追加" type="button" class="addList" />
											    <input value="削除" type="button" class="removeList" />
											  </td>
											</tr>
								<?php
										}
									}
								?>
							</tbody>
						</table>
					</td>
				</tr>

<?
}
?>
			</table>
		</td>
	</tr>

	<tr>
		<table width=885 cellpadding="1" >
			<tr>
				<td colspan="2" align="right"><input type="submit" value="登録" onclick="update_reason_mst(); return false;"></td>
			</tr>
		</table>
	</tr>

</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
