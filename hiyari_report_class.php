<?php
require_once("about_comedix.php");
require_once("smarty_setting.ini");
require_once("hiyari_wf_utils.php");
require_once("hiyari_common.ini");
require_once("hiyari_lock.php");
require_once("hiyari_item_time_setting_models.php");
require_once("hiyari_item_timelag_setting_models.php");

class hiyari_report_class {

    var $file_name; // 呼び出し元ファイル名
    var $_db_con;   // DBコネクション
    var $_all_items; // 全項目リスト

    /**
     * コンストラクタ
     * @param object $con DBコネクション
     * @param string $fname 画面名
     */
    function hiyari_report_class($con, $fname){
        // 呼び出し元ファイル名を保持
        $this->file_name = $fname;
        // DBコネクションを保持
        $this->_db_con = $con;
        // 全項目リストを初期化
        $this->_all_items = array();
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// レポート情報　取得　(項目入力値も取得)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 指定されたレポートＩＤに対するinci_reportテーブルのレコードデータを取得します。
     */
    function get_inci_report_record($report_id) {
        $sql = 'select inci_report.* from inci_report';
        $cond = "where inci_report.report_id=$report_id";

        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0){
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        if ( pg_num_rows( $result ) == 0 )
        {
            $ret = false;
        }
        else
        {
            $ret = pg_fetch_array($result,0);
        }
        return $ret;
    }

    /**
     * レポート情報/内容を取得する。
     *
     * 得られる配列は以下の形式となる。
     * array[{inci_reportのフィールド名}] = {inci_reportの値}
     * array["content"]["eid_id"] = レポートデータID ※array["eid_id"]と同値。
     * array["content"]["eis_id"] = 様式定義ID       ※array["eis_id"]と同値。
     * array["content"]["report_id"] = レポートID    ※array["report_id"]と同値。
     * array["content"]["input_items"][{各グループコード}][{各項目コード}][0-N] = {レポートの各入力値}
     *
     * @param string $report_id レポートID
     * @return array レポートデータ配列
     */
    function get_report($report_id){

        //レポート情報を検索
        $sql = 'select inci_report.* from inci_report';
        $cond = "where inci_report.report_id=$report_id";
        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0){
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        //レポート情報が得られない(レポートが存在しない)場合はfalseを返す。
        if ( pg_num_rows( $result ) == 0 ){
            $ret = false;
        }
        //レポート情報が得られた場合は、contentキーにレポート内容を追加する。
        else {

            //検索結果のレポート情報を配列化
            $ret = pg_fetch_assoc( $result );

            //レポート内容を検索
            $sql = 'SELECT * FROM inci_easyinput_data  LEFT JOIN inci_easyinput_item_type USING (grp_code,easy_item_code)';
            $cond = "WHERE eid_id=" . p( $ret['eid_id'] );
            $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con,"rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }

            //検索結果のレポート内容を配列化
            $ret_cont = pg_fetch_all( $result );

            //$contents[{グループコード}][{項目コード}] に 入力値(TSVを展開した配列) を格納する。
            $contents = array();
            if (is_array($ret_cont)){

                foreach ( $ret_cont as $cont ){
                    if ($cont['item_type'] === 'checkbox') {
                        $contents[ $cont['grp_code'] ][ $cont['easy_item_code'] ] = preg_split("/\t/", $cont['input_item'] );
                    }
                    else {
                        $contents[ $cont['grp_code'] ][ $cont['easy_item_code'] ][0] = $cont['input_item'];
                    }
                }
            }

            //レポート情報検索結果配列にcontentキーを追加し、レポート内容を格納する。
//          $ret['content']['eid_id'] = $ret_cont[0]['eid_id'];
//          $ret['content']['eis_id'] = $ret_cont[0]['eis_id'];
//          $ret['content']['report_id'] = $ret_cont[0]['report_id'];
            $ret['content']['eid_id'] = $ret['eid_id'];
            $ret['content']['eis_id'] = $ret['eis_id'];
            $ret['content']['report_id'] = $ret['report_id'];
            $ret['content']['input_items'] = $contents;
        }

        return $ret;
    }

    /**
     * 最新のメールIDを取得します。
     * 
     * @param  string レポートID
     * @return string メールID
     */
    function get_newest_mail_id($report_id){
        $result = "";
        if (!empty($report_id)) {
            $sql = "select MAX(mail_id) from inci_mail_send_info ";
            $cond = "where report_id=" . p($report_id) ;
            $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            
            // SQLの結果が返らない時は空文字を返す。
            if ($result == 0) {
                $result = "";
            }
            
            // 値取得
            while ( $row = pg_fetch_assoc($sel) ) {
                $result = $row['max'];
            }
        }
        return $result;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// レポート情報　登録
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * レポートを登録します。
     *
     * @param string  $emp_id 報告者ID
     * @param array   $inputs 登録画面送信データ配列
     * @param boolean $is_anonymous 匿名フラグ。trueの場合に匿名となる。
     * @param boolean $is_shitagaki 下書きフラグ。trueの場合に下書きとなる。
     * @return string レポートID
     */
    function set_report($session, $emp_id, $inputs, $is_anonymous, $is_shitagaki=false) {

        //==================================================
        //登録内容を作成
        //==================================================

        // 2012/01/24 invalid
        //レポートID(自動採番)
        // $report_id = get_col_max($this->_db_con, "inci_report", "report_id", $this->file_name) + 1;

        // 2012/01/24 add
		// シーケンスを使用し、レポートIDを採番する
		$result = pg_query($this->_db_con, "select nextval('inci_report_report_id_seq')");
		$report_id = pg_fetch_result($result, 0);

        //報告者名
        $registrant_name  = get_emp_kanji_name($this->_db_con,$emp_id,$this->file_name);

        //レポートタイトル
        $report_title = $inputs['report_title'];

        //職種ID
        $job_id = $inputs['job_id'];

        //登録日
        $now_date = sprintf("%s/%02d/%02d", $inputs['regist_date_y'], $inputs['regist_date_m'], $inputs['regist_date_d']);
        $registration_date = $now_date;

        // 登録時刻（タイムスタンプ）取得
        $now_time = time();

        $registration_time = date('YmdHis', $now_time);

        // タイムラグ区分取得
        $inputs['_105_70'] = getTimelagClass($this->_db_con, $this->file_name , $now_time, $inputs );

        // タイムラグ取得
        $inputs['_105_75'] = getTimelag($this->_db_con, $this->file_name , $now_time, $inputs );


        //所属情報
        $registrant_class = $inputs['registrant_class'];
        $registrant_attribute = $inputs['registrant_attribute'];
        $registrant_dept = $inputs['registrant_dept'];
        $registrant_room = $inputs['registrant_room'];
        if ($registrant_class == "") {
            $registrant_class = null;
        }

        if ($registrant_attribute == "") {
            $registrant_attribute = null;
        }

        if ($registrant_dept == "") {
            $registrant_dept = null;
        }

        if ($registrant_room == "") {
            $registrant_room = null;
        }

        //主治医
        $doctor_emp_id = $inputs['doctor_emp_id'];

        if ($is_shitagaki) {
            //下書きフラグ
            $shitagaki_flg = "t";

            //レポート番号なし
            $report_no = null;
        }
        else {
            //下書きフラグ
            $shitagaki_flg = "f";

            //レポート番号(自動採番)
            $report_no = $this->get_new_report_no();
        }

        //報告書様式ID
        $eis_no = $inputs['eis_no'];
        $eis_id = $this->get_eis_id_from_eis_no($eis_no);
        if ($eis_id == "") {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        //匿名レポートフラグ
        if ($is_anonymous) {
            $anonymous_report_flg = "t";
        }

        else {
            $anonymous_report_flg = "f";
        }

        //報告者職種
        $registrant_job = get_own_job($this->_db_con,$emp_id,$this->file_name);

        //==================================================
        //レポート情報を登録
        //==================================================

        //登録SQLを作成
        $sql = "insert into inci_report(report_id, report_title, job_id, registrant_id, registrant_name, registration_date, eis_id, report_no, shitagaki_flg, report_comment, anonymous_report_flg, registrant_class, registrant_attribute, registrant_dept, registrant_room, registrant_job, doctor_emp_id, first_send_time) values(";
        $registry_data = array(
            $report_id,
            p($report_title),
            $job_id,
            $emp_id,
            p($registrant_name),
            $registration_date,
            $eis_id,
            $report_no,
            $shitagaki_flg,
            "",//reoprt_comment
            $anonymous_report_flg,
            $registrant_class,
            $registrant_attribute,
            $registrant_dept,
            $registrant_room,
            $registrant_job,
            $doctor_emp_id,
            $registration_time
        );

        //登録SQLを実行
        $result = insert_into_table($this->_db_con, $sql, $registry_data, $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        //==================================================
        //レポート内容を登録
        //==================================================
        $this->set_report_content($session,$report_id, $eis_id, $inputs);


		//20130630
		//==================================================
		//ファイル添付情報を取得
		//==================================================
		$this->move_hiyari_file($report_id, $emp_id,$inputs["gamen_mode"]);
		$this->set_inci_file_report($report_id);


        return $report_id;
    }


    /**
     * レポート進捗を登録します。
     *
     * @param string $report_id レポートID
     */
    function regist_inci_report_progress_at_send($report_id, $default_to_list) {

		if(get_progress_adjust($this->_db_con,$this->file_name) == "t"){

			// 進捗登録設定されている担当を取得
			$sql = "select * from inci_report_progress_dflt_flg ";
			$cond = "where use_flg = 't'";
			$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
			$auth = pg_fetch_all($sel);

			$use_flg = array();

			for ($i=0; $i < count($auth); $i++){

				$charge_flg = false;
				// 担当者がいなかった場合は、use_flgをfにする
				foreach( $default_to_list as $auth_emp) {
					if($auth_emp["auth"] == $auth[$i]['auth'] ) $charge_flg = true;
				}
				if ($charge_flg == false) $use_flg[$i][$auth[$i]['auth']] = 'f';
				else $use_flg[$i][$auth[$i]['auth']] = 't';
			}

		}

		// 新規登録処理
        $sql = "insert into inci_report_progress("
              ."select $report_id, inci_report_progress_dflt_flg.auth, '', 'f', 'f', '', '', inci_report_progress_dflt_flg.use_flg, 'f', ''"
              ."from inci_report_progress_dflt_flg "
              ."inner join inci_auth_mst on inci_report_progress_dflt_flg.auth = inci_auth_mst.auth and inci_auth_mst.use_flg ";
        $content = "";
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
        if ($ins == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

		if(get_progress_adjust($this->_db_con,$this->file_name) == "t"){
			for($i=0; $i < count($use_flg); $i++){
				if($use_flg[$i][$auth[$i]['auth']] == 'f'){
					$sql = "update inci_report_progress set ";
					$set = array("use_flg");
					$set_value = 'f';
					$content = "where report_id = $report_id and auth = '".$auth[$i]['auth']."'";
					$upd = update_set_table($this->_db_con,$sql,$set,$set_value,$content,$this->file_name);

					if($upd == 0){
						pg_query($con,"rollback");
						pg_close($con);
						echo("<script type='text/javascript' src='./js/showpage.js'></script>");
						echo("<script language='javascript'>showErrorPage(window);</script>");
						exit;
					}
				}
			}
		}
    }


//  /**
//   * レポート番号を採番し、更新します。
//   *
//   * @param string $report_id レポートID
//   */
//  function set_report_no($report_id)
//  {
//      $report_no = $this->get_new_report_no();
//      $sql = "update inci_report set";
//      $set = array("report_no");
//      $setvalue = array($report_no);
//      $cond = "where report_id = $report_id";
//      $result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
//      if ($result == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//  }
    /**
     * (private関数)レポート番号を採番します。
     *
     * @return string レポート番号
     */
    function get_new_report_no()
    {

        //レポート番号の先頭4桁を決定
        $date1 = mktime(0,0,0,date('m'), date('d'), date('Y'));
        $yyyy = date('Y', $date1);
        $mm = date('m', $date1);
        //$yy = substr($yyyy,2,2);
        //$yymm = $yy.$mm;
        $no_header = $yyyy.$mm."-";

        //レポート番号下4桁を決定
        $sql = "SELECT max(report_no) as max FROM inci_report";
        $cond = "where report_no like '$no_header%'";
        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $sec = pg_result($result,0,0);
        if($sec == "")
        {
            $sec = "0001";
        }
        else
        {
            $sec = substr($sec,7,4);//下4桁
            $sec = $sec + 1;//採番
            $sec = sprintf("%04d", $sec);//0埋め4桁
        }

        //レポート番号を作成
        return $no_header.$sec;
    }


    /**
     * レポート履歴にエントリーします。
     *
     * @param string $rerpot_id レポートID
     * @param string $emp_id 更新者ID
     * @param string $mail_id 更新時に送信したメールID(送信していない場合は省略可能)
     */
    function set_report_rireki($report_id,$emp_id,$mail_id="")
    {
        //履歴IDを採番する。
        $sql = "select max(report_rireki_id) as rr_id from inci_report_rireki where report_id = $report_id";
        $sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        if(pg_num_rows($sel) < 1)
        {
            $new_rireki_id = 1;
        }
        else
        {
            $new_rireki_id = pg_fetch_result($sel,0,"rr_id");
            if($new_rireki_id == "")
            {
                $new_rireki_id = 1;
            }
            else
            {
                $new_rireki_id++;
            }
        }

        //システム日付
        $y = date('Y');
        $m = date('m');
        $d = date('d');
        $rireki_date = "$y/$m/$d";

        //履歴保存
        $sql  = " insert into inci_report_rireki";
        $sql .= " select ";
        $sql .= " $new_rireki_id as report_rireki_id";
        $sql .= " ,'$rireki_date' as report_rireki_date";
        $sql .= " ,'$emp_id' as report_rireki_emp_id";
        if($mail_id != "")
        {
            $sql .= " ,'$mail_id' as report_rireki_mail_id";
        }
        else
        {
            $sql .= " ,null as report_rireki_mail_id";
        }
        $sql .= " ,inci_report.*";
        $sql .= " from inci_report";
        $sql .= " where report_id = $report_id";
        $result = pg_query($this->_db_con,$sql);
        if($result == false){
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// レポート情報　削除
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * レポートを論理削除します。
     *
     * @param string $report_id レポートID
     */
    function del_report($report_id){
        $sql = "update inci_report set";
        $set = array("del_flag");
        $setvalue = array("t");
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * レポートを論理削除解除します。
     *
     * @param string $report_id レポートID
     */
    function del_report_cancel($report_id){
        $sql = "update inci_report set";
        $set = array("del_flag");
        $setvalue = array("f");
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * レポートをが論理削除されているか判定します。
     *
     * @param string $report_id レポートID
     * @return boolean 論理削除されている場合はtrue
     */
    function is_deleted_report($report_id){
        $sql = "select del_flag from inci_report where report_id = $report_id";
        $result = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $flg = pg_result($result,0,"del_flag");
        return $flg == "t";
    }

//  /**
//   * レポートを完全削除(２段階論理削除)します。
//   *
//   * @param string $report_id レポートID
//   */
//  function kill_report($report_id){
//      $sql = "update inci_report set";
//      $set = array("kill_flg");
//      $setvalue = array("t");
//      $cond = "where report_id = $report_id";
//      $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
//      if ($upd == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//  }

    /**
     * 全ての削除済みレポートを完全削除(２段階論理削除)します。
     *
     */
    function kill_report_all(){
        $sql = "update inci_report set";
        $set = array("kill_flg");
        $setvalue = array("t");
        $cond = "where not shitagaki_flg and del_flag";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// レポート情報　更新
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * inci_reportの指定されたレポートに対して指定された項目の値を更新します。
     *
     * @param string $report_id レポートID
     * @param string $field フィールド
     * @param string $val データ文字列
     */
    function set_report_data_field($report_id, $field, $val)
    {
        $sql = "update inci_report set";
        $set = array($field);
        $setvalue = array($val);
        $cond = "where report_id = $report_id";
        $result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

//  /**
//   * 下書きフラグを更新します。
//   *
//   * @param string $report_id レポートID
//   * @param boolean $is_shitagaki 下書きフラグ
//   */
//  function set_report_shitagaki_flg($report_id, $is_shitagaki)
//  {
//      $shitagaki_flg = "f";
//      if($is_shitagaki)
//      {
//          $shitagaki_flg = "t";
//      }
//
//      $sql = "update inci_report set";
//      $set = array("shitagaki_flg");
//      $setvalue = array($shitagaki_flg);
//      $cond = "where report_id = $report_id";
//      $result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
//      if ($result == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//  }



    //レポートタイトルを更新します。
    function set_report_title($report_id,$report_title)
    {
        $sql = "update inci_report set";
        $set = array("report_title");
        $setvalue = array(p($report_title));
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    //匿名情報を更新します。
    function set_report_anonymous($report_id,$is_anonymous)
    {
        $sql = "update inci_report set";
        $set = array("anonymous_report_flg");
        $setvalue = array(p($is_anonymous));
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

//  //レポートコメントを更新します。
//  function set_report_comment($report_id,$report_comment)
//  {
//      $sql = "update inci_report set";
//      $set = array("report_comment");
//      $setvalue = array(p($report_comment));
//      $cond = "where report_id = $report_id";
//      $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
//      if ($upd == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//  }

    // 「報告日」を当日に更新します。
    function set_registration_date($report_id, $regist_date_y, $regist_date_m, $regist_date_d) {
        $sql = "update inci_report set";
        $set = array("registration_date");

        $regist_date = $regist_date_y."/".$regist_date_m."/".$regist_date_d;
        $setvalue = array($regist_date);
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 報告書項目　登録/更新
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // 報告書内容を新規登録/更新する
    // ※この関数は新規登録及びレポート編集(下書きを含む)にてレポート内容を更新する際に使用することを前提とします。
    // 更新マージを行うパターンでは$input_grpsが必須です。
    // SM専用項目がありえない場合は$sessionは不要。
    function set_report_content($session,$report_id, $eis_id, $inputs,$input_grps="")
    {
        //===========================================
        // eid_idを採番
        //===========================================
        /* 2012/01/24 invalid
        // 注意：inci_easyinput_dataは入力項目がない場合に0レコードとなるため、単純には採番に使用できない。
        $eid_id = get_col_max($this->_db_con, "inci_easyinput_data", "eid_id", $this->file_name) +1;
        $eid_id2 = get_col_max($this->_db_con, "inci_report", "eid_id", $this->file_name) +1;
        if($eid_id<$eid_id2)
        {
            $eid_id = $eid_id2;
        }
        */
        // 2012/01/24 add
		// シーケンスを使用し、データIDを採番する
		$result = pg_query($this->_db_con, "select nextval('inci_report_eid_id_seq')");
		$eid_id = pg_fetch_result($result, 0);

        //===========================================
        //レポート内容を登録する。
        //===========================================
        $db_cols = array('eid_id', 'eis_id', 'report_id', 'grp_code', 'easy_item_code', 'input_item');
        $cols_str = join(', ', $db_cols);
        if($eis_id != "")
        {
            //$eis_id, $report_idは実質使用しない。但し、メンテナンスでは参考にできる。
            $cols_val_org = array($eid_id, $eis_id, $report_id);
        }
        else
        {
            $cols_val_org = array($eid_id, '-1', $report_id);
        }
        // 複数選択可能な項目番号を取得

        $sql = "insert into inci_easyinput_data($cols_str) values(";

        foreach ( $inputs as $key => $value ){
            if ( preg_match( "/^_(\d+)_(\d+)$/", $key, $matches ) ){
                if ( $value == "" ) continue;
                $val = ( is_array( $value ) ) ? join("\t", $value) : p($value);
                if ( $val == "" ) continue;
                $cols_val = array_merge( $cols_val_org, array( $matches[1], $matches[2], $val ) );

                $result = insert_into_table($this->_db_con, $sql, $cols_val, $this->file_name);
                if ($result == 0) {
                    pg_query($this->_db_con,"rollback");
                    pg_close($this->_db_con);
                    showErrorPage();
                    exit;
                }
            }
        }


        //===========================================
        //非入力項目が既に入力済みの場合はマージする。
        //===========================================

        //旧レポートを検索
        $sql = "select * from inci_report";
        $cond = "where report_id = $report_id";
        $old_report_db = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($old_report_db == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($old_report_db);

        //レポートが新規登録ではない場合
        if($num > 0)
        {
            //かつ、既にレポートに内容番号が割り当ててある場合
            $old_eid_id = pg_result($old_report_db,0,"eid_id");
            if($old_eid_id != "")
            {
                //画面入力項目グループに対する入力項目(一部)を取得する。
                $inci_easyinput_set2_all = $this->get_inci_easyinput_set2_all($eis_id,$input_grps);

                //旧レポートの入力済みグループを取得する。
                $sql = "select * from inci_easyinput_data";
                $cond = "where eid_id = $old_eid_id";
                $old_data_db = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
                if ($old_data_db == 0) {
                    pg_query($this->_db_con,"rollback");
                    pg_close($this->_db_con);
                    showErrorPage();
                    exit;
                }
                $old_data_array = pg_fetch_all($old_data_db);


                //旧レポートレコードごとにループ
                foreach ($old_data_array as $old_data_line) {

                    //今回のデータにマージ必要な旧データの場合
                    if ($this->is_merge_old_data($old_data_line, $old_data_array, $input_grps, $inci_easyinput_set2_all, $inputs, $session)) {

                        //今回のデータとして登録する。
                        $new_data_line = $old_data_line;
                        $new_data_line["eid_id"] = $eid_id;

                        //時系列をマージしない
                        if ($inputs["gamen_mode"]=='update' and $new_data_line["grp_code"] >= 4000 and $new_data_line["grp_code"] <= 4049) {
                            continue;
                        }

                        //inci_easyinpu_dataに既に登録している場合は、差分チェックでTRUEが出ても登録しない
                        //（同一eid_id、grp_code、easy_item_codeでのinci_easyinput_dataの二重登録を防止）
                        $sql_check = "select input_item from inci_easyinput_data where eid_id='" . $new_data_line["eid_id"] . "' and grp_code='" . $new_data_line["grp_code"] . "' and easy_item_code='" . $new_data_line["easy_item_code"] . "'";
                        $sql_check_db = select_from_table($this->_db_con, $sql_check, "", $this->file_name);
                        if ($sql_check_db == 0) {
                            pg_query($this->_db_con, "rollback");
                            pg_close($this->_db_con);
                            showErrorPage();
                            exit;
                        }

                        $num = pg_num_rows($sql_check_db);
                        //件数が返ってくれば、同じ項目が登録ありなので登録処理スキップ（二重登録回避）

                        if ($num < 1) {
                            $sql = "insert into inci_easyinput_data($cols_str) values(";
                            $val = array
                                (
                                $new_data_line["eid_id"],
                                $new_data_line["eis_id"],
                                $new_data_line["report_id"],
                                $new_data_line["grp_code"],
                                $new_data_line["easy_item_code"],
                                p($new_data_line["input_item"])
                            );

                            $result = insert_into_table($this->_db_con, $sql, $val, $this->file_name);
                            if ($result == 0) {
                                pg_query($this->_db_con, "rollback");
                                pg_close($this->_db_con);
                                showErrorPage();
                                exit;
                            }
                        }
                    }
                }
            }
        }

        // 発生日を取得
        $sql  = "select max(input_item) from inci_easyinput_data";
        $cond = "where eid_id = $eid_id and grp_code = 100 and easy_item_code = 5";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $incident_date = pg_fetch_result($sel, 0, 0);

        // 評価予定期日を取得
        $sql  = "select max(input_item) from inci_easyinput_data";
        $cond = "where eid_id = $eid_id and grp_code = 620 and easy_item_code = 33";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $evaluation_date = pg_fetch_result($sel, 0, 0);

        // 患者影響レベルを取得
        $sql  = "select max(input_item) from inci_easyinput_data";
        $cond = "where eid_id = $eid_id and grp_code = 90 and easy_item_code = 10";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $inci_level = pg_fetch_result($sel, 0, 0);

        //===========================================
        //レポートの内容番号、発生日、評価予定期日、患者影響レベルを更新する。
        //===========================================
        $sql = "update inci_report set";
        $set = array("eid_id", "incident_date", "evaluation_date", "inci_level");
        $setvalue = array($eid_id, $incident_date, $evaluation_date, $inci_level);
        $cond = "where report_id = $report_id";
        $result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

    }
    /**
     * (Private)指定された旧レコードが今回のマージ対象か判定します。
     *
     * $input_grpsには当事者2以降のグループコードは不要。ちなみに400グループも不要。(set2を参照するため。)
     * そのため、
     */
    function is_merge_old_data($old_data_line,$old_data_array,$input_grps,$inci_easyinput_set2_all,$inputs,$session)
    {
        //特記事項：過去データ障害により、SM専用項目(G690/I90)が常にマージされていたことがある。
        //          そのため、inci_easyinput_dataテーブルにSM専用項目(G690/I90)がダブってしまっている可能性あり。
        //          注意が必要。

        $old_grp_code = $old_data_line["grp_code"];
        $old_easy_item_code = $old_data_line["easy_item_code"];

        //=======================================================
        //患者プロフィールの場合(項目様式を持つ)
        //=======================================================
        if($old_grp_code == 210)
        {
            //今回の入力可能グループである場合
            if(in_array($old_grp_code,$input_grps))
            {
                //今回の入力可能項目である場合
                if(array_key_exists($old_grp_code,$inci_easyinput_set2_all) && in_array($old_easy_item_code,$inci_easyinput_set2_all[210]))
                {
                    //今回の入力が有効であるため、マージ不要。
                    return false;
                }
                //今回の入力可能項目ではない場合
                else
                {
                    //今回、入力欄が無いため、マージ必要。
                    return true;
                }
            }
            //今回の入力不可能グループである場合
            else
            {
                //今回、入力欄が無いため、マージ必要。
                return true;
            }
        }
        //=======================================================
        //インシデントの概要連動項目の場合(項目様式を持つ)(連動元変更に伴う影響あり)
        //=======================================================
        elseif($old_grp_code >= 400 && $old_grp_code <= 490)
        {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(120,$input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                //今回の連動元入力値を取得。
                $new_data_120_80 = $inputs['_120_80'];

                //今回、連動元入力値が無い(=空欄入力)場合
                if($new_data_120_80 == "")
                {
                    //連動先の入力も破棄される。すなわちマージ不要。
                    return false;
                }
                //今回、連動元入力値がある(=空欄入力ではない)場合
                else
                {
                    //今回の連動元入力値より、今回の連動先グループを特定。
                    $new_data_400_grp = 400 + ($new_data_120_80-1)*10;

                    //前回と今回で連動先グループが異なる場合(=連動元が変更されている場合)
                    if($old_grp_code != $new_data_400_grp)
                    {
                        //連動変化により、前回の入力値は無効となっているため、マージ不要。
                        return false;
                    }
                    //前回と今回の連動先グループが同じ場合(=連動元に変更が無い場合)
                    else
                    {
                        //今回の入力可能項目である場合
                        if(array_key_exists($old_grp_code,$inci_easyinput_set2_all) && in_array($old_easy_item_code,$inci_easyinput_set2_all[$new_data_400_grp]))
                        {
                            //今回の入力が有効であるため、マージ不要。
                            return false;
                        }
                        //今回の入力可能項目ではない場合
                        else
                        {
                            //今回、入力欄が無いため、マージ必要。
                            return true;
                        }
                    }
                }
            }
        }
        //=======================================================
        //概要（2010年版）の場合
        //=======================================================
        else if($old_grp_code >= 910 && $old_grp_code <= 980) {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(900,$input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                return false;
            }
        }
        else if($old_grp_code == 1000) {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(1300,$input_grps) && !in_array(1310, $input_grps) && !in_array(1320, $input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                return false;
            }
        }
        //=======================================================
        //発生要因詳細の場合(連動元変更に伴う影響あり)
        //=======================================================
        elseif($old_grp_code == 605)
        {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(600,$input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                //今回の連動元入力値を取得。
                $new_data_600_10 = $inputs['_600_10'];

                //連動に必要な、連動元入力値を取得。
                //※grp_code=600のeasy_code が grp_code=605のeasy_item_code である仕様が前提のロジックになっています。拡張時は注意。
                $need_600_10_value = $old_easy_item_code;

                // その他の項目を追加したときの処理
                if($need_600_10_value > 100) {
                    if($need_600_10_value < "112" || $need_600_10_value >= "118") {
                        $need_600_10_value -= 100;
                    } else if($need_600_10_value >= "112") {
                        $need_600_10_value -= 99;
                    }
                }

                //今回、連動元入力値が無い(=未チェック)場合
                if(! in_array($need_600_10_value ,$new_data_600_10))
                {
                    //連動先の入力も破棄される。すなわちマージ不要。
                    return false;
                }
                //今回、連動元入力値がある場合
                else
                {
                    //今回の入力可能項目である場合
                    if(in_array(605,$input_grps))
                    {
                        //今回の入力が有効であるため、マージ不要。
                        return false;
                    }
                    //今回の入力可能項目ではない場合
                    else
                    {
                        //今回、入力欄が無いため、マージ必要。
                        return true;
                    }
                }
            }

        }
        //=======================================================
        //アセスメント・患者の状態 20090304
        //=======================================================
        elseif($old_grp_code == 150)
        {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(140,$input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                //今回の連動元入力値を取得。
                $new_data_140_10 = $inputs['_140_10'];

                //連動に必要な、連動元入力値を取得。
                //※grp_code=140のeasy_code が grp_code=150のeasy_item_code である仕様が前提のロジックになっています。拡張時は注意。
                $need_140_10_value = $old_easy_item_code;

                //今回、連動元入力値が無い(=未チェック)場合
                if(! in_array($need_140_10_value ,$new_data_140_10))
                {
                    //連動先の入力も破棄される。すなわちマージ不要。
                    return false;
                }
                //今回、連動元入力値がある場合
                else
                {
                    //今回の入力可能項目である場合
                    if(in_array(140,$input_grps))
                    {
                        //今回の入力が有効であるため、マージ不要。
                        return false;
                    }
                    //今回の入力可能項目ではない場合
                    else
                    {
                        //今回、入力欄が無いため、マージ必要。
                        return true;
                    }
                }
            }

        }
        //=======================================================
        //ルート関連（自己抜去）ミトンあり 20090304
        //=======================================================
        elseif($old_grp_code == 160)
        {
            //今回、連動元グループが入力不可能の場合
            if(! in_array(140,$input_grps))
            {
                //連動先グループは更新不可能であるため、マージ必要。
                return true;
            }
            //今回、連動元グループが入力可能な場合
            else
            {
                //マージ不要。
                return false;
            }

        }
        //=======================================================
        //当事者項目の場合(全当事者単位で行う。)
        //=======================================================
        elseif($old_grp_code >= 3050 && $old_grp_code <= 3499)
        {
            //全ての当事者項目が今回入力不可能グループの場合
            if(
                !(
                   in_array(3050,$input_grps)
                || in_array(3100,$input_grps)
                || in_array(3150,$input_grps)
                || in_array(3200,$input_grps)
                || in_array(3250,$input_grps)
                || in_array(3300,$input_grps)
                || in_array(3350,$input_grps)
                || in_array(3400,$input_grps)
                || in_array(3450,$input_grps)
                )
            )
            {
                //当事者の項目がないものとみなせるため、マージ必要。
                return true;
            }

            //当事者1のグループコードに変換
            $old_touzisha1_grp = floor($old_grp_code/10)*10;

            //当事者番号を取得
            $old_touzisha_no = $old_grp_code%10 +1;

            //今回入力の最終当事者番号を特定
            $now_max_touzisha_no = 0;
            foreach($inputs as $input_key => $input_val)
            {
                if ( preg_match( "/^_(\d+)_(\d+)$/", $input_key, $matches ) )
                {
                    $g_tmp = $matches[1];
                    if($g_tmp >= 3050 && $g_tmp <= 3499)
                    {
                        $g_no_tmp = $g_tmp%10 +1;
                        if($now_max_touzisha_no < $g_no_tmp)
                        {
                            $now_max_touzisha_no = $g_no_tmp;
                        }
                    }
                }
            }

            //今回の最終当事者より後の当事者の場合(今回削除された当事者の場合)
            if($now_max_touzisha_no < $old_touzisha_no)
            {
                //同時に削除されるべきデータであるため、マージ不要。
                return false;
            }

            //当事者1のグループが、今回入力可能グループである場合
            if(in_array($old_touzisha1_grp,$input_grps))
            {
                //今回の入力が有効であるため、マージ不要。
                return false;
            }
            //当事者1のグループが、今回入力不可能グループである場合
            else
            {
                //今回、入力欄が無いため、マージ必要。
                return true;
            }
        }
        //=======================================================
        //関連する患者情報
        //=======================================================
        elseif($old_grp_code >= 310 && $old_grp_code <= 399)
        {
            //関連する患者情報の数が入力可能な場合
            if(in_array(200, $input_grps)){
                //関連する患者情報の数より後のデータの場合
                $num_patients = $inputs[_200_30];
                if( ($old_grp_code % 10) >= $num_patients){
                    //同時に削除されるべきデータであるため、マージ不要。
                    return false;
                }
            }

            //今回、入力可能グループである場合
            if(in_array( floor($old_grp_code/10)*10, $input_grps )){
                //今回の入力が有効であるため、マージ不要。
                return false;
            }
            //今回、入力不可能グループである場合
            else{
                //今回、入力欄が無いため、マージ必要。
                return true;
            }
        }
        //=======================================================
        //SM専用項目の場合
        //=======================================================
        elseif($old_grp_code == 690)
        {
            //SM専用項目がありえない場合($sessionが渡らない)
            if($session == "")
            {
                //SM専用項目はないため、マージ必要。
                return true;
            }
            //SMの場合
            if(is_sm_emp($session,$this->file_name))
            {
                //今回の入力可能項目である場合
                if(in_array(690,$input_grps))
                {
                    //今回の入力が有効であるため、マージ不要。
                    return false;
                }
                //今回の入力可能項目ではない場合
                else
                {
                    //今回、入力欄が無いため、マージ必要。
                    return true;
                }
            }
            //SMでは無い場合
            else
            {
                //SM専用項目はないため、マージ必要。
                return true;
            }
        }
        //=======================================================
        //通常項目(非表示項目を含む)の場合
        //=======================================================
        //※「患者への影響」、「対応」の場合もこのパターンとなる。(グループをまたがない連動であるため)
        else
        {
            //今回、入力可能グループである場合
            if(in_array($old_grp_code,$input_grps))
            {
                //今回の入力が有効であるため、マージ不要。
                return false;
            }
            //今回、入力不可能グループである場合
            else
            {
                //今回、入力欄が無いため、マージ必要。
                return true;
            }
        }
    }

//  /**
//   * 引数の条件において、レポート入力画面で入力でき得るグループを取得します。
//   * この関数はレポート入力画面専用であり、全項目様式／再発防止様式など様式情報の無いケースには対応しません。
//   * 連動変化項目が有効な場合は全ての連動変化項目が取得されます。
//   * 当事者項目が有効な場合は全ての当事者項目が取得されます。
//   * SMの場合はSM専用項目も取得されます。
//   * あくまでグループ単位で情報を取得するため、項目単位の様式は考慮しません。
//   *
//   * @param string $eis_id 様式ID
//   * @param string $session セッションID(省略時はSM専用項目はなし)
//   * @return array グループコード配列
//   */
//  function get_report_sheet_inputable_groups($eis_id,$session="")
//  {
//      //通常様式の様式
//      $grp_obj = $this->get_report_flags($eis_id);
//      $grps = $grp_obj['use_grp_code'];
//      $grps = $this->grp_flags_to_report_sheet_inputable_groups($grps);
//      //SM専用項目
//      if($session != "")
//      {
//          $is_sm = is_sm_emp($session,$this->file_name);
//          if($is_sm)
//          {
//              $grps[] = 690;
//          }
//      }
//      return $grps;
//  }
//
//
//  function grp_flags_to_report_sheet_inputable_groups($grps)
//  {
//      //連動変化項目
//      if(in_array(400,$grps))
//      {
//          $grps[] = 410;
//          $grps[] = 420;
//          $grps[] = 430;
//          $grps[] = 440;
//          $grps[] = 450;
//          $grps[] = 460;
//          $grps[] = 470;
//          $grps[] = 480;
//          $grps[] = 490;
//      }
//      //当事者項目
//      $touzisha_grp_arr = array(3050,3100,3150,3200,3250,3300,3350,3400,3450);
//      foreach($touzisha_grp_arr as $touzisha_grp)
//      {
//          if(in_array($touzisha_grp,$grps))
//          {
//              for($i=1;$i<10;$i++)
//              {
//                  $grps[] = $touzisha_grp + $i;
//              }
//          }
//      }
//      return $grps;
//  }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 項目情報：　全項目マスタ情報の取得
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // 全基本選択項目を取得する
    function get_all_easy_items(){
//      $rel_others = $this->get_relate_item_other();
        $rel_groups = $this->get_relate_item_group();

//      $easy_item_list_size = -1;

/*
        $sql  = "SELECT mate.grp_code, grp_name, to_number(mate.easy_item_code,'9999') AS easy_item_code, easy_item_name, easy_name, easy_code, item_type, item_must FROM inci_report_materials AS mate JOIN inci_easyinput_group_mst AS grp ON grp.grp_code = mate.grp_code JOIN inci_easyinput_item_mst AS item ON mate.grp_code = item.grp_code AND mate.easy_item_code = item.easy_item_code  JOIN inci_easyinput_item_type AS itype ON mate.grp_code = itype.grp_code AND mate.easy_item_code = itype.easy_item_code";
        $cond = "WHERE easy_flag='1' ORDER BY cate_code, grp_code, easy_item_code, easy_num";

        $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );

        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
*/


        $result = select_from_table($this->_db_con, "select grp_code, grp_name from inci_easyinput_group_mst", "", $this->file_name );
        $mstlist = array();

        while ( $row = pg_fetch_row($result) ) {
            $mstlist[$row[0]] = $row[1];
        }

        $sql = "
            SELECT mst.grp_code, mst.easy_item_code, mst.easy_item_name, typ.item_type, typ.explanatory_note
            FROM inci_easyinput_item_mst mst
            INNER JOIN inci_easyinput_item_type typ USING (grp_code,easy_item_code)
            ";
        $result = select_from_table($this->_db_con, $sql,"",$this->file_name);
        $itmlist = array();

        while ( $row = pg_fetch_assoc($result) ) {
            $itmlist[$row["grp_code"]][$row["easy_item_code"]] = $row;
        }

        $sql = "
            SELECT grp_code, easy_item_code, easy_name, easy_code, other_code, pause_flg, item_group_code
            FROM inci_report_materials WHERE easy_flag = '1'
            ORDER BY grp_code, easy_item_code, easy_num
            ";
        $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );
        $grp_list = array();
        $last_grp = "";
        $last_easy_item = "";
        $p_grp = 0;
        $p_easy = 0;

        while ( $row = pg_fetch_assoc($result) ){
            $grp_code = $row['grp_code'];

            if ( !$mstlist[$grp_code] ) {
                continue;
            }

            $easy_item_code = (int)$row['easy_item_code'];
            $itm = &$itmlist[$grp_code][$easy_item_code];

            if ( !$itm["easy_item_name"] ) {
                continue;
            }

            //グループコードが前回と違う場合 (同じ場合は既に設定されているため無視)
            if ( $grp_code != $last_grp ) {
                $grp_list[ $grp_code ] = array(
                    'grp_name' => $mstlist[$grp_code],
                    'easy_item_list' => array()
                );
                $p_grp = &$grp_list[$grp_code]["easy_item_list"];
            }

            //項目コードもしくはグループコードが前回と違う場合 (同じ場合は既に設定されているため無視)
            if ( $easy_item_code != $last_easy_item or $grp_code != $last_grp ){
                $p_grp[ $easy_item_code ] = array(
                    'easy_item_name' => $itm["easy_item_name"],
                    'easy_item_type' => $itm['item_type'],
                    'explanatory_note' => $itm['explanatory_note'],
//                  'easy_item_must' => $row['item_must'],
//                  'relate_other' => $rel_others[ $grp_code ][ $easy_item_code ],
                    'relate_group' => $rel_groups[ $grp_code ][ $easy_item_code ],
                    'easy_list' => array(),
                );
                $p_easy = &$p_grp[$easy_item_code]["easy_list"];
            }

            $p_easy[] = array(
                    'easy_code' => $row['easy_code'],
                    'easy_name' => $row['easy_name'],
                    'other_code' => $row['other_code'],
                    'pause_flg' => $row['pause_flg'],
                    'item_group_code' => $row['item_group_code']
            );
            
            //前回の情報を更新
            $last_grp = $grp_code;
            $last_easy_item = $easy_item_code;
        }
        return $grp_list;
    }

    // インシデントの分類方法変更の場面と内容を取得
    function get_all_super_item() {
        $sql = "SELECT super_item_id, super_item_name FROM inci_super_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' ORDER BY super_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // 影響と対応の分類方法変更の取得
    function get_all_ic_super_item() {
        $sql = "SELECT super_item_id, super_item_name FROM inci_ic_super_item ";
        $cond = "WHERE disp_flg = 't' AND available ='t' ORDER BY super_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの分類方法変更の場面と内容を統計分析用に取得 awaji
    function get_all_super_item_for_stats() {
        $sql = "SELECT super_item_id as easy_code, super_item_name as easy_name FROM inci_super_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' ORDER BY super_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの分類方法変更の場面と内容を統計分析用に取得
    function get_all_sub_item_for_stats($id, $code) {
        $sql = "SELECT sub_item_id as easy_code, sub_item_name as easy_name FROM inci_sub_item ";
        $cond = "WHERE super_item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY item_id, sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


            // インシデントの発生した場面を取得する
    function get_scene($id) {
        $sql = "SELECT item_id, item_name FROM inci_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'scene' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_scene_item($id) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'scene' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの発生した場面を取得する
    function get_kind($id) {
        $sql = "SELECT item_id, item_name FROM inci_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'kind' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_kind_item($id) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'kind' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの発生した場面を取得する
    function get_content($id) {
        $sql = "SELECT item_id, item_name FROM inci_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'content' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_content_item($id) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'content' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    function get_super_item_by_id($id) {
        $sql = "SELECT super_item_id, super_item_name FROM inci_super_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND super_item_id = {$id}";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }
    function get_item_by_id($id, $code) {
        $sql = "SELECT item_id, item_name FROM inci_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }
    function get_sub_item_by_id($id, $code) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND sub_item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }



    // インシデントの分類方法変更の場面と内容を取得 2010年版
    function get_all_super_item_2010() {
		/*
        $sql = "SELECT super_item_id, super_item_name FROM inci_super_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' ORDER BY super_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        */
        $sql = "
        	select
				base.super_item_id
				, base.super_item_name
				, case
					when base.code is null then
						plus.super_item_id
					else
						base.super_item_id
					end as export_item_id
        , base.other_code
			from
				inci_super_item_2010 base
				left outer join
				inci_super_item_2010 plus
				on
				base.export_code = plus.code
			where
				base.disp_flg = 't'
				and base.available = 't'
			order by
				base.super_item_order
		";
        $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの分類方法変更の場面と内容を統計分析用に取得 awaji
    function get_all_super_item_for_stats_2010() {
        $sql = "SELECT super_item_id as easy_code, super_item_name as easy_name FROM inci_super_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't'  ORDER BY super_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        return pg_fetch_all($result);
    }

    // 機能評価機構事例収集用概要取得
    function get_all_super_item_for_stats_2010_default() {
        $sql = "SELECT super_item_id as easy_code, super_item_name as easy_name FROM inci_super_item_2010 ";
        $cond = "WHERE super_item_id <= 8 ORDER BY super_item_id;";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }
    // IDからSUPER_IDを取得
    function get_super_id_for_id($id) {
        $sql = "SELECT super_item_id FROM inci_item_2010 ";
        $cond = "WHERE item_id = {$id} AND disp_flg = 't' AND available = 't'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_row($result);
    }
    // インシデントの分類方法変更の場面と内容を統計分析用に取得
    function get_all_item_for_stats_2010($id, $code) {
        $sql = "SELECT item_id as easy_code, item_name as easy_name FROM inci_item_2010 ";
        $cond = "WHERE super_item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY item_id, item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }
    // インシデントの分類方法変更の場面と内容を統計分析用に取得
    function get_all_sub_item_for_stats_2010($id, $code) {
        $sql = "SELECT sub_item_id as easy_code, sub_item_name as easy_name FROM inci_sub_item_2010 ";
        $cond = "WHERE super_item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY item_id, sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの分類方法変更の場面と内容を統計分析用に取得
    function get_all_sub_item_for_stats_2010_by_item($id, $code) {
        $sql = "SELECT sub_item_id as easy_code, sub_item_name as easy_name FROM inci_sub_item_2010 ";
        $cond = "WHERE item_id = {$id} AND item_code = '{$code}' AND disp_flg = 't' AND available = 't' ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // 発生要因詳細を統計分析用に取得
    function get_all_sub_item_for_detail_factor_by_item($easy_item_code) {
        $sql = "SELECT easy_code, easy_name FROM inci_report_materials ";
        $cond = "WHERE grp_code = 605 AND easy_item_code = '{$easy_item_code}' AND easy_flag = '1' AND available = 't' ORDER BY easy_num";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデント直前の患者の状態の詳細を統計分析用に取得
    function get_all_sub_item_for_patient_state_by_item($easy_item_code) {
        $sql = "SELECT easy_code, easy_name FROM inci_report_materials ";
        $cond = "WHERE grp_code = 295 AND easy_item_code = '{$easy_item_code}' AND easy_flag = '1' AND available = 't' ORDER BY easy_num";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // 発生場所の詳細を統計分析用に取得
    function get_all_sub_item_for_place_by_item($easy_item_code) {
        $sql = "SELECT easy_code, easy_name FROM inci_report_materials ";
        $cond = "WHERE grp_code = 115 AND easy_item_code = '{$easy_item_code}' AND easy_flag = '1' AND available = 't' ORDER BY easy_num";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        return pg_fetch_all($result);
    }

    // インシデントの発生した場面を取得する
    function get_scene_2010($id) {
        $sql = "SELECT item_id, item_name FROM inci_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'scene' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    // インシデントの発生した場面を取得する
    function get_ic_items($id, $item) {
        $sql = "SELECT item_id, item_name, other_code FROM inci_ic_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = '{$item}' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    // インシデントの発生した場面を取得する
    function get_ic_sub_items($id, $item) {
        $sql = "SELECT sub_item_id, item_id, super_item_id, sub_item_name FROM inci_ic_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = '{$item}' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_scene_item_2010($id) {
        $sql = "SELECT sub_item_id, sub_item_name, other_code FROM inci_sub_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'scene' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの発生した場面を取得する
    function get_kind_2010($id) {
        $sql = "SELECT item_id, item_name FROM inci_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'kind' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_kind_item_2010($id) {
        $sql = "SELECT sub_item_id, sub_item_name, other_code FROM inci_sub_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'kind' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    // インシデントの発生した場面を取得する
    function get_content_2010($id) {
        $sql = "SELECT item_id, item_name FROM inci_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'content' AND super_item_id = {$id} ORDER BY item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }


    function get_content_item_2010($id) {
        $sql = "SELECT sub_item_id, sub_item_name, other_code FROM inci_sub_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_code = 'content' AND item_id = {$id} ORDER BY sub_item_order";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    function get_super_item_by_id_2010($id) {
        $sql = "SELECT super_item_id, super_item_name FROM inci_super_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND super_item_id = {$id}";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }

    function get_ic_super_item_by_id($id) {
        $sql = "SELECT super_item_id, super_item_name FROM inci_ic_super_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND super_item_id = {$id}";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }

    function get_ic_item_name_by_sub_item_id($id) {
        $sql = "SELECT ii.item_name FROM inci_ic_item ii INNER JOIN inci_ic_sub_item isi ON ii.item_id = isi.item_id ";
        $cond = "WHERE ii.disp_flg = 't' AND ii.available = 't' AND isi.disp_flg = 't' AND isi.available = 't' AND isi.sub_item_id = {$id}";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_result($result, 0, "item_name");
    }

    function get_item_by_id_2010($id, $code) {
        $sql = "SELECT item_id, item_name FROM inci_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }
    function get_ic_item_by_id($id, $code) {
        $sql = "SELECT item_id, item_name FROM inci_ic_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }
    function get_sub_item_by_id_2010($id, $code) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_sub_item_2010 ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND sub_item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }
    function get_ic_sub_item_by_id($id, $code) {
        $sql = "SELECT sub_item_id, sub_item_name FROM inci_ic_sub_item ";
        $cond = "WHERE disp_flg = 't' AND available = 't' AND sub_item_id = {$id} AND item_code = '{$code}'";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_array($result);
    }

    /**
     * その他の内容のコードを取得
     *
     * @return array その他の内容コードの配列
     */
    function get_other_codes(){
        $others = array();

        //発見時間・発生時間の表示形式フラグ（2時間単位選択表示/時間・分選択表示）
        $is_2hour = getTimeSetting($this->_db_con, $PHP_SELF);

        //----------------------------------------------------------------------
        //その他を持つ項目のコードと、その他のコードを取得
        //----------------------------------------------------------------------
        //通常項目
        $sql = "SELECT grp_code, easy_item_code, grp_code AS other_grp_code, other_code AS other_easy_item_code";
        $sql.= " FROM inci_report_materials WHERE available = 't' AND other_code > 0";
        //概要
        $sql.= " UNION";
        $sql.= " SELECT 900 AS grp_code, 10 AS easy_item_code, 900 AS other_grp_code, other_code AS other_easy_item_code";
        $sql.= " FROM inci_super_item_2010 WHERE available = 't' AND other_code > 0";
        //概要：種類、発生場面、事例の内容
        $sql.= " UNION";
        $sql.= " SELECT DISTINCT";
        $sql.= " CASE WHEN item_code='kind' THEN 920";
        $sql.= "      WHEN item_code='scene' THEN 950";
        $sql.= "      WHEN item_code='content' THEN 980";
        $sql.= " END AS grp_code,";
        $sql.= " 10 AS easy_item_code,";
        $sql.= " CASE WHEN item_code='kind' THEN 930";
        $sql.= "      WHEN item_code='scene' THEN 960";
        $sql.= "      WHEN item_code='content' THEN 990";
        $sql.= " END AS grp_code,";
        $sql.= " other_code AS other_easy_code";
        $sql.= " FROM inci_sub_item_2010 WHERE available = 't' AND other_code > 0";
        //患者への影響
        $sql.= " UNION";
        $sql.= " SELECT DISTINCT 1400 AS grp_code,";
        $sql.= "  CASE WHEN item_code='influence' THEN 30";
        $sql.= "       WHEN item_code='mental' THEN 60";
        $sql.= "  END AS easy_item_code,";
        $sql.= "  1400 AS other_grp_code,";
        $sql.= "  other_code AS other_easy_item_code";
        $sql.= " FROM inci_ic_item WHERE available = 't' AND other_code > 0";

        $result = select_from_table($this->_db_con, $sql, '', $this->file_name);
        if ($result == 0) {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $work = pg_fetch_all($result);
        foreach($work as $w){
            $grp_code = $w['grp_code'];
            $easy_item_code = $w['easy_item_code'];
            $other_grp_code = $w['other_grp_code'];
            $other_easy_item_code = $w['other_easy_item_code'];

            //「いつごろ」がその他になるのは、2時間単位選択表示の場合のみ
            if (!$is_2hour){
                if (($grp_code==100 || $grp_code==105) && $easy_item_code==40){
                    continue;
                }
            }

            // その他の内容のコード一覧
            $others['others'][$other_grp_code][] = $other_easy_item_code;

            // その他が関連付けられた親アイテムとその他の内容のコード一覧
            $temp['grp_code'] = $other_grp_code;
            $temp['easy_item_code'] = $other_easy_item_code;
            $others['relate'][$grp_code][$easy_item_code]['others'][] = $temp;
        }

        return $others;
    }

    /**
     * 表示用の報告書データを取得
     *
     * @param string $report_id レポートID
     * @param string $eis_id タイトル用様式ID
     * @return array 報告書データの配列
     */
    function get_report_data_for_disp($report_id, $eis_title_id='') {
        //報告書データの取得
        $report_data = $this->get_report($report_id);

        //報告書マスタ情報の取得
        $all_easy_items = $this->get_all_easy_items();

        //その他の内容のコード取得
        $others = $this->get_other_codes();
        
        //SMかどうか
        $is_sm_emp_flg =  is_sm_emp($session, $fname, $con);
        
        //====================================================================================================
        //報告書の表示用アイテム一覧
        //====================================================================================================
        $rpt_data = array();
        $report_items = $this->get_report_item_for_disp($report_id, $eis_title_id);
        foreach($report_items as $cate_code => $cate) {
            foreach($cate['list'] as $grp_code => $grp) {
                foreach($grp as $easy_item_code => $title) {
                    // 医療安全担当者専用項目はSMの時のみ表示する。
                    if (!($grp_code == 690 && $easy_item_code == 90 && $is_sm_emp_flg == false)) {
                        $temp = array();
                        // 項目の値
                        $temp['content'] = get_item_value_for_mail_message($report_data,$all_easy_items,$grp_code,$easy_item_code,$this);

                        // その他の内容の値
                        foreach($others['relate'][$grp_code][$easy_item_code]['others'] as $o){
                            $content = get_item_value_for_mail_message($report_data,$all_easy_items,$o['grp_code'],$o['easy_item_code'],$this);
                            if ($content != ''){
                                $temp['others'][] = $content;
                            }
                        }

                        if ($temp['content'] != '' || count($temp['others']) > 0){
                            $temp['title'] = $title;
                            $rpt_data[$cate_code]['list'][] = $temp;
                        }
                    }
                }
            }

            if (isset($rpt_data[$cate_code]['list'])){
                $rpt_data[$cate_code]['name'] = $cate['name'];
            }
        }

        return $rpt_data;
    }


    /**
     * 報告書データから表示項目の一覧を取得
     *
     * @param string $report_id レポートID
     * @param string $eis_id タイトル用様式ID
     * @return array 報告書の表示項目配列
     */
    function get_report_item_for_disp($report_id, $eis_title_id='')
    {
        //==================================================
        //代替タイトルの取得
        //==================================================
        $replace_title = array();
        if ($eis_title_id ==''){
            $sql = "SELECT eis_id FROM inci_report WHERE report_id=".$report_id;
            $result = select_from_table($this->_db_con, $sql, '', $this->file_name);
            if ($result == 0) {
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }
            $eis_title_id = pg_fetch_result($result);
        }
        $sql = "SELECT * FROM inci_item_title WHERE eis_id=".$eis_title_id;
        $result = select_from_table($this->_db_con, $sql, '', $this->file_name);
        $rows = pg_fetch_all($result);
        foreach($rows as $r){
            $grp_code = $r['eis_column'];
            $title = $r['eis_title'];

            $replace_title[$grp_code] = $title;

            //----------------------------------------------
            //複数項目のグループ追加
            //----------------------------------------------
            $cnt = 0;

            //当事者
            if ($grp_code >= 3050 && $grp_code <= 3459){
                $cnt = 10;
            }
            //関連する患者情報
            elseif($grp_code>=310 && $grp_code<=390){
                $cnt = 10;
            }
            //事例の詳細(時系列)グループの場合
            if($grp_code == 4000) {
                $cnt = 50;
            }

            if ($cnt > 0){
                for($i = 1; $i < $cnt; $i++){
                    $replace_title[$grp_code + $i] = $title;
                }
            }
        }

        $sql = "SELECT user_cate_name FROM inci_easyinput_set WHERE eis_id=$eis_title_id";
        $result = select_from_table($this->_db_con, $sql, '', $this->file_name);
        if ($result == 0) {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $user_cate_name = pg_fetch_result($result, 0, 0);

        //==================================================
        //その他の内容のコード
        //==================================================
        $others = $this->get_other_codes();
        
        //====================================================================================================
        //報告書データから表示する項目名の一覧を取得
        //====================================================================================================
        $pre_cate_code = -1;
        $report_item = array();

        $sql = 'SELECT DISTINCT cat.cate_num, cat.cate_code, cat.cate_name, godr.order_no, grp.grp_num, data.grp_code, data.easy_item_code, itm.easy_item_name';
        $sql.= ' FROM (';
        //報告書データ
        $sql.= '  SELECT grp_code, easy_item_code';
        $sql.= '   FROM inci_easyinput_data data';
        $sql.= '   WHERE eid_id = (SELECT eid_id FROM inci_report WHERE report_id = ' . $report_id . ')';
        //その他を持つ項目（項目が未選択でも、その他の内容が記述されている場合に表示させるため）
        $sql.= "  UNION SELECT grp_code, easy_item_code FROM inci_report_materials WHERE available = 't' AND other_code > 0";
        $sql.= '  UNION SELECT 900, 10 ';   //概要
        $sql.= '  UNION SELECT 920, 10 ';   //概要：種類
        $sql.= '  UNION SELECT 950, 10 ';   //概要：発生場面
        $sql.= '  UNION SELECT 980, 10 ';   //概要：事例の内容
        $sql.= '  UNION SELECT 1400, 30 ';  //患者への影響
        $sql.= '  UNION SELECT 1400, 60 ';  //患者への精神的影響
        $sql.= ' ) data';
        //カテゴリ＞グループ順にするための連結
        $sql.= ' LEFT JOIN (SELECT DISTINCT cate_code, grp_code FROM inci_report_materials) cat_grp ON data.grp_code = cat_grp.grp_code';
        $sql.= ' LEFT JOIN inci_easyinput_category_mst cat ON cat_grp.cate_code=cat.cate_code';
        $sql.= ' LEFT JOIN inci_easyinput_group_index grp ON data.grp_code=grp.grp_code';
        $sql.= " LEFT JOIN inci_easyinput_set_group_order godr ON godr.eis_id = $eis_title_id AND data.grp_code = godr.grp_code";
        //項目名を取得するための連結
        $sql.= ' LEFT JOIN inci_easyinput_item_mst itm ON data.grp_code=itm.grp_code AND data.easy_item_code = itm.easy_item_code';
        //カテゴリ＞グループ順にする
        $sql.= ' ORDER BY cat.cate_num, cat.cate_code, godr.order_no, grp.grp_num';
        $result = select_from_table($this->_db_con, $sql, '', $this->file_name);

        $rows = pg_fetch_all($result);
        foreach($rows as $r) {
            $cate_code = $r['cate_code'];
            $grp_code = $r['grp_code'];
            $easy_item_code = $r['easy_item_code'];
            $easy_item_name = $r['easy_item_name'];

            //発生年月日・発見年月日関係の非表示項目
            if(    ( $grp_code == 100 && $easy_item_code == 10 ) || ( $grp_code == 100 && $easy_item_code == 30 )
                || ( $grp_code == 105 && $easy_item_code == 10 ) || ( $grp_code == 105 && $easy_item_code == 30 )
                || ( $grp_code == 105 && $easy_item_code == 70 ) || ( $grp_code == 105 && $easy_item_code == 75 )
                ) {
                continue;
            }

            //事例の詳細(時系列)の時分
            if($grp_code >= 4000 && $grp_code <= 4049) {
                if ($easy_item_code == 2 || $easy_item_code == 3){
                    continue;
                }
            }

            //150〜190(アセスメント関連)は非表示、140アセスメント・患者の状態にまとめる 20090304
            if ( in_array( $grp_code, array(150,160,170,180,190) ) ) {
                continue;
            }

            //その他の内容
            if (in_array($easy_item_code, $others['others'][$grp_code])){
                continue;
            }

            //125のデータを7に移す 20150204TN
            if ($grp_code == 125 && $easy_item_code == 85) {
                if(isset($replace_title[$grp_code])) {
                    $item_125 = $replace_title[$grp_code];
                    continue;
                } else {
                    $item_125 = $easy_item_name;
                    continue;
                }
            }

           
            //カテゴリ名
            if ($cate_code != $pre_cate_code){
                if ($cate_code == 10 && $user_cate_name != null){
                    $report_item[$cate_code]['name'] = $user_cate_name;
                } else {
                    $report_item[$cate_code]['name'] = $r['cate_name'];
                }
                $pre_cate_code = $cate_code;
                
                if ($cate_code == 7){
                    $report_item[$cate_code]['list'][125][85] = $item_125;
                }
            }

            //アイテム名
            if($grp_code == 1100) {
                $report_item[$cate_code]['list'][$grp_code][10] = "医療の実施の有無";
                $report_item[$cate_code]['list'][$grp_code][20] = "実施の程度";
                $report_item[$cate_code]['list'][$grp_code][30] = "事故の程度";
                $report_item[$cate_code]['list'][$grp_code][40] = "不明の場合";
                $report_item[$cate_code]['list'][$grp_code][50] = "仮に実施された場合の影響度";
            }
            else if($grp_code == 1110) {
                $report_item[$cate_code]['list'][$grp_code][10] = "特に報告を求める事例";
                $report_item[$cate_code]['list'][$grp_code][20] = "その他の場合";
            }
            else if($grp_code == 1120) {
                $report_item[$cate_code]['list'][$grp_code][10] = "発生件数集計再掲";
            }
            else if ($grp_code == 100 &&  $easy_item_code == 60) {// 発生時刻：時
                $report_item[$cate_code]['list'][$grp_code][$easy_item_code] = '発生時刻';
            }
            else if ($grp_code == 105 &&  $easy_item_code == 60) {// 発見時刻：分
                $report_item[$cate_code]['list'][$grp_code][$easy_item_code] = '発見時刻';

            } else {
                if(isset($replace_title[$grp_code])) {
                    $report_item[$cate_code]['list'][$grp_code][$easy_item_code] = $replace_title[$grp_code];
                }
                else {
                    $report_item[$cate_code]['list'][$grp_code][$easy_item_code] = $easy_item_name;
                }
            }
        }
        //ksort($report_item);  // 分類が追加されたあとにcate_codeキーでソートして整列する
        return $report_item;
    }

//  /**
//   * (Private関数)
//   * レポート入力画面保存時チェック用 連動チェック項目情報取得。
//   *
//   * 返却値の配列構造は以下のとおり (inci_easyinput_relate_other単位)
//   * array[{連動元グループコード}][{連動元項目コード}]["easy_code"] = 連動元選択項目コード
//   * array[{連動元グループコード}][{連動元項目コード}]["rel_item_code"] = 連動先項目コード
//   * array[{連動元グループコード}][{連動元項目コード}]["rel_item_name"] = 連動先項目名
//   *
//   */
//  function get_relate_item_other(){
//
//      $sql  = "SELECT irel.*, iname.easy_item_name FROM inci_easyinput_relate_other AS irel JOIN inci_easyinput_item_mst AS iname ON irel.grp_code = iname.grp_code AND irel.sub_item_code = iname.easy_item_code";
//      $cond = "ORDER BY grp_code, easy_item_code";
//      $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
//      if ($result == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//
//      $relates = array();
//      while ( $row = pg_fetch_assoc($result) ){
//
//          $relates[ $row['grp_code'] ][ $row['easy_item_code'] ] = array(
//              'easy_code' => $row['easy_code'],
//              'rel_item_code' => $row['sub_item_code'],
//              'rel_item_name' => $row['easy_item_name']
//          );
//
//      }
//
//      return $relates;
//  }
    /**
     * (Private関数)
     * 報告書登録用連動グループコードを取得する
     *
     * 返却値の配列構造は以下のとおり
     * array[{グループコード}][{項目コード}] = 連動先グループコード
     *
     */
    function get_relate_item_group(){

        $sql  = "SELECT *  FROM inci_easyinput_relate_group";
        $cond = "ORDER BY grp_code, sub_grp_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $relates = array();
        $last_grp = "";
        $last_easy_item = "";
        while ( $row = pg_fetch_assoc($result) ){

            //グループコードもしくは項目コードが前回と違う場合
            //TODO 気になる点：ソート順で、sub_grp_codeではなくeasy_item_codeを指定べきなのでは？
            if ( $row['grp_code'] != $last_grp or $row['easy_item_code'] != $last_easy_item){

                //返却値にエントリー
                $relates[ $row['grp_code'] ][ $row['easy_item_code'] ] = $row['sub_grp_code'];

                //前回の情報を更新
                $last_grp = $row['grp_code'];
                $last_easy_item = $row['easy_item_code'];
            }
        }

        return $relates;
    }



    /**
     * 指定した様式の報告書グループコードの表示順リストを取得します。
     * 
     * @param string $eis_id 様式ID
     * @param string $session セッションID(SM専用項目を対象外とする場合は指定不要。)
     * @param string $fname   画面名(SM専用項目を対象外とする場合は指定不要。)
     * @return array グループコード配列
     */
    function get_eis_grp_disp_list($eis_id, $session="",$fname="")
    {
        $sql  = " SELECT DISTINCT cate.cate_num, mate.cate_code, godr.order_no, gidx.grp_num, mate.grp_code";
        $sql .= " FROM inci_report_materials AS mate ";
        $sql .= " JOIN inci_easyinput_category_mst AS cate ON mate.cate_code = cate.cate_code ";
        $sql .= " JOIN inci_easyinput_group_mst AS grp ON grp.grp_code = mate.grp_code";
        $sql .= " left JOIN inci_easyinput_group_index as gidx ON grp.grp_code = gidx.grp_code";
        $sql .= " left JOIN inci_easyinput_set_group_order as godr ON godr.eis_id = $eis_id and grp.grp_code = godr.grp_code";
        $cond = " WHERE mate.easy_flag='1' AND mate.cate_code>0 AND (gidx.grp_num is not null or godr.grp_code is not null)";
        //当事者2以降を除く
        $cond .= " and not (mate.grp_code > 3000 and mate.grp_code < 3020 and mate.grp_code > 3040 and mate.grp_code < 3500 and not mate.grp_code::text like '___0')";
        //連動追加グループは400以外を除く
        $cond .= " and not (mate.grp_code > 400 and mate.grp_code < 500)";
        //未使用グループを除く
        $cond .= " and not mate.grp_code = 95";
        //SM以外の場合はSM専用項目を除く
        if($session == "" || !is_sm_emp($session,$fname))
        {
            $cond .= " and not mate.grp_code = 690";
        }
        $cond .= " ORDER BY cate.cate_num, mate.cate_code, godr.order_no, gidx.grp_num, mate.grp_code";

        $sel = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $grp_disp_list = null;
        $db_data = pg_fetch_all($sel);
        foreach($db_data as $db_record)
        {
            $grp_disp_list[] = $db_record["grp_code"];
        }

        return $grp_disp_list;
    }

    /**
     * 報告書グループコードの表示順リストを取得します。
     * 
     * @param string $eis_id 様式ID
     * @param string $session セッションID(SM専用項目を対象外とする場合は指定不要。)
     * @param string $fname   画面名(SM専用項目を対象外とする場合は指定不要。)
     * @return array グループコード配列
     */
    function get_grp_disp_list($session="",$fname="")
    {
        $sql  = " select grp_code from inci_easyinput_group_index";
        //当事者2以降を除く
        $sql .= " where not (grp_code > 3000 and grp_code < 3020 and grp_code > 3040 and grp_code < 3500 and not grp_code::text like '___0')";
        //連動追加グループは400以外を除く
        $sql .= " and not (grp_code > 400 and grp_code < 500)";
        //未使用グループを除く
        $sql .= " and not grp_code = 95";
        //SM以外の場合はSM専用項目を除く
        if($session == "" || !is_sm_emp($session,$fname))
        {
            $sql .= " and not grp_code = 690";
        }
        $sql .= " order by grp_num";

        $sel = select_from_table( $this->_db_con, $sql, "", $this->file_name );
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $grp_disp_list = null;
        $db_data = pg_fetch_all($sel);
        foreach($db_data as $db_record)
        {
            $grp_disp_list[] = $db_record["grp_code"];
        }

//      if($session != "" && is_sm_emp($session,$fname))
//      {
//          //SM専用項目あり
//          $grp_disp_list = array(100,110,3000,3050,3100,3150,3200,3250,3300,3350,3400,3450,3500,200,210,230,240,243,246,250,260,270,280,290,125,120,400,130,500,520,530,570,540,550,580,650,90,96,600,605,510,590,610,620,625,630,635,690 );
//      }
//      else
//      {
//          //SM専用項目なし
//          $grp_disp_list = array(100,110,3000,3050,3100,3150,3200,3250,3300,3350,3400,3450,3500,200,210,230,240,243,246,250,260,270,280,290,125,120,400,130,500,520,530,570,540,550,580,650,90,96,600,605,510,590,610,620,625,630,635 );
//      }
        return $grp_disp_list;
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 項目情報：　連動項目情報の取得
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * 報告書登録用の、項目値に対する連動グループコードを取得する
     *
     * 返却値の配列構造は以下のとおり
     * array[{グループコード}][{項目コード}][0-N][easy_code] = 連動元選択項目コード
     * array[{グループコード}][{項目コード}][0-N][rel_grp_code] = 連動先グループコード
     *
     */
    function get_relate_item_code_group(){
        $sql  = "SELECT *  FROM inci_easyinput_relate_group";
        $cond = "ORDER BY grp_code, sub_grp_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $relates = array();
        //$last_grp = "";
        //$last_easy_item = "";
        while ( $row = pg_fetch_assoc($result) ){

            $relates[ $row['grp_code'] ][ $row['easy_item_code'] ][] = array(
                'easy_code' => $row['easy_code'],
                'rel_grp_code' => $row['sub_grp_code']
            );

        }

        return $relates;
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 項目情報：　様式定義画面専用
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // 報告書様式設定用に、分類と、それに属するグループのリストを取得する
    function get_report_category_list($session=""){
        $rel_grps = $this->get_relate_groups();

        $cate_list = array();
        $last_cate = "";
        $cate_idx = -1;
        $cate_7_idx = -1;
        $tmp_125 = null;

        $sql  = " SELECT DISTINCT cate.cate_num, mate.cate_code, cate.cate_name, mate.grp_code, grp.grp_name, gidx.grp_num";
        $sql .= " FROM inci_report_materials AS mate ";
        $sql .= " JOIN inci_easyinput_category_mst AS cate ON mate.cate_code = cate.cate_code ";
        $sql .= " JOIN inci_easyinput_group_mst AS grp ON grp.grp_code = mate.grp_code";
        $sql .= " left JOIN inci_easyinput_group_index as gidx ON grp.grp_code = gidx.grp_code";
        $cond = " WHERE easy_flag='1' AND mate.cate_code>0 AND (gidx.grp_num is not null or grp.grp_code >= 10000)";
        $cond .= " ORDER BY cate.cate_num, mate.cate_code, gidx.grp_num, mate.grp_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $all_list = pg_fetch_all($result);
        foreach ( $all_list as $item ){

            //「追加」により増える項目の場合（当事者）
            if ( preg_match("/^3[0-4][05][1-9]$/", $item['grp_code']) )
            {
                //対象外
                continue;
            }

            //「追加」により増える項目の場合（関連する患者情報）
            if ( preg_match("/^3[1-9][1-9]$/", $item['grp_code']) )
            {
                //対象外
                continue;
            }

            //項目がSM専用項目の場合で、ユーザーの権限を特定できないorSM以外の場合
            if($item['grp_code'] == 690 && ($session == "" || !is_sm_emp($session,$this->file_name)))
            {
                //対象外
                continue;
            }

            // 「分類」を「分析」カテゴリへ移すため「何をどうした」カテゴリでは値だけ保持してスルー 20150204 T.N
            if ($item['cate_code'] == 4 && $item['grp_code'] == 125) {
                $tmp_125 = array(
                    'grp_code' => $item['grp_code'],
                    'grp_name' => $item['grp_name'],
                    'rel_grp'  => $rel_grps[$item['grp_code']]);
                continue;
            }
            
            //前回のカテゴリコードと異なる場合
            if ( $item['cate_code'] != $last_cate ){
                //前回のカテゴリコードを更新
                $last_cate = $item['cate_code'];
                $cate_idx++;

                $cate_list[$cate_idx] = array(
                    'cate_code' => $item['cate_code'],
                    'cate_name' => $item['cate_name'],
                    'grp_list'  => array()
                );

                // 「分析」カテゴリのインデックスを保持
                if ($item['cate_code'] == 7){
                    $cate_7_idx = $cate_idx;
                }
            }

            $cate_list[$cate_idx]['grp_list'][] = array(
                'grp_code' => $item['grp_code'],
                'grp_name' => $item['grp_name'],
                'rel_grp'  => $rel_grps[ $item['grp_code'] ]
            );
        }

        if ($tmp_125 != null && $cate_7_idx != -1){
             // 「分析」カテゴリの先頭に「分類」を挿入
            $cate_list[$cate_7_idx]['grp_list'] = array_merge(array($tmp_125), $cate_list[$cate_7_idx]['grp_list']);
        }

        return $cate_list;

    }
    /**
     * (Private関数)
     * 報告書様式設定用連動グループを取得する
     *
     * 返却値の配列構造は以下のとおり
     * array[{グループコード}]["grp_code"] = 連動先グループコード
     * array[{グループコード}]["grp_name"] = 連動先グループ名
     *
     */
    function get_relate_groups(){

        $sql  = "SELECT rel_grp.*, grp_mst.grp_name  FROM inci_easyinput_relate_group AS rel_grp JOIN inci_easyinput_group_mst AS grp_mst ON rel_grp.sub_grp_code=grp_mst.grp_code";
        $cond = "ORDER BY grp_code, sub_grp_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $relates = array();
        $last_grp = "";
        while ( $row = pg_fetch_assoc($result) ){

            //グループコードが前回と違う場合
            if ( $row['grp_code'] != $last_grp ){

                //返却値にエントリー
                $relates[ $row['grp_code'] ] = array(
                    'grp_code' => $row['sub_grp_code'],
                    'grp_name' => $row['grp_name']
                );

                //前回の情報を更新
                $last_grp = $row['grp_code'];
            }
        }

        return $relates;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 様式　ID関連
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//  /**
//   * 指定された業種の様式定義IDを取得します。
//   *
//   * @param string $job_id 業種ID
//   * @return string 様式定義ID、取得できない場合は空文字。
//   */
//  function get_eis_id_from_job($job_id)
//  {
//      $sql = "select eis_id from inci_easyinput_set";
//      $cond = "where job_id = $job_id and not del_flag = 't'";
//      $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
//      if ($sel == 0) {
//          pg_query($this->_db_con,"rollback");
//          pg_close($this->_db_con);
//          showErrorPage();
//          exit;
//      }
//      $num = pg_num_rows($sel);
//      if ($num == 0) {
//          return "";
//      }
//
//      return pg_result($sel,0,"eis_id");
//  }

    /**
     * 指定された様式番号の様式定義IDを取得します。
     *
     * @param string $eis_no 様式番号
     * @return string 様式定義ID、取得できない場合は空文字。
     */
    function get_eis_id_from_eis_no($eis_no)
    {
        $sql = "select eis_id from inci_easyinput_set";
        $cond = "where eis_no = $eis_no and not del_flag = 't'";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num == 0) {
            return "";
        }

        return pg_result($sel,0,"eis_id");
    }

    /**
     * 指定された様式定義IDの様式番号を取得します。
     *
     * @param string $eis_no 様式定義ID
     * @return string 様式番号、取得できない場合は空文字。
     */
    function get_eis_no_from_eis_id($eis_id)
    {
        $sql = "select eis_no from inci_easyinput_set";
        $cond = "where eis_id = $eis_id";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num == 0) {
            return "";
        }

        return pg_result($sel,0,"eis_no");
    }


    /**
     * 職種IDよりデフォルトの様式番号を取得します。
     */
    function get_default_eis_no_from_job($job_id)
    {
        $sql = "select eis_no from inci_set_job_link where job_id = $job_id and default_flg";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num == 0) {
            return "";
        }

        return pg_result($sel,0,"eis_no");
    }

    /**
     * 指定された様式番号の様式名称を取得します。
     *
     * @param string $eis_no 様式番号
     * @return string 様式名称。取得できない場合は空文字。
     */
    function get_eis_name_from_eis_no($eis_no)
    {
        $sql = "select eis_name from inci_easyinput_set where eis_no = $eis_no and not del_flag";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num == 0) {
            return "";
        }

        return pg_result($sel,0,"eis_name");
    }



    /**
     * 指定された様式番号の様式スタイルを取得します。
     *
     * @param string $eis_no 様式番号
     * @return integer 様式スタイル。取得できない場合は、1。
     */
    function get_eis_style_from_eis_no($eis_no)
    {
        $sql = "select style_code from inci_easyinput_set where eis_no = $eis_no and not del_flag";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $num = pg_num_rows($sel);
        if ($num == 0) {
            return 2;
        }

        $style_code = pg_result($sel,0,"style_code");
        if (!$style_code) $style_code=1;
        return $style_code;
    }





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 様式　保存/取得
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * 報告書様式をDBに保存する。
     *
     * @param string $eis_name 様式名称。
     * @param array $use_grp_code 使用するグループコード配列。
     * @param integer $style_code 様式スタイル
     * @param array $must_list 必須配列
     * @param string $user_cat_name ユーザ定義カテゴリ名称
     * @param string $eis_no 様式番号
     * @param string $order_no ソート番号
     */
    function set_report_flags( $eis_name,$use_grp_code,$style_code,$must_list,$user_cat_name, $eis_no="",$order_no="" )
    {

        //==============================
        // 様式IDを採番
        //==============================
        $eis_id = get_col_max($this->_db_con, "inci_easyinput_set", "eis_id", $this->file_name);
        $eis_id++;

        //==============================
        // 様式番号を採番　(様式番号がない(新規)場合のみ)
        //==============================
        if($eis_no == "")
        {
            $eis_no = get_col_max($this->_db_con, "inci_easyinput_set", "eis_no", $this->file_name);
            $eis_no++;
        }

        //==============================
        // ソート番号を採番 (ソート番号がない(新規)場合のみ)
        //==============================
        if($order_no == "")
        {
            $order_no = get_col_max($this->_db_con, "inci_easyinput_set", "order_no", $this->file_name);
            $order_no++;
        }

        //==============================
        // 旧データがあれば論理削除 (旧データは様式番号が等しい)
        //==============================
        $sql = "update inci_easyinput_set set";
        $set = array("del_flag");
        $setvalue = array('t');
        $cond = "WHERE eis_no = " . p( $eis_no ) . " and del_flag='f'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);

        //==============================
        // 新規保存
        //==============================

        // SQL文用項目文字列を作成
        $cols = array( 'eis_id', 'eis_name', 'job_id', 'wf_route_id', 'use_grp_code', 'eis_no', 'order_no', 'style_code', 'user_cate_name' );
        $cols_str = join( ", ", $cols );

        // DB登録用データを作成
        $cols_val = array(
            $eis_id,
            p($eis_name),
            1,                  // 1をダミー登録。
            99,                 // NotNullのため、ダミーデータをセット。
            join("\t", $use_grp_code),
            $eis_no,
            $order_no,
            $style_code,
            p($user_cat_name)
        );

        // 作成したデータをDBに登録
        $sql = "insert into inci_easyinput_set($cols_str) values(";
        $result = insert_into_table($this->_db_con, $sql, $cols_val, $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        //==============================
        // 必須設定を保存
        //==============================
        $this->set_item_must_grp($eis_id,$must_list);


        return $eis_id;
    }


    // 簡易報告書様式設定をDBから読み込む
    function get_report_flags($eis_id){
        $sql  = "SELECT * FROM inci_easyinput_set";
        $cond = "WHERE eis_id=" . p($eis_id);
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $flags = pg_fetch_assoc($result);
        $flags['use_grp_code'] = preg_split("/\t/", $flags['use_grp_code'], -1, PREG_SPLIT_NO_EMPTY);
        return $flags;
    }


    /**
     * 報告書様式をDBに保存する。(項目コードごとに登録)
     *
     * @param string $eis_id 様式ID。
     * @param string $grp_code グループコード。
     * @param array  $arr_easy_item_code 項目コード配列。
     */
    function set_report_flags_for_easy_item_code($eis_id, $grp_code, $arr_easy_item_code)
    {
        foreach($arr_easy_item_code as $easy_item_code)
        {
            $sql = "insert into inci_easyinput_set2 (eis_id, grp_code, easy_item_code) values(";
            $content = array($eis_id, $grp_code, $easy_item_code);

            $result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con, "rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }
        }
    }

    /**
     * 必須設定をDBに保存する。(項目コードごとに登録)
     *
     * @param string $eis_id 様式ID。
     * @param string $grp_code グループコード。
     * @param array  $arr_easy_item_code 項目コード配列。
     */
    function set_item_must_for_easy_item_code($eis_id, $grp_code, $arr_easy_item_code)
    {
        foreach($arr_easy_item_code as $easy_item_code)
        {
            // 影響と対応のイレギュラー設定
            if(($grp_code == '1400' && in_array($easy_item_code, array(20,40,50,60,70))) || ($grp_code == '1410' && in_array($easy_item_code, array(30,40)))) continue;

            $sql = "insert into inci_set_item_must (eis_id, grp_code, easy_item_code) values(";
            $content = array($eis_id, $grp_code, $easy_item_code);

            $result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con, "rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }
        }
    }

    /**
     * 報告書様式をDBに保存する。(選択コードごとに登録)　20140423 R.C
     *
     * @param string $eis_id 様式ID。
     * @param string $grp_code グループコード。
     * @param string $easy_item_code 項目コード。
     * @param array  $arr_easy_code 選択コード配列。
     */
    function insert_inci_easyinput_other_disp($eis_id, $grp_code, $easy_item_code, $arr_easy_code)
    {
    	foreach($arr_easy_code as $easy_code)
    	{
    		$sql = "insert into inci_easyinput_other_disp (eis_id, grp_code, easy_item_code, easy_code) values(";
    		$content = array($eis_id, $grp_code, $easy_item_code, $easy_code);

    		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
    		if ($result == 0) {
    			pg_query($this->_db_con, "rollback");
    			pg_close($this->_db_con);
    			showErrorPage();
    			exit;
    		}
    	}
    }

    /**
     * 報告書様式2の項目コードを取得する。
     * (現在は患者プロフィール項目／インシデントの概要連動項目のみ格納されている)
     *
     * @param string $eis_id 様式ID。
     * @param string $grp_code グループコード。
     * @return 項目コード配列。
     */
    function get_inci_easyinput_set2($eis_id, $grp_code)
    {
        $sql   = "SELECT easy_item_code FROM inci_easyinput_set2";
        $cond  = "WHERE eis_id=" . p($eis_id);
        $cond .= " AND grp_code =". p($grp_code);
        $cond .= " ORDER BY easy_item_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $arr = array();
        while($row = pg_fetch_array($result))
        {
            $arr[] = $row["easy_item_code"];
        }
        return $arr;
    }

    /**
     * 報告書様式2のグループ／項目コードを取得する。
     * (現在は患者プロフィール項目／インシデントの概要連動項目のみ格納されている)
     *
     * @param string $eis_id 様式ID。
     * @param array $use_grp_code 使用可能なグループコード配列
     * @return 様式情報の配列。$arr[{グループコード}][]={項目コード}
     */
    function get_inci_easyinput_set2_all($eis_id,$use_grp_code)
    {
        if($eis_id == "")
        {
            //様式が無い場合は使用可能なグループコードの全てを取得
            $sql   = "select grp_code,easy_item_code from inci_easyinput_item_mst ";
            if(!in_array("210",$use_grp_code) && !in_array("400",$use_grp_code))
            {
                $cond  = "where false ";
            }
            if(in_array("210",$use_grp_code) && !in_array("400",$use_grp_code))
            {
                $cond  = "where grp_code in(210) ";
            }
            if(!in_array("210",$use_grp_code) && in_array("400",$use_grp_code))
            {
                $cond  = "where grp_code in(400,410,420,430,440,450,460,470,480,490) ";
            }
            if(in_array("210",$use_grp_code) && in_array("400",$use_grp_code))
            {
                $cond  = "where grp_code in(210,400,410,420,430,440,450,460,470,480,490) ";
            }
            $cond .= "order by grp_code,easy_item_code ";
        }
        else
        {
            //様式がある場合は様式に情報を検索
            $sql   = "SELECT grp_code,easy_item_code FROM inci_easyinput_set2";
            $cond  = "WHERE eis_id=" . p($eis_id);
            $cond .= " ORDER BY grp_code,easy_item_code";
        }

        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $arr = "";
        while($row = pg_fetch_array($result))
        {
            $arr[$row["grp_code"]][] = $row["easy_item_code"];
        }
        return $arr;
    }

    /**
     * 報告書様式の子項目コードを取得する。 20140423 R.C
     *
     * @param string $eis_id 様式ID。
     * @param string $grp_code グループコード。
     * @param string $easy_item_code 項目コード。
     * @param string $easy_code 選択コード。
     * @return 選択コード配列。
     */
    function get_inci_easyinput_other_disp($eis_id, $grp_code, $easy_item_code)
    {
    	$sql   = "SELECT easy_code FROM inci_easyinput_other_disp";
    	$cond  = "WHERE eis_id=" . p($eis_id);
    	$cond .= " AND grp_code =". p($grp_code);
    	$cond .= " AND easy_item_code =". p($easy_item_code);
    	$cond .= " ORDER BY easy_code";
    	$result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
    	if ($result == 0) {
    		pg_query($this->_db_con,"rollback");
    		pg_close($this->_db_con);
    		showErrorPage();
    		exit;
    	}

    	$arr = array();
    	while($row = pg_fetch_array($result))
    	{
    		$arr[] = $row["easy_code"];
    	}
    	return $arr;
    }

    /**
     * 簡易報告書項目名マスタを取得する。
     *
     * @param string $grp_code グループコード。
     * @return 簡易報告書項目名マスタ配列。
     */
    function get_inci_easyinput_item_mst($grp_code)
    {

        $sql   = "SELECT * FROM inci_easyinput_item_mst";
        $cond  = "WHERE grp_code=" . p($grp_code);
        $cond .= " ORDER BY easy_item_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_all($result);
    }

    /**
     * 様式ID・グループコードより必須グループ情報を取得します。
     *
     * @param string $eis_id 様式ID
     * @param string $grp_code グループコード
     * @return array 必須グループ情報。(項目コードの配列)
     */
    function get_item_must_for_easy_item_code($eis_id, $grp_code)
    {
        $sql  = "select easy_item_code from inci_set_item_must";
        $cond = "where eis_id = " . p($eis_id) . " and grp_code = " . p($grp_code). " order by easy_item_code ";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $arr = array();
        while($row = pg_fetch_array($result))
        {
            $arr[] = $row["easy_item_code"];
        }
        return $arr;
    }





    /**
     * 様式設定のインシデントの概要の項目グループに対する使用設定を取得します。
     *
     * @param integer $eis_id 様式ID
     * @return array 使用する項目グループ配列
     */
    function get_eis_400_use($eis_id)
    {
        $ret_array = "";
        $conv_table = $this->get_conv_table_eis_400();

        //各項目グループに対して
        foreach($conv_table as $item_grp => $grp_list)
        {
            //先頭の項目の設定を取得
            foreach($grp_list as $grp_code => $item_list)
            {
                foreach($item_list as $easy_item_code => $is_mustable)
                {
                    $sql  = "";
                    $sql .= " select count(*) as cnt from inci_easyinput_set2";
                    $sql .= " where eis_id = $eis_id";
                    $sql .= " and grp_code = $grp_code";
                    $sql .= " and easy_item_code = $easy_item_code";
                    $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );
                    if ($result == 0) {
                        pg_query($this->_db_con,"rollback");
                        pg_close($this->_db_con);
                        showErrorPage();
                        exit;
                    }

                    if(pg_fetch_result($result,0,'cnt') > 0)
                    {
                        $ret_array[] = $item_grp;
                    }

                    //2番目以降の項目は判定不要
                    break;
                }

                //2番目以降の項目は判定不要
                break;
            }
        }

        return $ret_array;
    }
    /**
     * 様式設定のインシデントの概要の項目グループに対する使用設定を保存します。
     *
     * @param integer $eis_id 様式ID
     * @param array $summary_use_item_grp_code 使用する項目グループ配列
     */
    function set_eis_400_use($eis_id,$summary_use_item_grp_code)
    {
        $conv_table = $this->get_conv_table_eis_400();

        //各項目グループに対して
        foreach($conv_table as $item_grp => $grp_list)
        {
            //項目グループが対象となっている場合
            if(in_array($item_grp,$summary_use_item_grp_code))
            {
                //項目グループに属する全ての項目をエントリー
                foreach($grp_list as $grp_code => $item_list)
                {
                    foreach($item_list as $easy_item_code => $is_mustable)
                    {
                        $sql = "insert into inci_easyinput_set2 (eis_id, grp_code, easy_item_code) values(";
                        $content = array($eis_id, $grp_code, $easy_item_code);
                        $result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
                        if ($result == 0) {
                            pg_query($this->_db_con, "rollback");
                            pg_close($this->_db_con);
                            showErrorPage();
                            exit;
                        }
                    }
                }
            }
        }
    }
    /**
     * 様式設定のインシデントの概要の項目グループに対する必須設定を取得します。
     *
     * @param integer $eis_id 様式ID
     * @return array 必須とする項目グループ配列
     */
    function get_eis_400_must($eis_id)
    {
        $ret_array = "";
        $conv_table = $this->get_conv_table_eis_400();

        //各項目グループに対して
        foreach($conv_table as $item_grp => $grp_list)
        {
            //先頭の項目の設定を取得
            foreach($grp_list as $grp_code => $item_list)
            {
                foreach($item_list as $easy_item_code => $is_mustable)
                {
                    //必須指定不可能項目は除外する。
                    if(!$is_mustable)
                    {
                        continue;
                    }

                    $sql  = "";
                    $sql .= " select count(*) as cnt from inci_set_item_must";
                    $sql .= " where eis_id = $eis_id";
                    $sql .= " and grp_code = $grp_code";
                    $sql .= " and easy_item_code = $easy_item_code";
                    $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );
                    if ($result == 0) {
                        pg_query($this->_db_con,"rollback");
                        pg_close($this->_db_con);
                        showErrorPage();
                        exit;
                    }

                    if(pg_fetch_result($result,0,'cnt') > 0)
                    {
                        $ret_array[] = $item_grp;
                    }

                    //2番目以降の項目は判定不要
                    break;
                }

                //2番目以降の項目は判定不要
                break;
            }
        }

        return $ret_array;
    }
    /**
     * 様式設定のインシデントの概要の項目グループに対する必須設定を保存します。
     *
     * @param integer $eis_id 様式ID
     * @param array $summary_must_item_grp_code 必須とする項目グループ配列
     */
    function set_eis_400_must($eis_id,$summary_must_item_grp_code)
    {
        $conv_table = $this->get_conv_table_eis_400();

        //各項目グループに対して
        foreach($conv_table as $item_grp => $grp_list)
        {
            //項目グループが対象となっている場合
            if(in_array($item_grp,$summary_must_item_grp_code))
            {
                //項目グループに属する全ての必須可能項目をエントリー
                foreach($grp_list as $grp_code => $item_list)
                {
                    foreach($item_list as $easy_item_code => $is_mustable)
                    {
                        if($is_mustable)
                        {
                            $sql = "insert into inci_set_item_must (eis_id, grp_code, easy_item_code) values(";
                            $content = array($eis_id, $grp_code, $easy_item_code);
                            $result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
                            if ($result == 0) {
                                pg_query($this->_db_con, "rollback");
                                pg_close($this->_db_con);
                                showErrorPage();
                                exit;
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * インシデントの概要の項目グループのコード変換テーブルを取得します。
     *
     * @return array 変換テーブル(array['項目グループコード']['グループコード']['項目コード']='必須可能(boolean)')
     */
    function get_conv_table_eis_400()
    {
        $sql  = "";
        $sql .= " select";
        $sql .= " m.item_grp_code,";
        $sql .= " m.grp_code,";
        $sql .= " m.easy_item_code,";
        $sql .= " t.item_must_able";
        $sql .= " from ";
        $sql .= " (";

        $sql .= " select";
        $sql .= " case easy_item_name ";
        $sql .= " when '発生場面' then 110";
        $sql .= " when '内容' then 120";
        $sql .= " when '内容その他' then 120";
        $sql .= " else 130 end as item_grp_code,";
        $sql .= " grp_code,";
        $sql .= " easy_item_code";
        $sql .= " from inci_easyinput_item_mst";
        $sql .= " where grp_code >= 400 and grp_code < 500";

        $sql .= " ) m";
        $sql .= " left join inci_easyinput_item_type t";
        $sql .= " on m.grp_code = t.grp_code";
        $sql .= " and m.easy_item_code = t.easy_item_code";
        $result = select_from_table( $this->_db_con, $sql, "", $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $conv_table = "";
        $data_arr = pg_fetch_all($result);
        foreach($data_arr as $data)
        {
            $conv_table[  $data['item_grp_code']  ][  $data['grp_code']  ][  $data['easy_item_code']  ] =  ($data['item_must_able'] == 1);
        }

        return $conv_table;
    }


    // 該当職種の簡易報告書様式設定をDBから読み込む
    function get_report_flags_by_eis_no($eis_no){
        $eis_id = $this->get_eis_id_from_eis_no($eis_no);
        if($eis_id == "")
        {
            $flags = array();
            $flags['use_grp_code'] = array();
            return $flags;
        }

        $flags = $this->get_report_flags($eis_id);
        return $flags;
    }





    /**
     * 報告書画面にて固定様式で使用するグループコードを取得します。
     * ※ＳＭ専用項目は含みません。
     * ※当事者２以降は含みません。
     * ※カテゴリ４にカテゴリ０を含みます。
     *
     * @param integer $cate_code カテゴリコード(省略時は全項目)
     * @return array 表示するグループコード配列
     */
    function get_original_eis_grps($cate_code="")
    {
        $sql  = " select grp_code";
        $sql .= " from";
        $sql .= " (";
        $sql .= " select distinct grp_code from inci_report_materials";
        $sql .= " where grp_code <> 690";
        $sql .= " and not (grp_code > 3000 and grp_code < 3500 and grp_code <> 3032 and grp_code <> 3034 and grp_code <> 3036 and grp_code <> 3038 and not grp_code::text like '___0')";

        if($cate_code == 4)
        {
            $sql .= " and (cate_code = 4 or cate_code = 0)";
        }
        elseif($cate_code != "")
        {
            $sql .= " and cate_code = $cate_code";
        }

        $sql .= " ) a";
        $sql .= " natural left join inci_easyinput_group_index";
        $sql .= " order by grp_num, grp_code";

        $sel = select_from_table( $this->_db_con, $sql, "", $this->file_name );
        if ($sel == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $grps = null;
        $db_data = pg_fetch_all($sel);
        foreach($db_data as $db_record)
        {
            $grps[] = $db_record["grp_code"];
        }

        return $grps;
    }


    /**
     * 全レポート項目のグループコード一覧を返します。
     * 但し、追加により増える項目は含まれません。
     *
     * @return array グループコード配列
     */
    function get_full_grps()
    {
        return $this->get_original_eis_grps();
    }



    /**
     * 分析・再発防止用様式のレポートで有効なグループコード一覧を返します。
     * @param string $session セッションID
     * @return array グループコード配列
     */
    function get_analysis_grps($session="")
    {
        //セッションが指定されていない場合は分析全項目を返却します。
        if($session == "")
        {
            return $this->get_original_eis_grps(7);
        }

        //ユーザーの権限を特定
        $arr = get_my_inci_auth_list($session,$this->file_name,$this->_db_con);
        $arr_auth = array();
        if($arr)
        {
            for($i = 0; $i < count($arr); $i++)
            {
                $auth = $arr[$i]["auth"];
                array_push($arr_auth, $auth);
            }
        }
        else
        {
            array_push($arr_auth, "GU");
        }

        //分析・再発防止画面の更新可能グループを取得
        $auths = "";
        foreach($arr_auth as $val)
        {
            if($auths != "")
            {
                $auths .= ",";
            }

            $auths .= "'";
            $auths .= $val;
            $auths .= "'";
        }
        $sql  = "select distinct grp_code from inci_auth_analysis_use_item";
        $cond = "where auth in ($auths) and auth in(select auth from inci_auth_analysis_use where use_flg)";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $result_array = pg_fetch_all($result);
        $analysis_grps = array();
        foreach($result_array as $result1)
        {
            $analysis_grps[] = $result1["grp_code"];
            if ($result1["grp_code"]=='1100'){
                $analysis_grps[] = '1110';
                $analysis_grps[] = '1120';
            }
        }
        return $analysis_grps;
    }










    /**
     * 様式が定義されている職種(job_id)の配列を返します。
     *
     * @return array 職種IDの配列
     */
    function get_eis_job_list()
    {
        // 定義済み簡易報告書様式を取得

//      $sql = "select job_id from inci_easyinput_set ";
//      $cond = "where del_flag = 'f' order by job_id";
        $sql = "select distinct job_id from inci_set_job_link order by job_id";
        $cond = "";
        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0) {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $job_ids = array();
        for ( $i = 0; $i < pg_num_rows($result); $i++ ){
            array_push( $job_ids, pg_result($result, $i, 0) );
        }

        return $job_ids;
    }


    /**
     * 様式-職種のリンク情報を取得します。
     */
    function get_eis_job_link_info()
    {
        $sql  = " select    jt.job_id, et.eis_no, et.eis_name, lt.default_flg";
        $sql .= " from      inci_set_job_link  lt";
        $sql .= " left join jobmst             jt on lt.job_id = jt.job_id";
        $sql .= " left join inci_easyinput_set et on lt.eis_no = et.eis_no";
        $sql .= " where     not et.del_flag";//jobmstの論理削除は判定しない。(論理削除済み職種でもその職種でレポートが作成されている可能性があるため)
        $sql .= " order by  jt.order_no , et.order_no";
        $cond = "";
        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0) {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        if(pg_num_rows($result) == 0)
        {
            return false;
        }
        else
        {
            return pg_fetch_all($result);
        }

    }

    /**
     * 様式-職種のリンク情報をレポート情報から取得します。
     * ※get_eis_job_link_info()と同形式の結果を返します。
     */
    function get_eis_job_link_info_from_report_rireki($report_id)
    {
        $sql  = "";
        $sql .= " select c.job_id,c.eis_no,d.eis_name,false as default_flg from ";
        $sql .= " (";
            $sql .= " select job_id,eis_no,max(mail_id) as last_mail_id from";
            $sql .= " (";
                $sql .= " select report_id,mail_id,job_id,eis_id from inci_mail_send_info where report_id = $report_id";
            $sql .= " ) a";
            $sql .= " left join ";
            $sql .= " (";
                $sql .= " select eis_id,eis_no from inci_easyinput_set";
            $sql .= " ) b";
            $sql .= " on a.eis_id = b.eis_id";
            $sql .= " group by job_id,eis_no";
        $sql .= " ) c";
        $sql .= " left join ";
        $sql .= " (";
        $sql .= " select eis_no,eis_name from inci_easyinput_set where not del_flag";
        $sql .= " ) d";
        $sql .= " on c.eis_no = d.eis_no";
		$sql .= " order by last_mail_id desc";
        $cond = "";
        $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($result == 0) {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        if(pg_num_rows($result) == 0)
        {
            return false;
        }
        else
        {
            return pg_fetch_all($result);
        }

    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 様式(必須項目情報)　保存/取得
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * 様式IDより必須項目情報を取得します。
     *
     * @param string $eis_id 様式ID
     * @return array 必須項目情報。(グループコード_項目コードの配列)
     */
    function get_item_must_from_eis_id($eis_id)
    {
        $sql  = "select * from inci_set_item_must";
        $cond = "where eis_id = " . p($eis_id) . " order by grp_code,easy_item_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $must_list = array();
        while ( $row = pg_fetch_assoc($result) )
        {
            $must_list[] =  $row['grp_code']."_".$row['easy_item_code'];
        }
        return $must_list;
    }

//  /**
//   * 職種IDより必須項目情報を取得します。
//   *
//   * @param string $job_id 職種ID
//   * @return array 必須項目情報。(グループコード_項目コードの配列)
//   */
//  function get_item_must_from_job_id($job_id)
//  {
//      $eis_id = $this->get_eis_id_from_job($job_id);
//      if($eis_id == "")
//      {
//          return array();
//      }
//      return $this->get_item_must_from_eis_id($eis_id);
//  }

    /**
     * 様式番号より必須項目情報を取得します。
     *
     * @param string $eis_no 様式番号
     * @return array 必須項目情報。(グループコード_項目コードの配列)
     */
    function get_item_must_from_eis_no($eis_no)
    {
        $eis_id = $this->get_eis_id_from_eis_no($eis_no);
        if($eis_id == "")
        {
            return array();
        }
        return $this->get_item_must_from_eis_id($eis_id);
    }








    /**
     * 様式IDより必須グループ情報を取得します。
     *
     * @param string $eis_id 様式ID
     * @return array 必須グループ情報。(グループコードの配列)
     */
    function get_item_must_grp_from_eis_id($eis_id)
    {
        $sql  = "select distinct grp_code from inci_set_item_must";
        $cond = "where eis_id = " . p($eis_id) . " order by grp_code";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $must_list = array();
        while ( $row = pg_fetch_assoc($result) )
        {
            $must_list[] =  $row['grp_code'];
        }
        return $must_list;
    }

//  /**
//   * 様式IDより必須グループ情報を取得します。
//   *
//   * @param string $job_id 職種ID
//   * @return array 必須グループ情報。(グループコードの配列)
//   */
//  function get_item_must_grp_from_job_id($job_id)
//  {
//      $eis_id = $this->get_eis_id_from_job($job_id);
//      if($eis_id == "")
//      {
//          return array();
//      }
//      return $this->get_item_must_grp_from_eis_id($eis_id);
//  }

    /**
     * 様式番号より必須グループ情報を取得します。
     *
     * @param string $eis_no 様式番号
     * @return array 必須グループ情報。(グループコードの配列)
     */
    function get_item_must_grp_from_eis_no($eis_no)
    {
        $eis_id = $this->get_eis_id_from_eis_no($eis_no);
        if($eis_id == "")
        {
            return array();
        }
        return $this->get_item_must_grp_from_eis_id($eis_id);
    }

    /**
     * 様式に対する必須項目情報を保存します。
     *
     * @param string $eis_id 様式ID
     * @param array $must_list 様式定義画面の必須指定の送信データ。
     */
    function set_item_must_grp($eis_id,$must_list)
    {
        //==============================
        //デリート
        //==============================
        //SQLサンプル
        //delete from inci_set_item_must where eis_id = 1

        //常にeis_idが異なる状況でこの関数が呼び出されるため、不要。

        //==============================
        //インサート
        //==============================

        //SQLサンプル
        //insert into inci_set_item_must(eis_id,grp_code,easy_item_code)
        //select 1 as eis_id,grp_code,easy_item_code from inci_easyinput_item_type
        //where item_must_able = 1
        //and grp_code in(100,120)

        $in_num=count($must_list);
        if($in_num != 0)
        {
            $in = $this->get_sql_in_grp_str_from_must_list($must_list);

            $sql  = " insert into inci_set_item_must(eis_id,grp_code,easy_item_code)";
            $sql .= " select $eis_id as eis_id,grp_code,easy_item_code from inci_easyinput_item_type";
            $sql .= " where item_must_able = '1'";
            $sql .= " and grp_code in($in)";
            $result = pg_query($this->_db_con,$sql);
            if($result == false)
            {
                pg_query($this->_db_con,"rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }
        }

    }
    /**
     * プレビュー画面専用関数です。
     * 必須項目情報配列(入力値)より必須項目情報配列(グループ/項目)を取得します。
     *
     * @param array $must_list 様式定義画面の必須指定の送信データ。
     * @return array 必須項目情報。(グループコード_項目コードの配列)
     */
    function get_item_must_for_preview($must_list)
    {
        $in_num=count($must_list);
        if($in_num != 0)
        {
            $in = $this->get_sql_in_grp_str_from_must_list($must_list);

            $sql  = " select grp_code,easy_item_code from inci_easyinput_item_type";
            $cond  = " where item_must_able = '1'";
            $cond .= " and grp_code in($in)";

            $result = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con,"rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }

            $must_list = array();
            while ( $row = pg_fetch_assoc($result) )
            {
                $must_list[] =  $row['grp_code']."_".$row['easy_item_code'];
            }
            return $must_list;
        }
    }
    /**
     * (Private)
     * 様式定義画面の必須指定の送信データより、SQLのin句用文字列を取得します。
     */
    function get_sql_in_grp_str_from_must_list($must_list)
    {
        $in = "";
        $in_num=count($must_list);
        for($i=0;$i<$in_num;$i++)
        {
            if($i != 0)
            {
                $in .= ",";
            }

            if($must_list[$i] == 400)
            {
                $in .= "400,410,420,430,440,450,460,470,480,490";
            }
            elseif ( ($must_list[$i] >= 3000 && $must_list[$i] < 4000) || ($must_list[$i] >= 310 && $must_list[$i] <= 390) )
            {
                for($i2=0;$i2<10;$i2++)
                {
                    if($i2 != 0)
                    {
                        $in .= ",";
                    }
                    $in .= ($must_list[$i] + $i2);
                }
            }
            else
            {
                $in .= $must_list[$i];
            }
        }

        return $in;
    }


    /**
     * 項目要素の非表示情報を取得します。
     *
     * @param integer $class_id 部署ID
     * @param integer $attribute_id 課ID
     * @param integer $dept_id 科ID
     * @param integer $room_id 室ID
     * @return array 項目非表示情報(array[{グループコード}][{項目コード}][]={非表示項目要素})
     */
    function get_item_element_no_disp($class_id = "", $attribute_id = "", $dept_id = "", $room_id = "")
    {

        //==============================
        //共通設定の取得
        //==============================
        $sql  = " select grp_code,easy_item_code,easy_code from inci_easyinput_item_element_no_disp";
        $sql .= " where class_id isnull and attribute_id isnull and dept_id isnull and room_id isnull";
        $sql .= " order by grp_code,easy_item_code,easy_code";
        $result = select_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $no_disp_info_list = pg_fetch_all($result);

        //==============================
        //返却形式の配列($arr_no_disp)形成
        //==============================
        $result_array = array();
        $arr_no_disp = array();
        $old_grp_code = "";
        $old_easy_item_code = "";
        foreach($no_disp_info_list as $no_disp_info)
        {

            $new_grp_code = $no_disp_info['grp_code'];
            $new_easy_item_code = $no_disp_info['easy_item_code'];
            $easy_code = $no_disp_info['easy_code'];

            // 初回(1件目)もしくは、前回と違う項目の場合
            if($old_grp_code == "" || ($old_grp_code != $new_grp_code && $old_easy_item_code != $new_easy_item_code))
            {
                // 配列作成
                $arr_no_disp[$new_grp_code][$new_easy_item_code] = array();
            }

            $arr_no_disp[$new_grp_code][$new_easy_item_code][] = $easy_code;

            $old_grp_code = $new_grp_code;
            $old_easy_item_code = $new_easy_item_code;

        }

        //==============================
        //部署別設定の対応
        //==============================
        if($class_id != "")
        {
            //==============================
            //部署別設定の有無を取得
            //==============================
            $sql  = " select grp_code,easy_item_code, max(class_id) as class_id,max(attribute_id) as attribute_id,max(dept_id) as dept_id,max(room_id) as room_id";
            $sql .= " from";
            $sql .= " (";
            $sql .= " select * from inci_easyinput_item_element_no_disp_override";
            $sql .= " where";
            $sql .= " (";
            $sql .= " (class_id = $class_id and attribute_id isnull and dept_id isnull and room_id isnull)";
            if($attribute_id != "")
            {
                $sql .= " or (class_id = $class_id and attribute_id = $attribute_id and dept_id isnull and room_id isnull)";
                if($dept_id != "")
                {
                    $sql .= " or (class_id = $class_id and attribute_id = $attribute_id and dept_id = $dept_id and room_id isnull)";
                    if($room_id != "")
                    {
                        $sql .= " or (class_id = $class_id and attribute_id = $attribute_id and dept_id = $dept_id and room_id = $room_id)";
                    }
                }
            }
            $sql .= " )";
            $sql .= " ) a";
            $sql .= " group by grp_code,easy_item_code";
            $result = select_from_table($this->_db_con, $sql, "", $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con,"rollback");
                pg_close($this->_db_con);
                showErrorPage();
                exit;
            }

            $no_disp_override_list = pg_fetch_all($result);

            //==============================
            //部署別設定で返却配列($arr_no_disp)を更新
            //==============================
            foreach($no_disp_override_list as $no_disp_override)
            {
                //==============================
                //部署別設定の取得
                //==============================
                $o_grp_code = $no_disp_override["grp_code"];
                $o_easy_item_code = $no_disp_override["easy_item_code"];
                $o_class = $no_disp_override["class_id"];
                $o_attribute = $no_disp_override["attribute_id"];
                $o_dept = $no_disp_override["dept_id"];
                $o_room = $no_disp_override["room_id"];

                $sql  = " select easy_code from inci_easyinput_item_element_no_disp";
                $sql .= " where ";
                $sql .= ($o_class == "")? "class_id isnull" : "class_id = $o_class";
                $sql .= " and ";
                $sql .= ($o_attribute == "")? "attribute_id isnull" : "attribute_id = $o_attribute";
                $sql .= " and ";
                $sql .= ($o_dept == "")? "dept_id isnull" : "dept_id = $o_dept";
                $sql .= " and ";
                $sql .= ($o_room == "")? "room_id isnull" : "room_id = $o_room";
                $sql .= " and grp_code = $o_grp_code and easy_item_code = $o_easy_item_code";
                $sql .= " order by grp_code,easy_item_code,easy_code";
                $result = select_from_table($this->_db_con, $sql, "", $this->file_name);
                if ($result == 0) {
                    pg_query($this->_db_con,"rollback");
                    pg_close($this->_db_con);
                    showErrorPage();
                    exit;
                }

                $no_disp_override_result = pg_fetch_all($result);

                //==============================
                //返却配列($arr_no_disp)を更新
                //==============================
                $tmp_arr = "";
                foreach($no_disp_override_result as $no_disp_override_result2)
                {
                    $tmp_arr[] = $no_disp_override_result2["easy_code"];
                }
                $arr_no_disp[$o_grp_code][$o_easy_item_code] = $tmp_arr;
            }
        }


        //==============================
        //当事者と患者区分を分解
        //==============================
        $result_array = $arr_no_disp;
        foreach($arr_no_disp as $grp_code => $arr_grp)
        {
            //当事者
            if ($grp_code >= 3050 && $grp_code <= 3450)
            {
                for($i=1;$i<10;$i++)
                {
                    $result_array[$grp_code + $i] = $arr_grp;
                }
            }

            //患者区分1
            if ($grp_code==230){
                for($i=0;$i<10;$i++){
                    $result_array[350 + $i][10] = $arr_grp[60];
                }
            }

            //患者区分2
            if ($grp_code==240){
                for($i=0;$i<10;$i++){
                    $result_array[360 + $i][10] = $arr_grp[70];
                }
            }
        }
        $arr_no_disp = $result_array;

        return $arr_no_disp;
    }


    /**
     * 様式で今期のテーマを入力する
     */
    function get_this_term_theme($eis_no) {
        $sql = "SELECT theme FROM inci_this_term_theme WHERE eis_no = {$eis_no}";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        return pg_fetch_row($result);
    }



    /**
     * 様式で今期のテーマを挿入する
     */
    function set_this_term_theme($eis_no, $theme) {
        $sql = "DELETE FROM inci_this_term_theme WHERE eis_no = '{$eis_no}'";
        $result = delete_from_table($this->_db_con, $sql, "", $this->file_name);
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $sql = "INSERT INTO inci_this_term_theme(eis_no, theme) VALUES(";
        $content = array($eis_no, p($theme));
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 様式(デフォルト値)　保存/取得
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * デフォルト値を論理削除します。
     *
     * @param string $eis_id 様式ID
    */
    function del_default_value($eis_id){
        $sql = "update inci_easyinput_set_default set";
        $set = array("del_flg");
        $setvalue = array("t");
        $cond = "where eis_id=".$eis_id;
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * デフォルト値を保存します。
     *
     * @param string $eis_id 様式ID
     * @param array  $inputs 様式画面送信データ配列
     * @param string $eis_id 前の様式ID
    */
    function set_default_value($eis_id, $inputs, $old_eis_id=0)
    {
        //前の様式IDがあれば論理削除
        if ($old_eis_id>0){
          $this->del_default_value($old_eis_id);
        }

        $db_cols = array('eis_id', 'grp_code', 'easy_item_code', 'input_item_default', 'timestamp');
        $cols_str = join(', ', $db_cols);
        $sql = "insert into inci_easyinput_set_default($cols_str) values(";

        foreach ( $inputs as $key => $value ){
            if ( preg_match( "/^def_(\d+)_(\d+)$/", $key, $matches ) ){
                if ( $value == "" ) continue;
                $val = ( is_array( $value ) ) ? join("\t", $value) : p($value);
                if ( $val == "" ) continue;
                $cols_val = array($eis_id, $matches[1], $matches[2], $val, date('Y-m-d H:i:s'));

                $result = insert_into_table($this->_db_con, $sql, $cols_val, $this->file_name);
                if ($result == 0) {
                    pg_query($this->_db_con,"rollback");
                    pg_close($this->_db_con);
                    showErrorPage();
                    exit;
                }
            }
        }
    }

    /**
     * デフォルト値を取得します。
     *
     * @param string $eis_id 様式ID
     * @return array デフォルト値
    */
    function get_default_value($eis_id)
    {
        $sql = "SELECT * FROM inci_easyinput_set_default";
        $cond = "WHERE eis_id=".$eis_id;
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );

        $default = array();
        while ( $row = pg_fetch_assoc($result) ) {
            $default[$row["grp_code"]][$row["easy_item_code"]] = $row["input_item_default"];
        }

        return $default;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// データ変換
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * フォーム入力形式データを、DB登録データ取得時の形式に変換します。
     *
     * @param array $inputs フォーム入力形式データ(Session形式データ)
     * @return array DB登録データ取得時の形式データ( get_report()['content']['input_items']に相当 )
     */
    function conv_form_item_codes($inputs){
        $input_items = array();

        foreach ( $inputs as $key => $value ){

            if ( preg_match( "/^_(\d+)_(\d+)$/", $key, $matches ) ){
                $val = ( is_array( $value ) ) ? $value : array($value);
                // 20150106TN タイムラグは$grp_flagsとして生成しない
                if(!($key=="_105_75")){
                    $input_items[ $matches[1] ][ $matches[2] ] = $val;
                }
            }
        }

        return $input_items;
    }


    /**
     * 入力・保存されたコードを確認用文言に変換します。
     *
     * @param array $grps DB登録データ取得時の形式( get_report()['content']['input_items']に相当 )
     * @return array $grpsの値を文字変換したもの
     */
    function get_easy_item_names( $grps ){
        $item_map = $this->get_all_easy_item_names();
        $item_types = $this->get_easy_item_types();
        $item_names = array();

        foreach ( $grps as $grp_key => $easy_items ){
            foreach ( $easy_items as $easy_item_key => $easy_codes ){
                foreach ( $easy_codes as $easy_code ){
                    if ( $easy_code == "" ) continue;
                    if ($grp_key == 110 and $easy_item_key == 65) {
                        $item_names[ $grp_key ][ $easy_item_key ][] = $item_map[115][$grps[110][60][0]][$easy_code] ?
                        h($item_map[115][$grps[110][60][0]][$easy_code]) :
                        preg_replace( "/\n/", "<br>", h($easy_code) );
                    } else {
                        $type = $item_types[ $grp_key ][ $easy_item_key ]['type'];
                        $item_names[ $grp_key ][ $easy_item_key ][] =
                        ( $type == 'text' || $type == 'textarea' || $type == 'calendar' ) ?
                            preg_replace( "/\n/", "<br>", h( $easy_code) ):
                            h($item_map[$grp_key][ $easy_item_key ][$easy_code]);
                    }
                }
            }
        }

        return $item_names;
    }

    /**
     * 入力・保存されたコードを確認用文言に変換します。
     *
     * @param array $grps DB登録データ取得時の形式( get_report()['content']['input_items']に相当 )
     * @return array $grpsの値を文字変換したもの
     */
    function get_easy_item_names_8( $grps ){
        $item_map = $this->get_all_easy_item_names();
        $item_types = $this->get_easy_item_types();
        $item_names = array();

        foreach ( $grps as $grp_key => $easy_items ){
            foreach ( $easy_items as $easy_item_key => $easy_codes ){
                foreach ( $easy_codes as $easy_code ){
                    if ( $easy_code == "" || !in_array($grp_key, array(1100, 1110, 1120)) ) continue;
                    $item_names[ $grp_key ][ $easy_item_key ][] = h( $easy_code);
                }
            }
        }

        return $item_names;
    }


    /**
     * 入力・保存されたコードをHTMLのvalue値用文言に変換します。
     *
     * @param array $grps DB登録データ取得時の形式( get_report()['content']['input_items']に相当 )
     * @return array $grpsの値を文字変換したもの
     */
    function get_easy_item_names_value( $grps ){
        $item_map = $this->get_all_easy_item_names();
        $item_types = $this->get_easy_item_types();
        $item_names = array();

        foreach ( $grps as $grp_key => $easy_items ){
            foreach ( $easy_items as $easy_item_key => $easy_codes ){
                foreach ( $easy_codes as $easy_code ){
                    if ( $easy_code == "" ) continue;
                    $item_names[ $grp_key ][ $easy_item_key ][] =
                        ( preg_match( "/^text/", $item_types[ $grp_key ][ $easy_item_key ]['type'] ) ) ?
                            h( preg_replace( "/<br>/", "\n", $easy_code) ):
                            $easy_code;
                }
            }
        }
        return $item_names;
    }


    // (Private関数)全選択項目のコードと名前の対応表を取得
    function get_all_easy_item_names(){
        $sql  = "SELECT grp_code, to_number(mate.easy_item_code::text,'9999') AS easy_item_code, easy_name, easy_code FROM inci_report_materials AS mate";
        $cond = "WHERE easy_flag='1' ORDER BY grp_code, easy_item_code, easy_num";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }


        $last_grp = "";
        $last_easy_item = "";
        $last_easy = "";
        $grp_list = array();
        while ( $row = pg_fetch_assoc($result) ){

//          if ( $row['grp_code'] != $last_grp ){
//              $grp_list[ $row['grp_code'] ] = array(
//                  'grp_name' => $row['grp_name'],
//                  'easy_item_list' => array()
//              );
//              $last_grp = $row['grp_code'];
//          }
//
//          if ( $row['easy_item_code'] != $last_easy_item ){
//              $grp_list[ $row['grp_code'] ]['easy_item_list'][ $row['easy_item_code'] ] = array(
//                  'easy_item_name' => $row['easy_item_name'],
//                  'easy_item_type' => $row['item_type'],
//                  'easy_list' => array()
//              );
//              $last_easy_item = $row['easy_item_code'];
//          }
//
//          $grp_list[ $row['grp_code'] ]['easy_item_list'][ $row['easy_item_code'] ]['easy_list'][ $row['easy_code'] ] =

            $grp_list[ $row['grp_code'] ][ $row['easy_item_code'] ][ $row['easy_code'] ] = $row['easy_name'];

        }

        return $grp_list;

    }

    // (Private関数)入力項目形式を取得
    function get_easy_item_types(){
        $sql  = "SELECT * FROM inci_easyinput_item_type";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $plurals = array();
        while ( $row = pg_fetch_assoc($result) ){
            $plurals[ $row['grp_code'] ][ $row['easy_item_code'] ] = array(
                'type' => $row['item_type'],
                'must' => $row['item_must']
            );
        }

        return $plurals;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// インシレベル更新
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * インシレベルを更新します。
     *
     * @param string $report_id レポートID
     * @param string $inci_level インシレベル項目値
     */
    function update_inci_level($report_id,$inci_level)
    {
        //様式情報なし(特殊様式のため更新対象はグループで指定する。)
        $eis_id = "";

        //更新対象グループ(90:インシレベル　のみ)
        $grp_flags = array();
        $grp_flags[] = 90;

        //インシレベルをレポート入力データ形式に変換
        $input_data = array();
        if($inci_level != "")
        {
            $input_data['_90_10'] = $inci_level;
        }

        //レポートデータ更新
        $session = "";//SM項目なし
        $this->set_report_content($session,$report_id, $eis_id, $input_data, $grp_flags);
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// インシレベル説明文関連
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * インシレベルに対する説明文を更新します。
     *
     * @param string $easy_code インシレベルのコード
     * @param string $short_name 略名
     * @param string $message 説明文
     * @param boolean $use_flg_boolean 使用フラグ
     */
    function set_level_info($easy_code,$short_name,$message,$use_flg_boolean)
    {
        if($use_flg_boolean)
        {
            $use_flg = "t";
        }
        else
        {
            $use_flg = "f";
        }

        $sql = "update inci_level_message set";
        $set = array("short_name","message","use_flg");
        $setvalue = array($short_name,$message,$use_flg);
        $cond = "where easy_code = '$easy_code'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * インシレベルに対する情報を取得します。
     * 使用中のレベルを指定した場合、そのレベルはuse_flgは常にtrueとなります。
     *
     * 返却値の構造は以下の配列。第一次は表示順。
     * array[0-N]["easy_code"]=レベルコード
     *           ["easy_name"]=レベル名
     *           ["short_name"]=レベル略名
     *           ["message"]  =レベル説明文
     *           ["use_flg"]  =使用フラグ
     *
     * @return array インシデント情報配列。
     */
    function get_inci_level_infos($used_level = "", $use_only = false)
    {
        $sql  = " select rm.easy_code,easy_name,lm.short_name,lm.message,lm.use_flg from";
        $sql .= " (";
        $sql .= " select * from inci_report_materials where grp_code=90 and easy_item_code=10";
        $sql .= " ) rm";
        $sql .= " left join inci_level_message lm";
        $sql .= " on rm.easy_code = lm.easy_code";
        $sql .= " order by easy_num";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $arr = array();
        while ( $row = pg_fetch_assoc($result) ){

            //使用中のレベルに指定されている場合は常に利用可能
            $use_flg = $row['use_flg'] == 't';
            if($used_level == $row['easy_code'])
            {
                $use_flg = true;
            }

            if ($use_flg || !$use_only){
              $arr[] =
                  array(
                      'easy_code' => $row['easy_code'],
                      'easy_name' => $row['easy_name'],
                      'short_name' => $row['short_name'],
                      'message' => $row['message'],
                      'use_flg' => $use_flg
                  );
            }
        }

        return $arr;
    }


    /**
     * インシレベルマスタに対する情報を取得します。
     *
     * 返却値の構造は以下の配列。第一次は表示順。
     * array[0-N]["easy_code"]=レベルコード
     *           ["easy_name"]=レベル名
     *           ["short_name"]=レベル略名
     *           ["message"]  =レベル説明文
     *
     * @return array インシデントマスタ情報配列。
     */
    function get_inci_level_mst()
    {
        $sql  = " select rm.easy_code,lm.easy_name,lm.short_name,lm.message from";
        $sql .= " (";
        $sql .= " select * from inci_report_materials where grp_code=90 and easy_item_code=10";
        $sql .= " ) rm";
        $sql .= " left join inci_level_mst lm";
        $sql .= " on rm.easy_code = lm.easy_code";
        $sql .= " order by easy_num";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $arr = array();
        while ( $row = pg_fetch_assoc($result) ){
            $arr[] =
                array(
                    'easy_code' => $row['easy_code'],
                    'easy_name' => $row['easy_name'],
                    'short_name' => $row['short_name'],
                    'message' => $row['message'],
                );
        }

        return $arr;
    }

    /**
     * インシレベルの文言を更新します。
     *
     * @param string $easy_code インシレベルのコード
     * @param string $easy_name インシレベルの文言
     */
    function set_inci_easy_name($easy_code,$easy_name,$easy_num) {

        $sql = "update inci_report_materials set";
        $set = array("easy_name","easy_num");
        $setvalue = array($easy_name,$easy_num);
        $cond = "where grp_code = 90 and easy_item_code = 10 and easy_code = '$easy_code'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * 使用可能なインシレベルか判定します。
     *
     * @param string 判定対象のインシレベルの項目コード
     * @return boolean 使用可能な場合にtrue
     */
    function is_usable_inci_level($inci_level)
    {
        $sql  = "select use_flg from inci_level_message where easy_code = '$inci_level'";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $use_flg = pg_fetch_result($result,0,"use_flg");

        return $use_flg == 't';
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SMロック関係
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * ＳＭ以外編集ロックの設定を保存します。
     *
     * @param string  $report_id レポートID
     * @param boolean $is_lock ロックする場合にtrue
     */
    function set_sm_lock($report_id,$is_lock)
    {
		require_once('Cmx.php');
		require_once("l4pWClass.php");
		$lfp = new l4pWClass("FANTOL");

        if($is_lock)
        {
            $edit_lock_flg = "t";
			$lfp->debugRst("FANTOL","REPORT-GRM-LOCK:emp_id:".$_POST["emp_id"]." report_id:".$report_id);
        }
        else
        {
            $edit_lock_flg = "f";
			$lfp->debugRst("FANTOL","REPORT-GRM-UNLOCK:emp_id:".$_POST["emp_id"]." report_id:".$report_id);
        }

        $sql = "update inci_report set";
        $set = array("edit_lock_flg ");
        $setvalue = array($edit_lock_flg );
        $cond = "where report_id = $report_id";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * ＳＭ以外編集ロックの設定を取得します。
     *
     * @param string  $report_id レポートID
     * @return boolean ロックする場合にtrue
     */
    function get_sm_lock($report_id)
    {
        $sql  = "select edit_lock_flg from inci_report where report_id = $report_id";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $edit_lock_flg = pg_fetch_result($result,0,"edit_lock_flg");
        return $edit_lock_flg == 't';
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// メール画面の進捗関連
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * レポート進捗の権限を取得する
     *
     * @param string  $report_id  レポートID
     * @param string  $emp_id     職員ID
     * @return 指定した職員のレポート権限情報配列
     */
    function get_my_inci_auth_list_for_report_progress($report_id, $emp_id)
    {
        $arr = array();
        $sql = get_sql_emp_auth_part_max(get_sql_emp_auth_all_for_report($report_id,$this->_db_con,$this->file_name));
        $sql .= " inner join inci_auth_mst on inci_auth_all.auth = inci_auth_mst.auth ";
        $cond = "where emp_id = '$emp_id' and auth_part <> '審議者' order by auth_rank";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        $i=0;
        while ($row = pg_fetch_array($result)) {

            $auth = $row["auth"];
            $auth_part = $row["auth_part"];

            $tmp_arr = array('auth' => $auth, 'auth_part' => $auth_part);
            $arr[$i] = $tmp_arr;
            $i++;
        }
        return $arr;
    }

    /**
     * 確認ボタンで、レポート進捗・確認日付が更新可能か判定します。
     *
     * @param string  $report_id    レポートID
     * @param string  $arr_auth     権限ID配列
     * @return 更新可能な場合はtrue
     */
    function is_report_progress_kakunin_updatable($report_id, $arr_auth) {
        $auth_cond = "";
        for ( $i = 0; $i < count($arr_auth); $i++) {
            if ( $i > 0 ) {
                $auth_cond .= ',';
            }
            $auth_cond .= "'";
            $auth_cond .= $arr_auth[$i]['auth'];
            $auth_cond .= "'";
        }

        $sql = "select count(*) as cnt from inci_report_progress ";
        $cond = "where report_id = {$report_id} and use_flg = true and auth in ({$auth_cond}) and (end_date is null or end_date = '')";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $cnt = pg_fetch_result($result,0,"cnt");
        return $cnt > 0;
    }
    
        function is_report_progress_toray_kakunin_updatable($report_id, $arr_auth) {

        $sql = "select count(*) as cnt from inci_report_progress ";
        $cond = "where report_id = {$report_id} and use_flg = true and auth = '$arr_auth' and (end_date is null or end_date = '')";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $cnt = pg_fetch_result($result,0,"cnt");
        return $cnt > 0;
    }
    
    
       /**
     * 確認ボタンで、レポート進捗・確認日付が更新可能か判定します。
     *
     * @param string  $report_id    レポートID
     * @param string  $arr_auth     権限ID配列
     * @return 更新可能な場合はtrue
     */
    function is_report_progress_detail_kakunin_updatable($report_id,$emp_id) {
        $auth_cond = "";

        $sql = "select count(*) as cnt from inci_report_progress_detail ";
        $cond = "where report_id = {$report_id} and use_flg = true and emp_id='{$emp_id}' and (end_date is null or end_date = '')";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $cnt = pg_fetch_result($result,0,"cnt");
        return $cnt > 0;
    }

    
    
    
    

    /**
     * レポート進捗の受付日時を本日日付で保存します。
     * 但し、既に受付日時が設定されている場合は更新しません。
     *
     * @param string  $report_id レポートID
     * @param string  $auth      権限ID
     * @param string  $emp_id    報告者ID
     * @param string  $mail_id    mailID
     */
    function set_report_progress_start_date($report_id, $auth, $emp_id,$mail_id='')
    {
        $sql = "update inci_report_progress set";
        $set = array("start_date", "start_flg", "update_emp_id");
        $setvalue = array(date("Y/m/d"), 't', $emp_id);
        $cond = "where report_id = $report_id and auth = '$auth' and (start_date is null or start_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        
        $sql = "update inci_report_progress_detail set";
        $set = array("start_date", "start_flg");
        $setvalue = array(date("Y/m/d"), 't');
        $cond = "where mail_id = $mail_id and auth = '$auth' and emp_id = '$emp_id' and (start_date is null or start_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

    /**
     * レポート進捗の確認日時を本日日付で保存します。
     * 但し、既に確認日時が設定されている場合は更新しません。
     *
     * @param string  $report_id レポートID
     * @param string  $auth      権限ID
     * @param string  $emp_id    報告者ID
     * @param string  $mail_id    mailiD
     */
    function set_report_progress_end_date($report_id, $auth, $emp_id,$mail_id='')
    {
        $sql = "update inci_report_progress set";
        $set = array("end_date", "end_flg", "update_emp_id");
        $setvalue = array(date("Y/m/d"), 't', $emp_id);
        $cond = "where report_id = $report_id and auth = '$auth' and (end_date is null or end_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        
        $sql = "update inci_report_progress_detail set";
        $set = array("end_date", "end_flg");
        $setvalue = array(date("Y/m/d"), 't');
        $cond = "where mail_id = $mail_id and auth = '$auth'  and emp_id = '$emp_id' and (end_date is null or end_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        

        // SM確認時に報告書の更新をロックする。
	if ($auth == 'SM') {
            if ( get_progress_auto_lock($this->_db_con, $this->file_name )) {
		$this->set_sm_lock($report_id, true);
            }
	}
    }

    /**
     * レポート進捗を評価済みに更新
     *
     * @param string  $report_id レポートID
     * @param string  $auth      権限ID
     * @param string  $emp_id    報告者ID
     * @param string  $mail_id    mailiD
     */
    function set_report_progress_evaluation_date($report_id, $auth, $emp_id,$mail_id='')
    {
        $sql = "update inci_report_progress set";
        $set = array("evaluation_date", "evaluation_flg", "update_emp_id");
        $setvalue = array(date("Y/m/d"), 't', $emp_id);
        $cond = "where report_id = $report_id and auth = '$auth' and (evaluation_date is null or evaluation_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        
        $sql = "update inci_report_progress_detail set";
        $set = array("evaluation_date", "evaluation_flg");
        $setvalue = array(date("Y/m/d"), 't');
        $cond = "where mail_id = $mail_id and auth = '$auth' and emp_id = '$emp_id' and (evaluation_date is null or evaluation_date = '') and use_flg = 't'";
        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        
        
    }

    /**
     * レポート進捗のコメントを保存
     * (hiyari_rp_progress_comment_input.phpから移植)
     *
     * @param string  $report_id    レポートID
     * @param string  $emp_id       ユーザID
     * @param string  $comment_list コメントリスト
     */
    function set_report_progress_comment($report_id, $emp_id, $comment_list)
    {
        //==============================
        //デリート
        //==============================
        $sql = "delete from inci_report_progress_comment";
        $cond = "where report_id = $report_id and emp_id = '$emp_id'";
        $del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($del == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //==============================
        //インサート
        //==============================
        $date = date("Y/m/d");
        foreach($comment_list as $auth => $comment)
        {
            $sql = "insert into inci_report_progress_comment(report_id,auth,emp_id,progress_comment,comment_date) values(";
            $registry_data = array(
                $report_id,
                $auth,
                $emp_id,
                pg_escape_string($comment),
                $date
            );

            $result = insert_into_table($this->_db_con, $sql, $registry_data, $this->file_name);
            if ($result == 0) {
                pg_query($this->_db_con,"rollback");
                pg_close($this->_db_con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// データ連携関連
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // データ連携情報取得
    function get_inci_data_connect()
    {
        $sql  = "select * from inci_data_connect";
        $cond = "";

        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

        if ($sel == 0)
        {
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }

        $patient_profile_flg = pg_fetch_result($sel,0,"patient_profile_flg");
        $hospital_ymd_flg = pg_fetch_result($sel,0,"hospital_ymd_flg");
        $patient_search_url = pg_fetch_result($sel,0,"patient_search_url");
        $byoumei_search_flg = pg_fetch_result($sel,0,"byoumei_search_flg");
        $emr_flg = pg_fetch_result($sel,0,"emr_flg");
        $emr_path = pg_fetch_result($sel,0,"emr_path");

        return array(
            "patient_profile_flg" => $patient_profile_flg,
            "hospital_ymd_flg" => $hospital_ymd_flg,
            "patient_search_url" => $patient_search_url,
            "byoumei_search_flg" => $byoumei_search_flg,
            "emr_flg" => $emr_flg,
            "emr_path" => $emr_path,
            );
    }

    // データ連携情報更新
    function set_inci_data_connect($patient_profile_flg, $hospital_ymd_flg, $patient_search_url, $byoumei_search_flg, $emr_flg, $emr_path)
    {

        $sql = "update inci_data_connect set";
        $set = array("patient_profile_flg","hospital_ymd_flg","patient_search_url","byoumei_search_flg", "emr_flg", "emr_path");
        $setvalue = array("$patient_profile_flg", "$hospital_ymd_flg",pg_escape_string($patient_search_url),"$byoumei_search_flg", "$emr_flg", pg_escape_string($emr_path));
        $cond = "";

        $upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
        if ($upd == 0) {
            pg_query($this->_db_con, "rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 報告書の表示設定
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
      * コントロールタイプを変更（style_code=2の報告書用）
      *
      * @param array $grps
      * @return array $grpsの値を文字変換したもの
      */
    function change_easy_item_type( &$grps ){
      $grps[1100]['easy_item_list'][50]['easy_item_type']='radio';  //仮に実施された場合の影響度
      $grps[1110]['easy_item_list'][10]['easy_item_type']='radio';  //特に報告を求める事例
      $grps[105]['easy_item_list'][40]['easy_item_type']='radio';   //発見時間帯
      $grps[100]['easy_item_list'][40]['easy_item_type']='radio';   //発生時間帯
      $grps[116]['easy_item_list'][10]['easy_item_type']='radio';   //発生部署
      $grps[110]['easy_item_list'][60]['easy_item_type']='radio';   //発生場所
      $grps[110]['easy_item_list'][65]['easy_item_type']='radio';   //発生場所詳細
      $grps[3000]['easy_item_list'][10]['easy_item_type']='radio';  //発見者
      $grps[3020]['easy_item_list'][10]['easy_item_type']='radio';  //発見者職種
      for($i=0; $i<10; $i++){
        $grps[3050+$i]['easy_item_list'][30]['easy_item_type']='radio';  //当事者職種
        $grps[3350+$i]['easy_item_list'][110]['easy_item_type']='radio'; //直前一週間の夜勤回数
        $grps[3400+$i]['easy_item_list'][120]['easy_item_type']='radio'; //勤務形態
      }
      $grps[200]['easy_item_list'][10]['easy_item_type']='radio';   //患者の数
      $grps[210]['easy_item_list'][30]['easy_item_type']='radio';   //患者の性別
      $grps[230]['easy_item_list'][60]['easy_item_type']='radio';   //患者区分1
      $grps[240]['easy_item_list'][70]['easy_item_type']='radio';   //患者区分2
      $grps[550]['easy_item_list'][55]['easy_item_type']='radio';   //減免措置有無
      $grps[650]['easy_item_list'][50]['easy_item_type']='radio';   //事例分析検討対象
      $grps[90]['easy_item_list'][10]['easy_item_type']='radio';    //患者影響レベル
      $grps[96]['easy_item_list'][20]['easy_item_type']='radio';    //ヒヤリハットの影響度
      $grps[900]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：概要
      $grps[910]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：種類
      $grps[920]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：項目
      $grps[940]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：発生場面
      $grps[950]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：項目
      $grps[970]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：事例の内容
      $grps[980]['easy_item_list'][10]['easy_item_type']='radio';   //概要・場面・内容（2010年版）：項目
      $grps[570]['easy_item_list'][5]['easy_item_type']='radio';    //影響区分
      $grps[1400]['easy_item_list'][10]['easy_item_type']='radio';  //影響区分(カスタマイズ用)
      $grps[580]['easy_item_list'][5]['easy_item_type']='radio';    //対応区分
      $grps[1410]['easy_item_list'][10]['easy_item_type']='radio';  //対応区分(カスタマイズ用)
      $grps[605]['easy_item_list'][15]['easy_item_type']='radio';   //発生要因＞｢医薬品の問題｣の詳細
      $grps[605]['easy_item_list'][16]['easy_item_type']='radio';   //発生要因＞｢医療機器の問題｣の詳細
      $grps[605]['easy_item_list'][17]['easy_item_type']='radio';   //発生要因＞｢諸物品の問題｣の詳細
      $grps[510]['easy_item_list'][20]['easy_item_type']='radio';   //再発防止に資する(警鐘的)事例
      $grps[590]['easy_item_list'][90]['easy_item_type']='radio';   //事故調査委員会設置有無
    }

    /**
      * ラジオボタン・チェックボックスの選択肢から非表示項目を抜いたリストを取得
      *
      * @return array 選択肢の配列
      */
    function get_item_selection($grps, $item_element_no_disp){
        $selection = array();
        foreach ($grps as $grp_code => $grp){
            foreach($grp['easy_item_list'] as $easy_item_code => $list){
                if ($list['easy_item_type']=='radio' || $list['easy_item_type']=='checkbox'){
                    if ($item_element_no_disp[$grp_code][$easy_item_code] == ""){
                        $selection[$grp_code][$easy_item_code]['list'] = $list['easy_list'];
                    }
                    else{
                        $temp = array();
                        foreach($list['easy_list'] as $opt){
                            if(!in_array($opt['easy_code'], $item_element_no_disp[$grp_code][$easy_item_code]) || $opt['pause_flg'] == 't'){
                                $temp[] = $opt;
                            }
                        }
                        $selection[$grp_code][$easy_item_code]['list'] = $temp;
                    }
                    if (($grp_code==125 && $easy_item_code==85) ||
                        ($grp_code==600 && $easy_item_code==10) || $grp_code==605){
                        $selection[$grp_code][$easy_item_code]['col_num'] = 2;
                        $selection[$grp_code][$easy_item_code]['item_width'] = '50%';
                    }
                    else{
                        $selection[$grp_code][$easy_item_code]['col_num'] = 4;
                        $selection[$grp_code][$easy_item_code]['item_width'] = '25%';
                    }
                    
                    // グループ 20160120
                    foreach($selection[$grp_code][$easy_item_code]['list'] as $key => $item){
                        if ($item['item_group_code'] != null){
                            if ($selection[$grp_code][$easy_item_code]['group'] == null){
                                $selection[$grp_code][$easy_item_code]['group'] = array();
                            }
                            $selection[$grp_code][$easy_item_code]['group'][$item['item_group_code']][] = $key;
                        }
                    }
                }
            }
        }

        return $selection;
    }

    /**
      * 表示グループの最後の項目フラグを取得（design_mode==2の報告書用）
      *
      * @return array 最後の項目フラグ配列
      */
    function get_last_item($use_grp_code){
        $last_item = array();

        //いつ、どこで
        if (in_array(110, $use_grp_code)){//発生場所詳細
            $last_item[110][65] = true;
        }
        elseif (in_array(116, $use_grp_code)){//発生部署
            $last_item[116][10] = true;
        }
        elseif (in_array(100, $use_grp_code)){
            if ($time_setting_flg){ //発生日時
                $last_item[100][5] = true;
            }
            else{
                $last_item[100][50] = true; //いつごろ(不明の時に記載)
            }
        }
        else if (in_array(105, $use_grp_code)){
            if ($time_setting_flg){
                $last_item[105][5] = true;  //発見日時
            }
            else{
                $last_item[105][50] = true; //いつごろ(不明の時に記載)
            }
        }
        //誰が
        $last_item[3000][10] = true;    //発見者
        if (in_array(3038, $use_grp_code)){    //発見者部署配属年数
            $last_item[3038][10] = true;
        }
        elseif (in_array(3036, $use_grp_code)){//発見者職種経験年数
            $last_item[3036][10] = true;
        }
        elseif (in_array(3034, $use_grp_code)){//専門医・認定医及びその他の医療従事者の専門・認定医資格
            $last_item[3034][10] = true;
        }
        elseif (in_array(3032, $use_grp_code)){//発見者所属部署
            $last_item[3032][10] = true;
        }
        else if (in_array(3030, $use_grp_code)){//発見者氏名
            $last_item[3030][10] = true;
        }
        else if (in_array(3020, $use_grp_code)){//発見者職種
            $last_item[3020][10] = true;
        }
        $last_item[3500][150] = true;   //当事者以外の関連職種
        //誰に
        if (in_array(280, $use_grp_code)){//関連する疾患名3
            $last_item[280][110] = true;
        }
        elseif (in_array(270, $use_grp_code)){//関連する疾患名2
            $last_item[270][100] = true;
        }
        elseif (in_array(260, $use_grp_code)){//関連する疾患名1
            $last_item[260][90] = true;
        }
        elseif (in_array(250, $use_grp_code)){//インシデントに直接関連する疾患名
            $last_item[250][80] = true;
        }
        elseif (in_array(1200, $use_grp_code)){//病棟名
            $last_item[1200][10] = true;
        }
        elseif (in_array(246, $use_grp_code)){//入院日
            $last_item[246][76] = true;
        }
        elseif (in_array(243, $use_grp_code)){//主治医
            $last_item[243][73] = true;
        }
        elseif (in_array(240, $use_grp_code)){//患者区分2
            $last_item[240][70] = true;
        }
        elseif (in_array(230, $use_grp_code)){//患者区分1
            $last_item[230][60] = true;
        }
        elseif (in_array(210, $use_grp_code)){
            if (in_array(40, $patient_use) || in_array(50, $patient_use)){//患者の年齢
                $last_item[210][40] = true;
            }
            elseif (in_array(30, $patient_use)){//患者の性別
                $last_item[210][30] = true;
            }

            if (in_array(10, $patient_use) || in_array(20, $patient_use)){
                if (in_array(20, $patient_use)){//患者氏名
                    $last_item[210][20] = true;
                }
                elseif (in_array(10, $patient_use)){//患者ID
                    $last_item[210][10] = true;
                }
            }
        }
        elseif (in_array(200, $use_grp_code)){//患者の数
            $last_item[200][10] = true;
        }
        $last_item[130][90] = true;     //関連診療科
        if (!in_array(295, $use_grp_code)){
            $last_item[290][120] = true;    //インシデント直前の患者の状態
        }
        $last_item[140][10] = true;     //アセスメント・患者の状態
        //何をどうした
        $last_item[125][85] = true; //ヒヤリ・ハット分類
        $last_item[1000][15] = true;//関連医薬品
        $last_item[1000][30] = true;//医療材料・諸物品等
        $last_item[1000][55] = true;//医療機器等
        //内容
        if (in_array(530, $use_grp_code)){//インシデント発生（発見）時の経過
            $last_item[530][40] = true;
        }
        elseif (in_array(525, $use_grp_code)){//インシデントの内容
            $last_item[525][10] = true;
        }
        elseif (in_array(520, $use_grp_code)){//インシデントの内容
            $last_item[520][30] = true;
        }
        elseif (in_array(500, $use_grp_code)){//実施した医療行為の目的
            $last_item[500][10] = true;
        }
        //対応
        if (in_array(550, $use_grp_code)){//減免措置
            $last_item[550][60] = true;
        }
        elseif (in_array(540, $use_grp_code)){//初期対応
            $last_item[540][50] = true;
        }
        $last_item[1410][30] = true;//処置の詳細
        //分析
        $last_item[650][50] = true;//事例分析検討対象
        if (in_array(96, $use_grp_code)){//ヒヤリ・ハットの影響度
            $last_item[96][20] = true;
        }
        elseif (in_array(90, $use_grp_code)){//患者影響レベル
            $last_item[90][10] = true;
        }
        if (in_array(1530, $use_grp_code)){//教育研修への活用
            $last_item[1530][10] = true;
        }
        elseif (in_array(1520, $use_grp_code)){//システム改善の必要性
            $last_item[1520][10] = true;
        }
        elseif (in_array(1510, $use_grp_code)){//リスクの予測
            $last_item[1510][10] = true;
        }
        elseif (in_array(1504, $use_grp_code)){//リスクの評価: 頻度
            $last_item[1504][10] = true;
        }
        elseif (in_array(1502, $use_grp_code)){//リスクの評価: 緊急性
            $last_item[1502][10] = true;
        }
        elseif (in_array(1500, $use_grp_code)){//リスクの評価: 重要性
            $last_item[1500][10] = true;
        }
        $last_item[510][20] = true;//再発防止に資する(警鐘的)事例
        $last_item[590][90] = true;//事故調査委員会の設置有無
        if (in_array(635, $use_grp_code)){//備考
            $last_item[635][45] = true;
        }
        elseif (in_array(630, $use_grp_code)){//インシデント内容に関する自由記載欄
            $last_item[630][40] = true;
        }
        elseif (in_array(625, $use_grp_code)){//効果の確認
            $last_item[625][35] = true;
        }
        elseif (in_array(620, $use_grp_code)){
            $last_item[620][33] = true;//評価予定期日
        }
        elseif (in_array(610, $use_grp_code)){//インシデントの背景・要因
            $last_item[610][20] = true;
        }
        $last_item[690][90] = true;//医療安全者専用記載項目

        return $last_item;
    }


//20130630
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 報告書のファイル添付
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	     * 報告書添付ファイルをCoMedix参照可能エリアに移動
	     *
     * @param char $report_id
	  * @param char $emp_id
	  * @return
	     */
	function get_tmp_dir($st_get, $emp_id, $report_id=null){

		//登録前のテンポラリフォルダ
		if($st_get === "Tmp")
		{
			$emp_dir = "/tmp/hiyariTmp_".$emp_id;

			//ディレクトリ作成
			if(!is_dir($emp_dir))
			{
				mkdir($emp_dir);
			}

			if($report_id !=null)
			{
				$emp_dir = "/tmp/hiyariTmp_".$emp_id."/".$report_id;
			}
			else
			{
				$emp_dir = "/tmp/hiyariTmp_".$emp_id."/shitagaki";
			}

			//ディレクトリ作成
			if(!is_dir($emp_dir))
			{
				mkdir($emp_dir);
			}



			return($emp_dir);
		}
		else
		{
			//登録済みの添付ファイルフォルダ


			// 添付ファイル情報を取得
			$sql = "select registration_date from inci_report";
			$cond = "where report_id = '$report_id' ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			$registration_date = pg_result($sel, 0, "registration_date");

			$reg_date_arr = explode("/", $registration_date);

			//作成年
			$reg_year = $reg_date_arr[0];

			//作成月
			$reg_month = $reg_date_arr[1];

			$base_path = dirname(__FILE__);
			$tmp_path = $base_path."/inci_file";
			//ディレクトリ作成
			if(!is_dir($tmp_path))
			{
				mkdir($tmp_path);
			}



			$tmp_path = $base_path."/inci_file/".$reg_year;
			//ディレクトリ作成
			if(!is_dir($tmp_path))
			{
				mkdir($tmp_path);
			}

			$tmp_path = $base_path."/inci_file/".$reg_year."/".$reg_month;
			//ディレクトリ作成
			if(!is_dir($tmp_path))
			{
				mkdir($tmp_path);
			}

			$tmp_path = $base_path."/inci_file/".$reg_year."/".$reg_month."/".$report_id;
			//ディレクトリ作成
			if(!is_dir($tmp_path))
			{
				mkdir($tmp_path);
			}



//			$tmp_path = $base_path."/inci_file/".$report_id;
//			//ディレクトリ作成
//			if(!is_dir($tmp_path))
//			{
//				mkdir($tmp_path);
//			}



			return($tmp_path);
		}

	}

	//20130630
	/**
      * 報告書添付ファイルをCoMedix参照可能エリアに移動
      *
      * @param char $report_id
	  * @param char $emp_id
	  * @return
      */
	function move_hiyari_file($report_id, $emp_id,$gamen_mode=null){

		//一時フォルダ
		if($gamen_mode==="new")
		{
			//新規登録からのファイル添付フォルダ
			$emp_dir = $this->get_tmp_dir("Tmp",$emp_id);
		}
		else
		{
			//編集からのファイル添付フォルダ
			$emp_dir = $this->get_tmp_dir("Tmp",$emp_id,$report_id);
		}

		//登録フォルダ
		$tmp_path = $this->get_tmp_dir("",$emp_id,$report_id);

		$arrFileCheck = array();
		$arrFileCheck = $_COOKIE['hiyariFileEnc'];

		foreach ($arrFileCheck as $enc => $name) {

			//cookieの整合性チェック
			$ary = glob($emp_dir."/".$enc.".*") ;
			$up_file_arr = explode("/", $ary[0]);
			$up_file = $up_file_arr[count($up_file_arr)-1];

			if(rename ($ary[0], $tmp_path."/".$up_file))
			{

				//登録SQLを作成
				$sql = "insert into inci_file(emp_id, report_id, report_file_enc, report_file_name, up_file_time) values(";
				$registry_data = array(
						$emp_id,
						$report_id,
						$enc,
						p($name),
						date("YmdHis")
						);

				//登録SQLを実行
				$result = insert_into_table($this->_db_con, $sql, $registry_data, $this->file_name);
				if ($result == 0) {
					pg_query($this->_db_con,"rollback");
					pg_close($this->_db_con);
					showErrorPage();
					exit;
				}


				//画像ファイルのみの処理
				$type=exif_imagetype($tmp_path."/".$up_file);
				if(($type === IMAGETYPE_GIF) || ($type === IMAGETYPE_JPEG) || ($type === IMAGETYPE_PNG))
				{

					switch ($type) {
						case IMAGETYPE_GIF:
							$img = imagecreatefromgif($tmp_path."/".$up_file);
							//'gif';
							break;
						case IMAGETYPE_JPEG:
							$img = ImageCreateFromJPEG($tmp_path."/".$up_file);
							//'jpg';
							break;
						case IMAGETYPE_PNG:
							$img = imagecreatefrompng($tmp_path."/".$up_file);
							//'png';
							break;
						default:
							return(false);
							break;
					}


					//imagecreatefromgif
					//imagecreatefromjpeg
					//imagecreatefrompng
					//imagecreatefromwbmp

					$width = ImageSx($img);
					$height = ImageSy($img);


					if($height > 193)
					{
						$outPer = round(($height/192),1);
					}
					else
					{
						$outPer = 1;
					}

					$out = ImageCreateTrueColor($width/$outPer, $height/$outPer);
					ImageCopyResampled($out, $img,	0,0,0,0, $width/$outPer, $height/$outPer, $width, $height);

					$up_file_arrD = explode(".", $up_file);
					$up_file = $up_file_arrD[0];


					$result = imagejpeg($out, $tmp_path."/".$up_file_arrD[0]."D.".$up_file_arrD[1]);

					unset($img);
					unset($out);
					unset($result);

				}



				$out = $out;




			}
		}
	}


	//20130630
    /**
      * 報告書添付ファイル情報を取得する
      *
      * @param char $report_id
	  * @return
    */
	function get_inci_file($report_id)
	{


		// 添付ファイル情報を取得
		$sql = "select report_file_enc, report_file_name, registration_date from inci_file join inci_report on inci_file.report_id=inci_report.report_id ";
		$cond = "where inci_file.report_id = '$report_id' order by up_file_time";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$registration_date = pg_result($sel, 0, "registration_date");
		$reg_date_arr = explode("/", $registration_date);

		//作成年
		$reg_year = $reg_date_arr[0];

		//作成月
		$reg_month = $reg_date_arr[1];



		$base_path = dirname(__FILE__);

		$tmp_path = $base_path."/inci_file/".$reg_year;
		//ディレクトリ作成
		if(!is_dir($tmp_path))
		{
			return;
		}


		$tmp_path = $base_path."/inci_file/".$reg_year."/".$reg_month;
		//ディレクトリ作成
		if(!is_dir($tmp_path))
		{
			return;
		}

		$tmp_path = "inci_file/".$reg_year."/".$reg_month."/".$report_id;



		$file_arr = array();
		$cnt = 0;
		while ($row = pg_fetch_array($sel)) {

			$load_file_arr = explode(".", $row["report_file_name"]);
			$load_file_ext = $load_file_arr[count($load_file_arr) - 1];

			$file_arr[$cnt]["file_no"] = $row["report_file_enc"];
			$file_arr[$cnt]["file_name"] = $row["report_file_name"];
			$file_arr[$cnt]["ext"] = $load_file_ext;


			//画像ファイルのみ処理



			$file_arr[$cnt]["base"] = $tmp_path."/".$row["report_file_enc"].".".$load_file_ext;

			$file_arr[$cnt]["view"] = "hiyari_file_dl.php?s";
			$file_arr[$cnt]["target"] = "";

			//画像ファイルチェック
			$type=exif_imagetype($base_path."/".$file_arr[$cnt]["base"]);
			if(($type == IMAGETYPE_GIF) || ($type == IMAGETYPE_JPEG) || ($type == IMAGETYPE_PNG))
			{
				//画像ファイルと認識するので報告書で表示
				$file_arr[$cnt]["gazou_flg"] = "t";
				$file_arr[$cnt]["base"] = $tmp_path."/".$row["report_file_enc"]."D.".$load_file_ext;

//				$file_arr[$cnt]["view"] = "hiyari_file_view.php?s";
//				$file_arr[$cnt]["target"] = "target='_blank'";

				//ファイルサイズを取得
				list($width, $height, $type, $attr) = getimagesize($file_arr[$cnt]["base"]);

				//プレビューで表示するファイルサイズを決定する
				//横幅が350を基準として圧縮するかどうか決定する
//				if($width < 350)
//				{
//					$file_arr[$cnt]["fileWh"] = $width;
//				}
//				else
//				{
//					$file_arr[$cnt]["fileWh"] = 350;
//				}
			}
			else
			{


				switch ($file_arr[$cnt]["ext"])
				{
					case 'xls':
					case 'xlsx':
						// エクセル
						$file_arr[$cnt]["gazou_flg"] = "x";
						$file_arr[$cnt]["base"] = "img/icon/icXls2.JPG";
						break;

					case 'doc':
					case 'docx':
						// ワード
						$file_arr[$cnt]["gazou_flg"] = "d";
						$file_arr[$cnt]["base"] = "img/icon/icDoc2.JPG";

						break;

					case 'ppt':
					case 'pptx':
						// パワーポイント
						$file_arr[$cnt]["gazou_flg"] = "t";
						$file_arr[$cnt]["base"] = "img/icon/icPpt2.JPG";
						break;

					case 'pdf':
						// pdf
						$file_arr[$cnt]["gazou_flg"] = "p";
						$file_arr[$cnt]["base"] = "img/icon/icPdf2.JPG";
						break;

					default:
						// その他拡張子
						$file_arr[$cnt]["gazou_flg"] = "f";
						$file_arr[$cnt]["base"] = "img/icon/icFile2.JPG";

				}

			}



			foreach($file_arr as $key=>$value){
				$id[$key] = $value['gazou_flg'];
			}

			//gazou_flgでソート（画像以外を上位に表示する）
			array_multisort($id,SORT_ASC,$file_arr);


			$cnt++;
		}
		return $file_arr;

	}

	//20130630
	/**
      * inci_reportに報告書添付ファイル情報を追記する
      *
      * @param char $report_id
	  * @return
      */
	function set_inci_file_report($report_id)
	{

		$set_data_name = "";
		$set_data_enc = "";

		//添付ファイル情報を取得
		$report_arr = $this->get_inci_file($report_id);

		//ファイル名を連結して文字列を作成
		$cnt = 0;
		foreach ($report_arr as $key => $value) {

			if($cnt < 1)
			{
				$set_data_name = $value["file_name"];
				$set_data_enc = $value["file_no"];
			}
			else
			{
				$set_data_name .= "、".$value["file_name"];
				$set_data_enc .= ",".$value["file_no"];
			}

			$cnt++;

		}

		if($set_data_name != "")
		{

			//20131119 シングルクォートをエスケープ
			$set_data_name = str_replace("'", "\'", $set_data_name);

			$sql = "update inci_report set";
			$set = array("tmp_file_name","tmp_file_enc");
			$setvalue = array("$set_data_name","$set_data_enc");
			$cond = "where report_id = $report_id";
			$result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($result == 0) {
				pg_query($this->_db_con,"rollback");
				pg_close($this->_db_con);
				showErrorPage();
				exit;
			}
		}
	}

	//20130630
	/**
	     * 下書きから送信するとinci_reportが変わる為添付ファイルを移動させる
	     *
	     * @param char $emp_id
	     * @param char $report_id
	     * @param char $report_id_old
	  * @return
	     */
	function upd_tmpFile_for_shitagaki($emp_id, $report_id, $report_id_old)
	{

		$set_data_name = "";
		$set_data_enc = "";
		$base_path = dirname(__FILE__);

		//今回登録の添付ファイルフォルダ(tmp)
//		$tmp_path_old_temporary = $base_path."/inci_file/".$report_id;
		$tmp_path_old_temporary = $this->get_tmp_dir("Tmp",$emp_id,$report_id_old);

		//下書き保存時(旧report_id)の添付ファイルフォルダ
		//$tmp_path_old = $base_path."/inci_file/".$report_id_old;
		$tmp_path_old = $this->get_tmp_dir("","",$report_id_old);

		//送信時の添付ファイルフォルダ
		//$tmp_path_new = $base_path."/inci_file/".$report_id;
		$tmp_path_new = $this->get_tmp_dir("","",$report_id);


		$arrFileCheck = array();
		$arrFileCheck = $_COOKIE['hiyariFileEnc'];

//		foreach ($arrFileCheck as $enc => $name) {
//
//			//cookieの整合性チェック
//			$ary = glob($tmp_path_old_temporary."/".$enc.".*") ;
//			$up_file_arr = explode("/", $ary[0]);
//			$up_file = $up_file_arr[count($up_file_arr)-1];
//
//			rename ($ary[0], $tmp_path_old."/".$up_file);
//
//		}

		//Tmpファイルを実フォルダ(下書きＩＤ)へ移動
		$this->move_hiyari_file($report_id_old, $emp_id,"");

		//移動完了後の状態でinci_reportを更新する（下書きＩＤ）
		$this->set_inci_file_report($report_id_old);


		//過去に下書き保存してあるファイルを移動させる（下書きＩＤ→送信ＩＤ）
		$old_ful_path = $tmp_path_old."/*.*";
		foreach (glob($old_ful_path) as $sFilePath) {
			$mv_file_arr = explode("/", $sFilePath);
			$mv_file = $mv_file_arr[count($mv_file_arr)-1];

			//ファイルの移動
			rename ($sFilePath, $tmp_path_new."/".$mv_file);
		}


		//下書き保存時のinci_fileを送信時のinci_fileへ更新する（report_idの更新）
		$sql = "update inci_file set";
		$set = array("report_id");
		$setvalue = array("$report_id");
		$cond = "where report_id = $report_id_old";
		$result = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($result == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			showErrorPage();
			exit;
		}

		//移動完了後の状態でinci_reportを更新する
		$this->set_inci_file_report($report_id);
	}
     /**
     * 縦横軸表示非表示の値を取得
     */
    function get_axises() {
        $sql = "SELECT * FROM inci_axises_title ORDER BY order_no ASC";
        $cond = "";
        $result = select_from_table( $this->_db_con, $sql, $cond, $this->file_name );
        if ($result == 0) {
            pg_query($this->_db_con,"rollback");
            pg_close($this->_db_con);
            showErrorPage();
            exit;
        }
        return pg_fetch_all($result);
    }

}
