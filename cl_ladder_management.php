<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
//require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_application_workflow_select_box.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();
$calendar = '';
$calendar .= write_calendar('_authorize_from', 'date_authorize_from_y', 'date_authorize_from_m', 'date_authorize_from_d');
$calendar .= write_calendar('_authorize_to', 'date_authorize_to_y', 'date_authorize_to_m', 'date_authorize_to_d');
$log->debug("calendar：".$calendar);

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];
// 2012/07/12 Yamagawa add(s)
$login_emp_id = $emp_id;
// 2012/07/12 Yamagawa add(e)

$applicant_name = "$lt_nm"." "."$ft_nm";

/**
 * ラダーレベル
 */
$option_level = get_select_levels($_POST['now_level']);

/**
 * セクション名(部門・課・科・室)取得
 */
$pst_slct_bx_strct_str=get_post_select_box_strict(
	$con
	,$fname
	,$_POST['search_emp_class']
	,$_POST['search_emp_attribute']
	,$_POST['search_emp_dept']
	,$_POST['search_emp_room']
	,$obj
);

/**
 * 認定日
 */
$option_date_authorize_from_y = cl_get_select_years($_POST['date_authorize_from_y']);
$option_date_authorize_from_m = cl_get_select_months($_POST['date_authorize_from_m']);
$option_date_authorize_from_d = cl_get_select_days($_POST['date_authorize_from_d']);

$option_date_authorize_to_y = cl_get_select_years($_POST['date_authorize_to_y']);
$option_date_authorize_to_m = cl_get_select_months($_POST['date_authorize_to_m']);
$option_date_authorize_to_d = cl_get_select_days($_POST['date_authorize_to_d']);


$date_update_flg = $_POST['date_update_flg'];
$emp_date = $_POST['emp_date'];

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/search/cl_ladder_management_model.php");
$progress_manager_model = new cl_ladder_management_model($mdb2,$emp_id);
$log->debug("研修受講進捗用DBA取得",__FILE__,__LINE__);

if ($date_update_flg == 'update'){
	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	$log->debug('院外研修モデル読込　開始',__FILE__,__LINE__);
	require_once("cl/model/ladder/cl_personal_profile_model.php");
	$profile_model = new cl_personal_profile_model($mdb2, $emp_id);
	$log->debug('院外研修モデル読込　終了',__FILE__,__LINE__);

	$param = array(
		"ladder_data"		=> $emp_date
	);

	$res = $profile_model->update_print($param);

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

$select_flg = true;

if ($select_flg) {

	// 2012/07/12 Yamagawa add(s)
	require_once("cl/model/search/cl_level_analysis_model.php");
	$level_analysis_model = new cl_level_analysis_model(&$mdb2,$login_emp_id);
	$log->debug("レベル分析用DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
	$education_committee_model = new cl_mst_nurse_education_committee_model(&$mdb2,$login_emp_id);
	$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_setting_council_model.php");
	$council_model = new cl_mst_setting_council_model(&$mdb2,$login_emp_id);
	$log->debug("審議会DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
	$manager_st_model = new cl_mst_nurse_manager_st_model(&$mdb2,$login_emp_id);
	$log->debug("所属長DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_supervisor_st_model.php");
	$supervisor_st_model = new cl_mst_supervisor_st_model(&$mdb2,$login_emp_id);
	$log->debug("看護部長DBA取得",__FILE__,__LINE__);

	// 看護教育委員会チェック
	$education_committee = $education_committee_model->check_education_committee($login_emp_id);

	// ログインユーザーが看護教育委員会でない場合
	if (count($education_committee) == 0){
		// 審議会チェック
		$council = $council_model->check_council($login_emp_id);

		// ログインユーザーが看護教育委員会・審議会で無い場合
		if (count($council) == 0){
			// 所属長(役職)チェック
			$supervisor = $supervisor_st_model->check_supervisor($login_emp_id);

			if (count($supervisor) > 0){
				// 所属長(部署)取得
				$supervisor_class = $level_analysis_model->get_supervisor_class();
			}

			// 看護部長(役職)チェック
			$nurse_manager = $manager_st_model->check_nurse_manager($login_emp_id);

			if (count($nurse_manager) > 0){
				// 看護部長(部署)取得
				$nurse_manager_class = $level_analysis_model->get_nurse_manager_class();
			}

			if (count($supervisor) == 0 && count($nurse_manager) == 0){
				// ログイン不可ユーザーなので、ここに分岐されることは無い
			} else {
				if(count($supervisor) == 0){
					$class_division = $nurse_manager_class['class_division'];
				} else if(count($nurse_manager) == 0){
					$class_division = $supervisor_class['class_division'];
				} else {
					if ($nurse_manager_class['class_division'] > $supervisor_class['class_division']){
						$class_division = $supervisor_class['class_division'];
					} else {
						$class_division = $nurse_manager_class['class_division'];
					}
				}
			}
		} else {
			$class_division = 0;
		}
	} else {
		$class_division = 0;
	}
	// 2012/07/12 Yamagawa add(e)

	if (
		$_POST['date_authorize_from_y'] == ''
		|| $_POST['date_authorize_from_m'] == ''
		|| $_POST['date_authorize_from_d'] == ''
		|| $_POST['date_authorize_from_y'] == '-'
		|| $_POST['date_authorize_from_m'] == '-'
		|| $_POST['date_authorize_from_d'] == '-'
	) {
		$authorize_date_from = '';
	} else {
		$authorize_date_from = $_POST['date_authorize_from_y'].$_POST['date_authorize_from_m'].$_POST['date_authorize_from_d'];
	}

	if (
		$_POST['date_authorize_to_y'] == ''
		|| $_POST['date_authorize_to_m'] == ''
		|| $_POST['date_authorize_to_d'] == ''
		|| $_POST['date_authorize_to_y'] == '-'
		|| $_POST['date_authorize_to_m'] == '-'
		|| $_POST['date_authorize_to_d'] == '-'
	) {
		$authorize_date_to = '';
	} else {
		$authorize_date_to = $_POST['date_authorize_to_y'].$_POST['date_authorize_to_m'].$_POST['date_authorize_to_d'];
	}

	$param = array(
		'now_level'				=> $_POST['now_level']
		,'emp_nm'				=> $_POST['emp_nm']
		,'class_id'				=> $_POST['search_emp_class']
		,'atrb_id'				=> $_POST['search_emp_attribute']
		,'dept_id'				=> $_POST['search_emp_dept']
		,'room_id'				=> $_POST['search_emp_room']
		,'authorize_date_from'	=> $authorize_date_from
		,'authorize_date_to'	=> $authorize_date_to
		,'login_emp_id'			=> $login_emp_id
		,'class_division'		=> $class_division
		// 2012/08/24 Yamagawa add(s)
		,'retire_disp_flg'		=> $_POST['retire_disp_flg']
		// 2012/08/24 Yamagawa add(e)
	);

	// 一覧取得
	$data = $progress_manager_model->get_user_list($param);
}

for ($i = 0; $i < count($data); $i++){

	// 氏名
	$data[$i]['emp_full_nm']  = $data[$i]['emp_lt_nm'];
	$data[$i]['emp_full_nm'] .= '　';
	$data[$i]['emp_full_nm'] .= $data[$i]['emp_ft_nm'];

	// 所属
	$data[$i]['affiliation']  = $data[$i]['class_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['atrb_nm'];
	$data[$i]['affiliation'] .= ' > ';
	$data[$i]['affiliation'] .= $data[$i]['dept_nm'];
	if ($data[$i]['room_nm'] != '') {
		$data[$i]['affiliation'] .= ' > ';
		$data[$i]['affiliation'] .= $data[$i]['room_nm'];
	}

	// ラダーレベル
	switch($data[$i]['now_level']){
		case 1:
			$data[$i]['now_level'] = 'I';
			break;
		case 2:
			$data[$i]['now_level'] = 'II';
			break;
		case 3:
			$data[$i]['now_level'] = 'III';
			break;
		case 4:
			$data[$i]['now_level'] = 'IV';
			break;
		case 5:
			$data[$i]['now_level'] = 'V';
			break;
		default:
			$data[$i]['now_level'] = '';
	}

	// 認定日
	if ($data[$i]['get_level_date'] != ''){
		$apply_date_year = substr($data[$i]['get_level_date'],0,4);
		$apply_date_month = substr($data[$i]['get_level_date'],5,2);
		$apply_date_day = substr($data[$i]['get_level_date'],8,2);
		$data[$i]['get_level_date']  = $apply_date_year;
		$data[$i]['get_level_date'] .= '/';
		$data[$i]['get_level_date'] .= $apply_date_month;
		$data[$i]['get_level_date'] .= '/';
		$data[$i]['get_level_date'] .= $apply_date_day;
	}

	// 認定証発行日
	if ($data[$i]['level_certificate_date'] != ''){
		$apply_date_year = substr($data[$i]['level_certificate_date'],0,4);
		$apply_date_month = substr($data[$i]['level_certificate_date'],5,2);
		$apply_date_day = substr($data[$i]['level_certificate_date'],8,2);
		$data[$i]['level_certificate_date']  = $apply_date_year;
		$data[$i]['level_certificate_date'] .= '/';
		$data[$i]['level_certificate_date'] .= $apply_date_month;
		$data[$i]['level_certificate_date'] .= '/';
		$data[$i]['level_certificate_date'] .= $apply_date_day;
	}

}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

/**
 * テンプレートマッピング
 */

$smarty->assign( 'cl_title'						, $cl_title						);
$smarty->assign( 'session'						, $session						);
$smarty->assign( 'yui_cal_part'					, $yui_cal_part					);
$smarty->assign( 'calendar'						, $calendar						);
$smarty->assign( 'workflow_auth'				, $workflow_auth				);
$smarty->assign( 'aplyctn_mnitm_str'			, $aplyctn_mnitm_str			);
$smarty->assign( 'js_str'						, $js_str						);

$smarty->assign( 'option_level'					, $option_level					);
$smarty->assign( 'emp_nm'						, $emp_nm						);
$smarty->assign( 'pst_slct_bx_strct_str'		, $pst_slct_bx_strct_str		);
$smarty->assign( 'option_date_authorize_from_y'	, $option_date_authorize_from_y	);
$smarty->assign( 'option_date_authorize_from_m'	, $option_date_authorize_from_m	);
$smarty->assign( 'option_date_authorize_from_d'	, $option_date_authorize_from_d	);
$smarty->assign( 'option_date_authorize_to_y'	, $option_date_authorize_to_y	);
$smarty->assign( 'option_date_authorize_to_m'	, $option_date_authorize_to_m	);
$smarty->assign( 'option_date_authorize_to_d'	, $option_date_authorize_to_d	);

$smarty->assign( 'data'							, $data							);

$smarty->assign( 'date_update_flg'				, $date_update_flg				);
$smarty->assign( 'emp_date'						, $emp_date						);

// 2012/08/24 Yamagawa add(s)
if ($_POST['retire_disp_flg'] == '') {
	$retire_disp_flg = '';
} else {
	$retire_disp_flg = 'checked';
}
$smarty->assign( 'retire_disp_flg'				, $retire_disp_flg				);
// 2012/08/24 Yamagawa add(e)

// 2012/08/21 Yamagawa add(s)
if (count($council) > 0) {
	$council_flg = true;
} else {
	$council_flg = false;
}
$smarty->assign( 'council_flg'					, $council_flg					);
// 2012/08/21 Yamagawa add(e)

$log->debug("テンプレートマッピング",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート：".basename($_SERVER['PHP_SELF'],'.php').".tpl",__FILE__,__LINE__);
$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");
$log->debug("テンプレート出力",__FILE__,__LINE__);

if ($date_update_flg == 'update'){
	echo("<script language=\"javascript\">document.ladder_form.date_update_flg.value = '';</script>");
	echo("<script language=\"javascript\">output_to_print();</script>");
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function get_select_around_years($date)
{
	ob_start();
	show_years_around(1, 1, $date);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 年オプションHTML取得(報告日用)
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 必須レベルオプションHTML取得
 */
function get_select_levels($level)
{

	ob_start();
	show_levels_options($level);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_levels_options($level) {

	$arr_levels_nm = array("","I","II","III","IV","V");
	$arr_levels_id = array("","1","2","3","4","5");

	for($i=0;$i<count($arr_levels_nm);$i++) {

		echo("<option value=\"$arr_levels_id[$i]\"");
		if($level == $arr_levels_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_levels_nm[$i]\n");
	}
}

/**
 * セクション名(部門・課・科・室)取得
 */
function get_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj)
{
	ob_start();
	show_post_select_box_strict($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $obj, "", true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

	ob_start();
	write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
	$str_buff = ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_years_around($before_count, $after_count, $num){

	$now  = date(Y);
	if ($num == ''){
		$num = $now;
	}

	// 当年まで
	for($i=0; $i<=$before_count;$i++){
		$yr = $now - $before_count + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

	// 翌年から
	for($i=1; $i<=$after_count;$i++){
		$yr = $now + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

}

$log->info(basename(__FILE__)." END");
$log->shutdown();
