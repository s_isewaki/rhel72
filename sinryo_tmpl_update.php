<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");
require_once("show_sinryo_tmpl_header.ini");
require_once("sinryo_common.ini");
require_once("label_by_profile_type.ini");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("./conf/sql.inf");
require_once("sum_application_workflow_common_class.php");
require_once("show_class_name.ini");
require_once("library_common.php");
require_once("sot_util.php");
require_once("summary_common.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療区分登録権限チェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);

// 診療区分情報を取得(初期表示の場合のみ)
if(@$preview_flg != 1)
{
	$sql = "select * from tmplmst";
	$cond = "where tmpl_id = '$tmpl_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$tmpl_name   = pg_fetch_result($sel, 0, "tmpl_name"  );
	$tmpl_file   = pg_fetch_result($sel, 0, "tmpl_file"  );
	$window_size = pg_fetch_result($sel, 0, "window_size");
  $undeletable = pg_fetch_result($sel, 0, "undeletable");
  $disp_del_bttn = ($undeletable == "1") ? "0" : "1" ;
}
// ワークフローIDの取得
$tmp = select_from_table($con, "select wkfw_id from sum_wkfwmst where tmpl_id = ".$tmpl_id, "", $fname);
$wkfw_id = @pg_fetch_result($tmp, 0, "wkfw_id");

$tmpl_name_html = h($tmpl_name);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

$obj = new application_workflow_common_class($con, $fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 部門一覧を取得
$sel_class = $obj->get_classmst();
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	$classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sel_atrb = $obj->get_atrbmst();

// 科一覧を取得
$sel_dept = $obj->get_deptmst();
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_dept_nm = $row["dept_nm"];
	$dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sel_st = $obj->get_stmst();

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);


// 職員ID取得
$emp_id = get_emp_id($con,$session,$fname);

// ログインユーザの職員情報を取得
$sql = "select emp_class, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = '$emp_id'";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	require_once("summary_common.ini");
	summary_common_show_error_page_and_die();
}
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_name = pg_fetch_result($sel_emp, 0, "emp_name");

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

// プレビュー押下時
if (@$preview_flg == "1") {
// wkfw_content 保存
	if (strlen(@$wkfw_content) > 0) {
		$ext = ".php";
		$savefilename = "workflow/tmp/{$session}_t{$ext}";

		// 内容書き込み
		if (!$fp = fopen($savefilename, "w")) {
			echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}
		if (!fwrite($fp, $wkfw_content, 2000000)) {
			echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
			echo("<script language=\"javascript\">history.back();</script>");
			exit;
		}

		fclose($fp);

		$wkfw_title = str_replace("'","\'", $wkfw_title);
	}
}


if (@$back != "t" && @$wkfw_id != "") {
	$sql = "select * from sum_wkfwmst";
	$cond = "where tmpl_id = $tmpl_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	$wkfw_enabled = pg_fetch_result($sel, 0, "wkfw_enabled");
	$wkfw_type = pg_fetch_result($sel, 0, "wkfw_type");
	$wkfw_title = pg_fetch_result($sel, 0, "wkfw_title");
	$wkfw_content = pg_fetch_result($sel, 0, "wkfw_content");
	$start_date = pg_fetch_result($sel, 0, "wkfw_start_date");
	$end_date = pg_fetch_result($sel, 0, "wkfw_end_date");
	$wkfw_appr = pg_fetch_result($sel, 0, "wkfw_appr");
	$wkfw_content_type = pg_fetch_result($sel, 0, "wkfw_content_type");
	$wkfw_folder_id = pg_fetch_result($sel, 0, "wkfw_folder_id");
	$is_hide_apply_list = pg_fetch_result($sel, 0, "is_hide_apply_list");
	$is_hide_approve_list = pg_fetch_result($sel, 0, "is_hide_approve_list");
	$is_modifyable_by_other_emp = pg_fetch_result($sel, 0, "is_modifyable_by_other_emp");
	$is_hide_approve_status = pg_fetch_result($sel, 0, "is_hide_approve_status");

	$start_year = substr($start_date, 0, 4);
	$start_month = substr($start_date, 4, 2);
	$start_day = substr($start_date, 6, 2);
	$end_year = substr($end_date, 0, 4);
	$end_month = substr($end_date, 4, 2);
	$end_day = substr($end_date, 6, 2);


	$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
	$ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
	$ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");

	$short_wkfw_name = pg_fetch_result($sel, 0, "short_wkfw_name");
	$apply_title_disp_flg = pg_fetch_result($sel, 0, "apply_title_disp_flg");

	$send_mail_flg = pg_fetch_result($sel, 0, "send_mail_flg");
	$lib_reg_flg = pg_fetch_result($sel, 0, "lib_reg_flg");
	$lib_keyword = pg_fetch_result($sel, 0, "lib_keyword");
	$lib_no = pg_fetch_result($sel, 0, "lib_no");
	$lib_summary = pg_fetch_result($sel, 0, "lib_summary");
	$lib_archive = pg_fetch_result($sel, 0, "lib_archive");
	$lib_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");
	$lib_folder_id = pg_fetch_result($sel, 0, "lib_folder_id");
	$lib_show_login_flg = pg_fetch_result($sel, 0, "lib_show_login_flg");
	$lib_show_login_begin = pg_fetch_result($sel, 0, "lib_show_login_begin");
	$lib_show_login_end = pg_fetch_result($sel, 0, "lib_show_login_end");
	$lib_private_flg = pg_fetch_result($sel, 0, "lib_private_flg");
	$lib_ref_dept_st_flg = pg_fetch_result($sel, 0, "lib_ref_dept_st_flg");
	$lib_ref_dept_flg = pg_fetch_result($sel, 0, "lib_ref_dept_flg");
	$lib_ref_st_flg = pg_fetch_result($sel, 0, "lib_ref_st_flg");
	$lib_upd_dept_st_flg = pg_fetch_result($sel, 0, "lib_upd_dept_st_flg");
	$lib_upd_dept_flg = pg_fetch_result($sel, 0, "lib_upd_dept_flg");
	$lib_upd_st_flg = pg_fetch_result($sel, 0, "lib_upd_st_flg");
	$approve_label = pg_fetch_result($sel, 0, "approve_label");

	$lib_show_login_begin1 = substr($lib_show_login_begin, 0, 4);
	$lib_show_login_begin2 = substr($lib_show_login_begin, 4, 2);
	$lib_show_login_begin3 = substr($lib_show_login_begin, 6, 2);
	$lib_show_login_end1 = substr($lib_show_login_end, 0, 4);
	$lib_show_login_end2 = substr($lib_show_login_end, 4, 2);
	$lib_show_login_end3 = substr($lib_show_login_end, 6, 2);

	switch ($lib_archive) {
	case "1":  // 個人ファイル
	case "2":  // 全体共有
		$lib_private_flg1 = "";
		$lib_private_flg2 = "";
		break;
	case "3":  // 部署共有
		$lib_private_flg1 = $lib_private_flg;
		$lib_private_flg2 = "";
		break;
	case "4":  // 委員会・WG共有
		$lib_private_flg1 = "";
		$lib_private_flg2 = $lib_private_flg;
		break;
	}
	// 本文形式タイプのデフォルトを「テキスト」とする
	if ($wkfw_content_type == "") {$wkfw_content_type = "1";}

	// 受信階層情報取得
		$sql = "select * from sum_wkfwapvmng";
	$cond = "where wkfw_id = $wkfw_id order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}
	if ((int)@$approve_num < 1) $approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $approve_num; $i++) {

		$ret_arr = array();

		// 受信者指定区分
		$apv_div0_flg = "apv_div0_flg$i";
		$apv_div1_flg = "apv_div1_flg$i";
		$apv_div2_flg = "apv_div2_flg$i";
		$apv_div3_flg = "apv_div3_flg$i";
		$apv_div4_flg = "apv_div4_flg$i";
		$apv_div5_flg = "apv_div5_flg$i";
		$apv_div6_flg = "apv_div6_flg$i";

		@$$apv_div0_flg .= @pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		@$$apv_div1_flg .= @pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		@$$apv_div2_flg .= @pg_fetch_result($sel, $i - 1, "apv_div2_flg");
		@$$apv_div3_flg .= @pg_fetch_result($sel, $i - 1, "apv_div3_flg");
		@$$apv_div4_flg .= @pg_fetch_result($sel, $i - 1, "apv_div4_flg");
		@$$apv_div5_flg .= @pg_fetch_result($sel, $i - 1, "apv_div5_flg");
		@$$apv_div6_flg .= @pg_fetch_result($sel, $i - 1, "apv_div6_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		$apv_num = pg_fetch_result($sel, $i - 1, "apv_num");
		$apv_common_group_id = @pg_fetch_result($sel, $i - 1, "apv_common_group_id");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t") {
			search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, @$mode, $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t") {
			search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, @$mode, $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t") {
			search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, @$mode, $ret_arr);
		}

		// 委員会・ＷＧ
		if(pg_fetch_result($sel, $i - 1, "apv_div3_flg") == "t") {
			search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, @$mode, $ret_arr);
		}

		// その他
		if(pg_fetch_result($sel, $i - 1, "apv_div2_flg") == "t") {
			set_aply_apv($apv_num, $ret_arr);
		}

		// 病棟担当組織に所属する職員
		if(pg_fetch_result($sel, $i - 1, "apv_div5_flg") == "t") {
			search_ward_empclass_relation($con, $fname, $ret_arr);
		}

		// 共通グループ
		if(pg_fetch_result($sel, $i - 1, "apv_div6_flg") == "t") {
			search_common_group($con, $fname, $apv_common_group_id, $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row) {

			// 受信者欄(表示用)
			$var_name = "approve$i";
			if($j > 0) $$var_name .= "<BR>";
			@$$var_name .= @$row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "target_class_div$i";
			@$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "st_id$i";
			@$$var_name .= $row["st_id"];
			// 受信者ＩＤ
			$var_name = "emp_id$i";
			@$$var_name .= $row["emp_id"];
			// 受信者名
			$var_name = "emp_nm$i";
			@$$var_name .= $row["emp_nm"];
			// 受信者削除フラグ
			$var_name = "emp_del_flg$i";
			@$$var_name .= $row["emp_del_flg"];

			// 委員会ＷＧ親ＩＤ
			$var_name = "pjt_parent_id$i";
			@$$var_name .= $row["pjt_parent_id"];

			// 委員会ＷＧ親名称
			$var_name = "pjt_parent_nm$i";
			@$$var_name .= $row["pjt_parent_nm"];

			// 委員会ＷＧ子ＩＤ
			$var_name = "pjt_child_id$i";
			@$$var_name .= $row["pjt_child_id"];

			// 委員会ＷＧ子名称
			$var_name = "pjt_child_nm$i";
			@$$var_name .= $row["pjt_child_nm"];

			// 部門ＩＤ
			$var_name = "class_sect_id$i";
			@$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "atrb_sect_id$i";
			@$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "dept_sect_id$i";
			@$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "room_sect_id$i";
			@$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "st_sect_id$i";
			@$$var_name .= $row["st_sect_id"];

		}

		$var_name = "multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

		$var_name = "next_notice_recv_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_recv_div"));

		$var_name = "apv_num$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "apv_num"));

		$var_name = "apv_common_group_id$i";
		$$var_name = (@pg_fetch_result($sel, $i - 1, "apv_common_group_id"));

	}

	// 結果通知管理情報取得
	$notice = "";
	$arr_wkfwnoticemng = $obj->get_wkfwnoticemng($wkfw_id, @$mode);
	$rslt_ntc_div0_flg = $arr_wkfwnoticemng["rslt_ntc_div0_flg"];
	$rslt_ntc_div1_flg = $arr_wkfwnoticemng["rslt_ntc_div1_flg"];
	$rslt_ntc_div2_flg = $arr_wkfwnoticemng["rslt_ntc_div2_flg"];
	$rslt_ntc_div3_flg = $arr_wkfwnoticemng["rslt_ntc_div3_flg"];
	$rslt_ntc_div4_flg = $arr_wkfwnoticemng["rslt_ntc_div4_flg"];
	$rslt_ntc_div5_flg = $arr_wkfwnoticemng["rslt_ntc_div5_flg"];
	$rslt_ntc_div6_flg = $arr_wkfwnoticemng["rslt_ntc_div6_flg"];

	$notice_target_class_div = $arr_wkfwnoticemng["notice_target_class_div"];

	// 部署役職指定(結果通知)
	if($rslt_ntc_div0_flg == "t")
	{
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "0", $mode);
		if($notice_target_class_div != 0)
		{
			$arr_classname = $obj->get_classname();
			if     ($notice_target_class_div == "1") $notice_target_class_name = $arr_classname[0]["class_nm"];
			else if($notice_target_class_div == "2") $notice_target_class_name = $arr_classname[0]["atrb_nm"];
			else if($notice_target_class_div == "3") $notice_target_class_name = $arr_classname[0]["dept_nm"];
			else if($notice_target_class_div == "4") $notice_target_class_name = $arr_classname[0]["room_nm"];

			$notice .= "申請者の所属する【";
	 		$notice .=	$notice_target_class_name;
 			$notice .=	"】の";
		}
		else
		{
			if(count($arr_wkfwnoticestdtl) > 0)
			{
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
			 			$notice .=	"【".$in_hospital2_title."】の";
				}
			}
		}

		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_id != "")
			{
				$notice_st_id .= ",";
				$notice .= ", ";
			}
			$notice_st_id .= $wkfwnoticestdtl["st_id"];
			$notice .= $wkfwnoticestdtl["st_nm"];
		}
	}

	// 部署役職指定(結果通知)(部署指定)
	if($rslt_ntc_div4_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		// 部署
		$arr_wkfwnoticesectdtl = $obj->get_wkfwnoticesectdtl($wkfw_id, $mode);
		$notice_class_sect_id  = $arr_wkfwnoticesectdtl["class_id"];
		$notice_atrb_sect_id	 = $arr_wkfwnoticesectdtl["atrb_id"];
		$notice_dept_sect_id	 = $arr_wkfwnoticesectdtl["dept_id"];
		$notice_room_sect_id	 = $arr_wkfwnoticesectdtl["room_id"];

		$notice_class_sect_nm = $arr_wkfwnoticesectdtl["class_nm"];
		$notice_atrb_sect_nm	= $arr_wkfwnoticesectdtl["atrb_nm"];
		$notice_dept_sect_nm	= $arr_wkfwnoticesectdtl["dept_nm"];
		$notice_room_sect_nm	= $arr_wkfwnoticesectdtl["room_nm"];

		$notice_all_class_nm = $notice_class_sect_nm;
		if($notice_all_class_nm != "")
		{
			if($notice_atrb_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_atrb_sect_nm;
			}

			if($notice_dept_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_dept_sect_nm;
			}

			if($notice_room_sect_nm != "")
			{
				$notice_all_class_nm .= " > ";
				$notice_all_class_nm .= $notice_room_sect_nm;
			}
		}

		// 役職
		$arr_wkfwnoticestdtl = $obj->get_wkfwnoticestdtl($wkfw_id, "4", $mode);

		$notice_st_sect_id = "";
		$notice_st_sect_nm = "";
		foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
		{
			if($notice_st_sect_id != "")
			{
				$notice_st_sect_id .= ",";
				$notice_st_sect_nm .= ", ";
			}
			$notice_st_sect_id .= $wkfwnoticestdtl["st_id"];
			$notice_st_sect_nm .= $wkfwnoticestdtl["st_nm"];
		}

		if($notice_all_class_nm != "" && $notice_st_sect_nm != "")
		{
			$notice .= $notice_all_class_nm."の".$notice_st_sect_nm;
		}
	}

	// 職員指定(結果通知)
	if($rslt_ntc_div1_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticedtl = $obj->get_wkfwnoticedtl($wkfw_id, $mode);
		foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
		{
			if($notice_emp_id != "")
			{
				$notice_emp_id .= ",";
				$notice_emp_nm .= ", ";
				$notice .= ", ";
			}
			$notice_emp_id .= $wkfwnoticedtl["emp_id"];
			$notice_emp_lt_nm = $wkfwnoticedtl["emp_lt_nm"];
			$notice_emp_ft_nm = $wkfwnoticedtl["emp_ft_nm"];

			$notice_emp_nm .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
			$notice .= $notice_emp_lt_nm." ".$notice_emp_ft_nm;
		}
	}

	// 委員会・ＷＧ指定(結果通知)
	if($rslt_ntc_div3_flg == "t")
	{
		if($notice != "")
		{
			$notice .= "<BR>";
		}

		$arr_wkfwnoticepjtdtl = $obj->get_wkfwnoticepjtdtl($wkfw_id, $mode);
		$notice_pjt_parent_id = $arr_wkfwnoticepjtdtl[0]["pjt_parent_id"];
		$notice_pjt_parent_nm = $arr_wkfwnoticepjtdtl[0]["pjt_parent_nm"];
		$notice_pjt_child_id	= $arr_wkfwnoticepjtdtl[0]["pjt_child_id"];
		$notice_pjt_child_nm	= $arr_wkfwnoticepjtdtl[0]["pjt_child_nm"];

		$notice .= $notice_pjt_parent_nm;
		if($notice_pjt_child_nm != "")
		{
			$notice .= " > ";
			$notice .= $notice_pjt_child_nm;
		}
	}

	// その他
	if($rslt_ntc_div2_flg == "t") {
		if($notice != "") $notice .= "<BR>";
		$notice .= "申請時に結果通知者を指定する";
	}

	// デフォルト値の設定
	$ref_dept_flg = ($ref_dept_flg == "") ? "1" : $ref_dept_flg;
	$ref_st_flg = ($ref_st_flg == "") ? "1" : $ref_st_flg;

}

?>
<title>CoMedix <? echo($med_report_title); ?>管理 | テンプレート更新</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">

function submitForm() {
	if (document.tmpl.tmpl_name.value == '') {
		alert('テンプレート名を入力してください。');
		return false;
	}
	if (document.tmpl.tmpl_file.value == '') {
		alert('テンプレートファイルを指定してください。');
		return false;
	}

	document.tmpl.submit();
}

function referTemplate() {
	window.open('sinryo_tmpl_refer.php?session=<? echo($session); ?>&mode=regist', 'newwin', 'width=720,height=480,scrollbars=yes');
}
function replaceTemplate() {
	var fn = document.getElementById("tmpl_file").value;
	window.open('sinryo_tmpl_refer_replace.php?session=<? echo($session); ?>&tmpl_file_name='+fn, 'newwin', 'width=720,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	if (document.tmpl.tmpl_content.value == '') {
		alert('テンプレートを指定してください。');
		return false;
	}

	document.tmpl.action = "sinryo_tmpl_update.php";
	document.tmpl.preview_flg.value = "1";
	document.tmpl.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=600,height=700";
	window.open(url, 'preview_window',option);
}

function checkWkfwSend(){
	var disabled = (document.getElementById("wkfw_enabled1").checked ? "" : "disabled");
	document.getElementById("wkfw_appr1").disabled = disabled;
	document.getElementById("wkfw_appr2").disabled = disabled;
	document.getElementById("approve_num").disabled = disabled;
	document.getElementById("is_hide_apply_list0").disabled = disabled;
	document.getElementById("is_hide_apply_list1").disabled = disabled;
	document.getElementById("is_hide_approve_list0").disabled = disabled;
	document.getElementById("is_hide_approve_list1").disabled = disabled;
	document.getElementById("is_modifyable_by_other_emp").disabled = disabled;
	document.getElementById("is_hide_approve_status").disabled = disabled;
}
function checkApprove(){
	document.tmpl.action = "sinryo_tmpl_update.php";
	document.tmpl.submit();
}

function clear_notice(){
	document.getElementById('notice_content').innerHTML = '';
	document.tmpl.notice.value = '';

	document.tmpl.rslt_ntc_div0_flg.value = '';
	document.tmpl.rslt_ntc_div1_flg.value = '';
	document.tmpl.rslt_ntc_div2_flg.value = '';
	document.tmpl.rslt_ntc_div3_flg.value = '';
	document.tmpl.rslt_ntc_div4_flg.value = '';

	document.tmpl.notice_target_class_div.value = '';
	document.tmpl.notice_st_id.value = '';
	document.tmpl.notice_emp_id.value = '';
	document.tmpl.notice_emp_nm.value = '';
	document.tmpl.notice_pjt_parent_id.value = '';
	document.tmpl.notice_pjt_child_id.value = '';
	document.tmpl.notice_pjt_parent_nm.value = '';
	document.tmpl.notice_pjt_child_nm.value = '';

	document.tmpl.notice_class_sect_id.value = '';
	document.tmpl.notice_atrb_sect_id.value = '';
	document.tmpl.notice_dept_sect_id.value = '';
	document.tmpl.notice_room_sect_id.value = '';
	document.tmpl.notice_st_sect_id.value = '';
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_TMPL"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<?
show_sinryo_tmpl_header($session,$fname,"UPDATE",$tmpl_id);
?>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="tmpl" action="sinryo_tmpl_update_exe.php" method="post">
<table width="580" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td align="left" bgcolor="#f6f9ff"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート名</font></nobr></td>
<td><input name="tmpl_name" type="text" size="60" maxlength="40" value="<? echo($tmpl_name); ?>"></td>
</tr>

<tr height="22">
<td align="left" bgcolor="#f6f9ff"><nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ウィンドウサイズ</font></nobr></td>
<td>
<nobr>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input name="window_size" id="window_size_1" type="radio" size="60" maxlength="40" value="1" <?if($window_size == 1){?>checked<?}?> >標準
&nbsp
<input name="window_size" id="window_size_0" type="radio" size="60" maxlength="40" value="0" <?if($window_size == 0){?>checked<?}?> >最大
</font>

</nobr>
</td>
</tr>

</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<table width="580" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left" width="180">

<table width="180" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="left" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文&nbsp;&nbsp;
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
<input type="button" value="差し替え" onclick="replaceTemplate();">
</font>
</td>
</tr>
</table>

</td>
<td valign="middle">
<input name="tmpl_file" id="tmpl_file" type="text" size="46" maxlength="40" value="<? echo($tmpl_file); ?>" readonly style="border:0;"></td>
</td>
<td align="right" width="220">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">
<input type="button" name="update" value="更新" onclick="submitForm();"></td>
</tr>
<tr>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" id="font_tmpl_remark">
<? if ($tmpl_file){
		$sql = "select remark from sum_tmpl_remark where tmpl_name = '". pg_escape_string($tmpl_file). "'";
		$sel = select_from_table($con, $sql, "", $fname);
		$remark = @pg_fetch_result($sel, 0, 0);
		for($i = 0; $i < strlen($remark); $i++){
			echo h(mb_substr($remark, $i, 1))."<wbr>";
		}
} ?>
</font></td>
</tr>
<tr>
<?
if (@$preview_flg != "1") {
	// ファイルから本文を読み込む
	if (@$back == "t") {
		$savefilename = "workflow/tmp/{$session}_{$tmpl_file}";
		$fp = @fopen($savefilename, "r");
		if ($fp != false){
			$tmpl_content = fread($fp, filesize($savefilename));
			fclose($fp);
		}
	} else {
		$savefilename = $tmpl_file;
		$fp = fopen($savefilename, "r");
		if ($fp != false){
			$tmpl_content = fread($fp, filesize($savefilename));
			fclose($fp);
		}
		// XML作成用
		$pos = strpos($tmpl_content, "xml_creator");
//		$pos = @strrpos($tmpl_content, "<", $pos);  // strrposの第三引数は、PHP5から
		$pos1 = strpos($tmpl_content, "create_", $pos);
		$pos2 = strpos($tmpl_content, "\"", $pos1);
		$create_filename = substr($tmpl_content, $pos1, $pos2 - $pos1);

		if ($create_filename != $tmpl_file){
			$fp = @fopen($create_filename, "r");
			if ($fp != false){
				$utf_content = fread($fp, filesize($create_filename));
				fclose($fp);
			}

			// 文字コード変換　UTF-8 -> EUC
			$create_content = mb_convert_encoding(@$utf_content, "EUC-JP", "UTF-8");

			if (strpos($create_content, "<? // XML") === false) {
				$create_content = ereg_replace("<\?php", "<?", $create_content);
				$create_content = ereg_replace("<\?", "<? // XML作成用コード。この行は変更しないでください。", $create_content);
			}
			$tmpl_content .= $create_content;
		}
	}
}

$template_version_description = (strpos($tmpl_content, "COMEDIX_TEMPLATE_VERSION_DEFINITION") === false ? 0 : 1);
?>
<td colspan="3"><textarea name="tmpl_content" rows="20" cols="80" style="ime-mode: active; width:580px"><?=h($tmpl_content); ?></textarea></td>
</tr>



<tr>
<td colspan="3">

<table width="580" border="0" cellspacing="0" cellpadding="2" class="list">

<!-- 削除ボタンの表示 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除ボタンの表示</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="disp_del_bttn" id="disp_del_bttn1" value="1" <? if ($disp_del_bttn == "1"){ ?>checked<? } ?>>あり
<input type="radio" name="disp_del_bttn" id="disp_del_bttn0" value="0" <? if ($disp_del_bttn != "1"){ ?>checked<? } ?>>なし</font></td>
</tr>

<!-- ワークフロー機能 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ワークフロー機能</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="wkfw_enabled" id="wkfw_enabled2" value="0" onclick="checkWkfwSend()" <? if (@$wkfw_enabled != "1"){ ?>checked<? } ?>>利用しない
<input type="radio" name="wkfw_enabled" id="wkfw_enabled1" value="1" onclick="checkWkfwSend()" <? if (@$wkfw_enabled == "1"){ ?>checked<? } ?>>利用する
</font></td>
</tr>

<!-- 送信通知 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">送信通知</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="is_hide_apply_list" id="is_hide_apply_list1" value="0" onclick="checkWkfwSend()" <? if (@$is_hide_apply_list != "1"){ ?>checked<? } ?>>送信一覧へ表示
<input type="radio" name="is_hide_apply_list" id="is_hide_apply_list0" value="1" onclick="checkWkfwSend()" <? if (@$is_hide_apply_list == "1"){ ?>checked<? } ?>>送信一覧で非表示
</font></td>
</tr>

<!-- 受信通知 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信通知</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="is_hide_approve_list" id="is_hide_approve_list1" value="0" onclick="checkWkfwSend()" <? if (@$is_hide_approve_list != "1"){ ?>checked<? } ?>>受信一覧へ表示
<input type="radio" name="is_hide_approve_list" id="is_hide_approve_list0" value="1" onclick="checkWkfwSend()" <? if (@$is_hide_approve_list == "1"){ ?>checked<? } ?>>受信一覧で非表示
</font></td>
</tr>


<!-- 更新権限 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新権限</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="is_modifyable_by_other_emp" id="is_modifyable_by_other_emp" value="1" <? if (@$is_modifyable_by_other_emp == "1"){ ?>checked<? } ?>>他者による更新を許可する
</font></td>
</tr>


<!-- 受信状況表示 -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信状況表示</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="is_hide_approve_status" id="is_hide_approve_status" value="1" <? if (@$is_hide_approve_status == "1"){ ?>checked<? } ?>>受信状況を非表示
</font></td>
</tr>


<!-- 受信タイプ -->
<tr height="22">
<td width="28%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信タイプ</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="wkfw_appr" id="wkfw_appr1" value="1"<? if (@$wkfw_appr == "1") {echo(" checked");} ?>>同報
<input type="radio" name="wkfw_appr" id="wkfw_appr2" value="2"<? if (@$wkfw_appr == "2") {echo(" checked");} ?>>稟議（回覧）
</font></td>
</tr>


<!-- 管理ＣＤ -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理ＣＤ</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=h(@$short_wkfw_name);?></font></td>
<input type="hidden" name="short_wkfw_name" value="<?=h(@$short_wkfw_name);?>">


<!-- 受信階層数 -->
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">受信階層数</font></td>
<td colspan="2"><select name="approve_num" id="approve_num" onChange="checkApprove();"><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if (@$approve_num == $i) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>





<?
$approve = array(@$approve1, @$approve2, @$approve3, @$approve4, @$approve5, @$approve6, @$approve7, @$approve8, @$approve9, @$approve10, @$approve11, @$approve12, @$approve13, @$approve14, @$approve15, @$approve16, @$approve17, @$approve18, @$approve19, @$approve20);

$apv_div0_flg = array(@$apv_div0_flg1, @$apv_div0_flg2, @$apv_div0_flg3, @$apv_div0_flg4, @$apv_div0_flg5, @$apv_div0_flg6, @$apv_div0_flg7, @$apv_div0_flg8, @$apv_div0_flg9, @$apv_div0_flg10, @$apv_div0_flg11, @$apv_div0_flg12, @$apv_div0_flg13, @$apv_div0_flg14, @$apv_div0_flg15, @$apv_div0_flg16, @$apv_div0_flg17, @$apv_div0_flg18, @$apv_div0_flg19, @$apv_div0_flg20);
$apv_div1_flg = array(@$apv_div1_flg1, @$apv_div1_flg2, @$apv_div1_flg3, @$apv_div1_flg4, @$apv_div1_flg5, @$apv_div1_flg6, @$apv_div1_flg7, @$apv_div1_flg8, @$apv_div1_flg9, @$apv_div1_flg10, @$apv_div1_flg11, @$apv_div1_flg12, @$apv_div1_flg13, @$apv_div1_flg14, @$apv_div1_flg15, @$apv_div1_flg16, @$apv_div1_flg17, @$apv_div1_flg18, @$apv_div1_flg19, @$apv_div1_flg20);
$apv_div2_flg = array(@$apv_div2_flg1, @$apv_div2_flg2, @$apv_div2_flg3, @$apv_div2_flg4, @$apv_div2_flg5, @$apv_div2_flg6, @$apv_div2_flg7, @$apv_div2_flg8, @$apv_div2_flg9, @$apv_div2_flg10, @$apv_div2_flg11, @$apv_div2_flg12, @$apv_div2_flg13, @$apv_div2_flg14, @$apv_div2_flg15, @$apv_div2_flg16, @$apv_div2_flg17, @$apv_div2_flg18, @$apv_div2_flg19, @$apv_div2_flg20);
$apv_div3_flg = array(@$apv_div3_flg1, @$apv_div3_flg2, @$apv_div3_flg3, @$apv_div3_flg4, @$apv_div3_flg5, @$apv_div3_flg6, @$apv_div3_flg7, @$apv_div3_flg8, @$apv_div3_flg9, @$apv_div3_flg10, @$apv_div3_flg11, @$apv_div3_flg12, @$apv_div3_flg13, @$apv_div3_flg14, @$apv_div3_flg15, @$apv_div3_flg16, @$apv_div3_flg17, @$apv_div3_flg18, @$apv_div3_flg19, @$apv_div3_flg20);
$apv_div4_flg = array(@$apv_div4_flg1, @$apv_div4_flg2, @$apv_div4_flg3, @$apv_div4_flg4, @$apv_div4_flg5, @$apv_div4_flg6, @$apv_div4_flg7, @$apv_div4_flg8, @$apv_div4_flg9, @$apv_div4_flg10, @$apv_div4_flg11, @$apv_div4_flg12, @$apv_div4_flg13, @$apv_div4_flg14, @$apv_div4_flg15, @$apv_div4_flg16, @$apv_div4_flg17, @$apv_div4_flg18, @$apv_div4_flg19, @$apv_div4_flg20);
$apv_div5_flg = array(@$apv_div5_flg1, @$apv_div5_flg2, @$apv_div5_flg3, @$apv_div5_flg4, @$apv_div5_flg5, @$apv_div5_flg6, @$apv_div5_flg7, @$apv_div5_flg8, @$apv_div5_flg9, @$apv_div5_flg10, @$apv_div5_flg11, @$apv_div5_flg12, @$apv_div5_flg13, @$apv_div5_flg14, @$apv_div5_flg15, @$apv_div5_flg16, @$apv_div5_flg17, @$apv_div5_flg18, @$apv_div5_flg19, @$apv_div5_flg20);
$apv_div6_flg = array(@$apv_div6_flg1, @$apv_div6_flg2, @$apv_div6_flg3, @$apv_div6_flg4, @$apv_div6_flg5, @$apv_div6_flg6, @$apv_div6_flg7, @$apv_div6_flg8, @$apv_div6_flg9, @$apv_div6_flg10, @$apv_div6_flg11, @$apv_div6_flg12, @$apv_div6_flg13, @$apv_div6_flg14, @$apv_div6_flg15, @$apv_div6_flg16, @$apv_div6_flg17, @$apv_div6_flg18, @$apv_div6_flg19, @$apv_div6_flg20);


$target_class_div = array(@$target_class_div1, @$target_class_div2, @$target_class_div3, @$target_class_div4, @$target_class_div5, @$target_class_div6, @$target_class_div7, @$target_class_div8, @$target_class_div9, @$target_class_div10, @$target_class_div11, @$target_class_div12, @$target_class_div13, @$target_class_div14, @$target_class_div15, @$target_class_div16, @$target_class_div17, @$target_class_div18, @$target_class_div19, @$target_class_div20);
$st_id = array(@$st_id1, @$st_id2, @$st_id3, @$st_id4, @$st_id5, @$st_id6, @$st_id7, @$st_id8, @$st_id9, @$st_id10, @$st_id11, @$st_id12, @$st_id13, @$st_id14, @$st_id15, @$st_id16, @$st_id17, @$st_id18, @$st_id19, @$st_id20);
$emp_ids = array(@$emp_id1, @$emp_id2, @$emp_id3, @$emp_id4, @$emp_id5, @$emp_id6, @$emp_id7, @$emp_id8, @$emp_id9, @$emp_id10, @$emp_id11, @$emp_id12, @$emp_id13, @$emp_id14, @$emp_id15, @$emp_id16, @$emp_id17, @$emp_id18, @$emp_id19, @$emp_id20);
$emp_nm = array(@$emp_nm1, @$emp_nm2, @$emp_nm3, @$emp_nm4, @$emp_nm5, @$emp_nm6, @$emp_nm7, @$emp_nm8, @$emp_nm9, @$emp_nm10, @$emp_nm11, @$emp_nm12, @$emp_nm13, @$emp_nm14, @$emp_nm15, @$emp_nm16, @$emp_nm17, @$emp_nm18, @$emp_nm19, @$emp_nm20);
$emp_del_flg = array(@$emp_del_flg1, @$emp_del_flg2, @$emp_del_flg3, @$emp_del_flg4, @$emp_del_flg5, @$emp_del_flg6, @$emp_del_flg7, @$emp_del_flg8, @$emp_del_flg9, @$emp_del_flg10, @$emp_del_flg11, @$emp_del_flg12, @$emp_del_flg13, @$emp_del_flg14, @$emp_del_flg15, @$emp_del_flg16, @$emp_del_flg17, @$emp_del_flg18, @$emp_del_flg19, @$emp_del_flg20);
$pjt_parent_id = array(@$pjt_parent_id1, @$pjt_parent_id2, @$pjt_parent_id3, @$pjt_parent_id4, @$pjt_parent_id5, @$pjt_parent_id6, @$pjt_parent_id7, @$pjt_parent_id8, @$pjt_parent_id9, @$pjt_parent_id10, @$pjt_parent_id11, @$pjt_parent_id12, @$pjt_parent_id13, @$pjt_parent_id14, @$pjt_parent_id15, @$pjt_parent_id16, @$pjt_parent_id17, @$pjt_parent_id18, @$pjt_parent_id19, @$pjt_parent_id20);
$pjt_child_id = array(@$pjt_child_id1, @$pjt_child_id2, @$pjt_child_id3, @$pjt_child_id4, @$pjt_child_id5, @$pjt_child_id6, @$pjt_child_id7, @$pjt_child_id8, @$pjt_child_id9, @$pjt_child_id10, @$pjt_child_id11, @$pjt_child_id12, @$pjt_child_id13, @$pjt_child_id14, @$pjt_child_id15, @$pjt_child_id16, @$pjt_child_id17, @$pjt_child_id18, @$pjt_child_id19, @$pjt_child_id20);
$pjt_parent_nm = array(@$pjt_parent_nm1, @$pjt_parent_nm2, @$pjt_parent_nm3, @$pjt_parent_nm4, @$pjt_parent_nm5, @$pjt_parent_nm6, @$pjt_parent_nm7, @$pjt_parent_nm8, @$pjt_parent_nm9, @$pjt_parent_nm10, @$pjt_parent_nm11, @$pjt_parent_nm12, @$pjt_parent_nm13, @$pjt_parent_nm14, @$pjt_parent_nm15, @$pjt_parent_nm16, @$pjt_parent_nm17, @$pjt_parent_nm18, @$pjt_parent_nm19, @$pjt_parent_nm20);
$pjt_child_nm = array(@$pjt_child_nm1, @$pjt_child_nm2, @$pjt_child_nm3, @$pjt_child_nm4, @$pjt_child_nm5, @$pjt_child_nm6, @$pjt_child_nm7, @$pjt_child_nm8, @$pjt_child_nm9, @$pjt_child_nm10, @$pjt_child_nm11, @$pjt_child_nm12, @$pjt_child_nm13, @$pjt_child_nm14, @$pjt_child_nm15, @$pjt_child_nm16, @$pjt_child_nm17, @$pjt_child_nm18, @$pjt_child_nm19, @$pjt_child_nm20);

$class_sect_id = array(@$class_sect_id1, @$class_sect_id2, @$class_sect_id3, @$class_sect_id4, @$class_sect_id5, @$class_sect_id6, @$class_sect_id7, @$class_sect_id8, @$class_sect_id9, @$class_sect_id10, @$class_sect_id11, @$class_sect_id12, @$class_sect_id13, @$class_sect_id14, @$class_sect_id15, @$class_sect_id16, @$class_sect_id17, @$class_sect_id18, @$class_sect_id19, @$class_sect_id20);
$atrb_sect_id = array(@$atrb_sect_id1, @$atrb_sect_id2, @$atrb_sect_id3, @$atrb_sect_id4, @$atrb_sect_id5, @$atrb_sect_id6, @$atrb_sect_id7, @$atrb_sect_id8, @$atrb_sect_id9, @$atrb_sect_id10, @$atrb_sect_id11, @$atrb_sect_id12, @$atrb_sect_id13, @$atrb_sect_id14, @$atrb_sect_id15, @$atrb_sect_id16, @$atrb_sect_id17, @$atrb_sect_id18, @$atrb_sect_id19, @$atrb_sect_id20);
$dept_sect_id = array(@$dept_sect_id1, @$dept_sect_id2, @$dept_sect_id3, @$dept_sect_id4, @$dept_sect_id5, @$dept_sect_id6, @$dept_sect_id7, @$dept_sect_id8, @$dept_sect_id9, @$dept_sect_id10, @$dept_sect_id11, @$dept_sect_id12, @$dept_sect_id13, @$dept_sect_id14, @$dept_sect_id15, @$dept_sect_id16, @$dept_sect_id17, @$dept_sect_id18, @$dept_sect_id19, @$dept_sect_id20);
$room_sect_id = array(@$room_sect_id1, @$room_sect_id2, @$room_sect_id3, @$room_sect_id4, @$room_sect_id5, @$room_sect_id6, @$room_sect_id7, @$room_sect_id8, @$room_sect_id9, @$room_sect_id10, @$room_sect_id11, @$room_sect_id12, @$room_sect_id13, @$room_sect_id14, @$room_sect_id15, @$room_sect_id16, @$room_sect_id17, @$room_sect_id18, @$room_sect_id19, @$room_sect_id20);

$st_sect_id = array(@$st_sect_id1, @$st_sect_id2, @$st_sect_id3, @$st_sect_id4, @$st_sect_id5, @$st_sect_id6, @$st_sect_id7, @$st_sect_id8, @$st_sect_id9, @$st_sect_id10, @$st_sect_id11, @$st_sect_id12, @$st_sect_id13, @$st_sect_id14, @$st_sect_id15, @$st_sect_id16, @$st_sect_id17, @$st_sect_id18, @$st_sect_id19, @$st_sect_id20);

$multi_apv_flg = array(@$multi_apv_flg1, @$multi_apv_flg2, @$multi_apv_flg3, @$multi_apv_flg4, @$multi_apv_flg5, @$multi_apv_flg6, @$multi_apv_flg7, @$multi_apv_flg8, @$multi_apv_flg9, @$multi_apv_flg10, @$multi_apv_flg11, @$multi_apv_flg12, @$multi_apv_flg13, @$multi_apv_flg14, @$multi_apv_flg15, @$multi_apv_flg16, @$multi_apv_flg17, @$multi_apv_flg18, @$multi_apv_flg19, @$multi_apv_flg20);
$next_notice_div = array(@$next_notice_div1, @$next_notice_div2, @$next_notice_div3, @$next_notice_div4, @$next_notice_div5, @$next_notice_div6, @$next_notice_div7, @$next_notice_div8, @$next_notice_div9, @$next_notice_div10, @$next_notice_div11, @$next_notice_div12, @$next_notice_div13, @$next_notice_div14, @$next_notice_div15, @$next_notice_div16, @$next_notice_div17, @$next_notice_div18, @$next_notice_div19, @$next_notice_div20);
$next_notice_recv_div = array(@$next_notice_recv_div1, @$next_notice_recv_div2, @$next_notice_recv_div3, @$next_notice_recv_div4, @$next_notice_recv_div5, @$next_notice_recv_div6, @$next_notice_recv_div7, @$next_notice_recv_div8, @$next_notice_recv_div9, @$next_notice_recv_div10, @$next_notice_recv_div11, @$next_notice_recv_div12, @$next_notice_recv_div13, @$next_notice_recv_div14, @$next_notice_recv_div15, @$next_notice_recv_div16, @$next_notice_recv_div17, @$next_notice_recv_div18, @$next_notice_recv_div19, @$next_notice_recv_div20);

$apv_num = array(@$apv_num1, @$apv_num2, @$apv_num3, @$apv_num4, @$apv_num5, @$apv_num6, @$apv_num7, @$apv_num8, @$apv_num9, @$apv_num10, @$apv_num11, @$apv_num12, @$apv_num13, @$apv_num14, @$apv_num15, @$apv_num16, @$apv_num17, @$apv_num18, @$apv_num19, @$apv_num20);

$apv_common_group_id = array(@$apv_common_group_id1, @$apv_common_group_id2, @$apv_common_group_id3, @$apv_common_group_id4, @$apv_common_group_id5, @$apv_common_group_id6, @$apv_common_group_id7, @$apv_common_group_id8, @$apv_common_group_id9, @$apv_common_group_id10, @$apv_common_group_id11, @$apv_common_group_id12, @$apv_common_group_id13, @$apv_common_group_id14, @$apv_common_group_id15, @$apv_common_group_id16, @$apv_common_group_id17, @$apv_common_group_id18, @$apv_common_group_id19, @$apv_common_group_id20);

//$deci_flg = array(@$deci_flg1, @$deci_flg2, @$deci_flg3, @$deci_flg4, @$deci_flg5, @$deci_flg6, @$deci_flg7, @$deci_flg8, @$deci_flg9, @$deci_flg10, @$deci_flg11, @$deci_flg12, @$deci_flg13, @$deci_flg14, @$deci_flg15, @$deci_flg16, @$deci_flg17, @$deci_flg18, @$deci_flg19, @$deci_flg20);

for ($i = 0; $i < @$approve_num; $i++) {
	$j = $i + 1;

//	if(@$emp_del_flg[$i] == 't') {
//		$text_color = '#FF0000';
//	} else {
		$text_color = '#000000';
//	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"if (document.getElementById('wkfw_enabled1').checked) window.open('sum_workflow_approve_list.php?session=$session&approve=$j', 'newwin2', 'width=640,height=700,scrollbars=yes')\">受信階層" . $j . "</a></font></td>\n");
	echo("<td colspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"green\"><span id=\"approve_content". $j ."\">".$approve[$i]."</span></font>");

//	echo("<td colspan=\"2\"><input name=\"approve_content" . $j . "\" type=\"text\" size=\"50\" maxlength=\"50\" value=\"".$approve[$i]."\" style=\"color:".$text_color."\" readonly>\n");
//	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">決定権</font><input type=\"checkbox\" name=\"deci_flg" . $j . "\"");
//	if ($deci_flg[$i] == "on") {
//		echo(" checked");
//	}
//	echo(">");

	$target_class_div[$i] = ($target_class_div[$i] == "") ? "1" : $target_class_div[$i];
	$multi_apv_flg[$i] = ($multi_apv_flg[$i] == "") ? "f" : $multi_apv_flg[$i];
	$next_notice_div[$i] = ($next_notice_div[$i] == "") ? "2" : $next_notice_div[$i];
	$next_notice_recv_div[$i] = ($next_notice_recv_div[$i] != "2") ? "1" : "2";
	$apv_num[$i] = ($apv_num[$i] == "") ? "1" : $apv_num[$i];

	echo("<input type=\"hidden\" name=\"approve" . $j . "\" value=\"" . $approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_div0_flg" . $j . "\" value=\"" . $apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div1_flg" . $j . "\" value=\"" . $apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div2_flg" . $j . "\" value=\"" . $apv_div2_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div3_flg" . $j . "\" value=\"" . $apv_div3_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div4_flg" . $j . "\" value=\"" . $apv_div4_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div5_flg" . $j . "\" value=\"" . $apv_div5_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"apv_div6_flg" . $j . "\" value=\"" . $apv_div6_flg[$i] . "\">\n");


	echo("<input type=\"hidden\" name=\"target_class_div" . $j . "\" value=\"" . $target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"st_id" . $j . "\" value=\"" . $st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_id" . $j . "\" value=\"" . $emp_ids[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"emp_nm" . $j . "\" value=\"" . $emp_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_id" . $j . "\" value=\"" . $pjt_parent_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_id" . $j . "\" value=\"" . $pjt_child_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_parent_nm" . $j . "\" value=\"" . $pjt_parent_nm[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"pjt_child_nm" . $j . "\" value=\"" . $pjt_child_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"class_sect_id" . $j . "\" value=\"" . $class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"atrb_sect_id" . $j . "\" value=\"" . $atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"dept_sect_id" . $j . "\" value=\"" . $dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"room_sect_id" . $j . "\" value=\"" . $room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"st_sect_id" . $j . "\" value=\"" . $st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"multi_apv_flg" . $j . "\" value=\"" . $multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"next_notice_div" . $j . "\" value=\"" . $next_notice_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"next_notice_recv_div" . $j . "\" value=\"" . $next_notice_recv_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_num" . $j . "\" value=\"" . $apv_num[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"apv_common_group_id" . $j . "\" value=\"" . $apv_common_group_id[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");
}
?>





<!-- 送信者以外への結果通知 -->
<!--tr height="100">
<td align="right" bgcolor="#f6f9ff">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">送信者以外への結果通知</font><br>
<input type="button" value="設定" id="wkfw_notice_submit" style="margin-left:2em;width:4.0em;" onclick="window.open('sum_workflow_notice_list.php?session=<?=$session?>', 'newwin2', 'width=640,height=700,scrollbars=yes');"><br>
<input type="button" value="クリア" id="wkfw_notice_clear" style="margin-left:2em;width:4.0em;" onclick="clear_notice();">
</td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="green"><span id="notice_content"><?=@$notice?></span></font></td>
</font></td>
</tr-->

<input type="hidden" name="notice" value="<?=@$notice?>">

<input type="hidden" name="send_mail_flg" value="f">

<input type="hidden" name="rslt_ntc_div0_flg" id="rslt_ntc_div0_flg" value="<?=@$rslt_ntc_div0_flg?>">
<input type="hidden" name="rslt_ntc_div1_flg" id="rslt_ntc_div1_flg" value="<?=@$rslt_ntc_div1_flg?>">
<input type="hidden" name="rslt_ntc_div2_flg" id="rslt_ntc_div2_flg" value="<?=@$rslt_ntc_div2_flg?>">
<input type="hidden" name="rslt_ntc_div3_flg" id="rslt_ntc_div3_flg" value="<?=@$rslt_ntc_div3_flg?>">
<input type="hidden" name="rslt_ntc_div4_flg" id="rslt_ntc_div4_flg" value="<?=@$rslt_ntc_div4_flg?>">
<input type="hidden" name="rslt_ntc_div5_flg" id="rslt_ntc_div5_flg" value="<?=@$rslt_ntc_div5_flg?>">
<input type="hidden" name="rslt_ntc_div6_flg" id="rslt_ntc_div6_flg" value="<?=@$rslt_ntc_div6_flg?>">

<input type="hidden" name="notice_target_class_div" value="<?=@$notice_target_class_div?>">
<input type="hidden" name="notice_st_id" value="<?=@$notice_st_id?>">
<input type="hidden" name="notice_emp_id" value="<?=@$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" value="<?=@$notice_emp_nm?>">
<input type="hidden" name="notice_pjt_parent_id" value="<?=@$notice_pjt_parent_id?>">
<input type="hidden" name="notice_pjt_child_id" value="<?=@$notice_pjt_child_id?>">
<input type="hidden" name="notice_pjt_parent_nm" value="<?=@$notice_pjt_parent_nm?>">
<input type="hidden" name="notice_pjt_child_nm" value="<?=@$notice_pjt_child_nm?>">

<input type="hidden" name="notice_class_sect_id" id="notice_class_sect_id" value="<?=@$notice_class_sect_id?>">
<input type="hidden" name="notice_atrb_sect_id" id="notice_atrb_sect_id" value="<?=@$notice_atrb_sect_id?>">
<input type="hidden" name="notice_dept_sect_id" id="notice_dept_sect_id" value="<?=@$notice_dept_sect_id?>">
<input type="hidden" name="notice_room_sect_id" id="notice_room_sect_id" value="<?=@$notice_room_sect_id?>">
<input type="hidden" name="notice_st_sect_id" id="notice_st_sect_id" value="<?=@$notice_st_sect_id?>">

<input type="hidden" name="notice_apv_common_group_id" value="<?=@$notice_apv_common_group_id?>">

</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="wkfw_id" value="<?=@$wkfw_id?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="mode" value="update">
<input type="hidden" name="tmpl_id" value="<? echo(@$tmpl_id); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
<script type="text/javascript">
checkWkfwSend();
<?
// プレビュー押下時
if (@$preview_flg == "1") {
// content 保存
	$savefilename = "workflow/tmp/{$session}_{$tmpl_file}";

// 内容書き込み
	$fp = fopen($savefilename, "w");
	if (!fwrite($fp, $tmpl_content, 2000000)) {
		fclose($fp);
		echo("<script language=\"javascript\">alert('ファイルの書き込みができませんでした。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}

	fclose($fp);

?>
	var window_size = 0;
	if(document.getElementById('window_size_1').checked) {
		window_size = 1;
	}
	var url = 'sinryo_tmpl_preview.php'
		+ '?session=<? echo($session); ?>'
		+ '&filename=' + document.tmpl.tmpl_file.value
		+ '&window_size=' + window_size
		+ '&style2=<?=@$template_version_description?>';
	show_preview_window(url);
<?
}
?>
</script>
</body>
<? pg_close($con); ?>
</html>

<?
// ワークロー・受信者詳細情報取得
function search_wkfw_apv_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	$sql	= "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg ";
	$sql .= "from sum_wkfwapvdtl a ";
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "inner join authmst c on a.emp_id = c.emp_id ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order order by a.apv_sub_order asc";
	$cond = "";

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	$idx = 0;
	while($row = pg_fetch_array($sel))
	{

		if($idx > 0)
		{
			$emp_id .= ",";
			$approve .= ", ";
			$emp_del_flg .= ",";
		}
		@$emp_id .= $row["emp_id"];
		@$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		@$emp_del_flg .= $row["emp_del_flg"];

		$emp_full_nm = $emp_lt_nm ." ".$emp_ft_nm;
		@$approve .= $emp_full_nm;
		$idx++;
	}

	array_push($ret_arr, array("approve" => $approve,
									"target_class_div" => "",
								"st_id" => "",
									"emp_id" => $emp_id,
									"emp_nm" => $approve,
									"emp_del_flg" => $emp_del_flg,
									"pjt_parent_id" => "",
									"pjt_parent_nm" => "",
									"pjt_child_id" => "",
									"pjt_child_nm" => "",
	 									"class_sect_id" => "",
									"atrb_sect_id" => "",
									"dept_sect_id" => "",
									"room_sect_id" => "",
									"st_sect_id" => ""));

}

// ワークフロー・部署役職情報取得
function search_wkfw_apv_pst($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	global $_label_by_profile;
	global $profile_type;

	$sql	= "select ";
	$sql .= "a.target_class_div, ";
	$sql .= "case a.target_class_div ";
	$sql .= "when '1' then c.class_nm ";
	$sql .= "when '2' then c.atrb_nm ";
	$sql .= "when '3' then c.dept_nm ";
	$sql .= "when '4' then c.room_nm ";
	$sql .= "else '' ";
	$sql .= "end as target_class_name, ";
	$sql .= "b.st_id,";
	$sql .= "b.st_nm ";
	$sql .= "from sum_wkfwapvmng a left outer join ";
	$sql .= "(select a.wkfw_id, a.apv_order, a.st_id, b.st_nm ";
	$sql .= "from sum_wkfwapvpstdtl a inner join stmst b on a.st_id = b.st_id where st_div = 0) b on ";
	$sql .= "a.wkfw_id = b.wkfw_id and a.apv_order = b.apv_order, ";
	$sql .= "(select * from classname) c ";
	$sql .= "where a.wkfw_id = $wkfw_id and a.apv_order = $apv_order ";

		$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	$num = pg_numrows($sel);

	$approve = "";
	$target_class_div = "";
	$st_id = "";
	$st_nm = "";


	for ($i=0; $i<$num; $i++) {

		if($i == 0) {
				$target_class_div = pg_fetch_result($sel, $i, "target_class_div");
				$target_class_name = pg_fetch_result($sel, $i, "target_class_name");
		 		$st_id = pg_fetch_result($sel, $i, "st_id");
			$st_nm = pg_fetch_result($sel, $i, "st_nm");

			if($target_class_div != 0) {
				$approve .= "送信者の所属する【";
			 		$approve .=	$target_class_name;
		 			$approve .=	"】の";
			} else {
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
			 			$approve .=	"【".$in_hospital2_title."】の";
				}
			}

			$approve .=	$st_nm;

		} else {
			$st_id .=",";
		 		$st_id .= pg_fetch_result($sel, $i, "st_id");

			$st_nm = pg_fetch_result($sel, $i, "st_nm");
			$approve .=	", ";
			$approve .=	$st_nm;
		}


	}

	array_push($ret_arr, array("approve" => $approve,
									"target_class_div" => $target_class_div,
								"st_id" => $st_id,
									"emp_id" => "",
									"emp_nm" => "",
									"emp_del_flg" => "f",
									"pjt_parent_id" => "",
									"pjt_parent_nm" => "",
									"pjt_child_id" => "",
									"pjt_child_nm" => "",
									"class_sect_id" => "",
									"atrb_sect_id" => "",
									"dept_sect_id" => "",
									"room_sect_id" => "",
									"st_sect_id" => ""));
}

// 委員会ＷＧ情報取得
function search_wkfw_pjt_dtl($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	$sql	= "select pjt_id, pjt_name from project ";
	$sql .= "where pjt_id in (select parent_pjt_id from sum_wkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order) ";
	$sql .= "union all ";
	$sql .= "select pjt_id, pjt_name from project ";
	$sql .= "where pjt_id in (select child_pjt_id from sum_wkfwpjtdtl where wkfw_id = $wkfw_id and apv_order = $apv_order)";
	$sel	= select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	if(pg_numrows($sel) == 1)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

		$approve = $pjt_parent_nm;
	}
	else if(pg_numrows($sel) == 2)
	{
		$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
		$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
		$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
		$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

		$approve	= $pjt_parent_nm;
		$approve .= " > ";
		$approve .= $pjt_child_nm;
	}
	array_push($ret_arr, array("approve" => $approve,
									"target_class_div" => "",
								"st_id" => "",
									"emp_id" => "",
									"emp_nm" => "",
									"emp_del_flg" => "f",
									"pjt_parent_id" => $pjt_parent_id,
									"pjt_parent_nm" => $pjt_parent_nm,
									"pjt_child_id" => @$pjt_child_id,
									"pjt_child_nm" => @$pjt_child_nm,
									"class_sect_id" => "",
									"atrb_sect_id" => "",
									"dept_sect_id" => "",
									"room_sect_id" => "",
									"st_sect_id" => ""));
}

// 部署役職(部署所属)取得
function search_wkfw_apv_pst_sect($con, $fname, $wkfw_id, $apv_order, $mode, &$ret_arr) {
	$sql	= "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
	$sql .= "from sum_wkfwapvsectdtl a ";
	$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
	$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
	$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
	$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
	$sql .= "where a.wkfw_id = $wkfw_id ";
	$sql .= "and a.apv_order = $apv_order ";

	$sel	= select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		require_once("summary_common.ini");
		summary_common_show_error_page_and_die();
	}

	$class_sect_id = pg_fetch_result($sel, 0, "class_id");
	$atrb_sect_id = pg_fetch_result($sel, 0, "atrb_id");
	$dept_sect_id = pg_fetch_result($sel, 0, "dept_id");
	$room_sect_id = pg_fetch_result($sel, 0, "room_id");

	$class_nm = pg_fetch_result($sel, 0, "class_nm");
	$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
	$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
	$room_nm = pg_fetch_result($sel, 0, "room_nm");
	$class_sect_nm = "";

	if($class_nm != "")
	{
		$class_sect_nm .= $class_nm;

		if($atrb_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $atrb_nm;
		}

		if($dept_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $dept_nm;
		}

		if($room_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $room_nm;
		}
	}

	// 役職
	if($class_sect_nm != "") {
		$sql	= "select a.*, b.st_nm ";
		$sql .= "from sum_wkfwapvpstdtl a ";
		$sql .= "left join stmst b on a.st_id = b.st_id and not st_del_flg ";
		$sql .= "where a.wkfw_id = $wkfw_id ";
		$sql .= "and a.apv_order = $apv_order ";
		$sql .= "and a.st_div = 4 ";

		$sel	= select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			require_once("summary_common.ini");
			summary_common_show_error_page_and_die();
		}
		$all_st_sect_id = "";
		$all_st_sect_nm = "";
		while($row = pg_fetch_array($sel))
		{
			if($all_st_sect_id != "")
			{
				$all_st_sect_id .= ",";
				$all_st_sect_nm .= ", ";
			}

			$all_st_sect_id .= $row["st_id"];
			$all_st_sect_nm .= $row["st_nm"];
		}

	}
	$approve = "";
	if($class_sect_nm != "" && $all_st_sect_nm != "")
	{
		$approve = $class_sect_nm."の".$all_st_sect_nm;
	}

	array_push($ret_arr, array("approve" => $approve,
	 								"target_class_div" => "",
								"st_id" => "",
									"emp_id" => "",
									"emp_nm" => "",
									"emp_del_flg" => "f",
									"pjt_parent_id" => "",
									"pjt_parent_nm" => "",
									"pjt_child_id" => "",
									"pjt_child_nm" => "",
									"class_sect_id" => $class_sect_id,
									"atrb_sect_id" => $atrb_sect_id,
									"dept_sect_id" => $dept_sect_id,
									"room_sect_id" => $room_sect_id,
									"st_sect_id" => $all_st_sect_id));
}

function set_aply_apv($apv_num, &$ret_arr) {

	$approve = "送信時に受信者を指定する(".$apv_num."名)";
	array_push($ret_arr, array("approve" => $approve,
									"target_class_div" => "",
									"st_id" => "",
									"emp_id" => "",
									"emp_nm" => "",
	 								"emp_del_flg" => "f",
									"pjt_parent_id" => "",
									"pjt_parent_nm" => "",
									"pjt_child_id" => "",
									"pjt_child_nm" => "",
									"class_sect_id" => "",
									"atrb_sect_id" => "",
									"dept_sect_id" => "",
									"room_sect_id" => "",
									"st_sect_id" => ""));

}

function search_ward_empclass_relation($con, $fname, &$ret_arr) {

	// 患者の病棟を検索

	array_push($ret_arr, array("approve" => "病棟担当組織に所属する職員",
		"target_class_div" => "",
		"st_id" => "",
		"emp_id" => "",
		"emp_nm" => "",
		"emp_del_flg" => "f",
		"pjt_parent_id" => "",
		"pjt_parent_nm" => "",
		"pjt_child_id" => "",
		"pjt_child_nm" => "",
		"class_sect_id" => "",
		"atrb_sect_id" => "",
		"dept_sect_id" => "",
		"room_sect_id" => "",
		"st_sect_id" => "",
		"ward_empclass_relation" => "t")
	);

}

function search_common_group($con, $fname, $apv_common_group_id, &$ret_arr) {
	global $c_sot_util;
	// 共通グループ名を検索
	$sql = "select group_nm from comgroupmst where group_id = '" . $apv_common_group_id . "'";
	$sel = $c_sot_util->select_from_table($sql);
	$group_nm = pg_fetch_result($sel, 0, "group_nm");

	// 共通グループを検索
//	$sql = "select * from comgroup where group_id = '" . $apv_common_group_id . "' order by emp_id";

	array_push($ret_arr, array("approve" => "共通グループ(".$group_nm.")",
		"target_class_div" => "",
		"st_id" => "",
		"emp_id" => "",
		"emp_nm" => "",
		"emp_del_flg" => "f",
		"pjt_parent_id" => "",
		"pjt_parent_nm" => "",
		"pjt_child_id" => "",
		"pjt_child_nm" => "",
		"class_sect_id" => "",
		"atrb_sect_id" => "",
		"dept_sect_id" => "",
		"room_sect_id" => "",
		"st_sect_id" => "",
		"common_group_id" => $apv_common_group_id)
	);

}
?>
