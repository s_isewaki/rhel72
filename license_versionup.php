<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ライセンス管理 | バージョンアップ</title>
<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// ライセンス管理権限のチェック
$checkauth = check_authority($session, 38, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="license_menu.php?session=<? echo($session); ?>"><img src="img/icon/b29.gif" width="32" height="32" border="0" alt="ライセンス管理"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="license_menu.php?session=<? echo($session); ?>"><b>ライセンス管理</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="license_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ライセンス登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="license_versionup.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>バージョンアップ</b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" enctype="multipart/form-data" action="license/license_versionup_upload.php" method="post">
<table width="70%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>バージョンアップ手順</b></font></td>
</tr>
<tr height="10">
<td></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<b>手順1</b><br>
オーナーズクラブサイトからダウンロードしたアップデートファイル（comedix-update.zip）をサーバにアップロードしてください。（下記、「参照」ボタンで、PC上のアップデートファイルを選択した後、「アップロード」ボタンをクリックする）
</font></td>
</tr>
<tr height="22">
<td><input type="file" name="updatefile" style="width:400px;"></td>
</tr>
<tr height="22">
<td><input type="submit" value="アップロード" style="margin-left:347px;"></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<b>手順2</b><br>
サーバにrootでログインして、GNOME端末（もしくは、PCからsshでリモートログインしたターミナル画面）から、下記、コマンドを実行してください。
</font></td>
</tr>
<tr height="22">
<td>
<table width="400" border="0" cellspacing="0" cellpadding="10" bgcolor="black">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="white">
<pre style="margin:0;">cd /var/www/html<span style="color:gray;">[Enter]</span>
unzip -o comedix-update.zip<span style="color:gray;">[Enter]</span>
/bin/sh comedix-update.sh<span style="color:gray;">[Enter]</span></pre>
</font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
以上でアップデートは完了です。GNOME端末は「exit」を入力、リターンで終了します。<br>
アップデートファイルが適切でない場合は、日付パラメータアンマッチのエラーとなりますので、正しいアップデートファイルを取得し直してください。
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
</html>
