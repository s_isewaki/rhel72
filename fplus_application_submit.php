<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="fplus_menu.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_apply_id$i";
	echo("<input type=\"hidden\" name=\"precond_apply_id$i\" value=\"{$$varname}\">\n");
}

for ($i = 1; $i <= $precond_num; $i++) {
	$varname = "precond_wkfw_id$i";
	echo("<input type=\"hidden\" name=\"precond_wkfw_id$i\" value=\"{$$varname}\">\n");
}


//for ($i = 1; $i <= $approve_num; $i++) {
//	$varname = "approve_name$i";
//	echo("<input type=\"hidden\" name=\"approve_name$i\" value=\"{$$varname}\">\n");
//}

?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="wkfw_id" value="<? echo($wkfw_id); ?>">
<input type="hidden" name="wkfw_type" value="<? echo($wkfw_type); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="precond_num" value="<? echo($precond_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="apply_title_disp_flg" value="<?=$apply_title_disp_flg?>">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">
<input type="hidden" name="wkfwfile_history_no" value="<?=$wkfwfile_history_no?>">
</form>
<?

$fname=$PHP_SELF;

require_once("./conf/sql.inf");
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("fplus_common_class.php");


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 78, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
//	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("fplus")) {
	mkdir("fplus", 0755);
}
if (!is_dir("fplus/apply")) {
	mkdir("fplus/apply", 0755);
}
if (!is_dir("fplus/apply/tmp")) {
	mkdir("fplus/apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");

	$tmp_filename = "fplus/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

$obj = new fplus_common_class($con, $fname);

// トランザクション開始
pg_query($con, "begin");


// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];
$emp_class     = $arr_empmst[0]["emp_class"];
$emp_attribute = $arr_empmst[0]["emp_attribute"];
$emp_dept      = $arr_empmst[0]["emp_dept"];
$emp_room      = $arr_empmst[0]["emp_room"];

// 新規申請(apply_id)採番
$sql = "select max(apply_id) from fplusapply";
$cond = "";
$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
$max = pg_result($apply_max_sel,0,"max");
if($max == ""){
	$apply_id = "1";
}else{
	$apply_id = $max + 1;
}

// テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "fplus/workflow/tmp/{$session}_x{$ext}";
//	if (is_file($savexmlfilename)) {
//		include( $savexmlfilename );
//	}

	// ワークフロー情報取得
//20070918
//	$sel_wkfwmst = search_wkfwmst($con, $fname, $wkfw_id);
//	$wkfw_content = pg_fetch_result($sel_wkfwmst, 0, "wkfw_content");

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];
	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、報告してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、報告してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );
 
}

$apply_no = null;
if($draft != "on")
{
	$date = date("Ymd");
	$year = substr($date, 0, 4);
	$md   = substr($date, 4, 4);

	if($md >= "0101" and $md <= "0331")
	{
		$year = $year - 1;
	}
	$max_cnt = $obj->get_apply_cnt_per_year($year);
	$apply_no = $max_cnt + 1;
}

//レポートの種類
//"Episys107A"-エピネット報告書Ａ針刺し・切創
//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染
//"MRSA"-MRSA等耐性菌検出報告書
if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107A" || ($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107AO"))
{

	//"Episys107A"-エピネット報告書Ａ針刺し・切創

	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	
	$wcnt0 = 1;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			//エピネット報告書の入力データである
			
			//$set_data[$wcnt0] = $epi_val;
			$set_data[$wcnt0] = pg_escape_string($epi_val);
			$set_key[$wcnt0] = $epi_key;
			$wcnt0++;
		}
	}


	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107A")
	{ 
            //集計用データの登録処理("Episys107A"-エピネット報告書Ａ針刺し・切創)
            $obj->regist_Episys107A($set_key,$set_data);
        }else if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107AO"){
            //集計用データの登録処理("Episys107AO"-エピネット報告書ＡO針刺し・切創:手術用)
            $obj->regist_Episys107AO($set_key,$set_data);
        }
	
	
}
else if(($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B") || ($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107BO") ||
                      ($MRSA_REPORT == "MRSA"))
{
	
	//MRSA等耐性菌検出報告書
	//"Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染
		
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	
	$wcnt0 = 1;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{

			if(is_array($epi_val))
			{
				//エピネット、感染症報告書の入力データである(checkbox用)
				
				//$set_data[$wcnt0] = $epi_val[0];
				$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
				$set_key[$wcnt0] = $epi_key;
				$wcnt0++;
			}
			else
			{
				//エピネット、感染症報告書の入力データである
				
				//$set_data[$wcnt0] = $epi_val;
				$set_data[$wcnt0] = pg_escape_string($epi_val);
				$set_key[$wcnt0] = $epi_key;
				$wcnt0++;
			}
		}
	}
	
	
	if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107B")
	{ 
		//集計用データの登録処理("Episys107B"-エピネット報告書Ｂ皮膚・粘膜汚染)
		$obj->regist_Episys107B($set_key,$set_data);
	}
	elseif($MRSA_REPORT == "MRSA")
	{
		//集計用データの登録処理(MRSA等耐性菌検出報告書)
		$obj->regist_MRSA($set_key,$set_data);
	}else if($EPINET_REPORT_PIERCE_CUT_INFECTION == "Episys107BO"){
                //集計用データの登録処理("Episys107BO"-エピネット報告書ＢO皮膚・粘膜汚染手術)
		$obj->regist_Episys107BO($set_key,$set_data);
        }

}else if($IMMEDIATE_REPORT == "Immediaterepo"){
	//緊急報告受付

	// 新規レポートID採番
	$sql = "SELECT max(immediate_report_id) FROM fplus_immediate_report";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	// 新規リンク?採番
	$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
	$cond = "";
	$link_id_sel = select_from_table($con,$sql,$cond,$fname);
	$link_id = pg_result($link_id_sel,0,"max");
	if($link_id == ""){
		$link_id = 1;
	}else{
		$link_id = $link_id + 1;
	}

	//リンクテーブル登録項目ここから
	$set_data_link = array();
	$set_key_link = array();
	$set_data_link[0] = $link_id;
	$set_key_link[0] = "medical_accident_id";
	$set_data_link[1] = date("Y-m-d H:i:s");
	$set_key_link[1] = "update_time";
	$set_data_link[2] = "f";
	$set_key_link[2] = "del_flg";
	//リンクテーブル登録項目ここまで

	//緊急報告受付テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "immediate_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;

	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);

		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//緊急報告受付テーブル登録項目ここまで

	//管理テーブルの登録処理
	$obj->regist_link_table($set_key_link,$set_data_link);

	//データの登録処理(緊急報告受付)
	$obj->regist_immediate_repo($set_key,$set_data);

	

}else if($FIRST_REPORT == "Firstrepo"){
	
	//第一報

	// 新規レポートID採番
	$sql = "SELECT max(first_report_id) FROM fplus_first_report";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	// 新規リンク?採番
	$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
	$cond = "";
	$link_id_sel = select_from_table($con,$sql,$cond,$fname);
	$link_id = pg_result($link_id_sel,0,"max");
	if($link_id == ""){
		$link_id = 1;
	}else{
		$link_id = $link_id + 1;
	}

	//リンクテーブル登録項目ここから
	$set_data_link = array();
	$set_key_link = array();
	$set_data_link[0] = $link_id;
	$set_key_link[0] = "medical_accident_id";
	$set_data_link[1] = date("Y-m-d H:i:s");
	$set_key_link[1] = "update_time";
	$set_data_link[2] = "f";
	$set_key_link[2] = "del_flg";
	//リンクテーブル登録項目ここまで

	//第一報テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "first_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//第一報テーブル登録項目ここまで

	//管理テーブルの登録処理
	$obj->regist_link_table($set_key_link,$set_data_link);

	//データの登録処理(第一報)
	$obj->regist_first_repo($set_key,$set_data);

}else if($SECOND_REPORT == "Secondrepo"){
	
	//様式２

	// 新規レポートID採番
	$sql = "SELECT max(second_report_id) FROM fplus_second_report";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	if($hdn_link_id==""){
		// 新規リンク?採番
		$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
		$cond = "";
		$link_id_sel = select_from_table($con,$sql,$cond,$fname);
		$link_id = pg_result($link_id_sel,0,"max");
		if($link_id == ""){
			$link_id = 1;
		}else{
			$link_id = $link_id + 1;
		}

		//リンクテーブル登録項目ここから
		$set_data_link = array();
		$set_key_link = array();
		$set_data_link[0] = $link_id;
		$set_key_link[0] = "medical_accident_id";
		$set_data_link[1] = date("Y-m-d H:i:s");
		$set_key_link[1] = "update_time";
		$set_data_link[2] = "f";
		$set_key_link[2] = "del_flg";
		//リンクテーブル登録項目ここまで

	}else{
		//引用した第一報のリンクID
		$link_id = $hdn_link_id;
	}

	//様式２テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "second_report_id";
	$set_data[2] = $link_id;
	$set_key[2] = "medical_accident_id";
	$set_data[3] = $report_no;
	$set_key[3] = "report_no";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		$check .= $chk_key.",";
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//様式２テーブル登録項目ここまで

	if($hdn_link_id==""){
		//リンクテーブルの登録処理
		$obj->regist_link_table($set_key_link,$set_data_link);
	}

	//データの登録処理(様式２)
	$obj->regist_second_repo($set_key,$set_data);

}else if($CONSULTATION_REQUEST == "Consreq"){
	
	//"コンサルテーション依頼書

	// 新規レポートID採番
	$sql = "SELECT max(consultation_request_id) FROM fplus_consultation_request";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	if($hdn_link_id == ""){
		$link_id = NULL;
	}else{
		//引用したレポートID
		$link_id = $hdn_link_id;
	}
	$link_kbn = "";
	if($hdn_link_report_kbn!=""){
		//引用したレポートの区分
		$link_kbn = $hdn_link_report_kbn;
	}


	//コンサルテーション依頼書テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "consultation_request_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = $link_id;
	$set_key[3] = "report_id";
	$set_data[4] = $link_kbn;
	$set_key[4] = "report_type";
	$set_data[5] = date("Y-m-d H:i:s");
	$set_key[5] = "update_time";
	$set_data[6] = "f";
	$set_key[6] = "del_flg";

	$wcnt0 = 7;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//コンサルテーション依頼書テーブル登録項目ここまで

	//集計用データの登録処理("コンサルテーション依頼書)
	$obj->regist_EpisysConsreq($set_key,$set_data);

}else if($CONSULTATION_REPORT == "Consrep"){
	
	//"コンサルテーション報告書

	// 新規レポートID採番
	$sql = "SELECT max(consultation_report_id) FROM fplus_consultation_report";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	if($hdn_consultation_request_id == ""){
		$consultation_request_id = NULL;
	}else{
		//引用したレポートID
		$consultation_request_id = $hdn_consultation_request_id;
	}

	//コンサルテーション依頼書テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "consultation_report_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = $consultation_request_id;
	$set_key[3] = "consultation_request_id";
	$set_data[4] = date("Y-m-d H:i:s");
	$set_key[4] = "update_time";
	$set_data[5] = "f";
	$set_key[5] = "del_flg";

	$wcnt0 = 6;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//コンサルテーション依頼書テーブル登録項目ここまで

	//集計用データの登録処理(コンサルテーション報告書)
	$obj->regist_EpisysConsrep($set_key,$set_data);

}else if($MEDICAL_APPARATUS_FAULT_REPORT == "medicalapparatusfaultreport"){
	
	//医療機器不具合報告

	// 新規レポートID採番
	$sql = "SELECT max(medical_apparatus_fault_report_id) FROM fplus_medical_apparatus_fault_report";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);


	//医療機器不具合報告テーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "medical_apparatus_fault_report_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//医療機器不具合報告テーブル登録項目ここまで

	//データの登録処理(医療機器不具合報告)
	$obj->regist_medical_apparatus_fault_report($set_key,$set_data);

}else if($NUMBER_OF_OCCURRENCES == "number_of_occurrences"){
	
	//ヒヤリハット件数表

	// 新規報告?採番
	$sql = " SELECT a.short_wkfw_name ".
		" FROM fpluswkfwmst a inner join fpluscatemst b on a.wkfw_type = b.wkfw_type ".
		" WHERE ".
		" 	a.wkfw_id = '$wkfw_id' ".
		" 	AND a.wkfw_type = '$wkfw_type' ";
	$cond = "";
	$report_no_sel = select_from_table($con,$sql,$cond,$fname);
	$short_wkfw_name = pg_result($report_no_sel,0,"short_wkfw_name");
	$report_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

	if($draft != "on"){
		//年度、病院IDで既存データ検索
		$hospital_id = @$_REQUEST["data_hospital_id"];
		$data_occurrences_year = @$_REQUEST["data_occurrences_year"];
		$sql = " SELECT ".
			" 	lev.apply_id ".
			" FROM ".
			" 	fplus_number_of_occurrences_level lev ".
			" 	,fplusapply app  ".
			" WHERE ".
			" 	hospital_id = '$hospital_id' ".
			" 	AND occurrences_year = '$data_occurrences_year' ".
			" 	AND del_flg = 'f' ".
			" 	AND lev.apply_id = app.apply_id ".
			" 	AND app.delete_flg = 'f' ";
		$cond = "";
		$del_apply_id_sel = select_from_table($con,$sql,$cond,$fname);
		$del_apply_id = pg_result($del_apply_id_sel,0,"apply_id");

		if($del_apply_id != ""){
			if($apply_id != $del_apply_id){
				//レベル別発生件数データ削除処理(論理削除)
				$sql=	"update fplus_number_of_occurrences_level set";
				$set =	array("del_flg");
				$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
				$setvalue = array("t");
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				//職種別発生件数データ削除処理(論理削除)
				$sql=	"update fplus_number_of_occurrences set";
				$set =	array("del_flg");
				$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
				$setvalue = array("t");
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				//転倒・転落発生件数データ削除処理(論理削除)
				$sql=	"update fplus_number_of_occurrences_fall_accident set";
				$set =	array("del_flg");
				$cond = "where apply_id = '$del_apply_id' AND del_flg = 'f' ";
				$setvalue = array("t");
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				//報告データ削除（論理削除）
				$sql=	"update fplusapply set";
				$set =	array("delete_flg");
				$cond = "where apply_id = '$del_apply_id' AND delete_flg = 'f' ";
				$setvalue = array("t");
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
				if ($upd == 0) {
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
		}
	}

	// レベル別発生件数データ登録
	$sql = "SELECT max(levelt_id) FROM fplus_number_of_occurrences_level";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}
	//レベル別発生件数データテーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "levelt_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat1_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//レベル別発生件数データテーブル登録項目ここまで

	//データの登録処理(レベル別発生件数データ)
	$obj->regist_number_of_occurrences_level($set_key,$set_data);

	// 職種別発生件数データ登録
	// 新規レポートID採番
	$sql = "SELECT max(number_of_occurrences_id) FROM fplus_number_of_occurrences";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	//職種別発生件数データテーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "number_of_occurrences_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat2_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//発生件数データテーブル登録項目ここまで

	//データの登録処理(職種別発生件数データ)
	$obj->regist_number_of_occurrences($set_key,$set_data);


	//転倒・転落データ登録
	// 新規レポートID採番
	$sql = "SELECT max(fall_accident_id) FROM fplus_number_of_occurrences_fall_accident";
	$cond = "";
	$repo_id_sel = select_from_table($con,$sql,$cond,$fname);
	$repo_id = pg_result($repo_id_sel,0,"max");
	if($repo_id == ""){
		$repo_id = 1;
	}else{
		$repo_id = $repo_id + 1;
	}

	//転倒・転落発生件数データテーブル登録項目ここから
	$set_data = array();
	$set_key = array();
	$set_data[0] = $apply_id;
	$set_key[0] = "apply_id";
	$set_data[1] = $repo_id;
	$set_key[1] = "fall_accident_id";
	$set_data[2] = $report_no;
	$set_key[2] = "report_no";
	$set_data[3] = date("Y-m-d H:i:s");
	$set_key[3] = "update_time";
	$set_data[4] = "f";
	$set_key[4] = "del_flg";

	$wcnt0 = 5;
	
	foreach($_REQUEST as $epi_key => $epi_val)
	{
		$chk_key = substr($epi_key,0, 5);
		
		if($chk_key == "data_" or $chk_key == "dat3_")
		{
			$epi_key = substr($epi_key,5);
			if(is_array($epi_val))
			{
				//入力データcheckbox,radio用
				if($epi_val[0]!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val[0]);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
			else
			{
				//入力データ
				if($epi_val!=""){
					$set_data[$wcnt0] = pg_escape_string($epi_val);
					$set_key[$wcnt0] = $epi_key;
					$wcnt0++;
				}
			}
		}
	}
	//転倒・転落発生件数データテーブル登録項目ここまで

	//データの登録処理(転倒・転落発生件数)
	$obj->regist_number_of_occurrences_fall_accident($set_key,$set_data);


}


// 申請登録
$arr = array(
				"apply_id" => $apply_id,
				"wkfw_id" => $wkfw_id,
				"apply_content" => $content,
				"emp_id" => $emp_id,
				"apply_stat" => "0",
				"apply_date" => date("YmdHi"),
				"delete_flg" => "f",
				"apply_title" => $apply_title,
				"re_apply_id" => null,
				"apv_fix_show_flg" => "t",
				"apv_bak_show_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"apv_ng_show_flg" => "t",
				"emp_room" => $emp_room,
				"draft_flg" => ($draft == "on") ? "t" : "f",
				"wkfw_appr" => $wkfw_appr,
				"wkfw_content_type" => $wkfw_content_type,
				"apply_title_disp_flg" => $apply_title_disp_flg,
				"apply_no" => $apply_no,
				"notice_sel_flg" => $rslt_ntc_div2_flg,
				"wkfw_history_no" => ($wkfw_history_no == "") ? null : $wkfw_history_no,
				"wkfwfile_history_no" => ($wkfwfile_history_no == "") ? null : $wkfwfile_history_no
			 );

$obj->regist_apply($arr);

// 下書きでない場合、メール送信準備
$to_addresses = array();
if ($draft != "on") {
	$wkfw_send_mail_flg = fplus_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
} else {
	$wkfw_send_mail_flg = "f";
}

// 承認登録
for($i=1; $i<=$approve_num; $i++)
{
	$varname = "regist_emp_id$i";
	$regist_emp_id = ($$varname == "") ? null : $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "apv_order$i";
	$apv_order = ($$varname == "") ? null : $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = ($$varname == "") ? null : $$varname;

	$varname = "multi_apv_flg$i";
	$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

	$varname = "next_notice_div$i";
	$next_notice_div = ($$varname == "") ? null : $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;


	// 所属、役職も登録する
	$infos = get_empmst($con, $regist_emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];
	$emp_email     = $infos[31];

	$arr = array(
				"wkfw_id" => $wkfw_id,
				"apply_id" => $apply_id,
				"apv_order" => $apv_order,
				"emp_id" => $regist_emp_id,
				"apv_stat" => "0",
				"apv_date" => "",
				"delete_flg" => "f",
				"apv_comment" => "",
				"st_div" => ($st_div == "") ? null : $st_div,
				"deci_flg" => "t",
				"emp_class" => $emp_class,
				"emp_attribute" => $emp_attribute,
				"emp_dept" => $emp_dept,
				"emp_st" => $emp_st,
				"apv_fix_show_flg" => "t",
				"emp_room" => $emp_room,
				"apv_sub_order" => $apv_sub_order,
				"multi_apv_flg" => $multi_apv_flg,
				"next_notice_div" => $next_notice_div,
				"parent_pjt_id" => $parent_pjt_id,
				"child_pjt_id" => $child_pjt_id,
				"other_apv_flg" =>  "f"
				);

    $obj->regist_applyapv($arr);

	if (fplus_must_send_mail($wkfw_send_mail_flg, $emp_email, $wkfw_appr, $apv_order)) {
		$to_addresses[] = $emp_email;
	}
}


if($draft != "on")
{
    $obj->delete_applyapv($apply_id);
}

// 承認者候補登録
for($i=1; $i <= $approve_num; $i++)
{
	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "pst_approve_num$apv_order";
	$pst_approve_num = $$varname;

	for($j=1; $j<=$pst_approve_num; $j++)
	{
		$varname = "pst_emp_id$apv_order";
		$varname .= "_$j";
		$pst_emp_id = $$varname;

		$varname = "pst_st_div$apv_order";
		$varname .= "_$j";
		$pst_st_div = $$varname;

		$varname = "pst_parent_pjt_id$apv_order";
		$varname .= "_$j";
	    $pst_parent_pjt_id = ($$varname == "") ? null : $$varname;

		$varname = "pst_child_pjt_id$apv_order";
		$varname .= "_$j";
    	$pst_child_pjt_id = ($$varname == "") ? null : $$varname;

		$obj->regist_applyapvemp($apply_id, $apv_order, $j, $pst_emp_id, $pst_st_div, $pst_parent_pjt_id, $pst_child_pjt_id);
	}
}





// 添付ファイル登録
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 非同期・同期受信登録
if($wkfw_appr == "2")
{
	$previous_apv_order = "";
	$arr_apv = array();
	$arr_apv_sub_order = array();
	for($i=1; $i <= $approve_num; $i++)
	{
		$varname = "apv_order$i";
		$apv_order = $$varname;

		$varname = "apv_sub_order$i";
		$apv_sub_order = ($$varname == "") ? null : $$varname;

		$varname = "multi_apv_flg$i";
		$multi_apv_flg = ($$varname == "") ? "f" : $$varname;

		$varname = "next_notice_div$i";
		$next_notice_div = ($$varname == "") ? null : $$varname;

		if($previous_apv_order != $apv_order)
		{
			$arr_apv_sub_order = array();
		}
		$arr_apv_sub_order[] = $apv_sub_order;
		$arr_apv[$apv_order] = array("multi_apv_flg" => $multi_apv_flg, "next_notice_div" => $next_notice_div, "apv_sub_order" => $arr_apv_sub_order);

		$previous_apv_order = $apv_order;
	}

	$arr_send_apv_sub_order = array();
	foreach($arr_apv as $apv_order => $apv_info)
	{
		$multi_apv_flg = $apv_info["multi_apv_flg"];
		$next_notice_div = $apv_info["next_notice_div"];
		$arr_apv_sub_order = $apv_info["apv_sub_order"];

		foreach($arr_send_apv_sub_order as $send_apv_order => $arr_send_apv_sub)
		{
			// 非同期通知
			if($arr_send_apv_sub != null)
			{
				foreach($arr_send_apv_sub as $send_apv_sub_order)
				{
					foreach($arr_apv_sub_order as $recv_apv_sub_order)
					{
						$obj->regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $apv_order, $recv_apv_sub_order);
					}
				}
			}
			// 同期通知
			else
			{
				foreach($arr_apv_sub_order as $recv_apv_sub_order)
				{
					$obj->regist_applyasyncrecv($apply_id, $send_apv_order, null, $apv_order, $recv_apv_sub_order);
				}
			}

		}
		$arr_send_apv_sub_order = array();

		// 非同期通知の場合
		if($multi_apv_flg == "t" && $next_notice_div == "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = $arr_apv_sub_order;
		}
		// 同期通知または権限並列通知の場合
		else if($multi_apv_flg == "t" && $next_notice_div != "1" && count($arr_apv_sub_order) > 1)
		{
			$arr_send_apv_sub_order[$apv_order] = null;
		}
	}
}

// 申請結果通知登録
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// 前提とする申請書(申請用)登録
for($i=0; $i<$precond_num; $i++)
{
	$order = $i + 1;
	$varname = "precond_wkfw_id$order";
	$precond_wkfw_id = $$varname;

	$varname = "precond_apply_id$order";
	$precond_apply_id = $$varname;

	$obj->regist_applyprecond($apply_id, $precond_wkfw_id, $order, $precond_apply_id);
}

// メール送信に必要な情報を取得
if (count($to_addresses) > 0) {
	$emp_detail = $obj->get_empmst_detail($emp_id);
	$emp_nm = fplus_format_emp_nm($emp_detail[0]);
	$emp_mail = fplus_format_emp_mail($emp_detail[0]);
	$emp_pos = fplus_format_emp_pos($emp_detail[0]);

	$wkfwmst = $obj->get_wkfwmst($wkfw_id);
	$approve_label = ($wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] 受付依頼のお知らせ";
	$mail_content = fplus_format_mail_content($wkfw_content_type, $content);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の受付依頼がありました。\n\n";
		$mail_body .= "報告者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "表題：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");

	$tmp_filename = "fplus/apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "fplus/apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("fplus/apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}


if($draft == "on")
{
	echo("<script type=\"text/javascript\">location.href='fplus_menu.php?session=$session&apply_id=$apply_id';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href='fplus_apply_list.php?session=$session&mode=search&category=$wkfw_type&workflow=$wkfw_id';</script>");
}

?>
</body>
<?
// ワークフロー情報取得
/*
function search_wkfwmst($con, $fname, $wkfw_id) {

	$sql = "select * from wkfwmst";
	$cond="where wkfw_id='$wkfw_id'";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}
*/
?>
