<?php
include_once 'jnl_indicator_display.php';
class IndicatorReturnDisplay extends IndicatorDisplay
{
    /**
     * コンストラクタ
     * @param $arr
     */
    function IndicatorReturnDisplay($arr)
    {
        parent::IndicatorDisplay($arr);
    }
// [#1] implemetation of method to display data

// [@1]
    /**
     *
     * @param $dataArr
     * @param $plantArr
     * @param $arr
     * @return unknown_type
     */
    function getReturnDisplayData($dataArr, $plantArr, $arr)
    {
        $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
        $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

        $statusColorWhiteArr  = array('name' => 'background-color', 'value' => '#FFF;');
        $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
        $statusColorRedArr    = array('name' => 'background-color', 'value' => '#FFC8ED;');

        $statusWhiteColor  = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
        $statusYellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
        $statusRedColor    = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);

        $lastYear    = $this->getLastYear();
        $currentYear = $this->getCurrentYear();
        $startMonth  = $this->getFiscalYearStartMonth();

        $font = $this->getFont();
        if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
        $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';

        $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';

        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }
        $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '病院平均').'</td>';

        $tableHeader .= '</tr>';
        $dateTable   .= $tableHeader;

        $fCurrCell=$statusWhiteColor;

        $currentYearAverageColor = $statusWhiteColor;
        $currentAmgAvgRowAvgColColor = $statusWhiteColor;

        foreach ($plantArr as $plantKey => $plantVal)
        {
            $lastRowHeader = '<tr style="text-align : right;">'
                                .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';
            $currentRowHeader = '<tr style="text-align : right;">'
                                   .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="3">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                   .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

            $compRowHeader = '<tr style="text-align : right;">'
                                .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';
            $facilityId = $plantVal['facility_id'];

            $currRowTotal=0;
            $lastRowTotal=0;
            $compRowTotal=0;
            $currYearMonthCount = 0;
            $lastYearMonthCount = 0;
            $currRowStatusFlag = 1;
            $lastRowStatusFlag = 1;
            for ($i=0; $i < 12; $i++)
            {
                $monthRow    = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
                $lastYRow    = (($startMonth + $i) <= 12)? $lastYear        : $lastYear + 1;
                $currentYRow = (($startMonth + $i) <= 12)? $currentYear     : $currentYear + 1;

                $currentYearRow   = "";
                $lastYearRow      ="";
                $currYearRowColor = $statusWhiteColor;
                $lastYearRowColor = $statusWhiteColor;
                $compRow          = "";
                $currValue        = 0 ;
                $lastValue        = 0 ;
                $compValue        = 0 ;
                $compCellColor    = $compColor;
                $dataExist        = 0;

                for($j=0;$j<count($dataArr);$j++)
                {
                    if ($facilityId == $dataArr[$j]['facility_id'] && $monthRow == $dataArr[$j]['month'])
                    {
                        if($currentYRow == $dataArr[$j]['year'])
                        {
                            $dataExist    = 1;
                            $currValue    = $dataArr[$j]['gokei'];
                            $currRowTotal = $currRowTotal + round($currValue,1);

                            if($monthRow >= date("m") )
                            {
                                $currYearRowColor = ($currYearRowColor!=$statusWhiteColor)? $currYearRowColor : $statusWhiteColor;
                                //$compCellColor = $statusWhiteColor;
                            }
                            else
                            {
                                if($dataArr[$j]['status']==0)
                                {
                                    $currYearRowColor  = $statusYellowColor;
                                    $currentYearAverageColor = ($currentYearAverageColor==$statusRedColor)? $statusRedColor : $statusYellowColor;

                                    $currRowStatusFlag = 0;
                                    $compCellColor     = $statusYellowColor;

                                }
                                /*if($currValue==0)
                                {
                                    $currYearRowColor = $statusWhiteColor;
                                    $compCellColor = $statusWhiteColor;
                                }*/
//                                $currYearMonthCount = $currYearMonthCount + 1;
                            }
                            $currYearMonthCount = $currYearMonthCount + 1;
                        }

                        if($lastYRow == $dataArr[$j]['year'])
                        {
                            $lastValue= $dataArr[$j]['gokei'];
                            $lastRowTotal= $lastRowTotal + round($lastValue,1);
                            if($dataArr[$j]['status']==0)
                            {
                                $lastYearRowColor = $statusWhiteColor;
                                $lastRowStatusFlag = 0;
                            }
                            if($lastValue==0)
                            {
                                $lastYearRowColor = $statusWhiteColor;
                            }

                            $lastYearMonthCount = $lastYearMonthCount+1;

                        }
                    }
                }
                //echo "Present ".$dataExist." "." Month ".$monthRow." Yeare ".$currentYRow;
                if(($currValue==0 && $dataExist==0 && $monthRow < date("m") && $currentYRow <= date("Y")))
                {
                    $currYearRowColor = $statusRedColor;
                    $currentYearAverageColor = $statusRedColor;
                    $compCellColor    = $statusRedColor;
                }

                if($lastValue==0)
                {
                    $lastYearRowColor = $statusWhiteColor;
                }
                $compValue = round($currValue,1) - round($lastValue,1);

                $currentYearRow .='<td style="text-align : right;'.$currYearRowColor.'">'.sprintf($fontTag, number_format(round($currValue,1),1,".",",").'%').'</td>';
                $lastYearRow .='<td style="text-align : right;'.$lastYearRowColor.'">'.sprintf($fontTag, number_format(round($lastValue,1),1,".",",").'%').'</td>';
                $compRow .='<td style="text-align : right;'.$compCellColor.'">'.sprintf($fontTag, number_format(round($compValue,1),1,".",",").'%').'</td>';
                $currentRowHeader .= $currentYearRow;
                $lastRowHeader .= $lastYearRow;
                $compRowHeader .= $compRow;

            }
//            echo " compRowTotal ".$compRowTotal." currRowTotal ".$currRowTotal." lastRowTotal ".$lastRowTotal;
            if($currYearMonthCount > 0)
            {
                $currRowTotal = $currRowTotal / $currYearMonthCount ;
            }
            else
            {
                $currRowTotal = 0;
            }

            if($lastYearMonthCount>0)
            {
                $lastRowTotal = $lastRowTotal / $lastYearMonthCount ;
            }
            else
            {
                $lastRowTotal = 0;
            }

            $compRowTotal = $currRowTotal - $lastRowTotal;
            $compRowColor = $compColor;
//var_dump($currYearMonthCount);
//var_dump($currRowTotal);
            if($currYearMonthCount!=(date("m")-4))
            {
                $compRowColor= $currentYearAverageColor;
                if ($compRowColor == $statusWhiteColor) { $compRowColor = $compColor; }
//                $compRowColor= $statusRedColor;
                $TotalcurrentYearRow ='<td style="text-align : right;'.$currentYearAverageColor.'">'.sprintf($fontTag, number_format(round($currRowTotal, 1), 1, ".", ",").'%').'</td>';
//                $TotalcurrentYearRow ='<td style="text-align : right;'.$statusRedColor.'">'.sprintf($fontTag, number_format(round($currRowTotal, 1), 1, ".", "").'%').'</td>';
                if ($currentAmgAvgRowAvgColColor === $statusRedColor) {
                    $currentAmgAvgRowAvgColColor = $statusRedColor;
                }
                else if ($currentAmgAvgRowAvgColColor === $statusYellowColor)  {
                    $currentAmgAvgRowAvgColColor = $statusYellowColor;
                    if ($currentYearAverageColor === $statusRedColor) {
                        $currentAmgAvgRowAvgColColor = $statusRedColor;
                    }
                }
                else if($currentAmgAvgRowAvgColColor === $statusWhiteColor) { $currentAmgAvgRowAvgColColor = $currentYearAverageColor; }
            }
            else
            {
                if($currRowStatusFlag==0)
                {
                    $compRowColor= $statusYellowColor;
                    $TotalcurrentYearRow ='<td style="text-align : right;'.$statusYellowColor.'">'.sprintf($fontTag, number_format(round($currRowTotal, 1), 1, ".", ",").'%').'</td>';
                }
                else
                {
                    $TotalcurrentYearRow ='<td style="text-align : right;">'.sprintf($fontTag, number_format(round($currRowTotal, 1), 1, ".", ",").'%').'</td>';
                }
            }

            if($lastYearMonthCount!=12)
            {
                $TotallastYearRow ='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag, number_format(round($lastRowTotal, 1), 1, ".", ",").'%').'</td>';
            }
            else
            {
                if($lastRowStatusFlag==0)
                {
                    $TotallastYearRow ='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag, number_format(round($lastRowTotal, 1), 1, ".", ",").'%').'</td>';

                }
                else
                {
                    $TotallastYearRow ='<td style="text-align : right;">'.sprintf($fontTag, number_format(round($lastRowTotal, 1), 1, ".", ",").'%').'</td>';
                }
            }

            $TotalcompRow ='<td style="text-align : right;'.$compRowColor.'">'.sprintf($fontTag, number_format(round($compRowTotal,1),1,".",",").'%').'</td>';
            $currentRowHeader .= $TotalcurrentYearRow;
            $lastRowHeader .= $TotallastYearRow;
            $compRowHeader .= $TotalcompRow;
            $dateTable   .= $currentRowHeader.'</tr>'.$lastRowHeader.'</tr>'.$compRowHeader.'</tr>';
        }

        $totalLastRowHeader = '<tr style="text-align : right;">'
                                 .'<td style="text-align : center;">'.sprintf($fontTag, $lastYear.'年度').'</td>';
        $totalCurrentRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="3">'.sprintf($fontTag, 'AMG平均').'</td>'
                                    .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

        $totalCompRowHeader = '<tr style="text-align : right;">'
                                 .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

/////////////////
        $finalCurrTotal = 0;
        $finalLastTotal = 0;
        $finalCompTotal = 0;
        $finalCurrMonthCount = 0;
        $finalLastMonthCount = 0;
        $finalCurrStatusFlag = 1;
        $finalLastStatusFlag = 1;



        for ($i=0; $i < 12; $i++)
        {
            $monthCol       = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $lastCol        = (($startMonth + $i) <= 12)? $lastYear        : $lastYear + 1;
            $currentCol     = (($startMonth + $i) <= 12)? $currentYear     : $currentYear + 1;
            $currentYearCol = "";
            $lastYearCol    = "";
            $compCol        = "";

            $currColValue = 0 ;
            $lastColValue = 0 ;
            $compColValue = 0 ;
            $currTotalHospitalCount=0;
            $lastTotalHospitalCount=0;
            $currStatusFlag=1;
            $lastStatusFlag=1;
            foreach ($plantArr as $plantKey => $plantVal)
            {
                $facilityId = $plantVal['facility_id'];
                for($j=0;$j<count($dataArr);$j++)
                {
                    if ($facilityId == $dataArr[$j]['facility_id'] && $monthCol == $dataArr[$j]['month'])
                    {
                        if($currentCol == $dataArr[$j]['year'])
                        {
                            $currColValue= $currColValue + round($dataArr[$j]['gokei'],1);
                            $finalCurrTotal = $finalCurrTotal + round($dataArr[$j]['gokei'],1);
                            $currTotalHospitalCount = $currTotalHospitalCount + 1;

                            if($monthCol<date("m"))
                            {
//                                $finalCurrMonthCount = $finalCurrMonthCount + 1;
                                if($dataArr[$j]['status']==0)
                                {
                                    $currStatusFlag=0;
                                    $finalCurrStatusFlag=0;
                                }
                            }
                            $finalCurrMonthCount = $finalCurrMonthCount + 1;
                        }
                        if($lastCol == $dataArr[$j]['year'])
                        {
                            $lastColValue= $lastColValue + round($dataArr[$j]['gokei'],1);
                            $finalLastTotal = $finalLastTotal + round($dataArr[$j]['gokei'],1);
                            $lastTotalHospitalCount = $lastTotalHospitalCount + 1;
                            $finalLastMonthCount = $finalLastMonthCount + 1;
                            if($dataArr[$j]['status']==0)
                            {
                                $lastStatusFlag=0;
                                $finalLastStatusFlag=0;
                            }
                        }

                    }
                }
            }
            if($currTotalHospitalCount>0)
            {
                $currColValue = $currColValue/ $currTotalHospitalCount;
            }
            else
            {
                $currColValue = 0;
            }

            if($lastTotalHospitalCount>0)
            {
                $lastColValue = $lastColValue/ $lastTotalHospitalCount;
            }
            else
            {
                $lastColValue = 0;
            }

            $compColValue     = $currColValue - $lastColValue;
            $diffLastRowColor = $compColor;
            if($currTotalHospitalCount!=count($plantArr))
            {
                $diffLastRowColor=$statusRedColor;
                if($monthCol>=date("m")||$currentCol>date("Y"))
                {
                    $diffLastRowColor = $compColor;
                    $currentYearCol .='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag,number_format(round($currColValue,1),1,".",",").'%').'</td>';
                }
                else
                {
                    $currentYearCol .='<td style="text-align : right;'.$statusRedColor.'">'.sprintf($fontTag,number_format(round($currColValue,1),1,".",",").'%').'</td>';
                }
            }
            else
            {
                if($currStatusFlag==0)
                {
                    $diffLastRowColor = $statusYellowColor;
                    $currentYearCol .='<td style="text-align : right;'.$statusYellowColor.'">'.sprintf($fontTag,number_format(round($currColValue,1),1,".",",").'%').'</td>';
                }
                else
                {
                    $currentYearCol .='<td style="text-align : right;">'.sprintf($fontTag,number_format(round($currColValue,1),1,".",",").'%').'</td>';
                }
            }

            if($lastTotalHospitalCount!=count($plantArr))
            {
                $lastYearCol .='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag,number_format(round($lastColValue,1),1,".",",").'%').'</td>';
            }
            else
            {
                if($lastStatusFlag==0)
                {
                    $lastYearCol .='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag,number_format(round($lastColValue,1),1,".",",").'%').'</td>';
                }
                else
                {
                    $lastYearCol .='<td style="text-align : right;">'.sprintf($fontTag,number_format(round($lastColValue,1),1,".",",").'%').'</td>';
                }
            }
            $compCol .='<td style="text-align : right;'.$diffLastRowColor.'">'.sprintf                ($fontTag, number_format(round($compColValue,1),1,".",",").'%').'</td>';
            $totalCurrentRowHeader .= $currentYearCol;
            $totalLastRowHeader .= $lastYearCol;
            $totalCompRowHeader .= $compCol;

        }
//echo "CURR ".$finalCurrTotal. " finalCurrMonthCount".$finalCurrMonthCount;
//echo "Last ".$finalLastTotal. " finalCurrMonthCount".$finalLastMonthCount;
//var_dump($finalCurrMonthCount);
        if($finalCurrMonthCount>0)
        {
            $finalCurrTotal= $finalCurrTotal / $finalCurrMonthCount;
        }
        else
        {
            $finalCurrTotal=0;
        }

        if($finalLastMonthCount>0)
        {
            $finalLastTotal= $finalLastTotal / $finalLastMonthCount;
        }
        else
        {
            $finalLastTotal=0;
        }
        $finalCompTotal=round($finalCurrTotal,1) - round($finalLastTotal,1);
        $finalCellColor=$compColor;
        if($finalCurrMonthCount!=(count($plantArr)*(date("m")-4)))
        {
            $finalCellColor=$currentAmgAvgRowAvgColColor;
            if ($finalCellColor == $statusWhiteColor) { $finalCellColor = $compColor; }
//            $finalCellColor=$statusRedColor;
            $finalCurrentYearCol ='<td style="text-align : right;'.$currentAmgAvgRowAvgColColor.'">'.sprintf($fontTag, number_format(round($finalCurrTotal,1),1,".",",").'%').'</td>';
//            $finalCurrentYearCol ='<td style="text-align : right;'.$statusRedColor.'">'.sprintf($fontTag, number_format(round($finalCurrTotal,1),1,".","").'%').'</td>';
        }
        else
        {
            if($finalCurrStatusFlag==0)
            {
                $finalCellColor=$statusYellowColor;
                $finalCurrentYearCol ='<td style="text-align : right;'.$statusYellowColor.'">'.sprintf($fontTag, number_format(round($finalCurrTotal,1),1,".",",").'%').'</td>';
            }
            else
            {
                $finalCurrentYearCol ='<td style="text-align : right;">'.sprintf($fontTag,                     number_format(round($finalCurrTotal,1),1,".",",").'%').'</td>';
            }
        }

        if($finalLastMonthCount!=(count($plantArr)*12))
        {
            $finalLastYearCol ='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag,                     number_format(round($finalLastTotal,1),1,".",",").'%').'</td>';
        }
        else
        {
            if($finalLastStatusFlag==0)
            {
                $finalLastYearCol ='<td style="text-align : right;'.$statusWhiteColor.'">'.sprintf($fontTag,                     number_format(round($finalLastTotal,1),1,".",",").'%').'</td>';
            }
            else
            {
                $finalLastYearCol ='<td style="text-align : right;">'.sprintf($fontTag,                     number_format(round($finalLastTotal,1),1,".",",").'%').'</td>';
            }
        }
        $finalCompCol ='<td style="text-align : right;'.$finalCellColor.'">'.sprintf                ($fontTag, number_format($finalCompTotal,1,".",",").'%').'</td>';
        $totalCurrentRowHeader .= $finalCurrentYearCol;
        $totalLastRowHeader .= $finalLastYearCol;
        $totalCompRowHeader .= $finalCompCol;

        $dateTable .= $totalCurrentRowHeader.'</tr>'.$totalLastRowHeader.'</tr>'.$totalCompRowHeader.'</tr>';
        $dateTable .= '</tbody></table>';
        return $dateTable;
    }


/// Excel
    function getReturnExcelDisplayData($dataArr, $plantArr, $arr)
    {
        //get the excel columns in array
        $excelColumns=$this->getExcelColumnNames();
$dept=2;
$lastColCount=12;
$startColChar=2;
$startColCount=2;



       $dateTable= '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';


      $thColor      = $this->getRgbColor('blue', 'background-color');  // 青


        $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

    $statusColorWhiteArr = array('name' => 'background-color', 'value' => '#FFF;');
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '0.0%');


    $statusWhiteColor = $this->getArrValueByKey('name', $statusColorWhiteArr). ':'. $this->getArrValueByKey('value', $statusColorWhiteArr);
    $statusYellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $statusRedColor = "";//$this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);


    $percentStyle = $this->getArrValueByKey('name', $msoNumberFormat_parcent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_parcent);

    $lastYear    = $this->getLastYear();
    $currentYear = $this->getCurrentYear();
    $startMonth  = $this->getFiscalYearStartMonth();

        $font = $this->getFont();
        if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
        $dateTable .= '<table border="1" class="list this_table" width="100%"><tbody>';

        $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        // header
        $tableHeader = '<tr><td style="'.$thColor.'" colspan="2" />';

        for ($i=0; $i < 12; $i++) {
            $dispMonth = (($startMonth + $i) <= 12)? $startMonth + $i : $startMonth + $i - 12;
            $tableHeader .= '<td style="text-align: center; white-space: nowrap;'. $thColor. '">'. sprintf($fontTag, $dispMonth. '月'). '</td>';
        }
        $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">病院平均</td>';

        $tableHeader .= '</tr>';
       $dateTable   .= $tableHeader;

    foreach ($plantArr as $plantKey => $plantVal)
    {
         $lastRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center;">'.sprintf($fontTag,                         $lastYear.'年度').'</td>';
              $currentRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="3">'.sprintf($fontTag, $plantVal['facility_name']).'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $compRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';
           $facilityId = $plantVal['facility_id'];
        $currRowTotal=0;
        $lastRowTotal=0;
        $compRowTotal=0;
        $currYearMonthCount=0;
        $lastYearMonthCount=0;
        $currStatusColorFlag=1;
        $lastStatusColorFlag=1;
        for ($i=0; $i < 12; $i++)
        {
                  $monthRow = (($startMonth + $i) <= 12)? $startMonth + $i :                     $startMonth + $i - 12;
                       $lastYRow = (($startMonth + $i) <= 12)? $lastYear        :                     $lastYear + 1;
                       $currentYRow = (($startMonth + $i) <= 12)? $currentYear     :                     $currentYear + 1;
            $currentYearRow="";
            $lastYearRow="";
            $compRow="";
            $currValue = 0 ;
            $lastValue = 0 ;
            $compValue = 0 ;
            $currYearRowColor = $statusWhiteColor;
            $lastYearRowColor = $statusWhiteColor;
             for($j=0;$j<count($dataArr);$j++)
            {

                if ($facilityId == $dataArr[$j]['facility_id'] &&
                $monthRow == $dataArr[$j]['month'])
                {
                    if($currentYRow == $dataArr[$j]['year'])
                    {
                        $currValue= $dataArr[$j]['gokei'];
                        $currRowTotal= $currRowTotal + $currValue;
                        if($dataArr[$j]['status']==0)
                        {
                            $currYearRowColor = $statusYellowColor;
                            $currStatusColorFlag = 0;
                        }
                        if($currValue==0)
                        {
                            $currYearRowColor = $statusRedColor;
                        }
                        $currYearMonthCount = $currYearMonthCount+1;
                    }
                    if($lastYRow == $dataArr[$j]['year'])
                    {
                        $lastValue= $dataArr[$j]['gokei'];
                        $lastRowTotal= $lastRowTotal + $lastValue;
                        if($dataArr[$j]['status']==0)
                        {
                            $lastYearRowColor = $statusYellowColor;
                            $lastStatusColorFlag = 0;
                        }
                        if($lastValue==0)
                        {
                            $lastYearRowColor = $statusRedColor;
                        }

                        $lastYearMonthCount = $lastYearMonthCount+1;
                    }

                }
            }
            if($currValue==0)
            {
                $currYearRowColor = $statusRedColor;
            }

            if($lastValue==0)
            {
                $lastYearRowColor = $statusRedColor;
            }

            $compValue = $currValue - $lastValue;
            $currentYearRow .='<td style="text-align : right;'.$currYearRowColor.' '.$percentStyle.'">'.sprintf($fontTag, round($currValue,1)/100).'</td>';
            $lastYearRow .='<td style="text-align : right;'.$lastYearRowColor.' '.$percentStyle.'">'.sprintf($fontTag, round($lastValue,1)/100).'</td>';
            $compRow .='<td style="text-align : right;'.$compColor.'">'.sprintf                ($fontTag,$this->getDiffCell($excelColumns, $dept, $i).'').'</td>';
            $currentRowHeader .= $currentYearRow;
            $lastRowHeader .= $lastYearRow;
            $compRowHeader .= $compRow;

        }
        $compRowTotal = $currRowTotal - $lastRowTotal;
        //echo " compRowTotal ".$compRowTotal." currRowTotal ".$currRowTotal." lastRowTotal ".$lastRowTotal;


//  '.$excelColumns[3].$start.':'.$excelColumns[3].$start+10.'
        if($currYearMonthCount>0)
        {
            if($currYearMonthCount!=12)
            {
                $TotalcurrentYearRow ='<td style="text-align : right; '.$statusRedColor.' '.$percentStyle.'">=(SUM('.$excelColumns[$startColChar].$startColCount.':'.$excelColumns[$startColChar+11].$startColCount.'))/'.$currYearMonthCount.'</td>';
            }
            else
            {
                if($currStatusColorFlag == 0)
                {
                    $TotalcurrentYearRow ='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.'">=(SUM('.$excelColumns[$startColChar].$startColCount.':'.$excelColumns[$startColChar+11].$startColCount.'))/'.$currYearMonthCount.'</td>';
                }
                else
                {
                    $TotalcurrentYearRow ='<td style="text-align : right;'.$percentStyle.'">=(SUM('.$excelColumns[$startColChar].$startColCount.':'.$excelColumns[$startColChar+11].$startColCount.'))/'.$currYearMonthCount.'</td>';
                }

            }
        }
        else
        {
            $TotalcurrentYearRow ='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
        }
$nextRow=$startColCount+1;
        if($lastYearMonthCount>0)
        {
            if($lastYearMonthCount!=12)
            {
                $TotallastYearRow ='<td style="text-align : right;'.$statusRedColor.' '.$percentStyle.' ">=(SUM('.$excelColumns[$startColChar].$nextRow.':'.$excelColumns[$startColChar+11].$nextRow.'))/'.$lastYearMonthCount.'</td>';
            }
            else
            {
                if($lastStatusColorFlag == 0)
                {
                    $TotallastYearRow ='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.'">=(SUM('.$excelColumns[$startColChar].$nextRow.':'.$excelColumns[$startColChar+11].$nextRow.'))/'.$lastYearMonthCount.'</td>';
                }
                else
                {
                    $TotallastYearRow ='<td style="text-align : right;'.$percentStyle.'">=(SUM('.$excelColumns[$startColChar].$nextRow.':'.$excelColumns[$startColChar+11].$nextRow.'))/'.$lastYearMonthCount.'</td>';
                }

            }
        }
        else
        {
            $TotallastYearRow ='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
        }
        $TotalcompRow ='<td style="text-align : right;'.$compColor.'">'.sprintf                ($fontTag, $this->getDiffCell($excelColumns, $dept, $lastColCount).'').'</td>';
        $currentRowHeader .= $TotalcurrentYearRow;
        $lastRowHeader .= $TotallastYearRow;
        $compRowHeader .= $TotalcompRow;
        $dateTable   .= $currentRowHeader.'</tr>'.$lastRowHeader.'</tr>'.$compRowHeader.'</tr>';

$dept=$dept+3;
$startColCount = $startColCount+3;

    }


$totalLastRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center;">'.sprintf($fontTag,                         $lastYear.'年度').'</td>';
              $totalCurrentRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'" rowspan="3">'.sprintf($fontTag, 'AMG平均').'</td>'
                                       .'<td style="text-align : center; white-space: nowrap;">'.sprintf($fontTag, $currentYear.'年度').'</td>';

                $totalCompRowHeader = '<tr style="text-align : right;">'
                                    .'<td style="text-align : center; white-space: nowrap;'.$compColor.'">'.sprintf($fontTag, '増減').'</td>';

/////////////////
        $finalCurrTotal=0;
        $finalLastTotal=0;
        $finalCompTotal=0;
        $finalCurrMonthCount=0;
        $finalLastMonthCount=0;
        $finalCurrStatusFlag = 1;
        $finalLastStatusFlag = 1;
        for ($i=0; $i < 12; $i++)
        {
                  $monthCol = (($startMonth + $i) <= 12)? $startMonth + $i :                     $startMonth + $i - 12;
                       $lastCol = (($startMonth + $i) <= 12)? $lastYear        :                     $lastYear + 1;
                       $currentCol = (($startMonth + $i) <= 12)? $currentYear     :                     $currentYear + 1;
            $currentYearCol="";
            $lastYearCol="";
            $compCol="";
            $currColValue = 0 ;
            $lastColValue = 0 ;
            $compColValue = 0 ;
            $currTotalHospitalCount = 0;
            $lastTotalHospitalCount = 0;
            $currStatusFlag=1;
            $lastStatusFlag=1;
            foreach ($plantArr as $plantKey => $plantVal)
            {
                   $facilityId = $plantVal['facility_id'];
                 for($j=0;$j<count($dataArr);$j++)
                {

                    if ($facilityId == $dataArr[$j]['facility_id'] && $monthCol == $dataArr[$j]['month'])
                    {
                        if($currentCol == $dataArr[$j]['year'])
                        {
                        $currColValue= $currColValue + $dataArr[$j]['gokei'];
                        $finalCurrTotal = $finalCurrTotal + $dataArr[$j]['gokei'];
                        $finalCurrMonthCount = $finalCurrMonthCount+1;
                        $currTotalHospitalCount = $currTotalHospitalCount + 1;
                            if($dataArr[$j]['status']==0)
                            {
                                $finalCurrStatusFlag = 0;
                                $currStatusFlag=0;
                            }
                        }
                        if($lastCol == $dataArr[$j]['year'])
                        {
                        $lastColValue= $lastColValue + $dataArr[$j]['gokei'];
                        $finalLastTotal = $finalLastTotal + $dataArr[$j]['gokei'];
                        $lastTotalHospitalCount = $lastTotalHospitalCount + 1;
                        $finalLastMonthCount = $finalLastMonthCount+1;
                            if($dataArr[$j]['status']==0)
                            {
                                $finalLastStatusFlag = 0;
                                $lastStatusFlag=0;
                            }

                        }

                    }
                }
            }
            $compColValue = $currColValue - $lastColValue;
            if($currTotalHospitalCount>0)
            {
                if($currTotalHospitalCount!=count($plantArr))
                {
                    $currentYearCol .='<td style="text-align : right;'.$statusRedColor.' '.$percentStyle.'">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 2,$currTotalHospitalCount)).'</td>';
                }
                else
                {
                    if($currStatusFlag==0)
                    {
                        $currentYearCol .='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.' ">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 2,$currTotalHospitalCount)).'</td>';
                    }
                    else
                    {
                        $currentYearCol .='<td style="text-align : right;'.$percentStyle.'">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 2,$currTotalHospitalCount)).'</td>';
                    }
                }
            }
            else
            {
                $currentYearCol .='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
            }
            if($lastTotalHospitalCount>0)
            {
                if($lastTotalHospitalCount!=count($plantArr))
                {
                    $lastYearCol .='<td style="text-align : right;'.$statusRedColor.' '.$percentStyle.' ">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 3,$lastTotalHospitalCount)).'</td>';
                }
                else
                {
                    if($lastStatusFlag==0)
                    {
                        $lastYearCol .='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.' ">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 3,$lastTotalHospitalCount)).'</td>';
                    }
                    else
                    {
                        $lastYearCol .='<td style="text-align : right;'.$percentStyle.'">'.sprintf($fontTag, $this->getTotalColCellFormula($excelColumns,$i+2,count($plantArr), 3,$lastTotalHospitalCount)).'</td>';
                    }
                }
            }
            else
            {
                $lastYearCol .='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
            }
            $compCol .='<td style="text-align : right;'.$compColor.'">'.sprintf                ($fontTag, $this->getDiffCell($excelColumns, $dept, $i).'').'</td>';
            $totalCurrentRowHeader .= $currentYearCol;
            $totalLastRowHeader .= $lastYearCol;
            $totalCompRowHeader .= $compCol;

        }
        $finalCompTotal=$finalCurrTotal - $finalLastTotal;
//echo "wrwer1".$finalCurrMonthCount;
if($finalCurrMonthCount>0)
        {
            if($finalCurrMonthCount!=(12*count($plantArr)))
            {
                $finalCurrentYearCol ='<td style="text-align : right;'.$statusRedColor.' '.$percentStyle.' ">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 2, $finalCurrMonthCount).'</td>';
            }
            else
            {
                if($finalCurrStatusFlag==0)
                {
                    $finalCurrentYearCol ='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.' ">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 2, $finalCurrMonthCount).'</td>';
                }
                else
                {
                    $finalCurrentYearCol ='<td style="text-align : right;'.$percentStyle.'">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 2, $finalCurrMonthCount).'</td>';
                }
            }
        }
        else
        {
            $finalCurrentYearCol ='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
        }
$nextColCount=$startColCount+1;
if($finalLastMonthCount>0)
        {
            if($finalLastMonthCount!=(12*count($plantArr)))
            {
                $finalLastYearCol ='<td style="text-align : right;'.$statusRedColor.' '.$percentStyle.' ">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 3, $finalLastMonthCount).'</td>';
            }
            else
            {
                if($finalLastStatusFlag==0)
                {
                    $finalLastYearCol ='<td style="text-align : right;'.$statusYellowColor.' '.$percentStyle.' ">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 3, $finalLastMonthCount).'</td>';
                }
                else
                {
                    $finalLastYearCol ='<td style="text-align : right;'.$percentStyle.'">'.$this->getFinalCellFormula($excelColumns,$startColChar,count($plantArr), 3, $finalLastMonthCount).'</td>';
                }
            }
        }
        else
        {
            $finalLastYearCol ='<td style="text-align : right;'.$statusRedColor.$percentStyle.'">0.0%</td>';
        }



        //$finalCurrentYearCol ='<td style="text-align : center;">'.sprintf($fontTag,                     $finalCurrTotal.'%').'</td>';
        //$finalLastYearCol ='<td style="text-align : center;">'.sprintf($fontTag,                         $finalLastTotal.'%').'</td>';


            $finalCompCol ='<td style="text-align : right;'.$compColor.'">'.sprintf                ($fontTag, $this->getDiffCell($excelColumns, $dept, $lastColCount).'').'</td>';
            $totalCurrentRowHeader .= $finalCurrentYearCol;
            $totalLastRowHeader .= $finalLastYearCol;
            $totalCompRowHeader .= $finalCompCol;


////////////////////












$dateTable   .= $totalCurrentRowHeader.'</tr>'.$totalLastRowHeader.'</tr>'.$totalCompRowHeader.'</tr>';


        $dateTable .= '</tbody></table>';
        return $dateTable;
    }


function getReturnListDisplayData($dataArr, $plantArr, $arr, $yearMonthArr)
{




    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');
    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $year  = $this->getArrValueByKey('year', $yearMonthArr);
    $month = $this->getArrValueByKey('month', $yearMonthArr);

    $currentMonth=date("m");
    $currentYear=date("Y");
    if($year < $currentYear)
    {
        $redColor="";
        $yellowColor ="";
    }
    else
    {
        if(($year == $currentYear) && ($month < $currentMonth ) )
        {

        }
        else
        {
            $redColor="";
            $yellowColor ="";
        }
    }
        $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
        $font = $this->getFont();
        if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
        $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';

       $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        // header
        $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'" rowspan="2">'.sprintf($fontTag, '病院名').'</td>';


        $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '返戻率').'</td>';
      $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '査定率').'</td>';
       $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '返戻査定率').'</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">'.sprintf($fontTag, '再請求率').'</br>'.sprintf($fontTag, '(労災・自賠・').'</br>'.sprintf($fontTag, '自費を除く)').'</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">'.sprintf($fontTag, '当月').'</br>'.sprintf($fontTag, '返戻合計点数').'</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">'.sprintf($fontTag, '再請求').'</br>'.sprintf($fontTag, '合計点数').'</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">'.sprintf($fontTag, '労災･自賠･自費').'</br>'.sprintf($fontTag, '点数').'</td>';


        $tableHeader .= '</tr><tr>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
$tableHeader .= '</tr>';
      $dateTable   .= $tableHeader;
$totalPointFactorRatio=0;
$totalAccountFactorRatio=0;
$totalPointAssessRatio=0;
$totalAccountAssessRatio=0;
$totalPointSumRatio=0;
$totalAccountSumRatio=0;
$totalPointFactor=0;
$totalRechargeRatio=0;
$totalPointRecharge=0;
$totalPointInsurance=0;


$dataCountPointFactorRatio=0;
$dataCountAccountFactorRatio=0;
$dataCountPointAssessRatio=0;
$dataCountAccountAssessRatio=0;
$dataCountPointSumRatio=0;
$dataCountAccountSumRatio=0;
$dataCountPointFactor=0;
$dataCountRechargeRatio=0;
$dataCountPointRecharge=0;
$dataCountPointInsurance=0;




$colorFlagPointFactorRatio=1;
$colorFlagAccountFactorRatio=1;
$colorFlagPointAssessRatio=1;
$colorFlagAccountAssessRatio=1;
$colorFlagPointSumRatio=1;
$colorFlagAccountSumRatio=1;
$colorFlagPointFactor=1;
$colorFlagRechargeRatio=1;
$colorFlagPointRecharge=1;
$colorFlagPointInsurance=1;

for($i=0;$i<count($dataArr);$i++)
    {
        $RowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, $dataArr[$i]['facility_name']).'</td>';

if($dataArr[$i]['point_factor_ratio']==NULL)
{
    $dataArr[$i]['point_factor_ratio']=0;
}
else
{
    $dataCountPointFactorRatio= $dataCountPointFactorRatio + 1;
}
if($dataArr[$i]['account_factor_ratio']==NULL)
{
    $dataArr[$i]['account_factor_ratio']=0;
}
else
{
    $dataCountAccountFactorRatio= $dataCountAccountFactorRatio + 1;
}
if($dataArr[$i]['point_assess_ratio']==NULL)
{
    $dataArr[$i]['point_assess_ratio']=0;
}
else
{
    $dataCountPointAssessRatio= $dataCountPointAssessRatio + 1;
}
if($dataArr[$i]['account_assess_ratio']==NULL)
{
    $dataArr[$i]['account_assess_ratio']=0;
}
else
{
    $dataCountAccountAssessRatio= $dataCountAccountAssessRatio + 1;
}
if($dataArr[$i]['point_sum_ratio']==NULL)
{
    $dataArr[$i]['point_sum_ratio']=0;
}
else
{
    $dataCountPointSumRatio= $dataCountPointSumRatio + 1;
}
if($dataArr[$i]['account_sum_ratio']==NULL)
{
    $dataArr[$i]['account_sum_ratio']=0;
}
else
{
    $dataCountAccountSumRatio= $dataCountAccountSumRatio + 1;
}
if($dataArr[$i]['recharge_ratio']==NULL)
{
    $dataArr[$i]['recharge_ratio']=0;
}
else
{
    $dataCountRechargeRatio= $dataCountRechargeRatio + 1;
}
if($dataArr[$i]['point_factor']==NULL)
{
    $dataArr[$i]['point_factor']=0;
}
else
{
    $dataCountPointFactor= $dataCountPointFactor + 1;
}
if($dataArr[$i]['point_recharge']==NULL)
{
    $dataArr[$i]['point_recharge']=0;
}
else
{
    $dataCountPointRecharge= $dataCountPointRecharge + 1;
}
if($dataArr[$i]['point_insurance']==NULL)
{
    $dataArr[$i]['point_insurance']=0;
}
else
{
    $dataCountPointInsurance= $dataCountPointInsurance + 1;
}
$totalPointFactorRatio=$totalPointFactorRatio+$dataArr[$i]['point_factor_ratio'];
$totalAccountFactorRatio=$totalAccountFactorRatio+$dataArr[$i]['account_factor_ratio'];
$totalPointAssessRatio=$totalPointAssessRatio+$dataArr[$i]['point_assess_ratio'];
$totalAccountAssessRatio=$totalAccountAssessRatio+$dataArr[$i]['account_assess_ratio'];
$totalPointSumRatio=$totalPointSumRatio+$dataArr[$i]['point_sum_ratio'];
$totalAccountSumRatio=$totalAccountSumRatio+$dataArr[$i]['account_sum_ratio'];
$totalPointFactor=$totalPointFactor+$dataArr[$i]['point_factor'];
$totalRechargeRatio=$totalRechargeRatio+$dataArr[$i]['recharge_ratio'];
$totalPointRecharge=$totalPointRecharge+$dataArr[$i]['point_recharge'];
$totalPointInsurance=$totalPointInsurance+$dataArr[$i]['point_insurance'];
if($dataArr[$i]['point_factor_ratio']==0)
{
    $rowData ='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointFactorRatio=0;
        $rowData ='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData ='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor_ratio'],1,".",",").'%').'</td>';
    }

}
if($dataArr[$i]['account_factor_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_factor_ratio'],1,".",",").'%').'</td>';

}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountFactorRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_factor_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['account_factor_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['point_assess_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_assess_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointAssessRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_assess_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_assess_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['account_assess_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_assess_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountAssessRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_assess_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['account_assess_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['point_sum_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_sum_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointSumRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_sum_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_sum_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['account_sum_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_sum_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountSumRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['account_sum_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['account_sum_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['recharge_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['recharge_ratio'],1,".",",").'%').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagRechargeRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['recharge_ratio'],1,".",",").'%').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['recharge_ratio'],1,".",",").'%').'</td>';
    }
}
if($dataArr[$i]['point_factor']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor'],1,".",",")).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointFactor=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor'],1,".",",")).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_factor'],1,".",",")).'</td>';
    }
}
if($dataArr[$i]['point_recharge']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_recharge'],1,".",",")).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointRecharge=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_recharge'],1,".",",")).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_recharge'],1,".",",")).'</td>';
    }
}
if($dataArr[$i]['point_insurance']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_insurance'],1,".",",")).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointInsurance=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[$i]['point_insurance'],1,".",",")).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[$i]['point_insurance'],1,".",",")).'</td>';
    }
}
$rowData .='</tr>';
        $dateTable .= $RowHeader.$rowData ;
}
$totalRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, 'AMG平均').'</td>';

$totalFactorRatio = $totalPointFactorRatio + $totalAccountFactorRatio;
$totalAssessRatio = $totalPointAssessRatio + $totalAccountAssessRatio;
$totalSumRatio = $totalPointSumRatio + $totalAccountSumRatio;
$totalRRatio = $totalRechargeRatio;
$totalPFactor = $totalPointFactor;
$totalPFactor = $totalPointFactor;
$totalPRechange = $totalPointRecharge;
$totalPInsurance = $totalPointInsurance;

if($dataCountPointFactorRatio>0)
{
    $totalPointFactorRatio = $totalPointFactorRatio / $dataCountPointFactorRatio;
}
else
{
    $totalPointFactorRatio = 0;
}
if($dataCountAccountFactorRatio>0)
{
    $totalAccountFactorRatio = $totalAccountFactorRatio / $dataCountAccountFactorRatio;
}
else
{
    $totalAccountFactorRatio = 0;
}
if($dataCountPointAssessRatio>0)
{
    $totalPointAssessRatio = $totalPointAssessRatio / $dataCountPointAssessRatio;
}
else
{
    $totalPointAssessRatio = 0;
}
if($dataCountAccountAssessRatio>0)
{
    $totalAccountAssessRatio = $totalAccountAssessRatio / $dataCountAccountAssessRatio;
}
else
{
    $totalAccountAssessRatio = 0;
}
if($dataCountPointSumRatio>0)
{
    $totalPointSumRatio = $totalPointSumRatio / $dataCountPointSumRatio;
}
else
{
    $totalPointSumRatio = 0;
}
if($dataCountAccountSumRatio>0)
{
    $totalAccountSumRatio = $totalAccountSumRatio / $dataCountAccountSumRatio;
}
else
{
    $totalAccountSumRatio = 0;
}
if($dataCountRechargeRatio>0)
{
    $totalRechargeRatio = $totalRechargeRatio / $dataCountRechargeRatio;
}
else
{
    $totalRechargeRatio = 0;
}
if($dataCountPointFactor>0)
{
    $totalPointFactor = $totalPointFactor / $dataCountPointFactor;
}
else
{
    $totalPointFactor = 0;
}
if($dataCountPointRecharge>0)
{
    $totalPointRecharge = $totalPointRecharge / $dataCountPointRecharge;
}
else
{
    $totalPointRecharge = 0;
}
if($dataCountPointInsurance>0)
{
    $totalPointInsurance = $totalPointInsurance / $dataCountPointInsurance;
}
else
{
    $totalPointInsurance = 0;
}
if($dataCountPointFactorRatio!=count($dataArr))
{
    $totalColData ='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointFactorRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagPointFactorRatio==0)
    {
        $totalColData ='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointFactorRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData ='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointFactorRatio,1),1,".","").'%').'</td>';
    }
}

if($dataCountAccountFactorRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalAccountFactorRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagAccountFactorRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalAccountFactorRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalAccountFactorRatio,1),1,".",",").'%').'</td>';
    }
}

if($dataCountPointAssessRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointAssessRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagPointAssessRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointAssessRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointAssessRatio,1),1,".",",").'%').'</td>';
    }
}

if($dataCountAccountAssessRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalAccountAssessRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagAccountAssessRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalAccountAssessRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalAccountAssessRatio,1),1,".",",").'%').'</td>';
    }
}

if($dataCountPointSumRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointSumRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagPointSumRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointSumRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointSumRatio,1),1,".",",").'%').'</td>';
    }
}

if($dataCountAccountSumRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalAccountSumRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagAccountSumRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalAccountSumRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalAccountSumRatio,1),1,".",",").'%').'</td>';
    }
}

if($dataCountRechargeRatio!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalRechargeRatio,1),1,".",",").'%').'</td>';
}
else
{
    if($colorFlagRechargeRatio==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalRechargeRatio,1),1,".",",").'%').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalRechargeRatio,1),1,".",",").'%').'</td>';
    }
}


if($dataCountPointFactor!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointFactor,1),1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointFactor==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointFactor,1),1,".",",").'').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointFactor,1),1,".",",").'').'</td>';
    }
}

if($dataCountPointRecharge!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointRecharge,1),1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointRecharge==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointRecharge,1),1,".",",").'').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointRecharge,1),1,".",",").'').'</td>';
    }
}

if($dataCountPointInsurance!=count($dataArr))
{
    $totalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalPointInsurance,1),1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointInsurance==0)
    {
        $totalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalPointInsurance,1),1,".",",").'').'</td>';
    }
    else
    {
        $totalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round($totalPointInsurance,1),1,".",",").'').'</td>';
    }
}

$totalColData .='</tr>';

 $dateTable .= $totalRowHeader.$totalColData;
$finalRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, '合計点数').'</td>';

/*
if($dataCountPointFactorRatio!=count($dataArr) || $dataCountAccountFactorRatio!=count($dataArr))
{
    $finalColData ='<td colspan="2" style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round($totalFactorRatio,1),1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointFactorRatio==0 || $colorFlagAccountFactorRatio==0)
    {
        $finalColData ='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round($totalFactorRatio,1),1,".",",").'').'</td>';
    }
    else
    {
        $finalColData ='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format(round($totalFactorRatio,1),1,".",",").'').'</td>';
    }
}
if($dataCountPointAssessRatio!=count($dataArr) || $dataCountAccountAssessRatio!=count($dataArr))
{
    $finalColData .='<td colspan="2" style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalAssessRatio,1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointAssessRatio==0 || $colorFlagAccountAssessRatio==0)
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalAssessRatio,1,".",",").'').'</td>';
    }
    else
    {
        $finalColData .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalAssessRatio,1,".",",").'').'</td>';
    }
}
if($dataCountPointSumRatio!=count($dataArr) || $dataCountAccountSumRatio!=count($dataArr))
{
    $finalColData .='<td colspan="2" style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalSumRatio,1,".",",").'').'</td>';
}
else
{
    if($colorFlagPointSumRatio==0 || $colorFlagAccountSumRatio==0)
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalSumRatio,1,".",",").'').'</td>';
    }
    else
    {
        $finalColData .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalSumRatio,1,".",",").'').'</td>';
    }
}
if($dataCountRechargeRatio!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalRRatio,1,".",",").'').'</td>';
}
else
{
    if($colorFlagRechargeRatio==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalRRatio,1,".",",").'').'</td>';

    }
    else
    {
        $finalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($totalRRatio,1,".",",").'').'</td>';
    }
}
*/

//2011/0516　返戻率・査定率・返戻査定率・再請求率の合計点数の表示は要りません
$finalColData .='<td colspan="7" style="text-align:right;"> </td>';

if($dataCountPointFactor!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalPFactor,1,".",",")).'</td>';
}
else
{
    if($colorFlagPointFactor==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalPFactor,1,".",",")).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($totalPFactor,1,".",",")).'</td>';
    }
}

if($dataCountPointRecharge!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalPRechange,1,".",",")).'</td>';
}
else
{
    if($colorFlagPointRecharge==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalPRechange,1,".",",")).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($totalPRechange,1,".",",")).'</td>';
    }
}

if($dataCountPointInsurance!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($totalPInsurance,1,".",",")).'</td>';
}
else
{
    if($colorFlagPointInsurance==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalPInsurance,1,".",",")).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;">'.sprintf($fontTag, number_format($totalPInsurance,1,".",",")).'</td>';
    }
}


$finalColData .='</tr>';

    $dateTable .=  $finalRowHeader. $finalColData;
        $dateTable .= '</tbody></table>';
        return $dateTable;

}

//  List Excel
function getReturnListExcelDisplayData($dataArr, $plantArr, $arr)
{
//get the excel columns in array
        $excelColumns=$this->getExcelColumnNames();
      $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
        $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
        $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = "";//$this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '0.0%');

    $percentStyle = $this->getArrValueByKey('name', $msoNumberFormat_parcent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_parcent);


$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '0.0');

    $generalStyle = $this->getArrValueByKey('name', $msoNumberFormat_general). ':'. $this->getArrValueByKey('value', $msoNumberFormat_general);


        $font = $this->getFont();
        if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
   $dateTable = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';

        $dateTable .= '<table border="1" class="list this_table" width="100%"><tbody>';

       $fontTag = $this->getFontTag($font);
        list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

        // header
        $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'" rowspan="2">病院名</td>';


        $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">返戻率</td>';
      $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">査定率</td>';
       $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">返戻査定率</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">再請求率</br> （労災・自賠・</br>自費を除く）</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">当月</br>返戻合計点数</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">再請求</br>合計点数</td>';
$tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" rowspan="2">労災･自賠･自費</br>点数</td>';


        $tableHeader .= '</tr><tr>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td>';
$tableHeader .=   '<td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td><td width="60px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td>';
$tableHeader .= '</tr>';
      $dateTable   .= $tableHeader;
$totalPointFactorRatio=0;
$totalAccountFactorRatio=0;
$totalPointAssessRatio=0;
$totalAccountAssessRatio=0;
$totalPointSumRatio=0;
$totalAccountSumRatio=0;
$totalPointFactor=0;
$totalRechargeRatio=0;
$totalPointRecharge=0;
$totalPointInsurance=0;


$dataCountPointFactorRatio=0;
$dataCountAccountFactorRatio=0;
$dataCountPointAssessRatio=0;
$dataCountAccountAssessRatio=0;
$dataCountPointSumRatio=0;
$dataCountAccountSumRatio=0;
$dataCountPointFactor=0;
$dataCountRechargeRatio=0;
$dataCountPointRecharge=0;
$dataCountPointInsurance=0;

$colorFlagPointFactorRatio=1;
$colorFlagAccountFactorRatio=1;
$colorFlagPointAssessRatio=1;
$colorFlagAccountAssessRatio=1;
$colorFlagPointSumRatio=1;
$colorFlagAccountSumRatio=1;
$colorFlagPointFactor=1;
$colorFlagRechargeRatio=1;
$colorFlagPointRecharge=1;
$colorFlagPointInsurance=1;

for($i=0;$i<count($dataArr);$i++)
    {
        $RowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, $dataArr[$i]['facility_name']).'</td>';

if($dataArr[$i]['point_factor_ratio']==NULL)
{
    $dataArr[$i]['point_factor_ratio']=0;
}
else
{
    $dataCountPointFactorRatio= $dataCountPointFactorRatio + 1;
}
if($dataArr[$i]['account_factor_ratio']==NULL)
{
    $dataArr[$i]['account_factor_ratio']=0;
}
else
{
    $dataCountAccountFactorRatio= $dataCountAccountFactorRatio + 1;
}
if($dataArr[$i]['point_assess_ratio']==NULL)
{
    $dataArr[$i]['point_assess_ratio']=0;
}
else
{
    $dataCountPointAssessRatio= $dataCountPointAssessRatio + 1;
}
if($dataArr[$i]['account_assess_ratio']==NULL)
{
    $dataArr[$i]['account_assess_ratio']=0;
}
else
{
    $dataCountAccountAssessRatio= $dataCountAccountAssessRatio + 1;
}
if($dataArr[$i]['point_sum_ratio']==NULL)
{
    $dataArr[$i]['point_sum_ratio']=0;
}
else
{
    $dataCountPointSumRatio= $dataCountPointSumRatio + 1;
}
if($dataArr[$i]['account_sum_ratio']==NULL)
{
    $dataArr[$i]['account_sum_ratio']=0;
}
else
{
    $dataCountAccountSumRatio= $dataCountAccountSumRatio + 1;
}
if($dataArr[$i]['recharge_ratio']==NULL)
{
    $dataArr[$i]['recharge_ratio']=0;
}
else
{
    $dataCountRechargeRatio= $dataCountRechargeRatio + 1;
}
if($dataArr[$i]['point_factor']==NULL)
{
    $dataArr[$i]['point_factor']=0;
}
else
{
    $dataCountPointFactor= $dataCountPointFactor + 1;
}
if($dataArr[$i]['point_recharge']==NULL)
{
    $dataArr[$i]['point_recharge']=0;
}
else
{
    $dataCountPointRecharge= $dataCountPointRecharge + 1;
}
if($dataArr[$i]['point_insurance']==NULL)
{
    $dataArr[$i]['point_insurance']=0;
}
else
{
    $dataCountPointInsurance= $dataCountPointInsurance + 1;
}
$totalPointFactorRatio=$totalPointFactorRatio+$dataArr[$i]['point_factor_ratio'];
$totalAccountFactorRatio=$totalAccountFactorRatio+$dataArr[$i]['account_factor_ratio'];
$totalPointAssessRatio=$totalPointAssessRatio+$dataArr[$i]['point_assess_ratio'];
$totalAccountAssessRatio=$totalAccountAssessRatio+$dataArr[$i]['account_assess_ratio'];
$totalPointSumRatio=$totalPointSumRatio+$dataArr[$i]['point_sum_ratio'];
$totalAccountSumRatio=$totalAccountSumRatio+$dataArr[$i]['account_sum_ratio'];
$totalPointFactor=$totalPointFactor+$dataArr[$i]['point_factor'];
$totalRechargeRatio=$totalRechargeRatio+$dataArr[$i]['recharge_ratio'];
$totalPointRecharge=$totalPointRecharge+$dataArr[$i]['point_recharge'];
$totalPointInsurance=$totalPointInsurance+$dataArr[$i]['point_insurance'];
if($dataArr[$i]['point_factor_ratio']==0)
{
    $rowData ='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_factor_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointFactorRatio=0;
        $rowData ='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_factor_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData ='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_factor_ratio'])/100).'').'</td>';
    }
}

if($dataArr[$i]['account_factor_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_factor_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountFactorRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_factor_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_factor_ratio'])/100).'').'</td>';
    }
}

if($dataArr[$i]['point_assess_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_assess_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointAssessRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_assess_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_assess_ratio'])/100).'').'</td>';
    }
}
if($dataArr[$i]['account_assess_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_assess_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountAssessRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_assess_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_assess_ratio'])/100).'').'</td>';
    }
}

if($dataArr[$i]['point_sum_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_sum_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointSumRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_sum_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['point_sum_ratio'])/100).'').'</td>';
    }
}

if($dataArr[$i]['account_sum_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_sum_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagAccountSumRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_sum_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['account_sum_ratio'])/100).'').'</td>';
    }
}

if($dataArr[$i]['recharge_ratio']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['recharge_ratio'])/100).'').'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagRechargeRatio=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['recharge_ratio'])/100).'').'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$percentStyle.'">'.sprintf($fontTag, (($dataArr[$i]['recharge_ratio'])/100).'').'</td>';
    }
}


if($dataArr[$i]['point_factor']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]['point_factor']).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointFactor=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_factor']).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right;'.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_factor']).'</td>';
    }
}

if($dataArr[$i]['point_recharge']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_recharge']).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointRecharge=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_recharge']).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right'.$generalStyle.';">'.sprintf($fontTag, $dataArr[$i]
['point_recharge']).'</td>';
    }
}

if($dataArr[$i]['point_insurance']==0)
{
    $rowData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_insurance']).'</td>';
}
else
{
    if($dataArr[$i]['status']==0)
    {
        $colorFlagPointInsurance=0;
        $rowData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.sprintf($fontTag, $dataArr[$i]
['point_insurance']).'</td>';
    }
    else
    {
        $rowData .='<td style="text-align:right'.$generalStyle.';">'.sprintf($fontTag, $dataArr[$i]
['point_insurance']).'</td>';
    }
}
$rowData .='</tr>';
        $dateTable .= $RowHeader.$rowData ;
}
$totalRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">AMG平均</td>';

$totalFactorRatio = $totalPointFactorRatio + $totalAccountFactorRatio;
$totalAssessRatio = $totalPointAssessRatio + $totalAccountAssessRatio;
$totalSumRatio = $totalPointSumRatio + $totalAccountSumRatio;
$totalRRatio = $totalRechargeRatio;
$totalPFactor = $totalPointFactor;
$totalPFactor = $totalPointFactor;
$totalPRechange = $totalPointRecharge;
$totalPInsurance = $totalPointInsurance;

if($dataCountPointFactorRatio>0)
{
    $totalPointFactorRatio = $totalPointFactorRatio / $dataCountPointFactorRatio;
}
if($dataCountAccountFactorRatio>0)
{
    $totalAccountFactorRatio = $totalAccountFactorRatio / $dataCountAccountFactorRatio;
}
if($dataCountPointAssessRatio>0)
{
    $totalPointAssessRatio = $totalPointAssessRatio / $dataCountPointAssessRatio;
}
if($dataCountAccountAssessRatio>0)
{
    $totalAccountAssessRatio = $totalAccountAssessRatio / $dataCountAccountAssessRatio;
}
if($dataCountPointSumRatio>0)
{
    $totalPointSumRatio = $totalPointSumRatio / $dataCountPointSumRatio;
}
if($dataCountAccountSumRatio>0)
{
    $totalAccountSumRatio = $totalAccountSumRatio / $dataCountAccountSumRatio;
}
if($dataCountRechargeRatio>0)
{
    $totalRechargeRatio = $totalRechargeRatio / $dataCountRechargeRatio;
}
if($dataCountPointFactor>0)
{
    $totalPointFactor = $totalPointFactor / $dataCountPointFactor;
}
if($dataCountPointRecharge>0)
{
    $totalPointRecharge = $totalPointRecharge / $dataCountPointRecharge;
}
if($dataCountPointInsurance>0)
{
    $totalPointInsurance = $totalPointInsurance / $dataCountPointInsurance;
}
if($dataCountPointFactorRatio > 0)
{
    if($dataCountPointFactorRatio!=count($dataArr))
    {
        $totalColData ='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,1,count($dataArr), 3,$dataCountPointFactorRatio).'</td>';
    }
    else
    {
        if($colorFlagPointFactorRatio==0)
        {
            $totalColData ='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,1,count($dataArr), 3,$dataCountPointFactorRatio).'</td>';
        }
        else
        {
            $totalColData ='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,1,count($dataArr), 3,$dataCountPointFactorRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountAccountFactorRatio > 0)
{
    if($dataCountAccountFactorRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,2,count($dataArr), 3,$dataCountAccountFactorRatio).'</td>';
    }
    else
    {
        if($colorFlagAccountFactorRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,2,count($dataArr), 3,$dataCountAccountFactorRatio).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,2,count($dataArr), 3,$dataCountAccountFactorRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountPointAssessRatio > 0)
{
    if($dataCountPointAssessRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,3,count($dataArr), 3,$dataCountPointAssessRatio).'</td>';
    }
    else
    {
        if($colorFlagPointAssessRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,3,count($dataArr), 3,$dataCountPointAssessRatio).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,3,count($dataArr), 3,$dataCountPointAssessRatio).'</td>';
        }
    }

}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountAccountAssessRatio > 0)
{
    if($dataCountAccountAssessRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,4,count($dataArr), 3,$dataCountAccountAssessRatio).'</td>';
    }
    else
    {
        if($colorFlagAccountAssessRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,4,count($dataArr), 3,$dataCountAccountAssessRatio).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,4,count($dataArr), 3,$dataCountAccountAssessRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountPointSumRatio > 0)
{
    if($dataCountPointSumRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,5,count($dataArr), 3,$dataCountPointSumRatio).'</td>';
    }
    else
    {
        if($colorFlagPointSumRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,5,count($dataArr), 3,$dataCountPointSumRatio).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,5,count($dataArr), 3,$dataCountPointSumRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountAccountSumRatio > 0)
{
    if($dataCountAccountSumRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,6,count($dataArr), 3,$dataCountAccountSumRatio).'</td>';
    }
    else
    {
        if($colorFlagAccountSumRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,6,count($dataArr), 3,$dataCountAccountSumRatio).'</td>';
        }
        else
        {

            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,6,count($dataArr), 3,$dataCountAccountSumRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountRechargeRatio > 0)
{
    if($dataCountRechargeRatio!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,7,count($dataArr), 3,$dataCountRechargeRatio).'</td>';
    }
    else
    {
        if($colorFlagRechargeRatio==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,7,count($dataArr), 3,$dataCountRechargeRatio).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$percentStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,7,count($dataArr), 3,$dataCountRechargeRatio).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$percentStyle.'">0.0%</td>';
}
if($dataCountPointFactor > 0)
{
    if($dataCountPointFactor!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.' ">'.$this->getTotalColCellFormulaList($excelColumns,8,count($dataArr), 3,$dataCountPointFactor).'</td>';
    }
    else
    {
        if($colorFlagPointFactor==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,8,count($dataArr), 3,$dataCountPointFactor).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,8,count($dataArr), 3,$dataCountPointFactor).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">0.0</td>';
}
if($dataCountPointRecharge > 0)
{
    if($dataCountPointRecharge!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,9,count($dataArr), 3,$dataCountPointRecharge).'</td>';
    }
    else
    {
        if($colorFlagPointRecharge==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,9,count($dataArr), 3,$dataCountPointRecharge).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,9,count($dataArr), 3,$dataCountPointRecharge).'</td>';
        }
    }
}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">0.0</td>';
}
if($dataCountPointInsurance > 0)
{
    if($dataCountPointInsurance!=count($dataArr))
    {
        $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,10,count($dataArr), 3,$dataCountPointInsurance).'</td>';
    }
    else
    {
        if($colorFlagPointInsurance==0)
        {
            $totalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,10,count($dataArr), 3,$dataCountPointInsurance).'</td>';
        }
        else
        {
            $totalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalColCellFormulaList($excelColumns,10,count($dataArr), 3,$dataCountPointInsurance).'</td>';
        }
    }

}
else
{
    $totalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">0.0</td>';
}

$totalColData .='</tr>';

 $dateTable .= $totalRowHeader.$totalColData;
$finalRowHeader = '<tr style="text-align : right;">'
                               .'<td style="text-align : center; white-space: nowrap;'.$thColor.'">合計点数</td>';


/*
if($dataCountPointFactorRatio!=count($dataArr) || $dataCountAccountFactorRatio!=count($dataArr))
{
    $finalColData ='<td colspan="2" style="text-align:right;'.$redColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,1,2,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointFactorRatio==0 || $colorFlagAccountFactorRatio==0)
    {
        $finalColData ='<td colspan="2" style="text-align:right;'.$yellowColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,1,2,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData ='<td colspan="2" style="text-align:right;'.$redColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,1,2,count($dataArr)-1,3).'</td>';
    }
}
if($dataCountPointAssessRatio!=count($dataArr) || $dataCountAccountAssessRatio!=count($dataArr))
{
    $finalColData .='<td colspan="2" style="text-align:right;'.$redColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,3,4,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointAssessRatio==0 || $colorFlagAccountAssessRatio==0)
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$yellowColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,3,4,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,3,4,count($dataArr)-1,3).'</td>';
    }
}
if($dataCountPointSumRatio!=count($dataArr) || $dataCountAccountSumRatio!=count($dataArr))
{
    $finalColData .='<td colspan="2" style="text-align:right;'.$redColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,5,6,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointSumRatio==0 || $colorFlagAccountSumRatio==0)
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$yellowColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,5,6,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td colspan="2" style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,5,6,count($dataArr)-1,3).'</td>';
    }
}
if($dataCountRechargeRatio!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,7,7,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagRechargeRatio==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.' '.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,7,7,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListPercent($excelColumns,7,7,count($dataArr)-1,3).'</td>';

    }
}
*/

//2011/0516　返戻率・査定率・返戻査定率・再請求率の合計点数の表示は要りません
$finalColData .='<td colspan="7" style="text-align:right;"> </td>';
		
		


if($dataCountPointFactor!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,8,8,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointFactor==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,8,8,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,8,8,count($dataArr)-1,3).'</td>';
    }
}

if($dataCountPointRecharge!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,9,9,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointRecharge==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,9,9,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,9,9,count($dataArr)-1,3).'</td>';
    }
}

if($dataCountPointInsurance!=count($dataArr))
{
    $finalColData .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,10,10,count($dataArr)-1,3).'</td>';
}
else
{
    if($colorFlagPointInsurance==0)
    {
        $finalColData .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,10,10,count($dataArr)-1,3).'</td>';
    }
    else
    {
        $finalColData .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTotalTwoColCellFormulaListGeneral($excelColumns,10,10,count($dataArr)-1,3).'</td>';
    }
}

$finalColData .='</tr>';

    $dateTable .=  $finalRowHeader. $finalColData;
        $dateTable .= '</tbody></table>';
        return $dateTable;

}

/// Return Analysis table 1

function getReturnAnalysisTable1DisplayData($dataArr, $lastRowCol1, $lastRowCol2, $plantArr, $arr,$yearMonthArr)
{

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $year  = $this->getArrValueByKey('year', $yearMonthArr);
    $month = $this->getArrValueByKey('month', $yearMonthArr);

    $currentMonth=date("m");
    $currentYear=date("Y");
    if($year < $currentYear)
    {
            $redColor="";
            $yellowColor ="";
    }
    else
    {
        if(($year == $currentYear) && ($month < $currentMonth ) )
        {

        }
        else
        {
            $redColor="";
            $yellowColor ="";
        }
    }
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header
    $tableHeader = '<tr><td  style="text-align:center; white-space: nowrap;'.$thColor.'" rowspan="2" colspan="2"></td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '返戻').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '査定').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">'.sprintf($fontTag, '合計').'</td>';
    $tableHeader .= '</tr><tr>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;

    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">'.sprintf($fontTag, '社保').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '入院').'</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[0]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[0]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[0]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[0]['status12']==0 || $dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[0]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[0]['status12']==0 || $dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col6'],0,".",",")).'</td>';
        }

    }
    $tableRow .='</tr>';


    $totalRow2 = $dataArr[1]['col1'] + $dataArr[1]['col2'] + $dataArr[1]['col3'] + $dataArr[1]['col4'];

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '(過誤)').'</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[1]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[1]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
        }

    }

    if($totalRow2==0)
    {
        //$tableRow .='<td colspan="2" style="text-align:right;'.$redColor.' ">'.sprintf($fontTag, number_format($totalRow2,0,".",",")).'</td>';
			$tableRow .='<td colspan="2" >'.'</td>';
	}
    else
    {
        if( $dataArr[1]['status12']==0 || $dataArr[1]['status34']==0)
        {
            //$tableRow .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalRow2,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
		}
        else
        {
            //$tableRow .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalRow2,0,".",",")).'</td>';
			$tableRow .='<td colspan="2" >'.'</td>';
		}

    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '外来').'</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[2]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[2]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[2]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[2]['status12']==0 || $dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[2]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[2]['status12']==0 || $dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col6'],0,".",",")).'</td>';
        }

    }
    $tableRow .='</tr>';

$totalRow4 = $dataArr[3]['col1'] + $dataArr[3]['col2'] + $dataArr[3]['col3'] + $dataArr[3]['col4'];


    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '(過誤)').'</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[3]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[3]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col4'],0,".",",")).'</td>';
        }

    }
    if($totalRow4==0)
        {
            //$tableRow .='<td colspan="2" style="text-align:right;'.$redColor.' ">'.sprintf($fontTag, number_format($totalRow4,0,".",",")).'</td>';
			$tableRow .='<td colspan="2" >'.'</td>';
		}
        else
        {
            if( $dataArr[1]['status12']==0 || $dataArr[1]['status34']==0)
            {
                //$tableRow .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalRow4,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}
            else
            {
                //$tableRow .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalRow4,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}

    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '計').'</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[4]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[4]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[4]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[4]['status12']==0 || $dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[4]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[4]['status12']==0 || $dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col6'],0,".",",")).'</td>';
        }

    }
    $tableRow .='</tr>';



    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">'.sprintf($fontTag, '国保').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '入院').'</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[5]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[5]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[5]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[5]['status12']==0 || $dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[5]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[5]['status12']==0 || $dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col6'],0,".",",")).'</td>';
        }

    }
    $tableRow .='</tr>';

$totalRow7 = $dataArr[6]['col1'] + $dataArr[6]['col2'] + $dataArr[6]['col3'] + $dataArr[6]['col4'];
    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '(過誤)').'</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[6]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[6]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
        }

    }
    if($totalRow7==0)
        {
            //$tableRow .='<td colspan="2" style="text-align:right;'.$redColor.' ">'.sprintf($fontTag, number_format($totalRow7,0,".",",")).'</td>';
			$tableRow .='<td colspan="2" >'.'</td>';
		}
        else
        {
            if( $dataArr[1]['status12']==0 || $dataArr[1]['status34']==0)
            {
                //$tableRow .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalRow7,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}
            else
            {
                //$tableRow .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalRow7,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}

    }    $tableRow .='</tr>';


    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '外来').'</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[7]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[7]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[7]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[7]['status12']==0 || $dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[7]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[7]['status12']==0 || $dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col6'],0,".",",")).'</td>';
        }

    }    $tableRow .='</tr>';

$totalRow9 = $dataArr[8]['col1'] + $dataArr[8]['col2'] + $dataArr[8]['col3'] + $dataArr[8]['col4'];

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '(過誤)').'</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[8]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[8]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col4'],0,".",",")).'</td>';
        }

    }
    if($totalRow9==0)
        {
            //$tableRow .='<td colspan="2" style="text-align:right;'.$redColor.' ">'.sprintf($fontTag, number_format($totalRow9,0,".",",")).'</td>';
			$tableRow .='<td colspan="2" >'.'</td>';
		}
        else
        {
            if( $dataArr[1]['status12']==0 || $dataArr[1]['status34']==0)
            {
                //$tableRow .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($totalRow9,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}
            else
            {
                //$tableRow .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($totalRow9,0,".",",")).'</td>';
				$tableRow .='<td colspan="2" >'.'</td>';
			}

    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '計').'</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[9]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[9]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[9]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[9]['status12']==0 || $dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[9]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[9]['status12']==0 || $dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col6'],0,".",",")).'</td>';
        }

    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" colspan="2">'.sprintf($fontTag, '合計').'</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }

    }
    if($dataArr[10]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
        }

    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }

    }
    if($dataArr[10]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
        }

    }
    if($dataArr[10]['col5']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col5'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[10]['status12']==0 || $dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col5'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col5'],0,".",",")).'</td>';
        }

    }
    if($dataArr[10]['col6']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col6'],0,".",",")).'</td>';
    }
    else
    {
        if( $dataArr[10]['status12']==0 || $dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col6'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col6'],0,".",",")).'</td>';
        }

    }    $tableRow .='</tr>';

$lastRowCol3 = $lastRowCol1[0]['sum1'] + $lastRowCol2[0]['sum2'];
    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" colspan="2">'.sprintf($fontTag, '合計金額').'</td>';
    if($lastRowCol1[0]['sum1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'" colspan="2">'.sprintf($fontTag, number_format($lastRowCol1[0]['sum1'],0,".",",")).'</td>';
    }
    else
    {
        if($lastRowCol1[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'" colspan="2">'.sprintf($fontTag, number_format($lastRowCol1[0]['sum1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;" colspan="2">'.sprintf($fontTag, number_format($lastRowCol1[0]['sum1'],0,".","")).'</td>';
        }
    }
    if($lastRowCol2[0]['sum2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'" colspan="2">'.sprintf($fontTag, number_format($lastRowCol2[0]['sum2'],0,".",",")).'</td>';
    }
    else
    {
        if($lastRowCol2[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'" colspan="2">'.sprintf($fontTag, number_format($lastRowCol2[0]['sum2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;" colspan="2">'.sprintf($fontTag, number_format($lastRowCol2[0]['sum2'],0,".",",")).'</td>';
        }
    }
    if($lastRowCol3==0)
        {
            $tableRow .='<td colspan="2" style="text-align:right;'.$redColor.' ">'.sprintf($fontTag, number_format($lastRowCol3,0,".",",")).'</td>';
        }
        else
        {
            if( $lastRowCol1[0]['status']==0 || $lastRowCol2[0]['status']==0)
            {
                $tableRow .='<td colspan="2" style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($lastRowCol3,0,".",",")).'</td>';
            }
            else
            {
                $tableRow .='<td colspan="2" style="text-align:right;">'.sprintf($fontTag, number_format($lastRowCol3,0,".",",")).'</td>';
            }

    }

    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

/// Return Analysis table 4

function getReturnAnalysisTable4DisplayData($dataArr, $plantArr, $arr, $yearMonthArr)
{

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $year  = $this->getArrValueByKey('year', $yearMonthArr);
    $month = $this->getArrValueByKey('month', $yearMonthArr);

    $currentMonth=date("m");
    $currentYear=date("Y");
    if($year < $currentYear)
    {
            $redColor="";
            $yellowColor ="";
    }
    else
    {
        if(($year == $currentYear) && ($month < $currentMonth ) )
        {

        }
        else
        {
            $redColor="";
            $yellowColor ="";
        }
    }

    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header
    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'"  colspan="2">'.sprintf($fontTag, '再請求分').'</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '日数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '金額・割合').'</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;

    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">'.sprintf($fontTag, '社保').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '入院').'</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[0]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[0]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '外来').'</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".","")).'</td>';
        }
    }
    if($dataArr[1]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[1]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '計').'</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[2]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[2]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '事務').'</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }
    }

$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }
    }

	if($dataArr[3]['col4']==0)
	{
			$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[3]['status']==0)
		{
				$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col4'],1,".",",")).'%</td>';
		}
		else
		{
				$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col4'],1,".",",")).'%</td>';
		}
	}
		

//$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '診療').'</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }
    }

	if($dataArr[4]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[4]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col4'],1,".",",")).'%</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col4'],1,".",",")).'%</td>';
		}
	}
		
		
    $tableRow .='</tr>';

    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">'.sprintf($fontTag, '国保').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '入院').'</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[5]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[5]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col4'],1,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '外来').'</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[6]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[6]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '計').'</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[7]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[7]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '事務').'</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }
    }
$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }
    }

//$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
		
	if($dataArr[8]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[8]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col4'],1,".",",")).'%</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col4'],1,".",",")).'%</td>';
		}
	}
		
		
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '診療').'</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }
    }

    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }
    }

		
	if($dataArr[9]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[9]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col4'],1,".",",")).'%</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col4'],1,".",",")).'%</td>';
		}
	}
		
		
    $tableRow .='</tr>';
/////

    $tableRow .='<tr>';
    $tableRow .='<td rowspan="5" style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '労災').'</br>'.sprintf($fontTag, '自賠').'</br>'.sprintf($fontTag, '自費').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '入院').'</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".","")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
				$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[10]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[10]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    //$tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >自賠</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '外来').'</td>';
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[11]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[11]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    //$tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >自費</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '計').'</td>';
    if($dataArr[12]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[12]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[12]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[12]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';

    $tableRow .='<tr>';
    //$tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" ></td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '事務').'</td>';
    if($dataArr[13]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
        }
    }

$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
    if($dataArr[13]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
        }
    }
//$tableRow .='<td style="text-align:center;" rowspan="2"></td>';
		
	if($dataArr[13]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[13]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col4'],1,".",",")).'%</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[13]['col4'],1,".",",")).'%</td>';
		}
	}

        $tableRow .='</tr>';

    $tableRow .='<tr>';
    //$tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" ></td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '診療').'</td>';
    if($dataArr[14]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[14]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[14]['col1'],0,".",",")).'</td>';
        }
    }

    if($dataArr[14]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[14]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[14]['col3'],0,".",",")).'</td>';
        }
    }

	if($dataArr[14]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col4'],1,".",",")).'%</td>';
	}
	else
	{
		if($dataArr[14]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[14]['col4'],1,".",",")).'%</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[14]['col4'],1,".",",")).'%</td>';
		}
	}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td colspan="2" style="text-align:center; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '合計').'</td>';
    if($dataArr[15]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[15]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[15]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col2'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col2'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[15]['col2'],0,".",",")).'</td>';
        }
    }
    if($dataArr[15]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[15]['col3'],0,".",",")).'</td>';
        }
    }
    if($dataArr[15]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col4'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[15]['col4'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[15]['col4'],0,".",",")).'</td>';
        }
    }        $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}


/// Return Analysis table 2

function getReturnAnalysisTable2DisplayData($dataArr, $plantArr, $arr,$yearMonthArr)
{

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $year  = $this->getArrValueByKey('year', $yearMonthArr);
    $month = $this->getArrValueByKey('month', $yearMonthArr);

    $currentMonth=date("m");
    $currentYear=date("Y");
    if($year < $currentYear)
    {
            $redColor="";
            $yellowColor ="";
    }
    else
    {
        if(($year == $currentYear) && ($month < $currentMonth ) )
        {

        }
        else
        {
            $redColor="";
            $yellowColor ="";
        }
    }


    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header
    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'"  colspan="2">'.sprintf($fontTag, '返戻要因').'</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '構成比').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '構成比').'</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;
    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="4">'.sprintf($fontTag, '事務的').'</td>';


    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '保険証関係誤り').'</td>';

if($dataArr[0]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[0]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[0]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">0.0%</td>';
}

if($dataArr[0]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[0]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '病名不備').'</td>';
if($dataArr[1]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[1]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[1]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[1]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[1]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '継続承認外疾病').'</td>';
if($dataArr[2]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[2]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[2]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[2]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[2]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.nsprintf($fontTag, umber_format($dataArr[2]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, 'その他').'</td>';
if($dataArr[3]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[3]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[3]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[3]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[3]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="9">'.sprintf($fontTag, '診療的').'</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '投薬').'</td>';
    if($dataArr[4]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[4]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1']> 0)
{
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[4]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[4]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[4]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '注射').'</td>';
if($dataArr[5]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[5]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[5]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[5]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[5]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '検査').'</td>';
if($dataArr[6]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[6]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[6]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[6]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[6]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '処置').'</td>';
if($dataArr[7]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[7]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[7]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[7]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[7]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '手術').'</td>';
if($dataArr[8]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[8]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[8]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[8]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[8]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '放射線').'</td>';
if($dataArr[9]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[9]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[9]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[9]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[9]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, 'リハビリ').'</td>';
    if($dataArr[10]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[10]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[10]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[10]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[10]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '指導').'</td>';
    if($dataArr[11]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[11]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[11]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[11]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[11]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[11]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[11]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[11]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[11]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[11]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, 'その他').'</td>';
    if($dataArr[12]['col1']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[12]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col1'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col1'] > 0)
{
    if($dataArr[12]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[12]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[12]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[12]['col1']/$dataArr[13]['col1'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

if($dataArr[12]['col3']==0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[12]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[12]['col3'],0,".",",")).'</td>';
    }
}

if($dataArr[13]['col3'] > 0)
{
    if($dataArr[12]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[12]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[12]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[12]['col3']/$dataArr[13]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }
}
else
{

    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}

    $tableRow .='</tr>';
/////

    $tableRow .='<tr>';
    $tableRow .='<td colspan="2" style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '合計').'</td>';
if($dataArr[13]['col1'] == 0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
}
else
{
    if($dataArr[13]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[13]['col1'],0,".",",")).'</td>';
    }

}
    $tableRow .='<td style="text-align:center;"></td>';
if($dataArr[13]['col3'] == 0)
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
}
else
{
    if($dataArr[13]['status']==0)
    {
        $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
    }
    else
    {
        $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[13]['col3'],0,".",",")).'</td>';
    }

}
    $tableRow .='<td style="text-align:center;"></td>';
    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

/// Return Analysis table 5

function getReturnAnalysisTable5DisplayData($dataArrA, $dataArrB, $plantArr, $arr)
{

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table  border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

$tableHeader1 = '<tr><td  width="20%" style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="2">'.sprintf($fontTag, '返戻要因').'</br>'.sprintf($fontTag, 'コメント').'</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >'.sprintf($fontTag, '社保').'</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >'.sprintf($fontTag, '国保').'</td></tr>';
if($dataArrA==false)
{
$dataArrA[0]['txtcommentsi'] = "</br>";
$dataArrA[0]['txtcommentni'] = "</br>";
}


    $tableRow1 ='<tr><td style="text-align:center;">'.sprintf($fontTag, $dataArrA[0]['txtcommentsi']).'</td>';
    $tableRow1 .='<td style="text-align:center;">'.sprintf($fontTag, $dataArrA[0]['txtcommentni']).'</td></tr>';



    $dateTable   .= $tableHeader1.$tableRow1;
    $dateTable .= '</tbody></table>';


if($dataArrB==false)
{
$dataArrB[0]['txtcommentsi'] = "</br>";
$dataArrB[0]['txtcommentni'] = "</br>";
}

    $dateTable .= '</br><table border="1" class="list this_table" width="100%"><tbody>';
$tableHeader2 = '<tr><td width="20%" style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="2">'.sprintf($fontTag, '査定内訳').'</br>'.sprintf($fontTag, 'コメント').'</td><td style="text-align:center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, '社保').'</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >'.sprintf($fontTag, '国保').'</td></tr>';



    $tableRow2 ='<tr><td style="text-align:center;">'.sprintf($fontTag, $dataArrB[0]['txtcommentsi']).'</td>';
    $tableRow2 .='<td style="text-align:center;">'.sprintf($fontTag, $dataArrB[0]['txtcommentni']).'</td></tr>';



    $dateTable   .= $tableHeader2.$tableRow2;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

/// Return Analysis table 3

function getReturnAnalysisTable3DisplayData($dataArr, $plantArr, $arr, $yearMonthArr)
{


    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);

    $year  = $this->getArrValueByKey('year', $yearMonthArr);
    $month = $this->getArrValueByKey('month', $yearMonthArr);

    $currentMonth=date("m");
    $currentYear=date("Y");
    if($year < $currentYear)
    {
            $redColor="";
            $yellowColor ="";
    }
    else
    {
        if(($year == $currentYear) && ($month < $currentMonth ) )
        {

        }
        else
        {
            $redColor="";
            $yellowColor ="";
        }
    }

    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header



    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'">'.sprintf($fontTag, '査定内訳').'</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '件数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '点数').'</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">'.sprintf($fontTag, '構成比').'</td>';

    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;

    $tableRow ='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '投薬').'</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[0]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[0]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '注射').'</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[1]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[1]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '検査').'</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[2]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[2]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '処置').'</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[3]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[3]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '手術').'</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[4]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[4]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '放射線').'</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[5]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[5]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, 'リハビリ').'</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[6]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[6]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '指導').'</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[7]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[7]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '食事').'</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[8]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[8]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '処方箋').'</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[9]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[9]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, 'その他').'</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col1'],0,".",",")).'</td>';
        }
    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[10]['col3'],0,".",",")).'</td>';
        }
    }
if($dataArr[11]['col3'] > 0)
{
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format(round(($dataArr[10]['col3']/$dataArr[11]['col3'])*100,1),1,".",",")).'%'.'</td>';
        }
    }

}
else
{
    $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, '0.0%').'</td>';
}
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >'.sprintf($fontTag, '合計').'</td>';
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col1'],0,".",",")).'</td>';
        }
    }


    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.'">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;">'.sprintf($fontTag, number_format($dataArr[11]['col3'],0,".",",")).'</td>';
        }
    }    $tableRow .='<td style="text-align:center;"></td>';
    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

    function getTotalColCellFormula($excelColumns,$startCol,$total, $nRowIndexFunction, $totalHospitalCount ){
        $strFormula="=(SUM(";
        for($i=0;$i<$total;$i++){
        if($i==0)
        {
            $nRowIndex = $nRowIndexFunction;
        }
        else
        {
            $nRowIndex =    $nRowIndex+3;
        }
               if($i==($total-1))
        {
            $strFormula.=$excelColumns[$startCol].$nRowIndex;
        }
        else
        {
                 $strFormula.=$excelColumns[$startCol].$nRowIndex;
                 $strFormula.="+";
        }

        }
if($totalHospitalCount>0)
{
        $strFormula.="))/".$totalHospitalCount;
}
else
{
        $strFormula.="))";
}
        return $strFormula;
    }
function getFinalCellFormula($excelColumns,$startCol,$total, $nRowIndexFunction, $finalHospitalCount ){
$nLastColChar=$startCol+11;
$nLastColIndex=$startCol+11;
        $strFormula="=(";
        for($i=0;$i<$total;$i++){
 $strFormula.="SUM(";
        if($i==0)
        {
            $nRowIndex = $nRowIndexFunction;
        }
        else
        {
            $nRowIndex =    $nRowIndex+3;
        }
               if($i==($total-1))
        {
            $strFormula.=$excelColumns[$startCol].$nRowIndex.":".$excelColumns[$nLastColChar].$nRowIndex.")";
        }
        else
        {
                 $strFormula.=$excelColumns[$startCol].$nRowIndex.":".$excelColumns[$nLastColChar].$nRowIndex.")";
                 $strFormula.="+";
        }

        }
if($finalHospitalCount>0)
{
        $strFormula.=")/".$finalHospitalCount;
}
else
{
        $strFormula.="))";
}
        return $strFormula;
    }


function getTotalColCellFormulaList($excelColumns,$startCol,$total, $nRowIndexFunction, $totalHospitalCount ){
        $strFormula="=(SUM(";
        for($i=0;$i<$total;$i++){
        if($i==0)
        {
            $nRowIndex = $nRowIndexFunction;
        }
        else
        {
            $nRowIndex =    $nRowIndex+1;
        }
               if($i==($total-1))
        {
            $strFormula.=$excelColumns[$startCol].$nRowIndex;
        }
        else
        {
                 $strFormula.=$excelColumns[$startCol].$nRowIndex;
                 $strFormula.="+";
        }

        }
if($totalHospitalCount>0)
{
        $strFormula.="))/".$totalHospitalCount;
}
else
{
        $strFormula.="))";
}        return $strFormula;
    }

function getTotalTwoColCellFormulaListPercent($excelColumns,$startCol,$endCol,$total, $nRowIndexFunction){
    $strFormula="=SUM(";
    $strFormula.=$excelColumns[$startCol].$nRowIndexFunction;
    $strFormula.=":";
    $nRowIndexFunction = $nRowIndexFunction + $total;
    $strFormula.=$excelColumns[$endCol].$nRowIndexFunction;
    $strFormula.=")*100".$totalHospitalCount;
    return $strFormula;
    }

function getTotalTwoColCellFormulaListGeneral($excelColumns,$startCol,$endCol,$total, $nRowIndexFunction){
    $strFormula="=SUM(";
    $strFormula.=$excelColumns[$startCol].$nRowIndexFunction;
    $strFormula.=":";
    $nRowIndexFunction = $nRowIndexFunction + $total;
    $strFormula.=$excelColumns[$endCol].$nRowIndexFunction;
    $strFormula.=")".$totalHospitalCount;
    return $strFormula;
    }

   function getSelectDisplayHospital($arr)
     {
        $displayHospital = $this->getArrValueByKey('display_hospital', $arr);
        $category    = $this->getArrValueByKey('category',     $arr);
        $subCategory = $this->getArrValueByKey('sub_category', $arr);
        $no          = $this->getArrValueByKey('no',           $arr);
        $type        = $this->getArrValueByKey('type',         $arr);
        $plant       = $this->getArrValueByKey('plant ',       $arr);

        $data = new IndicatorData(array('fname' => $this->fname, 'con' => $this->con));
        $selectData = $data->getHospitalList();
        $rtnStr = sprintf('<select id="display_hospital" name="display_hospital" onchange="selectHospital(this, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');" >', $category, $subCategory, $no, $type, $plant);

        foreach ($selectData as $val) {
            if ($displayHospital == $val['facility_id']) {
                $rtnStr .= sprintf('<option value="%s" selected="selected" >%s</option>', $val['facility_id'], $val['facility_name']);
            }
            else {
                $rtnStr .= sprintf('<option value="%s" >%s</option>', $val['facility_id'], $val['facility_name']);
            }
        }
        $rtnStr .= '</select>';

        return $rtnStr;
    }

function getReturnAnalysisTable1DisplayDataExcel($dataArr, $lastRowCol1, $lastRowCol2, $plantArr, $arr, $display_year, $display_month, $hspName, $dataPointAcc)
{

    $excelColumns=$this->getExcelColumnNames();
    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

	//$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '0');
		$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0');
		
    $generalStyle = $this->getArrValueByKey('name', $msoNumberFormat_general). ':'. $this->getArrValueByKey('value', $msoNumberFormat_general);


$statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor =""; //$this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }


/*$dateTable .= '<table  width="100%">';
$dateTable .= '<tr><td><table border="1" class="list this_table" >';
$dateTable .= '<tr><td align="right" >病院名 '.$hspName[0]['facility_name'].'<td align="right" >'.$display_year.'年'.$display_month.'月</td></tr>';
$dateTable .= '<tr><td align="right" >'.$display_month.'月分総件数</td><td align="right" >'.$dataPointAcc[0]['total_point'].'</td><td align="left" > 件</td></tr>';
$dateTable .= '<tr><td align="right" >総点数</td><td align="right" >'.$dataPointAcc[0]['total_account'].'</td><td align="left" >点</td></tr>';
$dateTable .= '</table></td></tr>';*/
		

      $dateTable = '<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">';
		//		$dateTable .='<table border="1" width="100%"><tr><td>病院名 '.$hspName[0]['facility_name'].'</td><td>'.$display_year.'年'.$display_month.'月</td></tr><tr><td>'.$display_month.'月分総件数</td><td>'.number_format($dataPointAcc[0]['total_account'],0,".",",").' 件</td></tr><tr><td>総点数</td><td>'.number_format($dataPointAcc[0]['total_point'],0,".",",").' 点</td></tr></table>';
		$dateTable .='<table border="1" width="100%"><tr><td>病院名： '.$hspName[0]['facility_name'].'</td><td>'.$display_year.'年'.$display_month.'月</td></tr><tr><td>'.substr(date("Ym",strtotime("-1 month" ,strtotime($display_year."/".intval($display_month)."/1"))), 4, 2).'月分総件数</td><td>'.number_format($dataPointAcc[0]['total_account'],0,".",",").' 件</td></tr><tr><td>総点数</td><td>'.number_format($dataPointAcc[0]['total_point'],0,".",",").' 点</td></tr></table>';
		$dateTable .= '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header
    $tableHeader = '<tr><td  style="text-align:center; white-space: nowrap;'.$thColor.'" rowspan="2" colspan="2"></td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">返戻</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">査定</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '" colspan="2">合計</td>';
    $tableHeader .= '</tr><tr>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .=   '<td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td><td width="45px" style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;
    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">社保</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >入院</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
    }
    if($dataArr[0]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
        }
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
    }
    else
    {
        if($dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
    }
    if($dataArr[0]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
    }
    else
    {
        if($dataArr[0]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
        }
    }
    if((($dataArr[0]['col1'])+($dataArr[0]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,6).'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0 || $dataArr[0]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,6).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,6).'</td>';
        }
    }
    if((($dataArr[0]['col2'])+($dataArr[0]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,6).'</td>';
    }
    else
    {
        if($dataArr[0]['status12']==0 || $dataArr[0]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,6).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,6).'</td>';
        }
    }

    $tableRow .='</tr>';


    $totalRow2 = $dataArr[1]['col1'] + $dataArr[1]['col2'] + $dataArr[1]['col3'] + $dataArr[1]['col4'];

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >(過誤)</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
    }
    else
    {
        if($dataArr[1]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
    }
    if($dataArr[1]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
    }
    else
    {
        if($dataArr[1]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
        }
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
    }
    else
    {
        if($dataArr[1]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
    }
    if($dataArr[1]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
    }
    else
    {
        if($dataArr[1]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
        }
    }
    if($totalRow2==0)
    {
        //$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,7,2,5).'</td>';
		$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
	}
    else
    {
        if($dataArr[1]['status12']==0 || $dataArr[1]['status34']==0)
        {
            //$tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,7,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
        }
        else
        {
            //$tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,7,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >外来</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[2]['col1'].'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
    }
    if($dataArr[2]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[2]['col2'].'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[2]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[2]['col2'].'</td>';
        }
    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[2]['col3'].'</td>';
    }
    else
    {
        if($dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
    }
    if($dataArr[2]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[2]['col4'].'</td>';
    }
    else
    {
        if($dataArr[2]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[2]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[2]['col4'].'</td>';
        }
    }
    if((($dataArr[2]['col1'])+($dataArr[2]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,8).'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0 || $dataArr[2]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,8).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,8).'</td>';
        }
    }
    if((($dataArr[2]['col2'])+($dataArr[2]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,8).'</td>';
    }
    else
    {
        if($dataArr[2]['status12']==0 || $dataArr[2]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,8).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,8).'</td>';
        }
    }
    $tableRow .='</tr>';

$totalRow4 = $dataArr[3]['col1'] + $dataArr[3]['col2'] + $dataArr[3]['col3'] + $dataArr[3]['col4'];


    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >(過誤)</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
    }
    else
    {
        if($dataArr[3]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
    }
    if($dataArr[3]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col2'].'</td>';
    }
    else
    {
        if($dataArr[3]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col2'].'</td>';
        }
    }
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
    }
    else
    {
        if($dataArr[3]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
    }
    if($dataArr[3]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col4'].'</td>';
    }
    else
    {
        if($dataArr[3]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col4'].'</td>';
        }
    }
    if($totalRow4==0)
    {
        //$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,9,2,5).'</td>';
		$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
	}
    else
    {
        if($dataArr[3]['status12']==0 || $dataArr[3]['status34']==0)
        {
            //$tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,9,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
        else
        {
            //$tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,9,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
    }
    $tableRow .='</tr>';



    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >計</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,2,3).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,2,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,2,3).'</td>';
        }
    }
    if($dataArr[4]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,3,3).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,3,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,3,3).'</td>';
        }
    }

    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,4,3).'</td>';
    }
    else
    {
        if($dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,4,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,4,3).'</td>';
        }
    }

    if($dataArr[4]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,5,3).'</td>';
    }
    else
    {
        if($dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,5,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,6,5,3).'</td>';
        }
    }
    if((($dataArr[4]['col1'])+($dataArr[4]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,10).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0 || $dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,10).'</td>';
        }
    }
        if((($dataArr[4]['col2'])+($dataArr[4]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,10).'</td>';
    }
    else
    {
        if($dataArr[4]['status12']==0 || $dataArr[4]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,10).'</td>';
        }
    }
    $tableRow .='</tr>';



    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">国保</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >入院</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
    }
    if($dataArr[5]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
        }
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
    }
    else
    {
        if($dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
    }
    if($dataArr[5]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
    }
    else
    {
        if($dataArr[5]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
        }
    }
    if((($dataArr[5]['col1'])+($dataArr[5]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,11).'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0 || $dataArr[5]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,11).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,11).'</td>';
        }
    }
    if((($dataArr[5]['col2'])+($dataArr[5]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,11).'</td>';
    }
    else
    {
        if($dataArr[5]['status12']==0 || $dataArr[5]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,11).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,11).'</td>';
        }
    }
    $tableRow .='</tr>';

$totalRow7 = $dataArr[6]['col1'] + $dataArr[6]['col2'] + $dataArr[6]['col3'] + $dataArr[6]['col4'];
    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >(過誤)</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
    }
    else
    {
        if($dataArr[6]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
    }
    if($dataArr[6]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
    }
    else
    {
        if($dataArr[6]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
        }
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
    }
    else
    {
        if($dataArr[6]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
    }
    if($dataArr[6]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
    }
    else
    {
        if($dataArr[6]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
        }
    }
    if($totalRow7==0)
    {
        //$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,12,2,5).'</td>';
		$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
	}
    else
    {
        if($dataArr[6]['status12']==0 || $dataArr[6]['status34']==0)
        {
            //$tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,12,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
        else
        {
            //$tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,12,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
    }    $tableRow .='</tr>';


    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >外来</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[7]['col1'].'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
    }
    if($dataArr[7]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[7]['col2'].'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[7]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[7]['col2'].'</td>';
        }
    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[7]['col3'].'</td>';
    }
    else
    {
        if($dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
    }
    if($dataArr[7]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[7]['col4'].'</td>';
    }
    else
    {
        if($dataArr[7]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[7]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[7]['col4'].'</td>';
        }
    }
    if((($dataArr[7]['col1'])+($dataArr[7]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,13).'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0 || $dataArr[7]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,13).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,13).'</td>';
        }
    }
    if((($dataArr[7]['col2'])+($dataArr[7]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,13).'</td>';
    }
    else
    {
        if($dataArr[7]['status12']==0 || $dataArr[7]['status34']==0 )
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,13).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,13).'</td>';
        }
    }    $tableRow .='</tr>';

$totalRow9 = $dataArr[8]['col1'] + $dataArr[8]['col2'] + $dataArr[8]['col3'] + $dataArr[8]['col4'];

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >(過誤)</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
    }
    else
    {
        if($dataArr[8]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
    }
    if($dataArr[8]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col2'].'</td>';
    }
    else
    {
        if($dataArr[8]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col2'].'</td>';
        }
    }
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
    }
    else
    {
        if($dataArr[8]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
    }
    if($dataArr[8]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col4'].'</td>';
    }
    else
    {
        if($dataArr[8]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col4'].'</td>';
        }
    }
    if($totalRow9==0)
    {
        //$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,14,2,5).'</td>';
		$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
	}
    else
    {
        if($dataArr[8]['status12']==0 || $dataArr[8]['status34']==0)
        {
            //$tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,14,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
        else
        {
            //$tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$this->getSumFormula($excelColumns,14,2,5).'</td>';
			$tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.'</td>';
		}
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >計</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,2,3).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,2,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,2,3).'</td>';
        }
    }
    if($dataArr[9]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,3,3).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,3,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,3,3).'</td>';
        }
    }

    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,4,3).'</td>';
    }
    else
    {
        if($dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,4,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,4,3).'</td>';
        }
    }

    if($dataArr[9]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,5,3).'</td>';
    }
    else
    {
        if($dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,5,3).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowSumFormula($excelColumns,11,5,3).'</td>';
        }
    }
    if((($dataArr[9]['col1'])+($dataArr[9]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,15).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0 || $dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,15).'</td>';
        }
    }
        if((($dataArr[9]['col2'])+($dataArr[9]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,15).'</td>';
    }
    else
    {
        if($dataArr[9]['status12']==0 || $dataArr[9]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,15).'</td>';
        }
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" colspan="2">合計</td>';

    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,2,5,10).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,2,5,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,2,5,10).'</td>';
        }
    }

    if($dataArr[10]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,3,5,10).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,3,5,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,3,5,10).'</td>';
        }
    }


    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,4,5,10).'</td>';
    }
    else
    {
        if($dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,4,5,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,4,5,10).'</td>';
        }
    }

    if($dataArr[10]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,5,5,10).'</td>';
    }
    else
    {
        if($dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,5,5,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,5,5,10).'</td>';
        }
    }
    if((($dataArr[10]['col1'])+($dataArr[10]['col3']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,16).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0 || $dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,16).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,2,2,16).'</td>';
        }
    }
    if((($dataArr[10]['col2'])+($dataArr[10]['col4']))==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,16).'</td>';
    }
    else
    {
        if($dataArr[10]['status12']==0 || $dataArr[10]['status34']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,16).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getRowTwoCellSum($excelColumns,3,2,16).'</td>';
        }
    }

    $tableRow .='</tr>';

$lastRowCol3 = $lastRowCol1[0]['sum1'] + $lastRowCol2[0]['sum2'];
    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" colspan="2">合計金額</td>';
    if($lastRowCol1[0]['sum1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$lastRowCol1[0]['sum1'].'</td>';
    }
    else
    {
        if($lastRowCol1[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$lastRowCol1[0]['sum1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$lastRowCol1[0]['sum1'].'</td>';
        }
    }
    if($lastRowCol2[0]['sum2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$lastRowCol2[0]['sum2'].'</td>';
    }
    else
    {
        if($lastRowCol2[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$lastRowCol2[0]['sum2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$lastRowCol2[0]['sum2'].'</td>';
        }
    }
    if($lastRowCol3==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'" colspan="2">'.$this->getRowTwoCellSum($excelColumns,2,2,17).'</td>';
    }
    else
    {
        if($lastRowCol1[0]['status']==0 || $lastRowCol2[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'" colspan="2">'.$this->getRowTwoCellSum($excelColumns,2,2,17).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'" colspan="2">'.$this->getRowTwoCellSum($excelColumns,2,2,17).'</td>';
        }

    }


    $tableRow .='</tr>';



    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

function getRowTwoCellSum($excelColumns, $startColIndex, $diffBetColIndex, $rowIndex)
{
    $endColIndex= $startColIndex+$diffBetColIndex;
    $strFormula="=sum(".$excelColumns[$startColIndex].$rowIndex."+".$excelColumns[$endColIndex].$rowIndex.")";
    return $strFormula;
}

function getTwoRowCellSum($excelColumns, $startColIndex, $diffBetRowIndex, $rowIndex)
{
    $endRowIndex= $rowIndex+$diffBetRowIndex;
    $strFormula="=sum(".$excelColumns[$startColIndex].$rowIndex."+".$excelColumns[$startColIndex].$endRowIndex.")";
    return $strFormula;
}

function getRowSumFormula($excelColumns, $currentRow,$startIndex,$totalColumns){
$lastRowIndex = $currentRow + $totalColumns;
        $strFormula="=sum(".$excelColumns[$startIndex].$currentRow.":".$excelColumns[$startIndex].$lastRowIndex.")";
        /*
        for($i=$startIndex;$i<=$totalColumns;$i++){
            if($strFormula==""){
                $strFormula.="=(".$excelColumns[$i].$currentRow;
            }else{
                $strFormula.="+".$excelColumns[$i].$currentRow;
            }
        }*/
        return $strFormula;
      }

function getReturnAnalysisTable4DisplayDataExcel($dataArr, $plantArr, $arr)
{
        $excelColumns=$this->getExcelColumnNames();


    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

//$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '0');
		$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0');
		
    $generalStyle = $this->getArrValueByKey('name', $msoNumberFormat_general). ':'. $this->getArrValueByKey('value', $msoNumberFormat_general);

    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = "";//$this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');


	$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '0.0%');
		
	$percentStyle = $this->getArrValueByKey('name', $msoNumberFormat_parcent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_parcent);
		
    $yellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="0"><tr><td></br></td></tr><tr><td></br></td></tr><tr><td></br></td></tr></table> ';

    $dateTable .= '</br><table border="1" class="list this_table" width="100%"><tbody>';

    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header

    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'"  colspan="2">再請求分</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">件数</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">日数</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">金額</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;
    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">社保</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >入院</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
    }
    if($dataArr[0]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col2'].'</td>';
        }
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
    }
    if($dataArr[0]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[0]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >外来</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
    }
    if($dataArr[1]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col2'].'</td>';
        }
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
    }
    if($dataArr[1]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[1]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >計</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,5).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,5).'</td>';
        }
    }

    if($dataArr[2]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,5).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,5).'</td>';
        }
    }

    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,5).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,5).'</td>';
        }
    }


    if($dataArr[2]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,5).'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,5).'</td>';
        }
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >事務</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
    }

    $tableRow .='<td style="text-align:center;" rowspan="2"></td>';

    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
    }

	if($dataArr[3]['col4']==0)
	{
			$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[3]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[3]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[3]['col4'] / 100).'</td>';
		}
		else
		{
				$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[3]['col4'] / 100).'</td>';
		}
	}
		
//    $tableRow .='<td style="text-align:center;" rowspan="2"></td>';


    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >診療</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[4]['col1'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
    }

    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[4]['col3'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
    }
	
	if($dataArr[4]['col4']==0)
	{
			$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[4]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[3]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[4]['col4'] / 100).'</td>';
		}
		else
		{
				$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[4]['col4'] / 100).'</td>';
		}
	}
	
        $tableRow .='</tr>';

    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="5">国保</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >入院</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
    }
    if($dataArr[5]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col2'].'</td>';
        }
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
    }
    if($dataArr[5]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[5]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >外来</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
    }
    if($dataArr[6]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col2'].'</td>';
        }
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
    }
    if($dataArr[6]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[6]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >計</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,10).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,10).'</td>';
        }
    }

    if($dataArr[7]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,10).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,10).'</td>';
        }
    }

    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,10).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,10).'</td>';
        }
    }


    if($dataArr[7]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,10).'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,10).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,10).'</td>';
        }
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >事務</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
    }

    $tableRow .='<td style="text-align:center;" rowspan="2"></td>';

    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
    }

	if($dataArr[8]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[8]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[8]['status']==0)
		{
				$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[8]['col4'] / 100).'</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[8]['col4'] / 100).'</td>';
		}
	}
		
//        $tableRow .='<td style="text-align:center;" rowspan="2"></td>';


    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >診療</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[9]['col1'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
    }

    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[9]['col3'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
    }

	if($dataArr[9]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[9]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[9]['status']==0)
		{
				$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[9]['col4'] / 100).'</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[9]['col4'] / 100).'</td>';
		}
	}
		
		$tableRow .='</tr>';
/////
    $tableRow .='<tr>';
    $tableRow .='<td rowspan="5" style="text-align:left; white-space: nowrap;'.$thColor.'" >労災</br>自賠</br>自費</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >入院</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[10]['col1'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
    }
    if($dataArr[10]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[10]['col2'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[10]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[10]['col2'].'</td>';
        }
    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[10]['col3'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
    }
    if($dataArr[10]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[10]['col4'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[10]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[10]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
    //$tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >自賠</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >外来</td>';
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[11]['col1'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
    }
    if($dataArr[11]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[11]['col2'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[11]['col2'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[11]['col2'].'</td>';
        }
    }
    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[11]['col3'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[11]['col4'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[11]['col4'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[11]['col4'].'</td>';
        }
    }    $tableRow .='</tr>';

    $tableRow .='<tr>';
//    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >自費</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >計</td>';
    if($dataArr[12]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,15).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,11,1,15).'</td>';
        }
    }

    if($dataArr[12]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,15).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,12,1,15).'</td>';
        }
    }

    if($dataArr[12]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,15).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,13,1,15).'</td>';
        }
    }


    if($dataArr[12]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,15).'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,15).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getTwoRowCellSum($excelColumns,14,1,15).'</td>';
        }
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
//    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" ></td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >事務</td>';
    if($dataArr[13]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[13]['col1'].'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[13]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[13]['col1'].'</td>';
        }
    }

    $tableRow .='<td style="text-align:center;" rowspan="2"></td>';

    if($dataArr[13]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[13]['col3'].'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[13]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[13]['col3'].'</td>';
        }
    }

	if($dataArr[13]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[13]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[13]['status']==0)
		{
			$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[13]['col4'] / 100).'</td>';
		}
		else
		{
				$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[13]['col4'] / 100).'</td>';
		}
	}

//        $tableRow .='<td style="text-align:center;" rowspan="2"></td>';

    $tableRow .='</tr>';

    $tableRow .='<tr>';
//    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" ></td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >診療</td>';
    if($dataArr[14]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[14]['col1'].'</td>';
    }
    else
    {
        if($dataArr[14]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[14]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[14]['col1'].'</td>';
        }
    }

    if($dataArr[14]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$dataArr[14]['col3'].'</td>';
    }
    else
    {
        if($dataArr[14]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$dataArr[14]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$dataArr[14]['col3'].'</td>';
        }
    }

	if($dataArr[14]['col4']==0)
	{
		$tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">'.($dataArr[14]['col4'] / 100).'</td>';
	}
	else
	{
		if($dataArr[14]['status']==0)
		{
				$tableRow .='<td style="text-align:right;'.$yellowColor.$percentStyle.'">'.($dataArr[14]['col4'] / 100).'</td>';
		}
		else
		{
			$tableRow .='<td style="text-align:right;'.$percentStyle.'">'.($dataArr[14]['col4'] / 100).'</td>';
		}
	}
		
$tableRow .='</tr>';



    $tableRow .='<tr>';
    $tableRow .='<td colspan="2" style="text-align:center; white-space: nowrap;'.$thColor.'" >合計</td>';

    if($dataArr[15]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,11,3,7,5).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,11,3,7,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,11,3,7,5).'</td>';
        }
    }

    if($dataArr[15]['col2']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,12,3,7,5).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,12,3,7,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,12,3,7,5).'</td>';
        }
    }

    if($dataArr[15]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,13,3,7,5).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,13,3,7,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,13,3,7,5).'</td>';
        }
    }
    if($dataArr[15]['col4']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,14,3,7,5).'</td>';
    }
    else
    {
        if($dataArr[15]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,14,3,7,5).'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalStyle.'">'.$this->getDiffRowsSameColSum($excelColumns,14,3,7,5).'</td>';
        }
    }    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

function getReturnAnalysisTable3DisplayDataExcel($dataArr, $plantArr, $arr)
{

        $excelColumns=$this->getExcelColumnNames();

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

//$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '0.0');
		$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0');
		
    $generalStyle = $this->getArrValueByKey('name', $msoNumberFormat_general). ':'. $this->getArrValueByKey('value', $msoNumberFormat_general);


//$msoNumberFormat_generalNoPercent = array('name' => 'mso-number-format', 'value' => '0');
		$msoNumberFormat_generalNoPercent = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0');
		
    $generalNoPercentStyle = $this->getArrValueByKey('name', $msoNumberFormat_generalNoPercent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_generalNoPercent);


    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = "";//$this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
//$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '0.0%');
		$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0%');
		
    $percentStyle = $this->getArrValueByKey('name', $msoNumberFormat_parcent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_parcent);
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header



    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'">査定内訳</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">件数
</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">構成比</td>';

    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;

    $tableRow ='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >投薬</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[0]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,39,50).'</td>';
        }
        else
        {
            if($dataArr[0]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,39,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,39,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >注射</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[1]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,40,50).'</td>';
        }
        else
        {
            if($dataArr[1]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,40,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,40,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >検査</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[2]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,41,50).'</td>';
        }
        else
        {
            if($dataArr[2]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,41,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,41,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >処置</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
    }
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[3]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,42,50).'</td>';
        }
        else
        {
            if($dataArr[3]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,42,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,42,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >手術</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
    }
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[4]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,43,50).'</td>';
        }
        else
        {
            if($dataArr[4]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,43,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,43,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >放射線</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[5]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,44,50).'</td>';
        }
        else
        {
            if($dataArr[5]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,44,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,44,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >リハビリ</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[6]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,45,50).'</td>';
        }
        else
        {
            if($dataArr[6]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,45,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,45,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >指導</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[7]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,46,50).'</td>';
        }
        else
        {
            if($dataArr[7]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,46,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,46,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >食事</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
    }
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[8]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,47,50).'</td>';
        }
        else
        {
            if($dataArr[8]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,47,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,47,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >処方箋</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
    }
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[9]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,48,50).'</td>';
        }
        else
        {
            if($dataArr[9]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,48,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,48,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >その他</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
    }
    if($dataArr[11]['col3']>0)
    {
        if($dataArr[10]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,49,50).'</td>';
        }
        else
        {
            if($dataArr[10]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,49,50).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,49,50).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >合計</td>';
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
    }
    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
    }
    $tableRow .='<td style="text-align:right;"></td>';
    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}


function getReturnAnalysisTable2DisplayDataExcel($dataArr, $plantArr, $arr)
{
        $excelColumns=$this->getExcelColumnNames();

    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));

//$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '0.0');
		$msoNumberFormat_general = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0');
		
//$msoNumberFormat_noPercentgeneral = array('name' => 'mso-number-format', 'value' => '0');
		$msoNumberFormat_noPercentgeneral = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0');
		
    $generalStyle = $this->getArrValueByKey('name', $msoNumberFormat_general). ':'. $this->getArrValueByKey('value', $msoNumberFormat_general);

    $generalNoPercentStyle = $this->getArrValueByKey('name', $msoNumberFormat_noPercentgeneral). ':'. $this->getArrValueByKey('value', $msoNumberFormat_noPercentgeneral);


    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = "";// $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = "";//$this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
//$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '0.0%');
		$msoNumberFormat_parcent = array('name' => 'mso-number-format', 'value' => '\#\,\#\#0\.0%');
		
    $percentStyle = $this->getArrValueByKey('name', $msoNumberFormat_parcent). ':'. $this->getArrValueByKey('value', $msoNumberFormat_parcent);

    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
    $dateTable = '<table border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);
      // header
    $tableHeader = '<tr><td style="text-align:center; white-space: nowrap;'.$thColor.'"  colspan="2">返戻要因</td>';

    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">件数
</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">構成比</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">点数</td>';
    $tableHeader .=   '<td style="text-align:center; white-space: nowrap;'. $thColor. '">構成比</td>';
    $tableHeader .= '</tr>';
    $dateTable   .= $tableHeader;
    $tableRow ='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="4">事務的</td>';





    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >保険証関係誤り</td>';
    if($dataArr[0]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[0]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[0]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,23,36).'</td>';
        }
        else
        {
            if($dataArr[0]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,23,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,23,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[0]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
    }
    else
    {
        if($dataArr[0]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[0]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[0]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,23,36).'</td>';
        }
        else
        {
            if($dataArr[0]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,23,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,23,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >病名不備</td>';
    if($dataArr[1]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[1]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[1]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,24,36).'</td>';
        }
        else
        {
            if($dataArr[1]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,24,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,24,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[1]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
    }
    else
    {
        if($dataArr[1]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[1]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[1]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,24,36).'</td>';
        }
        else
        {
            if($dataArr[1]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,24,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,24,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >継続承認外疾病</td>';
    if($dataArr[2]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[2]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[2]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,25,36).'</td>';
        }
        else
        {
            if($dataArr[2]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,25,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,25,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[2]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
    }
    else
    {
        if($dataArr[2]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[2]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[2]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,25,36).'</td>';
        }
        else
        {
            if($dataArr[2]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,25,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,25,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >その他</td>';
    if($dataArr[3]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[3]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[3]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,26,36).'</td>';
        }
        else
        {
            if($dataArr[3]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,26,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,26,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[3]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
    }
    else
    {
        if($dataArr[3]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[3]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[3]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,26,36).'</td>';
        }
        else
        {
            if($dataArr[3]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,26,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,26,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr><td style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="9">診療的</td>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >投薬</td>';
    if($dataArr[4]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[4]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[4]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,27,36).'</td>';
        }
        else
        {
            if($dataArr[4]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,27,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,27,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[4]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
    }
    else
    {
        if($dataArr[4]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[4]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[4]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,27,36).'</td>';
        }
        else
        {
            if($dataArr[4]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,27,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,27,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >注射</td>';
    if($dataArr[5]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[5]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[5]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,28,36).'</td>';
        }
        else
        {
            if($dataArr[5]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,28,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,28,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[5]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
    }
    else
    {
        if($dataArr[5]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[5]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[5]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,28,36).'</td>';
        }
        else
        {
            if($dataArr[5]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,28,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,28,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >検査</td>';
    if($dataArr[6]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[6]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[6]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,29,36).'</td>';
        }
        else
        {
            if($dataArr[6]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,29,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,29,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[6]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
    }
    else
    {
        if($dataArr[6]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[6]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[6]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,29,36).'</td>';
        }
        else
        {
            if($dataArr[6]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,29,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,29,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >処置</td>';
    if($dataArr[7]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[7]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[7]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,30,36).'</td>';
        }
        else
        {
            if($dataArr[7]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,30,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,30,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[7]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
    }
    else
    {
        if($dataArr[7]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[7]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[7]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,30,36).'</td>';
        }
        else
        {
            if($dataArr[7]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,30,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,30,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >手術</td>';
    if($dataArr[8]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[8]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[8]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,31,36).'</td>';
        }
        else
        {
            if($dataArr[8]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,31,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,31,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[8]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
    }
    else
    {
        if($dataArr[8]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[8]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[8]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,31,36).'</td>';
        }
        else
        {
            if($dataArr[8]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,31,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,31,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >放射線</td>';
    if($dataArr[9]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[9]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[9]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,32,36).'</td>';
        }
        else
        {
            if($dataArr[9]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,32,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,32,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[9]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
    }
    else
    {
        if($dataArr[9]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[9]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[9]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,32,36).'</td>';
        }
        else
        {
            if($dataArr[9]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,32,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,32,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >リハビリ</td>';
    if($dataArr[10]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[10]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[10]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,33,36).'</td>';
        }
        else
        {
            if($dataArr[10]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,33,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,33,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[10]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
    }
    else
    {
        if($dataArr[10]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[10]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[10]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,33,36).'</td>';
        }
        else
        {
            if($dataArr[10]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,33,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,33,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >指導</td>';
    if($dataArr[11]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[11]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[11]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,34,36).'</td>';
        }
        else
        {
            if($dataArr[11]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,34,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,34,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[11]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
    }
    else
    {
        if($dataArr[11]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[11]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[11]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,34,36).'</td>';
        }
        else
        {
            if($dataArr[11]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,34,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,34,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    $tableRow .='</tr>';

    $tableRow .='<tr>';
    $tableRow .='<td style="text-align:left; white-space: nowrap;'.$thColor.'" >その他</td>';
    if($dataArr[12]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[12]['col1'].'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[12]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[12]['col1'].'</td>';
        }
    }

    if($dataArr[13]['col1']>0)
    {
        if($dataArr[12]['col1']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,35,36).'</td>';
        }
        else
        {
            if($dataArr[12]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,35,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,2,2,35,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }
    if($dataArr[12]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[12]['col3'].'</td>';
    }
    else
    {
        if($dataArr[12]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[12]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[12]['col3'].'</td>';
        }
    }
    if($dataArr[13]['col3']>0)
    {
        if($dataArr[12]['col3']==0)
        {
            $tableRow .='<td style="text-align:right;'.$redColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,35,36).'</td>';
        }
        else
        {
            if($dataArr[12]['status']==0)
            {
                $tableRow .='<td style="text-align:right;'.$yellowColor.' '.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,35,36).'</td>';
            }
            else
            {
                $tableRow .='<td style="text-align:right;'.$percentStyle.'">'.$this->getDivByNumberPercent($excelColumns,4,4,35,36).'</td>';
            }
        }
    }
    else
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$percentStyle.'">0%</td>';
    }

    $tableRow .='</tr>';
/////

    $tableRow .='<tr>';
    $tableRow .='<td colspan="2" style="text-align:left; white-space: nowrap;'.$thColor.'" >合計</td>';
    if($dataArr[13]['col1']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[13]['col1'].'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[13]['col1'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[13]['col1'].'</td>';
        }
    }
    $tableRow .='<td style="text-align:center;"></td>';
    if($dataArr[13]['col3']==0)
    {
        $tableRow .='<td style="text-align:right;'.$redColor.$generalNoPercentStyle.'">'.$dataArr[13]['col3'].'</td>';
    }
    else
    {
        if($dataArr[13]['status']==0)
        {
            $tableRow .='<td style="text-align:right;'.$yellowColor.$generalNoPercentStyle.'">'.$dataArr[13]['col3'].'</td>';
        }
        else
        {
            $tableRow .='<td style="text-align:right;'.$generalNoPercentStyle.'">'.$dataArr[13]['col3'].'</td>';
        }
    }
    $tableRow .='<td style="text-align:right;"></td>';
    $tableRow .='</tr>';


    $dateTable   .= $tableRow;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

function getReturnAnalysisTable5DisplayDataExcel($dataArrA, $dataArrB, $plantArr, $arr)
{
        $excelColumns=$this->getExcelColumnNames();


    $thColor      = $this->getRgbColor('blue', 'background-color');  // 青
    $compColor    = $this->getRgbColor('green', 'background-color'); // 緑
    $compColorArr = array('name' => 'background-color', 'value' => $this->getRgbColor('green'));
    $statusColorRedArr = array('name' => 'background-color', 'value' => '#FFC8ED;');

    $redColor = $this->getArrValueByKey('name', $statusColorRedArr). ':'. $this->getArrValueByKey('value', $statusColorRedArr);
    $statusColorYellowArr = array('name' => 'background-color', 'value' => '#FFE6D7;');

    $yellowColor = $this->getArrValueByKey('name', $statusColorYellowArr). ':'. $this->getArrValueByKey('value', $statusColorYellowArr);
    $font = $this->getFont();
      if (!is_null($fontStyle) && is_array($fontStyle)) {
            $font = $fontStyle;
        }
$dateTable = '<table border="0"><tr><td></br></td></tr><tr><td></br></td></tr><tr><td></br></td></tr><tr><td></br></td></tr><tr><td></br></td></tr></table> ';
    $dateTable .= '<table  border="1" class="list this_table" width="100%"><tbody>';
    $fontTag = $this->getFontTag($font);
      list($fontTagLeft, $fontTagRight) = explode('%s', $fontTag);

$tableHeader1 = '<tr><td  width="20%" style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="2">返戻要因</br>コメント</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >社保</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >国保</td></tr>';
if($dataArrA==false)
{
$dataArrA[0]['txtcommentsi'] = "</br>";
$dataArrA[0]['txtcommentni'] = "</br>";
}


    $tableRow1 ='<tr><td style="text-align:center;">'.$dataArrA[0]['txtcommentsi'].'</td>';
    $tableRow1 .='<td style="text-align:center;">'.$dataArrA[0]['txtcommentni'].'</td></tr>';



    $dateTable   .= $tableHeader1.$tableRow1;
    $dateTable .= '</tbody></table>';


if($dataArrB==false)
{
$dataArrB[0]['txtcommentsi'] = "</br>";
$dataArrB[0]['txtcommentni'] = "</br>";
}

    $dateTable .= '</br><table border="1" class="list this_table" width="100%"><tbody>';
$tableHeader2 = '<tr><td width="20%" style="text-align:left; white-space: nowrap;'.$thColor.'" rowspan="2">査定内訳</br>コメント</td><td style="text-align:center; white-space: nowrap;'.$thColor.'">社保</td><td style="text-align:center; white-space: nowrap;'.$thColor.'"  >国保</td></tr>';



    $tableRow2 ='<tr><td style="text-align:center;">'.$dataArrB[0]['txtcommentsi'].'</td>';
    $tableRow2 .='<td style="text-align:center;">'.$dataArrB[0]['txtcommentni'].'</td></tr>';



    $dateTable   .= $tableHeader2.$tableRow2;
    $dateTable .= '</tbody></table>';
    return $dateTable;

}

function getDiffRowsSameColSum($excelColumns,$startCol,$total, $nRowIndexFunction, $diffBetRowCount ){
        $strFormula="=(SUM(";
        for($i=0;$i<$total;$i++){
        if($i==0)
        {
            $nRowIndex = $nRowIndexFunction;
        }
        else
        {
            $nRowIndex =    $nRowIndex+$diffBetRowCount;
        }
               if($i==($total-1))
        {
            $strFormula.=$excelColumns[$startCol].$nRowIndex;
        }
        else
        {
                 $strFormula.=$excelColumns[$startCol].$nRowIndex;
                 $strFormula.="+";
        }

        }

        $strFormula.="))";
        return $strFormula;
    }

function getDivByNumberPercent($excelColumns,$ColChar,$DivColChar, $RowIndex, $DivRowIndex){
                $strFormula="=(";
            $strFormula.=$excelColumns[$ColChar].$RowIndex;
            $strFormula.="/";
            $strFormula.=$excelColumns[$DivColChar].$DivRowIndex;

$strFormula.=")";
$strFormula.="*100/100";

        return $strFormula;
    }












}
