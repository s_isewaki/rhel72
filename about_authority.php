<?php
// ログインユーザの権限情報を取得
function get_authority($session, $fname) {
    require_once("about_postgres.php");
    $con = connect2db($fname);

    $sql = "select emp_id from session";
    $cond = "where session_id = '$session'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");

    return get_emp_authority($con, $emp_id, $fname);
}

// 指定された職員の権限情報を取得
function get_emp_authority($con, $emp_id, $fname) {
	$sql = "select * from authmst";
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return array(
		pg_fetch_result($sel, 0, "emp_webml_flg"),
		pg_fetch_result($sel, 0, "emp_bbs_flg"),
		pg_fetch_result($sel, 0, "emp_schd_flg"),
		pg_fetch_result($sel, 0, "emp_wkflw_flg"),
		pg_fetch_result($sel, 0, "emp_phone_flg"),
		pg_fetch_result($sel, 0, "emp_attdcd_flg"),
		pg_fetch_result($sel, 0, "emp_qa_flg"),
		pg_fetch_result($sel, 0, "emp_aprv_flg"),
		pg_fetch_result($sel, 0, "emp_pass_flg"),
		pg_fetch_result($sel, 0, "emp_adbk_flg"),
		pg_fetch_result($sel, 0, "emp_conf_flg"),
		pg_fetch_result($sel, 0, "emp_resv_flg"),
		pg_fetch_result($sel, 0, "emp_tmgd_flg"),
		pg_fetch_result($sel, 0, "emp_schdplc_flg"),
		pg_fetch_result($sel, 0, "emp_ward_flg"),
		pg_fetch_result($sel, 0, "emp_ptif_flg"),
		pg_fetch_result($sel, 0, "emp_master_flg"),
		pg_fetch_result($sel, 0, "emp_attditem_flg"),
		pg_fetch_result($sel, 0, "emp_empif_flg"),
		pg_fetch_result($sel, 0, "emp_reg_flg"),
		pg_fetch_result($sel, 0, "emp_config_flg"),
		pg_fetch_result($sel, 0, "emp_ward_reg_flg"),
		pg_fetch_result($sel, 0, "emp_ptreg_flg"),
		pg_fetch_result($sel, 0, "emp_dept_flg"),
		pg_fetch_result($sel, 0, "emp_news_flg"),
		pg_fetch_result($sel, 0, "emp_biz_flg"),
		pg_fetch_result($sel, 0, "emp_job_flg"),
		pg_fetch_result($sel, 0, "emp_status_flg"),
		pg_fetch_result($sel, 0, "emp_entity_flg"),
		pg_fetch_result($sel, 0, "emp_aprv_auth"),
		pg_fetch_result($sel, 0, "emp_work_flg"),
		pg_fetch_result($sel, 0, "emp_pjt_flg"),
		pg_fetch_result($sel, 0, "emp_lib_flg"),
		pg_fetch_result($sel, 0, "emp_inout_flg"),
		pg_fetch_result($sel, 0, "emp_dishist_flg"),
		pg_fetch_result($sel, 0, "emp_outreg_flg"),
		pg_fetch_result($sel, 0, "emp_disreg_flg"),
		pg_fetch_result($sel, 0, "emp_hspprf_flg"),
		pg_fetch_result($sel, 0, "emp_lcs_flg"),
		pg_fetch_result($sel, 0, "emp_bkup_flg"),
		pg_fetch_result($sel, 0, "emp_svr_flg"),
		pg_fetch_result($sel, 0, "emp_fcl_flg"),
		pg_fetch_result($sel, 0, "emp_wkadm_flg"),
		pg_fetch_result($sel, 0, "emp_cmt_flg"),
		pg_fetch_result($sel, 0, "emp_cmtadm_flg"),
		pg_fetch_result($sel, 0, "emp_memo_flg"),
		pg_fetch_result($sel, 0, "emp_newsuser_flg"),
		pg_fetch_result($sel, 0, "emp_inci_flg"),
		pg_fetch_result($sel, 0, "emp_rm_flg"),
		pg_fetch_result($sel, 0, "emp_ext_flg"),
		pg_fetch_result($sel, 0, "emp_extadm_flg"),
		pg_fetch_result($sel, 0, "emp_intram_flg"),
		pg_fetch_result($sel, 0, "emp_stat_flg"),
		pg_fetch_result($sel, 0, "emp_allot_flg"),
		pg_fetch_result($sel, 0, "emp_life_flg"),
		pg_fetch_result($sel, 0, "emp_intra_flg"),
		pg_fetch_result($sel, 0, "emp_ymstat_flg"),
		pg_fetch_result($sel, 0, "emp_med_flg"),
		pg_fetch_result($sel, 0, "emp_qaadm_flg"),
		pg_fetch_result($sel, 0, "emp_medadm_flg"),
		pg_fetch_result($sel, 0, "emp_link_flg"),
		pg_fetch_result($sel, 0, "emp_linkadm_flg"),
		pg_fetch_result($sel, 0, "emp_bbsadm_flg"),
		pg_fetch_result($sel, 0, "emp_libadm_flg"),
		pg_fetch_result($sel, 0, "emp_adbkadm_flg"),
		pg_fetch_result($sel, 0, "emp_search_flg"),
		pg_fetch_result($sel, 0, "emp_webmladm_flg"),
		pg_fetch_result($sel, 0, "emp_cas_flg"),
		pg_fetch_result($sel, 0, "emp_casadm_flg"),
		pg_fetch_result($sel, 0, "emp_shift_flg"),
		pg_fetch_result($sel, 0, "emp_shiftadm_flg"),
		pg_fetch_result($sel, 0, "emp_nlcs_flg"),
		pg_fetch_result($sel, 0, "emp_nlcsadm_flg"),
		pg_fetch_result($sel, 0, "emp_kview_flg"),
		pg_fetch_result($sel, 0, "emp_kviewadm_flg"),
		pg_fetch_result($sel, 0, "emp_manabu_flg"),
		pg_fetch_result($sel, 0, "emp_manabuadm_flg"),
		pg_fetch_result($sel, 0, "emp_ptadm_flg"),
		pg_fetch_result($sel, 0, "emp_fplus_flg"),
		pg_fetch_result($sel, 0, "emp_fplusadm_flg"),
		pg_fetch_result($sel, 0, "emp_jnl_flg"),
		pg_fetch_result($sel, 0, "emp_jnladm_flg"),
		pg_fetch_result($sel, 0, "emp_pjtadm_flg"),
		pg_fetch_result($sel, 0, "emp_amb_flg"),
		pg_fetch_result($sel, 0, "emp_ambadm_flg"),
		pg_fetch_result($sel, 0, "emp_jinji_flg"),
		pg_fetch_result($sel, 0, "emp_jinjiadm_flg"),
		pg_fetch_result($sel, 0, "emp_ladder_flg"),
		pg_fetch_result($sel, 0, "emp_ladderadm_flg"),
		pg_fetch_result($sel, 0, "emp_shift2_flg"),
		pg_fetch_result($sel, 0, "emp_shift2adm_flg"),
		pg_fetch_result($sel, 0, "emp_cauth_flg"),
		pg_fetch_result($sel, 0, "emp_accesslog_flg"),
		pg_fetch_result($sel, 0, "emp_career_flg"),
		pg_fetch_result($sel, 0, "emp_careeradm_flg"),
		pg_fetch_result($sel, 0, "emp_ccusr1_flg"),
		pg_fetch_result($sel, 0, "emp_ccadm1_flg")
	);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function check_authority($session,$auth,$fname){            //セッションとページ値をもとにアクセス権限をチェックする

/***********ページ値対応一覧**********************
$webml     = ウェブメール権限[0]
$bbs       = 掲示板権限[1]
$schd      = スケジュール権限[2]
$wkflw     = ワークフロー権限[3]
$phone     = 伝言メモ権限[4]
$attdcd    = 出勤表権限[5]
$qa        = Q&A権限[6]
$aprv      = 決裁・申請権限[7]
$pass      = パスワード変更権限[8]
$adbk      = アドレス帳権限[9]
$conf      = ネットカンファレンス権限[10]
$resv      = 設備予約権限[11]
$tmgd      = 院内行事登録権限[12]
$schdplc   = スケジュール管理[13]
$ward      = 病床管理権限[14]
$ptif      = 患者基本情報権限[15]
$master    = マスターメンテナンス権限[16]
$attditem  = 出勤表項目管理権限[17]
$empif     = 職員参照権限[18]
$reg       = 職員登録権限[19]
$config    = 環境設定権限[20]
$ward_reg  = 病棟登録権限[21]
$ptreg     = 患者登録権限[22]
$dept      = 組織登録権限[23]
$news      = お知らせ管理権限[24]
$biz       = 経営支援権限[25]
$job       = 職種登録権限[26]
$status    = 役職登録権限[27]
$entity    = 事業所登録権限[28] -> 診療科登録権限[28]
$aprv_auth = 決裁者権限[29]
$work      = タスク権限[30]
$pjt       = 委員会・WG権限[31]
$lib       = 文書管理[32]
$inout     = 入院来院管理[33]
$dishist   = 診断名[34]
$outreg    = 来院登録[35]
$disreg    = 診断名登録[36]
$hspprf    = 組織プロフィール[37]
$lcs       = ライセンス管理[38]
$bkup      = バックアップ[39]
$svr       = サーバー情報管理[40]
$fcl       = 施設・設備登録[41]
$wkadm     = 勤務管理[42]
$cmt       = コミュニティサイト[43]
$cmtadm    = コミュニティサイト管理[44]
$memo      = 備忘録[45]
$newsuser  = お知らせ[46]
$inci      = ファントルくん[47]
$rm        = リスクマネージャ[48]
$ext       = 内線電話帳[49]
$extadm    = 内線電話帳管理[50]
$intram    = イントラメニュー設定[51]
$stat      = 稼働状況統計設定[52]
$allot     = 当直・外来分担表設定[53]
$life      = 私たちの生活サポート設定[54]
$intra     = イントラネット[55]
$ymstat    = 月間・年間稼働状況統計参照[56]
$med       = メドレポート[57]
$qaadm     = Q&A管理[58]
$medadm    = メドレポート管理[59]
$link      = リンクライブラリ[60]
$linkadm   = リンクライブラリ管理[61]
$bbsadm    = 掲示板管理[62]
$libadm    = 文書管理管理者[63]
$adbkadm   = アドレス帳管理者[64]
$search    = 検索ちゃん[65]
$webmladm  = ウェブメール管理者[66]
$cas       = CAS[67]
$casadm    = CAS（管理者）[68]
$shift     = 勤務シフト作成[69]
$shiftadm  = 勤務シフト作成（管理者）[70]
$nlcs      = 看護観察記録[71]
$nlcsadm   = 看護観察記録（管理者）[72]
$kview     = カルテビューワー[73]
$kviewadm  = カルテビューワー（管理者）[74]
$manabu    = バリテス[75]
$manabuadm = バリテス（管理者）[76]
$ptadm     = 患者管理（管理者）[77]
$fplus     = ファントルくん＋[78]
$fplusadm  = ファントルくん＋（管理者）[79]
$jnl       = 日報・月報[80]
$jnladm    = 日報・月報（管理者）[81]
$pjtadm    = 委員会・WG（管理者）[82]
$amb       = 看護支援[83]
$ambadm    = 看護支援（管理者）[84]
$jinji     = 人事管理[85]
$jinjiadm  = 人事管理（管理者）[86]
$ladder    = クリニカルラダー[87]
$ladderadm = クリニカルラダー（管理者）[88]
$shift2    = 勤務表[89]
$shift2adm = 勤務表（管理者）[90]
$cauth     = 端末認証[91]
$accesslog = アクセスログ[92]
$career    = キャリア開発ラダー[93]
$careeradm = キャリア開発ラダー（管理者）[94]
$ccusr1    = スタッフ・ポートフォリオ（一般）[95]
$ccadm1    = スタッフ・ポートフォリオ（管理者）[96]


*************************************************/

    $authorities = get_authority($session,$fname);          //権限をすべて取得

    $authority = $authorities[$auth];                           //アクセスされたページへのアクセス権限を取得
    if($authority == "f"){                                  //アクセス権限がなければログインページへ戻す
        return 0;
    }else{
        return 1;
    }
}
