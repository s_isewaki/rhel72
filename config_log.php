<?
require_once("about_session.php");
require_once("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');
require("config_log_list.php");

$fname = $PHP_SELF;

//ログファイルのディレクトリ指定
$dir_name = './log';

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}


// 権限のチェック
$checkauth = check_authority($session, 20, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}


$arr_disp_list = array();

//各ログファイルの情報を配列に格納
//各業務機能が追加されたら、下記の配列に追加するとログが出力設定される
$arr_disp_list["WEBMAIL"]["chkflg"]="";
$arr_disp_list["WEBMAIL"]["name"]="ウェブメール";

$arr_disp_list["SCHEDULE"]["chkflg"]="";
$arr_disp_list["SCHEDULE"]["name"]="スケジュール";

$arr_disp_list["PROJECT"]["chkflg"]="";
$arr_disp_list["PROJECT"]["name"]="委員会・ＷＧ";

$arr_disp_list["NEWS"]["chkflg"]="";
$arr_disp_list["NEWS"]["name"]="お知らせ・回覧板";

$arr_disp_list["TASK"]["chkflg"]="";
$arr_disp_list["TASK"]["name"]="タスク";

$arr_disp_list["MESSAGE"]["chkflg"]="";
$arr_disp_list["MESSAGE"]["name"]="伝言メモ";

$arr_disp_list["FACILITY"]["chkflg"]="";
$arr_disp_list["FACILITY"]["name"]="設備予約";

$arr_disp_list["TIMECARD"]["chkflg"]="";
$arr_disp_list["TIMECARD"]["name"]="出勤表";

$arr_disp_list["WORKFLOW"]["chkflg"]="";
$arr_disp_list["WORKFLOW"]["name"]="決裁・申請";

$arr_disp_list["NETCF"]["chkflg"]="";
$arr_disp_list["NETCF"]["name"]="ネットカンファレンス";

$arr_disp_list["BBS"]["chkflg"]="";
$arr_disp_list["BBS"]["name"]="掲示板・電子会議室";

$arr_disp_list["QA"]["chkflg"]="";
$arr_disp_list["QA"]["name"]="Ｑ＆Ａ";

$arr_disp_list["LIBRARY"]["chkflg"]="";
$arr_disp_list["LIBRARY"]["name"]="文書管理";

$arr_disp_list["ADDRESS"]["chkflg"]="";
$arr_disp_list["ADDRESS"]["name"]="アドレス帳";

$arr_disp_list["EXT"]["chkflg"]="";
$arr_disp_list["EXT"]["name"]="内線電話帳";

$arr_disp_list["LINK"]["chkflg"]="";
$arr_disp_list["LINK"]["name"]="リンクライブラリ";

$arr_disp_list["PASSWD"]["chkflg"]="";
$arr_disp_list["PASSWD"]["name"]="パスワード・本人情報";

//XX $arr_disp_list["COMM"]["chkflg"]="";
//XX $arr_disp_list["COMM"]["name"]="コミュニティサイト";

$arr_disp_list["MEMO"]["chkflg"]="";
$arr_disp_list["MEMO"]["name"]="備忘録";

$arr_disp_list["EVENT"]["chkflg"]="";
$arr_disp_list["EVENT"]["name"]="院内行事";

$arr_disp_list["EMPLOYEE"]["chkflg"]="";
$arr_disp_list["EMPLOYEE"]["name"]="職員登録";

$arr_disp_list["MASTER"]["chkflg"]="";
$arr_disp_list["MASTER"]["name"]="マスターメンテナンス";

$arr_disp_list["CONFIG"]["chkflg"]="";
$arr_disp_list["CONFIG"]["name"]="環境設定";

$arr_disp_list["LICENSE"]["chkflg"]="";
$arr_disp_list["LICENSE"]["name"]="ライセンス管理";

$arr_disp_list["PATIENT"]["chkflg"]="";
$arr_disp_list["PATIENT"]["name"]="患者管理";

$arr_disp_list["KENSAKU"]["chkflg"]="";
$arr_disp_list["KENSAKU"]["name"]="検索ちゃん";

$arr_disp_list["OUTPT"]["chkflg"]="";
$arr_disp_list["OUTPT"]["name"]="看護支援";

$arr_disp_list["BED"]["chkflg"]="";
$arr_disp_list["BED"]["name"]="病床管理";

$arr_disp_list["HPDEPT"]["chkflg"]="";
$arr_disp_list["HPDEPT"]["name"]="診療科";

$arr_disp_list["NURSE_REC"]["chkflg"]="";
$arr_disp_list["NURSE_REC"]["name"]="看護観察記録";

$arr_disp_list["FANTOL"]["chkflg"]="";
$arr_disp_list["FANTOL"]["name"]="ファントルくん";

$arr_disp_list["FPLUS"]["chkflg"]="";
$arr_disp_list["FPLUS"]["name"]="ファントルくん＋";

$arr_disp_list["E_LEARN"]["chkflg"]="";
$arr_disp_list["E_LEARN"]["name"]="バリテス";

$arr_disp_list["SUMMARY"]["chkflg"]="";
$arr_disp_list["SUMMARY"]["name"]="メドレポート";

$arr_disp_list["KVIEW"]["chkflg"]="";
$arr_disp_list["KVIEW"]["name"]="カルテビュー";

$arr_disp_list["BIZ"]["chkflg"]="";
$arr_disp_list["BIZ"]["name"]="経営支援";

$arr_disp_list["SHIFT"]["chkflg"]="";
$arr_disp_list["SHIFT"]["name"]="勤務シフト";

$arr_disp_list["CAS"]["chkflg"]="";
$arr_disp_list["CAS"]["name"]="ＣＡＳ";

$arr_disp_list["PERSONNEL"]["chkflg"]="";
$arr_disp_list["PERSONNEL"]["name"]="人事管理";

$arr_disp_list["JOURNAL"]["chkflg"]="";
$arr_disp_list["JOURNAL"]["name"]="日報・月報";

$arr_disp_list["INTRA"]["chkflg"]="";
$arr_disp_list["INTRA"]["name"]="イントラネット";

$arr_disp_list["SQL"]["chkflg"]="";
$arr_disp_list["SQL"]["name"]="データベース";

$arr_disp_list["COMMON"]["chkflg"]="";
$arr_disp_list["COMMON"]["name"]="共通";

$arr_disp_list["CONFIG"]["chkflg"]="";
$arr_disp_list["CONFIG"]["name"]="管理";


// ログ用system_config
$conf = new Cmx_SystemConfig();


if($log_upd_flg =="1")
{
    //プロパティファイルを更新する
    set_properties_file_active($arr_disp_list,$conf,$_POST);
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>CoMedix 環境設定｜その他</title>
<script type="text/javascript" src="js/fontsize.js"></script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>

<script type="text/javascript">

    ///-----------------------------------------------------------------------------
    //行／列ハイライト
    ///-----------------------------------------------------------------------------
    function fc_prop_upd()
    {
        document.mainform.log_upd_flg.value = "1";
        document.mainform.chc.value = 2;
        document.mainform.submit();
    }

</script>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing">
                        <a href="config_register.php?session=<? echo($session); ?>">
                            <img src="img/icon/b28.gif" width="32" height="32" border="0" alt="環境設定">
                        </a>
                    </td>
                    <td width="100%">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="config_register.php?session=<? echo($session); ?>"><b>環境設定</b></a>
                        </font>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr height="22">
                    <td width="110" align="center" bgcolor="#bdd1e7"><a href="config_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログイン画面</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_sidemenu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニュー</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_siteid.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイトID</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#5279a5"><a href="config_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>ログ</b></font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="110" align="center" bgcolor="#bdd1e7"><a href="config_server_information.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サーバ情報</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="120" align="center" bgcolor="#bdd1e7"><a href="config_client_auth.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">端末認証履歴</font></a></td>
                    <td width="5">&nbsp;</td>
                    <td width="90" align="center" bgcolor="#bdd1e7"><a href="config_other.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">その他</font></a></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
                </tr>
            </table>
            <form name="mainform" action="config_log.php" method="post">
                <img src="img/spacer.gif" width="1" height="20" alt=""><br>


                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <!-- left -->
                        <td width="160" valign="top">
                            <? log_menu_list($session, $chc) ?>
                        </td>
                        <!-- left -->
                        <!-- center -->
                        <td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
                        <!-- center -->
                        <!-- right -->
                        <td valign="top">

<?
if($chc == 2)
{
    //オプション設定画面

    //各プロパティファイルからログレベルの設定値を取得する
    check_properties_file_active($arr_disp_list,$conf);

?>
                            <table width="400" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="30">
                                    <td width="20%" align="left" bgcolor="#f6f9ff">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">システムログ詳細出力</font>
                                        <input type="button" value="設定" onclick="fc_prop_upd();">
                                    </td>
                                </tr>
<?
    foreach ($arr_disp_list as $dispKey => $dispValue)
    {
        $chk_lvl0 = "";
        $chk_lvl1 = "";
        $chk_lvl2 = "";

        if($dispValue["chkflg"] == "1")
        {
            $chk_lvl0 = "";
            $chk_lvl1 = "checked";
            $chk_lvl2 = "";

        }
        else if($dispValue["chkflg"] == "2")
        {
            $chk_lvl0 = "";
            $chk_lvl1 = "";
            $chk_lvl2 = "checked";
        }
        else
        {
            $chk_lvl0 = "checked";
            $chk_lvl1 = "";
            $chk_lvl2 = "";
        }


?>
                                <tr>
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<? echo($dispKey); ?>" id="<? echo($dispKey); ?>" value="0"  <? echo($chk_lvl0); ?> />標準</font>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<? echo($dispKey); ?>" id="<? echo($dispKey); ?>" value="1"  <? echo($chk_lvl1); ?> />詳細</font>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="<? echo($dispKey); ?>" id="<? echo($dispKey); ?>" value="2"  <? echo($chk_lvl2); ?> />緊急　　　<? echo($dispValue["name"]); ?></font>
                                    </td>
                                </tr>
<?
    }
?>
                            </table>
<?
}
else
{
    //ログ表示画面
?>
                            <table width="400" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="30">
                                    <td width="20%" align="right" bgcolor="#f6f9ff">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ログファイル</font>
                                    </td>
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    //ディレクトリ・ハンドルをオープン
    $res_dir = opendir($dir_name);

    $arr_file_list = array();

    //ディレクトリ内のファイル名を１つずつを取得
    while( $file_name = readdir( $res_dir ) )
    {
        if(($file_name == ".") || ($file_name == ".."))
        {
            //必要ないファイル名をスキップする
            continue;
        }

        if($chc == 1)
        {
            //システムログ
            //表示するログファイルのタイプをチェックする（日付がついていないものは表示しない）
            $chk_file_type = substr($file_name,0,8);
            if(!is_numeric($chk_file_type))
            {
                continue;
            }
        }
        else
        {
            //表示するログファイルのタイプをチェックする（日付付きは表示しない）
            $chk_file_type = substr($file_name,0,8);
            if(is_numeric($chk_file_type))
            {
                continue;
            }
        }

        //取得したファイルサイズを取得する
        $file_size=filesize($dir_name.'/'.$file_name);
        //$arr_file_list[$file_name] = $file_size;バイト表示
        //$arr_file_list[$file_name] = ceil($file_size/(1024*1024));メガバイト表示
        $arr_file_list[$file_name] = ceil($file_size/1024);
    }

    //画面遷移用URL付加情報
    if($chc == 1)
    {$str_chc = "&chc=1";}
    else
    {$str_chc = "";}

    ksort($arr_file_list);
    foreach ($arr_file_list as $key => $value)
    {
?>

    <a href="config_log.php?session=<?=$session?><?=$str_chc?>" onclick="window.open('config_log_download.php?session=<?=$session?>&f_n=<?=$key?>');"><?=$key?></a>
    <span>(<?=$value?>KBバイト)</span><br>
<?
    }
}
//ディレクトリ・ハンドルをクローズ
closedir( $res_dir );
?>
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="session" value="<? echo($session); ?>">
                <input type="hidden" name="log_upd_flg" value="">
                <input type="hidden" name="chc" value="">
            </form>
        </td>
    </tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?

// system_configからログレベルを取得し、ラジオボタンを判断する
function check_properties_file_active(&$arr_disp_list,$conf)
{
    foreach ($arr_disp_list as $dispKey => $dispValue)
    {
        $logLevel = $conf->get("l4p.".$dispKey);
        $arr_disp_list[$dispKey]["chkflg"] = $logLevel;
    }
}


// system_configのログレベル情報を更新する
//$arr_disp_listは連想配列名(業務名)を使用したいので引数としている。業務名以外で使用していない
function set_properties_file_active($arr_disp_list,$conf,$_POST)
{
    foreach ($arr_disp_list as $dispKey => $dispValue)
    {
        $logLevel = $conf->set("l4p.".$dispKey,$_POST[$dispKey]);
    }
}

?>