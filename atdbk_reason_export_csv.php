<?php
ob_start();
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");
require_once("settings_common.php");
require_once("timecard_hol_menu_common.php");
require_once("show_select_values.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$duty_type_flag = get_settings_value("employment_working_other_flg", "f");

// 出力するデータを取得する
$start_date = $_POST["start_yr"] . $_POST["start_mon"] . $_POST["start_day"];
$end_date = $_POST["end_yr"] . $_POST["end_mon"] . $_POST["end_day"];

$atdbkrslt_info = array();
$sql  = "select a.emp_id, a.reason, a.detail, a.date ";
$sql .= "from atdbkrslt a ";
$sql .= "INNER JOIN atdbk_reason_mst rn ON a.reason = rn.reason_id and rn.display_flag = 't' and rn.reason_id not in('24','35','36') ";
$sql .= "LEFT JOIN atdbk_reason_detail_mst dl ON a.reason = dl.reason_id and a.detail = dl.reason_detail_id ";
$sql .= "LEFT JOIN empmst b ON a.emp_id = b.emp_id ";
$sql .= "LEFT JOIN classmst c ON b.emp_class = c.class_id ";
$sql .= "LEFT JOIN atrbmst d ON b.emp_attribute = d.atrb_id ";
$sql .= "LEFT JOIN deptmst e ON b.emp_dept = e.dept_id ";
$cond  = "where EXISTS  (select e.emp_id from empmst e where a.emp_id = e.emp_id and e.emp_retire >= '$start_date' or e.emp_retire = '') ";
$cond .= "and a.date >= '$start_date' and a.date <= '$end_date' ";
$cond .= "order by c.order_no, d.order_no, e.order_no, b.emp_personal_id";

$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while($row = pg_fetch_array($sel)){
	$t_emp = $row["emp_id"];
	$t_reason = $row["reason"];
	$t_detail = $row["detail"];
	$t_date = $row["date"];
	
	$atdbkrslt_info[$t_emp][$t_reason]["date"][] = $t_date;
	
	if (!empty($t_detail)){
		$atdbkrslt_info[$t_emp][$t_reason][$t_detail][] = $t_date;
	}
}

// 出力職員ＩＤを取得
$arr_key = array();

// 事由マスタを取得する
$reason_info = array();
$sql  = "select reason_id, default_name, display_name, holiday_count from atdbk_reason_mst";
$cond = "where display_flag = 't' order by sequence";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while($row = pg_fetch_array($sel)){
	$t1_reason = $row["reason_id"];
	$t1_default = $row["default_name"]; 
	$t1_display = $row["display_name"]; 
	$t1_holiday = $row["holiday_count"];

	$reason_info[$t1_reason] = array("reason_id"=>$t1_reason 
									,"default_name"=>$t1_default
									, "display_name"=>$t1_display
									, "holiday_count"=>$t1_holiday
									);
}

// 事由の細目マスタを取得する
$sql = "select reason_id, reason_detail_id, reason_detail_name from atdbk_reason_detail_mst";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while($row = pg_fetch_array($sel)){
	$t2_reason = $row["reason_id"];
	$t2_id = $row["reason_detail_id"];
	$t2_name = $row["reason_detail_name"];
	
	if ($reason_info[$t2_reason]){
		$reason_info[$t2_reason]["detail"][$t2_id]= $t2_name;
	}
}

if(count($atdbkrslt_info) > 0){
	$arr_key = array_keys($atdbkrslt_info);
	$str_key = implode("','", $arr_key);
	
	// 職員ID情報を取得 array_keys($array)
	$emp_info = array();
	$sql  = "select f.emp_id, f.emp_personal_id, f.emp_lt_nm,f.emp_ft_nm,classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,";
	$sql .= "ec.duty_form, ec.duty_form_type, f.emp_sex,f.emp_birth,f.emp_join ";
	$sql .= "from  empmst f ";
	$sql .= "LEFT JOIN classmst ON f.emp_class = classmst.class_id ";
	$sql .= "LEFT JOIN atrbmst ON f.emp_attribute = atrbmst.atrb_id ";
	$sql .= "LEFT JOIN deptmst ON f.emp_dept = deptmst.dept_id ";
	$sql .= "LEFT JOIN empcond ec ON f.emp_id = ec.emp_id ";
	$cond  = "where f.emp_id in ('$str_key') ";
	$cond .= "and (f.emp_retire >= '$start_date' or f.emp_retire = '' or f.emp_retire is NULL) ";
	$cond .= "order by classmst.order_no, atrbmst.order_no, deptmst.order_no, f.emp_personal_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while($row = pg_fetch_array($sel)){
		$e_id       = $row["emp_id"];
		$e_personal = $row["emp_personal_id"];
		$e_lt       = $row["emp_lt_nm"];
		$e_ft       = $row["emp_ft_nm"];
		$e_class    = $row["class_nm"];
		$e_atrb     = $row["atrb_nm"];
		$e_dept     = $row["dept_nm"];
		$e_sex      = $row["emp_sex"];
		$e_birth    = trim($row["emp_birth"]);
		$e_join     = trim($row["emp_join"]);
		$e_birth    = (empty($e_birth)) ? "" : date("Y/m/d", strtotime($e_birth));
		$e_join     = (empty($e_join)) ? "" : date("Y/m/d", strtotime($e_join));
		
		switch($row["duty_form"]){
		case "1":
			$e_duty = "常勤";
			break;
		case "2":
			$e_duty = "非常勤";
			break;
		case "3":
			$e_duty = "短時間正職員";	
		}
		
		if ($duty_type_flag == "t"){
			switch($row["duty_form_type"]){
				case "1":
					$e_duty_type = "正職員";
					break;
				case "2":
					$e_duty_type = "再雇用職員";
					break;
				case "3":
					$e_duty_type = "嘱託職員";
					break;
				case "4":
					$e_duty_type = "臨時職員";
					break;
				case "5":
					$e_duty_type = "パート職員";
					break;
			}
		}
		
		switch($row["emp_sex"]){
		case "1":
			$e_sex = "男性";
			break;
		case "2":
			$e_sex = "女性";
			break;
		default:
			$e_sex = "不明";
		}
		
		
		if ($duty_type_flag == "t"){
			$emp_info[$e_id] = array($e_personal
									, $e_lt . " " . $e_ft
									, $e_class
									, $e_atrb
									, $e_dept
									, $e_duty
									, $e_duty_type
									, $e_sex
									, $e_birth
									, $e_join
									);
		}else{
			$emp_info[$e_id] = array($e_personal
									, $e_lt . " " . $e_ft
									, $e_class
									, $e_atrb
									, $e_dept
									, $e_duty
									, $e_sex
									, $e_birth
									, $e_join
									);
		}
	}
}	
// 情報をCSV形式で取得
$csv = get_list_csv($con, $session, $atdbkrslt_info, $reason_info, $emp_info, $duty_type_flag);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = "holiday_" . date("Ymd") . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

// 情報をCSV形式で取得
function get_list_csv($con, $session, $atdbkrslt_info, $reason_info, $emp_info, $duty_type_flag="f") {
	
	if ($duty_type_flag == "t"){
		$titles = array(
				"職員ID",
				"職員氏名",
				"組織1階層",
				"組織2階層",
				"組織3階層",					
				"雇用勤務形態",
				"雇用区分",
				"性別",			
				"生年月日",
				"採用年月日"					
		);
	}else{
		$titles = array(
				"職員ID",
				"職員氏名",
				"組織1階層",
				"組織2階層",
				"組織3階層",					
				"雇用勤務形態",
				"性別",			
				"生年月日",
				"採用年月日"					
		);
		
	}

	foreach ($reason_info as $reason){
		$reason_name = (empty($reason["display_name"])) ? $reason["default_name"] : $reason["display_name"];
		array_push($titles, $reason_name);
		
		foreach ($reason["detail"] as $detail){
			array_push($titles, $reason_name . ">" . $detail);
		}
	}
	
	foreach ($atdbkrslt_info as $key=>$rslt){
		if ($emp_info[$key]){
			foreach ($reason_info as $rn_key=>$reason){
				// 事由項目統計
				$emp_info[$key][] = ($rslt[$rn_key])? count($rslt[$rn_key]["date"]) * floatval($reason["holiday_count"]) : 0;
		
				// 事由の細目統計
				foreach ($reason["detail"] as $dl_key=>$detail){
					$emp_info[$key][] = ($rslt[$rn_key][$dl_key])? count(($rslt[$rn_key][$dl_key])) * floatval($reason["holiday_count"]) : 0;
				}
			}
		}
	}
	
	
	$item_num = count($titles);

	$buf = "";
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";

	foreach ($emp_info as $key=>$item){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $emp_info[$key][$j];
		}
		$buf .= "\r\n";
	}
	
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>
