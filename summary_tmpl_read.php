<?/*

画面パラメータ
	$session
		セッションID
	$diag
		診療区分名
		divmst.diag_div を検索し、テンプレートを取得する。
	$enc_diag
		urlエンコードされた診療区分名
		$diagが指定されなかった場合に$diagとして使用。
	$div_id
		enc_diagでの判定をやめる方向で、これがあればこちらを信用。
	$sansho_flg
		参照フラグ
		trueの場合に閉じるヘッダーや「次の画面へ」ボタン等が使用不可となる。
	$direct_registration
		直接登録フラグ
		trueの場合に一時格納ではなく、オーダを発行して正規データとしてしまう。温度板の結果登録用。
		また、表示時はテンプレート選択ドロップダウンを隠し、
		「次の画面へ」ボタンの代わりに「登録」ボタンを表示する。
	$tmpl_title
		「テンプレート」の代わりに表示する文字
	$xml_file
		指定された場合に、そのXMLの内容でテンプレートを初期表示する。
	$tmpl_copy_xml_file
		コピー画面から呼び出された場合のみ。コピー内容のXML。
	$tmpl_id
		テンプレートID
	$iframe
		iframe呼び出しの場合に呼び出し元のフレームサイズを
		動的変更するためにフレームオブジェクト名を指定する。
	$tmpl_copy_entry_info
		コピーにおけるフリー記載用のsummary_idCSV配列
	$is_copy
		コピーで呼び出された場合に"true"
	その他
		テンプレート処理PHPに転送するパラメータ


テンプレート処理PHP(例:create_tmpl_xml.php)の画面パラメータについて
	$session
		セッションID
	$pt_id
		患者ID
	$emp_id
		xmlファイル名作成時に使用。
	$summary_id
		テンプレート処理後、summary_update.phpへ呼び出す際にsummary_idとして転送
	$summary_seq
		emp_idに依存しないsummaryのユニーク番号。ないかもしれない。
	$update_flg
		"new"が指定されるとsummary_new.phpから呼ばれたと判断する。
	$xml_file
		$update_flgが"new"ではない場合に既存XMLファイル名として使用する。
	$tmpl_id
		テンプレートID ※この画面で指定したテンプレートを渡す。
	その他、呼び元画面の入力情報(当画面にて動的取得)
		$summary_problem
		$diag
		$cre_date
		$problem_id
	その他、各テンプレート側で設定するパラメータ
		$xml_creatorは必須。

*/
ob_start();
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("label_by_profile_type.ini");
require_once("show_select_values.ini");
require_once("get_values_for_template.ini");
require_once("summary_tmpl_common.ini");
require_once("sot_util.php");
require_once("summary_common.ini");
require_once("get_menu_label.ini");
require_once("tmpl_common.ini"); 
ob_end_clean();

$meta_euc = '<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">';

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo(
		"<html><head>".$meta_euc.
		"<script type='text/javascript' src='./js/showpage.js'></script>".
		"<script language='javascript'>showLoginPage(window);</script>".
		"</head></html>"
	);
	exit;
}

// ページ名
if (@$tmpl_title=="") $tmpl_title = "テンプレート";

//======================================
// もしsummary_seqがなければ登録する（2010/01からの仕様 ）
//======================================
$summary_seq = $c_sot_util->regist_summary_seq_if_not_exists(@$summary_id, @$pt_id, $con, $fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);


//======================================
// 診療区分IDが指定されていない場合、診療区分名から診療区分IDを取得する（その１）
//======================================
if (!@$div_id && @$diag) {
	$sel = $c_sot_util->select_from_table("select div_id from divmst where divmst.diag_div like '".pg_escape_string($diag)."'");
	$div_id = @pg_result($sel, 0, 0);
}
//======================================
// テンプレートが指定されていないが、診療記録区分が指定されている場合は、前回使用テンプレートを取得する。
//======================================
if (!$tmpl_id && @$div_id && @$tmpl_change_flg != "1" && @$xml_file == "" && @$copy_xml_file == "") {
	$sql = "select tmpl_id from tmpllastuse where emp_id = '".pg_escape_string($emp_id)."' and div_id = ".pg_escape_string($div_id);
	$sel = $c_sot_util->select_from_table($sql);
	$tmpl_id = @pg_fetch_result($sel, 0, 0);
	if ($tmpl_id)
	{
		// 取得した前回使用テンプレートが、指定された診療区分に属しているか調べる(診療区分との関連付けが前回時と変わった可能性があるため)
		$sql = "select tmpl_id from tmpl_div_link where div_id = " . $div_id . " and tmpl_id = " . $tmpl_id ;
		$sel = $c_sot_util->select_from_table($sql);
		$tmpl_id = pg_fetch_result($sel, 0, 0);
	}
}
//======================================
// テンプレートが指定されている場合、テンプレートから診療区分を取得する
//======================================
//if (@$tmpl_id && $tmpl_id != "free"){
if (!@$div_id && @$tmpl_id && $tmpl_id != "free"){
	$sel = $c_sot_util->select_from_table("select div_id from tmpl_div_link where tmpl_id = ".(int)$tmpl_id);
	$div_id = pg_fetch_result($sel, 0, 0);
}
//======================================
// 診療区分IDが指定されている場合、診療区分名を取得する
//======================================
if (@$div_id){
	$sel = $c_sot_util->select_from_table("select diag_div from divmst where div_id = ".(int)$div_id);
	$diag = @pg_fetch_result($sel, 0, 0);
}
//======================================
// 診療区分IDが指定されていない場合、診療区分名から診療区分IDを取得する（その２）
//======================================
if (!@$div_id && @$diag) {
	$sel = $c_sot_util->select_from_table("select div_id from divmst where divmst.diag_div like '".pg_escape_string($diag)."'");
	$div_id = @pg_result($sel, 0, 0);
}
//======================================
//診療記録区分名をデコード
//======================================
if(@$diag == "") $diag = urldecode(@$enc_diag);

//======================================
// 診療区分IDが指定されている場合、かつ「参照のみ」でなく、「温度板からの直接登録」でなければ、診療区分ドロップダウンを作成
//======================================
$tmpl_dropdown_list = array();
if (@$div_id && !@$sansho_flg && !@$direct_registration){
	$sql =
		" select tmpl_div_link.tmpl_id, tmpl_div_link.default_flg, tmplmst.tmpl_name".
		" from tmpl_div_link, tmplmst".
		" where tmpl_div_link.tmpl_id = tmplmst.tmpl_id and tmpl_div_link.div_id = ".(int)@$div_id.
		" order by tmpl_div_link.tmpl_id";
	$sel = $c_sot_util->select_from_table($sql);
	$tmpl_dropdown_list = pg_fetch_all($sel);
	if (!count($tmpl_dropdown_list) || !$tmpl_dropdown_list[0]){
		$summary_record_kubun_title =  $_label_by_profile["SUMMARY_RECORD_KUBUN"][$profile_type];
		$js = $c_sot_util->javascript('alert("選択された'.$summary_record_kubun_title.'に対するテンプレートがありません");close();');
		echo "<html><head>".$meta_euc.$js."</head></html>";
		exit;
	}
}
//======================================
// テンプレートIDがなければ、ドロップダウンリストの一番目とする。
//======================================
if (!@$tmpl_id && count($tmpl_dropdown_list)){
	$tmpl_id = $tmpl_dropdown_list[0]["tmpl_id"];
}
//======================================
//前回使用したテンプレートを保存する
//======================================
if(@$tmpl_id && @$tmpl_id!="free" && @$div_id) {
	$sql = "delete from tmpllastuse where emp_id = '".pg_escape_string($emp_id)."' and div_id = ".(int)$div_id;
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
	$sql =
		" insert into tmpllastuse (emp_id, div_id, tmpl_id) values (".
		" '".pg_escape_string($emp_id)."', ".(int)$div_id.", ".(int)$tmpl_id.")";
	$upd = update_set_table($con, $sql, array(), null, "", $fname);
}


//========================================
//表示テンプレートのテンプレートファイル名を取得
//========================================
$tmpl_file = "";
$window_size = 1;//通常表示
if($tmpl_id != 'free') {
	$sel = $c_sot_util->select_from_table("select * from tmplmst where tmplmst.tmpl_id = ".(int)@$tmpl_id);
	$tmpl_file = pg_result($sel,0,"tmpl_file");
	$window_size = pg_result($sel,0,"window_size");

	//テンプレートファイルが存在しない場合はエラー
	if(!file_exists($tmpl_file)) {
		$js = $c_sot_util->javascript('alert("テンプレートファイルが正しくありません。管理者に連絡してください。('.$tmpl_file.')");close();');
		echo "<html><head>".$meta_euc.$js."</head></html>";
		exit;
	}
}

//========================================
//テンプレートファイル読み込み
//========================================
if($tmpl_file != "") require($tmpl_file);

//========================================
// テンプレートのバージョン取得
//========================================
$template_behavior = function_exists("get_template_behavior") ? get_template_behavior() : 0;
$template_version  = @$template_behavior ? @$template_behavior["VERSION"] : 0;
$template_copyable = @$template_behavior ? @$template_behavior["COPYABLE"] : 0; // 複数テンプレート対応型の場合
if (!$template_copyable && defined('TEMPLATE_COPY_ABLE') && TEMPLATE_COPY_ABLE) $template_copyable = 1;

//========================================
//コピー用XMLが指定されている場合、コピー対応テンプレートであればコピーXMLを使用する。
//========================================
$use_xml_file = @$xml_file;
if(@$tmpl_copy_xml_file != ""/* && $template_copyable*/) {
	$use_xml_file = $tmpl_copy_xml_file;
} else {

	// XMLの患者IDが異なる場合（データ不整合時）はテンプレートの内容を表示しない
	$sel = $c_sot_util->select_from_table("select ptif_id from sum_xml where xml_file_name = '$use_xml_file'");
	if (pg_num_rows($sel) == 0) {
		$use_xml_file = "";
	} else {
		$tmp_ptif_id = pg_fetch_result($sel, 0, "ptif_id");
		if (strlen($tmp_ptif_id) > 0 && $tmp_ptif_id !== $pt_id) {
			$use_xml_file = "";
		}
	}
}


?>
<? if ($template_version < 2){ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<? } else { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<? } ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?
$med_report_title = get_report_menu_label($con, $fname);
// メドレポート/ＰＯレポート(Problem Oriented)
echo ($med_report_title); ?>｜入力テンプレート</TITLE>
<? if ($template_version < 2){ ?><script type="text/javascript" src="js/fontsize.js"></script><? } ?>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/numberOnly.js"></script>

<? if(defined('TEMPLATE_CALENDAR_COUNT')) set_calendar_js(TEMPLATE_CALENDAR_COUNT); // カレンダー用JS出力処理 ?>

<? if(@$sansho_flg == false && !@$direct_registration) { ?>
<script type="text/javascript">
	set_window_size();
	function set_window_size() {
		<? if($window_size == 1) { ?>
		var w = 600;
		var h = 760;
		var l = 500;
		var t = 100;
		<? } else { ?>
		var h = window.screen.availHeight;
		var w = window.screen.availWidth;
		var l = 0;
		var t = 0;
		<? } ?>
		//1回だけではうまくいかないケースあり。
		window.moveTo(l, t);
		window.resizeTo(w, h);
		window.moveTo(l, t);
		window.resizeTo(w, h);
	}
</script>
<? } ?>

<script type="text/javascript">
var pageLoaded = false;
var resizeAgain = 0;
var iframe_name = "<?=@$iframe?>";

function resizeIframe(){
	if (!window.parent) return;
	if (iframe_name=="") return;
	var iframe = window.parent.document.getElementById(iframe_name);
	if (!iframe) return;
	var iframe = window.parent.document.getElementById(iframe_name);
	iframe.style.height = "280px";
	var h = (document.documentElement.clientHeight || window.innerHeight || document.body.clientHeight);
	h = Math.max(h, document.body.scrollHeight) + (20*1);
	if  (h < 100) {
		resizeAgain++;
		if (resizeAgain<5){
			setTimeout(resizeIframe, 300);
			return;
		}
	}
	iframe.style.height = h + "px";
	if (window.parent.resizeIframeHeight) window.parent.resizeIframeHeight();
}

var isWindows = (navigator.userAgent.indexOf("Win") != -1 ); // Windows系かどうか
//var isMSIE = /*@cc_on!@*/false; //IE系かどうか
function adjustTextAreaHeight(id, rowsize, letDummyShowing){
	var ta = document.getElementById(id);
	if (!ta) return;
	if (isWindows) ta.style.letterSpacing = "1px"; // Windowsの場合は、文字間隔を開ける
	if (ta.value==""){
		ta.rows = rowsize;
		ta.style.height = null;
		if(rowsize <= 1) ta.style.height = "22px";
		return;
	}
	var dmy = document.getElementById("textarea_dummy");
	if (ta.style.fontSize) dmy.style.fontSize = ta.style.fontSize;
	else ta.style.fontSize = dmy.style.fontSize;
	if (ta.style.fontFamily) dmy.style.fontFamily = ta.style.fontFamily;
	else ta.style.fontFamily = dmy.style.fontFamily;
	dmy.style.width = ta.offsetWidth+"px";
	dmy.value = "test";
	dmy.style.height = null;
	dmy.rows = rowsize;
	dmy.style.display = "";
	var h = dmy.offsetHeight;
	dmy.style.height = "10px";
	dmy.value = ta.value;

//	var margin = isMSIE ? 14 : 2;// IE系の場合は、14px程度プラスして、次行も表示させる。それ以外は下を2pxあける。
	var margin = 14;
	for(var i=0; i<30; i++){
		dmy.scrollTop = 1000;
		if (dmy.scrollTop == 0) break;
		dmy.style.height = (dmy.offsetHeight + dmy.scrollTop + margin)+"px";
	}
	if (h < parseInt(dmy.style.height,10)) h = parseInt(dmy.style.height,10);
	if (h < 22) h = 22;
	ta.style.height = h+"px";
	if (!letDummyShowing) dmy.style.display = "none";
}
function initForPrint(){
	<?// この関数は、sum_application_detail_print.phpが実装しています。消さないでください。?>
	if (typeof(template_print_init)=="function") template_print_init();
}

function show_copy_window(){
	var url =
			'summary_copy.php'
		+ '?session=<?=$session?>'
		+ '&pt_id=<?=$pt_id?>'
		+ '&emp_id=<?=$emp_id?>'
		+ '&div_id=<?=@$div_id?>'
		+ '&update_flg=new';
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=1000,height=700";
	window.open(url, 'tmpl_copy_window',option);
}

	/****************************************************************
	* CheckLength()
	* 全角/半角文字を区別しての文字列長取得
	*
	* 引数 ： str文字列長を求める文字列
	* 戻り値： length　文字列長
	* 
	* 2011/02/01 KS追加
	****************************************************************/
function getLength(str) {
	var length = 0;
	for (var i = 0; i < str.length; i++) {
		var c = str.charCodeAt(i);
		// Shift_JIS: 0x0 〜 0x80, 0xa0 , 0xa1 〜 0xdf , 0xfd 〜 0xff
		// Unicode : 0x0 〜 0x80, 0xf8f0, 0xff61 〜 0xff9f, 0xf8f1 〜 0xf8f3
		if ( (c >= 0x0 && c < 0x81) || (c == 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4)) {
			length++;
		} else {
			length += 2;
		}
	}
	return length;
}

// "ぁぃぅぇぉあいうえお".toKatakanaCase(); // ァィゥェォアイウエオ
String.prototype.toKatakanaCase = function()
{
	var i, c, a = [];
	for(i=this.length-1;0<=i;i--)
	{
		c = this.charCodeAt(i);
		a[i] = (0x3041 <= c && c <= 0x3096) ? c + 0x0060 : c;
	};
	return String.fromCharCode.apply(null, a);
};

// "ァィゥェォアイウエオ".toHirakanaCase(); // ぁぃぅぇぉあいうえお
String.prototype.toHirakanaCase = function()
{
	var i, c, a = [];
	for(i=this.length-1;0<=i;i--)
	{
		c = this.charCodeAt(i);
		a[i] = (0x30A1 <= c && c <= 0x30F6) ? c - 0x0060 : c;
	};
	return String.fromCharCode.apply(null, a);
};

/****************************************************************
* getAge()
* 年齢を計算する
*
* 引数 ： str文字列長を求める文字列
* 戻り値： length　文字列長
* 
* 2011/02/01 KS追加
****************************************************************/
function getAge(birth)
{
	var nowdate = new Date();
	var ty = nowdate.getYear();
	var ty = (ty < 2000) ? ty+1900 : ty;
	var tm = nowdate.getYear() + 1;
	var td = nowdate.getDay();
	var arr = birth.split('/');
	var mm = Math.abs(nowdate.getMonth() - arr[1]);
	var a = ty - arr[0];
	var bd = 0;
	if(tm * 100 + td < arr[1] * 100 + arr[2]) a--;
	
	if(mm > 0) bd = a + "歳" + mm + "ヶ月";
	else bd = a + "歳";
	return bd;
}
//	WindowのOnLoadイベント時の処理をタグから出しました。 2011/02/10 K.Sato
window.onload = function(){
	// iframeに読み込み時にiframeリサイズがうまくいかない（body内のdocumentのレンダリングが終わってないため）場合があり、
	// リサイズがうまくいかなかった。遅延実行することで問題を解決した。
	// 処方箋のときのみ発生しているようだが、なぜほかのテンプレートで発生しないのかは不明
	setTimeout(function(){resizeIframe();initForPrint();if(window.load_end_action){load_end_action();};pageLoaded = 1;},2000);
	if(window.load_action){load_action();};
	loadResizingTextArea();

	<? if(defined('TEMPLATE_CALENDAR_COUNT')){ ?>initcal();<? } ?>

	<? if(@$iframe != ""){ ?>
	if(window.parent.document.getElementById('<?=$iframe ?>')) {
		var pfrm = window.parent.document.getElementById('<?=$iframe ?>');
		var h = pfrm.contentWindow.document.body.scrollHeight;
		if(h < pfrm.contentWindow.document.body.offsetHeight) {
			h = pfrm.contentWindow.document.body.offsetHeight;
		}
	}

	<? } ?>

	<? if(@$is_copy == "true" && @$tmpl_id != "free" && !$template_copyable){ ?>
	//alert('このテンプレートはコピーに対応していません。');
	<? } ?>
/*
	resizeIframe();
	initForPrint();
	if(window.load_end_action){load_end_action();};
	pageLoaded = 1;
*/
	<? // IPAD対応：summary_new.phpを参照のこと?>
	if (window.opener && window.opener.document.touroku && window.opener.document.touroku.popupWindowLoaded) {
		window.opener.document.touroku.popupWindowLoaded.value = '1';
	}
}

</script>
<? if ($template_version < 2){ ?>
<link rel="stylesheet" type="text/css" href="css/main.css">
<? } else { ?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<? } ?>
</head>

<? if(@$sansho_flg || @$direct_registration){ ?>
<? $background = "background-color:#ffc"; ?>
<? } else { ?>
<? $background = "background-color:#ffc; background-image:url(img/tmpl_white_header.gif); background-repeat:repeat-x;";?>
<? } ?>

<body style="padding:8px; margin:0; <?=$background?>" >

<?// これも消さないでください。理由はsum_application_detail_print.phpに関する上記同様。?>
<!-- END_OF_SUMMARY_TMPL_READ_BODY_TAG -->
<? if ($tmpl_id!="free"){ ?>
<textarea id="textarea_dummy" rows="2" onkeyup="tx();" onchange="tx();"
style="overflow:hidden; position:absolute; left:0; top:-1000px; display:none;"></textarea>
<? } ?>

<? //======================================== ?>
<? //閉じるヘッダーとテンプレート選択を表示   ?>
<? //======================================== ?>
<? if(@$sansho_flg == false){ ?>
	<div style="border-bottom:1px solid #2e79be; border-top:1px solid #8ac8fb; margin-bottom:16px; <?=@$direct_registration ? "display:none" : ""?>">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr style="background-color:#4baaf8;">
	<? if ($template_version >= 2){ ?>
	<td style="font-size:14px; color:#fff; padding:4px; letter-spacing:1px"><b><?=$tmpl_title?></b></td>
	<? } else { ?>
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$tmpl_title?></b></font></td>
	<? } ?>

	<td align="right">
		<form name="tmpl_change" action="summary_tmpl_read.php" method="post">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right"><nobr>

						<? //======================================== ?>
						<? //テンプレート入力完了ボタンを出力 その１  ?>
						<? //======================================== ?>
						<? if(@$sansho_flg == false && !@$direct_registration) { ?>
						<?   if($tmpl_id == "free") { ?>
						<input id="Button1" type="button" value="次の画面へ" onclick="if (!pageLoaded) return; document.tmplform.submit();window.close();">
						<?   } else { ?>
						<input id="Button1" type="button" value="次の画面へ" name="Button1" onclick="if (!pageLoaded) return; if(window.InputCheck){ if(InputCheck()){document.tmplform.submit();}; }else{document.tmplform.submit();}">
						<?   } ?>
						<? } ?>

						<? if ($template_copyable){ ?>
						<input type="button" onclick="show_copy_window();" value="記録からのコピー" />&nbsp;
						<? } ?>
						<? if (@$direct_registration){ ?>
						<input type="hidden" name="tmpl_id" value="<?=$tmpl_id?>">
						<? } else { ?>
						<select name="tmpl_id" onChange="document.tmpl_change.submit();">
							<? foreach($tmpl_dropdown_list as $idx => $row){ ?>
							<option value="<? echo $row["tmpl_id"]; ?>" <? echo ($row["tmpl_id"]==$tmpl_id) ? "selected" : ""; ?>><? echo h($row["tmpl_name"]); ?></option>
							<? } ?>
							<? if(@$is_copy == "true") { ?>
							<option value="free" <?=($tmpl_id == "free" ? "selected" : "")?> >フリー記載</option>
							<? } ?>
						</select>
						<? } ?>
					</nobr></td>
				</tr>
			</table>
			<input type="hidden" name="session" value="<? echo($session); ?>">
			<input type="hidden" name="pt_id" value="<? echo(@$pt_id); ?>">
			<input type="hidden" name="emp_id" value="<? echo(@$emp_id); ?>">
			<input type="hidden" name="summary_id" value="<? echo(@$summary_id); ?>">
			<input type="hidden" name="summary_seq" value="<? echo(@$summary_seq); ?>">
			<input type="hidden" name="apply_id" value="<? echo(@$apply_id); ?>">
			<input type="hidden" name="update_flg" value="<? echo(@$update_flg); ?>">
			<input type="hidden" name="diag" value="<? echo(@$diag); ?>">
			<input type="hidden" name="enc_diag" value="<? echo(@$enc_diag); ?>">
			<input type="hidden" name="div_id" value="<?=@$div_id?>">
			<input type="hidden" name="sansho_flg" value="<? echo(@$sansho_flg); ?>">
			<input type="hidden" name="is_copy" value="<? echo(@$is_copy); ?>">
			<input type="hidden" name="tmpl_copy_xml_file" value="<? echo(@$tmpl_copy_xml_file); ?>">
			<input type="hidden" name="tmpl_copy_entry_info" value="<? echo(@$tmpl_copy_entry_info); ?>">
			<input type="hidden" name="tmpl_change_flg" value="1">
			<input type="hidden" name="cre_date" value="<? echo(@$cre_date); ?>">
			<input type="hidden" name="direct_registration" value="<?=@$direct_registration?>"/>
			<input type="hidden" name="dairi_emp_id" value="<?=@$_REQUEST["dairi_emp_id"]?>">
			<input type="hidden" name="seiki_emp_id" value="<?=@$_REQUEST["seiki_emp_id"]?>">
		</form>
	</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center">
		<? if (!@$direct_registration) { ?>
		<a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a>
		<? } ?>
	</td>
	</tr>
	</table>
	</div>
<? } ?>



<? //======================================== ?>
<? //参照-最大表示時の閉じるヘッダー          ?>
<? //======================================== ?>
<? if(@$sansho_flg == true && $window_size != 1) { ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#5279a5">
	<? if ($template_version >= 2) { ?>
	<td class="spacing" style="font-size:16px; color:#fff"><b><?=$tmpl_title?></b></td>
	<? } else { ?>
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$tmpl_title?></b></font></td>
	<? } ?>
	<td align="right">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="right">
		<nobr>
		<input type="button" value="印刷" onclick="window.open('summary_naiyo_print.php?session=<?=$session?>&pt_id=<?=$pt_id?>&emp_id=<?=$emp_id?>&summary_seq=<?=$summary_seq?>&apply_id=<?=$apply_id?>&sum_id=<?=$summary_id?>&mode=template', 'naiyo_print', 'width=700,height=600,scrollbars=yes');">
		</nobr>
		</td>
		</tr>
		</table>
	</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	</table>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
<? } ?>


<? //======================================== ?>
<? //FORMタグ外HTML出力                       ?>
<? //======================================== ?>
<? tmpl_page_out_of_tmplform_html($session); ?>

<? //======================================== ?>
<? //送信用FORMタグ(1) 親画面用(フリー記載用) ?>
<? //======================================== ?>
<? if ($tmpl_id == "free") { ?>
	<form action="<?=($update_flg=="new" ? "summary_new.php" : "summary_update.php")?>" method="POST" name="tmplform" target="migi">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
	<input type="hidden" name="summary_id" value="<? echo($summary_id); ?>">
	<input type="hidden" name="summary_seq" value="<? echo($summary_seq); ?>">
	<input type="hidden" name="apply_id" value="<? echo(@$apply_id); ?>">
	<input type="hidden" name="xml_file" value="">
	<input type="hidden" name="tmpl_id" value="">
	<input type="hidden" name="cre_date" value="<? echo(@$cre_date); ?>">
	<input type="hidden" name="diag" value="">
	<input type="hidden" name="summary_div" value="">
	<input type="hidden" name="summary_problem" value="">
	<input type="hidden" name="priority" value="">
	<input type="hidden" name="problem_id" value="">
	<input type="hidden" name="sect_id" value="">
	<input type="hidden" name="outreg" value="">
	<input type="hidden" name="no_tmpl_window" value="1">
	<input type="hidden" name="sot_id" value="" />
	<input type="hidden" name="sot_col_index" value="" />
	<input type="hidden" name="direct_registration" value="<?=@$direct_registration?>"/>
	<input type="hidden" name="dairi_emp_id" value="<?=@$_REQUEST["dairi_emp_id"]?>">
	<input type="hidden" name="seiki_emp_id" value="<?=@$_REQUEST["seiki_emp_id"]?>">
	<input type="hidden" name="disabled_kousin_sousin_flg" value="0">

<? //======================================== ?>
<? //送信用FORMタグ(2) create_tmpl_xml.php用  ?>
<? //======================================== ?>
<? } else { ?>
	<form action="create_tmpl_xml.php" method="POST" name="tmplform">
	<input type="hidden" name="session" value="<? echo($session); ?>">
	<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
	<input type="hidden" name="emp_id" value="<? echo($emp_id); ?>">
	<input type="hidden" name="xml_file" value="<? echo(@$xml_file); ?>">
	<input type="hidden" name="summary_id" value="<? echo(@$summary_id); ?>">
	<input type="hidden" name="summary_seq" value="<? echo(@$summary_seq); ?>">
	<input type="hidden" name="apply_id" value="<? echo(@$apply_id); ?>">
	<input type="hidden" name="update_flg" value="<? echo(@$update_flg); ?>">
	<input type="hidden" name="tmpl_id" value="<? echo($tmpl_id); ?>">
	<input type="hidden" name="cre_date" value="<? echo(@$cre_date); ?>">
	<input type="hidden" name="diag" value="">
	<input type="hidden" name="summary_div" value="">
	<input type="hidden" name="summary_problem" value="">
	<input type="hidden" name="priority" value="">
	<input type="hidden" name="problem_id" value="">
	<input type="hidden" name="sect_id" value="">
	<input type="hidden" name="outreg" value="">
	<input type="hidden" name="sot_id" value="" />
	<input type="hidden" name="sot_col_index" value="" />
	<input type="hidden" name="direct_registration" value="<?=@$direct_registration?>"/>
	<input type="hidden" name="dairi_emp_id" value="<?=@$_REQUEST["dairi_emp_id"]?>">
	<input type="hidden" name="seiki_emp_id" value="<?=@$_REQUEST["seiki_emp_id"]?>">
	<input type="hidden" name="disabled_kousin_sousin_flg" value="0">
<? } ?>


<? //======================================== ?>
<? //入力フォームを出力                       ?>
<? //テンプレートインターフェース関数を呼び出す ?>
<? //======================================== ?>
<?
if($tmpl_file != "") {
	$tmpl_param = null;
	$tmpl_param['mode'] = 'disp';
	if(@$sansho_flg == false) $tmpl_param['mode'] = (@$update_flg == "new" ? "new" : "update");
	$tmpl_param['session'] = $session;
	$tmpl_param['login_emp_id'] = $emp_id;
	$tmpl_param['pt_id'] = $pt_id;
	$tmpl_param['summary_id'] = (@$summary_id != "" ? @$summary_id : null);
	$tmpl_param['summary_seq'] = (@$summary_seq != "" ? @$summary_seq : null);
	$tmpl_param['apply_id'] = (@$apply_id != "" ? @$apply_id : null);
	$tmpl_param['worksheet_date'] = (@$worksheet_date != "" ? @$worksheet_date : null); // ワークシートで開いている日
	$tmpl_param['cre_date'] = @$cre_date; // 作成年月日
	$tmpl_param['tmpl_id'] = @$tmpl_id; // 作成年月日
	$tmpl_param['update_flg'] = @$update_flg;
	write_tmpl_page($use_xml_file,$con,$fname,$tmpl_param); // 各テンプレート内に定義されている関数
	if (function_exists("create_utf8_xmldom_from_request")){
		echo '<input type="hidden" name="xml_creator" value="'.$tmpl_file.'" />';
		echo '<input type="hidden" name="xml_creator_use_http" value="1" />';
	}
} else {
	//フリー記載
	write_tmpl_free_page($con,$fname,$tmpl_copy_entry_info);
}
?>



<? if(@$sansho_flg != true && !@$direct_registration) { ?>
<script languege="javascript">
	document.tmplform.summary_problem.value = window.opener.document.touroku.summary_problem.value;
	document.tmplform.diag.value = window.opener.document.touroku.sel_diag.options[window.opener.document.touroku.sel_diag.selectedIndex].text;
	document.tmplform.summary_div.value = window.opener.document.touroku.sel_diag.options[window.opener.document.touroku.sel_diag.selectedIndex].text;
	document.tmplform.cre_date.value =
		window.opener.document.touroku.sel_yr.options[window.opener.document.touroku.sel_yr.selectedIndex].value
		+ window.opener.document.touroku.sel_mon.options[window.opener.document.touroku.sel_mon.selectedIndex].value
		+ window.opener.document.touroku.sel_day.options[window.opener.document.touroku.sel_day.selectedIndex].value;
	document.tmplform.priority.value = window.opener.document.touroku.priority.options[window.opener.document.touroku.priority.selectedIndex].text;
	document.tmplform.problem_id.value = window.opener.document.touroku.problem_id.value;
	document.tmplform.sect_id.value = window.opener.document.touroku.sect_id.value;
	document.tmplform.outreg.value = window.opener.document.touroku.outreg.value;
</script>
<? } ?>


<? //======================================== ?>
<? //テンプレート入力完了ボタンを出力 その２  ?>
<? //======================================== ?>
<? if(@$sansho_flg == false && !@$direct_registration) { ?>
<?   if($tmpl_id == "free") { ?>
<input id="Button1" type="button" value="次の画面へ" onclick="document.tmplform.submit();window.close();">
<?   } else { ?>
<input id="Button1" type="button" value="次の画面へ" name="Button1" onclick="if(window.InputCheck){ if(InputCheck()){document.tmplform.submit();}; }else{document.tmplform.submit();}">
<?   } ?>
<? } ?>

<? //======================================== ?>
<? //初期表示データ設定スクリプトを出力       ?>
<? //テンプレートインターフェース関数を呼び出す ?>
<? //======================================== ?>
<? if($tmpl_file != "" && function_exists("write_tmpl_page_data_script")) write_tmpl_page_data_script($use_xml_file,$con,$fname,$tmpl_param); ?>

</form>

<?
/**
 * フリー記載のテンプレートを出力します。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param string $tmpl_copy_entry_info エントリー情報文字列
 */
function write_tmpl_free_page($con,$fname,$tmpl_copy_entry_info) {
	require_once('summary_tmpl_copy_util.ini');
	$entry_list = convert_entry_list($tmpl_copy_entry_info);

	$disp_text = "";
	$disp_text_for_opera = "";
	foreach($entry_list as $entry_list1) {
		$summary_seq = $entry_list1['summary_seq'];
		$tag_id_array = $entry_list1['tag_id'];

		//フリー記録のみコピーする。
		if($tag_id_array == "") {
			if($disp_text != "") {
				$disp_text .= "<br>";
				$disp_text_for_opera .= "\\n";
			}

			$sql = "select smry_cmt from summary where summary_seq = ".(int)$summary_seq;
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0) {
				pg_close($con);
				summary_common_show_error_page_and_die();
			}
			$data = pg_fetch_array($sel,0);
			$disp_text .= $data['smry_cmt'];
			$disp_text_for_opera .= $data['smry_cmt'];
		}

		// メモ：データパターンについて。
		// ・テンプレートから入力されたデータはreadonlyのTEXTAREA用の文字列データとなる。(html形式ではない)
		//   ※get_summary_naiyo_html_for_tmpl_textarea_value()を参照。
		// ・フリー入力の場合、オペラ以外はhtml形式、オペラの場合はテキスト形式。
		// 　※ちなみに、ユーザーはオペラと他を併用してはならないことが運用仕様でり大前提となっている。
		// ・ここで表示する内容はオペラ以外はhtml形式、オペラの場合はテキスト形式。
		// テンプレートのデータも表示することになる場合はこの辺も要注意。
	}
?>
	<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript">
	var m_use_tyny_mce = false;
	if(!tinyMCE.isOpera) {
		m_use_tyny_mce = true;
		tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			plugins : "preview,table,emotions,fullscreen,layer",
			//language : "ja_euc-jp",
			language : "ja",
			width : "100%",
			height : "400",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			content_css : "tinymce/tinymce_content.css",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
			theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
			theme_advanced_buttons3 : "tablecontrols,|,visualaid",
			//theme_advanced_statusbar_location : "none",
			force_br_newlines : true,
			//forced_root_block : '',
			force_p_newlines : false
		});
	}
	</script>
	<TextArea id="tmpl_free_input" style="WIDTH: 100%; HEIGHT: 400; ime-mode: active" size="54" name="summary_naiyo" rows="1"><?=$disp_text?></TextArea>
	<script type="text/javascript">
	if(!m_use_tyny_mce) {
		document.getElementById('tmpl_free_input').value = "<? echo h($disp_text_for_opera); ?>";
	}
	</script>
<? } // end of function write_tmpl_free_page() ?>
</body>
</html>

