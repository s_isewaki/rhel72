<?
ob_start();
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("kango_format_10_3_util.ini");
require_once("kango_euc_special_char.ini");

//==============================
//前処理
//==============================
// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// パラメータ
$standard_cd = isset($_REQUEST["standard_cd"]) ? $_REQUEST["standard_cd"] : 1;
$fiscal_year = isset($_REQUEST["fiscal_year"]) ? $_REQUEST["fiscal_year"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ($con == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//====================================
//ファイル名
//====================================
switch ($standard_cd) {
    case 1:
        $filename = "kango_7_1.xls";
        break;
    case 2:
        $filename = "kango_10_1.xls";
        break;
    case 7:
        $filename = "kango_chiiki.xls";
        break;
    default:
        $filename = "kango_10_3.xls";
        break;
}

$download_data = get_excel_data($con, $fname, $fiscal_year, $standard_cd);


//====================================
//EXCEL出力
//====================================
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();



pg_close($con);

//====================================
//内部関数
//====================================

function get_excel_data($con, $fname, $fiscal_year, $standard_cd)
{
	//==============================
	//表示データ取得
	//==============================
	$date_list = get_format_10_3_year_disp_data($con,$fname,$fiscal_year,$standard_cd);

	//==============================
	//施設基準
	//==============================
	$standard_name = get_standard_name($con,$fname,$standard_cd);

	//==============================
	//エクセルデータ作成
	//==============================
	ob_clean();
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">

	<!-- ヘッダー START -->
	<table width="800" border='0' cellspacing='0' cellpadding='1'>

	<tr height="23">
	<td><font size='4' face='ＭＳ Ｐゴシック, Osaka'>様式１０の３</font></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>

	<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>

	<tr height="36">
	<td colspan="6"><font size='4' face='ＭＳ Ｐゴシック, Osaka'><?=$standard_name?>入院基本料における患者の重症度・看護必要度に係る報告書類</font></td>
	</tr>

	<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>

	<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>

	</table>
	<!-- ヘッダー END -->


	<!-- 一覧 START -->
	<table width="800" border='1' cellspacing='0' cellpadding='2'>

	<tr>
	<td rowspan="3" align="center" width="140">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'>届出<br>入院料</font>
		</td>
	<td rowspan="3" align="center" width="100">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'>届出<br>病床数</font>
		</td>
	<td colspan="3" align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'>入院患者の状況</font>
		</td>
	<td rowspan="2" align="center">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'>入院患者延べ<br>数の算出期間<br>(1ヶ月)</font>
		</td>
	</tr>

	<tr>
	<td rowspan="2" align="center" width="130">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'><?=euc_special_char('MARU_1')?>入院患者<br>延べ数</font>
		</td>
	<td rowspan="2" align="center" width="130">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'><?=euc_special_char('MARU_2')?>&nbsp;<?=euc_special_char('MARU_1')?>のうち<br>重症度・<br>看護必要度<br>の基準を<br>満たす患者<br>の延べ数</font>
		</td>
	<td rowspan="2" align="center" width="130">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'><?=euc_special_char('MARU_3')?>重症度・<br>看護必要度<br>の基準を<br>満たす患者<br>の割合<br>(<?=euc_special_char('MARU_2')?>/<?=euc_special_char('MARU_1')?>)</font>
		</td>
	</tr>

	<tr>
	<td align="center" width="130">
		<font size='3' face='ＭＳ Ｐゴシック, Osaka'><?=$fiscal_year?>年度</font>
		</td>
	</tr>



	<?
	foreach($date_list as $i => $date_list1)
	{
		?>
		<tr height="33">
		<td align="center">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?=$standard_name?>入院基本料
			</font>
			</td>
		<td align="right">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?
			if($date_list1['bed_count'] != "")
			{
			?>
			<?=$date_list1['bed_count']?>&nbsp;床
			<?
			}
			?>
			</font>
			</td>
		<td align="right">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?
			if($date_list1['pt_count'] != "")
			{
			?>
			<?=$date_list1['pt_count']?>&nbsp;名
			<?
			}
			?>
			</font>
			</td>
		<td align="right">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?
			if($date_list1['pt_7_1_count'] != "")
			{
			?>
			<?=$date_list1['pt_7_1_count']?>&nbsp;名
			<?
			}
			?>
			</font>
			</td>
		<td align="right">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?
			if($date_list1['pt_count'] != "" && $date_list1['pt_7_1_count'] != "")
			{
			?>
			<?=get_rate_7_1($date_list1['pt_count'],$date_list1['pt_7_1_count'])?>&nbsp;%
			<?
			}
			?>
			</font>
			</td>
		<td align="center">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka'>
			<?=$date_list1['date']?>
			</font>
			</td>
		</tr>
		<?
	}
	?>


	</table>
	<!-- 一覧 END -->

	<!-- フッター START -->
	<table width="800" border='0' cellspacing='0' cellpadding='1'>

	<tr height="33">
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>

	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>〔記載上の注意〕</font></td>
	</tr>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>１&nbsp;&nbsp;入院患者延べ数とは、算出期間中に<?=$standard_name?>入院基本料を算定している延べ患者数をいう。</font></td>
	</tr>
    <? if ($standard_cd != 7) {?>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;なお、<?=euc_special_char('MARU_1')?>から<?=euc_special_char('MARU_3')?>の患者数に産科及び小児科の患者数は含めない。</font></td>
	</tr>
    <?}?>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>２&nbsp;&nbsp;重症度・看護必要度の基準を満たす患者とは、別添６の別紙７の「一般病棟用の重症度・</font></td>
	</tr>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;看護必要度に係る評価票」を用いて評価を行い、Ａモニタリング及び処置等に係る得点が</font></td>
	</tr>
    <? if ($standard_cd == 7) {?>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;「１点以上」の患者をいう。</font></td>
	</tr>
    <?} else {?>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;「２点以上」、かつＢ患者の状況等に係る得点が「３点以上」である患者をいう。</font></td>
	</tr>
    <?}?>
	<? if ($standard_cd != 7) {?>
    <tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>３&nbsp;&nbsp;一般病棟と結核病棟を併せて１看護単位としている場合、重症度・看護必要度の算出に</font></td>
	</tr>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;あたっては、結核病棟に入院している患者を一般病棟の入院患者とみなし、</font></td>
	</tr>
	<tr height="33">
	<td colspan="6"><font size='3' face='ＭＳ Ｐゴシック, Osaka'>&nbsp;&nbsp;併せて計算する。</font></td>
	</tr>
    <?}?>

	</table>
	<!-- フッター START -->
	<?




	$download_data = ob_get_contents();
	ob_clean();
	return $download_data;
}


function get_rate_7_1($pt_count, $pt_7_1_count) {
    if ($pt_count == 0) {
        $rate_7_1 = 0;
    }
    else {
        $rate_7_1 = 100 * $pt_7_1_count / $pt_count;
    }
    $rate_7_1 = round($rate_7_1, 1); //小数点１桁で四捨五入
    return $rate_7_1;
}