<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix スケジュール | 種別</title>
<? require("about_session.php"); ?>
<? require("about_authority.php"); ?>
<? require("project_check.ini"); ?>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);
if ($pjt_admin_auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

if($ch_user_bulk_set == "1")
{
	//更新処理を実行

	

	// 全ユーザ分のスケジュール画面上の委員会・WGチェックボックスの処理
	$pjtgd = "on";
	$sql = "select emp_id from authmst";
	$cond = "where emp_del_flg = 'f' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	while ($row = pg_fetch_array($sel)) 
	{
		$emp_id = $row["emp_id"];
		project_update($con, $emp_id, $pjtgd, $fname);
	}	
	
	
	// 委員会ＷＧの件数ぶんループする
	$sql = "select pjt_id,pjt_response from project";
	$cond = "where pjt_delete_flag = 'f' ";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	while ($row = pg_fetch_array($sel)) 
	{
		
		//委員会ＷＧの責任者のスケジュールに表示登録されていたら何もせず、表示登録されていなかったら登録する
		ins_disp_schedule($fname, $con, $row["pjt_response"], $row["pjt_id"]);
		
		// 委員会ＷＧののメンバーぶんループする
		$sql = "select emp_id from promember";
		$cond = "where pjt_id = ".$row["pjt_id"];
		$sel2 = select_from_table($con, $sql, $cond, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		while ($row2 = pg_fetch_array($sel2)) 
		{
			//委員会ＷＧのメンバーのスケジュールに表示登録されていたら何もせず、表示登録されていなかったら登録する
			ins_disp_schedule($fname, $con, $row2["emp_id"], $row["pjt_id"]);
		}
	}	
}



function ins_disp_schedule($fname, $con, $emp_id, $pjt_id)
{

	//表示設定の情報を取得する
	$sql = "select emp_id from schdproject";
	$cond = "where pjt_id = $pjt_id and emp_id= '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	$num = pg_numrows($sel);
	
	if($num < 1)
	{

		//表示設定情報を登録する		
		$sql = "insert into schdproject (pjt_id, emp_id, ownother) values (";
		$content = array($pjt_id, $emp_id, '1');
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
			exit;
		}
	}

}



?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function onAction() 
{
	bulk_check_flg = 0;

	//登録用のフラグを設定する。
	for(i = 0; i < document.project_bulk.elements.length; i++)
	{
		if(document.project_bulk.elements[i].name == "ch_user_bulk_set")
		{
			if(document.project_bulk.elements[i].checked)
			{
				//汚染した体液ーその他がチェック
				bulk_check_flg = 1;
			}
		}
	}
	
	if(bulk_check_flg < 1)
	{
		alert("実行する場合はチェックをつけてください");
		return false;
	}

	confirm("一括設定を実行してよろしいですか");

	document.project_bulk.submit();
	
}

</script>

<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#5279a5 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<form name="project_bulk" action="project_bulk_set.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>
<? if ($pjt_admin_auth == "1") { ?>
<td width="85%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
<? }else{ ?>
<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_wg_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">WG登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_member_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >委員会・WG名簿</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_schedule_type_update.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_bulk_set.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括設定</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>





<table border="0" cellspacing="0" cellpadding="1"><tr valign="top"><td>　</td></tr></table>


<table width="500" cellspacing="0" cellpadding="1" border="0">
	<tr>
		<td align="right">
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
				<input type="checkbox" name="ch_user_bulk_set" id="ch_user_bulk_set" value="1" />
			</font>
			<label for="ch_user_bulk_set">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全ユーザの個人スケジュールに、所属している委員会のスケジュールを一括表示する</font>
			</label>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" align="right"><input type="button" value="更新" onclick="onAction()"></td>
	</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="update_act" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
