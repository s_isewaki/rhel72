<?php
require_once('CustomMBFPDF.php');

/**
 * AcrobatのJavaScript APIを利用する為のPDF作成拡張クラス
 */
class JavaScriptMBFPDF extends CustomMBFPDF
{
    /**
     * JavaScriptコード
     * 
     * @var string
     */
    var $_javascript;
    
    /**
     * オブジェクト番号
     * 
     * @var integer
     */
    var $_n_js;

    /**
     * JavaScriptコードをセットする
     * 
     * @param string $script JavaScriptコード
     * 
     * @return void
     */
    function IncludeJS($script)
    {
        $this->_javascript = $script;
    }

    /**
     * 自動的に印刷ダイアログを表示
     * 
     * @param string $pageHandling 印刷時のサイズオプション('fit':合わせる 'none':実際のサイズ 'shrink':特大ページを縮小)
     * 
     * @return void
     */
    function AutoPrint($pageHandling = 'none')
    {
        $script = <<< SCRIPT
var pp = this.getPrintParams();
pp.pageHandling = pp.constants.handling.$pageHandling;
print(pp);
SCRIPT;
        $this->IncludeJS($script);
    }
    
    /**
     * JavaScriptを出力する
     * 
     * @return void
     */
    function _putjavascript()
    {
        $this->_newobj();
        $this->_n_js = $this->n;
        $this->_out('<<');
        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
        $this->_out('>>');
        $this->_out('endobj');
        $this->_newobj();
        $this->_out('<<');
        $this->_out('/S /JavaScript');
        $this->_out('/JS '.$this->_textstring($this->_javascript));
        $this->_out('>>');
        $this->_out('endobj');
    }

    /**
     * 親クラスのメソッドのオーバーライド
     * 
     * @return void
     */
    function _putresources()
    {
        parent::_putresources();
        if (!empty($this->_javascript)) {
            $this->_putjavascript();
        }
    }

    /**
     * 親クラスのメソッドのオーバーライド
     * 
     * @return void
     */
    function _putcatalog()
    {
        parent::_putcatalog();
        if (!empty($this->_javascript)) {
            $this->_out('/Names <</JavaScript '.($this->_n_js).' 0 R>>');
        }
    }
}