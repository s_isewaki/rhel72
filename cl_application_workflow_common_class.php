<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_class_name.ini");

//ini_set("display_errors","1");

class cl_application_workflow_common_class
{
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function cl_application_workflow_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
	}


//-------------------------------------------------------------------------------------------------------------------------
// フォルダツリー関連
//-------------------------------------------------------------------------------------------------------------------------

	/**
	 * ワークフローカテゴリ/フォルダ情報取得
	 * @return   array  ワークフローカテゴリ/フォルダ情報配列
	 */
	function get_workflow_folder_list(&$wkfw_counts)
	{
		$category_list = $this->get_workflow_category();

		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "workflow", array(), "");

		return $category_list;
	}


	/**
	 * ワークフローカテゴリ情報取得
	 * @return   array  ワークフローカテゴリ情報配列
	 */
	function get_workflow_category()
	{
		$sql = "select wkfw_type, wkfw_nm from cl_wkfwcatemst";
		$cond = "where not wkfwcate_del_flg order by wkfw_type asc";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	/**
	 * ワークフローフォルダ情報取得
	 * @param    string  $parent_id   親フォルダID
	 * @param    string  $category_id カテゴリID
	 * @return   array  ワークフローフォルダ情報配列
	 */
	function get_workflow_tree($parent_id, $category_id)
	{

		$sql  = "select a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name ";
		$sql .= "from cl_wkfwfolder a ";
		$sql .= "left join (select * from cl_wkfwtree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
		$sql .= "where not a.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and b.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and b.wkfw_parent_id = $parent_id ";
		}

		if ($category_id != "") {
			$sql .= " and a.wkfw_type = $category_id ";
		}
		$sql .= "order by a.wkfw_folder_id ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$id      = $row["id"];
			$cate    = $row["cate"];
			$name    = $row["name"];

			$arr[] = array(
				"type"      => "folder",
				"cate"      => $cate,
				"parent_id" => $parent_id,
				"id"        => $id,
				"name"      => $name,
				"folders"   => $this->get_workflow_tree($id, $cate)
			);
		}

		return $arr;
	}


	/**
	 * ワークフロー情報取得
	 * @param    string $wkfw_type ワークフロータイプ
	 * @return   array  ワークフロー情報配列
	 */
	function get_wkfw_mst($wkfw_type)
	{
		$sql   = "select * from cl_wkfwmst";
		$cond  = "where wkfw_type = $wkfw_type and not wkfw_del_flg ";
		$cond .= "order by wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$arr[] = array("type" => "file", "wkfw_id" => $row["wkfw_id"], "wkfw_type" => $row["wkfw_type"], "wkfw_title" => $row["wkfw_title"]);
		}
		return $arr;
	}



	// ワークフローフォルダ一覧取得
	function get_cate_workflow_list($category_id, $folder_id, $parent_id)
	{

		if($category_id != "" && $folder_id == "")
		{
			//// 2011/12/19 Yamagawa upd(s)
			////$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			//$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id, null as short_wkfw_name ";
			$sql  = "select 1 as num, varchar(6) 'folder' as type, cast(a.wkfw_folder_id as varchar(2)) as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id, null as short_wkfw_name ";
			//// 2011/12/19 Yamagawa upd(e)
			$sql .= "from cl_wkfwfolder a ";
			$sql .= "left join (select * from cl_wkfwtree where not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where b.wkfw_parent_id is null and a.wkfw_type = $category_id and not a.wkfw_folder_del_flg ";
		}
		else if($category_id != "" && $folder_id != "")
		{
			//// 2011/12/19 Yamagawa upd(s)
			////$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id ";
			//$sql  = "select 1 as num, varchar(6) 'folder' as type, a.wkfw_folder_id as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id, null as short_wkfw_name ";
			$sql  = "select 1 as num, varchar(6) 'folder' as type, cast(a.wkfw_folder_id as varchar(2)) as id, a.wkfw_type as cate, a.wkfw_folder_name as name, b.wkfw_parent_id as parent_id, null as short_wkfw_name ";
			//// 2011/12/19 Yamagawa upd(e)
			$sql .= "from cl_wkfwfolder a ";
			$sql .= "inner join (SELECT * FROM cl_wkfwtree WHERE wkfw_parent_id = $folder_id and not wkfw_tree_del_flg) b on a.wkfw_folder_id = b.wkfw_child_id ";
			$sql .= "where not a.wkfw_folder_del_flg ";
		}
		$sql .= "union all ";

		// 2011/12/19 Yamagawa upd(s)
		//$sql .= "select 2 as num, varchar(6) 'file' as type, a.wkfw_id as id, a.wkfw_type as cate, a.wkfw_title as name, null as parent_id ";
		$sql .= "select 2 as num, varchar(6) 'file' as type, a.wkfw_id as id, a.wkfw_type as cate, a.wkfw_title as name, null as parent_id, a.short_wkfw_name ";
		// 2011/12/19 Yamagawa upd(e)
		$sql .= "from cl_wkfwmst a ";
		$sql .= "where a.wkfw_type = $category_id and not a.wkfw_del_flg ";
		if($folder_id == "")
		{
			$sql .= "and a.wkfw_folder_id is null ";
		}
		else if($folder_id != "")
		{
			$sql .= "and a.wkfw_folder_id = $folder_id ";
		}

		$sql .= "order by num, id asc ";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$tmp_folder_id      = $row["id"];
			$tmp_category_type  = $row["cate"];
			$wkfw_folder_name   = $row["name"];
			$tmp_parent_id      = $row["parent_id"];
			// 2011/12/19 Yamagawa add(s)
			$short_wkfw_name    = $row["short_wkfw_name"];
			// 2011/12/19 Yamagawa add(e)

			if($type == "folder")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => $tmp_parent_id,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
					// 2011/12/19 Yamagawa add(s)
					"short_wkfw_name" => null,
					// 2011/12/19 Yamagawa add(e)
				);
			}
			else if($type == "file")
			{
				$arr[] = array(
					"type" => $type,
					"cate" => $tmp_category_type,
					"parent_id" => null,
					"id" => $tmp_folder_id,
					"name" => $wkfw_folder_name,
					// 2011/12/19 Yamagawa add(s)
					"short_wkfw_name" => $short_wkfw_name,
					// 2011/12/19 Yamagawa add(e)
				);
			}
		}
		return $arr;
	}

	// フォルダパス取得
	function get_folder_path($folder_id)
	{

		// 当該フォルダ情報を取得
		$sql = "select wkfw_folder_id, wkfw_folder_name from cl_wkfwfolder";
		$cond = "where wkfw_folder_id = $folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$folder_name = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$arr = array("id" => $folder_id, "name" => $folder_name);

		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from cl_wkfwtree";
		$cond = "where wkfw_child_id = $folder_id and not wkfw_tree_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 親フォルダが存在しない場合は自分の情報のみ返す
		if (pg_num_rows($sel) == 0) {
			return array($arr);
		}

		// 親フォルダが存在した場合は先祖の情報も含めて返す
		$parent_id = pg_fetch_result($sel, 0, "wkfw_parent_id");
		$ancestor = $this->get_folder_path($parent_id);
		$ancestor[] = $arr;
		return $ancestor;
	}

	// カテゴリ取得
	function get_wkfwcate_mst($wkfw_type)
	{
		$sql = "select wkfw_type, wkfw_nm, ref_dept_st_flg, ref_dept_flg, ref_st_flg from cl_wkfwcatemst";
		$cond = "where not wkfwcate_del_flg and wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type       = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm         = pg_fetch_result($sel, 0, "wkfw_nm");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr  = array("wkfw_type" => $wkfw_type,
		                "wkfw_nm" => $wkfw_nm,
                        "ref_dept_st_flg" => $ref_dept_st_flg,
                        "ref_dept_flg" => $ref_dept_flg,
                        "ref_st_flg" => $ref_st_flg);
		return $arr;
	}

	// フォルダ取得
	function get_wkfwfolder_mst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg from cl_wkfwfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$foder_id        = pg_fetch_result($sel, 0, "wkfw_folder_id");
		$folder_name     = pg_fetch_result($sel, 0, "wkfw_folder_name");
		$ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
		$ref_dept_flg    = pg_fetch_result($sel, 0, "ref_dept_flg");
		$ref_st_flg      = pg_fetch_result($sel, 0, "ref_st_flg");

		$arr = array("wkfw_folder_id" => $foder_id,
                      "wkfw_folder_name" => $folder_name,
                      "ref_dept_st_flg" => $ref_dept_st_flg,
                      "ref_dept_flg" => $ref_dept_flg,
                      "ref_st_flg" => $ref_st_flg);

		return $arr;
	}


	// ワークフロー取得(カウント用)
	function get_workflow_for_count($srceen_mode, $arr_emp_info, $emp_id)
	{
		$sql = "select wkfw_type, wkfw_folder_id from cl_wkfwmst";
		$cond = "where not wkfw_del_flg";

		if($srceen_mode == "apply")
		{
			$today = date("Ymd");
			$cond .= " and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
			$cond .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";


			if($arr_emp_info != "")
			{
				$cond .= "and (";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$emp_class     = $arr_emp_info[$i]["emp_class"];
					$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
					$emp_dept      = $arr_emp_info[$i]["emp_dept"];
					$emp_st        = $arr_emp_info[$i]["emp_st"];

					if($i > 0)
					{
						$cond .= "or ";
					}

					$cond .= "(";

					$cond .= "(cl_wkfwmst.ref_dept_flg = '1' or (cl_wkfwmst.ref_dept_flg = '2' and ";
					$cond .= "exists (select * from cl_wkfw_refdept where cl_wkfw_refdept.wkfw_id = cl_wkfwmst.wkfw_id ";
					$cond .= "and cl_wkfw_refdept.class_id = $emp_class and cl_wkfw_refdept.atrb_id = $emp_attribute and cl_wkfw_refdept.dept_id = $emp_dept))) ";

					$cond .= "and ";

					$cond .= "(cl_wkfwmst.ref_st_flg = '1' or (cl_wkfwmst.ref_st_flg = '2' and exists (select * from cl_wkfw_refst where cl_wkfw_refst.wkfw_id = cl_wkfwmst.wkfw_id ";
					$cond .= "and cl_wkfw_refst.st_id = $emp_st)))";

					$cond .= ") ";
				}

				$cond .= "or exists (select * from cl_wkfw_refemp where cl_wkfw_refemp.wkfw_id = cl_wkfwmst.wkfw_id and cl_wkfw_refemp.emp_id = '$emp_id')";
				$cond .= ") ";
			}
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"]);
		}

		return $arr;
	}

	// ワークフロー数を算出
	function calc_workflow_for_count($folder_list, $screen_mode, $arr_emp_info, $emp_id)
	{

		$wkfw_list = $this->get_workflow_for_count($screen_mode, $arr_emp_info, $emp_id);
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}
		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 指定フォルダ配下のワークフロー数を算出
	function calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_wkfw_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}


	// ワークフローカテゴリ/フォルダ情報取得
	function get_workflow_folder_list_for_apply($emp_id, &$wkfw_counts)
	{

		$today = date("Ymd");

		// 職員の部署・役職取得
		if($emp_id != "")
		{
			$arr_emp_info = $this->get_emp_info($emp_id);
		}
		else
		{
			$arr_emp_info = "";
		}

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply($arr_emp_info, $emp_id);

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply("", $wkfw_type, $today, $arr_emp_info, $emp_id);

			$category_list[$i] = $category;
		}

		// フォルダごとのワークフロー数を算出
		$wkfw_counts = $this->calc_workflow_for_count($category_list, "apply", $arr_emp_info, $emp_id);

		return $category_list;
	}

	// ワークフローカテゴリ/フォルダ情報取得(申請参照一覧用)
	function get_workflow_folder_list_for_apply_refer(&$apply_counts)
	{

		// カテゴリ取得
		$category_list = $this->get_workflow_category_for_apply_refer();

		// フォルダ/ワークフロー取得
		foreach($category_list as $i => $category)
		{
			$wkfw_type = $category["wkfw_type"];
			$category["type"] = "category";
			$category["folders"] = $this->get_workflow_tree_for_apply_refer("", $wkfw_type);

			$category_list[$i] = $category;
		}

		// フォルダごとの申請数を算出
		$apply_counts = $this->calc_apply_for_count($category_list);

		return $category_list;
	}

    // ワークフローカテゴリ情報取得(申請参照一覧用)
	function get_workflow_category_for_apply_refer()
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from cl_wkfwcatemst";
		$cond  = "where not wkfwcate_del_flg ";
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}

	// フォルダ/ワークフロー情報取得(申請参照一覧用)
	function get_workflow_tree_for_apply_refer($parent_id, $category_id)
	{
		// フォルダ
		//$sql  = "select varchar(6) 'folder' as type, cl_wkfwfolder.wkfw_folder_id as id, cl_wkfwfolder.wkfw_type as cate, cl_wkfwfolder.wkfw_folder_name as name from cl_wkfwfolder ";
		$sql  = "select varchar(6) 'folder' as type, cast(cl_wkfwfolder.wkfw_folder_id as varchar(2)) as id, cl_wkfwfolder.wkfw_type as cate, cl_wkfwfolder.wkfw_folder_name as name from cl_wkfwfolder ";
		$sql .= "left join (select * from cl_wkfwtree where not wkfw_tree_del_flg) cl_wkfwtree on cl_wkfwfolder.wkfw_folder_id = cl_wkfwtree.wkfw_child_id ";
		$sql .= "where not cl_wkfwfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and cl_wkfwtree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and cl_wkfwtree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and cl_wkfwfolder.wkfw_type = $category_id ";
		}

		$sql .= "union all ";

		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, cl_wkfwmst.wkfw_id as id, cl_wkfwmst.wkfw_type as cate, cl_wkfwmst.wkfw_title as name from cl_wkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// 申請数を算出
	function calc_apply_for_count($folder_list)
	{

		$wkfw_list = $this->get_apply_for_count();
		$wkfw_count_per_folder = array();
		if (!is_null($wkfw_list))
		{
			foreach ($wkfw_list as $wkfw) {
				$cate_id = $wkfw["wkfw_type"];
				$folder_id = ($wkfw["wkfw_folder_id"] != "") ? $wkfw["wkfw_folder_id"] : "-";
				$doc_count_per_folder[$cate_id][$folder_id]++;
			}
		}

		if (!is_null($wkfw_list))
		{
			$wkfw_counts = array();
			foreach ($folder_list as $folder) {
				$this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
			}
		}
		return $wkfw_counts;
	}

	// 申請数取得(カウント用)
	function get_apply_for_count()
	{
		$sql  = "select a.wkfw_type, a.wkfw_folder_id, a.wkfw_id, b.apply_id ";
		$sql .= "from cl_wkfwmst a ";
		$sql .= "inner join cl_apply b on a.wkfw_id = b.wkfw_id ";
		// 2012/04/13 Yamagawa add(s)
		$sql .= "inner join empmst c on b.emp_id = c.emp_id ";
		$sql .= "inner join authmst auth on NOT auth.emp_del_flg AND auth.emp_id = c.emp_id ";
		// 2012/04/13 Yamagawa add(e)
		$cond = "where not wkfw_del_flg and not b.draft_flg and not b.delete_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_folder_id" => $row["wkfw_folder_id"], "wkfwr_id" => $row["wkfw_id"]);
		}

		return $arr;
	}

	// 指定フォルダ配下の申請数を算出
	function calc_apply_counts_per_folder($folder, $doc_count_per_folder, &$wkfw_counts) {

		if ($folder["type"] == "category")
		{
			$cate_id = $folder["wkfw_type"];
			$folder_id = "-";
			$wkfw_id = "-";
		}
		else if($folder["type"] == "folder")
		{
			$cate_id = $folder["cate"];
			$folder_id = $folder["id"];
			$wkfw_id = "-";
		}

		$wkfw_counts[$cate_id][$folder_id] = intval($doc_count_per_folder[$cate_id][$folder_id]);

		foreach ($folder["folders"] as $folder)
		{
			$wkfw_counts[$cate_id][$folder_id] += $this->calc_apply_counts_per_folder($folder, $doc_count_per_folder, $wkfw_counts);
		}

		return $wkfw_counts[$cate_id][$folder_id];
	}

	function get_apply_count($wkfw_id)
	{
		$sql  = "select count(*) as cnt from cl_apply ";
		// 2012/04/13 Yamagawa add(s)
		$sql .= "inner join empmst c on cl_apply.emp_id = c.emp_id ";
		$sql .= "inner join authmst auth on NOT auth.emp_del_flg AND auth.emp_id = c.emp_id ";
		// 2012/04/13 Yamagawa add(e)
		$cond = "where not draft_flg and not delete_flg and wkfw_id = '$wkfw_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}


	// フォルダ/ワークフロー情報取得
	function get_workflow_tree_for_apply($parent_id, $category_id, $today, $arr_emp_info, $emp_id)
	{

		// フォルダ
		//$sql  = "select varchar(6) 'folder' as type, cl_wkfwfolder.wkfw_folder_id as id, cl_wkfwfolder.wkfw_type as cate, cl_wkfwfolder.wkfw_folder_name as name from cl_wkfwfolder ";
		$sql  = "select varchar(6) 'folder' as type, cast(cl_wkfwfolder.wkfw_folder_id as varchar(2)) as id, cl_wkfwfolder.wkfw_type as cate, cl_wkfwfolder.wkfw_folder_name as name from cl_wkfwfolder ";
		$sql .= "left join (select * from cl_wkfwtree where not wkfw_tree_del_flg) cl_wkfwtree on cl_wkfwfolder.wkfw_folder_id = cl_wkfwtree.wkfw_child_id ";
		$sql .= "where not cl_wkfwfolder.wkfw_folder_del_flg ";

		if($parent_id == "")
		{
			$sql .= "and cl_wkfwtree.wkfw_parent_id is null ";
		}
		else
		{
			$sql .= "and cl_wkfwtree.wkfw_parent_id = $parent_id ";
		}
		if ($category_id != "") {
			$sql .= " and cl_wkfwfolder.wkfw_type = $category_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(cl_wkfwfolder.ref_dept_flg = '1' or (cl_wkfwfolder.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from cl_wkfwfolder_refdept where cl_wkfwfolder_refdept.wkfw_folder_id = cl_wkfwfolder.wkfw_folder_id ";
				$sql .= "and cl_wkfwfolder_refdept.class_id = $emp_class and cl_wkfwfolder_refdept.atrb_id = $emp_attribute and cl_wkfwfolder_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(cl_wkfwfolder.ref_st_flg = '1' or (cl_wkfwfolder.ref_st_flg = '2' and exists (select * from cl_wkfwfolder_refst where cl_wkfwfolder_refst.wkfw_folder_id = cl_wkfwfolder.wkfw_folder_id ";
				$sql .= "and cl_wkfwfolder_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}
			$sql .= "or exists (select * from cl_wkfwfolder_refemp where cl_wkfwfolder_refemp.wkfw_folder_id = cl_wkfwfolder.wkfw_folder_id and cl_wkfwfolder_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "union all ";


		// ワークフロー
		$sql .= "select varchar(6) 'file' as type, cl_wkfwmst.wkfw_id as id, cl_wkfwmst.wkfw_type as cate, cl_wkfwmst.wkfw_title as name from cl_wkfwmst ";
		$sql .= "where wkfw_type = $category_id and not wkfw_del_flg ";

		$sql .= "and ((wkfw_start_date <= '$today' and wkfw_end_date >= '$today') or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date <= '$today' and wkfw_end_date is null) or ";
		$sql .= "(wkfw_start_date is null and wkfw_end_date >= '$today')) ";

		if($parent_id == "")
		{
			$sql .= "and wkfw_folder_id is null ";
		}
		else if($parent_id != "")
		{
			$sql .= "and wkfw_folder_id = $parent_id ";
		}

		if($arr_emp_info != "")
		{
			$sql .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$sql .= "or ";
				}

				$sql .= "(";

				$sql .= "(cl_wkfwmst.ref_dept_flg = '1' or (cl_wkfwmst.ref_dept_flg = '2' and ";
				$sql .= "exists (select * from cl_wkfw_refdept where cl_wkfw_refdept.wkfw_id = cl_wkfwmst.wkfw_id ";
				$sql .= "and cl_wkfw_refdept.class_id = $emp_class and cl_wkfw_refdept.atrb_id = $emp_attribute and cl_wkfw_refdept.dept_id = $emp_dept))) ";

				$sql .= "and ";

				$sql .= "(cl_wkfwmst.ref_st_flg = '1' or (cl_wkfwmst.ref_st_flg = '2' and exists (select * from cl_wkfw_refst where cl_wkfw_refst.wkfw_id = cl_wkfwmst.wkfw_id ";
				$sql .= "and cl_wkfw_refst.st_id = $emp_st)))";

				$sql .= ") ";
			}

			$sql .= "or exists (select * from cl_wkfw_refemp where cl_wkfw_refemp.wkfw_id = cl_wkfwmst.wkfw_id and cl_wkfw_refemp.emp_id = '$emp_id')";
			$sql .= ") ";
		}

		$sql .= "order by type desc, id asc ";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$tree = array();
		while ($row = pg_fetch_array($sel))
		{
			$type               = $row["type"];
			$folder_id          = $row["id"];
			$category_type      = $row["cate"];
			$wkfw_folder_name   = $row["name"];

			if($type == "folder")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => $this->get_workflow_tree_for_apply($folder_id, $category_type, $today, $arr_emp_info, $emp_id)
				);
			}
			else if($type == "file")
			{
				$tree[] = array(
					"type" => $type,
					"cate" => $category_type,
					"parent_id" => $parent_id,
					"id" => $folder_id,
					"name" => $wkfw_folder_name,
					"folders" => array()
				);
			}
		}
		return $tree;
	}

	// カテゴリ名取得
	function get_category_from_folder_id($folder_id)
	{
		$sql  = "select wkfw_type, wkfw_nm from cl_wkfwcatemst ";
		$cond = "where wkfw_type in (select wkfw_type from cl_wkfwfolder where wkfw_folder_id = $folder_id) and not wkfwcate_del_flg ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$wkfw_type        = pg_fetch_result($sel, 0, "wkfw_type");
		$wkfw_nm = pg_fetch_result($sel, 0, "wkfw_nm");

		$arr[] = array("wkfw_type" => $wkfw_type, "wkfw_nm" => $wkfw_nm);

		return $arr;
	}

    // ワークフローカテゴリ情報取得(申請画面用)
	function get_workflow_category_for_apply($arr_emp_info, $emp_id)
	{
		// カテゴリ情報取得
		$sql   = "select wkfw_type, wkfw_nm from cl_wkfwcatemst";
		$cond  = "where not wkfwcate_del_flg ";

		if($arr_emp_info !=  "")
		{
			$cond .= "and (";

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$emp_class     = $arr_emp_info[$i]["emp_class"];
				$emp_attribute = $arr_emp_info[$i]["emp_attribute"];
				$emp_dept      = $arr_emp_info[$i]["emp_dept"];
				$emp_st        = $arr_emp_info[$i]["emp_st"];

				if($i > 0)
				{
					$cond .= "or ";
				}

				$cond .= "(";

				$cond .= "(ref_dept_flg = '1' or (ref_dept_flg = '2' and ";
				$cond .= "exists (select * from cl_wkfwcate_refdept where cl_wkfwcate_refdept.wkfw_type = cl_wkfwcatemst.wkfw_type ";
				$cond .= "and cl_wkfwcate_refdept.class_id = $emp_class and cl_wkfwcate_refdept.atrb_id = $emp_attribute and cl_wkfwcate_refdept.dept_id = $emp_dept))) ";

				$cond .= "and ";

				$cond .= "(ref_st_flg = '1' or (ref_st_flg = '2' and exists (select * from cl_wkfwcate_refst where cl_wkfwcate_refst.wkfw_type = cl_wkfwcatemst.wkfw_type ";
				$cond .= "and cl_wkfwcate_refst.st_id = $emp_st)))";

				$cond .= ") ";
			}

			$cond .= "or exists (select * from cl_wkfwcate_refemp where cl_wkfwcate_refemp.wkfw_type = cl_wkfwcatemst.wkfw_type and cl_wkfwcate_refemp.emp_id = '$emp_id')";
			$cond .= ") ";
		}
		$cond .= "order by wkfw_type asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "wkfw_nm" => $row["wkfw_nm"]);
		}
		return $arr;
	}


	// 職員の部署役職取得
	function get_emp_info($emp_id, $concurrent_flg = true)
	{
		// 職員情報取得(複数部署役職対応)
		$sql  = "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from empmst where emp_id = '$emp_id' ";
		if ($concurrent_flg) {
			$sql .= "union ";
			$sql .= "select emp_class, emp_attribute, emp_dept, emp_room, emp_st from concurrent where emp_id = '$emp_id'";
		}

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_class" => $row["emp_class"], "emp_attribute" => $row["emp_attribute"], "emp_dept" => $row["emp_dept"], "emp_room" => $row["emp_room"], "emp_st" => $row["emp_st"]);
		}

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// アクセス権限関連
//-------------------------------------------------------------------------------------------------------------------------

	// 部門一覧取得
	function get_classmst()
	{
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by class_id";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課一覧取得
	function get_atrbmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.class_id, atrbmst.atrb_id";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科一覧取得
	function get_deptmst()
	{
		$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
		$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.class_id, atrbmst.atrb_id, deptmst.dept_id";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 役職一覧取得
	function get_stmst()
	{
		$sql = "select st_id, st_nm from stmst";
		$cond = "where st_del_flg = 'f' order by order_no";
		$sel_st = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_st == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_st;
	}

	// カテゴリ用アクセス権（科）登録
	function delete_regist_wkfwcate_refdept($wkfw_type, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwcate_refdept";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into cl_wkfwcate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（役職）登録
	function delete_regist_wkfwcate_refst($wkfw_type, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwcate_refst";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into cl_wkfwcate_refst (wkfw_type, st_id) values (";
			$content = array($wkfw_type, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// カテゴリ用アクセス権（職員）登録
	function delete_regist_wkfwcate_refemp($wkfw_type, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwcate_refemp";
			$cond = "where wkfw_type = $wkfw_type";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into cl_wkfwcate_refemp (wkfw_type, emp_id) values (";
			$content = array($wkfw_type, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}


	// フォルダ用アクセス権（科）登録
	function delete_regist_wkfwfolder_refdept($wkfw_folder_id, $arr_ref_dept, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwfolder_refdept";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);
			$sql = "insert into cl_wkfwfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（役職）登録
	function delete_regist_wkfwfolder_refst($wkfw_folder_id, $arr_ref_st, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwfolder_refst";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_st as $ref_st)
		{
			$sql = "insert into cl_wkfwfolder_refst (wkfw_folder_id, st_id) values (";
			$content = array($wkfw_folder_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// フォルダ用アクセス権（職員）登録
	function delete_regist_wkfwfolder_refemp($wkfw_folder_id, $arr_ref_emp, $mode)
	{
		if($mode == "UPD")
		{
			$sql = "delete from cl_wkfwfolder_refemp";
			$cond = "where wkfw_folder_id = $wkfw_folder_id";
			$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($del == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		foreach ($arr_ref_emp as $ref_emp) {
			$sql = "insert into cl_wkfwfolder_refemp (wkfw_folder_id, emp_id) values (";
			$content = array($wkfw_folder_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_wkfw_refdept($wkfw_id, $arr_ref_dept, $mode)
	{
		foreach ($arr_ref_dept as $ref_dept)
		{
			list($class_id, $atrb_id, $dept_id) = split("-", $ref_dept);

			if($mode == "ALIAS")
			{
				$sql = "insert into cl_wkfw_refdept ";
			}
			else
			{
				$sql = "insert into cl_wkfw_refdept_real ";
			}
			$sql .= "(wkfw_id, class_id, atrb_id, dept_id) values (";
			$content = array($wkfw_id, $class_id, $atrb_id, $dept_id);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（科）削除
	function delete_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfw_refdept";
		}
		else
		{
			$sql = "delete from cl_wkfw_refdept_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_wkfw_refst($wkfw_id, $arr_ref_st, $mode)
	{
		foreach ($arr_ref_st as $ref_st)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into cl_wkfw_refst ";
			}
			else
			{
				$sql = "insert into cl_wkfw_refst_real ";
			}

			$sql .= "(wkfw_id, st_id) values (";
			$content = array($wkfw_id, $ref_st);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（役職）削除
	function delete_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfw_refst";
		}
		else
		{
			$sql = "delete from cl_wkfw_refst_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_wkfw_refemp($wkfw_id, $arr_ref_emp, $mode)
	{
		foreach ($arr_ref_emp as $ref_emp)
		{
			if($mode == "ALIAS")
			{
				$sql = "insert into cl_wkfw_refemp ";
			}
			else
			{
				$sql = "insert into cl_wkfw_refemp_real ";
			}

			$sql .= "(wkfw_id, emp_id) values (";
			$content = array($wkfw_id, $ref_emp);
			$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
			if ($ins == 0) {
				pg_query($this->_db_con, "rollback");
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}

	// ワークフロー用アクセス権（職員）削除
	function delete_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfw_refemp";
		}
		else
		{
			$sql = "delete from cl_wkfw_refemp_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）取得
	function get_wkfwcate_refdept($wkfw_type)
	{
		$sql = "select wkfw_type, class_id, atrb_id, dept_id from cl_wkfwcate_refdept";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// カテゴリ用アクセス権（役職）取得
	function get_wkfwcate_refst($wkfw_type)
	{
		$sql = "select wkfw_type, st_id from cl_wkfwcate_refst";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // カテゴリ用アクセス権（職員）取得
	function get_wkfwcate_refemp($wkfw_type)
	{
		$sql = "select wkfw_type, emp_id from cl_wkfwcate_refemp";
		$cond = "where wkfw_type = $wkfw_type";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_type" => $row["wkfw_type"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（科）取得
	function get_wkfwfolder_refdept($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, class_id, atrb_id, dept_id from cl_wkfwfolder_refdept";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// フォルダ用アクセス権（役職）取得
	function get_wkfwfolder_refst($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, st_id from cl_wkfwfolder_refst";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // フォルダ用アクセス権（職員）取得
	function get_wkfwfolder_refemp($wkfw_folder_id)
	{
		$sql = "select wkfw_folder_id, emp_id from cl_wkfwfolder_refemp";
		$cond = "where wkfw_folder_id = $wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_folder_id" => $row["wkfw_folder_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（科）取得
	function get_wkfw_refdept($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from cl_wkfw_refdept";
		}
		else
		{
			$sql = "select wkfw_id, class_id, atrb_id, dept_id from cl_wkfw_refdept_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "class_id" => $row["class_id"], "atrb_id" => $row["atrb_id"], "dept_id" => $row["dept_id"]);
		}
		return $arr;
	}

	// ワークフロー用アクセス権（役職）取得
	function get_wkfw_refst($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, st_id from cl_wkfw_refst";
		}
		else
		{
			$sql = "select wkfw_id, st_id from cl_wkfw_refst_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "st_id" => $row["st_id"]);
		}
		return $arr;
	}

    // ワークフロー用アクセス権（職員）取得
	function get_wkfw_refemp($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select wkfw_id, emp_id from cl_wkfw_refemp";
		}
		else
		{
			$sql = "select wkfw_id, emp_id from cl_wkfw_refemp_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "emp_id" => $row["emp_id"]);
		}
		return $arr;
	}


	// 結果通知管理情報取得
	function get_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select * from cl_wkfwnoticemng";
		}
		else
		{
			$sql  = "select * from cl_wkfwnoticemng_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$notice_target_class_div = pg_fetch_result($sel, 0, "target_class_div");
        $rslt_ntc_div0_flg = pg_fetch_result($sel, 0, "rslt_ntc_div0_flg");
        $rslt_ntc_div1_flg = pg_fetch_result($sel, 0, "rslt_ntc_div1_flg");
        $rslt_ntc_div2_flg = pg_fetch_result($sel, 0, "rslt_ntc_div2_flg");
        $rslt_ntc_div3_flg = pg_fetch_result($sel, 0, "rslt_ntc_div3_flg");
        $rslt_ntc_div4_flg = pg_fetch_result($sel, 0, "rslt_ntc_div4_flg");

		return array(
                       "notice_target_class_div" => $notice_target_class_div,
                       "rslt_ntc_div0_flg" => $rslt_ntc_div0_flg,
                       "rslt_ntc_div1_flg" => $rslt_ntc_div1_flg,
                       "rslt_ntc_div2_flg" => $rslt_ntc_div2_flg,
                       "rslt_ntc_div3_flg" => $rslt_ntc_div3_flg,
                       "rslt_ntc_div4_flg" => $rslt_ntc_div4_flg
		              );
	}

	// 部署役職指定(結果通知)情報取得
	function get_wkfwnoticestdtl($wkfw_id, $st_div, $mode)
	{
		$sql   = "select a.*, b.st_nm ";
		if($mode == "ALIAS")
		{
			$sql  .= "from cl_wkfwnoticestdtl a ";
		}
		else
		{
			$sql  .= "from cl_wkfwnoticestdtl_real a ";
		}
		$sql  .= "inner join stmst b on a.st_id = b.st_id ";
		$cond  = "where a.wkfw_id = '$wkfw_id' and st_div = $st_div";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"], "st_nm" => $row["st_nm"]);

		}
		return $arr;
	}

	// 職員指定(結果通知)情報取得
	function get_wkfwnoticedtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from cl_wkfwnoticedtl a ";
		}
		else
		{
			$sql .= "from cl_wkfwnoticedtl_real a ";
		}
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$cond = "where a.wkfw_id = '$wkfw_id' ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("emp_id" => $row["emp_id"], "emp_lt_nm" => $row["emp_lt_nm"], "emp_ft_nm" => $row["emp_ft_nm"]);

		}
		return $arr;
	}

	// 委員会・ＷＧ指定(結果通知)情報取得
	function get_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
        $sql  = "select pjt_id, pjt_name from project ";
		if($mode == "ALIAS")
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from cl_wkfwnoticepjtdtl where wkfw_id = '$wkfw_id') ";
		}
		else
		{
	        $sql .= "where pjt_id in (select parent_pjt_id from cl_wkfwnoticepjtdtl_real where wkfw_id = '$wkfw_id') ";
		}
		$sql .= "union all ";

		$sql .= "select pjt_id, pjt_name from project ";

		if($mode == "ALIAS")
		{
			$sql .= "where pjt_id in (select child_pjt_id from cl_wkfwnoticepjtdtl where wkfw_id = '$wkfw_id')";
		}
		else
		{
			$sql .= "where pjt_id in (select child_pjt_id from cl_wkfwnoticepjtdtl_real where wkfw_id = '$wkfw_id')";
		}

		$cond = "";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		if(pg_numrows($sel) == 1)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm);

		}
		else if(pg_numrows($sel) == 2)
		{
			$pjt_parent_id = pg_fetch_result($sel, 0, "pjt_id");
			$pjt_parent_nm = pg_fetch_result($sel, 0, "pjt_name");
			$pjt_child_id = pg_fetch_result($sel, 1, "pjt_id");
			$pjt_child_nm = pg_fetch_result($sel, 1, "pjt_name");

			$arr[] = array("pjt_parent_id" => $pjt_parent_id, "pjt_parent_nm" => $pjt_parent_nm, "pjt_child_id" => $pjt_child_id, "pjt_child_nm" => $pjt_child_nm);
		}
		return $arr;
	}

	// 部署役職指定(結果通知)(部署指定)情報取得
	function get_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
		if($mode == "ALIAS")
		{
			$sql .= "from cl_wkfwnoticesectdtl a ";
		}
		else
		{
			$sql .= "from cl_wkfwnoticesectdtl_real a ";
		}
		$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
		$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
		$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
		$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
		$cond = "where a.wkfw_id = '$wkfw_id' ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		$class_nm = pg_fetch_result($sel, 0, "class_nm");
		$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
		$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
		$room_nm = pg_fetch_result($sel, 0, "room_nm");

		return array("class_id" => $class_id,
         			   "atrb_id" => $atrb_id,
				       "dept_id" => $dept_id,
				       "room_id" => $room_id,
         			   "class_nm" => $class_nm,
         			   "atrb_nm" => $atrb_nm,
				       "dept_nm" => $dept_nm,
				       "room_nm" => $room_nm);

	}


//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー登録・更新・削除関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理登録
	function regist_wkfwapvmng($wkfw_id, $apv_order, $approve_mng, $mode)
	{

		$deci_flg         = $approve_mng["deci_flg"];
		$target_class_div = $approve_mng["target_class_div"];
		$multi_apv_flg    = $approve_mng["multi_apv_flg"];
		$next_notice_div  = $approve_mng["next_notice_div"];
		$apv_div0_flg     = $approve_mng["apv_div0_flg"];
		$apv_div1_flg     = $approve_mng["apv_div1_flg"];
		$apv_div2_flg     = $approve_mng["apv_div2_flg"];
		$apv_div3_flg     = $approve_mng["apv_div3_flg"];
		$apv_div4_flg     = $approve_mng["apv_div4_flg"];
		$apv_div5_flg     = $approve_mng["apv_div5_flg"];
		$apv_div6_flg     = $approve_mng["apv_div6_flg"];
		$apv_div7_flg     = $approve_mng["apv_div7_flg"];
		$apv_div8_flg     = $approve_mng["apv_div8_flg"];
		$apv_div9_flg     = $approve_mng["apv_div9_flg"];
		$apv_div10_flg    = $approve_mng["apv_div10_flg"];
		$apv_div11_flg    = $approve_mng["apv_div11_flg"];
		$apv_div12_flg    = $approve_mng["apv_div12_flg"];
		$apv_num          = $approve_mng["apv_num"];

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwapvmng ";
		}
		else
		{
			$sql = "insert into cl_wkfwapvmng_real ";
		}

		$sql .= "(wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num, apv_div5_flg, apv_div6_flg, apv_div7_flg, apv_div8_flg, apv_div9_flg, apv_div10_flg, apv_div11_flg, apv_div12_flg) values(";
		$content = array($wkfw_id, $apv_order, $deci_flg, $target_class_div, $multi_apv_flg, $next_notice_div, $apv_div0_flg, $apv_div1_flg, $apv_div2_flg, $apv_div3_flg, $apv_div4_flg, $apv_num, $apv_div5_flg, $apv_div6_flg, $apv_div7_flg, $apv_div8_flg, $apv_div9_flg, $apv_div10_flg, $apv_div11_flg, $apv_div12_flg);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定登録
	function regist_wkfwapvpstdtl($wkfw_id, $apv_order, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwapvpstdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwapvpstdtl_real ";
		}
		$sql .= "(wkfw_id, apv_order, st_id, st_div) values(";
		$content = array($wkfw_id, $apv_order, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_wkfwapvdtl($wkfw_id, $apv_order, $emp_id, $apv_sub_order, $mode)
	{

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwapvdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwapvdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, emp_id, apv_sub_order) values(";
		$content = array($wkfw_id, $apv_order, $emp_id, $apv_sub_order);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ登録
	function regist_wkfwpjtdtl($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwpjtdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwpjtdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $apv_order, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function regist_wkfwapvsectdtl($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{

		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwapvsectdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwapvsectdtl_real ";
		}

		$sql .= "(wkfw_id, apv_order, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $apv_order, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_wkfwfile($wkfw_id, $wkfwfile_no, $wkfwfile_name, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwfile ";
		}
		else
		{
			$sql = "insert into cl_wkfwfile_real ";
		}

		$sql .= "(wkfw_id, wkfwfile_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_name);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_wkfwnoticemng($arr, $mode)
	{
		$wkfw_id           = $arr["wkfw_id"];
		$target_class_div  = $arr["target_class_div"];
		$rslt_ntc_div0_flg = $arr["rslt_ntc_div0_flg"];
		$rslt_ntc_div1_flg = $arr["rslt_ntc_div1_flg"];
		$rslt_ntc_div2_flg = $arr["rslt_ntc_div2_flg"];
		$rslt_ntc_div3_flg = $arr["rslt_ntc_div3_flg"];
		$rslt_ntc_div4_flg = $arr["rslt_ntc_div4_flg"];

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwnoticemng ";
		}
		else
		{
			$sql = "insert into cl_wkfwnoticemng_real ";
		}
		$sql .= "(wkfw_id, target_class_div, rslt_ntc_div0_flg, rslt_ntc_div1_flg, rslt_ntc_div2_flg, rslt_ntc_div3_flg, rslt_ntc_div4_flg) values(";
		$content = array($wkfw_id, $target_class_div, $rslt_ntc_div0_flg, $rslt_ntc_div1_flg, $rslt_ntc_div2_flg, $rslt_ntc_div3_flg, $rslt_ntc_div4_flg);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_wkfwnoticestdtl($wkfw_id, $st_id, $st_div, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwnoticestdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwnoticestdtl_real ";
		}
		$sql .= "(wkfw_id, st_id, st_div) values(";
		$content = array($wkfw_id, $st_id, $st_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_wkfwnoticedtl($wkfw_id, $emp_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwnoticedtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwnoticedtl_real ";
		}
		$sql .= "(wkfw_id, emp_id) values(";
		$content = array($wkfw_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_wkfwnoticepjtdtl($wkfw_id, $pjt_parent_id, $pjt_child_id, $mode)
	{
		$pjt_child_id = ($pjt_child_id == "") ? null : $pjt_child_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwnoticepjtdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwnoticepjtdtl_real ";
		}
		$sql .= "(wkfw_id, parent_pjt_id, child_pjt_id) values(";
		$content = array($wkfw_id, $pjt_parent_id, $pjt_child_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_wkfwnoticesectdtl($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id, $mode)
	{
		$atrb_id = ($atrb_id == "") ? null : $atrb_id;
		$dept_id = ($dept_id == "") ? null : $dept_id;
		$room_id = ($room_id == "") ? null : $room_id;

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwnoticesectdtl ";
		}
		else
		{
			$sql = "insert into cl_wkfwnoticesectdtl_real ";
		}
		$sql .= "(wkfw_id, class_id, atrb_id, dept_id, room_id) values(";
		$content = array($wkfw_id, $class_id, $atrb_id, $dept_id, $room_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 前提とする申請書(ワークフロー用)登録
	function regist_wkfwfprecond($wkfw_id, $precond_order, $precond_wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwfprecond ";
		}
		else
		{
			$sql = "insert into cl_wkfwfprecond_real ";
		}

		$sql .= "(wkfw_id, precond_order, precond_wkfw_id) values(";
		$content = array($wkfw_id, $precond_order, $precond_wkfw_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// 承認者管理削除
	function delete_wkfwapvmng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwapvmng";
		}
		else
		{
			$sql = "delete from cl_wkfwapvmng_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職削除
	function delete_wkfwapvpstdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwapvpstdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwapvpstdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定削除
	function delete_wkfwapvdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwapvdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwapvdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ削除
	function delete_wkfwpjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwpjtdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwpjtdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)
	function delete_wkfwapvsectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwapvsectdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwapvsectdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理削除
	function delete_wkfwnoticemng($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwnoticemng";
		}
		else
		{
			$sql = "delete from cl_wkfwnoticemng_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)削除
	function delete_wkfwnoticestdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwnoticestdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwnoticestdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)削除
	function delete_wkfwnoticedtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwnoticedtl";
		}
		else
		{
			$sql = "delete from cl_wkfwnoticedtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)削除
	function delete_wkfwnoticepjtdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwnoticepjtdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwnoticepjtdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)削除
	function delete_wkfwnoticesectdtl($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwnoticesectdtl";
		}
		else
		{
			$sql = "delete from cl_wkfwnoticesectdtl_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_wkfwfile($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwfile";
		}
		else
		{
			$sql = "delete from cl_wkfwfile_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)
	function delete_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "delete from cl_wkfwfprecond";
		}
		else
		{
			$sql = "delete from cl_wkfwfprecond_real";
		}
		$cond = "where wkfw_id = '$wkfw_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}



	// ワークフロー登録
	function regist_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];

		if($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfwmst ";
		}
		else
		{
			$sql = "insert into cl_wkfwmst_real ";
		}

		$sql .= "(wkfw_id, wkfw_type, wkfw_title, wkfw_content, wkfw_start_date, wkfw_end_date, wkfw_appr, wkfw_content_type, wkfw_folder_id, ref_dept_st_flg, ref_dept_flg, ref_st_flg, short_wkfw_name, apply_title_disp_flg) values (";
		$content = array($wkfw_id, $wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー更新
	function update_wkfwmst($arr, $mode)
	{
		$wkfw_id = $arr["wkfw_id"];
		$wkfw_type = $arr["wkfw_type"];
		$wkfw_title = $arr["wkfw_title"];
		$wkfw_content = $arr["wkfw_content"];
		$start_date = $arr["start_date"];
		$end_date = $arr["end_date"];
		$wkfw_appr = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$wkfw_folder_id = $arr["wkfw_folder_id"];
		$ref_dept_st_flg = $arr["ref_dept_st_flg"];
		$ref_dept_flg = $arr["ref_dept_flg"];
		$ref_st_flg = $arr["ref_st_flg"];
		$short_wkfw_name = $arr["short_wkfw_name"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];

		if($mode == "ALIAS")
		{
			$sql = "update cl_wkfwmst set";
		}
		else
		{
			$sql = "update cl_wkfwmst_real set";
		}
		$set = array("wkfw_type", "wkfw_title", "wkfw_content", "wkfw_start_date", "wkfw_end_date", "wkfw_appr", "wkfw_content_type", "wkfw_folder_id", "ref_dept_st_flg", "ref_dept_flg", "ref_st_flg", "short_wkfw_name", "apply_title_disp_flg");
		$setvalue = array($wkfw_type, pg_escape_string($wkfw_title), pg_escape_string($wkfw_content), $start_date, $end_date, $wkfw_appr, $wkfw_content_type, $wkfw_folder_id, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, pg_escape_string($short_wkfw_name), $apply_title_disp_flg);
		$cond = "where wkfw_id = '$wkfw_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 略称取得
	function get_short_wkfw_name_used_cnt($short_wkfw_name, $wkfw_id)
	{
		$sql  .= "select count(*) as cnt from cl_wkfwmst_real";
		$cond .= "where short_wkfw_name = '$short_wkfw_name'";

		if($wkfw_id != "")
		{
			$cond .= "and wkfw_id <> '$wkfw_id'";
		}

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理取得
	function get_wkfwapvmng($wkfw_id)
	{
		$sql  = "select wkfw_id, apv_order, deci_flg, target_class_div, multi_apv_flg, next_notice_div, apv_div0_flg, apv_div1_flg, apv_div2_flg, apv_div3_flg, apv_div4_flg, apv_num, apv_div5_flg, apv_div6_flg, apv_div7_flg, apv_div8_flg, apv_div9_flg, apv_div10_flg, apv_div11_flg, apv_div12_flg ";
		$sql .= "from cl_wkfwapvmng ";
		$cond = "where wkfw_id = '$wkfw_id' order by apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfw_id" => $row["wkfw_id"],
                            "apv_order" => $row["apv_order"],
                            "deci_flg" => $row["deci_flg"],
                            "target_class_div" => $row["target_class_div"],
                            "multi_apv_flg" => $row["multi_apv_flg"],
                            "next_notice_div" => $row["next_notice_div"],
							"apv_div0_flg" => $row["apv_div0_flg"],
							"apv_div1_flg" => $row["apv_div1_flg"],
							"apv_div2_flg" => $row["apv_div2_flg"],
							"apv_div3_flg" => $row["apv_div3_flg"],
							"apv_div4_flg" => $row["apv_div4_flg"],
							"apv_div5_flg" => $row["apv_div5_flg"],
							"apv_div6_flg" => $row["apv_div6_flg"],
							"apv_div7_flg" => $row["apv_div7_flg"],
							"apv_div8_flg" => $row["apv_div8_flg"],
							"apv_div9_flg" => $row["apv_div9_flg"],
							"apv_div10_flg" => $row["apv_div10_flg"],
							"apv_div11_flg" => $row["apv_div11_flg"],
							"apv_div12_flg" => $row["apv_div12_flg"],
							"apv_num" => $row["apv_num"]
                           );
		}
		return $arr;
	}

	// 部署役職指定情報取得
	function get_wkfwapvpstdtl($wkfw_id, $apv_order, $target_class_div, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id, false);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}

		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from cl_wkfwapvpstdtl where empmst.emp_st = cl_wkfwapvpstdtl.st_id and wkfw_id = '$wkfw_id' and apv_order = $apv_order and st_div = 0) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "exists (select st_id from cl_wkfwapvpstdtl where concurrent.emp_st = cl_wkfwapvpstdtl.st_id and wkfw_id = '$wkfw_id' and apv_order = $apv_order and st_div = 0) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
   			                "apv_sub_order" => $idx
			               );
			$idx++;
		}
		return $arr;
	}

	// 職員指定情報取得
	function get_wkfwapvdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.emp_id, a.apv_sub_order, b.emp_lt_nm, b.emp_ft_nm, d.st_nm from cl_wkfwapvdtl a ";
		$sql  .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "inner join authmst c on a.emp_id = c.emp_id ";
		$sql  .= "and c.emp_del_flg = 'f' ";
		$sql  .= "inner join stmst d on b.emp_st = d.st_id ";
		$cond  = "where a.wkfw_id = '$wkfw_id' and a.apv_order = $apv_order ";
		$cond .= "order by apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $row["apv_sub_order"]
			               );
		}
		return $arr;
	}

	// 委員会・ＷＧ指定情報取得
	function get_wkfwpjtdtl($wkfw_id, $apv_order)
	{
		$sql  = "select parent_pjt_id, child_pjt_id from cl_wkfwpjtdtl ";
		$cond = "where wkfw_id = '$wkfw_id' and apv_order = $apv_order";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$parent_pjt_id = pg_fetch_result($sel, 0, "parent_pjt_id");
		$child_pjt_id  = pg_fetch_result($sel, 0, "child_pjt_id");

		return array("parent_pjt_id" => $parent_pjt_id, "child_pjt_id" => $child_pjt_id);

	}


	// 委員会・ＷＧメンバー取得
	function get_project_member($parent_pjt_id, $child_pjt_id, $except_secretariat_flg = false)
	{
		$sql  = "select emp.project_no, emp.pjt_response as emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, varchar(1) '0' as member_kind ";
		$sql .= "from ";
		$sql .= "( ";

        // 委員会(責任者)
		$sql .= "select varchar(1) '1' as project_no, project.pjt_id, project.pjt_response ";
		$sql .= "from project ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

        // ＷＧ(責任者)
	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '3' as project_no, project.pjt_id, project.pjt_response ";
			$sql .= "from project ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.pjt_response = empmst.emp_id ";
		$sql .= "inner join authmst on emp.pjt_response = authmst.emp_id and not emp_del_flg ";

		$sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on empmst.emp_room = classroom.room_id ";

		$sql .= "left join stmst on empmst.emp_st = stmst.st_id ";

		$sql .= "union all ";

		$sql .= "select emp.project_no, emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, emp.member_kind ";
		$sql .= "from ";
		$sql .= "( ";

        // 委員会(事務局・メンバー)
		$sql .= "select varchar(1) '2' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id, promember.member_kind ";
		$sql .= "from project ";
		$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
		$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id is null and project.pjt_id = $parent_pjt_id ";

        // ＷＧ(事務局・メンバー)
	 	if($child_pjt_id != "")
		{
			$sql .= "union all ";
			$sql .= "select varchar(1) '4' as project_no, project.pjt_id, promember.emp_id, promember.pjt_member_id, promember.member_kind  ";
			$sql .= "from project ";
			$sql .= "inner join promember on project.pjt_id = promember.pjt_id ";
			$sql .= "where project.pjt_public_flag and not project.pjt_delete_flag and project.pjt_parent_id = $parent_pjt_id and project.pjt_id = $child_pjt_id ";
		}

		$sql .= ") emp ";

		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not emp_del_flg ";

		$sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on empmst.emp_room = classroom.room_id ";

		$sql .= "left join stmst on empmst.emp_st = stmst.st_id ";
		$cond .= "order by project_no ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_id      = $row["emp_id"];
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$st_nm       = $row["st_nm"];

			$dpl_flg = false;

			// 重複があればセットしない。
			foreach($arr as $arr_apv)
			{
				if($emp_id == $arr_apv["emp_id"])
				{
					$dpl_flg = true;
					break;
				}
			}
			if ($dpl_flg) {
				continue;
			}

			// 事務局除外フラグが立っていたら、事務局メンバーをセットしない
			if ($except_secretariat_flg && $row["member_kind"] == "1") {
				continue;
			}

			$arr[] = array(
                            "emp_id"        => $row["emp_id"],
                            "emp_full_nm"   => $emp_full_nm,
                            "emp_lt_nm"     => $row["emp_lt_nm"],
                            "emp_ft_nm"     => $row["emp_ft_nm"],
                            "class_nm"      => $row["class_nm"],
                            "atrb_nm"       => $row["atrb_nm"],
                            "dept_nm"       => $row["dept_nm"],
                            "room_nm"       => $row["room_nm"],
                            "st_nm"         => $row["st_nm"],
                            "apv_sub_order" => $idx
                            );
            $idx++;
		}
		return $arr;
	}


	// 申請者以外の結果通知者取得
	function get_wkfw_notice_for_apply($wkfw_id,
	                                    $notice_target_class_div,
                                        $rslt_ntc_div0_flg,
                                        $rslt_ntc_div1_flg,
                                        $rslt_ntc_div2_flg,
                                        $rslt_ntc_div3_flg,
                                        $rslt_ntc_div4_flg,
	                                    $emp_id)
	{
		$arr_tmp = array();

		// 部署役職指定(申請書所属)
		if($rslt_ntc_div0_flg == "t")
		{
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "0", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

			for($i=0; $i<count($arr_apvpstdtl); $i++)
			{
				$arr_apvpstdtl[$i]["rslt_ntc_div"] = "0";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
		}

		// 部署役職指定(部署指定)
		if($rslt_ntc_div4_flg == "t")
		{
			$arrr_wkfwnoticesectdtl = $this->get_wkfwnoticesectdtl($wkfw_id, "ALIAS");
			$arr_wkfwnoticestdtl = $this->get_wkfwnoticestdtl($wkfw_id, "4", "ALIAS");
			$notice_st_id = "";
			foreach($arr_wkfwnoticestdtl as $wkfwnoticestdtl)
			{
				if($notice_st_id != "")
				{
					$notice_st_id .= ",";
				}
				$notice_st_id .= $wkfwnoticestdtl["st_id"];
			}
			$arr_post_sect = $this->get_emp_info_for_post_sect($arrr_wkfwnoticesectdtl["class_id"],
			                                        $arrr_wkfwnoticesectdtl["atrb_id"],
			                                        $arrr_wkfwnoticesectdtl["dept_id"],
			                                        $arrr_wkfwnoticesectdtl["room_id"],
			                                        $notice_st_id);

			for($i=0; $i<count($arr_post_sect); $i++)
			{
				$arr_post_sect[$i]["rslt_ntc_div"] = "4";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
		}

		// 職員指定
		if($rslt_ntc_div1_flg == "t")
		{
			$arr_emp_info = array();
			$arr_wkfwnoticedtl = $this->get_wkfwnoticedtl($wkfw_id, "ALIAS");
			foreach($arr_wkfwnoticedtl as $wkfwnoticedtl)
			{
				$arr_empmst_detail = $this->get_empmst_detail($wkfwnoticedtl["emp_id"]);
				$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
				$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
				$arr_emp_info[] = array(
								 "emp_id" => $arr_empmst_detail[0]["emp_id"],
								 "emp_full_nm" => $emp_full_nm,
								 "st_nm" => $arr_empmst_detail[0]["st_nm"]
								 );
			}

			for($i=0; $i<count($arr_emp_info); $i++)
			{
				$arr_emp_info[$i]["rslt_ntc_div"] = "1";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
		}

		// 委員会・ＷＧ指定
		if($rslt_ntc_div3_flg == "t")
		{
			$arr_wkfwnoticepjtdtl = $this->get_wkfwnoticepjtdtl($wkfw_id, "ALIAS");
			$arr_project_member = $this->get_project_member($arr_wkfwnoticepjtdtl[0]["pjt_parent_id"], $arr_wkfwnoticepjtdtl[0]["pjt_child_id"]);

			for($i=0; $i<count($arr_project_member); $i++)
			{
				$arr_project_member[$i]["rslt_ntc_div"] = "3";
			}

			// マージ処理
			$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}

		return $arr;
	}



	// 承認者情報取得
	function get_wkfwapv_info($wkfw_id, $emp_id, $short_wkfw_name)
	{
		$arr_wkfwapvmng = $this->get_wkfwapvmng($wkfw_id);

		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$apvmng["apv_setting_flg"] = "f";

			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg  = $apvmng["apv_div0_flg"];
			$apv_div1_flg  = $apvmng["apv_div1_flg"];
			$apv_div2_flg  = $apvmng["apv_div2_flg"];
			$apv_div3_flg  = $apvmng["apv_div3_flg"];
			$apv_div4_flg  = $apvmng["apv_div4_flg"];
			$apv_div5_flg  = $apvmng["apv_div5_flg"];
			$apv_div6_flg  = $apvmng["apv_div6_flg"];
			$apv_div7_flg  = $apvmng["apv_div7_flg"];
			$apv_div8_flg  = $apvmng["apv_div8_flg"];
			$apv_div9_flg  = $apvmng["apv_div9_flg"];
			$apv_div10_flg = $apvmng["apv_div10_flg"];
			$apv_div11_flg = $apvmng["apv_div11_flg"];
			$apv_div12_flg = $apvmng["apv_div12_flg"];

			if(
				(
					$apv_div2_flg == "t"
					|| $apv_div5_flg == "t"
					|| $apv_div6_flg == "t"
					|| $apv_div7_flg == "t"
				)
				&& $apv_div0_flg == "f"
				&& $apv_div1_flg == "f"
				&& $apv_div3_flg == "f"
				&& $apv_div4_flg == "f"
				&& $apv_div8_flg == "f"
				&& $apv_div9_flg == "f"
				&& $apv_div10_flg == "f"
				&& $apv_div11_flg == "f"
				&& $apv_div12_flg == "f"
			)
			{
				$apvmng["apv_setting_flg"] = "t";
			}

			// 所属長指定
			if($apv_div8_flg == "t"){

				// 所属長　対象部署取得
				$class_division = $this->get_supervisor_class();

				if ($class_division == 0) {

					// 所属長(部署指定無)取得
					$arr_wkfw_supervisor_apv = $this->get_supervisor_apv_class_none($emp_id);

				} else {

					// 所属長(部署指定有)取得
					$arr_wkfw_supervisor_apv = $this->get_supervisor_apv($emp_id,$class_division);

				}

				for($i=0; $i<count($arr_wkfw_supervisor_apv); $i++)
				{
					$arr_wkfw_supervisor_apv[$i]["apv_div"] = "8";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfw_supervisor_apv);

			}

			// 看護副部長指定
			if($apv_div9_flg == "t"){

				// 看護副部長(室)取得
				$arr_wkfw_nurse_deputy_manager_apv = $this->get_nurse_deputy_manager_apv($emp_id,4);

				// 看護副部長(室)が未設定の場合
				if (count($arr_wkfw_nurse_deputy_manager_apv) == 0) {

					// 看護副部長(科)取得
					$arr_wkfw_nurse_deputy_manager_apv = $this->get_nurse_deputy_manager_apv($emp_id,3);

				}

				// 看護副部長(室・科)が未設定の場合
				if (count($arr_wkfw_nurse_deputy_manager_apv) == 0) {

					// 看護副部長(課)取得
					$arr_wkfw_nurse_deputy_manager_apv = $this->get_nurse_deputy_manager_apv($emp_id,2);

				}

				// 看護副部長(室・科・課)が未設定の場合
				if (count($arr_wkfw_nurse_deputy_manager_apv) == 0) {

					// 看護副部長(事業所)取得
					$arr_wkfw_nurse_deputy_manager_apv = $this->get_nurse_deputy_manager_apv($emp_id,1);

				}

				// 看護副部長(室・科・課・事業所)が未設定の場合
				if (count($arr_wkfw_nurse_deputy_manager_apv) == 0) {

					// 看護副部長(組織)取得
					$arr_wkfw_nurse_deputy_manager_apv = $this->get_nurse_deputy_manager_apv_class_none($emp_id);

				}

				for($i=0; $i<count($arr_wkfw_nurse_deputy_manager_apv); $i++)
				{
					$arr_wkfw_nurse_deputy_manager_apv[$i]["apv_div"] = "9";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfw_nurse_deputy_manager_apv);

			}

			// 看護部長指定
			if($apv_div10_flg == "t"){

				// 看護部長　対象部署取得
				$class_division = $this->get_nurse_manager_class();

				if ($class_division == 0) {

					// 看護部長(部署指定無)取得
					$arr_wkfw_nurse_manager_apv = $this->get_nurse_manager_apv_class_none($emp_id);

				} else {

					// 看護部長(部署指定有)取得
					$arr_wkfw_nurse_manager_apv = $this->get_nurse_manager_apv($emp_id,$class_division);

				}

				for($i=0; $i<count($arr_wkfw_nurse_manager_apv); $i++)
				{
					$arr_wkfw_nurse_manager_apv[$i]["apv_div"] = "10";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfw_nurse_manager_apv);

			}

			// 審議会指定
			if($apv_div11_flg == "t"){

				// 2012/07/18 Yamagawa add(s)
				if ($short_wkfw_name == "c440"){
					$arr_wkfw_council_apv[0] = array();
				} else {
				// 2012/07/18 Yamagawa add(e)
					// 審議会取得
					$arr_wkfw_council_apv = $this->get_council_apv($emp_id);
				// 2012/07/18 Yamagawa add(s)
				}
				// 2012/07/18 Yamagawa add(e)

				for($i=0; $i<count($arr_wkfw_council_apv); $i++)
				{
					$arr_wkfw_council_apv[$i]["apv_div"] = "11";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfw_council_apv);

			}

			// 看護教育委員会指定
			if($apv_div12_flg == "t"){

				// 看護教育委員会取得
				$arr_wkfw_nurse_education_committee_apv = $this->get_nurse_education_committee_apv($emp_id);

				for($i=0; $i<count($arr_wkfw_nurse_education_committee_apv); $i++)
				{
					$arr_wkfw_nurse_education_committee_apv[$i]["apv_div"] = "12";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfw_nurse_education_committee_apv);

			}

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_wkfwapvpstdtl = $this->get_wkfwapvpstdtl($wkfw_id, $apvmng["apv_order"], $apvmng["target_class_div"], $emp_id);

				for($i=0; $i<count($arr_wkfwapvpstdtl); $i++)
				{
					$arr_wkfwapvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvpstdtl);

			}
			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{

  				$arr_apvpstdtl = $this->get_apvpstdtl($wkfw_id, $apvmng["apv_order"], "4");
				$st_sect_ids = "";
				foreach($arr_apvpstdtl as $apvpstdtl)
				{
					if($st_sect_ids != "")
					{
						$st_sect_ids .= ",";
					}
					$st_sect_ids .= $apvpstdtl["st_id"];
				}
				$arr_sect_dtl = $this->get_apvsectdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_post_sect = $this->get_emp_info_for_post_sect($arr_sect_dtl["class_id"], $arr_sect_dtl["atrb_id"], $arr_sect_dtl["dept_id"], $arr_sect_dtl["room_id"], $st_sect_ids);

				for($i=0; $i<count($arr_post_sect); $i++)
				{
					$arr_post_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_post_sect);
			}
			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_wkfwapvdtl = $this->get_wkfwapvdtl($wkfw_id, $apvmng["apv_order"]);
				for($i=0; $i<count($arr_wkfwapvdtl); $i++)
				{
					$arr_wkfwapvdtl[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_wkfwapvdtl);
			}
			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_pjt = $this->get_wkfwpjtdtl($wkfw_id, $apvmng["apv_order"]);
				$arr_project_member = $this->get_project_member($arr_pjt["parent_pjt_id"], $arr_pjt["child_pjt_id"], true);

				$pjt_nm = $this->get_pjt_nm($arr_pjt["parent_pjt_id"]);
				if($arr_pjt["child_pjt_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($arr_pjt["child_pjt_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
					$arr_project_member[$i]["parent_pjt_id"] = $arr_pjt["parent_pjt_id"];
					$arr_project_member[$i]["child_pjt_id"]  = $arr_pjt["child_pjt_id"];
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}

			// 主催者指定
			if($apv_div5_flg == "t"){

				$arr_setting_opener_apv[0] = array();

				$arr_setting_opener_apv[0]["apv_div"] = "5";

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_opener_apv);

			}

			// 講師指定
			if($apv_div6_flg == "t"){

				$arr_setting_teacher_apv[0] = array();

				$arr_setting_teacher_apv[0]["apv_div"] = "6";

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_teacher_apv);

			}

			// 課題提出先指定
			if($apv_div7_flg == "t"){

				$arr_setting_theme_present_apv[0] = array();

				$arr_setting_theme_present_apv[0]["apv_div"] = "7";

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_theme_present_apv);

			}

			// その他
			if($apv_div2_flg == "t")
			{
				$arr_setting_apv = "";
				for($i=0; $i<$apvmng["apv_num"]; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                               "emp_id" => $apv["emp_id"],
                                               "emp_full_nm" => $apv["emp_full_nm"],
                                               "st_nm" => $apv["st_nm"],
                                               "apv_div" => $apv["apv_div"],
                                               "pjt_nm" => $apv["pjt_nm"],
                                               "parent_pjt_id" => $apv["parent_pjt_id"],
                                               "child_pjt_id" => $apv["child_pjt_id"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if(
								$apv["apv_div"] == "2"
								|| $apv["apv_div"] == "5"
								|| $apv["apv_div"] == "6"
								|| $apv["apv_div"] == "7"
							)
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                           "emp_id" => $arr_apv[0]["emp_id"],
                                           "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                           "st_nm" => $arr_apv[0]["st_nm"],
                                           "apv_div" => $arr_apv[0]["apv_div"],
                                           "pjt_nm" => $arr_apv[0]["pjt_nm"],
                                           "parent_pjt_id" => $arr_apv[0]["parent_pjt_id"],
                                           "child_pjt_id" => $arr_apv[0]["child_pjt_id"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if(
							$arr_apv[0]["apv_div"] == "2"
							|| $arr_apv[0]["apv_div"] == "5"
							|| $arr_apv[0]["apv_div"] == "6"
							|| $arr_apv[0]["apv_div"] == "7"
						)
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;
	}

	// ワークフロー情報取得
	function get_wkfwmst($wkfw_id)
	{
		$sql  = "select * from cl_wkfwmst";
		$cond ="where wkfw_id = '$wkfw_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

// 2012/01/27 Yamagawa add(s)
	function get_wkfwmst_real_single($wkfw_id)
	{
		$sql  = "select * from cl_wkfwmst_real";
		$cond = "where wkfw_id = '$wkfw_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}
// 2012/01/27 Yamagawa add(e)

	// フォーマットファイル情報取得
	function get_wkfwfile($wkfw_id)
	{
		$sql  = "select wkfwfile_no, wkfwfile_name from cl_wkfwfile";
		$cond = "where wkfw_id = '$wkfw_id' order by wkfwfile_no";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// 前提とする申請書情報取得
	function get_wkfwfprecond($wkfw_id, $mode)
	{
		if($mode == "ALIAS")
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from cl_wkfwfprecond a ";
		}
		else
		{
			$sql  = "select a.precond_wkfw_id, b.wkfw_title from cl_wkfwfprecond_real a ";
		}

		$sql .= "inner join cl_wkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$cond = "where a.wkfw_id = '$wkfw_id' order by a.precond_order asc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請情報取得
	function get_approved_apply_list($session, $wkfw_id, $page, $max_page)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, b.short_wkfw_name ";
		$sql  .= "from cl_apply a ";
		$sql  .= "inner join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = '$wkfw_id' ";
		$cond .= "order by a.apply_date desc, a.apply_no desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 承認確定申請件数情報取得
	function get_approved_apply_list_count($session, $wkfw_id)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from cl_apply a ";
		$sql  .= "inner join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
		$cond .= "where not a.delete_flg and a.apply_stat = '1' and a.emp_id = '$emp_id' and b.wkfw_id = '$wkfw_id' ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 申請登録
	function regist_apply($arr)
	{
		$apply_id          = $arr["apply_id"];
		$wkfw_id           = $arr["wkfw_id"];
		$apply_content     = $arr["apply_content"];
		$emp_id            = $arr["emp_id"];
		$apply_stat        = $arr["apply_stat"];
		$apply_date        = $arr["apply_date"];
		$delete_flg        = $arr["delete_flg"];
		$apply_title       = $arr["apply_title"];
		$re_apply_id       = $arr["re_apply_id"];
		$apv_fix_show_flg  = $arr["apv_fix_show_flg"];
		$apv_bak_show_flg  = $arr["apv_bak_show_flg"];
		$emp_class         = $arr["emp_class"];
		$emp_attribute     = $arr["emp_attribute"];
		$emp_dept          = $arr["emp_dept"];
		$apv_ng_show_flg   = $arr["apv_ng_show_flg"];
		$emp_room          = $arr["emp_room"];
		$draft_flg         = $arr["draft_flg"];
		$wkfw_appr         = $arr["wkfw_appr"];
		$wkfw_content_type = $arr["wkfw_content_type"];
		$apply_title_disp_flg = $arr["apply_title_disp_flg"];
		$apply_no          = $arr["apply_no"];
        $notice_sel_flg    = $arr["notice_sel_flg"];
        $wkfw_history_no   = $arr["wkfw_history_no"];
		$wkfwfile_history_no = $arr["wkfwfile_history_no"];

		$sql  = "insert into cl_apply ";
		$sql .= "(apply_id, ";
		$sql .= "wkfw_id, ";
		$sql .= "apply_content, ";
		$sql .= "emp_id, ";
		$sql .= "apply_stat, ";
		$sql .= "apply_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apply_title, ";
		$sql .= "re_apply_id, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "apv_bak_show_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "apv_ng_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "draft_flg, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
		$sql .= "apply_no, ";
		$sql .= "notice_sel_flg, ";
		$sql .= "wkfw_history_no, ";
		$sql .= "wkfwfile_history_no) ";
		$sql .= "values (";

		$content = array(
							$apply_id,
							$wkfw_id,
							pg_escape_string($apply_content),
							$emp_id,
							$apply_stat,
							$apply_date,
							$delete_flg,
							pg_escape_string($apply_title),
							$re_apply_id,
							$apv_fix_show_flg,
							$apv_bak_show_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$apv_ng_show_flg,
							$emp_room,
							$draft_flg,
							$wkfw_appr,
							$wkfw_content_type,
							$apply_title_disp_flg,
							$apply_no,
							$notice_sel_flg,
							$wkfw_history_no,
							$wkfwfile_history_no
						);


		$ins = insert_into_table($this->_db_con,$sql,$content,$this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認登録
	function regist_applyapv($arr)
	{
		$wkfw_id          = $arr["wkfw_id"];
		$apply_id         = $arr["apply_id"];
		$apv_order        = $arr["apv_order"];
		$emp_id           = $arr["emp_id"];
		$apv_stat         = $arr["apv_stat"];
		$apv_date         = $arr["apv_date"];
		$delete_flg       = $arr["delete_flg"];
		$apv_comment      = $arr["apv_comment"];
		$st_div           = $arr["st_div"];
		$deci_flg         = $arr["deci_flg"];
		$emp_class        = $arr["emp_class"];
		$emp_attribute    = $arr["emp_attribute"];
		$emp_dept         = $arr["emp_dept"];
		$emp_st           = $arr["emp_st"];
		$apv_fix_show_flg = $arr["apv_fix_show_flg"];
		$emp_room         = $arr["emp_room"];
		$apv_sub_order    = $arr["apv_sub_order"];
		$multi_apv_flg    = $arr["multi_apv_flg"];
		$next_notice_div  = $arr["next_notice_div"];
		$parent_pjt_id    = $arr["parent_pjt_id"];
		$child_pjt_id     = $arr["child_pjt_id"];
		$other_apv_flg    = $arr["other_apv_flg"];

		$sql  = "insert into cl_applyapv ";
		$sql .= "(wkfw_id, ";
		$sql .= "apply_id, ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_stat, ";
		$sql .= "apv_date, ";
		$sql .= "delete_flg, ";
		$sql .= "apv_comment, ";
		$sql .= "st_div, ";
		$sql .= "deci_flg, ";
		$sql .= "emp_class, ";
		$sql .= "emp_attribute, ";
		$sql .= "emp_dept, ";
		$sql .= "emp_st, ";
		$sql .= "apv_fix_show_flg, ";
		$sql .= "emp_room, ";
		$sql .= "apv_sub_order, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id, ";
		$sql .= "other_apv_flg) ";

		$sql .= "values (";

		$content = array(
							$wkfw_id,
							$apply_id,
							$apv_order,
							$emp_id,
							$apv_stat,
							$apv_date,
							$delete_flg,
							$apv_comment,
							$st_div,
							$deci_flg,
							$emp_class,
							$emp_attribute,
							$emp_dept,
							$emp_st,
							$apv_fix_show_flg,
							$emp_room,
							($apv_sub_order=="") ? 1:$apv_sub_order,
							$multi_apv_flg,
							$next_notice_div,
							$parent_pjt_id,
							$child_pjt_id,
							$other_apv_flg
						);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者候補登録
	function regist_applyapvemp($apply_id, $apv_order, $person_order, $emp_id, $st_div, $parent_pjt_id, $child_pjt_id)
	{
		$sql = "insert into cl_applyapvemp (apply_id, apv_order, person_order, emp_id, delete_flg, st_div, parent_pjt_id, child_pjt_id) values (";
		$content = array($apply_id, $apv_order, $person_order, $emp_id, "f", $st_div, $parent_pjt_id, $child_pjt_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル登録
	function regist_applyfile($apply_id, $applyfile_no, $applyfile_name)
	{
		$sql = "insert into cl_applyfile (apply_id, applyfile_no, applyfile_name, delete_flg) values (";
		$content = array($apply_id, $applyfile_no, $applyfile_name, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{

		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "insert into cl_applyprecond (apply_id, precond_wkfw_id, precond_order, precond_apply_id, delete_flg) values (";
		$content = array($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_applynotice($apply_id, $recv_emp_id, $rslt_ntc_div)
	{
		$sql = "insert into cl_applynotice (apply_id, recv_emp_id, confirmed_flg, send_emp_id, send_date, delete_flg, rslt_ntc_div) values (";
		$content = array($apply_id, $recv_emp_id, "f", null, null, "f", $rslt_ntc_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//	// 年度の申請件数取得
//	function get_apply_cnt_per_year($year)
//	{
//		$this_ymd = $year."0401";
//		$next_year = $year + 1;
//		$next_ymd = $next_year."0331";
//
//		$sql  = "select count(*) as cnt from cl_apply ";
//		$cond = "where substring(apply_date from 1 for 8) between '$this_ymd' and '$next_ymd' and not draft_flg";
//
//		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
//		if ($sel == 0) {
//			pg_close($this->_db_con);
//			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//			exit;
//		}
//
//		return pg_fetch_result($sel, 0, "cnt");
//	}

	// 申請番号採番
	function get_apply_no()
	{
		$apply_no = null;
		$date = date("Ymd");
		$year = substr($date, 0, 4);
		$md   = substr($date, 4, 4);

		if($md >= "0101" and $md <= "0331")
		{
			$year = $year - 1;
		}

		//採番に使用するシーケンス名
		$seq_name = 'cl_apply_no_' . $year . '_seq';

		//採番
		$sql = "select nextval('" . $seq_name . "') as apply_no";
		$cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		$apply_no = pg_result($sel,0,"apply_no");

//		$max_cnt = $this->get_apply_cnt_per_year($year);
//		$apply_no = $max_cnt + 1;

		return $apply_no;
	}


//-------------------------------------------------------------------------------------------------------------------------
// 下書き・更新関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請・ワークフロー情報取得
	function get_apply_wkfwmst($apply_id)
	{
		$sql  = "select cl_apply.*, empmst.emp_lt_nm, empmst.emp_ft_nm, ";
		$sql .= "cl_wkfwmst.wkfw_title, cl_wkfwmst.wkfw_folder_id, cl_wkfwcatemst.wkfw_nm, cl_wkfwmst.wkfw_content, cl_wkfwmst.short_wkfw_name, ";
		$sql .= "classmst.class_nm as apply_class_nm, atrbmst.atrb_nm as apply_atrb_nm, deptmst.dept_nm as apply_dept_nm, classroom.room_nm as apply_room_nm ";
		$sql .= "from cl_apply ";
		$sql .= "inner join cl_wkfwmst on cl_apply.wkfw_id = cl_wkfwmst.wkfw_id ";
		$sql .= "inner join cl_wkfwcatemst on cl_wkfwmst.wkfw_type = cl_wkfwcatemst.wkfw_type ";
		$sql .= "inner join empmst on cl_apply.emp_id = empmst.emp_id ";
		$sql .= "left join classmst on cl_apply.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on cl_apply.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on cl_apply.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on cl_apply.emp_room = classroom.room_id ";
		$cond = "where cl_apply.apply_id = '$apply_id' ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}


	// 添付ファイル情報取得
	function get_applyfile($apply_id)
	{
		$sql  = "select applyfile_no, applyfile_name from cl_applyfile";
		$cond = "where apply_id = '$apply_id' order by applyfile_no asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("applyfile_no" => $row["applyfile_no"], "applyfile_name" => $row["applyfile_name"]);

		}
		return $arr;
	}

	// 承認情報取得
	function get_applyapv($apply_id)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.st_nm,  ";
		$sql .= "d.pjt_name as parent_pjt_name, f.pjt_name as child_pjt_name ";
		$sql .= "from cl_applyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "left join stmst c on a.emp_st = c.st_id ";
		$sql .= "left join project d on a.parent_pjt_id = d.pjt_id ";
		$sql .= "left join project f on a.child_pjt_id = f.pjt_id ";
		$cond = "where a.apply_id = '$apply_id' order by a.apv_order, a.apv_sub_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 承認者候補情報取得
	function get_applyapvemp($apply_id, $apv_order)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm, d.st_nm, c.emp_del_flg, ";
		$sql .= "f.pjt_name as parent_pjt_name, g.pjt_name as child_pjt_name ";
		$sql .= "from cl_applyapvemp a ";
		$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
		$sql .= "inner join authmst c on b.emp_id = c.emp_id ";
		$sql .= "left join stmst d on b.emp_st = d.st_id ";
		$sql .= "left join project f on a.parent_pjt_id = f.pjt_id ";
		$sql .= "left join project g on a.child_pjt_id = g.pjt_id ";
		$cond = "where a.apply_id = '$apply_id' and a.apv_order = $apv_order order by a.person_order asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 申請結果通知情報取得
	function get_applynotice($apply_id)
	{
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm ";
		$sql  .= "from cl_applynotice a ";
		$sql  .= "left join empmst b on a.recv_emp_id = b.emp_id ";
		$cond  = "where a.apply_id = '$apply_id' order by a.oid ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "recv_emp_id" => $row["recv_emp_id"],
                            "confirmed_flg" => $row["confirmed_flg"],
                            "send_emp_id" => $row["send_emp_id"],
                            "send_date" => $row["send_date"],
                            "delete_flg" => $row["delete_flg"],
                            "rslt_ntc_div" => $row["rslt_ntc_div"],
                            "emp_lt_nm" => $row["emp_lt_nm"],
                            "emp_ft_nm" => $row["emp_ft_nm"]
                           );
		}
		return $arr;
	}

	// 下書き申請取得
	function get_draft_apply($emp_id)
	{
        $sql   = "select a.apply_id, a.apply_title, a.wkfw_id, b.wkfw_title ";
        $sql  .= "from cl_apply a ";
        $sql  .= "left join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
        $cond  = "where not a.delete_flg and a.draft_flg = 't' and a.emp_id = '$emp_id' order by a.apply_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"], "apply_title" => $row["apply_title"], "wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
		}

		return $arr;
	}

	// 前提とする申請書(申請用)取得
	function get_applyprecond($apply_id)
	{
		$sql   = "select ";
		$sql  .= "a.precond_wkfw_id, ";
		$sql  .= "a.precond_order, ";
		$sql  .= "a.precond_apply_id, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.apply_title, ";
		$sql  .= "c.apply_date, ";
		$sql  .= "c.apply_no ";
		$sql  .= "from cl_applyprecond a ";
		$sql  .= "inner join cl_wkfwmst b on a.precond_wkfw_id = b.wkfw_id ";
		$sql  .= "left join cl_apply c on a.precond_apply_id = c.apply_id ";
		$cond  = "where a.apply_id = '$apply_id' ";
		$cond .= "order by a.precond_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("precond_wkfw_id" => $row["precond_wkfw_id"],
                            "precond_order" => $row["precond_wkfw_id"],
                            "precond_apply_id" => $row["precond_apply_id"],
                            "wkfw_title" => $row["wkfw_title"],
                            "short_wkfw_name" => $row["short_wkfw_name"],
                            "apply_title" => $row["apply_title"],
                            "apply_date" => $row["apply_date"],
                            "apply_no" => $row["apply_no"]
			               );
		}

		return $arr;
	}

	// 申請更新(下書き用)
	function update_apply_draft($apply_id, $apply_content, $apply_date, $apply_title, $draft_flg, $apply_no)
	{
		$sql = "update cl_apply set";

		if($apply_no != "")
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg", "apply_no");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg, $apply_no);
		}
		else
		{
			$set = array("apply_content", "apply_date", "apply_title", "draft_flg");
			$setvalue = array(pg_escape_string($apply_content), $apply_date, pg_escape_string($apply_title), $draft_flg);
		}
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請更新
	function update_apply($apply_id, $apply_content, $apply_title)
	{
		$sql = "update cl_apply set";
		$set = array("apply_content", "apply_title");
		$setvalue = array(pg_escape_string($apply_content), pg_escape_string($apply_title));
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新
	function update_applyapv($arr)
	{
		$apply_id       = $arr["apply_id"];
		$apv_order      = $arr["apv_order"];
		$apv_sub_order  = $arr["apv_sub_order"];
		$emp_id         = $arr["emp_id"];
		$st_div         = $arr["st_div"];
		$emp_class      = $arr["emp_class"];
		$emp_attribute  = $arr["emp_attribute"];
		$emp_dept       = $arr["emp_dept"];
		$emp_st         = $arr["emp_st"];
		$emp_room       = $arr["emp_room"];
		$parent_pjt_id  = $arr["parent_pjt_id"];
		$child_pjt_id   = $arr["child_pjt_id"];

		$sql = "update cl_applyapv set";
		$set = array("emp_id", "st_div", "emp_class", "emp_attribute", "emp_dept", "emp_st", "emp_room", "parent_pjt_id", "child_pjt_id");
		$setvalue = array($emp_id, $st_div, $emp_class, $emp_attribute, $emp_dept, $emp_st, $emp_room, $parent_pjt_id, $child_pjt_id);
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order ";

		if($apv_sub_order != "")
		{
			$cond .= "and apv_sub_order = $apv_sub_order";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル削除
	function delete_applyfile($apply_id)
	{
		$sql = "delete from cl_applyfile";
		$cond = "where apply_id = '$apply_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)更新
	function update_applyprecond($apply_id, $precond_wkfw_id, $precond_order, $precond_apply_id)
	{
		$precond_apply_id = ($precond_apply_id == "") ? null : $precond_apply_id;

		$sql = "update cl_applyprecond set";
		$set = array("precond_apply_id");
		$setvalue = array($precond_apply_id);
		$cond = "where apply_id = '$apply_id' and precond_wkfw_id = '$precond_wkfw_id' and precond_order = $precond_order ";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知削除
	function delete_applynotice($apply_id)
	{
		$sql = "delete from cl_applynotice";
		$cond = "where apply_id = '$apply_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // 承認者情報物理削除
    function delete_applyapv($apply_id)
    {
		$sql = "delete from cl_applyapv";
		$cond = "where apply_id = '$apply_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

//-------------------------------------------------------------------------------------------------------------------------
// 申請承認関連(論理削除)
//-------------------------------------------------------------------------------------------------------------------------
	// 申請論理削除
	function update_delflg_apply($apply_id, $delete_flg)
	{
		$sql = "update cl_apply set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認論理削除
	function update_delflg_applyapv($apply_id, $delete_flg)
	{
		$sql = "update cl_applyapv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認候補論理削除
	function update_delflg_applyapvemp($apply_id, $delete_flg)
	{
		$sql = "update cl_applyapvemp set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 添付ファイル論理削除
	function update_delflg_applyfile($apply_id, $delete_flg)
	{
		$sql = "update cl_applyfile set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信論理削除
	function update_delflg_applyasyncrecv($apply_id, $delete_flg)
	{
		$sql = "update cl_applyasyncrecv set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知論理削除
	function update_delflg_applynotice($apply_id, $delete_flg)
	{
		$sql = "update cl_applynotice set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)論理削除
	function update_delflg_applyprecond($apply_id, $delete_flg)
	{
		$sql = "update cl_applyprecond set";
		$set = array("delete_flg");
		$setvalue = array($delete_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請論理削除更新(全部)
	function update_delflg_all_apply($apply_id, $delete_flg)
	{
		// 申請情報を論理削除
		$this->update_delflg_apply($apply_id, $delete_flg);

		// 承認情報を論理削除
		$this->update_delflg_applyapv($apply_id, $delete_flg);

		// 承認者候補情報を論理削除
		$this->update_delflg_applyapvemp($apply_id, $delete_flg);

		// 添付ファイル情報論理削除
		$this->update_delflg_applyfile($apply_id, $delete_flg);

		// 非同期・同期受信論理削除
		$this->update_delflg_applyasyncrecv($apply_id, $delete_flg);

		// 申請結果通知論理削除
		$this->update_delflg_applynotice($apply_id, $delete_flg);

		// 前提とする申請書(申請用)論理削除
		$this->update_delflg_applyprecond($apply_id, $delete_flg);

	}

//-------------------------------------------------------------------------------------------------------------------------
// 再申請関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請登録
	function regist_re_apply($new_apply_id, $apply_id, $apply_content, $apply_title, $sceen_div)
	{
		$date = date("YmdHi");

//		$year = substr($date, 0, 4);
//		$md   = substr($date, 4, 4);
//		if($md >= "0101" and $md <= "0331")
//		{
//			$year = $year - 1;
//		}
//		$max_cnt = $this->get_apply_cnt_per_year($year);
//		$apply_no = $max_cnt + 1;

		$apply_no = $this->get_apply_no();

		$sql  = "insert into cl_apply ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "a.wkfw_id, ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_content', ";
		}
		else
		{
			$sql .= "a.apply_content, ";
		}

		$sql .= "a.emp_id, ";
		$sql .= "'0', ";
		$sql .= "'$date', ";
		$sql .= "'f', ";

		if($sceen_div == "DETAIL")
		{
			$sql .= "'$apply_title', ";
		}
		else
		{
			$sql .= "a.apply_title, ";
		}

		$sql .= "null, ";
		$sql .= "'t', ";
		$sql .= "'t', ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "'f', ";
		$sql .= "a.wkfw_appr, ";
		$sql .= "a.wkfw_content_type, ";
		$sql .= "apply_title_disp_flg, ";
        $sql .= "$apply_no, ";
		$sql .= "a.notice_sel_flg, ";
		$sql .= "a.wkfw_history_no ";
		$sql .= "from cl_apply a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認登録
	function regist_re_applyapv($new_apply_id, $apply_id)
	{
		$sql  = "insert into cl_applyapv ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "a.wkfw_id, ";
		$sql .= "a.apv_order, ";
		$sql .= "a.emp_id, ";
		$sql .= "'0', ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'', ";
		$sql .= "a.st_div, ";
		$sql .= "a.deci_flg, ";
		$sql .= "b.emp_class, ";
		$sql .= "b.emp_attribute, ";
		$sql .= "b.emp_dept, ";
		$sql .= "b.emp_st, ";
		$sql .= "'t', ";
		$sql .= "b.emp_room, ";
		$sql .= "a.apv_sub_order, ";
		$sql .= "a.multi_apv_flg, ";
		$sql .= "a.next_notice_div, ";
		$sql .= "a.parent_pjt_id, ";
		$sql .= "a.child_pjt_id, ";
		$sql .= "'f'  ";
		$sql .= "from cl_applyapv a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql .= "where a.apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

	// 承認者候補登録
	function regist_re_applyapvemp($new_apply_id, $apply_id)
	{
		$sql  = "insert into cl_applyapvemp ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "apv_order, ";
		$sql .= "person_order, ";
		$sql .= "emp_id, ";
		$sql .= "'f', ";
		$sql .= "st_div, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from cl_applyapvemp ";
		$sql .= "where apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 添付ファイル登録
	function regist_re_applyfile($new_apply_id, $apply_id)
	{

		$sql  = "insert into cl_applyfile ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "applyfile_no, ";
		$sql .= "applyfile_name, ";
		$sql .= "'f' ";
		$sql .= "from cl_applyfile ";
		$sql .= "where apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信登録
	function regist_re_applyasyncrecv($new_apply_id, $apply_id)
	{
		$sql  = "insert into cl_applyasyncrecv ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "send_apv_order, ";
		$sql .= "send_apv_sub_order, ";
		$sql .= "recv_apv_order, ";
		$sql .= "recv_apv_sub_order, ";
		$sql .= "null, ";
		$sql .= "'f', ";
		$sql .= "'f' ";
		$sql .= "from cl_applyasyncrecv ";
		$sql .= "where apply_id = '$apply_id'";


		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 申請結果通知登録
	function regist_re_applynotice($new_apply_id, $apply_id)
	{
		$sql  = "insert into cl_applynotice ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "recv_emp_id, ";
		$sql .= "'f', ";
		$sql .= "null, ";
		$sql .= "null, ";
		$sql .= "'f' ";
		$sql .= "from cl_applynotice ";
		$sql .= "where apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(申請用)登録
	function regist_re_applyprecond($new_apply_id, $apply_id)
	{
		$sql  = "insert into cl_applyprecond ";
		$sql .= "(select ";
		$sql .= "'$new_apply_id', ";
		$sql .= "precond_wkfw_id, ";
		$sql .= "precond_order, ";
		$sql .= "precond_apply_id, ";
		$sql .= "'f' ";
		$sql .= "from cl_applyprecond ";
		$sql .= "where apply_id = '$apply_id'";

		$ins = insert_into_table($this->_db_con, $sql, "", $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 再申請ＩＤ更新
	function update_re_apply_id($apply_id, $next_apply_id)
	{
		$sql = "update cl_apply set";
		$set = array("re_apply_id");
		$setvalue = array($next_apply_id);
		$cond = "where apply_id = '$apply_id'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 非同期・同期通知関連
//-------------------------------------------------------------------------------------------------------------------------
	// 非同期・同期受信登録
	function regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order)
	{
		$sql = "insert into cl_applyasyncrecv (apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg) values (";
		$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order, null, "f", "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信削除
	function delete_applyasyncrecv($apply_id) {
		$sql = "delete from cl_applyasyncrecv";
		$cond = "where apply_id = '$apply_id'";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// 承認
//-------------------------------------------------------------------------------------------------------------------------
	// 同一階層の承認者数取得
	function get_same_hierarchy_apvcnt($apply_id, $apv_order)
	{
		$sql  = "select count(*) as cnt from cl_applyapv";
		$cond ="where apply_id = '$apply_id' and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// 最終承認階層番号取得
	function get_last_apv_order($apply_id)
	{
		$sql  = "select max(apv_order) as max from cl_applyapv";
		$cond ="where apply_id = '$apply_id'";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}


	// 承認情報更新
	function update_apvstat($apv_stat, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div)
	{
		$sql = "update cl_applyapv set";

		if($screen_div == "DETAIL")
		{
			$set = array("apv_stat", "apv_date", "apv_comment", "draft_flg");
			$setvalue = array($apv_stat, $apv_date, pg_escape_string($apv_comment), "f");
		}
		else if($screen_div == "LIST")
		{
			$set = array("apv_stat", "apv_date", "draft_flg");
			$setvalue = array($apv_stat, $apv_date, "f");
		}

		if($apv_sub_order != "")
		{
			$cond = "where apply_id = '$apply_id' and apv_order = $apv_order and apv_sub_order = $apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = '$apply_id' and apv_order = $apv_order and apv_sub_order is null";
		}

		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 指定した承認ステータス数取得
	function get_apvstatcnt($apply_id, $apv_stat)
	{
		$sql = "select count(*) as cnt from cl_applyapv ";
		$cond = "where apply_id = '$apply_id' and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 全承認者数取得
	function get_allapvcnt($apply_id)
	{
		$sql = "select count(*) as cnt from cl_applyapv ";
		$cond = "where apply_id = '$apply_id'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請ステータス更新
	function update_applystat($apply_id, $apply_stat, $session, $screen_div, $inputs)
	{
		$sql = "update cl_apply set";
		$set = array("apply_stat");
		$setvalue = array($apply_stat);
		$cond = "where apply_id = '$apply_id'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		//========================================================================
		// 申請ステータスが承認(1)のとき、xmlを解析し対応テーブルにインサート
		// ここから
		//========================================================================
		// DB登録に必要なデータを引き直す
		$select_sql = "select A.apply_id, A.apply_content, A.emp_id, W.short_wkfw_name from cl_apply A, cl_wkfwmst W ";
		$select_cond = "where A.apply_id = '$apply_id' and A.wkfw_id = W.wkfw_id ";

		$select_result = select_from_table($this->_db_con, $select_sql, $select_cond, $this->file_name);
		if ($select_result == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = pg_result($select_result, 0, "apply_id");
		$xml_data = pg_result($select_result, 0, "apply_content");
		$emp_id = pg_result($select_result, 0, "emp_id");
		$tmpl_cd = pg_result($select_result, 0, "short_wkfw_name");

		if ($apply_stat == "1")
		{
             // 病棟評価の場合
            if($this->is_ward_template($tmpl_cd))
            {
				// 既存データを削除
				$this->delete_ward_grade_data($apply_id, $tmpl_cd);
				// DB定義XMLファイルを読み込む
				// TODO ファイルパス直書き
				$xml_db_path = "cl/db_xml/cl_wkfw_$tmpl_cd.xml";
				$xml_db = file_get_contents($xml_db_path);
				if ($xml_db === FALSE) {
					pg_query($this->_db_con, "rollback");
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}

				// 一時的にinclude_pathを変更
				$old_inc_dir = ini_get('include_path');
				$add_inc_dir = dirname(__FILE__) . "/PEAR";
				$inc_separate = ":";
/*
				$os_name = php_uname('s');
				if ('W' == $os_name[0]){
					$inc_separate = ";";
				}
*/
				ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

				// XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
				require_once("Unserializer.php");
				$options = array(
					'parseAttributes' => FALSE,
					'forceEnum' => array('q','var_name')
				);
				$Unserializer =&new XML_Unserializer($options);
if (PHP_VERSION >= '5') {
$xml_db = str_replace("EUC-JP", "EUCJP-win", $xml_db);
}
				$Unserializer->unserialize($xml_db);
				$xml_db_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_db_array);
}

				$Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$xml_data = str_replace("EUC-JP", "EUCJP-win", $xml_data);
}
				$Unserializer->unserialize($xml_data);
				$xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

				// include_pathを元に戻す
				ini_set('include_path', $old_inc_dir);


				// insertするデータの作成
				$table = $xml_db_array["table"];
				$insert_data = array();

				// key項目
				$insert_keys = array();
				$insert_keys["apply_id"] = $apply_id;
				$insert_keys["emp_id"] = $emp_id;
				$insert_keys["kanri_no"] = $tmpl_cd;
				foreach ($xml_db_array["key"] as $key){
					$insert_keys[$key] = $xml_data_array[$key];
				}


				// 一覧画面
				if($screen_div == "LIST")
				{
					// 質問と答え
					foreach ($xml_db_array["q"] as $qs){
						foreach ($qs["var_name"] as $var_name){

							// xmlの各データをテンプレートに対応するテーブルに登録する。
							$value = $xml_data_array[$var_name];

							$insert_data = array_merge($insert_keys, array(
								'var_name' => mb_convert_encoding($var_name, "UTF-8", "EUC-JP"),
								'value'     => mb_convert_encoding($value, "UTF-8", "EUC-JP"))
							);

							$insert_result = $this->insert_into_table_array($table, $insert_data);

							// 登録失敗時の処理
							if ($insert_result == 0)
							{
								pg_query($this->_db_con,"rollback");
								pg_close($this->_db_con);
								echo("<script type='text/javascript' src='./js/showpage.js'></script>");
								echo("<script language='javascript'>showErrorPage(window);</script>");
								exit;
							}
						}
					}
				}
				// 詳細画面
				else
				{
					foreach ($xml_db_array["q"] as $qs){
						foreach ($qs["var_name"] as $var_name){

							// xmlの各データをテンプレートに対応するテーブルに登録する。
							$value = $xml_data_array[$var_name];

							$insert_data = array_merge($insert_keys, array(
								'var_name' => mb_convert_encoding($var_name, "UTF-8", "EUC-JP"),
								'value'     => $inputs[mb_convert_encoding($var_name, "UTF-8", "EUC-JP")])
							);

							$insert_result = $this->insert_into_table_array($table, $insert_data);

							// 登録失敗時の処理
							if ($insert_result == 0)
							{
								pg_query($this->_db_con,"rollback");
								pg_close($this->_db_con);
								echo("<script type='text/javascript' src='./js/showpage.js'></script>");
								echo("<script language='javascript'>showErrorPage(window);</script>");
								exit;
							}
						}
					}
				}
            }
            // レベルアップ志願書
			if($this->is_application_book_template($tmpl_cd))
			{
				$arr_cl_recognize_schedule = $this->get_cl_recognize_schedule($apply_id);
				if(count($arr_cl_recognize_schedule) > 0)
				{
					// 認定スケジュール更新処理(表示フラグ更新)
					$this->update_disp_flg_cl_recognize_schedule($apply_id);
				}
				else
				{
					if($tmpl_cd == "c101")
					{
                        $level = 2;
						// 2011/12/2 Yamagawa add(s)
						$nurse_type = 1;
						// 2011/12/2 Yamagawa add(e)
					}
					else if($tmpl_cd == "c102")
					{
                        $level = 3;
						// 2011/12/2 Yamagawa add(s)
						$nurse_type = 1;
						// 2011/12/2 Yamagawa add(e)
					}
					// 2011/12/2 Yamagawa add(s)
					else if($tmpl_cd == "g101")
					{
						$level = 2;
						$nurse_type = 2;
					}
					else if($tmpl_cd == "g102")
					{
						$level = 2;
						$nurse_type = 3;
					}
					else if($tmpl_cd == "g103")
					{
						$level = 3;
						$nurse_type = 2;
					}
					else if($tmpl_cd == "g104")
					{
						$level = 3;
						$nurse_type = 3;
					}
					// 2011/12/2 Yamagawa add(e)

                    $arr_apply_wkfwmst = $this->get_apply_wkfwmst($apply_id);
                    $apply_date = $this->get_apply_date($arr_apply_wkfwmst[0]["apply_content"]);
                    $apply_date = ereg_replace("/", "", $apply_date);
                    $apply_date = (strlen($apply_date) != 8) ? "" : $apply_date;

					$arr_data = array(
                                     "apply_id" => $apply_id,
                                     "apply_date" => $apply_date,
                                     "emp_id" => $emp_id,
                                     "level" => $level,
                                     "no1_date" => "",
                                     "no1_apply_id" => null,
                                     "no2_date" => "",
                                     "no2_apply_id" => null,
                                     "no3_date" => "",
                                     "no3_apply_id" => null,
                                     "no4_date" => "",
                                     "no4_apply_id" => null,
                                     "no5_date" => "",
                                     "no5_apply_id" => null,
                                     "no6_date" => "",
                                     "no6_apply_id" => null,
                                     "no7_date" => "",
                                     "no7_apply_id" => null,
                                     "no8_date" => "",
                                     "no8_apply_id" => null,
                                     "no9_date" => "",
                                     "no9_apply_id" => "",
                                     "no10_date" => "",
                                     "no10_apply_id" => null,
                                     "no11_date" => "",
                                     "no11_apply_id" => null,
                                     "no12_date" => "",
                                     "no12_apply_id" => null,
                                     "disp_flg" => "t",
                                     "levelup_apply_id" => null,
                                     // 2011/12/2 Yamagawa add(s)
                                     "nurse_type" => $nurse_type,
                                     "no31_date" => "",
                                     "no31_apply_id" => null,
                                     "no32_date" => "",
                                     "no32_apply_id" => null,
                                     "no33_date" => "",
                                     "no33_apply_id" => null
                                     // 2011/12/2 Yamagawa add(e)
                                     );

					// 認定スケジュール情報登録
					$this->regist_cl_recognize_schedule($arr_data);
				}
			}

            // レベルアップ申請の場合
			if($this->is_levelup_apply_template($tmpl_cd))
            {
                $arr_cl_career_history = $this->get_cl_career_history($apply_id);
                if(count($arr_cl_career_history) > 0)
                {
                    $this->update_cl_career_history_disp_flg($apply_id, "t");
                }
            }
            // キャリア申告の場合
            if($this->is_career_apply_template($tmpl_cd))
            {
                $this->regist_career_apply($apply_id);
            }
            // 受講報告の場合
            if($this->is_attendance_apply_template($tmpl_cd))
            {
                $this->regist_cl_training_apply($apply_id);
            }
		}

		//========================================================================
		// ここまで
		//========================================================================


		// 申請結果通知更新
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];
		$this->update_send_applynotice($apply_id, $emp_id);
	}

	// 否認フラグ更新
	function update_ng_show_flg($apply_id, $apv_ng_show_flg)
	{
		$sql = "update cl_apply set";
		$set = array("apv_ng_show_flg");
		$setvalue = array($apv_ng_show_flg);
		$cond = "where apply_id = '$apply_id'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 差戻しフラグ更新
	function update_bak_show_flg($apply_id, $apv_bak_show_flg)
	{
		$sql = "update cl_apply set";
		$set = array("apv_bak_show_flg");
		$setvalue = array($apv_bak_show_flg);
		$cond = "where apply_id = '$apply_id'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 非同期・同期受信更新
	function update_apv_show_flg($apply_id, $send_apv_order, $send_apv_sub_order, $send_apved_order)
	{
		$sql = "update cl_applyasyncrecv set";
		$set = array("apv_show_flg", "send_apved_order");
		$setvalue = array("t", $send_apved_order);

		if($send_apv_sub_order != "")
		{
			$cond = "where apply_id = '$apply_id' and send_apv_order = $send_apv_order and send_apv_sub_order = $send_apv_sub_order";
		}
		else
		{
			$cond = "where apply_id = '$apply_id' and send_apv_order = $send_apv_order and send_apv_sub_order is null";
		}
		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認更新番号取得
	function get_max_send_apved_order($apply_id, $send_apv_order)
	{
		$sql  = "select max(send_apved_order) as max from cl_applyasyncrecv ";
		$cond = "where apply_id = '$apply_id' and send_apv_order = $send_apv_order";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// 同一階層で指定した承認ステータス数取得
	function get_same_hierarchy_apvstatcnt($apply_id, $apv_order, $apv_stat)
	{
		$sql = "select count(*) as cnt from cl_applyapv ";
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order and apv_stat = '$apv_stat'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 階層ごとの承認者情報取得
	function get_applyapv_per_hierarchy($apply_id, $apv_order)
	{
		$sql = "select * from cl_applyapv ";
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	function get_apply_stat($apply_id)
	{
		$sql = "select apply_stat from cl_apply ";
		$cond = "where apply_id = '$apply_id' ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "apply_stat");
	}


	// 承認処理
	function approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, $screen_div, $inputs, $short_wkfw_name)
	{
		// 承認ステータス更新
		$apv_date = date("YmdHi");
		$this->update_apvstat($approve, $apv_date, $apv_comment, $apply_id, $apv_order, $apv_sub_order, $screen_div);

		// 病棟評価表
		if($this->is_ward_template($short_wkfw_name))
		{
			$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

			// 未承認者がいた場合
			if($non_apvstatcnt > 0)
			{
				switch($approve)
				{
					case "1":   // 承認

						// 非同期・同期受信テーブル更新
						$this->update_apv_show_flg($apply_id, $apv_order, "", 1);

						// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
						$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");

						break;
					case "2":   // 否認

						// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
						$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
						// 申請ステータス更新
						$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
						break;
					case "3":   // 差戻し

						// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
						$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
						// 申請ステータス更新
						$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);

						break;
					default:
						break;
				}
			}
			// 承認
			if($approve == "1") {
				// 申請ステータス更新
				$this->update_applystat($apply_id, "1", $session, $screen_div, $inputs);
			}
		}
		// 同報タイプ
		if($wkfw_appr == "1")
		{
			// 全承認者数取得
			$allapvcnt = $this->get_allapvcnt($apply_id);
			switch($approve)
			{
				case "1":   // 承認
					$apvstatcnt = $this->get_apvstatcnt($apply_id, "1");
					if($allapvcnt == $apvstatcnt)
					{
						// 申請ステータス更新
						$this->update_applystat($apply_id, "1", $session, $screen_div, $inputs);
					}
					break;
				case "2":   // 否認
					// 申請ステータス更新
					$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
					break;

				case "3":   // 差戻し
					// 申請ステータス更新
					$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);
					break;
				default:
					break;
			}
		}
		// 稟議タイプ
		else if($wkfw_appr == "2")
		{
			$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);

			// 該当承認者の同一階層に他の承認者がいる場合(複数承認者)
			if($same_hierarchy_apvcnt > 1)
			{
				// 最終承認階層取得
				$last_apv_order = $this->get_last_apv_order($apply_id);

				// 承認者の階層より後につづく階層がある場合
				if($apv_order < $last_apv_order)
				{
					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						switch($approve)
						{
							case "1":   // 承認
								// 非同期・同期受信テーブル更新
								$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
								$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								break;
							case "2":   // 否認

								$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");

								// 全員が否認の場合
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
								}
								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}
								break;
							case "3":   // 差戻し

								// 同一階層で先に「承認」した人がいた場合
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvstatcnt > 0)
								{
									// 非同期・同期受信テーブル更新
									$apved_order = $this->get_max_send_apved_order($apply_id, $apv_order);
									$this->update_apv_show_flg($apply_id, $apv_order, $apv_sub_order, $apved_order+1);
								}

								break;
							default:
								break;
						}

						// 未承認者がいない ＡＮＤ 承認が一人もいない ＡＮＤ 差戻しが一人でもいる
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						$ok_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
						$bak_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
						if($non_apvstatcnt == 0 && $ok_apvstatcnt == 0 && $bak_apvstatcnt > 0)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
/*
						switch($approve)
						{
							case "1":   // 承認
								$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
								$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
								if($same_hierarchy_apvcnt == $same_hierarchy_apvstatcnt)
								{
									// 非同期・同期受信テーブル更新
									$this->update_apv_show_flg($apply_id, $apv_order, "", 1);
								}
								break;
							case "2":   // 否認

								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);

								break;
							case "3":   // 差戻し

								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);
								break;
							default:
								break;
						}
*/
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 非同期・同期受信テーブル更新
								$this->update_apv_show_flg($apply_id, $apv_order, "", 1);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 非同期・同期受信テーブル更新
									$this->update_apv_show_flg($apply_id, $apv_order, "", 1);

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");

									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);

									break;
								default:
									break;
							}
						}
					}
				}
				// 承認者の階層より後につづく階層がない場合（最終階層）
				else if($apv_order == $last_apv_order)
				{

					// 承認者の階層が非同期指定の場合
					if($next_notice_div == "1")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							// 承認が１つでもある場合
							$approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							if($approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 全員が否認の場合
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							if($same_hierarchy_apvcnt == $no_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 承認がなく差戻しがある場合
							if($approvecnt == 0)
							{
								$bak_cnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");
								if($bak_cnt > 0)
								{
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);
								}
							}
						}
					}
					// 承認者の階層が同期指定の場合
					else if($next_notice_div == "2")
					{
						$same_hierarchy_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");
						// 同一階層で未承認者がいない場合
						if($same_hierarchy_apvstatcnt == 0)
						{
							$same_hierarchy_apvcnt = $this->get_same_hierarchy_apvcnt($apply_id, $apv_order);
							$ok_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "1");
							$no_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "2");
							$bak_approvecnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "3");

							// 全員が承認の場合
							if($same_hierarchy_apvcnt == $ok_approvecnt)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "1", $session);
							}

							// 否認があって差戻しがない場合、否認にする。
							if($no_approvecnt > 0 && $bak_approvecnt == 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "2", $session);
							}

							// 差戻しが１つでもある場合
							if($bak_approvecnt > 0)
							{
								// 申請ステータス更新
								$this->update_applystat($apply_id, "3", $session);

							}
						}
					}
					// 権限並列指定の場合
					else if($next_notice_div == "3")
					{
						$non_apvstatcnt = $this->get_same_hierarchy_apvstatcnt($apply_id, $apv_order, "0");

						// 未承認者がいた場合
						if($non_apvstatcnt > 0)
						{
							switch($approve)
							{
								case "1":   // 承認

									// 他の承認者の承認ステータスが「未承認」の場合、「承認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "1");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "1", $session);
									break;
								case "2":   // 否認

									// 他の承認者の承認ステータスが「未承認」の場合、「否認」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "2");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "2", $session);
									break;
								case "3":   // 差戻し

									// 他の承認者の承認ステータスが「未承認」の場合、「差戻し」に更新
									$this->update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, "3");
									// 申請ステータス更新
									$this->update_applystat($apply_id, "3", $session);

									break;
								default:
									break;
							}
						}
					}
				}
			}
			// 該当承認者の同一階層に他の承認者がいない場合(承認者一人)
			else if($same_hierarchy_apvcnt == 1)
			{
				switch($approve)
				{
					case "1":   // 承認
						// 最終承認階層取得
						$last_apv_order = $this->get_last_apv_order($apply_id);
						if($apv_order == $last_apv_order)
						{
							// 申請ステータス更新
							$this->update_applystat($apply_id, "1", $session, $screen_div, $inputs);
						}
						break;
					case "2":   // 否認
						// 申請ステータス更新
						$this->update_applystat($apply_id, "2", $session, $screen_div, $inputs);
						break;
					case "3":   // 差戻し
						// 申請ステータス更新
						$this->update_applystat($apply_id, "3", $session, $screen_div, $inputs);
						break;
					default:
						break;

				}
			}
		}
	}

	// 承認時更新
	function update_apply_content($apply_id, $apply_content)
	{
		$sql = "update cl_apply set";
		$set = array("apply_content");
		$setvalue = array(pg_escape_string($apply_content));
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// 非同期・同期受信情報取得
	function get_applyasyncrecv($apply_id, $recv_apv_order, $recv_apv_sub_order, $send_apved_order)
	{
		$sql   = "select * from cl_applyasyncrecv ";
		$cond .= "where apply_id = '$apply_id' and ";
		$cond .= "recv_apv_order = $recv_apv_order and ";
        if($recv_apv_sub_order != "")
		{
			$cond .= "recv_apv_sub_order = $recv_apv_sub_order and ";
		}
		else
		{
			$cond .= "recv_apv_sub_order is null and ";
		}
		$cond .= "send_apved_order <= $send_apved_order ";
		$cond .= "order by send_apved_order asc ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_id" => $row["apply_id"],
                            "send_apv_order" =>  $row["send_apv_order"],
                            "send_apv_sub_order" =>  $row["send_apv_sub_order"],
                            "recv_apv_order" =>  $row["recv_apv_order"],
                            "recv_apv_sub_order" =>  $row["recv_apv_sub_order"],
                            "send_apved_order" =>  $row["send_apved_order"]
			               );
		}
		return $arr;
	}

	// 非同期・同期受信情報取得(送信先sub_order取得。評価申請用)
    function get_send_apv_sub_order($apply_id, $send_apved_order)
    {
		$sql   = "select * from cl_applyasyncrecv ";
		$cond .= "where apply_id = '$apply_id' and send_apv_order = 1 ";
		$cond .= "and send_apved_order = $send_apved_order ";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "send_apv_sub_order");
    }

	// 申請結果通知・送信者更新
	function update_send_applynotice($apply_id, $send_emp_id)
	{
		$date = date("YmdHi");

		$sql = "update cl_applynotice set";
		$set = array("send_emp_id", "send_date");
		$setvalue = array($send_emp_id, $date);
		$cond = "where apply_id = '$apply_id'";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認テーブル更新処理（権限並列用）
	function update_applyapv_for_parallel($apply_id, $apv_order, $apv_date, $apv_stat)
	{
		$sql = "update cl_applyapv set";
		$set = array("apv_stat", "other_apv_flg", "apv_date");
		$setvalue = array($apv_stat, "t", $apv_date);
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order and apv_stat = 0";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロープレビュー
//-------------------------------------------------------------------------------------------------------------------------

	// 承認者管理情報取得(プレビュー)
	function get_approve_mng_for_wkfwpreview($data)
	{
		// 承認者階層数取得
		$approve_num = $data["approve_num"];

		$arr = array();

		for($i=0; $i<$approve_num; $i++)
		{
			$j = $i + 1;

			$tmp_apv_div0_flg = "apv_div0_flg".$j;
			$apv_div0_flg = $data[$tmp_apv_div0_flg];

			$tmp_apv_div1_flg = "apv_div1_flg".$j;
			$apv_div1_flg = $data[$tmp_apv_div1_flg];

			$tmp_apv_div2_flg = "apv_div2_flg".$j;
			$apv_div2_flg = $data[$tmp_apv_div2_flg];

			$tmp_apv_div3_flg = "apv_div3_flg".$j;
			$apv_div3_flg = $data[$tmp_apv_div3_flg];

			$tmp_apv_div4_flg = "apv_div4_flg".$j;
			$apv_div4_flg = $data[$tmp_apv_div4_flg];

			$tmp_target_class_div = "target_class_div".$j;
			$target_class_div = $data[$tmp_target_class_div];

			$tmp_st_id = "st_id".$j;
			$st_id = $data[$tmp_st_id];

			$tmp_multi_apv_flg = "multi_apv_flg".$j;
			$multi_apv_flg = $data[$tmp_multi_apv_flg];

			$tmp_apv_num = "apv_num".$j;
			$apv_num = $data[$tmp_apv_num];

			$tmp_emp_id = "emp_id".$j;
			$emp_id = $data[$tmp_emp_id];

			$tmp_pjt_parent_id = "pjt_parent_id".$j;
			$pjt_parent_id = $data[$tmp_pjt_parent_id];

			$tmp_pjt_child_id = "pjt_child_id".$j;
			$pjt_child_id = $data[$tmp_pjt_child_id];

			$tmp_class_sect_id = "class_sect_id".$j;
			$class_sect_id = $data[$tmp_class_sect_id];

			$tmp_atrb_sect_id = "atrb_sect_id".$j;
			$atrb_sect_id = $data[$tmp_atrb_sect_id];

			$tmp_dept_sect_id = "dept_sect_id".$j;
			$dept_sect_id = $data[$tmp_dept_sect_id];

			$tmp_room_sect_id = "room_sect_id".$j;
			$room_sect_id = $data[$tmp_room_sect_id];

			$tmp_st_sect_id = "st_sect_id".$j;
			$st_sect_id = $data[$tmp_st_sect_id];


			$apv_setting_flg = "f";
			if($apv_div0_flg == "f" && $apv_div1_flg == "f" && $apv_div2_flg == "t" && $apv_div3_flg == "f" && $apv_div4_flg == "f")
			{
				$apv_setting_flg = "t";
			}

			$arr[] = array("apv_order" => $j,
                            "apv_div0_flg" => $apv_div0_flg,
                            "apv_div1_flg" => $apv_div1_flg,
                            "apv_div2_flg" => $apv_div2_flg,
                            "apv_div3_flg" => $apv_div3_flg,
                            "apv_div4_flg" => $apv_div4_flg,
                            "target_class_div" => $target_class_div,
                            "st_id" => $st_id,
                            "multi_apv_flg" => $multi_apv_flg,
                            "emp_id" => $emp_id,
                            "pjt_parent_id" => $pjt_parent_id,
                            "pjt_child_id" => $pjt_child_id,
                            "class_sect_id" => $class_sect_id,
                            "atrb_sect_id" => $atrb_sect_id,
                            "dept_sect_id" => $dept_sect_id,
                            "room_sect_id" => $room_sect_id,
                            "st_sect_id" => $st_sect_id,
                            "apv_num" => $apv_num,
                            "apv_setting_flg" => $apv_setting_flg);
		}
		return $arr;
	}

	// 承認者詳細情報取得(プレビュー)
	function get_approve_dtl_for_wkfwpreview($arr_wkfwapvmng, $emp_id, $short_wkfw_name)
	{
		$arr = array();
		foreach($arr_wkfwapvmng as $apvmng)
		{
			$arr_apv = array();
			$multi_apv_flg = $apvmng["multi_apv_flg"];

			$apv_div0_flg = $apvmng["apv_div0_flg"];
			$apv_div1_flg = $apvmng["apv_div1_flg"];
			$apv_div2_flg = $apvmng["apv_div2_flg"];
			$apv_div3_flg = $apvmng["apv_div3_flg"];
			$apv_div4_flg = $apvmng["apv_div4_flg"];

			// 部署役職(申請者所属)指定
			if($apv_div0_flg == "t")
			{
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($apvmng["target_class_div"], $apvmng["st_id"], $emp_id);
				for($i=0; $i<count($arr_apvpstdtl); $i++)
				{
					$arr_apvpstdtl[$i]["apv_div"] = "0";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_apvpstdtl);

			}

			// 部署役職(部署指定)指定
			if($apv_div4_flg == "t")
			{
				$arr_emp_info_sect = $this->get_emp_info_for_post_sect($apvmng["class_sect_id"], $apvmng["atrb_sect_id"], $apvmng["dept_sect_id"], $apvmng["room_sect_id"], $apvmng["st_sect_id"]);

				for($i=0; $i<count($arr_emp_info_sect); $i++)
				{
					$arr_emp_info_sect[$i]["apv_div"] = "4";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info_sect);
			}

			// 職員指定
			if($apv_div1_flg == "t")
			{
				$arr_emp_id = split(",", $apvmng["emp_id"]);
				$arr_emp_info = array();
				foreach($arr_emp_id as $emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
 									   );
				}
				$apvmng["emp_id"] = "";

				for($i=0; $i<count($arr_emp_info); $i++)
				{
					$arr_emp_info[$i]["apv_div"] = "1";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_emp_info);
			}

			// 委員会・ＷＧ
			if($apv_div3_flg == "t")
			{
				$arr_project_member = $this->get_project_member($apvmng["pjt_parent_id"], $apvmng["pjt_child_id"], true);

				$pjt_nm = $this->get_pjt_nm($apvmng["pjt_parent_id"]);
				if($apvmng["pjt_child_id"] != "")
				{
					$pjt_nm .= " > ";
					$pjt_nm .= $this->get_pjt_nm($apvmng["pjt_child_id"]);
				}

				for($i=0; $i<count($arr_project_member); $i++)
				{
					$arr_project_member[$i]["apv_div"] = "3";
					$arr_project_member[$i]["pjt_nm"] = $pjt_nm;
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_project_member);
			}

			// その他
			if($apv_div2_flg == "t")
			{
				$apv_num = $apvmng["apv_num"];
				$arr_setting_apv = "";
				for($i=0; $i<$apv_num; $i++)
				{
					$arr_setting_apv[] = array();
				}

				for($i=0; $i<count($arr_setting_apv); $i++)
				{
					$arr_setting_apv[$i]["apv_div"] = "2";
				}

				// マージ処理
				$arr_apv = $this->merge_arr_emp_info($arr_apv, $arr_setting_apv);

			}

			// 複数承認者「許可する」
			if($multi_apv_flg == "t")
			{
				if(count($arr_apv) > 0)
				{
					if(count($arr_apv) > 1)
					{
						$apv_sub_order = 1;
						foreach($arr_apv as $apv)
						{
							$arr_emp = array();
							$apvmng["apv_sub_order"] = $apv_sub_order;
							$arr_emp[] = array(
                                                "emp_id" => $apv["emp_id"],
                                                "emp_full_nm" => $apv["emp_full_nm"],
                                                "st_nm" => $apv["st_nm"],
                                                "apv_div" => $apv["apv_div"],
                                                "pjt_nm" => $apv["pjt_nm"]
                                               );
							$apvmng["emp_infos"] = $arr_emp;

							if($apv["apv_div"] == "2")
							{
								$apvmng["apv_setting_flg"] = "t";
							}

							$arr[] = $apvmng;
							$apv_sub_order++;
						}
					}
					else
					{
						$arr_emp = array();
						$apvmng["apv_sub_order"] = "";
						$arr_emp[] = array(
                                            "emp_id" => $arr_apv[0]["emp_id"],
                                            "emp_full_nm" => $arr_apv[0]["emp_full_nm"],
                                            "st_nm" => $arr_apv[0]["st_nm"],
                                            "apv_div" => $arr_apv[0]["apv_div"],
                                            "pjt_nm" => $arr_apv[0]["pjt_nm"]
                                           );
						$apvmng["emp_infos"] = $arr_emp;

						if($arr_apv[0]["apv_div"] == "2")
						{
							$apvmng["apv_setting_flg"] = "t";
						}

						$arr[] = $apvmng;
					}
				}
				else
				{
					$apvmng["emp_infos"] = array();
					$arr[] = $apvmng;
				}
			}
			// 複数承認者「許可しない」
			else
			{
				$apvmng["apv_sub_order"] = "";
				$apvmng["emp_infos"] = $arr_apv;
				$arr[] = $apvmng;
			}
		}

		return $arr;

	}


	// 部署役職指定情報取得(プレビュー用)
	function get_apvpstdtl_for_wkfwpreview($target_class_div, $st_id, $emp_id)
	{
		$arr = array();

		// 申請者の部署役職取得
		$arr_emp_info = $this->get_emp_info($emp_id, false);

		if($target_class_div == "4")
		{
			$fourth_post_exist_flg = false;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_room  = $emp_info["emp_room"];
				if($emp_room != "")
				{
					$fourth_post_exist_flg = true;
					break;
				}
			}

			if(!$fourth_post_exist_flg)
			{
				return $arr;
			}
		}


		$sql  = "select ";
		$sql .= "emp.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, empmst.emp_room, empmst.emp_st, stmst.st_nm ";
		$sql .= "from ";
    	$sql .= "(";
    	$sql .= "select emp_id from empmst where ";
		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}

		$sql .= "empmst.emp_st in ($st_id) ";
		$sql .= "union ";
		$sql .= "select emp_id from concurrent where ";

		// 「部署指定しない」以外
		if($target_class_div != 0)
		{
			$sql .= "( ";
			$idx = 0;
			foreach($arr_emp_info as $emp_info)
			{
				$emp_class     = $emp_info["emp_class"];
				$emp_attribute = $emp_info["emp_attribute"];
				$emp_dept      = $emp_info["emp_dept"];
				$emp_room      = $emp_info["emp_room"];

				if($target_class_div == "4")
				{
					if($emp_room == "")
					{
						continue;
					}
				}

				if($idx > 0)
				{
					$sql .= "or ";
				}

				if($target_class_div == "1")
				{
					$sql .= "emp_class = $emp_class ";
				}
				else if($target_class_div == "2")
				{
					$sql .= "emp_attribute = $emp_attribute ";
				}
				else if($target_class_div == "3")
				{
					$sql .= "emp_dept = $emp_dept ";
				}
				else if($target_class_div == "4")
				{
					$sql .= "emp_room = $emp_room ";
				}
				$idx++;
			}
			$sql .= ") ";
			$sql .= "and ";
		}
		$sql .= "concurrent.emp_st in ($st_id) ";
		$sql .= ") emp ";
		$sql .= "inner join empmst on emp.emp_id = empmst.emp_id ";
		$sql .= "inner join authmst on emp.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "inner join stmst on empmst.emp_st = stmst.st_id and not stmst.st_del_flg ";
		$cond = "order by emp.emp_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"]
			               );
			$idx++;
		}
		return $arr;
	}

	// 申請者以外の結果通知者取得(プレビュー用)
	function get_wkfw_notice_for_wkfwpreview($data, $emp_id)
	{
		$arr_tmp = array();
		$notice = $data["notice"];

		if($notice != "")
		{
			$rslt_ntc_div0_flg = $data["rslt_ntc_div0_flg"];
			$rslt_ntc_div1_flg = $data["rslt_ntc_div1_flg"];
			$rslt_ntc_div2_flg = $data["rslt_ntc_div2_flg"];
			$rslt_ntc_div3_flg = $data["rslt_ntc_div3_flg"];
			$rslt_ntc_div4_flg = $data["rslt_ntc_div4_flg"];

			// 部署役職指定(申請者所属)
			if($rslt_ntc_div0_flg == "t")
			{
				$notice_target_class_div = $data["notice_target_class_div"];
				$notice_st_id = $data["notice_st_id"];
				$arr_apvpstdtl = $this->get_apvpstdtl_for_wkfwpreview($notice_target_class_div, $notice_st_id, $emp_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_apvpstdtl);
			}

			// 部署役職指定
			if($rslt_ntc_div4_flg == "t")
			{
				$arr_post_sect = $this->get_emp_info_for_post_sect($data["notice_class_sect_id"], $data["notice_atrb_sect_id"], $data["notice_dept_sect_id"], $data["notice_room_sect_id"], $data["notice_st_sect_id"]);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_post_sect);
			}

			// 職員指定
			if($rslt_ntc_div1_flg == "t")
			{
				$notice_emp_id = $data["notice_emp_id"];
				$arr_notice_emp_id = split(",", $notice_emp_id);
				foreach($arr_notice_emp_id as $notice_emp_id)
				{
					$arr_empmst_detail = $this->get_empmst_detail($notice_emp_id);
					$emp_lt_nm = $arr_empmst_detail[0]["emp_lt_nm"];
					$emp_ft_nm = $arr_empmst_detail[0]["emp_ft_nm"];
                    $emp_full_nm = $emp_lt_nm." ".$emp_ft_nm;
					$arr_emp_info[] = array(
									 "emp_id" => $arr_empmst_detail[0]["emp_id"],
									 "emp_full_nm" => $emp_full_nm,
									 "st_nm" => $arr_empmst_detail[0]["st_nm"]
									 );
				}

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_emp_info);
			}

			// 委員会・ＷＧ指定
			if($rslt_ntc_div3_flg == "t")
			{
				$notice_pjt_parent_id = $data["notice_pjt_parent_id"];
				$notice_pjt_child_id = $data["notice_pjt_child_id"];
				$arr_project_member = $this->get_project_member($notice_pjt_parent_id, $notice_pjt_child_id);

				// マージ処理
				$arr_tmp = $this->merge_arr_emp_info($arr_tmp, $arr_project_member);
			}
		}

		// 申請者を除去する
		$arr = array();
		foreach($arr_tmp as $tmp)
		{
			$tmp_emp_id = $tmp["emp_id"];
			if($tmp_emp_id == $emp_id)
			{
				continue;
			}
			$arr[] = $tmp;
		}
		return $arr;
	}

	// 部署役職(部署指定)情報取得
	function get_apvsectdtl($wkfw_id, $apv_order)
	{
		$sql   = "select a.class_id, a.atrb_id, a.dept_id, a.room_id ";
		$sql  .= "from cl_wkfwapvsectdtl a ";
		$cond  = "where wkfw_id = '$wkfw_id' and apv_order = $apv_order";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$class_id = pg_fetch_result($sel, 0, "class_id");
		$atrb_id = pg_fetch_result($sel, 0, "atrb_id");
		$dept_id = pg_fetch_result($sel, 0, "dept_id");
		$room_id = pg_fetch_result($sel, 0, "room_id");

		return array("class_id" => $class_id, "atrb_id" => $atrb_id, "dept_id" => $dept_id, "room_id" => $room_id);
	}

	// 役職情報取得
	function get_apvpstdtl($wkfw_id, $apv_order, $st_div)
	{
		$sql   = "select st_id from cl_wkfwapvpstdtl ";
		$cond  = "where wkfw_id = '$wkfw_id' and apv_order = $apv_order and st_div = $st_div order by st_id";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("st_id" => $row["st_id"]);
		}

		return $arr;
	}

	// 職員情報取得(部署役職指定用)
	function get_emp_info_for_post_sect($class_id, $attribute_id, $dept_id, $room_id, $st_sect_id)
	{

		$sql  = "select a.emp_id, a.emp_lt_nm, a.emp_ft_nm, c.st_nm ";
		$sql .= "from ( ";
		$sql .= "select varchar(1) '1' as type, emp_id, emp_lt_nm, emp_ft_nm, emp_st from empmst ";

		if($class_id != "")
		{
			$sql .= "where emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and emp_st in ($st_sect_id) ";
		}

		$sql .= "union ";
		$sql .= "select varchar(1) '2' as type, sub_a.emp_id, sub_b.emp_lt_nm, sub_b.emp_ft_nm, sub_a.emp_st from concurrent sub_a ";
		$sql .= "inner join empmst sub_b on sub_a.emp_id = sub_b.emp_id ";

		if($class_id != "")
		{
			$sql .= "where sub_a.emp_class = $class_id ";
		}

		if($attribute_id != "")
		{
			$sql .= "and sub_a.emp_attribute = $attribute_id ";
		}

		if($dept_id != "")
		{
			$sql .= "and sub_a.emp_dept = $dept_id ";
		}

		if($room_id != "")
		{
			$sql .= "and sub_a.emp_room = $room_id ";
		}

		if($st_sect_id)
		{
			$sql .= "and sub_a.emp_st in ($st_sect_id) ";
		}

		$sql .= ") a ";
		$sql .= "inner join authmst b on a.emp_id = b.emp_id and not b.emp_del_flg ";
		$sql .= "left join stmst c on a.emp_st = c.st_id and not c.st_del_flg ";
		$sql .= "order by a.emp_id asc, a.type asc";

		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		$tmp_emp_id = "";
		$idx = 1;
		while($row = pg_fetch_array($sel))
		{
			if($tmp_emp_id == $row["emp_id"])
			{
				continue;
			}
			$emp_full_nm = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
			$arr[] = array("emp_id" => $row["emp_id"],
			                "emp_full_nm" => $emp_full_nm,
			                "st_nm" => $row["st_nm"],
			                "apv_sub_order" => $idx);

			$tmp_emp_id = $row["emp_id"];
			$idx++;
		}
		return $arr;
	}
//-------------------------------------------------------------------------------------------------------------------------
// 申請結果通知関連
//-------------------------------------------------------------------------------------------------------------------------

	// 申請結果通知一覧取得
	function get_applynotice_list($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];
		$page         = $arr["page"];
		$max_page     = $arr["max_page"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select ";
		$sql  .= "b.*, ";
		$sql  .= "d.emp_lt_nm as apply_lt_nm, ";
		$sql  .= "d.emp_ft_nm as apply_ft_nm, ";
		$sql  .= "a.confirmed_flg, ";
		$sql  .= "a.send_date, ";
		$sql  .= "c.emp_lt_nm as send_lt_nm, ";
		$sql  .= "c.emp_ft_nm as send_ft_nm, ";
		$sql  .= "e.wkfw_nm, ";
		$sql  .= "e.wkfw_title, ";
		$sql  .= "e.short_wkfw_name, ";
		$sql  .= "e.wkfw_folder_id ";
		$sql  .= "from cl_applynotice a ";
		$sql  .= "inner join cl_apply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join ";
		$sql  .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id ";
		$sql  .= "from cl_wkfwmst a ";
		$sql  .= "inner join cl_wkfwcatemst b on ";
		$sql  .= "a.wkfw_type = b.wkfw_type) e on ";
		$sql  .= "b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$cond .= "order by a.send_date desc, b.apply_date desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}


	// 申請結果通知一覧件数取得
	function get_applynotice_list_count($arr)
	{

		$session      = $arr["session"];
		$apply_emp_nm = $arr["apply_emp_nm"];
		$apply_stat   = $arr["apply_stat"];
		$class        = $arr["class"];
		$attribute    = $arr["attribute"];
		$dept         = $arr["dept"];
		$room         = $arr["room"];

		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from cl_applynotice a ";
		$sql  .= "inner join cl_apply b on a.apply_id = b.apply_id ";
		$sql  .= "inner join empmst c on a.send_emp_id = c.emp_id ";
		$sql  .= "inner join empmst d on b.emp_id = d.emp_id ";
		$sql  .= "inner join cl_wkfwmst e on b.wkfw_id = e.wkfw_id ";

		$cond .= "where not a.delete_flg and a.send_emp_id is not null and not b.draft_flg ";
		$cond .= "and a.recv_emp_id = '$emp_id' ";

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(d.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or d.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_lt_nm || d.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (d.emp_kn_lt_nm || d.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and b.apply_stat = '$apply_stat' ";
		}

		// 部署
		if($class != "")
		{
			$cond .= "and b.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and b.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and b.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and b.emp_room = $room ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請結果通知一覧未確認件数取得
	function get_non_confirmed_notice_count($session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql   = "select ";
		$sql  .= "count(a.apply_id) as cnt ";
		$sql  .= "from cl_applynotice a ";
		$cond .= "where not a.delete_flg and not a.confirmed_flg ";
		$cond .= "and a.send_emp_id is not null and a.recv_emp_id = '$emp_id' ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 確認済みフラグ更新
	function update_confirmed_flg($apply_id, $session)
	{
		$arr_empmst = $this->get_empmst($session);
		$emp_id = $arr_empmst[0]["emp_id"];

		$sql = "update cl_applynotice set";
		$set = array("confirmed_flg");
		$setvalue = array("t");
		$cond = "where not confirmed_flg and apply_id = '$apply_id' and recv_emp_id = '$emp_id' ";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

	}

//-------------------------------------------------------------------------------------------------------------------------
// 申請参照一覧関連
//-------------------------------------------------------------------------------------------------------------------------
	// 申請書件数取得(下書きは除く)
	function get_all_apply_count($delete_flg)
	{
		$sql  = "select count(*) as cnt from cl_apply ";
		// 2012/04/13 Yamagawa add(s)
		$sql .= "inner join empmst c on cl_apply.emp_id = c.emp_id ";
		$sql .= "inner join authmst auth on NOT auth.emp_del_flg AND auth.emp_id = c.emp_id ";
		// 2012/04/13 Yamagawa add(e)
		$cond = "where not draft_flg and delete_flg = '$delete_flg' ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}

	// 申請書取得(下書きは除く)
	function get_apply_list($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition)
	{
		$page = ($page - 1) * 20;

		$sql = $this->get_normal_apply_manage_sql($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition);

		$cond = "";
		$cond .= "order by a.apply_date desc, a.apply_no desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 申請書件数取得(下書きは除く)
	function get_apply_list_count($delete_flg, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition)
	{
		$sql  = "select count(*) as cnt from ( ";
		$sql .= $this->get_normal_apply_manage_sql($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition);
		$sql .= ") all_list ";

		$cond = "";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0,"cnt");
	}


//-------------------------------------------------------------------------------------------------------------------------
// テンプレート履歴およびワークファイル履歴関連
//-------------------------------------------------------------------------------------------------------------------------

	// テンプレート履歴登録
	// 2012/01/27 Yamagawa upd リアル・エイリアス区分追加
	/*
	function regist_wkfw_template_history($wkfw_id, $wkfw_history_no, $wkfw_content)
	{
		$sql = "insert into cl_wkfw_template_history (wkfw_id, wkfw_history_no, wkfw_content) values (";
		$content = array($wkfw_id, $wkfw_history_no, pg_escape_string($wkfw_content));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	*/
	function regist_wkfw_template_history($wkfw_id, $wkfw_history_no, $wkfw_content, $mode)
	{
		if ($mode == "ALIAS")
		{
			$sql = "insert into cl_wkfw_template_history ";
		}
		else if ($mode == "REAL")
		{
			$sql = "insert into cl_wkfw_template_history_real ";
		}
		$sql .= "(wkfw_id, wkfw_history_no, wkfw_content) values (";
		$content = array($wkfw_id, $wkfw_history_no, pg_escape_string($wkfw_content));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// テンプレート履歴取得
	function get_wkfw_template_history($wkfw_id, $wkfw_history_no)
	{
		$sql  = "select * from cl_wkfw_template_history ";
		$cond = "where wkfw_id = '$wkfw_id' and wkfw_history_no = $wkfw_history_no";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}


		$wkfw_history_no = pg_fetch_result($sel, 0, "wkfw_history_no");
		$wkfw_content    = pg_fetch_result($sel, 0, "wkfw_content");

		$arr = array("wkfw_history_no" => $wkfw_history_no, "wkfw_content" => $wkfw_content);
		return $arr;
	}

	// テンプレート履歴No(ＭＡＸ値)取得
	function get_max_wkfw_history_no($wkfw_id, $mode)
	{
		if ($mode == "ALIAS")
		{
			$sql  = "select max(wkfw_history_no) as max from cl_wkfw_template_history ";
		}
		else if ($mode == "REAL")
		{
			$sql  = "select max(wkfw_history_no) as max from cl_wkfw_template_history_real ";
		}
		$cond = "where wkfw_id = '$wkfw_id' ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイル履歴登録
	function regist_wkfwfile_history($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, $wkfwfile_name)
	{
		$sql = "insert into cl_wkfwfile_history (wkfw_id, wkfwfile_no, wkfwfile_history_no, wkfwfile_name) values (";
		$content = array($wkfw_id, $wkfwfile_no, $wkfwfile_history_no, pg_escape_string($wkfwfile_name));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークファイル履歴取得
	function get_wkfwfile_history($wkfw_id, $wkfwfile_history_no)
	{
		$sql  = "select * from cl_wkfwfile_history ";
		$cond = "where wkfw_id = '$wkfw_id' and wkfwfile_history_no = $wkfwfile_history_no order by wkfwfile_no ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("wkfwfile_history_no" => $row["wkfwfile_history_no"], "wkfwfile_no" => $row["wkfwfile_no"], "wkfwfile_name" => $row["wkfwfile_name"]);
		}
		return $arr;
	}

	// ワークファイル履歴No(ＭＡＸ値)取得
	function get_max_wkfwfile_history_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_history_no) as max from cl_wkfwfile_history ";
		$cond = "where wkfw_id = '$wkfw_id' ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

	// ワークファイルNo(ＭＡＸ値)取得
	function get_max_wkfwfile_no($wkfw_id)
	{
		$sql  = "select max(wkfwfile_no) as max from cl_wkfwfile_history ";
		$cond = "where wkfw_id = '$wkfw_id' ";
		$sel  = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・本体情報関連
//-------------------------------------------------------------------------------------------------------------------------

	// 削除済みワークフロー取得
	function get_deleted_workflow_real()
	{
		// 2011/12/19 Yamagawa upd(s)
		//$sql  = "select wkfw_id, wkfw_title from cl_wkfwmst_real";
		$sql  = "select wkfw_id, wkfw_title, short_wkfw_name from cl_wkfwmst_real";
		// 2011/12/19 Yamagawa upd(e)
		$cond = "where wkfw_del_flg order by wkfw_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			// 2011/12/19 Yamagawa upd(s)
			//$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"], "short_wkfw_name" => $row["short_wkfw_name"]);
			// 2011/12/19 Yamagawa upd(e)
		}
		return $arr;
	}

	// 削除済みワークフロー数取得
	function get_deleted_workflow_real_cnt()
	{
		$sql  = "select count(*) as cnt from cl_wkfwmst_real";
		$cond = "where wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// すべてフォルダ用ワークフロー取得
	function get_wkfwmst_real()
	{
		// 2011/12/19 Yamagawa upd(s)
		//$sql  = "select wkfw_id, wkfw_title from cl_wkfwmst_real";
		$sql  = "select wkfw_id, wkfw_title, short_wkfw_name from cl_wkfwmst_real";
		// 2011/12/19 Yamagawa upd(e)
		$cond = "where not wkfw_del_flg order by wkfw_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		while($row = pg_fetch_array($sel))
		{
			// 2011/12/19 Yamagawa upd(s)
			//$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"]);
			$arr[] = array("wkfw_id" => $row["wkfw_id"], "wkfw_title" => $row["wkfw_title"], "short_wkfw_name" => $row["short_wkfw_name"]);
			// 2011/12/19 Yamagawa upd(e)
		}
		return $arr;
	}

	// すべてフォルダ用ワークフロー数取得
	function get_wkfwmst_real_cnt()
	{
		$sql  = "select count(*) as cnt from cl_wkfwmst_real";
		$cond = "where not wkfw_del_flg";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);

		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$num = pg_fetch_result($sel, 0, "cnt");
		return $num;
	}

	// ワークフローＩＤのＭＡＸ値取得
	function get_max_wkfw_id($mode)
	{
		if($mode == "ALIAS")
		{
			$sql = "select max(wkfw_id) as max from cl_wkfwmst";
		}
		else
		{
			$sql = "select max(wkfw_id) as max from cl_wkfwmst_real";
		}
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);

		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// エイリアスワークフローＩＤ取得
	function get_alias_wkfw_id($real_wkfw_id)
	{
		$sql  = "select alias_wkfw_id from cl_wkfwaliasmng";
		$cond = "where real_wkfw_id = '$real_wkfw_id' order by alias_wkfw_no asc";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("alias_wkfw_id" => $row["alias_wkfw_id"]);
		}
		return $arr;
	}

	// エイリアス管理登録
	function regist_wkfwaliasmng($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no)
	{
		$sql = "insert into cl_wkfwaliasmng (real_wkfw_id, alias_wkfw_id, alias_wkfw_no, delete_flg) values (";
		$content = array($real_wkfw_id, $alias_wkfw_id, $alias_wkfw_no, "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// エイリアスワークフローＮｏ(ＭＡＸ)取得
	function get_max_alias_wkfw_no($real_wkfw_id)
	{
		$sql  = "select max(alias_wkfw_no) from cl_wkfwaliasmng";
		$cond = "where real_wkfw_id = '$real_wkfw_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "max");
	}

//-------------------------------------------------------------------------------------------------------------------------
// エイリアスワークフローコピー処理
//-------------------------------------------------------------------------------------------------------------------------

	// ワークフロー情報登録
	function regist_copy_wkfwmst($real_wkfw_id, $alias_wkfw_id, $wkfw_type, $wkfw_folder_id)
	{
		$wkfw_folder_id = ($wkfw_folder_id == "") ? "null" : $wkfw_folder_id;

		$sql  = "insert into cl_wkfwmst( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "$wkfw_type, ";
		$sql .= "wkfw_title, ";
		$sql .= "wkfw_content, ";
		$sql .= "'f', ";
		$sql .= "wkfw_start_date, ";
		$sql .= "wkfw_end_date, ";
		$sql .= "wkfw_appr, ";
		$sql .= "wkfw_content_type, ";
		$sql .= "$wkfw_folder_id, ";
		$sql .= "ref_dept_st_flg, ";
		$sql .= "ref_dept_flg, ";
		$sql .= "ref_st_flg, ";
		$sql .= "short_wkfw_name, ";
		$sql .= "apply_title_disp_flg ";
		$sql .= "from cl_wkfwmst_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";
		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認者管理登録
	function regist_copy_wkfwapvmng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwapvmng( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "apv_order, ";
		$sql .= "deci_flg, ";
		$sql .= "target_class_div, ";
		$sql .= "multi_apv_flg, ";
		$sql .= "next_notice_div, ";
		$sql .= "apv_div0_flg, ";
		$sql .= "apv_div1_flg, ";
		$sql .= "apv_div2_flg, ";
		$sql .= "apv_div3_flg, ";
		$sql .= "apv_div4_flg, ";
		$sql .= "apv_num, ";
		$sql .= "apv_div5_flg, ";
		$sql .= "apv_div6_flg, ";
		$sql .= "apv_div7_flg, ";
		$sql .= "apv_div8_flg, ";
		$sql .= "apv_div9_flg, ";
		$sql .= "apv_div10_flg, ";
		$sql .= "apv_div11_flg, ";
		$sql .= "apv_div12_flg ";
		$sql .= "from cl_wkfwapvmng_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by apv_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定登録
	function regist_copy_wkfwapvdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwapvdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "apv_order, ";
		$sql .= "emp_id, ";
		$sql .= "apv_sub_order ";
		$sql .= "from cl_wkfwapvdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by apv_order, apv_sub_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(申請者所属)指定登録
	function regist_copy_wkfwapvpstdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwapvpstdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "apv_order, ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from cl_wkfwapvpstdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定登録
	function regist_copy_wkfwpjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwpjtdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "apv_order, ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from cl_wkfwpjtdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職(部署指定)指定登録
	function regist_copy_wkfwapvsectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwapvsectdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "apv_order, ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from cl_wkfwapvsectdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by apv_order ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 結果通知管理登録
	function regist_copy_wkfwnoticemng($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwnoticemng( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "target_class_div, ";
		$sql .= "rslt_ntc_div0_flg, ";
		$sql .= "rslt_ntc_div1_flg, ";
		$sql .= "rslt_ntc_div2_flg, ";
		$sql .= "rslt_ntc_div3_flg, ";
		$sql .= "rslt_ntc_div4_flg ";
		$sql .= "from cl_wkfwnoticemng_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 職員指定(結果通知)登録
	function regist_copy_wkfwnoticedtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwnoticedtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "emp_id ";
		$sql .= "from cl_wkfwnoticedtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)登録
	function regist_copy_wkfwnoticestdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwnoticestdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "st_id, ";
		$sql .= "st_div ";
		$sql .= "from cl_wkfwnoticestdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 委員会・ＷＧ指定(結果通知)登録
	function regist_copy_wkfwnoticepjtdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwnoticepjtdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "parent_pjt_id, ";
		$sql .= "child_pjt_id ";
		$sql .= "from cl_wkfwnoticepjtdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 部署役職指定(結果通知)(部署指定)登録
	function regist_copy_wkfwnoticesectdtl($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwnoticesectdtl( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id, ";
		$sql .= "room_id ";
		$sql .= "from cl_wkfwnoticesectdtl_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 前提とする申請書(ワークフロー用)登録
	function regist_copy_wkfwfprecond($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwfprecond( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "precond_order, ";
		$sql .= "precond_wkfw_id ";
		$sql .= "from cl_wkfwfprecond_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' order by precond_order";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（科）登録
	function regist_copy_wkfw_refdept($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfw_refdept( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "class_id, ";
		$sql .= "atrb_id, ";
		$sql .= "dept_id ";
		$sql .= "from cl_wkfw_refdept_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（役職）登録
	function regist_copy_wkfw_refst($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfw_refst( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "st_id ";
		$sql .= "from cl_wkfw_refst_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー用アクセス権（職員）登録
	function regist_copy_wkfw_refemp($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfw_refemp( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "emp_id ";
		$sql .= "from cl_wkfw_refemp_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォーマットファイル登録
	function regist_copy_wkfwfile($real_wkfw_id, $alias_wkfw_id)
	{
		$sql  = "insert into cl_wkfwfile( ";
		$sql .= "select '$alias_wkfw_id', ";
		$sql .= "wkfwfile_no, ";
		$sql .= "wkfwfile_name ";
		$sql .= "from cl_wkfwfile_real ";
		$sql .= "where wkfw_id = '$real_wkfw_id' ";

		$content = "";
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// ワークフロー・ドラッグアンドドロップ処理
//-------------------------------------------------------------------------------------------------------------------------

	// 指定フォルダの子フォルダ取得
	function get_child_folder($wkfw_parent_id, &$res)
	{
		$arr_wkfw_child_folder = $this->get_wkfw_child_folder($wkfw_parent_id);
		foreach($arr_wkfw_child_folder as $wkfw_child_folder)
		{
			$res[] = $wkfw_child_folder;

			$this->get_child_folder($wkfw_child_folder, $res);
		}
	}

	// 子フォルダ取得
	function get_wkfw_child_folder($wkfw_parent_id)
	{
		$sql  = "select wkfw_child_id from cl_wkfwtree ";
		$cond = "where wkfw_parent_id = $wkfw_parent_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_child_id"];
		}
		return $arr;
	}

	// 指定フォルダの親フォルダ取得
	function get_parent_folder($wkfw_child_id)
	{
		// 親フォルダ情報を取得
		$sql = "select wkfw_parent_id from cl_wkfwtree";
		$cond = "where wkfw_child_id = $wkfw_child_id and not wkfw_tree_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "wkfw_parent_id");
	}

	// カテゴリ論理削除
	function update_wkfwcatemst_delflg($wkfw_type, $wkfwcate_del_flg)
	{
		$sql = "update cl_wkfwcatemst set";
		$set = array("wkfwcate_del_flg");
		$setvalue = array($wkfwcate_del_flg);
		$cond = "where wkfw_type = $wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ取得(wkfw_folder_idの最大値取得)
	function get_max_wkfw_folder_id()
	{
		$sql = "select max(wkfw_folder_id) as max from cl_wkfwfolder";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// カテゴリ取得(wkfw_typeの最大値取得)
	function get_max_wkfw_type()
	{
		$sql = "select max(wkfw_type) as max from cl_wkfwcatemst";
		$sel = select_from_table($this->_db_con, $sql, "", $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "max");
	}

	// ワークフローフォルダ登録
	function regist_wkfwfolder($arr)
	{
		$wkfw_folder_id      = $arr["wkfw_folder_id"];
		$wkfw_type           = $arr["wkfw_type"];
		$wkfw_folder_name    = $arr["wkfw_folder_name"];
		$ref_dept_st_flg     = $arr["ref_dept_st_flg"];
		$ref_dept_flg        = $arr["ref_dept_flg"];
		$ref_st_flg          = $arr["ref_st_flg"];

		$sql = "insert into cl_wkfwfolder (wkfw_folder_id, wkfw_type, wkfw_folder_name, wkfw_folder_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_folder_id, $wkfw_type, pg_escape_string($wkfw_folder_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（科）登録
	function regist_wkfwfolder_refdept($wkfw_folder_id, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into cl_wkfwfolder_refdept (wkfw_folder_id, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_folder_id, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（役職）登録
	function regist_wkfwfolder_refst($wkfw_folder_id, $st_id)
	{
		$sql = "insert into cl_wkfwfolder_refst (wkfw_folder_id, st_id) values (";
		$content = array($wkfw_folder_id, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダ用アクセス権（職員）登録
	function regist_wkfwfolder_refemp($wkfw_folder_id, $emp_id)
	{
		$sql = "insert into cl_wkfwfolder_refemp (wkfw_folder_id, emp_id) values (";
		$content = array($wkfw_folder_id, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update cl_wkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ファルダＩＤ更新
	function update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id)
	{
		$sql = "update cl_wkfwmst set";
		$set = array("wkfw_folder_id");
		$setvalue = array($new_wkfw_folder_id);
		$cond = "where wkfw_id = '$wkfw_id' and wkfw_folder_id is null";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフローＩ取得
	function get_wkfw_id_for_categoryDD($dest_wkfw_type)
	{
		$sql  = "select wkfw_id from cl_wkfwmst ";
		$cond = "where wkfw_type = $dest_wkfw_type and wkfw_folder_id is null and not wkfw_del_flg ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_id"];
		}
		return $arr;

	}

	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type)
	{
		$sql = "update cl_wkfwfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー登録
	function regist_wkfwtree($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "insert into cl_wkfwtree (wkfw_parent_id, wkfw_child_id, wkfw_tree_del_flg) values (";
		$content = array($wkfw_parent_id, $wkfw_child_id, "f");
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg($wkfw_child_id, $wkfw_tree_del_flg)
	{
		$sql = "update cl_wkfwtree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー論理削除
	function update_wkfwtree_del_flg_for_parent_id($wkfw_parent_id, $wkfw_tree_del_flg)
	{
		$sql = "update cl_wkfwtree set";
		$set = array("wkfw_tree_del_flg");
		$setvalue = array($wkfw_tree_del_flg);
		$cond = "where wkfw_parent_id = $wkfw_parent_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・親フォルダＩＤ更新
	function update_wkfwtree_wkfw_parent_id($wkfw_parent_id, $wkfw_child_id)
	{
		$sql = "update cl_wkfwtree set";
		$set = array("wkfw_parent_id");
		$setvalue = array($wkfw_parent_id);
		$cond = "where wkfw_child_id = $wkfw_child_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// フォルダツリー・件数取得
	function get_wkfwtree_count($wkfw_child_id)
	{
		$sql  = "select count(wkfw_parent_id) as cnt from cl_wkfwtree";
		$cond = "where not wkfw_tree_del_flg and wkfw_child_id = $wkfw_child_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// ワークフローフォルダＩＤ取得
	function get_wkfwfolder_from_wkfw_type($wkfw_type)
	{
		$sql  = "select wkfw_folder_id from cl_wkfwfolder";
		$cond = "where not wkfw_folder_del_flg and wkfw_type = $wkfw_type order by wkfw_folder_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = $row["wkfw_folder_id"];
		}
		return $arr;
	}


	// ワークフローフォルダ・ワークフロータイプ更新
	function update_wkfwfolder_wkfw_type_from_folder_id($wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update cl_wkfwfolder set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ更新
	function update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update cl_wkfwmst set";
		$set = array("wkfw_type");
		$setvalue = array($dest_wkfw_type);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ登録
	function regist_wkfwcatemst($wkfw_type, $category_name, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg)
	{
		$sql = "insert into cl_wkfwcatemst (wkfw_type, wkfw_nm, wkfwcate_del_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg) values (";
		$content = array($wkfw_type, pg_escape_string($category_name), "f", $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg);
		$result = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($result == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリ用アクセス権（科）登録
	function regist_wkfwcate_refdept($wkfw_type, $class_id, $atrb_id, $dept_id)
	{
		$sql = "insert into cl_wkfwcate_refdept (wkfw_type, class_id, atrb_id, dept_id) values (";
		$content = array($wkfw_type, $class_id, $atrb_id, $dept_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ用アクセス権（役職）登録
	function regist_wkfwcate_refst($wkfw_type, $st_id)
	{
		$sql = "insert into cl_wkfwcate_refst (wkfw_type, st_id) values (";
		$content = array($wkfw_type, $st_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // カテゴリ用アクセス権（職員）登録
	function regist_wkfwcate_refemp($wkfw_type, $emp_id)
	{
		$sql = "insert into cl_wkfwcate_refemp (wkfw_type, emp_id) values (";
		$content = array($wkfw_type, $emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// カテゴリフォルダ論理削除
	function update_wkfwfolder_del_flg($wkfw_folder_id, $del_flg)
	{
		$sql = "update cl_wkfwfolder set";
		$set = array("wkfw_folder_del_flg");
		$setvalue = array($del_flg);
		$cond = "where wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// ワークフロー申請書・ワークフロータイプ、フォルダーＩＤ更新
	function update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $wkfw_folder_id, $dest_wkfw_type)
	{
		$sql = "update cl_wkfwmst set";
		$set = array("wkfw_type", "wkfw_folder_id");
		$setvalue = array($dest_wkfw_type, null);
		$cond = "where wkfw_type = $src_wkfw_type and wkfw_folder_id = $wkfw_folder_id";

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}


	// カテゴリ移動
	function move_category($src_wkfw_type, $dest_wkfw_type, $dest_wkfw_folder_id)
	{
		// カテゴリ情報取得
	    $arr_wkfwcate_mst = $this->get_wkfwcate_mst($src_wkfw_type);

		// カテゴリ論理削除
		$this->update_wkfwcatemst_delflg($src_wkfw_type, "t");

		// フォルダ新規作成
		$max_wkfw_folder_id = $this->get_max_wkfw_folder_id();
		$new_wkfw_folder_id = intval($max_wkfw_folder_id) + 1;

		$arr = array
		(
			"wkfw_folder_id"     => $new_wkfw_folder_id,
			"wkfw_type"          => $dest_wkfw_type,
			"wkfw_folder_name"   => $arr_wkfwcate_mst["wkfw_nm"],
			"ref_dept_st_flg"    => $arr_wkfwcate_mst["ref_dept_st_flg"],
			"ref_dept_flg"       => $arr_wkfwcate_mst["ref_dept_flg"],
			"ref_st_flg"         => $arr_wkfwcate_mst["ref_st_flg"]
		);
		$this->regist_wkfwfolder($arr);

		// フォルダのアクセス権登録
		// カテゴリ用アクセス権（科）取得
		$arr_wkfwcate_refdept = $this->get_wkfwcate_refdept($src_wkfw_type);
		foreach($arr_wkfwcate_refdept as $wkfwcate_refdept)
		{
			$this->regist_wkfwfolder_refdept($new_wkfw_folder_id, $wkfwcate_refdept["class_id"], $wkfwcate_refdept["atrb_id"], $wkfwcate_refdept["dept_id"]);
		}

		// カテゴリ用アクセス権（役職）取得
		$arr_wkfwcate_refst = $this->get_wkfwcate_refst($src_wkfw_type);
		foreach($arr_wkfwcate_refst as $wkfwcate_refst)
		{
			$this->regist_wkfwfolder_refst($new_wkfw_folder_id, $wkfwcate_refst["st_id"]);
		}

	    // カテゴリ用アクセス権（職員）取得
		$arr_wkfwcate_refemp = $this->get_wkfwcate_refemp($src_wkfw_type);
		foreach($arr_wkfwcate_refemp as $wkfwcate_refemp)
		{
			$this->regist_wkfwfolder_refemp($new_wkfw_folder_id, $wkfwcate_refemp["emp_id"]);
		}

		// 遷移元カテゴリの直下にあるフォルダを取得。
		$arr_wkfwfolder = $this->get_wkfwfolder_from_wkfw_type($src_wkfw_type);
		foreach($arr_wkfwfolder as $wkfw_folder_id)
		{
			$wkfwtree_count = $this->get_wkfwtree_count($wkfw_folder_id);

			if($wkfwtree_count == 0)
			{
				// ワークフローツリー登録
				$this->regist_wkfwtree($new_wkfw_folder_id, $wkfw_folder_id);
			}
		}

		// 遷移先がフォルダの場合
		if($dest_wkfw_folder_id != "")
		{
			$this->regist_wkfwtree($dest_wkfw_folder_id, $new_wkfw_folder_id);
		}

		// ワークフローフォルダ・ワークフロータイプ更新
		$this->update_wkfwfolder_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフローＩＤ取得
		$arr_wkfw_id = $this->get_wkfw_id_for_categoryDD($src_wkfw_type);

		// ワークフロー申請書・ワークフロータイプ更新
		$this->update_wkfwmst_wkfw_type($src_wkfw_type, $dest_wkfw_type);

		// ワークフロー申請書・ファルダＩＤ	更新
		foreach($arr_wkfw_id as $wkfw_id)
		{
			$this->update_wkfwmst_wkfw_folder_id($new_wkfw_folder_id, $wkfw_id);
		}
	}




	// フォルダ移動
	function move_folder($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type, $dest_wkfw_folder_id, $root_flg)
	{
		// 遷移先がルートの場合
		if($root_flg)
		{
			// 遷移元フォルダ情報取得
			$arr_wkfwfolder_mst = $this->get_wkfwfolder_mst($src_wkfw_folder_id);

			// 遷移先カテゴリ新規作成
			$max_wkfw_type = $this->get_max_wkfw_type();
			$new_wkfw_type = intval($max_wkfw_type) + 1;

			$this->regist_wkfwcatemst($new_wkfw_type,
			                          $arr_wkfwfolder_mst["wkfw_folder_name"],
			                          $arr_wkfwfolder_mst["ref_dept_st_flg"],
			                          $arr_wkfwfolder_mst["ref_dept_flg"],
			                          $arr_wkfwfolder_mst["ref_st_flg"]);

			// 遷移先カテゴリアクセス権作成
			// 遷移元のフォルダ用アクセス権（科）取得、カテゴリ用アクセス権（科）登録
			$arr_wkfwfolder_refdept = $this->get_wkfwfolder_refdept($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refdept as $wkfwfolder_refdept)
			{
				$this->regist_wkfwcate_refdept($new_wkfw_type, $wkfwfolder_refdept["class_id"], $wkfwfolder_refdept["atrb_id"], $wkfwfolder_refdept["dept_id"]);
			}

			// フォルダ用アクセス権（役職）取得、カテゴリ用アクセス権（役職）登録
			$arr_wkfwfolder_refst = $this->get_wkfwfolder_refst($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refst as $wkfwfolder_refst)
			{
				$this->regist_wkfwcate_refst($new_wkfw_type, $wkfwfolder_refst["st_id"]);
			}
			// フォルダ用アクセス権（職員）取得、カテゴリ用アクセス権（職員）登録
			$arr_wkfwfolder_refemp = $this->get_wkfwfolder_refemp($src_wkfw_folder_id);
			foreach($arr_wkfwfolder_refemp as $wkfwfolder_refemp)
			{
				$this->regist_wkfwcate_refemp($new_wkfw_type, $wkfwfolder_refemp["emp_id"]);
			}

			// 遷移元フォルダ論理削除
			$this->update_wkfwfolder_del_flg($src_wkfw_folder_id, "t");

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// 遷移元フォルダが親または子フォルダをもっている場合、ワークフローツリー論理削除
			$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			$this->update_wkfwtree_del_flg_for_parent_id($src_wkfw_folder_id, "t");

			// ワークフローフォルダ・ワークフロータイプ更新(子フォルダ)
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $new_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_folder_id($src_wkfw_type, $src_wkfw_folder_id, $new_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $new_wkfw_type);
			}

		}
		else
		{
			// 遷移先がカテゴリの場合
			if($dest_wkfw_folder_id == "")
			{
				// 遷移元フォルダが親フォルダをもっている場合、ワークフローツリー論理削除
				$this->update_wkfwtree_del_flg($src_wkfw_folder_id, "t");
			}
			// 遷移先がフォルダの場合
			else
			{
				$wkfwtree_count = $this->get_wkfwtree_count($src_wkfw_folder_id);
				if($wkfwtree_count > 0)
				{
					// ワークフローツリー更新
					$this->update_wkfwtree_wkfw_parent_id($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
				else
				{
					// ワークフローツリー登録
					$this->regist_wkfwtree($dest_wkfw_folder_id, $src_wkfw_folder_id);
				}
			}

			// 遷移元フォルダの子フォルダを取得
			$arr_child_folder = array();
			$this->get_child_folder($src_wkfw_folder_id, $arr_child_folder);

			// ワークフローフォルダ・ワークフロータイプ更新
			$this->update_wkfwfolder_wkfw_type_from_folder_id($src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwfolder_wkfw_type_from_folder_id($child_folder_id, $dest_wkfw_type);
			}

			// ワークフロー申請書・ワークフロータイプ更新
			$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $src_wkfw_folder_id, $dest_wkfw_type);
			foreach($arr_child_folder as $child_folder_id)
			{
				$this->update_wkfwmst_wkfw_type_from_folder_id($src_wkfw_type, $child_folder_id, $dest_wkfw_type);
			}
		}
	}


//-------------------------------------------------------------------------------------------------------------------------
// 申請一覧用
//-------------------------------------------------------------------------------------------------------------------------

	// 通常申請のＳＱＬ取得
	function get_normal_apply_sql($arr_cond)
	{
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$class_yyyy_from = $arr_cond["class_yyyy_from"];
		$class_mm_from   = $arr_cond["class_mm_from"];
		$class_dd_from   = $arr_cond["class_dd_from"];
		$class_yyyy_to   = $arr_cond["class_yyyy_to"];
		$class_mm_to     = $arr_cond["class_mm_to"];
		$class_dd_to     = $arr_cond["class_dd_to"];
		$apply_stat      = $arr_cond["apply_stat"];
		$training_div    = $arr_cond["training_div"];


        $sql  = "select ";
        $sql .= "varchar(1) '1' as apply_type, ";
        $sql .= "A.apply_id, ";
        $sql .= "A.apply_title, ";
        $sql .= "A.apply_date, ";
        $sql .= "A.apply_stat, ";
        $sql .= "A.apply_no, ";
        $sql .= "A.apv_fix_show_flg, ";
        $sql .= "A.apv_bak_show_flg, ";
        $sql .= "A.apv_ng_show_flg, ";
        $sql .= "B.wkfw_title, ";
        $sql .= "B.wkfw_nm, ";
        $sql .= "B.short_wkfw_name, ";
        $sql .= "B.wkfw_folder_id, ";
        $sql .= "null as pjt_schd_id, ";
        $sql .= "A.apply_content, ";
        $sql .= "training_name_table.training_name ";
		// 2012/09/21 Yamagawa add(s)
		$sql .= ",levelup_emp_info_table.emp_lt_nm as levelup_emp_lt_nm ";
		$sql .= ",levelup_emp_info_table.emp_ft_nm as levelup_emp_ft_nm ";
		$sql .= ",levelup_emp_info_table.dept_nm as levelup_emp_dept_nm ";
		// 2012/09/21 Yamagawa add(e)

        $sql .= "from cl_apply A ";

		//検索条件として開催日が指定されている場合
		if( ($class_yyyy_from != "" && $class_yyyy_from != "-") || ($class_yyyy_to != "" && $class_yyyy_to != "-") )
		{
			//開催日の条件を満たすapply_idで絞込み
			$sql .= " inner join";
			$sql .= " (";
			$sql .= $this->get_plan_date_match_apply_id_list_sql($class_yyyy_from,$class_mm_from,$class_dd_from,$class_yyyy_to,$class_mm_to,$class_dd_to);
			$sql .= " ) plan_search";
			$sql .= " on a.apply_id = plan_search.apply_id ";
		}

        //ワークフロー情報を付加
        $sql .= " inner join ";
        $sql .= " ( ";
        $sql .= "   select WM.wkfw_id, WM.wkfw_type, WM.wkfw_title, CATE.wkfw_nm, WM.short_wkfw_name, WM.wkfw_folder_id ";
        $sql .= "   from cl_wkfwmst WM ";
        $sql .= "   inner join cl_wkfwcatemst CATE on ";
        $sql .= "   WM.wkfw_type = CATE.wkfw_type ";
        $sql .= " ) B on A.wkfw_id = B.wkfw_id ";

		//研修名を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= $this->get_apply_id_training_name_list_sql();//申請ID(apply_id)と研修名(training_name)の全リスト
        $sql .= " ) training_name_table ";
        $sql .= " on A.apply_id = training_name_table.apply_id ";

		// 2012/09/21 Yamagawa add(s)
		//志願者IDを付加
		$sql .= " left join ";
		$sql .= " ( ";
		$sql .= $this->get_apply_id_levelup_emp_id_list_sql();//申請ID(apply_id)と志願者ID(levelup_emp_id)の全リスト
		$sql .= " ) levelup_emp_id_table ";
		$sql .= " on A.apply_id = levelup_emp_id_table.apply_id ";

		//志願者氏名と所属(第三階層)を付加
		$sql .= " left join ";
		$sql .= " ( ";
		$sql .= "   select ";
		$sql .= "   a.emp_id, ";
		$sql .= "   a.emp_lt_nm, ";
		$sql .= "   a.emp_ft_nm, ";
		$sql .= "   b.dept_nm ";
		$sql .= "   from empmst a ";
		$sql .= "   inner join deptmst b ";
		$sql .= "   on a.emp_dept = b.dept_id ";
		$sql .= " ) levelup_emp_info_table ";
		$sql .= " on levelup_emp_id_table.levelup_emp_id = levelup_emp_info_table.emp_id ";
		// 2012/09/21 Yamagawa add(e)

        $sql .= "where A.emp_id = '$emp_id' ";
        $sql .= "and A.re_apply_id is null ";
        $sql .= "and not A.delete_flg ";
        $sql .= "and not A.draft_flg ";

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-") {
			$sql .= "and B.wkfw_type = $wkfw_type ";
		}

		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-") {
			$sql .= "and B.wkfw_id = '$wkfw_id' ";
		}

		// 表題
		if($apply_title != "") {
			$sql .= "and A.apply_title like '%$apply_title%' ";
		}

		// 承認者
		if($emp_nm != "") {

            $sql .= "and exists ";
            $sql .= "(select APV.apply_id from cl_applyapv APV ";
            $sql .= "where exists (select * from empmst EMP where APV.emp_id = EMP.emp_id and ";
            $sql .= "(EMP.emp_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_ft_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_lt_nm like '%$emp_nm%' ";
            $sql .= "or EMP.emp_kn_ft_nm like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_lt_nm || EMP.emp_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%' ";
            $sql .= "or (EMP.emp_kn_lt_nm || EMP.emp_kn_ft_nm) like '%$emp_nm%')) ";
            $sql .= "and A.apply_id = APV.apply_id ";
            $sql .= "group by APV.apply_id) ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(A.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-") {
			$sql .= "and A.apply_stat = '$apply_stat' ";
		}

		// 開催日
		// join時に絞込み

		// 研修区分
		$short_wkfw_name_list = $this->get_short_wkfw_name_list_from_training_div($training_div);
		if($short_wkfw_name_list != "")
		{
            $sql .= "and B.short_wkfw_name in(".$short_wkfw_name_list.") ";
		}

		return $sql;
	}


	// 申請一覧取得ＳＱＬ
    function get_applylist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apply_stat      = $arr_cond["apply_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_apply_sql($arr_cond);
		}
		return $sql;
    }


//-------------------------------------------------------------------------------------------------------------------------
// 申請一覧(管理画面)用
//-------------------------------------------------------------------------------------------------------------------------

	// 申請一覧(管理画面)用のＳＱＬ取得
	function get_normal_apply_manage_sql($delete_flg, $page, $max_page, $selected_cate, $selected_folder, $selected_wkfw_id, $arr_condition)
	{

		$category           = $arr_condition["category"];
		$workflow           = $arr_condition["workflow"];
		$apply_title        = $arr_condition["apply_title"];
		$apply_emp_nm       = $arr_condition["apply_emp_nm"];
		$apply_yyyy_from    = $arr_condition["date_y1"];
		$apply_mm_from      = $arr_condition["date_m1"];
		$apply_dd_from      = $arr_condition["date_d1"];
		$apply_yyyy_to      = $arr_condition["date_y2"];
		$apply_mm_to        = $arr_condition["date_m2"];
		$apply_dd_to        = $arr_condition["date_d2"];
		$apply_stat         = $arr_condition["apply_stat"];
		$class_yyyy_from    = $arr_condition["date_y3"];
		$class_mm_from      = $arr_condition["date_m3"];
		$class_dd_from      = $arr_condition["date_d3"];
		$class_yyyy_to      = $arr_condition["date_y4"];
		$class_mm_to        = $arr_condition["date_m4"];
		$class_dd_to        = $arr_condition["date_d4"];
		$class              = $arr_condition["class"];
		$attribute          = $arr_condition["attribute"];
		$dept               = $arr_condition["dept"];
		$room               = $arr_condition["room"];
		$apply_content      = $arr_condition["apply_content"];
		$training_div       = $arr_condition["training_div"];


		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.wkfw_title, ";
		$sql  .= "b.short_wkfw_name, ";
		$sql  .= "c.emp_lt_nm, ";
		$sql  .= "c.emp_ft_nm, ";
		$sql  .= "training_name_table.training_name, ";
		$sql  .= "dept_nm_table.dept_nm ";
		// 2012/09/25 Yamagawa add(s)
		$sql .= ",levelup_emp_info_table.emp_lt_nm as levelup_emp_lt_nm ";
		$sql .= ",levelup_emp_info_table.emp_ft_nm as levelup_emp_ft_nm ";
		$sql .= ",levelup_emp_info_table.dept_nm as levelup_emp_dept_nm ";
		// 2012/09/25 Yamagawa add(e)

		$sql  .= "from cl_apply a ";

		//検索条件として開催日が指定されている場合
		if( ($class_yyyy_from != "" && $class_yyyy_from != "-") || ($class_yyyy_to != "" && $class_yyyy_to != "-") )
		{
			//開催日の条件を満たすapply_idで絞込み
			$sql .= " inner join";
			$sql .= " (";
			$sql .= $this->get_plan_date_match_apply_id_list_sql($class_yyyy_from,$class_mm_from,$class_dd_from,$class_yyyy_to,$class_mm_to,$class_dd_to);
			$sql .= " ) plan_search";
			$sql .= " on a.apply_id = plan_search.apply_id ";
		}

		//検索条件として文字列検索が指定されている場合
		if ($apply_content != "")
		{
			//文字列検索の条件を満たすapply_idで絞込み
			$sql .= " inner join";
			$sql .= " (";
			$sql .= $this->get_text_match_apply_id_list_sql($apply_content);
			$sql .= " ) text_search";
			$sql .= " on a.apply_id = text_search.apply_id ";
		}

		$sql  .= "left join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";

		$sql  .= "inner join empmst c on a.emp_id = c.emp_id ";

		// 2012/04/13 Yamagawa add(s)
		$sql  .= "inner join authmst auth on NOT auth.emp_del_flg AND auth.emp_id = c.emp_id ";
		// 2012/04/13 Yamagawa add(e)

		$sql  .= "left join cl_training_history t on a.apply_id = t.apply_id ";


		//研修名を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= $this->get_apply_id_training_name_list_sql();//申請ID(apply_id)と研修名(training_name)の全リスト
        $sql .= " ) training_name_table ";
        $sql .= " on a.apply_id = training_name_table.apply_id ";


		//申請者の所属(第三階層)を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= "   select ";
        $sql .= "   a.emp_id, ";
        $sql .= "   b.dept_nm ";
        $sql .= "   from empmst a ";
        $sql .= "   inner join deptmst b ";
        $sql .= "   on a.emp_dept = b.dept_id ";
        $sql .= " ) dept_nm_table ";
        $sql .= " on a.emp_id = dept_nm_table.emp_id ";

		// 2012/09/25 Yamagawa add(s)
		//志願者IDを付加
		$sql .= " left join ";
		$sql .= " ( ";
		$sql .= $this->get_apply_id_levelup_emp_id_list_sql();//申請ID(apply_id)と志願者ID(levelup_emp_id)の全リスト
		$sql .= " ) levelup_emp_id_table ";
		$sql .= " on a.apply_id = levelup_emp_id_table.apply_id ";

		//志願者氏名と所属(第三階層)を付加
		$sql .= " left join ";
		$sql .= " ( ";
		$sql .= "   select ";
		$sql .= "   a.emp_id, ";
		$sql .= "   a.emp_lt_nm, ";
		$sql .= "   a.emp_ft_nm, ";
		$sql .= "   b.dept_nm ";
		$sql .= "   from empmst a ";
		$sql .= "   inner join deptmst b ";
		$sql .= "   on a.emp_dept = b.dept_id ";
		$sql .= " ) levelup_emp_info_table ";
		$sql .= " on levelup_emp_id_table.levelup_emp_id = levelup_emp_info_table.emp_id ";
		// 2012/09/25 Yamagawa add(e)

		$cond = "";
		$cond .= "where not a.draft_flg and a.delete_flg = '$delete_flg' ";

		if($selected_cate != "")
		{
			$cond .= "and b.wkfw_type = $selected_cate ";
		}

		if($selected_folder != "")
		{
			$cond .= "and b.wkfw_folder_id = $selected_folder ";
		}

		if($selected_wkfw_id != "")
		{
			$cond .= "and b.wkfw_id = '$selected_wkfw_id' ";
		}


		// カテゴリ
		if($category != "" && $category != "-")
		{
			$cond .= "and b.wkfw_type = $category ";
		}

		// 申請書名
		if($workflow != "" &&  $workflow != "-")
		{
			$cond .= "and b.wkfw_id = '$workflow' ";
		}

		// 表題
		if($apply_title != "")
		{
			$apply_title = pg_escape_string($apply_title);
			$cond .= "and a.apply_title like '%$apply_title%' ";
		}

		// 申請者名
		if($apply_emp_nm != "")
		{
			$apply_emp_nm = mb_ereg_replace("[ 　]", "", $apply_emp_nm);
			$apply_emp_nm = pg_escape_string($apply_emp_nm);
			$cond .= "and ";
			$cond .= "(c.emp_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_lt_nm like '%$apply_emp_nm%' ";
			$cond .= "or c.emp_kn_ft_nm like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_lt_nm || c.emp_ft_nm) like '%$apply_emp_nm%' ";
			$cond .= "or (c.emp_kn_lt_nm || c.emp_kn_ft_nm) like '%$apply_emp_nm%') ";
		}

		// 申請状況
		if($apply_stat != "" && $apply_stat != "-")
		{
			$cond .= "and a.apply_stat = '$apply_stat' ";
		}

		//開催日
		//join時に絞込み


		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$cond .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$cond .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}


		// 部署
		if($class != "")
		{
			$cond .= "and a.emp_class = $class ";
		}

		if($attribute != "")
		{
			$cond .= "and a.emp_attribute = $attribute ";
		}

		if($dept != "")
		{
			$cond .= "and a.emp_dept = $dept ";
		}

		if($room != "")
		{
			$cond .= "and a.emp_room = $room ";
		}

		// 文字列検索
		//join時に絞込み

		// 研修区分
		$short_wkfw_name_list = $this->get_short_wkfw_name_list_from_training_div($training_div);
		if($short_wkfw_name_list != "")
		{
            $cond .= "and b.short_wkfw_name in(".$short_wkfw_name_list.") ";
		}




		return $sql.$cond;
	}



//-------------------------------------------------------------------------------------------------------------------------
// 承認一覧
//-------------------------------------------------------------------------------------------------------------------------
	// 通常申請のＳＱＬ取得
	function get_normal_approve_sql($arr_cond)
    {
		$emp_id          = $arr_cond["emp_id"];
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$training_theme  = $arr_cond["training_theme"];
		$class           = $arr_cond["class"];
		$attribute       = $arr_cond["attribute"];
		$dept            = $arr_cond["dept"];
		$room            = $arr_cond["room"];
		$emp_nm          = $arr_cond["emp_nm"];
		$apply_yyyy_from = $arr_cond["apply_yyyy_from"];
		$apply_mm_from   = $arr_cond["apply_mm_from"];
		$apply_dd_from   = $arr_cond["apply_dd_from"];
		$apply_yyyy_to   = $arr_cond["apply_yyyy_to"];
		$apply_mm_to     = $arr_cond["apply_mm_to"];
		$apply_dd_to     = $arr_cond["apply_dd_to"];
		$class_yyyy_from = $arr_cond["class_yyyy_from"];
		$class_mm_from   = $arr_cond["class_mm_from"];
		$class_dd_from   = $arr_cond["class_dd_from"];
		$class_yyyy_to   = $arr_cond["class_yyyy_to"];
		$class_mm_to     = $arr_cond["class_mm_to"];
		$class_dd_to     = $arr_cond["class_dd_to"];
		$apv_stat        = $arr_cond["apv_stat"];
		$training_div    = $arr_cond["training_div"];

		$sql  = "select ";
		$sql .= "varchar(1) '1' as apply_type, ";
		$sql .= "a.apply_id, ";
		$sql .= "a.apply_stat, ";
		$sql .= "a.apply_date, ";
		$sql .= "a.apply_title, ";
		$sql .= "d.wkfw_title, ";
		$sql .= "d.wkfw_nm, ";
		$sql .= "e.emp_lt_nm, ";
		$sql .= "e.emp_ft_nm, ";
		$sql .= "b.apv_stat, ";
		$sql .= "null as pjt_schd_id, ";
		$sql .= "b.apv_order, ";
		$sql .= "b.apv_sub_order, ";
		$sql .= "c.send_apved_order, ";
		$sql .= "a.apply_no, ";
		$sql .= "d.short_wkfw_name, ";
		$sql .= "d.wkfw_folder_id, ";
		$sql .= "a.apply_content, ";
		$sql .= "training_name_table.training_name, ";
		$sql .= "dept_nm_table.dept_nm, ";
		$sql .= "levelup_emp_info_table.emp_lt_nm as levelup_emp_lt_nm, ";
		$sql .= "levelup_emp_info_table.emp_ft_nm as levelup_emp_ft_nm, ";
		$sql .= "levelup_emp_info_table.dept_nm as levelup_emp_dept_nm ";

		//cl_applyのうち、申請者が利用停止になっているものを除外
		//$sql .= "from cl_apply a ";
		$sql .= "from (SELECT * FROM cl_apply WHERE EXISTS(SELECT authmst.emp_id FROM authmst WHERE NOT emp_del_flg AND authmst.emp_id = cl_apply.emp_id)) a ";

		// 検索条件として研修名が指定されている場合
		if($training_theme != "")
		{
			//研修名の条件を満たすapply_idで絞込み
			$sql .= " inner join";
			$sql .= " (";
			$sql .= $this->get_training_name_match_apply_id_list_sql($training_theme);
			$sql .= " ) training_name_search";
			$sql .= " on a.apply_id = training_name_search.apply_id ";
		}

		//検索条件として開催日が指定されている場合
		if( ($class_yyyy_from != "" && $class_yyyy_from != "-") || ($class_yyyy_to != "" && $class_yyyy_to != "-") )
		{
			//開催日の条件を満たすapply_idで絞込み
			$sql .= " inner join";
			$sql .= " (";
			$sql .= $this->get_plan_date_match_apply_id_list_sql($class_yyyy_from,$class_mm_from,$class_dd_from,$class_yyyy_to,$class_mm_to,$class_dd_to);
			$sql .= " ) plan_search";
			$sql .= " on a.apply_id = plan_search.apply_id ";
		}

		$sql .= "inner join cl_applyapv b on a.apply_id = b.apply_id ";

		$sql .= "left join cl_applyasyncrecv c on b.apply_id = c.apply_id ";
		$sql .= "and b.apv_order = c.recv_apv_order ";
		$sql .= "and ((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";

		$sql .= "inner join ";
		$sql .= "(select a.wkfw_id, a.wkfw_type, a.wkfw_title, b.wkfw_nm, a.short_wkfw_name, a.wkfw_folder_id from cl_wkfwmst a ";
		$sql .= "inner join cl_wkfwcatemst b on a.wkfw_type = b.wkfw_type) d on a.wkfw_id = d.wkfw_id ";

		$sql .= "inner join empmst e on a.emp_id = e.emp_id ";

//		$sql .= "left join cl_training_history t on t.apply_id = a.apply_id ";


		//研修名を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= $this->get_apply_id_training_name_list_sql();//申請ID(apply_id)と研修名(training_name)の全リスト
        $sql .= " ) training_name_table ";
        $sql .= " on A.apply_id = training_name_table.apply_id ";

		//志願者IDを付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= $this->get_apply_id_levelup_emp_id_list_sql();//申請ID(apply_id)と志願者ID(levelup_emp_id)の全リスト
        $sql .= " ) levelup_emp_id_table ";
        $sql .= " on A.apply_id = levelup_emp_id_table.apply_id ";

		//志願者氏名と所属(第三階層)を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= "   select ";
        $sql .= "   a.emp_id, ";
        $sql .= "   a.emp_lt_nm, ";
        $sql .= "   a.emp_ft_nm, ";
        $sql .= "   b.dept_nm ";
        $sql .= "   from empmst a ";
        $sql .= "   inner join deptmst b ";
        $sql .= "   on a.emp_dept = b.dept_id ";
        $sql .= " ) levelup_emp_info_table ";
        $sql .= " on levelup_emp_id_table.levelup_emp_id = levelup_emp_info_table.emp_id ";

		//申請者の所属(第三階層)を付加
        $sql .= " left join ";
        $sql .= " ( ";
        $sql .= "   select ";
        $sql .= "   a.emp_id, ";
        $sql .= "   b.dept_nm ";
        $sql .= "   from empmst a ";
        $sql .= "   inner join deptmst b ";
        $sql .= "   on a.emp_dept = b.dept_id ";
        $sql .= " ) dept_nm_table ";
        $sql .= " on A.emp_id = dept_nm_table.emp_id ";



		$sql .= "where a.re_apply_id is null ";
		$sql .= "and not a.delete_flg and not a.draft_flg ";
		$sql .= "and b.emp_id = '$emp_id' ";
		$sql .= "and (c.apv_show_flg or c.apv_show_flg is null) ";

		// 承認状況
		if($apv_stat == "0")
		{
			$sql .= "and b.apv_stat = '0' ";

			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from cl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0')) ";

			$sql .= "or ";
			// 2012/05/29 Yamagawa upd(s)
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			//$sql .= "(a.wkfw_appr = '2' and ";
			// 2012/05/29 Yamagawa upd(e)
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from cl_applyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from cl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from cl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= ") ";

		}
		else if($apv_stat == "" || $apv_stat == "-")
		{
			$sql .= "and (";
			$sql .= "(a.wkfw_appr = '1' and ";
			$sql .= "exists (select apply_id from cl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id')) ";

			$sql .= "or ";
			// 2012/05/29 Yamagawa upd(s)
			$sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
			//$sql .= "(a.wkfw_appr = '2' and ";
			// 2012/05/29 Yamagawa upd(e)
			$sql .= "exists (select apv2.* from ";
			$sql .= "(select apply_id, apv_order from cl_applyapv where emp_id = '$emp_id' and apv_stat = '0' ";
			$sql .= "and apv_order = (select min(apv_order) from cl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
			$sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order)) ";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
			$sql .= "exists (select apply_id from cl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0'))";

			$sql .= "or ";
			$sql .= "(a.wkfw_appr = '2' and ";
			$sql .= "exists (select apv4.apply_id, apv4.apv_order from cl_applyapv apv4 where a.apply_id = apv4.apply_id and b.apv_order = apv4.apv_order and apv4.emp_id = '$emp_id' and apv4.apv_stat <> '0'))";

			$sql .= ") ";
		}
		else
		{
			if($apv_stat != "" && $apv_stat != "-")
			{
				$sql .= "and b.apv_stat = '$apv_stat' ";
			}
		}

		// カテゴリ
		if($wkfw_type != "" && $wkfw_type != "-")
		{
			$sql .= "and d.wkfw_type = $wkfw_type ";
		}
		// 申請書名
		if($wkfw_id != "" &&  $wkfw_id != "-")
		{
			$sql .= "and d.wkfw_id = '$wkfw_id' ";
		}
		// 表題
		if($apply_title != "")
		{
			$sql .= "and a.apply_title like '%$apply_title%' ";
		}

		// 研修名
		//join時に絞込み

		// 部署
		if ($class != "") {
			$sql .= "and a.emp_class = $class ";
		}
		if ($attribute != "") {
			$sql .= "and a.emp_attribute = $attribute ";
		}
		if ($dept != "") {
			$sql .= "and a.emp_dept = $dept ";
		}
		if ($room != "") {
			$sql .= "and a.emp_room = $room ";
		}

		// 申請者
		if($emp_nm != "") {

			$sql .= "and ";
			$sql .= "(e.emp_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_ft_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_lt_nm like '%$emp_nm%' ";
			$sql .= "or e.emp_kn_ft_nm like '%$emp_nm%' ";
			$sql .= "or (e.emp_lt_nm || e.emp_ft_nm) like '%$emp_nm%' ";
			$sql .= "or (e.emp_kn_lt_nm || e.emp_kn_ft_nm) like '%$emp_nm%') ";

		}

		// 申請日
		$srch_from_ymd = "$apply_yyyy_from$apply_mm_from$apply_dd_from";
		$srch_to_ymd = "$apply_yyyy_to$apply_mm_to$apply_dd_to";
		if (preg_match("/^(\d*)/", $srch_from_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) >= '$date_str' ";
		}
		if (preg_match("/^(\d*)/", $srch_to_ymd, $matches) == 1) {
			$date_str = $matches[1];
			$date_len = strlen($date_str);
			$sql .= "and substring(a.apply_date from 1 for $date_len) <= '$date_str' ";
		}

		// 開催日
		// join時に絞込み

		// 研修区分
		$short_wkfw_name_list = $this->get_short_wkfw_name_list_from_training_div($training_div);
		if($short_wkfw_name_list != "")
		{
            $sql .= "and d.short_wkfw_name in(".$short_wkfw_name_list.") ";
		}

		return $sql;
    }


	// 承認一覧取得ＳＱＬ
    function get_approvelist_sql($arr_cond)
    {
		$wkfw_type       = $arr_cond["wkfw_type"];
		$wkfw_id         = $arr_cond["wkfw_id"];
		$apply_title     = $arr_cond["apply_title"];
		$apv_stat        = $arr_cond["apv_stat"];

		$sql = "";

		//-------------------------------------------
		// ■通常の申請
		// ・「カテゴリ」がCoMedix(0)以外
		//-------------------------------------------
		if($wkfw_type != "0")
		{
			$sql .= $this->get_normal_approve_sql($arr_cond);
		}
		return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 申請一覧、受付一覧 共通
//-------------------------------------------------------------------------------------------------------------------------

	//研修区分から管理コードを取得します。
	function get_short_wkfw_name_list_from_training_div($training_div)
	{
		switch ($training_div)
		{
			//院内研修
			case 1:
				$short_wkfw_name_list = "'c100','c110','c120','c130'";
				break;

			//院外研修
			case 2:
				$short_wkfw_name_list = "'c140','c150'";
				break;

			//条件なし
			default:
				$short_wkfw_name_list = "";
		}

		return $short_wkfw_name_list;
	}


	//申請一覧、受付一覧用SQL
	//全申請書の申請ID(apply_id)と研修名(training_name)の一覧を返すSQLを取得します。
	//・院内研修申込、院内研修申込変更、院内研修課題提出、院内研修報告　･･･　院内研修マスタの研修名を抽出します。
	//・院外研修届け　･･･　申請書に記入した研修名を抽出します。
    function get_apply_id_training_name_list_sql()
    {
		$sql = "";

		//院内研修:申請書の研修IDに対する研修マスタの研修名が検索条件を満たす場合に抽出対象
		$sql .= " select ";
		$sql .= " a.apply_id,";
		$sql .= " b.training_name";
		$sql .= " from";
		$sql .= " (";

		//院内研修申込、院内研修申込変更
		$sql .= "     select apply_id,inside_training_id as training_id from cl_apl_inside_training_apply";

		//院内研修課題提出
		$sql .= "     union all";
		$sql .= "     select apply_id,training_id from cl_apl_inside_training_theme_present";

		//院内研修報告
		$sql .= "     union all";
		$sql .= "     select apply_id,training_id from cl_apl_inside_training_report";

		$sql .= " ) a";
		$sql .= " inner join";
		$sql .= " (";
		$sql .= "     select training_id,training_name from cl_mst_inside_training";
		$sql .= " ) b";
		$sql .= " on a.training_id = b.training_id";

		//院外研修:申請書に記入した研修名が検索条件を満たす場合に抽出対象
		$sql .= " union all";
		$sql .= " select apply_id,seminar_name as training_name from cl_apl_outside_seminar";

		return $sql;
    }

	//申請一覧、受付一覧用SQL
	//全申請書の申請ID(apply_id)と志願者ID(levelup_emp_id)の一覧を返すSQLを取得します。
	//レベルアップ系の申請のみ志願者を持ちます。
    function get_apply_id_levelup_emp_id_list_sql()
    {
		$sql = "";

		//レベルアップ申請
		$sql .= " select apply_id,levelup_emp_id from cl_apl_levelup_apply ";

		//レベルアップ評価表
		$sql .= " union all ";
		$sql .= " select apply_id,levelup_emp_id from cl_apl_levelup_evaluation ";

		//レベルアップ同僚評価表
		$sql .= " union all ";
		$sql .= " select apply_id,levelup_emp_id from cl_apl_levelup_evaluation_colleague ";

		//レベルアップ認定
		$sql .= " union all ";
		$sql .= " select apply_id,levelup_emp_id from cl_apl_levelup_recognize ";

		return $sql;
    }












	//申請一覧、受付一覧用SQL
	//開催日の検索条件に合致する申請書のapply_idを返すSQLを取得します。
	//・院内研修申込、院内研修申込変更、院内研修課題提出　･･･　申し込んだ全開催日が対象となります。
	//・院内研修報告　･･･　報告を行った開催日が対象となります。
	//・院外研修届け　･･･　開催期間が対象となります。
    function get_plan_date_match_apply_id_list_sql($class_yyyy_from,$class_mm_from,$class_dd_from,$class_yyyy_to,$class_mm_to,$class_dd_to)
    {
		//==================================================
		//■院内研修:apply_id → plan_from_date,plan_to_date 変換テーブル(1対多)
		//==================================================

		$inside_apply_to_plan_sql = "";
		$inside_apply_to_plan_sql .= " select";
		$inside_apply_to_plan_sql .= " a.apply_id,";
		$inside_apply_to_plan_sql .= " to_char(b.plan_date,'yyyymmdd') as plan_from_date,";
		$inside_apply_to_plan_sql .= " to_char(b.plan_date,'yyyymmdd') as plan_to_date";
		$inside_apply_to_plan_sql .= " ";

		//apply_id → plan_id 変換テーブル
		$inside_apply_to_plan_sql .= " from";
		$inside_apply_to_plan_sql .= " (";

	    // 院内研修申込、院内研修申込変更(plan_idはCSV形式)
		$inside_apply_to_plan_sql .= "     select apply_id,plan_id from cl_apl_inside_training_apply";

	    // 院内研修課題提出(plan_idはCSV形式)
		$inside_apply_to_plan_sql .= "     union all";
		$inside_apply_to_plan_sql .= "     select a.apply_id, b.plan_id";
		$inside_apply_to_plan_sql .= "     from cl_apl_inside_training_theme_present a";
		$inside_apply_to_plan_sql .= "     inner join cl_applied_inside_training b";
		$inside_apply_to_plan_sql .= "     on a.training_apply_id = b.training_apply_id";

	    // 院内研修報告(plan_idは単一)
		$inside_apply_to_plan_sql .= "     union all";
		$inside_apply_to_plan_sql .= "     select apply_id,plan_id  from cl_apl_inside_training_report";

		$inside_apply_to_plan_sql .= " ) a";

		//plan_id → plan_date 変換テーブル
		$inside_apply_to_plan_sql .= " inner join";
		$inside_apply_to_plan_sql .= " (";
		$inside_apply_to_plan_sql .= "    select plan_id, plan_date from cl_mst_inside_training_schedule";
		$inside_apply_to_plan_sql .= " ) b";
		$inside_apply_to_plan_sql .= " on a.plan_id like '%' || b.plan_id || '%'";


		//==================================================
		//■院外研修:apply_id → plan_from_date,plan_to_date 変換テーブル(1対1)
		//==================================================

		$outside_apply_to_plan_sql = "";
		$outside_apply_to_plan_sql .= " select";
		$outside_apply_to_plan_sql .= " apply_id,";
		$outside_apply_to_plan_sql .= " to_char(from_date,'yyyymmdd') as plan_from_date,";
		$outside_apply_to_plan_sql .= " to_char(to_date,'yyyymmdd') as plan_to_date";
		$outside_apply_to_plan_sql .= " from cl_apl_outside_seminar";


		//==================================================
		//■開催日条件に合致するapply_id
		//==================================================

		$sql = "";
		$sql .= " select";
		$sql .= " distinct apply_id";
		$sql .= " from";
		$sql .= " (";
		$sql .=       $inside_apply_to_plan_sql; //■院内研修:apply_id → plan_from_date,plan_to_date 変換テーブル(1対多)
		$sql .= "     union all";
		$sql .=       $outside_apply_to_plan_sql;//■院外研修:apply_id → plan_from_date,plan_to_date 変換テーブル(1対1)
		$sql .= " ) a";
		$sql .= " where";
		$sql .= " 0=0";

		if($class_yyyy_from == "-")
		{
			//絞り込まない
		}
		else if($class_mm_from == "-")
		{
			$sql .= "and substring(plan_to_date from 1 for 4) >= '$class_yyyy_from'";
		}
		else if($class_dd_from == "-")
		{
			$sql .= "and substring(plan_to_date from 1 for 6) >= '$class_yyyy_from$class_mm_from'";
		}
		else
		{
			$sql .= "and substring(plan_to_date from 1 for 8) >= '$class_yyyy_from$class_mm_from$class_dd_from'";
		}

		if($class_yyyy_to == "-")
		{
			//絞り込まない
		}
		else if($class_mm_to == "-")
		{
			$sql .= "and substring(plan_to_date from 1 for 4) <= '$class_yyyy_to'";
		}
		else if($class_dd_to == "-")
		{
			$sql .= "and substring(plan_to_date from 1 for 6) <= '$class_yyyy_to$class_mm_to'";
		}
		else
		{
			$sql .= "and substring(plan_to_date from 1 for 8) <= '$class_yyyy_to$class_mm_to$class_dd_to'";
		}

		return $sql;
    }

	//申請一覧、受付一覧用SQL
	//研修名の検索条件に合致する申請書のapply_idを返すSQLを取得します。
	//・院内研修申込、院内研修申込変更、院内研修課題提出、院内研修報告　･･･　院内研修マスタの研修名を判定します。
	//・院外研修届け　･･･　申請書に記入した研修名を判定します。
    function get_training_name_match_apply_id_list_sql($training_name)
    {
		$sql = "";

		//院内研修:申請書の研修IDに対する研修マスタの研修名が検索条件を満たす場合に抽出対象
		$sql .= " select ";
		$sql .= " a.apply_id";
		$sql .= " from";
		$sql .= " (";

		//院内研修申込、院内研修申込変更
		$sql .= "     select apply_id,inside_training_id as training_id from cl_apl_inside_training_apply";

		//院内研修課題提出
		$sql .= "     union all";
		$sql .= "     select apply_id,training_id from cl_apl_inside_training_theme_present";

		//院内研修報告
		$sql .= "     union all";
		$sql .= "     select apply_id,training_id from cl_apl_inside_training_report";

		$sql .= " ) a";
		$sql .= " inner join";
		$sql .= " (";
		$sql .= "     select training_id from cl_mst_inside_training where training_name like '%$training_name%'";
		$sql .= " ) b";
		$sql .= " on a.training_id = b.training_id";

		//院外研修:申請書に記入した研修名が検索条件を満たす場合に抽出対象
		$sql .= " union all";
		$sql .= " select apply_id from cl_apl_outside_seminar where seminar_name like '%$training_name%'";

		return $sql;
    }



	//申請一覧、受付一覧用SQL
	//文字列検索の検索条件に合致する申請書のapply_idを返すSQLを取得します。
	//文字列検索は以下を検索の対象とします。
	//・テンプレート内のフリー入力項目(テキストボックス・テキストエリア)
	//・院内研修系は院内研修名
    function get_text_match_apply_id_list_sql($apply_content)
    {
		//==================================================
		//サブクエリ：院内研修
		//==================================================
		$training_sql = "";

		$training_sql .= " select training_id from cl_mst_inside_training a";

		//院内研修名
		$training_sql .= " where ".$this->get_text_match_cond($apply_content,"training_name");

		//==================================================
		//院内研修申込／院内研修申込変更
		//==================================================
		$sql .= "";

		$sql .= " select apply_id from cl_apl_inside_training_apply a ";

		//変更理由
		$sql .= " where ".$this->get_text_match_cond($apply_content,"change_reason");
		//学びたいこと
		//※院内研修申込データのみ検索する。(「update_division=0:新規申込」で絞り込む)
		$sql .= " or ("  .$this->get_text_match_cond($apply_content,"learn_memo")." and update_division=0 )";
		//研修名
		$sql .= " or     exists(select * from (".$training_sql.") b where a.inside_training_id = b.training_id)";

		//==================================================
		//院内研修課題提出
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_inside_training_theme_present a ";

		//答案
		$sql .= " where ".$this->get_text_match_cond($apply_content,"present_answer");
		//師長コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"boss_comment");
		//主催者コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"sponsor_comment");
		//講師コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"teacher_comment");
		//研修名
		$sql .= " or     exists(select * from (".$training_sql.") b where a.training_id = b.training_id)";

		//==================================================
		//院内研修報告
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_inside_training_report a ";

		//研修から学んだこと
		$sql .= " where ".$this->get_text_match_cond($apply_content,"training_learned");
		//師長コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"boss_comment");
		//主催者コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"sponsor_comment");
		//講師コメント
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"teacher_comment");
		//研修名
		$sql .= " or     exists(select * from (".$training_sql.") b where a.training_id = b.training_id)";

		//==================================================
		//院内研修報告(アンケート)
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_inside_training_answer a ";

		//研修日時の適切性　理由
		$sql .= " where ".$this->get_text_match_cond($apply_content,"training_plan_degree_reason");
		//研修の方法　理由
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"training_method_reason");
		//研修の内容　理由
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"training_contents_reason");
		//研修内容の実践　理由
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"training_practice_reason");
		//気づいた点
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"point_notice");

		//==================================================
		//院外研修届け
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_outside_seminar a ";

		//院外研修名
		$sql .= " where ".$this->get_text_match_cond($apply_content,"seminar_name");
		//開催地
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"open_place");
		//講師名
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"teacher_name");

		//==================================================
		//レベルアップ申請
		//==================================================

		//フリー入力項目なし

		//==================================================
		//レベルアップ評価表・同僚評価表
		//==================================================

		//フリー入力項目なし

		//==================================================
		//レベルアップ認定
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_levelup_recognize a ";

		//認定理由
		$sql .= " where ".$this->get_text_match_cond($apply_content,"reason");

		//==================================================
		//院外研修届け
		//==================================================
		$sql .= " union all";

		$sql .= " select apply_id from cl_apl_outside_seminar a ";

		//院外研修名
		$sql .= " where ".$this->get_text_match_cond($apply_content,"seminar_name");
		//開催地
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"open_place");
		//講師名
		$sql .= " or "   .$this->get_text_match_cond($apply_content,"teacher_name");



		//==================================================
		//apply_idをdistinct
		//==================================================

		$sql = "select distinct apply_id from (".$sql.") a";

		return $sql;
    }

	//get_text_match_apply_id_list_sql()用関数
	function get_text_match_cond($apply_content,$feild_name)
	{
		//検索条件はスペース区切りのマルチワード部分一致検索。

		$search_keys = split(" ", pg_escape_string($apply_content));
//		$search_keys = split(" ", $apply_content);

		$cond = "";
		for ($i = 0, $j = count($search_keys); $i < $j; $i++)
		{
			if($cond != "")
			{
				$cond .= " or";
			}
			$cond .= $feild_name." like '%".$search_keys[$i]."%'";
		}

		return $cond;
	}



//-------------------------------------------------------------------------------------------------------------------------
// 共通
//-------------------------------------------------------------------------------------------------------------------------


	// 職員情報取得
	function get_empmst($session)
	{
		$sql  = "select * from empmst";
		$cond = "where emp_id in (select emp_id from session where session_id='$session')";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 職員詳細情報取得
	function get_empmst_detail($emp_id)
	{
		$sql  = "select empmst.*, stmst.st_nm, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm ";
		$sql .= "from empmst ";
		$sql .= "inner join authmst on empmst.emp_id = authmst.emp_id and not authmst.emp_del_flg ";
		$sql .= "left join stmst on empmst.emp_st = stmst.st_id ";
		$sql .= "left join classmst on empmst.emp_class = classmst.class_id ";
		$sql .= "left join atrbmst on empmst.emp_attribute = atrbmst.atrb_id ";
		$sql .= "left join deptmst on empmst.emp_dept = deptmst.dept_id ";
		$sql .= "left join classroom on empmst.emp_room = classroom.room_id ";
		$cond = "where empmst.emp_id = '$emp_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);

	}

	// 室名称取得
	function get_room_nm($room_id)
	{
		$sql  = "select * from classroom";
		$cond = "where room_id = $room_id";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$room_nm = pg_result($sel,0,"room_nm");
		return $room_nm;
	}

	// 部署情報取得
	function get_classname()
	{
		$sql  = "select * from classname";
		$cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_all($sel);
	}

	// 部門マスタ取得
	function get_class_mst() {
		$sql = "select class_id, class_nm from classmst";
		$cond = "where class_del_flg = 'f' order by class_id";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課マスタ取得
	function get_atrb_mst() {
		$sql = "select class_id, atrb_id, atrb_nm from atrbmst";
		$cond = "where atrb_del_flg = 'f' order by class_id, atrb_id";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科マスタ取得
	function get_dept_mst() {
		$sql = "select atrb_id, dept_id, dept_nm from deptmst";
		$cond = "where dept_del_flg = 'f' order by atrb_id, dept_id";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 室マスタ取得
	function get_room_mst() {
		$sql = "select dept_id, room_id, room_nm from classroom";
		$cond = "where room_del_flg = 'f' order by dept_id, room_id";
		$sel_room = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_room == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_room;
	}

	// 部門マスタ取得
	function get_class_mst_strict($emp_id) {
		$sql = "select c.class_id, c.class_nm from classmst c";
		$cond = "where c.class_del_flg = 'f' and (exists (select * from empmst e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_class = c.class_id and s.st_nm like '%師長%') or exists (select * from concurrent e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_class = c.class_id and s.st_nm like '%師長%')) order by c.class_id";
		$sel_class = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_class == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_class;
	}

	// 課マスタ取得
	function get_atrb_mst_strict($emp_id) {
		$sql = "select a.class_id, a.atrb_id, a.atrb_nm from atrbmst a";
		$cond = "where a.atrb_del_flg = 'f' and (exists (select * from empmst e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_attribute = a.atrb_id and s.st_nm like '%師長%') or exists (select * from concurrent e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_attribute = a.atrb_id and s.st_nm like '%師長%')) order by a.class_id, a.atrb_id";
		$sel_atrb = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_atrb == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_atrb;
	}

	// 科マスタ取得
	function get_dept_mst_strict($emp_id) {
		$sql = "select d.atrb_id, d.dept_id, d.dept_nm from deptmst d";
		$cond = "where d.dept_del_flg = 'f' and (exists (select * from empmst e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_dept = d.dept_id and s.st_nm like '%師長%') or exists (select * from concurrent e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_dept = d.dept_id and s.st_nm like '%師長%')) order by d.atrb_id, d.dept_id";
		$sel_dept = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_dept == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_dept;
	}

	// 室マスタ取得
	function get_room_mst_strict($emp_id) {
		$sql = "select r.dept_id, r.room_id, r.room_nm from classroom r";
		$cond = "where r.room_del_flg = 'f' and (exists (select * from empmst e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_room = r.room_id and s.st_nm like '%師長%') or exists (select * from concurrent e inner join stmst s on s.st_id = e.emp_st where e.emp_id = '$emp_id' and e.emp_room = r.room_id and s.st_nm like '%師長%')) order by r.dept_id, r.room_id";
		$sel_room = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel_room == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		return $sel_room;
	}

//-------------------------------------------------------------------------------------------------------------------------
// 認定スケジュール
//-------------------------------------------------------------------------------------------------------------------------
	// 認定スケジュール情報取得処理(認定スケジュール登録画面用)
	function get_cl_recognize_schedule($apply_id)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm from cl_recognize_schedule a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$cond = "where a.apply_id = '$apply_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
                            "apply_date"       => $row["apply_date"],
	                        "emp_id"           => $row["emp_id"],
	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],
	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"]
	                       );

		}
		return $arr;
	}

	/**
	 * 認定スケジュール情報取得処理(認定スケジュール登録画面用)
	 * @param $apply_id String 申請ID
	 * @return $arr array 該当スケジュール配列
	 * @author 2011.12.05 Add by matsuura
	 */
	function get_cl_recognize_schedule2($apply_id)
	{
		$sql  = "select a.*, b.emp_lt_nm, b.emp_ft_nm from cl_recognize_schedule a ";
		$sql .= "left join empmst b on a.emp_id = b.emp_id ";
		$cond = "where a.apply_id = '$apply_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
                            "apply_date"       => $row["apply_date"],
	                        "emp_id"           => $row["emp_id"],
	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],
                            "no12_date"        => $row["no12_date"],

                            // 2011.12.05 Add by matsuura
                            "no31_date"        => $row["no31_date"],
	                        "no31_apply_id"    => $row["no31_apply_id"],
                            "no32_date"        => $row["no32_date"],
	                        "no32_apply_id"    => $row["no32_apply_id"],
                            "no33_date"        => $row["no33_date"],
	                        "no33_apply_id"    => $row["no33_apply_id"],
	                        "nurse_type"         => $row["nurse_type"],

	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"]
	                       );

		}
		return $arr;
	}

	// 認定スケジュール情報取得(認定スケジュール画面用)
	function get_cl_recognize_schedule_list($arr_condition)
	{
		$emp_nm       = $arr_condition["emp_nm"];
		$level        = $arr_condition["level"];
		$class        = $arr_condition["class"];
		$attribute    = $arr_condition["attribute"];
		$dept         = $arr_condition["dept"];
		$room         = $arr_condition["room"];
		$page         = $arr_condition["page"];
		$max_page     = $arr_condition["max_page"];

		// 師長の場合、ログインユーザの職員IDが渡ってくる
		$login_emp_id = $arr_condition["login_emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
        $sql  .= "left join cl_apply c on a.apply_id = c.apply_id ";
		$cond  = "where a.level = $level and a.disp_flg and not c.delete_flg ";

		// 志願者名
		if($emp_nm != "")
		{
			$emp_nm = pg_escape_string($emp_nm);
			$cond .= "and ";
			$cond .= "(b.emp_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_ft_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_ft_nm like '%$emp_nm%' ";
			$cond .= "or (b.emp_lt_nm || b.emp_ft_nm) like '%$emp_nm%' ";
			$cond .= "or (b.emp_kn_lt_nm || b.emp_kn_ft_nm) like '%$emp_nm%') ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and c.emp_class = $class ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%')) ";
		}

		if ($attribute != "") {
			$cond .= "and c.emp_attribute = $attribute ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%')) ";
		}

		if ($dept != "") {
			$cond .= "and c.emp_dept = $dept ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%')) ";
		}

		if ($room != "") {
			$cond .= "and c.emp_room = $room ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or c.emp_room is null) ";
		}

        $cond .= "order by a.apply_id desc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	/**
	 * 認定スケジュール情報取得(認定スケジュール画面用)
	 * @param	$arr_condition : Object	検索条件オブジェクト
	 * @author 2011.12.01 Add by matsuura
	 */
	function get_cl_recognize_schedule_list2($arr_condition)
	{
		$emp_nm       = $arr_condition["emp_nm"];
		$level        = $arr_condition["level"];
		$class        = $arr_condition["class"];
		$attribute    = $arr_condition["attribute"];
		$dept         = $arr_condition["dept"];
		$room         = $arr_condition["room"];
		$page         = $arr_condition["page"];
		$max_page     = $arr_condition["max_page"];

		$nurse_type   = $arr_condition["nurse_type"];	//　看護種別（1：病棟　2：外来　3：外来看護師）

		// 師長の場合、ログインユーザの職員IDが渡ってくる
		$login_emp_id = $arr_condition["login_emp_id"];

		$page = ($page - 1) * $max_page;

		// 2011.12.05 Edit by matsuura
//		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm from cl_recognize_schedule a ";
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm, b.emp_class, b.emp_attribute, b.emp_dept, b.emp_room from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
        $sql  .= "left join cl_apply c on a.apply_id = c.apply_id ";
		$cond  = "where a.level = $level and a.disp_flg and not c.delete_flg ";

		// 志願者名
		if($emp_nm != "")
		{
			$emp_nm = pg_escape_string($emp_nm);
			$cond .= "and ";
			$cond .= "(b.emp_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_ft_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_ft_nm like '%$emp_nm%' ";
			$cond .= "or (b.emp_lt_nm || b.emp_ft_nm) like '%$emp_nm%' ";
			$cond .= "or (b.emp_kn_lt_nm || b.emp_kn_ft_nm) like '%$emp_nm%') ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and c.emp_class = $class ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%')) ";
		}

		if ($attribute != "") {
			$cond .= "and c.emp_attribute = $attribute ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%')) ";
		}

		if ($dept != "") {
			$cond .= "and c.emp_dept = $dept ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%')) ";
		}

		if ($room != "") {
			$cond .= "and c.emp_room = $room ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or c.emp_room is null) ";
		}

		// 看護種別
		if($nurse_type != ""){
			$cond .= "and a.nurse_type = $nurse_type ";
		}

        $cond .= "order by a.apply_id desc ";
		$cond .= "offset $page limit $max_page ";

//		echo($sql . $cond);

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_all($sel);
	}

	// 認定スケジュール情報一覧件数取得(認定スケジュール画面用)
	function get_cl_recognize_schedule_list_count($arr_condition)
	{
		$emp_nm       = $arr_condition["emp_nm"];
		$level        = $arr_condition["level"];
		$class        = $arr_condition["class"];
		$attribute    = $arr_condition["attribute"];
		$dept         = $arr_condition["dept"];
		$room         = $arr_condition["room"];

		// 師長の場合、ログインユーザの職員IDが渡ってくる
		$login_emp_id = $arr_condition["login_emp_id"];

		$sql   = "select count(a.apply_id) as cnt from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
        $sql  .= "left join cl_apply c on a.apply_id = c.apply_id ";
		$cond  = "where a.level = $level and a.disp_flg and not c.delete_flg ";

		// 志願者名
		if($emp_nm != "")
		{
			$emp_nm = pg_escape_string($emp_nm);
			$cond .= "and ";
			$cond .= "(b.emp_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_ft_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_ft_nm like '%$emp_nm%' ";
			$cond .= "or (b.emp_lt_nm || b.emp_ft_nm) like '%$emp_nm%' ";
			$cond .= "or (b.emp_kn_lt_nm || b.emp_kn_ft_nm) like '%$emp_nm%') ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and c.emp_class = $class ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%')) ";
		}

		if ($attribute != "") {
			$cond .= "and c.emp_attribute = $attribute ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%')) ";
		}

		if ($dept != "") {
			$cond .= "and c.emp_dept = $dept ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%')) ";
		}

		if ($room != "") {
			$cond .= "and c.emp_room = $room ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = c.emp_room and s.st_nm like '%師長%') or c.emp_room is null) ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
	}


	// 認定スケジュール登録処理
	function regist_cl_recognize_schedule($arr_data)
	{
		$sql  = "insert into cl_recognize_schedule";

		$sql .= "(";
		$sql .= "apply_id, ";
		$sql .= "apply_date, ";
		$sql .= "emp_id, ";
		$sql .= "level, ";
		$sql .= "no1_date, ";
		$sql .= "no1_apply_id, ";
		$sql .= "no2_date, ";
		$sql .= "no2_apply_id, ";
		$sql .= "no3_date, ";
		$sql .= "no3_apply_id, ";
		$sql .= "no4_date, ";
		$sql .= "no4_apply_id, ";
		$sql .= "no5_date, ";
		$sql .= "no5_apply_id, ";
		$sql .= "no6_date, ";
		$sql .= "no6_apply_id, ";
		$sql .= "no7_date, ";
		$sql .= "no7_apply_id, ";
		$sql .= "no8_date, ";
		$sql .= "no8_apply_id, ";
		$sql .= "no9_date, ";
		$sql .= "no9_apply_id, ";
		$sql .= "no10_date, ";
		$sql .= "no10_apply_id, ";
		$sql .= "no11_date, ";
		$sql .= "no11_apply_id, ";
		$sql .= "no12_date, ";
		$sql .= "no12_apply_id, ";
		$sql .= "disp_flg, ";
		$sql .= "levelup_apply_id, ";
		// 2011/12/2 Yamagawa add(s)
		$sql .= "nurse_type, ";
		$sql .= "no31_date, ";
		$sql .= "no31_apply_id, ";
		$sql .= "no32_date, ";
		$sql .= "no32_apply_id, ";
		$sql .= "no33_date, ";
		$sql .= "no33_apply_id";
		// 2011/12/2 Yamagawa add(e)
		$sql .= ") values(";

		$content = array(
						$arr_data["apply_id"],
						$arr_data["apply_date"],
						$arr_data["emp_id"],
						$arr_data["level"],
						$arr_data["no1_date"],
						$arr_data["no1_apply_id"],
						$arr_data["no2_date"],
						$arr_data["no2_apply_id"],
						$arr_data["no3_date"],
						$arr_data["no3_apply_id"],
						$arr_data["no4_date"],
						$arr_data["no4_apply_id"],
						$arr_data["no5_date"],
						$arr_data["no5_apply_id"],
						$arr_data["no6_date"],
						$arr_data["no6_apply_id"],
						$arr_data["no7_date"],
						$arr_data["no7_apply_id"],
						$arr_data["no8_date"],
						$arr_data["no8_apply_id"],
						$arr_data["no9_date"],
						$arr_data["no9_apply_id"],
						$arr_data["no10_date"],
						$arr_data["no10_apply_id"],
						$arr_data["no11_date"],
						$arr_data["no11_apply_id"],
						$arr_data["no12_date"],
						$arr_data["no12_apply_id"],
						$arr_data["disp_flg"],
						$arr_data["levelup_apply_id"],
						// 2011/12/2 Yamagawa add(s)
						$arr_data["nurse_type"],
						$arr_data["no31_date"],
						$arr_data["no31_apply_id"],
						$arr_data["no32_date"],
						$arr_data["no32_apply_id"],
						$arr_data["no33_date"],
						$arr_data["no33_apply_id"]
						// 2011/12/2 Yamagawa add(e)
						);

		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	/**
	 * 認定スケジュール登録処理
	 * @param array $arr_data
	 * @author 2011.12.07 Add by matsuura
	 */
	function regist_cl_recognize_schedule2($arr_data)
	{
		$sql  = "insert into cl_recognize_schedule";
		$sql .= "(";
		$sql .= "apply_id, ";
		$sql .= "apply_date, ";
		$sql .= "emp_id, ";
		$sql .= "level, ";
		$sql .= "no1_date, ";
		$sql .= "no1_apply_id, ";
		$sql .= "no2_date, ";
		$sql .= "no2_apply_id, ";
		$sql .= "no3_date, ";
		$sql .= "no3_apply_id, ";
		$sql .= "no4_date, ";
		$sql .= "no4_apply_id, ";
		$sql .= "no5_date, ";
		$sql .= "no5_apply_id, ";
		$sql .= "no6_date, ";
		$sql .= "no6_apply_id, ";
		$sql .= "no7_date, ";
		$sql .= "no7_apply_id, ";
		$sql .= "no8_date, ";
		$sql .= "no8_apply_id, ";
		$sql .= "no9_date, ";
		$sql .= "no9_apply_id, ";
		$sql .= "no10_date, ";
		$sql .= "no10_apply_id, ";
		$sql .= "no11_date, ";
		$sql .= "no11_apply_id, ";
		$sql .= "no12_date, ";
		$sql .= "no12_apply_id, ";
		$sql .= "no31_date, ";
		$sql .= "no31_apply_id, ";
		$sql .= "no32_date, ";
		$sql .= "no32_apply_id, ";
		$sql .= "no33_date, ";
		$sql .= "no33_apply_id, ";
		$sql .= "nurse_type, ";
		$sql .= "disp_flg, ";
		$sql .= "levelup_apply_id";
		$sql .= ") values(";

		$content = array(
						$arr_data["apply_id"],
						$arr_data["apply_date"],
						$arr_data["emp_id"],
						$arr_data["level"],
						$arr_data["no1_date"],
						$arr_data["no1_apply_id"],
						$arr_data["no2_date"],
						$arr_data["no2_apply_id"],
						$arr_data["no3_date"],
						$arr_data["no3_apply_id"],
						$arr_data["no4_date"],
						$arr_data["no4_apply_id"],
						$arr_data["no5_date"],
						$arr_data["no5_apply_id"],
						$arr_data["no6_date"],
						$arr_data["no6_apply_id"],
						$arr_data["no7_date"],
						$arr_data["no7_apply_id"],
						$arr_data["no8_date"],
						$arr_data["no8_apply_id"],
						$arr_data["no9_date"],
						$arr_data["no9_apply_id"],
						$arr_data["no10_date"],
						$arr_data["no10_apply_id"],
						$arr_data["no11_date"],
						$arr_data["no11_apply_id"],
						$arr_data["no12_date"],
						$arr_data["no12_apply_id"],
						$arr_data["no31_date"],
						$arr_data["no31_apply_id"],
						$arr_data["no32_date"],
						$arr_data["no32_apply_id"],
						$arr_data["no33_date"],
						$arr_data["no33_apply_id"],
						$arr_data["nurse_type"],
						$arr_data["disp_flg"],
						$arr_data["levelup_apply_id"]
						);
/*
		echo("***** regist_cl_recognize_schedule2 *****");
		echo(" sql = ");
		echo($sql);
		echo(" content = ");
		echo(print_r($content));
*/
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 認定スケジュール・日付更新処理(レベル２用)
	function update_cl_recognize_schedule_for_level2($arr_data)
	{
		$apply_id = $arr_data["apply_id"];

		$sql = "update cl_recognize_schedule set";
		$set = array(
					"no1_date",
					"no3_date",
					"no5_date",
					"no6_date",
					"no8_date",
					"no9_date",
					"no10_date",
                     );
		$setvalue = array(
	 						$arr_data["no1_date"],
							$arr_data["no3_date"],
							$arr_data["no5_date"],
							$arr_data["no6_date"],
							$arr_data["no8_date"],
							$arr_data["no9_date"],
							$arr_data["no10_date"],
						  );

		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	/**
	 * 認定スケジュール・日付更新処理(レベル２用)
	 * @param array $arr_data
	 * @author 2011.12.07 Add by matsuura
	 */
	function update_cl_recognize_schedule2_for_level2($arr_data)
	{
		$apply_id = $arr_data["apply_id"];

		$sql = "update cl_recognize_schedule set";
		$set = array(
					"no1_date",
					"no3_date",
					"no5_date",
					"no6_date",
					"no8_date",
					"no9_date",
					"no10_date",
					"no12_date",
					"no31_date",
					"no32_date",
					"no33_date",
					"nurse_type"
                     );

		$setvalue = array(
	 						$arr_data["no1_date"],
							$arr_data["no3_date"],
							$arr_data["no5_date"],
							$arr_data["no6_date"],
							$arr_data["no8_date"],
							$arr_data["no9_date"],
							$arr_data["no10_date"],
							$arr_data["no12_date"],
							$arr_data["no31_date"],
							$arr_data["no32_date"],
							$arr_data["no33_date"],
							$arr_data["nurse_type"]
						  );
		$cond = "where apply_id = '$apply_id'";
/*
		echo("***** update_cl_recognize_schedule2_for_level2 *****");
		echo(" sql = ");
		echo($sql);
		echo(" set = ");
		echo(print_r($set));
		echo(" setvalue = ");
		echo(print_r($setvalue));
*/
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 認定スケジュール・日付更新処理(レベル３用)
	function update_cl_recognize_schedule_for_level3($arr_data)
	{
		$apply_id = $arr_data["apply_id"];

		$sql = "update cl_recognize_schedule set";
		$set = array(
					"no2_date",
					"no4_date",
					"no5_date",
					"no7_date",
					"no8_date",
					"no9_date",
					"no11_date"
                     );
		$setvalue = array(
	 						$arr_data["no2_date"],
							$arr_data["no4_date"],
							$arr_data["no5_date"],
							$arr_data["no7_date"],
							$arr_data["no8_date"],
							$arr_data["no9_date"],
							$arr_data["no11_date"]
						  );

		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	/**
	 * 認定スケジュール・日付更新処理(レベル３用)
	 * @param array $arr_data
	 * @author 2011.12.07 Add by matsuura
	 */
	function update_cl_recognize_schedule2_for_level3($arr_data)
	{
		$apply_id = $arr_data["apply_id"];
		$sql = "update cl_recognize_schedule set";
		$set = array(
					"no2_date",
					"no4_date",
					"no5_date",
					"no7_date",
					"no8_date",
					"no9_date",
					"no11_date",
					"no12_date",
					"no31_date",
					"no32_date",
					"no33_date",
					"nurse_type"
                     );

		$setvalue = array(
	 						$arr_data["no2_date"],
							$arr_data["no4_date"],
							$arr_data["no5_date"],
							$arr_data["no7_date"],
							$arr_data["no8_date"],
							$arr_data["no9_date"],
							$arr_data["no11_date"],
							$arr_data["no12_date"],
							$arr_data["no31_date"],
							$arr_data["no32_date"],
							$arr_data["no33_date"],
							$arr_data["nurse_type"]
						  );

		$cond = "where apply_id = '$apply_id'";
/*
		echo("***** update_cl_recognize_schedule2_for_level3 *****");
		echo(" sql = ");
		echo($sql);
		echo(" set = ");
		echo(print_r($set));
		echo(" setvalue = ");
		echo(print_r($setvalue));
*/
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 認定スケジュール・申請ＩＤ更新処理(レベル２・３用)
    function update_cl_recognize_schedule_for_apply_id($arr_data)
    {
		$apply_id = $arr_data["apply_id"];
		$no_apply_id_key = $arr_data["no_apply_id_key"];
		$no_apply_id_val = $arr_data["no_apply_id_val"];

		$sql = "update cl_recognize_schedule set";
		$set = array($no_apply_id_key);
		$setvalue = array($no_apply_id_val);

		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }


	// 認定スケジュール更新処理(表示フラグ更新)
	function update_disp_flg_cl_recognize_schedule($apply_id)
	{
		$sql = "update cl_recognize_schedule set";

		$set = array("disp_flg");
		$setvalue = array("t");
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 認定スケジュール更新処理(レベルアップ申請ＩＤ更新)
    // レベルアップ志願書ID：$appliction_book_apply_id
    // レベルアップ申請ID：$apply_id
	function update_levelup_apply_id($appliction_book_apply_id, $apply_id)
	{
		$sql = "update cl_recognize_schedule set";
		$set = array("levelup_apply_id");
		$setvalue = array($apply_id);
		$cond = "where apply_id = $appliction_book_apply_id";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


//-------------------------------------------------------------------------------------------------------------------------
// 病棟評価表
//-------------------------------------------------------------------------------------------------------------------------
    function get_ward_exist_count($ward_table_name, $emp_id, $eval_year, $eval_month)
    {
	    $sql   = "select count(distinct a.apply_id) as cnt ";
	    $sql  .= "from $ward_table_name a ";
	    $cond  = "where a.emp_id = '$emp_id' and a.eval_year = $eval_year and a.eval_month = $eval_month ";
	    $cond .= "and exists(select * from cl_apply b where b.apply_id = a.apply_id and not b.delete_flg) ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
        return $cnt;
    }

    function get_ward_grade_data($emp_id, $short_wkfw_name, $start_date, $end_date)
    {
	    $sql   = "select a.eval_year, a.eval_month, a.eval_year || to_char(a.eval_month, 'FM00') AS eval_date, a.var_name, a.value ";
	    $sql  .= "from cl_wkfw_$short_wkfw_name a ";
        $cond  = "where a.emp_id = '$emp_id' ";
        $cond .= "and a.eval_year || to_char(a.eval_month, 'FM00') between $start_date and $end_date ";
        $cond .= "and exists(select * from cl_apply b where b.apply_id = a.apply_id and not b.delete_flg) ";
        $cond .= "order by a.eval_year, a.eval_month, a.cl_val_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	    return pg_fetch_all($sel);
    }

	//指定職員の病棟評価表のデータを取得する。
	//申請時の重複確認用となっている。下書き時を除くようにした(20090413)
    function get_ward_apply($short_wkfw_name, $emp_id)
    {
	    $sql   = "select * from cl_apply a ";
	    $cond  = "where exists(select * from cl_wkfwmst b where short_wkfw_name = '$short_wkfw_name' and a.wkfw_id = b.wkfw_id) ";
	    $cond .= "and a.emp_id = '$emp_id' and a.delete_flg = 'f' and a.draft_flg = 'f' and (apply_stat = 1 or apply_stat = 0) ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array("apply_content" => $row["apply_content"]);
		}
		return $arr;
    }

	// 評価表のデータをDBへ更新する
	function update_ward_grade_data($apply_id, $inputs) {

		//========================================================================
		// xmlを解析し対応テーブルにインサート
		//========================================================================
		// DB登録に必要なデータを取得
		$select_sql = "select A.apply_id, A.apply_content, A.emp_id, W.short_wkfw_name from cl_apply A, cl_wkfwmst W ";
		$select_cond = "where A.apply_id = '$apply_id' and A.wkfw_id = W.wkfw_id ";

		$select_result = select_from_table($this->_db_con, $select_sql, $select_cond, $this->file_name);
		if ($select_result == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$apply_id = pg_result($select_result, 0, "apply_id");
		$xml_data = pg_result($select_result, 0, "apply_content");
		$emp_id = pg_result($select_result, 0, "emp_id");
		$tmpl_cd = pg_result($select_result, 0, "short_wkfw_name");

		// DB定義XMLファイルを読み込む
		$xml_db_path = "cl/db_xml/cl_wkfw_$tmpl_cd.xml";
		$xml_db = file_get_contents($xml_db_path);
		if ($xml_db === FALSE) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 一時的にinclude_pathを変更
		$old_inc_dir = ini_get('include_path');
		$add_inc_dir = dirname(__FILE__) . "/PEAR";
		$inc_separate = ":";
/*
		$os_name = php_uname('s');
		if ('W' == $os_name[0]){
			$inc_separate = ";";
		}
*/
		ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

		// XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
		require_once("Unserializer.php");
		$options = array(
			'parseAttributes' => FALSE,
			'forceEnum' => array('q','var_name')
		);
		$Unserializer =&new XML_Unserializer($options);
if (PHP_VERSION >= '5') {
$xml_db = str_replace("EUC-JP", "EUCJP-win", $xml_db);
}
		$Unserializer->unserialize($xml_db);
		$xml_db_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_db_array);
}

		$Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$xml_data = str_replace("EUC-JP", "EUCJP-win", $xml_data);
}
		$Unserializer->unserialize($xml_data);
		$xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

		// include_pathを元に戻す
		ini_set('include_path', $old_inc_dir);


		// insertするデータの作成
		$table = $xml_db_array["table"];
		$insert_data = array();

		// key項目
		$insert_keys = array();
		$insert_keys["apply_id"] = $apply_id;
		$insert_keys["emp_id"] = $emp_id;
		$insert_keys["kanri_no"] = $tmpl_cd;
		foreach ($xml_db_array["key"] as $key){
			$insert_keys[$key] = $xml_data_array[$key];
		}

		// 既存データを削除
		$this->delete_ward_grade_data($apply_id, $tmpl_cd);

		foreach ($xml_db_array["q"] as $qs){
			foreach ($qs["var_name"] as $var_name){

				// xmlの各データをテンプレートに対応するテーブルに登録する。
				$value = $xml_data_array[$var_name];

				$insert_data = array_merge($insert_keys, array(
					'var_name' => mb_convert_encoding($var_name, "UTF-8", "EUC-JP"),
					'value'     => $inputs[mb_convert_encoding($var_name, "UTF-8", "EUC-JP")])
				);

				$insert_result = $this->insert_into_table_array($table, $insert_data);

				// 登録失敗時の処理
				if ($insert_result == 0)
				{
					pg_query($this->_db_con,"rollback");
					pg_close($this->_db_con);
					echo("<script type='text/javascript' src='./js/showpage.js'></script>");
					echo("<script language='javascript'>showErrorPage(window);</script>");
					exit;
				}
			}
		}

	}

	// 評価データ削除
	function delete_ward_grade_data($apply_id, $tmpl_cd) {

		$sql = "delete from cl_wkfw_$tmpl_cd ";
		$cond = "where apply_id = '$apply_id'";

		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}

//-------------------------------------------------------------------------------------------------------------------------
// 達成期間
//-------------------------------------------------------------------------------------------------------------------------

	// 達成期間情報取得
	function get_cl_achievement_period($emp_id, $achievement_order, $status, $level)
	{
		$sql  = "select * ";
		$sql .= "from cl_achievement_period ";
		$cond = "where emp_id = '$emp_id' ";

        if($status != "")
        {
            $cond .= "and status = $status ";
        }

        if($level != "")
        {
            $cond .= "and level = $level ";
        }

        if($achievement_order != "")
        {
            $cond .= "and achievement_order = $achievement_order ";
        }
        else
        {
            $cond .= "order by achievement_order asc ";
        }



		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
			                 "emp_id" => $row["emp_id"],
			                 "achievement_order" => $row["achievement_order"],
			                 "start_period" => $row["start_period"],
			                 "end_period" => $row["end_period"],
                             "approval_emp_ids" => $row["approval_emp_ids"],
                             "status" => $row["status"],
                             "level" => $row["level"]
			               );
		}
		return $arr;
	}

	// 達成期間情報登録
    function regist_cl_achievement_period($arr)
	{
		$emp_id = $arr["emp_id"];
		$achievement_order = $arr["achievement_order"];
		$start_period = $arr["start_period"];
		$end_period = $arr["end_period"];
        $approval_emp_ids = $arr["approval_emp_ids"];
        $status = $arr["status"];
        $level = $arr["level"];

		$sql = "insert into cl_achievement_period (emp_id, achievement_order, start_period, end_period, approval_emp_ids, status, level) values (";
		$content = array($emp_id, $achievement_order, $start_period, $end_period, $approval_emp_ids, $status, $level);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 達成期間情報削除
    function delete_cl_achievement_period($emp_id)
	{
        $sql = "delete from cl_achievement_period";
		$cond = "where emp_id = '$emp_id'";

		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // 病棟評価表用承認者情報配列作成
    function create_approve_array($emp_id, $achievement_order, $status, $level)
    {
        $arr_achievement_period = $this->get_cl_achievement_period($emp_id, $achievement_order, $status, $level);

        if(count($arr_achievement_period) == 0)
        {
            return array();
        }

        $cnt = count($arr_achievement_period);
        $emp_infos = array();
        $arr_approval_emp_ids = split(",", $arr_achievement_period[$cnt - 1]["approval_emp_ids"]);
        for($i=0; $i<count($arr_approval_emp_ids); $i++)
        {
            if($arr_approval_emp_ids[$i] != "")
            {
	            $arr_emp = $this->get_empmst_detail($arr_approval_emp_ids[$i]);

	            $emp_infos[] = array(
	                                  "emp_id" => $arr_emp[0]["emp_id"],
	                                  "emp_full_nm" => $arr_emp[0]["emp_lt_nm"]." ".$arr_emp[0]["emp_ft_nm"],
	                                  "st_nm" => $arr_emp[0]["st_nm"],
	                                  "apv_div" => "1",
                                      "emp_lt_nm" => $arr_emp[0]["emp_lt_nm"],
                                      "emp_ft_nm" => $arr_emp[0]["emp_ft_nm"]
	                                 );
            }
        }
        return $emp_infos;
    }

    // 病棟評価申請のＤＢ定義取得
    function get_ward_template_db($short_wkfw_name)
    {

		// DB定義XMLファイルを読み込む
		// TODO ファイルパス直書き
		$xml_db_path = "cl/db_xml/cl_wkfw_$short_wkfw_name.xml";
		$xml_db = file_get_contents($xml_db_path);
		if ($xml_db === FALSE) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 一時的にinclude_pathを変更
		$old_inc_dir = ini_get('include_path');
		$add_inc_dir = dirname(__FILE__) . "/PEAR";
		$inc_separate = ":";

		ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

		// XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
		require_once("Unserializer.php");
		$options = array(
			'parseAttributes' => FALSE,
			'forceEnum' => array('q','var_name')
		);
		$Unserializer =&new XML_Unserializer($options);
if (PHP_VERSION >= '5') {
$xml_db = str_replace("EUC-JP", "EUCJP-win", $xml_db);
}
		$Unserializer->unserialize($xml_db);
		$xml_db_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_db_array);
}

		// include_pathを元に戻す
		ini_set('include_path', $old_inc_dir);

        $arr = array();

        // 達成期間、評価月
		foreach ($xml_db_array["key"] as $key){
			$arr[] = $key;
		}

        // 自己評価、達成課題
        foreach ($xml_db_array["q"] as $qs){
            foreach ($qs["var_name"] as $var_name){

                $last = substr($var_name, strlen($var_name) - 2, 2);
                if($last != "_2")
                {
                    $arr[] = $var_name;
                }
	       }
       }
       return $arr;
    }
//-------------------------------------------------------------------------------------------------------------------------
// ＣＡＳテンプレートチェック
//-------------------------------------------------------------------------------------------------------------------------

    // 病棟評価申請チェック
	function is_ward_template($short_wkfw_name)
	{

        $arr = array(
                      "c001", "c002", "c003", "c004",
                      "c005", "c006", "c007", "c008",
                      "c009", "c010", "c011", "c012",
                      "c013", "c014", "c015", "c016",
                      "c017", "c018", "c019", "c020",
                      "c021", "c022", "c023", "c024",
                      "c025", "c026", "c027", "c028",
                      "c029", "c030", "c031", "c032",
                      // 2011/11/21 Yamagawa add(s)
                      "g001", "g002", "g003", "g004",
                      // 2011/11/21 Yamagawa add(e)
                      // 2011/12/21 Yamagawa add(s)
                      "g005", "g006", "g007", "g008",
                      "g009", "g010", "g011", "g012"
                      // 2011/12/21 Yamagawa add(e)
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // レベルアップ志願書チェック
	function is_application_book_template($short_wkfw_name)
	{
        $arr = array(
                      "c101", "c102",
                      // 2011/11/21 Yamagawa add(s)
                      "g101", "g102",
                      "g103", "g104"
                      // 2011/11/21 Yamagawa add(e)
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // 評価申請チェック
	function is_eval_template($short_wkfw_name)
	{
        $arr = array(
                      "c201", "c202", "c203", "c204",
                      "c205", "c206", "c207", "c208",
                      "c209", "c210", "c211", "c212",
                      "c213", "c214", "c215", "c216",
                      // 2011/11/25 Yamagawa add(s)
                      "g201", "g202", "g203", "g204",
                      "g205", "g206", "g207", "g208",
                      "g209", "g210", "g211", "g212",
                      "g213", "g214", "g215", "g216",
                      "g217", "g218", "g219", "g220",
                      "g221", "g222", "g223", "g224",
                      "g225", "g226", "g227", "g228",
                      "g229", "g230", "g231", "g232",
                      "g233", "g234", "g235", "g236",
                      "g237", "g238", "g239", "g240",
                      "g241", "g242", "g243"
                      // 2011/11/25 Yamagawa add(e)
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // 評価申請レベル２チェック
	function is_eval_level2_template($short_wkfw_name)
	{
        $arr = array(
                      "c201", "c202", "c203", "c204",
                      "c205", "c206", "c207", "c208"
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // 評価申請レベル３チェック
	function is_eval_level3_template($short_wkfw_name)
	{
        $arr = array(
                      "c209", "c210", "c211", "c212",
                      "c213", "c214", "c215", "c216"
                     );

         return in_array($short_wkfw_name, $arr);
	}


    // レベルアップ申請チェック
	function is_levelup_apply_template($short_wkfw_name)
	{
        $arr = array(
                      "c301", "c302"
                     // 2011/11/30 Yamagawa add(s)
                     ,"g301", "g302"
                     ,"g303", "g304"
                     // 2011/11/30 Yamagawa add(e)
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // キャリア申告チェック
	function is_career_apply_template($short_wkfw_name)
	{
        $arr = array(
                      "c401"
                     );

         return in_array($short_wkfw_name, $arr);
	}

    // 受講報告チェック
	function is_attendance_apply_template($short_wkfw_name)
	{
        $arr = array(
                      "c502", "c503", "c504", "c505", "c506"
                     );

         return in_array(strtolower($short_wkfw_name), $arr);
	}

    // 病棟評価申請(ステータス、レベル取得)
	function get_ward_template_status($short_wkfw_name)
	{
        switch($short_wkfw_name)
        {
            case "c001":
            case "c002":
            case "c003":
            case "c004":
                $status = "1";
                $level = "1";
                break;
            case "c005":
            case "c006":
            case "c007":
            case "c008":
                $status = "1";
                $level = "2";
                break;
            case "c009":
            case "c010":
            case "c011":
            case "c012":
                $status = "1";
                $level = "3";
                break;
            case "c013":
            case "c014":
            case "c015":
            case "c016":
                $status = "1";
                $level = "4";
                break;
            case "c017":
            case "c018":
            case "c019":
            case "c020":
                $status = "2";
                $level = "1";
                break;
            case "c021":
            case "c022":
            case "c023":
            case "c024":
                $status = "2";
                $level = "2";
                break;
            case "c025":
            case "c026":
            case "c027":
            case "c028":
                $status = "2";
                $level = "3";
                break;
            case "c029":
            case "c030":
            case "c031":
            case "c032":
                $status = "2";
                $level = "4";
                break;
// 2011/12/21 Yamagawa add(s)
            case "g001":
            case "g002":
            case "g003":
            case "g004":
                $status = "3";
                $level = "1";
                break;
            case "g005":
            case "g006":
            case "g007":
            case "g008":
                $status = "3";
                $level = "2";
                break;
            case "g009":
            case "g010":
            case "g011":
            case "g012":
                $status = "3";
                $level = "3";
                break;
// 2011/12/21 Yamagawa add(e)
        }
        return array("status" => $status, "level" => $level);
	}



//-------------------------------------------------------------------------------------------------------------------------
// 志願書選択
//-------------------------------------------------------------------------------------------------------------------------
	function get_cl_recognize_schedule_for_appliction_book_list($level, $emp_id, $kanri_no, $mode = "default") {

		// 2011/11/25 Yamagawa add(s)
		// 管理コードより看護区分を特定する
		switch ($kanri_no) {

			// 病棟
			case "c201":
			case "c202":
			case "c203":
			case "c204":
			case "c205":
			case "c206":
			case "c207":
			case "c208":
			case "c209":
			case "c210":
			case "c211":
			case "c212":
			case "c213":
			case "c214":
			case "c215":
			case "c216":
				$nurse_type = 1;
				break;

			// 外来
			case "g201":
			case "g202":
			case "g203":
			case "g204":
			case "g205":
			case "g206":
			case "g207":
			case "g208":
			case "g209":
			case "g210":
			case "g211":
			case "g223":
			case "g224":
			case "g225":
			case "g226":
			case "g227":
			case "g228":
			case "g229":
			case "g230":
			case "g231":
			case "g232":
				$nurse_type = 2;
				break;

			// 外来（准看護師）
			case "g212":
			case "g213":
			case "g214":
			case "g215":
			case "g216":
			case "g217":
			case "g218":
			case "g219":
			case "g220":
			case "g221":
			case "g222":
			case "g233":
			case "g234":
			case "g235":
			case "g236":
			case "g237":
			case "g238":
			case "g239":
			case "g240":
			case "g241":
			case "g242":
			case "g243":
				$nurse_type = 3;
				break;
		}
		// 2011/11/25 Yamagawa add(e)

		// 2011/11/25 Yamagawa upd(s)
		//$arr_tmp_recognize_schedule = $this->get_cl_recognize_schedule_for_appliction_book($level, $emp_id);
		$arr_tmp_recognize_schedule = $this->get_cl_recognize_schedule_for_appliction_book($level, $emp_id, $nurse_type);
		// 2011/11/25 Yamagawa upd(e)
		$arr_recognize_schedule = array();
		foreach ($arr_tmp_recognize_schedule as $schedule) {
			switch ($kanri_no) {
			case "c201":
				$no_apply_id = $schedule["no1_apply_id"];
				break;
			case "c202":
				$no_apply_id = $schedule["no3_apply_id"];
				break;
			case "c203":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "c204":
				$no_apply_id = $schedule["no6_apply_id"];
				break;
			case "c205":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "c206":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "c207":
				$no_apply_id = $schedule["no10_apply_id"];
				break;
			case "c208":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			case "c209":
				$no_apply_id = $schedule["no2_apply_id"];
				break;
			case "c210":
				$no_apply_id = $schedule["no4_apply_id"];
				break;
			case "c211":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "c212":
				$no_apply_id = $schedule["no7_apply_id"];
				break;
			case "c213":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "c214":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "c215":
				$no_apply_id = $schedule["no11_apply_id"];
				break;
			case "c216":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			// 2011/11/25 Yamagawa add(s)
			case "g201":
				$no_apply_id = $schedule["no1_apply_id"];
				break;
			case "g202":
				$no_apply_id = $schedule["no3_apply_id"];
				break;
			case "g203":
				$no_apply_id = $schedule["no31_apply_id"];
				break;
			case "g204":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "g205":
				$no_apply_id = $schedule["no6_apply_id"];
				break;
			case "g206":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "g207":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "g208":
				$no_apply_id = $schedule["no10_apply_id"];
				break;
			case "g209":
				$no_apply_id = $schedule["no32_apply_id"];
				break;
			case "g210":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			case "g211":
				$no_apply_id = $schedule["no33_apply_id"];
				break;
			case "g212":
				$no_apply_id = $schedule["no1_apply_id"];
				break;
			case "g213":
				$no_apply_id = $schedule["no3_apply_id"];
				break;
			case "g214":
				$no_apply_id = $schedule["no31_apply_id"];
				break;
			case "g215":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "g216":
				$no_apply_id = $schedule["no6_apply_id"];
				break;
			case "g217":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "g218":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "g219":
				$no_apply_id = $schedule["no10_apply_id"];
				break;
			case "g220":
				$no_apply_id = $schedule["no32_apply_id"];
				break;
			case "g221":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			case "g222":
				$no_apply_id = $schedule["no33_apply_id"];
				break;
			case "g223":
				$no_apply_id = $schedule["no2_apply_id"];
				break;
			case "g224":
				$no_apply_id = $schedule["no4_apply_id"];
				break;
			case "g225":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "g226":
				$no_apply_id = $schedule["no7_apply_id"];
				break;
			case "g227":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "g228":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "g229":
				$no_apply_id = $schedule["no11_apply_id"];
				break;
			case "g230":
				$no_apply_id = $schedule["no32_apply_id"];
				break;
			case "g231":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			case "g232":
				$no_apply_id = $schedule["no33_apply_id"];
				break;
			case "g233":
				$no_apply_id = $schedule["no1_apply_id"];
				break;
			case "g234":
				$no_apply_id = $schedule["no3_apply_id"];
				break;
			case "g235":
				$no_apply_id = $schedule["no31_apply_id"];
				break;
			case "g236":
				$no_apply_id = $schedule["no5_apply_id"];
				break;
			case "g237":
				$no_apply_id = $schedule["no7_apply_id"];
				break;
			case "g238":
				$no_apply_id = $schedule["no8_apply_id"];
				break;
			case "g239":
				$no_apply_id = $schedule["no9_apply_id"];
				break;
			case "g240":
				$no_apply_id = $schedule["no11_apply_id"];
				break;
			case "g241":
				$no_apply_id = $schedule["no32_apply_id"];
				break;
			case "g242":
				$no_apply_id = $schedule["no12_apply_id"];
				break;
			case "g243":
				$no_apply_id = $schedule["no33_apply_id"];
				break;
			// 2011/11/25 Yamagawa add(e)
			}

			// 評価申請済みの場合
			if ($no_apply_id != "") {
				$arr_no_apply_id = split(",", $no_apply_id);
				foreach ($arr_no_apply_id as $tmp_no_apply_id) {
					$arr_apply_wkfwmst = $this->get_apply_wkfwmst($tmp_no_apply_id);

					// 未承認のみを表示する場合
					if ($mode == "default") {

						// 承認済みの場合、表示対象外とする
						if ($arr_apply_wkfwmst[0]["apply_stat"] != "1") {
							$arr_recognize_schedule[] = $schedule;
							break;
						}

					// 承認済みのみを表示する場合
					} else if ($mode == "approved") {

						// 承認済みなら表示対象とする
						if ($arr_apply_wkfwmst[0]["apply_stat"] == "1") {
							$arr_recognize_schedule[] = $schedule;
							break;
						}

					// すべて表示する場合、必ず表示対象とする
					} else if ($mode == "all") {
						$arr_recognize_schedule[] = $schedule;
						break;
					}
				}

			// 評価未申請の場合
			} else {

				// 承認済みのみを表示する指定でなければ表示対象とする
				if ($mode != "approved") {
					$arr_recognize_schedule[] = $schedule;
				}
			}
		}
		return $arr_recognize_schedule;
	}

	// 認定スケジュール情報取得処理(志願書申請番号選択画面用)
	// 2011/11/25 Yamagawa upd(s)
	//function get_cl_recognize_schedule_for_appliction_book($level, $emp_id)
	function get_cl_recognize_schedule_for_appliction_book($level, $emp_id, $nurse_type)
	// 2011/11/25 Yamagawa upd(e)
	{
		$sql   = "select a.*, b.emp_lt_nm, b.emp_ft_nm, c.apply_content, c.apply_date, c.apply_no, c.short_wkfw_name ";
        $sql  .= "from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
        $sql  .= "left join (select cl_apply.apply_id, cl_apply.apply_content, cl_apply.apply_date, cl_apply.apply_no, cl_apply.delete_flg ,cl_wkfwmst.short_wkfw_name from cl_apply ";
        $sql  .= "left join cl_wkfwmst on cl_apply.wkfw_id = cl_wkfwmst.wkfw_id ) c on a.apply_id = c.apply_id ";
		// 2011/11/25 Yamagawa upd(s)
		//$cond  = "where a.levelup_apply_id is null and disp_flg and a.emp_id = '$emp_id' and a.level = $level and not c.delete_flg ";
		// 2011/12/22 Yamagawa upd(s)
		//$cond  = "where a.levelup_apply_id is null and disp_flg and a.emp_id = '$emp_id' and a.level = $level and a.nurse_type = $nurse_type and not c.delete_flg ";
		$cond  = "where disp_flg and a.emp_id = '$emp_id' and a.level = $level and a.nurse_type = $nurse_type and not c.delete_flg ";
		// 2011/12/22 Yamagawa upd(e)
		// 2011/11/25 Yamagawa upd(e)
        $cond .= "order by a.apply_id asc ";


		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
                            "apply_date"       => $row["apply_date"],
	                        "emp_id"           => $row["emp_id"],
	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],
	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"],
                            "apply_content"    => $row["apply_content"],
                            "apply_date"       => $row["apply_date"],
                            "apply_no"         => $row["apply_no"],
                            "short_wkfw_name"  => $row["short_wkfw_name"],
	                        // 2011/11/25 Yamagawa add(s)
	                        "nurse_type"       => $row["nurse_type"],
	                        "no31_date"        => $row["no31_date"],
	                        "no31_apply_id"    => $row["no31_apply_id"],
	                        "no32_date"        => $row["no32_date"],
	                        "no32_apply_id"    => $row["no32_apply_id"],
	                        "no32_date"        => $row["no33_date"],
	                        "no32_apply_id"    => $row["no33_apply_id"]
	                        // 2011/11/25 Yamagawa add(e)
	                       );

		}
		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// 評価結果
//-------------------------------------------------------------------------------------------------------------------------

    // 評価結果データ取得
    function get_cl_evaluation_result($apply_id, $apv_order, $apv_sub_order)
    {
		$sql  = "select * from cl_applyapv";
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order ";

        if($apv_sub_order != "")
        {
            $cond .= "and apv_sub_order = $apv_sub_order";
        }

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        $arr = array();
        while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "apv_order" => $row["apv_order"],
                            "apv_sub_order" => $row["apv_sub_order"],
                            "emp_id" => $row["emp_id"],
                            "draft_flg" => $row["draft_flg"],
                            "eval_content" => $row["eval_content"]
                           );
		}
		return $arr;
    }

	// 承認テーブル更新処理
	function update_eval_content($arr)
	{
		$apply_id      = $arr["apply_id"];
		$apv_order     = $arr["apv_order"];
		$apv_sub_order = ($arr["apv_sub_order"] == "") ? NULL : $arr["apv_sub_order"];
		$eval_content  = $arr["eval_content"];

		$sql = "update cl_applyapv set";
		$set = array("eval_content");
		$setvalue = array(pg_escape_string($eval_content));
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order ";

        if($apv_sub_order == "")
        {
            $cond .= "and apv_sub_order isnull ";
        }
        else
        {
            $cond .= "and apv_sub_order = $apv_sub_order ";
        }

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// 承認テーブル更新処理
	function update_draft_flg($arr)
	{
		$apply_id      = $arr["apply_id"];
		$apv_order     = $arr["apv_order"];
		$apv_sub_order = ($arr["apv_sub_order"] == "") ? NULL : $arr["apv_sub_order"];
     	$apv_comment   = $arr["apv_comment"];
     	$draft_flg     = $arr["draft_flg"];

		$sql = "update cl_applyapv set";
		$set = array("apv_comment","draft_flg");
		$setvalue = array(pg_escape_string($apv_comment), $draft_flg);
		$cond = "where apply_id = '$apply_id' and apv_order = $apv_order ";

        if($apv_sub_order == "")
        {
            $cond .= "and apv_sub_order isnull ";
        }
        else
        {
            $cond .= "and apv_sub_order = $apv_sub_order ";
        }

		$up = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if($up == 0)
		{
			pg_query($this->_db_con,"rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

//-------------------------------------------------------------------------------------------------------------------------
// レベルアップ志願者選択
//-------------------------------------------------------------------------------------------------------------------------

	function get_levelup_apply_emp_list($level, $class, $atrb, $dept, $room)
	{
		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.emp_lt_nm, ";
		$sql  .= "b.emp_ft_nm, ";
		$sql  .= "c.apply_content, ";
		$sql  .= "c.apply_date, ";
		$sql  .= "c.apply_no, ";
		$sql  .= "c.short_wkfw_name ";
		$sql  .= "from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "left join (";
		$sql  .= "select ";
		$sql  .= "cl_apply.apply_id, ";
		$sql  .= "cl_apply.apply_content, ";
		$sql  .= "cl_apply.apply_date, ";
		$sql  .= "cl_apply.apply_no, ";
		$sql  .= "cl_apply.delete_flg, ";
		$sql  .= "cl_wkfwmst.short_wkfw_name ";
		$sql  .= "from cl_apply ";
		$sql  .= "left join cl_wkfwmst on cl_apply.wkfw_id = cl_wkfwmst.wkfw_id ";
		$sql  .= ") c on a.apply_id = c.apply_id ";
		$cond  = "where a.disp_flg and a.level = $level and not c.delete_flg ";
		if ($class != "") {
			$cond .= "and (";
			$cond .= "(b.emp_class = $class ";
			if ($atrb != "") {
				$cond .= "and b.emp_attribute = $atrb ";
			}
			if ($dept != "") {
				$cond .= "and b.emp_dept = $dept ";
			}
			if ($room != "") {
				$cond .= "and b.emp_room = $room ";
			}
			$cond .= ") ";
			$cond .= " or ";
			$cond .= "(exists (select * from concurrent where concurrent.emp_id = b.emp_id and concurrent.emp_class = $class ";
			if ($atrb != "") {
				$cond .= "and concurrent.emp_attribute = $atrb ";
			}
			if ($dept != "") {
				$cond .= "and concurrent.emp_dept = $dept ";
			}
			if ($room != "") {
				$cond .= "and concurrent.emp_room = $room ";
			}
			$cond .= ")) ";
			$cond .= ") ";
		}
		$cond .= "order by a.apply_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
	                        "emp_id"           => $row["emp_id"],
	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],
	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"],
                            "apply_content"    => $row["apply_content"],
                            "apply_date"       => $row["apply_date"],
                            "apply_no"         => $row["apply_no"],
                            "short_wkfw_name"  => $row["short_wkfw_name"]
	                       );

		}
		return $arr;
    }

/**
 * レベルアップ志願者選択
 * @param $level String レベル
 * @param $classl  String 部
 * @param $atrb   String 課
 * @param $dept  String 科
 * @param $room  String 室
 * @param $emp_id String  職員ID
 * @param $nurse_type Integer 看護区分 2011.11.29 Add by Yamagawa
 * @return $arr Array　該当志願者
 * @author　2011.11.14　Add by matsuura
 */
	function get_levelup_apply_emp_list2($level, $class, $atrb, $dept, $room, $emp_id, $nurse_type)
	{
		$sql   = "select ";
		$sql  .= "a.*, ";
		$sql  .= "b.emp_lt_nm, ";
		$sql  .= "b.emp_ft_nm, ";

		// 2011.11.18 Add by matsuura
		$sql  .= "b.emp_class, b.emp_attribute, b.emp_dept, b.emp_room, ";

		$sql  .= "c.apply_content, ";
		$sql  .= "c.apply_date, ";
		$sql  .= "c.apply_no, ";
		$sql  .= "c.short_wkfw_name ";
		$sql  .= "from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
		$sql  .= "left join (";
		$sql  .= "select ";
		$sql  .= "cl_apply.apply_id, ";
		$sql  .= "cl_apply.apply_content, ";
		$sql  .= "cl_apply.apply_date, ";
		$sql  .= "cl_apply.apply_no, ";
		$sql  .= "cl_apply.delete_flg, ";
		$sql  .= "cl_wkfwmst.short_wkfw_name ";
		$sql  .= "from cl_apply ";
		$sql  .= "left join cl_wkfwmst on cl_apply.wkfw_id = cl_wkfwmst.wkfw_id ";
		$sql  .= ") c on a.apply_id = c.apply_id ";

		// 2011.11.29 Upd by Yamagawa
		//$cond  = "where a.disp_flg and a.level = $level and not c.delete_flg ";
//		$cond  = "where a.disp_flg and a.level = $level and a.nurse_type = $nurse_type and not c.delete_flg ";
		// 2011.12.07 Edit by matsuura
		$cond  = "where a.disp_flg and a.level = $level and not c.delete_flg ";
		if ($nurse_type != "") {
			$cond .= "and a.nurse_type = $nurse_type ";
		}

		if ($class != "") {
			$cond .= "and (";
			$cond .= "(b.emp_class = $class ";

			if ($atrb != "") {
				$cond .= "and b.emp_attribute = $atrb ";
			}
			if ($dept != "") {
				$cond .= "and b.emp_dept = $dept ";
			}
			if ($room != "") {
				$cond .= "and b.emp_room = $room ";
			}

//			if ($emp_id != "") {
//				$cond .= "and a.emp_id = $emp_id ";
//			}

			$cond .= ") ";
			$cond .= " or ";
			$cond .= "(exists (select * from concurrent where concurrent.emp_id = b.emp_id and concurrent.emp_class = $class ";
			if ($atrb != "") {
				$cond .= "and concurrent.emp_attribute = $atrb ";
			}
			if ($dept != "") {
				$cond .= "and concurrent.emp_dept = $dept ";
			}
			if ($room != "") {
				$cond .= "and concurrent.emp_room = $room ";
			}
			$cond .= ")) ";
			$cond .= ") ";
		}
		$cond .= "order by a.apply_id";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

//		echo($sql . $cond);

		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$iflag = 1;
			if($class != ""){
				if($row["emp_class"] != $class || $row["emp_attribute"] != $atrb || $row["emp_dept"] != $dept){
					$iflag = 0;
				}

				if($room != ""){
					if($row["emp_room"] != $room){
						$iflag = 0;
					}
				}

				if($emp_id != "" && $row["emp_id"] != $emp_id){
					$iflag = 0;
				}
			}

			if($iflag == 1){
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
	                        "emp_id"           => $row["emp_id"],

							// 2011.11.18 Add by matsuura
	                        "emp_class"           => $row["emp_class"],
	                        "emp_atrb"           => $row["emp_attribute"],
	                        "emp_dept"           => $row["emp_dept"],
	                        "emp_room"           => $row["emp_room"],

	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],

							// 2011.11.29 Add by Yamagawa
	                        "no31_date"        => $row["no31_date"],
	                        "no31_apply_id"    => $row["no31_apply_id"],
	                        "no32_date"        => $row["no32_date"],
	                        "no32_apply_id"    => $row["no32_apply_id"],
	                        "no33_date"        => $row["no33_date"],
	                        "no33_apply_id"    => $row["no33_apply_id"],
	                        "nurse_type"       => $row["nurse_type"],

	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"],
                            "apply_content"    => $row["apply_content"],
                            "apply_date"       => $row["apply_date"],
                            "apply_no"         => $row["apply_no"],
                            "short_wkfw_name"  => $row["short_wkfw_name"]
	                       );
			}

		}
		return $arr;
    }

    function get_levelup_apply($apply_id)
    {
		$sql   = "select ";
        $sql  .= "a.*, ";
        $sql  .= "b.emp_lt_nm, ";
        $sql  .= "b.emp_ft_nm, ";
        $sql  .= "c.apply_content, ";
        $sql  .= "c.apply_date, ";
        $sql  .= "c.apply_no, ";
        $sql  .= "c.short_wkfw_name, ";
        $sql  .= "c.class_nm, ";
        $sql  .= "c.atrb_nm, ";
        $sql  .= "c.dept_nm, ";
        $sql  .= "c.room_nm ";
        $sql  .= "from cl_recognize_schedule a ";
		$sql  .= "left join empmst b on a.emp_id = b.emp_id ";
        $sql  .= "left join (";
        $sql  .= "select ";
        $sql  .= "cl_apply.apply_id, ";
        $sql  .= "cl_apply.apply_content, ";
        $sql  .= "cl_apply.apply_date, ";
        $sql  .= "cl_apply.apply_no, ";
        $sql  .= "cl_wkfwmst.short_wkfw_name, ";
        $sql  .= "classmst.class_nm, ";
        $sql  .= "atrbmst.atrb_nm, ";
        $sql  .= "deptmst.dept_nm, ";
        $sql  .= "classroom.room_nm ";
        $sql  .= "from cl_apply ";
        $sql  .= "left join cl_wkfwmst on cl_apply.wkfw_id = cl_wkfwmst.wkfw_id ";
		$sql  .= "left join classmst on cl_apply.emp_class = classmst.class_id ";
		$sql  .= "left join atrbmst on cl_apply.emp_attribute = atrbmst.atrb_id ";
		$sql  .= "left join deptmst on cl_apply.emp_dept = deptmst.dept_id ";
		$sql  .= "left join classroom on cl_apply.emp_room = classroom.room_id ";
        $sql  .= ") c on a.apply_id = c.apply_id ";
		$cond  = "where a.apply_id = '$apply_id'";

		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"         => $row["apply_id"],
	                        "emp_id"           => $row["emp_id"],
	                        "level"            => $row["level"],
	                        "no1_date"         => $row["no1_date"],
	                        "no1_apply_id"     => $row["no1_apply_id"],
	                        "no2_date"         => $row["no2_date"],
	                        "no2_apply_id"     => $row["no2_apply_id"],
	                        "no3_date"         => $row["no3_date"],
	                        "no3_apply_id"     => $row["no3_apply_id"],
	                        "no4_date"         => $row["no4_date"],
	                        "no4_apply_id"     => $row["no4_apply_id"],
	                        "no5_date"         => $row["no5_date"],
	                        "no5_apply_id"     => $row["no5_apply_id"],
	                        "no6_date"         => $row["no6_date"],
	                        "no6_apply_id"     => $row["no6_apply_id"],
	                        "no7_date"         => $row["no7_date"],
	                        "no7_apply_id"     => $row["no7_apply_id"],
	                        "no8_date"         => $row["no8_date"],
	                        "no8_apply_id"     => $row["no8_apply_id"],
	                        "no9_date"         => $row["no9_date"],
	                        "no9_apply_id"     => $row["no9_apply_id"],
	                        "no10_date"        => $row["no10_date"],
	                        "no10_apply_id"    => $row["no10_apply_id"],
	                        "no11_date"        => $row["no11_date"],
	                        "no11_apply_id"    => $row["no11_apply_id"],
	                        "no12_date"        => $row["no12_date"],
	                        "no12_apply_id"    => $row["no12_apply_id"],
	                        "disp_flg"         => $row["disp_flg"],
	                        "levelup_apply_id" => $row["levelup_apply_id"],
	                        "emp_lt_nm"        => $row["emp_lt_nm"],
	                        "emp_ft_nm"        => $row["emp_ft_nm"],
                            "apply_content"    => $row["apply_content"],
                            "apply_date"       => $row["apply_date"],
                            "apply_no"         => $row["apply_no"],
                            "short_wkfw_name"  => $row["short_wkfw_name"],
                            "class_nm"         => $row["class_nm"],
                            "atrb_nm"          => $row["atrb_nm"],
                            "dept_nm"          => $row["dept_nm"],
                            "room_nm"          => $row["room_nm"],
							// 2011/12/2 Yamagawa add(s)
	                        "nurse_type"       => $row["nurse_type"],
	                        "no31_date"        => $row["no31_date"],
	                        "no31_apply_id"    => $row["no31_apply_id"],
	                        "no32_date"        => $row["no32_date"],
	                        "no32_apply_id"    => $row["no32_apply_id"],
	                        "no33_date"        => $row["no33_date"],
	                        "no33_apply_id"    => $row["no33_apply_id"]
							// 2011/12/2 Yamagawa add(e)
	                       );

		}
		return $arr;
    }

//-------------------------------------------------------------------------------------------------------------------------
// ＣＡＳ結果通知
//-------------------------------------------------------------------------------------------------------------------------

    // ＣＡＳ結果通知取得
    function get_cl_result_notice($apply_id)
    {
		$sql  = "select * from cl_result_notice";
		$cond = "where apply_id = '$apply_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        $arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "result_notice_contents" => $row["result_notice_contents"]
                           );
		}
		return $arr;
    }


    // ＣＡＳ結果通知登録
    function regist_cl_result_notice($arr)
    {
		$apply_id = $arr["apply_id"];
		$result_notice_contents = $arr["result_notice_contents"];

		$sql = "insert into cl_result_notice (apply_id, result_notice_contents) values (";
		$content = array($apply_id, pg_escape_string($result_notice_contents));
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // ＣＡＳ結果通知削除
    function delete_cl_result_notice($apply_id)
	{
        $sql = "delete from cl_result_notice";
		$cond = "where apply_id = '$apply_id'";

		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// 2011/12/2 Yamagawa upd(s)
    //function create_result_notice_contents_xml($POST, $level)
    function create_result_notice_contents_xml($POST, $level, $nurse_type)
	// 2011/12/2 Yamagawa upd(e)
    {
        $doc = domxml_new_doc("1.0");
        $root = $doc->create_element("apply");
        $root = $doc->append_child($root);

        if($level == "2")
        {
            // 2011/12/2 Yamagawa add(s)
            if($nurse_type == "1")
            {
            // 2011/12/2 Yamagawa add(e)
                $arr = array("1", "3", "5", "6", "8", "9", "10", "12");
            // 2011/12/2 Yamagawa add(s)
            }
            else if($nurse_type == "2" || $nurse_type == "3")
            {
                $arr = array("1", "3", "5", "6", "8", "9", "10", "12", "31", "32", "33");
            }
            // 2011/12/2 Yamagawa add(e)
        }
        else if($level == "3")
        {
            // 2011/12/2 Yamagawa add(s)
            if($nurse_type == "1")
            {
            // 2011/12/2 Yamagawa add(e)
                $arr = array("2", "4", "5", "7", "8", "9", "11", "12");
            // 2011/12/2 Yamagawa add(s)
            }
            else if($nurse_type == "2")
            {
                $arr = array("2", "4", "5", "7", "8", "9", "11", "12", "32", "33");
            }
            else if($nurse_type == "3")
            {
                $arr = array("1", "3", "5", "7", "8", "9", "11", "12", "31", "32", "33");
            }
            // 2011/12/2 Yamagawa add(e)
        }

        // 2011/12/2 Yamagawa upd(s)
        //for($i=1; $i<=12; $i++)
        for($i=1; $i<=33; $i++)
        // 2011/12/2 Yamagawa upd(e)
        {
            if(in_array($i, $arr))
            {
		        $tmpnode = $root->append_child($doc->create_element("chk_achieved_no$i"));
		        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["chk_achieved_no$i"], "UTF-8", "EUC-JP")));
		        $tmpnode->set_attribute("type", "checkbox");
            }
        }

        // 2011/12/2 Yamagawa upd(s)
        //for($i=1; $i<=12; $i++)
        for($i=1; $i<=33; $i++)
        // 2011/12/2 Yamagawa upd(e)
        {
            if(in_array($i, $arr))
            {
		        $tmpnode = $root->append_child($doc->create_element("chk_unachieved_no$i"));
		        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["chk_unachieved_no$i"], "UTF-8", "EUC-JP")));
		        $tmpnode->set_attribute("type", "checkbox");
            }
        }

        $tmpnode = $root->append_child($doc->create_element("radio_level_recognize"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["radio_level_recognize"], "UTF-8", "EUC-JP")));
        $tmpnode->set_attribute("type", "radio");
        $tmpnode = $root->append_child($doc->create_element("level"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["level"], "UTF-8", "EUC-JP")));
        $tmpnode = $root->append_child($doc->create_element("level_div"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["level_div"], "UTF-8", "EUC-JP")));

        $tmpnode = $root->append_child($doc->create_element("chk_2_1"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["chk_2_1"], "UTF-8", "EUC-JP")));
        $tmpnode->set_attribute("type", "checkbox");
        $tmpnode = $root->append_child($doc->create_element("chk_2_2"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["chk_2_2"], "UTF-8", "EUC-JP")));
        $tmpnode->set_attribute("type", "checkbox");
        $tmpnode = $root->append_child($doc->create_element("chk_2_3_text"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["chk_2_3_text"], "UTF-8", "EUC-JP")));

        $tmpnode = $root->append_child($doc->create_element("date_y1"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["date_y1"], "UTF-8", "EUC-JP")));
        $tmpnode = $root->append_child($doc->create_element("date_m1"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["date_m1"], "UTF-8", "EUC-JP")));
        $tmpnode = $root->append_child($doc->create_element("date_d1"));
        $tmpnode->append_child($doc->create_text_node(mb_convert_encoding($POST["date_d1"], "UTF-8", "EUC-JP")));

        $content = $doc->dump_mem(true, "EUC-JP");

        return $content;
    }

//-------------------------------------------------------------------------------------------------------------------------
// キャリア履歴
//-------------------------------------------------------------------------------------------------------------------------

    // キャリア履歴取得
    function get_cl_career_history($apply_id)
    {
		$sql  = "select * from cl_career_history";
		$cond = "where apply_id = '$apply_id'";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        $arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id" => $row["apply_id"],
                            "emp_id" => $row["emp_id"],
                            "apply_level" => $row["apply_level"],
                            "apply_date" => $row["apply_date"],
                            "recognize_level" => $row["recognize_level"],
                            "recognize_level_div" => $row["recognize_level_div"],
                            "recognize_date" => $row["recognize_date"],
                            "recognize_flg" => $row["recognize_flg"],
                            "entry_date" => $row["entry_date"],
                            "career_apply_flg" => $row["career_apply_flg"],
                            "disp_flg" => $row["disp_flg"],
							// 2011/12/2 Yamagawa add(s)
                            "nurse_type" => $row["nurse_type"]
							// 2011/12/2 Yamagawa add(e)
                           );
		}
		return $arr;
    }


    // キャリア履歴登録
    function regist_cl_career_history($arr)
    {
        $apply_id            = $arr["apply_id"];
        $emp_id              = $arr["emp_id"];
        $apply_level         = $arr["apply_level"];
        $apply_date          = $arr["apply_date"];
        $recognize_level     = $arr["recognize_level"];
        $recognize_level_div = $arr["recognize_level_div"];
        $recognize_date      = $arr["recognize_date"];
        $recognize_flg       = $arr["recognize_flg"];
        $entry_date          = $arr["entry_date"];
        $career_apply_flg    = $arr["career_apply_flg"];
        $disp_flg            = $arr["disp_flg"];
		// 2011/12/2 Yamagawa add(s)
		$nurse_type          = $arr["nurse_type"];
		// 2011/12/2 Yamagawa add(e)

		// 2011/12/2 Yamagawa upe(s)
        //$sql = "insert into cl_career_history (apply_id, emp_id, apply_level, apply_date, recognize_level, recognize_level_div, recognize_date, recognize_flg, entry_date, career_apply_flg, disp_flg) values (";
        //$content = array($apply_id, $emp_id, $apply_level, $apply_date, $recognize_level, $recognize_level_div, $recognize_date, $recognize_flg, $entry_date, $career_apply_flg, $disp_flg);
        $sql = "insert into cl_career_history (apply_id, emp_id, apply_level, apply_date, recognize_level, recognize_level_div, recognize_date, recognize_flg, entry_date, career_apply_flg, disp_flg, nurse_type) values (";
        $content = array($apply_id, $emp_id, $apply_level, $apply_date, $recognize_level, $recognize_level_div, $recognize_date, $recognize_flg, $entry_date, $career_apply_flg, $disp_flg, $nurse_type);
		// 2011/12/2 Yamagawa upd(e)
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // キャリア履歴削除
    function delete_cl_career_history($apply_id)
	{
        $sql = "delete from cl_career_history";
		$cond = "where apply_id = '$apply_id'";

		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


    // キャリア履歴更新(表示フラグ)
    function update_cl_career_history_disp_flg($apply_id, $disp_flg)
    {
        $sql = "update cl_career_history set";
		$set = array("disp_flg");
		$setvalue = array($disp_flg);
		$cond = "where apply_id = '$apply_id'";
		$upd = update_set_table($this->_db_con, $sql, $set, $setvalue, $cond, $this->file_name);
		if ($upd == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }


//-------------------------------------------------------------------------------------------------------------------------
// 研修履歴
//-------------------------------------------------------------------------------------------------------------------------

    // 研修履歴取得
    function get_cl_training_history($emp_id)
    {
		$sql  = "select * from cl_training_history";
		$cond = "where apply_id = '$apply_id' order by apply_id asc";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        $arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                            "apply_id"        => $row["apply_id"],
                            "emp_id"          => $row["emp_id"],
                            "start_date"      => $row["start_date"],
                            "end_date"        => $row["end_date"],
                            "training_code"   => $row["training_code"],
                            "training_theme"  => $row["training_theme"],
                            "short_wkfw_name" => $row["short_wkfw_name"]
                           );
		}
		return $arr;
    }


    // 研修履歴登録
    function regist_cl_training_history($arr)
    {
        $apply_id        = $arr["apply_id"];
        $emp_id          = $arr["emp_id"];
        $start_date      = $arr["start_date"];
        $end_date        = $arr["end_date"];
        $training_code   = $arr["training_code"];
        $training_theme  = $arr["training_theme"];
        $short_wkfw_name = $arr["short_wkfw_name"];
        $course_days     = $arr["course_days"];
        $course_times    = $arr["course_times"];

        $sql = "insert into cl_training_history (apply_id, emp_id, start_date, end_date, training_code, training_theme, short_wkfw_name, course_days, course_times) values (";
        $content = array($apply_id, $emp_id, $start_date, $end_date, $training_code, pg_escape_string($training_theme), $short_wkfw_name, $course_days, $course_times);
        $ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if ($ins == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // 研修履歴削除
    function delete_cl_training_history($apply_id)
	{
        $sql = "delete from cl_training_history";
		$cond = "where apply_id = '$apply_id'";

		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // 研修履歴登録処理
    function regist_cl_training_apply($apply_id)
    {
        $arr_apply_wkfwmst = $this->get_apply_wkfwmst($apply_id);
        $apply_content = $arr_apply_wkfwmst[0]["apply_content"];
        $emp_id = $arr_apply_wkfwmst[0]["emp_id"];
        $short_wkfw_name = $arr_apply_wkfwmst[0]["short_wkfw_name"];
        //array_merge

        $arr = array("apply_id" => $apply_id, "emp_id" => $emp_id, "short_wkfw_name" => $short_wkfw_name);
        $arr_training_apply = $this->get_training_apply($apply_content);

        $arr = array_merge($arr, $arr_training_apply);
        $this->regist_cl_training_history($arr);

    }

//-------------------------------------------------------------------------------------------------------------------------
// 評価申請関連
//-------------------------------------------------------------------------------------------------------------------------
	function create_arr_eval_apply_id($application_book_apply_id, $apply_id, $short_wkfw_name, $add_del_div) {
		switch ($short_wkfw_name) {
		case "c201":
		// 2011/11/29 Yamagawa add(s)
		case "g201":
		case "g212":
		case "g233":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no1_apply_id";
			break;
		case "c202":
		// 2011/11/29 Yamagawa add(s)
		case "g202":
		case "g213":
		case "g234":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no3_apply_id";
			break;
		case "c203":
		// 2011/11/29 Yamagawa add(s)
		case "g204":
		case "g215":
		case "g225":
		case "g236":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no5_apply_id";
			break;
		case "c204":
		// 2011/11/29 Yamagawa add(s)
		case "g205":
		case "g216":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no6_apply_id";
			break;
		case "c205":
		// 2011/11/29 Yamagawa add(s)
		case "g206":
		case "g217":
		case "g227":
		case "g238":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no8_apply_id";
			break;
		case "c206":
		case "c214":
		// 2011/11/29 Yamagawa add(s)
		case "g207":
		case "g218":
		case "g228":
		case "g239":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no9_apply_id";
			break;
		case "c207":
		// 2011/11/29 Yamagawa add(s)
		case "g208":
		case "g219":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no10_apply_id";
			break;
		case "c208":
		// 2011/11/29 Yamagawa add(s)
		case "g210":
		case "g221":
		case "g231":
		case "g242":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no12_apply_id";
			break;
		case "c209":
		// 2011/11/29 Yamagawa add(s)
		case "g223":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no2_apply_id";
			break;
		case "c210":
		// 2011/11/29 Yamagawa add(s)
		case "g224":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no4_apply_id";
			break;
		case "c211":
			$no_apply_id_key = "no5_apply_id";
			break;
		case "c212":
		// 2011/11/29 Yamagawa add(s)
		case "g226":
		case "g237":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no7_apply_id";
			break;
		case "c213":
			$no_apply_id_key = "no8_apply_id";
			break;
		case "c215":
		// 2011/11/29 Yamagawa add(s)
		case "g229":
		case "g240":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_key = "no11_apply_id";
			break;
		case "c216":
			$no_apply_id_key = "no12_apply_id";
			break;
		// 2011/11/29 Yamagawa add(s)
		case "g203":
		case "g214":
		case "g235":
			$no_apply_id_key = "no31_apply_id";
			break;
		case "g209":
		case "g220":
		case "g230":
		case "g241":
			$no_apply_id_key = "no32_apply_id";
			break;
		case "g211":
		case "g222":
		case "g232":
		case "g243":
			$no_apply_id_key = "no33_apply_id";
			break;
		// 2011/11/29 Yamagawa add(e)
		}

		$arr_recognize_schedule = $this->get_cl_recognize_schedule($application_book_apply_id);
		$cur_apply_id = $arr_recognize_schedule[0][$no_apply_id_key];

		if ($add_del_div == "ADD") {
			if ($cur_apply_id != "") {
				$no_apply_id_val = $cur_apply_id . "," . $apply_id;
			} else {
				$no_apply_id_val = $apply_id;
			}
		} else if ($add_del_div == "DEL") {  // 現在は不使用
			$arr_cur_apply_id = split(",", $cur_apply_id);
			for ($i = 0; $i < count($arr_cur_apply_id); $i++) {
				if ($i > 1) {
					$no_apply_id_val .= ",";
				}
				if ($arr_cur_apply_id[$i] != $apply_id) {
					$no_apply_id_val .= $arr_cur_apply_id[$i];
				}
			}
		}

		return array(
			"apply_id" => $application_book_apply_id,
			"no_apply_id_key" => $no_apply_id_key,
			"no_apply_id_val" => $no_apply_id_val
		);
	}

	function get_cl_recognize_schedule_apply_id($no_apply_id, $short_wkfw_name) {
		switch ($short_wkfw_name) {
		case "c201":
		// 2011/11/29 Yamagawa add(s)
		case "g201":
		case "g212":
		case "g233":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no1_apply_id";
			break;
		case "c202":
		// 2011/11/29 Yamagawa add(s)
		case "g202":
		case "g213":
		case "g234":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no3_apply_id";
			break;
		case "c203":
		// 2011/11/29 Yamagawa add(s)
		case "g204":
		case "g215":
		case "g225":
		case "g236":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no5_apply_id";
			break;
		case "c204":
		// 2011/11/29 Yamagawa add(s)
		case "g205":
		case "g216":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no6_apply_id";
			break;
		case "c205":
		// 2011/11/29 Yamagawa add(s)
		case "g206":
		case "g217":
		case "g227":
		case "g238":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no8_apply_id";
			break;
		case "c206":
		case "c214":
		// 2011/11/29 Yamagawa add(s)
		case "g207":
		case "g218":
		case "g228":
		case "g239":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no9_apply_id";
			break;
		case "c207":
		// 2011/11/29 Yamagawa add(s)
		case "g208":
		case "g219":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no10_apply_id";
			break;
		case "c208":
		// 2011/11/29 Yamagawa add(s)
		case "g210":
		case "g221":
		case "g231":
		case "g242":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no12_apply_id";
			break;
		case "c209":
		// 2011/11/29 Yamagawa add(s)
		case "g223":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no2_apply_id";
			break;
		case "c210":
		// 2011/11/29 Yamagawa add(s)
		case "g224":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no4_apply_id";
			break;
		case "c211":
			$no_apply_id_column = "no5_apply_id";
			break;
		case "c212":
		// 2011/11/29 Yamagawa add(s)
		case "g226":
		case "g237":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no7_apply_id";
			break;
		case "c213":
			$no_apply_id_column = "no8_apply_id";
			break;
		case "c215":
		// 2011/11/29 Yamagawa add(s)
		case "g229":
		case "g240":
		// 2011/11/29 Yamagawa add(e)
			$no_apply_id_column = "no11_apply_id";
			break;
		case "c216":
			$no_apply_id_column = "no12_apply_id";
			break;
		// 2011/11/29 Yamagawa add(s)
		case "g203":
		case "g214":
		case "g235":
			$no_apply_id_column = "no31_apply_id";
			break;
		case "g209":
		case "g220":
		case "g230":
		case "g241":
			$no_apply_id_column = "no32_apply_id";
			break;
		case "g211":
		case "g222":
		case "g232":
		case "g243":
			$no_apply_id_column = "no33_apply_id";
			break;
		// 2011/11/29 Yamagawa add(e)
		}

		$sql = "select * from cl_recognize_schedule ";
		$cond = "where $no_apply_id_column = '$no_apply_id' ";
		$cond .= "or $no_apply_id_column like '$no_apply_id,%' ";
		$cond .= "or $no_apply_id_column like '%,$no_apply_id,%' ";
		$cond .= "or $no_apply_id_column like '%,$no_apply_id' ";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "apply_id");
	}

    // 評価申請用の承認者取得
    function get_eval_approval($emp_id, $short_wkfw_name, $application_book_apply_id)
    {
        $arr_apv = array();

        // １階層目の承認者取得
        $level = "2";
		// 2011/11/29 Yamagawa upd(s)
        //if($short_wkfw_name == "c209" || $short_wkfw_name == "c210" || $short_wkfw_name == "c211" || $short_wkfw_name == "c212" ||
        //    $short_wkfw_name == "c213" || $short_wkfw_name == "c214" || $short_wkfw_name == "c215" || $short_wkfw_name == "c216")
		if(in_array($short_wkfw_name, array("c209", "c210", "c211", "c212", "c213", "c214", "c215", "c216",
											"g223", "g224", "g225", "g226", "g227", "g228", "g229", "g230",
											"g231", "g232", "g233", "g234", "g235", "g236", "g237", "g238",
											"g239", "g240", "g241", "g242", "g243")))
		// 2011/11/29 Yamagawa upd(e)
        {
            $level = "3";
        }
        $arr_recognize_schedule = $this->get_cl_recognize_schedule_for_appliction_book_list($level, $emp_id, $short_wkfw_name, (($application_book_apply_id == "") ? "default" : "all"));

        // 志願日より１か月以内の場合、自動表示する。→ 2012/01/17 Yamagawa upd 最後に申請(承認済み)した志願を自動表示する
        $arr_recognize_schedule = array_reverse($arr_recognize_schedule);
        foreach($arr_recognize_schedule as $recognize_schedule)
        {
			// 2012/01/17 Yamagawa del(s)
            //$recognize_schedule_apply_content = $recognize_schedule["apply_content"];
            //$recognize_schedule_apply_date = str_replace("/", "", $this->get_apply_date($recognize_schedule_apply_content));
			// 2012/01/17 Yamagawa del(e)

            if($application_book_apply_id == "")
            {
				// 2012/01/17 Yamagawa del(s)
	            //// 本日日付から１か月前日付取得
	            //$one_month_ago = date("Ymd",strtotime("-1 month" ,strtotime(date("Ymd"))));
	            //if($recognize_schedule_apply_date >= $one_month_ago)
	            //{
				// 2012/01/17 Yamagawa del(e)
	                $recognize_schedule_apply_id = $recognize_schedule["apply_id"];
	                $arr_eval_approval_1 = $this->get_eval_approval_1($short_wkfw_name, $recognize_schedule["apply_content"]);
	                break;
				// 2012/01/17 Yamagawa del(s)
	            //}
				// 2012/01/17 Yamagawa del(e)
            }
            else
            {
                if($application_book_apply_id == $recognize_schedule["apply_id"])
                {
                    $recognize_schedule_apply_id = $recognize_schedule["apply_id"];
	                $arr_eval_approval_1 = $this->get_eval_approval_1($short_wkfw_name, $recognize_schedule["apply_content"]);
                }
            }
        }

        if($recognize_schedule_apply_id != "")
        {
            $idx = 1;
            foreach($arr_eval_approval_1 as $eval_approval_1)
            {
                // 非同期設定にする
                $arr_apv[] = array(
                                   "apv_order" => 1,
                                   "target_class_div" => "0",
                                   "multi_apv_flg" => "t",
                                   "next_notice_div" => "1",
                                   "apv_div0_flg" => "f",
                                   "apv_div1_flg" => "t",
                                   "apv_div2_flg" => "f",
                                   "apv_div3_flg" => "f",
                                   "apv_div4_flg" => "f",
                                   "apv_num" => "1",
                                   "apv_setting_flg"  => "f",
                                   // 2011/12/1 Yamagawa upd(s)
                                   //"apv_sub_order" => ($short_wkfw_name == "c201" || $short_wkfw_name == "c209") ? $idx : "",
                                   "apv_sub_order" => (in_array($shrot_wkfw_name, array("c201", "c209", "g201", "g203", "g212", "g223", "g233"))) ? $idx : "",
                                   // 2011/12/1 Yamagawa upd(e)
                                   "emp_infos" => array($eval_approval_1)
                                   );
                $idx++;
            }
	        // ２階層目の承認者取得
			// ただし、評価申請のレベルI　No5,No6,No8,No9,No12、レベルII　No5,No7,No8,No9,No12の場合は２階層目を設定しない 20090430
//	        $arr_eval_approval_2 = $this->get_eval_approval_2($emp_id);
			$arr_eval_approval_2 = array();
			$arr_applyapv = $this->get_applyapv_per_hierarchy($recognize_schedule_apply_id, "1");
			foreach ($arr_applyapv as $applyapv) {
				$arr_empmst_detail = $this->get_empmst_detail($applyapv["emp_id"]);
				$arr_eval_approval_2[] = array(
					"emp_id" => $applyapv["emp_id"],
					"emp_full_nm" => $arr_empmst_detail[0]["emp_lt_nm"] . " " . $arr_empmst_detail[0]["emp_ft_nm"],
					"st_nm" => $arr_empmst_detail[0]["st_nm"],
					"apv_div" => "1",
				);
			}

			if (in_array($short_wkfw_name, array("c203", "c204", "c205", "c206", "c208", "c211", "c212", "c213", "c214", "c216"))) {
				$arr_wk = $arr_eval_approval_2;
				$arr_eval_approval_2 = array();
				//評価申請で階層１と同じ師長の職員IDなら設定しない
		        foreach($arr_wk as $eval_approval_2)
		        {
					if ($arr_eval_approval_1[0]["emp_id"] == $eval_approval_2["emp_id"]) {
						continue;
					}
					$arr_eval_approval_2[] = $eval_approval_2;
				}
			}
            $eval_approval_2_cnt = count($arr_eval_approval_2);
	        $idx = 1;
 	        foreach($arr_eval_approval_2 as $eval_approval_2)
	        {
                // 非同期設定にする
	            $arr_apv[] = array(
			                        "apv_order" => (count($arr_eval_approval_1) == 0) ? "1" : "2",
			                        "target_class_div" => "0",
			                        "multi_apv_flg" => "t",
			                        "next_notice_div" => "1",
			                        "apv_div0_flg" => "f",
			                        "apv_div1_flg" => "t",
			                        "apv_div2_flg" => "f",
			                        "apv_div3_flg" => "f",
			                        "apv_div4_flg" => "f",
			                        "apv_num" => "1",
			                        "apv_setting_flg"  => "f",
			                        "apv_sub_order" => ($eval_approval_2_cnt > 1) ? $idx : "",
			                        "emp_infos" => array($eval_approval_2)
	                               );
	             $idx++;
	        }
        }
         return $arr_apv;
    }

    // １階層目の承認者取得
    function get_eval_approval_1($short_wkfw_name, $apply_content)
    {
        $emp_infos = array();
        $arr_grader = $this->get_apply_grader($apply_content);

        // ナーシングプロセス評価１
		// 2011/12/1 Yamagawa upd(s)
        //if($short_wkfw_name == "c201" || $short_wkfw_name == "c209")
		if (in_array($short_wkfw_name, array("c201", "c209", "g201", "g212", "g223", "g233")))
		// 2011/12/1 Yamagawa upd(e)
        {
            for($i=1; $i<=2; $i++)
            {
	            $grader_name   = $arr_grader[0]["grader_name_".$i];
    	        $grader_emp_id = $arr_grader[0]["grader_emp_id_".$i];
            	$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

                $emp_infos[] = array(
                                     "emp_id" => $grader_emp_id,
                                     "emp_full_nm" => $grader_name,
                                     "st_nm" => $arr_empmst_detail[0]["st_nm"],
                                     "apv_div" => "1",
                                    );
            }
        }
        // ナーシングプロセス評価２
		// 2011/12/1 Yamagawa upd(s)
        //else if($short_wkfw_name == "c202" || $short_wkfw_name == "c210")
		else if(in_array($short_wkfw_name, array("c202", "c210", "g202", "g213", "g224", "g234")))
		// 2011/12/1 Yamagawa upd(e)
        {
            $grader_name   = $arr_grader[0]["grader_name_3"];
   	        $grader_emp_id = $arr_grader[0]["grader_emp_id_3"];
           	$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

            $emp_infos[] = array(
                                 "emp_id" => $grader_emp_id,
                                 "emp_full_nm" => $grader_name,
                                 "st_nm" => $arr_empmst_detail[0]["st_nm"],
                                 "apv_div" => "1",
                                 );

        }
        // 教育/自己学習能力
		// 2011/12/1 Yamagawa upd(s)
        //else if($short_wkfw_name == "c203" || $short_wkfw_name == "c204" || $short_wkfw_name == "c205" || $short_wkfw_name == "c206" ||
        //         $short_wkfw_name == "c211" || $short_wkfw_name == "c212" || $short_wkfw_name == "c213" || $short_wkfw_name == "c214")
		else if(in_array($short_wkfw_name, array("c203", "c204", "c205", "c206", "c211", "c212", "c213", "c214"
												, "g204", "g205", "g206", "g207", "g215", "g216", "g217", "g218"
												, "g225", "g226", "g227", "g228", "g236", "g237", "g238", "g239")))
		// 2011/12/1 Yamagawa upd(e)
        {
            $grader_name   = $arr_grader[0]["grader_name_4"];
   	        $grader_emp_id = $arr_grader[0]["grader_emp_id_4"];
           	$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

            $emp_infos[] = array(
                                 "emp_id" => $grader_emp_id,
                                 "emp_full_nm" => $grader_name,
                                 "st_nm" => $arr_empmst_detail[0]["st_nm"],
                                 "apv_div" => "1",
                                 );

        }
        // リーダーシップ能力
		// 2011/12/1 Yamagawa upd(s)
        //else if($short_wkfw_name == "c207" || $short_wkfw_name == "c215")
		else if(in_array($short_wkfw_name, array("c207", "c215", "g208", "g219", "g229", "g240")))
		// 2011/12/1 Yamagawa upd(e)
        {
            $grader_name   = $arr_grader[0]["grader_name_5"];
   	        $grader_emp_id = $arr_grader[0]["grader_emp_id_5"];
           	$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

            $emp_infos[] = array(
                                 "emp_id" => $grader_emp_id,
                                 "emp_full_nm" => $grader_name,
                                 "st_nm" => $arr_empmst_detail[0]["st_nm"],
                                 "apv_div" => "1",
                                 );

        }
        // 経験年数/経験領域
		// 2011/12/1 Yamagawa upd(s)
        //else if($short_wkfw_name == "c208" || $short_wkfw_name == "c216")
		else if(in_array($short_wkfw_name, array("c208", "c216", "g210", "g221", "g231", "g242")))
		// 2011/12/1 Yamagawa upd(e)
        {
            $grader_name   = $arr_grader[0]["grader_name_6"];
   	        $grader_emp_id = $arr_grader[0]["grader_emp_id_6"];
           	$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

            $emp_infos[] = array(
                                 "emp_id" => $grader_emp_id,
                                 "emp_full_nm" => $grader_name,
                                 "st_nm" => $arr_empmst_detail[0]["st_nm"],
                                 "apv_div" => "1",
                                 );
        }
		// 2011/12/1 Yamagawa add(s)
		// ナーシングプロセス評価3（外来II）
		else if($short_wkfw_name == "g203")
		{
			for($i=1; $i<=2; $i++)
			{
				$grader_name   = $arr_grader[0]["grader_name_3_".$i];
				$grader_emp_id = $arr_grader[0]["grader_emp_id_3_".$i];
				$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

				$emp_infos[] = array(
									"emp_id" => $grader_emp_id,
									"emp_full_nm" => $grader_name,
									"st_nm" => $arr_empmst_detail[0]["st_nm"],
									"apv_div" => "1",
									);
			}
		}
		// ナーシングプロセス評価3（外来II以外）
		else if(in_array($short_wkfw_name, array("g214", "g235")))
		{
			$grader_name   = $arr_grader[0]["grader_name_3_1"];
			$grader_emp_id = $arr_grader[0]["grader_emp_id_3_1"];
			$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

			$emp_infos[] = array(
								"emp_id" => $grader_emp_id,
								"emp_full_nm" => $grader_name,
								"st_nm" => $arr_empmst_detail[0]["st_nm"],
								"apv_div" => "1",
								);
		}
		// リーダーシップ能力評価2
		else if(in_array($short_wkfw_name, array("g209", "g220", "g230", "g241")))
		{
			$grader_name   = $arr_grader[0]["grader_name_5_2"];
			$grader_emp_id = $arr_grader[0]["grader_emp_id_5_2"];
			$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

			$emp_infos[] = array(
								"emp_id" => $grader_emp_id,
								"emp_full_nm" => $grader_name,
								"st_nm" => $arr_empmst_detail[0]["st_nm"],
								"apv_div" => "1",
								);
		}
		// 経験年数/経験領域評価2
		else if(in_array($short_wkfw_name, array("g211", "g222", "g232", "g243")))
		{
			$grader_name   = $arr_grader[0]["grader_name_6_2"];
			$grader_emp_id = $arr_grader[0]["grader_emp_id_6_2"];
			$arr_empmst_detail = $this->get_empmst_detail($grader_emp_id);

			$emp_infos[] = array(
								"emp_id" => $grader_emp_id,
								"emp_full_nm" => $grader_name,
								"st_nm" => $arr_empmst_detail[0]["st_nm"],
								"apv_div" => "1",
								);
		}
		// 2011/12/1 Yamagawa add(e)

        return $emp_infos;
    }

    // ２階層目の承認者取得
    function get_eval_approval_2($emp_id)
    {
        $emp_infos = array();

        $arr_emp = $this->get_empmst_detail($emp_id);
        $emp_room = $arr_emp[0]["emp_room"];

        $hierarchy_div = "3";
        if($emp_room != "")
        {
            $hierarchy_div = "4";
        }

        $arr_nursing_commander = $this->get_eval_approval_list($emp_id, $hierarchy_div, "3");
        foreach($arr_nursing_commander as $nursing_commander)
        {
            $emp_infos[] = array(
                                 "emp_id" => $nursing_commander["emp_id"],
                                 "emp_full_nm" => $nursing_commander["emp_lt_nm"]." ".$nursing_commander["emp_ft_nm"],
                                 "st_nm" => $nursing_commander["st_nm"],
                                 "apv_div" => "1",
                                 );
        }

        return $emp_infos;
    }

    // ログインユーザと同階層の「師長」またはそれ以外の職員を取得
    function get_eval_approval_list($emp_id, $hierarchy_div, $eval_approval_div, $srch_emp_nm = "")
    {
        $sql = $this->get_sql_eval_approval_list($emp_id, $hierarchy_div, $eval_approval_div, $srch_emp_nm);
        $cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        return pg_fetch_all($sel);
    }

    // ログインユーザと同階層の「師長」またはそれ以外の職員を取得ＳＱＬ
    function get_sql_eval_approval_list($emp_id, $hierarchy_div, $eval_approval_div, $srch_emp_nm = "")
    {
		$srch_emp_nm = mb_ereg_replace("[ 　]", "", $srch_emp_nm);

		$arr_emp = $this->get_empmst_detail($emp_id);
		$emp_class = $arr_emp[0]["emp_class"];
		$emp_attribute = $arr_emp[0]["emp_attribute"];
		$emp_dept = $arr_emp[0]["emp_dept"];
		$emp_room = $arr_emp[0]["emp_room"];

		$sql = "select empmst.*, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm, classroom.room_nm, stmst.st_nm from empmst ";
		$sql .= "left join classmst on classmst.class_id = empmst.emp_class ";
		$sql .= "left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute ";
		$sql .= "left join deptmst on deptmst.dept_id = empmst.emp_dept ";
		$sql .= "left join classroom on classroom.room_id = empmst.emp_room ";
		$sql .= "left join stmst on stmst.st_id = empmst.emp_st ";
		$sql .= "where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";
		$sql .= "and ( ";
			if ($srch_emp_nm == "") {
				$sql .= "(empmst.emp_class = $emp_class and empmst.emp_attribute = $emp_attribute and empmst.emp_dept = $emp_dept ";
				if ($hierarchy_div == "4") {
					$sql .= "and empmst.emp_room = $emp_room ";
				}
				if ($eval_approval_div == "3") {
					$sql .= "and stmst.st_nm like '%師長%' ";
				} else if ($eval_approval_div == "1") {
					$sql .= "and stmst.st_nm not like '%師長%' ";
				}
				$sql .= ") ";
				$sql .= " or ";
				$sql .= "(exists (select * from concurrent inner join stmst st on st.st_id = concurrent.emp_st where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = $emp_class and concurrent.emp_attribute = $emp_attribute and concurrent.emp_dept = $emp_dept ";
				if ($hierarchy_div == "4") {
					$sql .= "and concurrent.emp_room = $emp_room ";
				}
				if ($eval_approval_div == "3") {
					$sql .= "and st.st_nm like '%師長%' ";
				} else if ($eval_approval_div == "1") {
					$sql .= "and st.st_nm not like '%師長%' ";
				}
				$sql .= ")) ";
			} else {
				$sql .= "(empmst.emp_lt_nm || empmst.emp_ft_nm like '%$srch_emp_nm%') or (empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like '%$srch_emp_nm%')";
			}
		$sql .= ") ";
		$sql .= "order by empmst.emp_id desc ";

		return $sql;
	}

//-------------------------------------------------------------------------------------------------------------------------
// キャリア申告
//-------------------------------------------------------------------------------------------------------------------------
    // キャリア申告登録
    function regist_career_apply($apply_id)
    {
        $arr_apply_wkfwmst = $this->get_apply_wkfwmst($apply_id);
        $apply_content = $arr_apply_wkfwmst[0]["apply_content"];
        $emp_id = $arr_apply_wkfwmst[0]["emp_id"];
        $apply_date = $arr_apply_wkfwmst[0]["apply_date"];

        $arr_career_apply = $this->get_career_apply($apply_content);

        foreach($arr_career_apply as $career_apply)
        {
            $ymd = $career_apply["year"].$career_apply["month"].$career_apply["day"];
            $level = $career_apply["level"];
            $level_div = $career_apply["level_div"];
            if(strlen($ymd) == 8)
            {
                // 存在チェック
                $recognize_level_cnt = $this->get_cl_career_history_recognize_level_cnt($emp_id, $level, $level_div);
                // キャリア履歴登録
                if($recognize_level_cnt == 0)
                {
                    $entry_date = "";
                    if($level == "1")
                    {
                        $entry_date = $ymd;
                    }
                    $arr = array(
                                  "apply_id" => $apply_id,
                                  "emp_id" => $emp_id,
                                  "apply_level" => $level,
                                  "apply_date" => "",
                                  "recognize_level" => $level,
                                  "recognize_level_div" => $level_div,
                                  "recognize_date" => $ymd,
                                  "recognize_flg" => "t",
                                  "entry_date" => $entry_date,
                                  "career_apply_flg" => "t",
                                  "disp_flg" => "t",
                                  // 2011/12/2 Yamagawa add(s)
                                  "nurse_type" => "1"
                                  // 2011/12/2 Yamagawa add(e)
                                 );
                    $this->regist_cl_career_history($arr);
                }
            }
        }
    }

    function get_cl_career_history_recognize_level_cnt($emp_id, $level, $level_div)
    {
		$sql  = "select count(*) as cnt from cl_career_history";
		$cond = "where emp_id = '$emp_id' and recognize_level = $level  and recognize_level_div = '$level_div' and recognize_flg";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "cnt");
    }

//-------------------------------------------------------------------------------------------------------------------------
// 本人ＣＡ履歴
//-------------------------------------------------------------------------------------------------------------------------
    // ＣＡ履歴検索(本人)
    function get_cl_career_history_for_personal($emp_id, $level)
    {
		$sql   = "select a.* from cl_career_history a ";
		$cond  = "where a.emp_id = '$emp_id' and a.recognize_level = $level and a.disp_flg ";
		$cond .= "and exists(select * from cl_apply b where b.apply_id = a.apply_id and not b.delete_flg) ";
        $cond .= "order by a.apply_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"             => $row["apply_id"],
	                        "emp_id"               => $row["emp_id"],
	                        "apply_level"          => $row["apply_level"],
	                        "apply_date"           => $row["apply_date"],
	                        "recognize_level"      => $row["recognize_level"],
	                        "recognize_level_div"  => $row["recognize_level_div"],
	                        "recognize_date"       => $row["recognize_date"],
	                        "recognize_flg"        => $row["recognize_flg"],
	                        "entry_date"           => $row["entry_date"],
	                        "career_apply_flg"     => $row["career_apply_flg"],
	                        "disp_flg"             => $row["disp_flg"]
	                       );

		}
		return $arr;
	}

	// 研修履歴検索(本人)
	function get_cl_cl_training_history($emp_id) {
		$sql = "select w.wkfw_title, h.start_date, h.end_date, h.course_days, h.course_times, h.training_theme from cl_training_history h inner join cl_apply a on h.apply_id = a.apply_id inner join cl_wkfwmst w on a.wkfw_id = w.wkfw_id";
		$cond = "where h.emp_id = '$emp_id' and (not a.delete_flg) order by h.apply_id";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = pg_fetch_array($sel)) {
			$arr[] = array(
				"wkfw_title"     => $row["wkfw_title"],
				"start_date"     => $row["start_date"],
				"end_date"       => $row["end_date"],
				"course_days"    => $row["course_days"],
				"course_times"   => $row["course_times"],
				"training_theme" => $row["training_theme"]
			);
		}
		return $arr;
    }

//-------------------------------------------------------------------------------------------------------------------------
// ＣＡ履歴検索
//-------------------------------------------------------------------------------------------------------------------------
	// ＣＡ履歴検索情報一覧件数取得(ＣＡ履歴検索画面用)
	function get_cl_career_history_list_count($arr_condition)
	{
		$level        = $arr_condition["level"];
		$emp_nm       = $arr_condition["emp_nm"];
		$class        = $arr_condition["class"];
		$attribute    = $arr_condition["attribute"];
		$dept         = $arr_condition["dept"];
		$room         = $arr_condition["room"];

		// 師長の場合、ログインユーザの職員IDが渡ってくる
		$login_emp_id = $arr_condition["login_emp_id"];

		$sql   = "select distinct b.emp_id from empmst b ";
		$sql  .= "left join (select * from cl_career_history where disp_flg) a on b.emp_id = a.emp_id ";
        $sql  .= "left join (select * from cl_apply where not delete_flg) c on a.apply_id = c.apply_id ";
		$cond  = "where exists (select * from authmst auth where auth.emp_id = b.emp_id and (not auth.emp_del_flg))";
		if ($level != "-") {
			$cond .= " and a.recognize_level = $level ";
		}

		// 志願者名
		if($emp_nm != "")
		{
			$emp_nm = pg_escape_string($emp_nm);
			$cond .= "and ";
			$cond .= "(b.emp_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_ft_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_ft_nm like '%$emp_nm%' ";
			$cond .= "or (b.emp_lt_nm || b.emp_ft_nm) like '%$emp_nm%' ";
			$cond .= "or (b.emp_kn_lt_nm || b.emp_kn_ft_nm) like '%$emp_nm%') ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and b.emp_class = $class ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or (c.emp_class is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = b.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = b.emp_class and s.st_nm like '%師長%')))) ";
		}

		if ($attribute != "") {
			$cond .= "and b.emp_attribute = $attribute ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or (c.emp_attribute is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = b.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = b.emp_attribute and s.st_nm like '%師長%')))) ";
		}

		if ($dept != "") {
			$cond .= "and b.emp_dept = $dept ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or (c.emp_dept is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = b.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = b.emp_dept and s.st_nm like '%師長%')))) ";
		}

		if ($room != "") {
			$cond .= "and b.emp_room = $room ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and c.emp_room is null) or l.emp_room = c.emp_room) and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and c.emp_room is null) or l.emp_room = c.emp_room) and s.st_nm like '%師長%') or (c.emp_room is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = b.emp_room and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and b.emp_room is null) or l.emp_room = b.emp_room) and s.st_nm like '%師長%')))) ";
		}

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_num_rows($sel);
	}

	// ＣＡ履歴検索情報取得(ＣＡ履歴検索画面用)
	function get_cl_career_history_list($arr_condition)
	{
		$level        = $arr_condition["level"];
		$emp_nm       = $arr_condition["emp_nm"];
		$class        = $arr_condition["class"];
		$attribute    = $arr_condition["attribute"];
		$dept         = $arr_condition["dept"];
		$room         = $arr_condition["room"];
		$page         = $arr_condition["page"];
		$max_page     = $arr_condition["max_page"];

		// 師長の場合、ログインユーザの職員IDが渡ってくる
		$login_emp_id = $arr_condition["login_emp_id"];

		$page = ($page - 1) * 15;

		$sql   = "select b.emp_id, b.emp_lt_nm, b.emp_ft_nm, ec.class_nm, ea.atrb_nm, ed.dept_nm, er.room_nm from empmst b ";
		$sql  .= "inner join classmst ec on b.emp_class = ec.class_id ";
		$sql  .= "inner join atrbmst ea on b.emp_attribute = ea.atrb_id ";
		$sql  .= "inner join deptmst ed on b.emp_dept = ed.dept_id ";
		$sql  .= "left join classroom er on b.emp_room = er.room_id ";
		$sql  .= "left join (select * from cl_career_history where disp_flg) a on b.emp_id = a.emp_id ";
        $sql  .= "left join (select * from cl_apply where not delete_flg) c on a.apply_id = c.apply_id ";
		$cond  = "where exists (select * from authmst auth where auth.emp_id = b.emp_id and (not auth.emp_del_flg)) ";
		if ($level != "-") {
			$cond .= " and a.recognize_level = $level ";
		}

		// 志願者名
		if($emp_nm != "")
		{
			$emp_nm = pg_escape_string($emp_nm);
			$cond .= "and ";
			$cond .= "(b.emp_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_ft_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_lt_nm like '%$emp_nm%' ";
			$cond .= "or b.emp_kn_ft_nm like '%$emp_nm%' ";
			$cond .= "or (b.emp_lt_nm || b.emp_ft_nm) like '%$emp_nm%' ";
			$cond .= "or (b.emp_kn_lt_nm || b.emp_kn_ft_nm) like '%$emp_nm%') ";
		}

		// 部署
		if ($class != "") {
			$cond .= "and b.emp_class = $class ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = c.emp_class and s.st_nm like '%師長%') or (c.emp_class is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = b.emp_class and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_class = b.emp_class and s.st_nm like '%師長%')))) ";
		}

		if ($attribute != "") {
			$cond .= "and b.emp_attribute = $attribute ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = c.emp_attribute and s.st_nm like '%師長%') or (c.emp_attribute is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = b.emp_attribute and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_attribute = b.emp_attribute and s.st_nm like '%師長%')))) ";
		}

		if ($dept != "") {
			$cond .= "and b.emp_dept = $dept ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = c.emp_dept and s.st_nm like '%師長%') or (c.emp_dept is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = b.emp_dept and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_dept = b.emp_dept and s.st_nm like '%師長%')))) ";
		}

		if ($room != "") {
			$cond .= "and b.emp_room = $room ";
		} else if ($login_emp_id != "") {
			$cond .= "and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and c.emp_room is null) or l.emp_room = c.emp_room) and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and c.emp_room is null) or l.emp_room = c.emp_room) and s.st_nm like '%師長%') or (c.emp_room is null and (exists (select * from empmst l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and l.emp_room = b.emp_room and s.st_nm like '%師長%') or exists (select * from concurrent l inner join stmst s on s.st_id = l.emp_st where l.emp_id = '$login_emp_id' and ((l.emp_room is null and b.emp_room is null) or l.emp_room = b.emp_room) and s.st_nm like '%師長%')))) ";
		}

		$cond .= "group by b.emp_id, b.emp_lt_nm, b.emp_ft_nm, ec.class_nm, ea.atrb_nm, ed.dept_nm, er.room_nm ";
		$cond .= "order by b.emp_id asc ";
		$cond .= "offset $page limit $max_page ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while ($row = pg_fetch_array($sel)) {
			$arr[] = array(
				"emp_id"       => $row["emp_id"],
				"emp_lt_nm"    => $row["emp_lt_nm"],
				"emp_ft_nm"    => $row["emp_ft_nm"],
				"class_nm"     => $row["class_nm"],
				"atrb_nm"      => $row["atrb_nm"],
				"dept_nm"      => $row["dept_nm"],
				"room_nm"      => $row["room_nm"],
				"emp_ft_nm"    => $row["emp_ft_nm"]
			);
		}
		return $arr;
	}


	// ＣＡ履歴検索情報取得(ＣＡ履歴検索画面用)
	function get_cl_career_history_detail($emp_id)
	{

		$sql   = "select a.* from cl_career_history a ";
		$cond  = "where emp_id = '$emp_id' and a.entry_date = '' and a.disp_flg ";
        $cond .= "order by a.apply_id asc ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
        $arr = array();
		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
	                        "apply_id"             => $row["apply_id"],
	                        "emp_id"               => $row["emp_id"],
	                        "apply_level"          => $row["apply_level"],
	                        "apply_date"           => $row["apply_date"],
	                        "recognize_level"      => $row["recognize_level"],
	                        "recognize_level_div"  => $row["recognize_level_div"],
	                        "recognize_date"       => $row["recognize_date"],
	                        "recognize_flg"        => $row["recognize_flg"],
	                        "entry_date"           => $row["entry_date"],
	                        "career_apply_flg"     => $row["career_apply_flg"],
	                        "disp_flg"             => $row["disp_flg"]
	                       );
		}
		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// テンプレートＸＭＬデータ関連
//-------------------------------------------------------------------------------------------------------------------------
	function get_apply_date($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$date_y = $xml_data_array["date_y1"];
		$date_m = $xml_data_array["date_m1"];
		$date_d = $xml_data_array["date_d1"];

		return $date_y."/".$date_m."/".$date_d;

	}

	function get_ward_eval_ym($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$eval_year = $xml_data_array["eval_year"];
		$eval_month = $xml_data_array["eval_month"];

		return array("eval_year" => $eval_year, "eval_month" => $eval_month);
	}

	function get_levelup_application_book_id($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$levelup_application_book_id = $xml_data_array["levelup_application_book_id"];

		return $levelup_application_book_id	;
	}

	function get_levelup_apply_emp_id($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$levelup_apply_emp_id = $xml_data_array["levelup_apply_emp_id"];

		return $levelup_apply_emp_id	;
	}

	function get_levelup_apply_date($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$levelup_apply_date = $xml_data_array["levelup_apply_date"];
        $levelup_apply_date = ereg_replace("/", "", $levelup_apply_date);

		return $levelup_apply_date;
	}

	// 評価申請No.12
	function get_apply_eval_no12($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);
		$chk1    = $xml_data_array["chk1"];
		$date_y1 = $xml_data_array["date_y1"];
		$date_m1 = $xml_data_array["date_m1"];
		$date_d1 = $xml_data_array["date_d1"];
		$date_y2 = $xml_data_array["date_y2"];
		$date_m2 = $xml_data_array["date_m2"];
		$date_d2 = $xml_data_array["date_d2"];

	    $arr[] = array(
                         "chk1"    => $chk1,
 	                     "date_y1" => $date_y1,
	                     "date_m1" => $date_m1,
	                     "date_d1" => $date_d1,
	                     "date_y2" => $date_y2,
	                     "date_m2" => $date_m2,
	                     "date_d2" => $date_d2
	                   );

		return $arr;
	}

// 2011/11/24 Yamagawa add(s)
	// 評価申請No.33
	function get_apply_eval_no33($apply_content)
	{
		// 一時的にinclude_pathを変更
		$old_inc_dir = ini_get('include_path');
		$add_inc_dir = dirname(__FILE__) . "/PEAR";
		$inc_separate = ":";

		ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

		// XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
		require_once("Unserializer.php");

		$Unserializer =&new XML_Unserializer();
		if (PHP_VERSION >= '5') {
			$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
		}
		$Unserializer->unserialize($apply_content);
		$xml_data_array = $Unserializer->getUnserializedData();
		if (PHP_VERSION >= '5') {
			mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
		}

		// include_pathを元に戻す
		ini_set('include_path', $old_inc_dir);
		$departments_name1 = $xml_data_array["departments_name1"];
		$departments_name2 = $xml_data_array["departments_name2"];
		$departments_name3 = $xml_data_array["departments_name3"];
		$departments_name4 = $xml_data_array["departments_name4"];
		$departments_name5 = $xml_data_array["departments_name5"];

		$arr[] = array(
						"departments_name1" => $departments_name1,
						"departments_name2" => $departments_name2,
						"departments_name3" => $departments_name3,
						"departments_name4" => $departments_name4,
						"departments_name5" => $departments_name5
					  );

		return $arr;
	}
// 2011/11/24 Yamagawa add(e)

	// 評価者取得
	function get_apply_grader($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$grader_name_1 = $xml_data_array["grader_name_1"];
		$grader_emp_id_1 = $xml_data_array["grader_emp_id_1"];
		$grader_name_2 = $xml_data_array["grader_name_2"];
		$grader_emp_id_2 = $xml_data_array["grader_emp_id_2"];
		$grader_name_3 = $xml_data_array["grader_name_3"];
		$grader_emp_id_3 = $xml_data_array["grader_emp_id_3"];
		$grader_name_4 = $xml_data_array["grader_name_4"];
		$grader_emp_id_4 = $xml_data_array["grader_emp_id_4"];
		$grader_name_5 = $xml_data_array["grader_name_5"];
		$grader_emp_id_5 = $xml_data_array["grader_emp_id_5"];
		$grader_name_6 = $xml_data_array["grader_name_6"];
		$grader_emp_id_6 = $xml_data_array["grader_emp_id_6"];
		// 2011/12/2 Yamagawa add(s)
		$grader_name_3_1 = $xml_data_array["grader_name_3_1"];
		$grader_emp_id_3_1 = $xml_data_array["grader_emp_id_3_1"];
		$grader_name_3_2 = $xml_data_array["grader_name_3_2"];
		$grader_emp_id_3_2 = $xml_data_array["grader_emp_id_3_2"];
		$grader_name_5_2 = $xml_data_array["grader_name_5_2"];
		$grader_emp_id_5_2 = $xml_data_array["grader_emp_id_5_2"];
		$grader_name_6_2 = $xml_data_array["grader_name_6_2"];
		$grader_emp_id_6_2 = $xml_data_array["grader_emp_id_6_2"];
		// 2011/12/2 Yamagawa add(e)

	    $arr[] = array(
	                     "grader_name_1" => $grader_name_1,
	                     "grader_emp_id_1" => $grader_emp_id_1,
	                     "grader_name_2" => $grader_name_2,
	                     "grader_emp_id_2" => $grader_emp_id_2,
	                     "grader_name_3" => $grader_name_3,
	                     "grader_emp_id_3" => $grader_emp_id_3,
	                     "grader_name_4" => $grader_name_4,
	                     "grader_emp_id_4" => $grader_emp_id_4,
	                     "grader_name_5" => $grader_name_5,
	                     "grader_emp_id_5" => $grader_emp_id_5,
	                     "grader_name_6" => $grader_name_6,
	                     "grader_emp_id_6" => $grader_emp_id_6,
	                     // 2011/12/2 Yamagawa add(s)
	                     "grader_name_3_1" => $grader_name_3_1,
	                     "grader_emp_id_3_1" => $grader_emp_id_3_1,
	                     "grader_name_3_2" => $grader_name_3_2,
	                     "grader_emp_id_3_2" => $grader_emp_id_3_2,
	                     "grader_name_5_2" => $grader_name_5_2,
	                     "grader_emp_id_5_2" => $grader_emp_id_5_2,
	                     "grader_name_6_2" => $grader_name_6_2,
	                     "grader_emp_id_6_2" => $grader_emp_id_6_2
	                     // 2011/12/2 Yamagawa add(e)
	                   );


		return $arr;
	}


	// 評価者スコアー取得
	function get_grader_score($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$score = $xml_data_array["score"];

		return $score;
	}

	// スコアデータ取得
	// 2011/12/1 Yamagawa upd(s)
	//function get_score_data($apply_content, $level) {
	function get_score_data($apply_content, $level, $nurse_type) {
	// 2011/12/1 Yamagawa upd(e)

		// 一時的にinclude_pathを変更
		$old_inc_dir = ini_get('include_path');
		$add_inc_dir = dirname(__FILE__) . "/PEAR";
		$inc_separate = ":";

		ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

		// XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
		require_once("Unserializer.php");

		$Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
		$Unserializer->unserialize($apply_content);
		$xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

		// include_pathを元に戻す
		ini_set('include_path', $old_inc_dir);

		$item_score_No1  = ($xml_data_array["item_score_No1"] == "") ? 0 : intval($xml_data_array["item_score_No1"]);
		$item_score_No2  = ($xml_data_array["item_score_No2"] == "") ? 0 : intval($xml_data_array["item_score_No2"]);
		$item_score_No3  = ($xml_data_array["item_score_No3"] == "") ? 0 : intval($xml_data_array["item_score_No3"]);
		$item_score_No4  = ($xml_data_array["item_score_No4"] == "") ? 0 : intval($xml_data_array["item_score_No4"]);
		$item_score_No5  = ($xml_data_array["item_score_No5"] == "") ? 0 : intval($xml_data_array["item_score_No5"]);
		$item_score_No6  = ($xml_data_array["item_score_No6"] == "") ? 0 : intval($xml_data_array["item_score_No6"]);
		$item_score_No7  = ($xml_data_array["item_score_No7"] == "") ? 0 : intval($xml_data_array["item_score_No7"]);
		$item_score_No8  = ($xml_data_array["item_score_No8"] == "") ? 0 : intval($xml_data_array["item_score_No8"]);
		$item_score_No9  = ($xml_data_array["item_score_No9"] == "") ? 0 : intval($xml_data_array["item_score_No9"]);
		$item_score_No10 = ($xml_data_array["item_score_No10"] == "") ? 0 : intval($xml_data_array["item_score_No10"]);
		$item_score_No11 = ($xml_data_array["item_score_No11"] == "") ? 0 : intval($xml_data_array["item_score_No11"]);
		$item_score_No12 = ($xml_data_array["item_score_No12"] == "") ? 0 : intval($xml_data_array["item_score_No12"]);

		if ($level == "2") {
			// 2011/12/1 Yamagawa add(s)
			if ($nurse_type == "1") {
			// 2011/12/1 Yamagawa add(e)
				return array("スコア",
				             round($item_score_No1 / 20 * 100),
				             round($item_score_No3 / 30 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 12 * 100),
				             round(($item_score_No6 + $item_score_No9) / 13 * 100),
				             round($item_score_No10 / 20 * 100)
				            );
			// 2011/12/1 Yamagawa add(s)
			} else if ($nurse_type == "2") {
				return array("スコア",
				             round($item_score_No1 / 20 * 100),
				             round($item_score_No3 / 30 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 12 * 100),
				             round(($item_score_No6 + $item_score_No9) / 9 * 100),
				             round($item_score_No10 / 20 * 100)
				            );
			} else if ($nurse_type == "3") {
				return array("スコア",
				             round($item_score_No1 / 20 * 100),
				             round($item_score_No3 / 30 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 12 * 100),
				             round(($item_score_No6 + $item_score_No9) / 6 * 100),
				             round($item_score_No10 / 20 * 100)
				            );
			}
			// 2011/12/1 Yamagawa add(e)
		} else if ($level == "3") {
			// 2011/12/1 Yamagawa add(s)
			if ($nurse_type == "1") {
			// 2011/12/1 Yamagawa add(e)
				return array("スコア",
				             round($item_score_No2 / 15 * 100),
				             round($item_score_No4 / 15 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 6 * 100),
				             round(($item_score_No7 + $item_score_No9) / 19 * 100),
				             round($item_score_No11 / 40 * 100)
				            );
			// 2011/12/1 Yamagawa add(s)
			} else if ($nurse_type == "2") {
				return array("スコア",
				             round($item_score_No2 / 15 * 100),
				             round($item_score_No4 / 15 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 9 * 100),
				             round(($item_score_No7 + $item_score_No9) / 19 * 100),
				             round($item_score_No11 / 40 * 100)
				            );
			} else if ($nurse_type == "3") {
				return array("スコア",
				             round($item_score_No1 / 20 * 100),
				             round($item_score_No3 / 15 * 100),
				             round($item_score_No5 / 5 * 100),
				             round($item_score_No8 / 6 * 100),
				             round(($item_score_No7 + $item_score_No9) / 19 * 100),
				             round($item_score_No11 / 40 * 100)
				            );
			}
			// 2011/12/1 Yamagawa add(e)
		}
	}

    // キャリア申告
	function get_career_apply($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$arr_suffix = array("2_a", "2_b", "2_c", "2_d", "2_e", "3_a", "3_b", "3_c", "3_d", "4_a", "4_b", "4_c");

        $arr = array();
        foreach ($arr_suffix as $tmp_suffix) {
		    $date_y = $xml_data_array["date_y$tmp_suffix"];
		    $date_m = $xml_data_array["date_m$tmp_suffix"];
		    $date_d = $xml_data_array["date_d$tmp_suffix"];

			list($tmp_level, $tmp_level_div) = split("_", $tmp_suffix);
			$tmp_level_div = strtoupper($tmp_level_div);
            $arr[] = array(
                          "level" => $tmp_level,
                          "level_div" => $tmp_level_div,
                          "year" => $date_y,
                          "month" => $date_m,
                          "day" => $date_d
                         );
        }
		return $arr;
	}

    // 研修申告
	function get_training_apply($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

	    $course_code = $xml_data_array["course_code"];
	    $course_theme = $xml_data_array["course_theme"];

	    $date_y2 = $xml_data_array["date_y2"];
	    $date_m2 = $xml_data_array["date_m2"];
	    $date_d2 = $xml_data_array["date_d2"];

        $start_date = $date_y2.$date_m2.$date_d2;
        if(strlen($start_date) != 8)
        {
            $start_date = "";
        }

	    $date_y3 = $xml_data_array["date_y3"];
	    $date_m3 = $xml_data_array["date_m3"];
	    $date_d3 = $xml_data_array["date_d3"];

        $end_date = $date_y3.$date_m3.$date_d3;
        if(strlen($end_date) != 8)
        {
            $end_date = "";
        }

		$course_days = $xml_data_array["course_days"];
		if (strlen($course_days) > 6) {
			$course_days = substr($course_days, 0, 6);
		}

		$course_times = $xml_data_array["course_times"];
		if (strlen($course_times) > 6) {
			$course_times = substr($course_times, 0, 6);
		}

        $arr = array(
                       "training_code" => $course_code,
                       "training_theme" => $course_theme,
                       "start_date" => $start_date,
                       "end_date" => $end_date,
                       "course_days" => $course_days,
                       "course_times" => $course_times
                     );
		return $arr;
	}

	// プリセプターチュータ評価者スコアー取得
	function get_preceptor_grader_score($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

		$score = $xml_data_array["total_score"];

		return $score;
	}


//-------------------------------------------------------------------------------------------------------------------------
// レベルアップ志願者選択
//-------------------------------------------------------------------------------------------------------------------------
	function get_levelup_application_emp_info($apply_id, $session) {
		// 担当師長(ログインユーザ情報取得)
		$arr_empmst = $this->get_empmst($session);
		$arr_levelup_info["head_nurse_emp_id"] = $arr_empmst[0]["emp_id"];
		$arr_levelup_info["head_nurse_name"] = $arr_empmst[0]["emp_lt_nm"]." ".$arr_empmst[0]["emp_ft_nm"];

		// レベルアップ情報取得
		$arr_levelup_apply = $this->get_levelup_apply($apply_id);

		if (count($arr_levelup_apply) > 0) {
			$arr_eval_apply_id = array();

			// レベルアップ志願書ID
			$arr_levelup_info["levelup_application_book_id"] = $arr_levelup_apply[0]["apply_id"];

			// レベルアップ志願書レベル
			$arr_levelup_info["levelup_application_book_level"] = $arr_levelup_apply[0]["level"];

			// 申請日
			$arr_levelup_info["levelup_apply_date"] = $this->get_apply_date($arr_levelup_apply[0]["apply_content"]);

		    // 氏名
			$arr_levelup_info["levelup_apply_emp_name"] = $arr_levelup_apply[0]["emp_lt_nm"]." ".$arr_levelup_apply[0]["emp_ft_nm"];
			$arr_levelup_info["levelup_apply_emp_id"] = $arr_levelup_apply[0]["emp_id"];

			// 所属
			$arr_levelup_info["levelup_apply_emp_class"] = $arr_levelup_apply[0]["class_nm"]." > ".$arr_levelup_apply[0]["atrb_nm"]." > ".$arr_levelup_apply[0]["dept_nm"];
			if ($arr_levelup_apply[0]["room_nm"] != "") {
				$arr_levelup_info["levelup_apply_emp_class"] .= " > ";
				$arr_levelup_info["levelup_apply_emp_class"] .= $arr_levelup_apply[0]["room_nm"];
			}

			/////////////////////////////////////////////////////////////////////////////
			//評価者別集計表
			/////////////////////////////////////////////////////////////////////////////
			$arr_apply_grader = $this->get_apply_grader($arr_levelup_apply[0]["apply_content"]);

			// 同僚評価者1 または 看護診断委員会委員長
			$arr_levelup_info["grader_emp_name_1"] = $arr_apply_grader[0]["grader_name_1"];
			$arr_levelup_info["grader_emp_id_1"] = $arr_apply_grader[0]["grader_emp_id_1"];
			// 同僚評価者2 または 看護診断委員会副委員長
			$arr_levelup_info["grader_emp_name_2"] = $arr_apply_grader[0]["grader_name_2"];
			$arr_levelup_info["grader_emp_id_2"] = $arr_apply_grader[0]["grader_emp_id_2"];
			// 上司評価者
			$arr_levelup_info["grader_emp_name_3"] = $arr_apply_grader[0]["grader_name_3"];
			$arr_levelup_info["grader_emp_id_3"] = $arr_apply_grader[0]["grader_emp_id_3"];
			// 当該師長
			$arr_levelup_info["grader_emp_name_4"] = $arr_apply_grader[0]["grader_name_4"];
			$arr_levelup_info["grader_emp_id_4"] = $arr_apply_grader[0]["grader_emp_id_4"];
			// 上司評価者
			$arr_levelup_info["grader_emp_name_5"] = $arr_apply_grader[0]["grader_name_5"];
			$arr_levelup_info["grader_emp_id_5"] = $arr_apply_grader[0]["grader_emp_id_5"];
			// 当該師長
			$arr_levelup_info["grader_emp_name_6"] = $arr_apply_grader[0]["grader_name_6"];
			$arr_levelup_info["grader_emp_id_6"] = $arr_apply_grader[0]["grader_emp_id_6"];
			// 2011/12/2 Yamagawa add(s)
			// 同僚評価者1
			$arr_levelup_info["grader_emp_name_3_1"] = $arr_apply_grader[0]["grader_name_3_1"];
			$arr_levelup_info["grader_emp_id_3_1"] = $arr_apply_grader[0]["grader_emp_id_3_1"];
			// 同僚評価者2
			$arr_levelup_info["grader_emp_name_3_2"] = $arr_apply_grader[0]["grader_name_3_2"];
			$arr_levelup_info["grader_emp_id_3_2"] = $arr_apply_grader[0]["grader_emp_id_3_2"];
			// 上司評価者
			$arr_levelup_info["grader_emp_name_5_2"] = $arr_apply_grader[0]["grader_name_5_2"];
			$arr_levelup_info["grader_emp_id_5_2"] = $arr_apply_grader[0]["grader_emp_id_5_2"];
			// 当該師長
			$arr_levelup_info["grader_emp_name_6_2"] = $arr_apply_grader[0]["grader_name_6_2"];
			$arr_levelup_info["grader_emp_id_6_2"] = $arr_apply_grader[0]["grader_emp_id_6_2"];
			// 2011/12/2 Yamagawa add(e)

			// 評価者別スコアー
			$arr_levelup_info["grader_score_No1_1"] = "";
			$arr_levelup_info["grader_score_No1_2"] = "";
			$arr_levelup_info["grader_score_No2_1"] = "";
			$arr_levelup_info["grader_score_No2_2"] = "";
			$arr_levelup_info["grader_score_No3"] = "";
			$arr_levelup_info["grader_score_No4"] = "";
			$arr_levelup_info["grader_score_No5"] = "";
			$arr_levelup_info["grader_score_No6"] = "";
			$arr_levelup_info["grader_score_No7"] = "";
			$arr_levelup_info["grader_score_No8"] = "";
			$arr_levelup_info["grader_score_No9"] = "";
			$arr_levelup_info["grader_score_No10"] = "";
			$arr_levelup_info["grader_score_No11"] = "";
			$arr_levelup_info["grader_score_No12"] = "";
			// 2011/12/2 Yamagawa add(s)
			$arr_levelup_info["grader_score_No31_1"] = "";
			$arr_levelup_info["grader_score_No31_2"] = "";
			$arr_levelup_info["grader_score_No32"] = "";
			$arr_levelup_info["grader_score_No33"] = "";
			// 2011/12/2 Yamagawa add(e)
			$arr_levelup_info["grader_apply_id_No1_1"] = "";
			$arr_levelup_info["grader_apply_id_No1_2"] = "";
			$arr_levelup_info["grader_apply_id_No2_1"] = "";
			$arr_levelup_info["grader_apply_id_No2_2"] = "";
			$arr_levelup_info["grader_apply_id_No3"] = "";
			$arr_levelup_info["grader_apply_id_No4"] = "";
			$arr_levelup_info["grader_apply_id_No5"] = "";
			$arr_levelup_info["grader_apply_id_No6"] = "";
			$arr_levelup_info["grader_apply_id_No7"] = "";
			$arr_levelup_info["grader_apply_id_No8"] = "";
			$arr_levelup_info["grader_apply_id_No9"] = "";
			$arr_levelup_info["grader_apply_id_No10"] = "";
			$arr_levelup_info["grader_apply_id_No11"] = "";
			$arr_levelup_info["grader_apply_id_No12"] = "";
			// 2011/12/2 Yamagawa add(s)
			$arr_levelup_info["grader_apply_id_No31_1"] = "";
			$arr_levelup_info["grader_apply_id_No31_2"] = "";
			$arr_levelup_info["grader_apply_id_No32"] = "";
			$arr_levelup_info["grader_apply_id_No33"] = "";
			// 2011/12/2 Yamagawa add(e)

			// ■ナーシングプロセス
			// No1評価申請
			if ($arr_apply_grader[0]["grader_emp_id_1"] != "") {
				$arr_no1 = $this->get_no_apply_id($arr_levelup_apply[0]["no1_apply_id"], "1", "1");
				foreach ($arr_no1 as $no1) {
					$tmp_no1_grader_score = $this->get_approve_score($no1, "1", "1");
					if (is_null($tmp_no1_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No1_1"] = $tmp_no1_grader_score;
					$arr_levelup_info["grader_apply_id_No1_1"] = $no1;
					if (!in_array($no1, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no1);
					}
					break;
				}
			}
			if ($arr_apply_grader[0]["grader_emp_id_2"] != "") {
				$arr_no1 = $this->get_no_apply_id($arr_levelup_apply[0]["no1_apply_id"], "1", "2");
				foreach ($arr_no1 as $no1) {
					$tmp_no1_grader_score = $this->get_approve_score($no1, "1", "2");
					if (is_null($tmp_no1_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No1_2"] = $tmp_no1_grader_score;
					$arr_levelup_info["grader_apply_id_No1_2"] = $no1;
					if (!in_array($no1, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no1);
					}
					break;
				}
			}

			// No2評価申請
			if ($arr_apply_grader[0]["grader_emp_id_1"] != "") {
				$arr_no2 = $this->get_no_apply_id($arr_levelup_apply[0]["no2_apply_id"], "1", "1");
				foreach ($arr_no2 as $no2) {
					$tmp_no2_grader_score = $this->get_approve_score($no2, "1", "1");
					if (is_null($tmp_no2_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No2_1"] = $tmp_no2_grader_score;
					$arr_levelup_info["grader_apply_id_No2_1"] = $no2;
					if (!in_array($no2, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no2);
					}
					break;
				}
			}
			if ($arr_apply_grader[0]["grader_emp_id_2"] != "") {
				$arr_no2 = $this->get_no_apply_id($arr_levelup_apply[0]["no2_apply_id"], "1", "2");
				foreach ($arr_no2 as $no2) {
					$tmp_no2_grader_score = $this->get_approve_score($no2, "1", "2");
					if (is_null($tmp_no2_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No2_2"] = $tmp_no2_grader_score;
					$arr_levelup_info["grader_apply_id_No2_2"] = $no2;
					if (!in_array($no2, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no2);
					}
					break;
				}
			}

			// No3評価申請
			if ($arr_apply_grader[0]["grader_emp_id_3"] != "") {
				$arr_no3 = $this->get_no_apply_id($arr_levelup_apply[0]["no3_apply_id"], "1", "");
				foreach ($arr_no3 as $no3) {
					$tmp_no3_grader_score = $this->get_approve_score($no3, "1", "");
					if (is_null($tmp_no3_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No3"] = $tmp_no3_grader_score;
					$arr_levelup_info["grader_apply_id_No3"] = $no3;
					array_push($arr_eval_apply_id, $no3);
					break;
				}
			}

			// No4評価申請
			if ($arr_apply_grader[0]["grader_emp_id_3"] != "") {
				$arr_no4 = $this->get_no_apply_id($arr_levelup_apply[0]["no4_apply_id"], "1", "");
				foreach ($arr_no4 as $no4) {
					$tmp_no4_grader_score = $this->get_approve_score($no4, "1", "");
					if (is_null($tmp_no4_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No4"] = $tmp_no4_grader_score;
					$arr_levelup_info["grader_apply_id_No4"] = $no4;
					array_push($arr_eval_apply_id, $no4);
					break;
				}
			}

			// 2011/12/2 Yamagawa add(s)
			// No31評価申請
			if ($arr_apply_grader[0]["grader_emp_id_3_1"] != "") {

				if ($arr_levelup_apply[0]["level"] == "2" && $arr_levelup_apply[0]["nurse_type"] == "2") {
					$apply_sub_order = "1";
				} else {
					$apply_sub_order = "";
				}

				$arr_no31 = $this->get_no_apply_id($arr_levelup_apply[0]["no31_apply_id"], "1", $apply_sub_order);
				foreach ($arr_no31 as $no31) {
					$tmp_no31_grader_score = $this->get_approve_score($no31, "1", $apply_sub_order);
					if (is_null($tmp_no31_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No31_1"] = $tmp_no31_grader_score;
					$arr_levelup_info["grader_apply_id_No31_1"] = $no31;
					if (!in_array($no31, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no31);
					}
					break;
				}

			}
			if ($arr_apply_grader[0]["grader_emp_id_3_2"] != "") {
				$arr_no31 = $this->get_no_apply_id($arr_levelup_apply[0]["no31_apply_id"], "1", "2");
				foreach ($arr_no31 as $no31) {
					$tmp_no31_grader_score = $this->get_approve_score($no31, "1", "2");
					if (is_null($tmp_no31_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No31_2"] = $tmp_no31_grader_score;
					$arr_levelup_info["grader_apply_id_No31_2"] = $no31;
					if (!in_array($no31, $arr_eval_apply_id)) {
						array_push($arr_eval_apply_id, $no31);
					}
					break;
				}
			}
			// 2011/12/2 Yamagawa add(e)

			// ■教育能力・自己学習能力
			// No5評価申請
			if ($arr_apply_grader[0]["grader_emp_id_4"] != "") {
				$arr_no5 = $this->get_no_apply_id($arr_levelup_apply[0]["no5_apply_id"], "1", "");
				foreach ($arr_no5 as $no5) {
					$tmp_no5_grader_score = $this->get_approve_score($no5, "1", "");
					if (is_null($tmp_no5_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No5"] = $tmp_no5_grader_score;
					$arr_levelup_info["grader_apply_id_No5"] = $no5;
					array_push($arr_eval_apply_id, $no5);
					break;
				}
			}

			// No6評価申請
			if ($arr_apply_grader[0]["grader_emp_id_4"] != "") {
				$arr_no6 = $this->get_no_apply_id($arr_levelup_apply[0]["no6_apply_id"], "1", "");
				foreach ($arr_no6 as $no6) {
					$tmp_no6_grader_score = $this->get_approve_score($no6, "1", "");
					if (is_null($tmp_no6_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No6"] = $tmp_no6_grader_score;
					$arr_levelup_info["grader_apply_id_No6"] = $no6;
					array_push($arr_eval_apply_id, $no6);
					break;
				}
			}

			// No7評価申請
			if ($arr_apply_grader[0]["grader_emp_id_4"] != "") {
				$arr_no7 = $this->get_no_apply_id($arr_levelup_apply[0]["no7_apply_id"], "1", "");
				foreach ($arr_no7 as $no7) {
					$tmp_no7_grader_score = $this->get_approve_score($no7, "1", "");
					if (is_null($tmp_no7_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No7"] = $tmp_no7_grader_score;
					$arr_levelup_info["grader_apply_id_No7"] = $no7;
					array_push($arr_eval_apply_id, $no7);
					break;
				}
			}

			// No8評価申請
			if ($arr_apply_grader[0]["grader_emp_id_4"] != "") {
				$arr_no8 = $this->get_no_apply_id($arr_levelup_apply[0]["no8_apply_id"], "1", "");
				foreach ($arr_no8 as $no8) {
					$tmp_no8_grader_score = $this->get_approve_score($no8, "1", "");
					if (is_null($tmp_no8_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No8"] = $tmp_no8_grader_score;
					$arr_levelup_info["grader_apply_id_No8"] = $no8;
					array_push($arr_eval_apply_id, $no8);
					break;
				}
			}

			// No9評価申請
			if ($arr_apply_grader[0]["grader_emp_id_4"] != "") {
				$arr_no9 = $this->get_no_apply_id($arr_levelup_apply[0]["no9_apply_id"], "1", "");
				foreach ($arr_no9 as $no9) {
					$tmp_no9_grader_score = $this->get_approve_score($no9, "1", "");
					if (is_null($tmp_no9_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No9"] = $tmp_no9_grader_score;
					$arr_levelup_info["grader_apply_id_No9"] = $no9;
					array_push($arr_eval_apply_id, $no9);
					break;
				}
			}

			// ■リーダーシップ能力
			// No10評価申請
			if ($arr_apply_grader[0]["grader_emp_id_5"] != "") {
			$arr_no10 = $this->get_no_apply_id($arr_levelup_apply[0]["no10_apply_id"], "1", "");
				foreach ($arr_no10 as $no10) {
					$tmp_no10_grader_score = $this->get_approve_score($no10, "1", "");
					if (is_null($tmp_no10_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No10"] = $tmp_no10_grader_score;
					$arr_levelup_info["grader_apply_id_No10"] = $no10;
					array_push($arr_eval_apply_id, $no10);
					break;
				}
			}

			// No11評価申請
			if ($arr_apply_grader[0]["grader_emp_id_5"] != "") {
				$arr_no11 = $this->get_no_apply_id($arr_levelup_apply[0]["no11_apply_id"], "1", "");
				foreach ($arr_no11 as $no11) {
					$tmp_no11_grader_score = $this->get_approve_score($no11, "1", "");
					if (is_null($tmp_no11_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No11"] = $tmp_no11_grader_score;
					$arr_levelup_info["grader_apply_id_No11"] = $no11;
					array_push($arr_eval_apply_id, $no11);
					break;
				}
			}

			// 2011/12/2 Yamagawa add(s)
			// No32評価申請
			if ($arr_apply_grader[0]["grader_emp_id_5_2"] != "") {
				$arr_no32 = $this->get_no_apply_id($arr_levelup_apply[0]["no32_apply_id"], "1", "");
				foreach ($arr_no32 as $no32) {
					$tmp_no32_grader_score = $this->get_approve_score($no32, "1", "");
					if (is_null($tmp_no32_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No32"] = $tmp_no32_grader_score;
					$arr_levelup_info["grader_apply_id_No32"] = $no32;
					array_push($arr_eval_apply_id, $no32);
					break;
				}
			}
			// 2011/12/2 Yamagawa add(e)

			// ■経験年数/経験領域
			// No12評価申請
			if ($arr_apply_grader[0]["grader_emp_id_6"] != "") {
				$arr_no12 = $this->get_no_apply_id($arr_levelup_apply[0]["no12_apply_id"], "1", "");
				foreach ($arr_no12 as $no12) {
					$tmp_no12_grader_score = $this->get_approve_score($no12, "1", "");
					if (is_null($tmp_no12_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No12"] = $tmp_no12_grader_score;
					$arr_levelup_info["grader_apply_id_No12"] = $no12;
					array_push($arr_eval_apply_id, $no12);
					break;
				}
			}

			// 2011/12/2 Yamagawa add(s)
			// No33評価申請
			if ($arr_apply_grader[0]["grader_emp_id_6_2"] != "") {
				$arr_no33 = $this->get_no_apply_id($arr_levelup_apply[0]["no33_apply_id"], "1", "");
				foreach ($arr_no33 as $no33) {
					$tmp_no33_grader_score = $this->get_approve_score($no33, "1", "");
					if (is_null($tmp_no33_grader_score)) {
						continue;
					}
					$arr_levelup_info["grader_score_No33"] = $tmp_no33_grader_score;
					$arr_levelup_info["grader_apply_id_No33"] = $no33;
					array_push($arr_eval_apply_id, $no33);
					break;
				}
			}
			// 2011/12/2 Yamagawa add(e)

			/////////////////////////////////////////////////////////////////////////////
			//評価項目別集計表
			/////////////////////////////////////////////////////////////////////////////
			if ($arr_levelup_info["grader_score_No1_1"] == "" && $arr_levelup_info["grader_score_No1_2"] == "") {
				$arr_levelup_info["item_score_No1"] = "";
			} else {
				$arr_levelup_info["item_score_No1"] = $arr_levelup_info["grader_score_No1_1"] + $arr_levelup_info["grader_score_No1_2"];
			}
			if ($arr_levelup_info["grader_score_No2_1"] == "" && $arr_levelup_info["grader_score_No2_2"] == "") {
				$arr_levelup_info["item_score_No2"] = "";
			} else {
				$arr_levelup_info["item_score_No2"] = $arr_levelup_info["grader_score_No2_1"] + $arr_levelup_info["grader_score_No2_2"];
				// 2011/12/19 Yamagawa add(s)
				// No2は上限15点とする
				if ($arr_levelup_info["item_score_No2"] > 15) {
					$arr_levelup_info["item_score_No2"] = 15;
				}
				// 2011/12/19 Yamagawa add(e)
			}
			$arr_levelup_info["item_score_No3"] = $arr_levelup_info["grader_score_No3"];
			$arr_levelup_info["item_score_No4"] = $arr_levelup_info["grader_score_No4"];
			$arr_levelup_info["item_score_No5"] = $arr_levelup_info["grader_score_No5"];
			$arr_levelup_info["item_score_No6"] = $arr_levelup_info["grader_score_No6"];
			$arr_levelup_info["item_score_No7"] = $arr_levelup_info["grader_score_No7"];
			$arr_levelup_info["item_score_No8"] = $arr_levelup_info["grader_score_No8"];
			$arr_levelup_info["item_score_No9"] = $arr_levelup_info["grader_score_No9"];
			$arr_levelup_info["item_score_No10"] = $arr_levelup_info["grader_score_No10"];
			$arr_levelup_info["item_score_No11"] = $arr_levelup_info["grader_score_No11"];
			$arr_levelup_info["item_score_No12"] = $arr_levelup_info["grader_score_No12"];
			// 2011/12/2 Yamagawa add(s)
			if ($arr_levelup_info["grader_score_No31_1"] == "" && $arr_levelup_info["grader_score_No31_2"] == "") {
				$arr_levelup_info["item_score_No31"] = "";
			} else {
				$arr_levelup_info["item_score_No31"] = $arr_levelup_info["grader_score_No31_1"] + $arr_levelup_info["grader_score_No31_2"];
			}
			$arr_levelup_info["item_score_No32"] = $arr_levelup_info["grader_score_No32"];
			$arr_levelup_info["item_score_No33"] = $arr_levelup_info["grader_score_No33"];
			// 2011/12/2 Yamagawa add(e)

			// 総合計点
			if ($arr_levelup_apply[0]["level"] == "2") {
				$education_score = ($arr_levelup_info["item_score_No5"] + $arr_levelup_info["item_score_No6"] + $arr_levelup_info["item_score_No8"] + $arr_levelup_info["item_score_No9"]);
				// 2011/12/2 Yamagawa add(s)
				if ($arr_levelup_apply[0]["nurse_type"] == "1") {
				// 2011/12/2 Yamagawa add(e)
					$arr_levelup_info["total_score"] = $arr_levelup_info["item_score_No1"] +
					                                   $arr_levelup_info["item_score_No3"] +
					                                   $education_score +
					                                   $arr_levelup_info["item_score_No10"] +
					                                   $arr_levelup_info["item_score_No12"];
				// 2011/12/2 Yamagawa add(s)
				} else if ($arr_levelup_apply[0]["nurse_type"] == "2" || $arr_levelup_apply[0]["nurse_type"] == "3") {
					$arr_levelup_info["total_score"] = $arr_levelup_info["item_score_No1"] +
					                                   $arr_levelup_info["item_score_No31"] +
					                                   $arr_levelup_info["item_score_No3"] +
					                                   $education_score +
					                                   $arr_levelup_info["item_score_No10"] +
					                                   $arr_levelup_info["item_score_No32"] +
					                                   $arr_levelup_info["item_score_No12"] +
					                                   $arr_levelup_info["item_score_No33"];
				}
				// 2011/12/2 Yamagawa add(e)
			} else if($arr_levelup_apply[0]["level"] == "3") {
				$education_score = ($arr_levelup_info["item_score_No5"] + $arr_levelup_info["item_score_No7"] + $arr_levelup_info["item_score_No8"] + $arr_levelup_info["item_score_No9"]);
				// 2011/12/2 Yamagawa add(s)
				if ($arr_levelup_apply[0]["nurse_type"] == "1") {
				// 2011/12/2 Yamagawa add(e)
					$arr_levelup_info["total_score"] = $arr_levelup_info["item_score_No2"] +
					                                   $arr_levelup_info["item_score_No4"] +
					                                   $education_score +
					                                   $arr_levelup_info["item_score_No11"] +
					                                   $arr_levelup_info["item_score_No12"];
				// 2011/12/2 Yamagawa add(s)
				} else if ($arr_levelup_apply[0]["nurse_type"] == "2") {
					$arr_levelup_info["total_score"] = $arr_levelup_info["item_score_No2"] +
					                                   $arr_levelup_info["item_score_No4"] +
					                                   $education_score +
					                                   $arr_levelup_info["item_score_No11"] +
					                                   $arr_levelup_info["item_score_No32"] +
					                                   $arr_levelup_info["item_score_No12"] +
					                                   $arr_levelup_info["item_score_No33"];
				} else if ($arr_levelup_apply[0]["nurse_type"] == "3") {
					$arr_levelup_info["total_score"] = $arr_levelup_info["item_score_No1"] +
					                                   $arr_levelup_info["item_score_No31"] +
					                                   $arr_levelup_info["item_score_No3"] +
					                                   $education_score +
					                                   $arr_levelup_info["item_score_No11"] +
					                                   $arr_levelup_info["item_score_No32"] +
					                                   $arr_levelup_info["item_score_No12"] +
					                                   $arr_levelup_info["item_score_No33"];
				}
				// 2011/12/2 Yamagawa add(e)
			}

			$arr_tmp_file = array();
			for ($i = 0; $i < count($arr_eval_apply_id); $i++) {
				$arr_applyfile = $this->get_applyfile($arr_eval_apply_id[$i]);
				if (count($arr_applyfile) > 0) {
					$arr_tmp_file[$arr_eval_apply_id[$i]] = $arr_applyfile;
				}
			}
			$arr_levelup_info["apply_files"] = $arr_tmp_file;
		}
		return $arr_levelup_info;
	}

	// 評価申請ＩＤ取得
	function get_no_apply_id($no_apply_id, $apv_order, $apv_sub_order) {
		if ($no_apply_id == "") return array();

		// カンマ区切りで格納しているため、配列に変換する
		$arr_no_apply_id = split(",", $no_apply_id);

		// 未削除かつ「承認確定」済みのもののみに絞る
		$tmp_arr = array();
		foreach ($arr_no_apply_id as $tmp_no_apply_id) {
			$arr_apply_wkfwmst = $this->get_apply_wkfwmst($tmp_no_apply_id);
			if ($arr_apply_wkfwmst[0]["delete_flg"] == "t" || $arr_apply_wkfwmst[0]["apply_stat"] != "1") {
				continue;
			}

			$sql = "select apv_date from cl_applyapv";
			$cond = "where apply_id = '$tmp_no_apply_id' and apv_order = $apv_order";
			if ($apv_sub_order != "") {
				$cond .= " and apv_sub_order = $apv_sub_order";
			}
			$cond .= " and apv_date <> '' and apv_date is not null";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			if (pg_num_rows($sel) == 0) {
				continue;
			}

			while ($row = pg_fetch_array($sel)) {
				$tmp_arr[$row["apv_date"]] = $tmp_no_apply_id;
			}
		}
		krsort($tmp_arr);

		$ret = array();
		foreach ($tmp_arr as $tmp_no_apply_id) {
			$ret[] = $tmp_no_apply_id;
		}
		return $ret;
	}

    function get_approve_score($apply_id, $apv_order, $apv_sub_order)
    {
         $arr_evaluation_result = $this->get_cl_evaluation_result($apply_id, $apv_order, $apv_sub_order);
         if(count($arr_evaluation_result) > 0)
         {
             $apply_score = $this->get_grader_score($arr_evaluation_result[0]["eval_content"]);
         }
         return $apply_score;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 病棟評価表・職員参照範囲設定
//-------------------------------------------------------------------------------------------------------------------------
    function get_cl_nursing_commander_class()
    {
        $sql  = "select class_div from cl_nursing_commander_class ";
        $cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
   		return pg_fetch_result($sel, 0, "class_div");
    }

	function delete_cl_nursing_commander_class()
	{
		$sql = "delete from cl_nursing_commander_class";
        $cond = "";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	function regist_cl_nursing_commander_class($class_div)
    {
		$sql = "insert into cl_nursing_commander_class (class_div) values (";
		$content = array($class_div);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    function get_cl_particular_employee()
    {
        $sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm from cl_particular_employee a ";
        $sql .= "left join empmst b on a.emp_id = b.emp_id ";
        $cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $emp_id = $row["emp_id"];
            $emp_full_name = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
            $arr[] = array("emp_id" => $emp_id,"emp_full_name" => $emp_full_name);
        }
        return $arr;
    }

	function delete_cl_particular_employee()
	{
		$sql = "delete from cl_particular_employee";
        $cond = "";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	function regist_cl_particular_employee($emp_id)
    {
		$sql = "insert into cl_particular_employee (emp_id) values (";
		$content = array($emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // 該当職員が「特定職員」どうか判断する
    function is_particular_employee($emp_id)
    {
        $sql  = "select count(a.emp_id) as cnt from cl_particular_employee a ";
        $cond = "where a.emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
   		$cnt = pg_fetch_result($sel, 0, "cnt");

        return ($cnt == 0) ? false : true;
    }

    function get_cl_operator_employee()
    {
        $sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm from cl_operator_employee a ";
        $sql .= "left join empmst b on a.emp_id = b.emp_id ";
        $cond = "";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $emp_id = $row["emp_id"];
            $emp_full_name = $row["emp_lt_nm"]." ".$row["emp_ft_nm"];
            $arr[] = array("emp_id" => $emp_id,"emp_full_name" => $emp_full_name);
        }
        return $arr;
    }

	function delete_cl_operator_employee()
	{
		$sql = "delete from cl_operator_employee";
        $cond = "";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	function regist_cl_operator_employee($emp_id)
    {
		$sql = "insert into cl_operator_employee (emp_id) values (";
		$content = array($emp_id);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // 該当職員が「運用担当者」どうか判断する
    function is_operator_employee($emp_id)
    {
        $sql  = "select count(a.emp_id) as cnt from cl_operator_employee a ";
        $cond = "where a.emp_id = '$emp_id'";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
   		$cnt = pg_fetch_result($sel, 0, "cnt");

        return ($cnt == 0) ? false : true;
    }

    // 該当職員が「師長」どうか判断する
    function is_nursing_commander($emp_id)
    {
        $sql .= "select sum(e.cnt) as cnt from (";
        $sql .= "select count(a.emp_id) as cnt from empmst a ";
        $sql .= "where exists (select st_id from stmst b where b.st_id = a.emp_st and b.st_nm like '%師長%') ";
        $sql .= "and a.emp_id = '$emp_id'";
        $sql .= "union all ";
        $sql .= "select count(c.emp_id) as cnt from concurrent c ";
        $sql .= "where exists (select st_id from stmst d where d.st_id = c.emp_st and d.st_nm like '%師長%') ";
        $sql .= "and c.emp_id = '$emp_id') e ";

        $cond = "";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
   		$cnt = pg_fetch_result($sel, 0, "cnt");

        return ($cnt == 0) ? false : true;
    }

    // 該当職員が「評価者」どうか判断する
    function is_grader($emp_id)
    {
        $sql  = "select count(a.emp_id) as cnt from cl_achievement_period a ";
        $cond = "where a.approval_emp_ids like '%$emp_id%' ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
   		$cnt = pg_fetch_result($sel, 0, "cnt");

        return ($cnt == 0) ? false : true;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 職員名簿関連
//-------------------------------------------------------------------------------------------------------------------------
    function get_emp_list_book($query, $employee_div, $login_emp_id)
    {
        $arr = array();

        $query = str_replace(" ", "", $query);
        $query = str_replace("　", "", $query);
        $query_len = strlen($query);

        $sql  = "select ";
        $sql .= "varchar(1) '1' as mail_type, ";
        $sql .= "empmst.emp_id, ";
        $sql .= "empmst.emp_lt_nm || ' ' || empmst.emp_ft_nm as name, ";
        $sql .= "login.emp_login_id as email, ";
        $sql .= "login.emp_login_mail, ";
        $sql .= "(select class_nm from classmst where class_id = empmst.emp_class) as class_nm, ";
        $sql .= "(select atrb_nm from atrbmst where atrb_id = empmst.emp_attribute) as atrb_nm, ";
        $sql .= "(select dept_nm from deptmst where dept_id = empmst.emp_dept) as dept_nm, ";
        $sql .= "(select room_nm from classroom where room_id = empmst.emp_room) as room_nm, ";
        $sql .= "(select site_id from config) as site_id ";
        $sql .= "from login ";
        $sql .= "inner join empmst on empmst.emp_id = login.emp_id ";
        $sql .= "where exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f') ";

        $sql .= $this->get_sql_for_ward_grade($employee_div, $login_emp_id);

        if ($query_len > 0)
        {
            $sql .= "and (empmst.emp_lt_nm || empmst.emp_ft_nm like '%$query%' or empmst.emp_kn_lt_nm || empmst.emp_kn_ft_nm like '%$query%' or login.emp_login_id like '%$query%' or emp_email2 like '%$query%') ";
            // ウェブメールと異なりemailも条件に追加
        }

        $sql .= "order by emp_id, 1";
        $cond = "";

        $sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
        if ($sel == 0)
        {
	        pg_close($this->_db_con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }

        while($row = pg_fetch_array($sel))
        {

            $belong_to = $row["class_nm"] . " > " . $row["atrb_nm"] . " > " . $row["dept_nm"];
            if ($row["room_nm"] != '')
            {
                $belong_to .= " > " . $row["room_nm"];
            }

            $arr[] = array(
                            "emp_id"    => $row["emp_id"],
                        	"emp_name"  => $row["name"],
                            "belong_to" => $belong_to
                           );

        }
        return $arr;
    }

    function get_sql_for_ward_grade($employee_div, $login_emp_id)
    {
        $sql = "";

        // ログインユーザが「評価者」の場合
        if($employee_div == "GRADER")
        {
            $sql .= " and exists (select * from cl_achievement_period where cl_achievement_period.approval_emp_ids like '%$login_emp_id%' and cl_achievement_period.emp_id = empmst.emp_id) ";
        }
        // ログインユーザが「師長」の場合
        else if($employee_div == "NURSING_COMMANDER")
        {
            // 病棟評価表・職員参照範囲設定情報取得
            $class_div = $this->get_cl_nursing_commander_class();
            if($class_div == "")
            {
                 // 組織階層情報取得
                 $arr_class_name = get_class_name_array($this->_db_con, $this->file_name);
                 $class_div = $arr_class_name["class_cnt"];
            }

            // 部署役職取得(複数部署対応)
            switch($class_div)
            {
                case "1":
                    $class_column = "emp_class";
                    break;
                case "2":
                    $class_column = "emp_attribute";
                    break;
                case "3":
                    $class_column = "emp_dept";
                    break;
                case "4":
                    $class_column = "emp_room";
                    break;
            }

            $sql .= "and (exists (select * from ";
            $sql .= "(";
			$sql .= "select * from empmst where exists (select st_id from stmst where stmst.st_id = empmst.emp_st and stmst.st_nm like '%師長%') and empmst.emp_id = '$login_emp_id' ";
            $sql .= ") emp_sub1 ";
            $sql .= "where emp_sub1.$class_column = empmst.$class_column) ";

            $sql .= "or exists (select * from ";
            $sql .= "(";
			$sql .= "select * from concurrent where exists (select st_id from stmst where stmst.st_id = concurrent.emp_st and stmst.st_nm like '%師長%') and concurrent.emp_id = '$login_emp_id' ";
            $sql .= ") emp_sub2  ";
            $sql .= "where emp_sub2.$class_column = empmst.$class_column)) ";

        }

        return $sql;
    }

//-------------------------------------------------------------------------------------------------------------------------
// プリセプターチュータ評価選択
//-------------------------------------------------------------------------------------------------------------------------

    function get_preceptor_eval_list($emp_id)
    {
        $arr_preceptor_tutor_grader = $this->get_preceptor_tutor_grader($emp_id);

        $arr = array();
        foreach($arr_preceptor_tutor_grader as $preceptor_tutor_grader)
        {
            $total_score = $this->get_preceptor_grader_score($preceptor_tutor_grader["eval_content"]);
            $preceptor_tutor_grader["total_score"] = $total_score;
            if($total_score >= 70)
            {
                $arr[] = $preceptor_tutor_grader;
            }
        }

        return $arr;
    }

    function get_preceptor_tutor_grader($emp_id)
    {
        $sql   = "select a.*, b.apply_date, b.apply_no, c.emp_lt_nm, c.emp_ft_nm from cl_applyapv a ";
        $sql  .= "inner join ( ";
        $sql  .= "select * from cl_apply ca where exists (select * from cl_wkfwmst cw where cw.short_wkfw_name = 'c217' and ca.wkfw_id = cw.wkfw_id) and ";
        $sql  .= "ca.emp_id = '$emp_id' and  ";
        $sql  .= "ca.delete_flg = 'f' and  ";
        $sql  .= "ca.apply_stat = '1' ";
        $sql  .= ") b on a.apply_id = b.apply_id ";
        $sql  .= "left join empmst c on a.emp_id = c.emp_id ";
        $cond  = "where a.apv_stat = 1 ";
        $cond .= "order by a.apply_id, a.apv_order, a.apv_sub_order ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();

		while($row = pg_fetch_array($sel))
		{
			$arr[] = array(
                           "apply_date"    => $row["apply_date"],
                           "apply_no"      => $row["apply_no"],
                           "apply_id"      => $row["apply_id"],
                           "apv_order"     => $row["apv_order"],
                           "apv_sub_order" => $row["apv_sub_order"],
                           "emp_lt_nm"     => $row["emp_lt_nm"],
                           "emp_ft_nm"     => $row["emp_ft_nm"],
                           "apv_date"      => $row["apv_date"],
                           "eval_content"  => $row["eval_content"]
                           );
		}
		return $arr;

    }

//-------------------------------------------------------------------------------------------------------------------------
// 通知件数取得
//-------------------------------------------------------------------------------------------------------------------------
    // 承認済み情報取得
    function get_apply_fix($emp_id)
    {
        $sql  = "select varchar(1) '1' as apply_type, apply_id, null as pjt_schd_id from cl_apply ";
        $sql .= "where apply_stat = '1' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_fix_show_flg ";

	    $cond = "";
	    $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
	    if ($sel == 0) {
		    pg_close($this->_db_con);
		    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		    exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 差戻し情報取得
    function get_apply_bak($emp_id)
    {

        $sql  = "select varchar(1) '1' as apply_type, apply_id from cl_apply ";
        $sql .= "where apply_stat = '3' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_bak_show_flg ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 承認待ち情報情報
    function get_approve_wait($emp_id)
    {
        $sql  = "select varchar(1) '1' as apply_type, a.apply_id, a.wkfw_id, a.emp_id, a.apply_date, a.apply_title, null as pjt_schd_id, b.apv_order, b.apv_sub_order ";
        $sql .= "from cl_apply a ";
        $sql .= "inner join cl_applyapv b on a.apply_id = b.apply_id ";
        $sql .= "left join cl_applyasyncrecv c on b.apply_id = c.apply_id and b.apv_order = c.recv_apv_order and ";
        $sql .= "((b.apv_sub_order = c.recv_apv_sub_order) or (b.apv_sub_order is null and c.recv_apv_sub_order is null)) ";
        $sql .= "where ";
        $sql .= "not a.delete_flg and ";
        $sql .= "not a.draft_flg and ";
        $sql .= "(c.apv_show_flg or c.apv_show_flg is null) and ";
        $sql .= "b.emp_id = '$emp_id' and ";
        $sql .= "a.re_apply_id is null and ";
        $sql .= "b.apv_stat = '0' and ";
        $sql .= "(";
        $sql .= "(a.wkfw_appr = '1' and ";
        $sql .= "exists (select apply_id from cl_applyapv apv1 where a.apply_id = apv1.apply_id and apv1.emp_id = '$emp_id' and apv1.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= "or ";
//        $sql .= "(a.wkfw_appr = '2' and a.apply_stat = '0' and ";
        $sql .= "(a.wkfw_appr = '2' and ";
        $sql .= "exists (select apv2.* from (select apply_id, apv_order from cl_applyapv where emp_id = '$emp_id' and apv_stat = '0' and ";
        $sql .= "apv_order = (select min(apv_order) from cl_applyapv minapplyapv where minapplyapv.apply_id = a.apply_id and apv_stat = '0')) apv2 ";
        $sql .= "where a.apply_id = apv2.apply_id and b.apv_order = apv2.apv_order) ";
        $sql .= ") ";
        $sql .= "or ";
        $sql .= "(a.wkfw_appr = '2' and c.apv_show_flg and ";
        $sql .= "exists (select apply_id from cl_applyapv apv3 where a.apply_id = apv3.apply_id and apv3.emp_id = '$emp_id' and apv3.apv_stat = '0') ";
        $sql .= ") ";
        $sql .= ") ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $num = pg_numrows($sel);
        return $num;
    }


    // 否認件数取得
    function get_apply_ng($emp_id)
    {

        $sql  = "select varchar(1) '1' as apply_type, apply_id from cl_apply ";
        $sql .= "where apply_stat = '2' and emp_id = '$emp_id' and re_apply_id is null and not delete_flg and apv_ng_show_flg ";
        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_numrows($sel);
        return $num;
    }


    // 一部承認件数取得
    function get_apply_fix_1($emp_id)
    {

        $sql  = "select sum(case when b.apv_1 > 0 then 1 else 0 end) as apv_1_cnt ";
        $sql .= "from ";
        $sql .= "(select count(a.apply_id) as apv_1 from cl_applyapv a ";
        $sql .= "where a.apply_id in (select apply_id from cl_apply where emp_id = '$emp_id' and not delete_flg and apply_stat = '0') and ";
        $sql .= "a.apv_stat = '1' and ";
        $sql .= "not a.delete_flg and ";
        $sql .= "a.apv_fix_show_flg ";
        $sql .= "group by a.apply_id ";
        $sql .= ") b ";

        $cond = "";
        $sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
        if ($sel == 0) {
            pg_close($this->_db_con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $num = pg_fetch_result($sel, 0, "apv_1_cnt");
        if ($num == "") {
            $num = 0;
        }
        return $num;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 委員会関連
//-------------------------------------------------------------------------------------------------------------------------
   /**
    * 委員会設定情報取得
    * $div 認定委員会は「1」。看護診断委員会は「2」。
    *
    */
    function get_cl_committee($div)
    {
        $sql   = "select a.committee_div, a.parent_pjt_id, a.child_pjt_id, b.pjt_name as parent_pjt_nm, c.pjt_name as child_pjt_nm ";
        $sql  .= "from cl_committee a ";
        $sql  .= "left join project b on a.parent_pjt_id = b.pjt_id ";
        $sql  .= "left join project c on a.child_pjt_id = c.pjt_id ";
        $cond  = "where committee_div = $div order by a.order_no asc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $arr[] = array(
                           "committee_div" => $row["committee_div"],
                           "parent_pjt_id" => $row["parent_pjt_id"],
                           "child_pjt_id"  => $row["child_pjt_id"],
                           "parent_pjt_nm" => $row["parent_pjt_nm"],
                           "child_pjt_nm"  => $row["child_pjt_nm"]
                           );
        }
        return $arr;
    }

    // 委員会設定情報削除
	function delete_cl_committee()
	{
		$sql  = "delete from cl_committee";
		$cond = "";
		$del = delete_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($del == 0) {
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

    // 委員会設定情報登録
	function regist_cl_committee($arr)
    {
        $committee_div = $arr["committee_div"];
        $parent_pjt_id = $arr["parent_pjt_id"];
        $child_pjt_id  = ($arr["child_pjt_id"] == "") ? NULL : $arr["child_pjt_id"];
        $order_no      = $arr["order_no"];

		$sql = "insert into cl_committee (committee_div, parent_pjt_id, child_pjt_id, order_no) values (";
		$content = array($committee_div, $parent_pjt_id, $child_pjt_id, $order_no);
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
    }

    // 今後予定の委員会スケジュール情報取得
    function get_proschd($parent_pjt_id, $child_pjt_id)
    {
        $today = date("Y-m-d");

        // 委員会(時間指定あり)
        $sql   = "select pjt_schd_id, pjt_id, pjt_schd_title, pjt_schd_type, pjt_schd_start_time, pjt_schd_dur, pjt_schd_start_date, pjt_schd_start_time_v, pjt_schd_dur_v ";
        $sql  .= "from proschd ";
        $sql  .= "where pjt_id = '$parent_pjt_id' and pjt_schd_start_date >= '$today' ";

        // 委員会(時間指定なし)
        $sql  .= "union all ";
        $sql  .= "select pjt_schd_id, pjt_id, pjt_schd_title, pjt_schd_type, null, null, pjt_schd_start_date, null, null ";
        $sql  .= "from proschd2 ";
        $sql  .= "where pjt_id = '$parent_pjt_id' and pjt_schd_start_date >= '$today' ";

        if($child_pjt_id != "")
        {
            // ＷＧ(時間指定あり)
            $sql  .= "union all ";
            $sql  .= "select pjt_schd_id, pjt_id, pjt_schd_title, pjt_schd_type, pjt_schd_start_time, pjt_schd_dur, pjt_schd_start_date, pjt_schd_start_time_v, pjt_schd_dur_v ";
            $sql  .= "from proschd ";
            $sql  .= "where pjt_id = '$child_pjt_id' and pjt_schd_start_date >= '$today' ";

            // ＷＧ(時間指定なし)
            $sql  .= "union all ";
            $sql  .= "select pjt_schd_id, pjt_id, pjt_schd_title, pjt_schd_type, null, null, pjt_schd_start_date, null, null ";
            $sql  .= "from proschd2 ";
            $sql  .= "where pjt_id = '$child_pjt_id' and pjt_schd_start_date >= '$today' ";
        }

        $cond  .= "order by pjt_id, pjt_schd_start_date, pjt_schd_start_time ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $arr[] = array(
                           "pjt_schd_id"           => $row["pjt_schd_id"],
                           "pjt_id"                => $row["pjt_id"],
                           "pjt_schd_title"        => $row["pjt_schd_title"],
                           "pjt_schd_type"         => $row["pjt_schd_type"],
                           "pjt_schd_start_time"   => $row["pjt_schd_start_time"],
                           "pjt_schd_dur"          => $row["pjt_schd_dur"],
                           "pjt_schd_start_date"   => $row["pjt_schd_start_date"],
                           "pjt_schd_start_time_v" => $row["pjt_schd_start_time_v"],
                           "pjt_schd_dur_v"        => $row["pjt_schd_dur_v"]
                           );
        }
        return $arr;
    }

//-------------------------------------------------------------------------------------------------------------------------
// 院外受講申請関連
//-------------------------------------------------------------------------------------------------------------------------
    function get_outside_training($emp_id)
    {
        $sql   = "select a.* from cl_apply a ";
        $sql  .= "inner join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
        $cond  = "where a.apply_stat = 1 and a.emp_id = '$emp_id' and delete_flg = 'f' and b.short_wkfw_name = 'c502' ";
        $cond .= "order by a.apply_id desc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $arr[] = array(
                           "apply_id"  => $row["apply_id"],
                           "apply_content"  => $row["apply_content"]
                           );
        }
        return $arr;
    }

    // 受講申請
	function get_training_content($apply_content)
	{
	    // 一時的にinclude_pathを変更
	    $old_inc_dir = ini_get('include_path');
	    $add_inc_dir = dirname(__FILE__) . "/PEAR";
	    $inc_separate = ":";

	    ini_set('include_path', $old_inc_dir . $inc_separate . $add_inc_dir);

	    // XML_Serializerを利用して登録データ、DB定義をそれぞれパースする
	    require_once("Unserializer.php");

	    $Unserializer =&new XML_Unserializer();
if (PHP_VERSION >= '5') {
$apply_content = str_replace("EUC-JP", "EUCJP-win", $apply_content);
}
	    $Unserializer->unserialize($apply_content);
	    $xml_data_array = $Unserializer->getUnserializedData();
if (PHP_VERSION >= '5') {
mb_convert_variables("EUCJP-win", "UTF-8", $xml_data_array);
}

	    // include_pathを元に戻す
	    ini_set('include_path', $old_inc_dir);

        $arr = array();

        $course_code = $xml_data_array["course_code"];
        $course_theme = $xml_data_array["course_theme"];
        $purpose = $xml_data_array["purpose"];
        $outline = $xml_data_array["outline"];

        $date_y1 = $xml_data_array["date_y1"];
	    $date_m1 = $xml_data_array["date_m1"];
	    $date_d1 = $xml_data_array["date_d1"];

        $date_y2 = $xml_data_array["date_y2"];
	    $date_m2 = $xml_data_array["date_m2"];
	    $date_d2 = $xml_data_array["date_d2"];

        $course_days = $xml_data_array["course_days"];
        $course_times = $xml_data_array["course_times"];

        $arr[] = array(
                       "course_code" => $course_code,
                       "course_theme" => $course_theme,
                       "purpose" => $purpose,
                       "outline" => $outline,
                       "date_y1" => $date_y1,
                       "date_m1" => $date_m1,
                       "date_d1" => $date_d1,
                       "date_y2" => $date_y2,
                       "date_m2" => $date_m2,
                       "date_d2" => $date_d2,
                       "course_days" => $course_days,
                       "course_times" => $course_times
                       );

		return $arr;
	}

//-------------------------------------------------------------------------------------------------------------------------
// 受講報告一覧関連
//-------------------------------------------------------------------------------------------------------------------------
    function get_trainig_report_list($emp_id)
    {
        // 本日日付から2年前日付取得
        $two_month_ago = date("Ymd",strtotime("-2 year" ,strtotime(date("Ymd"))));

        $sql   = "select a.* from cl_apply a ";
        $sql  .= "inner join cl_wkfwmst b on a.wkfw_id = b.wkfw_id ";
        $cond  = "where a.apply_stat = 1 and a.emp_id = '$emp_id' and delete_flg = 'f' and b.short_wkfw_name = 'c503' ";
        $cond .= "and substring(a.apply_date from 1 for 8) >= '$two_month_ago' ";
        $cond .= "order by a.apply_id desc ";
		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr = array();
        while($row = $row = pg_fetch_array($sel))
        {
            $arr[] = array(
                           "apply_id"  => $row["apply_id"],
                           "apply_content"  => $row["apply_content"]
                           );
        }
        return $arr;
    }


//-------------------------------------------------------------------------------------------------------------------------
// その他
//-------------------------------------------------------------------------------------------------------------------------
    function create_ymd_format($ymd)
    {
        if(strlen($ymd) != 8)
        {
        	return $ymd;
        }

		$year    = substr($ymd, 0, 4);
		$month   = substr($ymd, 4, 2);
		$day     = substr($ymd, 6, 2);

        return $year."/".$month."/".$day;
    }

    function create_level_format($level)
    {
        if($level == "")
        {
            $level_format = "";
        }
        else if($level == "1")
        {
            $level_format = "I";
        }
        else if($level == "2")
        {
            $level_format = "II";
        }
        else if($level == "3")
        {
            $level_format = "III";
        }
        else if($level == "4")
        {
            $level_format = "IV";
        }

        return $level_format;
    }

    function create_recognize_flg_format($recognize_flg)
    {
        $recognize_flg_format = "不可";
        if($recognize_flg == "t")
        {
            $recognize_flg_format = "可";
        }
        return $recognize_flg_format;
    }

	// 委員会ＷＧ名取得
	function get_pjt_nm($pjt_id)
	{
		$sql  = "select pjt_name from project";
		$cond = "where pjt_id = $pjt_id ";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "pjt_name");
	}

	// 未承認以外をカウントする
	function get_applyapv_cnt($apply_id)
	{
		$sql  = "select count(*) as cnt from cl_applyapv";
		$cond = "where apply_id = '$apply_id' and apv_stat <> 0";

		$sel = select_from_table($this->_db_con,$sql,$cond,$this->file_name);
		if($sel == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		return pg_fetch_result($sel, 0, "cnt");
	}

	// マージ処理
	function merge_arr_emp_info($arr_all_emp_info, $arr_target_emp_info)
	{
		$arr_tmp_apv = array();
		foreach($arr_target_emp_info as $target_emp_info)
		{
			$dpl_flg = false;
			foreach($arr_all_emp_info as $all_emp_info)
			{
				if(
					$target_emp_info["emp_id"] != ""
					&& $target_emp_info["emp_id"] == $all_emp_info["emp_id"]
				)
				{
					$dpl_flg = true;
				}
			}
			if(!$dpl_flg)
			{
				$arr_tmp_apv[] = $target_emp_info;
			}
		}

		for($i=0; $i<count($arr_tmp_apv); $i++)
		{
			array_push($arr_all_emp_info, $arr_tmp_apv[$i]);
		}

		return $arr_all_emp_info;
	}

	// 本体用ワークフローディレクトリ作成
	function create_wkfwreal_directory()
	{
		if (!is_dir("cl/template/real"))
		{
			mkdir("cl/template/real", 0755);
			// フォーマットファイルコピー
			foreach (glob("cl/template/*.*") as $file)
			{
				$tmp_file = substr($file, 9);
				copy($file, "cl/template/real/$tmp_file");
			}
		}
	}

	// ワークフロー用ディレクトリ作成
	function create_wkfwtmp_directory()
	{
        if(!is_dir("cl/template"))
		{
			mkdir("cl/template", 0755);
		}
        if (!is_dir("cl/template/tmp"))
        {
            mkdir("cl/template/tmp", 0755);
        }
	}

	// 申請用ディレクトリ作成
	function create_applytmp_directory()
	{
        if(!is_dir("cl/apply"))
		{
			mkdir("cl/apply", 0755);
		}
        if (!is_dir("cl/apply/tmp"))
        {
            mkdir("cl/apply/tmp", 0755);
        }
	}

	//*****tableへのinsert(連想配列版)*****
	function insert_into_table_array($table, $array){
		@require("./conf/conf.inf");  								  						//環境設定ファイルの読み込み
		require_once("./about_sql_log.php");												//SQLのログをとるためのファイルを読み込む
		if(!defined("CONF_FILE")){	        					  						//読み込みの確認

				require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
				require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
				write_error($this->file_name, $ERRORLOG, $ERROR001);								//errorログへの書き込み

			return 0;																			//失敗の場合は０を返す
		}

		$insert_data = pg_convert($this->_db_con, $table, $array);
		if ($insert_data === FALSE){
			require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
			require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
			write_error($fname,$ERRORLOG, $ERROR103);								//errorログへの書き込み

			return 0;																			//失敗の場合は０を返す
		}

		$keys   = implode(',', array_keys($insert_data));
		$values = implode(',', array_values($insert_data));
		$SQL = "insert into $table ($keys) values ($values)";
		$wsql = write_sql_log($SQLLOG,$fname,$SQL,$SQLLOGFILE,$ERRORLOG);			//$SQLLOGがonだと実行されるSQLのログをとる

		if(!@($result = (pg_exec($this->_db_con, $SQL)))){										//SQLの実行

				require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
				require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
				write_error($fname,$ERRORLOG, $ERROR103);								//errorログへの書き込み

			return 0;																			//失敗の場合は０を返す
		}else{
			return 1;																			//成功の場合は１を返します
		}

	}

	// DB切断と終了処理
	function db_close_exit_dq()
	{
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// DBロールバック及び切断と終了処理
	function db_rollback_exit_dq()
	{
		pg_query($this->_db_con, "rollback");
		pg_close($this->_db_con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// h-iwamoto add start
	// 志願者名称取得
	function get_applicant_name($short_wkfw_name,$emp_id,$apply_content)
	{

		/**
		 * 志願・評価申請・レベルアップ申請の場合
		 */
		if(		$this->is_application_book_template($short_wkfw_name)
			||	$this->is_eval_template($short_wkfw_name)
			||	$this->is_levelup_apply_template($short_wkfw_name)
		){
			/**
			 * レベルアップ申請の場合
			 */
			if($this->is_levelup_apply_template($short_wkfw_name)){
				$applicant_id = preg_replace("/.*<levelup_apply_emp_id>(.*?)<\/levelup_apply_emp_id>.*/", "$1", str_replace("\n", "", $apply_content));
				$applicant_name=htmlspecialchars(get_emp_kanji_name($this->_db_con,$applicant_id,$this->file_name));
			}
			/**
			 * 志願・評価申請の場合
			 */
			else{
				$applicant_name=htmlspecialchars(get_emp_kanji_name($this->_db_con,$emp_id,$this->file_name));
			}
		}
		/**
		 * その他の申請の場合
		 */
		else{
			$applicant_name="<br/>";
		}
		return $applicant_name;
	}

	// h-iwamoto add end

	// 2012/03/03 Yamagawa add(s)
	// 承認者取得　主催者
	function get_training_opener($training_id){

		$sql  = "SELECT ";
		$sql .= " tra.training_opener ";
		$sql .= " ,emp.emp_lt_nm || ' ' || emp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " cl_mst_inside_training tra ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  tra.training_opener = emp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  emp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " tra.training_id = '$training_id' ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["training_opener"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　講師
	function get_training_teacher($training_id){

		$sql  = "SELECT ";
		$sql .= " tra.training_teacher1 ";
		$sql .= " ,emp1.emp_lt_nm || ' ' || emp1.emp_ft_nm AS emp_full_nm1 ";
		$sql .= " ,st1.st_nm AS st_nm1 ";
		$sql .= " ,tra.training_teacher2 ";
		$sql .= " ,emp2.emp_lt_nm || ' ' || emp2.emp_ft_nm AS emp_full_nm2 ";
		$sql .= " ,st2.st_nm AS st_nm2 ";
		$sql .= " ,tra.training_teacher3 ";
		$sql .= " ,emp3.emp_lt_nm || ' ' || emp3.emp_ft_nm AS emp_full_nm3 ";
		$sql .= " ,st3.st_nm AS st_nm3 ";
		$sql .= " ,tra.training_teacher4 ";
		$sql .= " ,emp4.emp_lt_nm || ' ' || emp4.emp_ft_nm AS emp_full_nm4 ";
		$sql .= " ,st4.st_nm AS st_nm4 ";
		$sql .= " ,tra.training_teacher5 ";
		$sql .= " ,emp5.emp_lt_nm || ' ' || emp5.emp_ft_nm AS emp_full_nm5 ";
		$sql .= " ,st5.st_nm AS st_nm5 ";
		$sql .= "FROM ";
		$sql .= " cl_mst_inside_training tra ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp1 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher1 = emp1.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st1 ";
		$sql .= " ON ";
		$sql .= "  emp1.emp_st = st1.st_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp2 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher2 = emp2.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st2 ";
		$sql .= " ON ";
		$sql .= "  emp2.emp_st = st2.st_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp3 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher3 = emp3.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st3 ";
		$sql .= " ON ";
		$sql .= "  emp3.emp_st = st3.st_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp4 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher4 = emp4.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st4 ";
		$sql .= " ON ";
		$sql .= "  emp4.emp_st = st4.st_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp5 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher5 = emp5.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st5 ";
		$sql .= " ON ";
		$sql .= "  emp5.emp_st = st5.st_id ";
		$sql .= "WHERE ";
		$sql .= " tra.training_id = '$training_id' ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			if ($row["training_teacher1"] != '') {
				$arr[] = array(
					"emp_id"		=> $row["training_teacher1"]
					,"emp_full_nm"	=> $row["emp_full_nm1"]
					,"st_nm"		=> $row["st_nm1"]
				);
			}
			if ($row["training_teacher2"] != '') {
				$arr[] = array(
					"emp_id"		=> $row["training_teacher2"]
					,"emp_full_nm"	=> $row["emp_full_nm2"]
					,"st_nm"		=> $row["st_nm2"]
				);
			}
			if ($row["training_teacher3"] != '') {
				$arr[] = array(
					"emp_id"		=> $row["training_teacher3"]
					,"emp_full_nm"	=> $row["emp_full_nm3"]
					,"st_nm"		=> $row["st_nm3"]
				);
			}
			if ($row["training_teacher4"] != '') {
				$arr[] = array(
					"emp_id"		=> $row["training_teacher4"]
					,"emp_full_nm"	=> $row["emp_full_nm4"]
					,"st_nm"		=> $row["st_nm4"]
				);
			}
			if ($row["training_teacher5"] != '') {
				$arr[] = array(
					"emp_id"		=> $row["training_teacher5"]
					,"emp_full_nm"	=> $row["emp_full_nm5"]
					,"st_nm"		=> $row["st_nm5"]
				);
			}
		}
		return $arr;

	}

	// 承認者取得　講師
	function get_training_teacher_from_apply($apply_id){

		$sql  = "SELECT ";
		$sql .= " mng.apv_order ";
		$sql .= " ,tra.training_teacher1 ";
		$sql .= " ,emp1.emp_lt_nm || ' ' || emp1.emp_ft_nm AS emp_full_nm1 ";
		$sql .= " ,apv1.emp_id AS emp_checked1 ";
		$sql .= " ,tra.training_teacher2 ";
		$sql .= " ,emp2.emp_lt_nm || ' ' || emp2.emp_ft_nm AS emp_full_nm2 ";
		$sql .= " ,apv2.emp_id AS emp_checked2 ";
		$sql .= " ,tra.training_teacher3 ";
		$sql .= " ,emp3.emp_lt_nm || ' ' || emp3.emp_ft_nm AS emp_full_nm3 ";
		$sql .= " ,apv3.emp_id AS emp_checked3 ";
		$sql .= " ,tra.training_teacher4 ";
		$sql .= " ,emp4.emp_lt_nm || ' ' || emp4.emp_ft_nm AS emp_full_nm4 ";
		$sql .= " ,apv4.emp_id AS emp_checked4 ";
		$sql .= " ,tra.training_teacher5 ";
		$sql .= " ,emp5.emp_lt_nm || ' ' || emp5.emp_ft_nm AS emp_full_nm5 ";
		$sql .= " ,apv5.emp_id AS emp_checked5 ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_apply apl ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply cl ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = cl.apply_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_wkfwapvmng mng ";
		$sql .= " ON ";
		$sql .= "  cl.wkfw_id = mng.wkfw_id ";
		$sql .= "  AND 't' = mng.apv_div6_flg ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  apl.inside_training_id = tra.training_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp1 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher1 = emp1.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp2 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher2 = emp2.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp3 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher3 = emp3.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp4 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher4 = emp4.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp5 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher5 = emp5.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv1 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv1.apply_id ";
		$sql .= "  AND tra.training_teacher1 = apv1.emp_id ";
		$sql .= "  AND mng.apv_order = apv1.apv_order ";
		$sql .= "  AND 6 = apv1.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv2 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv2.apply_id ";
		$sql .= "  AND tra.training_teacher2 = apv2.emp_id ";
		$sql .= "  AND mng.apv_order = apv2.apv_order ";
		$sql .= "  AND 6 = apv2.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv3 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv3.apply_id ";
		$sql .= "  AND tra.training_teacher3 = apv3.emp_id ";
		$sql .= "  AND mng.apv_order = apv3.apv_order ";
		$sql .= "  AND 6 = apv3.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv4 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv4.apply_id ";
		$sql .= "  AND tra.training_teacher4 = apv4.emp_id ";
		$sql .= "  AND mng.apv_order = apv4.apv_order ";
		$sql .= "  AND 6 = apv4.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv5 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv5.apply_id ";
		$sql .= "  AND tra.training_teacher5 = apv5.emp_id ";
		$sql .= "  AND mng.apv_order = apv5.apv_order ";
		$sql .= "  AND 6 = apv5.st_div ";
		$sql .= "WHERE ";
		$sql .= " apl.apply_id = '$apply_id' ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " mng.apv_order ";
		$sql .= " ,tra.training_teacher1 ";
		$sql .= " ,emp1.emp_lt_nm || ' ' || emp1.emp_ft_nm AS emp_full_nm1 ";
		$sql .= " ,apv1.emp_id AS emp_checked1 ";
		$sql .= " ,tra.training_teacher2 ";
		$sql .= " ,emp2.emp_lt_nm || ' ' || emp2.emp_ft_nm AS emp_full_nm2 ";
		$sql .= " ,apv2.emp_id AS emp_checked2 ";
		$sql .= " ,tra.training_teacher3 ";
		$sql .= " ,emp3.emp_lt_nm || ' ' || emp3.emp_ft_nm AS emp_full_nm3 ";
		$sql .= " ,apv3.emp_id AS emp_checked3 ";
		$sql .= " ,tra.training_teacher4 ";
		$sql .= " ,emp4.emp_lt_nm || ' ' || emp4.emp_ft_nm AS emp_full_nm4 ";
		$sql .= " ,apv4.emp_id AS emp_checked4 ";
		$sql .= " ,tra.training_teacher5 ";
		$sql .= " ,emp5.emp_lt_nm || ' ' || emp5.emp_ft_nm AS emp_full_nm5 ";
		$sql .= " ,apv5.emp_id AS emp_checked5 ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_theme_present apl ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply cl ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = cl.apply_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_wkfwapvmng mng ";
		$sql .= " ON ";
		$sql .= "  cl.wkfw_id = mng.wkfw_id ";
		$sql .= "  AND 't' = mng.apv_div6_flg ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  apl.training_id = tra.training_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp1 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher1 = emp1.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp2 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher2 = emp2.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp3 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher3 = emp3.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp4 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher4 = emp4.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp5 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher5 = emp5.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv1 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv1.apply_id ";
		$sql .= "  AND tra.training_teacher1 = apv1.emp_id ";
		$sql .= "  AND mng.apv_order = apv1.apv_order ";
		$sql .= "  AND 6 = apv1.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv2 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv2.apply_id ";
		$sql .= "  AND tra.training_teacher2 = apv2.emp_id ";
		$sql .= "  AND mng.apv_order = apv2.apv_order ";
		$sql .= "  AND 6 = apv2.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv3 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv3.apply_id ";
		$sql .= "  AND tra.training_teacher3 = apv3.emp_id ";
		$sql .= "  AND mng.apv_order = apv3.apv_order ";
		$sql .= "  AND 6 = apv3.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv4 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv4.apply_id ";
		$sql .= "  AND tra.training_teacher4 = apv4.emp_id ";
		$sql .= "  AND mng.apv_order = apv4.apv_order ";
		$sql .= "  AND 6 = apv4.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv5 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv5.apply_id ";
		$sql .= "  AND tra.training_teacher5 = apv5.emp_id ";
		$sql .= "  AND mng.apv_order = apv5.apv_order ";
		$sql .= "  AND 6 = apv5.st_div ";
		$sql .= "WHERE ";
		$sql .= " apl.apply_id = '$apply_id' ";
		$sql .= "UNION ALL ";
		$sql .= "SELECT ";
		$sql .= " mng.apv_order ";
		$sql .= " ,tra.training_teacher1 ";
		$sql .= " ,emp1.emp_lt_nm || ' ' || emp1.emp_ft_nm AS emp_full_nm1 ";
		$sql .= " ,apv1.emp_id AS emp_checked1 ";
		$sql .= " ,tra.training_teacher2 ";
		$sql .= " ,emp2.emp_lt_nm || ' ' || emp2.emp_ft_nm AS emp_full_nm2 ";
		$sql .= " ,apv2.emp_id AS emp_checked2 ";
		$sql .= " ,tra.training_teacher3 ";
		$sql .= " ,emp3.emp_lt_nm || ' ' || emp3.emp_ft_nm AS emp_full_nm3 ";
		$sql .= " ,apv3.emp_id AS emp_checked3 ";
		$sql .= " ,tra.training_teacher4 ";
		$sql .= " ,emp4.emp_lt_nm || ' ' || emp4.emp_ft_nm AS emp_full_nm4 ";
		$sql .= " ,apv4.emp_id AS emp_checked4 ";
		$sql .= " ,tra.training_teacher5 ";
		$sql .= " ,emp5.emp_lt_nm || ' ' || emp5.emp_ft_nm AS emp_full_nm5 ";
		$sql .= " ,apv5.emp_id AS emp_checked5 ";
		$sql .= "FROM ";
		$sql .= " cl_apl_inside_training_report apl ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_apply cl ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = cl.apply_id ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_wkfwapvmng mng ";
		$sql .= " ON ";
		$sql .= "  cl.wkfw_id = mng.wkfw_id ";
		$sql .= "  AND 't' = mng.apv_div6_flg ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_inside_training tra ";
		$sql .= " ON ";
		$sql .= "  apl.training_id = tra.training_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp1 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher1 = emp1.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp2 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher2 = emp2.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp3 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher3 = emp3.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp4 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher4 = emp4.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  empmst emp5 ";
		$sql .= " ON ";
		$sql .= "  tra.training_teacher5 = emp5.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv1 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv1.apply_id ";
		$sql .= "  AND tra.training_teacher1 = apv1.emp_id ";
		$sql .= "  AND mng.apv_order = apv1.apv_order ";
		$sql .= "  AND 6 = apv1.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv2 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv2.apply_id ";
		$sql .= "  AND tra.training_teacher2 = apv2.emp_id ";
		$sql .= "  AND mng.apv_order = apv2.apv_order ";
		$sql .= "  AND 6 = apv2.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv3 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv3.apply_id ";
		$sql .= "  AND tra.training_teacher3 = apv3.emp_id ";
		$sql .= "  AND mng.apv_order = apv3.apv_order ";
		$sql .= "  AND 6 = apv3.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv4 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv4.apply_id ";
		$sql .= "  AND tra.training_teacher4 = apv4.emp_id ";
		$sql .= "  AND mng.apv_order = apv4.apv_order ";
		$sql .= "  AND 6 = apv4.st_div ";
		$sql .= " LEFT JOIN ";
		$sql .= "  cl_applyapv apv5 ";
		$sql .= " ON ";
		$sql .= "  apl.apply_id = apv5.apply_id ";
		$sql .= "  AND tra.training_teacher5 = apv5.emp_id ";
		$sql .= "  AND mng.apv_order = apv5.apv_order ";
		$sql .= "  AND 6 = apv5.st_div ";
		$sql .= "WHERE ";
		$sql .= " apl.apply_id = '$apply_id' ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		// 申請
		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			if ($row["training_teacher1"] != "") {
				$arr[] = array(
					"apv_order"		=> $row["apv_order"]
					,"emp_id"		=> $row["training_teacher1"]
					,"emp_full_nm"	=> $row["emp_full_nm1"]
					,"checked"		=> $row["emp_checked1"]
				);
			}
			if ($row["training_teacher2"] != "") {
				$arr[] = array(
					"apv_order"		=> $row["apv_order"]
					,"emp_id"		=> $row["training_teacher2"]
					,"emp_full_nm"	=> $row["emp_full_nm2"]
					,"checked"		=> $row["emp_checked2"]
				);
			}
			if ($row["training_teacher3"] != "") {
				$arr[] = array(
					"apv_order"		=> $row["apv_order"]
					,"emp_id"		=> $row["training_teacher3"]
					,"emp_full_nm"	=> $row["emp_full_nm3"]
					,"checked"		=> $row["emp_checked3"]
				);
			}
			if ($row["training_teacher4"] != "") {
				$arr[] = array(
					"apv_order"		=> $row["apv_order"]
					,"emp_id"		=> $row["training_teacher4"]
					,"emp_full_nm"	=> $row["emp_full_nm4"]
					,"checked"		=> $row["emp_checked4"]
				);
			}
			if ($row["training_teacher5"] != "") {
				$arr[] = array(
					"apv_order"		=> $row["apv_order"]
					,"emp_id"		=> $row["training_teacher5"]
					,"emp_full_nm"	=> $row["emp_full_nm5"]
					,"checked"		=> $row["emp_checked5"]
				);
			}
		}
		return $arr;

	}

	// 承認者取得　課題提出先
	function get_training_theme_present($theme_id){

		$sql .= "SELECT ";
		$sql .= " theme.theme_present ";
		$sql .= " ,emp.emp_lt_nm || ' ' || emp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " cl_mst_inside_training_theme theme ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  theme.theme_present = emp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  emp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " theme.theme_id = '$theme_id' ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　所属長　部署取得
	function get_supervisor_class() {

		$sql  = "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_supervisor_class ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "class_division");

	}

	// 承認者取得　所属長(部署指定無)
	function get_supervisor_apv_class_none($emp_id) {

		$sql  = "SELECT ";
		$sql .= " apvemp.emp_id ";
		$sql .= " ,apvemp.emp_lt_nm || ' ' || apvemp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " empmst apvemp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  apvemp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " apvemp.emp_id != '$emp_id' ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   visor.st_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_mst_supervisor_st visor ";
		$sql .= "  WHERE ";
		$sql .= "   apvemp.emp_st = visor.st_id ";
		$sql .= " ) ";
		$sql .= "ORDER BY ";
		$sql .= " apvemp.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　所属長(部署指定有)
	function get_supervisor_apv($emp_id,$class_division){

		$sql  = "SELECT ";
		$sql .= " empid.emp_id ";
		$sql .= " ,empnm.emp_lt_nm || ' ' || empnm.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " ( ";
		$sql .= "  SELECT ";
		$sql .= "   empwk.emp_id ";
		$sql .= "  FROM ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplemp.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplemp.emp_room IS NULL ";
			$sql .= "       OR aplemp.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
				$sql .= "      AND aplemp.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
				$sql .= "      AND ( ";
				$sql .= "       aplemp.emp_room IS NULL ";
				$sql .= "       OR aplemp.emp_room = apvcon.emp_room ";
				$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvcon.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_supervisor_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "   ) empwk ";
		$sql .= "  WHERE ";
		$sql .= "   empwk.emp_id != '$emp_id' ";
		$sql .= "  GROUP BY ";
		$sql .= "   empwk.emp_id ";
		$sql .= " ) empid ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst empnm ";
		$sql .= " ON ";
		$sql .= "  empid.emp_id = empnm.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  empnm.emp_st = st.st_id ";
		$sql .= "ORDER BY ";
		$sql .= " empid.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　看護副部長(組織)
	function get_nurse_deputy_manager_apv_class_none($emp_id){

		$sql  = "SELECT ";
		$sql .= " dep.emp_id ";
		$sql .= " ,apvemp.emp_lt_nm || ' ' || apvemp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " empmst aplemp ";
		$sql .= " INNER JOIN ";
		$sql .= "  cl_mst_nurse_deputy_manager dep ";
		$sql .= " ON ";
		$sql .= "  dep.class_division = 0 ";
		$sql .= "  AND dep.emp_id != '$emp_id' ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst apvemp ";
		$sql .= " ON ";
		$sql .= "  dep.emp_id = apvemp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  apvemp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " aplemp.emp_id = '$emp_id' ";
		$sql .= "ORDER BY ";
		$sql .= " dep.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　看護副部長(事業所以下)
	function get_nurse_deputy_manager_apv($emp_id,$class_division){

		$sql  = "SELECT ";
		$sql .= " empid.emp_id ";
		$sql .= " ,apvemp.emp_lt_nm || ' ' || apvemp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " ( ";
		$sql .= "  SELECT ";
		$sql .= "   empwk.emp_id ";
		$sql .= "  FROM ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     dep.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_mst_nurse_deputy_manager dep ";
		$sql .= "     ON ";
		$sql .= "      dep.emp_id != '$emp_id' ";

		switch ($class_division){
			case 1:
				$sql .= "      AND( ";
				$sql .= "       dep.class_division = 1 ";
				$sql .= "       AND aplemp.emp_class = dep.class_id ";
				$sql .= "      ) ";
				break;
			case 2:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 2 ";
				$sql .= "       AND aplemp.emp_class = dep.class_id ";
				$sql .= "       AND aplemp.emp_attribute = dep.atrb_id ";
				$sql .= "      ) ";
				break;
			case 3:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 3 ";
				$sql .= "       AND aplemp.emp_class = dep.class_id ";
				$sql .= "       AND aplemp.emp_attribute = dep.atrb_id ";
				$sql .= "       AND aplemp.emp_dept = dep.dept_id ";
				$sql .= "      ) ";
				break;
			case 4:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 4 ";
				$sql .= "       AND aplemp.emp_class = dep.class_id ";
				$sql .= "       AND aplemp.emp_attribute = dep.atrb_id ";
				$sql .= "       AND aplemp.emp_dept = dep.dept_id ";
				$sql .= "       AND aplemp.emp_room = dep.room_id ";
				$sql .= "      ) ";
				break;
		}

		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     dep.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent con ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = con.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      cl_mst_nurse_deputy_manager dep ";
		$sql .= "     ON ";
		$sql .= "      dep.emp_id != '$emp_id' ";

		switch ($class_division){
			case 1:
				$sql .= "      AND( ";
				$sql .= "       dep.class_division = 1 ";
				$sql .= "       AND con.emp_class = dep.class_id ";
				$sql .= "      ) ";
				break;
			case 2:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 2 ";
				$sql .= "       AND con.emp_class = dep.class_id ";
				$sql .= "       AND con.emp_attribute = dep.atrb_id ";
				$sql .= "      ) ";
				break;
			case 3:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 3 ";
				$sql .= "       AND con.emp_class = dep.class_id ";
				$sql .= "       AND con.emp_attribute = dep.atrb_id ";
				$sql .= "       AND con.emp_dept = dep.dept_id ";
				$sql .= "      ) ";
				break;
			case 4:
				$sql .= "      AND ( ";
				$sql .= "       dep.class_division = 4 ";
				$sql .= "       AND con.emp_class = dep.class_id ";
				$sql .= "       AND con.emp_attribute = dep.atrb_id ";
				$sql .= "       AND con.emp_dept = dep.dept_id ";
				$sql .= "       AND con.emp_room = dep.room_id ";
				$sql .= "      ) ";
				break;
		}

		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "   ) empwk ";
		$sql .= "  GROUP BY ";
		$sql .= "   empwk.emp_id ";
		$sql .= " ) empid ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst apvemp ";
		$sql .= " ON ";
		$sql .= "  empid.emp_id = apvemp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  apvemp.emp_st = st.st_id ";
		$sql .= "ORDER BY ";
		$sql .= " empid.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　看護部長　部署取得
	function get_nurse_manager_class() {

		$sql  = "SELECT ";
		$sql .= " class_division ";
		$sql .= "FROM ";
		$sql .= " cl_mst_nurse_manager_class ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		return pg_fetch_result($sel, 0, "class_division");

	}

	// 承認者取得　看護部長(部署指定無)
	function get_nurse_manager_apv_class_none($emp_id) {

		$sql  = "SELECT ";
		$sql .= " apvemp.emp_id ";
		$sql .= " ,apvemp.emp_lt_nm || ' ' || apvemp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " empmst apvemp ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  apvemp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " apvemp.emp_id != '$emp_id' ";
		$sql .= " AND EXISTS( ";
		$sql .= "  SELECT ";
		$sql .= "   manager.st_id ";
		$sql .= "  FROM ";
		$sql .= "   cl_mst_nurse_manager_st manager ";
		$sql .= "  WHERE ";
		$sql .= "   apvemp.emp_st = manager.st_id ";
		$sql .= " ) ";
		$sql .= "ORDER BY ";
		$sql .= " apvemp.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　看護部長(部署指定有)
	function get_nurse_manager_apv($emp_id,$class_division){

		$sql .= "SELECT ";
		$sql .= " empid.emp_id ";
		$sql .= " ,empnm.emp_lt_nm || ' ' || empnm.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " ( ";
		$sql .= "  SELECT ";
		$sql .= "   empwk.emp_id ";
		$sql .= "  FROM ";
		$sql .= "   ( ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplemp.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplemp.emp_room IS NULL ";
			$sql .= "       OR aplemp.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_nurse_manager_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvemp.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvemp.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvemp.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvemp.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_nurse_manager_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvemp.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplemp.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplemp.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplemp.emp_room IS NULL ";
			$sql .= "       OR aplemp.emp_room = apvcon.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_nurse_manager_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "    UNION ALL ";
		$sql .= "    SELECT ";
		$sql .= "     apvemp.emp_id ";
		$sql .= "    FROM ";
		$sql .= "     empmst aplemp ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent aplcon ";
		$sql .= "     ON ";
		$sql .= "      aplemp.emp_id = aplcon.emp_id ";
		$sql .= "     INNER JOIN ";
		$sql .= "      concurrent apvcon ";
		$sql .= "     ON ";
		$sql .= "      aplcon.emp_class = apvcon.emp_class ";

		if ($class_division >= 2) {
			$sql .= "      AND aplcon.emp_attribute = apvcon.emp_attribute ";
		}

		if ($class_division >= 3) {
			$sql .= "      AND aplcon.emp_dept = apvcon.emp_dept ";
		}

		if ($class_division == 4) {
			$sql .= "      AND ( ";
			$sql .= "       aplcon.emp_room IS NULL ";
			$sql .= "       OR aplcon.emp_room = apvcon.emp_room ";
			$sql .= "      ) ";
		}

		$sql .= "      AND EXISTS( ";
		$sql .= "       SELECT ";
		$sql .= "        st_id ";
		$sql .= "       FROM ";
		$sql .= "        cl_mst_nurse_manager_st st ";
		$sql .= "       WHERE ";
		$sql .= "        apvcon.emp_st = st.st_id ";
		$sql .= "      ) ";
		$sql .= "     INNER JOIN ";
		$sql .= "      empmst apvemp ";
		$sql .= "     ON ";
		$sql .= "      apvcon.emp_id = apvemp.emp_id ";
		$sql .= "    WHERE ";
		$sql .= "     aplemp.emp_id = '$emp_id' ";
		$sql .= "   ) empwk ";
		$sql .= "  WHERE ";
		$sql .= "   empwk.emp_id != '$emp_id' ";
		$sql .= "  GROUP BY ";
		$sql .= "   empwk.emp_id ";
		$sql .= " ) empid ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst empnm ";
		$sql .= " ON ";
		$sql .= "  empid.emp_id = empnm.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  empnm.emp_st = st.st_id ";
		$sql .= "ORDER BY ";
		$sql .= " empid.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}

	// 承認者取得　審議会
	// 2012/07/19 Yamagawa upd(s)
	//function get_council_apv($emp_id){
	function get_council_apv($emp_id, $apply_id){
	// 2012/07/19 Yamagawa upd(e)

		$sql  = "SELECT ";
		$sql .= " con.emp_id ";
		$sql .= " ,emp.emp_lt_nm || ' ' || emp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		// 2012/07/19 Yamagawa add(s)
		if ($apply_id != '') {
			$sql .= " ,apv.emp_id AS checked ";
			$sql .= " ,apv.apv_order ";
		}
		// 2012/07/19 Yamagawa add(e)
		$sql .= "FROM ";
		$sql .= " cl_mst_setting_council con ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  con.emp_id = emp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  emp.emp_st = st.st_id ";
		// 2012/07/19 Yamagawa add(s)
		if ($apply_id != '') {
			$sql .= " LEFT JOIN ";
			$sql .= "  cl_applyapv apv ";
			$sql .= " ON ";
			$sql .= "  apv.apply_id = '$apply_id' ";
			$sql .= "  AND apv.emp_id = con.emp_id ";
			$sql .= "  AND apv.st_div = 11 ";
		}
		// 2012/07/19 Yamagawa add(e)
		$sql .= "WHERE ";
		$sql .= " con.emp_id != '$emp_id' ";
		$sql .= "ORDER BY ";
		$sql .= " con.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
				// 2012/07/19 Yamagawa add(s)
				,"checked"		=> $row["checked"]
				,"apv_order"	=> $row["apv_order"]
				// 2012/07/19 Yamagawa add(e)
			);
		}
		return $arr;

	}

	// 承認者取得　看護教育委員会
	function get_nurse_education_committee_apv($emp_id){

		$sql .= "SELECT ";
		$sql .= " comm.emp_id ";
		$sql .= " ,emp.emp_lt_nm || ' ' || emp.emp_ft_nm AS emp_full_nm ";
		$sql .= " ,st.st_nm ";
		$sql .= "FROM ";
		$sql .= " cl_mst_nurse_education_committee comm ";
		$sql .= " INNER JOIN ";
		$sql .= "  empmst emp ";
		$sql .= " ON ";
		$sql .= "  comm.emp_id = emp.emp_id ";
		$sql .= " LEFT JOIN ";
		$sql .= "  stmst st ";
		$sql .= " ON ";
		$sql .= "  emp.emp_st = st.st_id ";
		$sql .= "WHERE ";
		$sql .= " comm.emp_id != '$emp_id' ";
		$sql .= "ORDER BY ";
		$sql .= " comm.emp_id ";

		$sel = select_from_table($this->_db_con,$sql,"",$this->file_name);
		if($sel == 0)
		{
			pg_close($this->_db_con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$arr = array();
		while($row = $row = pg_fetch_array($sel))
		{
			$arr[] = array(
				"emp_id"		=> $row["emp_id"]
				,"emp_full_nm"	=> $row["emp_full_nm"]
				,"st_nm"		=> $row["st_nm"]
			);
		}
		return $arr;

	}
	// 2012/03/03 Yamagawa add(e)

}
?>
