<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// ワークフロー権限のチェック
$check_auth = check_authority($session, 3, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// パラメータ
$file_ids_real = (!is_array($file_ids_real)) ? array() : $file_ids_real;

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

foreach ($file_ids_real as $wkfw_id) {
    $sql = "update wkfwmst_real set";
    $cond = "where wkfw_id = " . p($wkfw_id);
    $upd = update_set_table($con, $sql, array("wkfw_kill_flg"), array("t"), $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面を再表示
echo("<script type=\"text/javascript\">location.href = 'workflow_menu.php?session=$session'</script>");
