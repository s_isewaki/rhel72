<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$hiyari = check_authority($session, 48, $fname);
if ($hiyari == "0")
{
	showLoginPage();
	exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==============================
//ポストバック時の処理
//==============================
if( $postback != "")
{
	//==============================
	//デフォルト変更処理
	//==============================

	// トランザクションの開始
	pg_query($con, "begin");

	//全職種一覧の取得(論理未削除データのみ　※画面の表示ルールと同じ)
	$sql = "select * from jobmst where not job_del_flg order by order_no";
	$sel_job = select_from_table($con,$sql,"",$fname);

	//職種ループ(一覧用)
	$job_count = pg_numrows($sel_job);
	for($i=0;$i<$job_count;$i++)
	{

		//職種ID
		$job_id      = pg_result($sel_job,$i,"job_id");

		//デフォルト様式番号(画面パラメータより取得)
		$parmname = "default_eis_of_job".$job_id;
		$default_eis_no  = $$parmname;

		//割り当てがある場合
		if($default_eis_no != "")
		{
			//デフォルトフラグをクリア
			$sql = "update inci_set_job_link set";
			$set = array("default_flg");
			$setvalue = array("f");
			$cond = "where inci_set_job_link.job_id = '$job_id'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			//デフォルトフラグを設定
			$sql = "update inci_set_job_link set";
			$set = array("default_flg");
			$setvalue = array("t");
			$cond = "where inci_set_job_link.job_id = '$job_id' and inci_set_job_link.eis_no = '$default_eis_no'";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}

	}

	// トランザクションのコミット
	pg_query($con, "commit");


	//==============================
	//処理継続し、画面を再表示。
	//==============================
}






//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | 様式管理</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">

.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}

.in_list {border-collapse:collapse;}
.in_list td {border:#FFFFFF solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 外の枠 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="600" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">



<!-- 送信領域 START-->
<form name="form1" action="hiyari_easyinput_set_main.php" method="post">
<input type="hidden" name="postback" value="true">
<input type="hidden" name="session" value="<?=$session?>">


<!-- 一覧 START-->
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td width="120" align="center" bgcolor="#DFFFDC">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職種</font>
</td>
<td width="70" align="center" bgcolor="#DFFFDC">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期表示</font>
</td>
<td align="center" bgcolor="#DFFFDC">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式名称</font>
</td>
<td width="70" align="center" bgcolor="#DFFFDC">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">様式割当</font>
</td>
</tr>


<?

//職種一覧の取得(論理未削除データのみ　※様式割り当て済み職種が論理削除された場合には非表示=更新不可能。)
$sql = "select * from jobmst where not job_del_flg order by order_no";
$sel_job = select_from_table($con,$sql,"",$fname);

//職種ループ(一覧用)
$job_count = pg_numrows($sel_job);
for($i=0;$i<$job_count;$i++)
{

	//職種情報
	$job_id      = pg_result($sel_job,$i,"job_id");
	$job_name    = pg_result($sel_job,$i,"job_nm");
	$job_name_html = htmlspecialchars($job_name);

	//職種に割り当てられている様式を取得
	$sql = "";
	$sql .= " select t1.*,t2.eis_name from";
	$sql .= " (";
	$sql .= " select * from inci_set_job_link where job_id = $job_id";
	$sql .= " ) t1";
	$sql .= " left join ";
	$sql .= " (";
	$sql .= " select * from inci_easyinput_set where not del_flag ";
	$sql .= " ) t2";
	$sql .= " on t1.eis_no = t2.eis_no";
	$sql .= " order by t2.order_no";
	$sel_eis = select_from_table($con,$sql,"",$fname);
	$eis_count = pg_numrows($sel_eis);

	//1つもデフォルトテンプレートがない場合は先頭テンプレートをデフォルトとする。
	$is_first_default = true;//先頭デフォルトフラグ
	for($i2=0;$i2<$eis_count;$i2++)
	{
		$default_flg = pg_result($sel_eis,$i2,"default_flg");
		if($default_flg == "t")
		{
			$is_first_default = false;
			break;
		}
	}

?>

<tr height="22">

<td align="center" bgcolor="#FFFFFF">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$job_name_html?></font>
	<a href="hiyari_easyinput_set_job_link.php?session=<? echo($session) ?>&job_id=<? echo($job_id) ?>">
	</a>
</td>


<?
if($eis_count == 0)
{
	?>
	<td bgcolor="#FFFFFF" align="right">
		&nbsp;
	</td>
	<td bgcolor="#FFFFFF" align="left">
		<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">なし</font>
	</td>
	<?
}
else
{
	?>
	<td bgcolor="#FFFFFF" align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="in_list">
	<?
	//テンプレートループ(ラジオボタンリスト用)
	for($i2=0;$i2<$eis_count;$i2++)
	{

		$eis_no   = pg_result($sel_eis,$i2,"eis_no");
		$default_flg = pg_result($sel_eis,$i2,"default_flg");

		//選択判定
		$checked_element = "";
		if($default_flg == "t")//デフォルトフラグ=TRUEの場合
		{
			$checked_element = "checked";
		}
		elseif($i2==0 && $is_first_default)//先頭テンプレート＆先頭デフォルトフラグ=TRUEの場合
		{
			$checked_element = "checked";
		}

		?>
		<tr height="22">
		<td align="center"><input type="radio" name="default_eis_of_job<? echo($job_id); ?>" value="<? echo($eis_no); ?>" <? echo($checked_element); ?>></td>
		</tr>
		<?
	}
	?>
	</table>
	</td>

	<td bgcolor="#FFFFFF" align="left">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="in_list">
	<?
	//テンプレートループ(ラジオボタンリスト用)
	for($i2=0;$i2<$eis_count;$i2++)
	{

		$eis_name = pg_result($sel_eis,$i2,"eis_name");
		$eis_name_html = htmlspecialchars($eis_name);

		?>
		<tr height="22">
		<td align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($eis_name_html); ?></font></td>
		</tr>
		<?
	}
	?>
	</table>
	</td>
<?
}
?>


<td align="center" bgcolor="#FFFFFF">
	<input type="button" onclick="location.href='hiyari_easyinput_set_job_link.php?session=<? echo($session) ?>&job_id=<? echo($job_id) ?>'" value="追加">
</td>


</tr>
<?
}
?>
</table>
<!-- 一覧 END -->


<!-- 送信ボタン START -->
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr align="left" height="22">
<td>
	<nobr><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※報告書様式を複数使用する場合は初期表示を指定してください。 </font></nobr>
</td>
<td align="right">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><input type="submit" value="初期表示更新"></font>
</td>
</tr>
</table>
<!-- 送信ボタン END -->



</form>
<!-- 送信領域 END -->



</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<!-- 外の枠 END -->

</td>
</tr>
</table>
<!-- 本体 END -->

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</td>
</tr>
</table>
<!-- BODY全体 END -->

</body>
</html>
<?



//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>

