<?php
ob_start();
ini_set("max_execution_time", 0);

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("duty_shift_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_diary_sfc_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj_sfc = new duty_shift_diary_sfc_class($con, $fname);
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

///-----------------------------------------------------------------------------
// 勤務シフトグループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj_sfc->get_group_info($group_id);
$sfc_group_code = $group_array["sfc_group_code"];
$arr_emp_info = $obj_sfc->get_emp_info_for_sfc($group_id, $duty_yyyy, $duty_mm, $group_array);

$arr_data = $obj_sfc->get_sfc_csv_data($group_id, $duty_yyyy, $duty_mm, $arr_emp_info, $group_array, "1");

//変換用ファイルを取得する
//シェルスクリプトから設定ディレクトリを取得
$sfc_set_dir = "";
//optディレクトリ確認
$sh_name = "/var/www/html/comedix/opt/duty_shift_sfc_csv_nec.sh";
if (!file_exists($sh_name)) {
    $sh_name = "/var/www/html/duty_shift_sfc_csv.sh";
}
$lines = file($sh_name);
foreach ($lines as $line) {
	if (strpos($line, "sfc_set_dir=") !== false) {
		$wk_item = split("'", $line);
		$sfc_set_dir = $wk_item[1];
		break;
	}
}

$arr_convert_id = array();
if ($sfc_set_dir != "") {
	$csv_file_name = $sfc_set_dir."/cmxsfc-convert.txt";
	$arr_convert_id = $obj_sfc->get_convert_id($csv_file_name);
}

// 情報をCSV形式で取得
$csv = get_list_csv($con, $session, $arr_emp_info, $arr_data, $arr_convert_id);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$month = sprintf("%02d", $duty_mm);
$file_name = "KINJ$duty_yyyy$month$sfc_group_code.txt";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------


/**
 * 管理日誌（SFC形式）情報をCSV形式で取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $session セッション
 * @param mixed $arr_emp_info 職員情報
 * @param mixed $arr_data 職員、日別のSFCコードの配列
 * @param mixed $arr_convert_id 職員ID変換用配列
 * @return mixed CSV形式の管理日誌（SFC形式）情報
 *
 */
function get_list_csv($con, $session, $arr_emp_info, $arr_data, $arr_convert_id) {

	$buf = "";

    //optディレクトリ確認
    $emp_id_prefix = "99";
    $sh_name = "/var/www/html/comedix/opt/duty_shift_sfc_csv_nec.sh";
    if (file_exists($sh_name)) {
        $emp_id_prefix = "00";
    }
	for($i=0;$i<count($arr_emp_info);$i++){
		//職員ID
		//変換用ファイルにIDがある場合は、SFC職員コードを設定
		$wk_emp_id = $arr_emp_info[$i]["emp_personal_id"];
		if ($arr_convert_id[$wk_emp_id] != "") {
			$wk_emp_id = $arr_convert_id[$wk_emp_id];
		} else {
			//ない場合は、"99"or"00"+職員IDの下6桁
			$wk_right_id = substr("000000".$wk_emp_id, -6);
            $wk_emp_id = $emp_id_prefix.$wk_right_id;
		}
		$buf .= $wk_emp_id;
		for ($j=1;$j<=31;$j++) {
			$sfc_code = $arr_data[$i][$j];
			if ($sfc_code == "") {
				$sfc_code = "0000";
			}
			$buf .= ",".$sfc_code;
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}
