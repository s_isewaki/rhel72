<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 26, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($job_name == "") {
	echo("<script type=\"text/javascript\">alert('職種名を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($job_name) > 60) {
	echo("<script type=\"text/javascript\">alert('職種名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種情報を更新
$sql = "update jobmst set";
$set = array("job_nm", "link_key");
$setvalue = array($job_name, $link_key);
$cond = "where job_id = '$job_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 職種一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'job_menu.php?session=$session';</script>");
?>
