<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("maintenance_menu_list.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// 変更履歴を取得
if (!empty($_REQUEST["calendarname_time_history_id"])) {
    $calendarname_time_history_id = pg_escape_string($_REQUEST["calendarname_time_history_id"]);
    $sql = "SELECT * FROM calendarname_time_history WHERE calendarname_time_history_id=$calendarname_time_history_id";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $hist = pg_fetch_assoc($sel);
    $day1_hour = substr($hist["day1_time"], 0, 2);
    $day1_min = substr($hist["day1_time"], 2, 2);
    $am_hour = substr($hist["am_time"], 0, 2);
    $am_min = substr($hist["am_time"], 2, 2);
    $pm_hour = substr($hist["pm_time"], 0, 2);
    $pm_min = substr($hist["pm_time"], 2, 2);
}
else {
    $hist = array("histdate" => date('Y-m-d H:i:s'));
    $day1_hour = "";
    $day1_min = "";
    $am_hour = "";
    $am_min = "";
    $pm_hour = "";
    $pm_min = "";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | カレンダー | 所定労働時間履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="./maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="./calendar_name.php?session=<? echo($session); ?>"><b>カレンダー</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="./img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="calendar_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="calendar_name_time_history_form.php?session=<? echo($session); ?>&calendarname_time_history_id=<? echo($calendarname_time_history_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>所定労働時間履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="calendar_year.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年間カレンダー</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="calendar_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<br />
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■所定労働時間 変更履歴の追加/変更</font><br />
<br />
<form action="calendar_name_time_history_submit.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働時間：</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="day1_hour"><? show_hour_options_0_23($day1_hour); ?></select>時間<select name="day1_min"><? show_min_options_00_59($day1_min); ?></select>分
    </font></td>
</tr>
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午前所定時間：</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="am_hour"><? show_hour_options_0_23($am_hour); ?></select>時間<select name="am_min"><? show_min_options_00_59($am_min); ?></select>分
    </font></td>
</tr>
<tr height="22">
    <td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">半日午後所定時間：</font></td>
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="pm_hour"><? show_hour_options_0_23($pm_hour); ?></select>時間<select name="pm_min"><? show_min_options_00_59($pm_min); ?></select>分
    </font></td>
</tr>
<tr height="22">
    <td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変更日時</font></td>
    <td>
        <input type="text" name="histdate" value="<?=substr($hist["histdate"],0,19)?>" size="25"><br />
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">　9999-99-99 99:99:99の形式で入力してください</font>
    </td>
</tr>
</table>

<p>
<button type="submit">登録</button>
</p>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="calendarname_time_history_id" value="<? echo($calendarname_time_history_id); ?>">
</form>

</td>
</tr>
</table>

</body>
</html>
<? pg_close($con); ?>