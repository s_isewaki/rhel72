<?php
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once('class/Cmx/Model/SystemConfig.php');

require_once("hiyari_common.ini");
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_header_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//デザイン（1=旧デザイン、2=新デザイン）
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// ページ番号の設定
if ($page == "") {$page = 1;}

// データベースに接続
$con = connect2db($fname);

define("ROWS_PER_PAGE", 20);

//=========
//=====================
// ニュース一覧
//==============================
require_once("show_class_name.ini");

$offset = ROWS_PER_PAGE * ($page - 1);
$limit = ROWS_PER_PAGE;

$sql = "select * from inci_news";
$cond = "where news_del_flg = 'f' order by news_date desc, news_id desc offset $offset limit $limit";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

$news_list = array();
while ($row = pg_fetch_array($sel)) {
    $tmp_news_id = $row["news_id"];
    $tmp_news_date = $row["news_date"];
    $tmp_news_title = htmlspecialchars($row["news_title"]);
    $tmp_news_category = $row["news_category"];
    $tmp_news_begin = $row["news_begin"];
    $tmp_news_end = $row["news_end"];
    $tmp_record_flg = $row["record_flg"];
    $style = get_style_by_marker_hiyari_management($row["marker"]);

		$tmp_news_date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_date);
		$tmp_news_begin = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_begin);
		$tmp_news_end = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_end);

		switch ($tmp_news_category) {
		case "1":
			$tmp_news_category_name = "全館";
			break;
		case "2":
			$tmp_news_category_name = $class_called;
			break;
		case "3":
			$tmp_news_category_name = "職種";
			break;
		}

    $news_list[$tmp_news_id]['news_date'] = $tmp_news_date;
    $news_list[$tmp_news_id]['news_title'] = $tmp_news_title;
    $news_list[$tmp_news_id]['category'] = $tmp_news_category_name;
    $news_list[$tmp_news_id]['news_begin'] = $tmp_news_begin;
    $news_list[$tmp_news_id]['news_end'] = $tmp_news_end;
    $news_list[$tmp_news_id]['record_flg'] = $tmp_record_flg;
    $news_list[$tmp_news_id]['style'] = $style;
}

//==============================
//ページング
//==============================
$sql = "select count(*) from inci_news";
$cond = "where news_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
}
$row_count = pg_fetch_result($sel, 0, 0);

$page_max=0;
$last_page=0;
$next_page=0;
if ($row_count > ROWS_PER_PAGE) {
    $page_max = ceil($row_count / ROWS_PER_PAGE);
    if ($page > 1) {
        $last_page = $page - 1;
    }
    if ($page < $page_max) {
        $next_page = $page + 1;
    }
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
if ($design_mode == 1){
    $smarty->assign("tab_info", $header->get_news_tab_info());
    $smarty->assign("is_news_menu", true);
}
else{
    $smarty->assign("tab_info", $header->get_user_tab_info());
}

//お知らせデータ
$smarty->assign("news_list", $news_list);

//ページング
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
if ($design_mode == 1){
    if ($last_page>0){
      $smarty->assign("last_page", $last_page);
    }
    if ($next_page>0){
      $smarty->assign("next_page", $next_page);
    }
}
else{
    if ($page_max > 1){
        $page_list = array();
        $page_stt = max(1, $page - 3);
        $page_end = min($page_stt + 6, $page_max);
        $page_stt = max(1, $page_end - 6);
        for ($i=$page_stt; $i<=$page_end; $i++){
            $page_list[] = $i;
        }
        $smarty->assign("page_list", $page_list);
    }
}

if ($design_mode == 1){
    $smarty->display("hiyari_news_menu1.tpl");
}
else{
    $smarty->display("hiyari_news_menu2.tpl");
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);

//========================================================================================================================================================================================================
// 以下、関数
//========================================================================================================================================================================================================
// ラインマーカーのスタイル定義を取得
function get_style_by_marker_hiyari_management($marker) {
	switch ($marker) {
		case 1: return " style=\"background-color:red;color:white;padding:2px;\"";// 赤
		case 2: return " style=\"background-color:aqua;color:blue;padding:2px;\"";// 青
		case 3: return " style=\"background-color:yellow;color:blue;padding:2px;\"";// 黄
		case 4: return " style=\"background-color:lime;color:blue;padding:2px;\"";// 緑
		case 5: return " style=\"background-color:fuchsia;color:white;padding:2px;\"";// ピンク
	default:return "";
	}
}
