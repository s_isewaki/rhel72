<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($path == "2") ? 62 : 1;
$check_auth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if (!is_array($category_ids)) {
	$category_ids = array();
}
if (!is_array($theme_ids)) {
	$theme_ids = array();
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin");

// 選択されたカテゴリをループ
foreach ($category_ids as $tmp_category_id) {

	// カテゴリ情報を論理削除
	$sql = "update bbscategory set";
	$set = array("bbscategory_del_flg");
	$setvalue = array("t");
	$cond = "where bbscategory_id = $tmp_category_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// カテゴリ配下のテーマ情報を取得
	$sql = "select bbsthread_id from bbsthread";
	$cond = "where bbscategory_id = $tmp_category_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// カテゴリ配下のテーマも選択されたものとみなす
	while ($row = pg_fetch_array($sel)) {
		$tmp_theme_id = $row["bbsthread_id"];
		if (!in_array($tmp_theme_id, $theme_ids)) {
			$theme_ids[] = $tmp_theme_id;
		}
	}
}

// 選択されたテーマをループ
foreach ($theme_ids as $tmp_theme_id) {

	// テーマ情報を論理削除
	$sql = "update bbsthread set";
	$set = array("bbsthread_del_flg");
	$setvalue = array("t");
	$cond = "where bbsthread_id = $tmp_theme_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 添付ファイル情報を削除
	$sql = "delete from bbsfile";
	$cond = "where bbsthread_id = $tmp_theme_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 添付ファイルの削除
	foreach (glob("bbs/{$tmp_theme_id}_*.*") as $tmpfile) {
		unlink($tmpfile);
	}
}

// 30分以上前に保存された一時ファイルを削除
foreach (glob("bbs/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 一覧画面に遷移
if ($path == "2") {
	echo("<script type=\"text/javascript\">location.href = 'bbs_admin_menu.php?session=$session&sort=$sort&show_content=$show_content';</script>");
} else {
	echo("<script type=\"text/javascript\">location.href = 'bbsthread_menu.php?session=$session&sort=$sort&show_content=$show_content';</script>");
}
?>
