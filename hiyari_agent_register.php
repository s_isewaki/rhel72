<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("show_select_values.ini");
require_once("hiyari_yui_calendar.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once("aclg_set.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

switch ($mode) {
	case "regist":
		// 登録処理
		$arr_agent_emp_id = array();
		if($disp_area_ids_1 != "")
		{
			$arr_agent_emp_id = array_merge($arr_agent_emp_id, split(",", $disp_area_ids_1));
		}


		if($start_date == "")
		{
			$start_ymd = null;
		}
		else
		{
			$start_ymd = substr($start_date, 0, 4);
			$start_ymd .= substr($start_date, 5, 2);
			$start_ymd .= substr($start_date, 8, 2);
		}
		if($end_date == "")
		{
			$end_ymd = null;
		}
		else
		{
			$end_ymd = substr($end_date, 0, 4);
			$end_ymd .= substr($end_date, 5, 2);
			$end_ymd .= substr($end_date, 8, 2);	
		}
		regist_inci_auth_agent($con, $fname, $emp_id, $arr_agent_emp_id, $start_ymd, $end_ymd);
		break;
	default:
		break;
}



// ログインユーザ情報取得
$emp_id = get_emp_id($con, $session, $fname);
$emp_nm = get_emp_kanji_name($con,$emp_id,$fname);

// 代行者情報取得
$arr_target = array();
$start_date = "";
$end_date = "";
$sel = search_inci_auth_agent($con, $fname, $emp_id);
$i = 0;
while ($row_target = pg_fetch_array($sel)) {

	if($i == 0) {
		$start_ymd = $row_target["start_date"];
		$end_ymd = $row_target["end_date"];
	}
	$tmp_target_id = $row_target["agent_emp_id"];
	$tmp_target_nm = $row_target["emp_name"];
	$arr_target[$tmp_target_id] = $tmp_target_nm;

	$i++;
}
if($start_ymd != "") {
	$start_date = substr($start_ymd, 0, 4);
	$start_date .= "/".substr($start_ymd, 4, 2);
	$start_date .= "/".substr($start_ymd, 6, 2);
}
if($end_ymd != "") {
	$end_date = substr($end_ymd, 0, 4);
	$end_date .= "/".substr($end_ymd, 4, 2);
	$end_date .= "/".substr($end_ymd, 6, 2);
}

// 代行者職員
foreach($arr_target as $id => $name)
{
  if($agent_ids != ""){
    $agent_ids    .= ",";
    $agent_names  .= ",";
  }
  $agent_ids   .= $id;
  $agent_names .= $name;
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);
$smarty->assign("mode", $mode);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());
$smarty->assign("tab_info", $header->get_user_tab_info());

//代行者データ
$smarty->assign("agent_ids", $agent_ids);
$smarty->assign("agent_names", $agent_names);

$smarty->assign("start_date", $start_date);
$smarty->assign("end_date", $end_date);

$smarty->assign("emp_id", $emp_id);
$smarty->assign("emp_nm", $emp_nm);

if ($design_mode == 1){
    $smarty->display("hiyari_agent_register1.tpl");
}
else{
    $smarty->display("hiyari_agent_register2.tpl");
}

//==============================
//DBコネクション終了
//==============================
pg_close($con);

//========================================================================================================================================================================================================
// 以下、関数
//========================================================================================================================================================================================================
//=================================
// 代行者情報取得
//=================================
function search_inci_auth_agent($con, $fname, $emp_id) {

	$sql = "select a.agent_emp_id, a.start_date, a.end_date, b.emp_name from inci_auth_agent a ";
	$sql .= "inner join (select a.emp_id, a.emp_lt_nm || ' ' || a.emp_ft_nm as emp_name from empmst a inner join authmst b on a.emp_id = b.emp_id where emp_del_flg = 'f') b on a.agent_emp_id = b.emp_id ";
	$cond = "where deciding_emp_id = '$emp_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;	
}

//=================================
// 代行者情報登録
//=================================
function regist_inci_auth_agent($con, $fname, $emp_id, $arr_agent_emp_id, $start_date, $end_date) {

	$sql = "delete from inci_auth_agent";
	$cond = "where deciding_emp_id = '$emp_id'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_exec($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	foreach($arr_agent_emp_id as $id) {
		$sql = "insert into inci_auth_agent (deciding_emp_id, agent_emp_id, start_date, end_date) values (";
		$content = array($emp_id, $id, $start_date, $end_date);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_exec($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}
//=================================
// 部門オプションを出力
//=================================
function show_class_options($arr_class, $class) {
	echo("<option value=\"-\">----------");
	foreach ($arr_class as $tmp_class_id => $arr) {
		$tmp_class_name = $arr["name"];
		echo("<option value=\"$tmp_class_id\"");
		if ($class === $tmp_class_id) {
			echo(" selected");
		}
		echo(">$tmp_class_name\n");
	}
}
?>