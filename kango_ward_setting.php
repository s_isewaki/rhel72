<?
require_once("about_comedix.php");
require_once("kango_common.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_KANRI, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback	= isset($_REQUEST["is_postback"])	? $_REQUEST["is_postback"]		: null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	if($postback_mode == "save")
	{
		//==============================
		//トランザクション開始
		//==============================
		pg_query($con,"begin transaction");

		//==============================
		//更新前の設定情報を取得
		//==============================
		$ward_setting_before_update = get_ward_setting($con, $fname, $bldg_cd, $ward_cd);
		
		//==============================
		//DB更新
		//==============================
		set_ward_setting($con, $fname, $bldg_cd, $ward_cd, $time1_must_flg, $time2_must_flg, $time3_must_flg, $standard_code);
		
		//==============================
		//評価票記録の更新
		//==============================
		
		//施設基準７対１に関係する変更が行われた場合
		if(
			   ($ward_setting_before_update['$standard_code'] == 1 && $standard_code != 1)
			|| ($ward_setting_before_update['$standard_code'] != 1 && $standard_code == 1)
			)
		{
			//病棟に属する評価記録を取得
			$sql = "select pt_id,input_date,input_time from nlcs_eval_record where bldg_cd = {$bldg_cd} and ward_cd = {$ward_cd}";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$update_target_record_info = pg_fetch_all($sel);
			
			//病棟に属する評価記録の７対１入院基準該当フラグを更新する。
			foreach($update_target_record_info as $row)
			{
				set_pt_7_1_flg($con,$fname,$row['pt_id'],$row['input_date'],$row['input_time']);
			}
		}
		
		//==============================
		//コミット
		//==============================
		pg_query($con, "commit");
	}
}


//==============================
//初期アクセス時の処理
//==============================
else
{
	//==============================
	//初期表示する病棟を特定
	//==============================
	$top_ward = get_top_ward($con,$fname);
	$bldg_cd = $top_ward['bldg_cd'];
	$ward_cd = $top_ward['ward_cd'];
}


//==============================
//施設基準コード一覧
//==============================

$sql = "select * from nlcs_standard_mst order by disp_index";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$standard_list = pg_fetch_all($sel);



//==============================
//病棟運用設定を取得
//==============================
$ward_setting = get_ward_setting($con, $fname, $bldg_cd, $ward_cd);
$time1_must_flg = $ward_setting['time1_must_flg'];
$time2_must_flg = $ward_setting['time2_must_flg'];
$time3_must_flg = $ward_setting['time3_must_flg'];
$standard_code  = $ward_setting['standard_code'];



//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "病棟設定";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//画面起動時の処理
function initPage()
{
	init_ward_select();
}

//病棟変更時の処理
function ward_changed(bldg_cd,ward_cd)
{
	document.mainform.postback_mode.value = "ward_changed";
	document.mainform.submit();
}

//更新ボタン押下時の処理
function save()
{
	document.mainform.postback_mode.value = "save";
	document.mainform.submit();
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname,true)
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">



<!-- 病棟選択・更新ボタン START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?
show_ward_select($con,$fname,$bldg_cd,$ward_cd);
?>
</td>
<td align="right">
<input type="button" value="更新" onclick="save()">
</td>
</tr>
</table>
<!-- 病棟選択・更新ボタン END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 評価時間帯設定一覧 START -->
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価時間帯設定</font>
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>時間帯</font></td>
<td bgcolor='#bdd1e7'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>条件</font></td>
<td bgcolor='#bdd1e7'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>アセスメント時刻</font></td>
<td bgcolor='#bdd1e7'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>範囲(コメント)</font></td>
</tr>
<tr>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>1(深夜勤)</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<select name="time1_must_flg">
	<option value="f" <? if($time1_must_flg == "f"){echo("selected");}?>>任意</option>
	<option value="t" <? if($time1_must_flg == "t"){echo("selected");}?>>必須</option>
	</select>
	</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>7時台</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>0時台から7時台の8時間</font></td>
</tr>
<tr>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>2(日勤)</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>必須<input type="hidden" name="time2_must_flg" value="t"></font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>14時台</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>8時台から15時台の8時間</font></td>
</tr>
<tr>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>3(準夜勤)</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<select name="time3_must_flg">
	<option value="f" <? if($time3_must_flg == "f"){echo("selected");}?>>任意</option>
	<option value="t" <? if($time3_must_flg == "t"){echo("selected");}?>>必須</option>
	</select>
	</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>23時台</font></td>
<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>16時台から23時台の8時間</font></td>
</tr>
</table>
<!-- 評価時間帯設定一覧 END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 施設基準 START -->
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>施設基準</font>
<br>
<select name="standard_code">
<?
foreach($standard_list as $standard_list1)
{
	$tmp_standard_code = $standard_list1['standard_code'];
	$tmp_standard_name = $standard_list1['standard_name'];
	?>
	<option value="<?=$tmp_standard_code?>" <?if($standard_code == $tmp_standard_code){echo("selected");}?>><?=h($tmp_standard_name)?></option>
	<?
}
?>
</select>
<!-- 施設基準 END -->






</form>
<!-- 全体 END -->

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>


