<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$chechauth = check_authority($session, 34, $fname);
if ($chechauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 診断名を削除
$sql = "delete from dishist";
$cond = "where ptif_id = '$pt_id' and dishist_emp_id = '$r_emp' and dishist_reg_dt = '$r_date' and dishist_reg_tm = '$r_time'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// プロブレムリスト画面を再表示
echo("<script type=\"text/javascript\">location.href = 'disease_detail.php?session=$session&pt_id=$pt_id';</script>");
?>
