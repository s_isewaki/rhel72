<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");
require("menu_common.ini");
require("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 55, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 当直・外来分担表設定権限の取得
$allot_setting_auth = check_authority($session, 53, $fname);

// 文書フォルダがなければ作成
if (!is_dir("intra")) {mkdir("intra", 0755);}
if (!is_dir("intra/allot")) {mkdir("intra/allot", 0755);}

// データベースに接続
$con = connect2db($fname);

// 文書一覧を取得
$sql = "select allotfcl.allotfcl_id, allotfcl.name, allot.date, allot.update_time from allotfcl left join (select allotfcl_id, date, update_time from allot where date = (select max(date) from allot allotsub where allotsub.allotfcl_id = allot.allotfcl_id)) allot on allotfcl.allotfcl_id = allot.allotfcl_id";
$cond = "where allotfcl.del_flg = 'f' order by allotfcl.allotfcl_id";
$sel_allot = select_from_table($con, $sql, $cond, $fname);
if ($sel_allot == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 文書登録可能な施設を取得
$sql = "select allotfcl_id from allotemp";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_fcl = select_from_table($con, $sql, $cond, $fname);
if ($sel_fcl == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$admin_fcls = array();
while ($row = pg_fetch_array($sel_fcl)) {
	$admin_fcls[] = $row["allotfcl_id"];
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$menu2 = pg_fetch_result($sel, 0, "menu2");
$menu2_4 = pg_fetch_result($sel, 0, "menu2_4");
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
// 社会福祉法人の場合、「・外来」を削除
if ($profile_type == PROFILE_TYPE_WELFARE) {
	$menu2_4 = mbereg_replace("・外来", "", $menu2_4);
}
?>
<title>イントラネット | <? echo($menu2); ?> | <? echo($menu2_4); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($menu2); ?></b></a> &gt; <a href="intra_info_allot.php?session=<? echo($session); ?>"><b><? echo($menu2_4); ?></b></a></font></td>
<? if ($allot_setting_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="allot_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<form name="mainform" action="intra_info_allot_delete.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($menu2_4)); ?>" align="center" bgcolor="#5279a5"><a href="intra_info_allot.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($menu2_4); ?></b></font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設名</font></td>
<td width="22%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="26%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
<td width="7%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font></td>
<td width="7%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">履歴</font></td>
</tr>
<?
while ($row = pg_fetch_array($sel_allot)) {
	$fcl_id = $row["allotfcl_id"];
	$fcl_name = $row["name"];
	$ymd = $row["date"];
	$date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $ymd);
	$update_time = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $row["update_time"]);

	$paths = glob("intra/allot/{$fcl_id}_{$ymd}.*");
	$path = $paths[0];

	echo("<tr height=\"22\">\n");
	echo("<td>\n");
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$fcl_name</font></td>\n");
	echo("<td width=\"13\">");
	if (in_array($fcl_id, $admin_fcls)) {
		echo("<a href=\"javascript:void(0);\" onclick=\"window.open('intra_info_allot_register.php?session=$session&fcl_id=$fcl_id', 'newwin', 'width=640,height=480,scrollbars=yes');\"><img src=\"img/pencil.gif\" alt=\"登録\" width=\"13\" height=\"13\" border=\"0\"></a>");
	}
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
	echo("</td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$date</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$update_time</font></td>\n");
	if ($date != "") {
		echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"window.open('$path');\"></td>\n");
		echo("<td align=\"center\"><input type=\"button\" value=\"履歴\" onclick=\"location.href = 'intra_info_allot_history.php?session=$session&fcl_id=$fcl_id';\"></td>\n");
	} else {
		echo("<td></td>\n");
		echo("<td></td>\n");
	}
	echo("</tr>\n");
}
?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
