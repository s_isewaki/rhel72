<?php

require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$check_auth = check_authority($session, 20, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if (strlen($report) > 40) {
    echo("<script type=\"text/javascript\">alert('メドレポートの表示メニュー名が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// 登録値の編集
if ($timecard_menu_flg == "") {
    $timecard_menu_flg = "f";
}
if ($calendar_menu_flg == "") {
    $calendar_menu_flg = "f";
}
if ($quick_menu_flg == "") {
    $quick_menu_flg = "f";
}
if ($basic_menu_flg == "") {
    $basic_menu_flg = "f";
}
//XX if ($community_menu_flg == "") {
    $community_menu_flg = "f";
//XX }
if ($business_menu_flg == "") {
    $business_menu_flg = "f";
}
if ($license_menu_flg == "") {
    $license_menu_flg = "f";
}
if ($LogOutMenu == "") {
    $LogOutMenu = "f";
}


// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 環境設定情報を更新
$sql = "update config set";
$set = array("timecard_menu_flg", "calendar_menu_flg", "quick_menu_flg", "basic_menu_flg", "community_menu_flg", "business_menu_flg", "license_menu_flg");
$setvalue = array($timecard_menu_flg, $calendar_menu_flg, $quick_menu_flg, $basic_menu_flg, $community_menu_flg, $business_menu_flg, $license_menu_flg);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// メニュー情報を更新
$sql = "delete from menuname";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$sql = "insert into menuname (report, shift) values (";
$content = array($report, $shift);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// ヘッダメニュー用system_config
$conf = new Cmx_SystemConfig();
$LogOutMenu = $conf->set("headmenu.logout", $LogOutMenu);


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 全画面をリフレッシュ
$f_url = urlencode("config_sidemenu.php?session=$session");
echo("<script type=\"text/javascript\">parent.location.href = 'main_menu.php?session=$session&f_url=$f_url';</script>");