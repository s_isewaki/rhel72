<?
/*************************************************************************
お知らせ管理
  オプション・マイページの表示条件、一括変更・登録
*************************************************************************/
require("about_session.php");
require("about_authority.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

if ($default_category == "") {
	$default_category = "1";
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ（2:「マイページの表示条件、一括変更」を変更）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,"2");

// トランザクションを開始
pg_query($con, "begin");


// オプション情報を更新
$sql = "UPDATE newsconfig SET";
$set = array("default_read_status");
$setvalue = array($sele_stat);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0 or pg_num_rows($upd) == 0) {
	// UPDATEに失敗したらINSERT
	$sql = "INSERT INTO newsconfig (default_record_flg,mypage_read_flg,default_read_status,rows_per_page, everyone) VALUES (";
	$content = array("f","f",$sele_stat,"20","t");
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		// INSERTに失敗したらエラー
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}


// ユーザ設定一括更新
$sql = "UPDATE newsempoption SET";
$set = array("last_status");
$setvalue = array($sele_stat);
$cond = "";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'news_option.php?session=$session&exe=1';</script>");
?>