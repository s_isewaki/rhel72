<?php
require_once("about_comedix.php");
require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );

// データベースに接続
$con = connect2db($fname);

// 病院名
if ($data_hospital_id != "") {
	
	//検索処理
	$sql=	"SELECT hospital_name, hospital_directer FROM fplus_hospital_master ";
	$cond = "WHERE hospital_id = $data_hospital_id AND del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hospital_name = pg_fetch_result($sel, 0, "hospital_name");
}
// 切断
pg_close($con);

// オブジェクト
$PHPWord = new PHPWord();

//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");

// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('緊急報告受付','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700);
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700);
$sectionStyle->setMarginBottom(1700);

// タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));
$section->addText(mb_convert_encoding('緊急報告受付','UTF-8','EUC-JP'), 'title_pStyle', 'title_align');

$section->addTextBreak(1);

// 発生年月日
if ($data_occurrences_date != "") {
	$y = substr($data_occurrences_date, 0, 4);
	$m = substr($data_occurrences_date, 5, 2);
	$d = substr($data_occurrences_date, 8, 2);
	$data_occurrences_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('1 発生年月日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_occurrences_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 緊急連絡日
if ($data_immediate_connect_date != "") {
	$y = substr($data_immediate_connect_date, 0, 4);
	$m = substr($data_immediate_connect_date, 5, 2);
	$d = substr($data_immediate_connect_date, 8, 2);
	$data_immediate_connect_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('2 緊急連絡日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_immediate_connect_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 発生病院名
$section->addText(mb_convert_encoding('3 発生病院名 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($hospital_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 連絡者名
$section->addText(mb_convert_encoding('4 連絡者名 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_connection_person_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 対象患者属性
$section->addText(mb_convert_encoding('5 対象患者属性 ','UTF-8','EUC-JP'));

// ・氏名
$section->addText(mb_convert_encoding('  氏名：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_patient_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・性別
// 性別
if ($data_patient_sex[0]=="1") {
	$str = "男性";
} else if ($data_patient_sex[0]=="2") {
	$str = "女性";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  性別：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($str,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・年齢
if ($data_patient_age != "") {
	$data_patient_age = $data_patient_age . "歳";
}
$section->addText(mb_convert_encoding('  年齢：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_patient_age,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

$section->addTextBreak(1);

// 事故概要
$section->addText(mb_convert_encoding('6 事故概要 ','UTF-8','EUC-JP'));

// ・タイトル
$section->addText(mb_convert_encoding('  [タイトル]','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding("  " . mb_convert_encoding($data_title,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・事故概要
$section->addText(mb_convert_encoding('  [事故概要]','UTF-8','EUC-JP'));
$ary_outline = preg_split("/\n/", $data_outline);
for($j=0;$j<count($ary_outline);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("　$ary_outline[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;

php?>
