<?
require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_common.ini");
require_once("about_authority.php");
require_once("about_session.php");
require_once("hiyari_show_news_refer_list.ini");
require_once("referer_common.ini");
require_once("hiyari_header_class.php");
require_once('class/Cmx/Model/SystemConfig.php');

define("ROWS_PER_PAGE_REFERRER_LIST", 20);

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 47, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//デザイン（1=旧デザイン、2=新デザイン）
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

// 処理モードの設定
define("LIST_REFERRED", 1);  // 参照済みリストを表示
define("LIST_UNREFERRED", 2);  // 未参照リストを表示
if ($ref_mode == "") {$ref_mode = LIST_REFERRED;}

// ページ番号の設定
if ($page == "") {$page = 1;}

// データベースに接続
$con = connect2db($fname);

// お知らせ情報の取得
$sql = "select news_title, news_category, class_id, job_id from inci_news";
$cond = "where news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$news_title = htmlspecialchars(pg_fetch_result($sel_news, 0, "news_title"));
$news_category = pg_fetch_result($sel_news, 0, "news_category");

// お知らせ対象者の取得用SQL
switch ($news_category) {
case "1":  // 全館
	$target_all_emp_sql = "select empmst.emp_id from empmst where exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
	break;

case "2":  // 部門
	$class_id = pg_fetch_result($sel_news, 0, "class_id");
	$target_all_emp_sql  = "select empmst.emp_id from empmst where emp_class = $class_id and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') ";
	$target_all_emp_sql .= "union ";
	$target_all_emp_sql .= "select concurrent.emp_id from concurrent where emp_class = $class_id and exists (select * from authmst where authmst.emp_id = concurrent.emp_id and authmst.emp_del_flg = 'f') ";

	break;

case "3":  // 職種
	$job_id = pg_fetch_result($sel_news, 0, "job_id");
	$target_all_emp_sql = "select empmst.emp_id from empmst where emp_job = $job_id and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f')";
	break;
}

//ファントルくんユーザー限定
$target_all_emp_sql = "select * from ($target_all_emp_sql) a natural inner join (select emp_id from authmst where emp_inci_flg) usable";

// お知らせ対象者数を取得
$sql = "select count(*) from ($target_all_emp_sql) a";
$sel_count_all = select_from_table($con, $sql, "", $fname);
if ($sel_count_all == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$count_all = pg_fetch_result($sel_count_all, 0, 0);

// 参照済みの職員数を取得
$referred_emp_sql = "select emp_id from inci_news_ref where news_id = $news_id";
$sql = "select count(*) from ($referred_emp_sql) a";
$cond = "";
$sel_count_referred = select_from_table($con, $sql, $cond, $fname);
if ($sel_count_referred == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$count_referred = pg_fetch_result($sel_count_referred, 0, 0);

// 未参照の職員数を取得 ※参照範囲を変更可能なため、「お知らせ対象者数」 = 「参照済みの職員数」 + 「未参照の職員数」 とは限らない。
$unreferred_emp_sql = "select emp_id from ($target_all_emp_sql) a where not emp_id in (select emp_id from inci_news_ref where news_id = $news_id)";
$sql = "select count(*) from ($unreferred_emp_sql) a";
$sel_count_all = select_from_table($con, $sql, "", $fname);
if ($sel_count_all == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$count_unreferred = pg_fetch_result($sel_count_all, 0, 0);

//参照者一覧
$list_emp_sql = ($ref_mode == LIST_REFERRED) ? $referred_emp_sql : $unreferred_emp_sql;
$referrer_list = get_ref_list($con, $news_id, $ref_mode, $page, $list_emp_sql, $fname);

//ページング
$count = ($ref_mode == LIST_REFERRED) ? $count_referred : $count_unreferred;
if ($count > ROWS_PER_PAGE_REFERRER_LIST) {
    $page_max = ceil($count / ROWS_PER_PAGE_REFERRER_LIST);
}

//==============================
//表示
//==============================
$smarty = new Cmx_View();

$smarty->assign("session", $session);

//ヘッダ用データ
$header = new hiyari_header_class($session, $fname, $con);
$smarty->assign("INCIDENT_TITLE", $header->get_inci_title());
$smarty->assign("inci_auth_name", $header->get_inci_auth_name());
$smarty->assign("is_kanrisha", $header->is_kanrisha());

if ($design_mode == 1){
    $smarty->assign("tab_info", $header->get_news_tab_info($menu_page, $news_id));
    $smarty->assign("is_news_menu", false);
}
else{
    $smarty->assign("tab_info", $header->get_user_tab_info());
}
$smarty->assign("menu_page", $menu_page);

//参照者一覧情報
$smarty->assign("news_title", $news_title);
$smarty->assign("ref_mode", $ref_mode);
$smarty->assign("is_referred", $ref_mode==LIST_REFERRED);
$smarty->assign("count_all", $count_all);
$smarty->assign("count_referred", $count_referred);
$smarty->assign("count_unreferred", $count_unreferred);
$smarty->assign("news_id", $news_id);

//参照者一覧
$smarty->assign("referrer_list", $referrer_list);

//ページング
$smarty->assign("page", $page);
$smarty->assign("page_max", $page_max);
if ($design_mode == 1){
    if ($page > 1){
        $smarty->assign("last_page", $page - 1);
    }
    if ($page < $page_max){
        $smarty->assign("next_page", $page + 1);
    }
}
else{
    if ($page_max > 1){        
        $page_list = array();
        $page_stt = max(1, $page - 3);
        $page_end = min($page_stt + 6, $page_max);
        $page_stt = max(1, $page_end - 6);
        for ($i=$page_stt; $i<=$page_end; $i++){
            $page_list[] = $i;
        }
        $smarty->assign("page_list", $page_list);
    }
}

//DBコネクション終了
pg_close($con);

if ($design_mode == 1){
    $smarty->display("hiyari_news_refer_list1.tpl");
}
else{
    $smarty->display("hiyari_news_refer_list2.tpl");
}

//==============================
//関数
//==============================
function get_ref_list($con, $news_id, $ref_mode, $page, $list_emp_sql, $fname){
    $offset = ROWS_PER_PAGE_REFERRER_LIST * ($page - 1);
    $limit = ROWS_PER_PAGE_REFERRER_LIST;

    switch ($ref_mode) {
    case LIST_REFERRED:
        $sql  = "select empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, inci_news_ref.ref_time ";
        $sql .= "from ($list_emp_sql) a ";
        $sql .= "left join inci_news_ref on a.emp_id = inci_news_ref.emp_id and inci_news_ref.news_id = $news_id";
        $sql .= "left join empmst on a.emp_id = empmst.emp_id";
        $cond = "order by ref_time desc offset $offset limit $limit";
        break;
    case LIST_UNREFERRED:
        $sql  = "select emp_personal_id, emp_lt_nm, emp_ft_nm, '' as ref_time ";
        $sql .= "from ($list_emp_sql) a ";
        $sql .= "left join empmst on a.emp_id = empmst.emp_id";
        $cond = "order by emp_personal_id offset $offset limit $limit";
        break;
    }

    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $list = array();
    while ($row = pg_fetch_array($sel)) {
        $info = array();
        $info["emp_id"] = $row["emp_personal_id"];
        $tmp_emp_lt_nm = $row["emp_lt_nm"];
        $tmp_emp_ft_nm = $row["emp_ft_nm"];
        $tmp_ref_time = $row["ref_time"];
        $info["emp_name"] = "$tmp_emp_lt_nm $tmp_emp_ft_nm";
        $info["ref_time"] = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $tmp_ref_time);

        $list[] = $info;
    }

    return $list;
}
