<?php
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("application_workflow_common_class.php");

$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 7, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}



// データベースに接続
$con = connect2db($fname);

$obj = new application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 承認状況チェック
$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
	$approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script language=\"javascript\">alert('他の{$approve_label}状況が変更されたため、申請取消ができません。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

// 申請論理削除
$obj->update_delflg_all_apply($apply_id, "t");

//----- メディアテック  Start -----
if (phpversion() >= '5.1') {
    $template_type = $_POST["kintai_template_type"];
    if (!empty($template_type)) {
        // MDB初期化
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction();

        $class = null;
        switch ($template_type) {
            case "ovtm": // 時間外勤務命令の場合
                if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                    $class = WORKFLOW_OVTM_CLASS;
                }
                break;
            case "hol": // 休暇申請の場合
                // 総合届け使用フラグ
                $general_flag = file_exists("opt/general_flag");
                if ($general_flag) {
                    if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                        $class = WORKFLOW_HOL_CLASS;
                    }
                }
                //休暇申請
                else {
                    if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                        $class = WORKFLOW_HOL2_CLASS;
                    }
                }
                break;
            case "travel": // 旅行命令
                if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                    $class = WORKFLOW_TRVL_CLASS;
                }
                break;
            case "late": // 遅刻早退の場合
                if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
                    $class = WORKFLOW_LATE_CLASS;
                }
                break;
        }
        if (!empty($class)) {
            $module = new $class;
            $module->cancel_apply($apply_id);
        }
    }

    if (!empty($template_type)) {
        $mdb->endTransaction();
    }
}
// モジュール・クラスの存在チェック
function module_check($module, $class) {
    if (file_exists(WORKFLOW_KINTAI_DIR."/".$module)) {
        require_once WORKFLOW_KINTAI_DIR."/".$module;
        return class_exists($class);
    }
    return false;
}
//----- メディアテック End  -----
// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");
