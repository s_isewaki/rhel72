<?php
require_once("Cmx.php");
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("aclg_set.php");

switch ($frompage) {
    case "1":
        $action = "newsuser_register.php";
        break;
    case "2":
        $action = "news_register.php";
        break;
    case "3":
        $action = "newsuser_copy.php";
        break;
    case "4":
        $action = "news_copy.php";
        break;
}
?>
<body>
<form name="items" action="<?=h($action); ?>" method="post">
<input type="hidden" name="news_title" value="<?=h($news_title) ?>">
<input type="hidden" name="marker" value="<?=h($marker) ?>">
<input type="hidden" name="news_category" value="<?=h($news_category) ?>">
<input type="hidden" name="show_login_flg" value="<?=h($show_login_flg) ?>">
<?php
if (is_array($class_id)) {
    for ($i = 0, $j = count($class_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"class_id[]\" value=\"$class_id[$i]\">\n");
        echo("<input type=\"hidden\" name=\"atrb_id[]\" value=\"$atrb_id[$i]\">\n");
        echo("<input type=\"hidden\" name=\"dept_id[]\" value=\"$dept_id[$i]\">\n");

        if ($atrb_id[$i] == "0") {$atrb_id[$i] = null;}
        if ($dept_id[$i] == "0") {$dept_id[$i] = null;}
    }
}
if (is_array($job_id)) {
    for ($i = 0, $j = count($job_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"job_id[]\" value=\"$job_id[$i]\">\n");
    }
}
?>
<input type="hidden" name="notice_id" value="<?=h($notice_id) ?>">
<input type="hidden" name="notice2_id" value="<?=h($notice2_id) ?>">
<?php
if (is_array($st_id)) {
    for ($i = 0, $j = count($st_id); $i < $j; $i++) {
        echo("<input type=\"hidden\" name=\"st_id[]\" value=\"$st_id[$i]\">\n");
    }
}
?>
<input type="hidden" name="news" value="<?=(h($news)) ?>">
<?php
foreach ($filename as $tmp_filename) {
    echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
    echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}
?>
<input type="hidden" name="src_class_id" value="<?=h($src_class_id) ?>">
<input type="hidden" name="src_atrb_id" value="<?=h($src_atrb_id) ?>">
<input type="hidden" name="src_dept_id" value="<?=h($src_dept_id) ?>">
<input type="hidden" name="src_room_id" value="<?=h($src_room_id) ?>">
<input type="hidden" name="src_emp_id" value="<?=h($src_emp_id) ?>">
<input type="hidden" name="src_emp_nm" value="<?=h($hid_src_emp_nm) ?>">
<input type="hidden" name="record_flg" value="<?=h($record_flg) ?>">
<input type="hidden" name="comment_flg" value="<?=h($comment_flg) ?>">
<input type="hidden" name="news_begin1" value="<?=h($news_begin1) ?>">
<input type="hidden" name="news_begin2" value="<?=h($news_begin2) ?>">
<input type="hidden" name="news_begin3" value="<?=h($news_begin3) ?>">
<input type="hidden" name="news_end1" value="<?=h($news_end1) ?>">
<input type="hidden" name="news_end2" value="<?=h($news_end2) ?>">
<input type="hidden" name="news_end3" value="<?=h($news_end3) ?>">
<input type="hidden" name="term" value="<?=h($term) ?>">
<input type="hidden" name="news_login1" value="<?=h($news_login1) ?>">
<input type="hidden" name="news_login2" value="<?=h($news_login2) ?>">
<input type="hidden" name="news_login3" value="<?=h($news_login3) ?>">
<input type="hidden" name="news_login_end1" value="<?=h($news_login_end1) ?>">
<input type="hidden" name="news_login_end2" value="<?=h($news_login_end2) ?>">
<input type="hidden" name="news_login_end3" value="<?=h($news_login_end3) ?>">
<input type="hidden" name="login_term" value="<?=h($login_term) ?>">
<input type="hidden" name="news_date1" value="<?=h($news_date1) ?>">
<input type="hidden" name="news_date2" value="<?=h($news_date2) ?>">
<input type="hidden" name="news_date3" value="<?=h($news_date3) ?>">
<input type="hidden" name="session" value="<?=h($session) ?>">
<input type="hidden" name="target_id_list1" value="<?=h($target_id_list1) ?>">
<input type="hidden" name="deadline_date1" value="<?=h($deadline_date1) ?>">
<input type="hidden" name="deadline_date2" value="<?=h($deadline_date2) ?>">
<input type="hidden" name="deadline_date3" value="<?=h($deadline_date3) ?>">
<input type="hidden" name="deadline_hour" value="<?=h($deadline_hour) ?>">
<input type="hidden" name="deadline_min" value="<?=h($deadline_min) ?>">
<input type="hidden" name="public_flg" value="<?=h($public_flg) ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="henshin_flg" value="<?=h($henshin_flg); ?>">
<input type="hidden" name="news_id" value="<?=h($news_id); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="<?=h($target_id_list1); ?>">
<input type="hidden" id="henshin_emp_id" name="henshin_emp_id" value="<?=h($henshin_emp_id); ?>">
<input type="hidden" id="henshin_emp_nm" name="henshin_emp_nm" value="<?=h($henshin_emp_nm); ?>">
<input type="hidden" name="henshin_origin_news_id"	value="<?=h($henshin_origin_news_id); ?>">
<input type="hidden" name="henshin_toroku_flg"	value="<?=h($henshin_toroku_flg); ?>">
<input type="hidden" id="henshin_origin_emp_id" name="henshin_origin_emp_id" value="<?=h($henshin_emp_id); ?>">
<input type="hidden" id="henshin_origin_emp_nm" name="henshin_origin_emp_nm" value="<?=h($henshin_emp_nm); ?>">
<?php
// データ設定、配列は1番目から使用
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$answer_comment_flg = array();
$type = array();

$dst_idx = 1;
for ($src_idx=1; $src_idx<=$qa_cnt; $src_idx++) {
    $v_num_src = "select_num_".$src_idx;
     $select_changeItem = "select_changeItem_".$src_idx;
    if ($$v_num_src == "" && $$select_changeItem = 0) {
        continue;
    }
    if($$v_num_src == "" &&  $$select_changeItem = 1){
        $$v_num_src = 0;
    }
    $select_num[$dst_idx] = $$v_num_src;

    $v_question_src = "question_".$src_idx;
    $question[$dst_idx] = $$v_question_src;

    $v_mult_src = "multi_flg_".$src_idx;
    $multi_flg[$dst_idx] = $$v_mult_src;
    if ($multi_flg[$dst_idx] == "") {
        $multi_flg[$dst_idx] = "f";
    }

    //ここに追加
   
    $type[$dst_idx] = $$select_changeItem;
    
    for ($i = 1; $i <= $$v_num_src; $i++) {
        $varname = "answer".$src_idx."_".$i;
        $answer[$dst_idx][$i] = $$varname;
        $v_com_src = "answer_comment_flg" . $src_idx . "_" . $i;
        if ($$v_com_src != 't') {
            $$v_com_src = 'f';
        }
        $answer_comment_flg[$dst_idx][$i] = $$v_com_src;
    }
    $dst_idx++;
}
$qa_cnt = $dst_idx - 1;
if ($qa_cnt == 0) {
    $qa_cnt = 1;
}
// 質問数分設定
for ($qa_idx=1; $qa_idx<=$qa_cnt; $qa_idx++) {
    echo("<input type=\"hidden\" name=\"question_{$qa_idx}\" value=\"{$question[$qa_idx]}\">\n");
    echo("<input type=\"hidden\" name=\"select_num_{$qa_idx}\" value=\"{$select_num[$qa_idx]}\">\n");
    
    echo("<input type=\"hidden\" name=\"select_changeItem_{$qa_idx}\" value=\"{$type[$qa_idx]}\">\n");
 
    echo("<input type=\"hidden\" name=\"multi_flg_{$qa_idx}\" value=\"{$multi_flg[$qa_idx]}\">\n");
    for ($i = 1; $i <= $select_num[$qa_idx]; $i++) {
        echo("<input type=\"hidden\" name=\"answer{$qa_idx}_{$i}\" value=\"{$answer[$qa_idx][$i]}\">\n");
        echo("<input type=\"hidden\" name=\"answer_comment_flg{$qa_idx}_{$i}\" value=\"{$answer_comment_flg[$qa_idx][$i]}\">\n");
    }
}
?>
<input type="hidden" name="qa_cnt" value="<?=h($qa_cnt) ?>">
</form>
<?php
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($frompage == "1" || $frompage == "3") ? 46 : 24;
$checkauth = check_authority($session, $auth_id, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
$news_title = trim($news_title);
if ($news_title == "") {
    echo("<script type=\"text/javascript\">alert('タイトルが入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (strlen($news_title) > 100) {
    echo("<script type=\"text/javascript\">alert('タイトルが長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
// 回覧時、対象者チェック
if ($news_category == 10000 && $target_id_list1 == "") {
    echo("<script type=\"text/javascript\">alert('対象者を指定してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

//返信を行う際には、送信者があて先の中に入っているかチェック
$check_henshin = explode(",",$target_id_list1);
if(!in_array($emp_id,$check_henshin)){
  if($henshin_flg == 't' && $news_category == "10000"){
		echo("<script type=\"text/javascript\">alert('自分を宛先に入れてください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}}


$news = trim($news);
if ($news == "") {
    echo("<script type=\"text/javascript\">alert('内容を入力してください。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$qa_input_cnt = 0;
if ($record_flg == "t") {
    // 質問数分確認
    for ($qa_idx=1; $qa_idx<=$qa_cnt; $qa_idx++) {
        if (strlen($question[$qa_idx]) != "") {
            $qa_input_cnt++;
        }
        if (strlen($question[$qa_idx]) > 100) {
            echo("<script type=\"text/javascript\">alert('質問{$qa_idx}が長すぎます。');</script>");
            echo("<script type=\"text/javascript\">document.items.submit();</script>");
            exit;
        }
        if ($question[$qa_idx] != "" && $type[$qa_idx]== 0) {
            if ($select_num[$qa_idx] == 0) {
                echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の選択肢数を選んでください。');</script>");
                echo("<script type=\"text/javascript\">document.items.submit();</script>");
                exit;
            }
            for ($i = 1; $i <= $select_num[$qa_idx]; $i++) {
                if ($answer[$qa_idx][$i] == "") {
                    echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の{$i}番目の回答内容を入力してください。');</script>");
                    echo("<script type=\"text/javascript\">document.items.submit();</script>");
                    exit;
                }
                if (strlen($answer[$qa_idx][$i]) > 40) {
                    echo("<script type=\"text/javascript\">alert('質問{$qa_idx}の{$i}番目の回答内容が長すぎます。');</script>");
                    echo("<script type=\"text/javascript\">document.items.submit();</script>");
                    exit;
                }
            }
        }
    }
}
// アンケートがある場合、締切日の確認
if ($qa_input_cnt > 0) {
    // 開始日より前はエラー
    if ($deadline_date1.$deadline_date2.$deadline_date3 < $news_date1.$news_date2.$news_date3) {
        echo("<script type=\"text/javascript\">alert('締切り日時は掲載期間の開始日以降にしてください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    // 現在時刻より前はエラー
    $deadline = $deadline_date1.$deadline_date2.$deadline_date3.$deadline_hour.$deadline_min;
    if ($deadline <= date('YmdHi')) {
        echo("<script type=\"text/javascript\">alert('締切り日時は現在より後にしてください。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}

if (!checkdate($news_begin2, $news_begin3, $news_begin1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（From）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_end2, $news_end3, $news_end1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（To）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news_begin = "$news_begin1$news_begin2$news_begin3";
$news_end = "$news_end1$news_end2$news_end3";
if ($news_begin > $news_end) {
    echo("<script type=\"text/javascript\">alert('掲載期間が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}

if (!checkdate($news_login2, $news_login3, $news_login1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面：From）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
if (!checkdate($news_login_end2, $news_login_end3, $news_login_end1)) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面：To）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
$news_login = "$news_login1$news_login2$news_login3";
$news_login_end = "$news_login_end1$news_login_end2$news_login_end3";
if ($news_login > $news_login_end) {
    echo("<script type=\"text/javascript\">alert('掲載期間（ログイン画面）が不正です。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
// 添付ファイルの確認
if (!is_dir("news")) {
    mkdir("news", 0755);
}
if (!is_dir("news/tmp")) {
    mkdir("news/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $ext = strrchr($filename[$i], ".");

    $tmp_filename = "news/tmp/{$session}_{$tmp_file_id}{$ext}";
    if (!is_file($tmp_filename)) {
        echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
        echo("<script language=\"javascript\">document.items.submit();</script>");
        exit;
    }
}

// 登録値の編集
if ($news_category != "3") {
    $job_id = null;
}
$show_login_flg = ($show_login_flg == "t") ? "t" : "f";
if ($notice_id == "0") {$notice_id = null;}
if ($notice2_id == "0") {$notice2_id = null;}
if ($src_class_id == 0) {$src_class_id = null;}
if ($src_atrb_id == 0) {$src_atrb_id = null;}
if ($src_dept_id == 0) {$src_dept_id = null;}
if ($src_room_id == 0) {$src_room_id = null;}

if ($news_category == "10000") {
	$st_id = array();
}
$st_id_cnt = 0;
for ($i = 0, $j = count($st_id); $i < $j; $i++) {
    if ($st_id[$i] != "") {
        $st_id_cnt++;
    }
}

// トランザクションの開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

// お知らせIDを採番
$sql = "select coalesce(max(news_id) + 1, 1) from news";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news_id = pg_fetch_result($sel, 0, 0);

//フラグ判定をする
if ($_REQUEST["henshin_flg"] == 't') {
    $_REQUEST["henshin_flg"] == 'true';
}
else {
    $_REQUEST["henshin_flg"] == 'false';
}

//既存のお知らせか確認
if($henshin_flg && $_REQUEST["news_id"] != ""){
	$sql = "select henshin_origin_news_id,news_id from news";
	$cond = "where news_id = '". $_REQUEST["news_id"]."'";
	$sel = select_from_table($con, $sql, $cond, $fname);

	$henshin_origin_news_id = pg_fetch_result($sel, 0, "henshin_origin_news_id");

		if($henshin_origin_news_id == "" ){
			$henshin_origin_news_id =  pg_fetch_result($sel, 0, "news_id");;
		}


}

//20120322
if ($henshin_toroku_flg == "1") {
    $chk_id = $henshin_origin_news_id;
}
else {
    //返信処理での登録でなければnullを代入
    $chk_id = null;
}

//20120322 add
if ($_REQUEST["henshin_flg"] == "t") {
    $henshin_flg = "t";
}
else {
    $henshin_flg = "f";
}

// お知らせ情報を登録
$sql = "insert into news (emp_id, news_id, date, news_date, news_begin, news_end, news_title, news_category, news, news_del_flg, job_id, show_login_flg, notice_id, notice2_id, src_class_id, src_atrb_id, src_dept_id, src_room_id, record_flg, src_emp_id, marker, st_id_cnt, news_add_category, comment_flg, deadline, public_flg,henshin_flg,henshin_origin_news_id,news_login,news_login_end) values (";
$content = array($emp_id, $news_id, date("YmdHi"), "$news_date1$news_date2$news_date3", $news_begin, $news_end, p($news_title), "", p($news), "f", null, $show_login_flg, $notice_id, $notice2_id, $src_class_id, $src_atrb_id, $src_dept_id, $src_room_id, $record_flg, $src_emp_id, $marker, $st_id_cnt, $news_category, $comment_flg, $deadline, $public_flg,$henshin_flg,$chk_id,$news_login,$news_login_end);

$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// お知らせ配信先部署情報を登録
if ($news_category == "2") {
    for ($i = 0, $j = count($class_id); $i < $j; $i++) {
        $sql = "delete from newsdept";
        $cond = "where news_id = $news_id and class_id = $class_id[$i]";
        if (!is_null($atrb_id[$i])) {
            $cond .= " and atrb_id = $atrb_id[$i]";
        } else {
            $cond .= " and atrb_id is null";
        }
        if (!is_null($dept_id[$i])) {
            $cond .= " and dept_id = $dept_id[$i]";
        } else {
            $cond .= " and dept_id is null";
        }
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        $sql = "insert into newsdept (news_id, class_id, atrb_id, dept_id) values (";
        $content = array($news_id, $class_id[$i], $atrb_id[$i], $dept_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// お知らせ職種情報を登録
if ($news_category == "3") {
    for ($i = 0, $j = count($job_id); $i < $j; $i++) {
        $sql = "insert into newsjob (news_id, job_id) values (";
        $content = array($news_id, $job_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 役職情報を更新
$sql = "delete from newsst";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if ($st_id_cnt > 0) {
    for ($i = 0, $j = count($st_id); $i < $j; $i++) {
        if ($st_id[$i] == "") {
            continue;
        }
        $sql = "insert into newsst (news_id, st_id) values (";
        $content = array($news_id, $st_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}


// 添付ファイル情報を作成
$no = 1;
foreach ($filename as $tmp_filename) {
    $sql = "insert into newsfile (news_id, newsfile_no, newsfile_name) values (";
    $content = array($news_id, $no, p($tmp_filename));
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $no++;
}

// 回答設定を更新
$sql = "delete from newsenquet_q";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$sql = "delete from newsenquet_a";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

for ($i = 1; $i <= $qa_cnt; $i++) {
    if ($question[$i] != "") {
        $sql = "insert into newsenquet_q (news_id, question_no, question, select_num, multi_flg,type) values (";
        $content = array($news_id, $i, $question[$i], $select_num[$i], $multi_flg[$i], $type[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        for ($j = 1; $j <= $select_num[$i]; $j++) {
            $sql = "insert into newsenquet_a (news_id, question_no, item_no, answer, answer_comment_flg) values (";
            $content = array($news_id, $i, $j, $answer[$i][$j], $answer_comment_flg[$i][$j]);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

// 回覧対象者を更新
$sql = "delete from newscomment";
$cond = "where news_id = $news_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

if ($news_category == "10000") {
    $arr_emp_id = explode(",", $target_id_list1);
    for ($i = 0, $j = count($arr_emp_id); $i < $j; $i++) {
        $sql = "insert into newscomment (news_id, emp_id) values (";
        $content = array($news_id, $arr_emp_id[$i]);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}


// トランザクションをコミット
pg_query($con, "commit");

// ユーザ画面の場合のカテゴリを取得
if ($frompage == "1" || $frompage == "3") {
    if ($news_category > "3" && $news_category < "10000") {
        $sql = "select standard_cate_id from newscate ";
        $cond = "where newscate_id = $news_category ";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $tmp_news_category = pg_fetch_result($sel, 0, 0);
    }
}

// データベース接続を閉じる
pg_close($con);

// 添付ファイルの移動
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $tmp_fileno = $i + 1;
    $ext = strrchr($tmp_filename, ".");

    $tmp_filename = "news/tmp/{$session}_{$tmp_file_id}{$ext}";
    copy($tmp_filename, "news/{$news_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("news/tmp/{$session}_*.*") as $tmpfile) {
    unlink($tmpfile);
}

$arg = "";
// お知らせ一覧画面に遷移
if ($frompage == "1" || $frompage == "3") {
    $list_page = "newsuser_menu.php";
} else {
    $list_page = "news_menu.php";
}
echo("<script type=\"text/javascript\">location.href = '$list_page?session=$session$arg';</script>");
?>
</body>
