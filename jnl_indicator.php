<?php
require_once('about_postgres.php');

//define('OPERATION_FIRST_YEAR', 2009);
define('OPERATION_FIRST_YEAR', 2010);

/**
 * 管理指標 基礎クラス
 */
class Indicator
{
    /**
     * @access protected
     */
    var $categoryArr = array(
        '0' => '施設別'
      , '1' => '病院集計'
      , '2' => '老健集計'
      , '3' => '返戻'
    );

    /**
     * @access protected
     */
    var $categoryMinNameArr = array(
        '1' => '病院'
      , '2' => '老健'
    );

    /**
     * @access protected
     */
    var $subCategoryArr = array(
        '1' => array(
            '1' => '外来患者数'
          , '2' => '延入院患者数'
          , '3' => '平均入院患者数'
          , '4' => '稼働率'
          , '5' => 'その他集計表'
        ),
    );

    /**
     * @access protected
     */
    var $noArr = array(
        /* ---------- 病院 外来患者数 ---------- */
        '1' => array(
            'name'         => '外来延患者数'
          , 'file'         => 'jnl_application_indicator_comp_previous_year.php'
          , 'category'     => '1'
          , 'sub_category' => '1'
          , 'total'        => '病院合計'
          , 'average'      => '病院平均'
          , 'raito'        => true
          , 'amg_total'    => true
          , 'file_name'    => 'name_y'
        ),
        '2' => array(
            'name'         => '外来平均患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '1',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        /* ---------- 病院 延入院患者数 ---------- */
        '3' => array(
            'name'         => '全病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '4' => array(
            'name'         => '一般病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '5' => array(
            'name'         => '障害患者病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '6' => array(
            'name'         => '精神一般病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '7' => array(
            'name'         => '医療療養病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '8' => array(
            'name'         => '介護療養病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '9' => array(
            'name'         => '回復期リハ病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '10' => array(
            'name'         => '亜急性期病床延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '11' => array(
            'name'         => '緩和ケア病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '12' => array(
            'name'         => 'ICU及びハイケアユニット延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '13' => array(
            'name'         => '小児管理料延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '14' => array(
            'name'         => '精神療養病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '15' => array(
            'name'         => '特殊疾患(2)延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '16' => array(
            'name'         => '認知症病棟延入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '2',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        /* ---------- 病院 平均入院患者数 ---------- */
        '17' => array(
            'name'         => '全病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '18' => array(
            'name'         => '一般病棟平均入院患者数',
            'file'         =>  'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '19' => array(
            'name'         => '障害者病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '20' => array(
            'name'         => '精神一般病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '21' => array(
            'name'         => '医療療養病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '22' => array(
            'name'         => '介護療養病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '23' => array(
            'name'         =>'回復期リハ病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '24' => array(
            'name'         => '亜急性期病床平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '25' => array(
            'name'         => '緩和ケア病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '26' => array(
            'name'         => 'ICU及びハイケアユニット平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '27' => array(
            'name'         => '小児管理料平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '28' => array(
            'name'         => '精神療養病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '29' => array(
            'name'         => '特殊疾患(2)平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '30' => array(
            'name'         => '認知症病棟平均入院患者数',
            'file'         => 'jnl_application_indicator_comp_previous_year_average.php',
            'category'     => '1',
            'sub_category' => '3',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        /* ---------- 病院 稼働率 ---------- */
        '31' => array(
            'name'         => '全病棟稼働率(稼働病床)',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '32' => array(
            'name'         => '全病棟稼働率(許可病床)',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '33' => array(
            'name'         => '一般病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '34' => array(
            'name'         => '障害者病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '35' => array(
            'name'         => '精神一般病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '36' => array(
            'name'         => '医療療養病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '37' => array(
            'name'         => '介護療養病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '38' => array(
            'name'         => '回復期リハ病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '39' => array(
            'name'         => '亜急性期病床稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '40' => array(
            'name'         => '緩和ケア病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '41' => array(
            'name'         => 'ICU及びハイケアユニット稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '42' => array(
            'name'         => '小児管理料稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '43' => array(
            'name'         => '精神療養病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '44' => array(
            'name'         => '特殊疾患(2)稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '45' => array(
            'name'         => '認知症病棟稼働率',
            'file'         => 'jnl_application_indicator_utilization_rates.php',
            'category'     => '1',
            'sub_category' => '4',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        /* ---------- 病院 その他集計 ---------- */
        '46' => array(
            'name'         => '全病棟入院患者件数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '47' => array(
            'name'         => '全病棟退院患者件数',
            'file'         => 'jnl_application_indicator_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '48' => array(
            'name'         => '一般病棟平均在院日数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'raito'        => true,
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '49' => array(
            'name'         => '新患者数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '50' => array(
            'name'         => '紹介患者数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '51' => array(
            'name'         => '救急依頼件数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '52' => array(
            'name'         => '救急受入率',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '53' => array(
            'name'         => '救急入院患者数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '54' => array(
            'name'         => '救急入院率',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
			'amg_average'  => true,
			'file_name'    => 'name_y',
        ),
        '55' => array(
            'name'         => '診療時間外患者数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '56' => array(
            'name'         => '時間外入院患者数',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '57' => array(
            'name'         => '診療時間外入院率',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'average'      => '病院平均',
			'amg_average'  => true,
			'file_name'    => 'name_y',
        ),
        '58' => array(
            'name'         => '紹介率',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'average'      => '病院平均',
            'amg_average'  => true,
            'file_name'    => 'name_y',
        ),
        '59' => array(
            'name'         => '手術件数(全麻)',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '60' => array(
            'name'         => '手術件数(腰麻)',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '61' => array(
            'name'         => '手術件数(伝達・局麻)',
            'file'         => 'jnl_application_indicator_monthly_report_comp_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'total'        => '病院合計',
            'average'      => '病院平均',
            'raito'        => true,
            'amg_total'    => true,
            'file_name'    => 'name_y',
        ),
        '62' => array(
            'name'         => '病院別患者日報',
            'file'         => 'jnl_application_indicator_daily_report_according_to_hospital.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m_d',
        ),
        '63' => array(
            'name'         => '病院別患者日報(シンプル版)',
            'file'         => 'jnl_application_indicator_daily_simple_report_according_to_hospital.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m_d',
        ),
        '65' => array(
            'name'         => '病院別患者月報',
            'file'         => 'jnl_application_indicator_monthly_report_according_to_hospital.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m',
        ),
        '66' => array(
            'name'         => '病院別患者月報(前年比較)',
            'file'         => 'jnl_application_indicator_monthly_report_comp_display_in_the_previous_year.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m',
        ),
        '67' => array(
            'name'         => '外来診療科別患者月報',
            'file'         => 'jnl_application_indicator_deptOPDMonth.php',
            'category'     => '1',
            'sub_category' => '5',
        ),
        '68' => array(
            'name'         => '外来診療科別患者月報(前年比較)',
            'file'         => 'jnl_application_indicator_comp_prevyear_diseasewiseOPD.php',
            'category'     => '1',
            'sub_category' => '5',
        ),
        '69' => array(
            'name'         => '入院診療科別患者月報',
            'file'         => 'jnl_application_indicator_deptIPDMonth.php',
            'category'     => '1',
            'sub_category' => '5',
        ),
        '70' => array(
            'name'         => '入院診療科別患者月報(前年比較)',
            'file'         => 'jnl_application_indicator_comp_prevyear_diseasewiseIPD.php',
            'category'     => '1',
            'sub_category' => '5',
        ),
        '71' => array(
            'name'         => '病院月間報告書患者数総括',
            'file'         => 'jnl_application_indicator_summary_of_number_of_hospital_monthly_report_patients.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m',
        ),
        '72' => array(
            'name'         => '病院月間報告書(行為件数)',
            'file'         => 'jnl_application_indicator_hospital_monthly_report.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m',
        ),
        '73' => array(
            'name'         => '病院別患者日報(累積統計表)',
            'file'         => 'jnl_application_indicator_daily_report_accumulation_statistics.php',
            'category'     => '1',
            'sub_category' => '5',
            'file_name'    => 'name_y_m_d',
        ),
    );

    /**
     * 基底フォントスタイルの配列
     * @access protected
     */
    var $font = array(
        'class' => 'j12'
      , 'size'  => '3'
      , 'face'  => 'ＭＳ Ｐゴシック, Osaka'
    );

    var $departmentArr = array(
             0 => array('display' => '内科',         'column' => 'naika'),
             1 => array('display' => '神経内科',     'column' => 'sinkei'),
             2 => array('display' => '呼吸器科',     'column' => 'kokyu'),
             3 => array('display' => '消化器科',     'column' => 'shoka'),
             4 => array('display' => '循環器科',     'column' => 'junkanki'),
             5 => array('display' => '小児科',       'column' => 'shouni'),
             6 => array('display' => '外科',         'column' => 'geka'),
             7 => array('display' => '整形外科',     'column' => 'seikei'),
             8 => array('display' => '形成(美)外科', 'column' => 'keiseibi'),
             9 => array('display' => '脳神経外科',   'column' => 'nou'),
            10 => array('display' => '皮膚科',       'column' => 'hifu'),
            11 => array('display' => '泌尿器科',     'column' => 'hinyo'),
            12 => array('display' => '産婦人科',     'column' => 'sanfu'),
            13 => array('display' => '眼科',         'column' => 'ganka'),
            14 => array('display' => '耳鼻咽喉科',   'column' => 'jibi'),
            15 => array('display' => '透析科',       'column' => 'touseki'),
            16 => array('display' => '精神科',       'column' => 'seisin'),
            17 => array('display' => '歯科',         'column' => 'sika'),
            18 => array('display' => '放射線科',     'column' => 'hoshasen'),
            19 => array('display' => '麻酔科',       'column' => 'masui'),
    );

    var $departmentTypeArr = array(
         0 => 'ip',
         1 => 'shog',
         2 => 'seip',
         3 => 'ir',
         4 => 'kaig',
         5 => 'kaif',
         6 => 'ak',
         7 => 'kan',
         8 => 'icu',
         9 => 'shoni',
        10 => 'seir',
        11 => 'tok',
        12 => 'nin',
    );

    /**
     * @access protected
     */
    var $session;

    /**
     * @access protected
     */
    var $con;

    /**
     * @access protected
     */
    var $fname;

    /**
     * @access protected
     */
    var $fileName;

    /**
     * 年度初めの月
     * @access private
     */
    var $fiscalYearStartMonth = 4;

    /**
     * 前年度の年
     * @access private
     */
    var $lastYear;

    /**
     * 表示対象年度の年
     * @access private
     */
    var $currentYear;

    /**
     * @access protected
     */
    var $displayYear;

    /**
     * @access protected
     */
    var $displayMonth;

    /**
     * @access protected
     */
    var $displayDay;

    /**
     * コンストラクタ
     * @access public
     * @param  array  $arr
     * @return void
     */
    function Indicator($arr)
    {
        $this->setSession($this->getArrValueByKey('session', $arr));
        $this->fileName = $this->getArrayValueByKeyForEmptyIsNull('file_name', $arr)? $arr['file_name'] : 'jnl_application_indicator_menu.php';
        $this->fname = $this->getArrValueByKey('fname', $arr);
        $this->setConnection('con', $arr);
    }

    /**
     * @access protected
     * @param $key
     * @param $arr
     */
    function setConnection($key, $arr)
    {
        $con = $this->getArrayValueByKeyForEmptyIsNull($key, $arr);
        $this->con = (is_null($con))? connect2db($this->fname) : $con;
    }

    /**
     * @access protected
     * @param $session
     */
    function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * 配列の中にある Key の value を返す。
     * key が無ければ NULL
     * @access public
     * @param  string $key
     * @param  array  $array
     * @return mixed
     */
    function getArrValueByKey($key, $array)
    {
        if (!is_array($array)) { return false; }
        return array_key_exists($key, $array)? $array[$key] : null;
    }

    /**
     * 配列の中にある Key の value を返す。
     * key が無ければ NULL、value が 0 か 空 か NULL でも、NULL を返す。
     * @access public
     * @param  string $key
     * @param  array  $array
     * @return mixed
     */
    function getArrayValueByKeyForEmptyIsNull ($key, $array)
    {
        if (!is_array($array)) { return false; }
        return array_key_exists($key, $array)? empty($array[$key])? null : $array[$key] : null;
    }

    /**
     * 施設のカテゴリを取得する。
     * $minFlg が null ではなかったら「集計」が付かない。
     * @access public
     * @param  integer $cNum
     * @param  bool    $minFlg default null
     * @return mixed
     */
    function getCategory($cNum=null, $minFlg=null)
    {
        $categoryArr = array();
        if (is_null($minFlg)) { $categoryArr = $this->categoryArr; }
        else { $categoryArr = $this->categoryMinNameArr; }

        if (is_null($cNum)) { return $categoryArr; }
        return $this->getArrValueByKey($cNum, $categoryArr);
    }

    /**
     * @access public
     * @param  integer $cNum
     * @param  integer $scNum
     * @return mixed
     */
    function getSubCategory($cNum, $scNum=null)
    {
        $subCategoryArr = $this->subCategoryArr;
        if (is_null($scNum)) { return $this->getArrValueByKey($cNum, $subCategoryArr); }
        return $this->getArrValueByKey($scNum, $this->getArrValueByKey($cNum, $subCategoryArr));
    }

    /**
     * @access public
     * @param  integer $cNum
     * @param  integer $scNum
     * @return mixed
     */
    function getNoForCategory($cNum, $scNum)
    {
        $noArr  = $this->noArr;
        $rtnArr = array();
        foreach ($noArr as $noKey => $noVal) { if ($noVal['category']==$cNum && $noVal['sub_category']==$scNum) { $rtnArr[$noKey] = $noVal; } }
        return $rtnArr;
    }

    function getDisplayNoNameArr($cNum, $scNum)
    {
        $rtnArr  = array();
        $dataArr = $this->getNoForCategory($cNum, $scNum);
        foreach ($dataArr as $dataKey => $dataVal) { $rtnArr[$dataKey] = $dataVal['name']; }
        return $rtnArr;
    }

    function getDisplayNoFileNameArr($cNum, $scNum)
    {
        $rtnArr  = array();
        $dataArr = $this->getNoForCategory($cNum, $scNum);
        foreach ($dataArr as $dataKey => $dataVal) { $rtnArr[$dataKey] = $dataVal['file']; }
        return $rtnArr;
    }

    /**
     * @access public
     * @param  integer $no
     * @return mixed
     */
    function getNoForNo($no)
    {
        $noArr = $this->noArr;
        return $this->getArrValueByKey($no, $noArr);
    }

    /**
     * @access public
     * @return array
     */
    function getFont()
    {
        return $this->font;
    }

    /**
     * font tag の取得
     * sprintf() で使用する想定。
     * @access public
     * @param  array  $fontStyleArr
     * @return string $rtnFiontTag
     */
    function getFontTag($fontStyleArr=null)
    {
        if (is_null($fontStyleArr)) { $fontStyleArr = $this->getFont(); }
        $class = $this->getArrayValueByKeyForEmptyIsNull('class', $fontStyleArr)? ' class="'. $fontStyleArr['class']. '"' : '';
        $color = $this->getArrayValueByKeyForEmptyIsNull('color', $fontStyleArr)? ' color="'. $fontStyleArr['color']. '"' : '';
        $size  = $this->getArrayValueByKeyForEmptyIsNull('size',  $fontStyleArr)? ' size="'. $fontStyleArr['size']. '"'   : '';
        $face  = $this->getArrayValueByKeyForEmptyIsNull('face',  $fontStyleArr)? ' face="'. $fontStyleArr['face']. '"'   : '';

        $fontTagLeft  = sprintf('<font%s%s%s%s>', $class, $color, $size, $face);
        $fontTagRight = '</font>';
        $rtnFiontTag  = $fontTagLeft.'%s'.$fontTagRight;

        return $rtnFiontTag;
    }

	
	

    /**
     * 渡した年月の末日を取得する。
     * @access protected
     * @param  integer   $year
     * @param  integer   $month
     * @return string
     */
    function getLastDay($year, $month)
    {
        return date('d', mktime(0, 0, 0, $month + 1, 0, $year));
    }

    /**
     * @access public
     * @return integer
     */
    function getLastYear()
    {
        return $this->lastYear;
    }

    /**
     * @access protected
     * @param  integer   $year
     */
    function setLastYear($year)
    {
        $this->lastYear = $year;
    }

    /**
     * 格納された表示年度を取得
     * @access public
     * @return unknown_type
     */
    function getCurrentYear()
    {
        return $this->currentYear;
    }

    /**
     * 表示年度を格納
     * @access protected
     * @param  string $year
     */
    function setCurrentYear($year=null)
    {
        $startMonth = $this->getFiscalYearStartMonth();
        if (is_null($year)) {
            if ($startMonth <= date('m')) { $this->currentYear = date('Y'); }
            else { $this->currentYear = date('Y') - 1; }
        }
        else { $this->currentYear = $year; }
    }

    /**
     * 年度初めの月を取得
     * @access public
     * @return integer
     */
    function getFiscalYearStartMonth()
    {
        return $this->fiscalYearStartMonth;
    }

    /**
     * 年度初めの月を格納する
     * @access protected
     * @param  string(numeric) $month
     * @return void
     */
    function setFiscalYearStartMonth($month)
    {
        $this->fiscalYearStartMonth = $month;
    }

    /**
     * 前年度の月初を取得する
     * @access public
     * @return string(numeric)
     */
    function getStartYearMonth()
    {
        $month      = $this->getDisplayMonth();
        $year       = $this->getDisplayYear();
        $startMonth = $this->getFiscalYearStartMonth();

        if ($startMonth > $month) { $fiscalYear = $this->getDateForMonthStart($year - 2, $startMonth); }
        else { $fiscalYear = $this->getDateForMonthStart($year - 1, $startMonth); }

        return $fiscalYear;
    }

    /**
     * 前年度の最初の月を YYYYMM の形式の文字列で返す
     */
    function getDateToLastYearStartMonth()
    {
        return sprintf('%04d%02d', $this->getLastYear(), $this->getFiscalYearStartMonth());
    }

    /**
     * 本年度(表示年度)の最後の月を YYYYMM の形式の文字列で返す
     */
    function getDateToCurrentYearLastMonth()
    {
        $currentLastMonth = ( ($this->getFiscalYearStartMonth() - 1) == 0)? 12 : ($this->getFiscalYearStartMonth() - 1);
        $currentYear      = $this->getCurrentYear();
        if ($currentLastMonth < 4) { $currentYear = $currentYear + 1; }
        return sprintf('%04d%02d', $currentYear, $currentLastMonth);
    }

    /**
     * 入力した年月の初日を取得する。
     */
    function getDateForMonthStart($year, $month)
    {
        return date('Y/m/d h:i:s', mktime(0, 0, 0, $month, 1, $year));
    }

    /**
     * 曜日の取得
     * @param  string(numeric) $year
     * @param  string(numeric) $month
     * @param  string(numeric) $day
     * @param  string          $lang
     * @return string
     */
    function getDayOfTheWeekForDate($year, $month, $day, $lang='jp')
    {
        $dayOfTheWeek = date('w', mktime(0, 0, 0, $month, $day, $year));
        switch ($lang) {
            case 'jp':
                $jpLangArr = array(0 => '日', 1 => '月', 2 => '火', 3 => '水', 4 => '木', 5 => '金', 6 => '土');
                $dayOfTheWeek = $jpLangArr[$dayOfTheWeek];
                break;

            case 'JP':
                $jpLangArr = array(0 => '日曜日', 1 => '月曜日', 2 => '火曜日', 3 => '水曜日', 4 => '木曜日', 5 => '金曜日', 6 => '土曜日');
                $dayOfTheWeek = $jpLangArr[$dayOfTheWeek];
                break;

            case 'EN':
                $dayOfTheWeek = date('F', mktime(0, 0, 0, $month, $day, $year));
                break;

            case 'en':
                $dayOfTheWeek = date('D', mktime(0, 0, 0, $month, $day, $year));
                break;

            default:
                break;
        }

        return $dayOfTheWeek;
    }

    function getDisplayYear()
    {
        return empty($this->displayYear)? $this->setDisplayYear() : $this->displayYear;
    }

    function setDisplayYear($year=null)
    {
        $this->displayYear = is_null($year)? date('Y') : $year;
    }

    function getDisplayMonth()
    {
        return empty($this->displayMonth)? $this->setDisplayMonth() : $this->displayMonth;
    }

    function setDisplayMonth($month=null)
    {
        $this->displayMonth = is_null($month)? date('m') : $month;
    }

    function getDisplayDay($month=null, $year=null) {
        if (empty($month)) { $month = $this->getDisplayMonth(); }
        if (empty($year))  { $year = $this->getDisplayYear(); }

        $day = $this->displayDay;
        if (empty($day)) { $day = date('d'); }

        return $day;
    }

    function setDisplayDay($day=null) {
        $this->displayDay = is_null($day)? date('d') : $day;
    }

    function getDepartment($type='all', $no=null)
    {
        $rtnArr = array();
        $arr    = $this->departmentArr;

        switch ($type) {
            case 'all':
                if (is_null($no)) { $rtnArr = $arr; }
                break;

            case 'display':
            case 'column':
                if (is_null($no)) { foreach ($arr as $key => $val) { $rtnArr[$key] = $val[$type]; } }
                else { foreach ($arr as $key => $val) { if ($no == $key) { $rtnArr[$key] = $val[$type]; } } }
                break;
        }
        return $rtnArr;
    }

    function getDepartmentType()
    {
        return $this->departmentTypeArr;
    }

    function setDepartmentType($arr)
    {
        if (is_array($arr)) { $this->departmentTypeArr = $arr; }
    }


    function getHtmlInput($attrArr)
    {
        $attr    = null;
        $rtnHtml = null;

        if (is_array($attrArr)) { foreach ($attrArr as $attrVal) { if (is_array($attrVal)) { $attr .= sprintf('%s="%s" ', $this->getArrValueByKey('name', $attrVal), $this->getArrValueByKey('value', $attrVal)); } } }

        $rtnHtml .= sprintf('<input %s/>', $attr);
        return $rtnHtml;
    }

    /**
     * HTML option タグを出力する
     * @param $key
     * @param $value
     * @param $selected
     * @return string(html option tag)
     */
    function getHtmlOption($key, $value, $selected=null)
    {
        if (!is_null($selected) && $key == $selected) { return sprintf('<option value="%s" selected="%s" >%s</option>', $key, $selected, $value); }
        return sprintf('<option value="%s" >%s</option>', $key, $value);
    }

    /**
     * HTML selectタブを出力する
     * @param  array                   $attrArr select の attribute を作成する
     * @param  array                   $dataArr option タグのデータ
     * @param  integer                 $seleted option タグの selected
     * @return string(html select tag) $rtnHtml
     */
    function getHtmlSelect($attrArr, $dataArr, $seleted=null)
    {
        $attr    = null;
        $rtnHtml = null;

        if (is_array($attrArr)) { foreach ($attrArr as $attrVal) { if (is_array($attrVal)) { $attr .= sprintf('%s="%s" ', $this->getArrValueByKey('name', $attrVal), $this->getArrValueByKey('value', $attrVal)); } } }
        $rtnHtml .= sprintf('<select %s>', $attr).'<option>選択してください</option>';
        if (is_array($dataArr)) { foreach ($dataArr as $dataArrKey => $dataArrVal) { $rtnHtml .= $this->getHtmlOption($dataArrKey, $dataArrVal, $seleted); } }

        $rtnHtml .= '</select>';
        return $rtnHtml;
    }

    function sc($int)
    {
        return $this->getEnvironmentalDependenceCharacterNumber($int);
    }

    /**
     * 直接記述すると eclipse が動かせなくなるので仕方なく作成
     * ○１とかの出力をします。
     * ####################################################################################
     * ## 参考までに                                                                     ##
     * ## (http://www.yaskey.cside.tv/mapserver/diary/diary.php?mode=main&COM=316)       ##
     * ##  --- "あ"を16進数のUnicodeへ変換 ---                                           ##
     * ##  $a ="あ";                                                                     ##
     * ##  $a = mb_convert_encoding($a, "UTF-16" ,"SJIS");//Shift_JISからUTF-16へ変換    ##
     * ##  $x = bin2hex("$a");//16進数の数値へ変換                                       ##
     * ##  echo $x;//"3042"と表示される                                                  ##
     * ##  --- 16進数のUnicodeを"あ"へ再変換 ---                                         ##
     * ##  $a = pack("H*","$x");//16進数の数値を文字コードへ変換                         ##
     * ##  $a = mb_convert_encoding($a,"SJIS", "UTF-16" );//UTF-16からShift_JISへ変換    ##
     * ##  echo $a;//"あ"に変換表示される                                                ##
     * ####################################################################################
     * @param  string $str
     * @return string $rtnStr 環境依存文字
     */
    function getEnvironmentalDependenceCharacterNumber($str)
    {
        $rtnStr = null;

        $arr = array(
            '1' => 'ff6dff61',
            '2' => 'ff6dff62',
            '3' => 'ff6dff63',
            '4' => 'ff6dff64',
            '5' => 'ff6dff65',
            '6' => 'ff6dff66',
            '7' => 'ff6dff67',
            '8' => 'ff6dff68',
            '9' => 'ff6dff69',
            '10' => 'ff6dff6a',
            '11' => 'ff6dff6b',
            '12' => 'ff6dff6c',
            '13' => 'ff6dff6d',
            '14' => 'ff6dff6e',
            '15' => 'ff6dff6f',
            '16' => 'ff6dff70',
            '17' => 'ff6dff71',
            '18' => 'ff6dff72',
            '19' => 'ff6dff73',
            '20' => 'ff6dff74',
            'roman_1' => 'ff6dff75',
            'roman_2' => 'ff6dff76',
            'roman_3' => 'ff6dff77',
            'roman_4' => 'ff6dff78',
            'roman_5' => 'ff6dff79',
            'roman_6' => 'ff6dff80',
            'roman_7' => 'ff6dff81',
            'roman_8' => 'ff6dff82',
            'roman_9' => 'ff6dff83',
            'roman_10' => 'ff6dff84',
            'roman_11' => 'ff6dff85',
            'roman_12' => 'ff6dff86',
        );

        $code = $this->getArrValueByKey($str, $arr);
        if (!is_null($code)) {
            $e = pack("H*", "$code");
            $rtnStr = mb_convert_encoding($e, "SJIS", "UTF-16" );
        }

        return $rtnStr;
    }

    /**
     * 日付文字列を timestamp として取得する。
     * @param  string    $yyyymmddd
     * @return timestamp
     */
    function getTimestamp($yyyymmddd)
    {
        return mktime(0, 0, 0, substr($yyyymmddd, 4, 2), substr($yyyymmddd, 6, 2), substr($yyyymmddd, 0, 4));
    }
}