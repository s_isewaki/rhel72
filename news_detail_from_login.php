<?php
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("news_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// データベースに接続
$con = connect2db($fname);

// 職員IDが渡っている場合、未読なら既読にする
if ($emp_id != "") {
    set_news_read_flg($con, $news_id, $emp_id, $fname);
}

// お知らせ情報を取得
$sql = "
    select
        news.*,
        newscate.newscate_name,
        newsnotice.notice_name,
        newsnotice2.notice2_name,
        empmst.emp_lt_nm,
        empmst.emp_ft_nm,
        stmst.st_nm,
        srcempmst.emp_lt_nm as src_emp_lt_nm,
        srcempmst.emp_ft_nm as src_emp_ft_nm,
        (select class_nm from classmst where classmst.class_id = news.src_class_id) as src_class_nm,
        (select atrb_nm from atrbmst where atrbmst.atrb_id = news.src_atrb_id) as src_atrb_nm,
        (select dept_nm from deptmst where deptmst.dept_id = news.src_dept_id) as src_dept_nm,
        (select room_nm from classroom where classroom.room_id = news.src_room_id) as src_room_nm,
        srcstmst.st_nm as src_st_nm
    from
        ((((news
        inner join empmst on news.emp_id = empmst.emp_id)
        left join newsnotice on news.notice_id = newsnotice.notice_id
        left join newsnotice2 on news.notice2_id = newsnotice2.notice2_id)
        left join empmst srcempmst on news.src_emp_id = srcempmst.emp_id)
        left join stmst on empmst.emp_st = stmst.st_id)
        left join stmst srcstmst on srcempmst.emp_st = srcstmst.st_id
        inner join newscate on news.news_add_category = newscate.newscate_id
    ";
$cond = "where news.news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news_title = pg_fetch_result($sel_news, 0, "news_title");
$news_category = pg_fetch_result($sel_news, 0, "news_add_category");
$notice_name = pg_fetch_result($sel_news, 0, "notice_name");
$notice2_name = pg_fetch_result($sel_news, 0, "notice2_name");
$news = pg_fetch_result($sel_news, 0, "news");
$src_class_nm = pg_fetch_result($sel_news, 0, "src_class_nm");
$src_atrb_nm = pg_fetch_result($sel_news, 0, "src_atrb_nm");
$src_dept_nm = pg_fetch_result($sel_news, 0, "src_dept_nm");
$src_room_nm = pg_fetch_result($sel_news, 0, "src_room_nm");
$news_begin = pg_fetch_result($sel_news, 0, "news_begin");
$news_end = pg_fetch_result($sel_news, 0, "news_end");
$news_login = pg_fetch_result($sel_news, 0, "news_login");
$news_login_end = pg_fetch_result($sel_news, 0, "news_login_end");
$emp_name = pg_fetch_result($sel_news, 0, "emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "emp_ft_nm");
$emp_st = pg_fetch_result($sel_news, 0, "st_nm");
$news_date = pg_fetch_result($sel_news, 0, "news_date");
$src_emp_nm = pg_fetch_result($sel_news, 0, "src_emp_lt_nm") . " " . pg_fetch_result($sel_news, 0, "src_emp_ft_nm");
$src_emp_st = pg_fetch_result($sel_news, 0, "src_st_nm");
$newscate_name = pg_fetch_result($sel_news, 0, "newscate_name");
$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
$deadline = pg_fetch_result($sel_news, 0, "deadline");
$public_flg = pg_fetch_result($sel_news, 0, "public_flg");

// 添付ファイル情報を取得
$sql = "select newsfile_no, newsfile_name from newsfile";
$cond = "where news_id = $news_id order by newsfile_no";
$sel_file = select_from_table($con, $sql, $cond, $fname);
if ($sel_file == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$file_id = array();
$filename = array();
while ($row = pg_fetch_array($sel_file)) {
    array_push($file_id, $row["newsfile_no"]);
    array_push($filename, $row["newsfile_name"]);
}

// 組織名の取得
$arr_class_name = get_class_name_array($con, $fname);
$class_called = $arr_class_name[0];

// お知らせ区分一覧を取得
$notice2_list = get_notice2_list($con, $fname);

// 日付の表示設定
$news_begin = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_begin);
$news_end = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_end);
$news_login = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_login);
$news_login_end = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_login_end);
$news_date = preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $news_date);

// カテゴリの表示設定
switch ($news_category) {

    case "1":
        $news_sub_category = "";
        break;

    case "2":
        $news_sub_category = get_sub_category_label($con, $news_id, $fname);
        break;

    case "3":
        $news_sub_category = get_sub_category_label($con, $news_id, $fname);
        break;
}
// 役職情報取得
$st_nms = "";
$newsst_info = get_newsst_info($con, $fname, $news_id);
foreach ($newsst_info as $st_id => $st_nm) {
    if ($st_nms != "") {
        $st_nms .= "、";
    }
    $st_nms .= $st_nm;
}
// 回答設定を取得
$qa_cnt = 0;
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$answer_comment_flg = array();
$type = array();

if ($record_flg == "t") {
    $sql = "select question, select_num, multi_flg,type from newsenquet_q";
    $cond = "where news_id = $news_id order by question_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $i = 1;
    while ($row = pg_fetch_array($sel)) {
        $question[$i] = $row["question"];
        $select_num[$i] = $row["select_num"];
        $multi_flg[$i] = $row["multi_flg"];
        $type[$i] = $row["type"];
        $qa_cnt++;
        $i++;
    }

    for ($i = 1; $i <= $qa_cnt; $i++) {
        $sql = "select answer, answer_comment_flg from newsenquet_a";
        $cond = "where news_id = $news_id and question_no = $i order by item_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $j = 1;
        while ($row = pg_fetch_array($sel)) {
            $answer[$i][$j] = $row["answer"];
            $answer_comment_flg[$i][$j] = $row["answer_comment_flg"];
            $j++;
        }
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ログイン画面 | お知らせ</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
p.attach {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>お知らせ</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr height="22">
<td width="1%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">タイトル</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo(h($news_title)); ?></font></td>
</tr>
<tr height="22">
<td width="1%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">対象カテゴリ</font></td>
<td colspan="3">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding-right:5px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($newscate_name); ?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($news_sub_category); ?></font></td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">対象役職</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($st_nms); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">通達区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($notice_name); ?></font></td>
</tr>
<?php if (!empty($notice2_list)) {?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>お知らせ区分</nobr></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($notice2_name); ?></font></td>
</tr>
<?php }?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">発信部署</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($src_class_nm); ?><?php if ($src_atrb_nm != "") { ?> &gt; <?php echo($src_atrb_nm); ?><?php } ?><?php if ($src_dept_nm != "") { ?> &gt; <?php echo($src_dept_nm); ?><?php } ?><?php if ($src_room_nm != "") { ?> &gt; <?php echo($src_room_nm); ?><?php } ?></font></td>
</tr>
<tr height="22">
<td width="1%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">発信者</font></td>
<td width="49%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($src_emp_nm); ?></font></td>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>発信者役職</nobr></font></td>
<td width="49%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($src_emp_st); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">内容</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka"><?php echo(str_replace("\n", "<br>", preg_replace("/>(\r?\n)+</", "><", $news))); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>添付ファイル</nobr></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<?php
$filename_flg = lib_get_filename_flg();
$arr_imgtype = array("1"=>"IMAGETYPE_GIF", "2"=>"IMAGETYPE_JPEG", "3"=>"IMAGETYPE_PNG", "6"=>"IMAGETYPE_BMP");
for ($i = 0; $i < count($filename); $i++) {
    $tmp_file_id = $file_id[$i];
    $tmp_filename = $filename[$i];
    $ext = strrchr($tmp_filename, ".");

    echo("<p id=\"p_{$tmp_file_id}\" class=\"attach\">\n");
    if ($filename_flg > 0) {
        echo("<a href=\"news_file_refer.php?session=$session&news_id=$news_id&newsfile_no=$tmp_file_id\">{$tmp_filename}\n");
    } else {
        echo("<a href=\"news/{$news_id}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}\n");
    }

    // 画像ファイルチェック 20140121
    $type=exif_imagetype("news/{$news_id}_{$tmp_file_id}{$ext}");
    if(array_key_exists($type, $arr_imgtype))
    {
        $img_path = "news/{$news_id}_{$tmp_file_id}{$ext}";
        echo("</br><img src=\"$img_path\" border=\"0\" width=200 height=150 /></a>\n");

    }else{
        echo("</a>\n");
    }

    echo("</p>\n");
}
?>
</font></td>
</tr>
<?php if ($record_flg == "t" && $qa_cnt > 0) { ?>
<tr height="22">
<td align="right" valign="top" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">アンケート</font></td>
<td colspan="3">
<form name="mainform">
<table id="qas" width="100%" border="0" cellspacing="0" cellpadding="2">
<?php

for ($i = 1; $i <= $qa_cnt; $i++) {

    echo("<tr><td>\n");
    echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j18\">");
    echo("質問{$i} ".h($question[$i])."<br>\n");
    if($type[$i]==0){
    for ($j = 1; $j <= $select_num[$i]; $j++) {
        if ($multi_flg[$i] == "t") {
            echo("<input type=\"checkbox\" name=\"answer{$i}_{$j}\" value=\"{$j}\" {$arr_checked[$i][$j]}>".h($answer[$i][$j])."&nbsp;");
        } else {
            echo("<input type=\"radio\" name=\"answer{$i}\" value=\"{$j}\" {$arr_checked[$i][$j]}>".h($answer[$i][$j])."&nbsp;");
        }
        if ($answer_comment_flg[$i][$j] == 't') {
            echo("<input type=\"hidden\" name=\"answer_comment_flg{$i}_{$j}\" value=\"t\">");
            echo("<input type=\"text\" name=\"answer_comment{$i}_{$j}\" style=\"width: 200px;\" value=\"\">");
        }
        echo("<br>");
    }
    }else if($type[$i]==1){
        echo("<textarea name=\"free_format\" rows=\"5\" cols=\"50\" style=\"ime-mode: active;\" onFocus=\"new ResizingTextArea(this);\" class=\"j18\" disabled></textarea>");
    }
    echo("</font><br></td></tr>\n");
    echo("<tr><td><img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"></td></tr>\n");
}
?>
</table>

<table border="0" cellspacing="0" cellpadding="2">
<?php if ($deadline != "") { ?>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">締切り日時&nbsp;</td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php
$deadline_str = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $deadline);
 echo($deadline_str); ?></font></td>
</tr>
<?php } ?>
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>集計結果の公開&nbsp;</nobr></td><td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php
if ($public_flg == "t") {
    echo("公開");
} else {
    echo("非公開");
}
?></font></td>
</tr>
</table>

</form>
</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>掲載期間<br />（ログイン画面）</nobr></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($news_begin); ?> 〜 <?php echo($news_end); ?><br />（<?php echo "{$news_login} 〜 {$news_login_end}"; ?>）</font></td>
</tr>
<tr height="22">
<td width="1%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録者</font></td>
<td width="49%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($emp_name); ?></font></td>
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><nobr>登録者役職</nobr></font></td>
<td width="49%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($emp_st); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">登録日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><?php echo($news_date); ?></font></td>
</tr>
</table>
</center>
</body>
</html>
<?php pg_close($con);