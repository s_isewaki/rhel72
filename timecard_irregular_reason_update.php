<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 遅刻早退理由</title>
<?
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 遅刻早退理由を取得
$sql = "select trim(reason) as reason, tmcd_group_id from iregrsn"; // メディアテック
$cond = "where reason_id = $reason_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$reason = pg_fetch_result($sel, 0, "reason");

// opt以下のphpを表示するかどうか(申請用追加機能有無判定)
$opt_display_flag = file_exists("opt/flag");
if ($opt_display_flag) {
    // ----- メディアテック Start -----
    $group_id = pg_fetch_result($sel, 0, "tmcd_group_id");

    // 勤務グループ一覧を取得
    $get_group_sql = "select * from wktmgrp order by group_id";
    $grp_list = select_from_table($con, $get_group_sql, null, $fname);
    if ($grp_list == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}
// ----- メディアテック End -----
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="atdbk_register.php?session=<? echo($session); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="atdbk_register.php?session=<? echo($session); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="atdbk_register.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="updform" action="timecard_irregular_reason_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻早退理由</font></td>
<td><input type="text" name="reason" value="<? echo($reason); ?>" size="50" maxlength="100" style="ime-mode:active;">
<input type="checkbox" name="ireg_no_deduction_flg" id="ireg_no_deduction_flg" value="t" <? if ($ireg_no_deduction_flg == "t") { echo(" checked"); }?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻控除しない</font>

<?php
if ($opt_display_flag) {
?>
<!-- メディアテック Start -->
</td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務グループ</font></td>
<td>
	<select name="wktmgrp">
		<option value="" label="">
		<?php
		while ($row = pg_fetch_array($grp_list)) {
			$id = $row["group_id"];
			$name = h($row["group_name"]);
			if ($id == $group_id) {
				echo "<option value=\"$id\" selected>$name</option>";
			} else {
				echo "<option value=\"$id\">$name</option>";
			}
		}
		?>
  </select>
<!-- メディアテック End -->
<? } ?>
</td>
</tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right">
<input type="submit" value="更新">
<input type="button" value="戻る" onclick="history.back();">
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="reason_id" value="<? echo($reason_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
