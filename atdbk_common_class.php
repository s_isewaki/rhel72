<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("holiday.php");

class atdbk_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション

	// コンボボックス変更用javascript
	var $javascript_pattern = "";

	// グループｘパターン保持領域
	var $group_pattern_array = array();

	// パターンｘ時間種別保持領域
	var $officehours_array = array();

	// グループIDｘグループ名
	var $group_info_array = array();

	// 事由情報保持領域
	var $reason_info_array = array();

	// カレンダー名情報
	var $calendarname_info_array = array();

	// 会議・研修・病棟外時間情報
	var $meeting_time_array = array();
    // 当直・待機
    var $duty_or_oncall_str = "当直";

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function atdbk_common_class($con, $fname)
	{
		global $javascript_pattern;
		global $group_pattern_array;
		global $group_info_array;
		global $reason_info_array;
		global $calendarname_info_array;
		global $group_pattern_reason_array;
		global $meeting_time_array;

		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;

		//全グループｘパターン取得
		$sql = "select group_id, group_name from wktmgrp";
		$cond = "order by group_id";
		$sel = select_from_table($con, $sql, $cond, $fname);

		while ($row = pg_fetch_array($sel)) {

			$group_id = $row["group_id"];
			$group_name = $row["group_name"];

			$pattern_array = array();
			$pattern_reason_array = array();
			$sql = "select * from atdptn";
//			$cond = "WHERE group_id = ".$group_id." order by atdptn_id";
//20101006
			$cond = "WHERE group_id = ".$group_id." and display_flag = 't' order by atdptn_order_no, atdptn_id";
			$sel2 = select_from_table($con, $sql, $cond, $fname);
			$javascript_pattern = $javascript_pattern."			case '".$group_id."':\n";
			while ($row2 = pg_fetch_array($sel2)) {
				//javascriptを保持
				$patternName = $row2["atdptn_nm"];
				$patternId   = $row2["atdptn_id"];
				$javascript_pattern = $javascript_pattern."				addCmbOpt(objId, '".$patternName."', '".$patternId."');\n";
				//パターンを保持
				$pattern_array["{$row2["atdptn_id"]}"] = $row2["atdptn_nm"];
				$pattern_reason_array["{$row2["atdptn_id"]}"] = $row2["reason"];
				//会議・研修・病棟外時間を保持
				if ($row2["meeting_start_time"] != "" && $row2["meeting_end_time"] != "") {
					$meeting_time_array[$group_id][$patternId]["meeting_start_time"] = $row2["meeting_start_time"];
					$meeting_time_array[$group_id][$patternId]["meeting_end_time"] = $row2["meeting_end_time"];
				}
			}
			$javascript_pattern = $javascript_pattern."				break;\n";

			//グループｘパターンを変数に保持
			$group_pattern_array["{$group_id}"] = $pattern_array;
			//グループIDｘグループ名を変数に保持
			$group_info_array["{$group_id}"] = $group_name;
			//事由
			$group_pattern_reason_array["{$group_id}"] = $pattern_reason_array;
		}

		// 事由情報保持領域
		$sql = "select * from atdbk_reason_mst";
		$cond = "order by sequence";
		$sel = select_from_table($con, $sql, $cond, $fname);

		while ($row = pg_fetch_array($sel)) {
			$reason_id = $row["reason_id"];

			//表示名商未指定時はデフォルト名称を出力
			$default_name   = $row["default_name"];
			$display_name   = $row["display_name"];
			//表示名商未指定時はデフォルト名称を出力
			if (empty($display_name)){
				$row["reason_name"] = $default_name;
			}
			else{
				$row["reason_name"] = $display_name;
			}

			$reason_info_array["{$reason_id}"] = $row;
		}
		// カレンダー名情報取得
		$sql = "select day1_time, day2, day3, day4, day5 from calendarname";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$calendarname_info = array();
		if (pg_num_rows($sel) > 0) {
			$calendarname_info["day2"] = pg_fetch_result($sel, 0, "day2");
			$calendarname_info["day3"] = pg_fetch_result($sel, 0, "day3");
			$calendarname_info["day4"] = pg_fetch_result($sel, 0, "day4");
			$calendarname_info["day5"] = pg_fetch_result($sel, 0, "day5");
			$calendarname_info["day1_time"] = pg_fetch_result($sel, 0, "day1_time");
		}
		else{
			$calendarname_info["day1_time"] = "";
		}
		if (empty($calendarname_info["day2"])) {
			$calendarname_info["day2"] = "法定休日";
		}
		if (empty($calendarname_info["day3"])) {
			$calendarname_info["day3"] = "所定休日";
		}
		if (empty($calendarname_info["day4"])) {
			$calendarname_info["day4"] = "勤務日1";
		}
		if (empty($calendarname_info["day5"])) {
			$calendarname_info["day5"] = "勤務日2";
		}
		$calendarname_info["day1"] = "通常勤務日";
		$calendarname_info["day6"] = "祝日";
		$calendarname_info["day7"] = "年末年始休暇";
        $calendarname_info["day8"] = "買取日";

		$calendarname_info_array["calendarname_info"] = $calendarname_info;

		// system_config
		$conf = new Cmx_SystemConfig();
		$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
		if ($duty_or_oncall_flg == "") {
			$duty_or_oncall_flg = "1";
		}
		$this->duty_or_oncall_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";
	}

	//コンボボックスの動的変更を行うためのjavascriptを出力する
	function setJavascript(){
		global $javascript_pattern;

		echo("	<script type='text/javascript'>\n");
		echo("		<!--\n\n");

		echo("			function addCmbOpt(id, text, value, select){\n");
		echo("				if(!document.createElement){\n");
		echo("					return;\n");
		echo("				}\n\n");

		echo("				// IDからコンボボックスのオブジェクトを取得\n");
		echo("				objCmb = document.getElementById(id);\n\n");


		echo("				// optionオブジェクトを生成\n");
		echo("				var option = document.createElement('OPTION');\n");
		echo("				// optionの表示部を生成\n");
//		echo("				option.innerHTML = text;\n");
		echo("				option.text = text;\n");

		echo("				// optionオブジェクトの値を設定\n");
		echo("				option.value     = value;\n");

		echo("				//追加した項目を選択\n");
		echo("				if (select){\n");
//		echo("					option.setAttribute('selected', true);\n");
		echo("					option.selected = true;\n");
		echo("				}\n");

		echo("				// selectオブジェクト(ノード)にoption部分(ノード)を追加 \n");
//		echo("				objCmb.appendChild(option);\n");
		echo("				objCmb.options[objCmb.length] = option;\n");

		echo("			}\n");

		echo("			function clearCmbOpt(id){\n");
		echo("				var objCmb = document.getElementById(id);\n");
		echo("				var comCount = objCmb.length;\n");
		echo("				while (objCmb.length > 0){\n");
		echo("					objCmb.remove(objCmb.length - 1);\n");
		echo("				}\n");
		echo("			}\n");

		echo("			function setAtdptn(groupId, objId, selected){\n");

		echo("				if (objId != null){\n");

		echo("					//クリア\n");
		echo("					clearCmbOpt(objId);\n");

		echo("					//先頭に空白行追加\n");
		echo("					addCmbOpt(objId, '', '--');\n");

		echo("					//追加\n");
		echo("					switch (groupId) {\n");

		echo($javascript_pattern);

		echo("					}\n");
		echo("				}\n");
		echo("			}\n");
		echo("		//-->\n");
		echo("	</script>\n");

	}

	/** グループコンボボックスの出力を行う
	 *  @param $name			selectタグのname
	 *  @param $id				selectタグのid
	 *  @param $selectedGroupId	コンボで選択するgroupId
	 *  @param $patternCmbId	グループ変更時に変更するパターンコンボボックスのid
	 *  @param $enable			コントロールの操作有効/無効 デフォルト：有効
	 *  @return                 最終的に表示されるグループのグループID
	 */
	function setGroupOption($name, $id, $selectedGroupId, $patternCmbId, $enable = true){

		if ($enable) {
			echo("<select name=\"$name\" id=\"$id\" onChange=\"setAtdptn(this.value, '$patternCmbId')\">");
		} else {
			//操作向こうの場合は、hiddenに値を保持
			echo("<input type=\"hidden\" name=\"$name\" value=\"$selectedGroupId\"> \n");
			echo("<select name=\"\" disabled>");
		}

		global $group_info_array;

		$selected_group_id = "";
		foreach ($group_info_array as $workGroupId => $group_val) {
			if (empty($selected_group_id)){
				//最初に追加するグループIDを保持
				$selected_group_id = $workGroupId;
			}

			echo("<option value=\"$workGroupId\"");

			if ($selectedGroupId == $workGroupId) {
				echo(" selected");
				//選択表示するグループIDを保持
				$selected_group_id = $workGroupId;
			}
			echo(">$group_val");
		}
		echo("</select>");

		return $selected_group_id;
	}

	/** 事由コンボボックスの出力を行う
	 *  $name				selectタグのname
	 *  $selected_reason_id	コンボで選択するreason_id
	 *  $enable				コントロールの操作有効/無効 デフォルト：有効
	 *  $str_flg            true:echoで出力している文字列を呼出元へ返す false:文字列をechoで表示する
	 *  return $str_flgがtrueの場合に<select>以下の文字列を返す
	 */
	function set_reason_option($name, $selected_reason_id, $enable = true, $str_flg = false){
		$str = "";
		if ($enable) {
			$str .= "<select class=\"js-class-$name\" name=\"$name\">\n";
        } else {
			//操作向こうの場合は、hiddenに値を保持
			$str .= "<input type=\"hidden\" name=\"$name\" value=\"$selected_reason_id\"> \n";
			$str .= "<select name=\"\" disabled>\n";
		}

		global $reason_info_array;

		$work_reason_option = "";
		$str .= "	<option value=\"--\">\n";
		foreach ($reason_info_array as $reason_id => $reason_info) {
			$display_name   = $reason_info["display_name"];
			$default_name   = $reason_info["default_name"];
			$display_flag   = $reason_info["display_flag"];
			$reason_name    = $reason_info["reason_name"];

			//選択した事由が非表示の場合最後に追加する
			if ($selected_reason_id == $reason_id && $display_flag != "t") {
				$work_reason_option = "	<option value=\"$reason_id\" selected>$reason_name\n";
			}
			//非表示以外の場合追加
			else if ($display_flag == "t"){
				if ($selected_reason_id == $reason_id) {
					$str .= "	<option value=\"$reason_id\" selected>$reason_name\n";
				}
				else{
					$str .= "	<option value=\"$reason_id\">$reason_name\n";
				}
			}
		}
		if(empty($work_reason_option) == false){
			$str .= $work_reason_option;
		}
		$str .= "</select>\n";

		if ($str_flg) {
			return $str;
		} else {
			echo($str);
		}
	}

	//パターンArray取得関数
	function get_pattern_array($group_id){
		global $group_pattern_array;
		return $group_pattern_array[$group_id];
	}

	//パターン名取得関数
	function get_pattern_name($group_id, $pattern){
		global $group_pattern_array;
		$patternArray = $group_pattern_array[$group_id];
		return $patternArray[$pattern];
	}

	//事由情報取得関数
	function get_pattern_reason_array($group_id){
		global $group_pattern_reason_array;
		if ($group_id == "") {
			return $group_pattern_reason_array;
		} else {
			return $group_pattern_reason_array[$group_id];
		}
	}

	//グループArray取得
	function get_group_array(){
		global $group_info_array;
		return $group_info_array;
	}

	//グループ名取得
	function get_group_name($group_id){
		global $group_info_array;
		return $group_info_array[$group_id];
	}

	//事由名取得
	function get_reason_name($reason_id){
		global $reason_info_array;
		$reason_info = $reason_info_array[$reason_id];
		return $reason_info["reason_name"];
	}

	//事由の日数取得
	function get_reason_day_count($reason_id){
		global $reason_info_array;
		$reason_info = $reason_info_array[$reason_id];
		$result_day_count = $reason_info["day_count"];
		if (empty($result_day_count)){
			$result_day_count = 0;
		}
		return $result_day_count;
	}
	//事由の表示フラグ取得
	function get_reason_display_flag($reason_id){
		global $reason_info_array;
		$reason_info = $reason_info_array[$reason_id];
		$result_display_flag = $reason_info["display_flag"];
		if (empty($result_display_flag)){
			$result_display_flag = 't';
		}
		return $result_display_flag;
	}

	//追加表示する事由IDを取得
	function get_add_disp_reason_id() {
		//年末年始追加61 法定22、所定23追加 20110121
        //debug 20140625
        $arr_reason_id = array("24", "22", "23", "5", "25", "26", "27", "28", "29", "30", "31", "61", "32", "40", "41");
/*
        , "4", "14", "15", "17"
        , "代替休暇", "代替出勤", "振替出勤", "振替休暇"
*/
		return $arr_reason_id;
	}
	//追加表示する事由名を取得（上の事由IDと対応する）
	function get_add_disp_reason_name() {
        $arr_reason_name = array("公休", "法定", "所定", "特休", "産休", "育休", "介休", "傷休", "学休", "忌引", "夏休", "年末<br>年始", "結休", "リフレッ<br>シュ", "初盆<br>休暇");
		return $arr_reason_name;
	}
	//追加表示する事由名を取得（上の事由IDと対応する）
	//表示変更がある場合はそちらを優先する
	function get_add_disp_reason_name2() {
		$arr_reason_id = $this->get_add_disp_reason_id();
		$arr_reason_name = $this->get_add_disp_reason_name();

		$cond_id = "";
		for ($i=0; $i<count($arr_reason_id); $i++) {
			if ($i != 0) {
				$cond_id .= ",";
			}
			$cond_id .= "'$arr_reason_id[$i]'";
		}

		$sql = "select reason_id,display_name from atdbk_reason_mst ";
		$cond = "where reason_id in ($cond_id) and display_name != ''";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {
			$display_name = $row["display_name"];
			if ($display_name != "") {
				$reason_id = $row["reason_id"];
				for ($i=0; $i<count($arr_reason_id); $i++) {
					if ($reason_id == $arr_reason_id[$i]) {
						$arr_reason_name[$i] = $display_name;
						break;
					}
				}
			}
		}
		return $arr_reason_name;
	}
	/**
	 * カレンダー名を返す
	 * @param  $type
	 * @param  $date
	 * @return $calendarname （通常勤務日、法定休日、祝日名等）
	 */
	function get_calendarname($type, $date){
		global $calendarname_info_array;

		switch ($type) {
		case "2":	// 勤務日1
			$var = "day4";
			break;
		case "3":	// 勤務日2
			$var = "day5";
			break;
		case "4":	// 法定休日
			$var = "day2";
			break;
		case "5":	// 所定休日
			$var = "day3";
			break;
		default:	// 1:通常勤務日 6:祝日 7:年末年始休暇
			$var = "day".$type;
		}

		$calendarname_info = $calendarname_info_array["calendarname_info"];

		//祝日、法定休日
		if ($type == "6" || $type == "4") {
			$calendarname = get_holiday_name($date);
//			$calendarname = $calendarname_info[$var]."(".$calendarname.")";
			// 振替休日以外は法定休日
			if ($type == "4" && $calendarname == "") {
				$calendarname = $calendarname_info[$var];
			}

		} else {
			$calendarname = $calendarname_info["$var"];
		}
		return $calendarname;
	}
	/**
	 * 支給時間を取得(支給時間＝勤務時間＋有給取得日数×所定労働時間)
	 * @param $work_time 勤務時間hh:mm or hh.hh(1.25等)
	 * @param $paid_hol_days 有給取得日数
	 * @param $emp_id 職員ID
	 *
	 * return 支給時間、単位分
	 *
	 * ※半日有休対応のため未使用 20110204
	 */
	function get_paid_time_old($work_time, $paid_hol_days, $emp_id) {
		//勤務時間を分単位にする
		if (strpos($work_time, ".", 0) !== false) {
			$arr_work_time = split("\.", $work_time);
			$hour = (int)$arr_work_time[0];
			$wk_minute = (strlen($arr_work_time[1]) == 1) ? $arr_work_time[1]."0" : $arr_work_time[1];
			$min = $hour * 60 + ($wk_minute * 60 / 100);
		} else {
			$arr_work_time = split(":", $work_time);
			$hour = (int)$arr_work_time[0];
			$min = $hour * 60 + (int)$arr_work_time[1];
		}

		//有給取得日数を時間換算
		if ($paid_hol_days > 0) {
			//所定労働時間を取得
			$sql =	"SELECT ".
					"specified_time FROM empcond ";
			$cond = "WHERE ".
					"empcond.emp_id = '$emp_id' ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$specified_time = "";
			if (pg_num_rows($sel) > 0) {
				$specified_time = pg_fetch_result($sel, 0, "specified_time");
			}

			//ない場合は、カレンダー名テーブルから取得
			if ($specified_time == "") {
				$sql =	"SELECT ".
						"day1_time FROM calendarname ";
				$cond = "";
				$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel == 0) {
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				if (pg_num_rows($sel) > 0) {
					$specified_time = pg_fetch_result($sel, 0, "day1_time");
				}
			}

			if ($specified_time != "") {
				//所定労働時間を分単位にする
				$hour2 = (int)substr($specified_time, 0, 2);
				$min2 = $hour2 * 60 + (int)substr($specified_time, 2, 3);
				//日数分を加算する
				$min += $min2 * $paid_hol_days;
			}

		}

		return $min;
	}
	/**
	 * 支給時間を取得(支給時間＝勤務時間＋有給取得日数(半日以外）×所定労働時間＋午前半休日数×半日午前所定時間＋午後半休日数×半日午後所定時間)
	 * 20110204半日有休対応
	 * @param $work_time 勤務時間hh:mm or hh.hh(1.25等)
	 * @param $paid_hol_days 有給取得日数(半日以外）
	 * @param $am_hol_days 午前半休回数
	 * @param $pm_hol_days 午後半休回数
	 * @param $emp_id 職員ID
	 *
	 * return 支給時間、単位分
	 */
	function get_paid_time($work_time, $paid_hol_days, $am_hol_days, $pm_hol_days, $emp_id) {
		//勤務時間を分単位にする
		if (strpos($work_time, ".", 0) !== false) {
			$arr_work_time = split("\.", $work_time);
			$hour = (int)$arr_work_time[0];
			$wk_minute = (strlen($arr_work_time[1]) == 1) ? $arr_work_time[1]."0" : $arr_work_time[1];
			$min = $hour * 60 + ($wk_minute * 60 / 100);
		} else {
			$arr_work_time = split(":", $work_time);
			$hour = (int)$arr_work_time[0];
			$min = $hour * 60 + (int)$arr_work_time[1];
		}

		//有給取得日数を時間換算
		if ($paid_hol_days > 0 || $am_hol_days > 0 || $pm_hol_days > 0) {
			//所定労働時間を取得
			$sql =	"SELECT ".
				"specified_time, am_time, pm_time FROM empcond ";
			$cond = "WHERE ".
				"empcond.emp_id = '$emp_id' ";
			$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($this->_db_con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$specified_time = "";
			if (pg_num_rows($sel) > 0) {
				$specified_time = pg_fetch_result($sel, 0, "specified_time");
				$am_time = pg_fetch_result($sel, 0, "am_time");
				$pm_time = pg_fetch_result($sel, 0, "pm_time");
			}

			//ない場合は、カレンダー名テーブルから取得
			if ($specified_time == "" || $am_time == "" || $pm_time == "") {
				$sql =	"SELECT ".
					"day1_time, am_time, pm_time FROM calendarname ";
				$cond = "";
				$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
				if ($sel == 0) {
					pg_close($this->_db_con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				if (pg_num_rows($sel) > 0) {
					//勤務条件にない場合に、カレンダー名から設定
					if ($specified_time == "") {
						$specified_time = pg_fetch_result($sel, 0, "day1_time");
					}
					if ($am_time == "") {
						$am_time = pg_fetch_result($sel, 0, "am_time");
					}
					if ($pm_time == "") {
						$pm_time = pg_fetch_result($sel, 0, "pm_time");
					}
				}
			}
			//半日午前所定時間がある場合は計算し加算
			if ($am_time != "") {
				$hour2 = (int)substr($am_time, 0, 2);
				$min2 = $hour2 * 60 + (int)substr($am_time, 2, 3);
				//日数分を加算する(0.5単位のため、1回分の計算として2倍する）
				$min += $min2 * $am_hol_days * 2;
			}
			else {
				//ない場合は、半日以外の所定に加算
				$paid_hol_days += $am_hol_days;
			}
			//半日午後所定時間がある場合は計算し加算
			if ($pm_time != "") {
				$hour2 = (int)substr($pm_time, 0, 2);
				$min2 = $hour2 * 60 + (int)substr($pm_time, 2, 3);
				//日数分を加算する(0.5単位のため、1回分の計算として2倍する）
				$min += $min2 * $pm_hol_days * 2;
			}
			else {
				//ない場合は、半日以外の所定に加算
				$paid_hol_days += $pm_hol_days;
			}

			//半日以外の換算日数
			if ($specified_time != "") {
				//所定労働時間を分単位にする
				$hour2 = (int)substr($specified_time, 0, 2);
				$min2 = $hour2 * 60 + (int)substr($specified_time, 2, 3);
				//日数分を加算する
				$min += $min2 * $paid_hol_days;
			}

		}

		return $min;
	}
	/**
	 * 早退時間を残業時間から減算する設定の場合の、法定内残業、法定外残業を計算して返す
	 * @param $hoteinai_zan 法定内残業
	 * @param $hoteinai_zan 法定内残業
	 * @param $hoteigai 法定外残業
	 * @param $early_leave_time 職員ID
	 *
	 * return array(法定内残業,法定外残業)
	 */
	function get_overtime_early_leave($hoteinai_zan, $hoteigai, $early_leave_time) {

		$ret_hoteinai_zan = $hoteinai_zan;
		$ret_hoteigai = $hoteigai;
		if ($early_leave_time > 0) {

			//減算
			$ret_hoteinai_zan -= $early_leave_time;
			//法定内時間がマイナスになる（残りがある）場合
			if ($ret_hoteinai_zan < 0) {
				//法定外時間から減算
				$ret_hoteigai += $ret_hoteinai_zan;
				$ret_hoteinai_zan = 0;
				//法定外時間がマイナスになる場合
				if ($ret_hoteigai < 0) {
					//法定内時間に表示
					$ret_hoteinai_zan = $ret_hoteigai;
					$ret_hoteigai = 0;
				}
			}
		}

		return array($ret_hoteinai_zan, $ret_hoteigai);
	}


	/**
	 * 会議・研修・病棟外時間情報取得
	 *
	 * @return array 会議・研修・病棟外時間情報 group_id、atdptn_ptn_id、"meeting_start_time"、"meeting_end_time"をキーとした配列
	 * 例 $meeting_time_array[1][2]["meeting_start_time"]
	 */
	function get_atdbk_meeting_time() {
		global $meeting_time_array;

		return $meeting_time_array;
	}


	/**
	 * 有給付与日数表の表名称取得
	 *
	 * @param $id 1〜8
	 * @return 名称 1:常勤、2:週４日、3:週３日、4:週２日、5:週１日、6:定時、7:パート、8:週５日、"":なし、範囲外:""
	 *　※2-5、8の名称はアマノ移行データと合わせ全角数字としている
	 */
	function get_paid_hol_tbl_id_name($id) {

        $str = "";
        switch($id){
        case "":
            $str = "なし";
            break;
        case 1:
            $str = "常勤";
            break;
        case 2:
            $str = "週４日";
            break;
        case 3:
            $str = "週３日";
            break;
        case 4:
            $str = "週２日";
            break;
        case 5:
            $str = "週１日";
            break;
        case 6:
            $str = "定時";
            break;
        case 7:
            $str = "パート";
            break;
        case 8:
            $str = "週５日";
            break;
        default:
            $str = "";
            break;
		}

        return $str;
	}
}
?>