<?
require_once('about_comedix.php');
require_once("show_select_values.ini");
require_once("hiyari_el_search.ini");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");
require_once("Cmx.php");
require_once('Cmx/View/Smarty.php');

$a = 2;//医療安全のみが検索対象

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
if (check_authority($session, 47, $fname) == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//==================================================
//デザイン（1=旧デザイン、2=新デザイン）
//==================================================
$sysConf = new Cmx_SystemConfig();
$design_mode = $sysConf->get('fantol.design.mode');

//==============================
// ログインユーザの職員IDを取得
//==============================
$self_emp_id = get_emp_id($con, $session, $fname);


//====================================
//表示
//====================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("PAGE_TITLE", "コンテンツ検索");
$smarty->assign("session", $session);

$smarty->assign("a", $a);
$smarty->assign("lib_nm", $lib_nm);
$smarty->assign("keywd", $keywd);
$smarty->assign("hid_emp_name", $hid_emp_name);
$smarty->assign("hid_emp_id", $hid_emp_id);
$smarty->assign("submit", $submit);

$smarty->assign("years",  get_select_years(10, $year, true));
$smarty->assign("months", get_select_months($month, true));
$smarty->assign("days",   get_select_days($day, true));

// コンテンツ一覧
$library_list = array();
$docs = get_search_library_list($con, $self_emp_id, $arch_id, $lib_nm, $hid_emp_id, $keywd, $year, $month, $day, $session, $fname);
foreach ($docs as $tmp_did => $tmp_doc){
    $library = array();
    
    $tmp_dnm = $tmp_doc["name"];
    $tmp_type = $tmp_doc["type"];
    $tmp_upd_time = $tmp_doc["upd_time"];
    $tmp_modify_flg = $tmp_doc["modify_flg"];
    $tmp_aid = $tmp_doc["aid"];
    $tmp_cid = $tmp_doc["cid"];
    $tmp_fid = $tmp_doc["fid"];
    $tmp_ext = $tmp_doc["ext"];

    // 容量
    $doc_path = get_lib_path($tmp_cid, $tmp_did, $tmp_ext);
    $size = filesize($doc_path);
    if ($size <= 1023){
        $size .= "バイト";
    }
    else if ($size >= 1024 && $size <= 1048576){
        $size = number_format(($size / 1024), 2) . "KB";
        $size = str_replace(".00", "", $size);
        $size = preg_replace("/(.*)\.(\d)0/", "$1.$2", $size);
    }
    else{
        $size = number_format(($size / 1024 / 1024), 2) . "MB";
        $size = str_replace(".00", "", $size);
        $size = preg_replace("/(.*)\.(\d)0/", "$1.$2", $size);
    }

    // タイプ
    switch ($tmp_type){
        case "1":
            $type_str = "Word";
            break;
        case "2":
            $type_str = "Excel";
            break;
        case "3":
            $type_str = "PowerPoint";
            break;
        case "4":
            $type_str = "PDF";
            break;
        case "5":
            $type_str = "テキスト";
            break;
        case "6":
            $type_str = "JPEG";
            break;
        case "7":
            $type_str = "GIF";
            break;
        case "99":
            $type_str = "その他";
            break;
        default:
            $type_str = "";
    }

    // 更新日時
    if (strlen($tmp_upd_time) == 14){
        $upd_time_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $tmp_upd_time);
    }
    else{
        $upd_time_str = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_upd_time);
    }

    // 権限
    if ($tmp_modify_flg == "t"){
        $auth = "更新";
    }
    else{
        $auth = "参照";
    }

    $tmp_lib_url = urlencode($doc_path);

    $library['did'] = $tmp_did;
    $library['cid'] = $tmp_cid;
    $library['fid'] = $tmp_fid;
    $library['dnm'] = $tmp_dnm;
    $library['type'] = $type_str;
    $library['size'] = $size;
    $library['upd_time'] = $upd_time_str;
    $library['auth'] = $auth;
    $library['url'] = $tmp_lib_url;

    $library_list[] = $library;
}
$smarty->assign("library_list", $library_list);
$smarty->assign("lib_get_filename_flg", lib_get_filename_flg());

if ($design_mode == 1){
    $smarty->display("hiyari_el_search1.tpl");
}
else{
    $smarty->display("hiyari_el_search2.tpl");
}
