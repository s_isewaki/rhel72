<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
// 所定時間の設定
// work_admin_csv_fixed_set.php
//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
    $title2 = "CSV所定時間の設定";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
    $title2 = "所定時間の設定";
}
?>
<title>CoMedix <? echo($title1." | ".$title2); ?></title>

<?
require_once("about_comedix.php");
require_once("work_admin_csv_common.ini");
require_once("duty_shift_mprint_common_class.php");
///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
if ($postback == "1") {
?>
<form name="items" method="post" action="work_admin_csv_fixed_set.php">
<input type="hidden" name="session" value="<?=$session?>">

<input type="hidden" name="ptn_id" value="<?=$ptn_id?>">
<input type="hidden" name="hour" value="<?=$hour?>">
<input type="hidden" name="minute" value="<?=$minute?>">
<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
<input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
</form>
	<?
	//入力チェック
	if ($hour != "" && !preg_match("/^[0-9]*$/", $hour)) {	
		echo("<script type=\"text/javascript\">alert('時間には数値を入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	if ($minute != "" && !preg_match("/^[0-9]*$/", $minute)) {	
		echo("<script type=\"text/javascript\">alert('分には数値を入力してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$arr_atdptn = get_atdptn_id_name($con, $fname);

///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------

$arr_ptn_id_list = array();
//登録時
if ($postback == "1") {
    update_fixed_set($con, $fname, $ptn_id, $hour, $minute, $layout_id, $from_flg);
} else {
	//初期表示時、DBから取得
    $arr_config = get_timecard_csv_config($con, $fname, $layout_id, $from_flg);
	$ptn_id = $arr_config["fixed_set_pattern_id"]."_".$arr_config["fixed_set_atdptn_ptn_id"];
	$hour = $arr_config["fixed_set_hour"];
	$minute = $arr_config["fixed_set_minute"];
}
//レイアウト名取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, $layout_id);
    $layout_name = $arr_layout[$layout_id]["name"];
}
else {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
    $layout_name = $arr_layout_mst["layout_name"];
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registFixedset() {

	document.mainform.postback.value = "1";
	document.mainform.action = "work_admin_csv_fixed_set.php";
	document.mainform.submit();
}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>所定時間の設定</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
<?
if ($postback == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
登録しました。
</font>
	<?	
}
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><br><?
if ($from_flg != "2") {
    echo("レイアウト".$layout_id);
}
else {
    echo("帳票名称");
}        
echo(" ");
echo($layout_name); ?></font></td>
	</tr>
	<tr height="22">
		<td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		所定時間＝勤務時間＋有給休暇日数×所定労働時間−法定外残業時間−
		</font><td>
		<td colspan="1" align="right">
		</td>
	</tr>
	</table>
		<table id="ptns" width="" border="0" cellspacing="0" cellpadding="0">
		<tr height="22">
		<td width="200">
		<select name="ptn_id" style="width:280px;">
<?
for ($i=0; $i<count($arr_atdptn); $i++) {
	$selected = ($ptn_id == $arr_atdptn[$i]["id"]) ? " selected" : "";
	echo("<option value=\"{$arr_atdptn[$i]["id"]}\" $selected>{$arr_atdptn[$i]["name"]}</option>\n");
}

		?>
		</select>
		</td>
		<td width="300" align="left" valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;×
		<input type="text" name="hour" size="2" maxlength="2" value="<?=$hour?>" style="ime-mode:inactive;">時間
		<input type="text" name="minute" size="2" maxlength="2" value="<?=$minute?>" style="ime-mode:inactive;">分
		</font>
		</td>
		</tr>
		</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
		<td width="20"><td>
		<td colspan="1" align="right">
			<input type="button" value="登録" onclick="registFixedset();">&nbsp;&nbsp;
		</td>
	</tr>
	</table>

		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="postback" value="">
		<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
	    <input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
	</form>

</body>
<? pg_close($con); ?>
</html>
<?
//更新
function update_fixed_set($con, $fname, $ptn_id, $hour, $minute, $layout_id, $from_flg) {
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_config";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_config";
        $wk_layout_id = "$layout_id";
    }
    
	///-----------------------------------------------------------------------------
	// トランザクションを開始
	///-----------------------------------------------------------------------------
	pg_query($con, "begin transaction");
	
    $sql = "select * from $tablename";
    $cond = "where layout_id = $wk_layout_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	list($pattern_id, $atdptn_ptn_id) = split("_", $ptn_id);
	if (pg_num_rows($sel) > 0) {
        $sql = "update $tablename set";
		$set = array("fixed_set_pattern_id", "fixed_set_atdptn_ptn_id", "fixed_set_hour", "fixed_set_minute");
		$setvalue = array($pattern_id, $atdptn_ptn_id, $hour, $minute);
        $cond = "where layout_id = $wk_layout_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
			
	} else {
		
        $sql = "insert into $tablename (fixed_set_pattern_id, fixed_set_atdptn_ptn_id, fixed_set_hour, fixed_set_minute, layout_id";
		$sql .= ") values (";
		
		$content = array($pattern_id, $atdptn_ptn_id, $hour, $minute, $layout_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}	
	///-----------------------------------------------------------------------------
	// トランザクションをコミット
	///-----------------------------------------------------------------------------
	pg_query("commit");
}

// 勤務パターン取得 id:シフトグループ_勤務パターン name:シフトグループ＞勤務パターンの配列
function get_atdptn_id_name($con, $fname) {
	
	//勤務パターンを取得
	$sql = "select a.group_id, a.atdptn_id, a.atdptn_nm, b.group_name, a.allowance from atdptn a left join wktmgrp b on b.group_id = a.group_id";
	$cond = "order by b.group_id, a.atdptn_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$arr_atdptn = array();
	if (pg_num_rows($sel) > 0) {
		while ($row = pg_fetch_array($sel)) {
			$tmp_group_id = $row["group_id"];
			$tmp_atdptn_id = $row["atdptn_id"];
			$tmp_group_name = $row["group_name"];
			$tmp_atdptn_nm = $row["atdptn_nm"];

			$arr_atdptn[] = array("id" => $tmp_group_id."_".$tmp_atdptn_id, "name" => $tmp_group_name."＞".$tmp_atdptn_nm);
		}
	}
	return $arr_atdptn;
	
}
?>
