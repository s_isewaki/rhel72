<?
ob_start();
require_once("about_comedix.php");
require_once("atdbk_menu_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_menu_common_class.php");
require_once("duty_shift_print_common_class.php");
require_once("duty_shift_duty_common_class.php");
ob_end_clean();
///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_menu = new duty_shift_menu_common_class($con, $fname);
$obj_print = new duty_shift_print_common_class($con, $fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//現在日付の取得
	///-----------------------------------------------------------------------------
	$date = getdate();
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	$now_yyyymm = sprintf("%04d%02d", $now_yyyy, $now_mm);
	$now_date = sprintf("%04d%02d%02d", $now_yyyy, $now_mm, $now_dd);
	///-----------------------------------------------------------------------------
	//翌月
	///-----------------------------------------------------------------------------
	if ((int)$now_mm == 12){
		$next_yyyy = (int)$now_yyyy + 1;
		$next_mm = 1;
	} else {
		$next_yyyy = $now_yyyy;
		$next_mm = (int)$now_mm + 1;
	}
	$next_dd = $now_dd;

	$next_yyyymm = sprintf("%04d%02d", $next_yyyy, $next_mm);
	///-----------------------------------------------------------------------------
	//表示年／月設定
	///-----------------------------------------------------------------------------
    if ($duty_yyyy == "") { $duty_yyyy = $cause_duty_yyyy; } //当年以外指定時の不具合対応 20131227
	if ($duty_mm == "") { $duty_mm = $next_mm; }
	$duty_yyyymm = sprintf("%04d%02d", $duty_yyyy, $duty_mm);
	///-----------------------------------------------------------------------------
	//日数
	///-----------------------------------------------------------------------------
//	$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
//	$start_day = 1;
//	$end_day = $day_cnt;
	///-----------------------------------------------------------------------------
	//各変数値の初期値設定
	///-----------------------------------------------------------------------------
	$individual_flg = "";
	$hope_get_flg = "";
	$plan_results_flg == 2 ? $show_data_flg = 1 : "";//２行目表示データフラグ

	if ($data_cnt == "") { $data_cnt = 0; }
	//入力形式で表示する日（開始／終了）
	$edit_start_day = 0;
	$edit_end_day = 0;

	$group_id = $cause_group_id;
	///-----------------------------------------------------------------------------
	// カレンダー(calendar)情報を取得
	///-----------------------------------------------------------------------------
//	$start_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, "01");
//	$end_date = sprintf("%04d%02d%02d",$duty_yyyy, $duty_mm, $day_cnt);
	$start_date = $duty_start_date;
	$end_date = $duty_end_date;
	$calendar_array = $obj->get_calendar_array($start_date, $end_date);
	//日数
	$day_cnt = count($calendar_array);
///-----------------------------------------------------------------------------
	//指定月の全曜日を設定
	///-----------------------------------------------------------------------------
	$week_array = array();
	for ($k=1; $k<=$day_cnt; $k++) {
		$wk_date = $calendar_array[$k-1]["date"];
		$wk_yyyy = substr($wk_date, 0, 4);
		$wk_mm = substr($wk_date, 4, 2);
		$wk_dd = substr($wk_date, 6, 2);
		$tmp_date = mktime(0, 0, 0, $wk_mm, $wk_dd, $wk_yyyy);
		$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	}
	///-----------------------------------------------------------------------------
	// ログインユーザの職員ID・氏名を取得
	///-----------------------------------------------------------------------------
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
	///-----------------------------------------------------------------------------
	//ＤＢ(stmst)より役職情報取得
	//ＤＢ(jobmst)より職種情報取得
	//ＤＢ(empmst)より職員情報を取得
	//ＤＢ(wktmgrp)より勤務パターン情報を取得
	///-----------------------------------------------------------------------------
	$data_st = $obj->get_stmst_array();
	$data_job = $obj->get_jobmst_array();
//	$data_emp = $obj->get_empmst_array("");
// 有効なグループの職員を取得
//	$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm);
// シフトグループに所属する職員を取得 20140210
	$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
	$data_wktmgrp = $obj->get_wktmgrp_array();
	///-----------------------------------------------------------------------------
	// 指定グループの病棟名を取得
	///-----------------------------------------------------------------------------
	$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
	if (count($wk_group) > 0) {
		$group_name = $wk_group[0]["group_name"];
		$pattern_id = $wk_group[0]["pattern_id"];
		$print_title = $wk_group[0]["print_title"];
	}
	///-----------------------------------------------------------------------------
	//ＤＢ(atdptn)より出勤パターン情報取得
	//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
	///-----------------------------------------------------------------------------
	$data_atdptn = $obj->get_atdptn_array($pattern_id);
    //20140220 start
    $data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
    //応援追加されている出勤パターングループ確認 
    $arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
    $pattern_id_cnt = count($arr_pattern_id);
    if ($pattern_id_cnt > 0) {
        $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
        $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
        $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
        $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
    }
    //応援なしの場合、同じパターングループのデータを設定
    else {
        $data_atdptn_all = $data_atdptn;
        $data_pattern_all = $data_pattern;
    }
    //20140220 end
    ///-----------------------------------------------------------------------------
	// 勤務シフト情報を取得
	// 再表示時、データ再設定
	///-----------------------------------------------------------------------------
	$plan_individual_array = array();
	$set_data = array();
	for($i=0;$i<$data_cnt;$i++) {
		//応援追加情報
		$assist_str = "assist_group_$i";
		$arr_assist_group = split(",", $$assist_str);
		//希望分
		if ($show_data_flg == "2") {
			$rslt_assist_str = "rslt_assist_group_$i";
			$arr_rslt_assist_group = split(",", $$rslt_assist_str);
		}
		///-----------------------------------------------------------------------------
		// 再表示時、データ再設定
		///-----------------------------------------------------------------------------
		for($k=1;$k<=$day_cnt;$k++) {
			$set_data[$i]["plan_rslt_flg"] = "1";		//１：実績は再取得

			$wk2 = "pattern_id_$i" . "_" . $k;
			$wk3 = "atdptn_ptn_id_$i" . "_" . $k;
			$wk6 = "reason_2_$i" . "_" . $k;

			$set_data[$i]["staff_id"] = $staff_id[$i];					//職員ＩＤ
			$set_data[$i]["pattern_id_$k"] = $$wk2;						//出勤グループＩＤ

			if ((int)substr($$wk3,0,2) > 0) {
				$set_data[$i]["atdptn_ptn_id_$k"] = (int)substr($$wk3,0,2);	//出勤パターンＩＤ
			}
			if ((int)substr($$wk3,2,2) > 0) {
				$set_data[$i]["reason_$k"] = (int)substr($$wk3,2,2);	//事由
			}

			$set_data[$i]["reason_2_$k"] = $$wk6;						//事由（午前有給／午後有給）
			//応援情報
			$set_data[$i]["assist_group_$k"] = $arr_assist_group[$k - 1];
			if ($show_data_flg == "2") {
				$set_data[$i]["rslt_assist_group_$k"] = $arr_rslt_assist_group[$k - 1];
			}

		}
		///-----------------------------------------------------------------------------
		//実績の追加
		///-----------------------------------------------------------------------------
//		//出勤グループＩＤ
//		$wk_array = split(",", $rslt_pattern_id_all[$i]);
//		$k = 1;
//		foreach ($wk_array as $wk) {
//			$set_data[$i]["rslt_pattern_id_$k"] = $wk;
//			$k++;
//		}
//		//出勤パターンＩＤ
//		$wk_array = split(",", $rslt_id_all[$i]);
//		$k = 1;
//		foreach ($wk_array as $wk) {
//			$set_data[$i]["rslt_id_$k"] = $wk;
//			$k++;
//		}
	}

	$plan_individual_array = $obj->get_duty_shift_plan_array(
									$group_id, $pattern_id,
									$duty_yyyy, $duty_mm, $day_cnt,
									$individual_flg,
									$hope_get_flg,
									$show_data_flg,
									$set_data, $week_array, $calendar_array,
									$data_atdptn, $data_pattern_all,
									$data_st, $data_job, $data_emp, $emp_id);
	$data_cnt = count($plan_individual_array);
/*
	for($i=0;$i<$data_cnt;$i++) {
		//応援追加情報
		$assist_str = "assist_group_$i";
		$arr_assist_group = split(",", $$assist_str);
		for($k=1;$k<=$day_cnt;$k++) {
			//応援情報
			$set_data[$i]["assist_group_$k"] = $arr_assist_group[$k - 1];
		}
	}
*/
	///-----------------------------------------------------------------------------
	// 行タイトル（行ごとの集計する勤務パターン）を取得
	// 列タイトル（列ごとの集計する勤務パターン）を取得
	///-----------------------------------------------------------------------------
	$title_gyo_array = $obj->get_total_title_array($pattern_id,	"1", $data_pattern);
	$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
	if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
		$err_msg_2 = "勤務シフトグループ情報が未設定です。管理者に連絡してください。";
	}
	///-----------------------------------------------------------------------------
	//個人日数合計（行）を算出設定
	//個人日数合計（列）を算出設定
	///-----------------------------------------------------------------------------
	$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
							"1", "", $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
	$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
							"2", "", $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

	$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
//print_r($title_hol_array);
//休暇事由情報
	$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);
//print_r($hol_cnt_array);

//echo("test $group_id, $pattern_id, 1, $wk_rs_flg, $day_cnt, $plan_individual_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn");
	///-----------------------------------------------------------------------------
	//最大文字数算出
	///-----------------------------------------------------------------------------
	$max_font_cnt = 1;
	for ($i=0; $i<$data_cnt; $i++) {
	for ($k=1; $k<=$day_cnt; $k++) {
		//予定
		if ($plan_individual_array[$i]["font_name_$k"] != "") {
		if ($max_font_cnt < strlen($plan_individual_array[$i]["font_name_$k"])) {
			if (strlen($plan_individual_array[$i]["font_name_$k"]) <= 2) {
				$max_font_cnt = 1;
			} else 	if (strlen($plan_individual_array[$i]["font_name_$k"]) <= 4) {
				$max_font_cnt = 2;
			}
		}
		}
		//実績
		if ($plan_individual_array[$i]["rslt_name_$k"] != "") {
		if ($max_font_cnt < strlen($plan_individual_array[$i]["rslt_name_$k"])) {
			if (strlen($plan_individual_array[$i]["rslt_name_$k"]) <= 2) {
				$max_font_cnt = 1;
			} else 	if (strlen($plan_individual_array[$i]["rslt_name_$k"]) <= 4) {
				$max_font_cnt = 2;
			}
		}
		}
	}
	}

//決裁欄役職名
	$st_name_array = $obj->get_duty_shift_group_st_name_array($group_id);

// 決裁欄印字有無を取得する 20140107
$approval_column_print_flg = $wk_group[0]["approval_column_print_flg"];

if($pdf_print_flg != 1){
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 勤務シフト希望／参照「印刷」</title>
<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="self.print();self.close();">

<? //print_r($set_data) ?>

	<?
	if ($err_msg_2 != "") {
		echo($err_msg_2);
		echo("<br>\n");
	} else {
	?>

	<!-- ------------------------------------------------------------------------ -->
	<!-- タイトル -->
	<!-- ------------------------------------------------------------------------ -->
	<?
	$wk_cnt = $data_cnt;
	if ($data_cnt <= 0) {
		$wk_cnt = 1;
	}
//	for ($i=0; $i<$wk_cnt; $i = $i + (10 / $max_font_cnt)) {
	for ($i=0; $i<$wk_cnt; $i = $i + $wk_cnt) {
	?>

		<br>

		<center>
		<table>
		<tr>
			<!-- ------------------------------------------------------------------------ -->
			<!-- 病棟 -->
			<!-- ------------------------------------------------------------------------ -->
			<td width="300" align="center">
			<table align="center">
			<tr height="40">
				<td width="150"><u><? echo($group_name) ?></u></td>
				<td align="center" width="120"><? echo($duty_yyyy) ?>年　<? echo($duty_mm) ?>月</td>
			</tr>
			</table>
			</td>
			<!-- ------------------------------------------------------------------------ -->
			<!-- 題名 -->
			<!-- ------------------------------------------------------------------------ -->
			<td width="300">
			<table align="center">
			<tr height="40" align="center">
			<? if ($refer_flg == "1") { ?>
				<td width="240"><font size="5"><?echo(h($print_title))?></font></td>
			<? } else { ?>
				<td width="300"><font size="5"><?echo(h($print_title))?>（希望）</font></td>
			<? } ?>
			</tr>
			</table>
			</td>
			<!-- ------------------------------------------------------------------------ -->
			<!-- 決裁 -->
			<!-- ------------------------------------------------------------------------ -->
			<td width="300">
            <?
            if($approval_column_print_flg == 't') {	// 決裁欄印字有無（デッドコードだが念のため）20140107
                echo("<table border=\"1\" align=\"right\">");
                echo("<tr align=\"center\">");
                echo("<td width=\"10\" heigth=\"50\" rowspan=\"2\"><font size=\"1\">決裁</font></td>");
                
                //決裁欄役職名
                for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
                    echo("<td width=\"60\" height=\"10\"><font size=\"1\">".$st_name_array[$st_idx]["st_name"]."</font></td>\n");
                }
                
                echo("</tr>");
                echo("<tr>");
                
                //決裁欄
                for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
                    echo("<td width=\"60\" height=\"50\">　</td>\n");
                }
                
                echo("</tr>");
                echo("</table>");
            }
?>
			</td>
		</tr>
		</table>
		</center>
		<!-- ------------------------------------------------------------------------ -->
		<!-- 勤務表 -->
		<!-- ------------------------------------------------------------------------ -->
		<form name="dutyshift" method="post">
			<!-- ------------------------------------------------------------------------ -->
			<!-- 見出し -->
			<!-- ------------------------------------------------------------------------ -->
			<table width="250" id="header" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
			<?
				//日・曜日
				echo($obj_print->showTitleWeek($duty_yyyy, $duty_mm, $day_cnt,
										$plan_individual_array, $week_array, $title_gyo_array, $calendar_array, $total_print_flg));
			?>
			</table>
			<!-- ------------------------------------------------------------------------ -->
			<!-- 勤務シフトデータ（職員（列）年月日（行） -->
			<!-- ------------------------------------------------------------------------ -->
			<table width="250" id="data" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
			<?
				//表示行計算
//				if ($data_cnt > ($i + (10 / $max_font_cnt))) {
//					$k = $i + (10 / $max_font_cnt);
//				} else {
					$k = $data_cnt;
//				}
				echo($obj_print->showList($pattern_id, $plan_individual_array, $title_gyo_array, $gyo_array, $i, $k, $day_cnt, $max_font_cnt, $total_print_flg, $show_data_flg, $group_id, $title_hol_array, $hol_cnt_array));
			?>
			</table>
		</form>

	<?
/*
		if ($data_cnt > ($i + (10 / $max_font_cnt))) {
			if ($max_font_cnt == 1) { $t = 7; }
			if ($max_font_cnt == 2) { $t = 6; }
			for ($p=0; $p<$t; $p++) {
				echo("<br>");
			}
		}
*/
	}
	?>

	<?
		//集計行列の印刷をする場合
		if ($total_print_flg == "1") {
	?>
	<form name="dutyshift2" method="post">
	<!-- ------------------------------------------------------------------------ -->
	<!-- 列計 -->
	<!-- ------------------------------------------------------------------------ -->
	<table width="250" id="summary" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" class="list">
	<?
		echo($obj_print->showTitleRetu($day_cnt, $plan_individual_array, $title_gyo_array, $gyo_array, $title_retu_array, $retu_array, "1", $title_hol_array, $hol_cnt_array));
	?>
	</table>
	</form>

	<?
		}
	}
	?>

</body>

<? pg_close($con); ?>

</html>

<?
//----------------------------------------
// PDF印刷
//----------------------------------------
}
else if($pdf_print_flg == 1){

  require_once('fpdf153/mbfpdf.php');
    // PDF作成については以下を参照しました。
    // http://www.phpbook.jp/fpdf/index.html

    class CustomMBFPDF extends MBFPDF {

        //｢EUC-JP → SJIS｣変換のためのオーバーライド
        //MultiCell()もWrite()もこのCell()で表示しているので、オーバーライドはCell()だけでOK
        function Cell($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = 0, $link = '')
        { 
          $enc = mb_internal_encoding();
          if($enc != "sjis-win"){
            $txt = mb_convert_encoding($txt, "sjis-win", $enc) ;
            parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link) ;
          }
        }
        //ｂｇカラーを10進数に変換して表示
        function bg_color_decimal_conversion($color_code){
            if($color_code != ""){
                $R = hexdec(substr($color_code, 1 ,2));
                $G = hexdec(substr($color_code, 3 ,2));
                $B = hexdec(substr($color_code, 5 ,2));
                $this->SetFillColor($R, $G, $B);
            } else{
                $this->SetFillColor(255, 255, 255);
            }
    	}
        //textカラーを10進数に変換して表示
        function font_color_decimal_conversion($color_code){
            if($color_code != ""){
                $R = hexdec(substr($color_code, 1 ,2));
                $G = hexdec(substr($color_code, 3 ,2));
                $B = hexdec(substr($color_code, 5 ,2));
                $this->SetTextColor($R, $G, $B);
            } else{
                $this->SetTextColor(0, 0, 0);
            }
    	}
    }
    //マージン
    $MOST_RIGHT = 5.0 ;
    $MOST_LEFT  = 4.5 ;
    $MOST_TOP   = 10.0 ;
    
    $total_print_flg == "1" ? $p_size = 'A3' : $p_size = 'A4';
	
    $pdf=new CustomMBFPDF('L', 'mm', $p_size);
    $pdf->AddMBFont(GOTHIC, 'SJIS') ;
    $pdf->SetMargins($MOST_LEFT, $MOST_TOP,$MOST_RIGHT) ;
    $pdf->SetAutoPageBreak(true, 5) ;
    $pdf->Open() ;
    $pdf->AddPage() ;
	
	
	
    $pdf->SetFont(GOTHIC,'',12);
    if ($err_msg_2 != "") {
	  $pdf->Cell(30, 5.0, $err_msg_2,'',1,'L');
    } else {
      $pdf->SetFont(GOTHIC,'',9);
	  $wk_cnt = $data_cnt;
      if ($data_cnt <= 0) {
        $wk_cnt = 1;
      }
    
      $X = $pdf->GetX();
      $Y = $pdf->GetY();
    	
      $show_data_flg != "" ? $hi = 10 : $hi = 5;
    
    	// 呼び出し元ファイル名を保持
		//$this->file_name = $fname;
		// DBコネクションを保持
		//$this->_db_con = $con;
		// クラス new
		//$this->obj = new duty_shift_common_class($con, $fname);
		$obj_duty = new duty_shift_duty_common_class($con, $fname);
    	
        //病棟
        $pdf->SetX($X + 5);
        $pdf->SetFont(GOTHIC,'U',12);
        $pdf->Cell(80, 10, $group_name, 0, 0, 'L');
        $pdf->SetFont(GOTHIC,'',12);
        //日付
        $pdf->Cell(25, 10, $duty_yyyy.'年　'.$duty_mm.'月', 0, 0, 'L');
        //題名
        $pdf->SetFont(GOTHIC,'',20);
        $pdf->Cell(100, 10, '　'.h($print_title), 0, 0, 'L');
        $pdf->SetFont(GOTHIC,'',10);
        //決裁
        if($approval_column_print_flg == 't') {	// 決裁欄印字有無 20140107 START
            $this_x = $pdf->GetX() + 5;
      	    $pdf->SetXY($pdf->GetX(), $Y - 5);
            $pdf->MultiCell(5, 5, '　決裁', 1, 'L');
            $pdf->SetXY($this_x, $Y - 5);
            for($st_idx=0; $st_idx < count($st_name_array); $st_idx++){
                $pdf->SetXY($this_x, $Y - 5);
                $pdf->Cell(15, 5.0, $st_name_array[$st_idx]["st_name"], 1, 2, 'C');
                $this_x = $pdf->GetX() + 15;
                $pdf->Cell(15, 15, '', 1, 1, 'C');
            }
        } else {
			$pdf->SetY($Y + 12);
		}
		// 決裁欄印字有無 20140107 END

      	$pdf->Cell(0, 3, '', 0, 1, 'C');
  
      	//-------------------------------------------------------------------------------
		//日段
		//-------------------------------------------------------------------------------
        $pdf->SetFont(GOTHIC,'',8);
		$pdf->Cell(6.8, 10, '番号', 1, 0, 'C');
		//チーム表示
        if ($wk_group[0]["team_disp_flg"] == "t") {
            $pdf->Cell(10, 10,'チーム', 1, 0, 'C');
        }
		$pdf->Cell(20, 10,'氏名', 1, 0, 'C');
		//職種表示
        if ($wk_group[0]["job_disp_flg"] == "t") {
            $pdf->Cell(15, 10,'職種', 1, 0, 'C');
        }
		//役職表示
        if ($wk_group[0]["st_disp_flg"] == "t") {
            $pdf->Cell(15, 10,'役職', 1, 0, 'C');
        }
		$pdf->Cell(10, 10,'', 1, 0, 'C');
        $d_x = $pdf->GetX();
		//勤務状況（日）
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk_date = $calendar_array[$k-1]["date"];
			$wk_dd = (int)substr($wk_date,6,2);
			//$wk_width = 20;
			$wk_width = $obj_print->default_width;
			$pdf->Cell(6.8, 5, $wk_dd, 'LTR', 0, 'C');
		}
		//集計行列の印刷をする場合
		if ($total_print_flg == "1") {
			//個人日数
			for ($k=0; $k<count($title_gyo_array); $k++) {
				$wk = $title_gyo_array[$k]["name"];
				if ($title_gyo_array[$k]["id"] == "10") {
					//公休、有給表示
					$pdf->Cell(6.8, 10,'公休', 1, 0, 'C');
					$this_x = $pdf->GetX();
					$this_y = $pdf->GetY(); 
					$pdf->MultiCell(8, 5,"年休\n有休", 0, 'L');
					$pdf->SetXY($this_x, $this_y);
					$pdf->Cell(6.8, 10,"", 1, 0, 'C');
				}
				if(mb_strlen(trim($wk), "EUC-JP") > 2){
					$this_x = $pdf->GetX();
					$this_y = $pdf->GetY();
					$pdf->MultiCell(8, 5,trim(mb_wordwrap(trim($wk), 2, "\n")), 0, 'L');
					$pdf->SetXY($this_x, $this_y);
					$pdf->Cell(6.8, 10,"", 1, 0, 'C');
				}else{
				    $pdf->Cell(6.8, 10,trim($wk), 1, 0, 'C');
				}
			}
		}
        $d_y = $pdf->GetY() + 5;
		//-------------------------------------------------------------------------------
		//曜日段
		//------------------------------------------------------------------------------
		//勤務状況（曜日）[
      	$pdf->SetXY($d_x, $d_y);
		for ($k=1; $k<=$day_cnt; $k++) {
			//土、日、祝日の文字色変更
			$wk_color = "";
			if ($week_array[$k]["name"] == "土"){
				$wk_color = "#0000ff";
			} else if ($week_array[$k]["name"] == "日") {
				$wk_color = "#ff0000";
			} else {
				if (($calendar_array[$k-1]["type"] == "4") ||
					($calendar_array[$k-1]["type"] == "5") ||
					($calendar_array[$k-1]["type"] == "6")) {
					$wk_color = "#ff0000";
				}
			}
			$pdf->font_color_decimal_conversion($wk_color);
			//表示
			$wk_name = $week_array[$k]["name"];
			$pdf->Cell(6.8, 5,$wk_name, 'LRB', 0, 'C');
			$pdf->font_color_decimal_conversion('#000000');
		}

    	// 当日
		$today = Date("Ymd");
		$wk_height = 22 * (int)$max_font_cnt;
		$reason_2_array = $obj->get_reason_2();

		//当直パターン取得
		$duty_pattern =	$obj_duty->get_duty_pattern_array();

		//*******************************************************************************
		// 予定データ
		//******************************************************************************* 
        $pdf->SetFont(GOTHIC,'',7.5);
		for ($i=0; $i<$wk_cnt; $i++){
            $pdf->SetXY($X, $pdf->GetY() + 5);

			//-------------------------------------------------------------------------------
			// 表示順
			//-------------------------------------------------------------------------------
			$wk = $plan_individual_array[$i]["no"];
			$pdf->Cell(6.8, $hi, trim($wk), 1, 0, 'C');
			//-------------------------------------------------------------------------------
			// チーム
			//-------------------------------------------------------------------------------
            if ($wk_group[0]["team_disp_flg"] == "t") {
                if ($plan_individual_array[$i]["other_staff_flg"] == "1") {
                    $wk_bgcolor = "#dce8bb"; //他病棟スタッフ
                    $wk_fill = 1;
                } else {
                    $wk_fill = 0;
                }
                $pdf->bg_color_decimal_conversion($wk_bgcolor);
                $wk = $plan_individual_array[$i]["team_name"];
                $pdf->Cell(10, $hi, trim($wk), 1, 0, 'C', $wk_fill);
            }
			//-------------------------------------------------------------------------------
			// 氏名
			//-------------------------------------------------------------------------------
			$wk = $plan_individual_array[$i]["staff_name"];
			$wk_color = ( $plan_individual_array[$i]["sex"] == 1 ) ? '#0000ff' : '#000000';
			$pdf->font_color_decimal_conversion($wk_color);
			$pdf->Cell(20, $hi, trim($wk), 1, 0, 'L', $wk_fill);
			$pdf->font_color_decimal_conversion('#000000');
			//-------------------------------------------------------------------------------
			// 職種
			//-------------------------------------------------------------------------------
            if ($wk_group[0]["job_disp_flg"] == "t") {
                $wk = $plan_individual_array[$i]["job_name"];
                $pdf->Cell(15, $hi, trim($wk), 1, 0, 'C');
            }
			//-------------------------------------------------------------------------------
			// 役職
			//-------------------------------------------------------------------------------
            if ($wk_group[0]["st_disp_flg"] == "t") {
                $wk = $plan_individual_array[$i]["status"];
                $pdf->Cell(15, $hi, trim($wk), 1, 0, 'C');
            }
			if($show_data_flg != "" ) $show_WX = $pdf->GetX();
			$pdf->Cell(10, 5, '予定', 1, 0, 'C');
			if($show_data_flg != "" ) $show_WY = $pdf->GetY();

			//-------------------------------------------------------------------------------
			// 勤務状況
			//-------------------------------------------------------------------------------
			for ($k=1; $k<=$day_cnt; $k++) {
				//勤務シフト予定（表示）
				$wk_font_color = "";
				$wk_back_color = "";
				if ($plan_individual_array[$i]["pattern_id_$k"] == $pattern_id &&
                ($plan_individual_array[$i]["assist_group_$k"] == "" ||
                $plan_individual_array[$i]["assist_group_$k"] == $group_id)) {
                    $wk_font_color = $plan_individual_array[$i]["font_color_$k"];
                	$wk_back_color = $plan_individual_array[$i]["back_color_$k"]; 
				} else {
					$wk_font_color = "#C0C0C0";//灰色
				}
				$pdf->font_color_decimal_conversion($wk_font_color); 
				$pdf->bg_color_decimal_conversion($wk_back_color); 

				//名称
				$wk = str_replace("&nbsp;","",$plan_individual_array[$i]["font_name_$k"]);
				if(mb_strlen(trim($wk), "EUC-JP") > 2){
				    $pdf->SetFont(GOTHIC,'',5.5);
				    $this_x = $pdf->GetX();
					$this_y = $pdf->GetY();
					$pdf->Cell(6.8, 5,"", 1, 0, 'C', 1);
					$pdf->SetXY($this_x, $this_y);
					$pdf->MultiCell(9, 2.5,trim($wk), 0, 'L');
					$pdf->SetXY($this_x + 6.8, $this_y);
				    $pdf->SetFont(GOTHIC,'',7.5);
				}else{
				$pdf->Cell(6.8, 5, $wk, 1, 0, 'C', 1);
				}
			}
            $pdf->font_color_decimal_conversion(); 
			//集計行列の印刷をする場合
			if ($total_print_flg == "1") {
				if ($show_data_flg == "3") {
					$span = "2";
				} else {
					$span = "1";
				}
				//公休・年休表示
				$wk_koukyu_cnt = 0;
				$wk_nenkyu_cnt = 0;
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休表示
					if ($title_hol_array[$k]["reason"] == "1") {
						$wk_nenkyu_cnt = $hol_cnt_array[$i][$k];
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						$wk_koukyu_cnt = $hol_cnt_array[$i][$k];
					}
				}
				//-------------------------------------------------------------------------------
				//個人日数
				//-------------------------------------------------------------------------------
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = $gyo_array[$i][$k];
					if ($title_gyo_array[$k]["id"] == "10") {
						//公休、有休表示
						$pdf->Cell(6.8, 5, $wk_koukyu_cnt, 1, 0, 'C');
						$pdf->Cell(6.8, 5, $wk_nenkyu_cnt, 1, 0, 'C');
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					$pdf->Cell(6.8, 5, trim($wk), 1, 0, 'C');
				}
			}
			
			//*******************************************************************************
			// 実績データ
			//*******************************************************************************
			if ($show_data_flg != "") {
				$pdf->font_color_decimal_conversion();
				switch ($show_data_flg) {
				case "1":
					$tmp_title = "実績";
					break;
				case "2":
					$tmp_title = "希望";
					break;
				case "3":
					$tmp_title = "当直";
					break;
				case "4":
					$tmp_title = "ｺﾒﾝﾄ";
					break;
				default:
					$tmp_title = "";
				}
				$pdf->SetXY($show_WX, $show_WY + 5);
				$pdf->Cell(10, 5, $tmp_title, 1, 0, 'C');
				//勤務状況（実績）
				for ($k=1; $k<=$day_cnt; $k++) {

					$wk = ""; // "_" -> " "
					$wk_font_color = "";
					$wk_back_color = "";
					
					if($show_data_flg == "4"){
                        $wk = h($plan_individual_array[$i]["comment_$k"]); //コメント
					} else if ($show_data_flg != "3") {
						//文字、背景色
						if (/*$plan_individual_array[$i]["rslt_pattern_id_$k"] == $pattern_id &&*/
							 ($plan_individual_array[$i]["assist_group_$k"] == "" ||
							  $plan_individual_array[$i]["assist_group_$k"] == $group_id)) {
							$wk_font_color = $plan_individual_array[$i]["rslt_font_color_$k"];
							$wk_back_color = $plan_individual_array[$i]["rslt_back_color_$k"];
						} else {
							$wk_font_color = "#C0C0C0";    //灰色
						}

						//勤務実績
						$wk = str_replace("&nbsp;","",$plan_individual_array[$i]["rslt_name_$k"]);
						//退勤がない場合の表示20100820
						if (($plan_individual_array[$i]["rslt_start_time_$k"] != "" &&
							$plan_individual_array[$i]["rslt_end_time_$k"] == "") ||
							($plan_individual_array[$i]["rslt_start_time_$k"] == "" && //出勤がない場合 20110124
							$plan_individual_array[$i]["rslt_end_time_$k"] != "")) {
							$wk .= "・";
						}
					} else {
						for ($m=0; $m<count($duty_pattern); $m++) {
							if ($plan_individual_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
								$wk = $duty_pattern[$m]["font_name"];	//表示文字名
								$wk_font_color = $duty_pattern[$m]["font_color"];
								$wk_back_color = $duty_pattern[$m]["back_color"];
							}
						}
					}
					$pdf->font_color_decimal_conversion($wk_font_color); 
					$pdf->bg_color_decimal_conversion($wk_back_color); 
					
                    if(mb_strlen(trim($wk), "EUC-JP") > 2){
				    $pdf->SetFont(GOTHIC,'',5.5);
				    $this_x = $pdf->GetX();
					$this_y = $pdf->GetY();
					$pdf->Cell(6.8, 5,"", 1, 0, 'C', 1);
					$pdf->SetXY($this_x, $this_y);
					$pdf->MultiCell(9, 2.5,trim($wk), 0, 'L');
					$pdf->SetXY($this_x + 6.8, $this_y);
				    $pdf->SetFont(GOTHIC,'',7.5);
    				}else{
    					$pdf->Cell(6.8, 5, $wk, 1, 0, 'C', 1);
    				}
				}
				//２段目の集計
				$pdf->font_color_decimal_conversion(); 
				if ($total_print_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
					$gyo_array_cnt = count($title_gyo_array);
					$hol_array_cnt = count($title_hol_array);

					//公休・年休表示
					$wk_koukyu_cnt = 0;
					$wk_nenkyu_cnt = 0;
					for ($k=0; $k<count($title_hol_array); $k++) {
						//公休・年休表示
						if ($title_hol_array[$k]["reason"] == "1") {
							$wk_nenkyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
						}
						if ($title_hol_array[$k]["reason"] == "24") {
							$wk_koukyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
						}
					}
					for ($k=0; $k<count($title_gyo_array); $k++) {
						$wk = $gyo_array[$i][$gyo_array_cnt + $k];
						if ($title_gyo_array[$k]["id"] == "10") {
							//公休、年休表示
							$pdf->Cell(6.8, 5, $wk_koukyu_cnt, 1, 0, 'C');
							$pdf->Cell(6.8, 5, $wk_nenkyu_cnt, 1, 0, 'C');
							$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
						}
						$pdf->Cell(6.8, 5, trim($wk), 1, 0, 'C');
					}
				}
			}
		}
    
    //-------------------------------------------
    //下の集計表
    //-------------------------------------------
    $pdf->font_color_decimal_conversion(); 
	if ($total_print_flg == "1") {
	    $reason_2_array = $obj->get_reason_2();

		for ($i=0; $i<count($title_retu_array); $i++) {
			$pdf->SetXY($X, $pdf->GetY() + 5);
		
			$pdf->Cell(6.8, 5, '', 1, 0, 'C');
		        //チーム表示
                if ($wk_group[0]["team_disp_flg"] == "t") {
                    $pdf->Cell(10, 5,'', 1, 0, 'C');
                }
			//-------------------------------------------------------------------------------
			// 名称
			//-------------------------------------------------------------------------------
			$wk = $title_retu_array[$i]["name"];
			$pdf->Cell(20, 5, trim($wk), 1, 0, 'C');
			//-------------------------------------------------------------------------------
			// 総合計
			//-------------------------------------------------------------------------------
			$wk_cnt = 0;
			for ($k=1; $k<=$day_cnt; $k++) {
				$wk_cnt += $retu_array[$k][$i];
			}
		        //職種表示
                if ($wk_group[0]["job_disp_flg"] == "t") {
                    $pdf->Cell(15, 5,'', 1, 0, 'C');
                }
                //役職表示
                if ($wk_group[0]["st_disp_flg"] == "t") {
                    $pdf->Cell(15, 5,'', 1, 0, 'C');
                }
                $pdf->Cell(10, 5,$wk_cnt, 1, 0, 'R');
			//-------------------------------------------------------------------------------
			// 日ごとの合計
			//-------------------------------------------------------------------------------
			for ($k=1; $k<=$day_cnt; $k++) {
				$wk = $retu_array[$k][$i];
				$wk_width = $obj_print->default_width;
				for ($m=0; $m<count($reason_2_array); $m++) {
					if ($plan_individual_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
						$wk_width = 30;
						break;
					}
					if ($plan_individual_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
						$wk_width = 30;
						break;
					}
				}
				$pdf->Cell(6.8, 5, trim($wk), 1, 0, 'C');
			}
			//-------------------------------------------------------------------------------
			// 個人日数
			//-------------------------------------------------------------------------------
			if (($total_gyo_flg != "2" && $i == 0) || ($total_gyo_flg == "2" && $i <= 1)) {
				$wk_nenkyu_cnt = 0;
				$wk_koukyu_cnt = 0;
				//集計
				for ($k=0; $k<count($title_hol_array); $k++) {
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_hol_array) + $k;
					}

					if ($title_hol_array[$k]["reason"] == "1") {
						for ($m=0; $m<count($plan_individual_array); $m++) {
							$wk_nenkyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						for ($m=0; $m<count($plan_individual_array); $m++) {
							$wk_koukyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
				}
				// 個人日数の列合計
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = 0;
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_gyo_array) + $k;
					}
					for ($m=0; $m<count($gyo_array); $m++) {
						$wk = $wk + $gyo_array[$m][$k2];
					}
					//公休、有休表示
					if ($title_gyo_array[$k]["id"] == "10") {
						$pdf->Cell(6.8, 5, trim($wk_koukyu_cnt), 1, 0, 'C');
						$pdf->Cell(6.8, 5, trim($wk_nenkyu_cnt), 1, 0, 'C');
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					$pdf->Cell(6.8, 5, trim($wk), 1, 0, 'C');
				}
			} else {
				// ブランク（個人日数用）
				for ($k=0; $k<count($title_gyo_array); $k++) {
				}
			}
		}
	  }
    }

    $pdf->Output();
    die;
}
	
function mb_wordwrap($string, $width=75, $break="\n", $cut = false) {
    if (!$cut) {
        $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.',}\b#U';
    } else {
        $regexp = '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){'.$width.'}#';
    }
    $string_length = mb_strlen($string,'UTF-8');
    $cut_length = ceil($string_length / $width);
    $i = 1;
    $return = '';
    while ($i < $cut_length) {
        preg_match($regexp, $string,$matches);
        $new_string = $matches[0];
        $return .= $new_string.$break;
        $string = substr($string, strlen($new_string));
        $i++;
    }
    return $return.$string;
}

?>

