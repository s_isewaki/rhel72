<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");
require_once("show_class_name.ini");

define("WBR", "&#8203;"); // HTML強制改行文字

// ページ名func
$fname = $PHP_SELF;
// セッションのチェック
$session = qualify_session($session,$fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 勤務管理権限チェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// リンク出力用変数の設定
if ($page == "") {
	$page = 0;
}
$limit = 10000;
$url_srch_name = urlencode($srch_name);

// DBコネクション作成
$con = connect2db($fname);

// 遷移元の設定
if ($work != "") {
	set_referer($con, $session, "work", $work, $fname);
	$referer = $work;
} else {
	$referer = get_referer($con, $session, "work", $fname);
}

if ($btn_event == "add_emp") {
	add_emp($con, $fname);
	$sort = 6;
}
if ($btn_event == "del_emp") {
	del_emp($con, $fname);
}
if ($btn_event == "show_tantou_branch") {
	show_tantou_branches($con, $fname);
	die;
}
if ($btn_event == "add_tantou_branch") {
	add_tantou_branches($con, $fname, $_REQUEST["target_emp_id"], $_REQUEST["cls"], $_REQUEST["atrb"], $_REQUEST["dept"], $_REQUEST["room"]);
?>
<script type="text/javascript">
window.opener.document.mainform.submit(); window.close();
</script>
<?
	die;
}
if ($btn_event == "del_employee_branch") {
    list ($class_id, $atrb_id, $dept_id, $room_id) = explode(",", $_REQUEST["target_branch_id"]);
	del_tantou_branches($con, $fname, $_REQUEST["target_emp_id"], $class_id, $atrb_id, $dept_id, $room_id);
}

// 該当する職員を検索
$sel_emp = false;
//検索条件SQLを取得
$cond = get_cond_tantou($con, $fname, $srch_name, $srch_id);
$sql = "select empmst.emp_id, empmst.emp_personal_id, empmst.emp_lt_nm, empmst.emp_ft_nm, empmst.emp_class, empmst.emp_attribute, empmst.emp_dept, tb.reg_date from timecard_tantou_branches tb ";
$sql .= "left join empmst on empmst.emp_id = tb.emp_id ";
$offset = $page * $limit;

//ソート順
if ($sort == "") {
	$sort = "3";
}
switch ($sort) {
	case "1":
		$orderby = "empmst.emp_personal_id ";
		break;
	case "2":
		$orderby = "empmst.emp_personal_id desc ";
		break;
	case "3":
		$orderby = "empmst.emp_kn_lt_nm, empmst.emp_kn_ft_nm, empmst.emp_personal_id ";
		break;
	case "4":
		$orderby = "empmst.emp_kn_lt_nm desc, empmst.emp_kn_ft_nm desc, empmst.emp_personal_id ";
		break;
	case "5":
		$orderby = "tb.reg_date ";
		break;
	case "6":
		$orderby = "tb.reg_date desc ";
		break;
}
$cond .= " order by $orderby offset $offset limit $limit";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_cnt = pg_num_rows($sel_emp);

$branches_list = get_emp_tantou_branches_list($con, $fname);

?>
<title>CoMedix 勤務管理｜担当者設定</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
var session = '<? echo($session); ?>';
<? // ?>
var dlgResizableOption = ", location=no, directories=no, menubar=no, status=no, alwaysRaised=yes, dependent=no, resizable=yes, scrollbars=yes";
var windowStocker = {};
function window_open(url, wname, width, height) {
    var x = (screen.width - width) / 2;
    var y = (screen.height - height) / 2;
    opt = "left=" + x + ",top=" + y + ",width=" + width + ",height=" + height + dlgResizableOption;
    if (windowStocker[wname]) {
        try { windowStocker[wname].close(); } catch(ex){}
    }
    windowStocker[wname] = window.open(url, wname, opt);
}

var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=10';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");

	document.mainform.btn_event.value = "add_emp";
	document.mainform.add_emp_ids.value = emp_ids.join(",");
	document.mainform.action="timecard_tantou.php";
	document.mainform.submit();
}
function deleteEmployee() {

	var emp_ids = new Array();
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			if (document.mainform.elements['emp_id[]'].checked) {
				emp_ids.push(document.mainform.elements['emp_id[]'].value);
			}
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				if (document.mainform.elements['emp_id[]'][i].checked) {
					emp_ids.push(document.mainform.elements['emp_id[]'][i].value);
				}
			}
		}
	}
    if (emp_ids.length == 0) {
	    alert('対象者が選択されていません。');
	    return;
    }
    if (!confirm("チェックを行った職員を削除します。よろしいですか?")) return;
	
	document.mainform.btn_event.value = "del_emp";
	document.mainform.action="timecard_tantou.php";
	document.mainform.submit();
}
function show_tantou_branch(emp_id) {
    var url = "timecard_tantou.php?btn_event=show_tantou_branch&session="+session+"&target_emp_id="+emp_id;
    window_open(url, "branch_edit", 600, 380);
}
function delete_branch_select(emp_id, branch_id) {
    if (!confirm("この所属への権限を削除します。よろしいですか？")) return;
    document.mainform.target_branch_id.value = branch_id;
    document.mainform.target_emp_id.value = emp_id;
    document.mainform.btn_event.value = "del_employee_branch";
    document.mainform.submit();
}
//ソート
function set_sort(id) {

	sort_key = document.mainform.sort.value;

	if (id == "EMP_ID") {
		sort_key = (sort_key == "1") ? "2" : "1";
	} else if (id == "EMP_NAME") {
		sort_key = (sort_key == "3") ? "4" : "3";
	} else if (id == "REG_DATE") {
		sort_key = (sort_key == "6") ? "5" : "6";
	}
	document.mainform.sort.value = sort_key;
	document.mainform.submit();
}
//チェックボックスONOFF
function checkAll() {

	stat = (document.mainform.checkall.value == '解除') ? false : true;
	document.mainform.checkall.value = (document.mainform.checkall.value == '解除') ? '選択' : '解除';
	if (document.mainform.elements['emp_id[]'] != null) {
		var box_cnt = document.mainform.elements['emp_id[]'].length;
		if (box_cnt == undefined) {
			document.mainform.elements['emp_id[]'].checked = stat;
		} else {
			for (var i = 0, j = box_cnt; i < j; i++) {
				document.mainform.elements['emp_id[]'][i].checked = stat;
			}
		}
	}
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block3 {border-collapse:collapse;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="timecard_tantou.php" method="post">
<table width="850" border="0" cellspacing="0" cellpadding="2">
<tr>
<td width="200" align="left">
<input type="button" value="追加" onclick="openEmployeeList('1');">
<input type="button" value="削除" onclick="deleteEmployee();">
</td>
<td width="200"></td>
<td>
</td>
</tr>
</table>
<table width="620" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></td>
		<td colspan="2"><input type="text" name="srch_id" value="<? echo($srch_id); ?>" size="15" maxlength="12" style="ime-mode:inactive;"></td>
		<td align="right" bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員名</font></td>
		<td colspan="2"><input type="text" name="srch_name" value="<? echo($srch_name); ?>" size="25" maxlength="50" style="ime-mode:active;"></td>
		<td align="left" style="border:none"><input type="submit" value="検索" onClick="document.mainform.sort.value=3;"></td>
	</tr>
</table>
<table width="850" border="0" cellspacing="0" cellpadding="2">
<tr>
<td width="100" align="left">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">該当</font><? echo $emp_cnt; ?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人</font>
</td>
<td width="200"></td>
<td>
</td>
</tr>
</table>
<? show_navi($emp_cnt, $limit, $page); ?>
<? $arr_emp_list = show_emp_list($sel_emp, $branches_list); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="srch_flg" value="1">
<input type="hidden" name="page" value="<? echo($page); ?>">
<input type="hidden" name="check_all" value="1">
<input type="hidden" name="btn_event" value="">
<input type="hidden" name="add_emp_ids" value="">
<input type="hidden" name="target_emp_id" value="" />
<input type="hidden" name="target_branch_id" value="" />
<input type="hidden" name="sort" value="<? echo($sort); ?>" />
</form>
</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
<?
// ページ切り替えリンクを表示
function show_navi($num, $limit, $page) {
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");

    global $session, $url_srch_name, $cls, $atrb, $dept, $srch_id;

	$total_page = ceil($num/$limit);
	$check_next = $num % $limit;
	$check_last = $total_page - 1;

	if ($total_page >= 2) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
  		echo("<tr>\n");
		echo("<td align=\"center\">\n");
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		echo("ページ");
		echo("&nbsp;");
		echo("</font>");

		if ($page > 0) {
			$back = $page - 1;

			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$back&srch_flg=1&srch_id=$srch_id\">←</a></font>");
		}

		for ($i = 0; $i < $total_page; $i++) {
			$j = $i + 1;
			if ($page != $i) {
                echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> <a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$i&srch_flg=1&srch_id=$srch_id\">$j</a> </font>");
			} else {
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
				echo("［{$j}］");
				echo("</font>");
			}
		}

		if ($check_last != $page) {
			$next = $page + 1;

            echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"$fname?session=$session&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&group_id=$group_id&page=$next&srch_flg=1&srch_id=$srch_id\">→</a></font>");
		}

		echo("</td>\n");
  		echo("</tr>\n");
		echo("</table>\n");
	}
}

// 担当者一覧を表示
function show_emp_list($sel, $branches_list) {
	if (!$sel) {
		return;
	}

    global $session, $url_srch_name, $cls, $atrb, $dept, $page, $emp_id, $check_all, $srch_id, $sort;
	$arr_emp_list = array();
	
	echo("<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"60\" align=\"center\"><input type=\"button\" id=\"checkall\" name=\"checkall\" value=\"選択\" onclick=\"checkAll();\">");
	echo("</td>\n");
	echo("<td width=\"130\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	$arrow = "";
	if ($sort == "1") {
		$arrow = "img/up.gif";
	}
	if ($sort == "2") {
		$arrow = "img/down.gif";
	}
	echo("<a href=\"javascript:void();\" onClick=\"set_sort('EMP_ID');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員ID</a>");
	echo("</font></td>\n");
	echo("<td width=\"200\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	$arrow = "";
	if ($sort == "3") {
		$arrow = "img/up.gif";
	}
	if ($sort == "4") {
		$arrow = "img/down.gif";
	}
	echo("<a href=\"javascript:void();\" onClick=\"set_sort('EMP_NAME');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">職員氏名</a>");
	echo("</font></td>\n");
	echo("<td width=\"30\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td>\n");
	echo("<td width=\"230\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">所属</font></td>\n");
	echo("<td width=\"100\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	$arrow = "";
	if ($sort == "5") {
		$arrow = "img/up.gif";
	}
	if ($sort == "6") {
		$arrow = "img/down.gif";
	}
	echo("<a href=\"javascript:void();\" onClick=\"set_sort('REG_DATE');\"><img src=\"{$arrow}\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">登録日</a>"); //時
	echo("</font></td>\n");
	echo("</tr>\n");

	while ($row = pg_fetch_array($sel)) {
		$tmp_id = $row["emp_id"];
		$tmp_personal_id = $row["emp_personal_id"];
		$tmp_name = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		$tmp_reg_date = preg_replace("/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/","$1年$2月$3日",$row["reg_date"]); //時刻除外 $4:$5
		
		$tmp_class = $row["emp_class"];
		$tmp_attribute = $row["emp_attribute"];
		$tmp_dept = $row["emp_dept"];
		$buf = $tmp_id . "," . $tmp_personal_id . "," . $tmp_name . "," . $tmp_class . "," . $tmp_attribute . "," . $tmp_dept;
		$arr_emp_list[$tmp_id] = $buf;
		
		echo("<tr height=\"22\">\n");
		echo("<td width=\"60\" align=\"center\"><input name=\"emp_id[]\" type=\"checkbox\" value=\"{$tmp_id}\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_personal_id</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_name</font></td>\n");
		echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><button type=\"button\" onclick=\"show_tantou_branch('{$tmp_id}')\" style=\"width:30px;\">＋</button></font></td>\n");
        //所属
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        foreach ($branches_list[$tmp_id] as $row) {
            echo '<a href="javascript:void(0)" onclick="delete_branch_select(\''.$tmp_id;
            echo '\',\''.$row["branch_id"].'\')">'.$row["branch_disp"].'</a><br>';
        }

		echo("</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_reg_date}");
		echo("</font></td>\n");

		echo("</tr>\n");
	}

	echo("</table>\n");
	return $arr_emp_list;
}

//レイアウト名称取得
function get_layout_name($con, $fname, $layout_id) {

	$arr_layout = array();

	$sql = "select * from timecard_csv_layout_mst ";
	if ($layout_id == "") {
		$cond = " order by layout_id";
	} else {
		$cond = "where layout_id = '$layout_id'";
	}
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);

	for ($i=0; $i<$num; $i++) {
		$id = pg_result($sel,$i,"layout_id");
		$arr_layout["$id"]["name"] = pg_result($sel,$i,"layout_name");
	}
	return $arr_layout;
}
//担当者検索用SQL取得
function get_cond_tantou($con, $fname, $srch_name, $srch_id) {

	$cond = "where tb.class_id = 0 ";
    //検索条件　職員ID
    if ($srch_id != "") {
        $tmp_srch_id = str_replace(" ", "", $srch_id);
        $cond .= " and (empmst.emp_personal_id like '%$tmp_srch_id%')";
    }
    //検索条件　職員名
	if ($srch_name != "") {
		$tmp_srch_name = str_replace(" ", "", $srch_name);
        $cond .= " and (emp_lt_nm like '%$tmp_srch_name%' or emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm like '%$tmp_srch_name%' or emp_kn_ft_nm like '%$tmp_srch_name%' or emp_lt_nm || emp_ft_nm like '%$tmp_srch_name%' or emp_kn_lt_nm || emp_kn_ft_nm like '%$tmp_srch_name%')";
	}
	return $cond;
}
//担当者追加
function add_emp($con, $fname) {
	$add_emp_ids = $_REQUEST["add_emp_ids"];
	$arr_emp_id = split(",", $add_emp_ids);
	
	pg_query($con, "begin transaction");
	foreach ($arr_emp_id as $wk_emp_id) {
		
		$sql = "select count(*) as cnt from timecard_tantou_branches ";
		$cond = "where emp_id = '$wk_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$cnt = pg_fetch_result($sel, 0, "cnt");
		if ($cnt == 0) {
			$sql = "insert into timecard_tantou_branches (emp_id, class_id, atrb_id, dept_id, room_id, reg_date) values (";
			$content = array($wk_emp_id, 0, 0, 0, 0, date("YmdHis"));
			$ins = insert_into_table($con, $sql, $content, $fname);
			if ($ins == 0) {
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
	}
	pg_query($con, "commit");

}
//担当者削除
function del_emp($con, $fname) {
	$arr_emp_id = $_REQUEST["emp_id"];
	
	foreach ($arr_emp_id as $wk_emp_id) {
		$sql = "DELETE FROM timecard_tantou_branches ";
		$cond = "WHERE emp_id = '$wk_emp_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	}
}

//担当者所属一覧取得
function get_emp_tantou_branches_list($con, $fname) {
    $sql =
    " select tb.emp_id, tb.class_id, tb.atrb_id, tb.dept_id, tb.room_id, cm.class_nm, am.atrb_nm, dm.dept_nm, cr.room_nm".
    " from timecard_tantou_branches tb".
    " left outer join classmst cm on (cm.class_id = tb.class_id and cm.class_del_flg = false)".
    " left outer join atrbmst am on (am.atrb_id = tb.atrb_id and am.atrb_del_flg = false)".
    " left outer join deptmst dm on (dm.dept_id = tb.dept_id and dm.dept_del_flg = false)".
    " left outer join classroom cr on (cr.room_id = tb.room_id and cr.room_del_flg = false)";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$b_rows = pg_fetch_all($sel);
    $ab_rows = array();
    foreach ($b_rows as $row) {
        if (!$row["class_id"]) continue;
        $ary = array($row["class_nm"]);
        if ($row["atrb_nm"]) $ary[]=$row["atrb_nm"];
        if ($row["dept_nm"]) $ary[]=$row["dept_nm"];
        if ($row["room_nm"]) $ary[]=$row["room_nm"];
        $ab_rows[$row["emp_id"]][] = array(
            "branch_disp" => str_replace("\n", WBR."＞", h(implode("\n", $ary))),
            "branch_id"   => $row["class_id"].",".$row["atrb_id"].",".$row["dept_id"].",".$row["room_id"]
        );
    }
	return $ab_rows;
}

//担当者一覧表示
function show_tantou_branches($con, $fname) {

	$arr_class_name = get_class_name_array($con, $fname);

	// 組織情報を取得し、配列に格納
	$arr_cls = array();
	$arr_clsatrb = array();
	$arr_atrbdept = array();
	$sql = "select class_id, class_nm from classmst";
	$cond = "where class_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$tmp_class_id = $row["class_id"];
		$tmp_class_nm = $row["class_nm"];
		array_push($arr_cls, array("id" => $tmp_class_id, "name" => $tmp_class_nm));

		$sql2 = "select atrb_id, atrb_nm from atrbmst";
	    $cond2 = "where class_id = '$tmp_class_id' and atrb_del_flg = 'f' order by order_no";
		$sel2 = select_from_table($con, $sql2, $cond2, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$arr_atrb = array();
		while ($row2 = pg_fetch_array($sel2)) {
			$tmp_atrb_id = $row2["atrb_id"];
			$tmp_atrb_nm = $row2["atrb_nm"];
			array_push($arr_atrb, array("id" => $tmp_atrb_id, "name" => $tmp_atrb_nm));

			$sql3 = "select dept_id, dept_nm from deptmst";
	        $cond3 = "where atrb_id = '$tmp_atrb_id' and dept_del_flg = 'f' order by order_no";
			$sel3 = select_from_table($con, $sql3, $cond3, $fname);
			if ($sel3 == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
				exit;
			}
			$arr_dept = array();
			while ($row3 = pg_fetch_array($sel3)) {
				$tmp_dept_id = $row3["dept_id"];
				$tmp_dept_nm = $row3["dept_nm"];
				array_push($arr_dept, array("id" => $tmp_dept_id, "name" => $tmp_dept_nm));
			}
			array_push($arr_atrbdept, array("atrb_id" => $tmp_atrb_id, "depts" => $arr_dept));
		}
		array_push($arr_clsatrb, array("class_id" => $tmp_class_id, "atrbs" => $arr_atrb));
	}

?>
<title>CoMedix 勤務管理｜所属選択</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
<!--
function initPage() {
	classOnChange('<? echo($atrb); ?>', '<? echo($dept); ?>');
}

function classOnChange(atrb_id, dept_id) {
	if (!document.mainform) {
		return;
	}

	var class_id = document.mainform.cls.value;

	deleteAllOptions(document.mainform.atrb);

	addOption(document.mainform.atrb, '-', '----------', atrb_id);

<? foreach ($arr_clsatrb as $tmp_clsatrb) { ?>
	if (class_id == '<? echo $tmp_clsatrb["class_id"]; ?>') {
	<? foreach($tmp_clsatrb["atrbs"] as $tmp_atrb) { ?>
		addOption(document.mainform.atrb, '<? echo $tmp_atrb["id"]; ?>', '<? echo $tmp_atrb["name"]; ?>', atrb_id);
	<? } ?>
	}
<? } ?>

	atrbOnChange(dept_id);
}

function atrbOnChange(dept_id) {
	var atrb_id = document.mainform.atrb.value;

	deleteAllOptions(document.mainform.dept);

	addOption(document.mainform.dept, '-', '----------', dept_id);

<? foreach ($arr_atrbdept as $tmp_atrbdept) { ?>
	if (atrb_id == '<? echo $tmp_atrbdept["atrb_id"]; ?>') {
	<? foreach($tmp_atrbdept["depts"] as $tmp_dept) { ?>
		addOption(document.mainform.dept, '<? echo $tmp_dept["id"]; ?>', '<? echo $tmp_dept["name"]; ?>', dept_id);
	<? } ?>
	}
<? } ?>
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
	if (selected == value) {
		box.selectedIndex = box.options.length -1;
	}
}
var gg_class_nm = "<? echo $arr_class_name[0]; ?>";
function exec_tantou_branch() {
    if (document.mainform.cls.value=="-") return alert(gg_class_nm+"を指定してください。");
    document.mainform.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.block3 {border-collapse:collapse;}
table.block3 td {border-width:0;}
.gray { color:#aaaaaa; font-size:13px; }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="3" leftmargin="3" marginheight="3" marginwidth="3" onload="initPage();">
<form name="mainform" method="post">
<input type="hidden" name="session" value="<? echo($_REQUEST["session"]); ?>" />
<input type="hidden" name="btn_event" value="add_tantou_branch" />
<input type="hidden" name="target_emp_id" value="<? echo($_REQUEST["target_emp_id"]); ?>" />
<input type="hidden" name="target_branch_id" value="" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>所属選択</b></font></td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff" width="100"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?></font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="cls" onchange="classOnChange();">
				<option value="-">----------
<?
foreach ($arr_cls as $tmp_cls) {
	$tmp_id = $tmp_cls["id"];
	$tmp_name = $tmp_cls["name"];
	echo("<option value=\"$tmp_id\"");
	if ($tmp_id == $cls) {
		echo(" selected");
	}
	echo(">$tmp_name");
}
?>
				</select>
</td>
</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?></font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="atrb" onchange="atrbOnChange();">
			</select>
</td>
</tr>
	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?></font></td>
		<td nowrap valign="top"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			<select name="dept">
			</select>
</font>
</td>
</tr>
</table>
<div style="padding:5px 0; text-align:left" class="gray">
指定所属より配下の所属へのアクセス権限を付与します。
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
		<td align="right">
		<input type="button" value="選択" onclick="exec_tantou_branch();">
</td>
</tr>
</table>

</form>
<?
}

//担当者の所属追加
function add_tantou_branches($con, $fname, $emp_id, $class_id, $atrb_id, $dept_id, $room_id) {
	if ($atrb_id == "-") { $atrb_id = 0; }
	if ($dept_id == "-") { $dept_id = 0; }
	if ($room_id == "" || $room_id == "-") { $room_id = 0; }

	del_tantou_branches($con, $fname, $emp_id, $class_id, $atrb_id, $dept_id, $room_id);
    $sql =
    "insert into timecard_tantou_branches (emp_id, class_id, atrb_id, dept_id, room_id) values (";
	$content = array($emp_id, $class_id, $atrb_id, $dept_id, $room_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}

//担当者の所属削除
function del_tantou_branches($con, $fname, $emp_id, $class_id, $atrb_id, $dept_id, $room_id) {
	if ($atrb_id == "-") { $atrb_id = 0; }
	if ($dept_id == "-") { $dept_id = 0; }
	if ($room_id == "" || $room_id == "-") { $room_id = 0; }
    $sql = " delete from timecard_tantou_branches ";
    $cond = "where emp_id = '$emp_id' and class_id = $class_id and atrb_id = $atrb_id and dept_id = $dept_id and room_id = $room_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

}

?>
