<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("maintenance_menu_list.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 組織プロフィール権限のチェック
$auth = check_authority($session, 37, $fname);
if ($auth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ロゴ画像が登録されているかチェック
$logo_exists = (
	is_file("profile/img/logo.gif") ||
	is_file("profile/img/logo.jpg") ||
	is_file("profile/img/logo.png")
);

// データベースへ接続
$con = connect2db($fname);

// 登録値の取得
$sql = "select * from profile";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
if (pg_num_rows($sel) == 1) {
	$name = pg_fetch_result($sel, 0, "prf_name");
	$zip1 = pg_fetch_result($sel, 0, "prf_zip1");
	$zip2 = pg_fetch_result($sel, 0, "prf_zip2");
	$prv = pg_fetch_result($sel, 0, "prf_prv");
	$addr1 = pg_fetch_result($sel, 0, "prf_addr1");
	$addr2 = pg_fetch_result($sel, 0, "prf_addr2");
	$tel1 = pg_fetch_result($sel, 0, "prf_tel1");
	$tel2 = pg_fetch_result($sel, 0, "prf_tel2");
	$tel3 = pg_fetch_result($sel, 0, "prf_tel3");
	$fax1 = pg_fetch_result($sel, 0, "prf_fax1");
	$fax2 = pg_fetch_result($sel, 0, "prf_fax2");
	$fax3 = pg_fetch_result($sel, 0, "prf_fax3");
	$org_cd = pg_fetch_result($sel, 0, "prf_org_cd");
	$bed_cnt = pg_fetch_result($sel, 0, "prf_bed_cnt");
	$url = pg_fetch_result($sel, 0, "prf_url");
	$logo_type = pg_fetch_result($sel, 0, "prf_logo_type");
	$type = pg_fetch_result($sel, 0, "prf_type");
}

// ロゴ表示タイプは「テキスト」がデフォルト
if ($logo_type == "") {
	$logo_type = "2";
}

// 組織タイプは「汎用」がデフォルト
if ($type == "") {
	$type = "2";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 組織プロフィール</title>
<script type="text/javascript" src="js/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
function setType() {
	var name, org_cd, bed_cnt_disp;
	if (document.prf.type.value == '1') {
		name = '医療機関名称';
		org_cd = '医療機関コード';
		bed_cnt_disp = '';
	} else if (document.prf.type.value == '2') {
		name = '企業名';
		org_cd = '契約番号';
		bed_cnt_disp = 'none';
	} else {
		name = '法人名';
		org_cd = '契約番号';
		bed_cnt_disp = 'none';
	}

	var sp_name = document.getElementById('sp_name');
	while (sp_name.hasChildNodes()) {
		sp_name.removeChild(sp_name.firstChild);
	}
	sp_name.appendChild(document.createTextNode(name));

	var sp_org_cd = document.getElementById('sp_org_cd');
	while (sp_org_cd.hasChildNodes()) {
		sp_org_cd.removeChild(sp_org_cd.firstChild);
	}
	sp_org_cd.appendChild(document.createTextNode(org_cd));

	document.getElementById('tr_bed_cnt').style.display = bed_cnt_disp;
}
$(function() {
    $('#org_cd_check').click(function() {
        if ($(this).get(0).checked) {
            $('#org_cd').prop('readonly', false).removeClass('readonly');
        }
        else {
            $('#org_cd').prop('readonly', true).addClass('readonly');
        }
    });
});
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.readonly {
    background-color: silver;
    color: grey;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setType();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="hospital_profile_menu.php?session=<? echo($session); ?>"><b>組織プロフィール</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#5279a5"><a href="hospital_profile_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>組織プロフィール</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="prf" action="hospital_profile_update.php" method="post" enctype="multipart/form-data">
    <table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
        <tr>
            <td width="120" height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織タイプ</font></td>
            <td width="480">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <select name="type" onchange="setType();">
                        <option value="2"<? if ($type == "2") { echo(" selected"); } ?>>汎用</option>
                        <option value="1"<? if ($type == "1") { echo(" selected"); } ?>>医療機関</option>
                        <option value="3"<? if ($type == "3") { echo(" selected"); } ?>>社会福祉法人</option>
                    </select>
                </font>
            </td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="sp_name"></span></font></td>
            <td><input type="text" name="name" value="<? echo($name) ?>" size="40" maxlength="100" style="ime-mode: active;"></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">郵便番号</font></td>
            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="zip1" value="<? echo($zip1); ?>" size="4" maxlength="3" style="ime-mode: inactive;">-<input type="text" name="zip2" value="<? echo($zip2); ?>" size="5" maxlength="4" style="ime-mode: inactive;"></font></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">都道府県</font></td>
            <td><select name="prv"><? show_select_provinces($prv); ?></select></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所1</font></td>
            <td><input type="text" name="addr1" value="<? echo($addr1); ?>" size="50" maxlength="100" style="ime-mode: active;"></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">住所2</font></td>
            <td><input type="text" name="addr2" value="<? echo($addr2); ?>" size="50" maxlength="100" style="ime-mode: active;"></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">電話番号</font></td>
            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="tel1" value="<? echo($tel1); ?>" size="5" maxlength="6" style="ime-mode: inactive;">-<input type="text" name="tel2" value="<? echo($tel2); ?>" size="5" maxlength="6" style="ime-mode: inactive;">-<input type="text" name="tel3" value="<? echo($tel3); ?>" size="5" maxlength="6" style="ime-mode: inactive;"></font></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">FAX</font></td>
            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="fax1" value="<? echo($fax1); ?>" size="5" maxlength="6" style="ime-mode: inactive;">-<input type="text" name="fax2" value="<? echo($fax2); ?>" size="5" maxlength="6" style="ime-mode: inactive;">-<input type="text" name="fax3" value="<? echo($fax3); ?>" size="5" maxlength="6" style="ime-mode: inactive;"></font></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="sp_org_cd"></span></font></td>
            <td>
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                <input type="text" id="org_cd" name="org_cd" value="<? echo($org_cd); ?>" size="14" maxlength="10" style="ime-mode: inactive;" <? if ($org_cd) echo 'readonly class="readonly"'; ?>>
                <? if ($org_cd) {?>
                <input type="checkbox" id="org_cd_check">変更する<br>
                <span style="color:red;">変更するとライセンス管理、ランチャー、シングルサインオンが動作しなくなります</span>
                </font>
                <?}?>
            </td>
        </tr>
        <tr id="tr_bed_cnt" style="display:none;">
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床数</font></td>
            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="bed_cnt" value="<? echo($bed_cnt); ?>" size="5" maxlength="4" style="ime-mode: inactive;">床</font></td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ロゴ画像</font></td>
            <td>
                <input type="file" name="logo" size="40">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <? if ($logo_exists) { echo("（変更する場合のみ）"); } ?><br>
                    サイズ: 幅150 x 高さ50 まで<br>
                    形式: jpg gif png
                </font>
            </td>
        </tr>
        <tr>
            <td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ホームページ</font></td>
            <td><input type="text" name="url" value="<? echo($url); ?>" size="50" maxlength="120" style="ime-mode: inactive;"></td>
        </tr>
        <tr>
            <td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示タイプ</font></td>
            <td height="22">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                    <input type="radio" name="logo_type" value="1"<? if ($logo_type == "1") { echo(" checked"); } ?>>ロゴ表示&nbsp;
                    <input type="radio" name="logo_type" value="2"<? if ($logo_type == "2") { echo(" checked"); } ?>>テキスト表示
                </font>
            </td>
        </tr>
    </table>
    <table width="600" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td height="22" align="right"><input type="submit" value="登録"></td>
        </tr>
    </table>
    <input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>
