<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>アドレス帳 | 一括登録</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<?php require("about_authority.php"); ?>
<?php require("about_session.php"); ?>
<?php require("show_prefectures.ini"); ?>
<?php
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アドレス帳権限のチェック
$checkauth = check_authority($session, 64, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 登録先のデフォルトは「個人アドレス帳」
if ($shared_flg == "") {
	$shared_flg = "f";
}
//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.target_emp_id.value == '') {
		alert('職員を選択してください。');
		return false;
	}
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	} else {
		closeEmployeeList();
		return true;
	}
}
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=13';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

function add_target_list(item_id, emp_id,emp_name)
{
	var emp_ids = emp_id.split(", ");
	var emp_names = emp_name.split(", ");
	if(emp_ids.length > 1) {
		alert('職員は１名のみ指定してください。');
		return false;
	}

	document.address.target_emp_id.value = emp_id;
	document.address.target_emp_name.value = emp_name;

}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="address_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b13.gif" width="32" height="32" border="0" alt="アドレス帳"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<?php echo($session); ?>"><b>アドレス帳</b></a> &gt; <a href="address_admin_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="address_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="./address_admin_menu.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">アドレス一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="address_admin_bulk_register.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="address" action="address_bulk_insert.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td width="120" height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録先</font></td>
<td width="480"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="shared_flg" value="f"<?php if ($shared_flg == "f") {echo(" checked");} ?>>個人アドレス帳&nbsp;<input type="radio" name="shared_flg" value="t"<?php if ($shared_flg == "t") {echo(" checked");} ?>>共有アドレス帳</font></td>
</tr>
<tr>
<td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font></td>
<td><input name="target_emp_name" type="text" value="" size="30" readonly><input type="button" name="emplist_btn" value="職員名簿" style="margin-left:2em;width: 5.5em;" onclick="openEmployeeList('1');"></td>
</tr>
<tr>
<td height="22" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="22" colspan="2" align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="admin_flg" value="t">
<input name="target_emp_id" type="hidden" value="">
</form>
<?php if ($result != "") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<?php } ?>
<?php if ($result == "f") { ?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルの内容に問題があるため、登録処理が実行できませんでした。</font></td>
</tr>
<?php } else if ($result == "t") {?>
<tr>
<td height="22" width="120"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。</font></td>
</tr>
<?php } ?>
<?php if ($result != "") { ?>
</table>
<?php } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。</p>
<ul style="margin:0;">
<li style="margin-left:40px;">姓（漢字）･･･必須</li>
<li style="margin-left:40px;">名（漢字）･･･必須</li>
<li style="margin-left:40px;">姓（カナ）</li>
<li style="margin-left:40px;">名（カナ）</li>
<li style="margin-left:40px;">会社名･･･必須</li>
<li style="margin-left:40px;">部門名１</li>
<li style="margin-left:40px;">部門名２</li>
<li style="margin-left:40px;">役職１</li>
<li style="margin-left:40px;">役職２</li>
<li style="margin-left:40px;">郵便番号･･･ハイフン区切り</li>
<li style="margin-left:40px;">都道府県名･･･「北海道」〜「沖縄県」</li>
<li style="margin-left:40px;">住所１</li>
<li style="margin-left:40px;">住所２</li>
<li style="margin-left:40px;">住所３</li>
<li style="margin-left:40px;">電話･･･ハイフン区切り</li>
<li style="margin-left:40px;">FAX･･･ハイフン区切り</li>
<li style="margin-left:40px;">携帯／PHS･･･ハイフン区切り</li>
<li style="margin-left:40px;">E-MAIL（PC）</li>
<li style="margin-left:40px;">E-MAIL（携帯）</li>
<li style="margin-left:40px;">備考</li>
</ul>
</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
