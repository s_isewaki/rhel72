<?
define("ROWS_PER_PAGE", 20);

function show_news($con, $page, $session, $fname) {
	require_once("show_class_name.ini");

	$offset = ROWS_PER_PAGE * ($page - 1);
	$limit = ROWS_PER_PAGE;

	$sql = "select * from inci_news";
	$cond = "where news_del_flg = 'f' order by news_date desc, news_id desc offset $offset limit $limit";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$arr_class_name = get_class_name_array($con, $fname);
	$class_called = $arr_class_name[0];

	while ($row = pg_fetch_array($sel)) {
		$tmp_news_id = $row["news_id"];
		$tmp_news_date = $row["news_date"];
		$tmp_news_title = htmlspecialchars($row["news_title"]);
		$tmp_news_category = $row["news_category"];
		$tmp_news_begin = $row["news_begin"];
		$tmp_news_end = $row["news_end"];
		$tmp_record_flg = $row["record_flg"];

		$style = get_style_by_marker_hiyari_management($row["marker"]);
		
		$tmp_news_date = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_date);
		$tmp_news_begin = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_begin);
		$tmp_news_end = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $tmp_news_end);

		switch ($tmp_news_category) {
		case "1":
			$tmp_news_category_name = "����";
			break;
		case "2":
			$tmp_news_category_name = $class_called;
			break;
		case "3":
			$tmp_news_category_name = "����";
			break;
		}

		echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
		echo("<td align=\"center\"><input name=\"news_ids[]\" type=\"checkbox\" value=\"$tmp_news_id\"></td>\n");
		echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">$tmp_news_date</font></td>\n");
		echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">$tmp_news_category_name</font></td>\n");
		echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"hiyari_news_update.php?session=$session&news_id=$tmp_news_id&page=$page\" $style>$tmp_news_title</a></font></td>\n");
		echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">$tmp_news_begin �� $tmp_news_end</font></td>\n");
		echo("<td><font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\">");
		if ($tmp_record_flg == "t") {
			echo("<a href=\"hiyari_news_refer_list.php?session=$session&news_id=$tmp_news_id&page=$page\">���ȼ԰���</a>");
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");
		echo("</tr>\n");
	}
}

function show_news_page_list($con, $page, $session, $fname) {
	$sql = "select count(*) from inci_news";
	$cond = "where news_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$row_count = pg_fetch_result($sel, 0, 0);

	if ($row_count <= ROWS_PER_PAGE) {
		return;
	}

	$max_page = ceil($row_count / ROWS_PER_PAGE);
	echo("<div align=\"right\">\n");
	if ($page > 1) {
		$last_page = $page - 1;
		echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"hiyari_news_menu.php?session=$session&page=$last_page\">��</a></font>\n");
	}
	for ($i = 1; $i <= $max_page; $i++) {
		if ($i == $page) {
			echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\" color=\"#FF6600\">[$i]</font>\n");
		} else {
			echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"hiyari_news_menu.php?session=$session&page=$i\">$i</a></font>\n");
		}
	}
	if ($page < $max_page) {
		$next_page = $page + 1;
		echo("<font size=\"3\" face=\"�ͣ� �Х����å�, Osaka\" class=\"j12\"><a href=\"hiyari_news_menu.php?session=$session&page=$next_page\">��</a></font>\n");
	}
	echo("</div>\n");
}


// �饤��ޡ������Υ���������������
function get_style_by_marker_hiyari_management($marker) {
	switch ($marker) {
		case 1: return " style=\"background-color:red;color:white;padding:2px;\"";// ��
		case 2: return " style=\"background-color:aqua;color:blue;padding:2px;\"";// ��
		case 3: return " style=\"background-color:yellow;color:blue;padding:2px;\"";// ��
		case 4: return " style=\"background-color:lime;color:blue;padding:2px;\"";// ��
		case 5: return " style=\"background-color:fuchsia;color:white;padding:2px;\"";// �ԥ�
	default:return "";  
	}
}


?>
