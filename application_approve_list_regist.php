<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
    <form name="items" action="application_approve_list.php" method="post">
        <input type="hidden" name="session" value="<?echo($session);?>">
        <input type="hidden" name="category" value="<?echo($category)?>">
        <input type="hidden" name="workflow" value="<?echo($workflow)?>">
        <input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
        <input type="hidden" name="apply_emp_nm" value="<?echo($apply_emp_nm)?>">
        <input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
        <input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
        <input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
        <input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
        <input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
        <input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
        <input type="hidden" name="apv_stat" value="<?echo($apv_stat)?>">
        <input type="hidden" name="page" value="<?echo($page)?>">
        <input type="hidden" name="all_disp_flg" value="<?echo($all_disp_flg)?>">
    </form>

    <?

    require_once("about_comedix.php");
    require_once("application_workflow_common_class.php");
    require_once("atdbk_workflow_common_class.php");
    require_once("show_clock_in_common.ini");
    require_once("get_values.ini");
    require_once("Cmx.php");
    require_once('Cmx/Model/SystemConfig.php');
    require_once("aclg_set.php");

    $fname = $PHP_SELF;

    // セッションのチェック
    $session = qualify_session($session, $fname);
    if ($session == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;

    }

    // 決裁・申請権限のチェック
    $checkauth = check_authority($session, 7, $fname);
    if ($checkauth == "0") {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }

    // 通らないはずだが念のためチェック
    if ($approve == "") {
        echo("<script type=\"text/javascript\">alert('承認ステータスが不明のため処理を中断します。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }

    $conf = new Cmx_SystemConfig();
    $title_label = ($conf->get('apply.view.title_label') == "2") ? "標題" : "表題";

    // データベースに接続
    $con = connect2db($fname);

    // アクセスログ
    aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

    //ワークフロー関連共通クラス
    $obj = new application_workflow_common_class($con, $fname);

    //出勤表ワークフロー関連共通クラス
    $atdbk_class = new atdbk_workflow_common_class($con, $fname);

    // ログインユーザの職員IDを取得
    $sql = "select emp_id from session";
    $cond = "where session_id = '$session'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");

    // トランザクションを開始
    pg_query($con, "begin");
    //勤怠申請機能を使用するかファイルで確認
    $kintai_flag = file_exists("kintai/common/mdb.php");
    // 総合届け使用フラグ
    $general_flag = file_exists("opt/general_flag");
    // ----- メディアテック Start -----
    if (phpversion() >= '5.1' && $kintai_flag) {
        require_once 'kintai/common/mdb_wrapper.php';
        require_once 'kintai/common/mdb.php';
        MDBWrapper::init();
        $mdb = new MDB();
        $mdb->beginTransaction(); // トランザクション開始
    }
    // ----- メディアテック End -----
    //休暇申請で確定する階層設定 "1":1階層目 "","2":最終承認の階層 20141010
    $apply_hol_cfm = $conf->get('apply.hol.cfm');
    if ($apply_hol_cfm == "") {
        $apply_hol_cfm = "2";
    }

    $must_send_mail_applys = array();
    $aprv_emp_id = get_emp_id($con, $session, $fname);
    $aprv_emp_detail = $obj->get_empmst_detail($aprv_emp_id);
    $aprv_emp_mail_header = format_emp_mail_header($aprv_emp_detail[0]);
    $aprv_emp_mail = format_emp_mail($aprv_emp_detail[0]);
    $aprv_mail_data = array();
    $files = array();
    for ($i = 0, $count = count($approve_chk); $i < $count; $i++) {
        // パラメータ取得
        $app_values = $approve_chk[$i];
        $arr_app = split(",", $app_values);

        //種別に応じた更新処理を行う
        $approve_type = $arr_app["3"]; //位置を0から3に変更 20141126
        $apply_id = $arr_app["0"];
        $apv_order = $arr_app["1"];
        $apv_sub_order = $arr_app["2"];

        //委員会議事録
        //一覧では未使用だが残す。使用する場合は、再確認すること
        if ($approve_type == "PRCD") {
            //承認の場合
            if ($approve == "1") {
                $pjt_schd_id = $arr_app["0"];
                approve_prcd($con, $fname, $emp_id, $pjt_schd_id);
            }
        }
        //残業申請
        elseif ($approve_type == "OVTM") {
            //承認処理
            approve_ovtm($con, $fname, $session, $atdbk_class, $apply_id, $apv_order, $apv_sub_order, $approve);
        }
        //時間修正申請
        elseif ($approve_type == "TMMD") {
            //承認処理
            approve_tmmd($con, $fname, $session, $atdbk_class, $apply_id, $apv_order, $apv_sub_order, $approve);
        }
        //呼出申請
        elseif ($approve_type == "RTN") {
            //承認処理
            approve_rtn($con, $fname, $session, $atdbk_class, $apply_id, $apv_order, $apv_sub_order, $approve);
        }
        // 勤務表月次承認
        elseif ($approve_type === "OPH") {
            // (承認のみ受け付ける)
            if ($approve === "1") {
                approve_oph($emp_id, $apply_id);
            }
        }
        else {

            $arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
            $wkfw_appr = $arr_apply_wkfwmst[0]["wkfw_appr"];

            $arr_applyapv_per_hierarchy = $obj->get_applyapv_per_hierarchy($apply_id, $apv_order);
            $next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

            // 承認処理
            $approval_just_fixed = $obj->approve_application($apply_id, $wkfw_appr, $apv_order, $apv_sub_order, $approve, "", $next_notice_div, $session, "LIST");

            // ----- メディアテック Start -----
            if (phpversion() >= '5.1' && $kintai_flag) {
                // テンプレートタイプを取得
                require_once 'kintai/common/workflow_common_util.php';
                require_once 'kintai/model/kintai_late_basic_model.php';
                require_once 'kintai/model/kintai_late_record.php';
                $template_type = WorkflowCommonUtil::get_template_type($apply_id);

                $kintai_late_info = KintaiLateBasicModel::get($apply_id); // 20140725

                $class = null;
                switch ($template_type) {
                    case "ovtm": // 時間外勤務命令の場合
                        if (module_check(WORKFLOW_OVTM_MODULE, WORKFLOW_OVTM_CLASS)) {
                            $class = WORKFLOW_OVTM_CLASS;
                        }
                        break;
                    case "hol": // 休暇申請の場合
                        //総合届け
                        if ($general_flag) {
                            if (module_check(WORKFLOW_HOL_MODULE, WORKFLOW_HOL_CLASS)) {
                                $class = WORKFLOW_HOL_CLASS;
                            }
                        }
                        //休暇申請
                        else {
                            if (module_check(WORKFLOW_HOL2_MODULE, WORKFLOW_HOL2_CLASS)) {
                                $class = WORKFLOW_HOL2_CLASS;
                            }
                        }
                        break;
                    case "travel": // 旅行命令
                        if (module_check(WORKFLOW_TRVL_MODULE, WORKFLOW_TRVL_CLASS)) {
                            $class = WORKFLOW_TRVL_CLASS;
                        }
                        break;
                    case "late": // 遅刻早退の場合
                        if ($kintai_late_info->transfer == "1" or $kintai_late_info->transfer == "2") {
                            if (module_check(WORKFLOW_LATE2_MODULE, WORKFLOW_LATE2_CLASS)) {
                                $class = WORKFLOW_LATE2_CLASS;
                                $module = new $class;
                            }
                        }
                        else {
                            if (module_check(WORKFLOW_LATE_MODULE, WORKFLOW_LATE_CLASS)) {
                                $class = WORKFLOW_LATE_CLASS;
                            }
                        }
                        break;
                }
                if (!empty($class)) {
                    $module = new $class;

                    switch ($approve) {
                        case "1": // 承認の場合
                            if ($template_type != "hol" || //テンプレートが休暇申請以外のとき
                                ($apply_hol_cfm == "2" && $template_type == "hol" && $approval_just_fixed) || //休暇申請で最終承認確認が必要の場合、承認確定時
                                ($apply_hol_cfm == "1" && $template_type == "hol")  //休暇申請で1階層目でよい場合、確定の確認をしない
                            ) {
                                $module->approve($apply_id, $con);
                            }
                            break;
                        case "2": // 否認の場合
                            $module->cancel_apply($apply_id);
                            break;
                        case "3": // 差戻しの場合
                            $module->update_status($apply_id, $approve);
                            break;
                    }
                }
            }
            // ----- メディアテック End  -----
            // 承認依頼受信通知メールを送るかどうか判断、送る場合は宛先を配列に追加
            if (must_send_mail_to_next_approvers($con, $obj, $apply_id, $wkfw_appr, $apv_order, $approve, $next_notice_div, $fname)) {

                // 次の階層で未承認の承認者（全員のはず）のメールアドレスを取得
                $next_apv_order = $apv_order + 1;
                $sql = "select e.emp_email2 from empmst e";
                $cond = "where exists (select * from applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id and a.apv_order = $next_apv_order and a.apv_stat = '0')";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }
                $to_addresses = array();
                while ($row = pg_fetch_array($sel)) {
                    if ($row["emp_email2"] != "") {
                        $to_addresses[] = $row["emp_email2"];
                    }
                }

                // 承認依頼受信通知メール送信に必要な情報を配列に格納
                if (count($to_addresses) > 0) {
                    $emp_detail = $obj->get_empmst_detail($arr_apply_wkfwmst[0]["emp_id"]);

                    $must_send_mail_applys[] = array(
                        "to_addresses" => $to_addresses,
                        "wkfw_content_type" => $arr_apply_wkfwmst[0]["wkfw_content_type"],
                        "content" => $arr_apply_wkfwmst[0]["apply_content"],
                        "apply_title" => $arr_apply_wkfwmst[0]["apply_title"],
                        "emp_nm" => format_emp_nm($emp_detail[0]),
                        "emp_mail_header" => format_emp_mail_header($emp_detail[0]),
                        "emp_mail" => format_emp_mail($emp_detail[0]),
                        "emp_pos" => format_emp_pos($emp_detail[0]),
                        "approve_label" => ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認"
                    );
                }
            }

            // 承認確定時なら、承認確定通知メールの送信に必要なデータを取得
            $must_send_aprv_mail = ($approval_just_fixed && get_wkfw_aprv_mail_flg($con, $arr_apply_wkfwmst[0]["wkfw_id"], $fname) == "t");
            if ($must_send_aprv_mail) {
                $apply_emp_detail = $obj->get_empmst_detail($arr_apply_wkfwmst[0]["emp_id"]);
                $apply_emp_mail = $apply_emp_detail[0]["emp_email2"];

                if ($apply_emp_mail != "") {
                    $approve_label = ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認";

                    $aprv_mail_data[] = array(
                        "apply_emp_mail" => $apply_emp_mail,
                        "approve_label" => $approve_label,
                        "wkfw_title" => $arr_apply_wkfwmst[0]["wkfw_title"],
                        "apply_time" => format_apply_time($arr_apply_wkfwmst[0]["apply_date"]),
                        "apply_title" => $arr_apply_wkfwmst[0]["apply_title"]
                    );
                }
            }

            // 文書登録処理
            $tmp_files = register_library($con, $approve, $obj, $apply_id, $fname);
            $files = array_merge($tmp_files, $files);
        }
    }

    // ----- メディアテック Start -----
    if (phpversion() >= '5.1' && $kintai_flag) {
        // MDBトランザクションをコミット
        $mdb->endTransaction();
    }
    // ----- メディアテック End -----
    // トランザクションをコミット
    pg_query($con, "commit");

    // 承認依頼受信通知メール送信
    if (count($must_send_mail_applys) > 0) {
        mb_internal_encoding("EUC-JP");
        mb_language("Japanese");

        foreach ($must_send_mail_applys as $data) {
            $mail_subject = "[CoMedix] {$data["approve_label"]}依頼のお知らせ";
            $mail_content = format_mail_content($con, $data["wkfw_content_type"], $data["content"], $fname);
            $mail_separator = str_repeat("-", 60) . "\n";
            $additional_headers = "From: {$data["emp_mail_header"]}";
            $additional_parameter = "-f{$data["emp_mail"]}";

            foreach ($data["to_addresses"] as $to_address) {
                $mail_body = "以下の{$data["approve_label"]}依頼がありました。\n\n";
                $mail_body .= "申請者：{$data["emp_nm"]}\n";
                $mail_body .= "所属：{$data["emp_pos"]}\n";
                $mail_body .= "{$title_label}：{$data["apply_title"]}\n";
                if ($mail_content != "") {
                    $mail_body .= $mail_separator;
                    $mail_body .= "{$mail_content}\n";
                }

                mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
            }
        }
    }

    // データベース接続を切断
    pg_close($con);

    // 承認確定通知メール送信
    mb_internal_encoding("EUC-JP");
    mb_language("Japanese");
    foreach ($aprv_mail_data as $tmp_amil_data) {
        $mail_subject = "[CoMedix] {$tmp_amil_data["approve_label"]}確定のお知らせ";

        $mail_body = "以下の申請が{$tmp_amil_data["approve_label"]}確定されました。\n\n";
        $mail_body .= "申請書名：{$tmp_amil_data["wkfw_title"]}\n";
        $mail_body .= "申請日時：{$tmp_amil_data["apply_time"]}\n";
        $mail_body .= "{$title_label}：{$tmp_amil_data["apply_title"]}\n";

        $additional_headers = "From: $aprv_emp_mail_header";
        $additional_parameter = "-f$aprv_emp_mail";

        mb_send_mail($apply_emp_mail, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
    }

    // ファイルコピー
    foreach ($files as $tmp_file) {
        copy_apply_file_to_library($tmp_file);
    }

    // 一覧画面に遷移
    //echo("<script type=\"text/javascript\">location.href = '$ret_url';</script>");
    //formに設定した情報を使用して一覧に戻るよう変更 20141128
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    ?>
</body>
<?php

/**
 * 委員会議事録承認
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname ファイル名
 * @param mixed $emp_id 職員ID
 * @param mixed $pjt_schd_id 委員会スケジュールID
 * @return なし
 *
 */
function approve_prcd($con, $fname, $emp_id, $pjt_schd_id)
{
    // 承認日をセット
    $sql = "update prcdaprv set";
    $set = array("prcdaprv_date");
    $setvalue = array(date("Ymd"));
    $cond = "where pjt_schd_id = '$pjt_schd_id' and prcdaprv_emp_id = '$emp_id'";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    // 承認者がすべて承認済みの場合、申請ステータスを「承認済」に変更
    $sql = "update proceeding set";
    $set = array("prcd_status");
    $setvalue = array("2");
    $cond = "where pjt_schd_id = '$pjt_schd_id' and not exists (select * from prcdaprv where prcdaprv.pjt_schd_id = proceeding.pjt_schd_id and prcdaprv.prcdaprv_date is null)";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

/**
 * 残業申請承認
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname ファイル名
 * @param mixed $session セッション
 * @param mixed $atdbk_class 出勤表ワークフロー関連共通クラス
 * @param mixed $apply_id 申請ID
 * @param mixed $apv_order_fix 承認順
 * @param mixed $apv_sub_order_fix 複数承認時の順番
 * @param mixed $approve 承認フラグ 1:承認 2:否認 3:差戻し
 * @return なし
 *
 */
function approve_ovtm($con, $fname, $session, $atdbk_class, $apply_id, $apv_order_fix, $apv_sub_order_fix, $approve)
{

    // 階層ごとの承認者情報取得
    $arr_applyapv_per_hierarchy = $atdbk_class->get_applyapv_per_hierarchy($apply_id, $apv_order_fix, "OVTM");
    // 区分 1:非同期 2:同期 3:権限並列
    $next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

    $apv_comment = "";
    $atdbk_class->approve_application($apply_id, $apv_order_fix, $apv_sub_order_fix, $approve, $apv_comment, $next_notice_div, $session, "OVTM");

    // 申請情報を取得
    $sql = "select apply_status, emp_id, target_date from ovtmapply";
    $cond = "where apply_id = $apply_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $status = pg_fetch_result($sel, 0, "apply_status");
    $apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
    $target_date = pg_fetch_result($sel, 0, "target_date");
    //否認の場合
    if ($status == "2") {

        // 勤務実績情報を取得
        $arr_result = get_atdbkrslt($con, $apply_emp_id, $target_date, $fname);
        $end_time = $arr_result["end_time"];
        $next_day_flag = $arr_result["next_day_flag"];

        // 勤務実績の退勤時刻を終業時刻で上書き
        $office_start_time = $arr_result["office_start_time"];
        $office_end_time = $arr_result["office_end_time"];
        $office_next_day_flag = 0;
        //開始時刻・終業時刻から終業時刻が翌日か判定を行う
        if ($office_start_time > $office_end_time) {
            $office_next_day_flag = 1;
        }
        $sql = "update atdbkrslt set";
        $set = array("end_time", "next_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
        $setvalue = array($office_end_time, $office_next_day_flag, "", "", 0, 0, "", "", 0, 0);
        $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

/**
 * 時間修正申請承認
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname ファイル名
 * @param mixed $session セッション
 * @param mixed $atdbk_class 出勤表ワークフロー関連共通クラス
 * @param mixed $apply_id 申請ID
 * @param mixed $apv_order_fix 承認順
 * @param mixed $apv_sub_order_fix 複数承認時の順番
 * @param mixed $approve 承認フラグ 1:承認 2:否認 3:差戻し
 * @return なし
 *
 */
function approve_tmmd($con, $fname, $session, $atdbk_class, $apply_id, $apv_order_fix, $apv_sub_order_fix, $approve)
{
    // 階層ごとの承認者情報取得
    $arr_applyapv_per_hierarchy = $atdbk_class->get_applyapv_per_hierarchy($apply_id, $apv_order_fix, "TMMD");
    // 区分 1:非同期 2:同期 3:権限並列
    $next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

    $atdbk_class->approve_application($apply_id, $apv_order_fix, $apv_sub_order_fix, $approve, $apv_comment, $next_notice_div, $session, "TMMD");

    // 申請情報を取得
    $sql = "select * from tmmdapply";
    $cond = "where apply_id = $apply_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $apply_status = pg_fetch_result($sel, 0, "apply_status");

    // 否認の場合
    if ($apply_status == "2") {

        $apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
        $target_date = pg_fetch_result($sel, 0, "target_date");
        //一日分の修正の場合
        if (strlen($target_date) == 8) {
            $b_tmcd_group_id = pg_fetch_result($sel, 0, "b_tmcd_group_id");
            $b_pattern = pg_fetch_result($sel, 0, "b_pattern");
            $b_reason = pg_fetch_result($sel, 0, "b_reason");
            $b_night_duty = pg_fetch_result($sel, 0, "b_night_duty");
            $b_allow_id = pg_fetch_result($sel, 0, "b_allow_id");
            $b_meeting_time = pg_fetch_result($sel, 0, "b_meeting_time");
            $b_previous_day_flag = pg_fetch_result($sel, 0, "b_previous_day_flag");
            $b_next_day_flag = pg_fetch_result($sel, 0, "b_next_day_flag");

            $b_start_time = pg_fetch_result($sel, 0, "b_start_time");
            $b_out_time = pg_fetch_result($sel, 0, "b_out_time");
            $b_ret_time = pg_fetch_result($sel, 0, "b_ret_time");
            $b_end_time = pg_fetch_result($sel, 0, "b_end_time");
            $a_end_time = pg_fetch_result($sel, 0, "a_end_time");
            for ($i = 1; $i <= 10; $i++) {
                $b_start_time_ver = "b_o_start_time$i";
                $b_end_time_ver = "b_o_end_time$i";
                $$b_start_time_ver = pg_fetch_result($sel, 0, "$b_start_time_ver");
                $$b_end_time_ver = pg_fetch_result($sel, 0, "$b_end_time_ver");
                $a_start_time_ver = "a_o_start_time$i";
                $$a_start_time_ver = pg_fetch_result($sel, 0, "$a_start_time_ver");
            }
            $b_allow_count = pg_fetch_result($sel, 0, "b_allow_count");
            $b_over_start_time = pg_fetch_result($sel, 0, "b_over_start_time");
            $b_over_end_time = pg_fetch_result($sel, 0, "b_over_end_time");
            $b_over_start_next_day_flag = pg_fetch_result($sel, 0, "b_over_start_next_day_flag");
            $b_over_end_next_day_flag = pg_fetch_result($sel, 0, "b_over_end_next_day_flag");
            $b_over_start_time2 = pg_fetch_result($sel, 0, "b_over_start_time2");
            $b_over_end_time2 = pg_fetch_result($sel, 0, "b_over_end_time2");
            $b_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_start_next_day_flag2");
            $b_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_end_next_day_flag2");

            // 勤務実績を申請前の状態に戻す
            $sql = "update atdbkrslt set";
            $set = array("tmcd_group_id", "pattern", "reason", "night_duty", "allow_id", "start_time", "out_time", "ret_time", "end_time", "o_start_time1", "o_end_time1", "o_start_time2", "o_end_time2", "o_start_time3", "o_end_time3", "o_start_time4", "o_end_time4", "o_start_time5", "o_end_time5", "o_start_time6", "o_end_time6", "o_start_time7", "o_end_time7", "o_start_time8", "o_end_time8", "o_start_time9", "o_end_time9", "o_start_time10", "o_end_time10", "meeting_time", "previous_day_flag", "next_day_flag", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
            $setvalue = array($b_tmcd_group_id, $b_pattern, $b_reason, $b_night_duty, $b_allow_id, $b_start_time, $b_out_time, $b_ret_time, $b_end_time, $b_o_start_time1, $b_o_end_time1, $b_o_start_time2, $b_o_end_time2, $b_o_start_time3, $b_o_end_time3, $b_o_start_time4, $b_o_end_time4, $b_o_start_time5, $b_o_end_time5, $b_o_start_time6, $b_o_end_time6, $b_o_start_time7, $b_o_end_time7, $b_o_start_time8, $b_o_end_time8, $b_o_start_time9, $b_o_end_time9, $b_o_start_time10, $b_o_end_time10, $b_meeting_time, $b_previous_day_flag, $b_next_day_flag, $b_allow_count, $b_over_start_time, $b_over_end_time, $b_over_start_next_day_flag, $b_over_end_next_day_flag, $b_over_start_time2, $b_over_end_time2, $b_over_start_next_day_flag2, $b_over_end_next_day_flag2);
            $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }

            // 退勤時刻が変わる場合は同日の残業申請を削除
            if ($b_end_time != $a_end_time) {

                $sql = "select apply_id from ovtmapply ";
                $cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                while ($row = pg_fetch_array($sel)) {
                    $ovtm_apply_id = $row["apply_id"];

                    // 残業申請承認論理削除
                    $sql = "update ovtmaprv set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $ovtm_apply_id";

                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 残業申請論理削除
                    $sql = "update ovtmapply set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $ovtm_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 承認候補を論理削除
                    $sql = "update ovtmaprvemp set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $ovtm_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 同期・非同期情報を論理削除
                    $sql = "update ovtm_async_recv set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $ovtm_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }

            // 退勤後復帰時刻が変わる場合は同日の退勤後復帰申請を削除
            if ($b_o_start_time1 != $a_o_start_time1 || $b_o_start_time2 != $a_o_start_time2 || $b_o_start_time3 != $a_o_start_time3 || $b_o_start_time4 != $a_o_start_time4 || $b_o_start_time5 != $a_o_start_time5 || $b_o_start_time6 != $a_o_start_time6 || $b_o_start_time7 != $a_o_start_time7 || $b_o_start_time8 != $a_o_start_time8 || $b_o_start_time9 != $a_o_start_time9 || $b_o_start_time10 != $a_o_start_time10) {

                $sql = "select apply_id from rtnapply ";
                $cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
                $sel = select_from_table($con, $sql, $cond, $fname);
                if ($sel == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                while ($row = pg_fetch_array($sel)) {
                    $rtn_apply_id = $row["apply_id"];

                    // 退勤後復帰申請承認論理削除
                    $sql = "update rtnaprv set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $rtn_apply_id";

                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 退勤後復帰申請論理削除
                    $sql = "update rtnapply set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $rtn_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 承認候補を論理削除
                    $sql = "update rtnaprvemp set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $rtn_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    // 同期・非同期情報を論理削除
                    $sql = "update rtn_async_recv set";
                    $set = array("delete_flg");
                    $setvalue = array("t");
                    $cond = "where apply_id = $rtn_apply_id";
                    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                    if ($upd == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }
                }
            }
        }
        //一括修正の場合
        else {
            // 一括修正情報を取得
            $sql = "select * from tmmdapplyall";
            $cond = "where apply_id = $apply_id order by target_date";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
            $num = pg_num_rows($sel);

            for ($i = 0; $i < $num; $i++) {
                $target_date = pg_fetch_result($sel, $i, "target_date");

                $b_tmcd_group_id = pg_fetch_result($sel, $i, "b_tmcd_group_id");
                $b_pattern = pg_fetch_result($sel, $i, "b_pattern");
                $b_reason = pg_fetch_result($sel, $i, "b_reason");
                $b_night_duty = pg_fetch_result($sel, $i, "b_night_duty");
                $b_allow_id = pg_fetch_result($sel, $i, "b_allow_id");

                $b_previous_day_flag = pg_fetch_result($sel, $i, "b_previous_day_flag");
                $b_next_day_flag = pg_fetch_result($sel, $i, "b_next_day_flag");

                $b_start_time = pg_fetch_result($sel, $i, "b_start_time");
                $b_out_time = pg_fetch_result($sel, $i, "b_out_time");
                $b_ret_time = pg_fetch_result($sel, $i, "b_ret_time");
                $b_end_time = pg_fetch_result($sel, $i, "b_end_time");
                $a_end_time = pg_fetch_result($sel, $i, "a_end_time");
                $b_o_start_time1 = pg_fetch_result($sel, $i, "b_o_start_time1");
                $b_o_end_time1 = pg_fetch_result($sel, $i, "b_o_end_time1");
                $a_o_start_time1 = pg_fetch_result($sel, $i, "a_o_start_time1");

                $b_allow_count = pg_fetch_result($sel, $i, "b_allow_count");
                $b_over_start_time = pg_fetch_result($sel, $i, "b_over_start_time");
                $b_over_end_time = pg_fetch_result($sel, $i, "b_over_end_time");
                $b_over_start_next_day_flag = pg_fetch_result($sel, $i, "b_over_start_next_day_flag");
                $b_over_end_next_day_flag = pg_fetch_result($sel, $i, "b_over_end_next_day_flag");
                $b_over_start_time2 = pg_fetch_result($sel, $i, "b_over_start_time2");
                $b_over_end_time2 = pg_fetch_result($sel, $i, "b_over_end_time2");
                $b_over_start_next_day_flag2 = pg_fetch_result($sel, $i, "b_over_start_next_day_flag2");
                $b_over_end_next_day_flag2 = pg_fetch_result($sel, $i, "b_over_end_next_day_flag2");

                // 勤務実績を申請前の状態に戻す
                $sql = "update atdbkrslt set";
                $set = array("tmcd_group_id", "pattern", "reason", "night_duty", "allow_id", "start_time", "out_time", "ret_time", "end_time", "o_start_time1", "o_end_time1", "previous_day_flag", "next_day_flag", "allow_count", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
                $setvalue = array($b_tmcd_group_id, $b_pattern, $b_reason, $b_night_duty, $b_allow_id, $b_start_time, $b_out_time, $b_ret_time, $b_end_time, $b_o_start_time1, $b_o_end_time1, $b_previous_day_flag, $b_next_day_flag, $b_allow_count, $b_over_start_time, $b_over_end_time, $b_over_start_next_day_flag, $b_over_end_next_day_flag, $b_over_start_time2, $b_over_end_time2, $b_over_start_next_day_flag2, $b_over_end_next_day_flag2);
                $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
                $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                if ($upd == 0) {
                    pg_query($con, "rollback");
                    pg_close($con);
                    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                    exit;
                }

                // 退勤時刻が変わる場合は同日の残業申請を削除
                if ($b_end_time != $a_end_time) {

                    $sql = "select apply_id from ovtmapply ";
                    $cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
                    $sel2 = select_from_table($con, $sql, $cond, $fname);
                    if ($sel2 == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    while ($row = pg_fetch_array($sel2)) {
                        $ovtm_apply_id = $row["apply_id"];

                        // 残業申請承認論理削除
                        $sql = "update ovtmaprv set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $ovtm_apply_id";

                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 残業申請論理削除
                        $sql = "update ovtmapply set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $ovtm_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 承認候補を論理削除
                        $sql = "update ovtmaprvemp set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $ovtm_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 同期・非同期情報を論理削除
                        $sql = "update ovtm_async_recv set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $ovtm_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }
                    }
                }

                // 退勤後復帰時刻が変わる場合は同日の退勤後復帰申請を削除
                if ($b_o_start_time1 != $a_o_start_time1) {

                    $sql = "select apply_id from rtnapply ";
                    $cond = "where emp_id = '$apply_emp_id' and target_date = '$target_date' order by apply_id";
                    $sel3 = select_from_table($con, $sql, $cond, $fname);
                    if ($sel3 == 0) {
                        pg_query($con, "rollback");
                        pg_close($con);
                        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                        exit;
                    }

                    while ($row = pg_fetch_array($sel3)) {
                        $rtn_apply_id = $row["apply_id"];

                        // 退勤後復帰申請承認論理削除
                        $sql = "update rtnaprv set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $rtn_apply_id";

                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 退勤後復帰申請論理削除
                        $sql = "update rtnapply set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $rtn_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 承認候補を論理削除
                        $sql = "update rtnaprvemp set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $rtn_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }

                        // 同期・非同期情報を論理削除
                        $sql = "update rtn_async_recv set";
                        $set = array("delete_flg");
                        $setvalue = array("t");
                        $cond = "where apply_id = $rtn_apply_id";
                        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
                        if ($upd == 0) {
                            pg_query($con, "rollback");
                            pg_close($con);
                            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                            exit;
                        }
                    }
                }
            }
        }
    }
}

/**
 * 呼出申請承認
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname ファイル名
 * @param mixed $session セッション
 * @param mixed $atdbk_class 出勤表ワークフロー関連共通クラス
 * @param mixed $apply_id 申請ID
 * @param mixed $apv_order_fix 承認順
 * @param mixed $apv_sub_order_fix 複数承認時の順番
 * @param mixed $approve 承認フラグ 1:承認 2:否認 3:差戻し
 * @return なし
 *
 */
function approve_rtn($con, $fname, $session, $atdbk_class, $apply_id, $apv_order_fix, $apv_sub_order_fix, $approve)
{

    // 階層ごとの承認者情報取得
    $arr_applyapv_per_hierarchy = $atdbk_class->get_applyapv_per_hierarchy($apply_id, $apv_order_fix, "RTN");
    // 区分 1:非同期 2:同期 3:権限並列
    $next_notice_div = $arr_applyapv_per_hierarchy[0]["next_notice_div"];

    $apv_comment = "";
    $atdbk_class->approve_application($apply_id, $apv_order_fix, $apv_sub_order_fix, $approve, $apv_comment, $next_notice_div, $session, "RTN");


    // 申請情報を取得
    $sql = "select apply_status, emp_id, target_date from rtnapply";
    $cond = "where apply_id = $apply_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $apply_status = pg_fetch_result($sel, 0, "apply_status");

    // 否認の場合
    if ($apply_status == "2") {


        $apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
        $target_date = pg_fetch_result($sel, 0, "target_date");

        // 勤務実績情報を取得
        $arr_result = get_atdbkrslt($con, $apply_emp_id, $target_date, $fname);

        // 申請情報を更新
        $sql = "update rtnapply set";
        $set = array();
        $setvalue = array();
        for ($i = 1; $i <= 10; $i++) {
            array_push($set, "o_start_time$i", "o_end_time$i");
            array_push($setvalue, $arr_result["o_start_time$i"], $arr_result["o_end_time$i"]);
        }
        $cond = "where apply_id = $apply_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        // 勤務実績の退勤後復帰・復帰後退勤時刻を初期化
        $sql = "update atdbkrslt set";
        $set = array();
        $setvalue = array();
        for ($i = 1; $i <= 10; $i++) {
            array_push($set, "o_start_time$i", "o_end_time$i");
            array_push($setvalue, null, null);
        }
        $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

/**
 * 勤務表月次承認
 *
 * @param mixed $emp_id ログインしている職員のID
 * @param mixed $apply_id 申請ID
 * @return なし
 *
 */
function approve_oph($emp_id, $apply_id)
{
    require_once 'Cmx/Model/Shift/Approval.php';
    $appr = new Cmx_Shift_Approval();

    list($group_id, $year, $month) = $appr->fetch_approval_by_id($apply_id);
    $appr->boss_approval_group($group_id, $year, $month, $emp_id);
}

function format_apply_time($datetime)
{
    return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $datetime);
}

// ----- メディアテック Start -----
// モジュール・クラスの存在チェック
function module_check($module, $class)
{
    if (file_exists(WORKFLOW_KINTAI_DIR . "/" . $module)) {
        require_once WORKFLOW_KINTAI_DIR . "/" . $module;
        return class_exists($class);
    }
    return false;
}

// ----- メディアテック End -----
