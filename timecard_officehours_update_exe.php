<?

require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
for ($i = 1; $i <= 5; $i++) {
    for ($j = 1; $j <= 3; $j++) {
        $hour_var_name = "start_hour{$j}_{$i}";
        $min_var_name = "start_min{$j}_{$i}";
        if ($$hour_var_name == "") {
            $$hour_var_name = "--";
        }
        if ($$min_var_name == "") {
            $$min_var_name = "--";
        }
        if (($$hour_var_name == "--" && $$min_var_name != "--") || ($$hour_var_name != "--" && $$min_var_name == "--")) {
            echo("<script type=\"text/javascript\">alert('時と分の一方のみは登録できません。');</script>");
            echo("<script language=\"javascript\">history.back();</script>");
            exit;
        }
        $start_time = "{$$hour_var_name}:{$$min_var_name}";

        $hour_var_name = "end_hour{$j}_{$i}";
        $min_var_name = "end_min{$j}_{$i}";
        if ($$hour_var_name == "") {
            $$hour_var_name = "--";
        }
        if ($$min_var_name == "") {
            $$min_var_name = "--";
        }
        if (($$hour_var_name == "--" && $$min_var_name != "--") || ($$hour_var_name != "--" && $$min_var_name == "--")) {
            echo("<script type=\"text/javascript\">alert('時と分の一方のみは登録できません。');</script>");
            echo("<script language=\"javascript\">history.back();</script>");
            exit;
        }
        $end_time = "{$$hour_var_name}:{$$min_var_name}";

        if (($start_time == "--:--" && $end_time != "--:--") || ($start_time != "--:--" && $end_time == "--:--")) {
            echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻の一方のみは登録できません。');</script>");
            echo("<script language=\"javascript\">history.back();</script>");
            exit;
        }
    }
}

// 所定労働未入力チェック（時間勤務を除く）
if ($time_work_flg != "t" && ($start_hour1_2 == "--" && $start_hour2_2 == "--" && $start_hour3_2 == "--")) {
    echo("<script type=\"text/javascript\">alert('所定労働は必ず登録してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}

// 時間帯データを配列に格納
for ($i = 1; $i <= 3; $i++) {
    for ($j = 1; $j <= 5; $j++) {
        $arr_var_name = "times{$i}_{$j}";
        $$arr_var_name = array();

        $var_name = "start_hour{$i}_{$j}";
        if ($$var_name != "--") {
            $tmp_start_hour = $$var_name;
            $var_name = "start_min{$i}_{$j}";
            $tmp_start_min = $$var_name;
            $var_name = "end_hour{$i}_{$j}";
            $tmp_end_hour = $$var_name;
            $var_name = "end_min{$i}_{$j}";
            $tmp_end_min = $$var_name;

            // From〜Toが正順の場合
            if ($tmp_start_hour < $tmp_end_hour || ($tmp_start_hour == $tmp_end_hour && $tmp_start_min < $tmp_end_min)) {

                $tmp_time = "{$tmp_start_hour}:{$tmp_start_min}";
                $end_time = "{$tmp_end_hour}:{$tmp_end_min}";
                while ($tmp_time < $end_time) {
                    array_push($$arr_var_name, $tmp_time);
                    list($tmp_hour, $tmp_min) = split(":", $tmp_time);
                    $tmp_min += 1;
                    if ($tmp_min == 60) {
                        $tmp_hour++;
                        $tmp_min = 0;
                    }
                    $tmp_time = sprintf("%02d:%02d", $tmp_hour, $tmp_min);
                }

                // From〜Toが逆順の場合
            }
            else if ($tmp_start_hour > $tmp_end_hour || ($tmp_start_hour == $tmp_end_hour && $tmp_start_min > $tmp_end_min)) {

                $tmp_time = "{$tmp_start_hour}:{$tmp_start_min}";
                $end_time = "23:59";
                while ($tmp_time <= $end_time) {
                    array_push($$arr_var_name, $tmp_time);
                    list($tmp_hour, $tmp_min) = split(":", $tmp_time);
                    $tmp_min += 1;
                    if ($tmp_min == 60) {
                        $tmp_hour++;
                        $tmp_min = 0;
                    }
                    $tmp_time = sprintf("%02d:%02d", $tmp_hour, $tmp_min);
                }

                $tmp_time = "00:00";
                $end_time = "{$tmp_end_hour}:{$tmp_end_min}";
                while ($tmp_time < $end_time) {
                    array_push($$arr_var_name, $tmp_time);
                    list($tmp_hour, $tmp_min) = split(":", $tmp_time);
                    $tmp_min += 1;
                    if ($tmp_min == 60) {
                        $tmp_hour++;
                        $tmp_min = 0;
                    }
                    $tmp_time = sprintf("%02d:%02d", $tmp_hour, $tmp_min);
                }

                // From〜Toが同じ場合
            }
            else {
                if ($over_24hour_flag != "1") {
                    echo("<script type=\"text/javascript\">alert('開始時刻と終了時刻が同じです。');</script>");
                    echo("<script language=\"javascript\">history.back();</script>");
                    exit;
                }
            }
        }
    }
}

// 関連チェック
//// 休憩が所定労働に含まれること
//「終了時刻は、開始時刻から２４時間を超えている」場合は、含まれるチェックは行わない  20100203
if ($over_24hour_flag != "1") {
    for ($i = 1; $i <= 3; $i++) {
        $arr1_var_name = "times{$i}_2";
        $arr2_var_name = "times{$i}_4";
        foreach ($$arr2_var_name as $tmp_time) {
            if (!in_array($tmp_time, $$arr1_var_name)) {
                echo("<script type=\"text/javascript\">alert('休憩は所定労働内で指定してください。');</script>");
                echo("<script language=\"javascript\">history.back();</script>");
                exit;
            }
        }
    }
}
//// 休憩終了時刻≠所定労働終了時刻であること
for ($i = 1; $i <= 3; $i++) {
    $var_name = "end_hour{$i}_4";
    $end_hour1 = $$var_name;
    if ($end_hour1 != "--") {
        $var_name = "end_min{$i}_4";
        $end_min1 = $$var_name;
        $var_name = "end_hour{$i}_2";
        $end_hour2 = $$var_name;
        $var_name = "end_min{$i}_2";
        $end_min2 = $$var_name;
        if ($end_hour1 == $end_hour2 && $end_min1 == $end_min2) {
            echo("<script type=\"text/javascript\">alert('所定労働終了時刻と休憩終了時刻が同じです。');</script>");
            echo("<script language=\"javascript\">history.back();</script>");
            exit;
        }
    }
}
//法定外残業の基準時間
if (($base_hour == "--" && $base_min != "--") || ($base_hour != "--" && $base_min == "--")) {
    echo("<script type=\"text/javascript\">alert('法定外残業の計算の時と分の一方のみは登録できません。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}
$base_time = "{$base_hour}{$base_min}";

//手当て数値チェック
if ($allowance != "") {
    if (!preg_match("/^[0-9]*$/", $allowance)) {
        echo("<script type=\"text/javascript\">alert('手当金額は数値を入力してください。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
}

//稼動時間チェック
for ($i = 1; $i <= 3; $i++) {
    $workmins = "workminutes{$i}";
    //数値チェック
    if (!preg_match("/^[0-9]*$/", $$workmins)) {
        echo("<script type=\"text/javascript\">alert('稼動時間は数値を入力してください。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    //桁数チェック
    if (mb_strlen($$workmins) > 4) {
        echo("<script type=\"text/javascript\">alert('稼動時間の上限は9999分です。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
}

//夜勤時間チェック
for ($i = 1; $i <= 3; $i++) {
    $nightduty_mins = "nightduty_minutes{$i}";
    //数値チェック
    if (!preg_match("/^[0-9]*$/", $$nightduty_mins)) {
        echo("<script type=\"text/javascript\">alert('夜勤時間は数値を入力してください。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    //桁数チェック
    if (mb_strlen($$nightduty_mins) > 4) {
        echo("<script type=\"text/javascript\">alert('夜勤時間の上限は9999分です。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
}

//会議・研修時間
if (($meeting_start_hour == "--" && $meeting_start_min != "--") || ($meeting_start_hour != "--" && $meeting_start_min == "--") || ($meeting_end_hour != "--" && $meeting_end_min == "--") || ($meeting_end_hour == "--" && $meeting_end_min != "--")) {
    echo("<script type=\"text/javascript\">alert('会議・研修・病棟外勤務の時と分の一方のみは登録できません。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}
$meeting_start_time = "{$meeting_start_hour}{$meeting_start_min}";
$meeting_end_time = "{$meeting_end_hour}{$meeting_end_min}";
if (($meeting_start_time == "----" && $meeting_end_time != "----") || ($meeting_start_time != "----" && $meeting_end_time == "----")) {
    echo("<script type=\"text/javascript\">alert('会議・研修・病棟外勤務の開始時刻と終了時刻の一方のみは登録できません。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
}

//応援勤務時間
$assist_start_time = "{$assist_start_hour}{$assist_start_min}";
$assist_end_time = "{$assist_end_hour}{$assist_end_min}";
//応援勤務
if ($assist_time_flag != "") {
    if ($assist_group_id == "" || $assist_start_time == "----" || $assist_end_time == "----") {
        echo("<script type=\"text/javascript\">alert('応援勤務開始時間を含む設定の場合は、応援先シフトグループと開始時刻と終了時刻も設定してください。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
    if (($assist_start_hour == "--" && $assist_start_min != "--") || ($assist_start_hour != "--" && $assist_start_min == "--") || ($assist_end_hour != "--" && $assist_end_min == "--") || ($assist_end_hour == "--" && $assist_end_min != "--")) {
        echo("<script type=\"text/javascript\">alert('応援勤務の時と分の一方のみは登録できません。');</script>");
        echo("<script language=\"javascript\">history.back();</script>");
        exit;
    }
}
// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// 勤務時間帯レコードを削除
$sql = "delete from officehours";
$cond = "where tmcd_group_id = $group_id and pattern = '$pattern'";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 勤務時間帯レコードを作成
for ($i = 1; $i <= 3; $i++) {
    $sql = "insert into officehours (tmcd_group_id, pattern, officehours_id, officehours1_start, officehours1_end, officehours2_start, officehours2_end, officehours3_start, officehours3_end, officehours4_start, officehours4_end, officehours5_start, officehours5_end, workminutes, nightduty_minutes) values (";
    $content = array($group_id, $pattern, $i);
    for ($j = 1; $j <= 5; $j++) {
        $hour_var_name = "start_hour{$i}_$j";
        $min_var_name = "start_min{$i}_$j";
        if ($$hour_var_name != "--" && $$min_var_name != "--") {
            array_push($content, "{$$hour_var_name}:{$$min_var_name}");
        }
        else {
            array_push($content, "");
        }

        $hour_var_name = "end_hour{$i}_$j";
        $min_var_name = "end_min{$i}_$j";
        if ($$hour_var_name != "--" && $$min_var_name != "--") {
            array_push($content, "{$$hour_var_name}:{$$min_var_name}");
        }
        else {
            array_push($content, "");
        }
    }
    //array_push($content, $pattern);
    //稼動時間
    $workmins = "workminutes{$i}";
    array_push($content, "{$$workmins}");

    //夜勤時間
    $nightduty_mins = "nightduty_minutes{$i}";
    array_push($content, "{$$nightduty_mins}");

    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

//勤務日数換算を更新
if ($reason == "" || $reason == "--") {
    $reason = null;
}
if ($allowance == "") {
    $allowance = null;
}
if ($meeting_start_time == "----") {
    $meeting_start_time = "";
}
if ($meeting_end_time == "----") {
    $meeting_end_time = "";
}
if ($assist_start_time == "----") {
    $assist_start_time = "";
}
if ($assist_end_time == "----") {
    $assist_end_time = "";
}
if ($base_time == "----") {
    $base_time = "";
}
//※変更時はtimecard_name_pattern_update_exe.phpのatdptnのselect、insert部分も改修すること。出勤パターン名画面で更新すると、追加項目がクリアされるため。
$sql = "update atdptn set";
$set = array("workday_count", "previous_day_possible_flag", "reason", "allowance", "previous_day_flag", "meeting_start_time", "meeting_end_time", "assist_time_flag", "assist_group_id", "assist_start_time", "assist_end_time", "base_time", "over_24hour_flag", "auto_clock_in_flag", "auto_clock_out_flag", "out_time_nosubtract_flag", "after_night_duty_flag", "duty_connection_flag", "empcond_officehours_flag", "no_count_flag", "previous_day_threshold_hour");

$setvalue = array($workday_count, $previous_day_possible_flag, $reason, $allowance, $previous_day_flag, $meeting_start_time, $meeting_end_time, $assist_time_flag, $assist_group_id, $assist_start_time, $assist_end_time, $base_time, $over_24hour_flag, $auto_clock_in_flag, $auto_clock_out_flag, $out_time_nosubtract_flag, $after_night_duty_flag, $duty_connection_flag, $empcond_officehours_flag, $no_count_flag, $previous_day_threshold_hour);

$cond = "where group_id = '$group_id' and atdptn_id = '$pattern'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 勤務時間帯画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_officehours.php?session=$session&group_id=$group_id&pattern=$pattern';</script>");
