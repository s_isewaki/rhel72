<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$summary = check_authority($session, 48, $fname);
if ($summary == "0") {
	showLoginPage();
	exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}




//==============================
//患者影響レベル情報取得
//==============================
$rep_obj = new hiyari_report_class($con, $fname);
$level_infos = $rep_obj->get_inci_level_infos($inci_lvl);



//==============================
//設定情報の更新
//==============================
if($is_postback == "true")
{
	//==============================
	//トランザクション開始
	//==============================
	pg_query($con,"begin transaction");
	
	//==============================
	//任意項目設定の更新
	//==============================
	
	//デリート
	$sql  = "delete from inci_hiyari_org_item_setting";
	$cond = "";
	$result = delete_from_table($con, $sql, $cond,  $fname);
	if ($result == 0){
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	//インサート
	for($org_item_no = 1; $org_item_no <= 10; $org_item_no++)
	{
		//送信パラメータの解析
		$param_input_type = "input_type_".$org_item_no;
		$param_auto_input_item = "auto_input_item_".$org_item_no;
		$param_must_flg = "must_flg_".$org_item_no;
		$input_type = $$param_input_type;
		$auto_input_item = $$param_auto_input_item;
		$must_flg = $$param_must_flg;
		if($must_flg != "t")
		{
			$must_flg = "f";
		}
		
		//設定方法が自動引用(1)のときだけ自動引用項目が有効
		if($input_type != 1 )
		{
			$auto_input_item = null;
		}
		//設定方法が入力しない(0)以外のときだけ必須指定の入力が有効
		if($input_type == 0 )
		{
			$must_flg = "f";
		}
		
		
		$sql = "insert into inci_hiyari_org_item_setting values(";
		$registry_data = array(
			$org_item_no,$input_type,$auto_input_item,$must_flg
		);
		$result = insert_into_table($con, $sql, $registry_data, $fname);
		if ($result == 0) {
			pg_query($con,"rollback");
			pg_close($db_con);
			showErrorPage();
			exit;
		}
	}
	
	//==============================
	//患者影響レベル設定の更新
	//==============================
	
	//デリート
	$sql  = "delete from inci_hiyari_folder_incident_level";
	$cond = "";
	$result = delete_from_table($con, $sql, $cond,  $fname);
	if ($result == 0){
		pg_query($con,"rollback");
		pg_close($con);
		showErrorPage();
		exit;
	}
	
	//インサート
	foreach($use_level as $use_level1)
	{
		$sql = "insert into inci_hiyari_folder_incident_level values(";
		$registry_data = array(
			$use_level1
		);
		$result = insert_into_table($con, $sql, $registry_data, $fname);
		if ($result == 0) {
			pg_query($con,"rollback");
			pg_close($db_con);
			showErrorPage();
			exit;
		}
	}

	//==============================
	//コミット
	//==============================
	pg_query($con, "commit");
}




//==============================
//患者影響レベル設定情報取得
//==============================
$sql  = " select * from inci_hiyari_folder_incident_level";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0){
	pg_query($con,"rollback");
	pg_close($con);
	showErrorPage();
	exit;
}
$level_setting_info = pg_fetch_all($sel);
$use_level = "";
foreach($level_setting_info as $level_setting_info1)
{
	$use_level[] = $level_setting_info1['target_level'];
}



//==============================
//任意項目設定情報取得
//==============================

$sql  = " select * from inci_hiyari_org_item_setting order by org_item_no";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0){
	pg_query($con,"rollback");
	pg_close($con);
	showErrorPage();
	exit;
}
$org_item_setting_info = pg_fetch_all($sel);





//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | ヒヤリ・ハット出力設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function load_action()
{
	org_item_disp_update_all();
}
function org_item_disp_update_all()
{
	for(var i = 1; i <= 10; i++)
	{
		org_item_disp_update(i)
	}
}
function org_item_disp_update(no)
{
	//設定方法の取得
	var input_type_obj_name = "input_type_" + no;
	var input_type_objs = document.getElementsByName(input_type_obj_name);
	var input_type_value = "";
	for(var i = 0; i < input_type_objs.length; i++)
	{
		if(input_type_objs[i].checked)
		{
			input_type_value = input_type_objs[i].value;
		}
	}
	
	//オブジェクトの取得
	var auto_input_item_obj_name = "auto_input_item_" + no;
	var auto_input_item_objs = document.getElementsByName(auto_input_item_obj_name);
	var auto_input_item_obj = auto_input_item_objs[0];
	var must_flg_obj_name = "must_flg_" + no;
	var must_flg_objs = document.getElementsByName(must_flg_obj_name);
	var must_flg_obj = must_flg_objs[0];
	
	
	//設定方法が自動引用(1)のときだけ自動引用項目が入力可能
	if(input_type_value == 1)
	{
		auto_input_item_obj.disabled = false;
	}
	else
	{
		auto_input_item_obj.disabled = true;
	}
	
	//設定方法が入力しない(0)以外のときだけ必須指定の入力が可能
	if(input_type_value != 0)
	{
		must_flg_obj.disabled = false;
	}
	else
	{
		must_flg_obj.checked = false;
		must_flg_obj.disabled = true;
	}
	
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="load_action()">
<form name="mainform" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">

<!-- BODY全体 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table border="0" cellspacing="0" cellpadding="0" width="600">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->


<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>対象とする患者影響レベル</B></font>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1">


	<!-- 患者影響レベル START -->
	<tr>
		<td bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
<?
$i = 0;
foreach ($level_infos as $level_info)
{
		if($level_info["use_flg"])
		{
			$code = $level_info["easy_code"];
			$name = $level_info["easy_name"];


			$i++;
			if($i !=1 && $i%4 == 1)
			{
?>
				</tr>
				<tr>
<?
			}
?>
					<td width="25%">
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					<nobr>
					<input type="checkbox" name="use_level[]" value="<?=$code?>" <? if(in_array($code,$use_level)){?>checked<?} ?> ><?=h($name)?>
					</nobr>
					</font>
					</td>
<?
		}
}
?>
				</tr>
				<tr>
<?
$code = "LVn"; //未設定 n for null
$name = "未設定"
?>
					<td width="25%">
					<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
					<nobr>
					<input type="checkbox" name="use_level[]" value="<?=$code?>" <? if(in_array($code,$use_level)){?>checked<?} ?> ><?=h($name)?>
					</nobr>
					</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- 患者影響レベル END -->

</table>


<img src="img/spacer.gif" width="1" height="10" alt=""><br>


<table border="0" cellspacing="1" cellpadding="0" class="list">
<tr>
<td width="200" bgcolor="#DFFFDC" align="center" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><B>任意項目の設定</B></font>
</td>
</tr>
</table>



<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
				<tr>
					<td width="50" bgcolor="#DFFFDC" align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						番号
						</font>
					</td>
					<td            bgcolor="#DFFFDC" align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						設定方法
						</font>
					</td>
					<td width="150" bgcolor="#DFFFDC" align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						自動引用
						</font>
					</td>
					<td width="50" bgcolor="#DFFFDC" align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						必須
						</font>
					</td>
				</tr>
				<tr>
<?
$i = 0;
foreach ($org_item_setting_info as $org_item_setting_info1)
{
	$org_item_no = $org_item_setting_info1["org_item_no"];
	$input_type = $org_item_setting_info1["input_type"];
	$auto_input_item = $org_item_setting_info1["auto_input_item"];
	$must_flg = $org_item_setting_info1["must_flg"];
	
	if($org_item_no != 1)
	{
?>
				</tr>
				<tr>
<?
	}
?>
					<td align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<?=$org_item_no?>
						</font>
					</td>
					<td align="left" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input type="radio" name="input_type_<?=$org_item_no?>" value="0" <? if($input_type == 0){?>checked<?} ?> onclick="org_item_disp_update(<?=$org_item_no?>)" >入力しない
						&nbsp
						<input type="radio" name="input_type_<?=$org_item_no?>" value="1" <? if($input_type == 1){?>checked<?} ?> onclick="org_item_disp_update(<?=$org_item_no?>)" >自動引用
						&nbsp
						<input type="radio" name="input_type_<?=$org_item_no?>" value="2" <? if($input_type == 2){?>checked<?} ?> onclick="org_item_disp_update(<?=$org_item_no?>)" >フリー入力
						</font>
					</td>
					<td align="left" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<select name="auto_input_item_<?=$org_item_no?>">
							<option value="report_class"   <? if($auto_input_item == "report_class"  ){?>selected<?} ?>>報告部署</option>
							<option value="touzisha_class" <? if($auto_input_item == "touzisha_class"){?>selected<?} ?>>当事者所属部署</option>
							<option value="incident_month" <? if($auto_input_item == "incident_month"){?>selected<?} ?>>発生月</option>
						</select>
						</font>
					</td>
					<td align="center" nowrap>
						<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
						<input type="checkbox" name="must_flg_<?=$org_item_no?>" value="t" <? if($must_flg == "t"){?>checked<?} ?> >
						</font>
					</td>
<?
}
?>
				</tr>
			</table>
		</td>
	</tr>
</table>





<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="submit" value="更新">
</td>
</tr>
</table>

<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->

</tr>
</table>
</form>
</body>
</html>


<?
//==============================
//DBコネクション終了
//==============================
pg_close($con);
?>
