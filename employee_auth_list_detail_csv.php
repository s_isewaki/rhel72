<?
ob_start();

require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;
$detail = $_GET['detail'];
$detail = mb_convert_encoding($detail,'sjis-win', 'auto');
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 18, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職員一覧をCSV形式で取得
$csv = get_emp_list_csv($con, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $view);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = $detail.".csv";

ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 情報をCSV形式で取得
function get_emp_list_csv($con, $tool_nm, $group_id, $class_cnt, $class_cond, $atrb_cond, $dept_cond, $room_cond, $job_cond, $emp_o, $view){

	// 組織情報を取得
	require_once("show_class_name.ini");
	$arr_class_name = get_class_name_array($con, $fname);
	$class_cnt = $arr_class_name["class_cnt"];
	$del_flg = ($view == "1") ? "f" : "t";

	// ヘッダ行を設定
	$buf = "職員ID,職員名,所属,職種,役職,権限グループ\r\n";

	// 出力するための情報を出力
	$sql = "select empmst.emp_personal_id,emp_lt_nm,emp_ft_nm,classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,classroom.room_nm,stmst.st_nm,jobmst.job_nm,authmst.group_id,authgroup.group_nm from empmst";
	$sql .= " left join classmst on classmst.class_id = empmst.emp_class";
	$sql .= " left join atrbmst on atrbmst.atrb_id = empmst.emp_attribute";
	$sql .= " left join deptmst on deptmst.dept_id = empmst.emp_dept";
	$sql .= " left join classroom on classroom.room_id = empmst.emp_room";
	$sql .= " left join stmst on stmst.st_id = empmst.emp_st";
	$sql .= " left join jobmst on jobmst.job_id = empmst.emp_job";
	$sql .= " left join authmst on authmst.emp_id = empmst.emp_id";
	$sql .= " left join authgroup on authmst.group_id = authgroup.group_id";
	$cond = "where authmst.emp_del_flg = '{$del_flg}'";

	if($tool_nm == "shift_staff" || $tool_nm == "shift2_staff") {
		$cond .= " and exists(select * from duty_shift_staff where duty_shift_staff.emp_id = empmst.emp_id)";
	}
	else if($group_id == "") {
		$cond .= " and (authmst.emp_".$tool_nm."_flg = 't' or authgroup.".$tool_nm."_flg = 't')";
	}
	else {
		$cond .= " and (authgroup.group_id='$group_id')";
	}

	// 各階層を判定
	if ($class_cond != 0) {
		$cond .= " and (empmst.emp_class=$class_cond)";
	}
	if ($atrb_cond != 0) {
		$cond .= " and (empmst.emp_attribute=$atrb_cond)";
	}
	if ($dept_cond != 0) {
		$cond .= " and (empmst.emp_dept=$dept_cond)";
	}
	if ($room_cond != 0) {
		$cond .= " and (empmst.emp_room=$room_cond)";
	}
	if ($job_cond != 0) {
		$cond .= " and emp_job = $job_cond";
	}
	if ($emp_o == "1") {
		$cond .= " order by emp_kn_lt_nm,emp_kn_ft_nm";
	} else if ($emp_o == "2") {
		$cond .= " order by emp_kn_lt_nm desc,emp_kn_ft_nm desc";
	} else if ($emp_o == "3") {
		$cond .= " order by classmst.order_no,atrbmst.order_no,deptmst.order_no,classroom.order_no";
	} else if ($emp_o == "4") {
		$cond .= " order by classmst.order_no desc,atrbmst.order_no desc,deptmst.order_no desc,classroom.order_no desc";
	} else if ($emp_o == "5") {
		$cond .= " order by job_id";
	} else if ($emp_o == "6") {
		$cond .= " order by job_id desc";
	} else if ($emp_o == "7") {
		$cond .= " order by st_id";
	} else if ($emp_o == "8") {
		$cond .= " order by st_id desc";
	} else if ($emp_o == "9") {
		$cond .= " order by group_id";
	} else {
		$cond .= " order by group_id desc";
	}

	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// ボディ行を設定
	while ($row = pg_fetch_array($sel)) {
		$buf .= $row['emp_personal_id'].",";
		$buf .= $row["emp_lt_nm"]." ".$row["emp_ft_nm"].",";
		$buf .= $row["class_nm"]."＞".$row["atrb_nm"]."＞".$row["dept_nm"];
		if ($class_cnt == 4) {
			$buf .= "＞".$row["room_nm"];
		}
		$buf .= ",".$row["job_nm"].",";
		$buf .= $row["st_nm"].",";
		$buf .= $row["group_nm"];
		$buf .= "\r\n";
	}

	// Shift_JISに変換
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");
}
?>
