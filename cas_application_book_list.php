<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cas_application_workflow_common_class.php");
require_once("cas_common.ini");
require_once("get_cas_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cas_application_workflow_common_class($con, $fname);


//====================================
// 初期処理
//====================================
// ログインユーザの職員IDを取得

$arr_empmst = $obj->get_empmst($session);
$emp_id = $arr_empmst[0]["emp_id"];

$arr_recognize_schedule = $obj->get_cas_recognize_schedule_for_appliction_book_list($level, $emp_id, $kanri_no, (($approved == "t") ? "approved" : "default"));

$cas_title = get_cas_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cas_title?>｜志願書申請番号選択</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">


function set_application_book_no(apply_id, apply_no, date_y, date_m, date_d) {
    if(opener.document.getElementById('application_book_no')){opener.document.getElementById('application_book_no').value = apply_no;}
    if(opener.document.getElementById('application_book_apply_id')){opener.document.getElementById('application_book_apply_id').value = apply_id;}
    if(opener.document.getElementById('date_y')){opener.document.getElementById('date_y').value = date_y;}
    if(opener.document.getElementById('date_m')){opener.document.getElementById('date_m').value = date_m;}
    if(opener.document.getElementById('date_d')){opener.document.getElementById('date_d').value = date_d;}

    opener.reload_apply_page();
    self.close();
}

function showApprovedList() {
	location.href = 'cas_application_book_list.php?session=<? echo($session); ?>&level=<? echo($level); ?>&kanri_no=<? echo($kanri_no); ?>&approved=t';
}

function showDefaultList() {
	location.href = 'cas_application_book_list.php?session=<? echo($session); ?>&level=<? echo($level); ?>&kanri_no=<? echo($kanri_no); ?>';
}
</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" id="kanri_no" name="kanri_no" value="<?=$kanri_no?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>志願書申請番号選択</b></font></td>
<td align="right" style="padding-right:10px;">
<?php if ($approved != "t") { ?>
<input type="button" value="評価済み志願書表示" onclick="showApprovedList();">
<?php } else { ?>
<input type="button" value="未評価志願書表示" onclick="showDefaultList();">
<?php } ?>
</td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<?php if(count($arr_recognize_schedule) == 0) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願書を申請してください。</font>
</td>
</tr>
</table>
<?php } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="list">
<tr bgcolor="#E5F6CD" align="center">
<td width="80">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">選択</font>
</td>
<td width="120">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願日</font>
</td>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請番号</font>
</td>
</tr>

<?php
foreach($arr_recognize_schedule as $schedule) {
    $apply_id = $schedule["apply_id"];
    $level = $schedule["level"];
    $apply_date = $schedule["apply_date"];
    $apply_no   = $schedule["apply_no"];
    $short_wkfw_name = $schedule["short_wkfw_name"];

	// 申請番号
    $year = substr($apply_date, 0, 4);
    $md   = substr($apply_date, 4, 4);
    if($md >= "0101" and $md <= "0331")
    {
        $year = $year - 1;
    }
    $apply_no = $short_wkfw_name."-".$year."".sprintf("%04d", $apply_no);

    // 評価開始日
    $date = array();
    $style = array("c208", "c216", "g210", "g221", "g231", "g242");
    if (in_array($kanri_no, $style)) {
        $date = $obj->get_date_ymd2($schedule["apply_content"]);
    }

    // 志願日
    $apply_date = $obj->get_apply_date($schedule["apply_content"]);

?>
<tr>
<td align="center">
<input type="button" value="選択" onclick="set_application_book_no('<?=$apply_id?>', '<?=$apply_no?>', '<?=$date["date_y"]?>', '<?=$date["date_m"]?>', '<?=$date["date_d"]?>')">
</td>
<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$apply_date?></font>
</td>

<td align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?=$apply_no?>
</font>
</td>
</tr>

<?php
$arr_grader = $obj->get_apply_grader($schedule["apply_content"]);
$grader_name_1   = $arr_grader[0]["grader_name_1"];
$grader_emp_id_1 = $arr_grader[0]["grader_emp_id_1"];
$grader_name_2   = $arr_grader[0]["grader_name_2"];
$grader_emp_id_2 = $arr_grader[0]["grader_emp_id_2"];
$grader_name_3   = $arr_grader[0]["grader_name_3"];
$grader_emp_id_3 = $arr_grader[0]["grader_emp_id_3"];
$grader_name_4   = $arr_grader[0]["grader_name_4"];
$grader_emp_id_4 = $arr_grader[0]["grader_emp_id_4"];
$grader_name_5   = $arr_grader[0]["grader_name_5"];
$grader_emp_id_5 = $arr_grader[0]["grader_emp_id_5"];
$grader_name_6   = $arr_grader[0]["grader_name_6"];
$grader_emp_id_6 = $arr_grader[0]["grader_emp_id_6"];
// 2011/12/2 Yamagawa add(s)
$grader_name_3_1   = $arr_grader[0]["grader_name_3_1"];
$grader_emp_id_3_1 = $arr_grader[0]["grader_emp_id_3_1"];
$grader_name_3_2   = $arr_grader[0]["grader_name_3_2"];
$grader_emp_id_3_2 = $arr_grader[0]["grader_emp_id_3_2"];
$grader_name_5_2   = $arr_grader[0]["grader_name_5_2"];
$grader_emp_id_5_2 = $arr_grader[0]["grader_emp_id_5_2"];
$grader_name_6_2   = $arr_grader[0]["grader_name_6_2"];
$grader_emp_id_6_2 = $arr_grader[0]["grader_emp_id_6_2"];
// 2011/12/2 Yamagawa add(e)

$arr_empmst_detail1 = $obj->get_empmst_detail($grader_emp_id_1);
$arr_empmst_detail2 = $obj->get_empmst_detail($grader_emp_id_2);
$arr_empmst_detail3 = $obj->get_empmst_detail($grader_emp_id_3);
$arr_empmst_detail4 = $obj->get_empmst_detail($grader_emp_id_4);
$arr_empmst_detail5 = $obj->get_empmst_detail($grader_emp_id_5);
$arr_empmst_detail6 = $obj->get_empmst_detail($grader_emp_id_6);
// 2011/12/2 Yamagawa add(s)
$arr_empmst_detail3_1 = $obj->get_empmst_detail($grader_emp_id_3_1);
$arr_empmst_detail3_2 = $obj->get_empmst_detail($grader_emp_id_3_2);
$arr_empmst_detail5_2 = $obj->get_empmst_detail($grader_emp_id_5_2);
$arr_empmst_detail6_2 = $obj->get_empmst_detail($grader_emp_id_6_2);
// 2011/12/2 Yamagawa add(e)

?>
<input type="hidden" id="<?=$apply_id?>_grader_name_1" value="<?=$grader_name_1?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_1" value="<?=$grader_emp_id_1?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_2" value="<?=$grader_name_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_2" value="<?=$grader_emp_id_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_3" value="<?=$grader_name_3?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_3" value="<?=$grader_emp_id_3?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_4" value="<?=$grader_name_4?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_4" value="<?=$grader_emp_id_4?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_5" value="<?=$grader_name_5?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_5" value="<?=$grader_emp_id_5?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_6" value="<?=$grader_name_6?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_6" value="<?=$grader_emp_id_6?>">
<!-- 2011/12/2 Yamagawa add(s) -->
<input type="hidden" id="<?=$apply_id?>_grader_name_3_1" value="<?=$grader_name_3_1?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_3_1" value="<?=$grader_emp_id_3_1?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_3_2" value="<?=$grader_name_3_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_3_2" value="<?=$grader_emp_id_3_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_5_2" value="<?=$grader_name_5_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_5_2" value="<?=$grader_emp_id_5_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_name_6_2" value="<?=$grader_name_6_2?>">
<input type="hidden" id="<?=$apply_id?>_grader_emp_id_6_2" value="<?=$grader_emp_id_6_2?>">
<!-- 2011/12/2 Yamagawa add(e) -->

<input type="hidden" id="<?=$apply_id?>_grader_st_nm_1" value="<?=$arr_empmst_detail1[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_2" value="<?=$arr_empmst_detail2[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_3" value="<?=$arr_empmst_detail3[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_4" value="<?=$arr_empmst_detail4[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_5" value="<?=$arr_empmst_detail5[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_6" value="<?=$arr_empmst_detail6[0]['st_nm']?>">
<!-- 2011/12/2 Yamagawa add(s) -->
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_3_1" value="<?=$arr_empmst_detail3_1[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_3_2" value="<?=$arr_empmst_detail3_2[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_5_2" value="<?=$arr_empmst_detail5_2[0]['st_nm']?>">
<input type="hidden" id="<?=$apply_id?>_grader_st_nm_6_2" value="<?=$arr_empmst_detail6_2[0]['st_nm']?>">
<!-- 2011/12/2 Yamagawa add(e) -->
<?php } ?>
</table>
<?php } ?>
<center>
</body>
</form>
</html>
<?php pg_close($con);