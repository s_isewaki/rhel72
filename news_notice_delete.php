<?

require_once("Cmx.php");
require_once("Cmx/Model/SystemConfig.php");
require_once("aclg_set.php");
require_once("about_comedix.php");

$fname = $_SERVER['PHP_SELF'];
$mode = $_REQUEST['mode'];
$order = $_REQUEST['order'];
$popup_flg = $_REQUEST['popup_flg'];
$notice_ids = $_REQUEST['notice_ids'];

// セッションのチェック
$session = qualify_session($_REQUEST['session'], $fname);
if ($session == "0") {
    js_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// 入力チェック
if ($mode === 'delete' and !is_array($notice_ids)) {
    echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

$conf = new Cmx_SystemConfig();

// トランザクションを開始
pg_query($con, "begin");

// 削除の場合
if ($mode === 'delete') {

    // アクセスログ
    aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"], $_POST, $con);

    // チェックされた通達区分をループ
    foreach ($notice_ids as $notice_id) {

        // お知らせに使用されていないかチェック
        $sql_count = "select count(*) from news where notice_id = " . p($notice_id);
        $sel = select_from_table($con, $sql_count, "", $fname);
        if ($sel == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_fetch_result($sel, 0, 0) > 0) {
            $sql_name = "select notice_name from newsnotice where notice_id = " . p($notice_id);
            $sel = select_from_table($con, $sql_name, "", $fname);
            if ($sel == 0) {
                pg_query($con, "rollback");
                pg_close($con);
                js_error_exit();
            }
            $notice_name = pg_fetch_result($sel, 0, "notice_name");

            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\">alert('「{$notice_name}」は、お知らせに使用されているため削除できません。');</script>");
            echo("<script type=\"text/javascript\">history.back();(window);</script>");
            exit;
        }

        // 通達区分を削除
        $sql_delete = "delete from newsnotice where notice_id = " . p($notice_id);
        $del = delete_from_table($con, $sql_delete, "", $fname);
        if ($del == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            js_error_exit();
        }
    }
}

// 並べ替えの場合
else if ($mode === 'sort') {
    $no = 1;
    foreach ($order as $notice_id) {
        $data = array(
            "order_no" => $no++,
            "popup_flg" => ($popup_flg[$notice_id] === 't' ? 't' : 'f'),
        );
        $upd = update_set_table($con, "update newsnotice set", array_keys($data), array_values($data), "where notice_id = " . p($notice_id), $fname);
        if ($upd == 0) {
            pg_close($con);
            js_error_exit();
        }
    }

    // (未設定)のポップアップ通知設定をセット
    $conf->set('news.global_popup_flg', $_REQUEST['global_popup_flg']);
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 通達区分一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_notice_list.php?session=$session';</script>");
