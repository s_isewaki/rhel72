<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("hiyari_report_class.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("hiyari_rm_stats_util.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//����̾
//====================================
$fname = $PHP_SELF;

//====================================
//���å����Υ����å�
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DB���ͥ���������
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
$filename = 'statistic_analyse.xls';

// �������������ʥե�����̾������ơ׹��ܤ�ɽ���������
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$filename);

//====================================
//�ե�����̾
//====================================
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}

$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());

$axis_detail_info = "";
$axis_detail_info["vertical_axises_detail"] = $vertical_axises_detail;
$axis_detail_info["horizontal_axises_detail"] = $horizontal_axises_detail;
$axis_detail_info["vertical_axises_post_class"] = $vertical_axises_post_class;
$axis_detail_info["vertical_axises_post_attribute"] = $vertical_axises_post_attribute;
$axis_detail_info["vertical_axises_post_dept"] = $vertical_axises_post_dept;
$axis_detail_info["vertical_axises_summary_item"] = $vertical_axises_summary_item;
$axis_detail_info["horizontal_axises_post_class"] = $horizontal_axises_post_class;
$axis_detail_info["horizontal_axises_post_attribute"] = $horizontal_axises_post_attribute;
$axis_detail_info["horizontal_axises_post_dept"] = $horizontal_axises_post_dept;
$axis_detail_info["horizontal_axises_summary_item"] = $horizontal_axises_summary_item;

//	session_name("hiyari_stats_sid");
//	session_start();
//	$item_shibori_info = $_SESSION['item_shibori_info'];

//	$stats_read_div = get_stats_read_div($session,$fname,$con);
//	$emp_id = get_emp_id($con,$session,$fname);
$sub_report_use_flg = ($sub_report_use == "true");
//	$download_data = get_excle_data($con, $fname, $vertical_axis, $horizontal_axis, $year, $month, $vertical_label, $horizontal_label, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info);
$download_data = get_excle_data($con, $fname, $vertical_axis, $horizontal_axis, $axis_detail_info, $cells_report_id_list_full_string, $vertical_no_disp_code_list_csv, $horizontal_no_disp_code_list_csv, $year, $month, $vertical_label, $horizontal_label, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $sub_report_use_flg,$item_shibori_info_string,$count_date_mode, $Date_Term);


//====================================
//����ʬ�ϥǡ���EXCEL����
//====================================
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();



pg_close($con);

//====================================
//�����ؿ�
//====================================

function get_excle_data($con, $fname, $vertical_axis, $horizontal_axis, $axis_detail_info, $cells_report_id_list_full_string, $vertical_no_disp_code_list_csv, $horizontal_no_disp_code_list_csv, $year, $month, $vertical_label, $horizontal_label, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $sub_report_use_flg,$item_shibori_info_string ,$count_date_mode, $Date_Term)
{

	// ����ޥ��������Ƥ����
	$rep_obj = new hiyari_report_class($con, $fname);
	$grps = $rep_obj->get_all_easy_items();
	
	//����ǡ����μ���
	$stats_util = new hiyari_rm_stats_util($con, $fname);
	$vertical_no_disp_code_list = $stats_util->conv_no_disp_code_list_csv_to_arr($vertical_no_disp_code_list_csv);
	$horizontal_no_disp_code_list = $stats_util->conv_no_disp_code_list_csv_to_arr($horizontal_no_disp_code_list_csv);
	$vertical_axis_stats_info = $stats_util->get_axis_stats_info('vertical',$grps,$vertical_axis,$axis_detail_info,$count_date_mode,false,$year,$month);
	$vertical_options = $vertical_axis_stats_info["axis_options"];
	$horizontal_axis_stats_info = $stats_util->get_axis_stats_info('horizontal',$grps,$horizontal_axis,$axis_detail_info,$count_date_mode,false,$year,$month);
	$horizontal_options = $horizontal_axis_stats_info["axis_options"];
	$cells_report_id_list_full = $stats_util->cells_report_id_list_full_from_string($cells_report_id_list_full_string);
	$arr_report_stats_data = $stats_util->get_report_stats_data2($cells_report_id_list_full,$vertical_options,$horizontal_options,$vertical_no_disp_code_list,$horizontal_no_disp_code_list);
	
	$vertical_options     = $arr_report_stats_data["vertical_options"];
	$horizontal_options   = $arr_report_stats_data["horizontal_options"];
	$stats_data_list      = $arr_report_stats_data["stats_data_list"];
	
	$counts               = $stats_data_list["cells"];
	$vertical_sums        = $stats_data_list["vertical_sums"];
	$horizontal_sums      = $stats_data_list["horizontal_sums"];
	$sums_sum             = $stats_data_list["sums_sum"];

	$post_name = get_post_name($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room);

	$download_data = "";
	if (count($counts) > 0) {
		$download_data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";

		
		//���׾��
		$download_data .= "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";

		$month2 = preg_replace("/^0/","",$month);//��0����
		$month2 = ($month2 == "") ? "": "{$month2}���";

		/*$n_year = $year + 1;
		if(empty($month)) { 
			$month2 .= "{$n_year}ǯ3��";
		} else { 
			$last_month = $month - 1;
			$month2 .= ($month == 1) ? "{$year}ǯ12��" : "{$n_year}ǯ{$last_month}��";
		}*/
		$month2 .= preg_replace("/^0/", "", $Date_Term) . "����ʬ";

		$sub_report_use_string = ($sub_report_use_flg) ? "��������ޤ�":"";
		$year_nendo = $year."ǯ".$month2."����ʬ�ϥǡ���(�ļ�����".h($vertical_label)."�� ��������".h($horizontal_label)."��)".$sub_report_use_string; 
		
		$download_data .= "<tr>";
		$download_data .= "<td nowrap>";
		$download_data .= "<b><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$year_nendo</font></b>";
		$download_data .= "</td>";
		$download_data .= "</tr>";

		$property = "��°��".h($post_name); 
		
		$download_data .= "<tr>";
		$download_data .= "<td nowrap>";
		$download_data .= "<b><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($property)."</font></b>";
		$download_data .= "</td>";
		$download_data .= "</tr>";

		if( $item_shibori_info_string != "")
		{
			$download_data .= "<tr>";
			$download_data .= "<td nowrap>";
			$download_data .= "<b><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">���ץǡ�����".h($item_shibori_info_string)."</font></b>";
			$download_data .= "</td>";
			$download_data .= "</tr>";
		}

		$download_data .= "</table>";

		//����ɽ
		$download_data .= "<table  border=\"1\" cellspacing=\"0\" cellpadding=\"2\">";
		
		//�����ȥ��
		$download_data .= "<tr height=\"22\">";
		$download_data .= "<td nowrap><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\"></font></td>";
		foreach($horizontal_options as $horizontal)
		{
			if(in_array($horizontal['easy_code'],$horizontal_no_disp_code_list))
			{
				continue;
			}
			
			$tmp_horizontal = $horizontal["easy_name"];
			$download_data .= "<td nowrap bgcolor=\"#c8c8c8\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($tmp_horizontal)."</font></td>";
		}
		$download_data .= "<td nowrap><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">��</font></td>";
		$download_data .= "</tr>";
		
		//�ǡ��������
		$vertical_index = -1;
		foreach($vertical_options as $vertical)
		{
			$vertical_index++;
			if(in_array($vertical['easy_code'],$vertical_no_disp_code_list))
			{
				continue;
			}
			
			$tmp_vertical = $vertical["easy_name"];
			$download_data .= "<tr height=\"22\">";
			$download_data .= "<td bgcolor=\"#c8c8c8\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($tmp_vertical)."</font></td>";
			
			$horizontal_index = -1;
			foreach($horizontal_options as $horizontal)
			{
				$horizontal_index++;
				if(in_array($horizontal['easy_code'],$horizontal_no_disp_code_list))
				{
					continue;
				}
				
				$count = $counts[$vertical_index][$horizontal_index];
				$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$count</font></td>";
			}
			$tmp_vertical_sums = $vertical_sums[$vertical_index];
			$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$tmp_vertical_sums</font></td>";
			$download_data .= "</tr>";
		}
		
		//��׹�
		$download_data .= "<tr height=\"22\">";
		$download_data .= "<td><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">��</font></td>";

		$horizontal_index = -1;
		foreach($horizontal_options as $horizontal)
		{
			$horizontal_index++;
			if(in_array($horizontal['easy_code'],$horizontal_no_disp_code_list))
			{
				continue;
			}
			
			$sum = $horizontal_sums[$horizontal_index];
			$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$sum</font></td>";
		}
		$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$sums_sum</font></td>";
		$download_data .= "</tr>";
		
		
		$download_data .= "</table>";
		
	}
	return $download_data;
}




//	
//	function get_excle_data($con, $fname, $vertical_axis, $horizontal_axis, $year, $month, $vertical_label, $horizontal_label, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info)
//	{
//		
//		// ����ޥ��������Ƥ����
//		$rep_obj = new hiyari_report_class($con, $fname);
//		$grps = $rep_obj->get_all_easy_items();
//	
//		$arr_post = array();
//		if($search_emp_class != "") {
//			array_push($arr_post, $search_emp_class);
//		}
//		if($search_emp_attribute != "") {
//			array_push($arr_post, $search_emp_attribute);
//		}
//		if($search_emp_dept != "") {
//			array_push($arr_post, $search_emp_dept);
//		}
//		if($search_emp_room != "") {
//			array_push($arr_post, $search_emp_room);
//		}
//	
//		$stats_util = new hiyari_rm_stats_util($con, $fname);
//		$arr_report_stats_data = $stats_util->get_report_stats_data($grps, $vertical_axis, $horizontal_axis, $year, $month, $arr_post, $count_date_mode,$emp_id,$stats_read_div,$sub_report_use_flg,$axis_detail_info);
//	
//		$vertical_options     = $arr_report_stats_data["vertical_options"];
//		$horizontal_options   = $arr_report_stats_data["horizontal_options"];
//		$stats_data_list      = $arr_report_stats_data["stats_data_list"];
//		
//		$counts               = $stats_data_list["cells"];
//		$vertical_sums        = $stats_data_list["vertical_sums"];
//		$horizontal_sums      = $stats_data_list["horizontal_sums"];
//		$sums_sum             = $stats_data_list["sums_sum"];
//	
//		$post_name = get_post_name($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room);
//	
//		$download_data = "";
//		if (count($counts) > 0) {
//			$download_data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
//	
//			$download_data .= "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//	
//			$download_data .= "<tr>";
//			$month2 = preg_replace("/^0/","",$month);//��0����
//			$month2 = ($month2 == "") ? "": "({$month2}��)";
//			$sub_report_use_string = ($sub_report_use_flg) ? "��������ޤ�":"";
//			$year_nendo = $year."ǯ��".$month2."����ʬ�ϥǡ���(�ļ�����".h($vertical_label)."�� ��������".h($horizontal_label)."��)".$sub_report_use_string; 
//			
//			
//			$download_data .= "<td nowrap><b><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$year_nendo</font></b></td>";
//			$download_data .= "</tr>";
//	
//			$download_data .= "<tr>";
//			$property = "��°��".h($post_name); 
//			$download_data .= "<td nowrap><b><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($property)."</font></b></td>";
//			$download_data .= "</tr>";
//	
//			$download_data .= "<tr>";
//			$download_data .= "<td>";
//			$download_data .= "<table  border=\"1\" cellspacing=\"0\" cellpadding=\"2\">";
//			$download_data .= "<tr height=\"22\">";
//			$download_data .= "<td nowrap><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\"></font></td>";
//			foreach($horizontal_options as $horizontal) {
//				$tmp_horizontal = $horizontal["easy_name"];
//				$download_data .= "<td nowrap bgcolor=\"#c8c8c8\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($tmp_horizontal)."</font></td>";
//			}
//			$download_data .= "<td nowrap><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">��</font></td>";
//			$download_data .= "</tr>";
//			$iterate_cnt = 0;
//			foreach($vertical_options as $vertical) {
//				$tmp_vertical = $vertical["easy_name"];
//				$download_data .= "<tr height=\"22\">";
//				$download_data .= "<td bgcolor=\"#c8c8c8\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">".h($tmp_vertical)."</font></td>";
//	
//				foreach($counts[$iterate_cnt] as $count) {
//					$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$count</font></td>";
//				}
//				$tmp_vertical_sums = $vertical_sums[$iterate_cnt];
//				$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$tmp_vertical_sums</font></td>";
//				$download_data .= "</tr>";
//				$iterate_cnt++;
//			}
//	
//			$download_data .= "<tr height=\"22\">";
//			$download_data .= "<td><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">��</font></td>";
//			foreach($horizontal_sums as $sum) {
//				$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$sum</font></td>";
//			}
//			$download_data .= "<td align=\"right\"><font size=\"2\" face=\"�ͣ� �Х����å�, Osaka\">$sums_sum</font></td>";
//			$download_data .= "</tr>";
//			$download_data .= "</table>";
//	
//			$download_data .= "</td>";
//			$download_data .= "</tr>";
//			$download_data .= "</table>";
//	
//		}
//		return $download_data;
//	}



function get_post_name($con, $fname, $search_emp_class, $search_emp_attribute, $search_emp_dept, $search_emp_room)
{
	// �ȿ�������
	$arr_class_name = get_class_name_array($con, $fname);

	for($i=0; $i<count($search_emp_class); $i++) { 
		// �����
		$post_name = "";
		
		if($search_emp_class[$i] != "") {
			$post_name .= get_class_nm($con, $search_emp_class[$i], $fname);

		} else {
			$post_name .= "����";
		}

		$post_name .= "��";

		if($search_emp_attribute[$i] != "") {
			$post_name .= get_atrb_nm($con, $search_emp_attribute[$i], $fname);
		} else {
			$post_name .= "����";
		}

		$post_name .= "��";

		if($search_emp_dept[$i] != "") {
			$post_name .= get_dept_nm($con, $search_emp_dept[$i], $fname);
		} else {
			$post_name .= "����";
		}

		if($arr_class_name["class_cnt"] == 4) {
			$post_name .= "��";
			if($search_emp_room[$i] != "") {
				$post_name .= get_room_nm($con, $search_emp_room[$i], $fname);
			} else {
				$post_name .= "����";
			}
		}
		$post_name_arr[] = $post_name;
	}

	return implode(" | ", $post_name_arr);
}
?>