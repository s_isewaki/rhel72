<?php
function show_workflow_block($con, $emp_id, $font_size, $session, $fname) {
    require_once("application_workflow_common_class.php");
    //--------------------------------------------------------------
	//「COMPANY」の情報取得
	//--------------------------------------------------------------
    require_once("Cmx/Model/SystemConfig.php");
    // SystemConfig

    $conf = New Cmx_systemConfig();

    $notice_company_use = $conf->get('shift.notice.company_use');
    $notice_company_url = $conf->get('shift.notice.company_url');
    $notice_company_timeout = $conf->get('shift.notice.company_timeout');
    $notice_company_transition_url = $conf->get('shift.notice.company_transition_url');

    
    $notice_approval_confirmed_display = $conf->get('shift.notice.approval_confirmed');
    

    
    $font_class = ($font_size == "2") ? "j16" : "j12";
    $icon_name = ($font_size =="2" ? "b08.gif" : "s08.gif");
	$icon_size = ($font_size === "2") ? "32" : "20";
    // 表示用情報
        
        
     
    $notice_data = array();
	$obj = new application_workflow_common_class($con, $fname);

	//--------------------------------------------------------------
	//「承認確定」の申請情報取得
	//--------------------------------------------------------------
       if(!$notice_approval_confirmed_display){ 
        $apply_fix_cnt = $obj->get_apply_fix($emp_id);
            if($apply_fix_cnt > 0) {
		$notice_data[] = array(
            'message'   => "承認確定された申請書が".$apply_fix_cnt."件あります。",
            'attr'      => " href=\"application_apply_list.php?session={$session}&apply_stat=1\""
            );
       }
       
            }
   
	//--------------------------------------------------------------
	//「否認」の申請情報取得
	//--------------------------------------------------------------
	$apply_ng_cnt = $obj->get_apply_ng($emp_id);
	if($apply_ng_cnt > 0) {
		$notice_data[] = array(
            'message'   => "否認された申請書が".$apply_ng_cnt."件あります。",
            'attr'      => " href=\"application_apply_list.php?session={$session}&apply_stat=2\""
        );
	}

	//--------------------------------------------------------------
	//「差戻し」の申請情報取得
	//--------------------------------------------------------------
    $apply_bak_cnt = $obj->get_apply_bak($emp_id);
	if($apply_bak_cnt > 0) {
		$notice_data[] = array(
            'message'   => "差戻しされた申請書が".$apply_bak_cnt."件あります。",
            'attr'      => " href=\"application_apply_list.php?session={$session}&apply_stat=3\""
        );
	}

	//--------------------------------------------------------------
	//「承認待ち」の申請情報取得
	//--------------------------------------------------------------
	$approve_wait_cnt = $obj->get_approve_wait($emp_id);
	if($approve_wait_cnt > 0) {
		$notice_data[] = array(
            'message'   => "承認待ち(未承認）の申請書が".$approve_wait_cnt."件あります。",
            'attr'      => " href=\"application_approve_list.php?session={$session}&apv_stat=0\""
        );
	}

	//--------------------------------------------------------------
	//「一部承認」の申請情報取得
	//--------------------------------------------------------------
    $apply_fix_1_cnt = $obj->get_apply_fix_1($emp_id);
	if($apply_fix_1_cnt > 0) {
		$notice_data[] = array(
            'message'   => "一部承認された申請書が".$apply_fix_1_cnt."件あります。",
            'attr'      => " href=\"application_apply_list.php?session={$session}&apply_stat=0\""
        );
	}

	//--------------------------------------------------------------
	//「結果通知」の申請情報取得
	//--------------------------------------------------------------
	$apply_result_notice_cnt = $obj->get_non_confirmed_notice_count($session);
	if($apply_result_notice_cnt > 0) {
        $notice_data[] = array(
            'message'   => "結果通知された申請書が".$apply_result_notice_cnt."件あります。",
            'attr'      => " href=\"application_apply_list.php?session={$session}\""
        );
	}

	//--------------------------------------------------------------
	//「合議チェック」の情報取得
	//--------------------------------------------------------------
	$apply_result_cnlcomment_cnt = $obj->get_council_count($emp_id);
	if($apply_result_cnlcomment_cnt > 0) {
        $notice_data[] = array(
            'message'   => "合議通知が".$apply_result_cnlcomment_cnt."件あります。",
            'attr'      => " href=\"application_apply_list_council.php?session={$session}\""
        );
	}


    
    if ($notice_company_use === "1" && isset($notice_company_url) && isset($notice_company_timeout)) {
        // COMPANYのURLから件数取得
        // 1以上の数値で有れば通知、0で有れば通知しない
        // それ以外の時はアラート
        // ログインユーザのpersonal_idを取得
        $company_connection_failed = false;
        $sql = "SELECT emp_login_id FROM login l, empmst e";
        $cond = "where l.emp_id = e.emp_id AND e.emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel === 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $emp_login_id = pg_fetch_result($sel, 0, "emp_login_id");
        $notice_company_cnt = trim(shell_exec('wget -q -T ' . escapeshellarg($notice_company_timeout) . ' -t 1 -O - '.escapeshellarg($notice_company_url."&empno=".$emp_login_id)));
        if(h($notice_company_cnt) > 0) {
            $notice_data[] = array(
                'message'   => "COMPANYに承認待ちの申請書が".h($notice_company_cnt)."件あります。",
                'attr'      => " href=\"javascript:void(0)\" onclick=\"return post_new_window(company_form, '{$notice_company_transition_url}');\""
            );
        }
        else if($notice_company_cnt !== "0") {
            $company_connection_failed = true;
        }
    }
	if (empty($notice_data)) {
        $notice_data[] = array(
            'message'   => ($notice_company_use === "1") ?
                    "「承認確定」「否認」「差戻し」「承認待ち」「一部承認」「結果通知」「合議通知」「COMPANY」の決裁申請はありません。" : "「承認確定」「否認」「差戻し」「承認待ち」「一部承認」「結果通知」「合議通知」の決裁申請はありません。",
            'attr'      => ""
        );
    }
    // COMPANY連携をする設定且つ、COMPANYから結果が返ってこなければアラート
    if($company_connection_failed){
        $notice_data[] = array(
            'message'   => "COMPANYの承認情報取得に失敗しました。",
            'attr'      => ""
        );
    }

?>

<script type="text/javascript">
function post_new_window(post_form, url){
    post_form.setAttribute("action", url);
    post_form.submit();
    return false;
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="2" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
    </tr>
    <tr>
        <td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
        <td width="100%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                 <td height="22" class="spacing" valign="middle" bgcolor="#bdd1e7">
                    <a href="application_menu.php?session=<?php echo($session); ?>">
                        <img src='img/icon/<?php echo($icon_name); ?>?v=1' width='<?php echo($icon_size) ?>' height='<?php echo($icon_size) ?>' border='0' alt='決裁・申請'>
                    </a>
                </td>
                <td width="100%" bgcolor="#bdd1e7" style="padding-left:4px">
                    <a href="application_menu.php?session=<?php echo($session); ?>">
                    <font size='3' face='ＭＳ Ｐゴシック, Osaka' class='<?php echo($font_class); ?>'><b>決裁・申請</b></font>
                    </a>
               </td>
            </tr>

            <tr>
                <td bgcolor="#5279a5" colspan="3"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
            </tr>

<?php
    foreach($notice_data as $val){
?>
        <tr valign="top">
            <td colspan="3">
                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class=<?php echo($font_class); ?>>
                    <a<?php echo($val['attr']); ?>><?php echo($val['message']); ?></a>
                </font>
            </td>
        </tr>
<?php } ?>
</table>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
    <td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>

<form name="company_form" action="" target="_blank" method="POST">
    <input type="hidden" name="user" value="<?php echo($emp_login_id);?>">
    <input type="hidden" name="@JUMP" value="root.cws.intray">
    <input type="hidden" name="@SUB" value="1">
    <input type="hidden" name="@SN" value="root.cws">
</form>

<?php
}
