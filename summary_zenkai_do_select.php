<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="X-UA-Compatible" content="IE=Edge, chrome=1">
<title>CoMedix メドレポート｜前回Do選択</title>
<?
require_once("about_comedix.php");
require_once('sot_util.php');

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 57, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

$con = connect2db($fname);

$sel2 = select_from_table($con,"select * from tmplmst","",$fname);
if($sel2 == 0){
//    echo pg_last_error($con);
//    echo "ng";
//    die;
    pg_close($con);
    js_error_exit();
}
$arr_tmplname = array();
$num2 = pg_numrows($sel2);
for ($ii=0; $ii<$num2; $ii++) {
    $id = pg_result($sel2,$ii,"tmpl_id");
    $arr_tmplname[$id] = pg_result($sel2,$ii,"tmpl_name");
}
$sql =
" select sum.cre_date, sum.tmpl_id, sum.summary_id, sum.summary_seq_max, xml.smry_html from sum_xml xml".
" inner join (".
"     select sum1.*, sum2.cre_date from (".
"         select summary_id, ptif_id, tmpl_id, max(summary_seq) as summary_seq_max from summary".
"         where ptif_id = '".$pt_id."'".
"         and priority = '1'".
"         and diag_div = '処方箋'".
"         group by tmpl_id, summary_id, ptif_id".
"     ) sum1".
"     inner join summary sum2 on (sum2.summary_id = sum1.summary_id and sum2.summary_seq = sum1.summary_seq_max)".
" ) sum on (sum.summary_id = xml.summary_id and sum.summary_seq_max = xml.summary_seq and sum.ptif_id = xml.ptif_id)".
" where xml.delete_flg = '0'".
" order by sum.cre_date desc limit 20";
$sel = select_from_table($con, $sql, "", $fname);

if ($sel == 0) {
//    echo pg_last_error($con);
//    die;
    pg_close($con);
    js_error_exit();
}


$summary_seq_ary = array();
$rows = array();
while ($row = pg_fetch_array($sel)) {
    if (!in_array($row["summary_seq_max"], $summary_seq_ary)) $summary_seq_ary[]= $row["summary_seq_max"];
    $rows[] = $row;
}
// 薬剤コードは文字を除去したい
$sql =
    " select sss.summary_seq, sai.yj_cd from sum_order_syoho_cyusya_rp rp".
    " inner join sum_med_saiyou_mst sai on (sai.seq::text = rp.med_seq_cd)".
    " inner join sot_summary_schedule sss on (sss.schedule_seq = rp.schedule_seq)".
    " where sss.summary_seq in (-1, ".implode(",",$summary_seq_ary).")".
    " group by sai.yj_cd, sss.summary_seq";
$sel2 = select_from_table($con, $sql, "", $fname);
if ($sel2 == 0) {
//    echo pg_last_error($con);
//    die;
    pg_close($con);
    js_error_exit();
}


$yj_cd_ary = array();
while ($row = pg_fetch_array($sel2)) {
    $seq = $row["summary_seq"];
//  if (!defined($yj_cd_ary[$seq])) $yj_cd_ary[$seq] = array();
    $yj_cd_ary[$seq][]= $row["yj_cd"];
}

?>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<style type="text/css">
    #prop_table_ex td { border-bottom:1px solid #9bc8ec; }
</style>
<script type="text/javascript" src="js/jquery/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/swapimg.js"></script>
<script type="text/javascript">
// 傷病名選択時の処理
function selectZenkaiDo(hiddenId, yj_cd_comma_list) {
    var yj_cd_list = yj_cd_comma_list.split(",");
    if (opener && !opener.closed && opener.selectZenkaiDo) {
        var html = document.getElementById(hiddenId).value;
        var out = [];
        html = html.split("\n");
        for (var idx=0; idx<html.length; idx++) {
            if (idx==0) continue;
            var line = html[idx];
            if (line.indexOf("\[薬\]")>=0) {
                for (var idx2=0; idx2<yj_cd_list.length; idx2++) {
                    line = line.replace(yj_cd_list[idx2], "");
                }
            }
            line = line.replace(/[\|｜]/g, " ");
            line = line.replace(/\[薬\]/g, "");
            line = line.replace(/\[用法\]/g, "");
            line = line.replace(/・/g, "");
            var chars = line.split(" ");
            for (var idx2=0; idx2<chars.length; idx2++) {
                var node = chars[idx2].replace(/(^\s+)|(\s+$)/g, "");
                var rpIdx1 = node.indexOf("＜R");
                if (rpIdx1==0) {
                    var rpIdx2 = node.indexOf("＞");
                    if (rpIdx2>0 && rpIdx2<5) node = "\n"+node.substring(rpIdx2+1);
                }
//                var nbsp = (node.indexOf("＜R")==0 ? "\n" : "");
                if (node!="") out.push(node);
            }
        }
        opener.selectZenkaiDo(out.join(" ").replace(/(^\n)/, "").replace(/(\s\n)/g, "\n"));
    }
}

// セルハイライトON
var currentTr = "";
function highlightCells(td, hiddenId) {
    if (currentTr) currentTr.style.backgroundColor = "";
    currentTr = td.parentNode;
    currentTr.style.backgroundColor = "#fefc8f";
    document.getElementById("divHtmlDetail").innerHTML = document.getElementById(hiddenId).value.split("\n").join("<br/>");
}

$(function(){
    resized();
});
function resized() {
    $("#divHtmlDetail").html("");
    $("#scrollDiv1").css({display:"none"});
    $("#scrollDiv2").css({display:"none"});
    var ee = function(id) { return document.getElementById(id); };
    var h = window.innerHeight || document.body.clientHeight;
    h -= 74;
    $("#scrollDiv1").css({height:h, display:"block"});
    $("#scrollDiv2").css({height:h, display:"block"});
    var div = $("#scrollDiv2").get(0);
    div.style.width = "100%";
    div.style.width = (div.parentNode.offsetWidth-10) + "px";
}
</script>
</head>
<body style="background-color:#f4f4f4" onresize="resized()">
<div style="height:100%; width:100%;" id="divBase">

<div style="padding:4px">
<div>
  <table class="dialog_titletable"><tr>
    <th>前回Do選択</th>
      <td><a href="javascript:window.close()"><img src="img/icon/close.gif" alt="閉じる"
        onmouseover="swapimg(this,'img/icon/closeo.gif');" onmouseout="swapimg(this,'img/icon/close.gif');" /></a></td>
  </tr></table>
</div>

<form name="frm" action="summary_kensakekka_timeseries.php" method="post">
<input type="hidden" name="session" value="<? echo $session; ?>">
<input type="hidden" name="pt_id" value="<? echo $pt_id; ?>">
<input type="hidden" style="width:100%;" name="itm_ord" id="itm_ord" value="">
</form>
<form id="mainform">
<table cellspacing="0" cellpadding="0" class="prop_table" style="width:100%">
<tr>
    <th style="width:250px">処方日一覧&nbsp;&nbsp;（直近20回分）</th>
    <th>処方内容</th>
</tr>

<tr>
    <td style="vertical-align:top">
    <div style="overflow-y:scroll; overflow-x:hidden; width:250px; height:300px" id="scrollDiv1">
    <table cellspacing="0" cellpadding="0" style="width:250px" id="prop_table_ex" title="クリックで呼出元画面へ追記反映します">
    <?
       foreach($rows as $row) {
            $naiyo_string = str_replace("<BR>", "\n",    $row["smry_html"]);
            $naiyo_string = str_replace("<br/>",   "\n", $naiyo_string);
            $yj_cd = @implode(",",$yj_cd_ary[$row["summary_seq_max"]]);
    ?>
    <tr onclick="selectZenkaiDo('html<?=$row["summary_id"]?>', '<?=$yj_cd?>');">
        <td style="cursor: pointer;" onmouseover="highlightCells(this, 'html<?=$row["summary_id"]?>')">
            <?=$c_sot_util->slash_yyyymmdd($row["cre_date"])?>&nbsp;&nbsp;<?=eh(@$arr_tmplname[$row["tmpl_id"]])?>
            <input type="hidden" id="html<?=$row["summary_id"]?>" value="<?=mb_ereg_replace('"', '""', $naiyo_string)?>" />
        </td>
    </tr>
    <? } ?>
    </table>
    </div>
    </td>
    <td style="vertical-align:top; padding:4px; background-color:#f5f5f5">
        <div style="overflow:scroll" id="scrollDiv2"><div id="divHtmlDetail"></div></div></td>
    </td>
</tr></table>
</form>
</div>
</div>
</body>
</html>
<? pg_close($con); ?>
<?
function format_date($ymd) {
    if (strlen($ymd) === 8) {
        return preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1/$2/$3", $ymd);
    }
    return "";
}
