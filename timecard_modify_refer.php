<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 勤務時間修正申請参照</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_attendance_pattern.ini");
require_once("atdbk_common_class.php");
require_once("show_timecard_apply_detail.ini");
require_once("show_timecard_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("timecard_common_class.php");
require_once("ovtm_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

$ovtm_class = new ovtm_class($con, $fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

// 勤務時間修正申請情報を取得
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, tmmdapply.emp_id, tmmdapply.target_date, mdfyrsn.reason, tmmdapply.reason as other_reason, tmmdapply.apply_time, tmmdapply.comment, tmmdapply.b_tmcd_group_id, tmmdapply.b_pattern, tmmdapply.b_reason, tmmdapply.b_night_duty, tmmdapply.b_allow_id, tmmdapply.b_start_time, tmmdapply.b_out_time, tmmdapply.b_ret_time, tmmdapply.b_end_time, tmmdapply.b_o_start_time1, tmmdapply.b_o_end_time1, tmmdapply.b_o_start_time2, tmmdapply.b_o_end_time2, tmmdapply.b_o_start_time3, tmmdapply.b_o_end_time3, tmmdapply.b_o_start_time4, tmmdapply.b_o_end_time4, tmmdapply.b_o_start_time5, tmmdapply.b_o_end_time5, tmmdapply.b_o_start_time6, tmmdapply.b_o_end_time6, tmmdapply.b_o_start_time7, tmmdapply.b_o_end_time7, tmmdapply.b_o_start_time8, tmmdapply.b_o_end_time8, tmmdapply.b_o_start_time9, tmmdapply.b_o_end_time9, tmmdapply.b_o_start_time10, tmmdapply.b_o_end_time10, tmmdapply.a_tmcd_group_id, tmmdapply.a_pattern, tmmdapply.a_reason, tmmdapply.a_night_duty, tmmdapply.a_allow_id, tmmdapply.a_start_time, tmmdapply.a_out_time, tmmdapply.a_ret_time, tmmdapply.a_end_time, tmmdapply.a_o_start_time1, tmmdapply.a_o_end_time1, tmmdapply.a_o_start_time2, tmmdapply.a_o_end_time2, tmmdapply.a_o_start_time3, tmmdapply.a_o_end_time3, tmmdapply.a_o_start_time4, tmmdapply.a_o_end_time4, tmmdapply.a_o_start_time5, tmmdapply.a_o_end_time5, tmmdapply.a_o_start_time6, tmmdapply.a_o_end_time6, tmmdapply.a_o_start_time7, tmmdapply.a_o_end_time7, tmmdapply.a_o_start_time8, tmmdapply.a_o_end_time8, tmmdapply.a_o_start_time9, tmmdapply.a_o_end_time9, tmmdapply.a_o_start_time10, tmmdapply.a_o_end_time10, tmmdapply.b_meeting_time, tmmdapply.a_meeting_time, tmmdapply.b_previous_day_flag, tmmdapply.a_previous_day_flag, tmmdapply.b_next_day_flag, tmmdapply.a_next_day_flag, tmmdapply.b_meeting_start_time, tmmdapply.b_meeting_end_time, tmmdapply.a_meeting_start_time, tmmdapply.a_meeting_end_time, tmmdapply.a_allow_count, tmmdapply.a_over_start_time, tmmdapply.a_over_end_time, tmmdapply.b_allow_count, tmmdapply.b_over_start_time, tmmdapply.b_over_end_time, tmmdapply.a_over_start_next_day_flag, tmmdapply.a_over_end_next_day_flag, tmmdapply.b_over_start_next_day_flag, tmmdapply.b_over_end_next_day_flag ".
	", tmmdapply.a_over_start_time2".
	", tmmdapply.a_over_end_time2".
	", tmmdapply.b_over_start_time2".
	", tmmdapply.b_over_end_time2".
	", tmmdapply.a_over_start_next_day_flag2".
	", tmmdapply.a_over_end_next_day_flag2".
	", tmmdapply.b_over_start_next_day_flag2".
	", tmmdapply.b_over_end_next_day_flag2 ".
	", tmmdapply.b_paid_hol_hour_start_time " . //時間有休
	", tmmdapply.a_paid_hol_hour_start_time " .
	", tmmdapply.b_paid_hol_use_hour " .
	", tmmdapply.a_paid_hol_use_hour " .
	", tmmdapply.b_paid_hol_use_minute " .
    ", tmmdapply.a_paid_hol_use_minute "; 
$sql .=	", tmmdapply.b_rest_start_time";
$sql .=	", tmmdapply.b_rest_end_time";
$sql .=	", tmmdapply.a_rest_start_time";
$sql .=	", tmmdapply.a_rest_end_time";
for ($i=3; $i<=5; $i++) {
    $sql .=	", tmmdapply.b_over_start_time{$i}";
    $sql .=	", tmmdapply.b_over_end_time{$i}";
    $sql .=	", tmmdapply.b_over_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.b_over_end_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_over_start_time{$i}";
    $sql .=	", tmmdapply.a_over_end_time{$i}";
    $sql .=	", tmmdapply.a_over_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_over_end_next_day_flag{$i}";
}
for ($i=1; $i<=5; $i++) {
    $sql .=	", tmmdapply.b_rest_start_time{$i}";
    $sql .=	", tmmdapply.b_rest_end_time{$i}";
    $sql .=	", tmmdapply.b_rest_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.b_rest_end_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_rest_start_time{$i}";
    $sql .=	", tmmdapply.a_rest_end_time{$i}";
    $sql .=	", tmmdapply.a_rest_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_rest_end_next_day_flag{$i}";
}
$sql .= ", tmmdapply.b_allow_id2, tmmdapply.b_allow_count2, tmmdapply.b_allow_id3, tmmdapply.b_allow_count3";
$sql .= ", tmmdapply.a_allow_id2, tmmdapply.a_allow_count2, tmmdapply.a_allow_id3, tmmdapply.a_allow_count3";
$sql .=	" from (tmmdapply inner join empmst on tmmdapply.emp_id = empmst.emp_id) left join mdfyrsn on tmmdapply.reason_id = mdfyrsn.reason_id";
$cond = "where tmmdapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
$target_date = pg_fetch_result($sel, 0, "target_date");

$timecard_common_class = new timecard_common_class($con, $fname, $apply_emp_id, $target_date, $target_date);

$reason = pg_fetch_result($sel, 0, "reason");
$other_reason = pg_fetch_result($sel, 0, "other_reason");
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$comment = pg_fetch_result($sel, 0, "comment");


$b_tmcd_group_id = pg_fetch_result($sel, 0, "b_tmcd_group_id");
$b_pattern = pg_fetch_result($sel, 0, "b_pattern");
$b_reason = pg_fetch_result($sel, 0, "b_reason");
$b_night_duty = pg_fetch_result($sel, 0, "b_night_duty");
$b_allow_id = pg_fetch_result($sel, 0, "b_allow_id");
$b_allow_count = pg_fetch_result($sel, 0, "b_allow_count");
$b_allow_count_value = ($b_allow_id == "") ? "" : " {$b_allow_count}";
$b_allow_id2 = pg_fetch_result($sel, 0, "b_allow_id2");
$b_allow_count2 = pg_fetch_result($sel, 0, "b_allow_count2");
$b_allow_count_value2 = ($b_allow_id2 == "") ? "" : " {$b_allow_count2}";
$b_allow_id3 = pg_fetch_result($sel, 0, "b_allow_id3");
$b_allow_count3 = pg_fetch_result($sel, 0, "b_allow_count3");
$b_allow_count_value3 = ($b_allow_id3 == "") ? "" : " {$b_allow_count3}";
$b_start_time = format_time(pg_fetch_result($sel, 0, "b_start_time"));
$b_out_time = format_time(pg_fetch_result($sel, 0, "b_out_time"));
$b_ret_time = format_time(pg_fetch_result($sel, 0, "b_ret_time"));
$b_end_time = format_time(pg_fetch_result($sel, 0, "b_end_time"));
$b_meeting_time = format_time(pg_fetch_result($sel, 0, "b_meeting_time"));
$b_previous_day_flag = pg_fetch_result($sel, 0, "b_previous_day_flag");
$b_next_day_flag = pg_fetch_result($sel, 0, "b_next_day_flag");
$b_meeting_start_time = format_time(pg_fetch_result($sel, 0, "b_meeting_start_time"));
$b_meeting_end_time = format_time(pg_fetch_result($sel, 0, "b_meeting_end_time"));
$b_over_start_time = format_time(pg_fetch_result($sel, 0, "b_over_start_time"));
$b_over_end_time = format_time(pg_fetch_result($sel, 0, "b_over_end_time"));
$b_over_start_next_day_flag = pg_fetch_result($sel, 0, "b_over_start_next_day_flag");
$b_over_end_next_day_flag = pg_fetch_result($sel, 0, "b_over_end_next_day_flag");
$b_over_start_time2 = format_time(pg_fetch_result($sel, 0, "b_over_start_time2"));
$b_over_end_time2 = format_time(pg_fetch_result($sel, 0, "b_over_end_time2"));
$b_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_start_next_day_flag2");
$b_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_end_next_day_flag2");
$b_paid_hol_hour_start_time = format_time(pg_fetch_result($sel, 0, "b_paid_hol_hour_start_time")); //時間有休
$b_paid_hol_use_hour = pg_fetch_result($sel, 0, "b_paid_hol_use_hour");
$b_paid_hol_use_minute = pg_fetch_result($sel, 0, "b_paid_hol_use_minute");

$a_tmcd_group_id = pg_fetch_result($sel, 0, "a_tmcd_group_id");
$a_pattern = pg_fetch_result($sel, 0, "a_pattern");
$a_reason = pg_fetch_result($sel, 0, "a_reason");
$a_night_duty = pg_fetch_result($sel, 0, "a_night_duty");
$a_allow_id = pg_fetch_result($sel, 0, "a_allow_id");
$a_allow_count = pg_fetch_result($sel, 0, "a_allow_count");
$a_allow_count_value = ($a_allow_id == "") ? "" : " {$a_allow_count}";
$a_allow_id2 = pg_fetch_result($sel, 0, "a_allow_id2");
$a_allow_count2 = pg_fetch_result($sel, 0, "a_allow_count2");
$a_allow_count_value2 = ($a_allow_id2 == "") ? "" : " {$a_allow_count2}";
$a_allow_id3 = pg_fetch_result($sel, 0, "a_allow_id3");
$a_allow_count3 = pg_fetch_result($sel, 0, "a_allow_count3");
$a_allow_count_value3 = ($a_allow_id3 == "") ? "" : " {$a_allow_count3}";
$a_start_time = format_time(pg_fetch_result($sel, 0, "a_start_time"));
$a_out_time = format_time(pg_fetch_result($sel, 0, "a_out_time"));
$a_ret_time = format_time(pg_fetch_result($sel, 0, "a_ret_time"));
$a_end_time = format_time(pg_fetch_result($sel, 0, "a_end_time"));
$a_meeting_time = format_time(pg_fetch_result($sel, 0, "a_meeting_time"));
$a_previous_day_flag = pg_fetch_result($sel, 0, "a_previous_day_flag");
$a_next_day_flag = pg_fetch_result($sel, 0, "a_next_day_flag");
$a_meeting_start_time = format_time(pg_fetch_result($sel, 0, "a_meeting_start_time"));
$a_meeting_end_time = format_time(pg_fetch_result($sel, 0, "a_meeting_end_time"));
$a_over_start_time = format_time(pg_fetch_result($sel, 0, "a_over_start_time"));
$a_over_end_time = format_time(pg_fetch_result($sel, 0, "a_over_end_time"));
$a_over_start_next_day_flag = pg_fetch_result($sel, 0, "a_over_start_next_day_flag");
$a_over_end_next_day_flag = pg_fetch_result($sel, 0, "a_over_end_next_day_flag");
$a_over_start_time2 = format_time(pg_fetch_result($sel, 0, "a_over_start_time2"));
$a_over_end_time2 = format_time(pg_fetch_result($sel, 0, "a_over_end_time2"));
$a_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_start_next_day_flag2");
$a_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_end_next_day_flag2");
$a_paid_hol_hour_start_time = format_time(pg_fetch_result($sel, 0, "a_paid_hol_hour_start_time")); //時間有休
$a_paid_hol_use_hour = pg_fetch_result($sel, 0, "a_paid_hol_use_hour");
$a_paid_hol_use_minute = pg_fetch_result($sel, 0, "a_paid_hol_use_minute");
//時間有休取得時間
$b_paid_hol_use_time = "";
if ($b_paid_hol_use_hour != "" && $b_paid_hol_use_minute != "") {
	$b_paid_hol_use_time = "{$b_paid_hol_use_hour}時間{$b_paid_hol_use_minute}分";
}
$a_paid_hol_use_time = "";
if ($a_paid_hol_use_hour != "" && $a_paid_hol_use_minute != "") {
	$a_paid_hol_use_time = "{$a_paid_hol_use_hour}時間{$a_paid_hol_use_minute}分";
}
for ($i = 1; $i <= 10; $i++) {
	$b_start_time_ver = "b_o_start_time$i";
	$b_end_time_ver = "b_o_end_time$i";
	$a_start_time_ver = "a_o_start_time$i";
	$a_end_time_ver = "a_o_end_time$i";
	$$b_start_time_ver = format_time(pg_fetch_result($sel, 0, "$b_start_time_ver"));
	$$b_end_time_ver = format_time(pg_fetch_result($sel, 0, "$b_end_time_ver"));
	$$a_start_time_ver = format_time(pg_fetch_result($sel, 0, "$a_start_time_ver"));
	$$a_end_time_ver = format_time(pg_fetch_result($sel, 0, "$a_end_time_ver"));
}

$b_rest_start_time = format_time(pg_fetch_result($sel, 0, "b_rest_start_time"));
$b_rest_end_time = format_time(pg_fetch_result($sel, 0, "b_rest_end_time"));
$a_rest_start_time = format_time(pg_fetch_result($sel, 0, "a_rest_start_time"));
$a_rest_end_time = format_time(pg_fetch_result($sel, 0, "a_rest_end_time"));
for ($i = 3; $i <= 5; $i++) {
    $b_start_time_ver = "b_over_start_time$i";
    $b_end_time_ver = "b_over_end_time$i";
    $a_start_time_ver = "a_over_start_time$i";
    $a_end_time_ver = "a_over_end_time$i";
    $$b_start_time_ver = format_time(pg_fetch_result($sel, 0, "$b_start_time_ver"));
    $$b_end_time_ver = format_time(pg_fetch_result($sel, 0, "$b_end_time_ver"));
    $$a_start_time_ver = format_time(pg_fetch_result($sel, 0, "$a_start_time_ver"));
    $$a_end_time_ver = format_time(pg_fetch_result($sel, 0, "$a_end_time_ver"));
}
for ($i = 3; $i <= 5; $i++) {
    $b_start_flag_ver = "b_over_start_next_day_flag$i";
    $b_end_flag_ver = "b_over_end_next_day_flag$i";
    $a_start_flag_ver = "a_over_start_next_day_flag$i";
    $a_end_flag_ver = "a_over_end_next_day_flag$i";
    $$b_start_flag_ver = format_time(pg_fetch_result($sel, 0, "$b_start_flag_ver"));
    $$b_end_flag_ver = format_time(pg_fetch_result($sel, 0, "$b_end_flag_ver"));
    $$a_start_flag_ver = format_time(pg_fetch_result($sel, 0, "$a_start_flag_ver"));
    $$a_end_flag_ver = format_time(pg_fetch_result($sel, 0, "$a_end_flag_ver"));
}
for ($i = 1; $i <= 5; $i++) {
    $b_start_time_ver = "b_rest_start_time$i";
    $b_end_time_ver = "b_rest_end_time$i";
    $a_start_time_ver = "a_rest_start_time$i";
    $a_end_time_ver = "a_rest_end_time$i";
    $$b_start_time_ver = format_time(pg_fetch_result($sel, 0, "$b_start_time_ver"));
    $$b_end_time_ver = format_time(pg_fetch_result($sel, 0, "$b_end_time_ver"));
    $$a_start_time_ver = format_time(pg_fetch_result($sel, 0, "$a_start_time_ver"));
    $$a_end_time_ver = format_time(pg_fetch_result($sel, 0, "$a_end_time_ver"));
}
for ($i = 1; $i <= 5; $i++) {
    $b_start_flag_ver = "b_rest_start_next_day_flag$i";
    $b_end_flag_ver = "b_rest_end_next_day_flag$i";
    $a_start_flag_ver = "a_rest_start_next_day_flag$i";
    $a_end_flag_ver = "a_rest_end_next_day_flag$i";
    $$b_start_flag_ver = format_time(pg_fetch_result($sel, 0, "$b_start_flag_ver"));
    $$b_end_flag_ver = format_time(pg_fetch_result($sel, 0, "$b_end_flag_ver"));
    $$a_start_flag_ver = format_time(pg_fetch_result($sel, 0, "$a_start_flag_ver"));
    $$a_end_flag_ver = format_time(pg_fetch_result($sel, 0, "$a_end_flag_ver"));
}
//tmmdapplyをまとめて取得
$arr_tmmdapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);

//前日チェックがあるかフラグ
$b_previous_day_flag_value = "";
$a_previous_day_flag_value = "";
if ($b_previous_day_flag == 1){
	$b_previous_day_flag_value = "　前日";
}

if ($a_previous_day_flag == 1){
	$a_previous_day_flag_value = "　前日";
}

//翌日チェックがあるかフラグ
$b_next_day_flag_value = "";
$a_next_day_flag_value = "";
if ($b_next_day_flag == 1){
	$b_next_day_flag_value = "　翌日";
}

if ($a_next_day_flag == 1){
	$a_next_day_flag_value = "　翌日";
}

$b_over_start_next_day_flag_value = "";
$a_over_start_next_day_flag_value = "";
if ($b_over_start_next_day_flag == 1){
	$b_over_start_next_day_flag_value = "　翌日";
}

if ($a_over_start_next_day_flag == 1){
	$a_over_start_next_day_flag_value = "　翌日";
}
$b_over_end_next_day_flag_value = "";
$a_over_end_next_day_flag_value = "";
if ($b_over_end_next_day_flag == 1){
	$b_over_end_next_day_flag_value = "　翌日";
}

if ($a_over_end_next_day_flag == 1){
	$a_over_end_next_day_flag_value = "　翌日";
}
$b_over_start_next_day_flag2_value = "";
$a_over_start_next_day_flag2_value = "";
if ($b_over_start_next_day_flag2 == 1){
	$b_over_start_next_day_flag2_value = "　翌日";
}

if ($a_over_start_next_day_flag2 == 1){
	$a_over_start_next_day_flag2_value = "　翌日";
}
$b_over_end_next_day_flag2_value = "";
$a_over_end_next_day_flag2_value = "";
if ($b_over_end_next_day_flag2 == 1){
	$b_over_end_next_day_flag2_value = "　翌日";
}

if ($a_over_end_next_day_flag2 == 1){
	$a_over_end_next_day_flag2_value = "　翌日";
}
for ($i=3; $i<=5; $i++) {
    $bsf = "b_over_start_next_day_flag".$i;
    $asf = "a_over_start_next_day_flag".$i;
    $bef = "b_over_end_next_day_flag".$i;
    $aef = "a_over_end_next_day_flag".$i;
    $bsfv = "b_over_start_next_day_flag".$i."_value";
    $asfv = "a_over_start_next_day_flag".$i."_value";
    $befv = "b_over_end_next_day_flag".$i."_value";
    $aefv = "a_over_end_next_day_flag".$i."_value";
    
    $$bsfv = "";
    $$asfv = "";
    $$befv = "";
    $$aefv = "";
    if ($$bsf == 1){
        $$bsfv = "　翌日";
    }
    if ($$asf == 1){
        $$asfv = "　翌日";
    }
    if ($$bef == 1){
        $$befv = "　翌日";
    }
    if ($$aef == 1){
        $$aefv = "　翌日";
    }
    
}
//休憩
if ($ovtm_class->ovtm_rest_disp_flg == "t") {
    for ($i=1; $i<=5; $i++) {
        $bsf = "b_rest_start_next_day_flag".$i;
        $asf = "a_rest_start_next_day_flag".$i;
        $bef = "b_rest_end_next_day_flag".$i;
        $aef = "a_rest_end_next_day_flag".$i;
        $bsfv = "b_rest_start_next_day_flag".$i."_value";
        $asfv = "a_rest_start_next_day_flag".$i."_value";
        $befv = "b_rest_end_next_day_flag".$i."_value";
        $aefv = "a_rest_end_next_day_flag".$i."_value";
        
        $$bsfv = "";
        $$asfv = "";
        $$befv = "";
        $$aefv = "";
        if ($$bsf == 1){
            $$bsfv = "　翌日";
        }
        if ($$asf == 1){
            $$asfv = "　翌日";
        }
        if ($$bef == 1){
            $$befv = "　翌日";
        }
        if ($$aef == 1){
            $$aefv = "　翌日";
        }
        
    }
}

// 申請者ユーザの出勤グループを取得
$tmcd_group_id = get_timecard_group_id($con, $apply_emp_id, $fname);

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);

$b_group_name = ($b_tmcd_group_id == "") ? "": $atdbk_common_class->get_group_name($b_tmcd_group_id);
$a_group_name = ($a_tmcd_group_id == "") ? "": $atdbk_common_class->get_group_name($a_tmcd_group_id);

$b_pattern_name = ($b_pattern == "") ? "": $atdbk_common_class->get_pattern_name($b_tmcd_group_id, $b_pattern);
$a_pattern_name = ($a_pattern == "") ? "": $atdbk_common_class->get_pattern_name($a_tmcd_group_id, $a_pattern);

$b_reason_name = ($b_reason == "") ? "": $atdbk_common_class->get_reason_name($b_reason);
$a_reason_name = ($a_reason == "") ? "": $atdbk_common_class->get_reason_name($a_reason);

$b_night_duty_name = "";
if($b_night_duty == "1")
{
    $b_night_duty_name = "有り";
}
else if($b_night_duty == "2")
{
    $b_night_duty_name = "無し";
}	

$a_night_duty_name = "";
if($a_night_duty == "1")
{
    $a_night_duty_name = "有り";
}
else if($a_night_duty == "2")
{
    $a_night_duty_name = "無し";
}


$b_allow_id_name = "";
$a_allow_id_name = "";
$b_allow_id_name2 = "";
$a_allow_id_name2 = "";
$b_allow_id_name3 = "";
$a_allow_id_name3 = "";
foreach($arr_allowance as $allowance)
{
	if($allowance["allow_id"] == $b_allow_id) {
		$b_allow_id_name = $allowance["allow_contents"];
	}
	if($allowance["allow_id"] == $a_allow_id) {
		$a_allow_id_name = $allowance["allow_contents"];
	}
	if($allowance["allow_id"] == $b_allow_id2) {
		$b_allow_id_name2 = $allowance["allow_contents"];
	}
	if($allowance["allow_id"] == $a_allow_id2) {
		$a_allow_id_name2 = $allowance["allow_contents"];
	}
	if($allowance["allow_id"] == $b_allow_id3) {
		$b_allow_id_name3 = $allowance["allow_contents"];
	}
	if($allowance["allow_id"] == $a_allow_id3) {
		$a_allow_id_name3 = $allowance["allow_contents"];
	}
}

//職員IDと日付から残業データ取得 20150108
$arr_ovtmapply = array();
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	$arr_ovtmapply = $ovtm_class->get_ovtmapply_emp_date($apply_emp_id, $target_date);
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
window.resizeTo(1024, 740);

function history_select(apply_id, target_apply_id) {

	document.mainform.apply_id.value = apply_id;
	document.mainform.target_apply_id.value = target_apply_id;
	document.mainform.action="timecard_modify_refer.php";
	document.mainform.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>勤務時間修正申請参照</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<form name="mainform" action="" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="25%">
<? show_timecard_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" align="center" width="75%">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="184" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($apply_emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<?
	//申請情報の日付を年月にした場合に、一括修正とする 20100219
if (strlen($target_date) == 8) {
?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_group_name, $a_group_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_pattern_name, $a_pattern_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_reason_name, $a_reason_name); ?></font></td>
</tr>
<?php
if ($ovtm_class->duty_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_night_duty_name, $a_night_duty_name); ?></font></td>
</tr>
<?php } ?>
<?php
if ($ovtm_class->allowance_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? show_value_diff($b_allow_id_name.$b_allow_count_value, $a_allow_id_name.$a_allow_count_value); ?>&nbsp;
<? show_value_diff($b_allow_id_name2.$b_allow_count_value2, $a_allow_id_name2.$a_allow_count_value2); ?>&nbsp;
<? show_value_diff($b_allow_id_name3.$b_allow_count_value3, $a_allow_id_name3.$a_allow_count_value3); ?>
</font></td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_start_time.$b_previous_day_flag_value, $a_start_time.$a_previous_day_flag_value); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_end_time.$b_next_day_flag_value, $a_end_time.$a_next_day_flag_value); ?></font></td>
</tr>
    <?
    //休憩
    if ($ovtm_class->rest_disp_flg == "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩開始時刻</font></td>
<td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<? show_value_diff($b_rest_start_time, $a_rest_start_time); ?>
	</font>
</td>
<td width="110" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩終了時刻</font></td>
<td>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<? show_value_diff($b_rest_end_time, $a_rest_end_time); ?>
	</font>
</td>
</tr>
<? } ?>
<?php
if ($ovtm_class->out_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_out_time, $a_out_time); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">復帰時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_ret_time, $a_ret_time); ?></font></td>
</tr>
<?php } ?>
    <?
    //残業追加タブを表示する場合、打刻データを表示
    if ($ovtm_class->ovtm_tab_flg == "t") {
        $arr_result = get_atdbkrslt($con, $apply_emp_id, $target_date, $fname);
        $ovtm_class->disp_btn_time_data($arr_result, "approve");
    }
?>
    <?
    $maxline = ($ovtm_class->ovtm_tab_flg == "t") ? 5 : 2;
    for ($i=1; $i<=$maxline; $i++) {
        $idx = ($i == 1) ? "" : $i;
        $b_start_time_ver = "b_over_start_time$idx";
        $b_end_time_ver = "b_over_end_time$idx";
        $a_start_time_ver = "a_over_start_time$idx";
        $a_end_time_ver = "a_over_end_time$idx";
        $b_start_flag_ver = "b_over_start_next_day_flag{$idx}_value";
        $b_end_flag_ver = "b_over_end_next_day_flag{$idx}_value";
        $a_start_flag_ver = "a_over_start_next_day_flag{$idx}_value";
        $a_end_flag_ver = "a_over_end_next_day_flag{$idx}_value";
        
    ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時刻<? echo($i); ?></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($$b_start_time_ver.$$b_start_flag_ver, $$a_start_time_ver.$$a_start_flag_ver); ?>　
<? show_value_diff($$b_end_time_ver.$$b_end_flag_ver, $$a_end_time_ver.$$a_end_flag_ver); ?>
        <?
        //休憩
        if ($ovtm_class->ovtm_rest_disp_flg == "t") {
            $b_rest_start_time_ver = "b_rest_start_time$i";
            $b_rest_end_time_ver = "b_rest_end_time$i";
            $a_rest_start_time_ver = "a_rest_start_time$i";
            $a_rest_end_time_ver = "a_rest_end_time$i";
            $b_rest_start_flag_ver = "b_rest_start_next_day_flag{$i}_value";
            $b_rest_end_flag_ver = "b_rest_end_next_day_flag{$i}_value";
            $a_rest_start_flag_ver = "a_rest_start_next_day_flag{$i}_value";
            $a_rest_end_flag_ver = "a_rest_end_next_day_flag{$i}_value";
?>
　休憩(<? show_value_diff($$b_rest_start_time_ver.$$b_rest_start_flag_ver, $$a_rest_start_time_ver.$$a_rest_start_flag_ver); ?>　
<? show_value_diff($$b_rest_end_time_ver.$$b_rest_end_flag_ver, $$a_rest_end_time_ver.$$a_rest_end_flag_ver); ?>)

            <?
        }
        //残業理由 20150108
        if ($ovtm_class->ovtm_reason2_input_flg == "t") {
        	$reason_val = $ovtm_class->get_ovtmrsn_reason($arr_ovtmapply["reason_id".$idx]);
			$other_reason_val = $arr_ovtmapply["reason".$idx];
			echo("&nbsp;");
	        echo($reason_val . $other_reason_val);
		}
        ?>
        </font></td>
        </tr>
        <?
    }  
    $arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($apply_emp_id, $target_date, "refer");
    $ovtm_class->disp_ovtm_total_time($arr_ovtm_data, "1");
?>
    <?
//時間有休追加 20111207 
	if ($obj_hol_hour->paid_hol_hour_flag == "t" &&
		$ovtm_class->hol_hour_input_nodisp_flg != "t") {
		$duty_form = $obj_hol_hour->get_duty_form($apply_emp_id);
		if (($duty_form == "1" && $obj_hol_hour->paid_hol_hour_full_flag == "t") ||
				($duty_form == "2" && $obj_hol_hour->paid_hol_hour_part_flag == "t")) {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休開始時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_paid_hol_hour_start_time, $a_paid_hol_hour_start_time); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時間有休取得時間</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_paid_hol_use_time, $a_paid_hol_use_time); ?></font></td>
</tr>
			<?
		}
	}
?>

	<?
for ($i = 1; $i <= 10; $i++) {
	$b_start_time_ver = "b_o_start_time$i";
	$b_end_time_ver = "b_o_end_time$i";
	$a_start_time_ver = "a_o_start_time$i";
	$a_end_time_ver = "a_o_end_time$i";

	if ($$b_start_time_ver == "" && $$b_end_time_ver == "" && $$a_start_time_ver == "" && $$a_end_time_ver == "") {
		break;
	}

	echo("<tr height=\"22\">\n");
		echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}時刻{$i}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	show_term_diff($$b_start_time_ver, $$b_end_time_ver, $$a_start_time_ver, $$a_end_time_ver);
	echo("</font></td>\n");
	echo("</tr>\n");
}

if ($ovtm_class->meeting_input_nodisp_flg != "t") {
?>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務開始時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	show_value_diff($b_meeting_start_time, $a_meeting_start_time);
?>
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務終了時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	show_value_diff($b_meeting_end_time, $a_meeting_end_time);
?>
</font></td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">修正理由</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reason . $other_reason); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $comment)); ?></font></td>

<? } else { ?>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<br>
一括修正申請です。
<br>
<br>
</font>
</td>
<? } ?>

</tr>

<?
$page = "timecard_refer";
show_application_apply_detail($con, $session, $fname, $target_apply_id, $page, false, $timecard_bean, $wherefrom);
?>

</table>
</td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">

</form>
</center>
</body>
</html>

<? 
$approve_cnt = 0;
$show_flg_name = "";
if($mode == 'show_flg_update') {

	$sql = "select apply_status, apv_fix_show_flg, apv_ng_show_flg, apv_bak_show_flg from tmmdapply";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_status = pg_fetch_result($sel, 0, "apply_status");
	$apv_fix_show_flg = pg_fetch_result($sel, 0, "apv_fix_show_flg");
	$apv_ng_show_flg = pg_fetch_result($sel, 0, "apv_ng_show_flg");
	$apv_bak_show_flg = pg_fetch_result($sel, 0, "apv_bak_show_flg");

	if ($apply_status == "1" && $apv_fix_show_flg == "t") {
		$approve_cnt = 1;
		$show_flg_name = "apv_fix_show_flg";
	}

	if ($apply_status == "2" && $apv_ng_show_flg == "t") {
		$approve_cnt = 1;
		$show_flg_name = "apv_ng_show_flg";
	}

	if ($apply_status == "3" && $apv_bak_show_flg == "t") {
		$approve_cnt = 1;
		$show_flg_name = "apv_bak_show_flg";
	}

	if($approve_cnt > 0) {

		// トランザクションを開始
		pg_query($con, "begin");
		
		$sql = "update tmmdapply set";
		$set = array($show_flg_name);
		$setvalue = array('f');
		$cond = "where apply_id = $apply_id and $show_flg_name = 't' and apply_status = '$apply_status'";	
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	
		// トランザクションをコミット
		pg_query($con, "commit");
	}
}

pg_close($con);
?>

<script type="text/javascript">
function page_close() {
<?
if($mode == 'show_flg_update' && $approve_cnt > 0) {
?>	
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?	
} else {
?>	
	window.close();
<?
}
?> 

}
</script>


<?
function show_value_diff($before, $after) {
	if ($before == "") {$before = "（登録なし）";}
	if ($after == "") {$after = "（登録なし）";}
	if ($after != $before) {
		echo("$before <font color=\"red\">→ $after</font>");
	} else {
		echo($before);
	}
}

function show_term_diff($before_start, $before_end, $after_start, $after_end) {
	if ($before_start == "") {$before_start = "（登録なし）";}
	if ($before_end == "") {$before_end = "（登録なし）";}
	if ($after_start == "") {$after_start = "（登録なし）";}
	if ($after_end == "") {$after_end = "（登録なし）";}
	show_value_diff("$before_start 〜 $before_end", "$after_start 〜 $after_end");
}

function get_reason_string($reason) {
	$reasons = array(
		"14" => "代替出勤",
		"15" => "振替出勤",
		"16" => "休日出勤",
		"1" => "有給休暇",
		"2" => "午前有休",
		"3" => "午後有休",
		"4" => "代替休暇",
		"17" => "振替休暇",
		"5" => "特別休暇",
		"6" => "一般欠勤",
		"7" => "病傷欠勤",
		"8" => "その他休",
		"9" => "通院",
		"10" => "私用",
		"11" => "交通遅延",
		"12" => "遅刻",
		"13" => "早退",
		"18" => "半前代替休",
		"19" => "半前代替休",
		"20" => "半前振替休",
		"21" => "半後振替休"
	);
	return $reasons[$reason];
}

function format_time($hhmm) {
	return preg_replace("/(\d{2})(\d{2})/", "$1:$2", $hhmm);
}
?>
