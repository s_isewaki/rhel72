<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

$session = qualify_session($session, $fname);
if ($session == "0") {
    js_login_exit();
}

$checkauth = check_authority($session, 9, $fname);
if ($checkauth == "0") {
    js_login_exit();
}

// 入力チェック
if ($group_name == "") {
	js_alert_exit("メール送信グループ名が入力されていません。");
}

$con = connect2db($fname);

// メール送信グループ名を更新
$sql = "update adbkgrp set";
$set = array("name");
$setvalue = array($group_name);
$cond = "where id = $group_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	js_error_exit();
}

pg_close($con);

// 親画面をリフレッシュし、自画面を閉じる
echo("<script type=\"text/javascript\">opener.location.href = 'address_group.php?session=$session&group_id=$group_id';</script>");
echo("<script type=\"text/javascript\">self.close();</script>");
