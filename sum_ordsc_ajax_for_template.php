<?
ob_start();
$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");
//require_once("tmpl_common.ini");
ob_end_clean();

$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 59, $fname);
$con = @connect2db($fname);
if(!$session || !$con){
  echo('{"error":"initial_error"}');
  exit;
}


//******************************************************************************
// 引数
//******************************************************************************

// ツリービュー用の薬効の階層番号(1から3)
$yakkou_bunrui_kaisou = (int)mb_convert_encoding(urldecode(urldecode(@$_REQUEST["yakkou_bunrui_kaisou"])), "EUC-JP", "UTF-8");
$yakkou_bunrui_code1 = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["yakkou_bunrui_code1"])), "EUC-JP", "UTF-8");
$yakkou_bunrui_code2 = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["yakkou_bunrui_code2"])), "EUC-JP", "UTF-8");
$yakkou_bunrui_code3 = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["yakkou_bunrui_code3"])), "EUC-JP", "UTF-8");

// 検索範囲："hist"(投与歴) "saiyo"(採用薬) "allmed"(全薬品)
$med_extent = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["med_extent"])), "EUC-JP", "UTF-8");

// 検索範囲が"hist"のときの診療科コード（本人なら"P"、全科なら"A"となる)
$med_extent_hist_option = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["med_extent_hist_option"])), "EUC-JP", "UTF-8");
// 検索範囲が"hist"かつ本人("P")のときに必要な、患者ID
$pt_id = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["pt_id"])), "EUC-JP", "UTF-8");

// ツリーのときは"treetype" 通常は"tabletype"
$list_type = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["list_type"])), "EUC-JP", "UTF-8");
// テンプレート右上の、検索条件の薬剤名
$med_name = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["med_name"])), "EUC-JP", "UTF-8");
// テンプレート右上の、成分分類ラジオボタン("N":内服 "G":外用 "A":全て(注射を除く) "C":注射)
$seibun_bunrui = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["seibun_bunrui"])), "EUC-JP", "UTF-8");

// セットに対する操作： "list" or "regist" or "update" or "delete"
$set_request = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["set_request"])), "EUC-JP", "UTF-8");
// セットのリスト取得の場合の、セット部分名
$set_name = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["set_name"])), "EUC-JP", "UTF-8");
// セットのリスト取得の場合の、検索診療科コード（医師本人なら"D"、全科なら"A")
$set_extent_option = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["set_extent_option"])), "EUC-JP", "UTF-8");
// 検索範囲が医師本人("D")のときに必要な、ドクターのログインID
$login_emp_id = get_emp_id($con, $session, $fname);

// セットの新規作成/更新時に値が存在
$new_set_name = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["new_set_name"])), "EUC-JP", "UTF-8");
// セットの新規作成/更新時に値が存在 検索診療科コード（本人なら"P"、全科なら"A")
$new_set_extent_option = mb_convert_encoding(urldecode(urldecode(@$_REQUEST["new_set_extent_option"])), "EUC-JP", "UTF-8");
// セットの削除用
$set_seq = (int)mb_convert_encoding(urldecode(urldecode(@$_REQUEST["set_seq"])), "EUC-JP", "UTF-8");

// セットの登録時、RPデータがごそっと以下のパラメータに含まれてやってくる。
// このデータを構造化している、sot_util.php convert_ordsc_rp_dump_from_request_to_array()も参照のこと。
// ordsc_basedata_stream
// ordsc_rpdata_stream

// オーダのロード時（通常オーダ）
$load_request_schedule_seq = (int)mb_convert_encoding(urldecode(urldecode(@$_REQUEST["load_request_schedule_seq"])), "EUC-JP", "UTF-8");
// オーダのロード時（セット）
$load_request_set_seq = (int)mb_convert_encoding(urldecode(urldecode(@$_REQUEST["load_request_set_seq"])), "EUC-JP", "UTF-8");
// 薬品検索時に「別名・備考欄」も検索するか
$is_med_search_remark = @$_REQUEST["is_med_search_remark"] == "true" ? true : false;


//******************************************************************************
// セット一覧検索
//******************************************************************************
if ($set_request == "list"){
	$sql  = " select set_seq, set_name from sum_order_syoho_cyusya where 1=1 and set_seq > 0";
	if ($set_name)          $sql .= " and set_name like '%"     . pg_escape_string($set_name)         . "%'";
	if      ($set_extent_option=="A") { $sql .= " and set_target = 'A'"; }
	else if ($set_extent_option=="D") { $sql .= " and set_target = 'D' and regist_emp_id = '" . pg_escape_string($login_emp_id) . "'"; }
	else if ($set_extent_option!="")  { $sql .= " and set_target = 'S' and sect_id = " . (int)$set_extent_option; }
	else { $sql .= " and 1 <> 1"; }
	$sql .= " order by set_seq";
	$sel = select_from_table($con, $sql, "", $fname);
	$out = array();
	while ($row = pg_fetch_array($sel)){
		$out[] = '{"set_seq":'.$row["set_seq"].',"set_name":"'.str_replace('"','\"',$row["set_name"]).'"}';
	}
	echo '{"set_request":"list","list":['.implode(",",$out).']}';
	die;
}








//******************************************************************************
// セット削除
//******************************************************************************
else if ($set_request=="delete"){
	$ret = update_set_table($con, "delete from sum_order_syoho_cyusya where set_seq = ".$set_seq, array(), null, "", $fname);
	$ret = update_set_table($con, "delete from sum_order_syoho_cyusya_rp where set_seq = ".$set_seq, array(), null, "", $fname);
	echo '{"set_request":"delete","result":"ok"}';
	die;
}










//******************************************************************************
// セット登録
//******************************************************************************
else if ($set_request=="regist"){

	$sel = select_from_table($con, "select max(set_seq) as m from sum_order_syoho_cyusya", "", $fname);
	$new_set_seq = ((int)pg_fetch_result($sel, 0, 0)) + 1;

	$set_target = "";
	$sect_id = 0;
	if      ($new_set_extent_option=="A") { $set_target = "A"; }
	else if ($new_set_extent_option=="D") { $set_target = "D"; }
	else if ($new_set_extent_option!="")  { $set_target = "S"; $sect_id = (int)$new_set_extent_option; }

	$req = $c_sot_util->convert_ordsc_rp_dump_from_request_to_array();
	$req_base = $req["base"];
	$req_rp   = $req["rp"];

	$req_base["set_target"] = $set_target;
	$req_base["set_name"] = $new_set_name;
	$req_base["order_ymd"] = "";
	$req_base["byoumei"] = "";
	$req_base["byoumei_code"] = "";
	$req_base["icd10"] = "";
	$req_base["enti_id"] = "0";
	$req_base["sect_id"] = $sect_id;
	$req_base["regist_emp_id"] = $login_emp_id;

	$c_sot_util->regist_ordsc_from_request($con, $fname, $req_base, $req_rp, 0, 0, $new_set_seq);

	// AJAX出力
	echo '{"set_request":"regist","result":"ok","set_seq":"'.$new_set_seq.'"}';
	die;
}



//******************************************************************************
// オーダ検索
//******************************************************************************
else if ($load_request_schedule_seq || $load_request_set_seq){
	$where = $load_request_schedule_seq ? "schedule_seq" : "set_seq";
	$key = $load_request_schedule_seq ? $load_request_schedule_seq : $load_request_set_seq;

	$sel = select_from_table($con, "select * from sum_order_syoho_cyusya where ".$where." = " . $key, "", $fname);
	$row = pg_fetch_array($sel);
	$order_params = array();
	foreach($row as $field => $value){
		if ((int)$field === $field) continue;
		$order_params[] = '"'.$field.'":"'.str_replace('"','\"',$value).'"';
	}

	$sql =
		" select rp.*, med.yj_cd, sai.seibun_bunrui, sai.zaikei_bunrui, sai.remark, sai.units_separated_tab from sum_order_syoho_cyusya_rp rp".
		" left outer join sum_med_saiyou_mst sai on (sai.seq::text = rp.med_seq_cd)".
		" left outer join sum_medmst med on (sai.hot13_cd = med.hot13_cd)".
		" where rp.".$where." = " . $key .
		" order by rp.rp_seq, rp.rp_line_seq";
	$sel = select_from_table($con, $sql, "", $fname);

	$rp_lines = array();
	while ($rows = pg_fetch_array($sel)){
		$rp_params = array();
		foreach($rows as $f => $v){
			if ((int)$f === $f) continue;
			$rp_params[] = '"'.$f.'":"'.str_replace('"','\"',$v).'"';
		}
		$rp_lines[] = "{".implode($rp_params,",")."}";
	}
	// AJAX出力
	echo '{"where":"'.$where.'","key":'.$key.',"order":{'.implode($order_params,",").'},"rp":['.implode($rp_lines,",").']}';
	die;
}










//******************************************************************************
// 薬品一覧検索（通常またはツリー）
//******************************************************************************


//=========================
// ツリーのとき、かつ最下薬品階層検索時は、各階層の値から、薬効分類コードを求める
//=========================
$yakkou_bunrui_cd = "";
if ($yakkou_bunrui_kaisou==4){
	$sql =
		" select yakkou_bunrui_cd from sum_med_yakkou_bunrui3_mst".
		" where 1 = 1".
		" and code1 = '" . pg_escape_string($yakkou_bunrui_code1) . "'".
		" and code2 = '" . pg_escape_string($yakkou_bunrui_code2) . "'".
		" and code3 = '" . pg_escape_string($yakkou_bunrui_code3) . "'";
	$sel = select_from_table($con, $sql, "", $fname);
	$yakkou_bunrui_cd = pg_fetch_result($sel, 0, 0);
}

//=========================
// 採用薬マスタの検索SQLを作成しておく
//=========================
$sql_saiyou_mst  = " select max(seq) as seq, med_name" .
                   ", yj_cd" .
                   ", remark" .
                   ", seibun_bunrui" .
                   ", zaikei_bunrui" .
                   ", yakkou_bunrui_cd" .
                   ", is_visible_at_selector" .
                   ", used_counter" .
                   ", units_separated_tab" .
                   " from sum_med_saiyou_mst where is_visible_at_selector = 1";

if ($med_name) {    // 薬剤名
    // 2文字で検索する場合、末尾に「*」が付加されているので削除する
    $last_str = mb_substr($med_name, -1, 1, "EUC-JP");
    if (strcmp($last_str, "*") === 0 || strcmp($last_str, "＊") === 0) {
        $med_name = mb_substr($med_name, 0, mb_strlen($med_name, "EUC-JP") - 1, "EUC-JP");
    }
    
    $med_name_escaped = pg_escape_string($med_name);
    if ($is_med_search_remark) {    // 「別名・備考欄」も検索する指定があった場合
        $sql_saiyou_mst .= " and (med_name like '%$med_name_escaped%' or remark like '%$med_name_escaped%')";
    } else {
        $sql_saiyou_mst .= " and med_name like '%$med_name_escaped%'";
    }
}
if ($yakkou_bunrui_cd) {    // 薬効分類コード
    $sql_saiyou_mst .= " and yakkou_bunrui_cd = '" . pg_escape_string($yakkou_bunrui_cd) . "'";
}
if ($seibun_bunrui) {    // 成分分類
    if ($seibun_bunrui!="A") $sql_saiyou_mst .= " and seibun_bunrui = '" . pg_escape_string($seibun_bunrui) . "'";
//    else $sql_saiyou_mst .= " and seibun_bunrui <> 'C'";    // 処方箋薬品検索時に「全て」で検索すると「注射」分が除外されてしまうので、削除
}
if ($med_extent == "saiyo") $sql_saiyou_mst .= " and used_counter <> 0";
$sql_saiyou_mst .= " group by med_name" .
                   ", yj_cd" .
                   ", remark" .
                   ", seibun_bunrui" .
                   ", zaikei_bunrui" .
                   ", yakkou_bunrui_cd" .
                   ", is_visible_at_selector" .
                   ", used_counter " .
                   ", units_separated_tab ";


//=========================
// 履歴参照の場合は、処方注射テーブルとスケジュールテーブルをJOIN
//=========================
if ($med_extent == "hist") {
    $and_pt_id   = "";
    $and_ptif_id = "";
    $and_sect_id = "";
    if ($med_extent_hist_option=="P") {
        $and_pt_id   = " and pt_id = '" . pg_escape_string($pt_id) . "'";
        $and_ptif_id = " and ptif_id = '" . pg_escape_string($pt_id) . "'";
    }
    else if ($med_extent_hist_option != "A" && $med_extent_hist_option != "") {
        $and_sect_id = " and sect_id = " . ((int)$med_extent_hist_option);
    }

    $sql  = " select sai.*, tran.med_unit_name from (" .
            " select rp.med_seq_cd, rp.med_unit_name from (" .
            " select sss.schedule_seq from sot_summary_schedule sss" .
            " inner join (" .
            " select summary_id from summary" .
            " where 1 = 1" .
              $and_ptif_id .
              $and_sect_id .
            " ) sum on (sum.summary_id = sss.summary_id)" .
            " where sss.delete_flg = 'f'" .
            " and sss.temporary_update_flg = 0" .
              $and_ptif_id .
            " ) sss" .
            " inner join (" .
            " select schedule_seq from sum_order_syoho_cyusya" .
            " where set_seq = 0" .
              $and_pt_id .
            " ) ord on (ord.schedule_seq = sss.schedule_seq)" .
            " inner join (" .
            " select schedule_seq, med_seq_cd, med_unit_name from sum_order_syoho_cyusya_rp" .
            " where set_seq = 0" .
            " and rp_line_div = 'M'" .
            " ) rp on (rp.schedule_seq = ord.schedule_seq)" .
            " group by rp.med_seq_cd, rp.med_unit_name" .
            " ) tran" .
            " inner join (" . $sql_saiyou_mst . ") sai on (sai.seq::text = tran.med_seq_cd)";
    $sql_saiyou_mst = $sql;
}


//=========================
// 一覧(1) 薬効分類マスタ検索の場合
//=========================
if ($yakkou_bunrui_kaisou > 0 && $yakkou_bunrui_kaisou <= 3){

    $sql = " select ym1.code1, ym1.sort_order";
    if ($yakkou_bunrui_kaisou > 1) $sql  .= ", ym2.sort_order, ym2.code2";
    if ($yakkou_bunrui_kaisou > 2) $sql  .= ", ym3.sort_order, ym3.code3";
    if ($yakkou_bunrui_kaisou == 1) $sql .= ", ym1.caption";
    if ($yakkou_bunrui_kaisou == 2) $sql .= ", ym2.caption";
    if ($yakkou_bunrui_kaisou == 3) $sql .= ", ym3.caption, ym3.yakkou_bunrui_cd";
    $sql .= ", count(seq) as cnt" .
            " from (" .
            " select * from sum_med_yakkou_bunrui1_mst where is_disabled = 0" .
            (($yakkou_bunrui_kaisou > 1) ? " and code1 = '" . pg_escape_string($yakkou_bunrui_code1) . "'" : "") .
            " ) ym1" .
            " inner join (" .
            " select * from sum_med_yakkou_bunrui2_mst where is_disabled = 0" .
            (($yakkou_bunrui_kaisou > 2) ? " and code2 = '" . pg_escape_string($yakkou_bunrui_code2) . "'" : "") .
            " ) ym2 on (ym2.code1 = ym1.code1)" .
            " inner join (" .
            " select * from sum_med_yakkou_bunrui3_mst where is_disabled = 0" .
            " ) ym3 on (ym3.code1 = ym1.code1 and ym3.code2 = ym2.code2)" .
            " inner join (" .
              $sql_saiyou_mst .
            " ) sai on (sai.yakkou_bunrui_cd = ym3.yakkou_bunrui_cd)";

    $sql .= " group by ym1.code1, ym1.sort_order";
    if ($yakkou_bunrui_kaisou > 1)  $sql .= ",ym2.sort_order, ym2.code2";
    if ($yakkou_bunrui_kaisou > 2)  $sql .= ",ym3.sort_order, ym3.code3";
    if ($yakkou_bunrui_kaisou == 1) $sql .= ",ym1.caption";
    if ($yakkou_bunrui_kaisou == 2) $sql .= ",ym2.caption";
    if ($yakkou_bunrui_kaisou == 3) $sql .= ",ym3.caption, ym3.yakkou_bunrui_cd";
    $sql .= " order by ym1.sort_order";
    if ($yakkou_bunrui_kaisou > 1)  $sql .= ",ym2.sort_order";
    if ($yakkou_bunrui_kaisou > 2)  $sql .= ",ym3.sort_order";

    $sel = select_from_table($con, $sql, "", $fname);
    $out = array();
    while ($row = pg_fetch_assoc($sel)) {
        $out[] = '{"code1":"' . @$row["code1"] . '", "code2":"' . @$row["code2"] . '", "code3":"' . @$row["code3"] . '", "yakkou_bunrui_cd":"' . @$row["yakkou_bunrui_cd"].'", "caption":"' . str_replace('"', '\"', $row["caption"]) . '", "medCount":"' . @$row["cnt"] . '"}';
    }
}

//=========================
// 一覧(2) 採用薬マスタの検索の場合
//=========================
else {
    $sql = "select distinct sai.*, med.yj_cd from " .
           "(" . $sql_saiyou_mst . ") sai " .
           "left join sum_medmst med on (sai.yj_cd = med.yj_cd) ";
    if (!$yakkou_bunrui_cd) $sql .= " limit 100"; // 薬効が指定されていなければ、100件までとする
    $sel = select_from_table($con, $sql, "", $fname);
    $out = array();
    while ($row = pg_fetch_assoc($sel)){
        if ($row['yj_cd'] != '') {
            $out[] = '{"seq":' . $row["seq"] . ', "med_name":"' . str_replace('"', '\"', $row["med_name"]) . '", "yj_cd":"' . str_replace('"', '\"', $row["yj_cd"]) . '", "med_unit_name":"' . str_replace('"', '\"', @$row["med_unit_name"]) . '", "remark":"' . str_replace('"', '\"', $row["remark"]) . '", "seibun_bunrui":"' . $row["seibun_bunrui"] . '", "zaikei_bunrui":"' . $row["zaikei_bunrui"] . '", "units_separated_tab":"' . str_replace(" ", "", $row["units_separated_tab"]) . '"}';
        }
    }
}

//=========================
// AJAX出力
//=========================
echo '{"list_type":"'.$list_type.'","yakkou_bunrui_kaisou":'.$yakkou_bunrui_kaisou.',"code1":"'.$yakkou_bunrui_code1.'","code2":"'.$yakkou_bunrui_code2.'","code3":"'.$yakkou_bunrui_code3.'","list":['.implode(",",$out).']}';
?>
