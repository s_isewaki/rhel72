<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 職員登録 | メニューグループ</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("label_by_profile_type.ini");
require_once("get_cas_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を取得
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel_job = select_from_table($con, $sql, $cond, $fname);
if ($sel_job == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 業務機能の使用可否を取得
$sql = "select * from license";
$cond = "";
$sel_func = select_from_table($con, $sql, $cond, $fname);
if ($sel_func == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lcs_func = array();
for ($i = 1; $i <= 60; $i++) {
	$lcs_func[$i] = pg_fetch_result($sel_func, 0, "lcs_func$i");
}

// グループ一覧を配列に格納

$sql = "select group_id, group_nm from menugroup";
$cond = "order by group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$groups = array();
while ($row = pg_fetch_array($sel)) {
	$groups[$row["group_id"]] = $row["group_nm"];
}
if ($group_id == "") {
	$group_id = key($groups);
}

// 権限グループを表示する場合
if ($group_id != "") {

	// 表示対象の表示グループの設定値を取得(メニュー部分)
	$sql = "select * from menugroup";
	$cond = "where group_id = $group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$menuinfo = pg_fetch_array($sel);

	$menu_ids = array();
	for ($i=1; $i <= 12; $i++){
		$menu_num = 'menu_id_'.$i;
		if ($menuinfo[$menu_num] == "") break;
		array_push($menu_ids, $menuinfo[$menu_num]);
	}
}

// system_config
$conf = new Cmx_SystemConfig();

// 表示グループの初期設定を取得する
$default_group_id = $conf->get("default_set.menu_group_id");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}
function registerGroup() {
	window.open('employee_menu_group_register.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function deleteGroup() {
	var selected = false;
	if (document.delform.elements['group_ids[]']) {
		if (document.delform.elements['group_ids[]'].length) {
			for (var i = 0, j = document.delform.elements['group_ids[]'].length; i < j; i++) {
				if (document.delform.elements['group_ids[]'][i].checked) {
					selected = true;
					break;
				}
			}
		} else if (document.delform.elements['group_ids[]'].checked) {
			selected = true;
		}
	}

	if (!selected) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}

function default_set() {
		document.defaultform.submit();
}
function reverseAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = !box.options[i].selected;
	}
}
function default_set() {
		document.defaultform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="employee_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b26.gif" width="32" height="32" border="0" alt="職員登録"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="employee_info_menu.php?session=<? echo($session); ?>"><b>職員登録</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="employee_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="140" align="center" bgcolor="#bdd1e7"><a href="employee_db_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員登録用DB操作</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_auth_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="employee_display_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示グループ</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="employee_menu_group.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>メニューグループ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_bulk_setting.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括設定</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_auth_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="employee_field_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項目管理</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<? if ($result == "t") { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループを設定しました。</font></td>
<? } ?>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="15%">
<form name="delform" action="employee_menu_group_delete.php">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
メニュー設定グループ<br>
<input type="button" value="作成" onclick="registerGroup();">
<input type="button" value="削除" onclick="deleteGroup();">
</font></td>
</tr>
<tr height="100" valign="top">
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<tr height=\"22\" valign=\"middle\">\n");
	echo("<td width=\"1\"><input type=\"checkbox\" name=\"group_ids[]\" value=\"$tmp_group_id\"></td>\n");
	if ($tmp_group_id == $group_id) {
		echo("<td style=\"padding-left:3px;background-color:#ffff66;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_group_nm</font></td>\n");
	} else {
		echo("<td style=\"padding-left:3px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"employee_menu_group.php?session=$session&group_id=$tmp_group_id\">$tmp_group_nm</a></font></td>\n");
	}
	echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="default_group_id" value="<? echo($default_group_id); ?>">
</form>


<img src="img/spacer.gif" width="1" height="8" alt=""><br>
<form name="defaultform" action="employee_menu_group_default.php">
	<table border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
			<td width="15%">
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">初期メニューグループ<input type="button" value="保存" onclick="default_set();"><br>
				<select name="group_id">
					<option value="">　　　　　</option>
<?
foreach ($groups as $tmp_group_id => $tmp_group_nm) {
	echo("<option value=\"$tmp_group_id\"");
	if ($tmp_group_id == $default_group_id) {
		echo(" selected");
	}
	echo(">$tmp_group_nm\n");
}
						?>
				</select>
				</font>
				<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red"><br>※保存された表示設定は<br>「職員登録＞新規登録」画面で初期表示されます</font>
			</td>
		</tr>
	</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>



</td>
<td><img src="img/spacer.gif" alt="" width="5" height="1"></td>
<td>
<? if($group_id != ""){ ?>
<form name="employee" action="employee_menu_group_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メニューグループ名</font></td>
<td colspan="3"><input type="text" name="group_nm" value="<? echo($groups[$group_id]); ?>" size="25" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ヘッダーメニュー</font></td>
<td colspan="3"><? show_header_menu_list($con, $lcs_func, $menu_ids, $fname); ?></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイドメニューを表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_show_flg" value="t"<? if ($menuinfo["sidemenu_show_flg"] == "t") {echo(" checked");} ?>>する
<input type="radio" name="sidemenu_show_flg" value="f"<? if ($menuinfo["sidemenu_show_flg"] == "f") {echo(" checked");} ?>>しない
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示サイド</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="sidemenu_position" value="1"<? if ($menuinfo["sidemenu_position"] == "1") {echo(" checked");} ?>>右
<input type="radio" name="sidemenu_position" value="2"<? if ($menuinfo["sidemenu_position"] == "2") {echo(" checked");} ?>>左
</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="group_id" value="<? echo($group_id); ?>">
</form>
</td>
</tr>
</table>
<? } ?>
</body>
<? pg_close($con); ?>
</html>
<?
function show_options($sel, $id_col, $nm_col, $tgt_ids) {
	while ($row = pg_fetch_array($sel)) {
		$id = $row[$id_col];
		$nm = $row[$nm_col];
		echo("<option value=\"$id\"");
		if (in_array($id, $tgt_ids)) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
}

function show_header_menu_list($con, $lcs_func, $menu_ids, $fname) {
	echo("<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	for ($i = 1; $i <= 11; $i++) {
		echo("<tr>\n");
		echo("<td width=\"20\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$i.</font></td>\n");
		echo("<td>");
		show_header_menu_listbox($con, $lcs_func, $menu_ids[$i - 1], $fname);
		echo("</td>\n");
		echo("</tr>\n");
	}
	echo("<tr>\n");
	echo("<td width=\"20\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">12.</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">ログアウト</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

function show_header_menu_listbox($con, $lcs_func, $selected_menu_id, $fname) {
	global $_label_by_profile;
	global $profile_type;

	require_once("get_menu_label.ini");
	$report_menu_label = get_report_menu_label($con, $fname);
	$shift_menu_label = get_shift_menu_label($con, $fname);

	echo("<select name=\"menu_ids[]\">");
	echo("<option value=\"-\">");
	echo("<option value=\"00\"");
	if ($selected_menu_id == "00") {echo(" selected");}
	echo(">マイページ");
	echo("<option value=\"01\"");
	if ($selected_menu_id == "01") {echo(" selected");}
	echo(">ウェブメール");
	echo("<option value=\"02\"");
	if ($selected_menu_id == "02") {echo(" selected");}
	echo(">スケジュール");
	echo("<option value=\"03\"");
	if ($selected_menu_id == "03") {echo(" selected");}
	echo(">委員会・WG");
	echo("<option value=\"35\"");
	if ($selected_menu_id == "35") {echo(" selected");}
	echo(">お知らせ・回覧板");
	echo("<option value=\"04\"");
	if ($selected_menu_id == "04") {echo(" selected");}
	echo(">タスク");
	echo("<option value=\"05\"");
	if ($selected_menu_id == "05") {echo(" selected");}
	echo(">伝言メモ");
	echo("<option value=\"06\"");
	if ($selected_menu_id == "06") {echo(" selected");}
	echo(">設備予約");
	echo("<option value=\"07\"");
	if ($selected_menu_id == "07") {echo(" selected");}
	echo(">出勤表");
	echo("<option value=\"08\"");
	if ($selected_menu_id == "08") {echo(" selected");}
	echo(">決裁・申請");
	echo("<option value=\"09\"");
	if ($selected_menu_id == "09") {echo(" selected");}
	echo(">ネットカンファレンス");
	echo("<option value=\"10\"");
	if ($selected_menu_id == "10") {echo(" selected");}
	echo(">掲示板");
	echo("<option value=\"11\"");
	if ($selected_menu_id == "11") {echo(" selected");}
	echo(">Q&amp;A");
	echo("<option value=\"12\"");
	if ($selected_menu_id == "12") {echo(" selected");}
	echo(">文書管理");
	echo("<option value=\"13\"");
	if ($selected_menu_id == "13") {echo(" selected");}
	echo(">アドレス帳");
	echo("<option value=\"36\"");
	if ($selected_menu_id == "36") {echo(" selected");}
	echo(">内線電話帳");
	echo("<option value=\"40\"");
	if ($selected_menu_id == "40") {echo(" selected");}
	echo(">リンクライブラリ");
	echo("<option value=\"14\"");
	if ($selected_menu_id == "14") {echo(" selected");}
	echo(">パスワード・本人情報");
	echo("<option value=\"15\"");
	if ($selected_menu_id == "15") {echo(" selected");}
	echo(">オプション設定");
//XX	echo("<option value=\"25\"");
//XX	if ($selected_menu_id == "25") {echo(" selected");}
//XX	echo(">コミュニティサイト");
	if ($lcs_func["4"] == "t") {
		echo("<option value=\"38\"");
		if ($selected_menu_id == "38") {echo(" selected");}
		echo(">イントラネット");
	}
	if ($lcs_func["7"] == "t") {
		echo("<option value=\"41\"");
		if ($selected_menu_id == "41") {echo(" selected");}
		echo(">検索ちゃん");
	}
	if ($lcs_func["1"] == "t") {
		echo("<option value=\"16\"");
		if ($selected_menu_id == "16") {echo(" selected");}
		// 患者管理/利用者管理
		echo(">".$_label_by_profile["PATIENT_MANAGE"][$profile_type]);
	}
	if ($lcs_func["15"] == "t") {
		echo("<option value=\"49\"");
		if ($selected_menu_id == "49") {echo(" selected");}
		echo(">看護支援");
	}
	if ($lcs_func["2"] == "t") {
		echo("<option value=\"17\"");
		if ($selected_menu_id == "17") {echo(" selected");}
		// 病床管理/居室管理
		echo(">".$_label_by_profile["BED_MANAGE"][$profile_type]);
	}
	if ($lcs_func["10"] == "t") {
		echo("<option value=\"44\"");
		if ($selected_menu_id == "44") {echo(" selected");}
		echo(">看護観察記録");
	}
	if ($lcs_func["5"] == "t") {
		echo("<option value=\"20\"");
		if ($selected_menu_id == "20") {echo(" selected");}
		echo(">ファントルくん");
	}
	if ($lcs_func["13"] == "t") {
		echo("<option value=\"47\"");
		if ($selected_menu_id == "47") {echo(" selected");}
		echo(">ファントルくん＋");
	}
	if ($lcs_func["12"] == "t") {
		echo("<option value=\"45\"");
		if ($selected_menu_id == "45") {echo(" selected");}
		echo(">バリテス");
	}
	if ($lcs_func["6"] == "t") {
		echo("<option value=\"39\"");
		if ($selected_menu_id == "39") {echo(" selected");}
		// メドレポート/ＰＯレポート(Problem Oriented)
		echo(">$report_menu_label");
	}
	if ($lcs_func["11"] == "t") {
		echo("<option value=\"46\"");
		if ($selected_menu_id == "46") {echo(" selected");}
		echo(">カルテビューワー");
	}
	if ($lcs_func["9"] == "t") {
		echo("<option value=\"43\"");
		if ($selected_menu_id == "43") {echo(" selected");}
		// 勤務シフト作成
		echo(">$shift_menu_label");
	}
	if ($lcs_func["8"] == "t") {
		echo("<option value=\"42\"");
		if ($selected_menu_id == "42") {echo(" selected");}
		echo(">" . get_cas_title_name());
	}
	if ($lcs_func["16"] == "t") {
		echo("<option value=\"50\"");
		if ($selected_menu_id == "50") {echo(" selected");}
		echo(">人事管理");
	}
	if ($lcs_func["17"] == "t") {
		echo("<option value=\"51\"");
		if ($selected_menu_id == "51") {echo(" selected");}
		echo(">クリニカルラダー");
	}
	if ($lcs_func["19"] == "t") {
		echo("<option value=\"53\"");
		if ($selected_menu_id == "53") {echo(" selected");}
		echo(">キャリア開発ラダー");
	}
	if ($lcs_func["18"] == "t") {
		echo("<option value=\"52\"");
		if ($selected_menu_id == "52") {echo(" selected");}
		echo(">勤務表");
	}
	if ($lcs_func["14"] == "t") {
		echo("<option value=\"48\"");
		if ($selected_menu_id == "48") {echo(" selected");}
		echo(">日報・月報");
	}
	if ($lcs_func["3"] == "t") {
		echo("<option value=\"18\"");
		if ($selected_menu_id == "18") {echo(" selected");}
		echo(">経営支援");
	}
	if ($lcs_func["31"] == "t") {
		echo("<option value=\"54\"");
		if ($selected_menu_id == "54") {echo(" selected");}
		echo(">スタッフ・ポートフォリオ");
	}
	echo("<option value=\"98\"");
	if ($selected_menu_id == "98") {echo(" selected");}
	echo(">open/close");
}
