<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("get_values.ini");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// 権限のチェック
$checkauth = check_authority($session, 69, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);
$emp_name = get_emp_kanji_name($con, $emp_id, $fname);
// シフトグループ変更履歴を取得
$sql = "SELECT a.*, b.group_name FROM duty_shift_staff_history a".
" left join duty_shift_group b on b.group_id = a.group_id ";
$cond = "WHERE a.emp_id='$emp_id' ORDER BY a.histdate DESC";
$sel_hist = select_from_table($con, $sql, $cond, $fname);
if ($sel_hist == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成 | シフトグループ履歴</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
/* シフトグループ 履歴 */
function history_delete(id) {
    if (confirm('削除してよろしいでしょうか?')) {
        document.dshist.action = 'duty_shift_staff_history_delete.php';
        document.dshist.duty_shift_staff_history_id.value = id;
        document.dshist.submit();
    }
}
function history_edit(id) {
    if (id) {
        document.dshist.duty_shift_staff_history_id.value = id;
    }
    document.dshist.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/employee/employee.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>シフトグループ履歴</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>
<? echo("$emp_name"); ?>
</b></font><br>

<form name="dshist" action="duty_shift_staff_history_form.php" method="post">
<button type="button" onclick="history_edit()">シフトグループの履歴を手動で追加する</button>
<? if (pg_num_rows($sel_hist) > 0) {?>
<table class="hist">
<thead>
<tr>
    <td class="j12">シフトグループ（病棟）名</td>
    <td class="j12">変更日時</td>
    <td class="j12">修正</td>
    <td class="j12">削除</td>
</tr>
</thead>
<tbody>
<? while ($r = pg_fetch_assoc($sel_hist)) {?>
<tr>
    <td class="j12"><?php eh($r["group_name"]);?></td>
    <td class="j12"><?php eh(substr($r["histdate"],0,19))?></td>
    <td><button type="button" onclick="history_edit(<?php eh($r["duty_shift_staff_history_id"]);?>)">修正</button></td>
    <td><button type="button" onclick="history_delete(<?php eh($r["duty_shift_staff_history_id"]);?>)">削除</button></td>
</tr>
<?}?>
</tbody>
</table>
<?}?>
<input type="hidden" name="session" value="<?php eh($session); ?>">
<input type="hidden" name="emp_id" value="<?php eh($emp_id); ?>">
<input type="hidden" name="duty_shift_staff_history_id" value="">
</form>

</td>
</tr>
</table>

</body>
<? pg_close($con); ?>
</html>