<?
//ini_set( 'display_errors', 1 );
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");
require_once("show_class_name.ini");
require_once("cl_common_log_class.inc");

//ini_set("display_errors","1");

class cl_apl_wf_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $mdb2;	// DBコネクション
	var $log;	// ログ

	/**
	 * コンストラクタ
	 * @param object $con DBコネクション
	 * @param string $fname 画面名
	 */
	function cl_apl_wf_common_class($mdb2, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->mdb2 = $mdb2;
		// 全項目リストを初期化
		$this->log = new cl_common_log_class(basename(__FILE__));

	}


    /**
     * 通常申請の承認情報取得
	 * @param object $apply_id 申請ID
     */
	function get_applyapv_for_applylist($apply_id,$emp_id)
	{

		$this->log->info(__FUNCTION__." START");
		$this->log->debug('$apply_id:'.$apply_id,__FILE__,__LINE__);
		$this->log->debug('$emp_id  :'.$emp_id  ,__FILE__,__LINE__);


		require_once(dirname(__FILE__) . "/cl/model/search/cl_applyapv_list.php");
		$cl_applyapv_model = new cl_applyapv_list($this->mdb2,$emp_id);
		$data=$cl_applyapv_model->getListByApplyId($apply_id);
		$this->log->info(__FUNCTION__." END");
		return $data;
	}

	// 非同期・同期受信登録
	function regist_applyasyncrecv($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order)
	{
		$sql = "insert into cl_applyasyncrecv (apply_id, send_apv_order, send_apv_sub_order, recv_apv_order, recv_apv_sub_order, send_apved_order, apv_show_flg, delete_flg) values (";
		$content = array($apply_id, $send_apv_order, $send_apv_sub_order, $recv_apv_order, $recv_apv_sub_order, null, "f", "f");
		$ins = insert_into_table($this->_db_con, $sql, $content, $this->file_name);
		if($ins == 0)
		{
			pg_query($this->_db_con, "rollback");
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}


}
?>
