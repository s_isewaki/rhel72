<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!checkdate($from_mon, $from_day, $from_yr) || !checkdate($to_mon, $to_day, $to_yr)) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
$from_date = "$from_yr$from_mon$from_day";
$to_date = "$to_yr$to_mon$to_day";
if ($from_date > $to_date) {
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($comment) > 200) {
	echo("<script type=\"text/javascript\">alert('コメントが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 付き添い情報をDELETE〜INSERT
$sql = "delete from inptfamily";
$cond = "where ptif_id = '$pt_id' and from_date = '$from_date'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sql = "insert into inptfamily (ptif_id, from_date, to_date, head_count, comment) values (";
$content = array($pt_id, $from_date, $to_date, $head_count, $comment);
$ins = insert_into_table($con, $sql, $content, $fname);
if ($ins == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 親画面をリロードする
echo "<script type=\"text/javascript\">window.opener.location.reload();</script>";

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'inpatient_family_register.php?session=$session&pt_id=$pt_id';</script>\n");
?>
