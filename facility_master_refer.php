<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 施設・設備｜テンプレート参照</title>
<?
require("./about_session.php");
require("./about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ワークフロー権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$fname = $PHP_SELF;

$con = connect2db($fname);

?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/yui_0.12.2/build/tabview/tabview-min.js"></script>
<script type="text/javascript">
function initPage() {
	new YAHOO.widget.TabView('maintab');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/tabview/assets/border_tabs.css">
<link rel="stylesheet" type="text/css" href="js/yui_0.12.2/build/tabview/assets/tabview.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.yui-navset ul.yui-nav {
	text-align:left;
}

.yui-navset .yui-nav li a, .yui-navset .yui-content {
    border:1px solid #5279a5;  /* label and content borders */
}

.yui-navset .yui-nav .selected a, .yui-navset .yui-nav a:hover, .yui-navset .yui-content {
    background-color:#ffffff; /* active tab, tab hover, and content bgcolor */
}

.yui-navset .yui-nav li a {
    background-color:#dddddd;
	color:blue;
}

.yui-navset .yui-content {
	padding:5px;
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="612" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="520" height="32" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>テンプレート参照</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>


<div id="maintab" class="yui-navset" style="width:612px;">
<ul class="yui-nav">
<li class="selected"><a href="#localpc"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>ローカルPC</em></font></a></span></li>
<li><a href="#library"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><em>テンプレート一覧</em></font></a></li>
</ul>
<div class="yui-content">

<form action="facility_master_refer_set.php" method="post" enctype="multipart/form-data" name="upload">

<div id="localpc" style="height:370px;overflow:auto;">

<table width="590" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="120" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレートファイル</font></td>
<td width="470">
<input type="file" name="template_file" size="56">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">※<? echo(ini_get("upload_max_filesize")); ?>まで</font>
</td>
</tr>
</table>
<table width="590" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="設定"></td>
</tr>
</table>

</div>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>


<div id="library" style="display:none;height:370px;overflow:auto;">
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
<?
	// テンプレート一覧の取得
	$sql_lib = "select fcltmpl_id, fcltmpl_name from fcltmpl";
	$cond_lib = "where fcltmpl_del_flg = 'f' order by fcltmpl_id";
	$sel_lib = select_from_table($con, $sql_lib, $cond_lib, $fname);
	if ($sel_lib == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	// 一覧をループ
	while ($row_lib = pg_fetch_array($sel_lib)) {

		// 行を出力
		$fcltmpl_id = $row_lib["fcltmpl_id"];
		$fcltmpl_name = $row_lib["fcltmpl_name"];
?>
	<tr height="22">
	<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="facility_master_refer_lib_set.php?session=<?=$session?>&fcltmpl_id=<?=$fcltmpl_id?>"><?=$fcltmpl_name?></a></font></td>
	</tr>

<?
	}
?>
	</table>
</div>

</div>
</div>


</center>
</body>
<? pg_close($con); ?>
</html>
