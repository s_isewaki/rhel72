<?php

ob_start();

require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("referer_common.ini");
require_once("get_values.ini");
require_once("./conf/sql.inf");
require_once("show_class_name.ini");
require_once("fplus_common.ini");

$fname = $PHP_SELF;

// セッションのチェック

$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,79,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
}
else {
	$encoding = mb_http_output();   // Firefox
}

$stamp = time();

$date = date('YmdHis', $stamp);


$fileName = "MRSA等耐性性菌検出報告書_".$date.".xls";

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$fileName = mb_convert_encoding(
		$fileName,
		$encoding,
		mb_internal_encoding());

ob_clean();
header("Content-Type: application/vnd.ms-excel");
header(sprintf('Content-Disposition: attachment; filename=%s', $fileName));
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');

$download_data = setData($fname,$con,$mrsa_sel_d1_y,$sel_vert_field_idx,$data_1_13_find_germ);

echo mb_convert_encoding(
		nl2br($download_data),
		'sjis',
		mb_internal_encoding()
		);

ob_end_flush();

pg_close($con); 


/**
 * /エクセル出力用データセット
 *
 * @param mixed $fname 画面名
 * @param mixed $con 接続情報
 * @param mixed $sel_d1_y 指定年度
 * @param mixed $sel_vert_field_idx 種別
 * @param mixed $data_1_13_find_germ 検出菌名/微生物名
 * @return mixed This is the return value description
 *
 */
function setData
($fname,$con,$sel_d1_y,$sel_vert_field_idx,$data_1_13_find_germ)
{
	
	$arr_class_name = get_class_name_array($con, $fname);
	
	//エクセル出力用変数
	$dispSetdata = "";	
	
	//出力用構造体
	$disp_data = array();
	
	$vert_field_ab_definision = 
	array(
			"00"=> "",
			"data_1_6_depart"=> "診療科",
			"data_1_7_ward"=> "病棟",
			"data_1_9_base_disease"=> "基礎疾患",
			"data_1_13_find_germ"=> "検出菌/微生物名",
			"data_2_1_specimen"=> "検出検体",
			"data_4_2_infectious"=> "感染症/保菌状態",
			"data_4_3_diagnosis"=> "診断名",
			"data_4_5_root"=> "感染経路"
	);


	//====================================================================
	// 汎用マスタを取得する。プログラムからはエピシス項目名でアクセスできるようにする。
	//====================================================================
	fplus_common_maerge_general_master_to_episys_master("A401", "data_3_1_job");
	fplus_common_maerge_general_master_to_episys_master("A403", "data_4_1_arise_area");
	fplus_common_maerge_general_master_to_episys_master("A404", "data_1_2_belong_dept");
	fplus_common_maerge_general_master_to_episys_master("A405", "data_1_13_find_germ");
	fplus_common_maerge_general_master_to_episys_master("A407", "data_4_3_diagnosis");
	fplus_common_maerge_general_master_to_episys_master("A409", "data_3_2_hospital_dept");


	if($sel_vert_field_idx == "data_1_6_depart")//"診療科"
		{$vert_data = fplus_common_get_episys_master_field_list("data_3_2_hospital_dept", "", true);}
	elseif($sel_vert_field_idx == "data_1_7_ward")//"病棟"
		{$vert_data = fplus_common_get_episys_master_field_list("data_1_2_belong_dept", "", true);}
	elseif($sel_vert_field_idx == "data_1_9_base_disease")//"基礎疾患"
		{$vert_data = fplus_common_get_001_master_field_list("data_1_11_base_disease", "", true);}
	elseif($sel_vert_field_idx == "data_1_13_find_germ")//"検出菌/微生物名"
		{$vert_data = fplus_common_get_episys_master_field_list("data_1_13_find_germ", "", true);}
	elseif($sel_vert_field_idx == "data_2_1_specimen")//"検出検体"
		{$vert_data = fplus_common_get_001_master_field_list("data_2_1_specimen", "", true);}
	elseif($sel_vert_field_idx == "data_4_2_infectious")//"感染症/保菌状態"
		{$vert_data = fplus_common_get_001_master_field_list("data_4_2_infectious", "", true);}
	elseif($sel_vert_field_idx == "data_4_3_diagnosis")//"診断名"
		{$vert_data = fplus_common_get_episys_master_field_list("data_4_3_diagnosis", "", true);}
	elseif($sel_vert_field_idx == "data_4_5_root")//"感染経路"
		{$vert_data = fplus_common_get_001_master_field_list("data_4_5_root", "", true);}
	
	
	$gokei_line = array();
	
	
	// 指定された日付
	//$sel_d1_y = (int)@$_REQUEST["sel_d1_y"];
	$www = $sel_d1_y;
        $sel_d3_y = (int)@$_REQUEST["sel_d3_y"];
	$sel_d3_m = (int)@$_REQUEST["sel_d3_m"];
        $sel_d3_d = (int)@$_REQUEST["sel_d3_d"];
        $sel_d4_y = (int)@$_REQUEST["sel_d4_y"];
        $sel_d4_m = (int)@$_REQUEST["sel_d4_m"];
        $sel_d4_d = (int)@$_REQUEST["sel_d4_d"];
	
        $header_month = array();
        $sel_d3_ms = $sel_d3_m;
        for($i=0;$i<=11;$i++){
            $header_month[$i]=$sel_d3_ms;
            $sel_d3_ms++;
            if($sel_d3_ms>12){
                $sel_d3_ms=$sel_d3_ms-12;
            }
        }
	
	// 指定された縦軸項目のインデックス
	//$sel_vert_field_idx = @$_REQUEST["sel_vert_field_idx"]; // 選択中の縦軸項目インデックス番号(0〜)
	$idx = $sel_vert_field_idx; // 選択中の縦軸項目インデックス番号(0〜)
	
	if($sel_vert_field_idx == "data_1_13_find_germ")
	{
		//縦軸の条件が"検出菌/微生物名"の場合は絞込み条件の"検出菌/微生物名"を検索条件にしない
		$germ_cond = "";
		
		//ヘッダにも表示しない
		$str_idx = "";
	}
	else
	{
		$germ_cond = "and data_1_13_find_germ = '$data_1_13_find_germ' ";
		
		//ヘッダに表示する検出菌名/微生物名を取得する
		$germ_list = fplus_common_get_episys_master_field_list("data_1_13_find_germ", "", true);
		foreach($germ_list as $germ_list_val )
		{
			if($germ_list_val["record_id"] == $data_1_13_find_germ)
			{
				$str_idx = "検出菌名/微生物名：".$germ_list_val["record_caption"];
			}
		}
	}
        
        if ($sel_d3_y)        $month_cond = " (data_1_14_pick_day >= '".sprintf("%04d/%02d/%02d",$sel_d3_y,$sel_d3_m,$sel_d3_d)."'"; // 開始日
        if ($sel_d4_y){        
            $month_cond .= " and data_1_14_pick_day <= '".sprintf("%04d/%02d/%02d",$sel_d4_y,$sel_d4_m,$sel_d4_d)."')  "; // 終了日
	}else{
            $month_cond .= " and data_1_14_pick_day < '".sprintf("%04d/%02d/%02d",$sel_d3_y+1,$sel_d3_m,$sel_d4_d)."')  "; // 終了日
        }
	
	/*$month_cond = array();
	
	$month_cond[0] = "data_1_14_pick_day >= '$sel_d1_y/04/01' and data_1_14_pick_day <= '$sel_d1_y/04/30' ";
	$month_cond[1] = "data_1_14_pick_day >= '$sel_d1_y/05/01' and data_1_14_pick_day <= '$sel_d1_y/05/31' ";
	$month_cond[2] = "data_1_14_pick_day >= '$sel_d1_y/06/01' and data_1_14_pick_day <= '$sel_d1_y/06/30' ";
	$month_cond[3] = "data_1_14_pick_day >= '$sel_d1_y/07/01' and data_1_14_pick_day <= '$sel_d1_y/07/31' ";
	$month_cond[4] = "data_1_14_pick_day >= '$sel_d1_y/08/01' and data_1_14_pick_day <= '$sel_d1_y/08/31' ";
	$month_cond[5] = "data_1_14_pick_day >= '$sel_d1_y/09/01' and data_1_14_pick_day <= '$sel_d1_y/09/30' ";
	$month_cond[6] = "data_1_14_pick_day >= '$sel_d1_y/10/01' and data_1_14_pick_day <= '$sel_d1_y/10/31' ";
	$month_cond[7] = "data_1_14_pick_day >= '$sel_d1_y/11/01' and data_1_14_pick_day <= '$sel_d1_y/11/30' ";
	$month_cond[8] = "data_1_14_pick_day >= '$sel_d1_y/12/01' and data_1_14_pick_day <= '$sel_d1_y/12/31' ";
	$month_cond[9] = "data_1_14_pick_day >= '".($sel_d1_y+1)."/01/01' and data_1_14_pick_day <= '".($sel_d1_y+1)."/01/31' ";
	$month_cond[10] = "data_1_14_pick_day >= '".($sel_d1_y+1)."/02/01' and data_1_14_pick_day <= '".($sel_d1_y+1)."/02/29' ";
	$month_cond[11] = "data_1_14_pick_day >= '".($sel_d1_y+1)."/03/01' and data_1_14_pick_day <= '".($sel_d1_y+1)."/03/31' ";
	*/
	$datalist = array();
	
	//for ($i = 0; $i <= 11; $i++){
                $sql = "select ".$idx.",data_1_14_pick_day from fplus_infection_mrsa 
				inner join fplusapply on (
				fplusapply.apply_id = fplus_infection_mrsa.apply_id
				and fplusapply.delete_flg = 'f'
				and fplusapply.apply_stat in ('0','1')
				and fplusapply.draft_flg = 'f'
				)
				where ".$month_cond . $germ_cond;

		$sel = @select_from_table($con, $sql, "", $fname);
		$row = @pg_fetch_all($sel);

		//表示しやすいように取得データから配列を加工しました
		for($i = 0; $i <= 11; $i++){
                $datalist[$i] = "";
		}
                foreach($row as $val )
		{
                    $split_day = split("/",$val["data_1_14_pick_day"]);
                    $i = $split_day["1"];
                    $i = ltrim($i, "0");
                    
                    $fin = 12-$sel_d3_m+$i;
                    if($fin>12){
                    $fin =$fin-12;
                    }
                    $datalist[$fin][$val[$idx]] ++;
		}
	//}

	$str_disp = "指定年度：".$sel_d1_y."  　　種別：".$vert_field_ab_definision[$sel_vert_field_idx]."  　　".$str_idx;
	
	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	
	$data .= "<table border=1 >";
	
	$data .= "<tr align=\"center\">";
	$data .= "<td align=\"left\" colspan=\"14\" ><font size=\"4\">".$str_disp."</font></td>";
	$data .= "</tr>";
	
	//ヘッダ行表示
	$data .= "<tr align=\"center\">";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">　</font></td>";
        for($i=0;$i<12;$i++){
            $data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">".$header_month[$i]."月</font></td>";
        }
	/*$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">5月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">6月</font></td>";	
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">7月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">8月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">9月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">10月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">11月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">12月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">1月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">2月</font></td>";
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">3月</font></td>";*/
	$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">合計</font></td>";
	$data .= "</tr>";
	
	//	foreach($vert_data as $val )
	for ($name_cnt = 0; $name_cnt <= count($vert_data); $name_cnt++)
	{
		
		$data .= "<tr>";
		
		if($name_cnt == count($vert_data))
		{
			//合計行を表示
			$data .= "<td bgcolor=\"#EEAEEE\" ><font size=\"4\">合計</font></td>";

			for ($gokei_cnt = 0; $gokei_cnt <= 12; $gokei_cnt++)
			{
				$data .= "<td align=\"right\" bgcolor=\"#EEAEEE\" ><font size=\"4\">".$gokei_line[$gokei_cnt]."</font></td>";
			}
		}
		else
		{
			//項目行を表示
			
			$gokei = 0;
			
			$data .= "<td style=\"background-color:#ffdfff;\"><font size=\"4\">".$vert_data[$name_cnt]["record_caption"]."</font></td>";
			
			for ($i = 0; $i <= 12; $i++)
			{
				//0:4月　1:5月　2:6月　3:7月　4:8月　5:9月　6:10月　7:11月　8:12月　9:1月　10:2月　11:3月　12:合計
				if($i == 12)
				{
					//合計行
					
					$data .= "<td align=\"right\"><font size=\"4\">".$gokei."</font></td>";
					
					$gokei_line[$i] = $gokei_line[$i] + $gokei;
				}
				else
				{
					//合計用足しこみ(横軸用)
					$data .= "<td align=\"right\"><font size=\"4\">".$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]]."</font></td>";
					
					if($datalist[$i][(string)$vert_data[$name_cnt]["record_id"]] != "")
					{
						$gokei = $gokei + (int)$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]];
						$gokei_line[$i] = $gokei_line[$i] + (int)$datalist[$i][(string)$vert_data[$name_cnt]["record_id"]];
					}
					
				}		
			}
		}
		$data .= "</tr>";
	}
	$data .= "</table>";
	
	return $data;	
}
?>
