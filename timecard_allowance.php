<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 手当</title>
<?
require_once("about_comedix.php");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_menu_common.ini");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// 手当一覧を取得
$sql = "select allow_id, allow_cd, allow_contents, allow_price, input_type from tmcdallow";
$cond = "where del_flg = 'f' order by allow_id asc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// opt以下のphpを表示するかどうか(申請用追加機能有無判定)
$opt_display_flag = file_exists("opt/flag");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteReason() {
	if (confirm('削除します。よろしいですか？')) {
		document.delform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? if ($opt_display_flag) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
&nbsp;&nbsp;
<a href="proxy.php?path=opt/timecard_allowance_daily.php&session=<?= $session ?>">手当回数入力(日別)</a>
&nbsp;&nbsp;
<a href="proxy.php?path=opt/timecard_allowance_monthly.php&session=<?= $session ?>">手当回数入力(月別)</a>
&nbsp;&nbsp;
<b>手当マスタ</b>
&nbsp;&nbsp;
<a href="proxy.php?path=opt/timecard_allowance_csv_download.php&session=<?= $session ?>">CSVダウンロード</a>
</font>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="regform" action="timecard_allowance_insert.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td><input type="text" id="allow_contents" name="allow_contents" value="<? echo($allow_contents); ?>" size="50" maxlength="100" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一日あたりの金額</font></td>
<td><input type="text" id="allow_price" name="allow_price" value="<? echo($allow_price); ?>" size="10" maxlength="7" style="ime-mode:inactive;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">円</font></td>
</tr>

<? if ($opt_display_flag) { ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入力種別</font></td>
<td><input type="radio" id="input_daily" name="input_type" value="1" />日別&nbsp;<input type="radio" id="input_monthly" name="input_type" value="2" />月別</td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当コード</font></td>
<td><input type="text" id="allow_cd" name="allow_cd" value="<? echo($allow_cd); ?>" size="4" maxlength="4" style="ime-mode:disabled;"></td>
</tr>
<? } ?>

</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" id="allow_id" name="allow_id" value="">
<input type="hidden" id="back_allow_contents" name="back_allow_contents" value="">
<input type="hidden" id="back_allow_price" name="back_allow_price" value="">
</form>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="delform" action="timecard_allowance_delete.php" method="post" style="border-top:#5279a5 solid 1px;padding-top:5px;">
<?
if (pg_num_rows($sel) > 0) {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td align=\"right\"><input type=\"button\" value=\"削除\" onclick=\"deleteReason();\"></td>\n");
	echo("</tr>\n");
	echo("</table>\n");

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
	echo("<td width=\"400\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">手当</font></td>\n");
	
	if ($opt_display_flag) {
	echo("<td width=\"80\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">入力種別</font></td>\n");
	echo("<td width=\"60\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">手当コード</font></td>\n");
	}
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">一日あたりの金額(円)</font></td>\n");
	echo("</tr>\n");

	while ($row = pg_fetch_array($sel)) {
		$tmp_allow_id = $row["allow_id"];
		$tmp_allow_contents = $row["allow_contents"];
		$tmp_allow_price = $row["allow_price"];
		$tmp_allow_cd = $row["allow_cd"];
		
		$input_name = "";
		switch($row["input_type"]) {
			case 1:
				$input_name = "日別";
				break;
			case 2:
				$input_name = "月別";
				break;
		}

		echo("<tr>\n");
		echo("<td width=\"30\" align=\"center\"><input type=\"checkbox\" name=\"allow_ids[]\" value=\"$tmp_allow_id\"></td>\n");
		echo("<td><a href=\"timecard_allowance_update.php?session=$session&allow_id=$tmp_allow_id\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_allow_contents</font></a></td>\n");
		if ($opt_display_flag) {
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$input_name</font></td>\n");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_allow_cd</font></td>\n");
		}
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_allow_price</font></td>\n");
		echo("</tr>\n");
	}
	
	echo("</table>\n");
} else {
	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">手当は登録されていません。</font></td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
