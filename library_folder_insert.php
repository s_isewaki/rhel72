<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="library_folder_register.php">
<input type="hidden" name="folder_name" value="<?php echo($folder_name); ?>">
<input type="hidden" name="folder_type" value="<?php echo($folder_type); ?>">
<input type="hidden" name="folder_no" value="<?php echo($folder_no); ?>">
<input type="hidden" name="archive" value="<?php echo($archive); ?>">
<input type="hidden" name="arch_id" value="<?php echo($arch_id); ?>">
<input type="hidden" name="category" value="<?php echo($category); ?>">
<input type="hidden" name="cate_id" value="<?php echo($cate_id); ?>">
<input type="hidden" name="folder_id" value="<?php echo($folder_id); ?>">
<input type="hidden" name="parent" value="<?php echo($parent); ?>">
<input type="hidden" name="private_flg1" value="<?php echo($private_flg1); ?>">
<input type="hidden" name="private_flg2" value="<?php echo($private_flg2); ?>">
<input type="hidden" name="ref_dept_st_flg" value="<?php echo($ref_dept_st_flg); ?>">
<input type="hidden" name="ref_dept_flg" value="<?php echo($ref_dept_flg); ?>">
<input type="hidden" name="ref_class_src" value="<?php echo($ref_class_src); ?>">
<input type="hidden" name="ref_atrb_src" value="<?php echo($ref_atrb_src); ?>">
<input type="hidden" name="class_id" value="<?php echo($class_id); ?>">
<input type="hidden" name="atrb_id" value="<?php echo($atrb_id); ?>">
<input type="hidden" name="link_arch_id" value="<?php echo($link_arch_id); ?>">
<input type="hidden" name="link_cate_id" value="<?php echo($link_cate_id); ?>">
<input type="hidden" name="link_id" value="<?php echo($link_id); ?>">

<?php
if (is_array($hid_ref_dept)) {
    foreach ($hid_ref_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"ref_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_ref_dept = array();
}

echo "<input type=\"hidden\" name=\"ref_st_flg\" value=\"{$ref_st_flg}\">";

if (is_array($ref_st)) {
    foreach ($ref_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"ref_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $ref_st = array();
}

echo "<input type=\"hidden\" name=\"target_id_list1\" value=\"{$target_id_list1}\">";
echo "<input type=\"hidden\" name=\"target_id_list2\" value=\"{$target_id_list2}\">";

if ($target_id_list1 != "") {
    $hid_ref_emp = split(",", $target_id_list1);
} else {
    $hid_ref_emp = array();
}
?>
<input type="hidden" name="upd_dept_st_flg" value="<?php echo($upd_dept_st_flg); ?>">
<input type="hidden" name="upd_dept_flg" value="<?php echo($upd_dept_flg); ?>">
<input type="hidden" name="upd_class_src" value="<?php echo($upd_class_src); ?>">
<input type="hidden" name="upd_atrb_src" value="<?php echo($upd_atrb_src); ?>">
<?php
if (is_array($hid_upd_dept)) {
    foreach ($hid_upd_dept as $tmp_dept_id) {
        echo("<input type=\"hidden\" name=\"upd_dept[]\" value=\"$tmp_dept_id\">\n");
    }
} else {
    $hid_upd_dept = array();
}
if ($archive=="4") {
    $upd_dept_flg = "1";
    $upd_st_flg = "1";
}
?>
<input type="hidden" name="upd_st_flg" value="<?php echo($upd_st_flg); ?>">
<?php
if (is_array($upd_st)) {
    foreach ($upd_st as $tmp_st_id) {
        echo("<input type=\"hidden\" name=\"upd_st[]\" value=\"$tmp_st_id\">\n");
    }
} else {
    $upd_st = array();
}
?>
<?php
if ($target_id_list2 != "") {
    $hid_upd_emp = split(",", $target_id_list2);
} else {
    $hid_upd_emp = array();
}
?>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="o" value="<?php echo($o); ?>">
<input type="hidden" name="path" value="<?php echo($path); ?>">
<input type="hidden" name="ref_toggle_mode" value="<?php echo($ref_toggle_mode); ?>">
<input type="hidden" name="upd_toggle_mode" value="<?php echo($upd_toggle_mode); ?>">
<input type="hidden" name="back" value="t">
</form>
<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("library_common.php");
require_once("Cmx.php");
require_once("aclg_set.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$auth_id = ($path == "3") ? 63 : 32;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 入力チェック
if ($folder_name == "") {
    echo("<script type=\"text/javascript\">alert('フォルダ名が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
else{
    if (preg_match("/[\/:*?\"<>|'\\\]/", $folder_name)) {
        echo("<script type=\"text/javascript\">alert(\"フォルダ名に以下の文字は使えません。\\n\\\ / : * ? \\\" < > | '\");</script>\n");
        echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
        exit;
    }
    if (strlen($folder_name) > 60) {
        echo("<script type=\"text/javascript\">alert('フォルダ名が長すぎます。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
if (strlen($folder_no) > 50) {
    echo("<script type=\"text/javascript\">alert('フォルダ番号が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">document.items.submit();</script>");
    exit;
}
// 入力チェック・フォルダ作成
if ($folder_type != 1) {
    if ($archive != "1" && (($ref_dept_flg != "1" && count($hid_ref_dept) == 0) || ($ref_st_flg != "1" && count($ref_st) == 0)) && count($hid_ref_emp) == 0 && !($archive == "4" && $private_flg2 == "t")) {
        echo("<script type=\"text/javascript\">alert('参照可能範囲が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
    // 全体、部署の場合更新可能者確認、個人、委員会（所属者がいるため）は不要
    if (($archive == "2" || $archive == "3") && (($upd_dept_flg != "1" && count($hid_upd_dept) == 0) || ($upd_st_flg != "1" && count($upd_st) == 0)) && count($hid_upd_emp) == 0) {
        echo("<script type=\"text/javascript\">alert('更新可能範囲が不正です。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
// 入力チェック・リンク作成
else {
    if (empty($link_id)) {
        echo("<script type=\"text/javascript\">alert('フォルダのリンクが選択されていません。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}
switch ($archive) {
case "1":  // 個人ファイル
case "2":  // 全体共有
    $private_flg = null;
    break;
case "3":  // 部署共有
    $private_flg = ($private_flg1 == "t") ? "t" : "f";
    break;
case "4":  // 委員会・WG共有
    $private_flg = ($private_flg2 == "t") ? "t" : "f";
    break;
}
if ($ref_dept_st_flg != "t") {$ref_dept_st_flg = "f";}
if ($ref_dept_flg == "") {$ref_dept_flg = null;}
if ($ref_st_flg == "") {$ref_st_flg = null;}
if ($upd_dept_st_flg != "t") {$upd_dept_st_flg = "f";}
if ($upd_dept_flg == "") {$upd_dept_flg = null;}
if ($upd_st_flg == "") {$upd_st_flg = null;}

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員情報を取得
$sql = "select emp_id, emp_class, emp_attribute, emp_dept, emp_st from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel_emp, 0, "emp_id");
$emp_class = pg_fetch_result($sel_emp, 0, "emp_class");
$emp_atrb = pg_fetch_result($sel_emp, 0, "emp_attribute");
$emp_dept = pg_fetch_result($sel_emp, 0, "emp_dept");
$emp_st = pg_fetch_result($sel_emp, 0, "emp_st");

$lock_show_flg = true;
$lock_show_all_flg = true;

// 管理画面以外、上位カテゴリ以外、個人以外の場合、更新権限確認
if ($path != "3" && $cate_id != "" && $archive != "1") {
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);
    $opt = array("is_admin"=>"", "is_login"=>"", "is_narrow_mode"=>1, "is_ignore_class_atrb"=>($archive==4 ? 1 : ""));
    if ($parent) {
        $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $archive, $cate_id, $parent, $opt);
    } else {
        $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $archive, $cate_id, $class_id, $atrb_id, $opt);
        //$sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $archive, $cate_id, 0, 0, $opt);
    }
    $top_row = lib_get_top_row($con, $fname, $sql);
    $upd_flg = ($top_row["writable"] ? true : false);

    if ($upd_flg == false) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\">alert('更新権限がありません。');</script>");
        echo("<script type=\"text/javascript\">document.items.submit();</script>");
        exit;
    }
}


// 書庫直下に作成する場合（＝カテゴリ）
if ($cate_id == "") {

    // カテゴリIDを採番
    $sql = "select max(lib_cate_id) from libcate";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $new_cate_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

    // カテゴリ情報を登録
    $now = date("YmdHis");
    $sql = "insert into libcate (lib_archive, lib_cate_id, lib_link_id, lib_cate_nm, lib_cate_no, reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg) values (";
    $content = array($archive, $new_cate_id, $emp_id, $folder_name, $folder_no, $emp_id, $now, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

// それ以外の場合
} else {

    // フォルダIDを採番
    $sql = "select max(folder_id) from libfolder";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_query($con,"rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $new_folder_id = intval(pg_fetch_result($sel, 0, 0)) + 1;

    // フォルダ情報を登録
    $now = date("YmdHis");
    if ($folder_type != 1) {
        $sql = "insert into libfolder (folder_id, lib_archive, lib_cate_id, folder_name, folder_no, reg_emp_id, upd_time, private_flg, ref_dept_st_flg, ref_dept_flg, ref_st_flg, upd_dept_st_flg, upd_dept_flg, upd_st_flg) values (";
        $content = array($new_folder_id, $archive, $cate_id, $folder_name, $folder_no, $emp_id, $now, $private_flg, $ref_dept_st_flg, $ref_dept_flg, $ref_st_flg, $upd_dept_st_flg, $upd_dept_flg, $upd_st_flg);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
    // リンク情報を登録
    else {
        $sql = "insert into libfolder (folder_id, lib_archive, lib_cate_id, folder_name, folder_no, reg_emp_id, upd_time, link_id) values (";
        $content = array($new_folder_id, $archive, $cate_id, $folder_name, $folder_no, $emp_id, $now, $link_id);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    // 親フォルダが指定されている場合
    if ($parent != "") {

        // 親フォルダ情報を登録
        $sql = "insert into libtree (parent_id, child_id) values (";
        $content = array($parent, $new_folder_id);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

// 権限情報
// 書庫直下に作成する場合（＝カテゴリ）
if ($cate_id == "") {
    $refdept_tblname = "libcaterefdept";
    $refst_tblname = "libcaterefst";
    $refemp_tblname = "libcaterefemp";
    $upddept_tblname = "libcateupddept";
    $updst_tblname = "libcateupdst";
    $updemp_tblname = "libcateupdemp";
    $id_name = "lib_cate_id";
    $lib_id = $new_cate_id;
} else {
    $refdept_tblname = "libfolderrefdept";
    $refst_tblname = "libfolderrefst";
    $refemp_tblname = "libfolderrefemp";
    $upddept_tblname = "libfolderupddept";
    $updst_tblname = "libfolderupdst";
    $updemp_tblname = "libfolderupdemp";
    $id_name = "folder_id";
    $lib_id = $new_folder_id;
}
// 参照可能部署情報を登録
$sql = "delete from $refdept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($hid_ref_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
    $sql = "insert into $refdept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 参照可能役職情報を登録
$sql = "delete from $refst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($ref_st as $tmp_st_id) {
    $sql = "insert into $refst_tblname ($id_name, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 参照可能職員情報を登録
$sql = "delete from $refemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($hid_ref_emp as $tmp_emp_id) {
    $sql = "insert into $refemp_tblname ($id_name, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 更新可能部署情報を登録
$sql = "delete from $upddept_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($hid_upd_dept as $tmp_val) {
    list($tmp_class_id, $tmp_atrb_id, $tmp_dept_id) = split("-", $tmp_val);
    $sql = "insert into $upddept_tblname ($id_name, class_id, atrb_id, dept_id) values (";
    $content = array($lib_id, $tmp_class_id, $tmp_atrb_id, $tmp_dept_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 更新可能役職情報を登録
$sql = "delete from $updst_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($upd_st as $tmp_st_id) {
    $sql = "insert into $updst_tblname ($id_name, st_id) values (";
    $content = array($lib_id, $tmp_st_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 更新可能職員情報を登録
$sql = "delete from $updemp_tblname";
$cond = "where $id_name = $lib_id";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
    pg_query($con, "rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
foreach ($hid_upd_emp as $tmp_emp_id) {
    $sql = "insert into $updemp_tblname ($id_name, emp_id) values (";
    $content = array($lib_id, $tmp_emp_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// トランザクションをコミット
pg_query($con, "commit");

// カテゴリを作成した場合、物理フォルダを作成
if ($cate_id == "") {
    $cate_id = $new_cate_id;
    if (!is_dir("docArchive")) {
        @mkdir("docArchive", 0755);
    }
    if ($archive == "1") {
        if (!is_dir("docArchive/private")) {
            @mkdir("docArchive/private", 0755);
        }
        @mkdir("docArchive/private/cate$cate_id", 0755);
    } else if ($archive == "2") {
        if (!is_dir("docArchive/all")) {
            @mkdir("docArchive/all", 0755);
        }
        @mkdir("docArchive/all/cate$cate_id", 0755);
    }
}

// 文書一覧画面に遷移
if ($path == "3") {
    echo("<script type=\"text/javascript\">location.href = 'library_admin_menu.php?session=$session&a=$archive&c=$cate_id&f=$new_folder_id&o=$o&link_id=$link_id';</script>");
} else {
    echo("<script type=\"text/javascript\">location.href = 'library_list_all.php?session=$session&a=$archive&c=$cate_id&f=$new_folder_id&o=$o';</script>");
}

// データベース接続を閉じる
pg_close($con);
