<?
require("about_session.php");
require("about_authority.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("./conf/sql.inf");
require("cl_workflow_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, $CL_MANAGE_AUTH, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "workflow", $fname);

// 職員ID取得 
$emp_id=get_emp_id($con,$session,$fname);

// ログインID取得
$emp_login_id = get_login_id($con,$emp_id,$fname);

// guest*ユーザはテンプレートを登録できない
$can_regist_flg = true;
if (substr($emp_login_id, 0, 5) == "guest") {
	$can_regist_flg = false;
}

// データ取得
if ($back != "t" && $preview_flg != "1") {
	$sql = "select * from cl_wkfwlib ";
	$cond = "where wkfwlib_id = '$wkfwlib_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$wkfw_title = pg_fetch_result($sel, 0, "wkfwlib_name");
	$wkfw_content = pg_fetch_result($sel, 0, "wkfwlib_content");
	$wkfwlib_filename = pg_fetch_result($sel, 0, "wkfwlib_filename");
}
$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$cl_title?>｜テンプレート更新</title>
<script type="text/javascript">

function submitForm() {
	document.wkfw.submit();
}

function referTemplate() {
	window.open('cl_workflow_lib_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}

function submitPreviewForm() {
	document.wkfw.action = "cl_workflow_lib_detail.php";
	document.wkfw.preview_flg.value = "1";
	document.wkfw.submit();
}

function show_preview_window(url){
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=500,top=100,width=600,height=700";
	window.open(url, 'preview_window',option);
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="cl_workflow_menu.php?session=<? echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="cl_workflow_menu.php?session=<? echo($session); ?>"><b>ワークフロー</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="ladder_menu.php?session=<? echo($session); ?>"><img src="img/icon/s42.gif" width="32" height="32" border="0" alt="<? echo($cl_title); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="ladder_menu.php?session=<? echo($session); ?>"><b><? echo($cl_title); ?></b></a> &gt; <a href="cl_workflow_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="ladder_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
$option = array("wkfwlib_id" => $wkfwlib_id);
show_wkfw_menuitem($session, $fname, $option);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#A0D25A"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="wkfw" action="cl_workflow_lib_update.php" method="post">
<table width="580" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td align="right" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート名称</font></td>
<td><input name="wkfw_title" type="text" size="50" maxlength="80" value="<? echo($wkfw_title); ?>" style="ime-mode: active;"></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<table width="580" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left" width="100">

<table width="100" border="0" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="left" bgcolor="#E5F6CD"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">本文</font>
&nbsp;
<input type="button" name="referbtn" value="参照" onclick="referTemplate();">
</td>
</tr>
</table>

</td>
<td align="right">
<input type="button" name="prevbtn" value="プレビュー" onclick="submitPreviewForm();">　
<input type="button" name="update" value="更新" onclick="submitForm();"></td>
</tr>
<tr>
<?
// テキストエリア中に/textareaがあると表示が崩れるため変換する
$wkfw_content = eregi_replace("/(textarea)", "_\\1", $wkfw_content);

?>
<td colspan="3"><textarea name="wkfw_content" rows="20" cols="80"><? echo($wkfw_content); ?></textarea></td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="preview_flg" value="">
<input type="hidden" name="wkfwlib_filename" value="<? echo($wkfwlib_filename); ?>">
<input type="hidden" name="wkfwlib_id" value="<? echo($wkfwlib_id); ?>">
<input type="hidden" name="back" value="">
</form>
</td>
</tr>
</table>
<script type="text/javascript">
<?
// プレビュー押下時
if ($preview_flg == "1") {
// wkfw_content 保存
$ext = ".php";
$savefilename = "cl/template/tmp/{$session}_t{$ext}";

// 内容書き込み
$fp = fopen($savefilename, "w");
fwrite($fp, $wkfw_content, 2000000);

fclose($fp);

$wkfw_title = str_replace("'","\'", $wkfw_title);

?>
	var url = 'cl_workflow_lib_register_preview.php'
		+ '?session=<? echo($session); ?>';
	show_preview_window(url);
<?
}

if ($can_regist_flg == false) {
?>
document.wkfw.update.disabled = true;
<? } ?>

</script>

</body>
<? pg_close($con); ?>
</html>
