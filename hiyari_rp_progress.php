<?
require_once('about_comedix.php');
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("hiyari_report_class.php");
require_once("Cmx.php");
require_once("aclg_set.php");
require_once('class/Cmx/Model/SystemConfig.php');
require_once('Cmx/View/Smarty.php');

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

//====================================
// 権限チェック
// ●インシデントレポート(47)
//====================================
$inci_auth_chk = check_authority($session, 47, $fname);
if ($inci_auth_chk == '0') {
    showLoginPage();
    exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);

//==================================================
//レポートが論理削除済みの場合
//==================================================
$rep_obj = new hiyari_report_class($con, $fname);
if($rep_obj->is_deleted_report($report_id))
{
    //画面終了。
    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
    <script language='javascript'>
    alert("指定された報告は既に削除されています。");
    window.close();
    </script>
    </head>
    <body>
    </body>
    </html>
    <?
    pg_close($con);
    exit;
}

//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();

//デザイン（1=旧デザイン、2=新デザイン）
$design_mode = $sysConf->get('fantol.design.mode');

//====================================
// パラメータ取得
//====================================
$mail_id = ($_GET['mail_id'] ) ? $_GET['mail_id'] : $_POST['mail_id'];
$mail_id_mode = ($_GET['mail_id_mode']) ? $_GET['mail_id_mode'] : $_POST['mail_id_mode'];
$tab_switch = $_GET['tab_switch'];
$return_list = ($_GET['return_list']) ? $_GET['return_list'] : $_POST['return_list']; //リスト一覧画面から 20140508 R.C

//====================================
// ログインユーザのＳＭチェック
//====================================
$sm_auth_flg = "0";
if(chk_safety_manager($con, $fname, $session)) {
    $sm_auth_flg = "1";
    if($tab_switch == "") {
        $flg = inci_report_progress_check($con, $fname, $report_id);
        if($flg) {
            $tab_switch = "PRGRES_TAB_2";
        } else {
            $tab_switch = "PRGRES_TAB_1";
        }
    }
} else {
    if($tab_switch == "") {
        $tab_switch = "PRGRES_TAB_2";
    }
}

//====================================
// 処理分岐
//====================================
switch ($tab_switch) {
    case "regist_tab1":
        //ユーザーID取得
        $emp_id = get_emp_id($con, $session, $fname);
        // 進捗設定処理
        regist_progress_setting($con, $fname, $emp_id);
        $tab_switch = "PRGRES_TAB_2";
        break;
    case "regist_tab2":
        // 進捗登録処理
        regist_progress($con, $fname, $design_mode,$mail_id);
        $tab_switch = "PRGRES_TAB_2";
        break;
    default:
        break;
}

//====================================
// 初期化処理
//====================================
$emp_id = sel_emp_id($con, $fname, $session);
$report_title = sel_report_title($con, $fname, $report_id);

// インシデント権限マスタ取得
$arr_auth = array('SM');
$arr_inci = get_inci_mst($con, $fname, $arr_auth);
$auth_short_name = $arr_inci['SM']['auth_short_name'];

//========================================================================
//テンプレートに値を設定
//========================================================================
$smarty = new Cmx_View();

$smarty->assign("INCIDENT_TITLE", $INCIDENT_TITLE);
$smarty->assign("session", $session);

$smarty->assign("emp_id", $emp_id);

$smarty->assign("report_id", $report_id);
$smarty->assign("mail_id", $mail_id);
$smarty->assign("mail_id_mode", $mail_id_mode);

$arr_btn_flg = get_inci_btn_show_flg($con, $fname, $session);
$smarty->assign("arr_btn_flg", $arr_btn_flg);

$smarty->assign("tab_switch", $tab_switch);
$smarty->assign("sm_auth_flg", $sm_auth_flg);

$smarty->assign("report_title", $report_title);
$smarty->assign("return_list", $return_list);  // 20140508 R.C

if($tab_switch == "PRGRES_TAB_1") {
    //--------------------------------------------------------------------------
    //患者影響レベルのタイトル
    //--------------------------------------------------------------------------
    $sql  = "select * from inci_easyinput_item_mst";
    $cond = "where grp_code = 90 and easy_item_code = 10";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $smarty->assign("level_title", pg_fetch_result($sel,0,"easy_item_name"));

    //--------------------------------------------------------------------------
    //患者影響レベル
    //--------------------------------------------------------------------------
    $inci_lvl = search_inci_easyinput_data($con, $fname, $report_id);
    $level_infos = $rep_obj->get_inci_level_infos($inci_lvl);

    //説明ポップアップの内容
    if ($design_mode == 1){
        $level_detail = "";
        if(count($level_infos) > 0){
            $level_detail .= "<table>";
            foreach ($level_infos as $info){
                if($info["use_flg"]){
                    $easy_name = h($info['easy_name'])."：";
                    $message   = h($info['message']);
                    $level_detail .= "<tr valign=\\'top\\'><td align=\\'right\\' nowrap><font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>$easy_name</font></td>";
                    $level_detail .= "<td><font size=\\'3\\' face=\\'ＭＳ Ｐゴシック, Osaka\\' class=\\'j12\\'>$message</font></td></tr>";
                }
            }
            $level_detail .= "</table>";
        }
        $smarty->assign("level_detail", h($level_detail));
    }
    
    //患者影響レベル一覧
    $level_list = array();
    foreach ($level_infos as $info){
        if($info["use_flg"]){
            $key = $info["easy_code"];
            $temp['key'] = $key;
            $temp['val'] = h($info["easy_name"]);
            $temp['description'] = h($info['message']);
            $temp['selected'] = ($inci_lvl == $key) ? "selected" : "";
            $level_list[] = $temp;
        }
    }  
    $smarty->assign("level_list", $level_list);
    
    //患者影響レベルに変更があった場合(履歴が2件以上ある場合)
    $level_rireki = get_level_rireki($con, $fname, $report_id);
    if(count($level_rireki) >= 2){
        $level_rireki_info = array();

        $mouseover_popup_flg = is_anonymous_readable($session, $con, $fname);
        $smarty->assign("mouseover_popup_flg", $mouseover_popup_flg);

        $r_index = -1;
        foreach($level_rireki as $lr){
            $temp = array();
            
            $level = $lr["input_item"];
            $r_date = $lr["report_rireki_date"];
            $r_emp_id = $lr["report_rireki_emp_id"];
            $r_anonymous_flg = $lr["report_rireki_emp_anonymous_flg"];
            $r_emp_name = get_emp_kanji_name($con,$r_emp_id,$fname);

            $level_name = "";
            foreach($level_infos as $info){
                if($info["easy_code"] == $level){
                    $level_name = $info["easy_name"];
                }
            }

            $r_index++;
            if($r_index == 0){
                $r_index_name = "初期値";
            }
            else{
                $r_index_name = "更新".$r_index;
            }

            $temp['index_name'] = $r_index_name;
            $temp['date'] = $r_date;

            if($r_anonymous_flg == "t"){
                $disp_emp_name =  h(get_anonymous_name($con,$fname));
            }
            else{
                $disp_emp_name =  h($r_emp_name);
            }
            $temp['disp_emp_name'] = $disp_emp_name;

            if($r_anonymous_flg == "t" && $mouseover_popup_flg == "t"){
                $temp['real_name'] = h($r_emp_name);
            }

            $temp['level_name'] = h($level_name);
            
            $level_rireki_info[] = $temp;
        }
        $smarty->assign("level_rireki_info", $level_rireki_info);
    }

    //担当毎の設定一覧
    $arr_inci_progress = search_inci_report_progress($con, $fname, $report_id);
    $num = pg_numrows($arr_inci_progress);
    $setting_info = array();
    for($i=0; $i<$num; $i++){
        $temp['auth_name'] = h(pg_result($arr_inci_progress,$i,"auth_name"));
        $temp['auth'] = pg_result($arr_inci_progress,$i,"auth");
        $temp['use_flg'] = pg_result($arr_inci_progress,$i,"use_flg");
        $setting_info[] = $temp;
    }
    $smarty->assign("setting_info", $setting_info);
}

if($tab_switch == "PRGRES_TAB_2") {
    // 本日日付
    if ($design_mode == 1){
        $smarty->assign("this_year", date('Y'));
        $smarty->assign("this_month", date('m'));
        $smarty->assign("this_day", date('d'));
    }

    //権限毎の進捗情報
    $progress_info = array();
    $sel_auth_info = search_inci_auth_emp($con, $fname, $session, $report_id);
    foreach($sel_auth_info as $info) {
        if($info['use_flg'] != "t" || !is_usable_auth($info['auth'],$fname,$con)) {
            continue;
        }

        $regist_flg = "0";
        if ($info['auth_part'] == "決裁者" || $info['auth_part'] == "代行者") {
            $regist_flg = "1";
        }

        $temp['auth_name'] = h($info['auth_name']);
        $temp['your_auth'] = h($info['auth_part']);

        if ($design_mode == 1){
            //受付日
            $temp['dates']['from']['years'] = get_select_years(20, $info['from_year']);
            $temp['dates']['from']['months'] = get_select_months($info['from_mon']);
            $temp['dates']['from']['days'] = get_select_days($info['from_day']);

            //確認日
            $temp['dates']['to']['years'] = get_select_years(20, $info['to_year']);
            $temp['dates']['to']['months'] = get_select_months($info['to_mon']);
            $temp['dates']['to']['days'] = get_select_days($info['to_day']);
        }
        else{
            //受付日
            if ($info['from_year'] == ''){
                $temp['dates']['from']['date'] = '';
            }
            else{
                $temp['dates']['from']['date'] = $info['from_year'] . '/' . $info['from_mon'] . '/' . $info['from_day'];
            }

            //確認日
            if ($info['to_year'] == ''){
                $temp['dates']['to']['date'] = '';
            }
            else{
                $temp['dates']['to']['date'] = $info['to_year'] . '/' .  $info['to_mon'] . '/' . $info['to_day'];
            }

            //評価日
            if ($info['eval_year'] == ''){
                $temp['dates']['eval']['date'] = '';
            }
            else{
                $temp['dates']['eval']['date'] = $info['eval_year'] . '/' . $info['eval_mon'] . '/' . $info['eval_day'];
            }
        }

        $temp['dates']['from']['is_disabled'] = ($regist_flg == "0");
        $temp['dates']['to']['is_disabled'] = ($regist_flg == "0");
        if ($design_mode == 2){
            if ($regist_flg == "0"){
                $temp['dates']['eval']['is_disabled'] = true;
            }
            else{
                $temp['dates']['eval']['is_disabled'] = !($sysConf->get('fantol.evaluation.use.' . strtolower($info['auth']) . '.flg') == 't');
            }
        }
        
        $temp['updater'] = $info['update_emp_name'];
        $temp['regist_flg'] = $regist_flg;
        $temp['auth'] = $info['auth'];

        $progress_info[] = $temp;
    }
    $smarty->assign("progress_info", $progress_info);
    
    $edit_lock_flg = $rep_obj->get_sm_lock($report_id);
    $smarty->assign("edit_lock_flg", $edit_lock_flg);
    
    $smarty->assign("sm_short_name", $auth_short_name);
}

//========================================================================
//表示
//========================================================================
if ($design_mode == 1){
    $smarty->display("hiyari_rp_progress1.tpl");
}
else{
    $smarty->display("hiyari_rp_progress2.tpl");
}

//========================================================================
// 【emp_id】取得
//========================================================================
function sel_emp_id($con, $fname, $session_id) {
    $sql = "select emp_id from session ";
    $cond = "where session_id = '$session_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $tmp_emp_id = pg_result($sel,0,"emp_id");
    return $tmp_emp_id;
}

//========================================================================
// レポートタイトル取得
//========================================================================
function sel_report_title($con, $fname, $report_id) {
    $sql = "select report_title from inci_report ";
    $cond = "where report_id = '$report_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $tmp_report_title = pg_result($sel,0,"report_title");
    return $tmp_report_title;
}

//========================================================================
// レポート進捗情報取得
//========================================================================
function search_inci_auth_emp($con, $fname, $session_id, $report_id) {
    $sql = "select " .
                "a.auth_name, " .
                "b.auth_part, " .
                "substr(c.start_date,1,4) as from_year, " .
                "substr(c.start_date,6,2) as from_mon, " .
                "substr(c.start_date,9,2) as from_day, " .
                "substr(c.end_date,1,4) as to_year, " .
                "substr(c.end_date,6,2) as to_mon, " .
                "substr(c.end_date,9,2) as to_day, " .
                "substr(c.evaluation_date,1,4) as eval_year, " .
                "substr(c.evaluation_date,6,2) as eval_mon, " .
                "substr(c.evaluation_date,9,2) as eval_day, " .
                "c.update_emp_name, " .
                "a.auth, " .
                "c.use_flg ".
            "from inci_auth_mst a " .
            "left outer join " .
                "(select " .
                    "a.auth, " .
                    "a.auth_part " .
                "from " .
//                  "inci_auth_emp a, " .
                    "( " . get_sql_emp_auth_part_max(get_sql_emp_auth_all_for_report($report_id,$con, $fname)) . " ) a, " .
                    "session b " .
                "where a.emp_id = b.emp_id " .
                "and b.session_id = '$session_id') b " .
            "on a.auth = b.auth " .
            "left outer join " .
                "(select " .
                    "a.report_id, " .
                    "a.auth, a.start_date, " .
                    "a.end_date, a.evaluation_date, a.update_emp_id, " .
                    "b.emp_lt_nm || ' ' || b.emp_ft_nm as update_emp_name, " .
                    "a.use_flg ".
                "from inci_report_progress a " .
                "left outer join empmst b " .
                "on a.update_emp_id = b.emp_id " .
                "where report_id = '$report_id') c " .
            "on a.auth = c.auth " .
            "order by a.auth_rank ";

    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    return pg_fetch_all($sel);
}

//========================================================================
// レポート進捗登録更新
//========================================================================
function regist_progress($con, $fname, $design_mode,$mail_id) {
    $report_id = $_POST["report_id"];
    $emp_id = $_POST["emp_id"];

    // トランザクションの開始
    pg_query($con, "begin transaction");

    // 進捗テーブル登録
    for ($i=0; $i<6; $i++) {
        if ($_POST["regist_flg".$i] == "1") {
            regist_inci_report_progress($con, $fname, $i, $report_id, $emp_id, $design_mode,$mail_id);
        }
    }

    // レポートテーブル編集ロックフラグ更新
    if($_POST["sm_auth_flg"] == '1') {
        $edit_lock_flg = $_POST["edit_lock_flg"];
        $rep_obj = new hiyari_report_class($con, $fname);
        $rep_obj->set_sm_lock($report_id, $edit_lock_flg);
    }
    
    // トランザクションをコミット
    pg_query($con, "commit");

    // 親画面をリフレッシュする。
    echo("<script language='javascript'>if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>\n");

}

//========================================================================
// レポート進捗登録更新
//========================================================================
function regist_inci_report_progress($con, $fname, $idx, $report_id, $emp_id, $design_mode,$mail_id) {
    $auth = $_POST["auth".$idx];

    $start_date = null;
    $end_date = null;
    $eval_date = null;
    $star_flg = "f";
    $end_flg = "f";
    $eval_flg = "f";
    $update_emp_id = null;

    if ($design_mode == 1){
        $year_from= $_POST["from_year".$idx];
        $mon_from = $_POST["from_mon".$idx];
        $day_from = $_POST["from_day".$idx];
        if ($year_from != "-" && $mon_from != "-" && $day_from != "-") {
            $start_date = $year_from."/".$mon_from."/".$day_from;
            $star_flg = "t";
            $update_emp_id = $emp_id;
        }

        $year_to = $_POST["to_year".$idx];
        $mon_to = $_POST["to_mon".$idx];
        $day_to = $_POST["to_day".$idx];
        if ($year_to != "-" && $mon_to != "-" && $day_to != "-") {
            $end_date = $year_to."/".$mon_to."/".$day_to;
            $end_flg = "t";
            $update_emp_id = $emp_id;
        }
    }
    else{
        if ($_POST['from'.$idx] != ''){
            $start_date = $_POST['from'.$idx];
            $star_flg = "t";
            $update_emp_id = $emp_id;
        }
        if ($_POST['to'.$idx] != '') {
            $end_date = $_POST['to'.$idx];
            $end_flg = "t";
            $update_emp_id = $emp_id;
        }
        if ($_POST['eval'.$idx] != '') {
            $eval_date = $_POST['eval'.$idx];
            $eval_flg = "t";
            $update_emp_id = $emp_id;
        }
    }
    
    $sql = "select count(*) as cnt from inci_report_progress ";
    $cond = "where report_id = '$report_id' and auth = '$auth'";
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $sel_cnt = pg_result($sel,0,"cnt");

    // 更新処理
    if ($sel_cnt > 0) {
        $sql = "update inci_report_progress set ";
        $set = array("update_emp_id", "start_flg", "end_flg", "start_date", "end_date", "evaluation_flg", "evaluation_date");
        $setvalue = array($update_emp_id, $star_flg, $end_flg, $start_date, $end_date, $eval_flg, $eval_date);
        $cond = "where report_id = '$report_id' and auth = '$auth'";
        $up = update_set_table($con,$sql,$set,$setvalue,$cond,$fname);

        if($up == 0){
            pg_exec($con,"rollback");
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        
        
         //if ($sysConf->get('fantol.progress.' . strtolower($arr_key) . '.uketsuke.flg') == 't'){
            $sql = "update inci_report_progress_detail set ";
            $set = array("emp_id", "start_flg", "end_flg", "start_date", "end_date", "evaluation_flg", "evaluation_date");
            $setvalue = array($emp_id, $star_flg, $end_flg, $start_date, $end_date, $eval_flg, $eval_date);
            $cond = "where mail_id = '$mail_id' and auth = '$auth' and emp_id = '$emp_id'";
            $up = update_set_table($con,$sql,$set,$setvalue,$cond,$fname);

            if($up == 0){
                pg_exec($con,"rollback");
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        //}
        
        
        
    }
    // 新規登録処理
    else {
        $sql = "insert into inci_report_progress(report_id, auth, update_emp_id, start_flg, end_flg, start_date, end_date, evaluation_flg, evaluation_date) values (";
        $content = array($report_id, $auth, $update_emp_id, $star_flg, $end_flg, $start_date, $end_date, $eval_flg, $eval_date);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_exec($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}

//========================================================================
// ＳＭチェック処理
//========================================================================
function chk_safety_manager($con, $fname, $session) {
    $chk_flg = false;
    $sql = "select count(*) as cnt ".
           "from inci_auth_emp a, ".
           "(select emp_id from session where session_id = '$session') b ".
           "where a.emp_id = b.emp_id ".
           "and a.auth = 'SM'";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $cnt = pg_result($sel,0,"cnt");

    if ($cnt > 0) {
        $chk_flg = true;
    }

    return $chk_flg;
}

//========================================================================
// 進捗テーブル取得処理
//========================================================================
function search_inci_report_progress($con, $fname, $report_id) {
    $sql = "select " .
                "a.auth_name,".
                "a.auth,".
                "b.use_flg ".
            "from ".
                "inci_auth_mst a ".
                "left outer join inci_report_progress b ".
                "on a.auth = b.auth and b.report_id = '$report_id'";
    $cond = "where a.use_flg order by a.auth_rank";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    return $sel;
}

//========================================================================
// 進捗テーブル・進捗管理対象フラグチェック処理
//========================================================================
function inci_report_progress_check($con, $fname, $report_id) {
    $chk_flg = false;

    $sql = "select * from inci_report_progress ";
    $cond = "where report_id = $report_id and use_flg = 't' ";
    $sel = select_from_table($con, $sql, $cond, $fname);

    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $num = pg_numrows($sel);
    if($num > 0) {
        $chk_flg = true;
    }

    return $chk_flg;
}
//========================================================================
// 進捗設定処理
//========================================================================
function regist_progress_setting($con, $fname,$emp_id) {
    // 2012/01/24 invalid
    // 報告書更新ロック
    // action_lock(LOCK_HIYARI_REPORT_UPDATE);

    // トランザクションの開始
    pg_query($con, "begin transaction");

    // 進捗設定
    regist_use_flg($con, $fname);

    // インシレベル
    regist_incieasyinput_data($con, $fname, $emp_id);

    // トランザクションをコミット
    pg_query($con, "commit");

    // 2012/01/24 invalid
    // 報告書更新ロック解除
    // action_unlock(LOCK_HIYARI_REPORT_UPDATE);


    echo("<script language='javascript'>if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>\n");
}


//========================================================================
// 進捗設定（工程）登録処理
//========================================================================
function regist_use_flg($con, $fname) {
    $report_id = $_POST["report_id"];

    $check_sm = $_POST["SM"];
    $check_rm = $_POST["RM"];
    $check_ra = $_POST["RA"];
    $check_sd = $_POST["SD"];
    $check_md = $_POST["MD"];
    $check_hd = $_POST["HD"];

    $arr_auth = array("SM" => $check_sm, "RM" => $check_rm, "RA" => $check_ra, "SD" => $check_sd, "MD" => $check_md, "HD" => $check_hd);

    foreach ($arr_auth as $key => $tmp) {

        $auth = $key;
        if ($tmp == "t") {
            $use_flg = 't';
        } else {
            $use_flg = 'f';
        }

        $sql = "select count(*) as cnt from inci_report_progress ";
        $cond = "where report_id = '$report_id' and auth = '$auth'";
        $sel = select_from_table($con,$sql,$cond,$fname);
        if($sel == 0){
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }
        $sel_cnt = pg_result($sel,0,"cnt");

        if ($sel_cnt > 0) {

            // 更新処理
            $sql = "update inci_report_progress set ";
            $set = array("use_flg");
            $setvalue = array($use_flg);
            $cond = "where report_id = '$report_id' and auth = '$auth'";
            $up = update_set_table($con,$sql,$set,$setvalue,$cond,$fname);

            if($up == 0){
                pg_exec($con,"rollback");
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        } else {

            // 新規登録処理
            $sql = "insert into inci_report_progress(report_id, auth, use_flg) values (";
            $content = array($report_id, $auth, $use_flg);
            $ins = insert_into_table($con, $sql, $content, $fname);
            if ($ins == 0) {
                pg_exec($con, "rollback");
                pg_close($con);
                echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
                echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
                exit;
            }
        }
    }
}

//========================================================================
// インシレベル(患者影響レベル）取得
//========================================================================
function search_inci_easyinput_data($con, $fname, $report_id) {
    $input_item = "";
    $sql = "select a.input_item as input_item ".
           "from inci_easyinput_data a, ".
           "(select eid_id from inci_report where report_id = '$report_id') b ".
           "where a.eid_id = b.eid_id ".
           "and a.grp_code = '90' ".
           "and a.easy_item_code = '10'";
    $cond = "";
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $rows = pg_numrows($sel);
    if ($rows > 0) {
        $input_item = pg_result($sel,0,"input_item");
    }
    return $input_item;
}

//========================================================================
// インシレベル(患者影響レベル）登録処理
//========================================================================
function regist_incieasyinput_data($con, $fname,$emp_id) {
    $report_id = $_POST["report_id"];
    $lvl = $_POST["inci_lvl"];

    $rep_obj = new hiyari_report_class($con, $fname);
    $rep_obj->update_inci_level($report_id,$lvl);

    //報告書履歴登録
    $rep_obj->set_report_rireki($report_id,$emp_id,"");

}

//========================================================================
// 患者影響レベルの更新履歴取得
//========================================================================
function get_level_rireki($con, $fname, $report_id)
{
    $sql  = " select report_rireki_id,report_rireki_date,report_rireki_emp_id,input_item,(r3.anonymous_send_flg = 't') as report_rireki_emp_anonymous_flg";
    $sql .= " from ";
    $sql .= " (select report_rireki_id,report_rireki_date,report_rireki_emp_id,report_rireki_mail_id,eid_id from inci_report_rireki where report_id = $report_id) r1";
    $sql .= " left join ";
    $sql .= " (select eid_id,input_item from inci_easyinput_data where grp_code = 90 and easy_item_code = 10) r2";
    $sql .= " on r1.eid_id = r2.eid_id";
    $sql .= " left join";
    $sql .= " inci_mail_send_info r3";
    $sql .= " on r1.report_rireki_mail_id = r3.mail_id";
    $sql .= " order by report_rireki_id";
    $sel = select_from_table($con,$sql,"",$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    //患者影響レベルが変更となったときの履歴だけを抽出する。
    $rireki_list = "";
    $old_level = "----------";//最初の１件目を必ずヒットさせるため、ありえないコードを設定
    $rireki_arr = pg_fetch_all($sel);
    foreach($rireki_arr as $rireki)
    {
        $level = $rireki["input_item"];
        if($level != $old_level)
        {
            $rireki_list[] = $rireki;
            $old_level = $level;
        }
    }
    return $rireki_list;
}

//========================================================================
// 【年】プルダウン項目
//========================================================================
function get_select_years($num, $fix)
{
    $years = array();
    $now = date("Y");
    if ($fix > 0) {
        $num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
    }
    for ($i = 0; $i < $num; $i++) {
        $yr = $now - $i;
        $temp['year'] = $yr;
        $temp['is_selected'] = ($fix == $yr);
        $years[] = $temp;
    }

    return $years;
}

//========================================================================
// 【月】プルダウン項目
//========================================================================
function get_select_months($fix)
{
    $months = array();
    for ($i = 1; $i <= 12; $i++) {
        $temp['val'] = sprintf("%02d", $i);
        $temp['is_selected'] = ($i == $fix);
        $temp['month'] = $i;
        $months[] = $temp;
    }

    return $months;
}

//========================================================================
// 【日】プルダウン項目初期化
//========================================================================
function get_select_days($fix)
{
    $days = array();
    for ($i = 1; $i <= 31; $i++) {
        $temp['val'] = sprintf("%02d", $i);
        $temp['is_selected'] = ($i == $fix);
        $temp['day'] = $i;
        $days[] = $temp;
    }

    return $days;
}

pg_close($con);
