<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 勤務時間修正申請参照</title>
<?
require("about_session.php");
require("about_authority.php");
require("get_values.ini");
require("show_clock_in_common.ini");
require("show_attendance_pattern.ini");
require_once("atdbk_common_class.php");

require_once("show_timecard_apply_detail.ini");
require_once("show_timecard_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("timecard_bean.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

switch ($wherefrom) {
    case "7":  // 勤務シフト作成の実績入力画面より
    case "8":
        $checkauth = check_authority($session, 69, $fname);
        if ($checkauth == "0") {
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
            exit;
        }
        break;
    default:
        // 勤務管理権限チェック
        $checkauth = check_authority($session, 42, $fname);
        if ($checkauth == "0") {
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showLoginPage(window);</script>");
            exit;
        }
        break;
}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);

if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}


// 勤務時間修正申請情報を取得
$sql = "select empmst.emp_id, empmst.emp_lt_nm, empmst.emp_ft_nm, tmmdapply.target_date, mdfyrsn.reason, tmmdapply.reason as other_reason, tmmdapply.apply_time, tmmdapply.comment, tmmdapply.b_pattern, tmmdapply.b_reason, tmmdapply.b_night_duty, tmmdapply.b_allow_id, tmmdapply.b_start_time, tmmdapply.b_out_time, tmmdapply.b_ret_time, tmmdapply.b_end_time, tmmdapply.b_o_start_time1, tmmdapply.b_o_end_time1, tmmdapply.b_o_start_time2, tmmdapply.b_o_end_time2, tmmdapply.b_o_start_time3, tmmdapply.b_o_end_time3, tmmdapply.b_o_start_time4, tmmdapply.b_o_end_time4, tmmdapply.b_o_start_time5, tmmdapply.b_o_end_time5, tmmdapply.b_o_start_time6, tmmdapply.b_o_end_time6, tmmdapply.b_o_start_time7, tmmdapply.b_o_end_time7, tmmdapply.b_o_start_time8, tmmdapply.b_o_end_time8, tmmdapply.b_o_start_time9, tmmdapply.b_o_end_time9, tmmdapply.b_o_start_time10, tmmdapply.b_o_end_time10, tmmdapply.a_pattern, tmmdapply.a_reason, tmmdapply.a_night_duty, tmmdapply.a_allow_id, tmmdapply.a_start_time, tmmdapply.a_out_time, tmmdapply.a_ret_time, tmmdapply.a_end_time, tmmdapply.a_o_start_time1, tmmdapply.a_o_end_time1, tmmdapply.a_o_start_time2, tmmdapply.a_o_end_time2, tmmdapply.a_o_start_time3, tmmdapply.a_o_end_time3, tmmdapply.a_o_start_time4, tmmdapply.a_o_end_time4, tmmdapply.a_o_start_time5, tmmdapply.a_o_end_time5, tmmdapply.a_o_start_time6, tmmdapply.a_o_end_time6, tmmdapply.a_o_start_time7, tmmdapply.a_o_end_time7, tmmdapply.a_o_start_time8, tmmdapply.a_o_end_time8, tmmdapply.a_o_start_time9, tmmdapply.a_o_end_time9, tmmdapply.a_o_start_time10, tmmdapply.a_o_end_time10, tmmdapply.b_tmcd_group_id, tmmdapply.a_tmcd_group_id, tmmdapply.b_meeting_time, tmmdapply.a_meeting_time, tmmdapply.b_previous_day_flag, tmmdapply.a_previous_day_flag, tmmdapply.b_next_day_flag, tmmdapply.a_next_day_flag, tmmdapply.b_meeting_start_time, tmmdapply.b_meeting_end_time, tmmdapply.a_meeting_start_time, tmmdapply.a_meeting_end_time, tmmdapply.a_allow_count, tmmdapply.a_over_start_time, tmmdapply.a_over_end_time, tmmdapply.b_allow_count, tmmdapply.b_over_start_time, tmmdapply.b_over_end_time ". 
	", tmmdapply.b_over_start_next_day_flag ".
	", tmmdapply.b_over_end_next_day_flag ".
	", tmmdapply.a_over_start_next_day_flag ".
	", tmmdapply.a_over_end_next_day_flag ".
	", tmmdapply.b_over_start_time2 ".
	", tmmdapply.b_over_end_time2 ".
	", tmmdapply.a_over_start_time2 ".
	", tmmdapply.a_over_end_time2 ".
	", tmmdapply.b_over_start_next_day_flag2 ".
	", tmmdapply.b_over_end_next_day_flag2 ".
	", tmmdapply.a_over_start_next_day_flag2 ".
	", tmmdapply.a_over_end_next_day_flag2 ".
	" from (tmmdapply inner join empmst on tmmdapply.emp_id = empmst.emp_id) left join mdfyrsn on tmmdapply.reason_id = mdfyrsn.reason_id";
$cond = "where tmmdapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
$apply_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$target_date = pg_fetch_result($sel, 0, "target_date");
//一括修正フラグ、申請情報の日付を年月にした場合に、一括修正とする
$all_flg =  (strlen($target_date) == 6) ? true : false;

$reason = pg_fetch_result($sel, 0, "reason");
$other_reason = pg_fetch_result($sel, 0, "other_reason");
$apply_time = pg_fetch_result($sel, 0, "apply_time");
$comment = pg_fetch_result($sel, 0, "comment");

$b_pattern = pg_fetch_result($sel, 0, "b_pattern");
$b_reason = pg_fetch_result($sel, 0, "b_reason");
$b_night_duty = pg_fetch_result($sel, 0, "b_night_duty");
$b_allow_id = pg_fetch_result($sel, 0, "b_allow_id");
$b_start_time = format_time(pg_fetch_result($sel, 0, "b_start_time"));
$b_out_time = format_time(pg_fetch_result($sel, 0, "b_out_time"));
$b_ret_time = format_time(pg_fetch_result($sel, 0, "b_ret_time"));
$b_end_time = format_time(pg_fetch_result($sel, 0, "b_end_time"));

$a_pattern = pg_fetch_result($sel, 0, "a_pattern");
$a_reason = pg_fetch_result($sel, 0, "a_reason");
$a_night_duty = pg_fetch_result($sel, 0, "a_night_duty");
$a_allow_id = pg_fetch_result($sel, 0, "a_allow_id");
$a_start_time = format_time(pg_fetch_result($sel, 0, "a_start_time"));
$a_out_time = format_time(pg_fetch_result($sel, 0, "a_out_time"));
$a_ret_time = format_time(pg_fetch_result($sel, 0, "a_ret_time"));
$a_end_time = format_time(pg_fetch_result($sel, 0, "a_end_time"));
for ($i = 1; $i <= 10; $i++) {
	$b_start_time_ver = "b_o_start_time$i";
	$b_end_time_ver = "b_o_end_time$i";
	$a_start_time_ver = "a_o_start_time$i";
	$a_end_time_ver = "a_o_end_time$i";
	$$b_start_time_ver = format_time(pg_fetch_result($sel, 0, "$b_start_time_ver"));
	$$b_end_time_ver = format_time(pg_fetch_result($sel, 0, "$b_end_time_ver"));
	$$a_start_time_ver = format_time(pg_fetch_result($sel, 0, "$a_start_time_ver"));
	$$a_end_time_ver = format_time(pg_fetch_result($sel, 0, "$a_end_time_ver"));
}

$b_meeting_time = pg_fetch_result($sel, 0, "b_meeting_time");
$b_tmcd_group_id = pg_fetch_result($sel, 0, "b_tmcd_group_id");
$a_meeting_time = pg_fetch_result($sel, 0, "a_meeting_time");
$a_tmcd_group_id = pg_fetch_result($sel, 0, "a_tmcd_group_id");
$b_previous_day_flag = pg_fetch_result($sel, 0, "b_previous_day_flag");
$a_previous_day_flag = pg_fetch_result($sel, 0, "a_previous_day_flag");
$b_next_day_flag = pg_fetch_result($sel, 0, "b_next_day_flag");
$a_next_day_flag = pg_fetch_result($sel, 0, "a_next_day_flag");

$b_meeting_start_time = format_time(pg_fetch_result($sel, 0, "b_meeting_start_time"));
$b_meeting_end_time = format_time(pg_fetch_result($sel, 0, "b_meeting_end_time"));
$a_meeting_start_time = format_time(pg_fetch_result($sel, 0, "a_meeting_start_time"));
$a_meeting_end_time = format_time(pg_fetch_result($sel, 0, "a_meeting_end_time"));

if ($a_meeting_time != null ){
	$a_meeting_time_hh = substr($a_meeting_time, 0, 2);
	$a_meeting_time_mm = substr($a_meeting_time, 2, 2);
}
else{
	$a_meeting_time_hh = "";
	$a_meeting_time_mm = "";
}

if ($b_meeting_time != null ){
	$b_meeting_time_hh = substr($b_meeting_time, 0, 2);
	$b_meeting_time_mm = substr($b_meeting_time, 2, 2);
}
else{
	$b_meeting_time_hh = "";
	$b_meeting_time_mm = "";
}

$b_allow_count = pg_fetch_result($sel, 0, "b_allow_count");
$b_allow_count_value = ($b_allow_id == "") ? "" : " {$b_allow_count}";
$b_over_start_time = format_time(pg_fetch_result($sel, 0, "b_over_start_time"));
$b_over_end_time = format_time(pg_fetch_result($sel, 0, "b_over_end_time"));
$a_allow_count = pg_fetch_result($sel, 0, "a_allow_count");
$a_allow_count_value = ($a_allow_id == "") ? "" : " {$a_allow_count}";
$a_over_start_time = format_time(pg_fetch_result($sel, 0, "a_over_start_time"));
$a_over_end_time = format_time(pg_fetch_result($sel, 0, "a_over_end_time"));
$b_over_start_next_day_flag = pg_fetch_result($sel, 0, "b_over_start_next_day_flag");
$b_over_end_next_day_flag = pg_fetch_result($sel, 0, "b_over_end_next_day_flag");
$a_over_start_next_day_flag = pg_fetch_result($sel, 0, "a_over_start_next_day_flag");
$a_over_end_next_day_flag = pg_fetch_result($sel, 0, "a_over_end_next_day_flag");

//残業時刻２
$b_over_start_time2 = format_time(pg_fetch_result($sel, 0, "b_over_start_time2"));
$b_over_end_time2 = format_time(pg_fetch_result($sel, 0, "b_over_end_time2"));
$a_over_start_time2 = format_time(pg_fetch_result($sel, 0, "a_over_start_time2"));
$a_over_end_time2 = format_time(pg_fetch_result($sel, 0, "a_over_end_time2"));
$b_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_start_next_day_flag2");
$b_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "b_over_end_next_day_flag2");
$a_over_start_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_start_next_day_flag2");
$a_over_end_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_end_next_day_flag2");

//前日チェックがあるかフラグ
$b_previous_day_flag_value = "";
$a_previous_day_flag_value = "";
if ($b_previous_day_flag == 1){
	$b_previous_day_flag_value = "　前日";
}

if ($a_previous_day_flag == 1){
	$a_previous_day_flag_value = "　前日";
}

//翌日チェックがあるかフラグ
$b_next_day_flag_value = "";
$a_next_day_flag_value = "";
if ($b_next_day_flag == 1){
	$b_next_day_flag_value = "　翌日";
}

if ($a_next_day_flag == 1){
	$a_next_day_flag_value = "　翌日";
}

$b_over_start_next_day_flag_value = "";
$a_over_start_next_day_flag_value = "";
if ($b_over_start_next_day_flag == 1){
	$b_over_start_next_day_flag_value = "　翌日";
}
if ($a_over_start_next_day_flag == 1){
	$a_over_start_next_day_flag_value = "　翌日";
}
$b_over_end_next_day_flag_value = "";
$a_over_end_next_day_flag_value = "";
if ($b_over_end_next_day_flag == 1){
	$b_over_end_next_day_flag_value = "　翌日";
}
if ($a_over_end_next_day_flag == 1){
	$a_over_end_next_day_flag_value = "　翌日";
}
$b_over_start_next_day_flag2_value = "";
$a_over_start_next_day_flag2_value = "";
if ($b_over_start_next_day_flag2 == 1){
	$b_over_start_next_day_flag2_value = "　翌日";
}
if ($a_over_start_next_day_flag2 == 1){
	$a_over_start_next_day_flag2_value = "　翌日";
}
$b_over_end_next_day_flag2_value = "";
$a_over_end_next_day_flag2_value = "";
if ($b_over_end_next_day_flag2 == 1){
	$b_over_end_next_day_flag2_value = "　翌日";
}
if ($a_over_end_next_day_flag2 == 1){
	$a_over_end_next_day_flag2_value = "　翌日";
}
//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
if ($tmcd_group_id == null && strlen($tmcd_group_id) == 0 && $err_back_page_flg == false){
	$tmcd_group_id = get_timecard_group_id($con, $apply_emp_id, $fname);
}

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);


$b_group_name = ($b_tmcd_group_id == "") ? "": $atdbk_common_class->get_group_name($b_tmcd_group_id);
$a_group_name = ($a_tmcd_group_id == "") ? "": $atdbk_common_class->get_group_name($a_tmcd_group_id);

$b_pattern_name = ($b_pattern == "") ? "": $atdbk_common_class->get_pattern_name($b_tmcd_group_id, $b_pattern);
$a_pattern_name = ($a_pattern == "") ? "": $atdbk_common_class->get_pattern_name($a_tmcd_group_id, $a_pattern);

$b_reason_name = ($b_reason == "") ? "": $atdbk_common_class->get_reason_name($b_reason);
$a_reason_name = ($a_reason == "") ? "": $atdbk_common_class->get_reason_name($a_reason);

$b_night_duty_name = "";
if($b_night_duty == "1")
{
    $b_night_duty_name = "有り";
}
else if($b_night_duty == "2")
{
    $b_night_duty_name = "無し";
}	

$a_night_duty_name = "";
if($a_night_duty == "1")
{
    $a_night_duty_name = "有り";
}
else if($a_night_duty == "2")
{
    $a_night_duty_name = "無し";
}

$b_allow_id_name = "";
$a_allow_id_name = "";
foreach($arr_allowance as $allowance)
{
	if($allowance["allow_id"] == $b_allow_id)
	{
		$b_allow_id_name = $allowance["allow_contents"];
	}

	if($allowance["allow_id"] == $a_allow_id)
	{
		$a_allow_id_name = $allowance["allow_contents"];
	}
}


$this_title = "勤務時間修正申請参照";
$this_url = "work_admin_modify_detail.php?session=$session&apply_id=$apply_id&pnt_url=" . urlencode($pnt_url);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">

function history_select(apply_id, target_apply_id) {

	document.mainform.apply_id.value = apply_id;
	document.mainform.target_apply_id.value = target_apply_id;
	document.mainform.action="timecard_modify_detail.php";
	document.mainform.submit();
}

function deleteApply() {
	if (confirm('削除します。よろしいですか？')) {
		document.mainform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード修正</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="24%">
<? show_timecard_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td valign="top" align="center" width="76%">
<img src="img/spacer.gif" alt="" width="1" height="2"><br>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td width="180" align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($this_title); ?></b></font></td>
<td width="5"></td>

<?
//一括修正以外の場合
if (!$all_flg) {
?>
<td width="180" align="center"><a href="atdbk_timecard_status_update.php?session=<? echo($session); ?>&emp_id=<? echo($apply_emp_id); ?>&date=<? echo($target_date); ?>&pnt_url=<? echo(urlencode($pnt_url)); ?>&ref_url=<? echo(urlencode($this_url)); ?>&ref_title=<? echo(urlencode($this_title)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤状況修正</font></a></td>
<? } ?>
<td>&nbsp;</td>
</tr>
</table>
<form name="mainform" action="work_admin_modify_delete.php" method="post">
<table width="640" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="220" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者</font></td>
<td width="420" ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($apply_emp_nm); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<?
//一括修正以外の場合
if (!$all_flg) {
?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_group_name, $a_group_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_pattern_name, $a_pattern_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_reason_name, $a_reason_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">当直</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_night_duty_name, $a_night_duty_name); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_allow_id_name.$b_allow_count_value, $a_allow_id_name.$a_allow_count_value); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_start_time.$b_previous_day_flag_value, $a_start_time.$a_previous_day_flag_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_out_time, $a_out_time); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">復帰時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_ret_time, $a_ret_time); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_end_time.$b_next_day_flag_value, $a_end_time.$a_next_day_flag_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業開始時刻1</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_over_start_time.$b_over_start_next_day_flag_value, $a_over_start_time.$a_over_start_next_day_flag_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業終了時刻1</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_over_end_time.$b_over_end_next_day_flag_value, $a_over_end_time.$a_over_end_next_day_flag_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業開始時刻2</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_over_start_time2.$b_over_start_next_day_flag2_value, $a_over_start_time2.$a_over_start_next_day_flag2_value); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業終了時刻2</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_value_diff($b_over_end_time2.$b_over_end_next_day_flag2_value, $a_over_end_time2.$a_over_end_next_day_flag2_value); ?></font></td>
</tr>
	<?
for ($i = 1; $i <= 10; $i++) {
	$b_start_time_ver = "b_o_start_time$i";
	$b_end_time_ver = "b_o_end_time$i";
	$a_start_time_ver = "a_o_start_time$i";
	$a_end_time_ver = "a_o_end_time$i";

	if ($$b_start_time_ver == "" && $$b_end_time_ver == "" && $$a_start_time_ver == "" && $$a_end_time_ver == "") {
		break;
	}

	echo("<tr height=\"22\">\n");
		echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}時刻{$i}</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
	show_term_diff($$b_start_time_ver, $$b_end_time_ver, $$a_start_time_ver, $$a_end_time_ver);
	echo("</font></td>\n");
	echo("</tr>\n");
}
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務開始時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	show_value_diff($b_meeting_start_time, $a_meeting_start_time);
?>
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務終了時刻</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	show_value_diff($b_meeting_end_time, $a_meeting_end_time);
?>
</font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">修正理由</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($reason . $other_reason); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(str_replace("\n", "<br>", $comment)); ?></font></td>

<? } else { ?>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<br>
一括修正申請です。
<br>
<br>
</font>
</td>
<? } ?>

</tr>

<?
$page = "work_admin_timecard";
show_application_apply_detail($con, $session, $fname, $apply_id, $page, $all_flg);
?>


</table>
</td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function show_value_diff($before, $after) {
	if ($before == "") {$before = "（登録なし）";}
	if ($after == "") {$after = "（登録なし）";}
	if ($after != $before) {
		echo("$before <font color=\"red\">→ $after</font>");
	} else {
		echo($before);
	}
}

function show_term_diff($before_start, $before_end, $after_start, $after_end) {
	if ($before_start == "") {$before_start = "（登録なし）";}
	if ($before_end == "") {$before_end = "（登録なし）";}
	if ($after_start == "") {$after_start = "（登録なし）";}
	if ($after_end == "") {$after_end = "（登録なし）";}
	show_value_diff("$before_start 〜 $before_end", "$after_start 〜 $after_end");
}

function get_reason_string($reason) {
	$reasons = array(
		"14" => "代替出勤",
		"15" => "振替出勤",
		"16" => "休日出勤",
		"1" => "有給休暇",
		"2" => "午前有休",
		"3" => "午後有休",
		"4" => "代替休暇",
		"17" => "振替休暇",
		"5" => "特別休暇",
		"6" => "一般欠勤",
		"7" => "病傷欠勤",
		"8" => "その他休",
		"9" => "通院",
		"10" => "私用",
		"11" => "交通遅延",
		"12" => "遅刻",
		"13" => "早退",
		"18" => "半前代替休",
		"19" => "半前代替休",
		"20" => "半前振替休",
		"21" => "半後振替休"
	);
	return $reasons[$reason];
}

function format_time($hhmm) {
	return preg_replace("/(\d{2})(\d{2})/", "$1:$2", $hhmm);
}
?>
