<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("cl_workflow_common.ini");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

$cl_title = cl_title_name();

/**
 * アプリケーションメニューHTML取得
 */
$wkfwctn_mnitm_str=get_wkfw_menuitem($session,$fname);

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベース接続オブジェクト取得
 */
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

/**
 * ログインユーザ情報取得
 */
require_once(dirname(__FILE__) . "/cl/model/workflow/empmst_model.php");
$empmst_model = new empmst_model($mdb2);
$arr_login = $empmst_model->select_login_emp($session);
$login_emp_id = $arr_login["emp_id"];

/**
 * 期間設定マスタ
 */
require_once(dirname(__FILE__) . "/cl/model/master/cl_mst_levelup_apply_term_model.php");
$levelup_apply_term_model = new cl_mst_levelup_apply_term_model($mdb2, $login_emp_id);

/**
 * 期間ＩＤ
 */
require_once(dirname(__FILE__) . "/cl/model/sequence/cl_mst_levelup_apply_term_id_seq_model.php");
$term_id_seq_model = new cl_mst_levelup_apply_term_id_seq_model($mdb2, $login_emp_id);

if ($_POST['regist_flg'] == 'true') {

	/**
	 * トランザクション開始
	 */
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	/**
	 * 期間取得（上期）
	 */
	$first_term = $levelup_apply_term_model->getRecordByDiv(1);

	/**
	 * 期間取得（下期）
	 */
	$second_term = $levelup_apply_term_model->getRecordByDiv(2);

	/**
	 * 期間削除（上期）
	 */
	if (
			$first_term['term_id'] != ''
		&&	(
				$_POST['date_first_from_m'].$_POST['date_first_from_d'] != $first_term['term_from_date']
			||	$_POST['date_first_to_m'].$_POST['date_first_to_d'] != $first_term['term_to_date']
		)
	) {
		$levelup_apply_term_model->logical_delete($first_term['term_id']);
	}

	/**
	 * 期間削除（下期）
	 */
	if (
			$second_term['term_id'] != ''
		&&	(
				$_POST['date_second_from_m'].$_POST['date_second_from_d'] != $second_term['term_from_date']
			||	$_POST['date_second_to_m'].$_POST['date_second_to_d'] != $second_term['term_to_date']
		)
	) {
		$levelup_apply_term_model->logical_delete($second_term['term_id']);
	}

	/**
	 * 期間登録（上期）
	 */
	if (
			$_POST['date_first_from_m'] != '-'
		&&	(
				$_POST['date_first_from_m'].$_POST['date_first_from_d'] != $first_term['term_from_date']
			||	$_POST['date_first_to_m'].$_POST['date_first_to_d'] != $first_term['term_to_date']
		)
	) {

		// 期間ＩＤ取得
		$term_id = $term_id_seq_model->getId();

		// パラメータセット
		$param = array(
				'term_id'			=>	$term_id
			,	'term_div'			=>	1
			,	'term_from_date'	=>	$_POST['date_first_from_m'].$_POST['date_first_from_d']
			,	'term_to_date'		=>	$_POST['date_first_to_m'].$_POST['date_first_to_d']
		);

		// 期間登録
		$levelup_apply_term_model->insert($param);

	}

	/**
	 * 期間登録（下期）
	 */
	if (
			$_POST['date_second_from_m'] != '-'
		&&	(
				$_POST['date_second_from_m'].$_POST['date_second_from_d'] != $second_term['term_from_date']
			||	$_POST['date_second_to_m'].$_POST['date_second_to_d'] != $second_term['term_to_date']
		)
	) {

		// 期間ＩＤ取得
		$term_id = $term_id_seq_model->getId();

		// パラメータセット
		$param = array(
				'term_id'			=>	$term_id
			,	'term_div'			=>	2
			,	'term_from_date'	=>	$_POST['date_second_from_m'].$_POST['date_second_from_d']
			,	'term_to_date'		=>	$_POST['date_second_to_m'].$_POST['date_second_to_d']
		);

		// 期間登録
		$levelup_apply_term_model->insert($param);

	}

	/**
	 * コミット処理
	 */
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

/**
 * 期間取得（上期）
 */
$first_term = $levelup_apply_term_model->getRecordByDiv(1);

/**
 * 期間取得（下期）
 */
$second_term = $levelup_apply_term_model->getRecordByDiv(2);

/**
 * 期間
 */
if ($first_term['term_from_date'] == '') {
	$date_m = '';
	$date_d = '';
} else {
	$date_m = substr($first_term['term_from_date'],0,2);
	$date_d = substr($first_term['term_from_date'],2,2);
}
$option_date_first_from_m = cl_get_select_months($date_m);
$option_date_first_from_d = cl_get_select_days($date_d);

if ($first_term['term_to_date'] == '') {
	$date_m = '';
	$date_d = '';
} else {
	$date_m = substr($first_term['term_to_date'],0,2);
	$date_d = substr($first_term['term_to_date'],2,2);
}
$option_date_first_to_m = cl_get_select_months($date_m);
$option_date_first_to_d = cl_get_select_days($date_d);

if ($second_term['term_from_date'] == '') {
	$date_m = '';
	$date_d = '';
} else {
	$date_m = substr($second_term['term_from_date'],0,2);
	$date_d = substr($second_term['term_from_date'],2,2);
}
$option_date_second_from_m = cl_get_select_months($date_m);
$option_date_second_from_d = cl_get_select_days($date_d);


if ($second_term['term_to_date'] == '') {
	$date_m = '';
	$date_d = '';
} else {
	$date_m = substr($second_term['term_to_date'],0,2);
	$date_d = substr($second_term['term_to_date'],2,2);
}
$option_date_second_to_m = cl_get_select_months($date_m);
$option_date_second_to_d = cl_get_select_days($date_d);

// DB切断
$mdb2->disconnect();

/**
 * テンプレートマッピング
 */
$log->debug("テンプレートマッピング開始",__FILE__,__LINE__);

$smarty->assign( 'cl_title'						, $cl_title						);
$smarty->assign( 'session'						, $session						);
$smarty->assign( 'regist_flg'					, $regist_flg					);
$smarty->assign( 'wkfwctn_mnitm_str'			, $wkfwctn_mnitm_str			);
$smarty->assign( 'js_str'						, $js_str						);
$smarty->assign( 'option_date_first_from_m'		, $option_date_first_from_m		);
$smarty->assign( 'option_date_first_from_d'		, $option_date_first_from_d		);
$smarty->assign( 'option_date_first_to_m'		, $option_date_first_to_m		);
$smarty->assign( 'option_date_first_to_d'		, $option_date_first_to_d		);
$smarty->assign( 'option_date_second_from_m'	, $option_date_second_from_m	);
$smarty->assign( 'option_date_second_from_d'	, $option_date_second_from_d	);
$smarty->assign( 'option_date_second_to_m'		, $option_date_second_to_m		);
$smarty->assign( 'option_date_second_to_d'		, $option_date_second_to_d		);

$log->debug("テンプレートマッピング終了",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート出力開始",__FILE__,__LINE__);

$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");

$log->debug("テンプレート出力終了",__FILE__,__LINE__);

/**
 * アプリケーションメニューHTML取得
 */
function get_wkfw_menuitem($session,$fname)
{
	ob_start();
	show_wkfw_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

$log->info(basename(__FILE__)." END");
$log->shutdown();
