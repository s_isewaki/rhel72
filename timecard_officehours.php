<?php
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_attendance_pattern.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_common_class.php");
require_once("atdbk_menu_common.ini");
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// デフォルトの出勤パターンを「日勤」とする
if ($pattern == "") {
    $pattern = "2";
}

// データベースに接続
$con = connect2db($fname);

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// グループ名を取得
$group_names = get_timecard_group_names($con, $fname);

// デフォルトのグループを設定
if ($group_id == "") {
    $group_id = 1;
}

// 出勤パターン名を取得 表示順対応 20101007
$arr_attendance_pattern_order = get_attendance_pattern_array_order($con, $fname, $group_id);

// 勤務時間帯情報を取得
$sql = "select * from officehours"; //$group_id
$cond = "where tmcd_group_id = $group_id and pattern = '$pattern' order by officehours_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$time_pattern = "/^(\d{2}):(\d{2})$/";
$j = 0;
while ($row = pg_fetch_array($sel)) {
    $tmp_id = $row["officehours_id"];

    for ($i = 1; $i <= 5; $i++) {
        $hour_var_name = "start_hour{$tmp_id}_{$i}";
        $min_var_name = "start_min{$tmp_id}_{$i}";
        if (preg_match($time_pattern, $row["officehours{$i}_start"], $matches) == 1) {
            $$hour_var_name = $matches[1];
            $$min_var_name = $matches[2];
        }

        $hour_var_name = "end_hour{$tmp_id}_{$i}";
        $min_var_name = "end_min{$tmp_id}_{$i}";
        if (preg_match($time_pattern, $row["officehours{$i}_end"], $matches) == 1) {
            $$hour_var_name = $matches[1];
            $$min_var_name = $matches[2];
        }
    }

    $workminutes[$j] = $row["workminutes"];
    $nightduty_minutes[$j] = $row["nightduty_minutes"];
    $j = $j + 1;
}

// 時間勤務フラグの設定
if ($start_hour1_2 == "" && $start_hour2_2 == "" && $start_hour3_2 == "") {
    $time_work_flg = true;
}
else {
    $time_work_flg = false;
}

//勤務日数換算取得
$sql = "select workday_count, previous_day_possible_flag, previous_day_flag, reason, allowance, meeting_start_time, meeting_end_time, assist_time_flag, assist_group_id, assist_start_time, assist_end_time, base_time, over_24hour_flag, auto_clock_in_flag, auto_clock_out_flag, out_time_nosubtract_flag, after_night_duty_flag, duty_connection_flag, empcond_officehours_flag, no_count_flag, previous_day_threshold_hour from atdptn";
$cond = "where group_id = '$group_id' and atdptn_id = '$pattern'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$workday_count = pg_fetch_result($sel, 0, "workday_count");
$previous_day_possible_flag = pg_fetch_result($sel, 0, "previous_day_possible_flag");
$previous_day_flag = pg_fetch_result($sel, 0, "previous_day_flag");
$reason = pg_fetch_result($sel, 0, "reason");
$allowance = pg_fetch_result($sel, 0, "allowance");
$meeting_start_time = pg_fetch_result($sel, 0, "meeting_start_time");
$meeting_end_time = pg_fetch_result($sel, 0, "meeting_end_time");
if ($meeting_start_time == "") {
    list($meeting_start_hour, $meeting_start_min) = array("", "");
}
else {
    list($meeting_start_hour, $meeting_start_min) = array(substr($meeting_start_time, 0, 2), substr($meeting_start_time, 2, 2));
}
if ($meeting_end_time == "") {
    list($meeting_end_hour, $meeting_end_min) = array("", "");
}
else {
    list($meeting_end_hour, $meeting_end_min) = array(substr($meeting_end_time, 0, 2), substr($meeting_end_time, 2, 2));
}

$assist_time_flag = pg_fetch_result($sel, 0, "assist_time_flag");
$assist_group_id = pg_fetch_result($sel, 0, "assist_group_id");
$assist_start_time = pg_fetch_result($sel, 0, "assist_start_time");
$assist_end_time = pg_fetch_result($sel, 0, "assist_end_time");
if ($assist_start_time == "") {
    list($assist_start_hour, $assist_start_min) = array("", "");
}
else {
    list($assist_start_hour, $assist_start_min) = array(substr($assist_start_time, 0, 2), substr($assist_start_time, 2, 2));
}
if ($assist_end_time == "") {
    list($assist_end_hour, $assist_end_min) = array("", "");
}
else {
    list($assist_end_hour, $assist_end_min ) = array(substr($assist_end_time, 0, 2), substr($assist_end_time, 2, 2));
}
//法定外残業の基準時間
$base_time = pg_fetch_result($sel, 0, "base_time");
if ($base_time == "") {
    list($base_hour, $base_min) = array("", "");
}
else {
    list($base_hour, $base_min) = array(substr($base_time, 0, 2), substr($base_time, 2, 2));
}
//$auto_split_flag = pg_fetch_result($sel, 0, "auto_split_flag");
$over_24hour_flag = pg_fetch_result($sel, 0, "over_24hour_flag");
$auto_clock_in_flag = pg_fetch_result($sel, 0, "auto_clock_in_flag");
$auto_clock_out_flag = pg_fetch_result($sel, 0, "auto_clock_out_flag");
$out_time_nosubtract_flag = pg_fetch_result($sel, 0, "out_time_nosubtract_flag");
$after_night_duty_flag = pg_fetch_result($sel, 0, "after_night_duty_flag");
$duty_connection_flag = pg_fetch_result($sel, 0, "duty_connection_flag");
$empcond_officehours_flag = pg_fetch_result($sel, 0, "empcond_officehours_flag");
$no_count_flag = pg_fetch_result($sel, 0, "no_count_flag");
$previous_day_threshold_hour = pg_fetch_result($sel, 0, "previous_day_threshold_hour");
if ($previous_day_threshold_hour == "") {
    $previous_day_threshold_hour = "22";
}

// シフトグループ情報を取得
$group_array = array();
$sql = "select * from duty_shift_group";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$num = pg_numrows($sel);
for ($i = 0; $i < $num; $i++) {
    $group_array[$i]["group_id"] = pg_fetch_result($sel, $i, "group_id");
    $group_array[$i] ["group_name"] = pg_fetch_result($sel, $i, "group_name");
}

//カレンダー情報を取得
$sql = "select day4,day5 from calendarname";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$day4 = pg_fetch_result($sel, 0, "day4");
$day5 = pg_fetch_result($sel, 0, "day5");
//分の単位 20141111
$conf = new Cmx_SystemConfig();
$min_unit = $conf->get("timecard.officehours_min_unit");
if ($min_unit == "") {
    $min_unit = 5;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
        <title>CoMedix 勤務管理 | 勤務時間帯</title>
        <script type="text/javascript" src="js/fontsize.js"></script>
        <script type="text/javascript">
            function setDisabled() {
                var disabled = (document.mainform.time_work_flg.checked);
                var select_elements = document.getElementsByTagName('select');
                for (var i = 0, j = select_elements.length; i < j; i++) {
                    if (select_elements[i].className != 'night' && select_elements[i].className != 'day' && select_elements[i].className != 'js-class-reason') {
                        select_elements[i].disabled = disabled;
                    }
                }
            }
        </script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="setDisabled();">
        <?if ($referer == "2") {?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<?echo($session);?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<?echo($session);?>"><b>勤務管理</b></a></font></td>
                </tr>
            </table>
            <?
        }
        else {
            ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr bgcolor="#f6f9ff">
                    <td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session));?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session));?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<?echo($session);?>"><b>管理画面</b></a></font></td>
                    <td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session));?>"><b>ユーザ画面へ</b></a></font></td>
                </tr>
            </table>
        <? }?>

        <?
        // メニュータブ
        show_work_admin_menuitem($session, $fname, "");
        ?>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
            </tr>
        </table>
        <img src="img/spacer.gif" alt="" width="1" height="5"><br>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td width="100">
                    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="border3">
                        <tr height="22" bgcolor="#f6f9ff">
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ名</font></td>
                        </tr>
                        <tr height="22">
                            <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                    <img src="img/spacer.gif" alt="" width="1" height="2"><br>
                                    <?
                                    foreach ($group_names as $tmp_group_id => $tmp_group_name) {
                                        if ($tmp_group_id == $group_id) {
                                            echo("<b>");
                                        }
                                        else {
                                            echo("<a href=\"$fname?session=$session&group_id=$tmp_group_id\">");
                                        }
                                        echo("$tmp_group_name");
                                        if ($tmp_group_id == $group_id) {
                                            echo("</b>");
                                        }
                                        else {
                                            echo("</a>");
                                        }
                                        echo("<br><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
                                    }
                                    ?>
                                </font></td>
                        </tr>
                    </table>
                </td>
                <td width="5"></td>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse:collapse; border:silver solid 1px">
                        <?
//非表示と休暇を除いた配列 20101007
                        $arr_attendance_pattern = array();
                        $cnt = 1; //表 示位置と対応するため1始まり
                        for ($i = 0; $i < count($arr_attendance_pattern_order); $i++) {
                            if ($arr_attendance_pattern_order[$i]["display_flag"] == "t" &&
                                $arr_attendance_pattern_order[$i] ["atdptn_id"] != "10") {
                                $arr_attendance_pattern[$cnt] = $arr_attendance_pattern_order[$i];
                                $cnt ++;
                            }
                        }
                        ?>
                        <?
                        for ($r = 1, $row_count = ceil(count($arr_attendance_pattern) / 10); $r <= $row_count; $r++) {
                            ?>
                            <tr height="22">
                                <?
                                for ($i = ($r - 1) * 10 + 1; $i <= ($r - 1) * 10 + 10; $i++) {

                                    if ($i > count($arr_attendance_pattern)) {
                                        echo("<td style=\"border:silver solid 1px;\"></td>\n");
                                        continue;
                                    }

                                    $wk_id = $arr_attendance_pattern[$i]["atdptn_id"];
                                    if ($wk_id == $pattern) {
                                        echo( "<td width=\"10%\" align=\"center\" style=\"border:silver solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><b>{$arr_attendance_pattern[$i]["atdptn_nm"]}</b></font></td>\n");
                                    }
                                    else {
                                        echo("<td width=\"10%\" align=\"center\" style=\"border:silver solid 1px;\"><a href=\"timecard_officehours.php?session=$session&group_id=$group_id&pattern={$arr_attendance_pattern[$i]["atdptn_id"]}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$arr_attendance_pattern[$i]["atdptn_nm"]}</font></a></td>\n");
                                    }
                                }
                                ?>
                            </tr>
                        <? }?>
                    </table>
                    <img src="img/spacer.gif" width="1" height="4" alt=""><br>
                    <form name="mainform" action="timecard_officehours_update_exe.php" method="post">
                        <table width="100%" border="0" cellspacing="0" cellpadding="2" class="border3">
                            <tr height="22" bgcolor="#f6f9ff">
                                <td></td>
                                <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通常勤務日/休日</font></td>
                                <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo ($day4);?></font></td>
                                <td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?echo($day5);?></font></td>
                            </tr>
                            <tr height="22">
                                <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所定労働</font></td>
                                <td align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour1_2"><?show_hour_options_0_23($start_hour1_2);?>
                                        </select>：<select name="start_min1_2"><?show_min_options_over_time($start_min1_2, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour1_2"><?show_hour_options_0_23($end_hour1_2);?>
                                        </select>：<select name="end_min1_2"><?show_min_options_over_time($end_min1_2, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour2_2"><?show_hour_options_0_23($start_hour2_2);?>
                                        </select>：<select name="start_min2_2"><?show_min_options_over_time($start_min2_2, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour2_2"><?show_hour_options_0_23($end_hour2_2);?>
                                        </select>：<select name="end_min2_2"><?show_min_options_over_time($end_min2_2, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour3_2"><?show_hour_options_0_23($start_hour3_2);?>
                                        </select>：<select name="start_min3_2"><?show_min_options_over_time($start_min3_2, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour3_2"><?show_hour_options_0_23($end_hour3_2);?>
                                        </select>：<select name="end_min3_2"><?show_min_options_over_time($end_min3_2, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩</font></td>
                                <td bgcolor="#fadede" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour1_4">   <?show_hour_options_0_23($start_hour1_4);?>
                                        </select>：<select name="start_min1_4"><?show_min_options_over_time($start_min1_4, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour1_4"><?show_hour_options_0_23($end_hour1_4);?>
                                        </select>：<select name="end_min1_4"><?show_min_options_over_time($end_min1_4, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td bgcolor="#fadede" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour2_4">   <?show_hour_options_0_23($start_hour2_4);?>
                                        </select>：<select name="start_min2_4"><?show_min_options_over_time($start_min2_4, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour2_4"><?show_hour_options_0_23($end_hour2_4);?>
                                        </select>：<select name="end_min2_4"><?show_min_options_over_time($end_min2_4, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td bgcolor="#fadede" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour3_4"><?show_hour_options_0_23($start_hour3_4);?>
                                        </select>：<select name="start_min3_4">   <?show_min_options_over_time($start_min3_4, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour3_4"><?show_hour_options_0_23($end_hour3_4);?>
                                        </select>：<select name="end_min3_4">   <?show_min_options_over_time($end_min3_4, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">深夜帯</font></td>
                                <td bgcolor="#defafa" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour1_5" class="night">   <?show_hour_options_0_23($start_hour1_5);?>
                                        </select>：<select name="start_min1_5" class="night"> <?show_min_options_over_time($start_min1_5, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour1_5" class="night"><?show_hour_options_0_23($end_hour1_5);?>
                                        </select>：<select name="end_min1_5" class="night"><?show_min_options_over_time($end_min1_5, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td bgcolor="#defafa" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour2_5" class="night">   <?show_hour_options_0_23($start_hour2_5);?>
                                        </select>：<select name="start_min2_5" class="night">   <?show_min_options_over_time($start_min2_5, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour2_5" class="night"><?show_hour_options_0_23($end_hour2_5);?>
                                        </select>：<select name="end_min2_5" class="night"><?show_min_options_over_time($end_min2_5, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                                <td bgcolor="#defafa" align="center">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        <select name="start_hour3_5" class="night"><?show_hour_options_0_23($start_hour3_5);?>
                                        </select>：<select name="start_min3_5" class="night"><?show_min_options_over_time($start_min3_5, $min_unit, true, false);?>
                                        </select>〜<select name="end_hour3_5" class="night"><?show_hour_options_0_23($end_hour3_5);?>
                                        </select>：<select name="end_min3_5" class="night"><?show_min_options_over_time($end_min3_5, $min_unit, true, false);?></select>
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td bgcolor="#f6f9ff">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">稼動時間</font>
                                </td>
                                <td bgcolor="#C8FFD5" align="center">
                                    <input type="text" value="<?echo($workminutes[0]);?>" name="workminutes1" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                                <td bgcolor="#C8FFD5" align="center">
                                    <input type="text" value="<?echo($workminutes[1]);?>" name="workminutes2" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                                <td bgcolor="#C8FFD5" align="center">
                                    <input type="text" value="<?echo($workminutes[2]);?>" name="workminutes3" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td bgcolor="#f6f9ff">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">夜勤時間</font>
                                </td>
                                <td bgcolor="#FFFACD" align="center">
                                    <input type="text" value="<?echo($nightduty_minutes[0]);?>" name="nightduty_minutes1" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                                <td bgcolor="#FFFACD" align="center">
                                    <input type="text" value="<?echo($nightduty_minutes[1]);?>" name="nightduty_minutes2" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                                <td bgcolor="#FFFACD" align="center">
                                    <input type="text" value="<?echo($nightduty_minutes[2]);?>" name="nightduty_minutes3" maxlength="4" size="4" style="ime-mode: disabled;"/>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">分</font>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr height="22">
                                <td align="left">
                                    <input type="submit" value="登録">
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="time_work_flg">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="time_work_flg" id="time_work_flg" value="t"<?
                                            if ($time_work_flg) {
                                                echo(" checked");
                                            }
                                            ?> onclick="setDisabled();">所定労働・休憩を指定しない（勤務開始予定・終了予定を時間給者が個別に登録する）
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="empcond_officehours_flag">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="empcond_officehours_flag" id="empcond_officehours_flag" value="1"<?
                                            if ($empcond_officehours_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?> >所定労働・休憩は個人の勤務条件を優先する
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="no_count_flag">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="no_count_flag" id="no_count_flag" value="1"<?
                                            if ($no_count_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?> >勤務時間を計算しない
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="out_time_nosubtract_flag">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="out_time_nosubtract_flag" id="out_time_nosubtract_flag" value="1"<?
                                            if ($out_time_nosubtract_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>外出時間を勤務時間から差し引かない
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日数換算
                                        <select name="workday_count" class="day">
                                            <option value="0"<?
                                            if ($workday_count == "0") {
                                                echo(" selected");
                                            }
                                            ?>>0
                                            <option value="0.5"<?
                                            if ($workday_count == "0.5") {
                                                echo(" selected");
                                            }
                                            ?>>0.5
                                            <option value="1"<?
                                            if ($workday_count == "1" || $workday_count == null || $workday_count == "") {
                                                echo(" selected");
                                            }
                                            ?>>1
                                            <option value="1.5"<?
                                            if ($workday_count == "1.5") {
                                                echo(" selected");
                                            }
                                            ?>>1.5
                                            <option value="2"<?
                                            if ($workday_count == "2") {
                                                echo(" selected");
                                            }
                                            ?>>2
                                        </select>日
                                        &nbsp;
                                        <label for="after_night_duty_flag">
                                            <input type="checkbox" name="after_night_duty_flag" id="after_night_duty_flag" value="1" <?
                                            if ($after_night_duty_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>「明け」の勤務パターンである
                                        </label>
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">法定外残業の計算
                                        <select name="base_hour"><?show_hour_options_0_47($base_hour);?></select>時間
                                        <select name="base_min"><?show_min_options_00_59($base_min);?></select>分（未登録の場合は、8時間）
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="zenjitsu">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="previous_day_possible_flag" id="zenjitsu" value="1"  <?
                                            if ($previous_day_possible_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?> onclick="setDisabled();">勤務開始時刻が前日になる場合がある
                                        </font>
                                    </label>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                        　前日の<select name="previous_day_threshold_hour">
                                            <?show_hour_options_0_23($previous_day_threshold_hour, false);?>
                                        </select>
                                        時以降の出勤のみ
                                    </font>
                                </td>
                            </tr>
                            <tr height= "22">
                                <td>
                                    <label for="zenjitsu2">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="previous_day_flag" id="zenjitsu2" value="1"<?
                                            if ($previous_day_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>勤務開始時刻は前日である
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="over_24hour">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="over_24hour_flag" id="over_24hour" value="1"<?
                                            if ($over_24hour_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>終了時刻は、開始時刻から24時間を超えている
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font>
                                    <?
                                    $atdbk_common_class->set_reason_option("reason", $reason, true);
                                    ?>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当金額（円）</font>
                                    <input type="text" name="allowance" value="<?echo($allowance);?>" size="11" maxlength="9" style="ime-mode:inactive;">
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・病棟外勤務</font>
                                    <select name="meeting_start_hour"><?show_hour_options_0_23($meeting_start_hour);?></select>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時
                                        <select name="meeting_start_min">
                                            <?show_min_options_00_59($meeting_start_min);?>
                                        </select>分 〜
                                        <select name="meeting_end_hour">
                                            <?show_hour_options_0_23($meeting_end_hour);?>
                                        </select>時
                                        <select name="meeting_end_min">
                                            <?show_min_options_00_59($meeting_end_min);?>
                                        </select>分
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <label for="assist_time">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="assist_time_flag" id="assist_time" value="1"<?
                                            if ($assist_time_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>応援勤務時間を含む
                                        </font>
                                    </label>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">応援先シフトグループ</font>
                                    <?
                                    echo("<select name=\"assist_group_id\">");
                                    echo("	<option value=\"\">\n");
                                    for ($k = 0; $k < count($group_array); $k++) {
                                        $wk_id = $group_array[$k]["group_id"];
                                        $wk_name = $group_array[$k]["group_name"];
                                        echo("<option  value=\"$wk_id\"");
                                        if ($assist_group_id == $wk_id) {
                                            echo(" selected");
                                        }
                                        echo(">$wk_name \n");
                                    }
                                    echo("</select>\n");
                                    ?>
                                    &nbsp;
                                    <select name="assist_start_hour">
                                        <?show_hour_options_0_23($assist_start_hour);?>
                                    </select>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時
                                        <select name="assist_start_min">
                                            <?show_min_options_00_59($assist_start_min);?>
                                        </select>分 〜
                                        <select name="assist_end_hour">
                                            <?show_hour_options_0_23($assist_end_hour);?>
                                        </select>時<select name="assist_end_min">
                                            <?show_min_options_00_59($assist_end_min);?>
                                        </select>分
                                    </font>
                                </td>
                            </tr>
                            <tr height="22">
                                <td>
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">自動打刻をする<br>
                                        <label for="clock_in">
                                            <input type="checkbox" name="auto_clock_in_flag" id="clock_in" value="1"<?
                                            if ($auto_clock_in_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>出勤
                                        </label>&nbsp;
                                        <label for="clock_out">
                                            <input type="checkbox" name="auto_clock_out_flag" id="clock_out" value="1"<?
                                            if ($auto_clock_out_flag == "1") {
                                                echo(" checked");
                                            }
                                            ?>>退勤
                                        </label>
                                    </font>
                                </td>
                            </tr>

                            <tr height="22">
                                <td>
                                    <label for="duty_connection">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <input type="checkbox" name="duty_connection_flag" id="duty_connection" value="1"<?
                                                   if ($duty_connection_flag == "1") {
                                                       echo(" checked");
                                                   }
                                                   ?>>当直と連動する(勤務表のみ)
                                        </font>
                                    </label>
                                </td>
                            </tr>

                            <tr height="22">
                                <td align="left">
                                    <input type="submit" value="登録">
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="session" value="<?echo($session);?>"/>
                        <input type="hidden" name="group_id" value="<?echo($group_id);?>"/>
                        <input type="hidden" name="pattern" value="<?echo($pattern);?>"/>
                    </form>
                </td>
            </tr>
        </table>
    </body>
    <?pg_close($con);?>
</html>

<?

// show_select_values.iniのshow_hour_options_0_23を流用
function show_hour_options_0_47($hour, $blank = true)
{
    if ($blank) {
        echo("<option value=\"--\">\n");
    }

    for ($i = 0; $i <= 47; $i++) {
        $val = sprintf("%02d", $i);
        echo("<option value=\"$val\"");
        if ($val == $hour) {
            echo(" selected");
        }
        echo(">$i\n");
    }
}
