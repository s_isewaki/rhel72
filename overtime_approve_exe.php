<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("atdbk_workflow_common_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

$obj = new atdbk_workflow_common_class($con, $fname);
$obj->approve_application($apply_id, $apv_order, $apv_sub_order, $approve, $apv_comment, $next_notice_div, $session, "OVTM");


// 申請情報を取得
$sql = "select apply_status, emp_id, target_date from ovtmapply";
$cond = "where apply_id = $apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$status = pg_fetch_result($sel, 0, "apply_status");
$apply_emp_id = pg_fetch_result($sel, 0, "emp_id");
$target_date = pg_fetch_result($sel, 0, "target_date");
//否認の場合
if ($status == "2") {

	// 勤務実績情報を取得
	$arr_result = get_atdbkrslt($con, $apply_emp_id, $target_date, $fname);
	$end_time = $arr_result["end_time"];
	$next_day_flag = $arr_result["next_day_flag"];

	// 勤務実績の退勤時刻を終業時刻で上書き
	$office_start_time = $arr_result["office_start_time"];
	$office_end_time   = $arr_result["office_end_time"];
	$office_next_day_flag = 0;
	//開始時刻・終業時刻から終業時刻が翌日か判定を行う
	if ($office_start_time > $office_end_time){
		$office_next_day_flag = 1;
	}
	$sql = "update atdbkrslt set";
	$set = array("end_time", "next_day_flag", "over_start_time", "over_end_time", "over_start_next_day_flag", "over_end_next_day_flag", "over_start_time2", "over_end_time2", "over_start_next_day_flag2", "over_end_next_day_flag2");
	$setvalue = array($office_end_time, $office_next_day_flag, "", "", 0, 0, "", "", 0, 0);
    //追加
    for ($i=3; $i<=5; $i++) {
        $s_t = "over_start_time".$i;
        $e_t = "over_end_time".$i;
        $s_f = "over_start_next_day_flag".$i;
        $e_f = "over_end_next_day_flag".$i;
        array_push($set, $s_t);
        array_push($set, $e_t);
        array_push($set, $s_f);
        array_push($set, $e_f);
        array_push($setvalue, "");
        array_push($setvalue, "");
        array_push($setvalue, 0);
        array_push($setvalue, 0);
    }    
    $cond = "where emp_id = '$apply_emp_id' and date = '$target_date'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}



// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);


// 親画面をリフレッシュし、自画面を閉じる
if($pnt_url != "") {
	echo("<script type=\"text/javascript\">opener.location.href = '$pnt_url'; self.close();</script>");
} else {
	echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");	
	echo("<script language=\"javascript\">window.close();</script>\n");
}
?>
