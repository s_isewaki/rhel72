<!-- ************************************************************************ -->
<!-- 勤務シフト作成　勤務シフト作成画面　自動シフト作成ＣＬＡＳＳ -->
<!-- ************************************************************************ -->
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_check_common_class.php");

class duty_shift_auto_common_class
{
	
	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;		// 勤務シフト作成共通クラス
	var $obj_check;	// 勤務シフト作成チェック共通クラス
	
	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function duty_shift_auto_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
		$this->obj_check = new duty_shift_check_common_class($con, $fname);
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	// 関数一覧
	//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 自動シフト作成
	// @param	$group_id				勤務シフトグループＩＤ
	// @param	$pattern_id				出勤グループＩＤ
	// @param	$plan_array				勤務シフト情報
	// @param	$old_plan_array			勤務シフト情報（自動シフト作成用過去データ）
	// @param	$rslt_array				勤務実績情報
	// @param	$need_array				必要人数(カレンダー)
	// @param	$staff_array			スタッフ情報（指定シフトグループ(病棟)の）
	// @param	$staff_employmen		スタッフ勤務条件情報（指定シフトグループ(病棟)の）
	// @param	$data_auto				自動シフト条件情報
	// @param	$NG_pattern_array		禁止組み合わせ情報
	// @param	$upper_limit_array		連続勤務シフト上限情報
	// @param	$interval_array			勤務シフト間隔情報
	// @param	$week_array				曜日情報（当月）
	// @param	$calendar_array			カレンダー情報（当月）
	// @param	$old_calendar_array_2	カレンダー情報（過去）
	// @param	$rslt_calendar_array	カレンダー情報（実績）
	// @param	$data_atdptn_all		出勤パターン情報
	// @param	$day_cnt				日数
	// @param	$duty_upper_limit_day	連続勤務日数上限
	// @param	$auto_start_day			自動シフト作成開始日
	// @param	$auto_end_day			自動シフト作成終了日
	// @param	$auto_ptn_id			自動シフト作成対象とするシフト 0:全て その他はマスタから取得したID
	// @param	$auto_btn_flg			自動シフト作成ボタンフラグ 1:自動作成 2:一括登録 3:クリア
	// @param	$arr_group_pattern_reason 事由情報
	//
	// @return	$ret	自動作成勤務シフト情報
	/*************************************************************************/
	function set_auto(
		$group_id,
		$pattern_id,
		$plan_array,
		$old_plan_array,
		$rslt_array,
		$need_array,
		$staff_array,
		$staff_employmen,
		$data_auto,
		$NG_pattern_array,
		$upper_limit_array,
		$interval_array,
		$week_array,
		$calendar_array,
		$old_calendar_array_2,
		$rslt_calendar_array,
		$data_atdptn_all,
		$day_cnt,
		$duty_upper_limit_day,
		$auto_start_day,
		$auto_end_day,
		$auto_ptn_id,
		$auto_btn_flg,
		$arr_group_pattern_reason) {
		
		///*****************************************************************************
		// 初期処理
		///*****************************************************************************
		///-----------------------------------------------------------------------------
		//初期設定
		///-----------------------------------------------------------------------------
		$data = array();
		for ($i=0; $i<count($plan_array); $i++) {
			//書き換え不可データを設定
			$data[$i]["staff_id"] = $plan_array[$i]["staff_id"];
			$data[$i]["job_id"] = $plan_array[$i]["job_id"];
			for ($k=1; $k<=$day_cnt; $k++) {
				$data[$i]["individual_flg_$k"] = $plan_array[$i]["individual_flg_$k"];
				$data[$i]["comment_$k"] = $plan_array[$i]["comment_$k"];
				// 応援グループID 20090625
				$data[$i]["assist_group_$k"] = $plan_array[$i]["assist_group_$k"];
			}
			//自動シフト作成開始日まではそのままデータ保存
			for ($k=1; $k<$auto_start_day; $k++) {
				$data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$k"];
				$data[$i]["atdptn_ptn_id_$k"] = $plan_array[$i]["atdptn_ptn_id_$k"];
				$data[$i]["reason_$k"] = $plan_array[$i]["reason_$k"];
				$data[$i]["reason_2_$k"] = $plan_array[$i]["reason_2_$k"];
			}
		}
		
		// 一括登録、クリア
		if (($auto_btn_flg == "2" && $auto_ptn_id != "0") ||
				$auto_btn_flg == "3") {
			for ($i=0; $i<count($plan_array); $i++) {
				for ($k=$auto_start_day; $k<=$day_cnt; $k++) {
					// 終了日以前、対象のシフトはクリア
					$wk_ptn_id = substr($plan_array[$i]["atdptn_ptn_id_$k"], 0, 2);
					// 一括登録
					if ($auto_btn_flg == "2" &&
							$k <= $auto_end_day &&
							$plan_array[$i]["atdptn_ptn_id_$k"] == "") {
						$data[$i]["pattern_id_$k"] = $pattern_id;
						$data[$i]["atdptn_ptn_id_$k"] = $auto_ptn_id;
						$data[$i]["reason_$k"] = ($auto_ptn_id == 10) ? "24" : "";
						$data[$i]["reason_2_$k"] = "";
					}
					// クリア
					elseif ($auto_btn_flg == "3" &&
							$k <= $auto_end_day &&
							$plan_array[$i]["pattern_id_$k"] == $pattern_id &&
							$plan_array[$i]["assist_group_$k"] == $group_id &&
							($auto_ptn_id == "0" ||
								((int)$auto_ptn_id != 10 && (int)$auto_ptn_id == (int)$wk_ptn_id) ||
								((int)$auto_ptn_id == 10 && (int)$auto_ptn_id == (int)$wk_ptn_id && $plan_array[$i]["reason_$k"] == "24"))) {
						$data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$k"];
						$data[$i]["atdptn_ptn_id_$k"] = "";
						$data[$i]["reason_$k"] = "";
						$data[$i]["reason_2_$k"] = "";
					} else {
						// 上記以外は元データを設定
						$data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$k"];
						$data[$i]["atdptn_ptn_id_$k"] = $plan_array[$i]["atdptn_ptn_id_$k"];
						$data[$i]["reason_$k"] = $plan_array[$i]["reason_$k"];
						$data[$i]["reason_2_$k"] = $plan_array[$i]["reason_2_$k"];
					}
				}
			}
			return $data;
		}
		
		
		//組み合わせ指定確認用配列
		$arr_seq_pattern = $this->obj->get_duty_shift_group_seq_pattern_array($group_id);
		
		//事由情報の組合せ配列
		$arr_atdptn_reason = array();
		foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
			foreach ($arr_pattern_reason as $wk_atdptn_id => $reason) {
				if ($reason != "") {
					$arr_atdptn_reason["{$wk_tmcd_group_id}_{$wk_atdptn_id}"] = "$reason";
				}
			}
		}
		
		//自動作成時のデフォルトパターン情報
		$arr_default_ptn_info = $this->obj->get_auto_default_ptn_info($pattern_id);
		
		// 判定等の処理のためreasonを0から""に変更
		for ($i=0; $i<count($arr_seq_pattern); $i++) {
			if ($arr_seq_pattern[$i]["today_reason"] == "0") {
				$arr_seq_pattern[$i]["today_reason"] = "";
			}
			if ($arr_seq_pattern[$i]["nextday_reason"] == "0") {
				$arr_seq_pattern[$i]["nextday_reason"] = "";
			}
		}
		
		//組み合わせ指定設定フラグ。（組み合わせ指定したシフトを必要人数より多い場合にクリアするのを防ぐために使用）
		$arr_chk_seq_set = array();
		
		//換算日数の配列 [勤務パターングループ][勤務パターン]
		$arr_workday_count = array();
		for ($m=0; $m<count($data_atdptn_all); $m++) {
			$group_id = $data_atdptn_all[$m]["group_id"];
			$atdptn_ptn_id = $data_atdptn_all[$m]["id"];
			$arr_workday_count[$group_id][$atdptn_ptn_id] = $data_atdptn_all[$m]["workday_count"];
		}
		///-----------------------------------------------------------------------------
		//判定用に過去勤務シフト情報を再設定
		///-----------------------------------------------------------------------------
		$data_old = $old_plan_array;					//勤務シフト情報（過去）
		$old_calendar_array = $old_calendar_array_2;	//カレンダー情報（過去）
		
		///-----------------------------------------------------------------------------
		//自動シフト開始日以前の過去勤務シフト情報を設定
		///-----------------------------------------------------------------------------
		for ($k=1; $k<$auto_start_day; $k++) {
			$old_calendar_cnt = count($old_calendar_array);
			for ($i=0; $i<count($data); $i++) {
				///-----------------------------------------------------------------------------
				// 対象勤務シフト（過去）を取得
				///-----------------------------------------------------------------------------
				$old_idx = -1;
				for ($m=0; $m<count($data_old); $m++) {
					if ($data[$i]["staff_id"] == $data_old[$m]["staff_id"]) {
						$old_idx = $m;
						break;
					}
				}
				///-----------------------------------------------------------------------------
				// 次日判定用に日／データ追加
				///-----------------------------------------------------------------------------
				$old_calendar_array[$old_calendar_cnt]["date"] = $calendar_array[$k - 1]["date"];
				$old_calendar_array[$old_calendar_cnt]["type"] = $calendar_array[$k - 1]["type"];
				$m = $old_calendar_cnt + 1;
				$data_old[$old_idx]["pattern_id_$m"] = $data[$i]["pattern_id_$k"];
				$data_old[$old_idx]["atdptn_ptn_id_$m"] = $data[$i]["atdptn_ptn_id_$k"];
				$data_old[$old_idx]["reason_$m"] = $data[$i]["reason_$k"];
				$data_old[$old_idx]["reason_2_$m"] = $data[$i]["reason_2_$k"];
			}
		}
		//組み合わせ判定に使用するため、先頭に前月末のデータを設定
		$old_calendar_cnt = count($old_calendar_array);
		for ($i=0; $i<count($data); $i++) {
			$old_idx = -1;
			for ($m=0; $m<count($data_old); $m++) {
				if ($data[$i]["staff_id"] == $data_old[$m]["staff_id"]) {
					$old_idx = $m;
					break;
				}
			}
			if ($old_idx >= 0) {
				$data[$i]["pattern_id_0"] = $data_old[$old_idx]["pattern_id_$old_calendar_cnt"];
				$data[$i]["atdptn_ptn_id_0"] = $data_old[$old_idx]["atdptn_ptn_id_$old_calendar_cnt"];
				$data[$i]["reason_0"] = $data_old[$old_idx]["reason_$old_calendar_cnt"];
			}
		}
		
		///*****************************************************************************
		// 当月シフトの決定
		///*****************************************************************************
		for ($k=$auto_start_day; $k<=$day_cnt; $k++) {
			///*****************************************************************************
			// （１）個人ごとに過去勤務パターンから当日シフトの決定
			///*****************************************************************************
			$old_calendar_cnt = count($old_calendar_array);
			
			// 必要人数充足確認用配列
			$arr_need_cnt = array();
			$arr_chk_need_cnt = array(); //設定初期状態、未設定チェック用
			// 職種／シフト毎の人数
			foreach ( $need_array as $need ) {
				$wk_need_id = $this->get_need_id($need_array, $need["atdptn_ptn_id"], $need["job_id"], $need["team_id"], $need["sex"]);
				$arr_need_cnt[$wk_need_id] += (int)$need["need_cnt"][$k];
				$arr_chk_need_cnt[$wk_need_id] += (int)$need["need_cnt"][$k];
			}
			// 設定済みを引く
			for ($m=0; $m<count($data); $m++) {
				// 勤務パターングループ一致、シフト設定済み
				if ($plan_array[$m]["pattern_id_$k"] == $pattern_id &&
						$plan_array[$m]["atdptn_ptn_id_$k"] != "") {
					$wk_need_id = $this->get_need_id($need_array, $plan_array[$m]["atdptn_ptn_id_$k"], $plan_array[$m]["job_id"], $plan_array[$m]["team_id"], $plan_array[$m]["sex"]);
					$arr_need_cnt[$wk_need_id]--;
				}
			}
			///-----------------------------------------------------------------------------
			// 指定日にデータ設定
			///-----------------------------------------------------------------------------
			for ($i=0; $i<count($data); $i++) {
				//職種、チーム、性別
				$wk_job_id = $plan_array[$i]["job_id"];
				$wk_team_id = $plan_array[$i]["team_id"];
				$wk_sex = $plan_array[$i]["sex"];
				
				///-----------------------------------------------------------------------------
				// 対象勤務シフト（過去）を取得
				///-----------------------------------------------------------------------------
				$old_idx = -1;
				for ($m=0; $m<count($data_old); $m++) {
					if ($data[$i]["staff_id"] == $data_old[$m]["staff_id"]) {
						$old_idx = $m;
						break;
					}
				}
				
				///-----------------------------------------------------------------------------
				// 当日のシフトを取得
				///-----------------------------------------------------------------------------
				$data[$i]["pattern_id_$k"] = $pattern_id;
				$data[$i]["atdptn_ptn_id_$k"] = "";
				$data[$i]["reason_$k"] = "";
				$data[$i]["reason_2_$k"] = "";
				
				if ($data[$i]["individual_flg_$k"] != "" ||
						($plan_array[$i]["pattern_id_$k"] != "" &&
							$plan_array[$i]["pattern_id_$k"] != $pattern_id) || 
						$plan_array[$i]["atdptn_ptn_id_$k"] != "" ||
						$k > $auto_end_day) {
					///-----------------------------------------------------------------------------
					//「勤務希望取込」で設定された日の場合
					// 出勤パターングループIDが違う場合を条件追加(重複職員の別グループのシフトは対象外とする) 2008/5/23
					// 設定済みのパターンは変えない 2008/11/18 
					// 終了日を追加 2008/11/18 
					///-----------------------------------------------------------------------------
					$data[$i]["pattern_id_$k"] = $plan_array[$i]["pattern_id_$k"];
					$data[$i]["atdptn_ptn_id_$k"] = $plan_array[$i]["atdptn_ptn_id_$k"];
					$data[$i]["reason_$k"] = $plan_array[$i]["reason_$k"];
					$data[$i]["reason_2_$k"] = $plan_array[$i]["reason_2_$k"];
					
					$prev_k = $k - 1;
					//既存データが禁止組み合わせの場合
					$wk_kinsi_flg = $this->obj_check->check_ng_pattern($data[$i]["atdptn_ptn_id_$prev_k"], $data[$i]["reason_$prev_k"], $data[$i]["atdptn_ptn_id_$k"], $data[$i]["reason_$k"], $NG_pattern_array);
					if ($wk_kinsi_flg == "1") {
						$data[$i]["atdptn_ptn_id_$k"] = "";
						$data[$i]["reason_$k"] = "";
						$data[$i]["reason_2_$k"] = "";
					}
					//既存データが勤務シフト間隔以内の場合
					$data_old[$old_idx]["ward_pattern_id"] = $pattern_id;
					$wk_interval_flg = $this->obj_check->check_interval($data_old[$old_idx], $old_calendar_cnt, $data[$i]["atdptn_ptn_id_$k"], $data[$i]["reason_$k"], $interval_array);
					if ($wk_interval_flg == "1") {
						$data[$i]["atdptn_ptn_id_$k"] = "";
						$data[$i]["reason_$k"] = "";
						$data[$i]["reason_2_$k"] = "";
					}
					//既存データが組み合わせ指定になっている場合
					if ($k >= 1 ) {
						for ($s_idx=0; $s_idx<count($arr_seq_pattern); $s_idx++) {
							
							if ($arr_seq_pattern[$s_idx]["today_atdptn_ptn_id"] == $data[$i]["atdptn_ptn_id_$prev_k"] &&
									$arr_seq_pattern[$s_idx]["today_reason"] == $data[$i]["reason_$prev_k"] &&
									$arr_seq_pattern[$s_idx]["nextday_atdptn_ptn_id"] == $data[$i]["atdptn_ptn_id_$k"] &&
									$arr_seq_pattern[$s_idx]["nextday_reason"] == $data[$i]["reason_$k"]) {
								
								// 組み合わせ指定設定フラグ
								$arr_chk_seq_set[$i][$k] = "1";
								break;
							}
						}
					}
				} else {
					///-----------------------------------------------------------------------------
					//上記以外の場合
					///-----------------------------------------------------------------------------
					$prev_k = $k - 1;
					//禁止確認用
					if ($k == 1) {
						$wk_cnt = count($old_calendar_array_2);
						$before_atdptn_ptn_id = $data_old[$old_idx]["atdptn_ptn_id_$wk_cnt"];
						$before_reason = $data_old[$old_idx]["reason_$wk_cnt"];
					} else {
						$before_atdptn_ptn_id = $data[$i]["atdptn_ptn_id_$prev_k"];
						$before_reason = $data[$i]["reason_$prev_k"];
					}
					if ($k < $day_cnt) {
						$wk_cnt = $k + 1;
						$next_atdptn_ptn_id = $plan_array[$i]["atdptn_ptn_id_$wk_cnt"];
						$next_reason = $plan_array[$i]["reason_$wk_cnt"];
					} else {
						$next_atdptn_ptn_id = "";
						$next_reason = "";
					}
					
					//組み合わせ指定確認
					$wk_seq_flg = "";
					if ($k >= 1 && $data[$i]["pattern_id_$prev_k"] == $pattern_id) {
						for ($s_idx=0; $s_idx<count($arr_seq_pattern); $s_idx++) {
							
							// 組み合わせ指定当日と前日シフトが同じ場合
							if ($arr_seq_pattern[$s_idx]["today_atdptn_ptn_id"] == $data[$i]["atdptn_ptn_id_$prev_k"] &&
									$arr_seq_pattern[$s_idx]["today_reason"] == $data[$i]["reason_$prev_k"]) {
								
								// 組み合わせ指定あり
								$wk_seq_flg = "1";
								
								$data[$i]["pattern_id_$k"] = $pattern_id;
								$data[$i]["atdptn_ptn_id_$k"] = $arr_seq_pattern[$s_idx]["nextday_atdptn_ptn_id"];
								$data[$i]["reason_$k"] = $arr_seq_pattern[$s_idx]["nextday_reason"];
								$data[$i]["reason_2_$k"] = "";
								// 事由組合せ
								$wk_atdptn_id = (int)$data[$i]["atdptn_ptn_id_$k"];
								$wk_id = $pattern_id."_".$wk_atdptn_id;
								
								if ($arr_atdptn_reason[$wk_id] != "") {
									$data[$i]["reason_2_$k"] = $arr_atdptn_reason[$wk_id];
								}
								
								// 必要人数を更新
								$wk_need_id2 = $this->get_need_id($need_array, $data[$m]["atdptn_ptn_id_$k"], $plan_array[$i]["job_id"], $plan_array[$i]["team_id"], $plan_array[$i]["sex"]);
								$arr_need_cnt[$wk_need_id2]--;
								// 組み合わせ指定設定フラグ
								$arr_chk_seq_set[$i][$k] = "1";
								break;
							}
						}
					}
					
					//過去データあり、組み合わせ指定なし
					if ($old_idx >= 0 && $wk_seq_flg == "") {
						
						$next_pattern = $this->set_auto_day($data_old[$old_idx], $data_auto, $k, $old_calendar_cnt);
						
						//禁止確認 20090521
						$data_old[$old_idx]["ward_pattern_id"] = $pattern_id;
						$next_pattern = $this->set_not_NG_pattern($next_pattern, $before_atdptn_ptn_id, $before_reason, $next_atdptn_ptn_id, $next_reason, $NG_pattern_array, $data_old[$old_idx], $old_calendar_cnt, $interval_array);
						//必要人数充足確認
						$next_pattern = $this->set_need_pattern($next_pattern, $arr_need_cnt, $wk_job_id, $arr_chk_need_cnt, $need_array, $wk_team_id, $wk_sex);
						
						//対象とする勤務シフト確認
						$is_target_ok = false;
						if ($auto_ptn_id == "0" ||
								($auto_ptn_id != "0" && 
									$next_pattern[0]["atdptn_ptn_id"] != "" &&
									$next_pattern[0]["atdptn_ptn_id"] == $auto_ptn_id)) {
							$is_target_ok = true;
						}
						if ($next_pattern[0]["atdptn_ptn_id"] != "" && $is_target_ok) {
							$data[$i]["pattern_id_$k"] = $next_pattern[0]["pattern_id"];
							$data[$i]["atdptn_ptn_id_$k"] = $next_pattern[0]["atdptn_ptn_id"];
							$data[$i]["reason_$k"] = $next_pattern[0]["reason"];
							$data[$i]["reason_2_$k"] = $next_pattern[0]["reason_2"];
							//必要人数を更新
							$wk_need_id2 = $this->get_need_id($need_array, $next_pattern[0]["atdptn_ptn_id"], $wk_job_id, $wk_team_id, $wk_sex);
							$arr_need_cnt[$wk_need_id2]--;
						} else {
							///-----------------------------------------------------------------------------
							// シフトが未設定の場合、
							// その日と同じ曜日（祝日の場合は祝日）でより一番多いパターンのシフトを設定する。
							///-----------------------------------------------------------------------------
							$next_pattern = $this->set_auto_day_2(
									$data_old[$old_idx],
									$k,
									$calendar_array,
									$week_array,
									$old_calendar_array,
									$old_calendar_cnt);
							//禁止確認 20090521
							$data_old[$old_idx]["ward_pattern_id"] = $pattern_id;
							$next_pattern = $this->set_not_NG_pattern($next_pattern, $before_atdptn_ptn_id, $before_reason, $next_atdptn_ptn_id, $next_reason, $NG_pattern_array, $data_old[$old_idx], $old_calendar_cnt, $interval_array);
							//必要人数充足確認
							$next_pattern = $this->set_need_pattern($next_pattern, $arr_need_cnt, $wk_job_id, $arr_chk_need_cnt, $need_array);
							//対象とする勤務シフト確認
							$is_target_ok = false;
							if ($auto_ptn_id == "0" ||
									($auto_ptn_id != "0" && 
										$next_pattern[0]["atdptn_ptn_id"] != "" &&
										$next_pattern[0]["atdptn_ptn_id"] == $auto_ptn_id)) {
								$is_target_ok = true;
							}
							if ($next_pattern[0]["atdptn_ptn_id"] != "" && $is_target_ok) {
								$data[$i]["pattern_id_$k"] = $next_pattern[0]["pattern_id"];
								$data[$i]["atdptn_ptn_id_$k"] = $next_pattern[0]["atdptn_ptn_id"];
								$data[$i]["reason_$k"] = $next_pattern[0]["reason"];
								$data[$i]["reason_2_$k"] = $next_pattern[0]["reason_2"];
								//職種複数対応
								$wk_need_id2 = $this->get_need_id($need_array, $next_pattern[0]["atdptn_ptn_id"], $wk_job_id, $wk_team_id, $wk_sex);
								$arr_need_cnt[$wk_need_id2]--;
							}
						}
					}
					// 休暇のデフォルトを公休とする(以前の事由無し34を24へ)2009/01/07
					if ($data[$i]["atdptn_ptn_id_$k"] == "10" &&
							($data[$i]["reason_$k"] == "" || $data[$i]["reason_$k"] == "34")) {
						$data[$i]["reason_$k"] = "24";
					}
				}
				//連続5日勤務後の翌日を公休とする 20090812
				//希望取込、組み合わせ指定、設定済の場合は変えない
				$wk_group_id = $data[$i]["pattern_id_$k"];
				$wk_atdptn_ptn_id = $data[$i]["atdptn_ptn_id_$k"];
				
				if ($data[$i]["individual_flg_$k"] == "" &&
						$arr_chk_seq_set[$i][$k] == "" &&
						$plan_array[$i]["atdptn_ptn_id_$k"] == "" &&
						$data[$i]["atdptn_ptn_id_$k"] != "10" &&
						($arr_workday_count[$wk_group_id][$wk_atdptn_ptn_id] != 0 ||
							$wk_atdptn_ptn_id == "")) {
					//過去データがある場合は、前月も確認
					if ($old_idx >= 0) {
						$chk_5day_flg = $this->obj_check->chk_work_5day($data_old[$old_idx], $arr_workday_count, $old_calendar_cnt);
					} else {
						//過去データが無い場合は今月分で確認
						$chk_5day_flg = $this->obj_check->chk_work_5day($data[$i], $arr_workday_count, $k-1);
					}
					
					if ($chk_5day_flg == "1") {
						//元のパターンの人数を戻す
						if ($data[$i]["atdptn_ptn_id_$k"] != "") {
							$wk_need_id2 = $this->get_need_id($need_array, $data[$i]["atdptn_ptn_id_$k"], $wk_job_id, $wk_team_id, $wk_sex);
							$arr_need_cnt[$wk_need_id2]++;
						}
						//公休に設定する
						$data[$i]["pattern_id_$k"] = $pattern_id;;
						$data[$i]["atdptn_ptn_id_$k"] = "10";
						$data[$i]["reason_$k"] = "24";
						$data[$i]["reason_2_$k"] = "";
						//後ろの処理でクリアしないように設定
						$arr_chk_seq_set[$i][$k] = "1";
						//必要人数を更新
						$wk_need_id2 = $this->get_need_id($need_array, "10", $wk_job_id, $wk_team_id, $wk_sex);
						$arr_need_cnt[$wk_need_id2]--;
					}
				}
				//}
				//連続勤務シフト上限判定
				if ($old_idx >= 0) {
					
					$wk_cnt = $old_calendar_cnt + $k;
					$check_data_old = array();
					$check_data_old[0] = $data_old[$old_idx];
					$check_data_old[0]["pattern_id_$wk_cnt"] = $pattern_id;
					$check_data_old[0]["atdptn_ptn_id_$wk_cnt"] = $data[$i]["atdptn_ptn_id_$k"];
					$data[$i]["ward_pattern_id"] = $pattern_id;
					
					$wk_flg_7 = $this->obj_check->check_upper_limit_shift($data[$i], $k, $upper_limit_array);
					// エラーの場合、未設定に戻す
					if ($wk_flg_7 == "1") {
						$data[$i]["atdptn_ptn_id_$k"] = "";
						$data[$i]["reason_$k"] = "";
						$data[$i]["reason_2_$k"] = "";
						$wk_need_id2 = $this->get_need_id($need_array, $data[$i]["atdptn_ptn_id_$k"], $wk_job_id, $wk_team_id, $wk_sex);
						$arr_need_cnt[$wk_need_id2]++;
					}
				}
				
				
				///-----------------------------------------------------------------------------
				// 次日判定用に日／データ追加
				///-----------------------------------------------------------------------------
				$old_calendar_array[$old_calendar_cnt]["date"] = $calendar_array[$k - 1]["date"];
				$old_calendar_array[$old_calendar_cnt]["type"] = $calendar_array[$k - 1]["type"];
				$m = $old_calendar_cnt + 1;
				$data_old[$old_idx]["pattern_id_$m"] = $data[$i]["pattern_id_$k"];
				$data_old[$old_idx]["atdptn_ptn_id_$m"] = $data[$i]["atdptn_ptn_id_$k"];
				$data_old[$old_idx]["reason_$m"] = $data[$i]["reason_$k"];
				$data_old[$old_idx]["reason_2_$m"] = $data[$i]["reason_2_$k"];
			}
			
			///*****************************************************************************
			// （２）当日の必要人数をチェック
			///*****************************************************************************
			///-----------------------------------------------------------------------------
			// チェック１（シフト勤務者の多い／少ない）
			///-----------------------------------------------------------------------------
			$data_err = $this->obj_check->check_duty_cnt($data, $need_array, $k, $pattern_id);
			for ($m=0; $m<count($data_err); $m++) {
				if ($data_err[$m]["need_cnt"] > 0) {
					//////////////////////////////////////
					$add_staff_array = array();
					$add_staff_cnt = 0;
					$key_staff_id = "";
					///*****************************************************************************
					// （３−１）過去の組み合わせ実績による調整（スタッフ追加）
					///*****************************************************************************
					for ($p=0; $p<count($staff_array); $p++) {
						///-----------------------------------------------------------------------------
						// 追加可能な職員を取得（既に利用されている人は対象外）
						///-----------------------------------------------------------------------------
						$staff_array[$p]["ng_flg"] = "";
						for ($t=0; $t<count($data); $t++) {
							if ($staff_array[$p]["id"] == $data[$t]["staff_id"]) {
								$staff_array[$p]["ng_flg"] = "1";
								break;
							}
						}
						///-----------------------------------------------------------------------------
						//勤務シフト情報(過去)の指定スタッフ位置取得
						///-----------------------------------------------------------------------------
						$old_idx = -1;
						for ($t=0; $t<count($data_old); $t++) {
							if ($staff_array[$p]["id"] == $data_old[$t]["staff_id"]) {
								$old_idx = $t;
								break;
							}
						}
						///-----------------------------------------------------------------------------
						// 追加可能な職員を取得（勤務シフト条件が可能な人）
						///-----------------------------------------------------------------------------
						$wk_need_id2 = $this->get_need_id($need_array, $data_err[$m]["atdptn_ptn_id"], $staff_array[$p]["job_id"], $staff_array[$p]["team_id"], $staff_array[$p]["sex"]);
						list($wk_ptn_id2, $wk_job_id2, $wk_team_id2, $wk_sex2) = explode("::",$wk_need_id2);
						
						if (($staff_array[$p]["ng_flg"] == "") &&
								($wk_job_id2 == $data_err[$m]["job_id"] or $wk_job_id2 == $data_err[$m]["job_id2"] or $wk_job_id2 == $data_err[$m]["job_id3"] ) and
								(empty($wk_team_id2) or $wk_team_id2 == $data_err[$m]["team_id"]) and
								(empty($wk_sex2) or $wk_sex2 == $data_err[$m]["sex"])) {
							///-----------------------------------------------------------------------------
							//スタッフの勤務条件取得
							///-----------------------------------------------------------------------------
							for ($t=0; $t<count($staff_employmen); $t++) {
								if ($staff_array[$p]["id"] == $staff_employmen[$t]["staff_id"]) {
									$data_staff_employmen = $staff_employmen[$t];
									break;
								}
							}
							$data_staff_employmen["pattern_id_$k"] = $pattern_id;
							$data_staff_employmen["atdptn_ptn_id_$k"] = $data_err[$m]["atdptn_ptn_id"];
							
							///-----------------------------------------------------------------------------
							//チェック２（勤務不可な日）
							//チェック３（勤務可能なシフト）
							//チェック４（夜勤の有無）
							///-----------------------------------------------------------------------------
							$wk_flg_2 = $this->obj_check->check_duty_day_2($data_staff_employmen, $week_array, $k);
							$wk_flg_3 = $this->obj_check->check_shift($data_staff_employmen, $week_array, $calendar_array, $k);
							$wk_flg_4 = $this->obj_check->check_night_duty($data_staff_employmen, $data_atdptn_all, $k);
							///-----------------------------------------------------------------------------
							//チェック５（連続勤務日数上限）
							///-----------------------------------------------------------------------------
							if ($old_idx >= 0) {
								$chk_cnt_5 = 1;
								for ($t=$old_calendar_cnt; $t>=1; $t--) {
									//勤務シフト名取得
									$atdptn_name = "";
									//パターンID
									$atdptn_id = "";
									for ($h=0; $h<count($data_atdptn_all); $h++) {
										if (($data_old[$old_idx]["pattern_id_$t"] == $data_atdptn_all[$h]["group_id"]) &&
												($data_old[$old_idx]["atdptn_ptn_id_$t"] == $data_atdptn_all[$h]["id"])) {
											$atdptn_name = $data_atdptn_all[$h]["name"];
											$atdptn_id = $data_atdptn_all[$h]["id"];
											$workday_count = $data_atdptn_all[$h]["workday_count"];
											break;
										}
									}
									//連続勤務日数カウント
									if (($data_old[$old_idx]["atdptn_ptn_id_$t"] == "") ||
											($atdptn_id == "10") || // 休暇を名称ではなくIDで確認
											($workday_count == 0)) { // 夜勤明け等換算日数が0の場合
										break;
									} else {
										$chk_cnt_5++;
									}
								}
								//チェック５（連続勤務日数上限）
								$wk_flg_5 = $this->obj_check->check_upper_limit_day($chk_cnt_5, $duty_upper_limit_day);
							}
							///-----------------------------------------------------------------------------
							//チェック６（禁止組み合わせ）
							///-----------------------------------------------------------------------------
							//前日の勤務シフトを取得
							$before_atdptn_ptn_id = "";
							$before_reason = "";
							if ($k == 1) {
								//初日以外は追加日の前は空白のはずなのでチェックの必要なし
								if ($old_idx >= 0) {
									$wk_cnt = count($old_calendar_array_2);
									if ($data_old[$old_idx]["pattern_id_$wk_cnt"] == $data_staff_employmen["ward_pattern_id"]) {
										$before_atdptn_ptn_id = $data_old[$old_idx]["atdptn_ptn_id_$wk_cnt"];
										$before_reason = $data_old[$old_idx]["reason_$wk_cnt"];
									}
								}
							}
							$wk_flg_6 = $this->obj_check->check_ng_pattern($before_atdptn_ptn_id, $before_reason, $data_err[$m]["atdptn_ptn_id"], "", $NG_pattern_array);
							
							///-----------------------------------------------------------------------------
							//チェック７（連続勤務シフト上限）
							///-----------------------------------------------------------------------------
							if ($old_idx >= 0) {
								$wk_cnt = $old_calendar_cnt + 1;
								$check_data_old = array();
								$check_data_old[0] = $data_old[$old_idx];
								$check_data_old[0]["pattern_id_$wk_cnt"] = $pattern_id;
								$check_data_old[0]["atdptn_ptn_id_$wk_cnt"] = $data_staff_employmen["atdptn_ptn_id_$k"];
								$check_data_old[0]["ward_pattern_id"] = $pattern_id;
								$wk_flg_7 = $this->obj_check->check_upper_limit_shift($check_data_old[0], $wk_cnt, $upper_limit_array);
							}
							
							///-----------------------------------------------------------------------------
							//利用可能なスタッフの場合、追加
							///-----------------------------------------------------------------------------
							if (($wk_flg_2 == "") &&
									($wk_flg_3 == "") &&
									($wk_flg_4 == "") &&
									($wk_flg_5 == "") &&
									($wk_flg_6 == "") &&
									($wk_flg_7 == "")) {
								///-----------------------------------------------------------------------------
								//既に追加したスタッフは対象外
								///-----------------------------------------------------------------------------
								$wk_flg = "";
								for ($t=0; $t<count($add_staff_array); $t++) {
									if ($add_staff_array[$t]["staff_id"] == $staff_array[$p]["id"]) {
										$wk_flg = "1";
										break;
									}
								}
								///-----------------------------------------------------------------------------
								//スタッフ追加
								///-----------------------------------------------------------------------------
								if ($wk_flg == "") {
									///-----------------------------------------------------------------------------
									//対象「職種／勤務シフト」の職員が存在するかチェック
									///-----------------------------------------------------------------------------
									$wk_flg = "";
									for ($t=0; $t<count($data); $t++) {
										$wk_need_id2 = $this->get_need_id($need_array, $data[$t]["atdptn_ptn_id_$k"], $data[$t]["job_id"], $data[$t]["team_id"], $data[$t]["sex"]);
										list($wk_ptn_id2, $wk_job_id2, $wk_team_id2, $wk_sex2) = explode("::",$wk_need_id2);
										if (($wk_job_id2 == $data_err[$m]["job_id"] or $wk_job_id2 == $data_err[$m]["job_id2"] or $wk_job_id2 == $data_err[$m]["job_id3"]) &&
												($wk_ptn_id2 == $data_err[$m]["atdptn_ptn_id"]) and
												(empty($wk_team_id2) or $wk_team_id2 == $data_err[$m]["team_id"]) and
												(empty($wk_sex2) or $wk_sex2 == $data_err[$m]["sex"])) {
											$wk_flg = "1";
											break;
										}
									}
									///-----------------------------------------------------------------------------
									//「職種／勤務シフト」の職員が１人もいない場合、無条件に追加
									///-----------------------------------------------------------------------------
									if ($wk_flg == "") {
										$wk_cnt = count($data);
										$data[$wk_cnt]["group_id"] = $group_id;
										$data[$wk_cnt]["staff_id"] =  $staff_array[$p]["id"];
										$data[$wk_cnt]["job_id"] = $data_err[$m]["job_id"];
										$data[$wk_cnt]["team_id"] = $data_err[$m]["team_id"];
										$data[$wk_cnt]["sex"] = $data_err[$m]["sex"];
										$data[$wk_cnt]["pattern_id_$k"] = $pattern_id;
										$data[$wk_cnt]["atdptn_ptn_id_$k"] = $data_err[$m]["atdptn_ptn_id"];
										$data[$wk_cnt]["reason_$k"] = "";
										$data[$wk_cnt]["reason_2_$k"] = "";
										// 休暇を名称ではなくIDで確認
										if ($data[$wk_cnt]["atdptn_ptn_id_$k"] == "10") {
											$data[$wk_cnt]["reason_$k"] = "24";			//公休へ変更20090107
										}
										$key_staff_id = $staff_array[$p]["id"];		//組み合わせ検索のキーとなるスタッフ
										$data_err[$m]["need_cnt"] = (int)$data_err[$m]["need_cnt"] - 1;
										///-----------------------------------------------------------------------------
										// 勤務シフト情報(過去)にも反映
										///-----------------------------------------------------------------------------
										$t = $old_calendar_cnt + 1;
										$data_old[$old_idx]["pattern_id_$t"] = $pattern_id;
										$data_old[$old_idx]["atdptn_ptn_id_$t"] = $data[$wk_cnt]["atdptn_ptn_id_$k"];
										$data_old[$old_idx]["reason_$t"] = $data[$wk_cnt]["reason_$k"];
										$data_old[$old_idx]["reason_2_$t"] = $data[$wk_cnt]["reason_2_$k"];
									} else {
										$add_staff_array[$add_staff_cnt]["staff_id"] = $staff_array[$p]["id"];
										$add_staff_array[$add_staff_cnt]["job_id"] = $data_err[$m]["job_id"];
										$add_staff_array[$add_staff_cnt]["team_id"] = $data_err[$m]["team_id"];
										$add_staff_array[$add_staff_cnt]["sex"] = $data_err[$m]["sex"];
										$add_staff_array[$add_staff_cnt]["atdptn_ptn_id"] = $data_err[$m]["atdptn_ptn_id"];
										$add_staff_array[$add_staff_cnt]["reason"] = "";
										$add_staff_array[$add_staff_cnt]["reason_2"] = "";
										// 休暇を名称ではなくIDで確認
										if ($add_staff_array[$add_staff_cnt]["atdptn_ptn_id"] == "10") {
											$add_staff_array[$add_staff_cnt]["reason"] = "24";	//公休へ変更20090107
										}
										$add_staff_array[$add_staff_cnt]["cnt"] = 0;
										$add_staff_array[$add_staff_cnt]["no"] = (int)$add_staff_cnt;
										$add_staff_cnt++;
									}
								}
							}
						}
					}
					//////////////////////////////////////
					///*****************************************************************************
					// （３−２）過去の組み合わせ実績による調整（シフト決定）
					// 実績から一緒の組み合わせで、出現回数の多い人から順番に抽出
					///*****************************************************************************
					///-----------------------------------------------------------------------------
					// 追加スタッフが存在する場合（対象職員が１人もいない場合、無条件に追加してるのではずす）
					///-----------------------------------------------------------------------------
					if (count($add_staff_array) > 0) {
						///-----------------------------------------------------------------------------
						// キーとなるスタッフを設定
						///-----------------------------------------------------------------------------
						if ($key_staff_id == "") {
							for ($i=0; $i<count($data); $i++) {
								//職種複数対応
								$wk_need_id2 = $this->get_need_id($need_array, $data[$i]["atdptn_ptn_id_$k"], $data[$i]["job_id"], $data[$i]["team_id"], $data[$i]["sex"]);
								list($wk_ptn_id2, $wk_job_id2, $wk_team_id2, $wk_sex2) = explode("::",$wk_need_id2);
								
								if (($wk_job_id2 == $data_err[$m]["job_id"] or $wk_job_id2 == $data_err[$m]["job_id2"] or $wk_job_id2 == $data_err[$m]["job_id3"]) &&
										($data[$i]["pattern_id_$k"] == $pattern_id) &&
										($data[$i]["atdptn_ptn_id_$k"] == $data_err[$m]["atdptn_ptn_id"]) and
										(empty($wk_team_id2) or $wk_team_id2 == $data_err[$m]["team_id"]) and
										(empty($wk_sex2) or $wk_sex2 == $data_err[$m]["sex"])) {
									$key_staff_id = $data[$i]["staff_id"];		//組み合わせ検索のキーとなるスタッフ
									break;
								}
							}
						}
						///-----------------------------------------------------------------------------
						// 実績から一緒の組み合わせで、出現回数を設定
						///-----------------------------------------------------------------------------
						if ($key_staff_id != "") {
							for ($p=1; $p<=count($rslt_calendar_array); $p++) {
								///-----------------------------------------------------------------------------
								// キーとなるスタッフが対象実績（「職種／シフト」）をしている日を抽出
								///-----------------------------------------------------------------------------
								$wk_flg = "";
								for ($i=0; $i<count($rslt_array); $i++) {
									//職種複数対応
									$wk_need_id2 = $this->get_need_id($need_array, $rslt_array[$i]["atdptn_ptn_id_$p"], $rslt_array[$i]["job_id"], $rslt_array[$i]["team_id"], $rslt_array[$i]["sex"]);
									list($wk_ptn_id2, $wk_job_id2, $wk_team_id2, $wk_sex2) = explode("::",$wk_need_id2);
									if (($rslt_array[$i]["staff_id"] == $key_staff_id) &&
											($wk_job_id2 == $data_err[$m]["job_id"] or $wk_job_id2 == $data_err[$m]["job_id2"] or $wk_job_id2 == $data_err[$m]["job_id3"]) &&
											(empty($wk_team_id2) or $wk_team_id2 == $data_err[$m]["team_id"]) and
											(empty($wk_sex2) or $wk_sex2 == $data_err[$m]["sex"]) and
											($rslt_array[$i]["pattern_id_$p"] == $pattern_id) &&
											($rslt_array[$i]["atdptn_ptn_id_$p"] == $data_err[$m]["atdptn_ptn_id"])) {
										$wk_flg = "1";
										break;
									}
								}
								///-----------------------------------------------------------------------------
								// その日に対象実績（「職種／シフト」）をしているスタッフなら組み合わせ回数をカウントアップ
								///-----------------------------------------------------------------------------
								if ($wk_flg == "1") {
									for ($t=0; $t<count($add_staff_array); $t++) {
										for ($i=0; $i<count($rslt_array); $i++) {
											//職種複数対応
											$wk_need_id2 = $this->get_need_id($need_array, $rslt_array[$i]["atdptn_ptn_id_$p"], $rslt_array[$i]["job_id"], $rslt_array[$i]["team_id"], $rslt_array[$i]["sex"]);
											list($wk_ptn_id2, $wk_job_id2, $wk_team_id2, $wk_sex2) = explode("::",$wk_need_id2);
											if (($rslt_array[$i]["staff_id"] == $add_staff_array[$t]["staff_id"]) &&
													($wk_job_id2 == $data_err[$m]["job_id"] or $wk_job_id2 == $data_err[$m]["job_id2"] or $wk_job_id2 == $data_err[$m]["job_id3"]) &&
													(empty($wk_team_id2) or $wk_team_id2 == $data_err[$m]["team_id"]) and
													(empty($wk_sex2) or $wk_sex2 == $data_err[$m]["sex"]) and
													($rslt_array[$i]["pattern_id_$p"] == $pattern_id) &&
													($rslt_array[$i]["atdptn_ptn_id_$p"] == $data_err[$m]["atdptn_ptn_id"])) {
												$add_staff_array[$t]["cnt"] += 1;
											}
										}
									}
								}
							}
						}
						///-----------------------------------------------------------------------------
						// 対象スタッフを降順にソート
						///-----------------------------------------------------------------------------
						//列方向の配列を得る
						$wk_key1 = array();
						$wk_key2 = array();
						for ($i=0; $i<count($add_staff_array); $i++) {
							$wk_key1[$i] = $add_staff_array[$i]["cnt"];
							$wk_key2[$i] = $add_staff_array[$i]["no"];
						}
						// キーでソートする。
						array_multisort($wk_key1, SORT_DESC, $wk_key2, SORT_ASC, $add_staff_array);
						///-----------------------------------------------------------------------------
						// 必要人数分、対象スタッフ追加
						///-----------------------------------------------------------------------------
						$p=count($data);
						for ($i=0; $i<count($add_staff_array); $i++) {
							if ($data_err[$m]["need_cnt"] > 0) {
								$data[$p]["staff_id"] = $add_staff_array[$i]["staff_id"];
								$data[$p]["job_id"] = $add_staff_array[$i]["job_id"];
								$data[$p]["team_id"] = $add_staff_array[$i]["team_id"];
								$data[$p]["sex"] = $add_staff_array[$i]["sex"];
								$data[$p]["pattern_id_$k"] = $pattern_id;
								$data[$p]["atdptn_ptn_id_$k"] = $add_staff_array[$i]["atdptn_ptn_id"];
								$data[$p]["reason_$k"] = $add_staff_array[$i]["reason"];
								$data[$p]["reason_2_$k"] = $add_staff_array[$i]["reason_2"];
								///-----------------------------------------------------------------------------
								// 勤務シフト情報(過去)にも反映
								///-----------------------------------------------------------------------------
								$t = $old_calendar_cnt + 1;
								$data_old[$old_idx]["pattern_id_$t"] = $pattern_id;
								$data_old[$old_idx]["atdptn_ptn_id_$t"] = $data[$p]["atdptn_ptn_id_$k"];
								$data_old[$old_idx]["reason_$t"] = $data[$p]["reason_$k"];
								$data_old[$old_idx]["reason_2_$t"] = $data[$p]["reason_2_$k"];
								
								$p++;
								$data_err[$m]["need_cnt"] = (int)$data_err[$m]["need_cnt"] - 1;
								
							}
						}
					}
					//////////////////////////////////////
				}
			}

			if ($k <= $auto_end_day) {
				for ($i=0; $i<count($data); $i++) {
					//未設定の場合は、勤務記号設定の一番上のパターンを設定する 20090812
					if ($data[$i]["atdptn_ptn_id_$k"] == "" &&
							($auto_ptn_id == "0" ||
								($auto_ptn_id == $arr_default_ptn_info["atdptn_ptn_id"]))) {
						$data[$i]["pattern_id_$k"] = $pattern_id;
						$data[$i]["atdptn_ptn_id_$k"] = $arr_default_ptn_info["atdptn_ptn_id"];
						$data[$i]["reason_$k"] = $arr_default_ptn_info["reason"];
						$data[$i]["reason_2_$k"] = "";
						
						$old_idx = -1;
						for ($m=0; $m<count($data_old); $m++) {
							if ($data[$i]["staff_id"] == $data_old[$m]["staff_id"]) {
								$old_idx = $m;
								break;
							}
						}
						if ($old_idx >= 0) {
							// 次日判定用に日／データ追加
							$m = $old_calendar_cnt + 1;
							$data_old[$old_idx]["pattern_id_$m"] = $data[$i]["pattern_id_$k"];
							$data_old[$old_idx]["atdptn_ptn_id_$m"] = $data[$i]["atdptn_ptn_id_$k"];
							$data_old[$old_idx]["reason_$m"] = $data[$i]["reason_$k"];
							$data_old[$old_idx]["reason_2_$m"] = $data[$i]["reason_2_$k"];
						}
					}
				}
			}
		}
		return $data;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	// 関数一覧（部品）
	//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 自動シフト作成（指定日の1番多い組み合わせパターンの日を設定）
	// @param	$data_old				勤務シフト情報（過去データ）
	// @param	$data_auto				自動シフト条件情報
	// @param	$duty_dd				勤務日 (Don't Use)
	// @param	$old_calendar_cnt		日数（過去データ）
	//
	// @return	$ret	勤務シフト
	/*************************************************************************/
	function set_auto_day(
		$data_old,
		$data_auto,
		$duty_dd,
		$old_calendar_cnt) {
		
		///-----------------------------------------------------------------------------
		// 初期値設定
		///-----------------------------------------------------------------------------
		$pattern_day = (int)$data_auto["pattern_day"];
		$ptn_search_cnt = (int)$data_auto["ptn_search_cnt"];
		if ($ptn_search_cnt == "") {
			$ptn_search_cnt = 999;	//無限指定時（３６５日以上を設定）
		}
		
		///*****************************************************************************
		// （１）個人ごとに過去勤務パターンから当日シフトの決定
		///*****************************************************************************
		///-----------------------------------------------------------------------------
		// 過去5日間の勤務パターンを取得(5日は設定により可変:$pattern_day)
		// 5日分の勤務パターンを一つの文字列にまとめる(join)
		///-----------------------------------------------------------------------------
		$data_pattern = array();
		for ($i = $old_calendar_cnt; $i > $old_calendar_cnt - $pattern_day; $i--) {
			$data_pattern[] = join("-", array(
						trim($data_old["pattern_id_$i"]),
						trim($data_old["atdptn_ptn_id_$i"]),
						trim($data_old["reason_$i"]),
						trim($data_old["reason_2_$i"]),
						));
		}
		$data_pattern_key = join("<>", $data_pattern);
		
		///-----------------------------------------------------------------------------
		// 過去3ヶ月の勤務パターンのデータをテーブル化(3ヶ月は設定により可変)
		///-----------------------------------------------------------------------------
		$pattern_table = array();
		for ($i = $old_calendar_cnt; $i > 0; $i--) {
			$pattern = array();
			for ( $j=0; $j < $pattern_day+1; $j++){
				$ii = $i - $j;
				$pattern[] = join("-", array(
							trim($data_old["pattern_id_$ii"]),
							trim($data_old["atdptn_ptn_id_$ii"]),
							trim($data_old["reason_$ii"]),
							trim($data_old["reason_2_$ii"]),
							));
			}
			$pattern_key = join("<>", array_slice($pattern,1));
			
			// 5日間のパターンと一致したらカウントする
			if ( $data_pattern_key == $pattern_key ) {
				list($pattern_id, $atdptn_ptn_id, $reason, $reason_2) = explode("-", $pattern[0]);
				
				// 休暇("10")のときは公休("24")しか通さない
				if (! ($atdptn_ptn_id == "10" and $reason != "24")){
					$pattern_table[$pattern[0]]++ ;
					if ($pattern_table[$pattern[0]] > $ptn_search_cnt) {
						$pattern_table[$pattern[0]] = $ptn_search_cnt;
					}
				}
			}
		}
		
		///-----------------------------------------------------------------------------
		// 出現回数の最も多いシフトパターンの翌日のシフトをソート
		///-----------------------------------------------------------------------------
		arsort($pattern_table, SORT_NUMERIC);
		
		///-----------------------------------------------------------------------------
		// return
		///-----------------------------------------------------------------------------
		$return_array = array();
		foreach( $pattern_table as $pattern => $cnt ) {
			list($pattern_id, $atdptn_ptn_id, $reason, $reason_2) = explode("-", $pattern);
			$return_array[] = array(
					pattern_id		=> $pattern_id,
					atdptn_ptn_id	=> $atdptn_ptn_id,
					reason			=> $reason,
					reason_2		=> $reason_2,
					);
		}
		return $return_array;
	}
	/*************************************************************************/
	// 自動シフト作成（その日と同じ曜日（祝日の場合は祝日）でより一番多いパターンのシフトを設定）
	// @param	$data_old				勤務シフト情報（過去データ）
	// @param	$duty_dd				勤務日
	// @param	$calendar_array
	// @param	$week_array				曜日情報
	// @param	$old_calendar_array
	// @param	$old_calendar_cnt		日数（過去データ）
	//
	// @return	$ret	勤務シフト
	/*************************************************************************/
	function set_auto_day_2(
		$data_old,
		$duty_dd,
		$calendar_array,
		$week_array,
		$old_calendar_array,
		$old_calendar_cnt) {
		///-----------------------------------------------------------------------------
		// 初期値設定
		///-----------------------------------------------------------------------------
		$k = $duty_dd;
		
		//曜日設定（土、日、祝日.....）
		$week_data = $week_array[$k]["name"];
		if ($week_data != "土" and $week_data != "日" and
				($calendar_array[$k-1]["type"] == "4" or
					$calendar_array[$k-1]["type"] == "5" or
					$calendar_array[$k-1]["type"] == "6")) {
			$week_data = "祝";
		}
		
		///*****************************************************************************
		// （１−２）個人ごとに過去勤務パターンから当日シフトの決定
		///*****************************************************************************
		///-----------------------------------------------------------------------------
		// その日と同じ曜日（祝日の場合は祝日）でより一番多いパターンのシフトを設定
		///-----------------------------------------------------------------------------
		$data_next = array();
		$pattern_table = array();
		$cnt=0;
		for ($m=$old_calendar_cnt; $m>=1; $m--) {
			///-----------------------------------------------------------------------------
			// 過去日の曜日を取得
			///-----------------------------------------------------------------------------
			$wk_yyyy = substr($old_calendar_array[$m - 1]["date"], 0, 4);
			$wk_mm = substr($old_calendar_array[$m - 1]["date"], 4, 2);
			$wk_dd = substr($old_calendar_array[$m - 1]["date"], 6, 2);
			$tmp_date = mktime(0, 0, 0, $wk_mm, $wk_dd, $wk_yyyy);
			$old_week_data = $this->obj->get_weekday($tmp_date);
			if ($old_week_data != "土" and $old_week_data != "日" and
					($old_calendar_array[$m-1]["type"] == "4" or
						$old_calendar_array[$m-1]["type"] == "5" or
						$old_calendar_array[$m-1]["type"] == "6")) {
				$old_week_data = "祝";
			}
			
			///-----------------------------------------------------------------------------
			// 指定日と同じ曜日（又は祝日）か判定
			///-----------------------------------------------------------------------------
			if ($week_data == $old_week_data) {
				//休日を除く 20090521
				if ($data_old["atdptn_ptn_id_$m"] == "10") {
					continue;
				}
				if ($data_old["atdptn_ptn_id_$m"] != "") {
					$data_pattern = join("-", array(
								trim($data_old["pattern_id_$m"]),
								trim($data_old["atdptn_ptn_id_$m"]),
								trim($data_old["reason_$m"]),
								trim($data_old["reason_2_$m"]),
								));
					$pattern_table[$data_pattern]++;
				}
			}
		}
		
		// ソート
		arsort($pattern_table, SORT_NUMERIC);
		
		///-----------------------------------------------------------------------------
		// return
		///-----------------------------------------------------------------------------
		$return_array = array();
		foreach( $pattern_table as $pattern => $cnt ) {
			list($pattern_id, $atdptn_ptn_id, $reason, $reason_2) = explode("-", $pattern);
			$return_array[] = array(
					pattern_id		=> $pattern_id,
					atdptn_ptn_id	=> $atdptn_ptn_id,
					reason			=> $reason,
					reason_2		=> $reason_2,
					);
		}
		return $return_array;
				
/*					
					///-----------------------------------------------------------------------------
					// 既にチェック済みパターンの場合、その出現情報を利用（出現回数カウントアップ）
					///-----------------------------------------------------------------------------
					$wk_flg = "";
					for ($p=0; $p<count($data_next); $p++) {
						if (($data_next[$p]["pattern_id"] == $data_old["pattern_id_$m"]) &&
								($data_next[$p]["atdptn_ptn_id"] == $data_old["atdptn_ptn_id_$m"]) &&
								($data_next[$p]["reason"] == $data_old["reason_$m"])) {
							$data_next[$p]["cnt"] = (int)$data_next[$p]["cnt"] + 1;
							$wk_flg = "1";
						}
					}
					///-----------------------------------------------------------------------------
					// はじめてのパターンの場合、出現情報にカウント１で設定
					///-----------------------------------------------------------------------------
					if ($wk_flg == "") {
						$data_next[$cnt]["no"] = $cnt;
						$data_next[$cnt]["pattern_id"] = $data_old["pattern_id_$m"];
						$data_next[$cnt]["atdptn_ptn_id"] = $data_old["atdptn_ptn_id_$m"];
						$data_next[$cnt]["reason"] = $data_old["reason_$m"];
						$data_next[$cnt]["reason_2"] = $data_old["reason_2_$m"];
						$data_next[$cnt]["cnt"] = 1;
						$cnt++;
					}
				}
			}
		}
		///-----------------------------------------------------------------------------
		// 出現回数の降順にソート
		///-----------------------------------------------------------------------------
		//列方向の配列を得る
		for ($i=0; $i<count($data_next); $i++) {
			$wk_key1[$i] = $data_next[$i]["cnt"];
			$wk_key2[$i] = $data_next[$i]["no"];
		}
		// キーでソートする。
		array_multisort($wk_key1, SORT_DESC, $wk_key2, SORT_ASC, $data_next);
		
		return $data_next;
*/
	}
	
	/*************************************************************************/
	// 必要人数の充足数を考慮した勤務シフトを設定
	// @param	$next_pattern		set_auto_day()で取得した出現回数の多い順の勤務パターン配列
	// @param	$arr_need_cnt		職種と勤務パターンをキーとした必要人数を格納した配列
	// @param	$job_id				対象職種
	// @param	$arr_chk_need_cnt	職種と勤務パターンをキーとした必要人数を格納した配列、初期状態
	// @param	$need_array
	// @param	$team_id			対象チーム
	// @param	$sex				対象性別
	//
	// @return	$next_pattern	勤務シフト
	/*************************************************************************/
	function set_need_pattern($next_pattern, $arr_need_cnt, $job_id, $arr_chk_need_cnt, $need_array, $team_id, $sex) {
		$arr_ret = array();
		
		// 候補分繰り返す
		foreach( $next_pattern as $pattern ) {
			//職種複数対応
			$wk_need_id = $this->get_need_id($need_array, $pattern["atdptn_ptn_id"], $job_id, $team_id, $sex);
			
			//休暇で必要人数が未設定だった場合も設定する
			if ($pattern["atdptn_ptn_id"] == "10" and $arr_chk_need_cnt[$wk_need_id] == "") {
				$wk_flg = 1;
			} else {
				$wk_flg = 0;
			}
			
			if ($arr_need_cnt[$wk_need_id] > 0 or $wk_flg == 1) {
				$arr_ret[0]["pattern_id"] = $pattern["pattern_id"];
				$arr_ret[0]["atdptn_ptn_id"] = $pattern["atdptn_ptn_id"];
				$arr_ret[0]["reason"] = $pattern["reason"];
				$arr_ret[0]["reason_2"] = $pattern["reason_2"];
				break;
			}
		}
		return $arr_ret;
	}
	
	/*************************************************************************/
	// 組み合わせ禁止指定を除いた勤務シフトを設定。勤務シフト間隔も確認する
	// @param	$next_pattern set_auto_day()で取得した出現回数の多い順の勤務パターン配列
	// @param	$before_atdptn_ptn_id 前日のシフト
	// @param	$before_reason 前日の事由
	// @param	$next_atdptn_ptn_id 翌日のシフト
	// @param	$next_reason 翌日の事由
	// @param	$NG_pattern_array 禁止組み合わせ情報
	// @param	$plan_array_staff	勤務シフト情報
	// @param	$duty_dd			対象日
	// @param	$interval_array		勤務シフト間隔情報
	//
	// @return	$next_pattern	勤務シフト
	/*************************************************************************/
	function set_not_NG_pattern($next_pattern, $before_atdptn_ptn_id, $before_reason, $next_atdptn_ptn_id, $next_reason, $NG_pattern_array, $plan_array_staff, $duty_dd, $interval_array) {
		
		$arr_ret = array();
		$wk_plan_array_staff = $plan_array_staff;
		$j = 0;
		// 候補分繰り返す
		for ($i=0; $i<count($next_pattern); $i++) {
			//前日の確認
			$wk_kinshi_flg = $this->obj_check->check_ng_pattern($before_atdptn_ptn_id, $before_reason, $next_pattern[$i]["atdptn_ptn_id"], $next_reason, $NG_pattern_array);
			//翌日の確認
			$wk_kinshi_flg2 = "";
			if ($next_atdptn_ptn_id != "") {
				$wk_kinshi_flg2 = $this->obj_check->check_ng_pattern($next_pattern[$i]["atdptn_ptn_id"], $before_reason, $next_atdptn_ptn_id, $next_reason, $NG_pattern_array);
			}
			//勤務シフト間隔
			$wk_interval_flg = "";
			//			$wk_plan_array_staff["atdptn_ptn_id_$duty_dd"] = $next_pattern[$i]["atdptn_ptn_id"];
			//			$wk_plan_array_staff["reason_$duty_dd"] = $next_pattern[$i]["reason"];
			//			$wk_interval_flg = $this->obj_check->check_interval($wk_plan_array_staff, $duty_dd, $interval_array);
			$wk_interval_flg = $this->obj_check->check_interval($plan_array_staff, $duty_dd, $next_pattern[$i]["atdptn_ptn_id"], $next_pattern[$i]["reason"], $interval_array);
			
			if ($wk_kinshi_flg != "1" && $wk_kinshi_flg2 != "1" && $wk_interval_flg != "1") {
				$arr_ret[$j]["pattern_id"] = $next_pattern[$i]["pattern_id"];
				$arr_ret[$j]["atdptn_ptn_id"] = $next_pattern[$i]["atdptn_ptn_id"];
				$arr_ret[$j]["reason"] = $next_pattern[$i]["reason"];
				$arr_ret[$j]["reason_2"] = $next_pattern[$i]["reason_2"];
				$j++;
			}
		}
		return $arr_ret;
	}
	
	/*************************************************************************/
	// 集計対象のIDを取得する（必要人数職種複数対応）
	// @param	$need_array			必要人数(カレンダー)
	// @param	$atdptn_ptn_id		勤務シフト（当日）
	// @param	$job_id				対象職種
	// @param	$team_id			対象チーム
	// @param	$sex				対象性別
	//
	// @return	$ret				集計対象ID
	/*************************************************************************/
	function get_need_id($need_array, $atdptn_ptn_id, $job_id, $team_id, $sex) {
		
		$wk_job_id = $job_id;
		$t_flg = 0;
		$s_flg = 0;
		
		foreach ( $need_array as $need ) {
			$need["sex"] = trim($need["sex"]);
			
			if ($atdptn_ptn_id == $need["atdptn_ptn_id"]){
				if ($job_id == $need["job_id"] or $job_id == $need["job_id2"] or $job_id == $need["job_id3"]) {
					if (!empty($need["team_id"]) and $team_id == $need["team_id"]) {
						if (!empty($need["sex"]) and $sex == $need["sex"]) {
							$wk_job_id = $need["job_id"];
							$t_flg = 0;
							$s_flg = 0;
							break;
						} elseif (empty($need["sex"])) {
							$wk_job_id = $need["job_id"];
							$t_flg = 0;
							$s_flg = 1;
						}
					} elseif (empty($need["team_id"])) {
						if (!empty($need["sex"]) and $sex == $need["sex"]) {
							$wk_job_id = $need["job_id"];
							$t_flg = 1;
							$s_flg = 0;
						} elseif (empty($need["sex"])) {
							$wk_job_id = $need["job_id"];
							$t_flg = 1;
							$s_flg = 1;
						}
					}
				}
			} 
		}
		if ( $t_flg == 1 ) {
			$team_id = "";
		}
		if ( $s_flg == 1 ) {
			$sex = "";
		}
		return join("::", array($atdptn_ptn_id, $wk_job_id, $team_id, $sex));
	}
}
?>