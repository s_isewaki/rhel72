<?
$fname = $_SERVER["PHP_SELF"];
$session = @$_REQUEST["session"];

$ymdhms = Date("YmdHis");
$analysis_id = (int)@$_REQUEST["analysis_id"];
$analysis_no = $_REQUEST["analysis_no"];
$chart_seq = (int)@$_REQUEST["chart_seq"];
$command_mode = $_REQUEST["command_mode"];


ob_start();
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");
ob_end_clean();
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
$login_emp_id  = get_emp_id($con,$session,$fname);

$warn_msg = "";
$error = "";
$editing_emp_id = "";
$editing_emp_name = "";


for (;;){
	if($session == "0") { $error = "01 / ". $session; break; }
	if($summary_check == "0") { $error = "02"; break; }
	if($con == "0") { $error = "03"; break; }

	// 画面パラメータチェック
	if (!$analysis_no) { $error = "11"; break; }
	if (!$chart_seq && !$analysis_id) { $error = "12"; break; }
	if ($command_mode != "lock" && $command_mode != "unlock") { $error = "13"; break; }

	// ロックの場合
	if ($command_mode == "lock"){
		$sql =
			" update inci_analysis_rca_chart set".
			" editing_emp_id = '".pg_escape_string($login_emp_id)."'".
			" where chart_seq = ". $chart_seq.
			" and (editing_emp_id is null or editing_emp_id = '')";
		if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "31"; break; }

		// ログインデータの再取得
		$sel = select_from_table($con, "select * from inci_analysis_rca_chart where chart_seq = ".$chart_seq, "", $fname);
		$rs_rca_chart = pg_fetch_array($sel);
		$editing_emp_id = $rs_rca_chart["editing_emp_id"];

		$sel = select_from_table($con, "select emp_lt_nm || ' ' || emp_ft_nm from empmst where emp_id = '".pg_escape_string($editing_emp_id)."'", "", $fname);
		$editing_emp_name = @pg_fetch_result($sel, 0, 0);

		if (!$editing_emp_id ) {$error = "32"; break; }
		if ($editing_emp_id != $login_emp_id) {$warn_msg = "他者によりロックがかけられているため、ロックができませんでした。"; break; }
	}
	else if ($command_mode == "unlock"){
		$sql =
			" update inci_analysis_rca_chart set".
			" editing_emp_id = null".
			" where chart_seq = ". $chart_seq;
		if (!update_set_table($con, $sql, array(), null, "", $fname)) {$error = "41"; break; }
	}



	break;
}
if ($error) $error = "エラーが発生しました。(".$error.")";
?>
{"chart_seq":"<?=$chart_seq?>","analysis_id":"<?=$analysis_id?>","error":"<?=$error?>",
"command_mode":"<?=$command_mode?>","warn_msg":"<?=$warn_msg?>","editing_emp_id":"<?=$editing_emp_id?>","editing_emp_name":"<?=$editing_emp_name?>"}