<?php
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("workflow_common.ini");
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;
$mode = $_GET["show_mode"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


if ($mode == "setting"){
	// 権限のチェック
	$checkauth = check_authority($session, 3, $fname);
	if ($checkauth == "0") {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}
	// 遷移元の設定
	if ($mode != "tmp_holiday"){
		if ($workflow != "") {
			set_referer($con, $session, "workflow", $workflow, $fname);
			$referer = $workflow;
		} else {
			$referer = get_referer($con, $session, "workflow", $fname);
		}
	}
	$title = "ワークフロー | 説明文設定";
}else{
	$title = "説明";
}

// データベースに接続
$con = connect2db($fname);

// テンプレートの説明文を取得
$sql = "select id, content from wkfwexplanation";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$content_list = array();
while($row = pg_fetch_array($sel)){
	$explan_id = $row["id"];
	$explan_content = $row["content"];
	
	$content_list[$explan_id] = $explan_content;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?php echo($title); ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/workflow/workflow.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<?php if ($mode == "setting"){ ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>
		<?php if ($referer == "2") { ?>
		<table id="header">
		<tr>
		<td class="icon"><a href="workflow_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b24.gif" width="32" height="32" border="0" alt="ワークフロー"></a></td>
		<td><a href="workflow_menu.php?session=<?php echo($session) ?>"><b>ワークフロー</b></a></td>
		</tr>
		</table>
		<?php } else { ?>
		<table id="header">
		<tr>
		<td class="icon"><a href="application_menu.php?session=<?php echo($session); ?>"><img src="img/icon/b08.gif" width="32" height="32" border="0" alt="決裁・申請"></a></td>
		<td>
			<a href="application_menu.php?session=<?php echo($session); ?>"><b>決裁・申請</b></a> &gt; 
			<a href="workflow_menu.php?session=<?php echo($session); ?>"><b>管理画面</b></a> &gt; 
			<a href="workflow_notice_setting.php?session=<?php echo($session); ?>"><b>オプション</b></a>
		</td>
		<td class="kanri"><a href="application_menu.php?session=<?php echo($session); ?>"><b>ユーザ画面へ</b></a></td>
		</tr>
		</table>
		<?php } ?>
	
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td valign="top" width="120"><?php show_wkfw_sidemenu($session); ?></td>
		<td width="5"><img src="img/spacer.gif" width="5" height="2" alt=""></td>
		<td valign="top">
	
		<?php show_wkfw_menuitem($session, $fname, ""); ?>
		
		<form action="workflow_explanation_setting_update_exe.php" method="post">
		<table border="0" cellspacing="0" cellpadding="2">
		<tr>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			テンプレート上でボタンをクリックすると説明文を表示します。<br>※休暇申請テンプレート（オプション）を使うときのみ有効<br>
			</font>
		</td>
		</tr>
		<?php 
			if(!empty($content_list)){
				foreach($content_list as $key=>$content){
		?>
					<tr>
						<td><textarea name="content[]" cols="80" rows="16"><?php echo($content); ?></textarea></td>
					</tr>
		<?php }}else{ ?>
					<tr>
						<td><textarea name="content[]" cols="80" rows="16"></textarea></td>
					</tr>
		<?php } ?>
			<tr>
			<td align="right"><input type="submit" value="更新"></td>
			</tr>
		</table>
		<input type="hidden" name="session" value="<? echo($session); ?>">
		</form>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	</table>
<?php }else{ ?>
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<?php 
			if(!empty($content_list)){
				foreach($content_list as $key=>$content){
			?>
						<tr>
							<td><pre><span><?php echo($content); ?></span></pre></td>
						</tr>
			<?php }}else{ ?>
						<tr>
							<td><pre><span>""</span></pre></td>
						</tr>
			<?php } ?>
	</table>
<?php } ?>
</body>
<? pg_close($con); ?>
</html>