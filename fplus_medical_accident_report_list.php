<?
ob_start();
require_once("about_comedix.php");
require_once("yui_calendar_util.ini");
require_once("fplus_common.ini");
require_once("fplus_common_class.php");
require_once("fplus_yui_calendar_text.ini");
require_once("show_fplusadm_all_apply_list.ini");

ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$fplusadm_auth = check_authority($session, 79, $fname);
$con = @connect2db($fname);

$obj = new fplus_common_class($con, $fname);

if(!$session || !$fplusadm_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

//------------------
// 横列フィールドの定義
//------------------
$horz_field_definision =array(
		array("dbkey"=> "occurrences_date",				"text" => "事故発生日"),
		array("dbkey"=> "immediate_connect_date",		"text" => "緊急連絡日"),
		array("dbkey"=> "first_report_date",			"text" => "第一報報告日"),
		array("dbkey"=> "second_report_limit",			"text" => "第二報報告期限"),
		array("dbkey"=> "second_report_date",			"text" => "第二報報告日"),
		array("dbkey"=> "hospital_name",				"text" => "発生病院名"),
		array("dbkey"=> "title",						"text" => "事故タイトル"),
		array("dbkey"=> "patient_name",					"text" => "患者氏名"),
		array("dbkey"=> "patient_age",					"text" => "年齢"),
		array("dbkey"=> "charge_section",				"text" => "担当課"),
		array("dbkey"=> "remaerks",						"text" => "備考（督促状況等）"),
		array("dbkey"=> "check_request",				"text" => "共有情報確認依頼"),
		array("dbkey"=> "draft_date",					"text" => "起案日"),
		array("dbkey"=> "approval_date",				"text" => "決済日"),
		array("dbkey"=> "notice_opening_date",			"text" => "掲示開始日"),
		array("dbkey"=> "notice_number",				"text" => "掲示板掲載��"),
		array("dbkey"=> "medical_accident_info_id",		"text" => "医療事故情報"),
		array("dbkey"=> "file_cnt",						"text" => "関連ファイル")
);

//====================================================================
// パラメータ取得
//====================================================================

//設定項目を取得する
$item_list = "";
$sql = "select * from fplus_etc_setting";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_list = pg_fetch_all($sel);

//更新ボタン押下時に"1"が入る
$update_mode = @$_REQUEST["update_mode"];
//統合ボタン押下時に"1"が入る
$unite_mode = @$_REQUEST["unite_mode"];
//分割ボタン押下時に"1"が入る
$split_mode = @$_REQUEST["split_mode"];
//Excelボタン押下時に"1"が入る
$is_print_mode = @$_REQUEST["print_mode"];


// 印刷（エクセル出力）ボタンを押した時に"1"が入る
//$is_print_mode = @$_REQUEST["print_mode"];

// 指定された日付
$sel_d1_y = (int)@$_REQUEST["sel_d1_y"];
$sel_d1_m = (int)@$_REQUEST["sel_d1_m"];
$sel_d1_d = (int)@$_REQUEST["sel_d1_d"];
$sel_d2_y = (int)@$_REQUEST["sel_d2_y"];
$sel_d2_m = (int)@$_REQUEST["sel_d2_m"];
$sel_d2_d = (int)@$_REQUEST["sel_d2_d"];

// 日付けがなければ、三ヶ月前から本日にセット
if (!checkdate((int)$sel_d1_m, (int)$sel_d1_d, (int)$sel_d1_y)){
	$sel_d1_y = date("Y", strtotime(date("Y/m/d")." -3 month"));
	$sel_d1_m = date("m", strtotime(date("Y/m/d")." -3 month"));
	$sel_d1_d = date("d", strtotime(date("Y/m/d")." -3 month"));
}
if (!checkdate((int)$sel_d2_m, (int)$sel_d2_d, (int)$sel_d2_y)){
	$sel_d2_y = date("Y"); $sel_d2_m = (int)date("m"); (int)$sel_d2_d = date("d");
}

function array_full_merge($a_base, $a_plus){
	foreach($a_plus as $idx => $row) $a_base[] = $row;
	return $a_base;
}

//====================================================================
// DB関係
//====================================================================

//**************************
// 更新処理
//**************************
if($update_mode == "1"){
	// トランザクション開始
	pg_query($con, "begin");

	$data_count = (int)@$_REQUEST["hdn_data_count"];

	for($i = 1;$i <= $data_count;$i++){
		$medical_accident_id = @$_REQUEST["hdn_medical_accident_id_$i"];
		if($medical_accident_id != ''){
			$set_data = array();
			$set_key = array();
			$set_data[0] = $medical_accident_id;
			$set_key[0] = "medical_accident_id";
			$set_data[1] = date("Y-m-d H:i:s");
			$set_key[1] = "update_time";
			$set_data[2] = "f";
			$set_key[2] = "del_flg";

			for($j = 10;$j <= count($horz_field_definision)-2;$j++){
				$set_data[$j-7] = @$_REQUEST["Text_{$j}_{$i}"];
				$set_key[$j-7] = $horz_field_definision[$j-1]['dbkey'];
			}

			//データの更新処理
			$obj->update_medical_accident_report_management($medical_accident_id,$set_key,$set_data);
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

//**************************
// 統合処理
//**************************
if($unite_mode == "1"){
	// トランザクション開始
	pg_query($con, "begin");

	$data_count = (int)@$_REQUEST["hdn_data_count"];

	$medical_accident_id = "";
	$immediate_apply_id = "";
	$first_apply_id = "";
	$second_apply_id = "";
	$main_no = 0;
	$del_medical_accident_id = array();
	$del_cnt = 0;

	for($i = 1;$i <= $data_count;$i++){
		$chk = $_REQUEST["Checkbox_0_$i"];
		if($chk == "on"){
			//更新対象の決定
			//緊急報告
			if(@$_REQUEST["hdn_im_apply_id_$i"] != ""){
				$immediate_apply_id = @$_REQUEST["hdn_im_apply_id_$i"];
				$main_no = $i;
			}
			//第一報
			if(@$_REQUEST["hdn_f_apply_id_$i"] != ""){
				$first_apply_id = @$_REQUEST["hdn_f_apply_id_$i"];
				if($main_no == 0){
					$main_no = $i;
				}
			}
			//第二報
			if(@$_REQUEST["hdn_s_apply_id_$i"] != ""){
				$second_apply_id = @$_REQUEST["hdn_s_apply_id_$i"];
			}
		}
	}

	// 結合先の管理IDを取得
	$medical_accident_id = @$_REQUEST["hdn_medical_accident_id_{$main_no}"];

	// 削除対象の決定
	for($i = 1;$i <= $data_count;$i++){
		$chk = $_REQUEST["Checkbox_0_$i"];
		if($chk == "on"){
			if($i != $main_no){
				$del_medical_accident_id[$del_cnt] = @$_REQUEST["hdn_medical_accident_id_{$i}"];
				$del_cnt++;
			}
		}
	}

	// 更新処理
	// 緊急報告
	if($immediate_apply_id != ""){
		$set_data = array();
		$set_key = array();
		$set_data[0] = $immediate_apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $medical_accident_id;
		$set_key[1] = "medical_accident_id";
		$set_data[2] = date("Y-m-d H:i:s");
		$set_key[2] = "update_time";
	
		//データの更新処理
		$obj->update_immediate_repo($immediate_apply_id,$set_key,$set_data);
	}
	// 第一報
	if($first_apply_id != ""){
		$set_data = array();
		$set_key = array();
		$set_data[0] = $first_apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $medical_accident_id;
		$set_key[1] = "medical_accident_id";
		$set_data[2] = date("Y-m-d H:i:s");
		$set_key[2] = "update_time";
	
		//データの更新処理
		$obj->update_first_repo($first_apply_id,$set_key,$set_data);
	}
	// 第二報
	if($second_apply_id != ""){
		$set_data = array();
		$set_key = array();
		$set_data[0] = $second_apply_id;
		$set_key[0] = "apply_id";
		$set_data[1] = $medical_accident_id;
		$set_key[1] = "medical_accident_id";
		$set_data[2] = date("Y-m-d H:i:s");
		$set_key[2] = "update_time";
	
		//データの更新処理
		$obj->update_second_repo($second_apply_id,$set_key,$set_data);
	}

	// 医療事故報告管理表テーブルデータ削除（論理削除）
	for($i = 0;$i < count($del_medical_accident_id);$i++){
		$set_data = array();
		$set_key = array();
		$set_data[0] = $del_medical_accident_id[$i];
		$set_key[0] = "medical_accident_id";
		$set_data[1] = date("Y-m-d H:i:s");
		$set_key[1] = "update_time";
		$set_data[2] = "t";
		$set_key[2] = "del_flg";
		
		$obj->update_medical_accident_report_management($del_medical_accident_id[$i],$set_key,$set_data);
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

//**************************
// 分割処理
//**************************
if($split_mode == "1"){
	// トランザクション開始
	pg_query($con, "begin");

	$data_count = (int)@$_REQUEST["hdn_data_count"];

	$immediate_apply_id = array();
	$first_apply_id = array();
	$second_apply_id = array();
	$chk_cnt = 0;

	for($i = 1;$i <= $data_count;$i++){
		$chk = $_REQUEST["Checkbox_0_$i"];
		if($chk == "on"){
			//更新対象の決定
			//緊急報告
			if(@$_REQUEST["hdn_im_apply_id_$i"] != ""){
				$immediate_apply_id[$chk_cnt] = @$_REQUEST["hdn_im_apply_id_$i"];
			}
			//第一報
			if(@$_REQUEST["hdn_f_apply_id_$i"] != ""){
				$first_apply_id[$chk_cnt] = @$_REQUEST["hdn_f_apply_id_$i"];
			}
			//第二報
			if(@$_REQUEST["hdn_s_apply_id_$i"] != ""){
				$second_apply_id[$chk_cnt] = @$_REQUEST["hdn_s_apply_id_$i"];
			}
			$chk_cnt++;
		}
	}

	// 新規リンク�盧糧�
	$sql = "SELECT max(medical_accident_id) FROM fplus_medical_accident_report_management";
	$cond = "";
	$link_id_sel = select_from_table($con,$sql,$cond,$fname);
	$link_id = pg_result($link_id_sel,0,"max");
	if($link_id == ""){
		$link_id = 1;
	}else{
		$link_id = $link_id + 1;
	}

	for($i = 0;$i < $chk_cnt;$i++){

		if($immediate_apply_id[$i] == ""){
			//医療事故報告管理表テーブル新規データ追加
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";

			$obj->regist_link_table($set_key_link,$set_data_link);

			//第二報更新
			$set_data = array();
			$set_key = array();
			$set_data[0] = $second_apply_id[$i];
			$set_key[0] = "apply_id";
			$set_data[1] = $link_id;
			$set_key[1] = "medical_accident_id";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			$obj->update_second_repo($second_apply_id[$i],$set_key,$set_data);

		}else if($first_apply_id[$i] == ""){
			//医療事故報告管理表テーブル新規データ追加
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";

			$obj->regist_link_table($set_key_link,$set_data_link);

			//第二報更新
			$set_data = array();
			$set_key = array();
			$set_data[0] = $second_apply_id[$i];
			$set_key[0] = "apply_id";
			$set_data[1] = $link_id;
			$set_key[1] = "medical_accident_id";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			$obj->update_second_repo($second_apply_id[$i],$set_key,$set_data);

		}else if($second_apply_id[$i] == ""){
			//医療事故報告管理表テーブル新規データ追加
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";

			$obj->regist_link_table($set_key_link,$set_data_link);

			//第一報更新
			$set_data = array();
			$set_key = array();
			$set_data[0] = $first_apply_id[$i];
			$set_key[0] = "apply_id";
			$set_data[1] = $link_id;
			$set_key[1] = "medical_accident_id";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			$obj->update_first_repo($first_apply_id[$i],$set_key,$set_data);

		}else{
			//医療事故報告管理表テーブル新規データ追加
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";

			$obj->regist_link_table($set_key_link,$set_data_link);

			//第一報更新
			$set_data = array();
			$set_key = array();
			$set_data[0] = $first_apply_id[$i];
			$set_key[0] = "apply_id";
			$set_data[1] = $link_id;
			$set_key[1] = "medical_accident_id";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			$obj->update_first_repo($first_apply_id[$i],$set_key,$set_data);

			$link_id++;
			//医療事故報告管理表テーブル新規データ追加
			$set_data_link = array();
			$set_key_link = array();
			$set_data_link[0] = $link_id;
			$set_key_link[0] = "medical_accident_id";
			$set_data_link[1] = date("Y-m-d H:i:s");
			$set_key_link[1] = "update_time";
			$set_data_link[2] = "f";
			$set_key_link[2] = "del_flg";

			$obj->regist_link_table($set_key_link,$set_data_link);

			//第二報更新
			$set_data = array();
			$set_key = array();
			$set_data[0] = $second_apply_id[$i];
			$set_key[0] = "apply_id";
			$set_data[1] = $link_id;
			$set_key[1] = "medical_accident_id";
			$set_data[2] = date("Y-m-d H:i:s");
			$set_key[2] = "update_time";

			$obj->update_second_repo($second_apply_id[$i],$set_key,$set_data);
		}
		$link_id++;
	}

	// トランザクションをコミット
	pg_query($con, "commit");

}

//**************************
// 検索
//**************************
$sql = " SELECT ".
	" 	list.medical_accident_id ".
	" 	, case ".
	" 		WHEN list.im_apply_id IS NOT NULL THEN CASE ".
	" 							    WHEN list.f_apply_id IS NOT NULL THEN CASE ".
	" 													WHEN list.s_apply_id IS NOT NULL THEN 't' ".
	" 													ELSE 'f' ".
	" 												  END ".
	" 							    ELSE 'f' ".
	" 							END ".
	" 		ELSE 'f' ".
	" 	  END AS dis_flg ".
	" 	, list.im_apply_id ".
	" 	, list.f_apply_id ".
	" 	, list.s_apply_id ".
	" 	, list.ss_apply_id ".
	" 	, to_char(list.occurrences_date, 'YYYY/MM/DD') as occurrences_date ".
	" 	, to_char(list.immediate_connect_date, 'YYYY/MM/DD') as immediate_connect_date ".
	" 	, to_char(list.first_report_date, 'YYYY/MM/DD') as first_report_date ".
	" 	, to_char(list.second_report_limit, 'YYYY/MM/DD') as second_report_limit ".
	" 	, to_char(list.second_report_date, 'YYYY/MM/DD') as second_report_date ".
	" 	, list.hospital_id ".
	" 	, hosp.hospital_name ".
	" 	, list.title ".
	" 	, list.patient_name ".
	" 	, list.patient_age || '歳' AS patient_age ".
	" 	, list.charge_section ".
	" 	, list.remaerks ".
	" 	, list.check_request ".
	" 	, list.draft_date ".
	" 	, list.approval_date ".
	" 	, list.notice_opening_date ".
	" 	, list.notice_number ".
	" 	, list.medical_accident_info_id ".
	" 	, case when list.file_cnt is null then 0 else list.file_cnt end as file_cnt".
	" FROM ".
	" 	(SELECT ".
	" 		am.medical_accident_id ".
	" 		, im.apply_id AS im_apply_id ".
	" 		, f.apply_id AS f_apply_id ".
	" 		, s.apply_id AS s_apply_id ".
	" 		, ss.apply_id AS ss_apply_id ".
	" 		,(CASE ".
	" 			WHEN s.occurrences_date IS NULL THEN CASE ".
	" 								WHEN ss.occurrences_date IS NULL THEN CASE ".
	" 											WHEN f.occurrences_date IS NULL THEN im.occurrences_date ".
	" 											ELSE f.occurrences_date ".
	" 										END ".
	" 								ELSE ss.occurrences_date ".
	" 							END ".
	" 			ELSE s.occurrences_date ".
	" 		END) as occurrences_date ".
	" 		,im.immediate_connect_date ".
	" 		,f.report_date AS first_report_date ".
	" 		,date(f.report_date + interval '45 day') AS second_report_limit ".
	" 		,(CASE ".
	" 			WHEN s.report_date IS NULL THEN ss.report_date ".
	" 			ELSE s.report_date ".
	" 		END)AS second_report_date ".
	" 		,(CASE ".
	" 			WHEN s.hospital_id IS NULL THEN CASE ".
	" 								WHEN ss.hospital_id IS NULL THEN CASE ".
	" 													WHEN f.hospital_id IS NULL THEN im.hospital_id ".
	" 													ELSE f.hospital_id ".
	" 												END ".
	" 								ELSE ss.hospital_id ".
	" 							END ".
	" 			ELSE s.hospital_id ".
	" 		END) as hospital_id ".
	" 		,(CASE ".
	" 			WHEN s.title IS NULL THEN CASE ".
	" 							WHEN ss.title IS NULL THEN CASE ".
	" 											WHEN f.title IS NULL THEN im.title ".
	" 											ELSE f.title ".
	" 										END ".
	" 							ELSE ss.title ".
	" 						END ".
	" 			ELSE s.title ".
	" 		END) as title ".
	" 		,(CASE ".
	" 			WHEN s.patient_name IS NULL THEN CASE ".
	" 								WHEN ss.patient_name IS NULL THEN CASE ".
	" 													WHEN f.patient_name IS NULL THEN im.patient_name ".
	" 													ELSE f.patient_name ".
	" 												END ".
	" 								ELSE ss.patient_name ".
	" 							END ".
	" 			ELSE s.patient_name ".
	" 		END) as patient_name ".
	" 		,(CASE ".
	" 			WHEN s.patient_age IS NULL THEN CASE ".
	" 								WHEN ss.patient_age IS NULL THEN CASE ".
	" 													WHEN f.patient_age IS NULL THEN im.patient_age ".
	" 													ELSE f.patient_age ".
	" 												END ".
	" 								ELSE ss.patient_age ".
	" 							END ".
	" 			ELSE s.patient_age ".
	" 		END) as patient_age ".
	" 		, am.charge_section ".
	" 		, am.remaerks ".
	" 		, am.check_request ".
	" 		, am.draft_date ".
	" 		, am.approval_date ".
	" 		, am.notice_opening_date ".
	" 		, am.notice_number ".
	" 		, info.medical_accident_info_id ".
	" 		, files.file_cnt ".
	" 	FROM ".
	" 		((((((fplus_medical_accident_report_management am left outer join (SELECT im.* FROM fplus_immediate_report im, fplusapply apl WHERE im.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') im on am.medical_accident_id = im.medical_accident_id) ".
	" 		  left outer join (SELECT f.* FROM fplus_first_report f, fplusapply apl WHERE f.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') f on am.medical_accident_id = f.medical_accident_id) ".
	" 		  left outer join (SELECT s.* FROM fplus_second_report s, fplusapply apl WHERE s.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat = '1') s on am.medical_accident_id = s.medical_accident_id) ".
	" 		  left outer join (SELECT ss.* FROM fplus_second_report ss, fplusapply apl WHERE ss.apply_id = apl.apply_id AND apl.delete_flg = 'f' AND apl.apply_stat <> '1') ss on am.medical_accident_id = ss.medical_accident_id) ".
	" 		  left outer join fplus_medical_accident_info info on am.medical_accident_id = info.medical_accident_id AND info.del_flg <> 't') ".
	" 		  left outer join (SELECT medical_accident_id, count(*) as file_cnt from fplus_medical_accident_report_files WHERE del_flg <> 't' GROUP BY medical_accident_id) files on am.medical_accident_id = files.medical_accident_id) ".
	" 	WHERE ".
	" 		(im.apply_id is not null ".
	" 		OR f.apply_id is not null ".
	" 		OR s.apply_id is not null) ".
	" 		AND am.del_flg <> 't') list ".
	" 	, fplus_hospital_master hosp ".
	" WHERE ".
	" 	list.hospital_id = hosp.hospital_id ".
	" 	AND hosp.del_flg <> 't' ";

if ($sel_d1_y)        $sql .= " and list.occurrences_date >= '".sprintf("%04d/%02d/%02d",$sel_d1_y,$sel_d1_m,$sel_d1_d)."' "; // 開始日
if ($sel_d2_y)        $sql .= " and list.occurrences_date <= '".sprintf("%04d/%02d/%02d",$sel_d2_y,$sel_d2_m,$sel_d2_d)."' "; // 終了日

$sql .= " ORDER BY ".
	" 	occurrences_date desc ".
	" 	, immediate_connect_date ".
	" 	, first_report_date ".
	" 	, second_report_date ";


$sel = @select_from_table($con, $sql, "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$result = pg_fetch_all($sel);

//====================================================================
// 印刷（XLSエクセル出力）の場合。内容を出力してdieする。
// もし失敗すれば、ブラウザ上に内容が表示されるであろう。
//====================================================================
if ($is_print_mode){

	//====================================================================
	// データマトリクスを作成する
	//====================================================================
	$data_count = (int)@$_REQUEST["hdn_data_count"];
	$data_list = array();

	for($i = 0;$i < $data_count;$i++){
		for($j = 0;$j < count($horz_field_definision);$j++){
			$data_list[$i][$j] = $result[$i][$horz_field_definision[$j]["dbkey"]];
		}
	}

	$kikan = $sel_d1_y."年".$sel_d1_m."月".$sel_d1_d."日 〜 ".$sel_d2_y."年".$sel_d2_m."月".$sel_d2_d."日";

	$title = "医療事故報告管理表";

	require_once("fplus_medical_accident_report_list_print.ini");

	fplus_medical_accident_report_list_print_execute(
		$horz_field_definision, // 横列フィールドの定義
		$data_list, // 検索データ
		$title, // タイトル
		$kikan //期間
	);
	die;

}

//====================================================================
// HTMLヘッダ
//====================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 医療事故報告管理表</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#E6B3D4 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#dfd95e solid 1px; padding:3px; }
</style>

<? //-------------------------------------------------------------------------?>
<? // JavaScript                                                              ?>
<? //-------------------------------------------------------------------------?>
<script type="text/javascript">

	<? // ************* エクセル出力開始 ************* ?>
	function startPrint(){
		if (document.frm.hdn_data_count.value == ""){
			alert("対象データが存在しません。");
			return;
		}
		document.frm.action = 'fplus_medical_accident_report_list.php';
		document.frm.print_mode.value = "1";
		document.frm.unite_mode.value = "";
		document.frm.update_mode.value = "";
		document.frm.split_mode.value = "";
		document.frm.target = "print_window";
		document.frm.submit();

		return false;

	}
	
	<? // ************* 画面の幅の取得 ************* ?>
	function getClientWidth(){
		if (document.body.clientWidth) return document.body.clientWidth;
		if (window.innerWidth) return window.innerWidth;
		if (document.documentElement && document.documentElement.clientWidth) {
			return document.documentElement.clientWidth;
		}
		return 0;
	}


	<? // ************* 上部スクロールバー制御 ************* ?>
	var scroll_div = null;
	var scrollbar_div = null;
	var scroll_image_gauge = null;
	var scroll_initialized = false;
	function listScrolled(id){
		var newW = getClientWidth();
		if (!newW) return;
		if (scroll_div==null) scroll_div = document.getElementById("scroll_div");
		if (!scroll_div) return;
		if (scrollbar_div==null) scrollbar_div = document.getElementById("scrollbar_div");
			scroll_div.style.overflowX = "scroll";

			if (scroll_div.scrollWidth > 0){
				if (scroll_image_gauge==null) scroll_image_gauge = document.getElementById("scroll_image_gauge");
				scroll_image_gauge.style.height = "1px";
				scroll_image_gauge.style.width = scroll_div.scrollWidth + "px";
			}

		scrollbar_div.style.width = (newW-2) + "px";
		scroll_div.style.width = (newW-2) + "px";

		scroll_initialized = true;
		if(!id) return;
		if (scrollbar_div.scrollLeft != scroll_div.scrollLeft){
			if (id == scroll_div.id){
				scrollbar_div.scrollLeft = scroll_div.scrollLeft;
			} else {
				scroll_div.scrollLeft = scrollbar_div.scrollLeft;
			}
		}
	}
	<? // ************* ポップアップ表示用 ************* ?>
	function show_sub_window(url) {
		var h = '700';
		var w = '1024';
		var option = "directories=no,location=no,menubar=no,resizable=no,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
		window.open(url, 'medical_accident_info',option);
	}

	<? // ************* 更新ボタン押下 ************* ?>
	function clickReLoad(){
		if (window.InputCheck) {
			if (!InputCheck()) {
				return;
			}
		}

		if (confirm('更新します。よろしいですか？')) {
			//更新モードON
			document.frm.action = 'fplus_medical_accident_report_list.php';
			document.frm.print_mode.value = "";
			document.frm.unite_mode.value = "";
			document.frm.update_mode.value = "1";
			document.frm.split_mode.value = "";
			document.frm.target = "_self";
			document.frm.submit();
			return false;
		}
	}

	<? // ************* 画面再表示 ************* ?>
	function reload_page(){
		document.frm.action = 'fplus_medical_accident_report_list.php';
		document.frm.print_mode.value = "";
		document.frm.unite_mode.value = "";
		document.frm.update_mode.value = "";
		document.frm.split_mode.value = "";
		document.frm.target = "_self";
		document.frm.submit();
		return false;
	}

	<? // ************* 統合ボタン押下時 ************* ?>
	function clickUnite(){
		var count = parseInt(document.frm.hdn_data_count.value);
		var chk = 0;
		var obj;
		var im_app_cnt = 0;
		var f_app_cnt = 0;
		var s_app_cnt = 0;
		for (var i=1;i <= count;i++) {
			obj = document.getElementById("Checkbox_0_"+ i);
			if(obj.checked){
				chk++;
				// チェックされた緊急報告の数をカウント
				if(document.getElementById("hdn_im_apply_id_"+ i).value != ""){
					im_app_cnt++;
				}
				// チェックされた第一報の数をカウント
				if(document.getElementById("hdn_f_apply_id_"+ i).value != ""){
					f_app_cnt++;
				}
				// チェックされた第二報の数をカウント
				if(document.getElementById("hdn_s_apply_id_"+ i).value != "" || document.getElementById("hdn_ss_apply_id_"+ i).value != ""){
					s_app_cnt++;
				}
			}
		}

		if(im_app_cnt > 1){
			alert("重複があるため、統合できません。");
			return false;
		}

		if(f_app_cnt > 1){
			alert("重複があるため、統合できません。");
			return false;
		}

		if(s_app_cnt > 1){
			alert("重複があるため、統合できません。");
			return false;
		}

		if (chk < 2) {
			alert("２つ以上選択してください。");
			return false;
		}

		if (confirm('統合します。よろしいですか？')) {
			//更新モードON
			document.frm.action = 'fplus_medical_accident_report_list.php';
			document.frm.print_mode.value = "";
			document.frm.unite_mode.value = "1";
			document.frm.update_mode.value = "";
			document.frm.split_mode.value = "";
			document.frm.target = "_self";
			document.frm.submit();
			return false;
		}
	}

	<? // ************* 分割ボタン押下時 ************* ?>
	function clickSplit(){

		var link_cnt = 0;
		var count = parseInt(document.frm.hdn_data_count.value);
		var obj;
		var chkcnt = 0;

		for (var i=1;i <= count;i++) {
			obj = document.getElementById("Checkbox_0_"+ i);
			link_cnt = 0;
			if(obj.checked){

				// チェックされた緊急報告の数をカウント
				if(document.getElementById("hdn_im_apply_id_"+ i).value != ""){
					link_cnt++;
				}
				// チェックされた第一報の数をカウント
				if(document.getElementById("hdn_f_apply_id_"+ i).value != ""){
					link_cnt++;
				}
				// チェックされた第二報の数をカウント
				if(document.getElementById("hdn_s_apply_id_"+ i).value != ""){
					link_cnt++;
				}

				if(link_cnt < 2){
					alert("分割できないものが含まれています。");
					return false;
				}
				chkcnt++;
			}
		}

		if(chkcnt == 0){
			alert("分割対象を選択してください。");
			return false;
		}

		if (confirm('分割します。よろしいですか？')) {
			//更新モードON
			document.frm.action = 'fplus_medical_accident_report_list.php';
			document.frm.print_mode.value = "";
			document.frm.unite_mode.value = "";
			document.frm.update_mode.value = "";
			document.frm.split_mode.value = "1";
			document.frm.target = "_self";
			document.frm.submit();
			return false;
		}

	}

	function date_clear(alea){
		document.getElementById(alea).value = "";
	}

	//一覧カレンダーからの値取得
	function call_back_fplus_calendar(setid,ymd){
		document.getElementById(setid).value = ymd;
	}

</script>

<? //条件用 カレンダ ?>
<? write_yui_calendar_use_file_read_0_12_2(); ?>
<? write_yui_calendar_script2(2); ?>

</head>
<body style="margin:0;" onload="initcal(); listScrolled();" onresize="listScrolled();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:100%;"><tr><td valign="top">

<? //-------------------------------------------------------------------------?>
<? // ぱんくず、管理画面へのリンク                                            ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
	<td width="32" height="32" class="spacing">
	  <a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a>
	</td>
	<td>
	  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplus_medical_accident_report_list.php?session=<? echo($session); ?>&epi_type=<?=$epi_type?>"><b>医療事故報告管理表</b></a></font>
	</td>
	<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplusadm_menu.php?session=<? echo($session); ?>&workflow=1"><b>管理画面へ</b></a></font>
	</td>
  </tr>
</table>


<? //-------------------------------------------------------------------------?>
<? // メインメニュータブと、ボトムマージン描画                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><? show_applycation_menuitem($session,$fname,""); ?></tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td bgcolor="#e6b3d4"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>


<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="fplus_medical_accident_report_list.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="session" id="session" value="<? echo($session); ?>" />
	<input type="hidden" name="print_mode" id="print_mode" value="" />
	<input type="hidden" name="update_mode" id="update_mode" value="" />
	<input type="hidden" name="unite_mode" id="unite_mode" value="" />
	<input type="hidden" name="split_mode" id="split_mode" value="" />
	<input type="hidden" name="hdn_data_count" id="hdn_data_count" value="<?=$result[0][$horz_field_definision[0]["dbkey"]] == "" ? "0" :count($result)?>" />

<? //-------------------------------------------------------------------------?>
<? // 検索条件                                                                ?>
<? //-------------------------------------------------------------------------?>
<div style="float:left;">
<table border="0" cellspacing="0" cellpadding="2" class="list1">
<tr>
	<td bgcolor="#feeae9"><?=$font?>期間</font></td>
	<td bgcolor="#fff8f7">
	<? // ************* 期間（日付From） ドロップダウン ******************** ?>
	<div style="float:left"><?=$font?>
	  <select id="date_y1" name="sel_d1_y"><option></option>
  		<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m1" name="sel_d1_m"><option></option>
  		<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d1" name="sel_d1_d"><option></option>
  		<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d1_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select></font>
	</div>
	<? // ************* 日付From のカレンダボタン ******************** ?>
  <div style="float:left; padding:0px 2px"><?=$font?>
	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
		<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	<div style="float:left"><?=$font?>
  〜</font>
  </div>
	<? // ************* 期間（日付To） ドロップダウン ******************** ?>
	<div style="float:left"><?=$font?>
	  <select id="date_y2" name="sel_d2_y"><option></option>
  		<? for($i=date("Y"); $i>=2000; $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_y ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_m2" name="sel_d2_m"><option></option>
  		<? for($i=1; $i<=12; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_m ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select>
	  <select id="date_d2" name="sel_d2_d"><option></option>
  		<? for($i=1; $i<=31; $i++) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_d ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
	  </select></font>
	</div>
	<? // ************* 日付Toのカレンダボタン ******************** ?>
	<div style="float:left; padding:0px 2px"><?=$font?>
	<img src="img/calendar_link.gif" style="left:3px;z-index:1;cursor:pointer;" onclick="show_cal2();"/>
		<div id="cal2Container" style="position:absolute;display:none;z-index:10000;"></div></font>
	</div>
	</td>
	<td bgcolor="#fff8f7"><?=$font?><input type="button" onclick="reload_page();" value="検索"  /></font></td>
</tr>
</table>
</div>
	<div style="margin-top:4px"><?=$font?>
		<? // ************* ボタン ******************** ?>
		<div style="float:right;">
			<table cellpadding="0">
				<tr>
					<td>
						<?=$font?><input type="button" onclick="startPrint();" value="EXCEL出力" style="width: 80px" <?= $result[0][$horz_field_definision[0]["dbkey"]] == "" ? "disabled" : "" ?> /></font>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>
						<?=$font?><input type="button" onclick="clickSplit();" value="報告分割" style="width: 80px" /></font>
					</td>
					<td>
						<?=$font?><input type="button" onclick="clickUnite();" value="報告統合" style="width: 80px" /></font>
					</td>
					<td>
						<?=$font?><input type="button" name="reload_btn" value="更新" style="width: 80px" onclick="clickReLoad()" /></font>
					</td>
				</tr>
			</table>
		</div>
		<br style="clear:both"/>
		</font>
	</div>

<? //-------------------------------------------------------------------------?>
<? // 一覧表                                                                  ?>
<? //-------------------------------------------------------------------------?>
<div style="margin-top:10px; border:1px solid #E6B3D4; background-color:#eee;z-index:-1;">
<div id="scrollbar_div" style="line-height:0; font-size:1px; height:18px; overflow-x:scroll;z-index:-1;" onscroll="listScrolled(this.id);"><img src="img/spacer.gif" id="scroll_image_gauge"/></div>
<div id="scroll_div" onscroll="listScrolled(this.id);" style="width:100px; overflow-x:scroll;z-index:-1;">
	<? $colcnt = count($horz_field_definision); ?>
	<? $colWidth = "100px"; ?>
	<? $tableWidth = "width:".(($colcnt*100) + 10)."px"?>
<table class="list1" style="<?=$tableWidth?>; background-color:#fff;">

	<? // 列ヘッダ行 ?>
	<tr style="background-color:#ffdfff;">
		<td id="cell_0_0" style="width:10px;"></td>
		<? $colttl = array(); ?>
		<? for ($hh = 0; $hh < count($horz_field_definision); $hh++){ ?>
			<td id="cell_<?=$hh+1?>_0" class="j12" style="text-align:center; padding: 6px" nowrap>
				<?=$horz_field_definision[$hh]["text"]?>
			</td>
			<? $colttl[$hh] = 0; ?>
		<? } ?>
	</tr>
<? if ($result[0][$horz_field_definision[0]["dbkey"]] == ""){ ?>
</table>
	<div style="color:#999; text-align:center; margin-top:10px"><?=$font?>該当データはありません。</font></div>
<? } else { ?>
	<? // 行データをレコード件数分くりかえし描画 ?>
	<? $cal_count = 0;?>
	<? for($vh = 0;$vh < count($result);$vh++){ ?>
	<tr>
		<td id="cell_0_<?=$vh+1?>" style="background-color:#ffffff;width:10px;" class="j12" align="center" >
			<input id="Checkbox_0_<?=$vh+1?>" name="Checkbox_0_<?=$vh+1?>" type="checkbox" <?=$disable?> />
		</td>
		<? for ($hh = 0; $hh < count($horz_field_definision); $hh++){ ?>
				<?
				if ($hh < 9) {
					$align_str = "left";
					if ($hh <= 4) {
						$align_str = "center";
					} else if ($hh == 8) {
						$align_str = "right";
					}
					?>
					<td id="cell_<?=$hh+1?>_<?=$vh+1?>" align="<?=$align_str?>" nowrap>
						<?
						if (($hh == 1 || $hh == 2 || $hh == 4) and $result[$vh][$horz_field_definision[$hh]["dbkey"]] != "") {
							switch ($hh) {
								case 1:
									$wk_apply = $result[$vh]["im_apply_id"];
									break;
								case 2:
									$wk_apply = $result[$vh]["f_apply_id"];
									break;
								case 4:
									if ($result[$vh]["s_apply_id"] != "") {
										$wk_apply = $result[$vh]["s_apply_id"];
									} else {
										$wk_apply = $result[$vh]["ss_apply_id"];
									}
									break;
							}
								
							$onclick_event = "onclick=\"show_sub_window('fplus_workflow_detail.php?session=$session&apply_id=$wk_apply');\"";
							?>
							<a href="javascript:void(0)" <?=$onclick_event?>>
							<?=$font?><?=$result[$vh][$horz_field_definision[$hh]["dbkey"]]?></font>
							</a>
						<?
						} else if ($hh == 6) {
						?>
							<?=$font?>
							<?
							if (mb_strlen($result[$vh][$horz_field_definision[$hh]["dbkey"]]) > 10) {
								?>
								<span title="<?= $result[$vh][$horz_field_definision[$hh]["dbkey"]] ?>">
								<?
								echo mb_substr($result[$vh][$horz_field_definision[$hh]["dbkey"]], 0, 10) . "...";
								echo "</span>";
							} else {
								echo $result[$vh][$horz_field_definision[$hh]["dbkey"]];
							}
							?>
							</font>
						<?
						} else {
						?>
							<?=$font?><?=$result[$vh][$horz_field_definision[$hh]["dbkey"]]?></font>
						<?
						}
						?>
					</td>
				<? }else if($hh < 16){ ?>
					<? if($hh == 12 || $hh == 13 || $hh == 14){ ?>
						<td style="padding: 2px" nowrap>
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td style="border-style: none; padding: 0px">
										<input id="Text_<?=$hh+1?>_<?=$vh+1?>" name="Text_<?=$hh+1?>_<?=$vh+1?>" type="text"  style="text-align:center; width: 80px" value="<?=$result[$vh][$horz_field_definision[$hh]["dbkey"]]?>" readonly />
									</td>
									<td style="border-style: none; padding: 0px; padding-left: 2px">
										<? // ************* 日付のカレンダボタン ******************** ?>
										<img id="date_caller_<?=$hh+1?>_<?=$vh+1?>" src="img/calendar_link.gif" style="position:relative;z-index:1;cursor:pointer; left: 0px;" alt="カレンダー" onclick="window.open('fplus_calendar.php?session=<? echo($session); ?>&setarea=Text_<?=$hh+1?>_<?=$vh+1?>&caller=Text_<?=$hh+1?>_<?=$vh+1?>', 'epinet_arise_calendar', 'width=640,height=480,scrollbars=yes');"/>
									</td>
									<td style="border-style: none; padding: 0px; padding-left: 2px">
										<input type="button" value="クリア" onclick="date_clear('Text_<?=$hh+1?>_<?=$vh+1?>');">
									</td>
								</tr>
							</table>
						</td>
						<? $cal_count++; ?>
					<? }else{ ?>
						<td><?=$font?><input id="Text_<?=$hh+1?>_<?=$vh+1?>" name="Text_<?=$hh+1?>_<?=$vh+1?>" type="text"  style="text-align:left;" value="<?=$result[$vh][$horz_field_definision[$hh]["dbkey"]]?>" /></font></td>
					<? } ?>
				<?
				// 関連ファイル
				}else if($hh == 17){
				?>
					<td id="Td1" align="center"><?=$font?>
						<a href="javascript:void(0)" onclick="show_sub_window('fplus_medical_accident_report_files.php?session=<?=$session?>&medical_accident_id=<?=$result[$vh]["medical_accident_id"]?>');" style="color:#00f">
						<?= $result[$vh][$horz_field_definision[$hh]["dbkey"]] > 0 ? "一覧" : "登録"; ?>
						</a>
					</font></td>
				<? }else{ ?>
					<td id="Td1" align="center"><?=$font?>
						<a href="javascript:void(0)" onclick="show_sub_window('fplus_medical_accident_info.php?session=<?=$session?>&medical_accident_id=<?=$result[$vh]["medical_accident_id"]?>&row_no=<?=($vh+1)?>&medical_accident_info_id=<?=$result[$vh]["medical_accident_info_id"]?>');" style="color:#00f"><?=$result[$vh]["medical_accident_info_id"] == '' ? "作成" : "作成済" ?></a>
					</font></td>
				<? } ?>
		<? } ?>
	</tr>
	<input type="hidden" name="hdn_medical_accident_id_<?=$vh+1?>" id="hdn_medical_accident_id_<?=$vh+1?>" value="<?=$result[$vh]["medical_accident_id"]?>" />
	<input type="hidden" name="hdn_im_apply_id_<?=$vh+1?>" id="hdn_im_apply_id_<?=$vh+1?>" value="<?=$result[$vh]["im_apply_id"]?>" />
	<input type="hidden" name="hdn_f_apply_id_<?=$vh+1?>" id="hdn_f_apply_id_<?=$vh+1?>" value="<?=$result[$vh]["f_apply_id"]?>" />
	<input type="hidden" name="hdn_s_apply_id_<?=$vh+1?>" id="hdn_s_apply_id_<?=$vh+1?>" value="<?=$result[$vh]["s_apply_id"]?>" />
	<input type="hidden" name="hdn_ss_apply_id_<?=$vh+1?>" id="hdn_ss_apply_id_<?=$vh+1?>" value="<?=$result[$vh]["ss_apply_id"]?>" />
	<input type="hidden" name="hdn_medical_accident_title_<?=$vh+1?>" id="hdn_medical_accident_title_<?=$vh+1?>" value="<?=$result[$vh]["title"]?>" />
	<? } ?>
</table>
</div>
</div>
<? } ?>
</form>
</td></tr></table>
</body>
</html>