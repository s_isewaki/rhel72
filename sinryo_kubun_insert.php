<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
//ファイルの読み込み
require("about_postgres.php");
require("about_session.php");
require("about_authority.php");
require("about_validation.php");
require("get_values.php");
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//権限チェック
$dept = check_authority($session,59,$fname);
if($dept == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//ＤＢのコネクション作成
$con = connect2db($fname);

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

//入力内容チェック
if($div_name ==""){
	echo("<script language=\"javascript\">alert(\"{$summary_kubun_title}名を入力してください\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

/*
$check = special_char_check($div_name);
if($check == 0){
	echo("<script language=\"javascript\">alert(\"特殊な文字は使用しないでください\");</script>\n");
	echo("<script language=\"javascript\">location.href=\"./sinryo_kubun_insert.php?session=$session\";</script>\n");
	exit;
}
*/

//ＤＢへ診療区分名を登録する




//同一名の診療区分が既に登録されているかチェック
$div_name = mb_ereg_replace("　"," ",$div_name);
$div_name = trim($div_name);
$cond = "where diag_div like '$div_name'";
$tbl = "select count(diag_div) from divmst";
$sel = select_from_table($con,$tbl,$cond,$fname);
if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$count = pg_result($sel,0,"count");
if($count > 0){
	echo("<script language=\"javascript\">alert(\"その{$summary_kubun_title}名は既に登録されています\");</script>\n");
	echo("<script language=\"javascript\">history.back();</script>\n");
	exit;
}

//診療区分ＩＤの最大値を取得
$cond = "";
$tbl = "select max(div_id) from divmst";
$sel = select_from_table($con,$tbl,$cond,$fname);

if($sel == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}

$max = pg_result($sel,0,"max");
$new_div_id = $max + 1;
$emp_id = get_emp_id($con, $session, $fname);





// 画像登録
$icon_file_name = $_FILES["div_icon_file"]["name"];
if (@$icon_file_name) {
  // ファイルアップロードチェック
  define(UPLOAD_ERR_OK, 0);
  define(UPLOAD_ERR_INI_SIZE, 1);
  define(UPLOAD_ERR_FORM_SIZE, 2);
  define(UPLOAD_ERR_PARTIAL, 3);
  define(UPLOAD_ERR_NO_FILE, 4);

  // gif, jpgチェック
  $pos = strrpos($icon_file_name, ".");
  $ext = "";
  if ($pos > 0 ) {
    $ext = substr($icon_file_name, $pos+1, 3);
    $ext = strtolower($ext);
  }

  if ($pos === false || ($ext != "gif" && $ext != "jpg" && $ext != "png")) {
    echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }

  switch ($_FILES["div_icon_file"]["error"]) {
  case UPLOAD_ERR_INI_SIZE:
  case UPLOAD_ERR_FORM_SIZE:
    echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  case UPLOAD_ERR_PARTIAL:
  case UPLOAD_ERR_NO_FILE:
    echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }

  // ファイル保存用ディレクトリがなければ作成
  if (!is_dir("summary")) mkdir("summary", 0755);
  if (!is_dir("summary/divicon")) mkdir("summary/divicon", 0755);

  $icon_file_name1 = "summary/divicon/".$new_div_id.".gif";
  $icon_file_name2 = "summary/divicon/".$new_div_id.".jpg";
  $icon_file_name3 = "summary/divicon/".$new_div_id.".png";

  // 画像がある場合は削除
  if (is_file($icon_file_name1)) unlink($icon_file_name1);
  if (is_file($icon_file_name2)) unlink($icon_file_name2);
  if (is_file($icon_file_name3)) unlink($icon_file_name3);

  // アップロードされたファイルを保存
  $savefilename = "summary/divicon/".$new_div_id.".".$ext;
  $ret = copy($_FILES["div_icon_file"]["tmp_name"], $savefilename);

  if ($ret == false) {
    echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
    echo("<script language=\"javascript\">history.back();</script>");
    exit;
  }
}





$sql =
	" insert into divmst (".
	" div_id, diag_div, div_del_flg".
	",div_display_auth_flg, div_modify_auth_flg".
	",div_display_auth_stid_list, div_modify_auth_stid_list".
	",div_show_link_flowsheet_button".
	",div_show_link_ondoban_button".
	",div_show_icon".
	" ) values (".
	" ".$new_div_id .
	",'".pg_escape_string($div_name)."'".
	",'f'".
	",".(int)@$_REQUEST["div_display_auth_flg"].
	",".(int)@$_REQUEST["div_modify_auth_flg"].
	",'".pg_escape_string(@$_REQUEST["div_display_auth_stid_list"])."'".
	",'".pg_escape_string(@$_REQUEST["div_modify_auth_stid_list"])."'".
	",".(int)@$_REQUEST["div_show_link_flowsheet_button"].
	",".(int)@$_REQUEST["div_show_link_ondoban_button"].
	",".(int)@$_REQUEST["div_show_icon"];
//	")";
$in = insert_into_table($con,$sql,array(),$fname);	//診療区分情報を挿入
pg_close($con);											//ＤＢとのコネクションを閉じる

if($in == 0){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}else{
	echo("<script language=\"javascript\">location.href=\"./summary_kanri_kubun.php?session=$session\";</script>\n");
	exit;
}

?>