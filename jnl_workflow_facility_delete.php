<?
require("about_session.php");
require("about_authority.php");
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//XX require_once("community/mainfile.php");
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);

// トランザクションを開始
pg_query($con, "begin");
//XX mysql_query("start transaction", $con_mysql);

// チェックされた権限グループをループ
foreach ($group_ids as $tmp_group_id) {


	// 病院・施設情報を削除
	$sql = "delete from jnl_facility_master";
	$cond = "where jnl_facility_id = $tmp_group_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 関連している日報・月報入力所を削除
	$sql = "delete from jnl_facility_register ";
	$cond = "where jnl_facility_id='".$tmp_group_id."'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 関連している日報・月報入力所を削除
	if($facility_type == "1")
	{
		//病院データ

		$sql = "delete from jnl_hospital_bed ";
		$cond = "where jnl_facility_id='".$tmp_group_id."'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	else
	{
		//老健データ
		$sql = "delete from jnl_nurse_home_bed ";
		$cond = "where jnl_facility_id='".$tmp_group_id."'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 整合性のチェック
//auth_check_consistency($con, $con_mysql, $session, $fname);

// トランザクションをコミット
pg_query($con, "commit");
//XX mysql_query("commit", $con_mysql);

// データベース接続を閉じる
pg_close($con);
//XX mysql_close($con_mysql);

// 権限グループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'jnl_workflow_register_list.php?session=$session&facility_type=$facility_type';</script>");
?>
