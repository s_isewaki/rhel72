<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
if ($bedundec1 != "t") {$bedundec1 = "f";}
if ($bedundec1 != "t" && $bedundec2 != "t") {$bedundec2 = "f";}
if ($bedundec1 != "t" && $bedundec2 != "t" && $bedundec3 != "t") {$bedundec3 = "f";}
?>
<body>
<form name="items" method="post" action="<? echo($back_url); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="out_yr" value="<? echo($out_yr); ?>">
<input type="hidden" name="out_mon" value="<? echo($out_mon); ?>">
<input type="hidden" name="out_day" value="<? echo($out_day); ?>">
<input type="hidden" name="out_hr" value="<? echo($out_hr); ?>">
<input type="hidden" name="out_min" value="<? echo($out_min); ?>">
<input type="hidden" name="result" value="<? echo($result); ?>">
<input type="hidden" name="result_dtl" value="<? echo($result_dtl); ?>">
<input type="hidden" name="out_rsn_cd" value="<? echo($out_rsn_cd); ?>">
<input type="hidden" name="out_pos_cd" value="<? echo($out_pos_cd); ?>">
<input type="hidden" name="out_pos_dtl" value="<? echo($out_pos_dtl); ?>">
<input type="hidden" name="out_inst_cd" value="<? echo($out_inst_cd); ?>">
<input type="hidden" name="out_sect_cd" value="<? echo($out_sect_cd); ?>">
<input type="hidden" name="out_doctor_no" value="<? echo($out_doctor_no); ?>">
<input type="hidden" name="out_city" value="<? echo($out_city); ?>">
<input type="hidden" name="pr_inst_cd" value="<? echo($pr_inst_cd); ?>">
<input type="hidden" name="pr_sect_cd" value="<? echo($pr_sect_cd); ?>">
<input type="hidden" name="pr_doctor_no" value="<? echo($pr_doctor_no); ?>">
<input type="hidden" name="fd_end" value="<? echo($fd_end); ?>">
<input type="hidden" name="comment" value="<? echo($comment); ?>">
<input type="hidden" name="ward" value="<? echo($ward); ?>">
<input type="hidden" name="ptrm" value="<? echo($ptrm); ?>">
<input type="hidden" name="bed" value="<? echo($bed); ?>">
<input type="hidden" name="bedundec1" value="<? echo($bedundec1); ?>">
<input type="hidden" name="bedundec2" value="<? echo($bedundec2); ?>">
<input type="hidden" name="bedundec3" value="<? echo($bedundec3); ?>">
<input type="hidden" name="back_in_yr" value="<? echo($back_in_yr); ?>">
<input type="hidden" name="back_in_mon" value="<? echo($back_in_mon); ?>">
<input type="hidden" name="back_in_day" value="<? echo($back_in_day); ?>">
<input type="hidden" name="back_in_hr" value="<? echo($back_in_hr); ?>">
<input type="hidden" name="back_in_min" value="<? echo($back_in_min); ?>">
<input type="hidden" name="back_sect" value="<? echo($back_sect); ?>">
<input type="hidden" name="back_doc" value="<? echo($back_doc); ?>">
<input type="hidden" name="back_change_purpose" value="<? echo($back_change_purpose); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="action_label" value="<? echo($action_label); ?>">
<input type="hidden" name="pt_nm" value="<? echo($pt_nm); ?>">
<input type="hidden" name="search_sect" value="<? echo($search_sect); ?>">
<input type="hidden" name="search_doc" value="<? echo($search_doc); ?>">
</form>
<?
require("about_session.php");
require("about_authority.php");
require("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// チェック情報を取得
$sql = "select * from bedcheckout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 9; $i++) {
	$varname = "required$i";
	$$varname = pg_fetch_result($sel, 0, "required$i");
}

// 入力チェック
if (!checkdate($out_mon, $out_day, $out_yr)) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('日付が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required1 == "t" && $result == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('転帰を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if (strlen($result_dtl) > 50) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('転帰詳細が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required2 == "t" && $out_rsn_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院理由を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required3 == "t" && $out_pos_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院先区分を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if (strlen($out_pos_dtl) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院先区分の詳細が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required4 == "t" && $out_inst_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院先施設名を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required5 == "t" && $out_sect_cd == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('診療科を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required6 == "t" && $out_doctor_no == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('担当医を選択してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if (strlen($out_city) > 100) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院先市区町村が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required7 == "t" && $out_city == "") {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('退院先市区町村を入力してください。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($required9 == "t") {
	if ($pr_inst_cd == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('かかりつけ医の医療機関名を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($pr_sect_cd == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('かかりつけ医の診療科を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($pr_doctor_no == "") {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('かかりつけ医の担当医を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}
if (strlen($comment) > 200) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('コメントが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($back_in_yr == "") {$back_in_yr = "-";}
if ($back_in_mon == "") {$back_in_mon = "-";}
if ($back_in_day == "") {$back_in_day = "-";}
if ($back_in_hr == "") {$back_in_hr = "--";}
if ($back_in_min == "") {$back_in_min = "--";}
if (($back_in_yr == "-" && ($back_in_mon != "-" || $back_in_day != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_mon == "-" && ($back_in_yr != "-" || $back_in_day != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_day == "-" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_hr != "--" || $back_in_min != "--")) || ($back_in_hr == "--" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_day != "-" || $back_in_min != "--")) || ($back_in_min == "--" && ($back_in_yr != "-" || $back_in_mon != "-" || $back_in_day != "-" || $back_in_hr != "--"))) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('入院日時が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if ($back_in_yr != "-") {
	if (!checkdate($back_in_mon, $back_in_day, $back_in_yr)) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('入院日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}
if (strlen($back_change_purpose) > 200) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('コメントが長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 入力値の編集
$date = "$out_yr$out_mon$out_day";
$time = sprintf("%02d%02d", $out_hr, $out_min);

if ($ward == 0) {
	$bldg_cd = null;
	$ward_cd = null;
} else {
	list($bldg_cd, $ward_cd) = explode("-", $ward);
}
if ($ptrm == 0) {$ptrm = null;}
if ($bed == 0) {$bed = null;}
if ($back_in_yr == "-") {
	$in_date = null;
	$in_time = null;
} else {
	$in_date = sprintf("%04d%02d%02d", $back_in_yr, $back_in_mon, $back_in_day);
	$in_time = sprintf("%02d%02d", $back_in_hr, $back_in_min);
}
if ($back_sect == 0) {$back_sect = null;}
if ($back_doc == 0) {$back_doc = null;}

// 登録・変更の場合
if ($is_delete != 't') {

	// 入院状況レコードを削除
	$sql = "delete from inptcond";
	$cond = "where ptif_id = '$pt_id' and cond_check = '9' and hist_flg = 'f'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// 入院状況レコードを登録
	update_inpatient_condition($con, $pt_id, $date, $time, "9", $session, $fname, true);

	// ログインユーザの職員IDを取得
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");

	// 退院予定情報を取得
	$sql = "select inpt_out_res_dt, inpt_out_res_tm, out_res_updated, inpt_back_in_dt, inpt_back_in_tm, back_res_updated from inptmst";
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$org_out_res_dt = trim(pg_fetch_result($sel, 0, "inpt_out_res_dt"));
	$org_out_res_tm = trim(pg_fetch_result($sel, 0, "inpt_out_res_tm"));
	if (strcmp($date, $org_out_res_dt) <> 0 || strcmp($time, $org_out_res_tm) <> 0) {
		$out_res_updated = date("YmdHis");
	} else {
		$out_res_updated = pg_fetch_result($sel, 0, "out_res_updated");
	}
	$org_back_in_dt = trim(pg_fetch_result($sel, 0, "inpt_back_in_dt"));
	$org_back_in_tm = trim(pg_fetch_result($sel, 0, "inpt_back_in_tm"));
	if (strcmp($in_date, $org_back_in_dt) <> 0 || strcmp($in_time, $org_back_in_tm) <> 0) {
		$back_res_updated = date("YmdHis");
	} else {
		$back_res_updated = pg_fetch_result($sel, 0, "back_res_updated");
	}

	// 入院患者情報を更新
	if ($out_sect_rireki == "") {$out_sect_rireki = null;}
	if ($out_doctor_no == "") {$out_doctor_no = null;}
	if ($pr_sect_rireki == "") {$pr_sect_rireki = null;}
	if ($pr_doctor_no == "") {$pr_doctor_no = null;}
	$sql = "update inptmst set";
	$set = array("inpt_in_flg", "inpt_out_res_dt", "inpt_out_res_flg", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_out_res_tm", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_out_rsn_rireki", "inpt_out_rsn_cd", "inpt_pr_inst_cd", "inpt_pr_sect_rireki", "inpt_pr_sect_cd", "inpt_pr_doctor_no", "out_res_updated", "back_res_updated");
	$setvalue = array("f", $date, "t", date("Ymd"), date("Hi"), $emp_id, $time, $result, $result_dtl, $out_pos_rireki, $out_pos_cd, $out_pos_dtl, $fd_end, $comment, $out_inst_cd, $out_sect_rireki, $out_sect_cd, $out_doctor_no, $out_city, $bldg_cd, $ward_cd, $ptrm, $bed, $in_date, $in_time, $back_sect, $back_doc, $back_change_purpose, $out_rsn_rireki, $out_rsn_cd, $pr_inst_cd, $pr_sect_rireki, $pr_sect_cd, $pr_doctor_no, $out_res_updated, $back_res_updated);
	$cond = "where ptif_id = '$pt_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

// 取消の場合
} else {

	// 「在院」に戻す
	update_inpatient_condition($con, $pt_id, $date, null, "8", $session, $fname, true);
}
// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面遷移
echo("<script type=\"text/javascript\">");
if ($back_url == "outpatient_reserve_detail.php") {
	if ($is_delete != 't') {
		echo("location.href = 'outpatient_reserve_detail.php?session=$session&pt_id=$pt_id';");
	} else {
		echo("location.href = 'inpatient_patient_detail.php?session=$session&pt_id=$pt_id&path=4';");
	}
} else if ($back_url == "bed_menu_inpatient_out_reserve.php") {
	if ($is_delete != 't') {
		echo("location.href = 'bed_menu_inpatient_out_reserve.php?session=$session&pt_id=$pt_id&pt_nm=" . urlencode($pt_nm) . "&search_sect=$search_sect&search_doc=$search_doc';");
	} else {
		echo("location.href = 'bed_menu_inpatient_patient.php?session=$session&pt_id=$pt_id&path=A&pt_nm=" . urlencode($pt_nm) . "&search_sect=$search_sect&search_doc=$search_doc';");
	}
} else {
	echo "opener.location.reload(); self.close();";
}
echo("</script>");
?>
</body>
