<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 登録情報を配列化
$categories = array();
$linkcate_ids = array();
for ($i = 0, $j = count($linkcate_nm); $i < $j; $i++) {
	if ($linkcate_nm[$i] == "") {
		continue;
	}

	$categories[] = array(
		"linkcate_id" => $linkcate_id[$i],
		"linkcate_nm" => $linkcate_nm[$i],
		"show_flg1" => $show_flg1[$i],
		"order1" => ($show_flg1[$i] == "t") ? max(intval($order1[$i]), 1) : null,
		"show_flg2" => $show_flg2[$i],
		"order2" => ($show_flg2[$i] == "t") ? max(intval($order2[$i]), 1) : null,
		"show_flg3" => $show_flg3[$i],
		"order3" => ($show_flg3[$i] == "t") ? max(intval($order3[$i]), 1) : null
	);

	if ($linkcate_id[$i] != "") {
		$linkcate_ids[] = $linkcate_id[$i];
	}
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションの開始
pg_query($con, "begin");

// 登録／更新対象カテゴリーに所属しないリンク情報を削除
$sql = "delete from link";
$cond = (count($linkcate_ids) > 0) ? "where linkcate_id not in (" . implode(",", $linkcate_ids) . ")" : "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 登録／更新対象カテゴリーに所属しないカテゴリー情報を削除
$sql = "delete from linkcate";
$cond = (count($linkcate_ids) > 0) ? "where linkcate_id not in (" . implode(",", $linkcate_ids) . ")" : "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// カテゴリー情報を登録
foreach ($categories as $category) {

	// カテゴリーID未採番なら登録処理を実行
	if ($category["linkcate_id"] == "") {
		$sql = "select max(linkcate_id) from linkcate";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$category["linkcate_id"] = intval(pg_fetch_result($sel, 0, 0)) + 1;

		$sql = "insert into linkcate (linkcate_id, linkcate_nm, show_flg1, order1, show_flg2, order2, show_flg3, order3) values (";
		$content = array($category["linkcate_id"], $category["linkcate_nm"], $category["show_flg1"], $category["order1"], $category["show_flg2"], $category["order2"], $category["show_flg3"], $category["order3"]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

	// カテゴリーID採番済みなら更新処理を実行
	} else {
		$sql = "update linkcate set";
		$set = array("linkcate_nm", "show_flg1", "order1", "show_flg2", "order2", "show_flg3", "order3");
		$setvalue = array($category["linkcate_nm"], $category["show_flg1"], $category["order1"], $category["show_flg2"], $category["order2"], $category["show_flg3"], $category["order3"]);
		$cond = "where linkcate_id = {$category["linkcate_id"]}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// カテゴリー表示設定画面を再表示
echo("<script type=\"text/javascript\">location.href = 'link_admin_menu.php?session=$session';</script>");
?>
