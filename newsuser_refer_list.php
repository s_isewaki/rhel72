<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ管理 | 参照者一覧</title>
<?
//ini_set("display_errors","1");
require_once("about_comedix.php");
require("show_newsuser_refer_list.ini");
require("news_common.ini");
require("get_values.ini");
$fname = $PHP_SELF;
// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// 権限のチェック
$check_auth = check_authority($session, 46, $fname);
if ($check_auth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// お知らせ管理権限の取得
$news_admin_auth = check_authority($session, 24, $fname);
// 処理モードの設定
define("LIST_REFERRED", 1);  // 確認済みリストを表示
define("LIST_UNREFERRED", 2);  // 未確認リストを表示
define("LIST_ALL", 4);  // 対象者リストを表示
define("LIST_UNREAD", 5);  // 未読リストを表示
define("LIST_READ", 6);  // 既読リストを表示
define("LIST_READ_UNREFERRED", 7);  // 既読・未確認リストを表示
// ページ番号の設定
if ($refer_page == "") {$refer_page = 1;}
// ソート順の初期設定 3:職員名昇順
// ソート順  1:職員ID昇順 2:職員ID降順 3:職員名昇順 4:職員名降順 5:参照時間昇順 6:参照時間降順（DB項目はコメント更新日時） 7:所属昇順 8:所属降順 9:役職昇順 10:役職降順
if ($sort_id == "") { $sort_id = "3"; }
// データベースに接続
$con = connect2db($fname);
// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);
// 参照者一覧の表示件数設定を取得
$refer_rows_per_page = get_refer_rows_per_page($con, $fname);
// お知らせ情報の取得
$sql = "select news_title, news_add_category, news.job_id as job_id1, news.st_id_cnt as st_id_cnt1, record_flg, newscate.standard_cate_id, newscate.job_id as job_id2, newscate.st_id_cnt as st_id_cnt2, newscate.newscate_id, newscate.newscate_name, news.public_flg, news.emp_id, news.read_control from news inner join newscate on news.news_add_category = newscate.newscate_id";
$cond = "where news_id = $news_id";
$sel_news = select_from_table($con, $sql, $cond, $fname);
if ($sel_news == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$news_title = pg_fetch_result($sel_news, 0, "news_title");
$news_category = pg_fetch_result($sel_news, 0, "news_add_category");
$standard_cate_id = pg_fetch_result($sel_news, 0, "standard_cate_id");
$newscate_id = pg_fetch_result($sel_news, 0, "newscate_id");
$record_flg = pg_fetch_result($sel_news, 0, "record_flg");
$public_flg = pg_fetch_result($sel_news, 0, "public_flg");
$reg_emp_id = pg_fetch_result($sel_news, 0, "emp_id");
$read_control = pg_fetch_result($sel_news, 0, "read_control");
// 標準カテゴリの場合
if ($news_category <= "3") {
    $tmp_news_category = $news_category;
    $st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt1");
    $job_id = pg_fetch_result($sel_news, 0, "job_id1");
} else if ($news_category == "10000") {
// 回覧板
    $tmp_news_category = $news_category;
} else {
// 追加カテゴリの場合
    $tmp_news_category = $standard_cate_id;
    $st_id_cnt = pg_fetch_result($sel_news, 0, "st_id_cnt2");
    $job_id = pg_fetch_result($sel_news, 0, "job_id2");
}
// 役職指定数がある場合条件追加
// 役職ID
$st_ids = "";
$st_cond = "";
if ($st_id_cnt > 0) {
    if ($news_category <= "3") {
        $newsst_info = get_newsst_info($con, $fname, $news_id);
    } else {
        $newsst_info = get_newscatest_info($con, $fname, $newscate_id);
    }
    foreach ($newsst_info as $st_id => $st_nm) {
        if ($st_ids != "") {
            $st_ids .= ",";
        }
        $st_ids .= $st_id;
    }
    // 役職条件
    $st_cond .= " and (empmst.emp_st in ($st_ids) or exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_st in ($st_ids)))";
}
//モードの初期値
if ($mode == "") {
    $mode = ($record_flg == 't') ? LIST_REFERRED : LIST_READ;
}
// お知らせ対象者数を取得
switch ($tmp_news_category) {
case "1":  // 全館
    $sql = "select emp_id from empmst";
    $cond = "where exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id and authmst.emp_newsuser_flg = 't')";
    // 役職条件追加
    if ($st_id_cnt > 0) {
        $cond .= $st_cond;
    }
    $sel_count_all = select_from_table($con, $sql, $cond, $fname);
    if ($sel_count_all == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    break;
case "2":  // 部門
    $sql = "select emp_id from empmst";
    // 標準カテゴリの場合
    if ($news_category <= "3") {
        $cond = "where exists (select * from newsdept where newsdept.news_id = $news_id and ((newsdept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newsdept.class_id))) and ((newsdept.atrb_id = empmst.emp_attribute) or (newsdept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newsdept.atrb_id))) and ((newsdept.dept_id = empmst.emp_dept) or (newsdept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newsdept.dept_id)))) and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id and authmst.emp_newsuser_flg = 't')";
    } else {
    // 追加カテゴリの場合
        $cond = "where exists (select * from newscatedept where newscatedept.newscate_id = $newscate_id and ((newscatedept.class_id = empmst.emp_class) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_class = newscatedept.class_id))) and ((newscatedept.atrb_id = empmst.emp_attribute) or (newscatedept.atrb_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_attribute = newscatedept.atrb_id))) and ((newscatedept.dept_id = empmst.emp_dept) or (newscatedept.dept_id is null) or (exists (select * from concurrent where concurrent.emp_id = empmst.emp_id and concurrent.emp_dept = newscatedept.dept_id)))) and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id and authmst.emp_newsuser_flg = 't')";
    }
    // 役職条件追加
    if ($st_id_cnt > 0) {
        $cond .= $st_cond;
    }
    $sel_count_all = select_from_table($con, $sql, $cond, $fname);
    if ($sel_count_all == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    break;
case "3":  // 職種
    $sql = "select emp_id from empmst";
    // 職種が1件の場合
    if ($job_id != "") {
        $cond = "where emp_job = $job_id"." and authmst.emp_newsuser_flg = 't'";
    } else {
        // 標準カテゴリの場合
        if ($news_category <= "3") {
            $cond = "where exists (select * from newsjob where newsjob.news_id = $news_id and empmst.emp_job = newsjob.job_id)";
        } else {
            $cond = "where exists (select * from newscatejob where newscatejob.newscate_id = $newscate_id and empmst.emp_job = newscatejob.job_id)";
        }
    }
    $cond .= " and exists (select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_id = empmst.emp_id and authmst.emp_newsuser_flg = 't')";
    // 役職条件追加
    if ($st_id_cnt > 0) {
        $cond .= $st_cond;
    }
    $sel_count_all = select_from_table($con, $sql, $cond, $fname);
    if ($sel_count_all == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    break;
case "10000":  // 回覧板
    $sql = "select emp_id from newscomment";
    $cond = "where news_id = $news_id";
    $cond .= " and exists(select * from authmst where authmst.emp_del_flg = 'f' and authmst.emp_newsuser_flg = 't' and authmst.emp_id = newscomment.emp_id)";
    $sel_count_all = select_from_table($con, $sql, $cond, $fname);
    if ($sel_count_all == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    break;
}
$target_emps = result_emps($sel_count_all);
$count_all = count($target_emps);

// 確認済み人数を取得
$sql = "select emp_id from newsref";
$cond = "where news_id = $news_id and ref_time is not null";
$sel_count_referred = select_from_table($con, $sql, $cond, $fname);
if ($sel_count_referred == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$referred_emps = result_emps($sel_count_referred);
$count_referred = count($referred_emps);

// 未確認人数を算出
$count_unreferred = count(array_diff($target_emps, $referred_emps));

// 既読人数を取得
$sql = "select emp_id from newsref";
$cond = "where news_id = $news_id and read_flg='t'";
$sel_count_read = select_from_table($con, $sql, $cond, $fname);
if ($sel_count_read == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$read_emps = result_emps($sel_count_read);
$count_read = count($read_emps);

// 未読人数を算出
$count_unread = count(array_diff($target_emps, $read_emps));

// 既読・未確認人数を取得
$sql = "select count(*) from newsref";
$cond = "where news_id = $news_id and read_flg='t' and ref_time is null";
$sel_count_read_unreferred = select_from_table($con, $sql, $cond, $fname);
if ($sel_count_read_unreferred == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$count_read_unreferred = pg_fetch_result($sel_count_read_unreferred, 0, 0);

// 回答設定を取得
$qa_cnt = 0;
$question = array();
$select_num = array();
$multi_flg = array();
$answer = array();
$arr_answer_cnt = array();
$free_format_cnt = array();
$type = array();
if ($record_flg == "t") {
    $sql = "select question, select_num, multi_flg,type from newsenquet_q";
    $cond = "where news_id = $news_id order by question_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $i = 1;
    while ($row = pg_fetch_array($sel)) {
        $question[$i] = $row["question"];
        $select_num[$i] = $row["select_num"];
        $multi_flg[$i] = $row["multi_flg"];
        $type[$i] = $row["type"];
        $qa_cnt++;
        $i++;
    }
    for ($i=1; $i<=$qa_cnt; $i++) {
        $sql = "select answer from newsenquet_a";
        $cond = "where news_id = $news_id and question_no = $i order by item_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $j = 1;
        while ($row = pg_fetch_array($sel)) {
            $answer[$i][$j] = $row["answer"];
            $j++;
        }
    }
    // 回答数取得
    if ($qa_cnt > 0) {
        $sql = "select question_no, item_no, count(*) as cnt from newsanswer";
        $cond = "where news_id = $news_id group by question_no, item_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $arr_answer_cnt[$row["question_no"]][$row["item_no"]] = $row["cnt"];
        }
        
        $sql = "select question_no, item_no, count(*) as free_cnt from newsanswer";
        $cond = "where news_id = $news_id and free_format !='' group by question_no, item_no";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        while ($row = pg_fetch_array($sel)) {
            $free_format_cnt[$row["question_no"]][$row["item_no"]] = $row["free_cnt"];
        }
        
        
    }
}
// 一般ユーザ登録許可フラグを取得
$everyone_flg = get_everyone_flg($con, $fname);
// 職員IDのカラム表示条件の初期値を取得
$default_emp_id_chk     = get_emp_id_read_status($con, $fname);
// 職員組織のカラム表示条件の初期値を取得
$default_emp_class_chk  = get_emp_class_read_status($con, $fname);
// 職員職種のカラム表示条件の初期値を取得
$default_emp_job_chk        = get_emp_job_read_status($con, $fname);
// 職員役職のカラム表示条件の初期値を取得 20130218 中嶌追記
$default_emp_st_chk        = get_emp_st_read_status($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--
function downloadCSV(csv_flg) {
    document.csv.action = 'news_refer_csv.php?admin_flg=0';
    document.csv.csv_flg.value = csv_flg;
    document.csv.submit();
}
//-->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="newsuser_menu.php?session=<? echo($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="newsuser_menu.php?session=<? echo($session); ?>"><b>お知らせ・回覧板</b></a></font></td>
<? if ($news_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="news_menu.php?session=<? echo($session); ?>&news=1"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="<?
/*
$frompage "1":全館 "2":部署 "3":職種 "4":回覧 "5":自己登録分
*/
switch ($frompage) {
case "1":
    echo("newsuser_menu.php");
    break;
// 部署、職種、回覧板も同じモジュールで表示するよう変更 2008/08/20
//case "2":
//  echo("newsuser_class.php");
//  break;
//case "3":
//  echo("newsuser_job.php");
//  break;
//case "4":
//  echo("newsuser_circular.php");
//  break;
default:
    echo("newsuser_list.php");
}
?>?session=<? echo($session); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="newsuser_refer_list.php?session=<? echo($session); ?>&news_id=<? echo($news_id); ?>&page=<? echo($page); ?><?
if ($frompage != "") {
    echo("&frompage=$frompage");
}
?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>参照者一覧</b></font></a></td>
<? if ($everyone_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="newsuser_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="720" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
<td colspan="<? echo ($record_flg == 't') ? 11 : 5; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(h($news_title)); ?></font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
if ($news_category == 10000) {
    echo("回覧");
} else {
    echo("お知らせ");
}
?>対象者数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_ALL) {
    echo("<b>{$count_all}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_ALL. "&page=$page&frompage=$frompage\">{$count_all}名</a>");
}
?>
</font></td>
<? if ($record_flg == 't') {?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未確認人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_UNREFERRED) {
    echo("<b>{$count_unreferred}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_UNREFERRED. "&page=$page&frompage=$frompage\">{$count_unreferred}名</a>");
}
?>
</font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認済み人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_REFERRED) {
    echo("<b>{$count_referred}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_REFERRED. "&page=$page&frompage=$frompage\">{$count_referred}名</a>");
}
?>
</font></td>
<? } ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">未読人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_UNREAD) {
    echo("<b>{$count_unread}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_UNREAD. "&page=$page&frompage=$frompage\">{$count_unread}名</a>");
}
?>
</font></td>
<? if ($record_flg == 't') {?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">既読・未確認人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_READ_UNREFERRED) {
    echo("<b>{$count_read_unreferred}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_READ_UNREFERRED. "&page=$page&frompage=$frompage\">{$count_read_unreferred}名</a>");
}
?>
</font></td>
<? } ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">既読人数</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($mode == LIST_READ) {
    echo("<b>{$count_read}名</b>");
}
else {
    echo("<a href=\"newsuser_refer_list.php?session=$session&news_id=$news_id&mode=". LIST_READ. "&page=$page&frompage=$frompage\">{$count_read}名</a>");
}
?>
</font></td>
</tr>
</table>
<?
// 督促メール
if (($mode == LIST_UNREFERRED OR $mode == LIST_UNREAD OR $mode == LIST_READ_UNREFERRED) && $emp_id == $reg_emp_id) {
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="mainform" action="news_mail.php" method="post" onsubmit="return confirm('送信してもよろしいですか？');">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="news_id" value="<? echo($news_id); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="path" value="2">
<table width="50%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td bgcolor="#f6f9ff" colspan="2" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font>&nbsp;&nbsp;<input type="submit" value="督促メール送信">
</td>
</tr>
<tr height="22">
<td valign="top" width="50">
<select name="limit">
<option value="今日中に、">今日中に、</option>
<option value="明日までに、">明日までに、</option>
<option value=""></option>
</select>
</td>
<td>
<textarea name="send_message" cols="70" rows="5">
必ず確認してください。</textarea>
</td>
</tr>
</table>
</form>
<?
}
?>
<?
// 選択肢の集計表
if ($record_flg == "t" && $qa_cnt > 0) {
    // 登録者のみダウンロード可
    if ($emp_id == $reg_emp_id) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="left">
<input type="button" value="CSVダウンロード（集計）" onclick="downloadCSV(1);">
<input type="button" value="CSVダウンロード（明細）" onclick="downloadCSV(2);">
</td>
</tr>
</table>
<form name="csv" method="get" target="download">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="news_id" value="<? echo($news_id); ?>">
<input type="hidden" name="csv_flg" value="">
<input type="hidden" name="admin_flg" value="<? echo('0'); ?>">
</form>
<iframe name="download" width="0" height="0" frameborder="0"></iframe>
<?
    }
    // 公開の場合か、登録者の場合に表示
    if ($public_flg == "t" || $emp_id == $reg_emp_id) {
    for ($i=1; $i<=$qa_cnt; $i++) {
        $width = 30 + 7 * $select_num[$i];
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">質問<? echo("{$i} ".h($question[$i])); ?></font><br>
<table width="<?=$width?>%" border="0" cellspacing="0" cellpadding="2" class="list">

<? if($type[$i]==0){ ?>    
<tr height="22">
<td align="center" bgcolor="#f6f9ff" width="50"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">項番</font></td>
<?
for ($j=1; $j<=$select_num[$i]; $j++) {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$j</font></td>\n");
}
?>
</tr>
<tr height="22">
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">内容</font></td>
<?
for ($j=1; $j<=$select_num[$i]; $j++) {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".h($answer[$i][$j])."</font></td>\n");
}
?>
</tr>
<? } ?>
<tr height="22">
<td align="center" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">人数</font></td>
<?
if($type[$i]==0){
for ($j=1; $j<=$select_num[$i]; $j++) {
    // 職員一覧
    if ($arr_answer_cnt[$i][$j] == "") {
        $arr_answer_cnt[$i][$j] = "0";
    }
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    // 登録者の場合にリンクを表示
    if ($arr_answer_cnt[$i][$j] != "0" && $emp_id == $reg_emp_id) {
        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('news_answer_list.php?session=$session&news_id=$news_id&question_no=$i&item_no=$j&page=1&admin_flg=0', 'newwin', 'width=730,height=700,scrollbars=yes')\">&nbsp;{$arr_answer_cnt[$i][$j]}&nbsp;</a>\n");
    } else {
        echo($arr_answer_cnt[$i][$j]);
    }
    echo("</font></td>\n");
}
}else if($type[$i]==1){
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
    // 登録者の場合にリンクを表示
    if ($free_format_cnt[$i]['1'] != "0" && $emp_id == $reg_emp_id) {
        echo("<a href=\"javascript:void(0);\" onclick=\"window.open('news_answer_list.php?session=$session&news_id=$news_id&question_no=$i&item_no=1&page=1&admin_flg=0', 'newwin', 'width=730,height=700,scrollbars=yes')\">&nbsp;{$free_format_cnt[$i]['1']}&nbsp;</a>\n");
    } else {
        echo($free_format_cnt[$i]['1']);
    }
    echo("</font></td>\n");
}
?>
</tr>
</table>
<?
    }
    } // 公開、登録者
}
?>
<?
// ソート用のURL設定
$url = "newsuser_refer_list.php?session=$session&news_id=$news_id&mode=$mode&frompage=$frompage&sort_id=";
$tmp_sort_id = ($sort_id == "1") ? "2" : "1";
$emp_id_url = $url.$tmp_sort_id;
$tmp_sort_id = ($sort_id == "3") ? "4" : "3";
$emp_name_url = $url.$tmp_sort_id;
$tmp_sort_id = ($sort_id == "5") ? "6" : "5";
$refer_time_url = $url.$tmp_sort_id;
$tmp_sort_id = ($sort_id == "7") ? "8" : "7";
$position_url = $url.$tmp_sort_id;
$tmp_sort_id = ($sort_id == "9") ? "10" : "9";
$status_url = $url.$tmp_sort_id;
$tmp_sort_id = ($sort_id == "11") ? "12" : "11";
$job_url = $url.$tmp_sort_id;
// 矢印設定
$updown_gif1 = "";
$updown_gif2 = "";
$updown_gif3 = "";
switch ($sort_id) {
    case "1":
    case "2":
        $gif_name = ($sort_id == "1") ? "up.gif" : "down.gif";
        $updown_gif1 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
    case "3":
    case "4":
        $gif_name = ($sort_id == "3") ? "up.gif" : "down.gif";
        $updown_gif2 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
    case "5":
    case "6":
        $gif_name = ($sort_id == "5") ? "up.gif" : "down.gif";
        $updown_gif3 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
    case "7":
    case "8":
        $gif_name = ($sort_id == "7") ? "up.gif" : "down.gif";
        $updown_gif4 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
    case "9":
    case "10":
        $gif_name = ($sort_id == "9") ? "up.gif" : "down.gif";
        $updown_gif5 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
    case "11":
    case "12":
        $gif_name = ($sort_id == "11") ? "up.gif" : "down.gif";
        $updown_gif6 = "<img src=\"img/$gif_name\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">";
        break;
}
?>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
<? if ($default_emp_id_chk != 'f') { ?>
<td><a href="<?=$emp_id_url?>"><?=$updown_gif1?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID</font></a></td>
<? } ?>
<td><a href="<?=$emp_name_url?>"><?=$updown_gif2?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員氏名</font></a></td>
<? if ($default_emp_class_chk != 'f') { ?>
<td><a href="<?=$position_url?>"><?=$updown_gif4?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属</font></a></td>
<? } ?>
<? if ($default_emp_st_chk != 'f') { ?>
<td><a href="<?=$status_url?>"><?=$updown_gif5?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></a></td>
<? } ?>



<?php 
if ($default_emp_job_chk != 'f') {
?>
    <td>
        <a href="<?=$job_url?>">
            <?=$updown_gif6?>
            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                職種
            </font>
        </a>
    </td>
<?php
}
?>


<? if ($mode == LIST_UNREFERRED OR $mode == LIST_UNREAD OR $mode == LIST_READ_UNREFERRED) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">督促メール送信日時</font></td>
<? } ?>
<? if ($mode == LIST_ALL) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認</font></td>
<? } ?>
<? if ($record_flg == "t" and ($mode == LIST_ALL || $mode == LIST_REFERRED)) { ?>
<td><a href="<?=$refer_time_url?>"><?=$updown_gif3?><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">確認日時</font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コメント</font></td>
<? } ?>
</tr>
<? show_news_refer_list($con, $news_id, $mode, $news_category, $job_id, $refer_page, $session, $fname, $st_cond, $standard_cate_id, $newscate_id, $sort_id, $refer_rows_per_page); ?>
</table>
<?
switch ($mode) {
    case LIST_ALL:
        $count = $count_all;
        break;
    case LIST_REFERRED:
        $count = $count_referred;
        break;
    case LIST_UNREFERRED:
        $count = $count_unreferred;
        break;
    case LIST_UNREAD:
        $count = $count_unread;
        break;
    case LIST_READ:
        $count = $count_read;
        break;
    case LIST_READ_UNREFERRED:
        $count = $count_read_unreferred;
        break;
}
show_news_refer_page_list($con, $count, $news_id, $mode, $refer_page, $page, $session, $fname, $frompage, $sort_id, $refer_rows_per_page);
?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function result_emps($sel) {
	if (pg_num_rows($sel) == 0) return array();

	$emps = array();
	while ($row = pg_fetch_array($sel)) {
		$emps[] = $row["emp_id"];
	}
	return $emps;
}
