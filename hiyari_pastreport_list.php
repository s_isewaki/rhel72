<?php

require_once('Cmx.php');
require_once('Cmx/View/Smarty.php');
require_once("hiyari_header_class.php");
require_once("hiyari_db_class.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("get_values.php");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if ( $session == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$summary = check_authority($session, 47, $fname);
if ( $summary == "0" ) {
    showLoginPage();
    exit;
}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if ( $con == "0" ) {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
// DBクラスのインスタンス化
$db = new hiyari_db_class($con, $fname);

$callerpage = $_GET["callerpage"];
//==================================================
//オプション
//==================================================
$sysConf = new Cmx_SystemConfig();

/*
// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

//権限ユーザーフラグ
$is_inci_auth_emp_flg = is_inci_auth_emp($session,$fname,$con);

//更新権限(範囲無視)
$is_report_db_update_able = is_report_db_update_able($session,$fname,$con);
*/

$smarty = new Cmx_View();


$report_id = $_GET["report_id"];

//$smarty->assign("emd_id",$emd_id);
//ユーザーID取得
//==============================
$registrant_id = $_GET["registrant_id"];

//$registrant_id = get_emp_id($con,$session,$fname);
$personal_report_all = get_report($con,$registrant_id,$fname,$sort_item,$sort_div);

//ユーザーID取得
//==============================
$mail_id = $_GET["mail_id"];


//==============================
//Excelリスト出力用フォルダー内レポート一覧情報
//==============================

$excel_target_report_id_list = "";
$report_list = array();
$anonymous_name = "匿名";
$mouseover_popup_flg = is_anonymous_readable($session, $con, $fname);
$smarty->assign('is_mouseover_popup', $mouseover_popup_flg == "t");
$profile_use = get_inci_profile($fname, $con);

if ($personal_report_all ) {
    foreach ($personal_report_all as $i => $list_data ) {
        $report_list[$i]['report_no'] = $list_data["report_no"];
        $report_list[$i]['report_title'] = h($list_data["report_title"]);
        $report_list[$i]['report_id'] = $list_data["report_id"];

        //報告部署
        $class_nm = "";
        if($profile_use["class_flg"] == "t" && $list_data["registrant_class"] != ""){
            $class_nm .= get_class_nm($con,$list_data["registrant_class"],$fname);
        }
        if($profile_use["attribute_flg"] == "t" && $list_data["registrant_attribute"] != ""){
            $class_nm .= get_atrb_nm($con,$list_data["registrant_attribute"],$fname);
        }
        if($profile_use["dept_flg"] == "t" && $list_data["registrant_dept"] != ""){
            $class_nm .= get_dept_nm($con,$list_data["registrant_dept"],$fname);
        }
        if($profile_use["room_flg"] == "t" && $list_data["registrant_room"] != ""){
            $class_nm .= get_room_nm($con,$list_data["registrant_room"],$fname);
         }
        $report_list[$i]['class_nm'] = $class_nm;
        
        //報告者
        $create_emp_name =  $list_data["registrant_name"];
        $report_list[$i]['real_name'] = h($create_emp_name);

        //表示名
        if($list_data["anonymous_report_flg"] == "t"){
            $report_list[$i]['is_anonymous_report'] = true;
            $disp_emp_name = $anonymous_name;
        }else{
            $report_list[$i]['is_anonymous_report'] = false;
            $disp_emp_name = $create_emp_name;
        }
        $report_list[$i]['disp_emp_name'] = h($disp_emp_name);
    
        $report_list[$i]['send_time'] =  $list_data["send_time"];
     
        $tmp_report_id =  $list_data["report_id"];
        if ( $excel_target_report_id_list != "" ) {
            $excel_target_report_id_list .= ",";
        }
        $excel_target_report_id_list .= $tmp_report_id;
    }
    $smarty->assign("excel_target_report_id_list",$excel_target_report_id_list);
}

$smarty->assign("original_report_id",$_GET["report_id"]);
$smarty->assign("session",$session);

$smarty->assign("report_list",$report_list);


$smarty->assign("personal_report_all",$personal_report_all);
$smarty->assign("sort_item", $sort_item);
$smarty->assign("sort_div", $sort_div);
$smarty->assign("mail_id", $mail_id);
$smarty->assign("callerpage", $callerpage);



$smarty->display("hiyari_pastreport_list2.tpl");



function get_report($con,$registrant_id,$fname,$sort_item,$sort_div) {
    
    $sql = "select ir.report_id,ir.report_no,ir.first_send_time, ir.report_title,ir.registrant_id ,ir.registrant_name ,ir.registrant_class,ir.registrant_attribute,ir.registrant_dept,ir.registrant_room,ir.anonymous_report_flg,am.atrb_nm from inci_report ir";
    $sql .= " left join atrbmst am on am.atrb_id = ir.registrant_attribute ";
    $cond = "where registrant_id ='$registrant_id'";
    $cond .= " AND ir.shitagaki_flg = false AND ir.del_flag = false ";
   
    if($sort_div == "0"){
	$sort = "asc";
    }else{
	$sort = "desc";
    }	
    $order = "order by ir.report_id desc";
    switch($sort_item)
    {
		case "REPORT_NO":
			$order = "order by ir.report_no $sort";
			break;
		case "REPORT_TITLE":
			$order = "order by ir.report_title $sort, ir.report_id desc";
			break;
		case "INCIDENT_DATE":
			$order = "order by ir.first_send_time $sort, report_id desc";
			break;
		default:
			//何もしない。
			break;
	}
	$cond .= " $order";

 
    $sel = select_from_table($con,$sql,$cond,$fname);
    if($sel == 0){
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $tmp_array = array();
    $i=0;
    while($row = pg_fetch_array($sel)){
            $first_send_time = sprintf("%s/%s/%s", substr($row["first_send_time"], 0, 4), 
                                                            substr($row["first_send_time"], 4, 2), 
                                                            substr($row["first_send_time"], 6, 2));
 
             $tmp_array[]= $row;
             $tmp_array[$i]["send_time"] = $first_send_time;
             $i++;
    }

    return  $tmp_array; 
}



?>