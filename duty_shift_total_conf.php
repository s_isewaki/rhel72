<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務シフト作成｜管理｜勤務集計表設定</title>

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common.ini");
require_once("duty_shift_manage_tab_common.ini");
require_once("duty_shift_common_class.php");
require_once("duty_shift_total_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);

$obj_total = new duty_shift_total_common_class($con, $fname);
///-----------------------------------------------------------------------------
// 権限のチェック
///-----------------------------------------------------------------------------
//管理画面用
$chk_flg = $obj->check_authority_Management($session, $fname);
if ($chk_flg == "") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//ユーザ画面用
$section_admin_auth = $obj->check_authority_user($session, $fname);
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------

// 勤務パターン情報を取得
$wktmgrp_array = $obj->get_wktmgrp_array();
if (count($wktmgrp_array) <= 0) {
	$err_msg_1 = "勤務パターングループ情報(wktmgrp)が未設定です。管理者に連絡してください。";
}
///-----------------------------------------------------------------------------
// グループ情報を取得
///-----------------------------------------------------------------------------
$group_array = $obj->get_duty_shift_group_array("", $data_emp, $data_wktmgrp);
if (count($group_array) <= 0) {
	$err_msg_1 = "勤務シフトグループ情報が未設定です。シフトグループ登録画面で登録してください";
}

if ($group_id == "") {
	$pattern_id = $group_array[0]["pattern_id"];	
	$group_id = $group_array[0]["group_id"];	
} else {
	for ($i=0; $i<count($group_array); $i++) {
		if ($group_id == $group_array[$i]["group_id"]) {
			$pattern_id = $group_array[$i]["pattern_id"];	
			break;
		}
	}
}

//ＤＢ(atdptn)より出勤パターン情報取得
$atdptn_array = $obj->get_atdptn_array($pattern_id);


$data_cnt = count($atdptn_array);

if ($data_cnt <= 0) {
	$err_msg_1 = "勤務シフト記号情報が未設定です。勤務シフト記号登録画面で登録してください。";
}
// 手当一覧を取得
$sql = "select allow_id, allow_contents, allow_price from tmcdallow";
$cond = "where del_flg = 'f' order by allow_id asc";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$arr_allow = array();
$allow_max = 0;
if (pg_num_rows($sel) > 0) {
	$i = 0;
	while ($row = pg_fetch_array($sel)) {
		$tmp_allow_id = $row["allow_id"];
		$tmp_allow_contents = $row["allow_contents"];
		$tmp_allow_price = $row["allow_price"];
		$arr_allow[$i] = $row;
		$allow_max = $row["allow_id"];
		$i++;
	}
}
//$allow_cnt = count($arr_allow);

$total_conf = $obj_total->get_duty_shift_total_config($group_id);
$normal_overtime_flg = $total_conf["normal_overtime_flg"];
$delay_minute = $total_conf["delay_minute"];
$irrg_month_time = $total_conf["irrg_month_time"];
$parttime_split_hour = $total_conf["parttime_split_hour"];
$hol_late_night_disp_flg = $total_conf["hol_late_night_disp_flg"];
$adjust_time_legal_hol_flg = $total_conf["adjust_time_legal_hol_flg"];

$ptn_full = $obj_total->get_duty_shift_total_ptn($group_id, 1);
$tra_full = $obj_total->get_duty_shift_total_tra_ptn($group_id);

$ptn_part = $obj_total->get_duty_shift_total_ptn($group_id, 2);
$tra_part = $obj_total->get_duty_shift_total_ptn($group_id, 3);
//手当情報
$allow_full = $obj_total->get_duty_shift_total_allow($group_id, 1);
$allow_part = $obj_total->get_duty_shift_total_allow($group_id, 2);
//休日出勤時間数除外パターン
$hol_except_ptn = $obj_total->get_duty_shift_hol_except_ptn($group_id);


?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>

<?
//デバック
//print_r($pattern_array);
?>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 画面遷移／タブ -->
	<!-- ------------------------------------------------------------------------ -->
<?
// 画面遷移
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_manage_title($session, $section_admin_auth);			//duty_shift_common.ini
echo("</table>\n");

// タブ
$arr_option = "";
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
show_manage_tab_menuitem($session, $fname, $arr_option);	//duty_shift_manage_tab_common.ini
echo("</table>\n");

// 下線
echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor=\"#5279a5\"><img src=\"img/spacer.gif\" width\"1\" height=\"2\" alt=\"\"></td></tr></table><img src=\"img/spacer.gif\" width=\"1\" height=\"5\" alt=\"\"><br>\n");
	?>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr height="22" >
		<td ><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<a href="duty_shift_entity_allotment.php?session=<?=$session?>">勤務分担表</a>&nbsp;&nbsp;
		勤務集計表&nbsp;&nbsp;
		</font></td>
		</tr>
		</table>
	<!-- ------------------------------------------------------------------------ -->
	<!-- データ不備時 -->
	<!-- ------------------------------------------------------------------------ -->
<?
if ($err_msg_1 != "") {
	echo($err_msg_1);
	echo("<br>\n");
} else {
	?>
	<!-- ------------------------------------------------------------------------ -->
	<!-- 入力エリア -->
	<!-- ------------------------------------------------------------------------ -->
	<form name="sign" action="duty_shift_total_conf_update_exe.php" method="post">
		<!-- ------------------------------------------------------------------------ -->
		<!-- 登録ボタン -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="250" border="0" cellspacing="0" cellpadding="2">
	<?
	echo("<tr height=\"22\">\n");
	///-----------------------------------------------------------------------------
	// シフトグループ名
	///-----------------------------------------------------------------------------
	echo("<td width=\"150\" align=\"left\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">シフトグループ（病棟）名</font></td>\n");
	echo("<td width=\"100\"><select name=\"group_id\" onchange=\"this.form.action='$fname'; this.form.submit();\">\n");
	for ($k=0; $k<count($group_array); $k++) {
		$wk_id = $group_array[$k]["group_id"];
		$wk_name = $group_array[$k]["group_name"];
		echo("<option  value=\"$wk_id\"");
		if ($group_id == $wk_id) {
			echo(" selected");
		}
		echo(">$wk_name  \n");
	}
	echo("</select></td>\n");
	///-----------------------------------------------------------------------------
	// 登録ボタン
	///-----------------------------------------------------------------------------
//	echo("<td align=\"right\" width=\"50\"><input type=\"submit\" value=\"登録\"></td>\n");
	echo("</tr>\n");
		?>
		</table>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜常勤集計表の設定＞</b></font></td>
<td align="right" width="50"><input type="submit" value="登録"></td>
</tr>
</table>
		<!-- ------------------------------------------------------------------------ -->
		<!--  -->
		<!-- ------------------------------------------------------------------------ -->
		<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">普通残業時間</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="normal_overtime_flg" value="1"<? if ($normal_overtime_flg == "1") {echo(" checked");} ?>>深夜、休日残業を含まない<input type="radio" name="normal_overtime_flg" value="2"<? if ($normal_overtime_flg == "2") {echo(" checked");} ?>>深夜、休日残業を含む</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">超勤深夜</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業時間で深夜（２２時から５時）の時間数</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">超勤休日深夜</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="hol_late_night_disp_flg" value="t"<? if ($hol_late_night_disp_flg != "f") {echo(" checked");} ?>>表示する<input type="radio" name="hol_late_night_disp_flg" value="f"<? if ($hol_late_night_disp_flg == "f") {echo(" checked");} ?>>表示しない</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅刻回数</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="delay_minute" value="<? echo($delay_minute); ?>" style="ime-mode:inactive;" size="5" maxlength="3">分以上の遅刻回数</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示勤務パターン</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($atdptn_array); $i++) {
		
		if ($atdptn_array[$i]["id"] == "10") {
			continue;
		}
		//checked
		$wk_id = $atdptn_array[$i]["id"];
		$checked = (in_array($wk_id, $ptn_full)) ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"ptn1_{$wk_id}\" value=\"1\" $checked>");
		//echo($atdptn_array[$i]["id"]);
		echo($atdptn_array[$i]["name"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休日出勤時間から除外する勤務パターン</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($atdptn_array); $i++) {
		
		if ($atdptn_array[$i]["id"] == "10") {
			continue;
		}
		$wk_id = $atdptn_array[$i]["id"];
		$checked = (in_array($wk_id, $hol_except_ptn)) ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"ptn5_{$wk_id}\" value=\"1\" $checked>");
		echo($atdptn_array[$i]["name"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($arr_allow); $i++) {
		
		$wk_id = $arr_allow[$i]["allow_id"];
		//初期時はチェックなしとする
//		$checked = (in_array($wk_id, $allow_full) || count($allow_full) == 0) ? " checked" : "";
		$checked = ($allow_full[$wk_id] == "t") ? " checked" : "";

		echo("<input type=\"checkbox\" name=\"allow1_{$wk_id}\" value=\"1\" $checked>");
		echo($arr_allow[$i]["allow_contents"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">繰越方法</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="radio" name="adjust_time_legal_hol_flg" value="1"<? if ($adjust_time_legal_hol_flg != "2") {echo(" checked");} ?>>労働時間<input type="radio" name="adjust_time_legal_hol_flg" value="2"<? if ($adjust_time_legal_hol_flg == "2") {echo(" checked");} ?>>公休数</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">変則労働月間時間</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="irrg_month_time" value="<? echo($irrg_month_time); ?>" style="ime-mode:inactive;" size="5" maxlength="3">時間</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通費該当勤務パターン</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($atdptn_array); $i++) {
		
		if ($atdptn_array[$i]["id"] == "10") {
			continue;
		}
		//checked
		$wk_id = $atdptn_array[$i]["id"];
		$checked = (key_exists($wk_id, $tra_full)) ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"ptn2_{$wk_id}\" value=\"1\" $checked>");
		//echo($atdptn_array[$i]["id"]);
		echo($atdptn_array[$i]["name"]);
		$wk_cnt = $tra_full[$wk_id];
		echo("<input type=\"text\" name=\"tracnt_{$wk_id}\" value=\"{$wk_cnt}\"  style=\"ime-mode:inactive;\" size=\"3\" maxlength=\"2\">");
		echo("回分&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		</table>
<table width="800" border="0" cellspacing="0" cellpadding="2">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>＜非常勤集計表の設定＞</b></font></td>
</tr>
</table>
		<table width="800" border="0" cellspacing="0" cellpadding="2" class="list">
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間の集計</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="text" name="parttime_split_hour" value="<? echo($parttime_split_hour); ?>" style="ime-mode:inactive;" size="3" maxlength="2">時で分割集計する</font></td>
			</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示勤務パターン</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($atdptn_array); $i++) {
		
		if ($atdptn_array[$i]["id"] == "10") {
			continue;
		}
		//checked
		$wk_id = $atdptn_array[$i]["id"];
		$checked = (in_array($wk_id, $ptn_part)) ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"ptn3_{$wk_id}\" value=\"1\" $checked>");
		//echo($atdptn_array[$i]["id"]);
		echo($atdptn_array[$i]["name"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($arr_allow); $i++) {
		
		$wk_id = $arr_allow[$i]["allow_id"];
		//初期時はチェックなしとする
		$checked = ($allow_part[$wk_id] == "t") ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"allow2_{$wk_id}\" value=\"1\" $checked>");
		echo($arr_allow[$i]["allow_contents"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		<tr height="22">
		<td width="20%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通費該当勤務パターン</font></td>
		<td width="80%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<?
	for ($i=0; $i<count($atdptn_array); $i++) {
		
		if ($atdptn_array[$i]["id"] == "10") {
			continue;
		}
		//checked
		$wk_id = $atdptn_array[$i]["id"];
		$checked = (in_array($wk_id, $tra_part)) ? " checked" : "";
		echo("<input type=\"checkbox\" name=\"ptn4_{$wk_id}\" value=\"1\" $checked>");
		echo($atdptn_array[$i]["name"]);
		echo("&nbsp;\n");
	}
	?>
		</font></td>
		</tr>
		</table>
	<!-- 登録ボタン -->
		<table width="800" border="0" cellspacing="0" cellpadding="2">
	<?
	echo("<tr height=\"22\">\n");
	echo("<td colspan=\"2\" align=\"right\"><input type=\"submit\" value=\"登録\"></td>\n");
	echo("</tr>\n");
		?>
		</table>
		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
	<?
	echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
	echo("<input type=\"hidden\" name=\"data_cnt\" value=\"$data_cnt\">\n");
	echo("<input type=\"hidden\" name=\"allow_max\" value=\"$allow_max\">\n");
		?>
	</form>

	<?
}
	?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
