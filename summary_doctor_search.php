<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);");
	echo("if (window.opener && !window.opener.closed) {");
	echo("	window.opener.close();");
	echo("}");
	echo("</script>");
	exit;
}

// 権限のチェック
/* 別機能でも使用できるようチェックなしにした
$checkauth = check_authority($session, 47, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
*/

// 「あ行」をデフォルトとする
if ($in_id == "") {$in_id = 0;}

// データベースに接続
$con = connect2db($fname);

// 職種一覧を配列に格納
$sql = "select job_id, job_nm from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$jobs = array();
while ($row = pg_fetch_array($sel)) {
	$jobs[$row["job_id"]] = $row["job_nm"];
}

// 職種のデフォルト値を設定
if ($job == "") {
	if ($job_name == "") {
		$job_name = "医師";
	}
	$job = array_search($job_name, $jobs);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>職員選択</title>
<script type="text/javascript">
function setEmployeeName(emp_id,emp_nm) {
	//呼び出し元画面に通知
	if(window.opener && !window.opener.closed && window.opener.call_back_summary_emp_search)
	{
		var result = new Object();
		result.emp_id = emp_id;
		result.emp_nm = emp_nm;
		window.opener.call_back_summary_emp_search('<?=$caller?>',result);
	}

	//自画面を終了
	window.close();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<center>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr height="32" bgcolor="#5279a5">
	<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職員選択</b></font></td>
	<td>&nbsp</td>
	<td width="10">&nbsp</td>
	<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
	</tr>
	<img src="img/spacer.gif" width="10" height="10" alt=""><br>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? /*
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
*/ ?>
<tr>
<? /*
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
*/ ?>
<td>
<form action="summary_doctor_search.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#FFFFFF">
<td><select name="job" onchange="this.form.submit();"><? show_job_list($jobs, $job); ?></select></td>
</tr>
</table>
<? show_initial_list($job, $in_id, $form_name, $caller, $session); ?>
<? show_doctor_list($con, $job, $in_id, $session, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="form_name" value="<? echo($form_name); ?>">
<input type="hidden" name="caller" value="<? echo($caller); ?>">
</form>
</td>
<? /*
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
*/ ?>
</tr>
<? /*
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
*/ ?>
</table>
</center>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="1" alt="">
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_job_list($jobs, $job) {
	foreach ($jobs as $tmp_job_id => $tmp_job_nm) {
		echo("<option value=\"$tmp_job_id\"");
		if ($tmp_job_id == $job) {echo(" selected");}
		echo(">$tmp_job_nm\n");
	}

}

function show_initial_list($job, $in_id, $form_name, $caller, $session) {
	$initials = array("あ", "か", "さ", "た", "な", "は", "ま", "や", "ら", "わ");

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	echo("<tr height=\"22\" bgcolor=\"#FFFFFF\">\n");
	echo("<td>\n");
	for ($i = 0, $j = count($initials); $i < $j; $i++) {
		echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($i != $in_id) {
			echo("<a href=\"summary_doctor_search.php?session=$session&job=$job&in_id=$i&form_name=$form_name&caller=$caller\">{$initials[$i]}行</a> ");
		} else {
			echo("{$initials[$i]}行 ");
		}
		echo("</font>");
	}
	echo("</td>\n");
	echo("</tr>\n");
	echo("</table>\n");
}

function show_doctor_list($con, $job, $in_id, $session, $fname) {
	$keywords = array();
	switch ($in_id) {
	case "0":  // あ行
		array_push($keywords, "01", "02", "03", "04", "05");
		break;
	case "1":  // か行
		array_push($keywords, "06", "07", "08", "09", "10");
		array_push($keywords, "11", "12", "13", "14", "15");
		break;
	case "2":  // さ行
		array_push($keywords, "16", "17", "18", "19", "20");
		array_push($keywords, "21", "22", "23", "24", "25");
		break;
	case "3":  // た行
		array_push($keywords, "26", "27", "28", "29", "30");
		array_push($keywords, "31", "32", "33", "34", "35");
		break;
	case "4":  // な行
		array_push($keywords, "36", "37", "38", "39", "40");
		break;
	case "5":  // は行
		array_push($keywords, "41", "42", "43", "44", "45");
		array_push($keywords, "46", "47", "48", "49", "50");
		array_push($keywords, "51", "52", "53", "54", "55");
		break;
	case "6":  // ま行
		array_push($keywords, "56", "57", "58", "59", "60");
		break;
	case "7":  // や行
		array_push($keywords, "61", "62", "63");
		break;
	case "8":  // ら行
		array_push($keywords, "64", "65", "66", "67", "68");
		break;
	case "9":  // わ行
		array_push($keywords, "69", "70", "71", "72", "73");
		array_push($keywords, "99");
		break;
	}

	$sql  = "select emp_id,emp_personal_id, emp_lt_nm, emp_ft_nm from ";
	$sql .= "empmst";
//	$sql .= "(select * from empmst natural inner join (select emp_id from authmst where emp_inci_flg) usable) empmst";
	$cond = "where emp_job = $job and (emp_keywd like '";
	$cond .= join("%' or emp_keywd like '", $keywords);
	$cond .= "%')";
	$cond .= " and exists (select * from authmst where authmst.emp_id = empmst.emp_id and authmst.emp_del_flg = 'f') order by emp_keywd";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
	while ($row = pg_fetch_array($sel)) {
		$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		$emp_id = $row["emp_id"];

		echo("<tr bgcolor=\"#FFFFFF\">\n");
//		echo("<tr bgcolor=\"#F5FFE5\">\n");
//		echo("<td width=\"140\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$row["emp_personal_id"]}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"javascript:void(0);\" onclick=\"setEmployeeName('$emp_id','$tmp_emp_nm');\">$tmp_emp_nm</a></font></td>\n");
		echo("</tr>\n");
	}
	echo("</table>\n");
}
?>
