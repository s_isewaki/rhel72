<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 施設・設備 | 権限設定</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_class_name.ini");
require("show_facility.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 41, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員ID・所属部署IDを取得
$sql = "select emp_id, emp_class, (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
$emp_class = pg_fetch_result($sel, 0, "emp_class");
$emp_name = pg_fetch_result($sel, 0, "emp_name");

// 部門一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
    $tmp_class_id = $row["class_id"];
    $tmp_class_nm = $row["class_nm"];
    $classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, classmst.class_nm, atrbmst.atrb_nm from atrbmst inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' order by classmst.order_no, atrbmst.order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 科一覧を取得
$sql = "select classmst.class_id, atrbmst.atrb_id, deptmst.dept_id, classmst.class_nm, atrbmst.atrb_nm, deptmst.dept_nm from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id inner join classmst on atrbmst.class_id = classmst.class_id";
$cond = "where classmst.class_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and deptmst.dept_del_flg = 'f' order by classmst.order_no, atrbmst.order_no, deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
    $tmp_class_id = $row["class_id"];
    $tmp_atrb_id = $row["atrb_id"];
    $tmp_dept_id = $row["dept_id"];
    $tmp_dept_nm = $row["dept_nm"];
    $dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sql = "select st_id, st_nm from stmst";
$cond = "where st_del_flg = 'f' order by order_no";
$sel_st = select_from_table($con, $sql, $cond, $fname);
if ($sel_st == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 初期表示時の設定
if (!is_array($ref_dept)) {$ref_dept = array();}
if (!is_array($ref_st)) {$ref_st = array();}
if (!is_array($upd_dept)) {$upd_dept = array();}
if (!is_array($upd_st)) {$upd_st = array();}
if ($back != "t") {

    // 施設・設備情報を取得
    $sql = "select facility.*, fclcate.fclcate_name from facility inner join fclcate on facility.fclcate_id = fclcate.fclcate_id";
    $cond = "where facility.facility_id = $facility_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $cate_name = pg_fetch_result($sel, 0, "fclcate_name");
    $facility_name = pg_fetch_result($sel, 0, "facility_name");
    $has_admin_flg = pg_fetch_result($sel, 0, "has_admin_flg");
    $ref_dept_st_flg = pg_fetch_result($sel, 0, "ref_dept_st_flg");
    $ref_dept_flg = pg_fetch_result($sel, 0, "ref_dept_flg");
    $ref_st_flg = pg_fetch_result($sel, 0, "ref_st_flg");
    $upd_dept_st_flg = pg_fetch_result($sel, 0, "upd_dept_st_flg");
    $upd_dept_flg = pg_fetch_result($sel, 0, "upd_dept_flg");
    $upd_st_flg = pg_fetch_result($sel, 0, "upd_st_flg");

    $sql = "select f.class_id, f.atrb_id, f.dept_id from fclrefdept f left join classmst c on f.class_id = c.class_id left join atrbmst a on f.atrb_id = a.atrb_id left join deptmst d on f.dept_id = d.dept_id";
    $cond = "where f.facility_id = $facility_id order by c.order_no, a.order_no, d.order_no";
    $sel_ref_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_dept == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $ref_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from fclrefst";
    $cond = "where facility_id = $facility_id order by st_id";
    $sel_ref_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_st == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_ref_st)) {
        $tmp_st_id = $row["st_id"];
        $ref_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from fclrefemp";
    $cond = "where facility_id = $facility_id order by emp_id";
    $sel_ref_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_ref_emp == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list1 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_ref_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list1 .= ",";
        }
        $target_id_list1 .= $tmp_emp_id;
    }

    $sql = "select f.class_id, f.atrb_id, f.dept_id from fclupddept f left join classmst c on f.class_id = c.class_id left join atrbmst a on f.atrb_id = a.atrb_id left join deptmst d on f.dept_id = d.dept_id";
    $cond = "where f.facility_id = $facility_id order by c.order_no, a.order_no, d.order_no";
    $sel_upd_dept = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_dept == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_dept)) {
        $tmp_class_id = $row["class_id"];
        $tmp_atrb_id = $row["atrb_id"];
        $tmp_dept_id = $row["dept_id"];
        $upd_dept[] = "$tmp_class_id-$tmp_atrb_id-$tmp_dept_id";
    }

    $sql = "select st_id from fclupdst";
    $cond = "where facility_id = $facility_id order by st_id";
    $sel_upd_st = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_st == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel_upd_st)) {
        $tmp_st_id = $row["st_id"];
        $upd_st[] = $tmp_st_id;
    }

    $sql = "select emp_id from fclupdemp";
    $cond = "where facility_id = $facility_id order by emp_id";
    $sel_upd_emp = select_from_table($con, $sql, $cond, $fname);
    if ($sel_upd_emp == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $target_id_list2 = "";
    $is_first_flg = true;
    while ($row = pg_fetch_array($sel_upd_emp)) {
        $tmp_emp_id = $row["emp_id"];
        if ($is_first_flg) {
            $is_first_flg = false;
        } else {
            $target_id_list2 .= ",";
        }
        $target_id_list2 .= $tmp_emp_id;
    }
}

// デフォルト値の設定
if ($upd_dept_flg != "1" && $upd_dept_flg != "2") {$upd_dept_flg = "1";}
if ($upd_st_flg != "1" && $upd_st_flg != "2") {$upd_st_flg = "1";}
if ($ref_dept_flg != "1" && $ref_dept_flg != "2") {$ref_dept_flg = "1";}
if ($ref_st_flg != "1" && $ref_st_flg != "2") {$ref_st_flg = "1";}

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];

// メンバー情報を配列に格納
for ($list_idx=1; $list_idx<=2; $list_idx++) {
    $arr_target["$list_idx"] = array();
    $target_id_list_name = "target_id_list$list_idx";
    $target_id_list = $$target_id_list_name;
    if ($target_id_list != "") {
        $arr_target_id = split(",", $target_id_list);
        for ($i = 0; $i < count($arr_target_id); $i++) {
            $tmp_emp_id = $arr_target_id[$i];
            $sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
            $cond = "where emp_id = '$tmp_emp_id'";
            $sel = select_from_table($con, $sql, $cond, $fname);
            if ($sel == 0) {
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
            }
            $tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
            array_push($arr_target["$list_idx"], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
        }
    }
}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
    mode = parseInt(item_id) + 6;
    dx = screen.availWidth - 10;
    dy = screen.top;
    base = 0;
    wx = 720;
    wy = 600;
    var url = './emplist_popup.php';
    url += '?session=<?=$session?>';
    url += '&emp_id=<?=$emp_id?>';
    url += '&mode='+mode;
    url += '&item_id='+item_id;
    childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

    childwin.focus();
}

function closeEmployeeList() {
    if (childwin != null && !childwin.closed) {
        childwin.close();
    }
    childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
    for ($i=1; $i<=2; $i++) {
        $script = "m_target_list['$i'] = new Array(";
        $is_first = true;
        foreach($arr_target["$i"] as $row)
        {
            if($is_first)
            {
                $is_first = false;
            }
            else
            {
                $script .= ",";
            }
            $tmp_emp_id = $row["id"];
            $tmp_emp_name = $row["name"];
            $script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
        }
        $script .= ");\n";
        print $script;
    }
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
    if(confirm("登録対象者を削除します。よろしいですか？"))
    {
        var is_exist_flg = false;
        for(var i=0;i<m_target_list[item_id].length;i++)
        {
            if(emp_id == m_target_list[item_id][i].emp_id)
            {
                is_exist_flg = true;
                break;
            }
        }
        m_target_list[item_id] = new Array();
        if (is_exist_flg == true) {
            m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
        }
        update_target_html(item_id);
    }
}

var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("atrbs[$tmp_class_id] = [];\n");
	}

	echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

	$pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];
	$tmp_dept_nm = $row["dept_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("depts[$tmp_class_id] = {};\n");
	}

	if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
		echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
	}

	echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id, name: '$tmp_dept_nm'});\n");

	$pre_class_id = $tmp_class_id;
	$pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>

//----------

function initPage() {
    onChangeArchive(true, '<? echo($ref_class_src); ?>', '<? echo($ref_atrb_src); ?>', '<? echo($upd_class_src); ?>', '<? echo($upd_atrb_src); ?>');
    //登録対象者を設定する。
    update_target_html("1");
    update_target_html("2");
}

function onChangeArchive(
    init_flg,
    default_ref_class_src,
    default_ref_atrb_src,
    default_upd_class_src,
    default_upd_atrb_src
) {
    setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src);
    setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src);
    setDisabled();
}

function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {
    if (!init_flg) {
        deleteAllOptions(document.mainform.ref_dept);
    }

    deleteAllOptions(document.mainform.ref_class_src);

    addOption(document.mainform.ref_class_src, '-', '----------', default_ref_class_src);
	for (var i = 0, len = classes.length; i < len; i++) {
		addOption(document.mainform.ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
    }

    setRefAtrbSrcOptions(default_ref_atrb_src);
}

function setRefAtrbSrcOptions(default_ref_atrb_src) {
    deleteAllOptions(document.mainform.ref_atrb_src);

    addOption(document.mainform.ref_atrb_src, '-', '----------', default_ref_atrb_src);

    var class_id = document.mainform.ref_class_src.value;
    if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(document.mainform.ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
        }
    }

    setRefDeptSrcOptions();
}

function setRefDeptSrcOptions() {
    deleteAllOptions(document.mainform.ref_dept_src);

    var class_id = document.mainform.ref_class_src.value;
    var atrb_id = document.mainform.ref_atrb_src.value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(document.mainform.ref_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function setUpdClassSrcOptions(init_flg, default_upd_class_src, default_upd_atrb_src) {
    if (!init_flg) {
        deleteAllOptions(document.mainform.upd_dept);
    }

    deleteAllOptions(document.mainform.upd_class_src);

    addOption(document.mainform.upd_class_src, '-', '----------', default_upd_class_src);
	for (var i = 0, len = classes.length; i < len; i++) {
		addOption(document.mainform.upd_class_src, classes[i].id, classes[i].name, default_upd_class_src);
    }

    setUpdAtrbSrcOptions(default_upd_atrb_src);
}

function setUpdAtrbSrcOptions(default_upd_atrb_src) {
    deleteAllOptions(document.mainform.upd_atrb_src);

    addOption(document.mainform.upd_atrb_src, '-', '----------', default_upd_atrb_src);

    var class_id = document.mainform.upd_class_src.value;
    if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(document.mainform.upd_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_upd_atrb_src);
        }
    }

    setUpdDeptSrcOptions();
}

function setUpdDeptSrcOptions() {
    deleteAllOptions(document.mainform.upd_dept_src);

    var class_id = document.mainform.upd_class_src.value;
    var atrb_id = document.mainform.upd_atrb_src.value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(document.mainform.upd_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function setDisabled() {
    var disabled;

    disabled = !(!document.mainform.ref_dept_st_flg.disabled && !document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_dept_flg[0].disabled = disabled;
    document.mainform.ref_dept_flg[1].disabled = disabled;

    disabled = !(!document.mainform.ref_dept_flg[1].disabled && document.mainform.ref_dept_flg[1].checked);
    document.mainform.ref_dept_all.disabled = disabled;
    document.mainform.ref_class_src.disabled = disabled;
    document.mainform.ref_atrb_src.disabled = disabled;
    document.mainform.ref_dept_src.disabled = disabled;
    document.mainform.ref_dept.disabled = disabled;
    document.mainform.add_ref_dept.disabled = disabled;
    document.mainform.delete_ref_dept.disabled = disabled;
    document.mainform.delete_all_ref_dept.disabled = disabled;
    document.mainform.select_all_ref_dept.disabled = disabled;

    disabled = (document.mainform.ref_dept_st_flg.disabled || document.mainform.ref_dept_st_flg.checked);
    document.mainform.ref_st_flg[0].disabled = disabled;
    document.mainform.ref_st_flg[1].disabled = disabled;

    disabled = !(!document.mainform.ref_st_flg[1].disabled && document.mainform.ref_st_flg[1].checked);
    document.mainform.elements['ref_st[]'].disabled = disabled;

    disabled = !(!document.mainform.upd_dept_st_flg.disabled && !document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_dept_flg[0].disabled = disabled;
    document.mainform.upd_dept_flg[1].disabled = disabled;

    disabled = !(!document.mainform.upd_dept_flg[1].disabled && document.mainform.upd_dept_flg[1].checked);
    document.mainform.upd_dept_all.disabled = disabled;
    document.mainform.upd_class_src.disabled = disabled;
    document.mainform.upd_atrb_src.disabled = disabled;
    document.mainform.upd_dept_src.disabled = disabled;
    document.mainform.upd_dept.disabled = disabled;
    document.mainform.add_upd_dept.disabled = disabled;
    document.mainform.delete_upd_dept.disabled = disabled;
    document.mainform.delete_all_upd_dept.disabled = disabled;
    document.mainform.select_all_upd_dept.disabled = disabled;

    disabled = (document.mainform.upd_dept_st_flg.disabled || document.mainform.upd_dept_st_flg.checked);
    document.mainform.upd_st_flg[0].disabled = disabled;
    document.mainform.upd_st_flg[1].disabled = disabled;

    disabled = !(!document.mainform.upd_st_flg[1].disabled && document.mainform.upd_st_flg[1].checked);
    document.mainform.elements['upd_st[]'].disabled = disabled;
}

function submitForm() {
    var ref_dept_box = document.mainform.ref_dept;
    if (!ref_dept_box.disabled) {
        for (var i = 0, j = ref_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_ref_dept[]', ref_dept_box.options[i].value);
        }
    }

    var upd_dept_box = document.mainform.upd_dept;
    if (!upd_dept_box.disabled) {
        for (var i = 0, j = upd_dept_box.length; i < j; i++) {
            addHiddenElement(document.mainform, 'hid_upd_dept[]', upd_dept_box.options[i].value);
        }
    }

    closeEmployeeList();

    document.mainform.submit();
}

function addOption(box, value, text, selected) {
    var opt = document.createElement("option");
    opt.value = value;
    opt.text = text;
    if (selected == value) {
        opt.selected = true;
    }
    box.options[box.length] = opt;
    if (!box.multiple) {
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }
}

function addSelectedOptions(dest_box, src_box) {
    var options = new Array();
    for (var i = 0, j = dest_box.length; i < j; i++) {
        options[dest_box.options[i].value] = dest_box.options[i].text;
    }
    deleteAllOptions(dest_box);
    for (var i = 0, j = src_box.length; i < j; i++) {
        if (src_box.options[i].selected) {
            options[src_box.options[i].value] = src_box.options[i].text;
        }
    }

    for (var i in options) {
        addOption(dest_box, i, options[i]);
    }
}

function selectAllOptions(box) {
    for (var i = 0, j = box.length; i < j; i++) {
        box.options[i].selected = true;
    }
}

function deleteSelectedOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        if (box.options[i].selected) {
            box.options[i] = null;
        }
    }
}

function deleteAllOptions(box) {
    for (var i = box.length - 1; i >= 0; i--) {
        box.options[i] = null;
    }
}

function addHiddenElement(frm, name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}

function showDeptAll(div) {
    window.open('select_dept_all.php?session=<? echo($session); ?>&module=facility&div='.concat(div), 'deptall', 'width=800,height=500,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a> &gt; <a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<? echo($default_url); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_master_menu.php?session=<? echo($session); ?>&opens=<? echo($cate_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">施設・設備一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_category_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="facility_auth_setting.php?session=<? echo($session); ?>&cate_id=<? echo($cate_id); ?>&facility_id=<? echo($facility_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>権限設定</b></font></a></td>
<? if ($has_admin_flg == "t") { ?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_admin_setting.php?session=<? echo($session); ?>&cate_id=<? echo($cate_id); ?>&facility_id=<? echo($facility_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">管理者設定</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_master_type.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="facility_admin_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="facility_tmpl_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テンプレート登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="mainform" action="facility_auth_setting_confirm.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="更新" onclick="submitForm();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="16%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ</font></td>
<td width="34%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($cate_name); ?></font></td>
<td width="16%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名称</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($facility_name); ?></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照可能範囲の指定</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td bgcolor="#f6f9ff" colspan="2" class="spacing">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td align="right"><input type="button" name="ref_dept_all" value="全画面" onclick="showDeptAll('ref');"></td>
</tr>
</table>
</td>
<td bgcolor="#f6f9ff" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr>
<td colspan="2">
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_dept_flg" value="1"<? if ($ref_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ref_class_src" onchange="setRefAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="ref_atrb_src" onchange="setRefDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="ref_dept" size="6" multiple style="width:120px;">
<?
foreach ($ref_dept as $tmp_dept_id) {
    echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
}
?>
</select>
</td>
<td align="center"><input type="button" name="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);"></td>
<td><select name="ref_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
<td></td>
<td><input type="button" name="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_st_flg" value="1"<? if ($ref_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="ref_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
    $tmp_st_id = $row["st_id"];
    $tmp_st_nm = $row["st_nm"];
    echo("<option value=\"$tmp_st_id\"");
    if (in_array($tmp_st_id, $ref_st)) {
        echo(" selected");
    }
    echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="3">

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" colspan="4" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">予約可能範囲の指定</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="upd_dept_st_flg" value="t"<? if ($upd_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td bgcolor="#f6f9ff" colspan="2" class="spacing">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td align="right"><input type="button" name="upd_dept_all" value="全画面" onclick="showDeptAll('upd');"></td>
</tr>
</table>
</td>
<td bgcolor="#f6f9ff" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>
<tr>
<td colspan="2">
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="upd_dept_flg" value="1"<? if ($upd_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="upd_dept_flg" value="2"<? if ($upd_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="upd_class_src" onchange="setUpdAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="upd_atrb_src" onchange="setUpdDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="upd_dept" size="6" multiple style="width:120px;">
<?
foreach ($upd_dept as $tmp_dept_id) {
    echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
}
?>
</select>
</td>
<td align="center"><input type="button" name="add_upd_dept" value=" &lt; " onclick="addSelectedOptions(this.form.upd_dept, this.form.upd_dept_src);"><br><br><input type="button" name="delete_upd_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.upd_dept);"></td>
<td><select name="upd_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_upd_dept" value="全て消去" onclick="deleteAllOptions(this.form.upd_dept);"></td>
<td></td>
<td><input type="button" name="select_all_upd_dept" value="全て選択" onclick="selectAllOptions(this.form.upd_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="upd_st_flg" value="1"<? if ($upd_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="upd_st_flg" value="2"<? if ($upd_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="upd_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
    $tmp_st_id = $row["st_id"];
    $tmp_st_nm = $row["st_nm"];
    echo("<option value=\"$tmp_st_id\"");
    if (in_array($tmp_st_id, $upd_st)) {
        echo(" selected");
    }
    echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist2" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('2');"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('2','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="3">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area2"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="更新" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="cate_id" value="<? echo($cate_id); ?>">
<input type="hidden" name="facility_id" value="<? echo($facility_id); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" id="target_id_list2"   name="target_id_list2" value="">
<input type="hidden" id="target_name_list2" name="target_name_list2" value="">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
