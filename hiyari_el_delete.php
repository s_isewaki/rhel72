<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}

//========================================================================================================================
//DB処理開始
//========================================================================================================================


//DBコネクション取得
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//トランザクションの開始
pg_query($con, "begin");


//========================================================================================================================
// ログインユーザの職員IDを取得
//========================================================================================================================
$emp_id = get_emp_id($con, $session, $fname);


//========================================================================================================================
//カテゴリの削除
//========================================================================================================================

// カテゴリが選択された場合
if (is_array($cid))
{
	// 選択されたカテゴリをループ
	foreach ($cid as $tmp_cid)
	{
		//==============================
		//配下存在チェック
		//==============================
		
		// カテゴリ直下にフォルダが存在した場合、エラーとする
		$sql = "select count(*) from el_folder";
		$cond = "where lib_cate_id = $tmp_cid and not exists (select * from el_tree where child_id = el_folder.folder_id)";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0)
		{
			$sql = "select lib_cate_nm from el_cate";
			$cond = "where lib_cate_id = $tmp_cid";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0)
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");

			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$cate_nm}」は、フォルダが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		
		// カテゴリ直下にコンテンツが存在した場合、エラーとする
		$sql = "select count(*) from el_info";
		$cond = "where lib_cate_id = $tmp_cid and folder_id is null";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0)
		{
			$sql = "select lib_cate_nm from el_cate";
			$cond = "where lib_cate_id = $tmp_cid";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0)
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$cate_nm = pg_fetch_result($sel, 0, "lib_cate_nm");

			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$cate_nm}」は、コンテンツが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}
		
		
		//==============================
		//カテゴリデータを削除
		//==============================
		$sql = "delete from el_cate";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		//==============================
		//カテゴリ権限情報を削除
		//==============================
		$sql = "delete from el_cate_ref_dept";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_cate_ref_st";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_cate_ref_emp";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_cate_upd_dept";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_cate_upd_st";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_cate_upd_emp";
		$cond = "where lib_cate_id = $tmp_cid";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

//========================================================================================================================
//フォルダの削除
//========================================================================================================================

// フォルダが選択された場合
if (is_array($fid))
{
	// 選択されたフォルダをループ
	foreach ($fid as $tmp_fid)
	{
		//==============================
		//配下存在チェック
		//==============================
		
		// フォルダ情報を取得
		$sql = "select lib_cate_id, folder_name from el_folder";
		$cond = "where folder_id = $tmp_fid";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$tmp_cate_id = pg_fetch_result($sel, 0, "lib_cate_id");
		$folder_name = pg_fetch_result($sel, 0, "folder_name");

		// フォルダ直下にフォルダが存在した場合、エラーとする
		$sql = "select count(*) from el_tree";
		$cond = "where parent_id = $tmp_fid";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、フォルダが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		// フォルダ直下にコンテンツが存在した場合、エラーとする
		$sql = "select count(*) from el_info";
		$cond = "where folder_id = $tmp_fid";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (intval(pg_fetch_result($sel, 0, 0)) > 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\">alert('「{$folder_name}」は、コンテンツが存在するため削除できません。');</script>");
			echo("<script type=\"text/javascript\">history.back();</script>");
			exit;
		}

		//==============================
		//上位フォルダ(カテゴリ)の更新日時を更新
		//==============================
		
		lib_update_folder_modified($con, $tmp_cate_id, $tmp_fid, $emp_id, $fname);
	}
}

// フォルダデータを削除
reset($fid);
foreach ($fid as $tmp_fid) 
{
	//==============================
	//ツリー情報の削除
	//==============================
	
	$sql = "delete from el_tree";
	$cond = "where parent_id = $tmp_fid or child_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}


	//==============================
	//フォルダ情報の削除
	//==============================
	
	$sql = "delete from el_folder";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	//==============================
	//フォルダ権限情報の削除
	//==============================
	
	$sql = "delete from el_folder_ref_dept";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from el_folder_ref_st";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from el_folder_ref_emp";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from el_folder_upd_dept";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from el_folder_upd_st";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$sql = "delete from el_folder_upd_emp";
	$cond = "where folder_id = $tmp_fid";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//========================================================================================================================
//コンテンツの削除
//========================================================================================================================

// 削除対象のコンテンツが存在する場合
if (is_array($did) && count($did) > 0)
{
	//削除対象のコンテンツ情報を取得
	$sql = "select lib_id, lib_archive, lib_cate_id, folder_id from el_info";
	$did_list = implode(", ", $did);
	$cond = "where lib_delete_flag = 'f' and lib_id in (select lib_id from el_edition where base_lib_id in (select base_lib_id from el_edition where lib_id in ($did_list)))";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0)
	{
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$del_docs = array();
	while ($row = pg_fetch_array($sel))
	{
		$del_docs[] = array(
			"id" => $row["lib_id"],
			"archive" => $row["lib_archive"],
			"category" => $row["lib_cate_id"],
			"folder_id" => $row["folder_id"]
		);
	}

	//削除対象の全コンテンツに対して
	foreach ($del_docs as $tmp_doc)
	{
		//==============================
		//コンテンツの感想を削除
		//==============================
		$base_lib_id = get_base_lib_id($con,$fname,$tmp_doc["id"]);
		$sql = "delete from el_kansou";
		$cond = "where base_lib_id = {$base_lib_id}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		
		//==============================
		//コンテンツ参照ログを削除
		//==============================
		$sql = "delete from el_reflog";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		
		//==============================
		//コンテンツ版情報を削除
		//==============================
		$sql = "delete from el_edition";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		
		//==============================
		//コンテンツ情報を削除
		//==============================
		$sql = "delete from el_info";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		
		//==============================
		//コンテンツ権限情報を削除
		//==============================
		$sql = "delete from el_ref_dept";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_ref_st";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_ref_emp";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_upd_dept";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_upd_st";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$sql = "delete from el_upd_emp";
		$cond = "where lib_id = {$tmp_doc["id"]}";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0)
		{
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		//==============================
		//上位フォルダ(カテゴリ)の更新日時を更新
		//==============================
		lib_update_folder_modified($con, $tmp_doc["category"], $tmp_doc["folder_id"], $emp_id, $fname);
	}
}

//========================================================================================================================
//DB処理終了
//========================================================================================================================

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

//========================================================================================================================
//物理ファイル／物理フォルダの削除
//========================================================================================================================

//コンテンツの物理ファイルを削除
$cwd = getcwd();
reset($del_docs);
foreach ($del_docs as $tmp_doc)
{
	$cate_dir = get_cate_directory_path($tmp_doc["category"]);
	exec("rm -f {$cwd}/{$cate_dir}/document{$tmp_doc["id"]}.*");
}

//カテゴリの削除の場合はカテゴリの物理フォルダを削除
if (is_array($cid))
{
	reset($cid);
	foreach ($cid as $tmp_cid)
	{
		$cate_dir = get_cate_directory_path($tmp_cid);
		exec("rm -fr {$cwd}/{$cate_dir}");
	}
}

//========================================================================================================================
//画面遷移
//========================================================================================================================

//管理画面版ログ参照画面の場合
if ($path == "2")
{
	echo("<script type=\"text/javascript\">location.href = 'hiyari_el_admin_refer_log.php?session=$session&sort=$sort&page=$page';</script>");
}
//管理画面の場合
if ($path == "3")
{
	echo("<script type=\"text/javascript\">location.href = 'hiyari_el_admin_menu.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
}
//ユーザー画面の場合
else
{
	echo("<script type=\"text/javascript\">location.href = 'hiyari_el_list_all.php?session=$session&a=$archive&c=$category&f=$folder_id&o=$o';</script>");
}
?>
