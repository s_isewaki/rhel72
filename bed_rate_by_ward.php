<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>病床管理｜病床利用率（病棟別）</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_calc_bed_by_ward.ini");
require("show_select_values.ini");
require("get_values.ini");
require("get_ward_div.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限のチェック
$ward = check_authority($session,14,$fname);
if($ward == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

if (!checkdate($mm, $dd, $yyyy)) {
	echo("<script language='javascript'>alert('日付が不正です');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
} else if (date("YmdHi") < "$yyyy$mm$dd$hh$ii") {
	echo("<script language='javascript'>alert('未来日時は計算対象外です');</script>");
	echo("<script language='javascript'>history.back();</script>");
	exit;
}

// DBへのコネクション作成
$con = connect2db($fname);

// ベット数除外対象のURLクエリ文字列を作成
if (!is_array($omit_types)) {
	$omit_types = array();
} else {
	$omit_query = "";
	foreach ($omit_types as $omit_type) {
		$omit_query .= "&omit_types[]=$omit_type";
	}
}

// 病棟区分名を取得
$arr_ward_div = get_ward_div($con, $fname, false, true, true);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#bdd1e7"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床管理トップ</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#5279a5"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床利用率管理</b></font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td>&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="34%" height="22" align="center">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率</font>
</td>
<td width="33%" align="center">
<a href="bed_rate_average.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">平均病床利用率</font></a>
</td>
<td width="34%" align="center">
<a href="bed_rate_activity.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床稼動率</font></a>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- 日時 -->
<form action="bed_rate_by_ward.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="30" bgcolor="#f6f9ff">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
日時：<select name="yyyy">
<? show_select_years(15, $yyyy) ?>
</select>/<select name="mm">
<? show_select_months($mm) ?>
</select>/<select name="dd">
<? show_select_days($dd) ?>
</select>&nbsp;<select name="hh">
<? show_select_hrs_0_23($hh) ?>
</select>：<select name="ii">
<? show_select_min($ii) ?>
</select>
<img src="img/spacer.gif" alt="" width="5" height="1">
ベット数除外対象：<input type="checkbox" name="omit_types[]" value="3"<? if (in_array("3", $omit_types)) {echo(" checked");} ?>>回復室
<input type="checkbox" name="omit_types[]" value="4"<? if (in_array("4", $omit_types)) {echo(" checked");} ?>>ICU
<input type="checkbox" name="omit_types[]" value="5"<? if (in_array("5", $omit_types)) {echo(" checked");} ?>>HCU
<input type="checkbox" name="omit_types[]" value="6"<? if (in_array("6", $omit_types)) {echo(" checked");} ?>>NICU
<input type="checkbox" name="omit_types[]" value="7"<? if (in_array("7", $omit_types)) {echo(" checked");} ?>>GCU
<img src="img/spacer.gif" alt="" width="5" height="1">
<input type="submit" value="計算">
</font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- タブ2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100" height="22" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">区分別</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病棟別</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>
<!-- メニュー2 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr>
<td width="20%" height="22" align="center">
<? if ($ward_type != "1" && $arr_ward_div["1"]["use_flg"]) { ?><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>&ward_type=1"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]["name"]); ?></font>
<? if ($ward_type != "1" && $arr_ward_div["1"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "2" && $arr_ward_div["2"]["use_flg"]) { ?><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>&ward_type=2"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]["name"]); ?></font>
<? if ($ward_type != "2" && $arr_ward_div["2"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "3" && $arr_ward_div["3"]["use_flg"]) { ?><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>&ward_type=3"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]["name"]); ?></font>
<? if ($ward_type != "3" && $arr_ward_div["3"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "4" && $arr_ward_div["4"]["use_flg"]) { ?><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>&ward_type=4"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]["name"]); ?></font>
<? if ($ward_type != "4" && $arr_ward_div["4"]["use_flg"]) { ?></a><? } ?>
</td>
<td width="20%" align="center">
<? if ($ward_type != "5" && $arr_ward_div["5"]["use_flg"]) { ?><a href="bed_rate_by_ward.php?session=<? echo($session); ?>&yyyy=<? echo($yyyy); ?>&mm=<? echo($mm); ?>&dd=<? echo($dd); ?>&hh=<? echo($hh); ?>&ii=<? echo($ii); ?><? echo($omit_query); ?>&ward_type=5"><? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]["name"]); ?></font>
<? if ($ward_type != "5" && $arr_ward_div["5"]["use_flg"]) { ?></a><? } ?>
</td>
</tr>
</table>
<img src='img/spacer.gif' width='1' height='5' alt=''><br>

<!-- 病床利用率 -->
<? if ($ward_type != "") { ?>
<table width="40%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr height="22" bgcolor="#f6f9ff">
<td width="50%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟</font></td>
<td width="50%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率</font></td>
</tr>
<? show_bed_rate_list($con, $ward_type, $arr_ward_div, "$yyyy$mm$dd$hh$ii", $fname, $omit_types); ?>
</table>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
//==============================================================================
// 関数
//==============================================================================

//------------------------------------------------------------------------------
// 病床利用率の出力
//------------------------------------------------------------------------------
function show_bed_rate_list($con, $ward_type, $arr_ward_div, $yyyymmddhhii, $fname, $omit_types) {

	// 棟一覧を取得（名称を突き合わせるため）
	$sql = "select bldg_cd, bldg_name from bldgmst";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$buildings = array();
	while ($row = pg_fetch_array($sel)) {
		$buildings[$row["bldg_cd"]] = $row["bldg_name"];
	}

	// 病棟一覧を取得（名称を突き合わせるため）
	$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
	$cond = "where ward_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$wards[$row["bldg_cd"]][$row["ward_cd"]] = $row["ward_name"];
	}

	// 下記いずれかの条件に該当する病室一覧を取得
	// 条件1：病室の病棟区分が未設定、かつ、所属病棟の病棟区分＝指定の病棟区分
	// 条件2：病室の病棟区分＝指定の病棟区分
	$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name, ward_type from ptrmmst";
	$cond = "where ptrm_del_flg = 'f' and ((ward_type = '0' and exists (select * from wdmst where wdmst.bldg_cd = ptrmmst.bldg_cd and wdmst.ward_cd = ptrmmst.ward_cd and wdmst.ward_type = '$ward_type')) or ward_type = '$ward_type') order by bldg_cd, ward_cd, ward_type, ptrm_room_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// 病室一覧を表示形式に加工
	while ($row = pg_fetch_array($sel)) {
		if (!isset($rooms[$row["bldg_cd"]][$row["ward_cd"]])) {
			$rooms[$row["bldg_cd"]][$row["ward_cd"]] = array();
		}

		// 病室の病棟区分が未設定の場合：1行にまとめる
		if ($row["ward_type"] == "0") {
			if (!isset($rooms[$row["bldg_cd"]][$row["ward_cd"]][0])) {
				$rooms[$row["bldg_cd"]][$row["ward_cd"]][0] = array(
					"title" => $buildings[$row["bldg_cd"]] . $wards[$row["bldg_cd"]][$row["ward_cd"]],
					"ptrm_room_no_list" => array()
				);
			}
			$rooms[$row["bldg_cd"]][$row["ward_cd"]][0]["ptrm_room_no_list"][] = $row["ptrm_room_no"];

		// 病室の病棟区分が設定ありの場合：1行1病室
		} else {
			$rooms[$row["bldg_cd"]][$row["ward_cd"]][] = array(
				"title" => $buildings[$row["bldg_cd"]] . $wards[$row["bldg_cd"]][$row["ward_cd"]] . $row["ptrm_name"],
				"ptrm_room_no_list" => array($row["ptrm_room_no"])
			);
		}
	}
	unset($buildings);
	unset($wards);

	foreach ($rooms as $tmp_bldg_cd => $tmp_wards) {
		foreach ($tmp_wards as $tmp_ward_cd => $tmp_rows) {
			foreach ($tmp_rows as $tmp_row) {
				echo("<tr>\n");
				echo("<td align=\"right\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>{$tmp_row["title"]}</td>");
				echo("<td align=\"right\"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>");
				echo(get_bed_rate_past($con, $tmp_bldg_cd, $tmp_ward_cd, $tmp_row["ptrm_room_no_list"], $yyyymmddhhii, $fname, $omit_types));
				echo("%</td>");
				echo("</tr>\n");
			}
		}
	}
}
?>
