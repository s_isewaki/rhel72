<?php
ob_start();

require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("settings_common.php");
require_once("get_values.ini");
require_once("timecard_tantou_class.php");

$fname = $PHP_SELF;

// ���å����Υ����å�
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ���¤Υ����å�
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0" && $mmode != "usr") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// �ǡ����١�������³
$con = connect2db($fname);
if ($mmode == "usr") {
	$emp_id = get_emp_id($con, $session, $fname);
	$timecard_tantou_class = new timecard_tantou_class($con, $fname);
	$timecard_tantou_branches = $timecard_tantou_class->get_tantou_branches($emp_id);
}

$duty_type_flag = get_settings_value("employment_working_other_flg", "f");

$cls = intval($_POST["cls"]);
$cls_name = ($_POST["cls_name"]);
$atrb = intval($_POST["atrb"]);
$atrb_name = ($_POST["atrb_name"]);
$dept = intval($_POST["dept"]);
$dept_name = ($_POST["dept_name"]);
$start_date = $_POST["select_start_yr"] . $_POST["select_start_mon"];
$end_date = $_POST["select_end_yr"] . $_POST["select_end_mon"];

// ����ID�������� array_keys($array)
$emp_info = array();
$data_list = array();
$sql = "select w.emp_id, w.yyyymm, w.overtime_hour, w.overtime_min, ";
$sql .= "f.emp_id, f.emp_personal_id, f.emp_lt_nm, f.emp_ft_nm, e.st_nm, g.job_nm, ";
$sql .= "classmst.class_nm,atrbmst.atrb_nm,deptmst.dept_nm,ec.duty_form, ec.duty_form_type ";
$sql .= "from wktotalovtm w ";
$sql .= "INNER JOIN  empmst f on w.emp_id = f.emp_id ";
$sql .= "LEFT JOIN stmst e ON f.emp_st = e.st_id AND e.st_del_flg = 'f' ";
$sql .= "LEFT JOIN jobmst g ON f.emp_job = g.job_id AND g.job_del_flg = 'f' ";
$sql .= "LEFT JOIN classmst ON w.class_id = classmst.class_id AND classmst.class_del_flg = 'f' ";
$sql .= "LEFT JOIN atrbmst ON w.atrb_id = atrbmst.atrb_id AND atrbmst.atrb_del_flg = 'f' ";
$sql .= "LEFT JOIN deptmst ON w.dept_id = deptmst.dept_id AND deptmst.dept_del_flg = 'f' ";
$sql .= "LEFT JOIN empcond ec ON f.emp_id = ec.emp_id ";
$cond = "where w.yyyymm >= '$start_date' and w.yyyymm <= '$end_date' ";
if (!empty($cls) && $cls != "-"){
	$cond .= "and w.class_id = $cls ";

	if (!empty($atrb) && $atrb != "-"){
		$cond .= "and w.atrb_id = $atrb ";
			
		if (!empty($dept) && $dept != "-"){
			$cond .= "and w.dept_id = $dept ";
		}
	}
}
//ô����°
if ($mmode == "usr") {
    $cond .= " and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond .= " or (";
        if ($r["class_id"]) $cond .= "     w.class_id=".$r["class_id"];
        if ($r["atrb_id"])  $cond .= "     and w.atrb_id=".$r["atrb_id"];
        if ($r["dept_id"])  $cond .= "     and w.dept_id=".$r["dept_id"];
        if ($r["class_id"]) $cond .= " )";
    }
    //��̳
    $cond_ccr = " or exists (select * from concurrent where concurrent.emp_id = w.emp_id and (1 <> 1";
    foreach ($timecard_tantou_branches as $r) {
        if ($r["class_id"]) $cond_ccr .= " or (";
        if ($r["class_id"]) $cond_ccr .= "     concurrent.emp_class=".$r["class_id"];
        if ($r["atrb_id"])  $cond_ccr .= "     and concurrent.emp_attribute=".$r["atrb_id"];
        if ($r["dept_id"])  $cond_ccr .= "     and concurrent.emp_dept=".$r["dept_id"];
        if ($r["class_id"]) $cond_ccr .= " )";
    }
    $cond_ccr .= "))";
    $cond .= $cond_ccr;
    $cond .= ") ";
}
$cond .= "order by w.yyyymm, w.emp_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while($row = pg_fetch_array($sel)){
	$e_id       = $row["emp_id"];
	$e_personal = $row["emp_personal_id"];
	$e_lt       = $row["emp_lt_nm"];
	$e_ft       = $row["emp_ft_nm"];
	$e_st       = $row["st_nm"];
	$e_job      = $row["job_nm"];
	$e_class    = $row["class_nm"];
	$e_atrb     = $row["atrb_nm"];
	$e_dept     = $row["dept_nm"];
	$e_yyyymm   = $row["yyyymm"];
	$overtime_hour = (empty($row["overtime_hour"])) ? "0" : $row["overtime_hour"];
	$overtime_min = (empty($row["overtime_min"])) ? "0" : $row["overtime_min"];
	$e_time = $overtime_hour . ":" . $overtime_min;
	
	switch($row["duty_form"]){
		case "1":
			$e_duty = "���";
			break;
		case "2":
			$e_duty = "����";
			break;
		case "3":
			$e_duty = "û����������";
	}

	if ($duty_type_flag == "t"){
		switch($row["duty_form_type"]){
			case "1":
				$e_duty_type = "������";
				break;
			case "2":
				$e_duty_type = "�Ƹ��ѿ���";
				break;
			case "3":
				$e_duty_type = "��������";
				break;
			case "4":
				$e_duty_type = "�׻�����";
				break;
			case "5":
				$e_duty_type = "�ѡ��ȿ���";
				break;
		}
	}

	if(!$emp_info[$e_id]){
		if ($duty_type_flag == "t"){
			$emp_info[$e_id] = array($e_personal
									, $e_lt . " " . $e_ft
									, $e_job
									, $e_st
									, $e_duty
									, $e_duty_type
									, $e_class
									, $e_atrb
									, $e_dept
							   		);
		}else{
			$emp_info[$e_id] = array($e_personal
									, $e_lt . " " . $e_ft
									, $e_job
									, $e_st
									, $e_duty
									, $e_class
									, $e_atrb
									, $e_dept
									);
		}
	}

	if ($data_list[$e_id][$e_yyyymm]){
		// ��ʣ�ξ��ϲû�����
		$arr_time = explode(":", $data_list[$e_id][$e_yyyymm]);
		$tmp_hour = intval($arr_time[0]) + intval($overtime_hour);
		$tmp_min  = intval($arr_time[1]) + intval($overtime_min);
		
		$result_hour = intval($tmp_hour) + floor($tmp_min/60);
		$result_min  = sprintf("%02d", $tmp_min%60);
		
		$data_list[$e_id][$e_yyyymm] = $result_hour . ":" . $result_min;
	}else{
		$data_list[$e_id][$e_yyyymm] = $e_time;
	}
}


// �����CSV�����Ǽ���
$csv = get_list_csv($emp_info, $data_list, $cls_name, $atrb_name, $dept_name, $start_date, $end_date, $duty_type_flag);

// �ǡ����١�����³���Ĥ���
pg_close($con);

// CSV�����
$file_name = "OvertimeDepartment_" . $start_date . "-" . $end_date . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// �ؿ�
//------------------------------------------------------------------------------

// �����CSV�����Ǽ���
function get_list_csv($emp_info, $data_list, $cls_name, $atrb_name, 
					  $dept_name, $start_date, $end_date, $duty_type_flag="f") {
	
	if ($duty_type_flag == "t"){
		$titles = array(
					"����ID",
					"��̾",					
					"����",
					"��",
					"���Ѷ�̳����",
					"���Ѷ�ʬ",
					$cls_name,				
					$atrb_name,
					$dept_name					
				);
	}else{
		$titles = array(
					"����ID",
					"��̾",
					"����",
					"��",
					"���Ѷ�̳����",
					$cls_name,				
					$atrb_name,
					$dept_name					
				);
	}

 	$start_mm = strtotime($start_date . "01");
 	$end_mm = strtotime($end_date . "01");
 	$start_mon = date("Y", $start_mm) * 12 + date("m", $start_mm);
 	$end_mon = date("Y", $end_mm) * 12 + date("m", $end_mm);
 	$mon_count =  $end_mon - $start_mon; //�����׻�
	
	for ($i = 0; $i <= $mon_count; $i++){
		$buf = date("n", strtotime("$i month", $start_mm)) . "��";
		array_push($titles, $buf);
	}
	
	foreach ($data_list as $key=>$item){
		for ($i = 0; $i <= $mon_count; $i++){
			$buf_ym = date("Ym", strtotime("$i month", $start_mm));
			
			if ($item[$buf_ym]){
				$emp_info[$key][] = $item[$buf_ym];
			}else{
				$emp_info[$key][] = "0:00";
			}
		}
	}
	
	$item_num = count($titles);
	
	$buf = "";
	$buf .= $start_date . "," . $end_date . "\r\n";
	
	for ($j=0;$j<$item_num;$j++) {
		if ($j != 0) {
			$buf .= ",";
		}
		$buf .= $titles[$j];
	}
	$buf .= "\r\n";

	foreach ($emp_info as $key=>$item){
		for ($j=0;$j<$item_num;$j++) {
			if ($j != 0) {
				$buf .= ",";
			}
			$buf .= $emp_info[$key][$j];
		}
		$buf .= "\r\n";
	}
	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}

?>