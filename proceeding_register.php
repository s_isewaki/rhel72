<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 議事録作成</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("show_facility.ini");

// セッションのチェック
$session = qualify_session($session, $fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo ("<script type=\"text/javascript\">alert('ユーザ権限がありません。');</script>");
	echo ("<script type=\"text/javascript\">window.close();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);


// ログインユーザの職員IDを取得
$sql = "select emp_id from session where session_id = '$session'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");


// 委員会・WGスケジュール情報を取得
$table_name = ($timeless != "t") ? "proschd" : "proschd2";
$sql = "select * from $table_name";
$cond = "where pjt_schd_id = '$pjt_schd_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pjt_id = pg_fetch_result($sel, 0, "pjt_id");

$conf = new Cmx_SystemConfig();
$link_to_yoshiki9 = ($conf->get('project.link_to_yoshiki9') == 't');

// 初期表示時
if ($back != "t") {
	$prcd_place_id = pg_fetch_result($sel, 0, "pjt_schd_place_id");
	$prcd_place_detail = pg_fetch_result($sel, 0, "pjt_schd_plc");
	$prcd_date = pg_fetch_result($sel, 0, "pjt_schd_start_date");
	$tmp_start_time = pg_fetch_result($sel, 0, "pjt_schd_start_time_v");
	$prcd_start_time = substr($tmp_start_time,0,2).":".substr($tmp_start_time,2,2);
	$tmp_end_time = pg_fetch_result($sel, 0, "pjt_schd_dur_v");
	$prcd_end_time = substr($tmp_end_time,0,2).":".substr($tmp_end_time,2,2);
	$prcd_year = substr($prcd_date, 0, 4);
	$prcd_month = substr($prcd_date, 5, 2);
	$prcd_day = substr($prcd_date, 8, 2);
	$prcd_start_hour = substr($prcd_start_time, 0, 2);
	$prcd_start_min = substr($prcd_start_time, 3, 2);
	$prcd_end_hour = substr($prcd_end_time, 0, 2);
	$prcd_end_min = substr($prcd_end_time, 3, 2);

	// 承認者情報(設定部分)を取得〜配列に格納
	$sql = "select * from prcdaprv_mng_default ";
	$cond = "where pjt_id = '$pjt_id' order by prcdaprv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_def_mng_ap = array();
	$cnt = 0;
	while ($row = pg_fetch_array($sel)) 
	{
		$arr_def_mng_ap[$cnt]["notice"] = $row["next_notice_div"];
		$arr_def_mng_ap[$cnt]["order"] =  $row["prcdaprv_order"];
		$cnt++;
		
	}
	
	// 承認者情報を取得〜配列に格納
	$sql = "select prcdaprv_default.prcdaprv_order, prcdaprv_default.prcdaprv_no, prcdaprv_default.prcdaprv_emp_id, 
			empmst.emp_lt_nm, empmst.emp_ft_nm from prcdaprv_default inner join empmst on prcdaprv_default.prcdaprv_emp_id = empmst.emp_id";
	$cond = "where pjt_id = '$pjt_id' order by prcdaprv_order, prcdaprv_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == "0") {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$arr_def_ap_user = array();
	$cnt = 0;
	$level_cnt_forward = "";
	while ($row = pg_fetch_array($sel)) 
	{
		$tmp_emp_nm = "{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}";
		
		$level_cnt = ($row["prcdaprv_order"] - 1);
		
		if($cnt == 0)
		{
			//初期値設定
			$level_cnt_forward = $level_cnt;
		}
		else
		{
			if($level_cnt_forward != $level_cnt)
			{
				//階層が変わったので配列を変えます
				$level_cnt_forward = $level_cnt;
				$cnt = 0;
			}
		}
		
		$arr_def_ap_user[$level_cnt][$cnt]["id"] =$row["prcdaprv_emp_id"];
		$arr_def_ap_user[$level_cnt][$cnt]["order"] = $row["prcdaprv_order"];
		$arr_def_ap_user[$level_cnt][$cnt]["name"] = $tmp_emp_nm;
		//$arr_def_mng_ap[$cnt]["no"] =$row["prcdaprv_no"];
		
		$cnt++;
		
	}
	


	
}
else
{
	//ポストバック
	
	// メンバー情報を配列に格納
	$arr_target['1'] = array();
	if ($target_id_list1 != "") {
		$arr_target_id = split(",", $target_id_list1);
		for ($i = 0; $i < count($arr_target_id); $i++) {
			$tmp_emp_id = $arr_target_id[$i];
			$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
			$cond = "where emp_id = '$tmp_emp_id'";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type='text/javascript' src='./js/showpage.js'></script>");
				echo("<script language='javascript'>showErrorPage(window);</script>");
			}
			$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
			if ($tmp_emp_id == $schd_emp_id) {
				$schd_emp_name = $tmp_emp_name;
			}
			array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
		}
	}
}

// 委員会名（WG名）を取得
$sql = "select project.pjt_name, parent.pjt_name as parent_name from project left join project parent on project.pjt_parent_id = parent.pjt_id";
$cond = "where project.pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$pjt_name = pg_fetch_result($sel, 0, "pjt_name");
$parent_name = pg_fetch_result($sel, 0, "parent_name");
if ($parent_name != "") {
	$pjt_name = "$parent_name &gt; $pjt_name";
}

// ログインユーザの氏名を取得
$sql = "select emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$prcd_create_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . ' ' . pg_fetch_result($sel, 0, "emp_ft_nm");



// 時刻指定なしの場合、デフォルト時刻を9時とする
if ($prcd_start_hour == "") {$prcd_start_hour = 9;}
if ($prcd_end_hour == "") {$prcd_end_hour = 9;}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<?
//require("project_emplist_caller_javascript.php");
require("emplist_caller_javascript.php");
insert_javascript();
?>

<script type="text/javascript">
if(!tinyMCE.isOpera)
{
	tinyMCE.init({
		mode : "exact",
		elements : "prcd_summary",
		theme : "advanced",
		plugins : "preview,table,emotions,fullscreen,layer",
		//language : "ja_euc-jp",
		language : "ja",
		width : "100%",
		height : "300",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,forecolor,backcolor,|,removeformat",
		theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,hr,|,charmap,emotions,|,preview,|,undo,redo,|,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid",
		content_css : "tinymce/tinymce_content.css",
		theme_advanced_statusbar_location : "none",
		force_br_newlines : true,
		forced_root_block : '',
		force_p_newlines : false
	});
}

function showAprvFields() {
	document.prcd.action = 'proceeding_register.php';
	document.prcd.submit();
}

function attachFile() {
	window.open('proceeding_attach.php?session=<? echo($session); ?>', 'newwin3', 'width=640,height=480,scrollbars=yes');
}

function detachFile(e) {
	if (e == undefined) {
		e = window.event;
	}

	var btn_id;
	if (e.target) {
		btn_id = e.target.getAttribute('id');
	} else {
		btn_id = e.srcElement.id;
	}
	var id = btn_id.replace('btn_', '');

	var p = document.getElementById('p_' + id);
	document.getElementById('attach').removeChild(p);
}

function checkEndMinute() {
	if (document.prcd.prcd_end_hour.value == '24' &&
		document.prcd.prcd_end_min.value != '00') {
		document.prcd.prcd_end_min.value = '00';
		alert('終了時刻が24時の場合には00分しか選べません。');
	}
}


//ここから変更


var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode='+item_id;
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}


function closeEmployeeList() 
{
	//登録前チェック

	var radioList = document.getElementsByName("force_aprv_flag");
	for(var i=0; i<radioList.length; i++)
	{
		if (radioList[i].checked) 
		{
			if(radioList[i].value == 't')
			{
				//必須入力チェックあり
				if(m_target_list['3'].length < 1)
				{
					alert("承認者を設定してください");
					return false;
				}
			}
		}
	}


	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;

	document.prcd.submit();
	
}


function onDraft() {
	//下書き保存用のフラグを設定する。
	document.prcd.draft_flg.value = "t";

	document.prcd.submit();
}

function checkApprove()
{
	document.prcd.action = "proceeding_register.php";
	document.prcd.submit();
}


//ここまで変更

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>議事録作成</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="prcd" action="proceeding_insert.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

	<tr height="22">
		<td width="100" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG名</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($pjt_name); ?></font></td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議題</font></td>
		<td><input type="text" name="prcd_subject" value="<? echo($prcd_subject); ?>" size="65" maxlength="200" style="ime-mode:active;"></td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">場所</font></td>
		<td><select name="prcd_place_id"><? show_schedule_place_selected($con, $prcd_place_id, $fname); ?><option value="0"<? if ($prcd_place_id == 0) {echo(" selected");} ?>>その他</select>&nbsp;<input type="text" name="prcd_place_detail" value="<? echo($prcd_place_detail); ?>" size="35" maxlength="100" style="ime-mode:active;"></td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="prcd_year"><? show_update_years($prcd_year, 1); ?></select>/<select name="prcd_month"><? show_select_months($prcd_month); ?></select>/<select name="prcd_day"><? show_select_days($prcd_day); ?></select></font></td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><select name="prcd_start_hour">
		<? show_hour_options_0_23($prcd_start_hour, false); ?></select>
		：
		<select name="prcd_start_min"><? show_min_options_5($prcd_start_min, false); ?></select>
		〜
		<select name="prcd_end_hour" onchange="checkEndMinute();">
		<? show_select_hrs_by_args(0, 24, $prcd_end_hour, false); ?></select>
		：
		<select name="prcd_end_min" onchange="checkEndMinute();"><? show_min_options_5($prcd_end_min, false); ?></select></font>
		</td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('proceeding_member.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>', 'promem', 'width=640,height=480,scrollbars=yes');">出席者</a></font></td>
		<td><textarea name="prcd_person" cols="50" rows="3" style="ime-mode:active;"<? if ($link_to_yoshiki9) {echo(" readonly");} ?>><? echo($prcd_person); ?></textarea><input type="hidden" name="prcd_atnd" value="<? echo($prcd_atnd); ?>"></td>
	</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">欠席者</font></td>
		<td><textarea name="prcd_absentee" cols="50" rows="3" style="ime-mode:active;"><? echo($prcd_absentee); ?></textarea></td>
	</tr>

		<tr height="22">
			<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">記録者</font></td>
			<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($prcd_create_emp_nm); ?></font></td>
		</tr>

	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">概要</font></td>
		<td><textarea name="prcd_summary" cols="50" rows="20" style="ime-mode:active;"><? echo($prcd_summary); ?></textarea></td>
	</tr>


	<tr height="22">
		<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">添付ファイル</font></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
		<div id="attach">
		<?
		for ($i = 0; $i < count($filename); $i++) {
			$tmp_file_id = $file_id[$i];
			$tmp_filename = $filename[$i];
			$ext = strrchr($tmp_filename, ".");

			echo("<p id=\"p_{$tmp_file_id}\">\n");
		//	echo("<a href=\"proceeding/tmp/{$session}_{$tmp_file_id}{$ext}\" target=\"_blank\">{$tmp_filename}</a>\n");
			echo("<a href=\"proceeding_attach_refer.php?session=$session&file_id=$tmp_file_id&filename=" . urlencode($tmp_filename) . "\">$tmp_filename</a>\n");
			echo("<input type=\"button\" id=\"btn_{$tmp_file_id}\" name=\"btn_{$tmp_file_id}\" value=\"削除\" onclick=\"detachFile(event);\">\n");
			echo("<input type=\"hidden\" name=\"filename[]\" value=\"{$tmp_filename}\">\n");
			echo("<input type=\"hidden\" name=\"file_id[]\" value=\"{$tmp_file_id}\">\n");
			echo("</p>\n");
		}
		?>
		</div>
		<input type="button" value="追加" onclick="attachFile();">
		</font></td>
	</tr>

<?

if (($back != "t") && ($ovtm_approve_num ==""))
{
	//初期表示の場合
	$ovtm_approve_num = count($arr_def_mng_ap);
}

?>

<!-- 承認階層数 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="ovtm_kaisofont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >承認階層数</font></td>
<td colspan="2">
<input type="hidden" name="ovtm_approve_num" value="<? echo($ovtm_approve_num); //画面遷移する際にdisableだとデータが渡せないので影で持つ?>">
<select name="ovtm_approve_num" id="ovtm_approve_num" onChange="checkApprove();"><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $ovtm_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
<?
//追加部分↓

$ovtm_approve = array($ovtm_approve1, $ovtm_approve2, $ovtm_approve3, $ovtm_approve4, $ovtm_approve5, $ovtm_approve6, $ovtm_approve7, $ovtm_approve8, $ovtm_approve9, $ovtm_approve10, $ovtm_approve11, $ovtm_approve12, $ovtm_approve13, $ovtm_approve14, $ovtm_approve15, $ovtm_approve16, $ovtm_approve17, $ovtm_approve18, $ovtm_approve19, $ovtm_approve20);

$ovtm_emp_id = array($ovtm_emp_id1, $ovtm_emp_id2, $ovtm_emp_id3, $ovtm_emp_id4, $ovtm_emp_id5, $ovtm_emp_id6, $ovtm_emp_id7, $ovtm_emp_id8, $ovtm_emp_id9, $ovtm_emp_id10, $ovtm_emp_id11, $ovtm_emp_id12, $ovtm_emp_id13, $ovtm_emp_id14, $ovtm_emp_id15, $ovtm_emp_id16, $ovtm_emp_id17, $ovtm_emp_id18, $ovtm_emp_id19, $ovtm_emp_id20);
$ovtm_emp_nm = array($ovtm_emp_nm1, $ovtm_emp_nm2, $ovtm_emp_nm3, $ovtm_emp_nm4, $ovtm_emp_nm5, $ovtm_emp_nm6, $ovtm_emp_nm7, $ovtm_emp_nm8, $ovtm_emp_nm9, $ovtm_emp_nm10, $ovtm_emp_nm11, $ovtm_emp_nm12, $ovtm_emp_nm13, $ovtm_emp_nm14, $ovtm_emp_nm15, $ovtm_emp_nm16, $ovtm_emp_nm17, $ovtm_emp_nm18, $ovtm_emp_nm19, $ovtm_emp_nm20);

$ovtm_multi_apv_flg = array($ovtm_multi_apv_flg1, $ovtm_multi_apv_flg2, $ovtm_multi_apv_flg3, $ovtm_multi_apv_flg4, $ovtm_multi_apv_flg5, $ovtm_multi_apv_flg6, $ovtm_multi_apv_flg7, $ovtm_multi_apv_flg8, $ovtm_multi_apv_flg9, $ovtm_multi_apv_flg10, $ovtm_multi_apv_flg11, $ovtm_multi_apv_flg12, $ovtm_multi_apv_flg13, $ovtm_multi_apv_flg14, $ovtm_multi_apv_flg15, $ovtm_multi_apv_flg16, $ovtm_multi_apv_flg17, $ovtm_multi_apv_flg18, $ovtm_multi_apv_flg19, $ovtm_multi_apv_flg20);
$ovtm_next_notice_div = array($ovtm_next_notice_div1, $ovtm_next_notice_div2, $ovtm_next_notice_div3, $ovtm_next_notice_div4, $ovtm_next_notice_div5, $ovtm_next_notice_div6, $ovtm_next_notice_div7, $ovtm_next_notice_div8, $ovtm_next_notice_div9, $ovtm_next_notice_div10, $ovtm_next_notice_div11, $ovtm_next_notice_div12, $ovtm_next_notice_div13, $ovtm_next_notice_div14, $ovtm_next_notice_div15, $ovtm_next_notice_div16, $ovtm_next_notice_div17, $ovtm_next_notice_div18, $ovtm_next_notice_div19, $ovtm_next_notice_div20);

$ovtm_apv_num = array($ovtm_apv_num1, $ovtm_apv_num2, $ovtm_apv_num3, $ovtm_apv_num4, $ovtm_apv_num5, $ovtm_apv_num6, $ovtm_apv_num7, $ovtm_apv_num8, $ovtm_apv_num9, $ovtm_apv_num10, $ovtm_apv_num11, $ovtm_apv_num12, $ovtm_apv_num13, $ovtm_apv_num14, $ovtm_apv_num15, $ovtm_apv_num16, $ovtm_apv_num17, $ovtm_apv_num18, $ovtm_apv_num19, $ovtm_apv_num20);

for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;
	
	if ($back == "t") 
	{
		//画面遷移のエラーチェックで戻ってきた場合
			
		$str_emp_idW = "ovtm_emp_id".$j;
		$str_emp_id = $$str_emp_idW;
		
		$str_emp_nmW = "ovtm_emp_nm".$j;
		$str_emp_nm = $$str_emp_nmW;
		
		//$str_contentW = "ovtm_approve_content".$j;
		//$str_content = $$str_contentW;
		
		$str_noticeW = "ovtm_next_notice_div".$j;
		$str_notice = $$str_noticeW;
		
		$default_str_emp_idW = "ovtm_emp_id_def".$j;
		$default_str_emp_id = $$default_str_emp_idW;

	}
	else
	{
		//初期表示の場合

		if($arr_def_ap_user[$i][0]["name"] != "" )
		{
			
			for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
			{
				if($wcnt1 == 0)
				{
					$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
					$str_emp_id = $arr_def_ap_user[$i][$wcnt1]["id"];
					$default_str_emp_id = $arr_def_ap_user[$i][$wcnt1]["id"];//初期値確保用承認者データ
				}
				else
				{
					$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
					$str_emp_id = $str_emp_id.",".$arr_def_ap_user[$i][$wcnt1]["id"];
					$default_str_emp_id = $str_emp_id.", ".$arr_def_ap_user[$i][$wcnt1]["id"];//初期値確保用承認者データ
				}			
			}
		}
		else
		{
			$str_emp_nm = "";
			$str_emp_id = "";
			$default_str_emp_id = "";
		}
		
		$str_notice = $arr_def_mng_ap[$i]["notice"];
		
	}
	
	
	
	//	if($ovtm_wkfw_div == "1"){
	$linktarget = "window.open('project_approval_list.php?session=$session&approve=$j&maxCnt=$ovtm_approve_num&from_parent_st=prcd&wkfw_nm=ovtm', 'newwin3', 'width=640,height=700,scrollbars=yes')";
	$fontcolor = "";
	$approve_content_color = "green";
	//	}
	//	else{
	//		$linktarget = "";
	//		$fontcolor = "#a9a9a9";
	//		$approve_content_color = "#a9a9a9";
	//	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"ovtm_kaisou". $j ."\" href=\"javascript:void(0);\" onclick=\"" . $linktarget . "\"><font id=\"ovtm_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></a></font></td>\n");
	
	echo("<td colspan=\"2\">");
	echo("<font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\">");
	echo("<span id=\"ovtm_approve_content". $j ."\"></span></font>\n");
	
	//	echo("<td colspan=\"2\"><font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"target_disp_area3\"></span></font>");
	
	/*	
		$ovtm_multi_apv_flg[$i] = ($ovtm_multi_apv_flg[$i] == "") ? "f" : $ovtm_multi_apv_flg[$i];
		$ovtm_next_notice_div[$i] = ($ovtm_next_notice_div[$i] == "") ? "1" : $ovtm_next_notice_div[$i];
		$ovtm_apv_num[$i] = ($ovtm_apv_num[$i] == "") ? "1" : $ovtm_apv_num[$i];
	*/	
	//echo("<input type=\"hidden\" name=\"ovtm_approve" . $j . "\" value=\"" . $ovtm_approve[$i] . "\">\n");
	
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"".$str_emp_id."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"".$str_emp_nm."\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id_def" . $j . "\" value=\"".$default_str_emp_id."\">\n");

	//echo("<input type=\"hidden\" name=\"ovtm_multi_apv_flg" . $j . "\" value=\"" . $ovtm_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"".$str_notice."\">\n");
	
	//echo("<input type=\"hidden\" name=\"ovtm_apv_num" . $j . "\" value=\"" . $ovtm_apv_num[$i] . "\">\n");
	
	echo("</td>\n");
	echo("</tr>\n");
	
}
//追加部分↑

?>





</table>


</td>
</tr>



</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="下書き保存" onclick="onDraft();"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_schd_id" value="<? echo($pjt_schd_id); ?>">
<input type="hidden" name="timeless" value="<? echo($timeless); ?>">
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" id="draft_flg" name="draft_flg" value="">
</form>
</center>



<script type="text/javascript">
// 承認階層
<?
for ($i = 0; $i < $ovtm_approve_num; $i++)
{
	$j = $i+1;
	


	if ($back == "t") 
	{
		$str_emp_nmW = "ovtm_emp_nm".$j;
		$str_emp_nm = $$str_emp_nmW;
	}
	else
	{
		//初期表示の場合
		
		
		if($arr_def_ap_user[$i][0]["name"] != "" )
		{
			
			for ($wcnt1 = 0; $wcnt1 < count($arr_def_ap_user[$i]); $wcnt1++)
			{
				if($wcnt1 == 0)
				{
					$str_emp_nm = $arr_def_ap_user[$i][$wcnt1]["name"];
				}
				else
				{
					$str_emp_nm = $str_emp_nm.", ".$arr_def_ap_user[$i][$wcnt1]["name"];
				}			
			}
		}
		else
		{
			$str_emp_nm = "";
			$str_emp_id = "";
		}
	}
	
	
?>
document.getElementById('ovtm_approve_content<?=$j?>').innerHTML = '<?=$str_emp_nm?>';
	<?
}
?>
</script>



</body>
<? pg_close($con); ?>
</html>
