<?php
require_once('about_comedix.php');

// 出張旅費・共通関数

// 申請状態
$array_apply_stat = array('未承認', '承認確定', '否認', '差戻し');

// 都道府県
$array_pref = array("北海道","青森県","岩手県","宮城県","秋田県","山形県","福島県","茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県","新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県","静岡県","愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県","鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県","高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県");

// 区分
$array_kubun = array('','出張(日帰り)','出張(4日間以内の宿泊)','出張(5日間以上の宿泊)','長期日帰り(同一出張先に連続して3週間以上)','長期宿泊出張(同一出張先に連続して2週間以上)','自己研修(日帰り)','自己研修(4日間以内の宿泊)','自己研修(5日間以上の宿泊)','公用外出');

// 要件
$array_youken = array('','学会','学会以外');

// 演題発表
$array_presentation = array('無','有');

// 主演
$array_act = array('','主演','共演');

// 交通手段
$array_transit = array('','公共交通機関','社用車','タクシー','航空機','宿泊と交通費が組み込まれたビジネスパック、出張パック','私有車');

// 謝礼
$array_sharei = array('','なし','あり（出張者本人が受領）','あり（病院が受領）');

// 前渡金
$array_prepayment = array('','前渡金を申請しない','前渡金を申請する');

// 前渡しチケット
$array_preticket = array('','チケットを申請しない','チケットを申請する');

// 宿泊地
$array_stay = array('', '東京23区内', '東京23区以外');

// 往復
$array_oufuku = array('', '片道', '往復');

// ビジネスパック内容
$array_tt2type = array('', '交通費のみ', '宿泊のみ', '交通費＋宿泊', 'マンスリーアパート', 'その他');

// 私有車区分
$array_tt3type = array('', '私有車', '高速料金', '駐車料金');

// 私有車車種
$array_tt3car = array('', '普通車', '軽自動車');

//-----------------------------------------------
// 出張届クラス
//-----------------------------------------------
class business_trip1
{
    var $rows;

    // Constractor
    function business_trip1($apply_id, $param_cond)
    {
        global $con, $fname;

        $sql = <<<__SQL_END__
SELECT a.*,wkfw_title,t2.apply_stat as t2_apply_stat
FROM apply a
__SQL_END__;

        if (!$apply_id) {
            $sql .= " LEFT JOIN session s USING (emp_id) ";
        }
        $sql .= " LEFT JOIN wkfwmst USING (wkfw_id) ";
        $sql .= " LEFT JOIN trip_link tl ON a.apply_id=tl.trip1_id ";
        $sql .= " LEFT JOIN apply t2 ON tl.trip2_id=t2.apply_id AND NOT t2.delete_flg";

        if ($apply_id) {
            $cond = " WHERE tl.trip2_id='{$apply_id}'";
        }
        else {
            $param_cond .= !empty($param_cond) ? ' AND ' : '';
            $cond = <<<__SQL_END__
WHERE {$param_cond} not a.draft_flg AND not a.delete_flg AND a.re_apply_id IS NULL AND a.wkfw_id IN (SELECT wkfw_id FROM trip_workflow WHERE type=1)
ORDER BY a.apply_date DESC
LIMIT 20
__SQL_END__;
        }

        $sel = select_from_table($con,$sql,$cond,$fname);
        if ($sel == 0) {
            die;
        }
        $this->rows = pg_num_rows($sel) > 0 ? pg_fetch_all($sel) : array();
    }

    // is_row
    function is_row()
    {
        return !empty($this->rows) ? true : false;
    }

    // show select_options
    function show_select_options($flg=false)
    {
        $array_apply_stat2 = array("" => '[未報告]', "0" => '[報告済(申請中)]', "1" => '[報告済(承認確定)]', "2" => '[報告済(否認)]', "3" => '[報告済(差戻し)]');
        $data = array();
        foreach ($this->rows as $r) {
            $apply_stat = $flg ? $array_apply_stat2[ $r['t2_apply_stat'] ] : "";
            $content = get_array_apply_content($r['apply_content']);
            $data[] = sprintf('<option value="%s">%04d/%02d/%02d-%04d/%02d/%02d: %s: (%s) %s</option>',
                    $r['apply_id'],
                    $content['date_y1'], $content['date_m1'], $content['date_d1'],
                    $content['date_y2'], $content['date_m2'], $content['date_d2'],
                    $r['apply_title'],
                    $r['wkfw_title'],
                    $apply_stat
                    );
        }
        print implode("",$data);
    }

    // show javascript_vars
    function show_javascript_vars()
    {
        $data = array();
        foreach ($this->rows as $r) {
            $content = get_array_apply_content($r['apply_content']);
            $json_array = array_merge($r, $content);
            unset($json_array['apply_content']);
            $data[] = sprintf("trip1['%s'] = %s;\n", $r['apply_id'], cmx_json_encode($json_array));
        }
        print "trip1 = new Array();\n" . implode("",$data);
    }
}

//-----------------------------------------------
// 注意事項データの取得
//-----------------------------------------------
function get_trip_attension() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_attension WHERE view_flg='t'";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $attension = array();
    while ($r = pg_fetch_assoc($sel)) {
        $attension[ $r["attension_id"] ] = $r["attension"];
    }
    return $attension;
}

//-----------------------------------------------
// 交通機関データの取得
//-----------------------------------------------
function get_trip_transport() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_transport WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[$r["transport_id"]] = $r["title"];
    }
    return $options;
}

//-----------------------------------------------
// 交通機関データの取得(select_options)
//-----------------------------------------------
function get_trip_transport_select_options() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_transport WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[] = sprintf('<option value="%s">%s</option>', $r["transport_id"], $r["title"]);
    }
    return implode("", $options);
}

//-----------------------------------------------
// 新幹線駅名データの取得
//-----------------------------------------------
function get_trip_station() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_station WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[$r["station_id"]] = $r["title"];
    }
    return $options;
}

//-----------------------------------------------
// 新幹線駅名データの取得(select_options)
//-----------------------------------------------
function get_trip_station_select_options() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_station WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[] = sprintf('<option value="%s">%s</option>', $r["station_id"], $r["title"]);
    }
    return implode("", $options);
}

//-----------------------------------------------
// 空港名データの取得
//-----------------------------------------------
function get_trip_airport() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_airport WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[$r["airport_id"]] = $r["title"];
    }
    return $options;
}

//-----------------------------------------------
// 空港名データの取得(select_options)
//-----------------------------------------------
function get_trip_airport_select_options() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_airport WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    $options = array();
    while ($r = pg_fetch_assoc($sel)) {
        $options[] = sprintf('<option value="%s">%s</option>', $r["airport_id"], $r["title"]);
    }
    return implode("", $options);
}

//-----------------------------------------------
// 経路データの取得
//-----------------------------------------------
function get_trip_transit() {
    global $con, $fname;
    $sql = "SELECT * FROM trip_transit WHERE view_flg='t' ORDER BY no";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    return pg_num_rows($sel) > 0 ? pg_fetch_all($sel) : array();
}

//-----------------------------------------------
// 個人経路データの取得
//-----------------------------------------------
function get_trip_transit_emp($session) {
    global $con, $fname;
    $sql = <<<__SQL_END__
SELECT transport_id,oufuku,from_name,to_name,fee
FROM trip_transit_emp
JOIN session USING (emp_id)
JOIN apply a USING (apply_id)
WHERE apply_stat='1'
AND NOT delete_flg
AND session_id='{$session}'
GROUP BY transport_id,oufuku,from_name,to_name,fee
ORDER BY COUNT(*) DESC
__SQL_END__;

    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    return pg_num_rows($sel) > 0 ? pg_fetch_all($sel) : array();
}

//-----------------------------------------------
// 支給基準データの取得
//-----------------------------------------------
function get_trip_payment($session) {
    global $con, $fname;
    $sql = "SELECT tp.* from trip_payment tp JOIN empmst ON st_id=emp_st JOIN session USING (emp_id) WHERE session_id='$session' AND tp.view_flg='t';";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    return pg_num_rows($sel) == 1 ? pg_fetch_assoc($sel) : array("hotel" => 0, "hotel_tokyo" => 0, "stay" => 0, "daytrip" => 0);
}

//-----------------------------------------------
// 支給基準データの取得(st_id, type指定)
//-----------------------------------------------
function get_trip_payment_st_and_type($st_id, $type) {
    global $con, $fname;
    $sql = "SELECT * from trip_payment WHERE st_id=$st_id AND type=$type AND view_flg='t';";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    return pg_num_rows($sel) == 1 ? pg_fetch_assoc($sel) : array("hotel" => 0, "hotel_tokyo" => 0, "stay" => 0, "daytrip" => 0);
}

//-----------------------------------------------
// オプションデータの取得
//-----------------------------------------------
function get_trip_option() {
    global $con, $fname;
    $sql = "SELECT * from trip_option";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    return pg_num_rows($sel) == 1 ? pg_fetch_assoc($sel) : array();
}

//-----------------------------------------------
// チケットメール宛先の取得
//-----------------------------------------------
function get_trip_preticket_to() {
    require("webmail/config/config.php");
    global $con, $fname;
    $sql = "SELECT get_mail_login_id(emp_id) as mail_id, emp_lt_nm, emp_ft_nm FROM trip_preticket_mail LEFT JOIN empmst USING (emp_id)";
    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }

    $to_list = array();
    while ($row = pg_fetch_assoc($sel)) {
        $emp_nm = $row["emp_lt_nm"].$row["emp_ft_nm"];
        $address = mb_encode_mimeheader(mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <{$row["mail_id"]}@$domain>";
        $to_list[] = array(
                "email" => "{$row["mail_id"]}@$domain",
                "address" => $address
        );
    }
    return $to_list;
}

//-----------------------------------------------
// チケットメール送信者の取得
//-----------------------------------------------
function get_trip_preticket_from($emp_id) {
    require("webmail/config/config.php");
    global $con, $fname;
    $sql = <<<__SQL_END__
SELECT get_mail_login_id(emp_id) as mail_id, emp_personal_id, emp_lt_nm, emp_ft_nm, c1.class_nm, c2.atrb_nm, c3.dept_nm, c4.room_nm
FROM empmst e
LEFT JOIN classmst c1 ON e.emp_class = c1.class_id
LEFT JOIN atrbmst c2 ON e.emp_attribute = c2.atrb_id
LEFT JOIN deptmst c3 ON e.emp_dept = c3.dept_id
LEFT JOIN classroom c4 ON e.emp_room = c4.room_id
WHERE emp_id='{$emp_id}'
__SQL_END__;

    $sel = select_from_table($con,$sql,"",$fname);
    if ($sel == 0) {
        die;
    }
    if (pg_num_rows($sel) != 1) {
        return array();
    }
    $row = pg_fetch_assoc($sel);
    $emp_nm = $row["emp_lt_nm"].$row["emp_ft_nm"];
    $classes = array($row["class_nm"], $row["atrb_nm"], $row["dept_nm"]);
    if (!empty($row["room_nm"])) {
        $classes[] = $row["room_nm"];
    }
    $address = mb_encode_mimeheader(mb_convert_encoding($emp_nm,"ISO-2022-JP","AUTO")) ." <{$row["mail_id"]}@$domain>";
    return array(
            "id" => $row["emp_personal_id"],
            "name" => $emp_nm,
            "class" => implode(" > ", $classes),
            "email" => "{$row["mail_id"]}@$domain",
            "address" => $address
            );
}

//-----------------------------------------------
// 申請番号取得
//-----------------------------------------------
function get_apply_number($apply_id) {
    global $con, $fname, $wkfw_id, $apply_no;
    if (empty($apply_no)) {
        $sql = "SELECT w.short_wkfw_name, a.apply_date, a.apply_no from apply a JOIN wkfwmst w USING (wkfw_id) WHERE apply_id='{$apply_id}'";
        $sel = select_from_table($con,$sql,"",$fname);
        if ($sel == 0) {
            die;
        }
        if (pg_num_rows($sel) != 1) {
            return "";
        }
        $r = pg_fetch_assoc($sel);
        $y  = substr($r["apply_date"], 0, 4);
        $md = substr($r["apply_date"], 4, 4);
        if ($md >= "0101" && $md <= "0331") {
            $y = $y - 1;
        }
        return sprintf("%s-%s%04d", $r["short_wkfw_name"], $y, $r["apply_no"]);
    }
    else {
        $sql = "SELECT short_wkfw_name FROM wkfwmst WHERE wkfw_id='{$wkfw_id}'";
        $sel = select_from_table($con,$sql,"",$fname);
        if ($sel == 0) {
            die;
        }
        if (pg_num_rows($sel) != 1) {
            return "";
        }
        $short_wkfw_name = pg_fetch_result($sel, 0, 0);
        $y  = date('Y');
        $md = date('md');
        if ($md >= "0101" && $md <= "0331") {
            $y = $y - 1;
        }
        return sprintf("%s-%s%04d", $short_wkfw_name, $y, $apply_no);
    }
}

//-----------------------------------------------
// 決済申請XMLデータをarrayにして返す
//-----------------------------------------------
function get_array_apply_content($apply_content) {
    $apply_content = preg_replace("/[\x01-\x08\x0b\x0c\x0e-\x1f\x7f]+/","",$apply_content);
    $apply_content = str_replace("\0","",$apply_content);
    $doc = domxml_open_mem($apply_content);
    $root = $doc->get_elements_by_tagname('apply');
    $node_array = $root[0]->child_nodes();

    $array = array();
    foreach ($node_array as $child) {
        if ($child->node_type() != XML_ELEMENT_NODE) {
            continue;
        }
        if ($child->get_attribute("type") == 'checkbox') {
            $array[ $child->tagname ][] = mb_convert_encoding($child->get_content(), "eucJP-win", "UTF-8");
        }
        else {
            $array[ $child->tagname ] = mb_convert_encoding($child->get_content(), "eucJP-win", "UTF-8");
        }
    }

    return $array;
}

//-----------------------------------------------
// application_mode
//-----------------------------------------------
function application_mode() {
    if (strpos($_SERVER['PHP_SELF'],'/application_menu.php') !== false) {
        return 'menu';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_apply_detail.php') !== false) {
        return 'apply_detail';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_apply_detail_print.php') !== false) {
        return 'apply_detail_print';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_approve_detail.php') !== false) {
        return 'approve_detail';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_approve_detail_print.php') !== false) {
        return 'approve_detail_print';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_workflow_detail.php') !== false) {
        return 'workflow_detail';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_workflow_detail_print.php') !== false) {
        return 'workflow_detail_print';
    }
    if (strpos($_SERVER['PHP_SELF'],'/application_draft_template_print.php') !== false) {
        return 'draft_template_print';
    }
}

//-----------------------------------------------
// js_array_encode
//-----------------------------------------------
function js_array_encode($a) {
    $array = array();
    foreach ($a as $data) {
        $array[] = sprintf("'%s'", $data);
    }
    return implode(",", $array);
}

//-----------------------------------------------
// show_select_years_for_trip
//-----------------------------------------------
function show_select_years_for_trip($num, $fix, $blank = false, $include_next = false) {
    if ($blank) {
        echo("<option value=\"-\">\n");
    }

    $now = date("Y");

    if ($include_next && date("m") >= 4) {
        $num++;
        $now++;
    }

    if ($fix > 0) {
        $num = ($now - $num + 1 > $fix) ? $now - $fix + 1 : $num;
    }

    for ($i = 0; $i < $num; $i++) {
        $yr = $now - $i;
        echo("<option value=\"$yr\"");
        if ($fix == $yr) {
            echo(" selected");
        }
        echo(">$yr</option>\n");
    }
}
