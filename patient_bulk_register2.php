<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜一括登録2</title>
<?
require_once("about_comedix.php");
require_once("show_select_values.ini");
require_once("label_by_profile_type.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 22, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// データベースに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 初期表示時
if ($mode == "") {

	// 文字コードのデフォルトはShift_JIS
	if ($encoding == "") {$encoding = "1";}

// 登録ボタン押下時
} else {

	// ファイルが正常にアップロードされたかどうかチェック
	$uploaded = false;
	if (array_key_exists("csvfile", $_FILES)) {
		if ($_FILES["csvfile"]["error"] == 0 && $_FILES["csvfile"]["size"] > 0) {
			$uploaded = true;
		}
	}
	if ($uploaded) {
		$result = import_csv($con, $option, $fname);
	}
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function checkForm(form) {
	if (form.csvfile.value == '') {
		alert('ファイルを選択してください。');
		return false;
	}
	return true;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="patient_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="patient_bulk_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一括登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="patient_waiting_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">待機患者一覧</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="patient_bulk_register.php?session=<? echo($session); ?>">タイプ1</a></font></td>
<td style="padding-right:30px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイプ2</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="patient_bulk_register2.php" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
<table name="patient" width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="140" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">CSVファイル</font></td>
<td><input name="csvfile" type="file" value="" size="50"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ファイルの文字コード</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="encoding" value="1"<? if ($encoding == "1") {echo(" checked");} ?>>Shift_JIS（Windows/Mac標準）
<input type="radio" name="encoding" value="2"<? if ($encoding == "2") {echo(" checked");} ?>>EUC-JP
<input type="radio" name="encoding" value="3"<? if ($encoding == "3") {echo(" checked");} ?>>UTF-8
</font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></td>
<td><select name="option">
<option value="0"<? if ($option == "0") {echo(" selected");} ?>>すべて取り込む
<option value="1"<? if ($option == "1") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[1桁]のデータは無視する
<option value="2"<? if ($option == "2") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[2桁]のデータは無視する
<option value="3"<? if ($option == "3") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[3桁]のデータは無視する
<option value="4"<? if ($option == "4") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[4桁]のデータは無視する
<option value="5"<? if ($option == "5") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[5桁]のデータは無視する
<option value="6"<? if ($option == "6") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[6桁]のデータは無視する
<option value="7"<? if ($option == "7") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[7桁]のデータは無視する
<option value="8"<? if ($option == "8") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[8桁]のデータは無視する
<option value="9"<? if ($option == "9") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[9桁]のデータは無視する
<option value="10"<? if ($option == "10") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[10桁]のデータは無視する
<option value="11"<? if ($option == "11") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[11桁]のデータは無視する
<option value="12"<? if ($option == "12") {echo(" selected");} ?>><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が[12桁]のデータは無視する
</select></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="mode" value="import">
</form>
<? if ($mode == "import") { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<? if (!$uploaded) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">ファイルのアップロードに失敗しました。</font></td>
</tr>
<? } else if ($uploaded && $result["failed_count"] > 0) { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="red">
<? if ($result["succeeded_count"] > 0) { ?>
以下のエラーが発生しましたが、登録処理が完了しました。（登録件数：<? echo($result["succeeded_count"]); ?>件、エラー件数：<? echo($result["failed_count"]); ?>件）
<? } else { ?>
以下のエラーが発生しました。（登録件数：0件、エラー件数：<? echo($result["failed_count"]); ?>件）
<? } ?>
</font></td>
</tr>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<ul>
<? foreach ($result["errors"] as $error) { ?>
<li><? echo(h($error)); ?></li>
<? } ?>
</ul>
</font></td>
</tr>
<? } else { ?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録処理が完了しました。（登録件数：<? echo($result["succeeded_count"]); ?>件、エラー件数：0件）</font></td>
</tr>
<? } ?>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<dl style="margin-top:0;">
<dt><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>注意</b></font></dt>
<dd>
<ul style="margin:0;">
<li><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>が重複する場合、データが上書きされます。</font></li>
</ul>
</dd>
</dl>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>CSVのフォーマット</b></font></td>
</tr>
<tr>
<td width="30"></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<p style="margin:0;">下記項目をカンマで区切り、レコードごとに改行します。<? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>〜生年月日は必須です。</p>
<ul style="margin:0;">
<li style="margin-left:40px;"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?>……使える文字は半角英数字のみです。</li>
<li style="margin-left:40px;">氏名（漢字）……苗字と名前はスペース区切り（スペースは全角半角個数問わず）</li>
<li style="margin-left:40px;">氏名（カナ）……同上</li>
<li style="margin-left:40px;">性別……数字1桁（0：不明、1：男性、2：女性）</li>
<li style="margin-left:40px;">生年月日……YYYY/MM/DD形式</li>
<li style="margin-left:40px;">郵便番号……数字7桁</li>
<li style="margin-left:40px;">住所</li>
<li style="margin-left:40px;">電話番号……ハイフン区切り</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>1</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>2</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>3</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>4</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>5</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>6</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>7</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>8</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>9</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>10</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>11</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>12</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>13</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>14</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>15</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>16</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>17</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>18</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>19</li>
<li style="margin-left:40px;"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?>20</li>
</ul>
<dl style="margin:0;">
<dt>例：</dt>
<dd style="margin-left:20px;font-family:monospace;">0000000001,山田 太郎,ﾔﾏﾀﾞ ﾀﾛｳ,1,1955/05/05,1230000,東京都○○区○○,03-1234-5678,000-000001,000-000002,000-000003,000-000004,000-000005,000-000006,000-000007,000-000008,000-000009,000-000010,000-000011,000-000012,000-000013,000-000014,000-000015,000-000016,000-000017,000-000018,000-000019,000-000020</dd>
<dd style="margin-left:20px;font-family:monospace;">0000000002,鈴木 花子,ｽｽﾞｷ ﾊﾅｺ,2,1977/07/07,0004567,北海道○○市○○,0123-456-7890,100-000001,100-000002,100-000003,100-000004,100-000005,100-000006,100-000007,100-000008,100-000009,100-000010,100-000011,100-000012,100-000013,100-000014,100-000015,100-000016,100-000017,100-000018,100-000019,100-000020</dd>
</dl>
</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function import_csv($con, $option, $fname) {
	ini_set("max_execution_time", 0);
	require_once("get_values.ini");
	require_once("label_by_profile_type.ini");

	// 文字コードの設定
	switch ($_POST["encoding"]) {
	case "1":
		$file_encoding = "SJIS";
		break;
	case "2":
		$file_encoding = "EUC-JP";
		break;
	case "3":
		$file_encoding = "UTF-8";
		break;
	default:
		exit;
	}

	// EUC-JPで扱えない文字列は「・」に変換するよう設定
	mb_substitute_character(0x30FB);

	// 文字変換テーブルの取得
	$character_table = get_character_table($file_encoding);

	// 最小カラム数
	$min_column_count = 5;

	// CSVデータを配列に格納
	$patients = array();
	$record_no = 0;
	$lines = file($_FILES["csvfile"]["tmp_name"]);
	foreach ($lines as $line) {

		// 異体字を変換
		$line = convert_encoding($line, $character_table);

		// 文字コードを変換
		$line = trim(mb_convert_encoding($line, "EUC-JP", $file_encoding));

		// EOFを削除
		$line = str_replace(chr(0x1A), "", $line);

		$record_no++;
		$patients[$record_no] = split(",", $line);
	}
	unset($lines);

	// トランザクションを開始
	pg_query($con, "begin");

	// 患者データを登録
	foreach ($patients as $record_no => $patient_data) {
		$ptif_id = trim($patient_data[0]);
		if ($ptif_id == "") $ptif_id = "（不明）";

		if (count($patient_data) < $min_column_count) {
			$errors[] = "行番号：{$record_no}、患者ID：{$ptif_id}、理由：カラム数が不正です";
			continue;
		}

		$error = insert_patient_data($con, $record_no, $patient_data, $file_encoding, $option, $session, $fname);
		if ($error != "") $errors[] = "行番号：{$record_no}、患者ID：{$ptif_id}、理由：{$error}";
	}

	// トランザクションをコミット
	pg_query($con, "commit");

	$failed_count = count($errors);
	return array(
		"succeeded_count" => count($patients) - $failed_count,
		"failed_count" => $failed_count,
		"errors" => $errors
	);
}


// 患者データを登録
function insert_patient_data($con, $record_no, $patient_data, $encoding, $option, $session, $fname) {
	global $_label_by_profile, $profile_type;

	$ptif_id = trim($patient_data[0]);

	// 患者IDの桁数によっては無視
	if ($option > 0 && strlen($ptif_id) == $option) {
		return;
	}

	$pt_nm = trim(mb_convert_kana($patient_data[1], "s"));
	$pt_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_nm);
	list($lt_nm, $ft_nm) = explode(" ", $pt_nm);

	$pt_kana_nm = trim(mb_convert_kana($patient_data[2], "HVcs"));
	$pt_kana_nm = preg_replace("/^(\S*)\s+(\S*)$/", "$1 $2", $pt_kana_nm);
	list($lt_kana_nm, $ft_kana_nm) = explode(" ", $pt_kana_nm);

	$sex = trim($patient_data[3]);
	$birth = str_replace("/", "", trim($patient_data[4]));

	$zip = isset($patient_data[5]) ? trim($patient_data[5]) : "";
	$address = isset($patient_data[6]) ? trim($patient_data[6]) : "";
	$tel = isset($patient_data[7]) ? trim($patient_data[7]) : "";

	for ($i = 1; $i <= 20; $i++) {
		$var_name = "karte_cd$i";
		$$var_name = isset($patient_data[7+$i]) ? trim($patient_data[7+$i]) : "";
	}

	// 入力チェック
	if ($ptif_id == "") {
		return "{$_label_by_profile["PATIENT_ID"][$profile_type]}は必須です";
	}
	if (preg_match("/[^0-9a-zA-Z]/", $ptif_id) > 0) {
		return "{$_label_by_profile["PATIENT_ID"][$profile_type]}に使える文字は半角英数字のみです";
	}
	if (strlen($ptif_id) > 12) {
		return "{$_label_by_profile["PATIENT_ID"][$profile_type]}が長すぎます";
	}
	$sql = "select count(*) from ptifmst";
	$cond = "where ptif_id = '$ptif_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$patient_exists = (intval(pg_fetch_result($sel, 0, 0)) > 0);
	if ($lt_nm == "") {
		return "苗字（漢字）を入力してください";
	}
	if (strlen($lt_nm) > 20) {
		return "苗字（漢字）が長すぎます";
	}
	if ($ft_nm == "") {
		$ft_nm = "　";
	}
	if (strlen($ft_nm) > 20) {
		return "名前（漢字）が長すぎます";
	}
	if ($lt_kana_nm == "") {
		return "苗字（カナ）を入力してください";
	}
	if (strlen($lt_kana_nm) > 20) {
		return "苗字（カナ）が長すぎます";
	}
	if ($ft_kana_nm == "") {
		$ft_kana_nm = "　";
	}
	if (strlen($ft_kana_nm) > 20) {
		return "名前（カナ）が長すぎます";
	}
	if ($sex != "0" && $sex != "1" && $sex != "2") {
		return "性別が不正です";
	}
	if (preg_match("/^\d{8}$/", $birth) == 0) {
		return "生年月日が不正です";
	} else {
		$birth_yr = substr($birth, 0, 4);
		$birth_mon = substr($birth, 4, 2);
		$birth_day = substr($birth, 6, 2);
		if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
			return "生年月日が不正です";
		}
	}
	if ($zip != "") {
		if (preg_match("/^\d{7}$/", $zip) == 0) {
			return "郵便番号が不正です";
		} else {
			$zip1 = substr($zip, 0, 3);
			$zip2 = substr($zip, 3, 4);
		}
	} else {
		$zip1 = "";
		$zip2 = "";
	}
	if (strlen($address) > 100) {
		return "住所が長すぎます";
	}
	if ($tel != "") {
		if (preg_match("/^(\d{1,6})-(\d{1,6})$/", $tel, $matches)) {
			$tel1 = "";
			$tel2 = $matches[1];
			$tel3 = $matches[2];
		} else if (preg_match("/^(\d{1,6})-(\d{1,6})-(\d{1,6})$/", $tel, $matches)) {
			$tel1 = $matches[1];
			$tel2 = $matches[2];
			$tel3 = $matches[3];
		} else {
			$tel = preg_replace("/\D/", "", $tel);
			if (strlen($tel) > 12) {
				return "電話番号が長すぎます";
			}
			$tel1 = substr($tel, 0, 4);
			$tel2 = substr($tel, 4, 4);
			$tel3 = substr($tel, 8);
			if ($tel3 == "") {
				$tel3 = $tel2;
				$tel2 = $tel1;
				$tel1 = "";
			}
		}
	} else {
		$tel1 = "";
		$tel2 = "";
		$tel3 = "";
	}
	for ($i = 1; $i <= 20; $i++) {
		$var_name = "karte_cd$i";
		if (strlen($$var_name) > 20) {
			return "{$_label_by_profile["KARTE_NO"][$profile_type]}{$i}が長すぎます";
		}
	}

	$keyword = get_keyword($lt_kana_nm);
	$in_out = "1";
	$prv = null;
	$prefectures = CoMedix_Prefecture::getList();
	foreach ($prefectures as $pref_id => $prefecture) {
		if (strpos($address, $prefecture) === 0) {
			$prv = $pref_id;
			$address = substr($address, strlen($prefecture));
			break;
		}
	}

	if (!$patient_exists) {
		$sql = "insert into ptifmst (ptif_id, ptif_lt_kaj_nm, ptif_ft_kaj_nm, ptif_lt_kana_nm, ptif_ft_kana_nm, ptif_birth, ptif_sex, ptif_keywd, ptif_in_out, ptif_zip1, ptif_zip2, ptif_prv, ptif_addr1, ptif_tel1, ptif_tel2, ptif_tel3) values (";
		$content = array($ptif_id, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, $in_out, $zip1, $zip2, $prv, $address, $tel1, $tel2, $tel3);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		$sql = "update ptifmst set";
		$set = array("ptif_lt_kaj_nm", "ptif_ft_kaj_nm", "ptif_lt_kana_nm", "ptif_ft_kana_nm", "ptif_birth", "ptif_sex", "ptif_keywd", "ptif_in_out", "ptif_zip1", "ptif_zip2", "ptif_prv", "ptif_addr1", "ptif_tel1", "ptif_tel2", "ptif_tel3");
		$setvalue = array($lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $birth, $sex, $keyword, $in_out, $zip1, $zip2, $prv, $address, $tel1, $tel2, $tel3);
		$cond = "where ptif_id = '$ptif_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	if ($patient_exists) {
		$sql = "delete from ptkarte";
		$cond = "where ptif_id = '$ptif_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	$sql = "insert into ptkarte (ptif_id, karte_cd1, karte_cd2, karte_cd3, karte_cd4, karte_cd5, karte_cd6, karte_cd7, karte_cd8, karte_cd9, karte_cd10, karte_cd11, karte_cd12, karte_cd13, karte_cd14, karte_cd15, karte_cd16, karte_cd17, karte_cd18, karte_cd19, karte_cd20) values (";
	$content = array($ptif_id, $karte_cd1, $karte_cd2, $karte_cd3, $karte_cd4, $karte_cd5, $karte_cd6, $karte_cd7, $karte_cd8, $karte_cd9, $karte_cd10, $karte_cd11, $karte_cd12, $karte_cd13, $karte_cd14, $karte_cd15, $karte_cd16, $karte_cd17, $karte_cd18, $karte_cd19, $karte_cd20);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if ($patient_exists) {
		$sql = "update inptmst set";
		$set = array("inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_keywd");
		$setvalue = array($lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $keyword);
		$cond = "where ptif_id = '$ptif_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

class CoMedix_Prefecture
{
	function getList() {
		$list = array();
		$list["0"] = "北海道";
		$list["1"] = "青森県";
		$list["2"] = "岩手県";
		$list["3"] = "宮城県";
		$list["4"] = "秋田県";
		$list["5"] = "山形県";
		$list["6"] = "福島県";
		$list["7"] = "茨城県";
		$list["8"] = "栃木県";
		$list["9"] = "群馬県";
		$list["10"] = "埼玉県";
		$list["11"] = "千葉県";
		$list["12"] = "東京都";
		$list["13"] = "神奈川県";
		$list["14"] = "新潟県";
		$list["15"] = "富山県";
		$list["16"] = "石川県";
		$list["17"] = "福井県";
		$list["18"] = "山梨県";
		$list["19"] = "長野県";
		$list["20"] = "岐阜県";
		$list["21"] = "静岡県";
		$list["22"] = "愛知県";
		$list["23"] = "三重県";
		$list["24"] = "滋賀県";
		$list["25"] = "京都府";
		$list["26"] = "大阪府";
		$list["27"] = "兵庫県";
		$list["28"] = "奈良県";
		$list["29"] = "和歌山県";
		$list["30"] = "鳥取県";
		$list["31"] = "島根県";
		$list["32"] = "岡山県";
		$list["33"] = "広島県";
		$list["34"] = "山口県";
		$list["35"] = "徳島県";
		$list["36"] = "香川県";
		$list["37"] = "愛媛県";
		$list["38"] = "高知県";
		$list["39"] = "福岡県";
		$list["40"] = "佐賀県";
		$list["41"] = "長崎県";
		$list["42"] = "熊本県";
		$list["43"] = "大分県";
		$list["44"] = "宮崎県";
		$list["45"] = "鹿児島県";
		$list["46"] = "沖縄県";
		return $list;
	}
}

function get_character_table($file_encoding) {
	switch ($file_encoding) {
	case "SJIS":
		$table_file = "character_table_sjis.txt";
		break;
	case "EUC-JP":
		$table_file = "character_table_euc.txt";
		break;
	case "UTF-8":
		$table_file = "character_table_utf8.txt";
		break;
	}

	if (!file_exists($table_file)) return array();

	$lines = file($table_file);

	$table = array();
	for ($i = 0, $j = count($lines); $i < $j; $i++) {
		$tmp_columns = split(',', trim($lines[$i]));
		if (count($tmp_columns) == 2) {
			$table[] = $tmp_columns;
		}
	}
	return $table;
}

function convert_encoding($line, $character_table) {
	foreach ($character_table as $pair) {
		$line = str_replace($pair[0], $pair[1], $line);
	}
	return $line;
}
?>
