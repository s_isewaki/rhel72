<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 54, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($stage_ids)) {
	echo("<script type=\"text/javascript\">alert('削除対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// チェックされたライフステージをループ
foreach ($stage_ids as $stage_id) {

	// 紐付くレコードの数を取得
	$sql = "select count(*) from welfare";
	$cond = "where lifestage_id = $stage_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$welfare_count = intval(pg_fetch_result($sel, 0, 0));

	// レコードが存在した場合
	if ($welfare_count > 0) {

		// ライフステージ名を取得
		$sql = "select name from lifestage";
		$cond = "where lifestage_id = $stage_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$name = pg_fetch_result($sel, 0, "name");

		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('「{$name}」は文書が登録されているため削除できません。');</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}

	// ライフステージ情報を削除
	$sql = "delete from lifestage";
	$cond = "where lifestage_id = $stage_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 表示順を振りなおし
$sql = "select lifestage_id from lifestage";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$i = 1;
while ($row = pg_fetch_array($sel)) {
	$sql = "update lifestage set";
	$set = array("order_no");
	$setvalue = array($i);
	$cond = "where lifestage_id = {$row["lifestage_id"]}";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$i++;
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// ライフステージ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'life_master_stage_list.php?session=$session';</script>");
?>
