<?
ob_start();

require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = ($mmode != "usr") ? 42 : 5;
	break;
default:
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// system_config
$conf = new Cmx_SystemConfig();
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {
	$duty_or_oncall_flg = "1";
}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";

// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
$sql = "select duty_form from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
// ************ oose add end *****************

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が未登録の場合はエラーとする
if ($closing == "") {
	echo("<script type=\"text/javascript\">alert('タイムカード締め日が登録されていません。');</script>");
	exit;
}

if ($yyyymm != "") {
	$year = substr($yyyymm, 0, 4);
	$month = intval(substr($yyyymm, 4, 2));
	$month_set_flg = true; //月が指定された場合
} else {
	$year = date("Y");
	$month = date("n");
	$yyyymm = $year . date("m");
	$month_set_flg = false; //月が未指定の場合
}

// 開始日・締め日を算出
$calced = false;
switch ($closing) {
case "1":  // 1日
	$closing_day = 1;
	break;
case "2":  // 5日
	$closing_day = 5;
	break;
case "3":  // 10日
	$closing_day = 10;
	break;
case "4":  // 15日
	$closing_day = 15;
	break;
case "5":  // 20日
	$closing_day = 20;
	break;
case "6":  // 末日
	$start_year = $year;
	$start_month = $month;
	$start_day = 1;
	$end_year = $start_year;
	$end_month = $start_month;
	$end_day = days_in_month($end_year, $end_month);
	$calced = true;
	break;
}
if (!$calced) {
	$day = date("j");
	$start_day = $closing_day + 1;
	$end_day = $closing_day;

	//月が指定された場合
	if ($month_set_flg) {
		//当月〜翌月
		if ($closing_month_flg != "2") {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
		//前月〜当月
		else {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		}
			
	} else {
		if ($day <= $closing_day) {
			$start_year = $year;
			$start_month = $month - 1;
			if ($start_month == 0) {
				$start_year--;
				$start_month = 12;
			}
			
			$end_year = $year;
			$end_month = $month;
		} else {
			$start_year = $year;
			$start_month = $month;
			
			$end_year = $year;
			$end_month = $month + 1;
			if ($end_month == 13) {
				$end_year++;
				$end_month = 1;
			}
		}
	}
}
$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);

// 出勤予定表情報をCSV形式で取得
$csv = get_attendance_book_csv($con, $emp_id, $start_date, $end_date, $fname);

// データベース接続を閉じる
pg_close($con);

// CSVを出力
$file_name = sprintf("%04d%02d", $year, $month) . ".csv";
ob_clean();
header("Content-Disposition: attachment; filename=$file_name");
header("Content-Type: application/octet-stream; name=$file_name");
header("Content-Length: " . strlen($csv));
echo($csv);
ob_end_flush();

//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

// 出勤予定表情報をCSV形式で取得
function get_attendance_book_csv($con, $emp_id, $start_date, $end_date, $fname) {

	global $duty_str;
	$buf = "日付,曜日,種別,グループ,出勤予定,事由,{$duty_str},手当,勤務開始予定,勤務終了予定\r\n";
	//出勤表関連共通クラス
	require_once("atdbk_common_class.php");
	$atdbk_common_class = new atdbk_common_class($con, $fname);
/*
	// 勤務日・休日の名称を取得
	$sql = "select day2, day3, day4, day5 from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
	}
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}
*/
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

	$tmp_date = $start_date;
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		// 処理日付の勤務日種別を取得
		$sql = "select type from calendar";
		$cond = "where date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$type = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "type") : "";
		// カレンダー名を取得
		$type = $atdbk_common_class->get_calendarname($type, $tmp_date);
/*
		switch ($type) {
		case "1":
			$type = "通常勤務日";
			break;
		case "2":
			$type = $day4;
			break;
		case "3":
			$type = $day5;
			break;
		case "4":
			$type = $day2;
			break;
		case "5":
			$type = $day3;
			break;
		}
*/
		// 処理日付の出勤予定を取得
//		$sql = "select pattern, reason, night_duty, prov_start_time, prov_end_time, allow_id from atdbk";
//		$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
		$sql =	"SELECT ".
					"atdbk.tmcd_group_id, ".
					"atdbk.pattern, ".
					"atdbk.reason, ".
					"atdbk.night_duty, ".
					"atdbk.prov_start_time, ".
					"atdbk.prov_end_time, ".
					"atdbk.allow_id, ".
					"wktmgrp.group_name, ".
					"atdptn.atdptn_nm, ".
					"atdbk_reason_mst.default_name, ".
					"atdbk_reason_mst.display_name ".
				"FROM ".
					"atdbk  ".
						"LEFT JOIN wktmgrp ON atdbk.tmcd_group_id = wktmgrp.group_id ".
						"LEFT JOIN atdptn  ON atdbk.tmcd_group_id = atdptn.group_id AND atdbk.pattern = CAST(atdptn.atdptn_id AS varchar) ".
						"LEFT JOIN atdbk_reason_mst ON atdbk.reason = atdbk_reason_mst.reason_id ".
				"WHERE ".
					"atdbk.emp_id = '$emp_id' AND ".
					"atdbk.date = '$tmp_date'";
		$cond = "";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$pattern = pg_fetch_result($sel, 0, "pattern");
			$reason = pg_fetch_result($sel, 0, "reason");
			$night_duty = pg_fetch_result($sel, 0, "night_duty");
			$prov_start_time = pg_fetch_result($sel, 0, "prov_start_time");
			$prov_end_time = pg_fetch_result($sel, 0, "prov_end_time");
			$allow_id = pg_fetch_result($sel, 0, "allow_id");
			$group_name = pg_fetch_result($sel, 0, "group_name");
			$atdptn_nm = pg_fetch_result($sel, 0, "atdptn_nm");
			$default_name = pg_fetch_result($sel, 0, "default_name");
			$display_name = pg_fetch_result($sel, 0, "display_name");

			$reason_name = $default_name;
			if (empty($display_name) == false){
				$reason_name = $display_name;
			}
		} else {
			$pattern = "";
			$reason = "";
			$night_duty = "";
			$prov_start_time = "";
			$prov_end_time = "";
			$allow_id = "";
			$group_name = "";
			$atdptn_nm = "";
			$reason_name = "";
		}

		// CSV文字列を作成
		$buf .= sprintf("%04d/%02d/%02d", $tmp_year, $tmp_month, $tmp_day) . ",";
		$buf .= get_weekday(to_timestamp($tmp_date)) . ",";
		$buf .= "$type,";

		if ($group_name != "") {
			$buf .= $group_name;
		}
		$buf .= ",";

		if ($atdptn_nm != "") {
			$buf .= $atdptn_nm;
		}
		$buf .= ",";
		$buf .= "$reason_name,";
		/*
		switch ($reason) {
		case "1":
			$buf .= "有給休暇,";
			break;
		case "2":
			$buf .= "午前有休,";
			break;
		case "3":
			$buf .= "午後有休,";
			break;
		case "4":
			$buf .= "代替休暇,";
			break;
		case "5":
			$buf .= "特別休暇,";
			break;
		case "6":
			$buf .= "一般欠勤,";
			break;
		case "7":
			$buf .= "病傷欠勤,";
			break;
		case "8":
			$buf .= "その他休,";
			break;
		case "9":
			$buf .= "通院,";
			break;
		case "10":
			$buf .= "私用,";
			break;
		case "11":
			$buf .= "交通遅延,";
			break;
		case "12":
			$buf .= "遅刻,";
			break;
		case "13":
			$buf .= "早退,";
			break;
		case "14":
			$buf .= "代替出勤,";
			break;
		case "15":
			$buf .= "振替出勤,";
			break;
		case "16":
			$buf .= "休日出勤,";
			break;
		case "17":
			$buf .= "振替休暇,";
			break;
		case "18":
			$buf .= "半前代替休,";
			break;
		case "19":
			$buf .= "半後代替休,";
			break;
		case "20":
			$buf .= "半前振替休,";
			break;
		case "21":
			$buf .= "半後振替休,";
			break;
		default:
			$buf .= ",";
			break;
		}*/

		switch ($night_duty) {
		case "1":
			$buf .= "有り,";
			break;
		case "2":
			$buf .= "無し,";
			break;
		default:
			$buf .= ",";
			break;
		}

		if ($allow_id != "")
		{
			foreach($arr_allowance as $allowance)
			{
				if($allow_id == $allowance["allow_id"])
				{
					$buf .= $allowance["allow_contents"];
					break;
				}
			}
		}
		$buf .= ",";

		if ($prov_start_time != "") {
			$buf .= preg_replace("/^0?(\d{1,2})(\d{2})$/", "$1:$2", $prov_start_time);
		}
		$buf .= ",";

		if ($prov_end_time != "") {
			$buf .= preg_replace("/^0?(\d{1,2})(\d{2})$/", "$1:$2", $prov_end_time);
		}
		$buf .= "\r\n";

		$tmp_date = next_date($tmp_date);
	}

	return mb_convert_encoding($buf, "SJIS", "EUC-JP");

}

// 指定年月の日数を求める
function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
	return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}

// タイムスタンプから曜日を返す
function get_weekday($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return "日";
		break;
	case "1":
		return "月";
		break;
	case "2":
		return "火";
		break;
	case "3":
		return "水";
		break;
	case "4":
		return "木";
		break;
	case "5":
		return "金";
		break;
	case "6":
		return "土";
		break;
	}
}
?>
