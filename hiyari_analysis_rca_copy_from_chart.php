<?


//==========================================================
//初期処理と認証
//==========================================================
ob_start();
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
ob_end_clean();

$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$session_auth = check_authority($session, 47, $fname);
$con = @connect2db($fname);
if(!$session || !$session_auth || !$con){
  echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
  exit;
}
$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// member_idで編集可能かcheckする
$login_emp_id  = get_emp_id($con,$session,$fname);


$field_idx = (int)$_REQUEST["field_idx"];

//==========================================================
// 内容取得
//==========================================================
$analysis_id = (int)@$_REQUEST["a_id"];
$sql = "select chart_v_data from inci_analysis_rca_chart where analysis_id = ".$analysis_id ." and delete_flg = 'f' and is_inga_mode = ''";
$sel = select_from_table($con, $sql, "", $fname);
$chart_v_data = explode("\t", @pg_fetch_result($sel, 0, 0));





?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 根本原因の参照 | ファントルくん | 出来事分析 | ＲＣＡ分析</title>
</head>
<script type="text/javascript" src="js/fontsize.js"></script>
<style type="text/css">
    form {margin:0;}
    table.list {border-collapse:collapse; border:0;}
    table.list td {border:solid #35B341 1px;}
    .always_blue { color:#00f; }
    .always_blue:hover { color:#ff5809; }
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="padding:0; margin:0" onload="<? if ($initial_tab=='change'){ ?> changeTab('change'); <?}?> bodyResized();" onresize="bodyResized()">

<script>

function getClientHeight(){
    if (document.body.clientHeight) return document.body.clientHeight;
    if (window.innerHeight) return window.innerHeight;
    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }
    return 0;
}

var TOP_TO_SCROLL_CONTENT_HEIGHT = 50;
function bodyResized(){
    var h = getClientHeight();
    if (h < 1) return;
    document.getElementById("scroll_div_inyou").style.height = (h - TOP_TO_SCROLL_CONTENT_HEIGHT) + "px";
}

var inyou_chart_seq = "";
function rowInyouSelected(idx){
    var str = document.getElementById("reference_"+idx).value;
    if (window.opener && !window.opener.closed && window.opener.resultReference){
        window.opener.resultReference(<?=$field_idx?>, str);
    } else {
        alert("引用先画面が検出できませんでした。");
    }
    window.close();
}


function inyou(inyou_pattern){
    if (!window.opener.swapChartData) { alert("値をセットできません。(エラー01)"); return; }
    var msg = "";
    var swap_chart_seq = "";
    var swap_cliplist_seq = "";
    if (inyou_pattern=="chart"){
        swap_chart_seq = inyou_chart_seq;
        msg += "現在編集中のデータをクリアして、指定した分析のチャートデータを読み込みます。\n\nよろしいですか？";
    }
    if (inyou_pattern=="cliplist"){
        swap_cliplist_seq = inyou_chart_seq;
        msg += "指定した分析に登録されているクリップリストデータを読み込みます。\n現在のクリップリストのデータはクリアされます。\n\nよろしいですか？";
    }
    if (inyou_pattern=="both"){
        swap_chart_seq = inyou_chart_seq;
        swap_cliplist_seq = inyou_chart_seq;
        msg += "現在編集中のデータをクリアして、チャートとクリップリストを引用します。\n\nよろしいですか？";
    }
    if (!confirm(msg)) return;
    window.opener.swapChartData(swap_chart_seq, swap_cliplist_seq);
    window.close();
    return;
}

</script>





<? //------------------------------------------------------------------------ ?>
<? // ヘッダー                                                                ?>
<? //------------------------------------------------------------------------ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr height="32" bgcolor="#35B341">
    <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>根本原因の参照</b></font></td>
    <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる"
        width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
    </tr>
</table>

<div style="color:#999; padding:2px 8px;"><?=$font?>ＲＣＡ図で作成した以下の「根本原因」を、転記することができます。</font></div>



<div style="padding:6px">




<? //------------------------------------------------------------------------ ?>
<? // 引用・コピータブ内                                                      ?>
<? //------------------------------------------------------------------------ ?>
<div id="inyou_div">

    <!-- 引用・コピースクロール領域 -->
    <div id="scroll_div_inyou" style="overflow:scroll; border:#cccccc solid 1px; background-color:#eee">
            <!-- 引用・コピー対象リスト -->
            <table id="list_scroll_table_inyou" border="0" cellspacing="0" cellpadding="2" width="100%" class="list" style="background-color:#fff; cursor:pointer">

                <? if (count($chart_v_data)<= 2 && $chart_v_data[0]=="") { ?>
                    <tr><td colspan="4" style="text-align:center; border:0; background-color:#eeeeee; padding-top:20px; font-size:14px">転記可能な「根本原因」データがありません。<br/>「ＲＣＡ図の作成」において、「根本原因」を記述する必要があります。</td></tr>
                <? } else { ?>
                    <? $idx = 0; ?>
                    <? foreach ($chart_v_data as $str){ ?>
                    <? $idx++; ?>
                    <tr>
                        <td style="padding-left:3px"><?=$font?>
                            <a href="" class="always_blue" onclick="rowInyouSelected(<?=$idx?>); return false;"><?=h($str)?></a>
                            <input type="hidden" id="reference_<?=$idx?>" value="<?= str_replace('"', '\"', $str)?>"/>
                        </font></td>
                    </tr>
                    <? } ?>
                <? } ?>
            </table>
    </div>

</div>








</div>
</body>
</html>

