<?
ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	show_login_exit();
}

// 権限のチェック
$checkauth = check_authority($session, 14, $fname);
if ($checkauth == "0") {
	show_login_exit();
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// データベースに接続
$con = connect2db($fname);

// 表示期間の設定
if ($year == "") {$year = date("Y");}
if ($month == "") {$month = date("m");}
if ($day == "") {$day = date("d");}
if ($term == "") {$term = "1w";}
$start_date = "$year$month$day";
switch ($term) {
case "1w":
	$end_date = date("Ymd", strtotime("+6 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "2w":
	$end_date = date("Ymd", strtotime("+13 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "3w":
	$end_date = date("Ymd", strtotime("+20 days", mktime(0, 0, 0, $month, $day, $year)));
	break;
case "1m":
	$end_date = date("Ymd", strtotime("+1 month -1 day", mktime(0, 0, 0, $month, $day, $year)));
	break;
}

// 医療機関マスタを取得
$sql = "select mst_cd, mst_name from institemmst";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	show_error_exit();
}
$insts = array();
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]] = $row["mst_name"];
}

// 退院先マスタを取得
$out_pos_master = get_out_pos_master($con, $fname);

// 患者一覧を取得
if ($mode == "") $mode = "r";  // デフォルトは予定（r）／実績（o）
if ($mode == "r") {
	$sql = "select i.inpt_out_res_dt as out_dt, i.inpt_out_res_tm as out_tm, w.ward_name, r.ptrm_name, i.inpt_short_stay, i.inpt_lt_kj_nm, i.inpt_ft_kj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_back_in_dt, i.inpt_back_in_tm, i.inpt_out_pos_rireki, i.inpt_out_pos_cd, i.inpt_out_inst_cd, i.out_res_updated as updated, i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, (select max(n.note) from bed_outnote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'r') as note from inptmst i ";
	$cond = q("where i.inpt_out_res_dt between '%s' and '%s' order by out_dt, out_tm", $start_date, $end_date);
} else {
	$sql = "select i.inpt_out_dt as out_dt, i.inpt_out_tm as out_tm, w.ward_name, r.ptrm_name, i.inpt_short_stay, i.inpt_lt_kj_nm, i.inpt_ft_kj_nm, p.ptif_birth, p.ptif_sex, d.dr_nm, i.inpt_back_in_dt, i.inpt_back_in_tm, i.inpt_out_pos_rireki, i.inpt_out_pos_cd, i.inpt_out_inst_cd, i.out_updated as updated, i.ptif_id, i.inpt_in_dt, i.inpt_in_tm, (select max(n.note) from bed_outnote n where n.ptif_id = i.ptif_id and n.type = 'p' and n.mode = 'o' and n.in_dttm = cast((i.inpt_in_dt || i.inpt_in_tm) as varchar)) as note from inpthist i ";
	$cond = q("where i.inpt_out_dt between '%s' and '%s' order by out_dt, out_tm", $start_date, $end_date);
}
$sql .= "inner join ptifmst p on p.ptif_id = i.ptif_id inner join wdmst w on w.bldg_cd = i.bldg_cd and w.ward_cd = i.ward_cd inner join ptrmmst r on r.bldg_cd = i.bldg_cd and r.ward_cd = i.ward_cd and r.ptrm_room_no = i.ptrm_room_no left join drmst d on d.sect_id = i.inpt_sect_id and d.dr_id = i.dr_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	show_error_exit();
}
$patients = array();
$notes = array();
while ($row = pg_fetch_array($sel)) {
	$in_dttm = $row["inpt_in_dt"] . $row["inpt_in_tm"];

	$patients[$row["out_dt"]][] = array(
		"ward_name" => $row["ward_name"],
		"ptrm_name" => $row["ptrm_name"],
		"short_stay" => format_checked($row["inpt_short_stay"]),
		"kanji_name" => $row["inpt_lt_kj_nm"] . " " . $row["inpt_ft_kj_nm"],
		"birth" => format_date($row["ptif_birth"]),
		"age" => format_age($row["ptif_birth"]),
		"sex" => format_sex($row["ptif_sex"]),
		"dr_nm" => $row["dr_nm"],
		"operators" => get_operators($con, $mode, $row["ptif_id"], $row["inpt_in_dt"], $row["inpt_in_tm"], $fname),
		"back_in_dttm" => format_datetime($row["inpt_back_in_dt"], $row["inpt_back_in_tm"]),
		"out_inst" => format_out_inst($out_pos_master, $row["inpt_out_pos_rireki"], $row["inpt_out_pos_cd"], $insts, $row["inpt_out_inst_cd"]),
		"out_tm" => format_time($row["out_tm"]),
		"updated" => $row["updated"],
		"ptif_id" => $row["ptif_id"],
		"in_dttm" => $in_dttm
	);

	if ($mode == "r") {
		$notes["p"][$row["ptif_id"]] = $row["note"];
	} else {
		$notes["p"][$row["ptif_id"]][$in_dttm] = $row["note"];
	}
}

// 備考リストを作成（日付分）
$sql = "select * from bed_outnote";
$cond = q("where type = 'd' and mode = '%s' and date between '%s' and '%s'", $mode, $start_date, $end_date);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	show_error_exit();
}
while ($row = pg_fetch_array($sel)) {
	$notes["d"][$row["date"]] = $row["note"];
}
?>
</head>
<body>
<p>退院予定表</p>
<table border="1" cellspacing="0" cellpadding="1">
<tr>
<td nowrap align="center" bgcolor="#CCFFFF">日付</td>
<td nowrap align="center" bgcolor="#CCFFFF">曜日</td>
<td nowrap align="center" bgcolor="#CCFFFF">NEW</td>
<td nowrap align="center" bgcolor="#CCFFFF">病棟</td>
<td nowrap align="center" bgcolor="#CCFFFF">部屋番号</td>
<td nowrap align="center" bgcolor="#CCFFFF">短期</td>
<td nowrap align="center" bgcolor="#CCFFFF">患者名</td>
<td nowrap align="center" bgcolor="#CCFFFF">生年月日</td>
<td nowrap align="center" bgcolor="#CCFFFF">年齢</td>
<td nowrap align="center" bgcolor="#CCFFFF">性別</td>
<td nowrap align="center" bgcolor="#CCFFFF">主治医</td>
<td nowrap align="center" bgcolor="#CCFFFF">担当</td>
<td nowrap align="center" bgcolor="#CCFFFF">退院先施設名</td>
<td nowrap align="center" bgcolor="#CCFFFF">退院時間</td>
<td nowrap align="center" bgcolor="#CCFFFF">戻り入院予定</td>
<td nowrap align="center" bgcolor="#CCFFFF">備考</td>
</tr>
<?
$new_start = date("YmdHis", strtotime("-1 day"));

$date = $start_date;
while ($date <= $end_date) {
	$tmp_year = substr($date, 0, 4);
	$tmp_month = substr($date, 4, 2);
	$tmp_day = substr($date, 6, 2);
	$timestamp = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);
	$rowspan = count($patients[$date]);
	if ($rowspan == 0) $rowspan = 1;
?>
<tr>
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><? eh(format_date($date)); ?></td>
<td nowrap align="center" rowspan="<? eh($rowspan); ?>"><? eh(get_weekday($timestamp)); ?></td>
<?
	if (isset($patients[$date][0])) {
?>
<td nowrap align="center"><? show_label_if_new($new_start, $patients[$date][0]["updated"]); ?></td>
<td nowrap><? eh($patients[$date][0]["ward_name"]); ?></td>
<td nowrap><? eh($patients[$date][0]["ptrm_name"]); ?></td>
<td nowrap align="center"><? eh($patients[$date][0]["short_stay"]); ?></td>
<td nowrap><? eh($patients[$date][0]["kanji_name"]); ?></td>
<td nowrap><? eh($patients[$date][0]["birth"]); ?></td>
<td nowrap><? eh($patients[$date][0]["age"]); ?></td>
<td nowrap><? eh($patients[$date][0]["sex"]); ?></td>
<td nowrap><? eh($patients[$date][0]["dr_nm"]); ?></td>
<td nowrap><? show_operators($patients[$date][0]["operators"]); ?></td>
<td nowrap><? eh($patients[$date][0]["out_inst"]); ?></td>
<td nowrap><? eh($patients[$date][0]["out_tm"]); ?></td>
<td nowrap><? eh($patients[$date][0]["back_in_dttm"]); ?></td>
<? if ($mode == "r") { ?>
<td nowrap><? eh($notes["p"][$patients[$date][0]["ptif_id"]]); ?></td>
<? } else { ?>
<td nowrap><? eh($notes["p"][$patients[$date][0]["ptif_id"]][$patients[$date][0]["in_dttm"]]); ?></td>
<? } ?>
<?
	} else {
		for ($j = 1; $j <= 13; $j++) {
?>
<td></td>
<?
		}
?>
<td nowrap><? eh($notes["d"][$date]); ?></td>
<?
	}
?>
</tr>
<?
	for ($j = 1; $j < $rowspan; $j++) {
?>
<tr>
<td nowrap align="center"><? show_label_if_new($new_start, $patients[$date][$j]["updated"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["ward_name"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["ptrm_name"]); ?></td>
<td nowrap align="center"><? eh($patients[$date][$j]["short_stay"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["kanji_name"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["birth"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["age"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["sex"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["dr_nm"]); ?></td>
<td nowrap><? show_operators($patients[$date][$j]["operators"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["out_inst"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["out_tm"]); ?></td>
<td nowrap><? eh($patients[$date][$j]["back_in_dttm"]); ?></td>
<? if ($mode == "r") { ?>
<td nowrap><? eh($notes["p"][$patients[$date][$j]["ptif_id"]]); ?></td>
<? } else { ?>
<td nowrap><? eh($notes["p"][$patients[$date][$j]["ptif_id"]][$patients[$date][$j]["in_dttm"]]); ?></td>
<? } ?>
</tr>
<?
	}
	$date = date("Ymd", strtotime("+1 day", $timestamp));
}
?>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
$excel_data = mb_convert_encoding(ob_get_clean(), "Shift_JIS", 'EUC-JP');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=discharge_list.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
echo($excel_data);

function get_out_pos_master($con, $fname) {
	$sql = "select rireki_index, item_cd, item_name from tmplitemrireki";
	$cond = "where mst_cd = 'A307' and disp_flg";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		show_error_exit();
	}

	$master = array();
	while ($row = pg_fetch_array($sel)) {
		$master[$row["rireki_index"]][$row["item_cd"]] = $row["item_name"];
	}
	return $master;
}

function get_weekday($timestamp) {
	switch (date("w", $timestamp)) {
	case "0":
		return "日";
	case "1":
		return "月";
	case "2":
		return "火";
	case "3":
		return "水";
	case "4":
		return "木";
	case "5":
		return "金";
	case "6":
		return "土";
	}
}

function show_label_if_new($new_start, $updated) {
	if ($updated >= $new_start) {
?>
NEW
<?
	}
}

function format_checked($checked) {
	return ($checked == "t") ? "○" : "";
}

function format_date($date) {
	return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $date);
}

function format_age($birth) {
	if ($birth == "") return "";

	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = date("Y") - $birth_yr;
	if ($birth_md > date("md")) {
		$age--;
	}
	return "{$age}歳";
}

function format_sex($sex) {
	switch ($sex) {
	case "0":
		return "不明";
	case "1":
		return "男性";
	case "2":
		return "女性";
	default:
		return "";
	}
}

function get_operators($con, $mode, $ptif_id, $in_dt, $in_tm, $fname) {
	if ($mode == "r") {
		$sql = "select e.emp_lt_nm from inptop";
		$cond = q("where i.ptif_id = '%s'", $ptif_id);
	} else {
		$sql = "select e.emp_lt_nm from inptophist";
		$cond = q("where i.ptif_id = '%s' and i.inpt_in_dt = '%s' and i.inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
	}
	$sql .= " i inner join empmst e on e.emp_id = i.emp_id";
	$cond .= " and exists (select * from jobmst j where j.job_id = e.emp_job and j.bed_op_flg) order by i.order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		show_error_exit();
	}

	$operators = array();
	while ($row = pg_fetch_array($sel)) {
		$operators[] = $row["emp_lt_nm"];
	}
	return $operators;
}

function show_operators($operators) {
	for ($i = 0, $j = count($operators); $i < $j; $i++) {
		if ($i > 0) {
?>
<br>
<?
		}
		eh($operators[$i]);
	}
}

function format_intro_inst($pre_div, $inst_master, $intro_inst_cd) {
	if ($inst_master[$intro_inst_cd] != "") return $inst_master[$intro_inst_cd];

	switch ($pre_div) {
	case "1":
		return "自宅";
	case "2":
		return "医療機関他";
	default:
		return "";
	}
}

function format_datetime($date, $time) {
	$date = format_date($date);
	$time = format_time($time);
	return trim("{$date} {$time}");
}

function format_out_inst($out_pos_master, $out_pos_rireki, $out_pos_cd, $insts, $out_inst_cd) {
	if (isset($out_pos_master[$out_pos_rireki][$out_pos_cd])) {
		$out_inst = $out_pos_master[$out_pos_rireki][$out_pos_cd];
	} else {
		$out_inst = "";
	}

	if (isset($insts[$out_inst_cd])) {
		if ($out_inst != "") {
			$out_inst .= "　";
		}
		$out_inst .= $insts[$out_inst_cd];
	}

	return $out_inst;
}

function format_time($time) {
	return preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $time);
}
