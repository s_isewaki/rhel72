<?php
require_once("about_postgres.php");

//==============================
//初期処理
//==============================

//画面名
//$fname = $PHP_SELF;

//==============================
//DBコネクション取得
//==============================
//$con = connect2db($fname);
//if$con = connect2db($fname);
//if($con == "0")
//{
//	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
//	echo("<script language='javascript'>showLoginPage(window);</script>");
//	exit;
//}

//$db = new hiyari_db_class($con, $fname);
//$sql = 'select * from inci_test';
//$set = array("aaaa");
//$setvalue = $db->escape(array("11"));
//$cond = 'where aaaa = 10';
//$content = $db->escape(array("10", "aa'a'a", "1234567890"));
//$db->query($sql);
//$result = $db->getOne('bbbb');
//print_r($result);


class hiyari_db_class {
	// DB接続リソース
	var $_con;

	// ファイル名
	var $_fname;

	// エラー情報
	var $_error = false;

	// SQLの戻り値
	var $_result;

	// 戻り値のモード
	var $_mode = 'assoc';

	/*****************************************************************
	 * コンストラクタ
	 *
	 * @param	RESOURCE	$con	DB接続リソース
	 * @param	STRING		$fname	ファイル名
	 *****************************************************************/
	function hiyari_db_class($con, $fname) {
		$this->_con = $con;
		$this->_fname = $fname;
	}

	/*****************************************************************
	 * SELECTする際のクエリをリソース型で返します
	 *
	 * @param	STRING		$SQL	SQL文
	 *****************************************************************/
	function query($sql) {

		@require("./conf/conf.inf");  								  						//環境設定ファイルの読み込み
		require_once("./about_sql_log.php");												//SQLのログをとるためのファイルを読み込む
		if(!defined("CONF_FILE")){	        					  						//読み込みの確認

			require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
			require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
			write_error($this->_fname,$ERRORLOG, $ERROR001);								//errorログへの書き込み

			$this->_error = true;
			return 0;																			//失敗の場合は０を返す
		}

		$wsql = write_sql_log($SQLLOG,$this->_fname,$sql,$SQLLOGFILE,$ERRORLOG);		//$SQLLOGがonだと実行されるSQLのログをとる

		if(!@($result = (pg_exec($this->_con, $sql)))){										//SQLの実行

			require("./conf/error.inf");												//エラーメッセージ一覧ファイルを読み込む
			require_once("./about_error.php");									//エラーメッセージの書き込み処理をする関数のファイルを読み込む
			write_error($this->_fname, $ERRORLOG, "fail to execute \"$sql\"; " . pg_last_error($this->_con));

			$this->_error = true;
			return 0;																			//失敗の場合は０を返す
		}else{
			$this->_result = $result;																	//成功の場合はSQLの結果を返します
		}
	}

	/*****************************************************************
	 * １フィールド分だけ値を返す
	 *
	 * @param	MIXED	$key	取得するカラム名(番号)
	 * @return	MIXED	1カラム分の値
	 *****************************************************************/
	function getOne($key=0) {
		// エラーを返す
		if($this->_error == true) $this->errorMessage();

		// 一件レコードを返す
		return pg_fetch_result($this->_result, 0, $key);
	}

	/*****************************************************************
	 * １レコード分だけ値を返す
	 *
	 * @param	MIXED	$mode	配列か連想配列かの判別
	 * @return	MIXED	1レコード分の値
	 *****************************************************************/
	function getRow($mode="") {
		// エラーを返す
		if($this->_error == true) $this->errorMessage();

		// モードが指定されていなければデフォルトで連想配列
		if(empty($mode)) $mode = $this->_mode;

		// 念のため、小文字にしておく
		$mode = strtolower($mode);

		if($mode == 'assoc') { // 連想配列
			return @pg_fetch_assoc($this->_result, 0);
		} else { // 数字をキーとする
			return pg_fetch_row($this->_result, 0);
		}
	}

	/*****************************************************************
	 * 全ての値を返す
	 *
	 * @param	MIXED	$mode	配列か連想配列かの判別
	 * @return	MIXED	全ての値
	 *****************************************************************/
	function getAll($mode="") {
		// エラーを返す
		if($this->_error == true) $this->errorMessage();

		// モードが指定されていなければデフォルトで連想配列
		if(empty($mode)) $mode = $this->_mode;

		// 念のため、小文字にしておく
		$mode = strtolower($mode);

		$result = array();

		if($mode == 'assoc') { // 連想配列
			while($r = pg_fetch_assoc($this->_result)) {
				$result[] = $r;
			}
		} else { // 数字をキーとする
			while($r = pg_fetch_row($this->_result)) {
				$result[] = $r;
			}
		}

		return $result;
	}

	/*****************************************************************
	 * INSERT処理
	 *
	 * @param	STRING	$sql		SQL
	 * @param	MIXED	$data		挿入する値
	 * @param	BOOLEAN	$mode		trueの時、エスケープ処理を行う
	 *****************************************************************/
	function insert($sql, $data, $mode=false) {
		if($mode == true) {
			$content = $this->escape_array($data);
		} else {
			$content = $data;
		}

		$result = insert_into_table($this->_con,$sql,$content,$this->_fname);

		if($result == 0) $this->errorMessage();
	}

	/*****************************************************************
	 * UPDATE処理
	 *
	 * @param	STRING	$sql		SQL
	 * @param	MIXED	$set		セットするカラム名
	 * @param	MIXED	$data		セットする値
	 * @param	MIXED	$cond		条件
	 * @param	BOOLEAN	$mode		trueの時、エスケープ処理を行う
	 *****************************************************************/
	function update($sql, $set, $data, $cond, $mode=false) {
		if($mode == true) {
			$setvalue = $this->escape_array($data);
		} else {
			$setvalue = $data;
		}

		$result = update_set_table($this->_con,$sql,$set,$setvalue,$cond,$this->_fname);

		if($result == 0) $this->errorMessage();
	}

	/*****************************************************************
	 * UPDATE処理(連想配列のキーをカラム名とする)
	 *
	 * @param	STRING	$sql		SQL
	 * @param	MIXED	$data		カラム名と値の配列
	 * @param	MIXED	$cond		条件
	 * @param	BOOLEAN	$mode		trueの時、エスケープ処理を行う
	 *****************************************************************/
	function update_assoc($sql, $data, $cond, $mode=false) {
		foreach($data as $column => $value) {
			$set[] = $column;	// フィールドをセット

			if($mode == true) {
				$setvalue[] = $this->escape($value);
			} else {
				$setvalue[] = $value;	// 値をセット
			}
		}

		$result = update_set_table($this->_con,$sql,$set,$setvalue,$cond,$this->_fname);

		if($result == 0) $this->errorMessage();
	}

	/*****************************************************************
	 * DELETE処理
	 *
	 * @param	STRING	$sql		SQL
	 * @param	MIXED	$cond		条件
	 *****************************************************************/
	function delete($sql, $cond) {
		$result = delete_from_table($this->_con,$sql,$cond,$this->_fname);

		if($result == 0) $this->errorMessage();
	}

	/*****************************************************************
	 * エスケープ処理
	 *
	 * @param	MIXED	$data	エスケープする値
	 *****************************************************************/
	function escape($data) {
		if(is_null($data)) {
			$value = "null";
		} else {
			$value = pg_escape_string($data);
		}

		return $value;
	}

	/*****************************************************************
	 * エスケープ処理(配列)
	 *
	 * @param	STRING	$arr_data	配列
	 * @param	MIXED	$mode		今のところ使用しない
	 * @return	MIXED	エスケープされた配列
	 *****************************************************************/
	function escape_array($arr_data, $mode="") {
		// 配列でなかったらエラーを返す
		if(!is_array($arr_data)) $this->errorMessage();

		foreach($arr_data as $key => $value) {
			if(is_null($value)) {
				$val[$key] = "null";
			} else {
				$val[$key] = pg_escape_string($value);
			}
		}

		return $val;
	}

	/*****************************************************************
	 * ファイル名をセット
	 *
	 * @param	STRING	$fname		ファイル名
	 *****************************************************************/
	function setFileName($fname) {
		$this->_fname = $fname;
	}

	/*****************************************************************
	 * エラーメッセージを返し、強制終了
	 *****************************************************************/
	function errorMessage() {
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}
?>
