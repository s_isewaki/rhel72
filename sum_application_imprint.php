<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ワークフロー｜印影登録</title>
<?
require("about_session.php");
require("about_authority.php");
require("about_postgres.php");
require("show_select_values.ini");
require("referer_common.ini");
require("get_values.ini");
require("sum_application_imprint_common.ini");
require("./conf/sql.inf");
require_once("summary_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
  echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
  echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
  exit;
}

//====================================
//権限チェック
//====================================
$summary_check=check_authority($session,57,$fname);
if($summary_check=="0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}
// ワークフロー権限の取得
//$workflow_auth = check_authority($session, 3, $fname);

$con = connect2db($fname);

// 職員ID取得
$emp_id=get_emp_id($con,$session,$fname);


?>

<?
// 印影機能フラグ取得
$db_imprint_flg = get_imprint_flg($con, $emp_id, $fname);

$delbtn_flg = "f";

// 画像登録
if (@$regist_flg == "1") {
  // ファイルアップロードチェック
  define(UPLOAD_ERR_OK, 0);
  define(UPLOAD_ERR_INI_SIZE, 1);
  define(UPLOAD_ERR_FORM_SIZE, 2);
  define(UPLOAD_ERR_PARTIAL, 3);
  define(UPLOAD_ERR_NO_FILE, 4);

  $filename_accepted = $_FILES["imprint_accepted"]["name"];
  $filename_operated = $_FILES["imprint_operated"]["name"];

  if ($filename_accepted != "") {
    $delbtn_flg = "t";
    // gif, jpgチェック
    $pos = strrpos($filename_accepted, ".");
    $ext = "";
    if ($pos > 0 ) {
      $ext = substr($filename_accepted, $pos+1, 3);
      $ext = strtolower($ext);
    }

    if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
      echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    switch ($_FILES["imprint_accepted"]["error"]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    // ファイル保存用ディレクトリがなければ作成
    if (!is_dir("summary")) {
      mkdir("summary", 0755);
    }
    if (!is_dir("summary/imprint")) {
      mkdir("summary/imprint", 0755);
    }


    // 画像がある場合は削除
    $filename_accepted1 = "summary/imprint/".$emp_id."_accepted.gif";
    $filename_accepted2 = "summary/imprint/".$emp_id."_accepted.jpg";

    if (is_file($filename_accepted1)) {
      unlink($filename_accepted1);
    }
    if (is_file($filename_accepted2)) {
      unlink($filename_accepted2);
    }
    // アップロードされたファイルを保存
    $savefilename = "summary/imprint/".$emp_id."_accepted.".$ext;
    $ret = copy($_FILES["imprint_accepted"]["tmp_name"], $savefilename);

    if ($ret == false) {
      echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }
  }

  if ($filename_operated != "") {
    $delbtn_flg = "t";
    // gif, jpgチェック
    $pos = strrpos($filename_operated, ".");
    $ext = "";
    if ($pos > 0 ) {
      $ext = substr($filename_operated, $pos+1, 3);
      $ext = strtolower($ext);
    }

    if ($pos === false || ($ext != "gif" && $ext != "jpg")) {
      echo("<script language=\"javascript\">alert('拡張子はgifかjpgのファイルを指定してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    switch ($_FILES["imprint_operated"]["error"]) {
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      echo("<script language=\"javascript\">alert('ファイルサイズが大きすぎます。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    case UPLOAD_ERR_PARTIAL:
    case UPLOAD_ERR_NO_FILE:
      echo("<script language=\"javascript\">alert('アップロードに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }

    // ファイル保存用ディレクトリがなければ作成
    if (!is_dir("summary")) {
      mkdir("summary", 0755);
    }
    if (!is_dir("summary/imprint")) {
      mkdir("summary/imprint", 0755);
    }


    // 画像がある場合は削除
    $filename_operated1 = "summary/imprint/".$emp_id."_operated.gif";
    $filename_operated2 = "summary/imprint/".$emp_id."_operated.jpg";

    if (is_file($filename_operated1)) {
      unlink($filename_operated1);
    }
    if (is_file($filename_operated2)) {
      unlink($filename_operated2);
    }
    // アップロードされたファイルを保存
    $savefilename = "summary/imprint/".$emp_id."_operated.".$ext;
    $ret = copy($_FILES["imprint_operated"]["tmp_name"], $savefilename);

    if ($ret == false) {
      echo("<script language=\"javascript\">alert('ファイルのコピーに失敗しました。再度実行してください。');</script>");
      echo("<script language=\"javascript\">history.back();</script>");
      exit;
    }
  }

  // 印影機能フラグ登録
  $insert_update_flg = "insert";
  if ($db_imprint_flg != "") {
    $insert_update_flg = "update";
  }
  regist_imprint_flg($con, $fname, $emp_id, $imprint_flg, $insert_update_flg);


} else if (@$regist_flg == "2") {
  $delbtn_flg = "f";
  // 画像がある場合は削除
  $filename_accepted1 = "summary/imprint/".$emp_id."_accepted.gif";
  $filename_accepted2 = "summary/imprint/".$emp_id."_accepted.jpg";
  $filename_operated1 = "summary/imprint/".$emp_id."_operated.gif";
  $filename_operated2 = "summary/imprint/".$emp_id."_operated.jpg";

  if (is_file($filename_accepted1)) unlink($filename_accepted1);
  if (is_file($filename_accepted2)) unlink($filename_accepted2);
  if (is_file($filename_operated1)) unlink($filename_operated1);
  if (is_file($filename_operated2)) unlink($filename_operated2);

  // 印影機能レコード削除
  regist_imprint_flg($con, $fname, $emp_id, "", "delete");
}

if (@$imprint_flg == "") {
  $imprint_flg = "f";
  if ($db_imprint_flg != "") $imprint_flg = $db_imprint_flg;
}


?>


<script type="text/javascript">
function submitForm(flg) {
  document.wkfw.regist_flg.value = flg;
  document.wkfw.submit();
}

function referTemplate() {
  window.open('sum_workflow_template_refer.php?session=<? echo($session); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
</head>
<body>


<? summary_common_show_sinryo_top_header($session, $fname, "印影登録"); ?>

<? summary_common_show_main_tab_menu($con, $fname, "印影登録", 1); ?>


<form name="wkfw" action="sum_application_imprint.php" method="post" enctype="multipart/form-data">
<table class="list_table" cellspacing="1">
	<tr>
		<th style="text-align:right; width:200px">印影機能を利用する</th>
		<td>
			<label><input type="radio" name="imprint_flg" class="baseline" value="t"<? if ($imprint_flg == "t") {echo(" checked");} ?>>はい</label>
			<label><input type="radio" name="imprint_flg" class="baseline" value="f"<? if ($imprint_flg != "t") {echo(" checked");} ?>>いいえ</label>
		</td>
		<th style="text-align:center; width:100px">プレビュー</th>
	</tr>
	<tr>
		<th style="text-align:right">[受付]印影画像指定(jpeg,gif)</th>
		<td><input type="file" name="imprint_accepted" size="60"></td>
		<td style="text-align:center">
			<?
			// 画像がある場合は表示
			$filename = "";
			$filename1 = "summary/imprint/".$emp_id."_accepted.gif";
			$filename2 = "summary/imprint/".$emp_id."_accepted.jpg";
			if (is_file($filename1)) $filename = $filename1;
			else if (is_file($filename2)) $filename = $filename2;

			if ($filename == "") { ?>
			<img src="img/inei.gif">
			<? } else {
			  show_imprint_image($session, $emp_id, 1, "t", "t", "1", 1);
			  $delbtn_flg = "t";
			} ?>
		</td>
	</tr>

	<tr>
		<th style="text-align:right">[実施]印影画像指定(jpeg,gif)</th>
		<td><input type="file" name="imprint_operated" size="60"></td>
		<td style="text-align:center">
			<?
			// 画像がある場合は表示
			$filename = "";
			$filename1 = "summary/imprint/".$emp_id."_operated.gif";
			$filename2 = "summary/imprint/".$emp_id."_operated.jpg";
			if (is_file($filename1)) $filename = $filename1;
			else if (is_file($filename2)) $filename = $filename2;

			if ($filename == "") { ?>
			<img src="img/inei.gif">
			<? } else {
			  // 印影表示関数
			  show_imprint_image($session, $emp_id, 4, "t", "t", "1", 1);
			  $delbtn_flg = "t";
			} ?>
		</td>
	</tr>
</table>

<div class="search_button_div">
	<? if ($delbtn_flg == "t") { ?>
	<input type="button" onclick="if(confirm('削除します。よろしいですか？')){submitForm('2');}" value="削除"/>
	<? } ?>
	<input type="button" onclick="submitForm('1');" value="登録"/>
</div>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="regist_flg" value="">

</form>

</body>
<? pg_close($con); ?>

</html>
