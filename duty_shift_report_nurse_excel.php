<?
///*****************************************************************************
// 勤務シフト作成 | 看護職員表「ＥＸＣＥＬ」 
///*****************************************************************************

ini_set("max_execution_time", 0);
ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_report_common_class.php");
require_once("duty_shift_report1_common_class.php");
require_once("duty_shift_report2_common_class.php");

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_report = new duty_shift_report_common_class($con, $fname);
$obj_report1 = new duty_shift_report1_common_class($con, $fname);
$obj_report2 = new duty_shift_report2_common_class($con, $fname);
///-----------------------------------------------------------------------------
//処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------
	$sum_night_times = array();	//「夜勤専従者及び月１６時間以下」の合計
	$excel_flg = "1";			//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
	$download_data = "";		//ＥＸＣＥＬデータ
	///-----------------------------------------------------------------------------
	//現在日付の取得
	///-----------------------------------------------------------------------------
	$date = getdate();
	$now_yyyy = $date["year"];
	$now_mm = $date["mon"];
	$now_dd = $date["mday"];
	///-----------------------------------------------------------------------------
	//表示年／月設定
	///-----------------------------------------------------------------------------
	if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
	if ($duty_mm == "") { $duty_mm = $now_mm; }
	$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
	///-----------------------------------------------------------------------------
	//終了年月
	///-----------------------------------------------------------------------------
	if ($night_start_day == "") {
		$night_start_day = "1";
	}
	$end_yyyy = (int)$duty_yyyy;
	$end_mm = (int)$duty_mm;
	$end_dd = (int)$day_cnt;
	if ($recomputation_flg == "2") {
		$wk_day = (int)$night_start_day + 27;
		$tmp_time = mktime(0, 0, 0, $duty_mm, $wk_day, $duty_yyyy);
		$tmp_date = date("Ymd", $tmp_time);
		$end_yyyy = (int)substr($tmp_date,0,4);
		$end_mm = (int)substr($tmp_date,4,2);
		$end_dd = (int)substr($tmp_date,6,2);
		$day_cnt = 28;
	}
//画面の情報をhiddenで引き継ぐため、DBからのデータ取得は不要 20110901
$data_array = array();
$data_sub_array = array();

	///-----------------------------------------------------------------------------
	//ファイル名
	///-----------------------------------------------------------------------------
if  ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
    $filename = 'youshiki_9_3.xls';
}
else {
    $filename = 'youshiki_9.xls';
}
	if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	    $encoding = 'sjis';
	} else {
	    $encoding = mb_http_output();   // Firefox
	}
	$filename = mb_convert_encoding(
	                $filename,
	                $encoding,
	                mb_internal_encoding());

    ///*****************************************************************************
    //ＨＴＭＬ作成（入院基本料の施設基準等に係る届出書添付書類(様式９)）
    ///*****************************************************************************
    $prt_flg = "1";
    //特定入院料関連データ
    $specific_data = compact("specific_type", "nurse_staffing_cnt", "assistance_staffing_cnt");
    
    //申し送り時間
    $send_hm = compact("send_start_hhmm_1", "send_end_hhmm_1", "send_start_hhmm_2", "send_end_hhmm_2", "send_start_hhmm_3", "send_end_hhmm_3");

    //作成年月日(yyyymmdd)
    $create_ymd = compact("create_yyyy", "create_mm", "create_dd");

    $wk_array = $obj_report->showData(
                                    $prt_flg,               //表示画面フラグ（１：看護職員表、２：看護補助職員表）
                                    $excel_flg,             //ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
                                    $recomputation_flg,     //１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
                                    $data_array,            //看護師／准看護師情報
                                    $data_sub_array,        //看護補助者情報
                                    $hospital_name,         //保険医療機関名
                                    $ward_cnt,              //病棟数
                                    $sickbed_cnt,           //病床数
                                    $report_kbn,            //届出区分
                                    $report_kbn_name,       //届出区分名
                                    $report_patient_cnt,    //届出時入院患者数
                                    $hosp_patient_cnt,      //１日平均入院患者数
                                    $compute_start_yyyy,    //１日平均入院患者数 算出期間（開始年）
                                    $compute_start_mm,      //１日平均入院患者数 算出期間（開始月）
                                    $compute_end_yyyy,      //１日平均入院患者数 算出期間（終了年）
                                    $compute_end_mm,        //１日平均入院患者数 算出期間（終了月）
                                    $nurse_sumtime,             //勤務時間数（看護師）
                                    $sub_nurse_sumtime,         //勤務時間数（准看護師）
                                    $assistance_nurse_sumtime,  //勤務時間数（看護補助者）
                                    $nurse_cnt,             //看護師数
                                    $sub_nurse_cnt,         //准看護師数
                                    $assistance_nurse_cnt,  //看護補助者数
                                    $hospitalization_cnt,   //平均在院日数
                                    $ave_start_yyyy,        //平均在院日数 算出期間（開始年）
                                    $ave_start_mm,          //平均在院日数 算出期間（開始月）
                                    $ave_end_yyyy,          //平均在院日数 算出期間（終了年）
                                    $ave_end_mm,            //平均在院日数 算出期間（終了月）
                                    $prov_start_hh,         //夜勤時間帯 （開始時）
                                    $prov_start_mm,         //夜勤時間帯 （開始分）
                                    $prov_end_hh,           //夜勤時間帯 （終了時）
                                    $prov_end_mm,           //夜勤時間帯 （終了分）
                                    $duty_yyyy,             //対象年
                                    $duty_mm,               //対象月
                                    $day_cnt,               //日数
                                    $date_y1,               //カレンダー用年
                                    $date_m1,               //カレンダー用月
                                    $date_d1,               //カレンダー用日
                                    $labor_cnt,             //常勤職員の週所定労働時間
                                    compact("create_yyyy", "create_mm", "create_dd"),    //作成年月日(yyyymmdd)
                                    $nurse_staffing_flg,    //看護配置加算の有無                
                                    $acute_nursing_flg,     //急性期看護補助加算の有無              
                                    $acute_nursing_cnt,     //急性期看護補助加算の配置比率              
                                    $nursing_assistance_flg,    //看護補助加算の有無            
                                    $nursing_assistance_cnt,    //看護補助加算の配置比率
                                    $acute_assistance_flg,  //急性期看護補助体制加算の届出区分
                                    $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率
                                    $night_acute_assistance_flg,    //夜間急性期看護補助体制加算の届出区分
                                    $night_acute_assistance_cnt,    //夜間急性期看護補助体制加算の配置比率
                                    $night_shift_add_flg,   //看護職員夜間配置加算の有無
                                    $specific_data,         //特定入院料関連データ
                                    $send_hm,               //申し送り時間
                                    $sum_night_b,
                                    $sum_night_c,
                                    $sum_night_d,
                                    $sum_night_e,
                                    $sum_night_f,
                                    $sum_night_g,
                                    $sum_sub_night_b,
                                    $sum_sub_night_c,
                                    $sum_sub_night_d,
                                    $sum_sub_night_e,
                                    $sum_sub_night_f,
                                    $sum_sub_night_g,
                                    $plan_result_flag       //予定実績フラグ 20120209
                                    );              
    $download_data_0 = $wk_array["data"];

	///*****************************************************************************
	//ＨＴＭＬ作成（看護職員表）
	///*****************************************************************************
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（見出し）
	///-----------------------------------------------------------------------------
//集計表だけ出力し看護職員表は出力しない 20090513
/*
	$btn_show_flg = "";	//１：ボタン表示

	$download_data_1 = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	$download_data_1 .= $obj_report1->showNurseTitle_1($btn_show_flg,
													$recomputation_flg,			//１ヵ月計算４週計算フラグ（１：１ヵ月計算、２：４週計算）
													$date_y1,					//カレンダー用年
													$date_m1,					//カレンダー用月
													$date_d1,					//カレンダー用日
													$day_cnt);					//日数

//	$download_data_1 .= "</table>";
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（タイトル）
	///-----------------------------------------------------------------------------
	$download_data_1 .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	$wk_array1 = $obj_report1->showNurseData_1($excel_flg, $week_array, $calendar_array, $day_cnt);
	$download_data_1 .= $wk_array1["data"];
//	$download_data_1 .= "</table>";
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（データ） 
	///-----------------------------------------------------------------------------
//	$download_data_2 = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
//	$wk_array2 = $obj_report1->showNurseData_2($excel_flg, $data_array, $day_cnt);
//	for ($i=0; $i<count($data_array); $i++) {
//		$download_data_2 .= $wk_array2[$i]["data"];
//	}
//	$download_data_2 .= "</table>";
*/
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（合計　日勤時間数）
	// ＨＴＭＬ作成（合計　夜勤時間数）
	///-----------------------------------------------------------------------------
	$download_data_3 = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
//	$wk_array3 = $obj_report1->showNurseData_3($excel_flg, $data_array, $day_cnt, $wk_array2["sum_night_times"]);
//	$download_data_3 .= $wk_array3["data"];
// レイアウトのため幅を調整する
	$wk_font =  "<font size=\"1\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j9\" color=\"ffffff\">";
	$download_data_3 .= "<tr>\n";
	for ($i=0; $i<30; $i++) {
		$download_data_3 .= "<td width=\"8\">". $wk_font . "_</font></td>\n";
	}

	$download_data_3 .= "</tr>\n";
	$download_data_3 .= "</table>";
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（療養病棟入院基本料における夜勤体制の要件を満たすことの確認）
	///-----------------------------------------------------------------------------
	$download_data_4 = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-family: ＭＳ Ｐゴシック, Osaka; font-size: 13px;\">";
//平成２４年度改訂対応 20120406
$wk_duty_yyyymm = $duty_yyyy.sprintf("%02d", $duty_mm);
if ($wk_duty_yyyymm >= "201204") {
    $wk_array4 = $obj_report1->showNurseData_4_new($excel_flg,
            $day_cnt,
            $sum_night_b,
            $sum_night_c,
            $sum_night_d,
            $sum_night_e,
            $sum_night_f,
            $sum_night_g,
            $sum_night_h,
            $sum_night_i,
            $report_kbn
            );
}
else {
    $wk_array4 = $obj_report1->showNurseData_4($excel_flg,
            $day_cnt,
            $sum_night_b,
            $sum_night_c,
            $sum_night_d,
            $sum_night_e,
            $sum_night_f,
            $sum_night_g);
}
	$download_data_4 .= $wk_array4["data"];
	$download_data_4 .= "</table>";
	///-----------------------------------------------------------------------------
	// ＨＴＭＬ作成（療養病棟入院基本料における夜勤体制の要件を満たすことの確認）
	///-----------------------------------------------------------------------------
	$download_data_5 = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n";

//地域包括ケア対応 20140326
if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
    //看護職員
    $wk_array5 = $obj_report1->showCommunityCareData($excel_flg,
            $day_cnt,
            $sum_night_b,
            $sum_night_c,
            $sum_night_d,
            $sum_night_e,
            $sum_night_f,
            $sum_night_g,
            $sum_night_h,
            $sum_night_i,
            $report_kbn
            );
    //看護補助者
    $wk_array6 = $obj_report2->showCommunityCareData($excel_flg,
            $day_cnt,
            $sum_sub_night_b,
            $sum_sub_night_c,
            $sum_sub_night_d,
            $sum_sub_night_e,
            $sum_sub_night_f,
            $sum_sub_night_g,
            $sum_night_c,
            $sum_night_f,
            $hosp_patient_cnt,
            $night_acute_assistance_cnt,
            $report_kbn,
            $nurse_staffing_flg    //看護配置加算の有無
            );
}
elseif ($wk_duty_yyyymm >= "201204") {
    $wk_array5 = $obj_report2->showNurseData_4_new($excel_flg,
            $day_cnt,
            $sum_sub_night_b,
            $sum_sub_night_c,
            $sum_sub_night_d,
            $sum_sub_night_e,
            $sum_sub_night_f,
            $sum_sub_night_g,
            $sum_night_c,
            $sum_night_f,
            $hosp_patient_cnt,
            $acute_assistance_cnt,  //急性期看護補助体制加算の配置比率 20141111
            $night_acute_assistance_cnt,
            $report_kbn
            );
}
else {
    $wk_array5 = $obj_report2->showNurseData_4($excel_flg,
            $day_cnt,
            $sum_sub_night_b,
            $sum_sub_night_c,
            $sum_sub_night_d,
            $sum_sub_night_e,
            $sum_sub_night_f,
            $sum_sub_night_g,
            $sum_night_c,
            $sum_night_f);
}
$download_data_5 .= $wk_array5["data"];
if ($report_kbn == REPORT_KBN_COMMUNITY_CARE || $report_kbn == REPORT_KBN_COMMUNITY_CARE2) {
    $download_data_5 .= $wk_array6["data"];
}
	$download_data_5 .= "</table>\n";
	///-----------------------------------------------------------------------------
	//データEXCEL出力
	///-----------------------------------------------------------------------------
	ob_clean();
	header("Content-Type: application/vnd.ms-excel");
	header('Content-Disposition: attachment; filename=' . $filename);
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
	header('Pragma: public');
	echo mb_convert_encoding(nl2br($download_data_0), 'sjis', mb_internal_encoding());
//	echo mb_convert_encoding(nl2br($download_data_1), 'sjis', mb_internal_encoding());
//	echo mb_convert_encoding(nl2br($download_data_2), 'sjis', mb_internal_encoding());
	echo mb_convert_encoding(nl2br($download_data_3), 'sjis', mb_internal_encoding());
	echo mb_convert_encoding(nl2br($download_data_4), 'sjis', mb_internal_encoding());
	echo mb_convert_encoding(nl2br($download_data_5), 'sjis', mb_internal_encoding());
	ob_end_flush();

	pg_close($con);
?>

