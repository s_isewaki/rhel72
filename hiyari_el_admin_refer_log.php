<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");


//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
if (check_authority($session, 48, $fname) == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
// デフォルト値の設定
//==============================
if ($sort == "") {$sort = "1";}  // 並び順：「累積参照数の多い順」
if ($page == "") {$page = 1;}  // ページ数：1


//画面名
$PAGE_TITLE = "参照履歴";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteDocument(lib_id)
{
    if (!confirm('削除します。よろしいですか？'))
    {
        return;
    }
    document.delform.elements['did[]'].value = lib_id;
    document.delform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- ヘッダー START -->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<?
    show_hiyari_header_for_sub_window($PAGE_TITLE);
?>
</table>
<!-- ヘッダー END -->


</td></tr><tr><td>

<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">





<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#FFFFFF">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の多い順", "1", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の多い順", "2", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の多い順", "3", $sort, $session); ?></font></td>
</tr>
<tr height="22" bgcolor="#FFFFFF">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の少ない順", "4", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の少ない順", "5", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の少ない順", "6", $sort, $session); ?></font></td>
</tr>
</table>
<?
show_library_list($con, $sort, $page, $session, $fname);
?>




        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->





</td>
</tr>
</table>
<form name="delform" action="hiyari_el_delete.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="did[]" value="">
<input type="hidden" name="path" value="2">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
<?







function show_submenu_label($label, $sort, $selected, $session)
{
    if ($selected == $sort)
    {
        echo("<b>");
    }
    else
    {
        echo("<a href=\"hiyari_el_admin_refer_log.php?session=$session&sort=$sort\">");
    }
    echo($label);
    if ($selected == $sort)
    {
        echo("</b>");
    }
    else
    {
        echo("</a>");
    }
}

function show_library_list($con, $sort, $page, $session, $fname)
{
    $limit = 15;

    // コンテンツ数を取得（ページング処理のため）
    $sql = "select count(*) from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
    $cond = "where lib_delete_flag = 'f' and lib_id in (select lib_id from el_edition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from el_edition group by base_lib_id) latest_edition where latest_edition.base_lib_id = el_edition.base_lib_id and latest_edition.edition_no = el_edition.edition_no))";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $lib_count = intval(pg_fetch_result($sel, 0, 0));
    if ($lib_count == 0)
    {
        return;
    }

    // コンテンツ情報の取得
    $sql_common = "el_info.*, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = el_info.emp_id) as emp_nm, el_cate.lib_cate_nm, (select folder_name from el_folder where el_folder.folder_id = el_info.folder_id) as folder_nm from el_info inner join (select * from el_cate where el_cate.libcate_del_flg = 'f') el_cate on el_info.lib_cate_id = el_cate.lib_cate_id";
    if ($sort == "1" || $sort == "4")
    {
        // 期間指定なし
        $sql = "select $sql_common";
    }
    else
    {
        // 期間指定あり
        if ($sort == "2" || $sort == "5")
        {
            // 直近3ヶ月
            $comp_time = date("YmdHi", strtotime("-3 months"));
        }
        else
        {
            $comp_time = date("YmdHi", strtotime("-1 month"));
        }
        $sql = "select el_reflog.ref_count2, $sql_common left join (select lib_id, count(*) as ref_count2 from el_reflog where ref_date >= '$comp_time' group by lib_id) el_reflog on el_info.lib_id = el_reflog.lib_id";
    }
    $cond = "where el_info.lib_delete_flag = 'f' and el_info.lib_id in (select lib_id from el_edition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from el_edition group by base_lib_id) latest_edition where latest_edition.base_lib_id = el_edition.base_lib_id and latest_edition.edition_no = el_edition.edition_no)) ";
    switch ($sort)
    {
        case "1":  // 累積参照数の多い順
            $cond .= "order by el_info.ref_count desc, el_info.lib_up_date desc";
            break;
        case "2":  // 直近3ヶ月で参照数の多い順
            $cond .= "order by coalesce(el_reflog.ref_count2, 0) desc, el_info.lib_up_date desc";
            break;
        case "3":  // 直近1ヶ月で参照数の多い順
            $cond .= "order by coalesce(el_reflog.ref_count2, 0) desc, el_info.lib_up_date desc";
            break;
        case "4":  // 累積参照数の少ない順
            $cond .= "order by el_info.ref_count, el_info.lib_up_date";
            break;
        case "5":  // 直近3ヶ月で参照数の少ない順
            $cond .= "order by coalesce(el_reflog.ref_count2, 0), el_info.lib_up_date";
            break;
        case "6":  // 直近1ヶ月で参照数の少ない順
            $cond .= "order by coalesce(el_reflog.ref_count2, 0), el_info.lib_up_date";
            break;
    }
    $offset = $limit * ($page - 1);
    $cond .= " offset $offset limit $limit";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ページ番号の表示
    if ($lib_count > $limit)
    {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:5px 0 2px;\">\n");
        echo("<tr>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");

        if ($page > 1)
        {
            echo("<a href=\"$fname?session=$session&sort=$sort&page=" . ($page - 1) . "\">＜前</a>　\n");
        }
        else
        {
            echo("＜前　\n");
        }

        $page_count = ceil($lib_count / $limit);
        for ($i = 1; $i <= $page_count; $i++)
        {
            if ($i != $page)
            {
                echo("<a href=\"$fname?session=$session&sort=$sort&page=$i\">$i</a>\n");
            }
            else
            {
                echo("<b>$i</b>\n");
            }
        }

        if ($page < $page_count)
        {
            echo("　<a href=\"$fname?session=$session&sort=$sort&page=" . ($page + 1) . "\">次＞</a>\n");
        }
        else
        {
            echo("　次＞\n");
        }

        echo("</font></td>\n");
        echo("</tr>\n");

        echo("</table>\n");
    }
    else
    {
        echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
    }

    // コンテンツ一覧の表示
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
    echo("<tr height=\"22\" bgcolor=\"#DFFFDC\">\n");
    echo("<td width=\"22%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">コンテンツ名</font></td>\n");
    echo("<td width=\"7%\" align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照数</font></td>\n");
    echo("<td width=\"12%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">更新日時</font></td>\n");
    echo("<td width=\"11%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録者</font></td>\n");
    echo("<td width=\"36%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">保存先</font></td>\n");
    echo("<td width=\"6%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照</font></td>\n");
    echo("<td width=\"6%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
    echo("</tr>\n");
    while ($row = pg_fetch_array($sel))
    {
        $tmp_lib_id = $row["lib_id"];
        $tmp_lib_nm = $row["lib_nm"];
        if ($sort == "1" || $sort == "4")
        {
            $tmp_ref_count =  intval($row["ref_count"]);
        }
        else
        {
            $tmp_ref_count =  intval($row["ref_count2"]);
        }
        $tmp_lib_up_date = format_datetime($row["lib_up_date"]);
        $tmp_emp_nm = $row["emp_nm"];
//      $tmp_lib_archive = get_lib_archive_label($row["lib_archive"]);
        $tmp_lib_cate_nm = $row["lib_cate_nm"];
        $tmp_folder_id = $row["folder_id"];
        $tmp_lib_url = get_lib_url($row["lib_cate_id"], $tmp_lib_id, $row["lib_extension"], $session);

        echo("<tr height=\"22\" valign=\"top\"bgcolor=\"#FFFFFF\">\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_lib_nm</font></td>\n");
        echo("<td align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($tmp_ref_count > 0)
        {
            echo("<a href=\"javascript:void(0);\" onclick=\"window.open('hiyari_el_admin_refer_log_detail.php?session=$session&lib_id=$tmp_lib_id', 'newwin', 'scrollbars=yes,width=640,height=700');\">");
        }
        echo($tmp_ref_count);
        if ($tmp_ref_count > 0)
        {
            echo("</a>");
        }
        echo("</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_lib_up_date</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_nm</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        echo($tmp_lib_cate_nm);
        write_folder_path($con, $tmp_folder_id, $fname, true);
        echo("</font></td>\n");
        if (lib_get_filename_flg() > 0)
        {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"location.href = '$tmp_lib_url';\"></td>\n");
        }
        else
        {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"window.open('$tmp_lib_url');\"></td>\n");
        }
        echo("<td align=\"center\"><input type=\"button\" value=\"削除\" onclick=\"deleteDocument('$tmp_lib_id');\"></td>\n");
        echo("</tr>\n");
    }
    echo("</table>\n");
}

//function get_lib_archive_label($lib_archive)
//{
//  $archives = array(
//      "1" => "個人ファイル",
//      "2" => "全体共有",
//      "3" => "部署共有",
//      "4" => "委員会・WG共有"
//      );
//  return $archives[$lib_archive];
//}

function format_datetime($datetime)
{
    if (strlen($datetime) == 14)
    {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5:$6", $datetime);
    }
    else
    {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $datetime);
    }
}

?>
