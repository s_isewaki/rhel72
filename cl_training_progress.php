<?
//ini_set("display_errors","1");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("cl_application_list.inc");
require_once("cl_yui_calendar_util.ini");
require_once("cl_application_common.ini");
require_once("cl_common.ini");
require_once("cl_application_workflow_common_class.php");
require_once("cl_title_name.ini");
require_once("cl_common_log_class.inc");
require_once("cl_smarty_setting.ini");
require_once("Cmx.php");
require_once("MDB2.php");

$log = new cl_common_log_class(basename(__FILE__));

$log->info(basename(__FILE__)." START");

foreach( $_REQUEST as $key => $value ) {
	$log->debug('$_REQUEST['.$key.']:'.$_REQUEST[$key],__FILE__,__LINE__);
}


$smarty = cl_get_smarty_obj();
$smarty->template_dir = dirname(__FILE__) . "/cl/view";

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
// ワークフロー権限の取得
$workflow_auth = check_authority($session, $CL_MANAGE_AUTH, $fname);

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

/**
 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
 */
$yui_cal_part=get_yui_cal_part();

$calendar = '';

/**
 * アプリケーションメニューHTML取得
 */
$aplyctn_mnitm_str=get_applycation_menuitem($session,$fname);

/**
 * 志願申請、評価申請用の志願者取得（申請者名）
 */
$arr_emp = $obj->get_empmst($session);
$emp_id  = $arr_emp["0"]["emp_id"];
$lt_nm  = $arr_emp["0"]["emp_lt_nm"];
$ft_nm  = $arr_emp["0"]["emp_ft_nm"];

$applicant_name = "$lt_nm"." "."$ft_nm";

/**
 * Javascriptコード生成（動的オプション項目変更用）
 */
$js_str = get_js_str($arr_wkfwcatemst,$workflow);

/**
 * データベースクローズ
 */
pg_close($con);

/**
 *
 */
if (
	$_REQUEST['mode'] == 'detail'
	|| $_POST['mode'] == 'detail'
) {
	$mode = 'detail';
} else {
	$mode = '';
}

if ($_REQUEST['emp_id'] != ''){
	$target_emp_id = $_REQUEST['emp_id'];
	if ($_REQUEST['year'] == '') {
		if (date(n) <= 3){
			$option_search_year = date(Y) - 1;
		} else {
			$option_search_year = date(Y);
		}
	} else {
		$option_search_year = $_REQUEST['year'];
	}
} else if ($_POST['target_emp_id'] != '') {
	$target_emp_id = $_POST['target_emp_id'];
	$option_search_year = $_POST['search_year'];
} else {
	$target_emp_id = $emp_id;
	$option_search_year = get_select_around_years($_POST['search_year']);
}

//##########################################################################
// データベース接続オブジェクト取得
//##########################################################################
$log->debug("MDB2オブジェクト取得開始",__FILE__,__LINE__);
$mdb2 = MDB2::connect(CMX_DB_DSN);
if (PEAR::isError($mdb2)) {
	$log->error("MDB2オブジェクト取得エラー：".$mdb2->getDebugInfo(),__FILE__,__LINE__);
}
$log->debug("MDB2オブジェクト取得終了",__FILE__,__LINE__);

require_once("cl/model/search/cl_training_progress_model.php");
$progress_model = new cl_training_progress_model($mdb2,$emp_id);
$log->debug("研修受講進捗用DBA取得",__FILE__,__LINE__);





//========================================
// 権限判定(レベルV認定項目の編集可否)
//========================================
//※当画面は看護教育委員会、審議会、看護部長、所属長がアクセス可能

if ($mode == 'detail')
{
	// 2012/08/21 Yamagawa del(s)
	//require_once("cl/model/master/cl_mst_nurse_education_committee_model.php");
	//$education_committee_model = new cl_mst_nurse_education_committee_model($mdb2,$emp_id);
	//$log->debug("看護教育委員会DBA取得",__FILE__,__LINE__);
	// 2012/08/21 Yamagawa del(e)

	require_once("cl/model/master/cl_mst_setting_council_model.php");
	$council_model = new cl_mst_setting_council_model($mdb2,$emp_id);
	$log->debug("審議会DBA取得",__FILE__,__LINE__);

	// 2012/08/21 Yamagawa del(s)
	/*
	require_once("cl/model/master/cl_mst_nurse_manager_st_model.php");
	$manager_st_model = new cl_mst_nurse_manager_st_model($mdb2,$emp_id);
	$log->debug("所属長DBA取得",__FILE__,__LINE__);

	require_once("cl/model/master/cl_mst_supervisor_st_model.php");
	$supervisor_st_model = new cl_mst_supervisor_st_model($mdb2,$emp_id);
	$log->debug("看護部長DBA取得",__FILE__,__LINE__);

	// 看護教育委員会チェック
	$education_committee = $education_committee_model->check_education_committee($emp_id);
	*/
	// 2012/08/21 Yamagawa del(e)

	// 審議会チェック
	$council = $council_model->check_council($emp_id);

	// 2012/08/21 Yamagawa del(s)
	/*
	// 所属長(役職)チェック
	$supervisor = $supervisor_st_model->check_supervisor($emp_id);

	// 看護部長(役職)チェック
	$nurse_manager = $manager_st_model->check_nurse_manager($emp_id);
	*/
	// 2012/08/21 Yamagawa del(e)

	// 2012/08/21 Yamagawa upd(s)
	/*
	//看護教育委員会、審議会、看護部長の場合
	if (count($education_committee) > 0 || count($council) > 0 ||  count($nurse_manager) > 0)
	{
		//レベルV認定の編集が可能
		$level5_edit_flg = true;
	}
	//所属長の場合(看護教育委員会、審議会、看護部長と兼務していない場合)
	else
	{
		//レベルV認定の編集が不可能
		$level5_edit_flg = false;
	}
	*/
	if (count($council) > 0) {
		$council_flg = true;
	} else {
		$council_flg = false;
	}
	// 2012/08/21 Yamagawa upd(e)
}
else
{
	//この変数は使用しない。
	// 2012/08/21 Yamagawa upd(s)
	//$level5_edit_flg = false;
	$council_flg = false;
	// 2012/08/21 Yamagawa upd(e)
}









if ($_POST['regist_flg'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	$log->debug('院外研修モデル読込　開始',__FILE__,__LINE__);
	require_once("cl/model/template/cl_apl_outside_seminar_model.php");
	$outside_seminar_model = new cl_apl_outside_seminar_model($mdb2, $emp_id);
	$log->debug('院外研修モデル読込　終了',__FILE__,__LINE__);

	$seminar_apply_id	= explode(',',$all_seminar_apply_id);
	$report_date		= explode(',',$all_report_date);
	$log->debug('院外研修申請ID：'.count($seminar_apply_id),__FILE__,__LINE__);
	$log->debug('報告日'.count($report_date),__FILE__,__LINE__);

	for($i=0;$i<count($seminar_apply_id);$i++){
		if ($report_date[$i] == '') {
			$param = array(
				'login_user_id'			=> $login_user_id
				,'seminar_apply_id'		=> $seminar_apply_id[$i]
			);

			$res = $outside_seminar_model->report_date_delete($param);
		} else {
			$param = array(
				'report_date'			=> $report_date[$i]
				,'login_user_id'		=> $login_user_id
				,'seminar_apply_id'		=> $seminar_apply_id[$i]
			);

			$res = $outside_seminar_model->report_date_regist($param);
		}
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);

}

if ($_REQUEST['level_date_regist'] == 'true') {

	// トランザクション開始
	$mdb2->beginTransaction();
	$log->debug("トランザクション開始",__FILE__,__LINE__);

	$log->debug('院外研修モデル読込　開始',__FILE__,__LINE__);
	require_once("cl/model/ladder/cl_personal_profile_model.php");
	$profile_model = new cl_personal_profile_model($mdb2, $emp_id);
	$log->debug('院外研修モデル読込　終了',__FILE__,__LINE__);

	$set_now_level = 0;
	if (
		$_REQUEST['auth_year_level1'] == '-'
		&& $_REQUEST['auth_month_level1'] == '-'
		&& $_REQUEST['auth_day_level1'] == '-'
	){
		$set_level1_date = '';
	} else {
		$set_level1_date = $_POST['auth_year_level1'].'-'. $_POST['auth_month_level1'].'-'.$_POST['auth_day_level1'];
		$set_now_level = 1;
	}

	if (
		$_REQUEST['auth_year_level2'] == '-'
		&& $_REQUEST['auth_month_level2'] == '-'
		&& $_REQUEST['auth_day_level2'] == '-'
	){
		$set_level2_date = '';
	} else {
		$set_level2_date = $_POST['auth_year_level2'].'-'. $_POST['auth_month_level2'].'-'.$_POST['auth_day_level2'];
		$set_now_level = 2;
	}

	if (
		$_REQUEST['auth_year_level3'] == '-'
		&& $_REQUEST['auth_month_level3'] == '-'
		&& $_REQUEST['auth_day_level3'] == '-'
	){
		$set_level3_date = '';
	} else {
		$set_level3_date = $_POST['auth_year_level3'].'-'. $_POST['auth_month_level3'].'-'.$_POST['auth_day_level3'];
		$set_now_level = 3;
	}

	if (
		$_REQUEST['auth_year_level4'] == '-'
		&& $_REQUEST['auth_month_level4'] == '-'
		&& $_REQUEST['auth_day_level4'] == '-'
	){
		$set_level4_date = '';
	} else {
		$set_level4_date = $_POST['auth_year_level4'].'-'. $_POST['auth_month_level4'].'-'.$_POST['auth_day_level4'];
		$set_now_level = 4;
	}

	if (
		$_REQUEST['auth_year_level5'] == '-'
		&& $_REQUEST['auth_month_level5'] == '-'
		&& $_REQUEST['auth_day_level5'] == '-'
	){
		$set_level5_date = '';
	} else {
		$set_level5_date = $_POST['auth_year_level5'].'-'. $_POST['auth_month_level5'].'-'.$_POST['auth_day_level5'];
		$set_now_level = 5;
	}

	//2012/05/14 K.Fujii ins(s)
	//職員情報取得
	$log->debug("require START cl_emp_class_history_list_model.php",__FILE__,__LINE__);
	require_once(dirname(__FILE__) . "/cl/model/search/cl_emp_class_history_list_model.php");
	$log->debug("require END cl_emp_class_history_list_model.php",__FILE__,__LINE__);

	$log->debug("cl_emp_class_history_list_model インスタンス作成開始",__FILE__,__LINE__);
	$cl_emp_class_history_list_model = new cl_emp_class_history_list_model($mdb2, $user);
	$log->debug("cl_emp_class_history_list_model インスタンス作成終了",__FILE__,__LINE__);

	$log->debug("職員情報取得開始",__FILE__,__LINE__);
	$log->debug("emp_id:".$target_emp_id,__FILE__,__LINE__);
	$emp_data = $cl_emp_class_history_list_model->get_emp_data_history($target_emp_id);
	$log->debug("職員情報取得終了",__FILE__,__LINE__);

	//認定時の所属設定
	if(count($emp_data) == 1){
		if($set_level1_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level1_date >= $emp_data[0]["histdate"]){
				$emp_class_L1 = $emp_data[0]["emp_class"];
				$emp_attribute_L1 = $emp_data[0]["emp_attribute"];
				$emp_dept_L1 = $emp_data[0]["emp_dept"];
				$emp_room_L1 = $emp_data[0]["emp_room"];
			}else{
				$emp_class_L1 = $emp_data[0]["his_class"];
				$emp_attribute_L1 = $emp_data[0]["his_atrib"];
				$emp_dept_L1 = $emp_data[0]["his_dept"];
				$emp_room_L1 = $emp_data[0]["his_room"];
			}
		}
		if($set_level2_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level2_date >= $emp_data[0]["histdate"]){
				$emp_class_L2 = $emp_data[0]["emp_class"];
				$emp_attribute_L2 = $emp_data[0]["emp_attribute"];
				$emp_dept_L2 = $emp_data[0]["emp_dept"];
				$emp_room_L2 = $emp_data[0]["emp_room"];
			}else{
				$emp_class_L2 = $emp_data[0]["his_class"];
				$emp_attribute_L2 = $emp_data[0]["his_atrib"];
				$emp_dept_L2 = $emp_data[0]["his_dept"];
				$emp_room_L2 = $emp_data[0]["his_room"];
			}
		}
		if($set_level3_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level3_date >= $emp_data[0]["histdate"]){
				$emp_class_L3 = $emp_data[0]["emp_class"];
				$emp_attribute_L3 = $emp_data[0]["emp_attribute"];
				$emp_dept_L3 = $emp_data[0]["emp_dept"];
				$emp_room_L3 = $emp_data[0]["emp_room"];
			}else{
				$emp_class_L3 = $emp_data[0]["his_class"];
				$emp_attribute_L3 = $emp_data[0]["his_atrib"];
				$emp_dept_L3 = $emp_data[0]["his_dept"];
				$emp_room_L3 = $emp_data[0]["his_room"];
			}
		}
		if($set_level4_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level4_date >= $emp_data[0]["histdate"]){
				$emp_class_L4 = $emp_data[0]["emp_class"];
				$emp_attribute_L4 = $emp_data[0]["emp_attribute"];
				$emp_dept_L4 = $emp_data[0]["emp_dept"];
				$emp_room_L4 = $emp_data[0]["emp_room"];
			}else{
				$emp_class_L1 = $emp_data[0]["his_class"];
				$emp_attribute_L1 = $emp_data[0]["his_atrib"];
				$emp_dept_L1 = $emp_data[0]["his_dept"];
				$emp_room_L1 = $emp_data[0]["his_room"];
			}
		}
		if($set_level5_date != ""){
			if($emp_data[0]["histdate"] == "" || $set_level5_date >= $emp_data[0]["histdate"]){
				$emp_class_L5 = $emp_data[0]["emp_class"];
				$emp_attribute_L5 = $emp_data[0]["emp_attribute"];
				$emp_dept_L5 = $emp_data[0]["emp_dept"];
				$emp_room_L5 = $emp_data[0]["emp_room"];
			}else{
				$emp_class_L5 = $emp_data[0]["his_class"];
				$emp_attribute_L5 = $emp_data[0]["his_atrib"];
				$emp_dept_L5 = $emp_data[0]["his_dept"];
				$emp_room_L5 = $emp_data[0]["his_room"];
			}
		}
	}else{
		$last_his_date = "";
		for($i=0;$i<count($emp_data);$i++){
			if($set_level1_date != ""){
				if($i == 0 && $set_level1_date < $emp_data[$i]["histdate"]){
					$emp_class_L1 = $emp_data[$i]["his_class"];
					$emp_attribute_L1 = $emp_data[$i]["his_atrib"];
					$emp_dept_L1 = $emp_data[$i]["his_dept"];
					$emp_room_L1 = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level1_date && $set_level1_date < $emp_data[$i]["histdate"]){
					$emp_class_L1 = $emp_data[$i]["his_class"];
					$emp_attribute_L1 = $emp_data[$i]["his_atrib"];
					$emp_dept_L1 = $emp_data[$i]["his_dept"];
					$emp_room_L1 = $emp_data[$i]["his_room"];
				}
			}
			if($set_level2_date != ""){
				if($i == 0 && $set_level2_date < $emp_data[$i]["histdate"]){
					$emp_class_L2 = $emp_data[$i]["his_class"];
					$emp_attribute_L2 = $emp_data[$i]["his_atrib"];
					$emp_dept_L2 = $emp_data[$i]["his_dept"];
					$emp_room_L2 = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level2_date && $set_level2_date < $emp_data[$i]["histdate"]){
					$emp_class_L2 = $emp_data[$i]["his_class"];
					$emp_attribute_L2 = $emp_data[$i]["his_atrib"];
					$emp_dept_L2 = $emp_data[$i]["his_dept"];
					$emp_room_L2 = $emp_data[$i]["his_room"];
				}
			}
			if($set_level3_date != ""){
				if($i == 0 && $set_level3_date < $emp_data[$i]["histdate"]){
					$emp_class_L3 = $emp_data[$i]["his_class"];
					$emp_attribute_L3 = $emp_data[$i]["his_atrib"];
					$emp_dept_L3 = $emp_data[$i]["his_dept"];
					$emp_room_L3 = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level3_date && $set_level3_date < $emp_data[$i]["histdate"]){
					$emp_class_L3 = $emp_data[$i]["his_class"];
					$emp_attribute_L3 = $emp_data[$i]["his_atrib"];
					$emp_dept_L3 = $emp_data[$i]["his_dept"];
					$emp_room_L3 = $emp_data[$i]["his_room"];
				}
			}
			if($set_level4_date != ""){
				if($i == 0 && $set_level4_date < $emp_data[$i]["histdate"]){
					$emp_class_L4 = $emp_data[$i]["his_class"];
					$emp_attribute_L4 = $emp_data[$i]["his_atrib"];
					$emp_dept_L4 = $emp_data[$i]["his_dept"];
					$emp_room_L4 = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level4_date && $set_level4_date < $emp_data[$i]["histdate"]){
					$emp_class_L4 = $emp_data[$i]["his_class"];
					$emp_attribute_L4 = $emp_data[$i]["his_atrib"];
					$emp_dept_L4 = $emp_data[$i]["his_dept"];
					$emp_room_L4 = $emp_data[$i]["his_room"];
				}
			}
			if($set_level5_date != ""){
				if($i == 0 && $set_level5_date < $emp_data[$i]["histdate"]){
					$emp_class_L5 = $emp_data[$i]["his_class"];
					$emp_attribute_L5 = $emp_data[$i]["his_atrib"];
					$emp_dept_L5 = $emp_data[$i]["his_dept"];
					$emp_room_L5 = $emp_data[$i]["his_room"];
				}
				if($last_his_date <= $set_level5_date && $set_level5_date < $emp_data[$i]["histdate"]){
					$emp_class_L5 = $emp_data[$i]["his_class"];
					$emp_attribute_L5 = $emp_data[$i]["his_atrib"];
					$emp_dept_L5 = $emp_data[$i]["his_dept"];
					$emp_room_L5 = $emp_data[$i]["his_room"];
				}
			}

			$last_his_date = $emp_data[$i]["histdate"];
		}
		if($set_level1_date != ""){
			if($emp_class_L1 == ""){
				$emp_class_L1 = $emp_data[0]["emp_class"];
				$emp_attribute_L1 = $emp_data[0]["emp_attribute"];
				$emp_dept_L1 = $emp_data[0]["emp_dept"];
				$emp_room_L1 = $emp_data[0]["emp_room"];
			}
		}
		if($set_level2_date != ""){
			if($emp_class_L2 == ""){
				$emp_class_L2 = $emp_data[0]["emp_class"];
				$emp_attribute_L2 = $emp_data[0]["emp_attribute"];
				$emp_dept_L2 = $emp_data[0]["emp_dept"];
				$emp_room_L2 = $emp_data[0]["emp_room"];
			}
		}
		if($set_level3_date != ""){
			if($emp_class_L3 == ""){
				$emp_class_L3 = $emp_data[0]["emp_class"];
				$emp_attribute_L3 = $emp_data[0]["emp_attribute"];
				$emp_dept_L3 = $emp_data[0]["emp_dept"];
				$emp_room_L3 = $emp_data[0]["emp_room"];
			}
		}
		if($set_level4_date != ""){
			if($emp_class_L4 == ""){
				$emp_class_L4 = $emp_data[0]["emp_class"];
				$emp_attribute_L4 = $emp_data[0]["emp_attribute"];
				$emp_dept_L4 = $emp_data[0]["emp_dept"];
				$emp_room_L4 = $emp_data[0]["emp_room"];
			}
		}
		if($set_level5_date != ""){
			if($emp_class_L5 == ""){
				$emp_class_L5 = $emp_data[0]["emp_class"];
				$emp_attribute_L5 = $emp_data[0]["emp_attribute"];
				$emp_dept_L5 = $emp_data[0]["emp_dept"];
				$emp_room_L5 = $emp_data[0]["emp_room"];
			}
		}
	}

	$emp_class_L1 != "" ? $emp_class_L1 : null;
	$emp_class_L2 != "" ? $emp_class_L2 : null;
	$emp_class_L3 != "" ? $emp_class_L3 : null;
	$emp_class_L4 != "" ? $emp_class_L4 : null;
	$emp_class_L5 != "" ? $emp_class_L5 : null;
	$emp_attribute_L1 != "" ? $emp_attribute_L1 : null;
	$emp_attribute_L2 != "" ? $emp_attribute_L2 : null;
	$emp_attribute_L3 != "" ? $emp_attribute_L3 : null;
	$emp_attribute_L4 != "" ? $emp_attribute_L4 : null;
	$emp_attribute_L5 != "" ? $emp_attribute_L5 : null;
	$emp_dept_L1 != "" ? $emp_dept_L1 : null;
	$emp_dept_L2 != "" ? $emp_dept_L2 : null;
	$emp_dept_L3 != "" ? $emp_dept_L3 : null;
	$emp_dept_L4 != "" ? $emp_dept_L4 : null;
	$emp_dept_L5 != "" ? $emp_dept_L5 : null;
	$emp_room_L1 != "" ? $emp_room_L1 : null;
	$emp_room_L2 != "" ? $emp_room_L2 : null;
	$emp_room_L3 != "" ? $emp_room_L3 : null;
	$emp_room_L4 != "" ? $emp_room_L4 : null;
	$emp_room_L5 != "" ? $emp_room_L5 : null;

	//個人プロフィール取得
	$log->debug("個人プロフィール取得開始",__FILE__,__LINE__);
	$log->debug("emp_id:".$target_emp_id,__FILE__,__LINE__);
	$data=$profile_model->select_profile_data($target_emp_id);
	$log->debug("個人プロフィール取得終了",__FILE__,__LINE__);

	$level1_total_value = $data[0]["level1_total_value"] != "" ? $data[0]["level1_total_value"] : null;
	$level2_total_value = $data[0]["level2_total_value"] != "" ? $data[0]["level2_total_value"] : null;
	$level3_total_value = $data[0]["level3_total_value"] != "" ? $data[0]["level3_total_value"] : null;
	$level4_total_value = $data[0]["level4_total_value"] != "" ? $data[0]["level4_total_value"] : null;
	$level5_total_value = $data[0]["level5_total_value"] != "" ? $data[0]["level5_total_value"] : null;
	$level1_certificate_date = $data[0]["level1_certificate_date"];
	$level2_certificate_date = $data[0]["level2_certificate_date"];
	$level3_certificate_date = $data[0]["level3_certificate_date"];
	$level4_certificate_date = $data[0]["level4_certificate_date"];
	$level5_certificate_date = $data[0]["level5_certificate_date"];
	$level1_levelup_recognize_id = $data[0]["level1_levelup_recognize_id"];
	$level2_levelup_recognize_id = $data[0]["level2_levelup_recognize_id"];
	$level3_levelup_recognize_id = $data[0]["level3_levelup_recognize_id"];
	$level4_levelup_recognize_id = $data[0]["level4_levelup_recognize_id"];
	$level5_levelup_recognize_id = $data[0]["level5_levelup_recognize_id"];
	//2012/05/14 K.Fujii ins(e)

	$param = array(
		"emp_id"			=> $target_emp_id
		,"now_level"		=> $set_now_level
		,"get_level1_date"	=> $set_level1_date
		,"get_level2_date"	=> $set_level2_date
		,"get_level3_date"	=> $set_level3_date
		,"get_level4_date"	=> $set_level4_date
		,"get_level5_date"	=> $set_level5_date
		//2012/05/14 K.Fujii ins(s)
		,"level1_total_value"	=> $level1_total_value
		,"level2_total_value"	=> $level2_total_value
		,"level3_total_value"	=> $level3_total_value
		,"level4_total_value"	=> $level4_total_value
		,"level5_total_value"	=> $level5_total_value
		,"level1_certificate_date"	=> $level1_certificate_date
		,"level2_certificate_date"	=> $level2_certificate_date
		,"level3_certificate_date"	=> $level3_certificate_date
		,"level4_certificate_date"	=> $level4_certificate_date
		,"level5_certificate_date"	=> $level5_certificate_date
		,"level1_levelup_recognize_id"	=> $level1_levelup_recognize_id
		,"level2_levelup_recognize_id"	=> $level2_levelup_recognize_id
		,"level3_levelup_recognize_id"	=> $level3_levelup_recognize_id
		,"level4_levelup_recognize_id"	=> $level4_levelup_recognize_id
		,"level5_levelup_recognize_id"	=> $level5_levelup_recognize_id
		,"get_level1_class"	=> $emp_class_L1
		,"get_level2_class"	=> $emp_class_L2
		,"get_level3_class"	=> $emp_class_L3
		,"get_level4_class"	=> $emp_class_L4
		,"get_level5_class"	=> $emp_class_L5
		,"get_level1_atrb"	=> $emp_attribute_L1
		,"get_level2_atrb"	=> $emp_attribute_L2
		,"get_level3_atrb"	=> $emp_attribute_L3
		,"get_level4_atrb"	=> $emp_attribute_L4
		,"get_level5_atrb"	=> $emp_attribute_L5
		,"get_level1_dept"	=> $emp_dept_L1
		,"get_level2_dept"	=> $emp_dept_L2
		,"get_level3_dept"	=> $emp_dept_L3
		,"get_level4_dept"	=> $emp_dept_L4
		,"get_level5_dept"	=> $emp_dept_L5
		,"get_level1_room"	=> $emp_room_L1
		,"get_level2_room"	=> $emp_room_L2
		,"get_level3_room"	=> $emp_room_L3
		,"get_level4_room"	=> $emp_room_L4
		,"get_level5_room"	=> $emp_room_L5
		//2012/05/14 K.Fujii ins(e)
	);
	$log->debug("param =".print_r($param,true),__FILE__,__LINE__);

	//2012/05/14 K.Fujii del(s)
	//$data=$profile_model->select($target_emp_id);
	//2012/05/14 K.Fujii del(s)

	if (count($data) == 0) {
		//2012/05/14 K.Fujii upd(s)
		//$res = $profile_model->insert_level($param);
		$res = $profile_model->get_level_data_insert($param);
		//2012/05/14 K.Fujii upd(e)
	} else {
		//2012/05/14 K.Fujii upd(s)
		//$res = $profile_model->update_level($param);
		$res = $profile_model->get_level_data_update($param);
		//2012/05/14 K.Fujii upd(e)
	}

	// コミット
	$mdb2->commit();
	$log->debug("コミット",__FILE__,__LINE__);
	echo("<script language='javascript'>window.opener.research();</script>");
}

/**
 * ラダーレベル取得
 */
$data_level = $progress_model->get_level_list($target_emp_id);
$emp_full_nm = $data_level['emp_lt_nm'].' '.$data_level['emp_ft_nm'];

$class_nm  = $data_level['class_nm'];
$class_nm .= ' > ';
$class_nm .= $data_level['atrb_nm'];
$class_nm .= ' > ';
$class_nm .= $data_level['dept_nm'];
if ($data_level['room_nm'] != '') {
	$class_nm .= ' > ';
	$class_nm .= $data_level['class_nm'];
}

if ($data_level['get_level1_date'] != ''){
	$level1_year = substr($data_level['get_level1_date'],0,4);
	$level1_month = substr($data_level['get_level1_date'],5,2);
	$level1_day = substr($data_level['get_level1_date'],8,2);
	$level1_date = $level1_year.'/'.$level1_month.'/'.$level1_day;
} else {
	$level1_year = '';
	$level1_month = '';
	$level1_day = '';
	$level1_date = '';
}

if ($mode == 'detail') {
	$option_level1_year  = cl_get_select_years($level1_year);
	$option_level1_month = cl_get_select_months($level1_month);
	$option_level1_day = cl_get_select_days($level1_day);
	$calendar .= write_calendar('level1', 'auth_year_level1', 'auth_month_level1', 'auth_day_level1');
}

if ($data_level['get_level2_date'] != ''){
	$level2_year = substr($data_level['get_level2_date'],0,4);
	$level2_month = substr($data_level['get_level2_date'],5,2);
	$level2_day = substr($data_level['get_level2_date'],8,2);
	$level2_date = $level2_year.'/'.$level2_month.'/'.$level2_day;
} else {
	$level2_year = '';
	$level2_month = '';
	$level2_day = '';
	$level2_date = '';
}

if ($mode == 'detail') {
	$option_level2_year  = cl_get_select_years($level2_year);
	$option_level2_month = cl_get_select_months($level2_month);
	$option_level2_day = cl_get_select_days($level2_day);
	$calendar .= write_calendar('level2', 'auth_year_level2', 'auth_month_level2', 'auth_day_level2');
}

if ($data_level['get_level3_date'] != ''){
	$level3_year = substr($data_level['get_level3_date'],0,4);
	$level3_month = substr($data_level['get_level3_date'],5,2);
	$level3_day = substr($data_level['get_level3_date'],8,2);
	$level3_date = $level3_year.'/'.$level3_month.'/'.$level3_day;
} else {
	$level3_year = '';
	$level3_month = '';
	$level3_day = '';
	$level3_date = '';
}

if ($mode == 'detail') {
	$option_level3_year  = cl_get_select_years($level3_year);
	$option_level3_month = cl_get_select_months($level3_month);
	$option_level3_day = cl_get_select_days($level3_day);
	$calendar .= write_calendar('level3', 'auth_year_level3', 'auth_month_level3', 'auth_day_level3');
}

if ($data_level['get_level4_date'] != ''){
	$level4_year = substr($data_level['get_level4_date'],0,4);
	$level4_month = substr($data_level['get_level4_date'],5,2);
	$level4_day = substr($data_level['get_level4_date'],8,2);
	$level4_date = $level4_year.'/'.$level4_month.'/'.$level4_day;
} else {
	$level4_year = '';
	$level4_month = '';
	$level4_day = '';
	$level4_date = '';
}

if ($mode == 'detail') {
	$option_level4_year  = cl_get_select_years($level4_year);
	$option_level4_month = cl_get_select_months($level4_month);
	$option_level4_day = cl_get_select_days($level4_day);
	$calendar .= write_calendar('level4', 'auth_year_level4', 'auth_month_level4', 'auth_day_level4');
}

if ($data_level['get_level5_date'] != ''){
	$level5_year = substr($data_level['get_level5_date'],0,4);
	$level5_month = substr($data_level['get_level5_date'],5,2);
	$level5_day = substr($data_level['get_level5_date'],8,2);
	$level5_date = $level5_year.'/'.$level5_month.'/'.$level5_day;
} else {
	$level5_year = '';
	$level5_month = '';
	$level5_day = '';
	$level5_date = '';
}

if ($mode == 'detail') {
	$option_level5_year  = cl_get_select_years($level5_year);
	$option_level5_month = cl_get_select_months($level5_month);
	$option_level5_day = cl_get_select_days($level5_day);
	$calendar .= write_calendar('level5', 'auth_year_level5', 'auth_month_level5', 'auth_day_level5');
}

/**
 * 研修講座一覧 院内研修取得
 */
if ($_REQUEST['year'] != ''){
	$year = $_REQUEST['year'];
} else if ($_POST['search_year'] != ''){
	$year = $_POST['search_year'];
} else if (date(n) <= 3){
	$year = date(Y) - 1;
} else {
	$year = date(Y);
}

$param = array(
	"emp_id" => $target_emp_id
	,"year" => $year
);
$data_inside = $progress_model->get_inside_training_list($param);
for ($i = 0; $i < count($data_inside); $i++){
/*
	if ($data_inside[$i]['count_index'] == ''){
		$data_inside[$i]['count'] = '';
	} else {
		$data_inside[$i]['count'] = $data_inside[$i]['count_index'].' / '.$data_inside[$i]['count_all'];
	}
*/

	if ($data_inside[$i]['inside_training_date'] != ''){
		$inside_training_year = substr($data_inside[$i]['inside_training_date'],0,4);
		$inside_training_month = substr($data_inside[$i]['inside_training_date'],5,2);
		$inside_training_day = substr($data_inside[$i]['inside_training_date'],8,2);
		$data_inside[$i]['inside_training_date'] = $inside_training_year.'/'.$inside_training_month.'/'.$inside_training_day;
	}

	if ($data_inside[$i]['plan_date'] != ''){
		$plan_year = substr($data_inside[$i]['plan_date'],0,4);
		$plan_month = substr($data_inside[$i]['plan_date'],5,2);
		$plan_day = substr($data_inside[$i]['plan_date'],8,2);
		$data_inside[$i]['plan_date'] = $plan_year.'/'.$plan_month.'/'.$plan_day;
	}

	if ($data_inside[$i]['report_date'] != ''){
		$report_year = substr($data_inside[$i]['report_date'],0,4);
		$report_month = substr($data_inside[$i]['report_date'],5,2);
		$report_day = substr($data_inside[$i]['report_date'],8,2);
		$data_inside[$i]['report_date'] = $report_year.'/'.$report_month.'/'.$report_day;
	}

	// 2012/05/24 M.Yamagawa add(s)
	$data_inside[$i]['training_name'] = str_replace('\'\'',   '&#039;', $data_inside[$i]['training_name']);
	$data_inside[$i]['training_name'] = str_replace('"',      '&quot;', $data_inside[$i]['training_name']);
	$data_inside[$i]['training_name'] = str_replace('\\\\',   '\\',     $data_inside[$i]['training_name']);
	$data_inside[$i]['training_name'] = str_replace('&lt;',   '<',      $data_inside[$i]['training_name']);
	$data_inside[$i]['training_name'] = str_replace('&gt;',   '>',      $data_inside[$i]['training_name']);
	// 2012/05/24 M.Yamagawa add(e)
}

/**
 * 研修講座一覧 院外研修取得
 */
$data_outside = $progress_model->get_outside_training_list($param);
for ($i = 0; $i < count($data_outside); $i++){

	if ($data_outside[$i]['apply_date'] != ''){
		$apply_year = substr($data_outside[$i]['apply_date'],0,4);
		$apply_month = substr($data_outside[$i]['apply_date'],4,2);
		$apply_day = substr($data_outside[$i]['apply_date'],6,2);
		$data_outside[$i]['apply_date'] = $apply_year.'/'.$apply_month.'/'.$apply_day;
	}

	if ($data_outside[$i]['apply_stat'] == 1){
		if ($data_outside[$i]['from_date'] != ''){
			$from_year = substr($data_outside[$i]['from_date'],0,4);
			$from_month = substr($data_outside[$i]['from_date'],5,2);
			$from_day = substr($data_outside[$i]['from_date'],8,2);
			$data_outside[$i]['open_date'] = $from_year.'/'.$from_month.'/'.$from_day;
		}
		if (
			$data_outside[$i]['to_date'] != ''
			&& $data_outside[$i]['from_date'] != $data_outside[$i]['to_date']
		){
			$to_year = substr($data_outside[$i]['to_date'],0,4);
			$to_month = substr($data_outside[$i]['to_date'],5,2);
			$to_day = substr($data_outside[$i]['to_date'],8,2);
			$data_outside[$i]['open_date'] .= ' - '.$to_year.'/'.$to_month.'/'.$to_day;
		}
	} else {
		$data_outside[$i]['expense'] = '';
		$data_outside[$i]['open_date'] = '';
	}

	if ($data_outside[$i]['report_date'] != ''){
		$report_year = substr($data_outside[$i]['report_date'],0,4);
		$report_month = substr($data_outside[$i]['report_date'],5,2);
		$report_day = substr($data_outside[$i]['report_date'],8,2);
		$data_outside[$i]['report_date'] = $report_year.'/'.$report_month.'/'.$report_day;
	} else {
		$report_year = '';
		$report_month = '';
		$report_day = '';
		$data_outside[$i]['report_date'] = '';
	}

	if ($mode != 'detail') {
		$calendar .= write_calendar($data_outside[$i]['seminar_apply_id'], 'report_year_'.$data_outside[$i]['seminar_apply_id'], 'report_month_'.$data_outside[$i]['seminar_apply_id'], 'report_day_'.$data_outside[$i]['seminar_apply_id']);
		$data_outside[$i]['option_report_year'] = cl_get_select_years($report_year);
		$data_outside[$i]['option_report_month'] = cl_get_select_months($report_month);
		$data_outside[$i]['option_report_day'] = cl_get_select_days($report_day);
	}
}

// DB切断
$mdb2->disconnect();
$log->debug("DB切断",__FILE__,__LINE__);

/**
 * テンプレートマッピング
 */

$smarty->assign( 'cl_title'				, $cl_title				);
$smarty->assign( 'session'				, $session				);
$smarty->assign( 'target_emp_id'		, $target_emp_id		);
$smarty->assign( 'mode'					, $mode					);
$smarty->assign( 'yui_cal_part'			, $yui_cal_part			);
$smarty->assign( 'calendar'				, $calendar				);
$smarty->assign( 'workflow_auth'		, $workflow_auth		);
$smarty->assign( 'aplyctn_mnitm_str'	, $aplyctn_mnitm_str	);
$smarty->assign( 'js_str'				, $js_str				);

$smarty->assign( 'emp_full_nm'			, $emp_full_nm			);
$smarty->assign( 'class_nm'				, $class_nm				);
$smarty->assign( 'option_level1_year'	, $option_level1_year	);
$smarty->assign( 'option_level1_month'	, $option_level1_month	);
$smarty->assign( 'option_level1_day'	, $option_level1_day	);
$smarty->assign( 'level1_date'			, $level1_date	);
$smarty->assign( 'option_level2_year'	, $option_level2_year	);
$smarty->assign( 'option_level2_month'	, $option_level2_month	);
$smarty->assign( 'option_level2_day'	, $option_level2_day	);
$smarty->assign( 'level2_date'			, $level2_date	);
$smarty->assign( 'option_level3_year'	, $option_level3_year	);
$smarty->assign( 'option_level3_month'	, $option_level3_month	);
$smarty->assign( 'option_level3_day'	, $option_level3_day	);
$smarty->assign( 'level3_date'			, $level3_date	);
$smarty->assign( 'option_level4_year'	, $option_level4_year	);
$smarty->assign( 'option_level4_month'	, $option_level4_month	);
$smarty->assign( 'option_level4_day'	, $option_level4_day	);
$smarty->assign( 'level4_date'			, $level4_date	);
$smarty->assign( 'option_level5_year'	, $option_level5_year	);
$smarty->assign( 'option_level5_month'	, $option_level5_month	);
$smarty->assign( 'option_level5_day'	, $option_level5_day	);
$smarty->assign( 'level5_date'			, $level5_date	);

$smarty->assign( 'option_search_year'	, $option_search_year	);

$smarty->assign( 'data_inside'			, $data_inside			);
$smarty->assign( 'data_outside'			, $data_outside			);

$smarty->assign( 'emp_id'				, $_REQUEST['emp_id']	);
$smarty->assign( 'year'					, $_REQUEST['year']		);
$smarty->assign( 'p_session'			, $_REQUEST['p_session']);
$smarty->assign( 'common_module'		, $_REQUEST['common_module']);

if ($mode == 'detail')
{
	// 2012/08/21 Yamagawa upd(s)
	//$smarty->assign( 'level5_edit_flg'				, $level5_edit_flg				);// レベルV認定項目の変更可否(true:変更可能、false:変更不可能)
	$smarty->assign( 'council_flg'				, $council_flg				);// 認定項目の変更可否(true:変更可能、false:変更不可能)
	// 2012/08/21 Yamagawa upd(e)
}

/*
$smarty->assign( 'training_id_search'	, $training_id_search	);
$smarty->assign( 'training_name'		, $training_name		);
$smarty->assign( 'level_options_str'	, $level_options_str	);
$smarty->assign( 'teacher_name'			, $teacher_name			);

$smarty->assign( 'option_date_y_from'	, $option_date_y_from	);
$smarty->assign( 'option_date_m_from'	, $option_date_m_from	);
$smarty->assign( 'option_date_d_from'	, $option_date_d_from	);

$smarty->assign( 'option_date_y_to'		, $option_date_y_to		);
$smarty->assign( 'option_date_m_to'		, $option_date_m_to		);
$smarty->assign( 'option_date_d_to'		, $option_date_d_to		);

$smarty->assign( 'data'					, $data					);
*/
$log->debug("テンプレートマッピング",__FILE__,__LINE__);

/**
 * テンプレート出力
 */
$log->debug("テンプレート：".basename($_SERVER['PHP_SELF'],'.php').".tpl",__FILE__,__LINE__);
if ($mode == 'detail'){
	$smarty->display("cl_training_progress_detail.tpl");
} else {
	$smarty->display(basename($_SERVER['PHP_SELF'],'.php').".tpl");
}
$log->debug("テンプレート出力",__FILE__,__LINE__);

/**
 * Javascript文字列取得
 */
function get_js_str($arr_wkfwcatemst,$workflow)
{
	$str_buff[]  = "";
	foreach ($arr_wkfwcatemst as $tmp_cate_id => $arr) {
		$str_buff[] .= "\n\t\tif (cate_id == '".$tmp_cate_id."') {\n";


		 foreach($arr["wkfw"] as $tmp_wkfw_id => $tmp_wkfw_nm){
			$tmp_wkfw_nm = htmlspecialchars($tmp_wkfw_nm, ENT_QUOTES);
			$tmp_wkfw_nm = str_replace("&#039;", "\'", $tmp_wkfw_nm);
			$tmp_wkfw_nm = str_replace("&quot;", "\"", $tmp_wkfw_nm);

			$str_buff[] .= "\t\t\taddOption(obj_wkfw, '".$tmp_wkfw_id."', '".$tmp_wkfw_nm."',  '".$workflow."');\n";

		}

		$str_buff[] .= "\t\t}\n";
	}

	return implode('', $str_buff);
}

/**
 * 年オプションHTML取得
 */
function get_select_around_years($date)
{
	// 表示開始年は1990年なので、差分の90年分を引く
	if (date(n) <= 3){
		$year  = date(Y) - 1 - 1990;
	} else {
		$year  = date(Y) - 1990;
	}

	ob_start();
	show_years_around($year, 1, $date);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 年オプションHTML取得(報告日用)
 */
function cl_get_select_years($date)
{
	ob_start();
	show_select_years(10, $date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 月オプションHTML取得
 */
function cl_get_select_months($date)
{

	ob_start();
	show_select_months($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}

/**
 * 日オプションHTML取得
 */
function cl_get_select_days($date)
{

	ob_start();
	show_select_days($date, true);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * アプリケーションメニューHTML取得
 */
function get_applycation_menuitem($session,$fname)
{
	ob_start();
	show_applycation_menuitem($session,$fname,"");
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;
}
/**
 * 必須レベルオプションHTML取得
 */
function get_select_levels($level)
{

	ob_start();
	show_levels_options($level);
	$str_buff=ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

function show_levels_options($level) {

	$arr_levels_nm = array("","I","II","III","IV","V");
	$arr_levels_id = array("","1","2","3","4","5");

	for($i=0;$i<count($arr_levels_nm);$i++) {

		echo("<option value=\"$arr_levels_id[$i]\"");
		if($level == $arr_levels_id[$i]) {
			echo(" selected");
		}
		echo(">$arr_levels_nm[$i]\n");
	}
}

/**
 * YUIカレンダー用スクリプトHTML取得
 */
function get_yui_cal_part()
{

	ob_start();
	/**
	 * YUIカレンダーに必要な外部ファイルの読み込みを行います。
	 */
	write_yui_calendar_use_file_read_0_12_2();
	$yui_cal_part=ob_get_contents();
	ob_end_clean();
	return $yui_cal_part;
}

function write_calendar($suffix, $year_control_id, $month_control_id, $date_control_id){

	ob_start();
	write_cl_calendar($suffix, $year_control_id, $month_control_id, $date_control_id);
	$str_buff = ob_get_contents();
	ob_end_clean();
	return $str_buff;

}

// カテゴリ名称を取得
function search_wkfwcatemst($con, $fname) {

	$sql = "select * from cl_wkfwcatemst";
	$cond = "where wkfwcate_del_flg = 'f' order by wkfw_type";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

// ワークフロー情報取得
function search_wkfwmst($con, $fname, $wkfw_type) {

	$sql = "select * from cl_wkfwmst";
	$cond="where wkfw_type='$wkfw_type' and wkfw_del_flg = 'f' order by wkfw_id asc";
	$sel=select_from_table($con,$sql,$cond,$fname);
	if($sel==0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	return $sel;
}

function show_years_around($before_count, $after_count, $num){

	if (date(n) <= 3){
		$now  = date(Y) - 1;
	} else {
		$now  = date(Y);
	}
	if ($num == ''){
		$num = $now;
	}

	// 当年まで
	for($i=0; $i<=$before_count;$i++){
		$yr = $now - $before_count + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

	// 翌年
	for($i=1; $i<=$after_count;$i++){
		$yr = $now + $i;
		echo("<option value=\"".$yr."\"");
		if($num == $yr){
			echo(" selected");
		}
		echo(">".$yr."</option>\n");
	}

}

$log->info(basename(__FILE__)." END");
$log->shutdown();
