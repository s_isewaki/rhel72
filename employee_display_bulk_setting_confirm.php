<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="employee_display_bulk_setting.php" method="post">
<?
foreach ($_POST as $key => $val) {
	if (is_array($val)) {
		foreach ($val as $val2) {
			echo("<input type=\"hidden\" name=\"{$key}[]\" value=\"$val2\">\n");
		}
	} else {
		echo("<input type=\"hidden\" name=\"$key\" value=\"$val\">\n");
	}
}
?>
<input type="hidden" name="back" value="t">
<input type="hidden" name="result" value="">
</form>
<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($tgt_job)) {
	echo("<script type=\"text/javascript\">alert('職種が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if (!is_array($tgt_st)) {
	echo("<script type=\"text/javascript\">alert('役職が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}
if ($action_mode == "1" && $disp_group_id == "") {
	echo("<script type=\"text/javascript\">alert('表示グループが選択されていません。');</script>");
	echo("<script type=\"text/javascript\">document.items.submit();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 「表示グループで設定」の場合
if ($action_mode == "1") {
	$sql = "select * from dispgroup";
	$cond = "where group_id = $disp_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	$disp_groups = pg_fetch_assoc($sel);
	$setvalue = array($disp_groups["default_page"], $disp_groups["font_size"], $disp_groups["top_mail_flg"], $disp_groups["top_ext_flg"], $disp_groups["top_event_flg"], $disp_groups["top_schd_flg"], $disp_groups["top_info_flg"], $disp_groups["top_task_flg"], $disp_groups["top_msg_flg"], $disp_groups["top_aprv_flg"], $disp_groups["top_schdsrch_flg"], $disp_groups["top_lib_flg"], $disp_groups["top_bbs_flg"], $disp_groups["top_memo_flg"], $disp_groups["top_link_flg"], $disp_groups["top_intra_flg"], $disp_groups["bed_info"], $disp_groups["schd_type"], $disp_groups["top_wic_flg"], $disp_groups["top_fcl_flg"], $disp_groups["top_inci_flg"], $disp_groups["top_cas_flg"], $disp_groups["top_fplus_flg"], $disp_groups["top_jnl_flg"], $disp_groups["top_free_text_flg"], $disp_groups["top_manabu_flg"], $disp_groups["ladder_flg"], $disp_groups["top_timecard_error_flg"], $disp_groups["top_workflow_flg"], $disp_groups["top_ccusr1_flg"], $disp_groups["disp_group_id"]);

// 「個別に設定」の場合
} else {
	if ($mail_flg == "") {$mail_flg = "f";}
	if ($ext_flg == "") {$ext_flg = "f";}
	if ($inci_flg == "") {$inci_flg = "f";}
	if ($fplus_flg == "") {$fplus_flg = "f";}
	if ($cas_flg == "") {$cas_flg = "f";}
	if ($event_flg == "") {$event_flg = "f";}
	if ($schd_flg == "") {$schd_flg = "f";}
	if ($info_flg == "") {$info_flg = "f";}
	if ($task_flg == "") {$task_flg = "f";}
	if ($whatsnew_flg == "t") {
		$msg_flg = "t";
		$aprv_flg = "t";
	} else {
		$msg_flg = "f";
		$aprv_flg = "f";
	}
	if ($schdsrch_flg == "") {$schdsrch_flg = "f";}
	if ($fcl_flg == "") {$fcl_flg = "f";}
	if ($lib_flg == "") {$lib_flg = "f";}
	if ($bbs_flg == "") {$bbs_flg = "f";}
	if ($memo_flg == "") {$memo_flg = "f";}
	if ($link_flg == "") {$link_flg = "f";}
	if ($intra_flg == "") {$intra_flg = "f";}
	if ($bedinfo == "") {$bedinfo = "f";}
	if ($wic_flg == "") {$wic_flg = "f";}
	if ($jnl_flg == "") {$jnl_flg = "f";}
	if ($top_free_text_flg == "") {$top_free_text_flg = "f";}
	if ($ladder_flg == "") {$ladder_flg = "f";}
	if ($manabu_flg == "") {$manabu_flg = "f";}
	if ($top_timecard_error_flg == "") {$top_timecard_error_flg = "f";}
	if ($top_workflow_flg == "") {$top_workflow_flg = "f";}
	if ($top_ccusr1_flg == "") {$top_ccusr1_flg = "f";}

	$setvalue = array($default_page, $font_size, $mail_flg, $ext_flg, $event_flg, $schd_flg, $info_flg, $task_flg, $msg_flg, $aprv_flg, $schdsrch_flg, $lib_flg, $bbs_flg, $memo_flg, $link_flg, $intra_flg, $bedinfo, $schd_type, $wic_flg, $fcl_flg, $inci_flg, $cas_flg, $fplus_flg, $jnl_flg, $top_free_text_flg,$manabu_flg, $ladder_flg, $top_timecard_error_flg, $top_workflow_flg, $top_ccusr1_flg, 0);
}

$set = array("default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_ladder_flg", "top_timecard_error_flg", "top_workflow_flg", "top_ccusr1_flg", "disp_group_id");


foreach ($tgt_job as $tmp_job) {
	foreach ($tgt_st as $tmp_st) {
		$sql = "update option set";
		$cond = "where exists (select * from empmst where empmst.emp_id = option.emp_id and empmst.emp_job = $tmp_job and empmst.emp_st = $tmp_st) and exists (select * from authmst where authmst.emp_id = option.emp_id and authmst.emp_del_flg = 'f')";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query("rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 権限一括設定画面を再表示
echo("<script type=\"text/javascript\">document.items.result.value = 't';</script>");
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
