<?
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 26, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if($st_name ==""){
	echo("<script type=\"text/javascript\">alert(\"役職名を入力してください\");</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}
if (strlen($st_name) > 60) {
	echo("<script type=\"text/javascript\">alert('役職名が長すぎます。');</script>\n");
	echo("<script type=\"text/javascript\">history.back();</script>\n");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 役職情報を更新
$sql = "update stmst set";
$set = array("st_nm", "link_key");
$setvalue = array($st_name, $link_key);
$cond = "where st_id = '$st_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を切断
pg_close($con);

// 役職一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'status_menu.php?session=$session';</script>");
?>
