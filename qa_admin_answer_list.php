<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>Q&amp;A | 質問一覧</title>
<?
require("about_session.php");
require("about_authority.php");
require("referer_common.ini");
require("menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 58, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// カテゴリ名･質問を取得
$sql = "select question.qa_category_id, qacategory.qa_category, question.question from question inner join qacategory on to_char(qacategory.qa_category_id,'FM999999999999') = question.qa_category_id";
$cond = "where question.question_id = $question_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$category_id = pg_fetch_result($sel, 0, "qa_category_id");
$category = pg_fetch_result($sel, 0, "qa_category");
$question = pg_fetch_result($sel, 0, "question");

// 遷移元の設定
if ($qa != "") {
	set_referer($con, $session, "qa", $qa, $fname);
	$referer = $qa;
} else {
	$referer = get_referer($con, $session, "qa", $fname);
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$intra_menu3 = pg_fetch_result($sel, 0, "menu3");
$intra_menu3_3 = pg_fetch_result($sel, 0, "menu3_3");
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteAnswer() {
	var checked = false;
	if (document.mainform.elements['answer_ids[]']) {
		if (document.mainform.elements['answer_ids[]'].length) {
			for (var i = 0, j = document.mainform.elements['answer_ids[]'].length; i < j; i++) {
				if (document.mainform.elements['answer_ids[]'][i].checked) {
					checked = true;
					break;
				}
			}
		} else {
			checked = document.mainform.elements['answer_ids[]'].checked;
		}
	}
	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除します。よろしいですか？')) {
		document.mainform.action = 'qa_admin_answer_delete.php';
		document.mainform.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_life.php?session=<? echo($session); ?>"><b><? echo($intra_menu3); ?></b></a> &gt; <a href="qa_category_menu.php?session=<? echo($session); ?>"><b><? echo($intra_menu3_3); ?></b></a> &gt; <a href="qa_admin_category_list.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="qa_category_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="qa_category_menu.php?session=<? echo($session); ?>"><img src="img/icon/b11.gif" width="32" height="32" border="0" alt="Q&amp;A"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="qa_category_menu.php?session=<? echo($session); ?>"><b>Q&amp;A</b></a> &gt; <a href="qa_admin_category_list.php?session=<? echo($session); ?>"><b>管理画面</b></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="qa_category_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="100" align="center" bgcolor="#bdd1e7"><a href="qa_admin_category_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#bdd1e7"><a href="qa_admin_question_list.php?session=<? echo($session); ?>&category_id=<? echo($category_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">質問一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="100" align="center" bgcolor="#5279a5"><a href="qa_admin_answer_list.php?session=<? echo($session); ?>&question_id=<? echo($question_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>回答一覧</b></font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteAnswer();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($category); ?> &gt; <? echo($question); ?></b></font></td>
</tr>
</table>
<form name="mainform" method="post">
<? show_answers($con, $question_id, $session, $fname); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="question_id" value="<? echo($question_id); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_answers($con, $question_id, $session, $fname) {
	if ($question_id == "") {
		return;
	}

	// 回答一覧を取得
	$sql = "select answer.answer_id, answer.answer, empmst.emp_lt_nm, empmst.emp_ft_nm, answer.date from answer inner join empmst on empmst.emp_id = answer.emp_id";
	$cond = "where answer.question_id = $question_id and answer.a_del_flg = 'f' order by answer.answer_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	if (pg_num_rows($sel) == 0) {
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
		echo("<tr height=\"22\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">回答は登録されていません。</font></td>\n");
		echo("</tr>\n");
		echo("</table>\n");
		return;
	}

	echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");

	// ヘッダ行を出力
	echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"40\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
	echo("<td width=\"360\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">回答</font></td>\n");
	echo("<td width=\"120\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録者</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録日時</font></td>\n");
	echo("</tr>\n");

	// 明細行を出力
	while ($row = pg_fetch_array($sel)) {
		echo("<tr height=\"22\">\n");
		echo("<td align=\"center\"><input type=\"checkbox\" name=\"answer_ids[]\" value=\"{$row["answer_id"]}\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"qa_admin_answer_update.php?session=$session&answer_id={$row["answer_id"]}\">{$row["answer"]}</a></font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$row["emp_lt_nm"]} {$row["emp_ft_nm"]}</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5", $row["date"]) . "</font></td>\n");
		echo("</tr>\n");
	}

	echo("</table>\n");
}
?>
