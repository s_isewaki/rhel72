<?php
define("DEBUG", false);

require_once("class/Cmx/Model/SystemConfig.php");

ob_start();
require_once("about_postgres.php");
require_once("about_authority.php");
ob_end_clean();

// output=xmlならXML出力
if ($_POST["output"] == 'xml') {
    require_once("mypage_for_launcher_xml.php");
    exit;
}

header("Content-Type: text/html; charset=EUC-JP");

if (!isset($_POST["id"]) || (!isset($_POST["passwd"]) && !isset($_POST["systemid"]))) {
    show_form();
    exit;
}

$id = $_POST["id"];
$pass = $_POST["passwd"];
$systemid = $_POST["systemid"];

$fname = $PHP_SELF;
$con = connect2db($fname);

if ($systemid != "") {
    $sql = "select prf_org_cd from profile";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $org_cd = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "prf_org_cd") : "";
    if (strcmp($systemid, md5($org_cd)) == 0) {
        $sql = "select l.emp_login_pass from login l";
        $cond = "where l.emp_login_id = '$id' and exists (select * from authmst a where a.emp_id = l.emp_id and (not a.emp_del_flg))";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $pass = (pg_num_rows($sel) == 1) ? pg_fetch_result($sel, 0, "emp_login_pass") : "";
    }
    else {
        pg_close($con);
        exit;
    }
}

$result = create_session($con, $id, $pass, $fname);
if (!$result) {
    pg_close($con);
    exit;
}
$emp_id = $result[0];
$session = $result[1];

$authorities = get_authority($session, $fname);
$font_size = "1";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix</title>
<script type="text/javascript">
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
table.block td table.block td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<?
// ウェブメール
if ($_POST["msg_mail"] == "t" && $authorities[0] == "t") {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%">
<?
    $webmail_for_launcher = true;
    require_once("menu_webmail.ini");
?>
</td>
</tr>
</table>
<?
}

// ファントルくん
if ($_POST["msg_fantol"] == "t" && $authorities[47] == "t") {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<?
    require_once("hiyari_parts.ini");
    show_incident_infomation($session, $fname, $con);
?>
</td>
</table>
<?
}

// お知らせ・回覧板
if ($_POST["msg_info"] == "t" && $authorities[46] == "t") {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<form name="menu_news" action="menu_news_display.php" method="post">
<?
    require_once("show_class_name.ini");
    $arr_class_name = get_class_name_array($con, $fname);

    require_once("menu_news.ini");
    show_menu_news($con, $authorities, $font_size, $arr_class_name, $session, $fname, $emp_id, "");
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
</tr>
</table>
<?
}

// 最新情報
$memo_disp = ($_POST["msg_memo"] == "t" && $authorities[4] == "t");
$app_disp = ($_POST["msg_app"] == "t" && $authorities[7] == "t");
if ($memo_disp || $app_disp) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#bdd1e7">
<td width="19" height="22" class="spacing"></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>最新情報</b></font></td>
</tr>
<tr>
<td colspan="2" bgcolor="#5279a5"><img src="./img/spacer.gif" width="1" height="1"></td>
</tr>
<?
    if ($memo_disp) {
        require_once("menu_message.ini");
        show_menu_message($con, $emp_id, $font_size, $session, $fname);
    }

    if ($app_disp) {
?>
<tr>
<td colspan="2">
<?
        if ($memo_disp) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?
        }

        require_once("show_apply_approve_notice.ini");
        show_apply_approve_notice($con, $emp_id, $font_size, $session, $fname);
    }
?>
</table>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<?
}
?>
</td>
</tr>
</table>
</body>
</html>
<?
function show_form() {
?>
<body>
<form method="POST" action="mypage_for_launcher.php">
<dl>
<dt>ID</dt>
<dd><input type="text" name="id" value=""></dd>
<dt>パスワード</dt>
<dd><input type="password" name="passwd" value=""></dd>
<dt>システムID</dt>
<dd><input type="password" name="systemid" value=""></dd>
<dt>WEBメール</dt>
<dd><input type="checkbox" name="msg_mail" value="t"></dd>
<dt>お知らせ</dt>
<dd><input type="checkbox" name="msg_info" value="t"></dd>
<dt>ファントルくん</dt>
<dd><input type="checkbox" name="msg_fantol" value="t"></dd>
<dt>承認待ち（未承認）</dt>
<dd><input type="checkbox" name="msg_app" value="t"></dd>
<dt>伝言メモ</dt>
<dd><input type="checkbox" name="msg_memo" value="t"></dd>
</dl>
<p><input type="submit" value="送信"></p>
</form>
</body>
<?
}

function create_session($con, $id, $passwd, $fname) {
    $sql = "select login.emp_id from login";
    $cond = "where login.emp_login_id = '$id' and login.emp_login_pass = '$passwd' and exists (select * from authmst where authmst.emp_id = login.emp_id and authmst.emp_del_flg = 'f')";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
        return false;
    }
    if (pg_num_rows($sel) != 1) {
        if (DEBUG) write_log("employee count is " . pg_num_rows($sel));
        return false;
    }
    $emp_id = pg_fetch_result($sel, 0, "emp_id");

    my_purge_session($con, $emp_id, $fname);
    $session_id = my_register_session($con, $emp_id, $fname);
    if ($session_id == "") {
        if (DEBUG) write_log("generate failed");
        return false;
    }

    return array($emp_id, $session_id);
}

function my_register_session($con, $emp_id, $fname) {
    $hash_id = my_create_hash_id();
    if ($hash_id == "") {
        return "";
    }

    $sql = "insert into session (session_id, emp_id, session_made) values (";
    $content = array($hash_id, $emp_id, time());
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
        return "";
    }

    return $hash_id;
}

function my_purge_session($con, $emp_id, $fname) {
    $conf = new Cmx_SystemConfig();
    $purge_on_login = $conf->get('session.purge_on_login');
    if (!$purge_on_login) {
        return;
    }

    $sql = "delete from session";
    $cond = "where emp_id = '$emp_id' and session_made <= '" . strtotime("-3 days") . "'";
    delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        if (DEBUG) write_log("sql failed, " . pg_last_error($con));
    }
}

function my_create_hash_id() {
    $hash_id = md5(uniqid(rand(), 1));
    if ($hash_id == "") {
        if (DEBUG) write_log("hash failed");
        return "";
    }
    return $hash_id;
}

function write_log($log) {
    $fp = fopen("myage_for_launcher.log", "a");
    if (!$fp) return;
    fwrite($fp, "[". date("Y/m/d H:i:s") . "]" . $log . "\r\n");
    fclose($fp);
}
