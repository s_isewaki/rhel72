<?php
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 24, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 入力チェック
if ($cate1 == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ１が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (strlen($cate1) > 40) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ１が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if ($cate2 == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ２が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (strlen($cate2) > 40) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ２が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if ($cate3 == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ３が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (strlen($cate3) > 40) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ３が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if ($cate4 == "") {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ４が入力されていません。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

if (strlen($cate4) > 40) {
    pg_close($con);
    echo("<script type=\"text/javascript\">alert('カテゴリ４が長すぎます。');</script>");
    echo("<script type=\"text/javascript\">history.back();</script>");
    exit;
}

// データベースに接続
$con = connect2db($fname);

// カテゴリ名および非表示フラグの更新
for ($i = 1; $i <= 4; $i++) {
    $sql = "update newscate set";
    $set = array("newscate_name", "no_disp_flg");
    $varname = "cate".$i;
    $no_disp_flg = "no_disp_flg".$i;
    $$no_disp_flg = ($$no_disp_flg == 't') ? 't' : 'f';
    $setvalue = array($$varname, $$no_disp_flg);
    if ($i <= 3) {
        $tmp_id = $i;
    } else {
        $tmp_id = 10000;
    }

    $cond = "where newscate_id = $tmp_id";
    $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
    if ($upd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// データベース接続を閉じる
pg_close($con);

// カテゴリ一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'news_cate_list.php?session=$session&page=$page';</script>");
