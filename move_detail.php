<html>
<head>
<title>病床管理 | 移動詳細</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<?
//ページ名
$fname = $PHP_SELF;
//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//病棟登録権限チェック
$wardreg = check_authority($session,14,$fname);
if($wardreg == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//DBへのコネクション作成
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5">
<center>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#5279a5">
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>移動詳細</b></font></td>
<td width="80" align="center"><a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a></td>
</tr>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff" height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">救護区分・移動手段： <? echo(get_move_list($con, $pt_id, $fname)); ?></font></td>
</tr>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
//------------------------------------------------------------------------------
// 関数
//------------------------------------------------------------------------------

function get_move_list($con, $pt_id, $fname) {

require("./conf/sql.inf");

	// 移動情報を格納する配列の初期化
	$arr_move = array();

	// 患者詳細情報の取得
	$cond = "where ptif_id = '$pt_id'";
	$sel = select_from_table($con, $SQL86, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}

	// 各種移動手段について、選択されていたら配列に保存
	//// 杖歩行
	$move1 = pg_result($sel, 0, "ptsubif_move1");
	if ($move1 == "t") {
		array_push($arr_move, "杖歩行");
	}
	//// 介助歩行
	$move2 = pg_result($sel, 0, "ptsubif_move2");
	if ($move2 == "t") {
		array_push($arr_move, "介助歩行");
	}
	//// 車椅子（自己駆動）
	$move3 = pg_result($sel, 0, "ptsubif_move3");
	if ($move3 == "t") {
		array_push($arr_move, "車椅子（自己駆動）");
	}
	//// 車椅子（介助）
	$move4 = pg_result($sel, 0, "ptsubif_move4");
	if ($move4 == "t") {
		array_push($arr_move, "車椅子（介助）");
	}
	//// 担送
	$move5 = pg_result($sel, 0, "ptsubif_move5");
	if ($move5 == "t") {
		array_push($arr_move, "担送");
	}
	//// 酸素必要
	$move6 = pg_result($sel, 0, "ptsubif_move6");
	if ($move6 == "t") {
		array_push($arr_move, "酸素必要");
	}

	// 配列をカンマで結合
	$move_list = join(", ", $arr_move);

	return $move_list;

}
?>
