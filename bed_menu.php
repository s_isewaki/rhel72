<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 病床管理 | お知らせ</title>
<?
require("about_authority.php");
require("about_session.php");
require("show_calc_bed_by_type.ini");
require("show_select_values.ini");
require("get_ward_div.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 病床管理権限チェック
$auth = check_authority($session,14,$fname);
if($auth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 病棟登録権限を取得
$ward_admin_auth = check_authority($session, 21, $fname);

// DBコネクションの作成
$con = connect2db($fname);

// 病床利用率の取得
$arr_bed_rate = get_arr_bed_rate($con, $fname);

// 病棟区分名を取得
$arr_ward_div = get_ward_div($con, $fname);

if ($hide_avg == "") {

	// 表示設定を取得
	$sql = "select hide_avg from beddisp";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$hide_avg = pg_fetch_result($sel, 0, "hide_avg");
}

// 平均在院日数を表示する設定の場合
if ($hide_avg == "f") {
	$prev_ym = prev_ym(date("Ym"));
	$from_year = substr($prev_ym, 0, 4);
	$from_month = substr($prev_ym, 4, 2);
	$to_year = $from_year;
	$to_month = $from_month;

	$arr_avg_in_days["ALL"]["sum_days"] = 0;
	$arr_avg_in_days["ALL"]["in_cnt"] = 0;
	$arr_avg_in_days["ALL"]["out_cnt"] = 0;

	for ($ward_type = 1; $ward_type <= 5; $ward_type++) {
		$avg_status[$ward_type] = calc_avg_status($con, $from_year, $from_month, $to_year, $to_month, $ward_type, $fname);

		$months = array_keys($avg_status[$ward_type]);
		$arr_avg_in_days[$ward_type]["avg_days"] = $avg_status[$ward_type][$months[2]]["avg3"];

		$arr_avg_in_days[$ward_type]["sum_days"] = 0;
		foreach ($months as $tmp_month) {
			$arr_avg_in_days[$ward_type]["sum_days"] += $avg_status[$ward_type][$tmp_month]["total"];
			$arr_avg_in_days["ALL"]["sum_days"] += $avg_status[$ward_type][$tmp_month]["total"];

			$arr_avg_in_days[$ward_type]["in_cnt"] += $avg_status[$ward_type][$tmp_month]["new_in_ward"];
			$arr_avg_in_days["ALL"]["in_cnt"] += $avg_status[$ward_type][$tmp_month]["new_in_ward"];

			$arr_avg_in_days[$ward_type]["out_cnt"] += $avg_status[$ward_type][$tmp_month]["new_out_ward"];
			$arr_avg_in_days["ALL"]["out_cnt"] += $avg_status[$ward_type][$tmp_month]["new_out_ward"];
		}
	}

	$arr_avg_in_days["ALL"]["avg_days"] = calc_avg_in_days($arr_avg_in_days["ALL"]["sum_days"], $arr_avg_in_days["ALL"]["in_cnt"], $arr_avg_in_days["ALL"]["out_cnt"]);
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
form {margin:0;}
table.submenu {border-collapse:collapse;}
table.submenu td {border:#5279a5 solid 1px;}
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
strong {font-size:1.6em; font-weight:normal;  font-family:'Times New Roman';}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="bed_menu.php?session=<? echo($session); ?>"><img src="img/icon/b17.gif" width="32" height="32" border="0" alt="病床管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="bed_menu.php?session=<? echo($session); ?>"><b>病床管理</b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? } else if ($ward_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="building_list.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="115" align="center" bgcolor="#5279a5"><a href="bed_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>病床管理トップ</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="ward_reference.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病棟照会</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="inpatient_reserve_register_list.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">入退院登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="bedmake_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ベットメイク</font></a></td>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="bed_rate_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">病床利用率管理</font></a>
</td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="bed_date_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院日数管理</font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="bed_patient_export.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">患者データ抽出</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!-- メニュー -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="submenu">
<tr height="22">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ</font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_status.php?session=<? echo($session); ?>">病棟状況一覧表</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_schedule.php?session=<? echo($session); ?>">入退院カレンダー</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_inpatient_search.php?session=<? echo($session); ?>">入院患者検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_vacant_search.php?session=<? echo($session); ?>">空床検索</a></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="bed_menu_occupation.php?session=<? echo($session); ?>">病床利用状況</a></font></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 現在の入院患者数 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#bdd1e7">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>現在の入院患者数</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block" style="position:relative;top:-1px;">
<tr bgcolor="#f6f9ff">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<tr>
<td align="center"><strong><? echo($arr_bed_rate["1"]["pt_cnt"]); ?>名</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["2"]["pt_cnt"]); ?>名</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["3"]["pt_cnt"]); ?>名</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["4"]["pt_cnt"]); ?>名</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["5"]["pt_cnt"]); ?>名</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["ALL"]["pt_cnt"]); ?>名</strong></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 病床利用率 -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#bdd1e7">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>病床利用率</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block" style="position:relative;top:-1px;">
<tr bgcolor="#f6f9ff">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<tr>
<td align="center"><strong><? echo($arr_bed_rate["1"]["rate"]); ?>%</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["2"]["rate"]); ?>%</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["3"]["rate"]); ?>%</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["4"]["rate"]); ?>%</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["5"]["rate"]); ?>%</strong></td>
<td align="center"><strong><? echo($arr_bed_rate["ALL"]["rate"]); ?>%</strong></td>
</tr>
</table>

<? if ($hide_avg == "f") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 平均在院日数（直近3か月） -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#bdd1e7">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>平均在院日数（直近3か月）</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block" style="position:relative;top:-1px;">
<tr bgcolor="#f6f9ff">
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="17%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<tr>
<td align="center"><strong><? echo($arr_avg_in_days["1"]["avg_days"]); ?>日</strong></td>
<td align="center"><strong><? echo($arr_avg_in_days["2"]["avg_days"]); ?>日</strong></td>
<td align="center"><strong><? echo($arr_avg_in_days["3"]["avg_days"]); ?>日</strong></td>
<td align="center"><strong><? echo($arr_avg_in_days["4"]["avg_days"]); ?>日</strong></td>
<td align="center"><strong><? echo($arr_avg_in_days["5"]["avg_days"]); ?>日</strong></td>
<td align="center"><strong><? echo($arr_avg_in_days["ALL"]["avg_days"]); ?>日</strong></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<!-- 直近3か月間のデータ -->
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block">
<tr bgcolor="#bdd1e7">
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>直近3か月間のデータ</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="block" style="position:relative;top:-1px;">
<tr bgcolor="#f6f9ff">
<td>&nbsp;</td>
<td width="14%" height="22" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["1"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["2"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["3"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["4"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_ward_div["5"]); ?></font></td>
<td width="14%" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">全体</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">在院患者延日数</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["1"]["sum_days"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["2"]["sum_days"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["3"]["sum_days"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["4"]["sum_days"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["5"]["sum_days"]); ?>日</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["ALL"]["sum_days"]); ?>日</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新入棟患者数</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["1"]["in_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["2"]["in_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["3"]["in_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["4"]["in_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["5"]["in_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["ALL"]["in_cnt"]); ?>名</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新退棟患者数</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["1"]["out_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["2"]["out_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["3"]["out_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["4"]["out_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["5"]["out_cnt"]); ?>名</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_avg_in_days["ALL"]["out_cnt"]); ?>名</font></td>
</tr>
</table>
<? } else { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22">
<td align="right"><input type="button" value="平均在院日数表示" onclick="location.href = 'bed_menu.php?session=<? echo($session); ?>&amp;hide_avg=f'"></td>
</tr>
</table>
<? } ?>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function calc_avg_status($con, $from_year, $from_month, $to_year, $to_month, $ward_type, $fname) {

	// 期間のFromが今月以降であれば、計算しない
	$this_year = date("Y");
	$this_month = date("m");
	if ($from_year > $this_year || ($from_year == $this_year && $from_month >= $this_month)) {
		return false;
	}

	// 期間のToが今月以降であれば、前月までとみなす
	if ($to_year > $this_year || ($to_year == $this_year && $to_month >= $this_month)) {
		$prev_ym = prev_ym(date("Ym"));
		$to_year = substr($prev_ym, 0, 4);
		$to_month = substr($prev_ym, 4, 2);
	}

	// 処理期間の設定（平均在院日数計算のため、From年月2ヶ月前からとする）
	$tmp_from_year = $from_year;
	$tmp_from_month = $from_month;
	for ($i = 1; $i <= 2; $i++) {
		$tmp_from_month--;
		if ($tmp_from_month < 1) {
			$tmp_from_year--;
			$tmp_from_month = 12;
		}
	}
	$from_date = sprintf("%04d%02d%02d", $tmp_from_year, $tmp_from_month, 1);
	$to_date = sprintf("%04d%02d%02d", $to_year, $to_month, days_in_month($to_year, $to_month));

	// 処理期間内の稼働状況配列（月別）を初期化
	$status = array();
	$tmp_year = intval(substr($from_date, 0, 4));
	$tmp_month = intval(substr($from_date, 4, 2));
	$to_year = intval(substr($to_date, 0, 4));
	$to_month = intval(substr($to_date, 4, 2));
	$from_ym = sprintf("%04d%02d", $from_year, $from_month);
	while (true) {
		$tmp_ym = sprintf("%04d%02d", $tmp_year, $tmp_month);

		$status[$tmp_ym] = array(
			"title"         => $tmp_month,
			"display"       => ($tmp_ym >= $from_ym),
			"inpatient"     => 0,
			"outpatient"    => 0,
			"new_in_ward"   => 0,
			"re_in_ward"    => 0,
			"new_out_ward"  => 0,
			"re_out_ward"   => 0,
			"actual_number" => 0,
			"same_day"      => 0,
			"except"        => 0
		);

		if ($tmp_year == $to_year && $tmp_month == $to_month) {
			break;
		}

		$tmp_month++;
		if ($tmp_month > 12) {
			$tmp_year++;
			$tmp_month = 1;
		}
	}

	// 指定された病棟区分に属する病室一覧を取得
	$sql = "select ptrmmst.bldg_cd, ptrmmst.ward_cd, ptrmmst.ptrm_room_no from ptrmmst inner join wdmst on wdmst.bldg_cd = ptrmmst.bldg_cd and wdmst.ward_cd = ptrmmst.ward_cd";
	$cond = "where wdmst.ward_del_flg = 'f' and ptrmmst.ptrm_del_flg = 'f' and ((wdmst.ward_type = '$ward_type' and ptrmmst.ward_type = '0') or (ptrmmst.ward_type = '$ward_type'))";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ptrm_ids = array();
	while ($row = pg_fetch_array($sel)) {
		$ptrm_ids[] = "{$row["bldg_cd"]}-{$row["ward_cd"]}-{$row["ptrm_room_no"]}";
	}

	// 除外対象部屋一覧を取得
	$except_rooms = get_except_rooms($con, $fname);

	// 期間内在院患者を取得
	$sel = get_patient_list($con, $from_date, $to_date, false, $fname);

	// 同日退院カウント用の配列を初期化
	$same_day_counter = array();

	// 患者情報をループ
	while ($row = pg_fetch_array($sel)) {
		$tmp_ptif_id = $row["ptif_id"];
		$tmp_pt_type = $row["pt_type"];
		$tmp_in_dt = $row["inpt_in_dt"];
		$tmp_in_tm = $row["inpt_in_tm"];
		$tmp_in_dttm = "$tmp_in_dt$tmp_in_tm";
		$tmp_out_dt = $row["inpt_out_dt"];
		$tmp_out_tm = $row["inpt_out_tm"];
		$tmp_bldg_cd = $row["bldg_cd"];
		$tmp_ward_cd = $row["ward_cd"];
		$tmp_ptrm_room_no = $row["ptrm_room_no"];
		$tmp_except_flg = $row["inpt_except_flg"];
		$tmp_except_terms = array();
		for ($i = 1; $i <= 5; $i++) {
			$tmp_except_terms[$i] = array("from" => $row["inpt_except_from_date$i"],
			                              "to"   => $row["inpt_except_to_date$i"]);
		}

		// 在院患者の場合、退院日を「9999/12/31」に仮設定
		if ($tmp_pt_type == "in") {
			$tmp_out_dt = "99991231";
			$tmp_out_tm = "2330";
		}
		$tmp_out_dttm = "$tmp_out_dt$tmp_out_tm";

		// 入院期間内の移転確定情報を取得
		$sql_move = "select * from inptmove";
		$cond_move = "where ptif_id = '$tmp_ptif_id' and move_cfm_flg and (not move_del_flg) and ((move_dt = '$tmp_in_dt' and move_tm > '$tmp_in_tm') or (move_dt > '$tmp_in_dt')) and ((move_dt = '$tmp_out_dt' and move_tm < '$tmp_out_tm') or (move_dt < '$tmp_out_dt')) order by move_dt, move_tm";
		$sel_move = select_from_table($con, $sql_move, $cond_move, $fname);
		if ($sel_move == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$movings = (pg_num_rows($sel_move) > 0) ? pg_fetch_all($sel_move) : array();

		// 入院日時時点の病室を求める
		$tmp_current_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";
		$tmp_in_ptrm = get_initial_room($movings, $tmp_current_ptrm);

		// 入院日が期間内の場合
		if ($tmp_in_dt >= $from_date && $tmp_in_dt <= $to_date) {

			// 入院日時時点の病室がカウント対象であれば
			if (is_countable_ptrm($tmp_in_ptrm, $ptrm_ids, $except_rooms)) {

				// 除外患者でなければ「入院」を増やす
				if (!is_except_patient($tmp_except_flg, $tmp_in_dt, $tmp_except_terms)) {
					$status[substr($tmp_in_dt, 0, 6)]["inpatient"]++;

					// 同日退院カウント用データをセット
					if (!isset($same_day_counter[$tmp_ptif_id]["in"])) {
						$same_day_counter[$tmp_ptif_id]["in"] = array();
					}
					if (!in_array($tmp_in_dt, $same_day_counter[$tmp_ptif_id]["in"])) {
						$same_day_counter[$tmp_ptif_id]["in"][] = $tmp_in_dt;
					}
				}
			}
		}

		// 退院日が期間内の場合
		if ($tmp_out_dt >= $from_date && $tmp_out_dt <= $to_date) {

			// 退院日時時点の病室を求める
			$tmp_out_ptrm = "{$tmp_bldg_cd}-{$tmp_ward_cd}-{$tmp_ptrm_room_no}";

			// カウント対象の病室であれば
			if (is_countable_ptrm($tmp_out_ptrm, $ptrm_ids, $except_rooms)) {

				// 除外患者でなければ「退院」を増やす
				if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
					$status[substr($tmp_out_dt, 0, 6)]["outpatient"]++;

					// 同日退院カウント用データをセット
					if (!isset($same_day_counter[$tmp_ptif_id]["out"])) {
						$same_day_counter[$tmp_ptif_id]["out"] = array();
					}
					if (!in_array($tmp_out_dt, $same_day_counter[$tmp_ptif_id]["out"])) {
						$same_day_counter[$tmp_ptif_id]["out"][] = $tmp_out_dt;
					}
				}
			}
		}

		// 期間の年月を変数に保存
		$to_ym = substr($to_date, 0, 6);

		// 転棟情報をループ
		$tmp_new_in_ward = true;
		$tmp_new_out_ward = true;
		foreach ($movings as $tmp_moving) {
			$tmp_move_dt = $tmp_moving["move_dt"];
			$tmp_move_ym = substr($tmp_move_dt, 0, 6);
			$tmp_from_bldg_cd = $tmp_moving["from_bldg_cd"];
			$tmp_from_ward_cd = $tmp_moving["from_ward_cd"];
			$tmp_from_ptrm_room_no = $tmp_moving["from_ptrm_room_no"];
			$tmp_to_bldg_cd = $tmp_moving["to_bldg_cd"];
			$tmp_to_ward_cd = $tmp_moving["to_ward_cd"];
			$tmp_to_ptrm_room_no = $tmp_moving["to_ptrm_room_no"];

			// 年月が未来であればカウント処理に無関係になるのでループを抜ける
			if ($tmp_move_ym > $to_ym) {
				break;
			}

			$tmp_from_ptrm = "{$tmp_from_bldg_cd}-{$tmp_from_ward_cd}-{$tmp_from_ptrm_room_no}";
			$tmp_to_ptrm = "{$tmp_to_bldg_cd}-{$tmp_to_ward_cd}-{$tmp_to_ptrm_room_no}";

			// 入棟カウント対象の場合
			if (must_count_as_in_ward($tmp_from_ptrm, $tmp_to_ptrm, $ptrm_ids, $except_rooms)) {

				// 期間内であれば、除外患者を考慮しつつカウント処理
				if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
					if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
						$tmp_key = ($tmp_new_in_ward) ? "new_in_ward" : "re_in_ward";
						$status[substr($tmp_move_dt, 0, 6)][$tmp_key]++;
					}
				}

				// 次以降を再入棟とする
				if ($tmp_new_in_ward) {
					$tmp_new_in_ward = false;
				}
			}

			// 退棟カウント対象の場合
			if (must_count_as_out_ward($tmp_from_ptrm, $tmp_to_ptrm, $ptrm_ids, $except_rooms)) {

				// 期間内であれば、除外患者を考慮しつつカウント処理
				if ($tmp_move_dt >= $from_date && $tmp_move_dt <= $to_date) {
					if (!is_except_patient($tmp_except_flg, $tmp_out_dt, $tmp_except_terms)) {
						$tmp_key = ($tmp_new_out_ward) ? "new_out_ward" : "re_out_ward";
						$status[substr($tmp_move_dt, 0, 6)][$tmp_key]++;
					}
				}

				// 次以降を再退棟とする
				if ($tmp_new_out_ward) {
					$tmp_new_out_ward = false;
				}
			}
		}

		// 指定期間内の日付をループ
		$tmp_date = $from_date;
		while (true) {

			// 翌日0時を処理対象時刻とする
			$tmp_year = substr($tmp_date, 0, 4);
			$tmp_month = substr($tmp_date, 4, 2);
			$tmp_day = substr($tmp_date, 6, 2);
			$tmp_tomorrow = date("Ymd", strtotime("+1 day", mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year)));
			$tmp_dttm = $tmp_tomorrow . "0000";

			// 入院前の場合、日付を進める
			if ($tmp_dttm < $tmp_in_dttm) {
				if ($tmp_tomorrow > $to_date) {
					break;
				}
				$tmp_date = $tmp_tomorrow;
				continue;
			}

			// 退院後の場合、ループを抜ける
			if ($tmp_dttm >= $tmp_out_dttm) {
				break;
			}

			// 0時時点の在院病室を取得
			$tmp_ptrm = get_room_by_datetime($tmp_dttm, $tmp_in_dttm, $tmp_out_dttm, $movings, $tmp_current_ptrm);

			// 除外病室の場合、日付を進める
			if (!is_countable_ptrm($tmp_ptrm, $ptrm_ids, $except_rooms)) {
				if ($tmp_tomorrow > $to_date) {
					break;
				}
				$tmp_date = $tmp_tomorrow;
				continue;
			}

			// 除外患者を考慮しつつカウント処理（前日の24時とみなす）
			if (is_except_patient($tmp_except_flg, $tmp_date, $tmp_except_terms)) {
				$status[$tmp_year . $tmp_month]["except"]++;
			} else {
				$status[$tmp_year . $tmp_month]["actual_number"]++;
			}

			// 日付を進める
			if ($tmp_tomorrow > $to_date) {
				break;
			}
			$tmp_date = $tmp_tomorrow;
		}
	}

	// 同日退院カウント用配列（患者単位）をループ
	foreach ($same_day_counter as $tmp_inout) {

		// 入院→退院または退院→入院のあった日をカウント
		$tmp_dates = array_intersect($tmp_inout["in"], $tmp_inout["out"]);
		foreach ($tmp_dates as $tmp_date) {
			$status[substr($tmp_date, 0, 6)]["same_day"]++;
		}
	}

	// 延べ数・平均在院日数を計算
	foreach ($status as $tmp_ym => $tmp_status) {

		// 延べ数
		$tmp_total_of_this_month = $tmp_status["actual_number"] + $tmp_status["new_out_ward"] + $tmp_status["same_day"];
		$status[$tmp_ym]["total"] = $tmp_total_of_this_month;

		$tmp_inpatient_of_this_month = $tmp_status["inpatient"];
		$tmp_outpatient_of_this_month = $tmp_status["outpatient"];
		$tmp_new_in_ward_of_this_month = $tmp_status["new_in_ward"];
		$tmp_new_out_ward_of_this_month = $tmp_status["new_out_ward"];

		// 表示期間外なら計算しない
		if ($tmp_status["display"]) {

			// 平均在院日数（当月のみ）
			$tmp_total = $tmp_total_of_this_month;
			$tmp_in_count = ($tmp_inpatient_of_this_month + $tmp_new_in_ward_of_this_month);
			$tmp_out_count = ($tmp_outpatient_of_this_month + $tmp_new_out_ward_of_this_month);
			$status[$tmp_ym]["avg1"] = calc_avg_in_days($tmp_total, $tmp_in_count, $tmp_out_count);

			// 平均在院日数（直近2ヶ月）
			$tmp_total += $tmp_total_of_last_month;
			$tmp_in_count += ($tmp_inpatient_of_last_month + $tmp_new_in_ward_of_last_month);
			$tmp_out_count += ($tmp_outpatient_of_last_month + $tmp_new_out_ward_of_last_month);
			$status[$tmp_ym]["avg2"] = calc_avg_in_days($tmp_total, $tmp_in_count, $tmp_out_count);

			// 平均在院日数（直近3ヶ月）
			$tmp_total += $tmp_total_of_month_before_last;
			$tmp_in_count += ($tmp_inpatient_of_month_before_last + $tmp_new_in_ward_of_month_before_last);
			$tmp_out_count += ($tmp_outpatient_of_month_before_last + $tmp_new_out_ward_of_month_before_last);
			$status[$tmp_ym]["avg3"] = calc_avg_in_days($tmp_total, $tmp_in_count, $tmp_out_count);
		}

		// 先月、先々月のデータを退避
		$tmp_total_of_month_before_last = $tmp_total_of_last_month;
		$tmp_inpatient_of_month_before_last = $tmp_inpatient_of_last_month;
		$tmp_outpatient_of_month_before_last = $tmp_outpatient_of_last_month;
		$tmp_new_in_ward_of_month_before_last = $tmp_new_in_ward_of_last_month;
		$tmp_new_out_ward_of_month_before_last = $tmp_new_out_ward_of_last_month;
		$tmp_total_of_last_month = $tmp_total_of_this_month;
		$tmp_inpatient_of_last_month = $tmp_inpatient_of_this_month;
		$tmp_outpatient_of_last_month = $tmp_outpatient_of_this_month;
		$tmp_new_in_ward_of_last_month = $tmp_new_in_ward_of_this_month;
		$tmp_new_out_ward_of_last_month = $tmp_new_out_ward_of_this_month;
	}

	return $status;
}

function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

// 入院日時時点の病室を返す
function get_initial_room($movings, $cur_ptrm) {
	if (count($movings) == 0) {
		return $cur_ptrm;
	} else {
		return "{$movings[0]["from_bldg_cd"]}-{$movings[0]["from_ward_cd"]}-{$movings[0]["from_ptrm_room_no"]}";
	}
}

// 指定日時時点の病室を返す（入院日時時点の病室が分かっている前提）
function get_room_by_datetime($datetime, $in_dttm, $out_dttm, $movings, $cur_ptrm) {

	// 入院期間外であれば、falseを返す
	if ($datetime < $in_dttm || $datetime >= $out_dttm) {
		return false;
	}

	// 指定日時直後の転棟データを取得
	$post_moving = get_post_moving($datetime, $movings);

	// 転棟データが見つかれば、そのfrom病室を返す
	if ($post_moving) {
		return "{$post_moving["from_bldg_cd"]}-{$post_moving["from_ward_cd"]}-{$post_moving["from_ptrm_room_no"]}";
	}

	// 現在入院中の病室を返す
	return $cur_ptrm;
}

// 指定時刻直後の転棟情報を返す
function get_post_moving($datetime, $movings) {
	for ($i = 0, $j = count($movings); $i < $j; $i++) {
		if ("{$movings[$i]["move_dt"]}{$movings[$i]["move_tm"]}" >= $datetime) {
			return $movings[$i];
		}
	}
	return false;
}

function is_countable_ptrm($ptrm, $ptrm_ids, $except_rooms) {
	if (in_array($ptrm, $except_rooms)) {
		return false;
	}

	return in_array($ptrm, $ptrm_ids);
}

function is_except_patient($except_flg, $date, $except_terms) {
	if ($except_flg == "f") {
		return false;
	}

	if ($except_terms[1]["from"] == "") {
		return true;
	}

	foreach ($except_terms as $tmp_except_term) {
		if ($tmp_except_term["from"] == "") {
			continue;
		}

		if ($tmp_except_term["from"] <= $date && $date <= $tmp_except_term["to"]) {
			return true;
		}
	}

	return false;
}

function must_count_as_in_ward($from_ptrm, $to_ptrm, $ptrm_ids, $except_rooms) {

	// 転棟元が選択区分の病室の場合はカウント対象外
	if (in_array($from_ptrm, $ptrm_ids)) {
		return false;
	}

	// 転棟先がカウント対象外の場合はカウント対象外
	if (!is_countable_ptrm($to_ptrm, $ptrm_ids, $except_rooms)) {
		return false;
	}

	return true;
}

function must_count_as_out_ward($from_ptrm, $to_ptrm, $ptrm_ids, $except_rooms) {

	// 転棟先が選択区分の病室の場合はカウント対象外
	if (in_array($to_ptrm, $ptrm_ids)) {
		return false;
	}

	// 転棟元がカウント対象外の場合はカウント対象外
	if (!is_countable_ptrm($from_ptrm, $ptrm_ids, $except_rooms)) {
		return false;
	}

	return true;
}
