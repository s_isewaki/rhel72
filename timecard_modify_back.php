<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix タイムカード | 勤務時間修正再申請</title>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("show_clock_in_common.ini");
require_once("show_select_values.ini");
require_once("show_attendance_pattern.ini");
require_once("atdbk_common_class.php");
require_once("html_utils.php");
require_once("show_timecard_apply_detail.ini");
require_once("show_timecard_apply_history.ini");
require_once("application_imprint_common.ini");
require_once("timecard_bean.php");
require_once("timecard_paid_hol_hour_class.php");
require_once("timecard_common_class.php");
require_once("ovtm_class.php");


$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
$timecard_common_class = new timecard_common_class($con, $fname, $emp_id, $date, $date);
//時間有休
$obj_hol_hour = new timecard_paid_hol_hour_class($con, $fname);
$obj_hol_hour->select();
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

//出勤表関連共通クラス
$atdbk_common_class = new atdbk_common_class($con, $fname);
//javascript出力
$atdbk_common_class->setJavascript();

$ovtm_class = new ovtm_class($con, $fname);

// ログインユーザの職員IDを取得
$emp_id = get_emp_id($con, $session, $fname);
// 氏名を取得
$sql  = "SELECT EMP_LT_NM ||' '|| EMP_FT_NM AS NAME ";
$sql .= "  FROM EMPMST ";
$cond = " WHERE EMP_ID = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_name = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "name") : "";

if($target_apply_id == "") {
	$target_apply_id = $apply_id;
}

//登録エラーから戻ったフラグ
$err_back_page_flg = ($back == "t");

// 勤務時間修正申請情報を取得
$sql = "select empmst.emp_lt_nm, empmst.emp_ft_nm, tmmdapply.target_date, tmmdapply.reason_id, mdfyrsn.reason, tmmdapply.reason as other_reason, tmmdapply.apply_time, tmmdapply.comment, tmmdapply.a_pattern, tmmdapply.a_reason, tmmdapply.a_night_duty, tmmdapply.a_allow_id, atdbkrslt.start_time as a_start_time, tmmdapply.a_out_time, tmmdapply.a_ret_time, atdbkrslt.end_time as a_end_time, tmmdapply.a_o_start_time1, tmmdapply.a_o_end_time1, tmmdapply.a_o_start_time2, tmmdapply.a_o_end_time2, tmmdapply.a_o_start_time3, tmmdapply.a_o_end_time3, tmmdapply.a_o_start_time4, tmmdapply.a_o_end_time4, tmmdapply.a_o_start_time5, tmmdapply.a_o_end_time5, tmmdapply.a_o_start_time6, tmmdapply.a_o_end_time6, tmmdapply.a_o_start_time7, tmmdapply.a_o_end_time7, tmmdapply.a_o_start_time8, tmmdapply.a_o_end_time8, tmmdapply.a_o_start_time9, tmmdapply.a_o_end_time9, tmmdapply.a_o_start_time10, tmmdapply.a_o_end_time10, a_tmcd_group_id, a_meeting_time, a_previous_day_flag, a_next_day_flag, atdbkrslt.start_btn_time, atdbkrslt.end_btn_time, atdbkrslt.start_btn_time2, atdbkrslt.end_btn_time2, atdbkrslt.start_btn_date1, atdbkrslt.end_btn_date1, atdbkrslt.start_btn_date2, atdbkrslt.end_btn_date2, tmmdapply.a_meeting_start_time, tmmdapply.a_meeting_end_time, tmmdapply.a_allow_count, tmmdapply.a_over_start_time, tmmdapply.a_over_end_time, tmmdapply.a_allow_count, tmmdapply.a_over_start_time, tmmdapply.a_over_end_time, tmmdapply.a_over_start_next_day_flag, tmmdapply.a_over_end_next_day_flag, tmmdapply.a_over_start_time2, tmmdapply.a_over_end_time2, tmmdapply.a_over_start_next_day_flag2, tmmdapply.a_over_end_next_day_flag2 " .
	", tmmdapply.b_paid_hol_hour_start_time " . //時間有休
	", tmmdapply.a_paid_hol_hour_start_time " .
	", tmmdapply.b_paid_hol_use_hour " .
	", tmmdapply.a_paid_hol_use_hour " .
	", tmmdapply.b_paid_hol_use_minute " .
    ", tmmdapply.a_paid_hol_use_minute ";
$sql .=	", tmmdapply.a_rest_start_time";
$sql .=	", tmmdapply.a_rest_end_time";
for ($i=3; $i<=5; $i++) {
    $sql .=	", tmmdapply.a_over_start_time{$i}";
    $sql .=	", tmmdapply.a_over_end_time{$i}";
    $sql .=	", tmmdapply.a_over_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_over_end_next_day_flag{$i}";
}
for ($i=1; $i<=5; $i++) {
    $sql .=	", tmmdapply.a_rest_start_time{$i}";
    $sql .=	", tmmdapply.a_rest_end_time{$i}";
    $sql .=	", tmmdapply.a_rest_start_next_day_flag{$i}";
    $sql .=	", tmmdapply.a_rest_end_next_day_flag{$i}";
}
$sql .=	", tmmdapply.a_allow_id2";
$sql .=	", tmmdapply.a_allow_count2";
$sql .=	", tmmdapply.a_allow_id3";
$sql .=	", tmmdapply.a_allow_count3";
$sql .= " from (tmmdapply inner join empmst on tmmdapply.emp_id = empmst.emp_id) left join mdfyrsn on tmmdapply.reason_id = mdfyrsn.reason_id left join atdbkrslt on atdbkrslt.emp_id = tmmdapply.emp_id and CAST(atdbkrslt.date AS varchar) = tmmdapply.target_date ";
$cond = "where tmmdapply.apply_id = $target_apply_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

$target_date = pg_fetch_result($sel, 0, "target_date");
//一括修正フラグ、申請情報の日付を年月にした場合に、一括修正とする 20100219
$all_flg =  (strlen($target_date) == 6) ? true : false;

$apply_time = pg_fetch_result($sel, 0, "apply_time");
$arr_tmmdapply = array();
if ($err_back_page_flg || $postback == "t") {
	//登録エラーから戻った場合
	$work_tmcd_group_id = $groupId;
	$other_reason = $reason;

    $start_btn_time = pg_fetch_result($sel, 0, "start_btn_time");
    $end_btn_time = pg_fetch_result($sel, 0, "end_btn_time");
    $start_btn_time2 = pg_fetch_result($sel, 0, "start_btn_time2");
    $end_btn_time2 = pg_fetch_result($sel, 0, "end_btn_time2");
    $start_btn_date1 = pg_fetch_result($sel, 0, "start_btn_date1");
    $end_btn_date1 = pg_fetch_result($sel, 0, "end_btn_date1");
    $start_btn_date2 = pg_fetch_result($sel, 0, "start_btn_date2");
    $end_btn_date2 = pg_fetch_result($sel, 0, "end_btn_date2");
}
else{
	$reason_id = pg_fetch_result($sel, 0, "reason_id");
	$reason = pg_fetch_result($sel, 0, "reason");
	$other_reason = pg_fetch_result($sel, 0, "other_reason");
    if ($reason_id == "" && $other_reason != "") {
        $reason_id = "other";
    }
    $comment = pg_fetch_result($sel, 0, "comment");

	$meeting_time = pg_fetch_result($sel, 0, "a_meeting_time");
	$work_tmcd_group_id = pg_fetch_result($sel, 0, "a_tmcd_group_id");
	$pattern = pg_fetch_result($sel, 0, "a_pattern");
	$reason_jiyu = pg_fetch_result($sel, 0, "a_reason");
	$night_duty = pg_fetch_result($sel, 0, "a_night_duty");
	$allow_id = pg_fetch_result($sel, 0, "a_allow_id");
	$allow_count = pg_fetch_result($sel, 0, "a_allow_count");
	$allow_id2 = pg_fetch_result($sel, 0, "a_allow_id2");
	$allow_count2 = pg_fetch_result($sel, 0, "a_allow_count2");
	$allow_id3 = pg_fetch_result($sel, 0, "a_allow_id3");
	$allow_count3 = pg_fetch_result($sel, 0, "a_allow_count3");

	$start_time = pg_fetch_result($sel, 0, "a_start_time");
	$out_time = pg_fetch_result($sel, 0, "a_out_time");
	$ret_time = pg_fetch_result($sel, 0, "a_ret_time");
	$end_time = pg_fetch_result($sel, 0, "a_end_time");
    //追加
    $rest_start_time = pg_fetch_result($sel, 0, "a_rest_start_time");
    $rest_end_time = pg_fetch_result($sel, 0, "a_rest_end_time");
    for ($i = 1; $i <= 10; $i++) {
		$start_time_ver = "o_start_time$i";
		$end_time_ver = "o_end_time$i";
		$$start_time_ver = pg_fetch_result($sel, 0, "a_o_start_time$i");
		$$end_time_ver = pg_fetch_result($sel, 0, "a_o_end_time$i");
	}

	$previous_day_flag = pg_fetch_result($sel, 0, "a_previous_day_flag");
	$next_day_flag = pg_fetch_result($sel, 0, "a_next_day_flag");

	$start_btn_time = pg_fetch_result($sel, 0, "start_btn_time");
	$end_btn_time = pg_fetch_result($sel, 0, "end_btn_time");
    $start_btn_time2 = pg_fetch_result($sel, 0, "start_btn_time2");
    $end_btn_time2 = pg_fetch_result($sel, 0, "end_btn_time2");
    $start_btn_date1 = pg_fetch_result($sel, 0, "start_btn_date1");
    $end_btn_date1 = pg_fetch_result($sel, 0, "end_btn_date1");
    $start_btn_date2 = pg_fetch_result($sel, 0, "start_btn_date2");
    $end_btn_date2 = pg_fetch_result($sel, 0, "end_btn_date2");
    $meeting_start_time = pg_fetch_result($sel, 0, "a_meeting_start_time");
	$meeting_end_time = pg_fetch_result($sel, 0, "a_meeting_end_time");
	$allow_count = pg_fetch_result($sel, 0, "a_allow_count");
	$over_start_time = pg_fetch_result($sel, 0, "a_over_start_time");
	$over_end_time = pg_fetch_result($sel, 0, "a_over_end_time");
	$over_start_time = pg_fetch_result($sel, 0, "a_over_start_time");
	$over_end_time = pg_fetch_result($sel, 0, "a_over_end_time");
	$over_start_next_day_flag = pg_fetch_result($sel, 0, "a_over_start_next_day_flag");
	$over_end_next_day_flag = pg_fetch_result($sel, 0, "a_over_end_next_day_flag");
	$over_start_time2 = pg_fetch_result($sel, 0, "a_over_start_time2");
	$over_end_time2 = pg_fetch_result($sel, 0, "a_over_end_time2");
	$over_start_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_start_next_day_flag2");
	$over_end_next_day_flag2 = pg_fetch_result($sel, 0, "a_over_end_next_day_flag2");

	$paid_hol_hour_start_time = pg_fetch_result($sel, 0, "a_paid_hol_hour_start_time"); //時間有休
	$paid_hol_start_hour = substr($paid_hol_hour_start_time, 0, 2);
	$paid_hol_start_min = substr($paid_hol_hour_start_time, 2, 2);
	$paid_hol_hour = pg_fetch_result($sel, 0, "a_paid_hol_use_hour");
	$paid_hol_min = pg_fetch_result($sel, 0, "a_paid_hol_use_minute");
    //tmmdapplyをまとめて取得
    $arr_tmmdapply = pg_fetch_array($sel, 0, PGSQL_ASSOC);
}
list($start_btn_hour, $start_btn_min) = split_hm($start_btn_time);
list($end_btn_hour, $end_btn_min) = split_hm($end_btn_time);


if ($err_back_page_flg == false && $postback != "t"){
	// 時刻を時・分に分割
	list($start_hour, $start_min) = split_hm($start_time);
	list($out_hour, $out_min) = split_hm($out_time);
	list($ret_hour, $ret_min) = split_hm($ret_time);
	list($end_hour, $end_min) = split_hm($end_time);
    list($rest_start_hour, $rest_start_min) = split_hm($rest_start_time);
    list($rest_end_hour, $rest_end_min) = split_hm($rest_end_time);
    for ($i = 1; $i <= 10; $i++) {
		$start_time_var = "o_start_time$i";
		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		list($$start_hour_var, $$start_min_var) = split_hm($$start_time_var);
		$end_time_var = "o_end_time$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";
		list($$end_hour_var, $$end_min_var) = split_hm($$end_time_var);
	}

	if ($meeting_time != null ){
		$meeting_time_hh = substr($meeting_time, 0, 2);
		$meeting_time_mm = substr($meeting_time, 2, 2);
	}
	list($meeting_start_hour, $meeting_start_min) = split_hm($meeting_start_time);
	list($meeting_end_hour, $meeting_end_min) = split_hm($meeting_end_time);
	list($over_start_hour, $over_start_min) = split_hm($over_start_time);
	list($over_end_hour, $over_end_min) = split_hm($over_end_time);
	list($over_start_hour2, $over_start_min2) = split_hm($over_start_time2);
	list($over_end_hour2, $over_end_min2) = split_hm($over_end_time2);
}
else {
	$allow_id = $allow_ids[0];
	$allow_count = $allow_counts[0];
	$allow_id2 = $allow_ids2[0];
	$allow_count2 = $allow_counts2[0];
	$allow_id3 = $allow_ids3[0];
	$allow_count3 = $allow_counts3[0];
}

// 「退勤後復帰」ボタン表示フラグを取得
$sql = "select ret_btn_flg from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$ret_btn_flg = pg_fetch_result($sel, 0, "ret_btn_flg");

// 退勤後復帰時刻表示フラグを設定
$ret_show_flg = ($ret_btn_flg == "t" || $o_start_hour1 != "");

// 勤務時間修正理由一覧を取得
$sql = "select reason_id, reason from mdfyrsn";
$cond = "where del_flg = 'f' order by reason_id";
$sel_reason = select_from_table($con, $sql, $cond, $fname);
if ($sel_reason == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//出勤予定にグループが指定されていた場合、出勤予定のグループを優先する
if ($work_tmcd_group_id == null && strlen($work_tmcd_group_id) == 0 && $err_back_page_flg == false){
	// ログインユーザの出勤グループを取得
	$work_tmcd_group_id = get_timecard_group_id($con, $emp_id, $fname);
}

// 出勤パターン名・事由名を配列で取得
$arr_attendance_pattern = $atdbk_common_class->get_pattern_array($work_tmcd_group_id);

// 手当情報取得
$arr_allowance = get_timecard_allowance($con, $fname);

if ($postback != "t") {
    $start_hour = ($start_time != "") ? intval(substr($start_time, 0, 2), 10) : "";
    $start_min = substr($start_time, 2, 2);
    $end_hour = ($end_time != "") ? intval(substr($end_time, 0, 2), 10) : "";
    $end_min = substr($end_time, 2, 2);
    $rest_start_hour = ($rest_start_time != "") ? intval(substr($rest_start_time, 0, 2), 10) : "";
    $rest_start_min = substr($rest_start_time, 2, 2);
    $rest_end_hour = ($rest_end_time != "") ? intval(substr($rest_end_time, 0, 2), 10) : "";
    $rest_end_min = substr($rest_end_time, 2, 2);
    $out_hour = ($out_time != "") ? intval(substr($out_time, 0, 2), 10) : "";
    $out_min = substr($out_time, 2, 2);
    $ret_hour = ($ret_time != "") ? intval(substr($ret_time, 0, 2), 10) : "";
    $ret_min = substr($ret_time, 2, 2);
}

//前日チェックがあるかフラグ
$previous_day_flag_checked = "";
if ($previous_day_flag == 1){
	$previous_day_flag_checked = "checked";
}

//翌日チェックがあるかフラグ
$next_day_flag_checked = "";
if ($next_day_flag == 1){
	$next_day_flag_checked = "checked";
}
//翌日フラグデフォルト設定 20100114
$over_start_next_day_flag_checked = "";
if ($over_start_next_day_flag == 1){
	$over_start_next_day_flag_checked = "checked";
}
$over_end_next_day_flag_checked = "";
if ($over_end_next_day_flag == 1){
	$over_end_next_day_flag_checked = "checked";
}
$over_start_next_day_flag2_checked = "";
if ($over_start_next_day_flag2 == 1){
	$over_start_next_day_flag2_checked = "checked";
}
$over_end_next_day_flag2_checked = "";
if ($over_end_next_day_flag2 == 1){
	$over_end_next_day_flag2_checked = "checked";
}
// 分の"--"を""とする
if ($over_start_min == "--") {
	$over_start_min = "";
}
if ($over_end_min == "--") {
	$over_end_min = "";
}
if ($over_start_min2 == "--") {
	$over_start_min2 = "";
}
if ($over_end_min2 == "--") {
	$over_end_min2 = "";
}

$this_title = "勤務時間修正再申請";
$this_url = "timecard_modify_back.php?session=$session&apply_id=$apply_id&pnt_url=" . urlencode($pnt_url);

// 残業理由一覧を取得
$sql = "select reason_id, reason from ovtmrsn";
$cond = "where del_flg = 'f' order by reason_id";
$ovtmrsn_sel = select_from_table($con, $sql, $cond, $fname);
if ($ovtmrsn_sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 勤務条件テーブルより残業管理を取得
$sql = "select no_overtime from empcond";
$cond = "where emp_id = '$emp_id' ";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$no_overtime = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "no_overtime") : "";

//残業理由表示フラグ
if ((($over_start_hour != "" && $over_start_hour != "--") ||
		($over_start_min != "" && $over_start_min != "--") ||
		($over_end_hour != "" && $over_end_hour != "--") ||
		($over_end_min != "" && $over_end_min != "--") ||
		($over_start_hour2 != "" && $over_start_hour2 != "--") ||
		($over_start_min2 != "" && $over_start_min2 != "--") ||
		($over_end_hour2 != "" && $over_end_hour2 != "--") ||
		($over_end_min2 != "" && $over_end_min2 != "--")
		)
		&& $timecard_bean->over_time_apply_type != "0"
		&& $no_overtime != "t" // 残業管理をする場合
	){
	$ovtm_rsn_id_disp = "";

	//エラーで再表示でない場合、残業申請情報を取得
	if ($err_back_page_flg == false){
		$sql = "select apply_id, reason_id, reason, reason_detail, apply_status from ovtmapply";
        $cond = "where emp_id = '$emp_id' and target_date = '$target_date' and delete_flg = 'f' order by apply_id desc limit 1";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0)
		{
			$ovtm_apply_id = pg_fetch_result($sel, 0, "apply_id");
			$ovtm_reason_id = pg_fetch_result($sel, 0, "reason_id");
			$ovtm_reason = pg_fetch_result($sel, 0, "reason");
			//$ovtm_reason_detail = pg_fetch_result($sel, 0, "reason_detail");
			//$apply_status = pg_fetch_result($sel, 0, "apply_status");
		}
	}
}
else {
	$ovtm_rsn_id_disp = "none";
}
// 勤務実績情報を取得、所定時間等
$arr_result = $timecard_common_class->get_clock_in_atdbkrslt_array($con, $emp_id, $fname, "1", $target_date);
//残業理由
$arr_ovtmrsn = $ovtm_class->get_ovtmrsn($arr_result["tmcd_group_id"]);

$table_width = ($ovtm_class->keyboard_input_flg != "t" && $ovtm_class->ovtm_rest_disp_flg == "t") ? 720 : 620;
//$maxline = ($_POST["ovtmadd_disp_flg"] == '1') ? 5 : 2;
$maxline = ($ovtmadd_disp_flg == '1') ? 5 : 2;

if ($_POST["ovtmadd_disp_flg"] == "" && $ovtmadd_disp_flg != "") {
    $_POST["ovtmadd_disp_flg"] = $ovtmadd_disp_flg;
}
$_POST["stat_update_flg"] = $stat_update_flg;

$arr_ovtm_data = $timecard_common_class->get_ovtm_month_total($emp_id, $target_date, "input");

// 未設定時のデフォルト取得
if ($arr_result["over_start_time"] == "" || $arr_result["over_end_time"] == "") {
    $emp_officehours = $timecard_common_class->get_officehours_emp($emp_id, $target_date, $arr_result["tmcd_group_id"], $arr_result["pattern"]);

    $arr_default_time = $timecard_common_class->get_default_overtime($arr_result["start_time"], $arr_result["end_time"], $arr_result["next_day_flag"], $arr_result["tmcd_group_id"], $arr_result["pattern"], $arr_result["type"], $arr_result["previous_day_flag"], $emp_officehours["office_end_time"] );
}

$arr_rest_data = $timecard_common_class->get_office_rest_data($emp_id, $target_date);
$page = "timecard_back";
$btn_str = $ovtm_class->get_btn_str($target_apply_id, $page);
//追加タブリンクで移動時、勤務パターンが変更されていたら、チェック用情報を設定
if ($postback == "t") {
    if ($pattern != $arr_result["pattern"] || $groupId != $arr_result["tmcd_group_id"]) {
        $arr_officetime_info = $timecard_common_class->get_officetime_info($emp_id, $groupId, $pattern, $target_date);
        $arr_result["office_start_time"] = $arr_officetime_info["office_start_time"];
        $arr_result["office_end_time"] = $arr_officetime_info["office_end_time"];
        $arr_result["office_rest_start_time"] = $arr_officetime_info["rest_start_time"];
        $arr_result["office_rest_end_time"] = $arr_officetime_info["rest_end_time"];
        $arr_rest_data["office_time_min"] = $arr_officetime_info["office_time_min"];
        $arr_rest_data["rest_time_min"] = $arr_officetime_info["rest_time_min"];
    }
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/focustonext.js?<? echo time(); ?>"></script>
<script type="text/javascript" src="js/atdbk_timecard_edit.js?<? echo time(); ?>"></script>
<script type="text/javascript">
<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	echo("resizeTo(1220, 740);\n");
}
?>
var arr_atdptn_reason = new Array();
<?
//事由情報を出力
$arr_group_pattern_reason = $atdbk_common_class->get_pattern_reason_array("");
foreach ($arr_group_pattern_reason as $wk_tmcd_group_id => $arr_pattern_reason) {
    foreach ($arr_pattern_reason as $wk_atdptn_id => $wk_reason) {
        if ($wk_reason != "") {
            echo("arr_atdptn_reason['{$wk_tmcd_group_id}_{$wk_atdptn_id}'] = '$wk_reason';\n");
        }
    }
}
?>
function setReason(atdptn_id) {

	if (atdptn_id == '--') {
		document.mainform["reason_jiyu"].value = '--';
		return;
	}
	wk_tmcd_group_id = document.mainform["list_tmcd_group_id"].value;
	wk_id = wk_tmcd_group_id+'_'+atdptn_id;
	if (arr_atdptn_reason[wk_id] == undefined) {
		document.mainform["reason_jiyu"].value = '--';
	}
    else {
	    document.mainform["reason_jiyu"].value = arr_atdptn_reason[wk_id];
    }

    //ajax、所定時間取得 20140826
    var emp_id = document.getElementById('emp_id').value;
    var tmcd_group_id = document.getElementById('list_tmcd_group_id').value;
    var pattern = document.getElementById('pattern').value;
    var date = document.getElementById('date').value;
    get_officetime_ajax(emp_id, tmcd_group_id, pattern, date);

}

function setOtherReasonDisabled() {
	if (document.getElementById('other_reason')) {
		document.getElementById('other_reason').style.display = (document.mainform.reason_id.value == 'other') ? '' : 'none';
	}
}

function history_select(apply_id, target_apply_id) {

    document.mainform.postback.value = '';
    document.mainform.ovtmadd_disp_flg.value = 0;
	document.mainform.apply_id.value = apply_id;
	document.mainform.target_apply_id.value = target_apply_id;
	document.mainform.action="timecard_modify_back.php?session=<?=$session?>";
	document.mainform.submit();
}

//休憩不足時間（分）
var rest_fusoku_min = 0;
//残業限度超時間（メッセージ用）
var over_limit_str = '';

var msg = '';
function re_apply() {
    //休憩時間、残業時間チェック
    chk_flg = checkTime(1);
	if(chk_flg)
	{
        if (chk_flg == 1) {
            return false;
        }

		if (!confirm(msg))
		{
			return;
		}
		document.mainform.action = 'timecard_modify_back_exe.php';
		document.mainform.submit();
	}
	else if (confirm('再申請します。よろしいですか？')) {
		document.mainform.action = 'timecard_modify_back_exe.php';
		document.mainform.submit();
	}
}

// テキストエリア行数の自動拡張
var ResizingTextArea = Class.create();

ResizingTextArea.prototype = {
    defaultRows: 1,

    initialize: function(field)
    {
        this.defaultRows = Math.max(field.rows, 1);
        this.resizeNeeded = this.resizeNeeded.bindAsEventListener(this);
        Event.observe(field, "click", this.resizeNeeded);
        Event.observe(field, "keyup", this.resizeNeeded);
    },

    resizeNeeded: function(event)
    {
        var t = Event.element(event);
        var lines = t.value.split('\n');
        var newRows = lines.length + 1;
        var oldRows = t.rows;
        for (var i = 0; i < lines.length; i++)
        {
            var line = lines[i];
            if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
        }
        if (newRows > t.rows) t.rows = newRows;
        if (newRows < t.rows) t.rows = Math.max(this.defaultRows, newRows);

        // 履歴表示table高さ調整
        resize_history_tbl();
    }
}

// 全テキストエリアの行数変更
function resizeAllTextArea() {
	var objs = document.getElementsByTagName('textarea');
	for (var i = 0, j = objs.length; i < j; i++) {
	    var t = objs[i];
		var defaultRows = Math.max(t.rows, 1);
	    var lines = t.value.split('\n');
	    var newRows = lines.length + 1;
	    var oldRows = t.rows;
	    for (var k = 0; k < lines.length; k++)
	    {
	        var line = lines[k];
	        if (line.length >= t.cols) newRows += Math.floor(line.length / t.cols);
	    }
	    if (newRows > t.rows) t.rows = newRows;
	    if (newRows < t.rows) t.rows = Math.max(defaultRows, newRows);
	}
}


// 履歴表示tableの高さ調節
function resize_history_tbl()
{
    document.getElementById('history_tbl').style.height = document.getElementById('dtl_tbl').offsetHeight;
}
//残業理由その他表示
function setOvtmReasonDisabled(item_idx) {
	var ovtm_reason_id_nm = 'ovtm_reason_id'+item_idx;
	var ovtm_reason_div_nm = 'ovtm_reason_div'+item_idx;

	if (document.getElementById(ovtm_reason_div_nm)) {
		document.getElementById(ovtm_reason_div_nm).style.display = (document.getElementById(ovtm_reason_id_nm).value == 'other') ? '' : 'none';
	}
}

//残業理由表示
function setOvtmReasonIdDisabled() {
	var disp = 'none';
	var disp = 'none';
	if ((document.getElementById('over_start_hour').value != '' &&
		 document.getElementById('over_start_hour').value != '--') ||
		(document.getElementById('over_start_hour2').value != '' &&
		 document.getElementById('over_start_hour2').value != '--')) {
		disp = '';
	}

	document.getElementById('ovtm_rsn_id').style.display = disp;
}

function ovtmAddDisp(flg) {
    document.mainform.postback.value = 't';
    var val = (flg) ? '1' : '0';
    document.mainform.ovtmadd_disp_flg.value = val;
    document.mainform.action = 'timecard_modify_back.php';
    document.mainform.submit();
    return false;
}

<? //翌日フラグチェック ?>
var base_time = '<?
$base_time = $arr_result["office_end_time"];
if ($arr_result["office_end_time"] < $arr_result["office_start_time"]) {
    $base_time = $arr_result["office_start_time"];
}
$base_time = str_replace(":", "", $base_time);
echo $base_time; ?>';
    //労働基準法の休憩時間が取れていない場合は警告する
    var rest_check_flg = '<? echo $ovtm_class->rest_check_flg; ?>';
    //36協定の残業限度時間をこえたら警告する
    var ovtm_limit_check_flg = '<? echo $ovtm_class->ovtm_limit_check_flg; ?>';

    //行数
    var maxline = <? echo $maxline; ?>;
    //休憩時間チェック
    var kinmu_total_min = 0;
    var rest_total_min = 0;
    var office_time_min = <? echo $arr_rest_data["office_time_min"]; ?>;
    var rest_time_min = <? echo $arr_rest_data["rest_time_min"]; ?>;

    //36協定残業限度時間
var ovtm_limit_month = <?
$limit_month = ($ovtm_class->ovtm_limit_month == '') ? 0 : $ovtm_class->ovtm_limit_month;
echo $limit_month;
    ?>;
    var ovtm_limit_month_min = ovtm_limit_month * 60;
    //当月残業累計時間申請中含む
    var ovtm_approve_apply_min = <? echo $arr_ovtm_data["ovtm_approve_apply_min"]; ?>;

    //当月残業累計時間承認のみ
    var ovtm_approve_min = <? echo $arr_ovtm_data["ovtm_approve_min"]; ?>;

    var ovtm_rest_disp_flg = '<? echo $ovtm_class->ovtm_rest_disp_flg; ?>';

    var rest_disp_flg = '<? echo $ovtm_class->rest_disp_flg; ?>';

    var over_time_apply_type = '<? echo $timecard_bean->over_time_apply_type; ?>';

    var no_overtime = '<? echo $no_overtime; ?>';

    var apply_update_flg = '1'; //1:申請 2:更新

    var _session = '<? echo $session; ?>';
    var office_start_time = '<? echo $arr_result["office_start_time"]; ?>';
    var office_end_time = '<? echo $arr_result["office_end_time"]; ?>';
    var rest_start_time = '<? echo $arr_result["office_rest_start_time"]; ?>';
    var rest_end_time = '<? echo $arr_result["office_rest_end_time"]; ?>';
    var org_tmcd_group_id = '<? echo $arr_result["tmcd_group_id"]; ?>';
    var org_pattern = '<? echo $pattern; ?>';
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;padding:1px;}
table.block td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#5279a5 solid 0px;}


table.block2 {border-collapse:collapse;}
table.block2 td {border:#5279a5 solid 1px;padding:1px;}
table.block2 td {border-width:1;}

table.block3 {border-collapse:collapse;}
table.block3 td {border:#5279a5 solid 0px;}
table.block3 td {border-width:0;}


</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="setOtherReasonDisabled();resizeAllTextArea();resize_history_tbl();document.onkeydown=focusToNext;document.mainform.over_start_hour.focus();setOvtmReasonDisabled('');<?php
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	for ($i=2; $i<=5; $i++) {
		echo "setOvtmReasonDisabled('$i');";
	}
}
if ($ovtm_class->ovtm_reason2_input_flg == "t") {
	echo("resizeTo(1220, 740);\n");
}
?>">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>タイムカード修正</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="page_close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<? if (strpos($pnt_url, "application_menu.php") === false) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<? $bgcolor = ($ovtmadd_disp_flg != '1') ? "#f6f9ff" : ""; ?>
<td width="180" align="center" bgcolor="<? echo($bgcolor); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($ovtmadd_disp_flg != '1') { ?>
<b>
<? } else { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(false);">
<? } ?>
<? echo($this_title); ?>
<? if ($ovtmadd_disp_flg != '1') { ?>
</b>
<? } else { ?>
</a>
<? } ?>
</font></td>
<td width="5"></td>
<?
//一括修正以外の場合
if (!$all_flg) {
?>
<td width="180" align="center"><a href="atdbk_timecard_status_update.php?session=<? echo($session); ?>&date=<? echo($target_date); ?>&wherefrom=<? echo($wherefrom); ?>&pnt_url=<? echo(urlencode($pnt_url)); ?>&ref_url=<? echo(urlencode($this_url)); ?>&ref_title=<? echo(urlencode($this_title)); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤状況修正</font></a></td>
<? } ?>
<? $bgcolor = ($ovtmadd_disp_flg == '1') ? "#f6f9ff" : ""; ?>
    <? //タブを追加
    if ($ovtm_class->ovtm_tab_flg == "t") {
?>
<td width="180" align="center" bgcolor="<? echo($bgcolor); ?>">
        <?
if ($ovtmadd_disp_flg != '1') { ?>
<a href="javascript:void(0);" onclick="return ovtmAddDisp(true);">
<? } else { ?>
<b>
<? } ?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
残業追加
</font>
<? if ($ovtmadd_disp_flg != '1') { ?>
</a>
<? } else { ?>
</b>
<? } ?>
</td>
<? } ?>
<td>&nbsp;</td>
<td align="right">
<? echo $btn_str; ?>
</td>
</tr>
</table>
<? } ?>
<form name="mainform" action="timecard_modify_back_exe.php" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="140">
<? show_timecard_history_for_apply($con, $session, $fname, $apply_id, $target_apply_id);?>
</td>
<td><img src="img/spacer.gif" width="5" height="2" alt=""></td>
<td valign="top" width="">
<div id="dtl_tbl">
<?php
$colspan_num = "3";
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="90" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">氏名</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($emp_name); ?></font></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請日時</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\d{2}$/", "$1/$2/$3 $4:$5", $apply_time)); ?></font></td>
</tr>
<tr height="22">
<?
//一括修正以外の場合
if (!$all_flg) {
?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務日</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo(preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $target_date)); ?></font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<?
	//グループコンボボックス出力
    $atdbk_common_class->setGroupOption("groupId", "list_tmcd_group_id", $work_tmcd_group_id, "pattern");
?>
</td>
</tr>


<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務実績</font></td>
<?
echo("<td colspan=\"{$colspan_num}\" height=\"22\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
echo("<select name=\"pattern\" id=\"pattern\" onChange=\"setReason(this.value);\">");
echo("<option value=\"--\">");
foreach ($arr_attendance_pattern as $atdptn_id => $atdptn_val) {
	echo("<option value=\"$atdptn_id\"");
	if ($pattern == $atdptn_id) {
		echo(" selected");
	}
	echo(">$atdptn_val");
}
echo("</select>");
echo("</font></td>\n");
?>
</tr>

<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<?
echo("<td colspan=\"{$colspan_num}\" class=\"txt\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
$atdbk_common_class->set_reason_option("reason_jiyu", $reason_jiyu);
echo("</font></td>\n");
?>
</tr>
<?php
if ($ovtm_class->duty_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($atdbk_common_class->duty_or_oncall_str); ?></font></td>
<td colspan="<?php echo $colspan_num; ?>"><select name="night_duty"><?show_night_duty_options($night_duty);?></td>
</tr>
<?php } ?>
<?php
if ($ovtm_class->allowance_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td colspan="<?php echo $colspan_num; ?>"><nobr>
<?
show_allowance_list2($allow_id, $arr_allowance, 1);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count, 1);
echo "&nbsp";
show_allowance_list2($allow_id2, $arr_allowance, 2);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count2, 2);
echo "&nbsp";
show_allowance_list2($allow_id3, $arr_allowance, 3);
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">回数</font>
<?
show_allowance_count2($allow_count3, 3);
?>

</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="start_hour"  id="start_hour" value="<? echo $start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='start_min';backElement='<?
        //
        $backElement =($ovtm_class->ovtm_rest_disp_flg == "t") ? "rest_end_min$maxline" : "over_end_min$maxline";
        echo $backElement;
        ?>';">：<input type="text" name="start_min"  id="start_min" value="<? echo $start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='<?
        $backElement = ($ovtm_class->rest_disp_flg == "t") ? "rest_start_hour" : "end_hour";
        echo $backElement;
              ?>';backElement='start_hour';">
        <?
    }
    else {
    ?>
		<select name="start_hour" id="start_hour"><? show_hour_options_0_23($start_hour); ?></select>：<select name="start_min" id="start_min"><? show_min_options_00_59($start_min); ?></select>
        <? } ?>
		<label for="zenjitsu"><input type="checkbox" name="previous_day_flag" id="zenjitsu" <?=$previous_day_flag_checked ?> value="1">前日</label>
	</font>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    if ($ovtmadd_disp_flg != '1') {
        echo ("　　打刻&nbsp;");
        $tmp_start_btn_time = "";
        if ($start_btn_hour != "") {
	        echo($ovtm_class->get_btn_date_format($start_btn_date1));
            if ($start_btn_hour < 10) {
                $tmp_start_btn_time = "&nbsp;".(int)$start_btn_hour.":".$start_btn_min;
            } else {
                $tmp_start_btn_time = $start_btn_hour.":".$start_btn_min;
            }
        }
        echo($tmp_start_btn_time);
        if ($start_btn_time2 != "") {
            echo ("　　　出勤2&nbsp;");
	        echo($ovtm_class->get_btn_date_format($start_btn_date2));
            $tmp_start_btn_time2 = $ovtm_class->get_btn_time_format($start_btn_time2);
            echo($tmp_start_btn_time2);
        }
    }
?>
	</font>
</td>
</tr>
    <?
    if ($ovtm_class->rest_disp_flg == "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">休憩時間</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
        <?
        if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="rest_start_hour"  id="rest_start_hour" value="<? echo $rest_start_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_start_min';backElement='start_min';">：<input type="text" name="rest_start_min"  id="rest_start_min" value="<? echo $rest_start_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_end_hour';backElement='rest_start_hour';"> 〜
        <input type="text" name="rest_end_hour"  id="rest_end_hour" value="<? echo $rest_end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='rest_end_min';backElement='rest_start_min';">：<input type="text" name="rest_end_min"  id="rest_end_min" value="<? echo $rest_end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_hour';backElement='rest_end_hour';">
            <?
        }
        else {
    ?>
		<select name="rest_start_hour" id="rest_start_hour"><? show_hour_options_0_23($rest_start_hour); ?></select>：<select name="rest_start_min" id="rest_start_min"><? show_min_options_00_59($rest_start_min); ?></select> 〜
		<select name="rest_end_hour" id="rest_end_hour"><? show_hour_options_0_23($rest_end_hour); ?></select>：<select name="rest_end_min" id="rest_end_min"><? show_min_options_00_59($rest_end_min); ?></select>
        <? } ?>
	</font>
</td>
</tr>
<? } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">退勤時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="end_hour"  id="end_hour" value="<? echo $end_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='end_min';backElement='<?
        $backElement = ($ovtm_class->rest_disp_flg == "t") ? "rest_end_min" : "start_min";
        echo $backElement;
              ?>';">：<input type="text" name="end_min"  id="end_min" value="<? echo $end_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='<?
        $nextElement = ($ovtm_class->out_input_nodisp_flg != "t") ? "out_hour" : "over_start_hour";
        echo $nextElement;
              ?>';backElement='end_hour';">
        <?
    }
    else {
    ?>
		<select name="end_hour" id="end_hour"><? show_hour_options_0_23($end_hour); ?></select>：<select name="end_min" id="end_min"><? show_min_options_00_59($end_min); ?></select>
        <? } ?>
		<label for="yokujitsu"><input type="checkbox" name="next_day_flag" id="yokujitsu" <?=$next_day_flag_checked ?> value="1">翌日</label>
	</font>
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
    <?
    if ($ovtmadd_disp_flg != '1') {
        echo ("　　打刻&nbsp;");
        $tmp_end_btn_time = "";
        if ($end_btn_hour != "") {
	        echo($ovtm_class->get_btn_date_format($end_btn_date1));
            if ($end_btn_hour < 10) {
                $tmp_end_btn_time = "&nbsp;".(int)$end_btn_hour.":".$end_btn_min;
            } else {
                $tmp_end_btn_time = $end_btn_hour.":".$end_btn_min;
            }
        }
        echo($tmp_end_btn_time);
        if ($end_btn_time2 != "") {
            echo ("　　　退勤2&nbsp;");
	        echo($ovtm_class->get_btn_date_format($end_btn_date2));
            $tmp_end_btn_time2 = $ovtm_class->get_btn_time_format($end_btn_time2);
            echo($tmp_end_btn_time2);
        }
    }
?>
	</font>
</td>
</tr>
<?php
if ($ovtm_class->out_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外出時刻</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">
    <?
    if ($ovtm_class->keyboard_input_flg == "t") {
        ?>
        <input type="text" name="out_hour"  id="out_hour" value="<? echo $out_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='out_min';backElement='end_min';">：<input type="text" name="out_min"  id="out_min" value="<? echo $out_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='ret_hour';backElement='out_hour';"> 〜
        <input type="text" name="ret_hour"  id="ret_hour" value="<? echo $ret_hour; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='ret_min';backElement='out_min';">：<input type="text" name="ret_min"  id="ret_min" value="<? echo $ret_min; ?>" size="2" maxlength="2" style="ime-mode:inactive;" onkeydown="nextElement='over_start_hour';backElement='ret_hour';">
        <?
    }
    else {
    ?>
<select name="out_hour" id="out_hour"><? show_hour_options_0_23($out_hour); ?></select>：<select name="out_min" id="out_min"><? show_min_options_00_59($out_min); ?></select> 〜 <select name="ret_hour" id="ret_hour"><? show_hour_options_0_23($ret_hour); ?></select>：<select name="ret_min" id="ret_min"><? show_min_options_00_59($ret_min); ?></select>
        <? } ?>
</font></td>
</tr>
<?php } ?>
    <?
    $ovtm_class->disp_btn_time_data($arr_result, "input", $colspan_num);
?>
    <? //20140620
    $ovtm_class->disp_input_form($arr_result, $timecard_bean, $arr_default_time, $apply_status, $arr_tmmdapply, "1", $no_overtime, $obj_hol_hour->paid_hol_hour_flag, $ret_show_flg, $ovtm_apply_id, $colspan_num); //20150107$ovtm_apply_id追加
    $ovtm_class->disp_ovtm_total_time($arr_ovtm_data, "1");
    ?>
    <?
	//残業理由表示用のJavascript関数呼出文字列
	if ($timecard_bean->over_time_apply_type != "0" &&
			$no_overtime != "t") {
		$wk_onchange_str = "onChange=\"setOvtmReasonIdDisabled();\"";
	}
	else {
		$wk_onchange_str = "";
	}

?>
	<? //時間有休追加 20111207
if ($ovtm_class->hol_hour_input_nodisp_flg != "t") {
	$obj_hol_hour->show_input_item($paid_hol_start_hour, $paid_hol_start_min, $paid_hol_hour, $paid_hol_min, $emp_id, $colspan_num);
}
?>
	<?
if ($ret_show_flg) {
	$break_flg = false;
	for ($i = 1; $i <= 10; $i++) {
		if ($break_flg) {
			break;
		}

		$start_hour_var = "o_start_hour$i";
		$start_min_var = "o_start_min$i";
		$end_hour_var = "o_end_hour$i";
		$end_min_var = "o_end_min$i";

		echo("<tr height=\"22\">\n");
			echo("<td align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$ret_str}時刻{$i}</font></td>\n");
        echo("<td colspan=\"{$colspan_num}\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><select name=\"$start_hour_var\" id=\"$start_hour_var\">");
		show_hour_options_0_23($$start_hour_var);
        echo("</select>：<select name=\"$start_min_var\" name=\"$start_min_var\" id=\"$start_min_var\">");
		show_min_options_00_59($$start_min_var);
        echo("</select> 〜 <select name=\"$end_hour_var\" id=\"$end_hour_var\">");
		show_hour_options_0_23($$end_hour_var);
        echo("</select>：<select name=\"$end_min_var\" id=\"$end_min_var\">");
		show_min_options_00_59($$end_min_var);
		echo("</select></font></td>\n");
		echo("</tr>\n");

		if ($$start_hour_var == "") {
			$break_flg = true;
		}

		//エラーで戻った場合は次の値が空の場合ループを止める
		$err_start_hour_var = "o_start_hour".($i + 1);
        if (($err_back_page_flg || $postback == "t") && $$err_start_hour_var == "") {
			$break_flg = true;
		}
	}
}

if ($ovtm_class->meeting_input_nodisp_flg != "t") {
?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">会議・研修・<br>病棟外勤務</font></td>
<td colspan="<?php echo $colspan_num; ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<select name="meeting_start_hour" id="meeting_start_hour"><? show_hour_options_0_23($meeting_start_hour); ?></select>：<select name="meeting_start_min" id="meeting_start_min"><? show_min_options_00_59($meeting_start_min); ?></select> 〜 <select name="meeting_end_hour" id="meeting_end_hour"><? show_hour_options_0_23($meeting_end_hour); ?></select>：<select name="meeting_end_min" id="meeting_end_min"><? show_min_options_00_59($meeting_end_min); ?></select>
</font>
</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">修正理由</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<select name="reason_id" id="reason_id" onchange="setOtherReasonDisabled();">
<?
    //修正理由に空白追加 20121109
    $reason_select = "";
    if($reason_id == ""){
        $reason_select = "selected";
    }
    echo("<option value='' $reason_select></option>");
    while ($row = pg_fetch_array($sel_reason)) {
	$tmp_reason_id = $row["reason_id"];
	$tmp_reason = $row["reason"];

	echo("<option value=\"$tmp_reason_id\"");
	if ($tmp_reason_id == $reason_id) {echo(" selected");}
        echo(">$tmp_reason</option>\n");
}
    $onkeydown_str = " onblur=\"document.onkeydown=focusToNext;\" onfocus=\"document.onkeydown='';\""; //20141017
?>
<option value="other"<? if ($reason_id == "other") {echo(" selected");} ?>>その他</option>
</select><br>
<div id="other_reason" style="display:none;"><input type="text" name="reason" value="<? echo($other_reason); ?>" size="50" maxlength="100" style="ime-mode:active;" <?php echo $onkeydown_str; ?>></div>
</td>
<?php
//残業時刻2以降にも理由を入力する場合は既存の理由を表示しない
if ($ovtm_class->ovtm_reason2_input_flg != "t") {
?>
</tr>
<tr height="22" id="ovtm_rsn_id" style="display:<? echo($ovtm_rsn_id_disp); ?>;">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業理由</font></td>
<td colspan="<?php echo $colspan_num; ?>">
<select name="ovtm_reason_id" id="ovtm_reason_id" onchange="setOvtmReasonDisabled('');">
	<?
    $str = $ovtm_class->get_ovtmrsn_option($arr_result["tmcd_group_id"], $ovtm_reason_id);
    echo $str;
    //理由が入力済みの場合、その他とする
	$selected = ($ovtm_reason != "") ? " selected" : "";
	$ovtm_reason_disp = ($ovtm_reason != "" || pg_num_rows($ovtmrsn_sel) == 0) ? "" : "none"; //残業理由が０件はその他の入力ができるようにする 20120125
?>
<option value="other" <? echo($selected); ?>>その他
</select><br>
<div id="ovtm_reason_div" style="display:<? echo($ovtm_reason_disp); ?>;"><input type="text" name="ovtm_reason" value="<? echo($ovtm_reason); ?>" size="50" maxlength="100" style="ime-mode:active;" <?php echo $onkeydown_str; ?>></div>
</td>
</tr>
<?php } ?>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">申請者コメント</font></td>
<td colspan="<?php echo $colspan_num; ?>"><textarea name="comment" rows="5" cols="40" style="ime-mode:active;" onFocus="new ResizingTextArea(this);document.onkeydown='';" onblur="document.onkeydown=focusToNext;"><? echo($comment); ?></textarea></td>

<? } else { ?>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<br>
一括修正申請です。
<br>
<br>
</font>
</td>
<? } ?>

</tr>

<?
$page = "timecard_back";
show_application_apply_detail($con, $session, $fname, $target_apply_id, $page, $all_flg, $timecard_bean);
?>

</table>
</td>
</tr>
</table>

<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="emp_id" id="emp_id" value="<? echo($emp_id); ?>">
<input type="hidden" name="date" id="date" value="<? echo($target_date); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="target_apply_id" value="<? echo($target_apply_id); ?>">
<input type="hidden" name="pre_end_time" value="<? echo("$end_hour$end_min"); ?>">
<input type="hidden" name="pre_next_day_flag" value="<? echo("$next_day_flag"); ?>">
<input type="hidden" name="pre_night_duty_flag" value="<? echo(($night_duty == 1)); ?>">
<?
if ($ret_show_flg) {
	for ($i = 1; $i <= 10; $i++) {
		$hour_var = "o_start_hour$i";
		$min_var = "o_start_min$i";
		echo("<input type=\"hidden\" name=\"pre_o_start_time$i\" value=\"${$hour_var}${$min_var}\">\n");
	}
}
?>
<input type="hidden" name="pnt_url" value="<? echo($pnt_url); ?>">
<input type="hidden" name="start_btn_time" value="<? echo($start_btn_time); ?>">
<input type="hidden" name="end_btn_time" value="<? echo($end_btn_time); ?>">
<input type="hidden" name="postback" value="t">
<input type="hidden" name="ovtmadd_disp_flg" value="<? echo($ovtmadd_disp_flg); ?>">
<input type="hidden" name="wherefrom" value="<? echo($wherefrom); ?>">
</form>
</center>
</body>
</html>
<?

$update_cnt = 0;
$show_flg_name = "";
if($mode == 'show_flg_update') {

	// トランザクションを開始
	pg_query($con, "begin");

	$sql = "select apply_status, apv_fix_show_flg, apv_ng_show_flg, apv_bak_show_flg from tmmdapply";
	$cond = "where apply_id = $apply_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$apply_status = pg_fetch_result($sel, 0, "apply_status");
	$apv_fix_show_flg = pg_fetch_result($sel, 0, "apv_fix_show_flg");
	$apv_ng_show_flg = pg_fetch_result($sel, 0, "apv_ng_show_flg");
	$apv_bak_show_flg = pg_fetch_result($sel, 0, "apv_bak_show_flg");

	if ($apply_status == "1" && $apv_fix_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_fix_show_flg";
	}

	if ($apply_status == "2" && $apv_ng_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_ng_show_flg";
	}

	if ($apply_status == "3" && $apv_bak_show_flg == "t") {
		$update_cnt = 1;
		$show_flg_name = "apv_bak_show_flg";
	}

	if($update_cnt > 0) {

		$sql = "update tmmdapply set";
		$set = array($show_flg_name);
		$setvalue = array('f');
		$cond = "where apply_id = $apply_id and $show_flg_name = 't' and apply_status = '$apply_status'";
		$up = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if($up ==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
	}

	// トランザクションをコミット
	pg_query($con, "commit");
}

pg_close($con);
?>

<script type="text/javascript">
function page_close() {
<?
if($mode == 'show_flg_update' && $update_cnt > 0) {
?>
	if(window.opener && !window.opener.closed && window.opener.reload_page) {
		window.opener.reload_page();
		window.close();
	} else {
		window.close();
	}
<?
} else {
?>
	window.close();
<?
}
?>

}
</script>


<?
// 時刻を時・分に分割
function split_hm($time) {
	if ($time != "") {
		return array(substr($time, 0, 2), substr($time, 2, 2));
	} else {
		return array("", "");
	}
}

// 当直オプションを出力
function show_night_duty_options($night_duty) {
	$arr_night_duty_value = array("", "1", "2");
	$arr_night_duty_name = array("", "有り", "無し");
	for ($i = 0; $i < count($arr_night_duty_value); $i++) {
		$night_duty_value = $arr_night_duty_value[$i];
		$night_duty_name = $arr_night_duty_name[$i];

		echo("<option value=\"$night_duty_value\"");
		if ($night_duty_value == $night_duty) {
			echo(" selected");
		}
		echo(">$night_duty_name");
	}
}
?>
