<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
// 承認者画面登録
?>
<body>
<form name="items" action="timecard_approval.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="class_id" value="<? echo($class_id); ?>">
<input type="hidden" name="atrb_id" value="<? echo($atrb_id); ?>">
<input type="hidden" name="dept_id" value="<? echo($dept_id); ?>">
<input type="hidden" name="target_id_list1" value="<? echo($target_id_list1); ?>">

<input type="hidden" name="ovtm_approve_num" value="<? echo($ovtm_approve_num); ?>">
<input type="hidden" name="ovtm_wkfw_approve_num" value="<? echo($ovtm_wkfw_approve_num); ?>">
<input type="hidden" name="ovtm_wkfw_div" value="<? echo($ovtm_wkfw_div); ?>">

<input type="hidden" name="time_approve_num" value="<? echo($time_approve_num); ?>">
<input type="hidden" name="time_wkfw_approve_num" value="<? echo($time_wkfw_approve_num); ?>">
<input type="hidden" name="time_wkfw_div" value="<? echo($time_wkfw_div); ?>">

<input type="hidden" name="rtn_approve_num" value="<? echo($rtn_approve_num); ?>">
<input type="hidden" name="rtn_wkfw_approve_num" value="<? echo($rtn_wkfw_approve_num); ?>">
<input type="hidden" name="rtn_wkfw_div" value="<? echo($rtn_wkfw_div); ?>">

<input type="hidden" name="aprv_skip_flg" value="<? echo($aprv_skip_flg); ?>">

<input type="hidden" name="back" value="<? echo($back); ?>">

<?
if ($target_id_list1 != "") {
	$emp_ids = split(",", $target_id_list1);
} else {
	$emp_ids = array();
}
?>


<?
//ワークフロー承認階層
for ($i = 1; $i <= 20; $i++) {

    // 残業申請ワークフロー
	$varname = "ovtm_approve$i";
	echo("<input type=\"hidden\" name=\"ovtm_approve$i\" value=\"{$$varname}\">\n");

	$varname = "ovtm_apv_div0_flg$i";
	echo("<input type=\"hidden\" name=\"ovtm_apv_div0_flg$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_apv_div1_flg$i";
	echo("<input type=\"hidden\" name=\"ovtm_apv_div1_flg$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_apv_div4_flg$i";
	echo("<input type=\"hidden\" name=\"ovtm_apv_div4_flg$i\" value=\"{$$varname}\">\n");

	$varname = "ovtm_emp_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_emp_id$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_target_class_div$i";
	echo("<input type=\"hidden\" name=\"ovtm_target_class_div$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_st_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_st_id$i\" value=\"{$$varname}\">\n");

	$varname = "ovtm_multi_apv_flg$i";
	echo("<input type=\"hidden\" name=\"ovtm_multi_apv_flg$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_next_notice_div$i";
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div$i\" value=\"{$$varname}\">\n");

	$varname = "ovtm_class_sect_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_class_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_atrb_sect_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_atrb_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_dept_sect_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_dept_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_room_sect_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_room_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "ovtm_st_sect_id$i";
	echo("<input type=\"hidden\" name=\"ovtm_st_sect_id$i\" value=\"{$$varname}\">\n");

	$varname = "ovtm_apv_num$i";
	echo("<input type=\"hidden\" name=\"ovtm_apv_num$i\" value=\"{$$varname}\">\n");


    // 勤務時間修正申請申請ワークフロー
	$varname = "time_approve$i";
	echo("<input type=\"hidden\" name=\"time_approve$i\" value=\"{$$varname}\">\n");

	$varname = "time_apv_div0_flg$i";
	echo("<input type=\"hidden\" name=\"time_apv_div0_flg$i\" value=\"{$$varname}\">\n");
	$varname = "time_apv_div1_flg$i";
	echo("<input type=\"hidden\" name=\"time_apv_div1_flg$i\" value=\"{$$varname}\">\n");
	$varname = "time_apv_div4_flg$i";
	echo("<input type=\"hidden\" name=\"time_apv_div4_flg$i\" value=\"{$$varname}\">\n");

	$varname = "time_emp_id$i";
	echo("<input type=\"hidden\" name=\"time_emp_id$i\" value=\"{$$varname}\">\n");
	$varname = "time_target_class_div$i";
	echo("<input type=\"hidden\" name=\"time_target_class_div$i\" value=\"{$$varname}\">\n");
	$varname = "time_st_id$i";
	echo("<input type=\"hidden\" name=\"time_st_id$i\" value=\"{$$varname}\">\n");

	$varname = "time_multi_apv_flg$i";
	echo("<input type=\"hidden\" name=\"time_multi_apv_flg$i\" value=\"{$$varname}\">\n");
	$varname = "time_next_notice_div$i";
	echo("<input type=\"hidden\" name=\"time_next_notice_div$i\" value=\"{$$varname}\">\n");

	$varname = "time_class_sect_id$i";
	echo("<input type=\"hidden\" name=\"time_class_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "time_atrb_sect_id$i";
	echo("<input type=\"hidden\" name=\"time_atrb_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "time_dept_sect_id$i";
	echo("<input type=\"hidden\" name=\"time_dept_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "time_room_sect_id$i";
	echo("<input type=\"hidden\" name=\"time_room_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "time_st_sect_id$i";
	echo("<input type=\"hidden\" name=\"time_st_sect_id$i\" value=\"{$$varname}\">\n");

	$varname = "time_apv_num$i";
	echo("<input type=\"hidden\" name=\"time_apv_num$i\" value=\"{$$varname}\">\n");

    // 退勤後復帰申請申請ワークフロー
	$varname = "rtn_approve$i";
	echo("<input type=\"hidden\" name=\"rtn_approve$i\" value=\"{$$varname}\">\n");

	$varname = "rtn_apv_div0_flg$i";
	echo("<input type=\"hidden\" name=\"rtn_apv_div0_flg$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_apv_div1_flg$i";
	echo("<input type=\"hidden\" name=\"rtn_apv_div1_flg$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_apv_div4_flg$i";
	echo("<input type=\"hidden\" name=\"rtn_apv_div4_flg$i\" value=\"{$$varname}\">\n");

	$varname = "rtn_emp_id$i";
	echo("<input type=\"hidden\" name=\"rtn_emp_id$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_target_class_div$i";
	echo("<input type=\"hidden\" name=\"rtn_target_class_div$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_st_id$i";
	echo("<input type=\"hidden\" name=\"rtn_st_id$i\" value=\"{$$varname}\">\n");

	$varname = "rtn_multi_apv_flg$i";
	echo("<input type=\"hidden\" name=\"rtn_multi_apv_flg$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_next_notice_div$i";
	echo("<input type=\"hidden\" name=\"rtn_next_notice_div$i\" value=\"{$$varname}\">\n");

	$varname = "rtn_class_sect_id$i";
	echo("<input type=\"hidden\" name=\"rtn_class_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_atrb_sect_id$i";
	echo("<input type=\"hidden\" name=\"rtn_atrb_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_dept_sect_id$i";
	echo("<input type=\"hidden\" name=\"rtn_dept_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_room_sect_id$i";
	echo("<input type=\"hidden\" name=\"rtn_room_sect_id$i\" value=\"{$$varname}\">\n");
	$varname = "rtn_st_sect_id$i";
	echo("<input type=\"hidden\" name=\"rtn_st_sect_id$i\" value=\"{$$varname}\">\n");

	$varname = "rtn_apv_num$i";
	echo("<input type=\"hidden\" name=\"rtn_apv_num$i\" value=\"{$$varname}\">\n");

}
?>

</form>
<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへ接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

// 残業申請ワークフロー
if($ovtm_wkfw_div == "1")
{
	// 入力チェック
	if ($ovtm_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('残業申請の承認階層数を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}

	$ovtm_approve_mng = array();
	$chk_apv_exist_flg = true;
	for ($i = 0; $i < $ovtm_approve_num; $i++) {
		$idx = $i + 1;
	
		$apv_div0_flg = "ovtm_apv_div0_flg$idx";
		$apv_div1_flg = "ovtm_apv_div1_flg$idx";
		$apv_div4_flg = "ovtm_apv_div4_flg$idx";
	
		$target_class_div = "ovtm_target_class_div$idx";
		$multi_apv_flg = "ovtm_multi_apv_flg$idx";
		$next_notice_div = "ovtm_next_notice_div$idx";
	
		$apv_num = "ovtm_apv_num$idx";
	
		array_push($ovtm_approve_mng, array(
	                                    "apv_div0_flg" => ($$apv_div0_flg == "") ? "f" : $$apv_div0_flg,
	                                    "apv_div1_flg" => ($$apv_div1_flg == "") ? "f" : $$apv_div1_flg,
	                                    "apv_div4_flg" => ($$apv_div4_flg == "") ? "f" : $$apv_div4_flg,
	                                    "deci_flg" => "t",
	                                    "target_class_div" => $$target_class_div,
	                                    "multi_apv_flg" => $$multi_apv_flg,
	                                    "next_notice_div" => ($$next_notice_div == "") ? null : $$next_notice_div,
	                                    "apv_num" => ($$apv_num == "") ? null : $$apv_num,
	                                    "wkfw_div" => "1"
	                                   ));
	
		$approve = "ovtm_approve$idx";
		if($$approve == "") {
			$chk_apv_exist_flg = false;	 	
		}	
	}
	
	if(!$chk_apv_exist_flg) {
		echo("<script type=\"text/javascript\">alert('残業申請の承認階層を設定してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	
	if($ovtm_wkfw_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('残業申請の確定を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}

// 時間修正申請ワークフロー設定
if($time_wkfw_div == "1")
{
	// 入力チェック
	if ($time_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('時間修正申請の承認階層数を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	$time_approve_mng = array();
	$chk_apv_exist_flg = true;
	for ($i = 0; $i < $time_approve_num; $i++) {
		$idx = $i + 1;
	
		$apv_div0_flg = "time_apv_div0_flg$idx";
		$apv_div1_flg = "time_apv_div1_flg$idx";
		$apv_div4_flg = "time_apv_div4_flg$idx";
	
		$target_class_div = "time_target_class_div$idx";
		$multi_apv_flg = "time_multi_apv_flg$idx";
		$next_notice_div = "time_next_notice_div$idx";
	
		$apv_num = "time_apv_num$idx";
	
		array_push($time_approve_mng, array(
	                                    "apv_div0_flg" => ($$apv_div0_flg == "") ? "f" : $$apv_div0_flg,
	                                    "apv_div1_flg" => ($$apv_div1_flg == "") ? "f" : $$apv_div1_flg,
	                                    "apv_div4_flg" => ($$apv_div4_flg == "") ? "f" : $$apv_div4_flg,
	                                    "deci_flg" => "t",
	                                    "target_class_div" => $$target_class_div,
	                                    "multi_apv_flg" => $$multi_apv_flg,
	                                    "next_notice_div" => ($$next_notice_div == "") ? null : $$next_notice_div,
	                                    "apv_num" => ($$apv_num == "") ? null : $$apv_num,
	                                    "wkfw_div" => "2"
	                                   ));
	
		$approve = "time_approve$idx";
		if($$approve == "") {
			$chk_apv_exist_flg = false;	 	
		}	
	}
	
	if(!$chk_apv_exist_flg) {
		echo("<script type=\"text/javascript\">alert('時間修正申請の承認階層を設定してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	
	if($time_wkfw_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('時間修正申請の確定を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}



// 退勤後復帰申請ワークフロー設定
if($rtn_wkfw_div == "1")
{
	// 入力チェック
	if ($rtn_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('{$ret_str}申請の承認階層数を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	$rtn_approve_mng = array();
	$chk_apv_exist_flg = true;
	for ($i = 0; $i < $rtn_approve_num; $i++) {
		$idx = $i + 1;
	
		$apv_div0_flg = "rtn_apv_div0_flg$idx";
		$apv_div1_flg = "rtn_apv_div1_flg$idx";
		$apv_div4_flg = "rtn_apv_div4_flg$idx";
	
		$target_class_div = "rtn_target_class_div$idx";
		$multi_apv_flg = "rtn_multi_apv_flg$idx";
		$next_notice_div = "rtn_next_notice_div$idx";
	
		$apv_num = "rtn_apv_num$idx";
	
		array_push($rtn_approve_mng, array(
	                                    "apv_div0_flg" => ($$apv_div0_flg == "") ? "f" : $$apv_div0_flg,
	                                    "apv_div1_flg" => ($$apv_div1_flg == "") ? "f" : $$apv_div1_flg,
	                                    "apv_div4_flg" => ($$apv_div4_flg == "") ? "f" : $$apv_div4_flg,
	                                    "deci_flg" => "t",
	                                    "target_class_div" => $$target_class_div,
	                                    "multi_apv_flg" => $$multi_apv_flg,
	                                    "next_notice_div" => ($$next_notice_div == "") ? null : $$next_notice_div,
	                                    "apv_num" => ($$apv_num == "") ? null : $$apv_num,
	                                    "wkfw_div" => "3"
	                                   ));
	
		$approve = "rtn_approve$idx";
		if($$approve == "") {
			$chk_apv_exist_flg = false;	 	
		}	
	}
	
	if(!$chk_apv_exist_flg) {
		echo("<script type=\"text/javascript\">alert('{$ret_str}申請の承認階層を設定してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
	
	if($rtn_wkfw_approve_num == 0) {
		echo("<script type=\"text/javascript\">alert('{$ret_str}申請の確定を選択してください。');</script>");
		echo("<script type=\"text/javascript\">document.items.submit();</script>");
		exit;
	}
}


// トランザクションを開始
pg_query($con, "begin transaction");

// 承認者情報を削除
$sql = "delete from tmcdaprv";
$cond = "";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 承認者情報を登録
foreach ($emp_ids as $emp_id) {
	$sql = "insert into tmcdaprv (emp_id) values (";
	$content = array($emp_id);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

$obj = new atdbk_workflow_common_class($con, $fname);


//ワークフロー情報設定
$arr_atdbk_wkfw = $obj->get_atdbk_wkfw();

$ovtm_wkfw_div = ($ovtm_wkfw_div == "") ? "0" : $ovtm_wkfw_div;
$time_wkfw_div = ($time_wkfw_div == "") ? "0" : $time_wkfw_div;
$rtn_wkfw_div = ($rtn_wkfw_div == "") ? "0" : $rtn_wkfw_div;

if(count($arr_atdbk_wkfw) == 0)
{
    $obj->regist_atdbk_wkfw($ovtm_wkfw_div, $time_wkfw_div, $rtn_wkfw_div);
}
else
{
    $arr = array("ovtm_wkfw_div" => $ovtm_wkfw_div, "time_wkfw_div" => $time_wkfw_div, "rtn_wkfw_div" => $rtn_wkfw_div);
    $obj->update_atdbk_wkfw($arr);
}



// 残業申請ワークフロー設定
$ovtm_wkfw_approve_num = ($ovtm_wkfw_approve_num == "") ? NULL : $ovtm_wkfw_approve_num;
$arr = array("ovtmcfm" => $ovtm_wkfw_approve_num);
$obj->update_atdbk_wkfw($arr);

// ワークフロー・承認者管理情報を削除
$obj->delete_atdbk_apvmng("1");

// 部署役職(申請者所属)登録情報を削除
$obj->delete_atdbk_pvpstdtl("1");

// 職員登録情報を削除
$obj->delete_atdbk_apvdtl("1");

// 部署役職(部署指定)登録情報を削除
$obj->delete_atdbk_apvsectdtl("1");

// ワークフロー・承認者管理情報登録
$apv_order = 1;
foreach($ovtm_approve_mng as $mng)
{
	$obj->regist_atdbk_apvmng($apv_order, $mng);
	$apv_order++;
}

// 部署役職(申請者所属)、職員、部署役職(部署指定)登録
for ($i = 0; $i < $ovtm_approve_num; $i++)
{

	$idx = $i + 1;
	$apv_div0_flg = "ovtm_apv_div0_flg$idx";
	$apv_div1_flg = "ovtm_apv_div1_flg$idx";
	$apv_div4_flg = "ovtm_apv_div4_flg$idx";

	// 部署役職(申請者所属)登録
	if($$apv_div0_flg == "t")
	{
		$st_id = "ovtm_st_id$idx";
		$arr_st_id = split(",", $$st_id);
		for($j=0; $j<count($arr_st_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_id[$j], "0", "1");
		}
	}
	// 職員登録
	if($$apv_div1_flg == "t")
	{
		$emp_id = "ovtm_emp_id$idx";
		$arr_emp_id = split(",", $$emp_id);
		for($j=0; $j<count($arr_emp_id); $j++)
		{
			$sub_order = $j + 1;
			if(count($arr_emp_id) == 1)
			{
				$sub_order = null;
			}
			$obj->regist_atdbk_apvdtl($idx, $arr_emp_id[$j], $sub_order, "1");
		}
	}
	// 部署役職(部署指定)登録
	if($$apv_div4_flg == "t")
	{
		// 部署
		$class_sect_id     = "ovtm_class_sect_id$idx";
		$atrb_sect_id      = "ovtm_atrb_sect_id$idx";
		$dept_sect_id      = "ovtm_dept_sect_id$idx";
		$room_sect_id      = "ovtm_room_sect_id$idx";

		$obj->regist_atdbk_apvsectdtl($idx, $$class_sect_id, $$atrb_sect_id, $$dept_sect_id, $$room_sect_id, "1");

		// 役職
		$st_sect_id = "ovtm_st_sect_id$idx";
		$arr_st_sect_id = split(",", $$st_sect_id);
		for($j=0; $j<count($arr_st_sect_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_sect_id[$j], "1", "1");
		}
	}
}



// 時間修正申請ワークフロー設定
$time_wkfw_approve_num = ($time_wkfw_approve_num == "") ? NULL : $time_wkfw_approve_num;
$arr = array("timecfm" => $time_wkfw_approve_num);
$obj->update_atdbk_wkfw($arr);

// ワークフロー・承認者管理情報を削除
$obj->delete_atdbk_apvmng("2");

// 部署役職(申請者所属)登録情報を削除
$obj->delete_atdbk_pvpstdtl("2");

// 職員登録情報を削除
$obj->delete_atdbk_apvdtl("2");

// 部署役職(部署指定)登録情報を削除
$obj->delete_atdbk_apvsectdtl("2");

// ワークフロー・承認者管理情報登録
$apv_order = 1;
foreach($time_approve_mng as $mng)
{
	$obj->regist_atdbk_apvmng($apv_order, $mng);
	$apv_order++;
}

// 部署役職(申請者所属)、職員、部署役職(部署指定)登録
for ($i = 0; $i < $time_approve_num; $i++)
{

	$idx = $i + 1;
	$apv_div0_flg = "time_apv_div0_flg$idx";
	$apv_div1_flg = "time_apv_div1_flg$idx";
	$apv_div4_flg = "time_apv_div4_flg$idx";

	// 部署役職(申請者所属)登録
	if($$apv_div0_flg == "t")
	{
		$st_id = "time_st_id$idx";
		$arr_st_id = split(",", $$st_id);
		for($j=0; $j<count($arr_st_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_id[$j], "0", "2");
		}
	}
	// 職員登録
	if($$apv_div1_flg == "t")
	{
		$emp_id = "time_emp_id$idx";
		$arr_emp_id = split(",", $$emp_id);
		for($j=0; $j<count($arr_emp_id); $j++)
		{
			$sub_order = $j + 1;
			if(count($arr_emp_id) == 1)
			{
				$sub_order = null;
			}
			$obj->regist_atdbk_apvdtl($idx, $arr_emp_id[$j], $sub_order, "2");
		}
	}
	// 部署役職(部署指定)登録
	if($$apv_div4_flg == "t")
	{
		// 部署
		$class_sect_id     = "time_class_sect_id$idx";
		$atrb_sect_id      = "time_atrb_sect_id$idx";
		$dept_sect_id      = "time_dept_sect_id$idx";
		$room_sect_id      = "time_room_sect_id$idx";

		$obj->regist_atdbk_apvsectdtl($idx, $$class_sect_id, $$atrb_sect_id, $$dept_sect_id, $$room_sect_id, "2");

		// 役職
		$st_sect_id = "time_st_sect_id$idx";
		$arr_st_sect_id = split(",", $$st_sect_id);
		for($j=0; $j<count($arr_st_sect_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_sect_id[$j], "1", "2");
		}
	}
}



// 退勤後復帰申請ワークフロー設定
$rtn_wkfw_approve_num  = ($rtn_wkfw_approve_num == "") ? NULL : $rtn_wkfw_approve_num;
$arr = array("rtncfm" => $rtn_wkfw_approve_num);
$obj->update_atdbk_wkfw($arr);

// ワークフロー・承認者管理情報を削除
$obj->delete_atdbk_apvmng("3");

// 部署役職(申請者所属)登録情報を削除
$obj->delete_atdbk_pvpstdtl("3");

// 職員登録情報を削除
$obj->delete_atdbk_apvdtl("3");

// 部署役職(部署指定)登録情報を削除
$obj->delete_atdbk_apvsectdtl("3");

// ワークフロー・承認者管理情報登録
$apv_order = 1;
foreach($rtn_approve_mng as $mng)
{
	$obj->regist_atdbk_apvmng($apv_order, $mng);
	$apv_order++;
}

// 部署役職(申請者所属)、職員、部署役職(部署指定)登録
for ($i = 0; $i < $rtn_approve_num; $i++)
{

	$idx = $i + 1;
	$apv_div0_flg = "rtn_apv_div0_flg$idx";
	$apv_div1_flg = "rtn_apv_div1_flg$idx";
	$apv_div4_flg = "rtn_apv_div4_flg$idx";

	// 部署役職(申請者所属)登録
	if($$apv_div0_flg == "t")
	{
		$st_id = "rtn_st_id$idx";
		$arr_st_id = split(",", $$st_id);
		for($j=0; $j<count($arr_st_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_id[$j], "0", "3");
		}
	}
	// 職員登録
	if($$apv_div1_flg == "t")
	{
		$emp_id = "rtn_emp_id$idx";
		$arr_emp_id = split(",", $$emp_id);
		for($j=0; $j<count($arr_emp_id); $j++)
		{
			$sub_order = $j + 1;
			if(count($arr_emp_id) == 1)
			{
				$sub_order = null;
			}
			$obj->regist_atdbk_apvdtl($idx, $arr_emp_id[$j], $sub_order, "3");
		}
	}
	// 部署役職(部署指定)登録
	if($$apv_div4_flg == "t")
	{
		// 部署
		$class_sect_id     = "rtn_class_sect_id$idx";
		$atrb_sect_id      = "rtn_atrb_sect_id$idx";
		$dept_sect_id      = "rtn_dept_sect_id$idx";
		$room_sect_id      = "rtn_room_sect_id$idx";

		$obj->regist_atdbk_apvsectdtl($idx, $$class_sect_id, $$atrb_sect_id, $$dept_sect_id, $$room_sect_id, "3");

		// 役職
		$st_sect_id = "rtn_st_sect_id$idx";
		$arr_st_sect_id = split(",", $$st_sect_id);
		for($j=0; $j<count($arr_st_sect_id); $j++)
		{
            $obj->regist_atdbk_apvpstdtl($idx, $arr_st_sect_id[$j], "1", "3");
		}
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// system_config
$conf = new Cmx_SystemConfig();
// 承認階層開始判定
$conf->set('timecard.aprv_skip_flg', $aprv_skip_flg);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_approval.php?session=$session'</script>");
?>
</body>
