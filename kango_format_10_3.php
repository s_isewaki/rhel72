<?php
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("show_select_values.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$bldg_cd = isset($_REQUEST["bldg_cd"]) ? $_REQUEST["bldg_cd"] : null;
$ward_cd = isset($_REQUEST["ward_cd"]) ? $_REQUEST["ward_cd"] : null;
$standard_cd = isset($_REQUEST["standard_cd"]) ? $_REQUEST["standard_cd"] : 1;
$fiscal_year = isset($_REQUEST["fiscal_year"]) ? $_REQUEST["fiscal_year"] : null;
$target_date_y = isset($_REQUEST["target_date_y"]) ? $_REQUEST["target_date_y"] : null;
$target_date_m = isset($_REQUEST["target_date_m"]) ? $_REQUEST["target_date_m"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
    if($postback_mode == "save_year_format")
    {
        //==============================
        //トランザクション開始
        //==============================
        pg_query($con,"begin transaction");

        //==============================
        //更新：デリート
        //==============================
        $sql = "delete from nlcs_format_10_3 where yyyy='{$target_date_y}' and mm='{$target_date_m}' and standard_code='{$standard_cd}'";
        $result = pg_exec_with_log($con, $sql, $fname);
        if ($result == 0)
        {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //==============================
        //更新：インサート
        //==============================
        $sql  = "insert into nlcs_format_10_3(fiscal_year,yyyy,mm,bed_count,pt_count,pt_7_1_count,standard_code) ";
        $sql .= "values('{$fiscal_year}','{$target_date_y}','{$target_date_m}',{$bed_count},{$pt_count2},{$pt_7_1_count},{$standard_cd})";
        $result = pg_exec_with_log($con, $sql, $fname);
        if ($result == 0)
        {
            pg_query($con,"rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        //==============================
        //コミット
        //==============================
        pg_query($con, "commit");

        //==============================
        //画面遷移
        //==============================
        ?>
        <script type="text/javascript">
        var url = "kango_format_10_3_year.php?session=<?=$session?>&fiscal_year=<?=$fiscal_year?>&standard_cd=<?=$standard_cd?>";
        location.href = url;
        </script>
        <?
        exit;
    }
}

// 除外診療科
$out_sect_id_list = get_out_sect_id_list($con, $fname);

// 一般病床
$general_ward_divs = get_general_ward_divs($con, $fname);

//==============================
//選択された病棟の情報
//==============================
if (!empty($bldg_cd) and !empty($ward_cd)) {
    $sql = "select * from nlcs_ward_setting where bldg_cd={$bldg_cd} and ward_cd={$ward_cd}";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $nlcs_ward = pg_fetch_assoc($sel);
    $standard_cd = $nlcs_ward["standard_code"] ? $nlcs_ward["standard_code"] : 1;
}
$standard_name = get_standard_name($con,$fname,$standard_cd);



//==============================
//７対１,10対１入院基準の病棟を持つ棟が１つか判定
//==============================
$only_bldg_cd = get_only_bldg_cd_for_format_10_3($con,$fname);

if($is_postback != "true")
{
    $bldg_cd = $only_bldg_cd;
}

$year_action_flg = true;
if($only_bldg_cd == "" && $bldg_cd != "")
{
    $year_action_flg = false;
}
if($only_bldg_cd != "" && $ward_cd != "")
{
    $year_action_flg = false;
}


//==============================
//病床数
//==============================

//各病室のベッド数に対し、７対１,10対１基準病棟以外を取り除き、集計。棟・病棟選択による絞り込みも。
$sql  = " select sum(ptrm_bed_cur) as bed_count from";
$sql .= " (";
$sql .=       get_bldg_ward_room_sql();
$sql .= " ) bldg_ward_room_sql";
$sql .= " where not exists";
$sql .= " (";
$sql .= " select * from wdmst left join nlcs_ward_setting wsetting using (bldg_cd,ward_cd) where COALESCE(standard_code,1) <> {$standard_cd}";
$sql .= " and wdmst.bldg_cd = bldg_ward_room_sql.bldg_cd";
$sql .= " and wdmst.ward_cd = bldg_ward_room_sql.ward_cd";
$sql .= " )";
if($bldg_cd != "")
{
    $sql .= " and bldg_ward_room_sql.bldg_cd = $bldg_cd";
}
if($ward_cd != "")
{
    $sql .= " and bldg_ward_room_sql.ward_cd = $ward_cd";
}
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$bed_count = intval(pg_fetch_result($sel,0,'bed_count'));


//==============================
//７対１,10対１入院基準以外の病棟
//==============================
$sql = "select bldg_cd,ward_cd from wdmst left join nlcs_ward_setting wsetting using (bldg_cd,ward_cd) where COALESCE(standard_code,1) <> {$standard_cd}";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$non_7_1_ward = pg_fetch_all_in_array($sel);


//==============================
//日付
//==============================

//初期表示の場合　※他画面からセットされる場合あり
if($target_date_y == "")
{
    $target_date_y = date("Y");
}
if($target_date_m == "")
{
    $target_date_m = date("m");
}


$target_date_dumy_min = $target_date_y.$target_date_m."00";
$target_date_dumy_max = $target_date_y.$target_date_m."99";



$fiscal_year = get_fiscal_year($target_date_y.$target_date_m);


//==============================
//入棟情報
//==============================
//表示する期間の患者の入棟情報(７対１,10対１入院基準の病棟のみ)。棟・病棟選択による絞り込みも。
$all_inpt_info =  get_all_inpt_info($con, $fname, $target_date_dumy_min, $target_date_dumy_max, "", false);
$wk_data = array();
foreach($all_inpt_info as $all_inpt_info1)
{
    $is_ok = true;
    if($bldg_cd != "" && $all_inpt_info1['bldg_cd'] != $bldg_cd)
    {
        $is_ok = false;
    }
    if($ward_cd != "" && $all_inpt_info1['ward_cd'] != $ward_cd)
    {
        $is_ok = false;
    }
    if($is_ok)
    {
        foreach($non_7_1_ward as $non_7_1_ward1)
        {
            if($non_7_1_ward1['bldg_cd'] == $all_inpt_info1['bldg_cd'] && $non_7_1_ward1['ward_cd'] == $all_inpt_info1['ward_cd'])
            {
                $is_ok = false;
                break;
            }
        }
    }
    if($is_ok)
    {
        $wk_data[] = $all_inpt_info1;
    }
}
$month_all_inpt_info = $wk_data;




//==============================
//評価記録情報
//==============================
//表示する期間の評価記録情報(日勤のデータのみ、７対１,10対１入院基準の病棟のみ)
$sql  = " select * from nlcs_eval_record where input_date >= '{$target_date_dumy_min}' and input_date <= '{$target_date_dumy_max}' and input_time = 2 and pt_7_1_flg = 't' and not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
    pg_query($con,"rollback");
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$month_eval_record_info = pg_fetch_all_in_array($sel);






//==============================
//表示データ作成
//==============================
$data_all = null;
$data_all['pt_count1'] = 0;
$data_all['pt_count2'] = 0;
$data_all['pt_7_1_count'] = 0;

$date_list = null;
$days = 0;
for ($i = 1; $i<=31;$i++) {
    $tmp_datetime = mktime(0, 0, 0, $target_date_m, $i, $target_date_y);
    if(date("m",$tmp_datetime) != $target_date_m)
    {
        break;
    }

    $tmp_date = date("Ymd",$tmp_datetime);

    //その日の入棟患者情報の作成、入院患者数IIの算出
    $wk_data = array();
    $pt_count2 = 0;
    foreach ($month_all_inpt_info as $month_all_inpt_info1) {
        if ($tmp_date >= $month_all_inpt_info1['inpt_in_dt'] && $tmp_date <= $month_all_inpt_info1['inpt_out_dt']) {
            $wk_data[] = $month_all_inpt_info1;

            // 病棟移動ならカウントしない
            if ($month_all_inpt_info1["inptmove_flg"]) {
                continue;
            }

            // 当日退院であればカウントしない
            if ($month_all_inpt_info1["inpt_out_dt"] == $tmp_date && $month_all_inpt_info1["inpt_in_dt"] != $tmp_date && $month_all_inpt_info1["ref_table_name"] != "inptmove") {
                continue;
            }

            // 一般病床でなければカウントしない
            if (!in_array($month_all_inpt_info1["ward_type"], $general_ward_divs)) {
                continue;
            }

            // 7対1, 10対1
            if ($standard_cd != 7) {

                // 除外診療科であればカウントしない
                if (in_array($month_all_inpt_info1["inpt_sect_id"], $out_sect_id_list)) {
                    continue;
                }

                // 15歳以上でなければカウントしない
                $age = floor(($tmp_date - $month_all_inpt_info1['birth']) / 10000);
                if ($age < 15) {
                    continue;
                }
            }

            $pt_count2++;
        }
    }
    $date_all_inpt_info = $wk_data;

    //その日の７対１入院基準を満たす評価票記録
    $wk_data = array();
    $duplicate = array();
    foreach($month_eval_record_info as $month_eval_record_info1)
    {
        if($tmp_date == $month_eval_record_info1['input_date'])
        {
            //入棟患者に存在する場合のみ
            if(!$duplicate[$month_eval_record_info1['pt_id']][$month_eval_record_info1['input_date']] and get_row_data_pg_fetch_all($date_all_inpt_info,'ptif_id',$month_eval_record_info1['pt_id']) != null)
            {
                $wk_data[] = $month_eval_record_info1;
                $duplicate[$month_eval_record_info1['pt_id']][$month_eval_record_info1['input_date']] = 1;
            }
        }
    }
    $date_7_1_eval_record_info = $wk_data;


    $line = array();
    $line['date'] = substr($tmp_date,4,2)."/".substr($tmp_date,6,2);
    $line['pt_count1'] = count($date_all_inpt_info);
    $line['pt_count2'] = $pt_count2;
    $line['pt_7_1_count'] = count($date_7_1_eval_record_info);

    if($line['pt_count2'] == 0)
    {
        $rate_7_1 = 0;
    }
    else
    {
        $rate_7_1 = 100 * $line['pt_7_1_count'] / $line['pt_count2'];
    }
    $rate_7_1 = round( $rate_7_1,1);//小数点１桁で四捨五入
    $rate_7_1 = $rate_7_1."%";
    $line['pt_7_1_rate'] = $rate_7_1;

    $date_list[] = $line;

    $data_all['pt_count1'] += $line['pt_count1'];
    $data_all['pt_count2'] += $line['pt_count2'];
    $data_all['pt_7_1_count'] += $line['pt_7_1_count'];
    $days++;
}

$data_avg = array(
    'pt_count1' => sprintf("%.1f", round($data_all['pt_count1'] / $days,1)),
    'pt_count2' => sprintf("%.1f", round($data_all['pt_count2'] / $days,1)),
    'pt_7_1_count' => sprintf("%.1f", round($data_all['pt_7_1_count'] / $days,1)),
);


if($data_all['pt_count2'] == 0)
{
    $rate_7_1 = 0;
}
else
{
    $rate_7_1 = 100 * $data_all['pt_7_1_count'] / $data_all['pt_count2'];
}
$rate_7_1 = round( $rate_7_1,1);//小数点１桁で四捨五入
$rate_7_1 = $rate_7_1."%";
$data_all['pt_7_1_rate'] = $rate_7_1;




//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "様式10の3";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//画面起動時の処理
function initPage()
{
    init_ward_select();
}

//病棟変更時の処理
function ward_changed(bldg_cd,ward_cd)
{
    document.mainform.postback_mode.value = "ward_changed";
    document.mainform.submit();
}

function date_changed()
{
    document.mainform.postback_mode.value = "date_changed";
    document.mainform.submit();
}

function show_year_format()
{
    var url = "kango_format_10_3_year.php?session=<?=$session?>&fiscal_year=<?=$fiscal_year?>&standard_cd=<?=$standard_cd?>";
    location.href = url;
}


function save_year_format()
{
    document.mainform.postback_mode.value = "save_year_format";
    document.mainform.submit();
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname);
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="fiscal_year" value="<?=$fiscal_year?>">
<input type="hidden" name="bed_count" value="<?=$bed_count?>">
<input type="hidden" name="pt_count2" value="<?=$data_all['pt_count2']?>">
<input type="hidden" name="pt_7_1_count" value="<?=$data_all['pt_7_1_count']?>">


<!-- レイアウト START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 表示条件 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<td width="250">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

<!-- 日付選択 START -->
月指定：<select id='target_date_y' name='target_date_y' onchange="date_changed()">
<?
$date_y_min = 2008;
$date_y_max = date("Y");
for($tmp_y = $date_y_min; $tmp_y <= $date_y_max; $tmp_y++)
{
    ?>
    <option value="<?=$tmp_y?>" <?if($tmp_y == $target_date_y){echo('selected');}?>><?=$tmp_y?></option>
    <?
}
?>
</select>年
<select id='target_date_m' name='target_date_m' onchange="date_changed()">
<?
for ($i = 1; $i <= 12; $i++)
{
    if($target_date_y == "2008" && $i<=3)
    {
        continue;
    }
    if($target_date_y == date("Y") && $i>date("m"))
    {
        continue;
    }
    $tmp_m = sprintf("%02d", $i);
    ?>
    <option value="<?=$tmp_m?>" <?if($tmp_m == $target_date_m){echo('selected');}?>><?=$tmp_m?></option>
    <?
}
?>
</select>月
<!-- 日付選択 END -->


</font>
</td>



<td align="left">

<!-- 棟/病棟選択 START -->
<?
show_ward_select_for_format_10_3($con,$fname,$bldg_cd,$ward_cd,$standard_cd);
?>
<!-- 棟/病棟選択 END -->

</td>






<td>
&nbsp;
</td>


<?
if($year_action_flg)
{
?>
<td align="right" nowrap>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="button" value="報告書表示" onclick="show_year_format()">
<input type="button" value="月合計反映" onclick="save_year_format()">
</font>
</td>
<?
}
?>


</tr>
</table>
<!-- 表示条件 END -->




</td>
</tr>

<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>

<tr>
<td>





<!-- 一覧 START -->
<table width="100%" border='0' cellspacing='0' cellpadding='1' class="list">

<tr>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="140"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>届出<br>入院料</font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="100"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>届出<br>病床数</font></td>
<td bgcolor='#bdd1e7' colspan="4" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院患者の状況</font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日付</font></td>
</tr>

<tr>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院患者数I<br>(注1)</font></td>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>入院患者数II<br>(注2)</font></td>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$standard_name?>条件患者数</font></td>
<td bgcolor='#bdd1e7' align="center" width="130"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>比率</font></td>
</tr>

<?
foreach($date_list as $date_list1)
{
    ?>
    <tr>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$standard_name?>入院基本料</font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$bed_count?>床</font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_list1['pt_count1']?></font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_list1['pt_count2']?></font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_list1['pt_7_1_count']?></font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_list1['pt_7_1_rate']?></font></td>
    <td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_list1['date']?></font></td>
    </tr>
    <?
}
?>

<tr>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td style="padding:0px"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>

<tr>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$standard_name?>入院基本料</font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$bed_count?>床</font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_all['pt_count1']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_all['pt_count2']?></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_all['pt_7_1_count']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_all['pt_7_1_rate']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>月合計</font></td>
</tr>

<tr>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>-</td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>-</td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_avg['pt_count1']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_avg['pt_count2']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$data_avg['pt_7_1_count']?></font></td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>-</td>
<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>月平均</font></td>
</tr>



</table>
<!-- 一覧 END -->


<ol style="margin-left:0;padding-left:0;list-style-type:none;">
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注1）全病棟区分、全診療科、15歳未満、当日退院分を含む人数です。</font></li>
<? if ($standard_cd != 7) {?>
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注2）一般病床に入院中の患者で、産科と15歳未満の小児および当日退院患者を除外した人数です。一般病床と産科の設定は、管理画面から行ってください。</font></li>
<?} else {?>
<li style="margin-left:0;padding-left:0;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">注2）一般病床に入院中の患者で、当日退院患者を除外した人数です。一般病床の設定は、管理画面から行ってください。</font></li>
<?}?>
</ol>


</td>
</tr>
</table>
<!-- レイアウト END -->



</form>
<!-- 全体 END -->

</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);



//========================================================================================================================================================================================================
// Private関数
//========================================================================================================================================================================================================

/**
 * 棟が１件だけの場合にその棟コードを返します。
 * 棟が複数ある場合は空文字を返します。
 *
 * ７対１入院基準と10対１施設基準の病棟のみ対象とします。
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @return array 棟と病棟のコード
 */
function get_only_bldg_cd_for_format_10_3($con,$fname)
{
    $sql  = " select distinct bldg_cd from";
    $sql .= " (";
    $sql .= get_bldg_ward_room_sql();
    $sql .= " ) bldg_ward_room_sql";
    $sql .= " where not exists";
    $sql .= " (";
    $sql .= " select bldg_cd,ward_cd from nlcs_ward_setting wsetting where standard_code not in (1,2)";
    $sql .= " and wsetting.bldg_cd = bldg_ward_room_sql.bldg_cd";
    $sql .= " and wsetting.ward_cd = bldg_ward_room_sql.ward_cd";
    $sql .= " )";
    $sel = select_from_table($con, $sql, "", $fname);
    if ($sel == 0)
    {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $ret = "";
    if(pg_num_rows($sel) == 1)
    {
        $ret = pg_fetch_result($sel,0,"bldg_cd");
    }
    return $ret;
}



/**
 * 様式１０の３専用の病棟選択のプルダウンを表示します。
 * ・「すべて」あり。
 * ・７対１施設基準と10対１施設基準の病棟のみ選択可能。
 *
 * 画面起動時に以下のJavaScriptを実効してください。
 *      init_ward_select();
 * 病棟変更の通知を受ける場合は以下の関数を定義してください。
 *      ward_changed(bldg_cd,ward_cd)
 *
 *
 * @param object $con DBコネクション
 * @param string $fname 画面名
 * @param integer $bldg_cd 初期表示する棟コード(省略可能)
 * @param integer $ward_cd 初期表示する病棟コード(省略可能)
 * @param integer $standard_cd 初期表示する施設基準コード(省略可能)
 */
function show_ward_select_for_format_10_3($con,$fname,$bldg_cd="",$ward_cd="",$standard_cd="")
{
    //==============================
    //棟-病棟-病室情報(７対１施設基準と10対１施設基準の病棟のみ)
    //==============================
    $sql  = " select * from";
    $sql .= " (";
    $sql .= get_bldg_ward_room_sql();
    $sql .= " ) bldg_ward_room_sql";
    $sql .= " where not exists";
    $sql .= " (";
    $sql .= " select bldg_cd,ward_cd from nlcs_ward_setting wsetting where standard_code not in (1,2)";
    $sql .= " and wsetting.bldg_cd = bldg_ward_room_sql.bldg_cd";
    $sql .= " and wsetting.ward_cd = bldg_ward_room_sql.ward_cd";
    $sql .= " )";
    $sql .= " order by bldg_cd,ward_cd,ptrm_room_no";
    $sel = select_from_table($con, $sql, "", $fname);
    $bldg_ward_room_info = pg_fetch_all_in_array($sel);

    //==============================
    //棟一覧を取得
    //==============================
    $bldg_list = array();
    $bldg_list_cd = array();
    foreach($bldg_ward_room_info as $row)
    {
        if(!in_array($row['bldg_cd'],$bldg_list_cd))
        {
            $bldg_list1 = null;
            $bldg_list1['bldg_cd'] = $row['bldg_cd'];
            $bldg_list1['bldg_name'] = $row['bldg_name'];
            $bldg_list[] = $bldg_list1;
            $bldg_list_cd[] = $row['bldg_cd'];
        }
    }

    //==============================
    //病棟一覧を取得
    //==============================
    $ward_list = array();
    $ward_list_cd = array();
    foreach($bldg_ward_room_info as $row)
    {
        if(!in_array($row['bldg_cd'].'/'.$row['ward_cd'],$ward_list_cd))
        {
            $ward_list1 = null;
            $ward_list1['bldg_cd'] = $row['bldg_cd'];
            $ward_list1['ward_cd'] = $row['ward_cd'];
            $ward_list1['ward_name'] = $row['ward_name'];
            $ward_list[] = $ward_list1;
            $ward_list_cd[] = $row['bldg_cd'].'/'.$row['ward_cd'];
        }
    }

    //==============================
    //HTML/JavaScript出力
    //==============================
    ?>
    <!-- 病棟選択 START -->
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">

    事業所（棟）：<select id="bldg_cd" name="bldg_cd" onchange="resetWardOptions(true,'');">
    <option value="">すべて</option>
    <?
    foreach($bldg_list as $bldg_list1)
    {
        $bldg_selected = "";
        if ($bldg_list1['bldg_cd'] == $bldg_cd)
        {
            $bldg_selected = "selected";
        }
        ?>
        <option value="<?=$bldg_list1['bldg_cd']?>" <?=$bldg_selected?>><?=$bldg_list1['bldg_name']?></option>
        <?
    }
    ?>
    </select>

    病棟：<select id="ward_cd" name="ward_cd" onchange="notify_ward_changed()">
    </select>

    <? if (empty($ward_cd)) {?>
    施設基準：<select id="standard_cd" name="standard_cd" onchange="notify_ward_changed()">
    <option value="1" <? if ($standard_cd == 1) { echo "selected";}?>>7対1</option>
    <option value="2" <? if ($standard_cd == 2) { echo "selected";}?>>10対1</option>
    <option value="7" <? if ($standard_cd == 7) { echo "selected";}?>>地域包括ケア</option>
    </select>
    <?}?>

    </font>
    <!-- 病棟選択 END -->
    <!-- 病棟選択script START -->
    <script type="text/javascript">

    //病棟選択を初期化します。
    function init_ward_select()
    {
        resetWardOptions(false,'<?=$ward_cd?>');
    }


    //棟情報を元に病棟選択を再構築します。
    function resetWardOptions(need_notify_ward_changed, default_ward_cd)
    {
        //棟・病棟のコントロール
        var bldg_cd_obj = document.getElementById("bldg_cd");
        var ward_cd_obj = document.getElementById("ward_cd");

        //棟の値
        var bldg_cd = bldg_cd_obj.value;

        //病棟の項目を全削除
        deleteAllOptions_for_ward_select(ward_cd_obj);

        //「すべて」を追加
        addOption_for_ward_select(ward_cd_obj, '', 'すべて', default_ward_cd);

        //棟の値に応じて病棟の値をセット
        switch (bldg_cd)
        {
            <?
            $pre_bldg_cd = "";
            foreach($ward_list as $ward_list1)
            {
                $cur_bldg_cd = $ward_list1["bldg_cd"];

                if ($cur_bldg_cd != $pre_bldg_cd)
                {
                    if ($pre_bldg_cd != "")
                    {
                        ?>
                        break;
                        <?
                    }
                    ?>
                    case '<?=$cur_bldg_cd?>' :
                    <?
                    $pre_bldg_cd = $cur_bldg_cd;
                }

                ?>
                addOption_for_ward_select(ward_cd_obj, '<?=$ward_list1["ward_cd"]?>', '<?=$ward_list1["ward_name"]?>', default_ward_cd);
                <?
            }
            ?>
        }

        //病棟変更を通知
        if(need_notify_ward_changed)
        {
            notify_ward_changed();
        }
    }
    //病棟選択専用：プルダウン項目全削除
    function deleteAllOptions_for_ward_select(box)
    {
        for (var i = box.length - 1; i >= 0; i--)
        {
            box.options[i] = null;
        }
    }
    //病棟選択専用：プルダウン項目追加
    function addOption_for_ward_select(box, value, text, selected)
    {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = text;
        if (selected == value)
        {
            opt.selected = true;
        }
        box.options[box.length] = opt;
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
    }

    //病棟の変更をward_changed()に通知します。
    function notify_ward_changed()
    {
        var bldg_cd = document.getElementById("bldg_cd").value;
        var ward_cd = document.getElementById("ward_cd").value;

        if(window.ward_changed)
        {
            ward_changed(bldg_cd,ward_cd);
        }
    }

    </script>
    <!-- 病棟選択script END -->
    <?
}
