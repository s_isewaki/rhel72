<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="jnl_application_apply_list.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="category" value="<?echo($category)?>">
<input type="hidden" name="workflow" value="<?echo($workflow)?>">
<input type="hidden" name="apply_title" value="<?echo($apply_title)?>">
<input type="hidden" name="approve_emp_nm" value="<?echo($approve_emp_nm)?>">
<input type="hidden" name="date_y1" value="<?echo($date_y1)?>">
<input type="hidden" name="date_m1" value="<?echo($date_m1)?>">
<input type="hidden" name="date_d1" value="<?echo($date_d1)?>">
<input type="hidden" name="date_y2" value="<?echo($date_y2)?>">
<input type="hidden" name="date_m2" value="<?echo($date_m2)?>">
<input type="hidden" name="date_d2" value="<?echo($date_d2)?>">
<input type="hidden" name="apply_stat" value="<?echo($apply_stat)?>">
</form>


<?
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("jnl_application_workflow_common_class.php");


$fname=$PHP_SELF;


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 80, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}


// データベースに接続
$con = connect2db($fname);

$obj = new jnl_application_workflow_common_class($con, $fname);

// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];

// トランザクションを開始
pg_query($con, "begin");

$arr_next_appry_id = array();
$must_send_mail_applys = array();
for ($i = 0, $count = count($re_apply_chk); $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];

		// 再申請チェック
		$arr_apply_wkfwmst = $obj->get_apply_wkfwmst($apply_id);
		$apply_stat = $arr_apply_wkfwmst[0]["apply_stat"];
//		if($apply_stat != "3") {
//			echo("<script type=\"text/javascript\">alert(\"申請状況が変更されたため、再申請できません。\");</script>");
//			echo("<script type=\"text/javascript\">history.back();</script>");
//			exit;
//		}

		// 新規申請(apply_id)採番
		$sql = "select max(apply_id) from jnl_apply";
		$cond = "";
		$apply_max_sel = select_from_table($con,$sql,$cond,$fname);
		$max = pg_result($apply_max_sel,0,"max");
		if($max == ""){
			$new_apply_id = "1";
		}else{
			$new_apply_id = $max + 1;
		}

		//集計処理用データの更新処理を行う↓
		
		//集計用データのステータスを指定された値にに更新する////////////////////////////////////////////////
		//第一にどの病院、施設のデータか発見する
		$selected_table = $obj->get_update_report_table($apply_id);
		
		if($selected_table == "jnl_return_main")
		{
			//返戻の場合はサブテーブルも更新する
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			//対象テーブルのデータを更新
			$sql = "update $selected_table set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id=$apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			
			//対象テーブルのデータを更新
			$sql = "update jnl_return_total set ";
			$set = array("newest_apply_id");
			$setvalue = array($new_apply_id);
			
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			
			//対象テーブルのデータを更新
			$sql = "update jnl_return_factor set ";
			
			for($upd_cnt2=1; $upd_cnt2<=13;$upd_cnt2++) 
			{
				for($upd_cnt1=1; $upd_cnt1<=2;$upd_cnt1++) 
				{
					
					$cond = "where newest_apply_id=$apply_id and kind_factor=$upd_cnt2 and kind_consult=$upd_cnt1";
					
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) 
					{
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_return_assessment set ";
			
			for($upd_cnt2=1; $upd_cnt2<=11;$upd_cnt2++) 
			{
				for($upd_cnt1=1; $upd_cnt1<=2;$upd_cnt1++) 
				{
					
					$cond = "where newest_apply_id=$apply_id and kind_factor=$upd_cnt2 and kind_consult=$upd_cnt1";
					
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) 
					{
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_return_re_charge set ";
			
			for($upd_cnt2=1; $upd_cnt2<=2;$upd_cnt2++) 
			{
				for($upd_cnt1=1; $upd_cnt1<=2;$upd_cnt1++) 
				{
					
					$cond = "where newest_apply_id=$apply_id and kind_factor=$upd_cnt2 and kind_consult=$upd_cnt1";
					
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) 
					{
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			
			
			
			//対象テーブルのデータを更新
			$sql = "update jnl_return_comment set ";
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($selected_table == "jnl_nurse_home_main")
		{
			
			//老健の場合はサブテーブルも更新する
			


			//老健ヘッダテーブルからステータスを取得し、1であったら「老健日報」であり、それ以外は「老健月報」↓
			$sql   = "select jnl_report_flg ";
			$sql  .= "from jnl_nurse_home_main ";
			$cond = "where newest_apply_id = $apply_id ";
			
			$sel = select_from_table($con, $sql, $cond, $this->file_name);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			$report_status = pg_fetch_result($sel, 0, "jnl_report_flg");
			

			//mainテーブルを更新する
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			//対象テーブルのデータを更新
			$sql = "update $selected_table set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id=$apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			

			if($report_status == 1)
			{
				//老健日報の場合はサブテーブルも更新する
				
				$upd_table_list = array();
				$upd_table_list[0] = "jnl_nurse_home_day_cnt";
				$upd_table_list[1] = "jnl_nurse_home_day_enter";
				$upd_table_list[2] = "jnl_nurse_home_day_visit";
				$upd_table_list[3] = "jnl_nurse_home_day_visit_prevention";
				$upd_table_list[4] = "jnl_nurse_home_day_short";
				$upd_table_list[5] = "jnl_nurse_home_day_short_prevention";
				$upd_table_list[6] = "jnl_nurse_home_day_rehab";
				$upd_table_list[7] = "jnl_nurse_home_day_rehab_prevention";
				
				for($wcnt1=0;$wcnt1<=7;$wcnt1++) 
				{
					
					//対象テーブルのデータを更新
					$sql = "update ".$upd_table_list[$wcnt1]." set ";
					$set = array("newest_apply_id");
					$setvalue = array($new_apply_id);
					$cond = "where newest_apply_id = $apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) 
					{
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
			}
			else
			{
				//老健月報の場合はサブテーブルも更新する
				
				$upd_table_list = array();
				$upd_table_list[0] = "jnl_nurse_home_mon_enter";
				$upd_table_list[1] = "jnl_nurse_home_mon_short";
				$upd_table_list[2] = "jnl_nurse_home_mon_commute";
				$upd_table_list[3] = "jnl_nurse_home_mon_commute_pre";
				$upd_table_list[4] = "jnl_nurse_home_mon_visit";
				$upd_table_list[5] = "jnl_nurse_home_mon_visit_pre";
				
				for($wcnt1=0;$wcnt1<=5;$wcnt1++) 
				{
					
					//対象テーブルのデータを論理削除
					$sql = "update ".$upd_table_list[$wcnt1]." set ";
					$set = array("newest_apply_id");
					$setvalue = array($new_apply_id);
					$cond = "where newest_apply_id = $apply_id";
					$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
					if ($upd == 0) 
					{
						pg_query($con, "rollback");
						pg_close($con);
						echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
						echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
						exit;
					}
				}
				
			}
			
			
			
		}
		elseif($selected_table == "jnl_reward_synthesis_medi_main")
		{
			//診療報酬総括表（医科）の場合はサブテーブルも更新する
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_reward_synthesis_medi_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_reward_synthesis_medi set ";
			$set = array("newest_apply_id");
			$setvalue = array($new_apply_id);
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($selected_table == "jnl_reward_synthesis_dental_main")
		{
			//診療報酬総括表（歯科）の場合はサブテーブルも更新する
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_reward_synthesis_dental_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_reward_synthesis_dental set ";
			$set = array("newest_apply_id");
			$setvalue = array($new_apply_id);
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($selected_table == "jnl_bill_diagnosis_enter_main")
		{
			//入院診療行為別明細の場合はサブテーブルも更新する
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_diagnosis_enter_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_diagnosis_enter set ";
			$set = array("newest_apply_id");
			$setvalue = array($new_apply_id);
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($selected_table == "jnl_bill_diagnosis_visit_main")
		{
			//外来診療行為別明細の場合はサブテーブルも更新する
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_diagnosis_visit_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_diagnosis_visit set ";
			$set = array("newest_apply_id");
			$setvalue = array($new_apply_id);
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		elseif($selected_table == "jnl_bill_recuperation_main")
		{
			//療養費明細書の場合はサブテーブルも更新する
			

			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_recuperation_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			$upd_table_list = array();
			$upd_table_list[0] = "jnl_bill_recuperation_insure_rehab";
			$upd_table_list[1] = "jnl_bill_recuperation_service";
			$upd_table_list[2] = "jnl_bill_recuperation_own_payment";
			$upd_table_list[3] = "jnl_bill_recuperation_etc";
			
			for($wcnt1=0;$wcnt1<=3;$wcnt1++) 
			{

				//対象テーブルのデータを更新
				$sql = "update ".$upd_table_list[$wcnt1]." set ";
				$set = array("newest_apply_id");
				$setvalue = array($new_apply_id);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0) 
				{
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
			
		}
		elseif($selected_table == "jnl_bill_recuperation_prepay_main")
		{
			//予防給付療養費明細書の場合はサブテーブルも更新する
			
			
			$setvalue = array();
			$setvalue[0] = $new_apply_id;
			$setvalue[1] = '0';
			
			//対象テーブルのデータを更新
			$sql = "update jnl_bill_recuperation_prepay_main set ";
			$set =	array("newest_apply_id");
			array_push($set,"jnl_status");
			$cond = "where newest_apply_id = $apply_id";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
			if ($upd == 0) 
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			$upd_table_list = array();
			$upd_table_list[0] = "jnl_bill_recuperation_insure_rehab_prepay";
			$upd_table_list[1] = "jnl_bill_recuperation_service_prepay";
			$upd_table_list[2] = "jnl_bill_recuperation_own_payment_prepay";
			$upd_table_list[3] = "jnl_bill_recuperation_etc_prepay";
			
			for($wcnt1=0;$wcnt1<=3;$wcnt1++) 
			{
				
				//対象テーブルのデータを更新
				$sql = "update ".$upd_table_list[$wcnt1]." set ";
				$set = array("newest_apply_id");
				$setvalue = array($new_apply_id);
				$cond = "where newest_apply_id = $apply_id";
				$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $this->file_name);
				if ($upd == 0) 
				{
					pg_query($con, "rollback");
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
			}
			
		}
		else
		{
			// 集計データのステータスを更新する
			$obj->re_apply_report_status($selected_table,"0",$new_apply_id,$apply_id,$emp_id);
		}	




		
		//集計処理用データの更新処理を行う↑

		// 申請登録
		$obj->regist_re_apply($new_apply_id, $apply_id, "", "", "LIST",$emp_id);

		// 承認登録
		$obj->regist_re_applyapv($new_apply_id, $apply_id);

		// 承認者候補登録
		$obj->regist_re_applyapvemp($new_apply_id, $apply_id);

		// 添付ファイル登録
		$obj->regist_re_applyfile($new_apply_id, $apply_id);

		// 非同期・同期受信登録
		$obj->regist_re_applyasyncrecv($new_apply_id, $apply_id);

		// 申請結果通知登録
		$obj->regist_re_applynotice($new_apply_id, $apply_id);

		// 前提とする申請書(申請用)登録
		$obj->regist_re_applyprecond($new_apply_id, $apply_id);

		// 元の申請書に再申請ＩＤを更新
		$obj->update_re_apply_id($apply_id, $new_apply_id);

		array_push($arr_next_appry_id, $new_apply_id);

		// メール送信不要であれば次の申請書へ
		$wkfw_id = jnl_get_wkfw_id_by_apply_id($con, $new_apply_id, $fname);
		$wkfw_send_mail_flg = jnl_get_wkfw_send_mail_flg($con, $wkfw_id, $fname);
		if ($wkfw_send_mail_flg != "t") {
			continue;
		}

		// メール送信に必要な情報を配列に格納
		$sql = "select e.emp_email2, a.apv_order from jnl_applyapv a inner join empmst e on e.emp_id = a.emp_id";
		$cond = "where a.apply_id = $new_apply_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$to_addresses = array();
		while ($row = pg_fetch_array($sel)) {
			if (jnl_must_send_mail($wkfw_send_mail_flg, $row["emp_email2"], $arr_apply_wkfwmst[0]["wkfw_appr"], $row["apv_order"])) {
				$to_addresses[] = $row["emp_email2"];
			}
		}

		// 送信先がなければ次の申請書へ
		if (count($to_addresses) == 0) {
			continue;
		}

		// メール送信に必要な情報を配列に格納
		$must_send_mail_applys[] = array(
			"to_addresses" => $to_addresses,
			"wkfw_content_type" => $arr_apply_wkfwmst[0]["wkfw_content_type"],
			"content" => $arr_apply_wkfwmst[0]["apply_content"],
			"apply_title" => $arr_apply_wkfwmst[0]["apply_title"],
			"approve_label" => ($arr_apply_wkfwmst[0]["approve_label"] != "2") ? "承認" : "確認"
		);
	}
}



// 添付ファイルをコピー
// 添付ファイルの確認
if (!is_dir("jnl_apply")) {
	mkdir("jnl_apply", 0755);
}
for ($i = 0; $i < $count; $i++) {
	if ($re_apply_chk[$i] != "") {

		$apply_id = $re_apply_chk[$i];
		$sql = "select * from jnl_applyfile";
		$cond = "where apply_id = $apply_id order by apply_id asc";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if($sel==0){
			pg_exec($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		while ($row = pg_fetch_array($sel)) {

			$fine_no = $row["applyfile_no"];
			$file_name = $row["applyfile_name"];
			$ext = strrchr($file_name, ".");

			$tmp_filename = "jnl_apply/{$apply_id}_{$fine_no}{$ext}";
			copy($tmp_filename, "jnl_apply/{$arr_next_appry_id[$i]}_{$fine_no}{$ext}");
		}
	}
}

// メール送信準備
if (count($must_send_mail_applys) > 0) {
	$emp_id = get_emp_id($con, $session, $fname);
	$emp_detail = $obj->get_empmst_detail($emp_id);
	$emp_nm = jnl_format_emp_nm($emp_detail[0]);
	$emp_mail = jnl_format_emp_mail($emp_detail[0]);
	$emp_pos = jnl_format_emp_pos($emp_detail[0]);
}

// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($must_send_mail_applys) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	foreach ($must_send_mail_applys as $data) {
		$mail_subject = "[CoMedix] {$data["approve_label"]}依頼のお知らせ";
		$mail_content = jnl_format_mail_content($con, $data["wkfw_content_type"], $data["content"], $fname);
		$mail_separator = str_repeat("-", 60) . "\n";
		$additional_headers = "From: {$emp_mail}";
		$additional_parameter = "-f{$emp_mail}";

		foreach ($data["to_addresses"] as $to_address) {
			$mail_body = "以下の{$data["approve_label"]}依頼がありました。\n\n";
			$mail_body .= "申請者：{$emp_nm}\n";
			$mail_body .= "所属：{$emp_pos}\n";
			$mail_body .= "表題：{$data["apply_title"]}\n";
			if ($mail_content != "") {
				$mail_body .= $mail_separator;
				$mail_body .= "{$mail_content}\n";
			}

			mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
		}
	}
}

// データベース接続を切断
pg_close($con);

// 一覧画面に遷移
echo("<script type=\"text/javascript\">document.items.submit();</script>");
?>
</body>
