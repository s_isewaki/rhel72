<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");
require("about_authority.php");

$fname=$PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = 24;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($news_ids)) {
	pg_close($con);
	echo("<script type=\"text/javascript\">alert('処理対象が選択されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// トランザクションを開始
pg_query($con, "begin");

// チェックされたお知らせをループ
foreach ($news_ids as $tmp_news_id) {

	// 削除の場合
	if ($mode == "del") {
		// お知らせ情報を削除
		$sql = "delete from news";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ配信先部署情報を削除
		$sql = "delete from newsdept";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ職種情報を削除
		$sql = "delete from newsjob";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ参照情報を削除
		$sql = "delete from newsref";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせアンケート情報を削除
		$sql = "delete from newsenquet_q";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "delete from newsenquet_a";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		$sql = "delete from newsanswer";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// お知らせ督促メール情報を削除
		$sql = "delete from newsmail";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		// 添付ファイル情報を削除
		$sql = "delete from newsfile";
		$cond = "where news_id = $tmp_news_id";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
	// 元に戻す場合
		// お知らせ情報の削除フラグをfalseに更新
		$sql = "update news set";
		$set = array("news_del_flg");
		$setvalue = array('f');
		$cond = "where news_id = $tmp_news_id";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

if ($mode == "del") {
	// 添付ファイルの削除
	foreach ($news_ids as $tmp_news_id) {
		foreach (glob("news/{$tmp_news_id}_*.*") as $tmpfile) {
			unlink($tmpfile);
		}
	}
}
foreach (glob("news/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}

// 30分以上前に保存された一時ファイルを削除
foreach (glob("news/tmp/*.*") as $tmpfile) {
	if (time() - filemtime($tmpfile) >= 30 * 60) {
		unlink($tmpfile);
	}
}

// お知らせゴミ箱画面に遷移
$list_page = "news_trash.php";
echo("<script type=\"text/javascript\">location.href = '$list_page?session=$session&page=$page';</script>");
?>
