<?
require_once("about_comedix.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<?
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	js_login_exit();
}

// 権限のチェック
$check_auth = check_authority($session, 14, $fname);
if ($check_auth == "0") {
	js_login_exit();
}

// データベースに接続
$con = connect2db($fname);

// トランザクションの開始
pg_query($con, "begin");

// 入院中であればエラーとする
$sql = "select ptif_id from inptmst";
$cond = q("where ptif_id = '%s' limit 1", $ptif_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("入院中患者のため取消できません。");
}

// 入院予定登録済みであればエラーとする
$sql = "select ptif_id from inptres";
$cond = q("where ptif_id = '%s' limit 1", $ptif_id);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (pg_num_rows($sel) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("入院予定登録済み患者のため取消できません。");
}

// 最新の退院情報でなければエラーとする
$sql = "select count(*) from inpthist";
$cond = q("where ptif_id = '%s' and (inpt_in_dt > '%s' or (inpt_in_dt = '%s' and inpt_in_tm > '%s'))", $ptif_id, $in_dt, $in_dt, $in_tm);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("最新の退院情報でないため取消できません。");
}

// 退院情報を取得
$sql = "select * from inpthist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
$ward_cd = pg_fetch_result($sel, 0, "ward_cd");
$ptrm_room_no = pg_fetch_result($sel, 0, "ptrm_room_no");
$inpt_bed_no = pg_fetch_result($sel, 0, "inpt_bed_no");
$inpt_lt_kn_nm = pg_fetch_result($sel, 0, "inpt_lt_kn_nm");
$inpt_ft_kn_nm = pg_fetch_result($sel, 0, "inpt_ft_kn_nm");
$inpt_lt_kj_nm = pg_fetch_result($sel, 0, "inpt_lt_kj_nm");
$inpt_ft_kj_nm = pg_fetch_result($sel, 0, "inpt_ft_kj_nm");
$inpt_keywd = pg_fetch_result($sel, 0, "inpt_keywd");
$inpt_vacant_flg = pg_fetch_result($sel, 0, "inpt_vacant_flg");
$inpt_out_res_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");
$inpt_arrival = pg_fetch_result($sel, 0, "inpt_arrival");
$inpt_enti_id = pg_fetch_result($sel, 0, "inpt_enti_id");
$inpt_sect_id = pg_fetch_result($sel, 0, "inpt_sect_id");
$dr_id = pg_fetch_result($sel, 0, "dr_id");
$nurse_id = pg_fetch_result($sel, 0, "nurse_id");
$inpt_insurance = pg_fetch_result($sel, 0, "inpt_insurance");
$inpt_insu_rate_rireki = pg_fetch_result($sel, 0, "inpt_insu_rate_rireki");
if ($inpt_insu_rate_rireki == "") {$inpt_insu_rate_rireki = null;}
$inpt_insu_rate_cd = pg_fetch_result($sel, 0, "inpt_insu_rate_cd");
$inpt_reduced = pg_fetch_result($sel, 0, "inpt_reduced");
if ($inpt_reduced == "") {$inpt_reduced = null;}
$inpt_impaired = pg_fetch_result($sel, 0, "inpt_impaired");
if ($inpt_impaired == "") {$inpt_impaired = null;}
$inpt_insured = pg_fetch_result($sel, 0, "inpt_insured");
$inpt_meet_flg = pg_fetch_result($sel, 0, "inpt_meet_flg");
$inpt_dcb_cls = pg_fetch_result($sel, 0, "inpt_dcb_cls");
$inpt_dcb_bas = pg_fetch_result($sel, 0, "inpt_dcb_bas");
$inpt_dcb_exp = pg_fetch_result($sel, 0, "inpt_dcb_exp");
$inpt_rhb_cls = pg_fetch_result($sel, 0, "inpt_rhb_cls");
$inpt_rhb_bas = pg_fetch_result($sel, 0, "inpt_rhb_bas");
$inpt_rhb_exp = pg_fetch_result($sel, 0, "inpt_rhb_exp");
$inpt_ecg_flg = pg_fetch_result($sel, 0, "inpt_ecg_flg");
$inpt_sample_blood = pg_fetch_result($sel, 0, "inpt_sample_blood");
$inpt_observe = pg_fetch_result($sel, 0, "inpt_observe");
$inpt_purpose_rireki = pg_fetch_result($sel, 0, "inpt_purpose_rireki");
if ($inpt_purpose_rireki == "") {$inpt_purpose_rireki = null;}
$inpt_purpose_cd = pg_fetch_result($sel, 0, "inpt_purpose_cd");
$inpt_purpose_content = pg_fetch_result($sel, 0, "inpt_purpose_content");
$inpt_short_stay = pg_fetch_result($sel, 0, "inpt_short_stay");
if ($inpt_short_stay == "") {$inpt_short_stay = null;}
$inpt_outpt_test = pg_fetch_result($sel, 0, "inpt_outpt_test");
if ($inpt_outpt_test == "") {$inpt_outpt_test = null;}
$inpt_diagnosis = pg_fetch_result($sel, 0, "inpt_diagnosis");
$inpt_nrs_obsv = pg_fetch_result($sel, 0, "inpt_nrs_obsv");
$inpt_free = pg_fetch_result($sel, 0, "inpt_free");
$inpt_special = pg_fetch_result($sel, 0, "inpt_special");
$inpt_disease = pg_fetch_result($sel, 0, "inpt_disease");
$inpt_patho_from = pg_fetch_result($sel, 0, "inpt_patho_from");
$inpt_patho_to = pg_fetch_result($sel, 0, "inpt_patho_to");
$inpt_in_tm = pg_fetch_result($sel, 0, "inpt_in_tm");
$inpt_out_res_tm = pg_fetch_result($sel, 0, "inpt_out_res_tm");
$inpt_in_res_dt_flg = pg_fetch_result($sel, 0, "inpt_in_res_dt_flg");
$inpt_in_res_dt = pg_fetch_result($sel, 0, "inpt_in_res_dt");
$inpt_in_res_tm = pg_fetch_result($sel, 0, "inpt_in_res_tm");
$inpt_in_res_ym = pg_fetch_result($sel, 0, "inpt_in_res_ym");
$inpt_in_res_td = pg_fetch_result($sel, 0, "inpt_in_res_td");
$inpt_ope_dt_flg = pg_fetch_result($sel, 0, "inpt_ope_dt_flg");
$inpt_ope_dt = pg_fetch_result($sel, 0, "inpt_ope_dt");
$inpt_ope_ym = pg_fetch_result($sel, 0, "inpt_ope_ym");
$inpt_ope_td = pg_fetch_result($sel, 0, "inpt_ope_td");
$inpt_emergency = pg_fetch_result($sel, 0, "inpt_emergency");
$inpt_in_plan_period = pg_fetch_result($sel, 0, "inpt_in_plan_period");
$inpt_in_way = pg_fetch_result($sel, 0, "inpt_in_way");
$inpt_nursing = pg_fetch_result($sel, 0, "inpt_nursing");
$inpt_staple_fd = pg_fetch_result($sel, 0, "inpt_staple_fd");
$inpt_fd_type = pg_fetch_result($sel, 0, "inpt_fd_type");
$inpt_fd_dtl = pg_fetch_result($sel, 0, "inpt_fd_dtl");
$inpt_wish_rm_rireki = pg_fetch_result($sel, 0, "inpt_wish_rm_rireki");
if ($inpt_wish_rm_rireki == "") {$inpt_wish_rm_rireki = null;}
$inpt_wish_rm_cd = pg_fetch_result($sel, 0, "inpt_wish_rm_cd");
$inpt_fd_start = pg_fetch_result($sel, 0, "inpt_fd_start");
$inpt_result = pg_fetch_result($sel, 0, "inpt_result");
$inpt_result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
$inpt_out_rsn_rireki = pg_fetch_result($sel, 0, "inpt_out_rsn_rireki");
if ($inpt_out_rsn_rireki == "") {$inpt_out_rsn_rireki = null;}
$inpt_out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");
$inpt_out_pos_rireki = pg_fetch_result($sel, 0, "inpt_out_pos_rireki");
if ($inpt_out_pos_rireki == "") {$inpt_out_pos_rireki = null;}
$inpt_out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");
$inpt_out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
$inpt_fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
$inpt_out_comment = pg_fetch_result($sel, 0, "inpt_out_comment");
$inpt_res_chg_psn = pg_fetch_result($sel, 0, "inpt_res_chg_psn");
$inpt_res_chg_psn_dtl = pg_fetch_result($sel, 0, "inpt_res_chg_psn_dtl");
$inpt_res_chg_rsn = pg_fetch_result($sel, 0, "inpt_res_chg_rsn");
$inpt_res_chg_ctt = pg_fetch_result($sel, 0, "inpt_res_chg_ctt");
$inpt_except_flg = pg_fetch_result($sel, 0, "inpt_except_flg");
$inpt_except_from_date1 = pg_fetch_result($sel, 0, "inpt_except_from_date1");
$inpt_except_to_date1 = pg_fetch_result($sel, 0, "inpt_except_to_date1");
$inpt_except_from_date2 = pg_fetch_result($sel, 0, "inpt_except_from_date2");
$inpt_except_to_date2 = pg_fetch_result($sel, 0, "inpt_except_to_date2");
$inpt_except_from_date3 = pg_fetch_result($sel, 0, "inpt_except_from_date3");
$inpt_except_to_date3 = pg_fetch_result($sel, 0, "inpt_except_to_date3");
$inpt_except_from_date4 = pg_fetch_result($sel, 0, "inpt_except_from_date4");
$inpt_except_to_date4 = pg_fetch_result($sel, 0, "inpt_except_to_date4");
$inpt_except_from_date5 = pg_fetch_result($sel, 0, "inpt_except_from_date5");
$inpt_except_to_date5 = pg_fetch_result($sel, 0, "inpt_except_to_date5");
$inpt_privacy_flg = pg_fetch_result($sel, 0, "inpt_privacy_flg");
$inpt_privacy_text = pg_fetch_result($sel, 0, "inpt_privacy_text");
$inpt_pre_div = pg_fetch_result($sel, 0, "inpt_pre_div");
$inpt_except_reason_id1 = pg_fetch_result($sel, 0, "inpt_except_reason_id1");
if ($inpt_except_reason_id1 == "") {$inpt_except_reason_id1 = null;}
$inpt_except_reason_id2 = pg_fetch_result($sel, 0, "inpt_except_reason_id2");
if ($inpt_except_reason_id2 == "") {$inpt_except_reason_id2 = null;}
$inpt_except_reason_id3 = pg_fetch_result($sel, 0, "inpt_except_reason_id3");
if ($inpt_except_reason_id3 == "") {$inpt_except_reason_id3 = null;}
$inpt_except_reason_id4 = pg_fetch_result($sel, 0, "inpt_except_reason_id4");
if ($inpt_except_reason_id4 == "") {$inpt_except_reason_id4 = null;}
$inpt_except_reason_id5 = pg_fetch_result($sel, 0, "inpt_except_reason_id5");
if ($inpt_except_reason_id5 == "") {$inpt_except_reason_id5 = null;}
$inpt_out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
$inpt_out_sect_rireki = pg_fetch_result($sel, 0, "inpt_out_sect_rireki");
if ($inpt_out_sect_rireki == "") {$inpt_out_sect_rireki = null;}
$inpt_out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
$inpt_out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
if ($inpt_out_doctor_no == "") {$inpt_out_doctor_no = null;}
$inpt_out_city = pg_fetch_result($sel, 0, "inpt_out_city");
$inpt_back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
if ($inpt_back_bldg_cd == "") {$inpt_back_bldg_cd = null;}
$inpt_back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
if ($inpt_back_ward_cd == "") {$inpt_back_ward_cd = null;}
$inpt_back_ptrm_room_no = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
if ($inpt_back_ptrm_room_no == "") {$inpt_back_ptrm_room_no = null;}
$inpt_back_bed_no = pg_fetch_result($sel, 0, "inpt_back_bed_no");
if ($inpt_back_bed_no == "") {$inpt_back_bed_no = null;}
$inpt_back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
if ($inpt_back_in_dt == "") {$inpt_back_in_dt = null;}
$inpt_back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
if ($inpt_back_in_tm == "") {$inpt_back_in_tm = null;}
$inpt_back_sect_id = pg_fetch_result($sel, 0, "inpt_back_sect_id");
if ($inpt_back_sect_id == "") {$inpt_back_sect_id = null;}
$inpt_back_dr_id = pg_fetch_result($sel, 0, "inpt_back_dr_id");
if ($inpt_back_dr_id == "") {$inpt_back_dr_id = null;}
$inpt_back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");
$inpt_care_no = pg_fetch_result($sel, 0, "inpt_care_no");
$inpt_care_grd_rireki = pg_fetch_result($sel, 0, "inpt_care_grd_rireki");
if ($inpt_care_grd_rireki == "") {$inpt_care_grd_rireki = null;}
$inpt_care_grd_cd = pg_fetch_result($sel, 0, "inpt_care_grd_cd");
$inpt_care_apv = pg_fetch_result($sel, 0, "inpt_care_apv");
$inpt_care_from = pg_fetch_result($sel, 0, "inpt_care_from");
$inpt_care_to = pg_fetch_result($sel, 0, "inpt_care_to");
$inpt_dis_grd_rireki = pg_fetch_result($sel, 0, "inpt_dis_grd_rireki");
if ($inpt_dis_grd_rireki == "") {$inpt_dis_grd_rireki = null;}
$inpt_dis_grd_cd = pg_fetch_result($sel, 0, "inpt_dis_grd_cd");
$inpt_dis_type_rireki = pg_fetch_result($sel, 0, "inpt_dis_type_rireki");
if ($inpt_dis_type_rireki == "") {$inpt_dis_type_rireki = null;}
$inpt_dis_type_cd = pg_fetch_result($sel, 0, "inpt_dis_type_cd");
$inpt_spec_name = pg_fetch_result($sel, 0, "inpt_spec_name");
$inpt_spec_from = pg_fetch_result($sel, 0, "inpt_spec_from");
$inpt_spec_to = pg_fetch_result($sel, 0, "inpt_spec_to");
$inpt_intro_inst_cd = pg_fetch_result($sel, 0, "inpt_intro_inst_cd");
$inpt_intro_sect_rireki = pg_fetch_result($sel, 0, "inpt_intro_sect_rireki");
if ($inpt_intro_sect_rireki == "") {$inpt_intro_sect_rireki = null;}
$inpt_intro_sect_cd = pg_fetch_result($sel, 0, "inpt_intro_sect_cd");
$inpt_intro_doctor_no = pg_fetch_result($sel, 0, "inpt_intro_doctor_no");
if ($inpt_intro_doctor_no == "") {$inpt_intro_doctor_no = null;}
$inpt_hotline = pg_fetch_result($sel, 0, "inpt_hotline");
if ($inpt_hotline == "") {$inpt_hotline = null;}
$inpt_pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
$inpt_pr_sect_rireki = pg_fetch_result($sel, 0, "inpt_pr_sect_rireki");
if ($inpt_pr_sect_rireki == "") {$inpt_pr_sect_rireki = null;}
$inpt_pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
$inpt_pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
if ($inpt_pr_doctor_no == "") {$inpt_pr_doctor_no = null;}
$in_updated = pg_fetch_result($sel, 0, "in_updated");
$back_res_updated = pg_fetch_result($sel, 0, "back_res_updated");
$inpt_exemption = pg_fetch_result($sel, 0, "inpt_exemption");
if ($inpt_exemption == "") {$inpt_exemption = null;}

// 現在空床でなければエラーとする
$sql = "select count(*) from inptmst";
$cond = q("where ptif_id is not null and ptif_id <> '' and bldg_cd = %d and ward_cd = %d and ptrm_room_no = %d and inpt_bed_no = '%s'", $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_alert_exit("病床が使用中のため取消できません。");
}

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = q("where session_id  = '%s'", $session);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 入院情報を作成（空床情報のアップデート）
if (trim($inpt_out_res_dt) == "") {
	$inpt_in_flg = "t";
	$inpt_out_res_flg = "f";
	$inpt_result = null;
	$inpt_result_dtl = null;
	$inpt_out_rsn_rireki = null;
	$inpt_out_rsn_cd = null;
	$inpt_out_pos_rireki = null;
	$inpt_out_pos_cd = null;
	$inpt_out_pos_dtl = null;
	$inpt_fd_end = null;
	$inpt_out_comment = null;
	$inpt_out_inst_cd = null;
	$inpt_out_sect_rireki = null;
	$inpt_out_sect_cd = null;
	$inpt_out_doctor_no = null;
	$inpt_out_city = null;
	$inpt_back_bldg_cd = null;
	$inpt_back_ward_cd = null;
	$inpt_back_ptrm_room_no = null;
	$inpt_back_bed_no = null;
	$inpt_back_in_dt = null;
	$inpt_back_in_tm = null;
	$inpt_back_sect_id = null;
	$inpt_back_dr_id = null;
	$inpt_back_change_purpose = null;
	$inpt_pr_inst_cd = null;
	$inpt_pr_sect_rireki = null;
	$inpt_pr_sect_cd = null;
	$inpt_pr_doctor_no = null;
} else {
	$inpt_in_flg = "f";
	$inpt_out_res_flg = "t";
}
$sql = "update inptmst set";
$set = array("ptif_id", "inpt_lt_kn_nm", "inpt_ft_kn_nm", "inpt_lt_kj_nm", "inpt_ft_kj_nm", "inpt_keywd", "inpt_vacant_flg", "inpt_res_flg", "inpt_in_dt", "inpt_in_flg", "inpt_out_dt", "inpt_out_flg", "inpt_out_res_dt", "inpt_out_res_flg", "inpt_enti_id", "inpt_sect_id", "dr_id", "nurse_id", "inpt_insurance", "inpt_meet_flg", "inpt_ecg_flg", "inpt_sample_blood", "inpt_observe", "inpt_purpose_rireki", "inpt_purpose_cd", "inpt_purpose_content", "inpt_diagnosis", "inpt_special", "inpt_disease", "inpt_up_dt", "inpt_up_tm", "inpt_op_no", "inpt_in_tm", "inpt_out_tm", "inpt_out_res_tm", "inpt_in_res_dt_flg", "inpt_in_res_dt", "inpt_in_res_tm", "inpt_in_res_ym", "inpt_in_res_td", "inpt_ope_dt_flg", "inpt_ope_dt", "inpt_ope_ym", "inpt_ope_td", "inpt_emergency", "inpt_in_plan_period", "inpt_in_way", "inpt_nursing", "inpt_staple_fd", "inpt_fd_type", "inpt_fd_dtl", "inpt_wish_rm_rireki", "inpt_wish_rm_cd", "inpt_fd_start", "inpt_result", "inpt_result_dtl", "inpt_out_pos_rireki", "inpt_out_pos_cd", "inpt_out_pos_dtl", "inpt_fd_end", "inpt_out_comment", "inpt_res_chg_psn", "inpt_res_chg_psn_dtl", "inpt_res_chg_rsn", "inpt_res_chg_ctt", "inpt_except_flg", "inpt_except_from_date1", "inpt_except_to_date1", "inpt_except_from_date2", "inpt_except_to_date2", "inpt_except_from_date3", "inpt_except_to_date3", "inpt_except_from_date4", "inpt_except_to_date4", "inpt_except_from_date5", "inpt_except_to_date5", "inpt_privacy_flg", "inpt_privacy_text", "inpt_except_reason_id1", "inpt_except_reason_id2", "inpt_except_reason_id3", "inpt_except_reason_id4", "inpt_except_reason_id5", "inpt_out_inst_cd", "inpt_out_sect_rireki", "inpt_out_sect_cd", "inpt_out_doctor_no", "inpt_out_city", "inpt_back_bldg_cd", "inpt_back_ward_cd", "inpt_back_ptrm_room_no", "inpt_back_bed_no", "inpt_back_in_dt", "inpt_back_in_tm", "inpt_back_sect_id", "inpt_back_dr_id", "inpt_back_change_purpose", "inpt_short_stay", "inpt_outpt_test", "inpt_insu_rate_rireki", "inpt_insu_rate_cd", "inpt_reduced", "inpt_impaired", "inpt_insured", "inpt_arrival", "inpt_patho_from", "inpt_patho_to", "inpt_out_rsn_rireki", "inpt_out_rsn_cd", "inpt_dcb_cls", "inpt_dcb_bas", "inpt_dcb_exp", "inpt_rhb_cls", "inpt_rhb_bas", "inpt_rhb_exp", "inpt_care_no", "inpt_care_grd_rireki", "inpt_care_grd_cd", "inpt_care_apv", "inpt_care_from", "inpt_care_to", "inpt_dis_grd_rireki", "inpt_dis_grd_cd", "inpt_dis_type_rireki", "inpt_dis_type_cd", "inpt_spec_name", "inpt_spec_from", "inpt_spec_to", "inpt_pre_div", "inpt_intro_inst_cd", "inpt_intro_sect_rireki", "inpt_intro_sect_cd", "inpt_intro_doctor_no", "inpt_hotline", "inpt_nrs_obsv", "inpt_free", "inpt_pr_inst_cd", "inpt_pr_sect_rireki", "inpt_pr_sect_cd", "inpt_pr_doctor_no", "in_updated", "out_res_updated", "back_res_updated", "inpt_exemption");
$setvalue = array($ptif_id, $inpt_lt_kn_nm, $inpt_ft_kn_nm, $inpt_lt_kj_nm, $inpt_ft_kj_nm, $inpt_keywd, $inpt_vacant_flg, "f", $in_dt, $inpt_in_flg, null, "f", $inpt_out_res_dt, $inpt_out_res_flg, $inpt_enti_id, $inpt_sect_id, $dr_id, $nurse_id, $inpt_insurance, $inpt_meet_flg, $inpt_ecg_flg, $inpt_sample_blood, $inpt_observe, $inpt_purpose_rireki, $inpt_purpose_cd, $inpt_purpose_content, $inpt_diagnosis, $inpt_special, $inpt_disease, date("Ymd"), date("Hi"), $emp_id, $inpt_in_tm, null, $inpt_out_res_tm, $inpt_in_res_dt_flg, $inpt_in_res_dt, $inpt_in_res_tm, $inpt_in_res_ym, $inpt_in_res_td, $inpt_ope_dt_flg, $inpt_ope_dt, $inpt_ope_ym, $inpt_ope_td, $inpt_emergency, $inpt_in_plan_period, $inpt_in_way, $inpt_nursing, $inpt_staple_fd, $inpt_fd_type, $inpt_fd_dtl, $inpt_wish_rm_rireki, $inpt_wish_rm_cd, $inpt_fd_start, $inpt_result, $inpt_result_dtl, $inpt_out_pos_rireki, $inpt_out_pos_cd, $inpt_out_pos_dtl, $inpt_fd_end, $inpt_out_comment, $inpt_res_chg_psn, $inpt_res_chg_psn_dtl, $inpt_res_chg_rsn, $inpt_res_chg_ctt, $inpt_except_flg, $inpt_except_from_date1, $inpt_except_to_date1, $inpt_except_from_date2, $inpt_except_to_date2, $inpt_except_from_date3, $inpt_except_to_date3, $inpt_except_from_date4, $inpt_except_to_date4, $inpt_except_from_date5, $inpt_except_to_date5, $inpt_privacy_flg, $inpt_privacy_text, $inpt_except_reason_id1, $inpt_except_reason_id2, $inpt_except_reason_id3, $inpt_except_reason_id4, $inpt_except_reason_id5, $inpt_out_inst_cd, $inpt_out_sect_rireki, $inpt_out_sect_cd, $inpt_out_doctor_no, $inpt_out_city, $inpt_back_bldg_cd, $inpt_back_ward_cd, $inpt_back_ptrm_room_no, $inpt_back_bed_no, $inpt_back_in_dt, $inpt_back_in_tm, $inpt_back_sect_id, $inpt_back_dr_id, $inpt_back_change_purpose, $inpt_short_stay, $inpt_outpt_test, $inpt_insu_rate_rireki, $inpt_insu_rate_cd, $inpt_reduced, $inpt_impaired, $inpt_insured, $inpt_arrival, $inpt_patho_from, $inpt_patho_to, $inpt_out_rsn_rireki, $inpt_out_rsn_cd, $inpt_dcb_cls, $inpt_dcb_bas, $inpt_dcb_exp, $inpt_rhb_cls, $inpt_rhb_bas, $inpt_rhb_exp, $inpt_care_no, $inpt_care_grd_rireki, $inpt_care_grd_cd, $inpt_care_apv, $inpt_care_from, $inpt_care_to, $inpt_dis_grd_rireki, $inpt_dis_grd_cd, $inpt_dis_type_rireki, $inpt_dis_type_cd, $inpt_spec_name, $inpt_spec_from, $inpt_spec_to, $inpt_pre_div, $inpt_intro_inst_cd, $inpt_intro_sect_rireki, $inpt_intro_sect_cd, $inpt_intro_doctor_no, $inpt_hotline, $inpt_nrs_obsv, $inpt_free, $inpt_pr_inst_cd, $inpt_pr_sect_rireki, $inpt_pr_sect_cd, $inpt_pr_doctor_no, $in_updated, date("YmdHis"), $back_res_updated, $inpt_exemption);
$cond = q("where bldg_cd = %d and ward_cd = %d and ptrm_room_no = %d and inpt_bed_no = '%s'", $bldg_cd, $ward_cd, $ptrm_room_no, $inpt_bed_no);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 退院情報の削除
$sql = "delete from inpthist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// チェックリスト履歴情報を取得
$sql = "select * from inptqhist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// チェックリスト履歴情報が存在する場合
if (pg_num_rows($sel) > 0) {
	$inpt_q1 = pg_fetch_result($sel, 0, "inpt_q1");
	$inpt_q2 = pg_fetch_result($sel, 0, "inpt_q2");
	$inpt_q3 = pg_fetch_result($sel, 0, "inpt_q3");
	$inpt_q4_1 = pg_fetch_result($sel, 0, "inpt_q4_1");
	$inpt_q4_2 = pg_fetch_result($sel, 0, "inpt_q4_2");
	$inpt_q5 = pg_fetch_result($sel, 0, "inpt_q5");
	$inpt_q6_1 = pg_fetch_result($sel, 0, "inpt_q6_1");
	$inpt_q6_2 = pg_fetch_result($sel, 0, "inpt_q6_2");
	$inpt_q7 = pg_fetch_result($sel, 0, "inpt_q7");
	$inpt_q7_com = pg_fetch_result($sel, 0, "inpt_q7_com");
	$inpt_q8_1 = pg_fetch_result($sel, 0, "inpt_q8_1");
	$inpt_q8_2 = pg_fetch_result($sel, 0, "inpt_q8_2");

	// チェックリスト履歴情報を削除
	$sql = "delete from inptqhist";
	$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}

	// チェックリスト情報を作成
	$sql = "insert into inptq (ptif_id, inpt_q1, inpt_q2, inpt_q3, inpt_q4_1, inpt_q4_2, inpt_q5, inpt_q6_1, inpt_q6_2, inpt_q7, inpt_q7_com, inpt_q8_1, inpt_q8_2, inpt_reg_date) values (";
	$content = array($ptif_id, $inpt_q1, $inpt_q2, $inpt_q3, $inpt_q4_1, $inpt_q4_2, $inpt_q5, $inpt_q6_1, $inpt_q6_2, $inpt_q7, $inpt_q7_com, $inpt_q8_1, $inpt_q8_2, date("YmdHi"));
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 栄養問診表履歴情報を取得
$sql = "select * from inptnuthist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 栄養問診表履歴情報が存在する場合
if (pg_num_rows($sel) > 0) {
	$nut_q1 = pg_fetch_result($sel, 0, "nut_q1");
	$nut_q1_1 = pg_fetch_result($sel, 0, "nut_q1_1");
	$nut_q1_2 = pg_fetch_result($sel, 0, "nut_q1_2");
	$nut_q1_3 = pg_fetch_result($sel, 0, "nut_q1_3");
	$nut_q1_4 = pg_fetch_result($sel, 0, "nut_q1_4");
	$nut_q1_5_com = pg_fetch_result($sel, 0, "nut_q1_5_com");
	$nut_q1_6_com = pg_fetch_result($sel, 0, "nut_q1_6_com");
	$nut_q2_1 = pg_fetch_result($sel, 0, "nut_q2_1");
	$nut_q2_2 = pg_fetch_result($sel, 0, "nut_q2_2");
	$nut_q2_3 = pg_fetch_result($sel, 0, "nut_q2_3");
	$nut_q2_4 = pg_fetch_result($sel, 0, "nut_q2_4");
	$nut_q2_5 = pg_fetch_result($sel, 0, "nut_q2_5");
	$nut_q2_6 = pg_fetch_result($sel, 0, "nut_q2_6");
	$nut_q2_7 = pg_fetch_result($sel, 0, "nut_q2_7");
	$nut_q2_8 = pg_fetch_result($sel, 0, "nut_q2_8");
	$nut_q3 = pg_fetch_result($sel, 0, "nut_q3");
	$nut_q4 = pg_fetch_result($sel, 0, "nut_q4");
	$nut_q5_1 = pg_fetch_result($sel, 0, "nut_q5_1");
	$nut_q5_2 = pg_fetch_result($sel, 0, "nut_q5_2");

	// チェックリスト履歴情報を削除
	$sql = "delete from inptnuthist";
	$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}

	// チェックリスト情報を作成
	$sql = "insert into inptnut (ptif_id, nut_q1, nut_q1_1, nut_q1_2, nut_q1_3, nut_q1_4, nut_q1_5_com, nut_q1_6_com, nut_q2_1, nut_q2_2, nut_q2_3, nut_q2_4, nut_q2_5, nut_q2_6, nut_q2_7, nut_q2_8, nut_q3, nut_q4, nut_q5_1, nut_q5_2, nut_reg_date) values (";
	$content = array($ptif_id, $nut_q1, $nut_q1_1, $nut_q1_2, $nut_q1_3, $nut_q1_4, $nut_q1_5_com, $nut_q1_6_com, $nut_q2_1, $nut_q2_2, $nut_q2_3, $nut_q2_4, $nut_q2_5, $nut_q2_6, $nut_q2_7, $nut_q2_8, $nut_q3, $nut_q4, $nut_q5_1, $nut_q5_2, date("YmdHi"));
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 入院状況履歴情報を更新（入院情報に紐づける）
$sql = "update inptcond set";
$set = array("hist_flg");
$setvalue = array("f");
$cond = q("where ptif_id = '%s' and date >= '%s'", $ptif_id, $in_dt);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院履歴分の担当者情報を退避
$sql = "select emp_id, order_no from inptophist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s' order by order_no", $ptif_id, $in_dt, $in_tm);
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院履歴分の担当者情報を削除
$sql = "delete from inptophist";
$cond = q("where ptif_id = '%s' and inpt_in_dt = '%s' and inpt_in_tm = '%s'", $ptif_id, $in_dt, $in_tm);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 入院分の担当者情報を作成
while ($row = pg_fetch_array($sel)) {
	$sql = "insert into inptop (ptif_id, emp_id, order_no) values (";
	$content = array($ptif_id, $row["emp_id"], $row["order_no"]);
	$ins = insert_into_table($con, $sql, q($content), $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		js_error_exit();
	}
}

// 退院予定表の備考を移動（念のため削除してから）
$sql = "delete from bed_outnote";
$cond = q("where ptif_id = '%s' and type = 'p' and mode = 'r'", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$sql = "update bed_outnote set";
$set = array("mode", "in_dttm");
$setvalue = array("r", null);
$cond = q("where ptif_id = '%s' and in_dttm = '%s%s' and type = 'p' and mode = 'o'", $ptif_id, $in_dt, $in_tm);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// 戻り入院の備考を引き継ぐ（念のため削除してから）
$sql = "delete from bed_innote";
$cond = q("where type = 'p' and mode = 'r' and ptif_id = '%s' and back_flg = 't' and in_dttm is null", $ptif_id);
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}
$sql = "update bed_innote set";
$set = array("in_dttm");
$setvalue = array(null);
$cond = q("where type = 'p' and mode = 'r' and ptif_id = '%s' and back_flg = 't' and in_dttm = '%s%s'", $ptif_id, $in_dt, $in_tm);
$upd = update_set_table($con, $sql, $set, q($setvalue), $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	js_error_exit();
}

// トランザクションのコミット
pg_query($con, "commit");
//pg_query($con, "rollback");

// データベース接続を閉じる
pg_close($con);

// 患者一覧画面を再表示
?>
<form name="reloadform">
<? if ($list_type == "8") { ?>
<input type="hidden" name="href" value="out_inpatient_list.php?session=<? ehu($session); ?>&bldgwd=<? ehu($bldgwd); ?>&list_sect_id=<? ehu($list_sect_id); ?>&initial=<? ehu($initial); ?>&order=<? ehu($order); ?>&page=<? ehu($page); ?>">
<? } else if ($list_type == "A") { ?>
<input type="hidden" name="href" value="inpatient_date_list.php?session=<? ehu($session); ?>&bldgwd=<? ehu($bldgwd); ?>&list_sect_id=<? ehu($list_sect_id); ?>&list_year=<? ehu($list_year); ?>&list_month=<? ehu($list_month); ?>&list_day=<? ehu($list_day); ?>&list_year2=<? ehu($list_year2); ?>&list_month2=<? ehu($list_month2); ?>&list_day2=<? ehu($list_day2); ?>&initial=<? ehu($initial); ?>&order=<? ehu($order); ?>&page=<? ehu($page); ?>">
<? } ?>
</form>
<script type="text/javascript">
location.href = document.reloadform.href.value;
</script>
</body>
