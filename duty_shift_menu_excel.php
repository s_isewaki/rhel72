<?
///*****************************************************************************
// 勤務シフト作成 | シフト作成「ＥＸＣＥＬ」
///*****************************************************************************

ob_start();
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");
require_once("duty_shift_duty_common_class.php");
require_once("show_class_name.ini");	// ユニット（３階層名）表示のため追加

///-----------------------------------------------------------------------------
//画面名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
//セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//DBコネクション取得
///-----------------------------------------------------------------------------
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
//利用するCLASS
///-----------------------------------------------------------------------------
$obj = new duty_shift_common_class($con, $fname);
$obj_duty = new duty_shift_duty_common_class($con, $fname);
///-----------------------------------------------------------------------------
//処理
///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------
$excel_flg = "1";			//ＥＸＣＥＬ表示フラグ（１：ＥＸＣＥＬ表示）
$download_data = "";		//ＥＸＣＥＬデータ
///-----------------------------------------------------------------------------
//現在日付の取得
///-----------------------------------------------------------------------------
$date = getdate();
$now_yyyy = $date["year"];
$now_mm = $date["mon"];
$now_dd = $date["mday"];

///-----------------------------------------------------------------------------
//表示年／月設定
///-----------------------------------------------------------------------------
if ($duty_yyyy == "") { $duty_yyyy = $now_yyyy; }
if ($duty_mm == "") { $duty_mm = $now_mm; }
///-----------------------------------------------------------------------------
//日数
///-----------------------------------------------------------------------------
$day_cnt = $obj->days_in_month($duty_yyyy, $duty_mm);
$start_day = 1;
$end_day = $day_cnt;
///-----------------------------------------------------------------------------
//各変数値の初期値設定
///-----------------------------------------------------------------------------
$individual_flg = "";
$hope_get_flg = "";
//	$show_data_flg = "1";		//２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直、４：コメント）
//	if ($show_data_flg == "4") {
//		$show_data_flg = "1";
//	}

if ($data_cnt == "") { $data_cnt = 0; }
//入力形式で表示する日（開始／終了）
$edit_start_day = 0;
$edit_end_day = 0;

$group_id = $cause_group_id;
///-----------------------------------------------------------------------------
// ログインユーザの職員ID・氏名を取得
///-----------------------------------------------------------------------------
$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");
///-----------------------------------------------------------------------------
//ＤＢ(stmst)より役職情報取得
//ＤＢ(jobmst)より職種情報取得
//ＤＢ(empmst)より職員情報を取得
//ＤＢ(wktmgrp)より勤務パターン情報を取得
///-----------------------------------------------------------------------------
$data_st = $obj->get_stmst_array();
$data_job = $obj->get_jobmst_array();
//	$data_emp = $obj->get_empmst_array("");
// 有効なグループの職員を取得
//$data_emp = $obj->get_valid_empmst_array($emp_id, $duty_yyyy, $duty_mm); //20140210
// シフトグループに所属する職員を取得 20140210
$data_emp = $obj->get_empmst_by_group_id($group_id, $duty_yyyy, $duty_mm);
$data_wktmgrp = $obj->get_wktmgrp_array();

// 追加がある場合、取得
$add_staff_id_all_array = split(",", $add_staff_id_all);
$add_staff_id_all_cnt = count($add_staff_id_all_array);
if ($add_staff_cnt > 0 || $add_staff_id_all_cnt > 0) {
	$wk_emp_id_array = split(",", $add_staff_id);
	$wk_array = array_merge($add_staff_id_all_array, $wk_emp_id_array);
	$wk_cnt = count($data_emp);
	$add_staff_id_all = "";
	foreach ($wk_array as $wk_add_staff_id) {
		if ($wk_add_staff_id == "") {
			continue;
		}
		$wk_data_emp = $obj->get_empmst_array($wk_add_staff_id);
		$data_emp[$wk_cnt]["id"] 	= $wk_data_emp[0]["id"];
		$data_emp[$wk_cnt]["name"] 	= $wk_data_emp[0]["name"];
		$data_emp[$wk_cnt]["job_id"] 	= $wk_data_emp[0]["job_id"];
		$data_emp[$wk_cnt]["st_id"] 	= $wk_data_emp[0]["st_id"];
		$data_emp[$wk_cnt]["join"] 	= $wk_data_emp[0]["join"];
		$wk_cnt++;

		if ($add_staff_id_all != "") {
			$add_staff_id_all .= ",";
		}
		$add_staff_id_all .= $wk_add_staff_id;
	}
}

///-----------------------------------------------------------------------------
// 指定グループの病棟名を取得
///-----------------------------------------------------------------------------
$wk_group = $obj->get_duty_shift_group_array($group_id, $data_emp, $data_wktmgrp);
if (count($wk_group) > 0) {
	$group_name = $wk_group[0]["group_name"];
	$pattern_id = $wk_group[0]["pattern_id"];
	$start_month_flg1 = $wk_group[0]["start_month_flg1"];
	$start_day1 = $wk_group[0]["start_day1"];
	$month_flg1 = $wk_group[0]["month_flg1"];
	$end_day1 = $wk_group[0]["end_day1"];
	$start_day2 = $wk_group[0]["start_day2"];
	$month_flg2 = $wk_group[0]["month_flg2"];
	$end_day2 = $wk_group[0]["end_day2"];
	$print_title = $wk_group[0]["print_title"];
	$reason_setting_flg = $wk_group[0]["reason_setting_flg"];
}
if ($start_month_flg1 == "") {$start_month_flg1 = "1";}
if ($start_day1 == "" || $start_day1 == "0") {$start_day1 = "1";}
if ($month_flg1 == "" || $end_day1 == "0") {$month_flg1 = "1";}
if ($end_day1 == "" || $end_day1 == "0") {	$end_day1 = "99";}
if ($start_day2 == "" || $start_day2 == "0") {	$start_day2 = "0";}
if ($month_flg2 == "" || $end_day2 == "") {	$month_flg2 = "1";}
if ($end_day2 == "" || $end_day2 == "0") {	$end_day2 = "99";}
///-----------------------------------------------------------------------------
// カレンダー(calendar)情報を取得
///-----------------------------------------------------------------------------
$arr_date = $obj->get_term_date($duty_yyyy, $duty_mm, $start_month_flg1, $start_day1, $month_flg1, $end_day1);

$start_date = $arr_date[0];
$end_date = $arr_date[1];

$calendar_array = $obj->get_calendar_array($start_date, $end_date);
$day_cnt=count($calendar_array);
///-----------------------------------------------------------------------------
//指定月の全曜日を設定
///-----------------------------------------------------------------------------
$week_array = array();
$tmp_date = $start_date;
for ($k=1; $k<=$day_cnt; $k++) {
	$tmp_date = strtotime($tmp_date);
	$week_array[$k]["name"] = $obj->get_weekday($tmp_date);
	$tmp_date = date("Ymd", strtotime("+1 day", $tmp_date));
}
///-----------------------------------------------------------------------------
//ＤＢ(atdptn)より出勤パターン情報取得
//ＤＢ(duty_shift_pattern)より勤務シフト記号情報を取得
///-----------------------------------------------------------------------------
$data_atdptn = $obj->get_atdptn_array($pattern_id);
//20140220 start
$data_pattern = $obj->get_duty_shift_pattern_array($pattern_id, $data_atdptn);
//応援追加されている出勤パターングループ確認 
$arr_pattern_id = $obj->get_arr_pattern_id($group_id, $duty_yyyy, $duty_mm, $wk_array, $pattern_id);
$pattern_id_cnt = count($arr_pattern_id);
if ($pattern_id_cnt > 0) {
    $data_atdptn_tmp = $obj->get_atdptn_array_2($arr_pattern_id); //配列対応
    $data_atdptn_all = array_merge($data_atdptn, $data_atdptn_tmp);
    $data_pattern_tmp = $obj->get_duty_shift_pattern_array("array", $data_atdptn_tmp, $arr_pattern_id); //配列対応
    $data_pattern_all = array_merge($data_pattern, $data_pattern_tmp);
}
//応援なしの場合、同じパターングループのデータを設定
else {
    $data_atdptn_all = $data_atdptn;
    $data_pattern_all = $data_pattern;
}
//20140220 end
///-----------------------------------------------------------------------------
//遷移元のデータに変更
///-----------------------------------------------------------------------------
$set_data = array();
$plan_array = array();
for($i=0;$i<$data_cnt;$i++) {
	//応援追加情報
	$assist_str = "assist_group_$i";
	$arr_assist_group = split(",", $$assist_str);
	//希望分
	if ($show_data_flg == "2") {
		$rslt_assist_str = "rslt_assist_group_$i";
		$arr_rslt_assist_group = split(",", $$rslt_assist_str);
	}
	///-----------------------------------------------------------------------------
	//遷移元のデータに変更
	///-----------------------------------------------------------------------------
	for($k=1;$k<=$day_cnt;$k++) {
		$set_data[$i]["plan_rslt_flg"] = "1";			//１：実績は再取得
		$wk2 = "pattern_id_$i" . "_" . $k;
		$wk3 = "atdptn_ptn_id_$i" . "_" . $k;
		$wk5 = "comment_$i" . "_" . $k;
		$wk6 = "reason_2_$i" . "_" . $k;

		$set_data[$i]["staff_id"] = $staff_id[$i];					//職員ＩＤ
		$set_data[$i]["pattern_id_$k"] = $$wk2;						//出勤グループＩＤ

		if ((int)substr($$wk3,0,2) > 0) {
			$set_data[$i]["atdptn_ptn_id_$k"] = (int)substr($$wk3,0,2);	//出勤パターンＩＤ
		}
		if ((int)substr($$wk3,2,2) > 0) {
			$set_data[$i]["reason_$k"] = (int)substr($$wk3,2,2);	//事由
		}

		$set_data[$i]["reason_2_$k"] = $$wk6;						//事由（午前休暇／午後休暇）
		$set_data[$i]["comment_$k"] = $$wk5;							//コメント
		//応援情報
		$set_data[$i]["assist_group_$k"] = $arr_assist_group[$k - 1];
		if ($show_data_flg == "2") {
			$set_data[$i]["rslt_assist_group_$k"] = $arr_rslt_assist_group[$k - 1];
		}
		// 勤務シフト希望のデータ
		$wk7 = "rslt_pattern_id_$i" . "_" . $k;
		$wk8 = "rslt_id_$i" . "_" . $k;
		$wk9 = "rslt_reason_2_$i" . "_" . $k;
		if ($show_data_flg == "2") {
			// 勤務シフト希望のデータ
			$set_data[$i]["rslt_pattern_id_$k"] = $$wk7;						//出勤グループＩＤ

			if ((int)substr($$wk8,0,2) > 0) {
				$set_data[$i]["rslt_id_$k"] = (int)substr($$wk8,0,2);	//出勤パターンＩＤ
			}
			if ((int)substr($$wk8,2,2) > 0) {
				$set_data[$i]["rslt_reason_$k"] = (int)substr($$wk8,2,2);		//事由
			}

			$set_data[$i]["rslt_reason_2_$k"] = $$wk9;						//事由（午前有給／午後有給）
		}
	}
}
//----------------------------------------------
// 当初予定か勤務予定か
//----------------------------------------------
$disp_tosYotei_flg = $wk_group[0]["disp_tosYotei_flg"];

// 取得テーブルを変更する
if ($disp_tosYotei_flg == "t") {
    $sw_sche = "_sche";
} else {
    $sw_sche = "";
}
//----------------------------------------------
$plan_array = $obj->get_duty_shift_plan_array(
        $group_id, $pattern_id,
        $duty_yyyy, $duty_mm, $day_cnt,
        $individual_flg,
        $hope_get_flg,
        $show_data_flg,
        $set_data, $week_array, $calendar_array,
        $data_atdptn, $data_pattern_all,
        $data_st, $data_job, $data_emp, "", null, $sw_sche);
$data_cnt = count($plan_array);
///-----------------------------------------------------------------------------
// 行タイトル（行ごとの集計する勤務パターン）を取得
// 列タイトル（列ごとの集計する勤務パターン）を取得
///-----------------------------------------------------------------------------
$title_gyo_array = $obj->get_total_title_array($pattern_id,	"1", $data_pattern);
$title_retu_array = $obj->get_total_title_array($pattern_id, "2", $data_pattern);
if ((count($title_gyo_array) <= 0) || (count($title_retu_array) <= 0)) {
	$err_msg_2 = "勤務シフトグループ情報が未設定です。管理者に連絡してください。";
}
///-----------------------------------------------------------------------------
//個人日数合計（行）を算出設定
//個人日数合計（列）を算出設定
///-----------------------------------------------------------------------------
//2段目が実績の場合、システム日以降は、行を集計しない。
if ($show_data_flg == "1") {
	$wk_rs_flg = "2";
} else {
	$wk_rs_flg = "";
}
$gyo_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);
//2段表示、実績、勤務希望の場合、それぞれの内容を列集計する 20160222
if ($show_data_flg == "1" || $show_data_flg == "2") {
	$wk_rs_flg = "1";
} else {
	$wk_rs_flg = "";
}
$retu_array = $obj->get_total_cnt_array($group_id, $pattern_id,
		"2", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $data_atdptn);

$title_hol_array = $obj->get_total_title_hol_array($pattern_id, $data_pattern, $data_atdptn);
//print_r($title_hol_array);
//休暇事由情報
$hol_cnt_array = $obj->get_hol_cnt_array($group_id, $pattern_id, "1", $wk_rs_flg, $day_cnt, $plan_array, $title_gyo_array, $title_retu_array, $calendar_array, $title_hol_array, $data_atdptn);

$nenkyu_array = array();
if ($hol_dt_flg == "1") {
	//年休残
	$nenkyu_array = $obj->get_paid_hol_rest_array(
			$plan_array,
			$calendar_array,
			$duty_yyyy,
			$duty_mm,
	        "create");
}
///-----------------------------------------------------------------------------
//最大文字数算出
///-----------------------------------------------------------------------------
$max_font_cnt = 1;
for ($i=0; $i<$data_cnt; $i++) {
	for ($k=1; $k<=$day_cnt; $k++) {
		//予定
		if ($plan_array[$i]["font_name_$k"] != "") {
			if ($max_font_cnt < strlen($plan_array[$i]["font_name_$k"])) {
				if (strlen($plan_array[$i]["font_name_$k"]) <= 2) {
					$max_font_cnt = 1;
				} else 	if (strlen($plan_array[$i]["font_name_$k"]) <= 4) {
					$max_font_cnt = 2;
				}
			}
		}
		//実績
		if ($plan_array[$i]["rslt_name_$k"] != "") {
			if ($max_font_cnt < (int)strlen($plan_array[$i]["rslt_name_$k"])) {
				if (strlen($plan_array[$i]["rslt_name_$k"]) <= 2) {
					$max_font_cnt = 1;
				} else 	if (strlen($plan_array[$i]["rslt_name_$k"]) <= 4) {
					$max_font_cnt = 2;
				}
			}
		}
	}
}

//決裁欄役職名
$st_name_array = $obj->get_duty_shift_group_st_name_array($group_id);
// 決裁欄印字有無を取得する
$approval_column_print_flg = $wk_group[0]["approval_column_print_flg"];

///-----------------------------------------------------------------------------
// ユニット表示仕変
$unit_disp_level = $obj->get_emp_unitlevel($group_id);
///-----------------------------------------------------------------------------

// ユニット（３階層名）表示のため追加
$arr_class_name = get_class_name_array($con, $fname);
$unit_name = $arr_class_name[intval($unit_disp_level) - 1];

///-----------------------------------------------------------------------------
//ファイル名
///-----------------------------------------------------------------------------
$filename = 'shift_'.$duty_yyyy.sprintf("%02d",$duty_mm).'.xls';
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
	$encoding = 'sjis';
} else {
	$encoding = mb_http_output();   // Firefox
}
$filename = mb_convert_encoding(
		$filename,
		$encoding,
		mb_internal_encoding());

///*****************************************************************************
//ＨＴＭＬ作成
///*****************************************************************************

$wk_array = showData($err_msg_2,
		$group_name,
		$duty_yyyy,
		$duty_mm,
		$print_title,
		$now_yyyy,			//作成年
		$now_mm,			//作成月
		$now_dd,			//作成日
		$day_cnt,
		$st_name_array,
        $approval_column_print_flg,		// 決裁欄フラグ
        $disp_tosYotei_flg				// 当初予定フラグ
        );
$download_data_0 = $wk_array["data"];

//$total_print_flg = "1";
//日・曜日
$download_data_1 = showTitleWeek($obj, $duty_yyyy, $duty_mm, $day_cnt,
        $plan_array, $week_array, $title_gyo_array,$calendar_array, $total_print_flg, $show_data_flg, $hol_dt_flg, $title_hol_array,
        $wk_group[0], $unit_name);

//勤務シフトデータ（職員（列）年月日（行）
$download_data_2 = showList($obj, $obj_duty, $pattern_id, $plan_array, $title_gyo_array, $gyo_array, 0, $data_cnt, $day_cnt, $max_font_cnt, $total_print_flg, $show_data_flg, $group_id, $reason_setting_flg, $hol_dt_flg, $title_hol_array, $hol_cnt_array, $nenkyu_array, $calendar_array, $week_array,
        $wk_group[0]);

//集計行列の出力をする場合
if ($total_print_flg == "1") {
	//2段目の集計有無
	if ($show_data_flg == "1" || $show_data_flg == "2") {
		$wk_total_gyo_flg = "2";
	} else {
		$wk_total_gyo_flg = "1";
	}
    $download_data_3 = showTitleRetu($obj, $day_cnt, $plan_array, $title_gyo_array, $gyo_array, $title_retu_array, $retu_array, $wk_total_gyo_flg, $total_print_flg, $hol_dt_flg, $title_hol_array, $hol_cnt_array, $nenkyu_array, $calendar_array, $week_array, $show_data_flg,
            $wk_group[0]);

}
///-----------------------------------------------------------------------------
//データEXCEL出力
///-----------------------------------------------------------------------------
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding(nl2br($download_data_0), 'sjis', mb_internal_encoding());
echo mb_convert_encoding(nl2br($download_data_1), 'sjis', mb_internal_encoding());
echo mb_convert_encoding(nl2br($download_data_2), 'sjis', mb_internal_encoding());
if ($total_print_flg == "1") {
	echo mb_convert_encoding(nl2br($download_data_3), 'sjis', mb_internal_encoding());
}
$data = "</table> \n";
echo mb_convert_encoding(nl2br($data), 'sjis', mb_internal_encoding());

//	echo "data_cnt=".$data_cnt; //debug
ob_end_flush();

pg_close($con);


function showData($err_msg,
	$group_name,
	$duty_yyyy,
	$duty_mm,
	$print_title,
	$create_yyyy,			//作成年
	$create_mm,				//作成月
	$create_dd,				//作成日
	$day_cnt,
    $st_name_array,
    $approval_column_print_flg,
    $disp_tosYotei_flg
    
	) {
	$data = "";
	// metaタグで文字コード設定必須（ない場合にはMacで文字化けするため）
	$data .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\">";
	$data .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"non_list\">\n";
	$data .= "<tr height=\"30\">\n";
	$wk_colspan = $day_cnt + 5;
	$data .= "<td colspan=\"$wk_colspan\">";
	$data .= "</td>";

    // ---------------------------------------------------------------
    // 決裁欄の印字選択（初期＝印字する）20131204
    // ---------------------------------------------------------------
    // 構造は変えずに中身と罫線のみ取り除く
    // ---------------------------------------------------------------
    if ($approval_column_print_flg == 't') {
        $data .= "<td rowspan=\"2\" align=\"center\" style=\"border:solid 1px\">";
        $data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">決<br>裁</font>";
        $data .= "</td>";
        for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
            $data .= "<td colspan=\"2\" align=\"center\" style=\"border:solid 1px\">";
            $data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$st_name_array[$st_idx]["st_name"]."</font>";
            $data .= "</td>";
        }
    }
    $data .= "</tr>\n";
	$data .= "<tr height=\"60\">\n";

	if ($err_msg != "") {
		$data .= "<td>";
		$data .= $err_msg;
		$data .= "</td></tr></table>\n";

		$wk_array = array();
		$wk_array["data"] = $data;

		return $wk_array;
	}

	$wk_title = "<u>$group_name</u>  ";
	$wk_title .= $duty_yyyy."年";
	$wk_title .= $duty_mm."月　　";

	$data .= "<td nowrap align=\"left\" colspan=\"$wk_colspan\"><b><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . $wk_title . "</font><font size=\"5\">".$print_title."</font></b></td>\n";
    if ($approval_column_print_flg == 't') {	// 決裁欄の印字選択
        for ($st_idx=0; $st_idx<count($st_name_array); $st_idx++) {
            $data .= "<td colspan=\"2\" style=\"border:solid 1px\">";
            $data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"> </font>";
            $data .= "</td>";
        }
    }

	//		$data .= "<td nowrap align=\"right\" colspan=\"17\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成年月日";
	//		$data .= $create_yyyy . "年";
	//		$data .= $create_mm . "月";
	//		$data .= $create_dd . "日";
	//		$data .= "</font></td>\n";
	$data .= "</tr>\n";

    //当初予定印字の場合
    if ($disp_tosYotei_flg == "t") {
        $data .="<tr><td>※予定は当初予定です</td></tr>";
    }

    $data .= "</table>\n";

	///------------------------------------------------------------------------------
	//データ設定
	///------------------------------------------------------------------------------
	$wk_array = array();
	$wk_array["data"] = $data;

	return $wk_array;
}

// 以下の関数はduty_shift_print_common_class.phpからの流用のため、修正がある場合は同時に行う
// 事由の出力について、印刷とは異なる
/*************************************************************************/
//
// 見出し（日段）
//
/*************************************************************************/

function showTitleWeek(
	$obj,
	$duty_yyyy,			//勤務年
	$duty_mm,			//勤務月
	$day_cnt,			//日数
	$plan_array,		//勤務シフト情報
	$week_array,		//週情報
	$title_gyo_array,	//行合計タイトル情報
	$calendar_array,	//カレンダー情報
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$show_data_flg,		//２行目表示データフラグ（１：勤務実績、２：勤務希望、３：当直、４：コメント）
	$hol_dt_flg,		//休暇日数詳細 1:表示 "":非表示
    $title_hol_array,	//休暇タイトル情報
    $wk_group,         		// チーム表示フラグ等
    $unit_name          	// ユニット名
	){
	$reason_2_array = $obj->get_reason_2();

	$data = "";
	$data .= "<table width=\"250\" id=\"header\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";

	//-------------------------------------------------------------------------------
	//日段
	//-------------------------------------------------------------------------------
	$data .= "<tr height=\"22\"> \n";
	$data .= "<td width=\"20\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">番号</font></td> \n";
    if($wk_group["team_disp_flg"] == 't') {
        $data .= "<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">チーム</font></td> \n";
    }

    if($wk_group["unit_disp_flg"] == 't') {
        $data .= "<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$unit_name</font></td> \n";
    }
    
    $data .= "<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">氏名</font></td> \n";

    if($wk_group["job_disp_flg"] == 't') {
        $data .= "<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">職種</font></td> \n";
    }

    if($wk_group["st_disp_flg"] == 't') {
        $data .= "<td width=\"40\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">役職</font></td> \n";
    }

    if($wk_group["es_disp_flg"] == 't') {
        $data .= "<td width=\"100\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">雇用・勤務形態</font></td> \n";
    }

	$data .= "<td width=\"32\" align=\"center\" rowspan=\"2\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font></td> \n";
	//勤務状況（日）
	for ($k=1; $k<=$day_cnt; $k++) {
		$wk_date = $calendar_array[$k-1]["date"];
		$wk_dd = (int)substr($wk_date,6,2);
//*		$wk_width = 20;
//*		$data .= "<td width=\"$wk_width\" align=\"center\" colspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_dd</font></td>\n";

		$wk_back_color = "";
		if ($week_array[$k]["name"] == "土"){
			$wk_back_color = "#E0FFFF";
		} else if ($week_array[$k]["name"] == "日") {
			$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
			//			$wk_back_color = "#FFF0F5";
		} else {
			if (($calendar_array[$k-1]["type"] == "4") ||
					($calendar_array[$k-1]["type"] == "5") ||
					($calendar_array[$k-1]["type"] == "6")) {
				$wk_back_color = "#C8FFD5";
			}
		}

		$data .= "<td width=\"20\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_dd</font></td>\n";
	}
	//集計行列の表示をする場合
	if ($total_print_flg == "1") {
		//個人日数
		for ($k=0; $k<count($title_gyo_array); $k++) {
			$wk = $title_gyo_array[$k]["name"];
			if ($title_gyo_array[$k]["id"] == "10") {
				//公休、有給表示
				$data .= "<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n";
				$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">公休</font></td> \n";
				$data .= "<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n";
				$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">年休有休</font></td> \n";
				$wk .= "計";
			}
			$data .= "<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n";
			$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
		}
	}
	//休暇詳細
	if ($hol_dt_flg == "1") {
		for ($k=0; $k<count($title_hol_array); $k++) {
			//公休・年休非表示
			if ($title_hol_array[$k]["reason"] == "1" ||
					$title_hol_array[$k]["reason"] == "24") {
				continue;
			}
			$wk = $title_hol_array[$k]["name"];
			$data .= "<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n";
			$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
		}
		$wk = "年休有休残";
		$data .= "<td width=\"30\" align=\"center\" rowspan=\"2\" style=\"word-break:break-all;\"> \n";
		$data .= "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
	}
	$data .= "</tr> \n";
	//-------------------------------------------------------------------------------
	//曜日段
	//-------------------------------------------------------------------------------
	$data .= "<tr height=\"22\"> \n";
	//勤務状況（曜日）
	for ($k=1; $k<=$day_cnt; $k++) {
		//土、日、祝日の文字色＆背景色変更
		$wk_color = "";
		$wk_back_color = "";
		if ($week_array[$k]["name"] == "土"){
			$wk_color = "#0000ff";
			$wk_back_color = "#E0FFFF";
		} else if ($week_array[$k]["name"] == "日") {
			$wk_color = "#ff0000";
			$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
//			$wk_back_color = "#FFF0F5";
		} else {
			if (($calendar_array[$k-1]["type"] == "4") ||
					($calendar_array[$k-1]["type"] == "5") ||
					($calendar_array[$k-1]["type"] == "6")) {
				$wk_color = "#ff0000";
				$wk_back_color = "#C8FFD5";
			}
		}
		//表示
		$wk_name = $week_array[$k]["name"];
//*		$wk_width = 20;
//*		$data .= "<td width=\"$wk_width\" align=\"center\" colspan=\"1\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=$wk_color>$wk_name</font></td> \n";
		$data .= "<td width=\"20\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=$wk_color>$wk_name</font></td> \n";
	}
	$data .= "</tr> \n";
	//		$data .= "</table> \n";

	return $data;
}
/*************************************************************************/
//
// 表データ
//
/*************************************************************************/
function showList(
	$obj,
	$obj_duty,
	$pattern_id,			//出勤パターンＩＤ
	$plan_array,			//勤務シフト情報
	$title_gyo_array,		//行合計タイトル情報
	$gyo_array,				//行合計情報
	$start_cnt,				//行表示開始位置
	$end_cnt,				//行表示終了位置
	$day_cnt,				//日数
	$max_font_cnt,			//最大文字数
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$show_data_flg,			//２行目表示データフラグ（""：予定のみ、１：勤務実績、２：勤務希望、３：当直、４：コメント）
	$group_id,
	$reason_setting_flg,
	$hol_dt_flg,			//休暇日数詳細 1:表示 "":非表示
	$title_hol_array,		//休暇タイトル情報
	$hol_cnt_array,			//休暇日数
	$nenkyu_array,			//年休情報
	$calendar_array,
    $week_array,
    $wk_group				// チーム表示フラグ等
	){
	// 当日
	$today = Date("Ymd");

	//		$wk_height = 22 * (int)$max_font_cnt;
	// 高さを22固定とする2009/01/06
	$wk_height = 22;
	//		$wk_height = 66;
	$reason_2_array = $obj->get_reason_2();

	//当直パターン取得
	$duty_pattern =	$obj_duty->get_duty_pattern_array();

	$data = "";
	//		$data .= "<table width=\"250\" id=\"data\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";

	// 役職・職種・雇用勤務形態の履歴情報 20130219
	$history_info = $obj->get_history_info($calendar_array, $wk_group);

	for ($i=$start_cnt; $i<$end_cnt; $i++) {
		//*******************************************************************************
		// 予定データ
		//*******************************************************************************
		$data .= "<tr height=\"$wk_height\"> \n";
		if ($show_data_flg != "") {
			$span = "2";
		} else {
			$span = "1";
		}
		//-------------------------------------------------------------------------------
		// 表示順
		//-------------------------------------------------------------------------------
		$wk = $plan_array[$i]["no"];
		$data .= "<td width=\"20\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
		//-------------------------------------------------------------------------------
		// チーム
		//-------------------------------------------------------------------------------
        if ($wk_group["team_disp_flg"] == 't') {
            // チームの背景色
            $wk_color = "";
            if ($plan_array[$i]["other_staff_flg"] == "1") {
                //他病棟スタッフ
                $wk_color = "#dce8bb";
            }
            $wk = $plan_array[$i]["team_name"];
            $data .= "<td width=\"40\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>";
        }
        //-------------------------------------------------------------------------------
        // ユニット
        //-------------------------------------------------------------------------------
        if ($wk_group["unit_disp_flg"] == 't') {
            $staff_id = $plan_array[$i]["staff_id"];
            $wk_color = "000000";
            // ユニット表示仕変
            $wk = $obj->get_emp_unitname($plan_array[$i]["staff_id"], $group_id);
            $data .= "<td width=\"100\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\" >$wk</font></td>\n";
        }
        //-------------------------------------------------------------------------------
		// 氏名
		//-------------------------------------------------------------------------------
		// 氏名の背景色
		$wk_color = "";
		if ($plan_array[$i]["other_staff_flg"] == "1") {
			$wk_color = "#dce8bb"; //他病棟スタッフ
		}
		// 氏名
		$wk = $plan_array[$i]["staff_name"];
		$color = ( $plan_array[$i]["sex"] == 1 ) ? 'blue' : 'black';
		$data .= "<td id=\"$wk_td_id\" width=\"100\" align=\"center\" rowspan=\"$span\" bgcolor=\"$wk_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$color\">$wk</font></td>\n";
		//-------------------------------------------------------------------------------
		// 職種
		//-------------------------------------------------------------------------------
        if ($wk_group["job_disp_flg"] == 't') {
            $staff_id = $plan_array[$i]["staff_id"];
            
            //職種履歴を考慮する 20130219
            $arr_name_color = $obj->get_show_name($history_info["job_id_info"][$staff_id], $history_info["job_id_name"], "job_id", $history_info["start_date"], $history_info["end_date"]);
            $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["job_name"];
            $wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black";
            
            $data .= "<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\" >$wk</font></td>\n";
        }
		//-------------------------------------------------------------------------------
		// 役職
		//-------------------------------------------------------------------------------
        if ($wk_group["st_disp_flg"] == 't') {
            $staff_id = $plan_array[$i]["staff_id"];
            //役職履歴を考慮する 20130219
            $arr_name_color = $obj->get_show_name($history_info["st_id_info"][$staff_id], $history_info["st_id_name"], "st_id", $history_info["start_date"], $history_info["end_date"]);
            $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $plan_array[$i]["status"];
            $wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black";
            $data .= "<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\" >$wk</font></td>\n";
        }
		//-------------------------------------------------------------------------------
		// 雇用・勤務形態 20130219
		//-------------------------------------------------------------------------------
        if ($wk_group["es_disp_flg"] == 't') {
            $arr_wk = array("1"=>"常勤","2"=>"非常勤","3"=>"短時間正職員");
            $buf_wk = $plan_array[$i]["duty_form"];
            $arr_name_color = $obj->get_show_name($history_info["duty_form_info"][$staff_id], $arr_wk, "duty_form", $history_info["start_date"], $history_info["end_date"]);
            $wk = ($arr_name_color["name"] !="") ? $arr_name_color["name"] : $arr_wk[$buf_wk];
            $wk_color = ($arr_name_color["color"] !="") ? $arr_name_color["color"] : "black";
            $data .= "<td width=\"40\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_color\" >$wk</font></td>\n";
        }
		$data .= "<td width=\"32\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">予定</font></td>\n";
		//-------------------------------------------------------------------------------
		// 勤務状況
		//-------------------------------------------------------------------------------
		for ($k=1; $k<=$day_cnt; $k++) {

			//勤務シフト予定（表示）
			if ($plan_array[$i]["pattern_id_$k"] == $pattern_id &&
					($plan_array[$i]["assist_group_$k"] == "" ||
						$plan_array[$i]["assist_group_$k"] == $group_id)) {
				$wk_font_color = $plan_array[$i]["font_color_$k"];
				$wk_back_color = $plan_array[$i]["back_color_$k"];
			} else {
				$wk_font_color = "#C0C0C0";		//灰色
				$wk_back_color = "";
			}

			//*曜日で背景色設定
			if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF"){
				if ($week_array[$k]["name"] == "土"){
					$wk_back_color = "#E0FFFF";
				} else if ($week_array[$k]["name"] == "日") {
//*					$wk_back_color = "#FFF0F5";
					$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
				} else {
					if (($calendar_array[$k-1]["type"] == "4") || ($calendar_array[$k-1]["type"] == "5") || ($calendar_array[$k-1]["type"] == "6")) {
						$wk_back_color = "#C8FFD5";
					}
				}
			}

			//名称
			$wk = $plan_array[$i]["font_name_$k"];
			//2008/11/27 改修
			if ($plan_array[$i]["reason_2_$k"] != "" &&
					$reason_setting_flg == "t") {
				for ($m=0; $m<count($reason_2_array); $m++) {
					if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"]) {
						$wk_reason_name = $reason_2_array[$m]["font_name"];
						$wk .= $wk_reason_name;
						break;
					}
				}
			}
//*			$wk_width = 20;
			$data .= "<td width=\"20\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_font_color\" >$wk</font></td>\n";
		}
		//集計行列の表示をする場合
		if ($total_print_flg == "1") {
			if ($show_data_flg == "3" || $show_data_flg == "4") {
				$span = "2";
			} else {
				$span = "1";
			}
			//-------------------------------------------------------------------------------
			//個人日数
			//-------------------------------------------------------------------------------
			//公休・年休表示
			$wk_koukyu_cnt = 0;
			$wk_nenkyu_cnt = 0;
			for ($k=0; $k<count($title_hol_array); $k++) {
				if ($title_hol_array[$k]["reason"] == "1") {
					$wk_nenkyu_cnt = $hol_cnt_array[$i][$k];
				}
				if ($title_hol_array[$k]["reason"] == "24") {
					$wk_koukyu_cnt = $hol_cnt_array[$i][$k];
				}
			}
			for ($k=0; $k<count($title_gyo_array); $k++) {
				$wk = $gyo_array[$i][$k];
				if ($title_gyo_array[$k]["id"] == "10") {
					//公休、有休表示
					$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td> \n";
					$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td> \n";
					$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
				}
				$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
			}
		}
		//休暇日数詳細
		if ($hol_dt_flg == "1") {
			if ($show_data_flg == "3" || $show_data_flg == "4") {
				$span = "2";
			} else {
				$span = "1";
			}
			for ($k=0; $k<count($title_hol_array); $k++) {
				//公休・年休非表示
				if ($title_hol_array[$k]["reason"] == "1" ||
						$title_hol_array[$k]["reason"] == "24") {
					continue;
				}
				$wk = $hol_cnt_array[$i][$k];
				$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
			}
			$wk = $nenkyu_array["plan"][$i];
			$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
		}
		$data .= "</tr> \n";
		//*******************************************************************************
		// 実績データ
		//*******************************************************************************
		if ($show_data_flg != "") {
			$data .= "<tr height=\"$wk_height\"> \n";
			switch ($show_data_flg) {
				case "1":
					$tmp_title = "実績";
					break;
				case "2":
					$tmp_title = "希望";
					break;
				case "3":
					$tmp_title = $obj->duty_or_oncall_str;
					break;
				case "4":
					$tmp_title = "コメント";
					break;
				default:
					$tmp_title = "";
			}
			$data .= "<td width=\"32\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_title</font></td>\n";
			//勤務状況（実績）
			for ($k=1; $k<=$day_cnt; $k++) {

				//コメントの場合
				if ($show_data_flg == "4") {
					$wk = $plan_array[$i]["comment_$k"];
					$wk_font_color = "#000000";
					$wk_back_color = "#ffffff";
				} else
					if ($show_data_flg != "3") {
						// 当日以前の場合に表示、希望は無条件に表示
						//$duty_ymd = $plan_array[$i]["year"] . sprintf("%02d", $plan_array[$i]["month"]) . sprintf("%02d", $k);
						$duty_ymd = $calendar_array[$k-1]["date"]; //前月から当月が作成期間の場合、出力されない不具合対応 20111027
						if ($duty_ymd <= $today || $show_data_flg == "2") {

							//文字、背景色
							if ($plan_array[$i]["rslt_pattern_id_$k"] == $pattern_id) {
								$wk_font_color = $plan_array[$i]["rslt_font_color_$k"];
								$wk_back_color = $plan_array[$i]["rslt_back_color_$k"];
							} else {
								$wk_font_color = "#C0C0C0";		//灰色
								$wk_back_color = "";
							}
							//勤務実績
							$wk = $plan_array[$i]["rslt_name_$k"];
							//2008/11/27 改修
							if ($plan_array[$i]["rslt_reason_2_$k"] != "" &&
									$reason_setting_flg == "t") {
								for ($m=0; $m<count($reason_2_array); $m++) {
									if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"]) {
										$wk_reason_name = $reason_2_array[$m]["font_name"];
										$wk .= $wk_reason_name;
										break;
									}
								}
							}
						} else {
							$wk_font_color = "";
							$wk_back_color = "";
							$wk = "&nbsp;"; // "_" -> " "
						}
					} else {
						$wk = "&nbsp;"; // "_" -> " "
						$wk_font_color = "";
						$wk_back_color = "";
						for ($m=0; $m<count($duty_pattern); $m++) {
							if ($plan_array[$i]["night_duty_$k"] == $duty_pattern[$m]["id"]) {
								$wk = $duty_pattern[$m]["font_name"];	//表示文字名
								$wk_font_color = $duty_pattern[$m]["font_color"];
								$wk_back_color = $duty_pattern[$m]["back_color"];
							}
						}
					}

					//*曜日で背景色設定
					if ($wk_back_color=="" || strtoupper($wk_back_color)=="#FFFFFF"){
						if ($week_array[$k]["name"] == "土"){
							$wk_back_color = "#E0FFFF";
						} else if ($week_array[$k]["name"] == "日") {
							//*					$wk_back_color = "#FFF0F5";
							$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
						} else {
							if (($calendar_array[$k-1]["type"] == "4") || ($calendar_array[$k-1]["type"] == "5") || ($calendar_array[$k-1]["type"] == "6")) {
								$wk_back_color = "#C8FFD5";
							}
						}
					}

				$wk_width = 20;
				$data .= "<td width=\"$wk_width\" align=\"center\" colspan=\"1\" bgcolor=\"$wk_back_color\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"$wk_font_color\" >$wk</font></td>\n";
			}
			//２段目の集計
			if ($total_print_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
				$gyo_array_cnt = count($title_gyo_array);
				$hol_array_cnt = count($title_hol_array);

				//公休・年休表示
				$wk_koukyu_cnt = 0;
				$wk_nenkyu_cnt = 0;
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休表示
					if ($title_hol_array[$k]["reason"] == "1") {
						$wk_nenkyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						$wk_koukyu_cnt = $hol_cnt_array[$i][$hol_array_cnt + $k];
					}
				}
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = $gyo_array[$i][$gyo_array_cnt + $k];
					if ($title_gyo_array[$k]["id"] == "10") {
						$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td> \n";
						$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td> \n";
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
				}
			}
			//休暇日数詳細
			if ($hol_dt_flg == "1" && ($show_data_flg == "1" || $show_data_flg == "2")) {
				if ($show_data_flg == "3" || $show_data_flg == "4") {
					$span = "2";
				} else {
					$span = "1";
				}
				$gyo_array_cnt = count($title_hol_array);
				for ($k=0; $k<count($title_hol_array); $k++) {
					//公休・年休非表示
					if ($title_hol_array[$k]["reason"] == "1" ||
							$title_hol_array[$k]["reason"] == "24") {
						continue;
					}
					$wk = $hol_cnt_array[$i][$gyo_array_cnt + $k];
					$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
				}
				if ($show_data_flg == "1") {
				    $wk = $nenkyu_array["result"][$i];
				}  elseif ($show_data_flg == "2") {
				    $wk = $nenkyu_array["hope"][$i];
				}
				$data .= "<td width=\"30\" align=\"center\" rowspan=\"$span\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td> \n";
			}
			$data .= "</tr> \n";
		}
	}

	return $data;
}
/*************************************************************************/
//
// 列計
//
/*************************************************************************/
function showTitleRetu(
	$obj,
	$day_cnt,				//日数
	$plan_array,			//勤務シフト情報
	$title_gyo_array,		//行合計タイトル情報
	$gyo_array,				//行合計情報
	$title_retu_array,		//列合計タイトル情報
	$retu_array,			//列合計情報
	$total_gyo_flg="1",		// 2:2段 2以外:1段
	$total_print_flg="1",	//集計行列の印刷フラグ 0しない 1:する
	$hol_dt_flg,			//休暇日数詳細 1:表示 "":非表示
	$title_hol_array,		//休暇タイトル情報
	$hol_cnt_array,			//休暇日数
	$nenkyu_array,			//年休情報
	$calendar_array,
	$week_array,
    $show_data_flg,
    $wk_group				// チーム表示フラグ等
	){
	$reason_2_array = $obj->get_reason_2();

	$data = "";
	$data_cnt = count($plan_array);

	for ($i=0; $i<count($title_retu_array); $i++) {
		$data .= "<tr height=\"22\"> \n";
		$data .= "<td width=\"20\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n";	// 番号
        if($wk_group["team_disp_flg"] == 't') {
            $data .= "<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n";
        }
        
        if($wk_group["unit_disp_flg"] == 't') {
            $data .= "<td width=\"100\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n";
        }

        //-------------------------------------------------------------------------------
		// 名称
		//-------------------------------------------------------------------------------
		$wk = $title_retu_array[$i]["name"];
		$data .= "<td width=\"100\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
		//-------------------------------------------------------------------------------
		// 総合計
		//-------------------------------------------------------------------------------
		$wk_cnt = 0;
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk_cnt += $retu_array[$k][$i];
		}
		if($wk_group["job_disp_flg"] == 't') {
		    $data .= "<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n";
		}

		if($wk_group["st_disp_flg"] == 't') {
		    $data .= "<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n";
		}

		if($wk_group["es_disp_flg"] == 't') {
		    $data .= "<td width=\"40\" align=\"center\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">　</font></td>\n"; //雇用勤怠形態 20130219
        }

        $data .= "<td width=\"32\" align=\"right\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_cnt</font></td>\n";
		//-------------------------------------------------------------------------------
		// 日ごとの合計
		//-------------------------------------------------------------------------------
		for ($k=1; $k<=$day_cnt; $k++) {
			$wk = $retu_array[$k][$i];
			$wk_width = 20;
			for ($m=0; $m<count($reason_2_array); $m++) {
				if ($plan_array[$i]["reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
					$wk_width = 30;
					break;
				}
				if ($plan_array[$i]["rslt_reason_2_$k"] == $reason_2_array[$m]["id"] && $reason_2_array[$m]["day_count"] == 0.5) {
					$wk_width = 30;
					break;
				}
			}

			//*曜日で背景色設定
			$wk_back_color = "";
			if ($week_array[$k]["name"] == "土"){
				$wk_back_color = "#E0FFFF";
			} else if ($week_array[$k]["name"] == "日") {
				//*					$wk_back_color = "#FFF0F5";
				$wk_back_color = "#FF99CC";		//Excel2003では 薄いピンク『#FFF0F5』が無いのでローズ『#FF99CC』で代替
			} else {
				if (($calendar_array[$k-1]["type"] == "4") || ($calendar_array[$k-1]["type"] == "5") || ($calendar_array[$k-1]["type"] == "6")) {
					$wk_back_color = "#C8FFD5";
				}
			}

//			$data .= "<td width=\"$wk_width\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
			$data .= "<td width=\"$wk_width\" align=\"center\" bgcolor=\"$wk_back_color\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
		}
		//-------------------------------------------------------------------------------
		// 個人日数
		//-------------------------------------------------------------------------------
		if ($total_print_flg == "1") {
			if (($total_gyo_flg != "2" && $i == 0) || ($total_gyo_flg == "2" && $i <= 1)) {
				$wk_nenkyu_cnt = 0;
				$wk_koukyu_cnt = 0;
				//集計
				for ($k=0; $k<count($title_hol_array); $k++) {
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_hol_array) + $k;
					}

					if ($title_hol_array[$k]["reason"] == "1") {
						for ($m=0; $m<$data_cnt; $m++) {
							$wk_nenkyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
					if ($title_hol_array[$k]["reason"] == "24") {
						for ($m=0; $m<$data_cnt; $m++) {
							$wk_koukyu_cnt += $hol_cnt_array[$m][$k2];
						}
					}
				}

				// 個人日数の列合計
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$wk = 0;
					if ($i == 0) {
						$k2 = $k;
					} else {
						$k2 = count($title_gyo_array) + $k;
					}
					for ($m=0; $m<count($gyo_array); $m++) {
						$wk = $wk + $gyo_array[$m][$k2];
					}
					//公休、有休表示
					if ($title_gyo_array[$k]["id"] == "10") {
						$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_koukyu_cnt</font></td>\n";
						$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk_nenkyu_cnt</font></td>\n";
						$wk = $wk - $wk_nenkyu_cnt - $wk_koukyu_cnt;
					}
					$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
				}
				//休暇詳細
				if ($hol_dt_flg == "1") {
					for ($k=0; $k<count($title_hol_array); $k++) {
						//公休・年休非表示
						if ($title_hol_array[$k]["reason"] == "1" ||
								$title_hol_array[$k]["reason"] == "24") {
							continue;
						}
						$wk = 0;
						if ($i == 0) {
							$k2 = $k;
						} else {
							$k2 = count($title_hol_array) + $k;
						}
						for ($m=0; $m<$data_cnt; $m++) {
							$wk = $wk + $hol_cnt_array[$m][$k2];
						}
						$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$wk</font></td>\n";
					}
					if ($i == 0) {
					    $wk_nenkyu_array = $nenkyu_array["plan"];
					} elseif ($show_data_flg == "1") {
					    $wk_nenkyu_array = $nenkyu_array["result"];
					} elseif ($show_data_flg == "2") {
					    $wk_nenkyu_array = $nenkyu_array["hope"];
					}
					$wk = 0;
					foreach ($wk_nenkyu_array as $value) {
					    $wk += $value;
					}
					$data .= "<td width=\"30\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"></font>$wk</td>\n";
				}

			} else {
				// ブランク（個人日数用）
				for ($k=0; $k<count($title_gyo_array); $k++) {
					$data .= "<td width=\"30\" align=\"center\" style=\"border:none;\"></td>\n";
				}
			}

		}
		$data .= "</tr>\n";
	}
	return $data;
}

?>

