<?
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("holiday.php");

//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_USER, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$order = isset($_REQUEST["order"]) ? $_REQUEST["order"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//ユーザーID
//==============================
$emp_id = get_emp_id($con,$session,$fname);

//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	if($postback_mode == "ward_changed")
	{
		set_user_selected_bldg($con,$fname,$emp_id,$bldg_cd,$ward_cd);
	}
}


//==============================
//初期表示する病棟を特定
//==============================
if($is_postback != "true")
{
	$user_selected_bldg = get_user_selected_bldg($con,$fname,$emp_id);
	$bldg_cd = "";
	$ward_cd = "";
	if($user_selected_bldg != null)
	{
		$bldg_cd = $user_selected_bldg['bldg_cd'];
		$ward_cd = $user_selected_bldg['ward_cd'];
	}
	if($bldg_cd == "")
	{
		$top_ward = get_top_ward($con,$fname);
		$bldg_cd = $top_ward['bldg_cd'];
		$ward_cd = $top_ward['ward_cd'];
	}
	if($ward_cd == "")
	{
		$sql  = " select min(ward_cd) as ward_cd from";
		$sql .= " (";
		$sql .= get_bldg_ward_room_sql();
		$sql .= " ) bldg_ward_room_sql";
		$sql .= " where bldg_cd = {$bldg_cd}";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$ward_cd = pg_fetch_result($sel,0,'ward_cd');
	}
}

//==============================
//日付、時間帯
//==============================
if($is_postback != "true")
{
	$target_date = date("Ymd");
	$target_time = get_now_time();

	if($target_time == 1)
	{
		$ward_setting = get_ward_setting($con, $fname, $bldg_cd, $ward_cd);
		if($ward_setting['time1_must_flg'] == "f")
		{
			$target_time = 2;
		}
	}
	if($target_time == 3)
	{
		$ward_setting = get_ward_setting($con, $fname, $bldg_cd, $ward_cd);
		if($ward_setting['time3_must_flg'] == "f")
		{
			$target_time = 2;
		}
	}
}

$target_date_y = substr($target_date,0,4);
$target_date_m = substr($target_date,4,2);
$target_date_d = substr($target_date,6,2);


$week_day_list = array('日','月','火','水','木','金','土');

//日付一覧
$date_list = null;
$date_w_list = null;
$date_color_list = null;
for($i=6;$i>=0;$i--)
{
	$tmp_datetime = mktime(0, 0, 0, $target_date_m, $target_date_d - $i, $target_date_y);
	$tmp_date = date("Ymd",$tmp_datetime);
	$tmp_date_w = $week_day_list[date("w", $tmp_datetime)];

	//今日
	if($tmp_date == date("Ymd"))
	{
		$tmp_date_color = "#ccffcc";
	}
	//日祝
	elseif((get_holiday_name($tmp_date) != "") || $tmp_date_w == '日')
	{
		$tmp_date_color = "#fadede";
	}
	//土
	elseif($tmp_date_w == '土')
	{
		$tmp_date_color = "#defafa";
	}
	//平日
	else
	{
		$tmp_date_color = "#fefcdf";
	}

	$date_list[] = $tmp_date;
	$date_w_list[] = $tmp_date_w;
	$date_color_list[] = $tmp_date_color;
}




//日付、時間帯の移動
$prev_date_1d = date("Ymd",mktime(0, 0, 0, $target_date_m, $target_date_d - 1, $target_date_y));
$prev_date_1w = date("Ymd",mktime(0, 0, 0, $target_date_m, $target_date_d - 7, $target_date_y));
$prev_date_1m = date("Ymd",mktime(0, 0, 0, $target_date_m - 1, $target_date_d, $target_date_y));
$next_date_1d = date("Ymd",mktime(0, 0, 0, $target_date_m, $target_date_d + 1, $target_date_y));
$next_date_1w = date("Ymd",mktime(0, 0, 0, $target_date_m, $target_date_d + 7, $target_date_y));
$next_date_1m = date("Ymd",mktime(0, 0, 0, $target_date_m + 1, $target_date_d, $target_date_y));

//未来の評価を行うと退院転棟による影響を受けやすいため、アクセス禁止
if($next_date_1d > date("Ymd"))
{
	$next_date_1d = date("Ymd");
}
if($next_date_1w > date("Ymd"))
{
	$next_date_1w = date("Ymd");
}
if($next_date_1m > date("Ymd"))
{
	$next_date_1m = date("Ymd");
}

//2008/04/01以前は評価票の定義がないため、アクセス禁止
if($prev_date_1d < "20080407")
{
	$prev_date_1d = "20080407";
}
if($prev_date_1w < "20080407")
{
	$prev_date_1w = "20080407";
}
if($prev_date_1m < "20080407")
{
	$prev_date_1m = "20080407";
}

$is_next_date_move_able = ($target_date < date("Ymd"));
$is_prev_date_move_able = ($target_date > "20080407");



//==============================
//入棟情報
//==============================

//	//全患者の入棟情報
//	$all_inpt_info =  get_all_inpt_info($con,$fname);
//
//	//表示する期間の、病棟の患者の入棟情報
//	$wk_data = null;
//	foreach($all_inpt_info as $all_inpt_info1)
//	{
//		if(
//			    $date_list[6] >= $all_inpt_info1['inpt_in_dt']
//			 && $date_list[0] <= $all_inpt_info1['inpt_out_dt']
//			 && $bldg_cd == $all_inpt_info1['bldg_cd']
//			 && $ward_cd == $all_inpt_info1['ward_cd']
//			)
//		{
//			$wk_data[] = $all_inpt_info1;
//		}
//	}
//	$bldg_date_all_inpt_info = $wk_data;
//	$bldg_date_all_inpt_info = usort_pg_fetch_all($bldg_date_all_inpt_info,array("ward_cd","ptrm_room_no","inpt_bed_no","ptif_id"));



//表示する期間の、病棟の患者の入棟情報
$all_inpt_info = get_all_inpt_info($con,$fname,$date_list[0],$date_list[6]);
$wk_data = null;
foreach($all_inpt_info as $all_inpt_info1)
{
	if(
		    $bldg_cd == $all_inpt_info1['bldg_cd']
		 && $ward_cd == $all_inpt_info1['ward_cd']
		)
	{
		$wk_data[] = $all_inpt_info1;
	}
}
$bldg_date_all_inpt_info = $wk_data;
$bldg_date_all_inpt_info = usort_pg_fetch_all($bldg_date_all_inpt_info,array("ward_cd","ptrm_room_no","inpt_bed_no","ptif_id"));


//==============================
//評価記録情報
//==============================
$sql  = " select * from nlcs_eval_record where input_date >= '{$date_list[0]}' and input_date <= '{$date_list[6]}' and input_time = '{$target_time}' and bldg_cd = {$bldg_cd} and not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_date_eval_record_info = pg_fetch_all_in_array($sel);




//==============================
//病室名情報
//==============================

//病室名
$sql  = " select * from";
$sql .= " (";
$sql .= get_bldg_ward_room_sql();
$sql .= " ) bldg_ward_room_sql";
$sql .= " where bldg_cd = {$bldg_cd}";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$bldg_name_info = pg_fetch_all_in_array($sel);

//==============================
//表示データ
//==============================
$disp_data = array();
foreach($bldg_date_all_inpt_info as $bldg_date_all_inpt_info1)
{
	//病棟名・病室名
	$ward_name = "";
	$ptrm_name = "";
	foreach($bldg_name_info as $bldg_name_info1)
	{
		if(
			   $bldg_date_all_inpt_info1['ward_cd'] === $bldg_name_info1['ward_cd']
			&& $bldg_date_all_inpt_info1['ptrm_room_no'] === $bldg_name_info1['ptrm_room_no']
			)
		{
			$ward_name = $bldg_name_info1['ward_name'];
			$ptrm_name = $bldg_name_info1['ptrm_name'];
		}
	}

	//患者氏名
	$pt_id = $bldg_date_all_inpt_info1['ptif_id'];
	$pt_name = "";
	$pt_keywd = "";
	if ($pt_id != "") {
		$sql = "select ptif_lt_kaj_nm || ' ' || ptif_ft_kaj_nm as pt_name, ptif_keywd from ptifmst where ptif_id = '{$pt_id}'";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$pt_name = pg_fetch_result($sel,0,'pt_name');
		$pt_keywd = pg_fetch_result($sel,0,'ptif_keywd');
	}

	//記録結果
	$result_list = array();
	foreach($date_list as $input_date)
	{
		$is_inpt = false;
		if($bldg_date_all_inpt_info1['inpt_in_dt'] <= $input_date && $bldg_date_all_inpt_info1['inpt_out_dt'] >= $input_date)
		{
			$is_inpt = true;
		}

		if(!$is_inpt)
		{
			$result = "-";//未入棟
		}
		else
		{
			$input_state = 0;//未入力
			$pt_class_level = "";
			foreach($bldg_date_eval_record_info as $bldg_date_eval_record_info1)
			{
				if($bldg_date_eval_record_info1['pt_id'] === $pt_id && $bldg_date_eval_record_info1['input_date'] === $input_date)
				{
					$input_state = $bldg_date_eval_record_info1['input_state'];
					$pt_class_level = $bldg_date_eval_record_info1['pt_class_level'];
					break;
				}
			}

			if($input_state == 0)
			{
				$result = "未入力";
			}
			elseif($input_state == 2)
			{
				$result = "保留";
			}
			elseif($input_state == 3)
			{
				$result = "不在";
			}
			else//if($input_state == 1)
			{
				$result = $pt_class_level;
			}
		}

		$result_list[] = $result;
	}

	$disp1 = null;
	$disp1['pt_id'] = $pt_id;
	$disp1['ward_name'] = $ward_name;
	$disp1['ptrm_name'] = $ptrm_name;
	$disp1['inpt_bed_no'] = $bldg_date_all_inpt_info1['inpt_bed_no'];
	$disp1['pt_name'] = $pt_name;
	$disp1['pt_keywd'] = $pt_keywd;
	$disp1['result_list'] = $result_list;
	$disp_data[] = $disp1;
}

if ($order == "") {$order = "1";}

switch ($order) {
case "1":
	usort($disp_data, 'sort_by_bed');
	break;
case "2":
	usort($disp_data, 'sort_by_bed_desc');
	break;
case "3":
	usort($disp_data, 'sort_by_id');
	break;
case "4":
	usort($disp_data, 'sort_by_id_desc');
	break;
case "5":
	usort($disp_data, 'sort_by_name');
	break;
case "6":
	usort($disp_data, 'sort_by_name_desc');
	break;
}

//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "入院患者一覧";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">

//画面起動時の処理
function initPage()
{
	init_ward_select();
}

function date_changed(change_mode)
{
	switch(change_mode)
	{
		case "p1d":
			document.mainform.target_date.value = "<?=$prev_date_1d?>";
			break;
		case "p1w":
			document.mainform.target_date.value = "<?=$prev_date_1w?>";
			break;
		case "p1m":
			document.mainform.target_date.value = "<?=$prev_date_1m?>";
			break;
		case "n1d":
			document.mainform.target_date.value = "<?=$next_date_1d?>";
			break;
		case "n1w":
			document.mainform.target_date.value = "<?=$next_date_1w?>";
			break;
		case "n1m":
			document.mainform.target_date.value = "<?=$next_date_1m?>";
			break;
	}
	document.mainform.postback_mode.value = "date_changed";
	document.mainform.submit();
}


function time_changed(time)
{
	document.mainform.target_time.value = time;
	document.mainform.postback_mode.value = "time_changed";
	document.mainform.submit();
}

//病棟変更時の処理
function ward_changed(bldg_cd,ward_cd)
{
	document.mainform.postback_mode.value = "ward_changed";
	document.mainform.submit();
}

function goto_eval_form(pt_id,input_date)
{
	document.goto_eval_form.pt_id.value = pt_id;
	document.goto_eval_form.input_date.value = input_date;
	document.goto_eval_form.cal_y.value = input_date.substr(0,4);
	document.goto_eval_form.cal_m.value = input_date.substr(4,2);
	document.goto_eval_form.submit();
}




</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname)
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">

<input type="hidden" name="target_date" value="<?=$target_date?>">
<input type="hidden" name="target_time" value="<?=$target_time?>">







<!-- 表示条件 START -->
<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>



<!-- 棟/病棟選択 START -->
<?
show_ward_select($con,$fname,$bldg_cd,$ward_cd);
?>
<!-- 棟/病棟選択 END -->


</td>


<td>
&nbsp;
</td>

</tr>
</table>
<!-- 表示条件 END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 機能タイトル START -->
<table width="800" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="250">
<b>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
看護必要度
</font>
</b>
</td>
<td>
&nbsp;
</td>


<td width="350" align="center">

<!-- 時間帯選択 START -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100" align="right" nowrap>
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<?
	if($is_prev_date_move_able)
	{
		?>
		&nbsp;
		<a href="javascript:date_changed('p1m')">&lt;</a>
		&nbsp;
		<a href="javascript:date_changed('p1w')">&lt;</a>
		&nbsp;
		<a href="javascript:date_changed('p1d')">&lt;</a>
		&nbsp;
		<?
	}
	else
	{
		?>
		&nbsp;
		&lt;
		&nbsp;
		&lt;
		&nbsp;
		&lt;
		&nbsp;
		<?
	}
	?>
	</font>
	</td>
<td width="50" <?if($target_time == 1){echo(" bgcolor='#88FFFF'");}?> align="center">
	<a href="javascript:time_changed(1)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>深夜帯</font>
	</a>
	</td>
<td width="50" <?if($target_time == 2){echo(" bgcolor='#88FFFF'");}?> align="center">
	<a href="javascript:time_changed(2)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>日勤帯</font>
	</a>
	</td>
<td width="50" <?if($target_time == 3){echo(" bgcolor='#88FFFF'");}?> align="center">
	<a href="javascript:time_changed(3)">
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>準夜帯</font>
	</a>
	</td>
<td width="100" align="left" nowrap>
	<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
	<?
	if($is_next_date_move_able)
	{
		?>
		&nbsp;
		<a href="javascript:date_changed('n1d')">&gt;</a>
		&nbsp;
		<a href="javascript:date_changed('n1w')">&gt;</a>
		&nbsp;
		<a href="javascript:date_changed('n1m')">&gt;</a>
		&nbsp;
		<?
	}
	else
	{
		?>
		&nbsp;
		&gt;
		&nbsp;
		&gt;
		&nbsp;
		&gt;
		&nbsp;
		<?
	}
	?>
	</font>
	</td>
</tr>
</table>
<!-- 時間帯選択 END -->

</td>


</tr>
</table>
<!-- 機能タイトル END -->



<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
</table>



<!-- 患者一覧 START -->
<table width='800' border='0' cellspacing='0' cellpadding='1' class="list">
<tr>
<td bgcolor='#bdd1e7' rowspan="2" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href="kango_inpt_list.php?session=<? echo($session); ?>&order=<? if ($order == "1") {echo("2");} else {echo("1");} ?>">部屋</a></font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="40"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>ベッド<br>番号</font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center" width="90"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href="kango_inpt_list.php?session=<? echo($session); ?>&order=<? if ($order == "3") {echo("4");} else {echo("3");} ?>">患者ID</a></font></td>
<td bgcolor='#bdd1e7' rowspan="2" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href="kango_inpt_list.php?session=<? echo($session); ?>&order=<? if ($order == "5") {echo("6");} else {echo("5");} ?>">患者氏名</a></font></td>
<?
foreach($date_list as $i => $date_list1)
{
	$tmp_m = substr($date_list1,4,2) + 0;
	$tmp_d = substr($date_list1,6,2) + 0;
	?>
	<td bgcolor='<?=$date_color_list[$i]?>' width="50" align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$tmp_m?>/<?=$tmp_d?></font></td>
	<?
}
?>
</tr>


<tr>
<?
foreach($date_w_list as $i => $date_w_list1)
{
	?>
	<td bgcolor='<?=$date_color_list[$i]?>' align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$date_w_list1?></font></td>
	<?
}
?>
</tr>

<?
foreach($disp_data as $disp1)
{
	?>
	<tr>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($disp1['ptrm_name'])?></font></td>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($disp1['inpt_bed_no'])?></font></td>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($disp1['pt_id'])?></font></td>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($disp1['pt_name'])?></font></td>

	<?
	foreach($disp1['result_list'] as $i => $result)
	{
		?>
		<td align="center">
			<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
			<?
			if($result == "-")
			{
				?>
				<?=$result?>
				<?
			}
			else
			{
				?>
				<a href="javascript:goto_eval_form('<?=$disp1['pt_id']?>','<?=$date_list[$i]?>')">
				<?=$result?>
				</a>
				<?
			}
			?>
			</font>
			</td>
		<?
	}
	?>
	</tr>
	<?
}
?>


</table>
<!-- 患者一覧 END -->






</form>
<!-- 全体 END -->




<!-- 評価記録入力画面移動用フォーム START -->
<form name="goto_eval_form" action="kango_eval_form_input.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="pt_id" value="">
<input type="hidden" name="input_date" value="">
<input type="hidden" name="input_time" value="<?=$target_time?>">
<input type="hidden" name="cal_y" value="">
<input type="hidden" name="cal_m" value="">
</form>
<!-- 評価記録入力画面移動用フォーム END -->




</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);

function sort_by_bed($pt1, $pt2) {
	if ($pt1["ptrm_name"] != $pt2["ptrm_name"]) {
		return strnatcmp($pt1["ptrm_name"], $pt2["ptrm_name"]);
	}
	return strnatcmp($pt1["inpt_bed_no"], $pt2["inpt_bed_no"]);
}

function sort_by_bed_desc($pt1, $pt2) {
	return sort_by_bed($pt1, $pt2) * -1;
}

function sort_by_id($pt1, $pt2) {
	return strnatcmp($pt1["pt_id"], $pt2["pt_id"]);
}

function sort_by_id_desc($pt1, $pt2) {
	return sort_by_id($pt1, $pt2) * -1;
}

function sort_by_name($pt1, $pt2) {
	return strcmp($pt1["pt_keywd"], $pt2["pt_keywd"]);
}

function sort_by_name_desc($pt1, $pt2) {
	return sort_by_name($pt1, $pt2) * -1;
}
?>
