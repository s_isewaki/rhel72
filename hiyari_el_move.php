<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$auth_id = ($path != "3") ? 47 : 48;
if (check_authority($session, $auth_id, $fname) == "0")
{
	showLoginPage();
	exit;
}


// 登録値の編集
if ($f == "") {$f = null;}

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// トランザクションの開始
//==============================
pg_query($con, "begin");


//==============================
// ログインユーザの職員IDを取得
//==============================
$emp_id = get_emp_id($con, $session, $fname);


//==============================
// コンテンツ情報を取得
//==============================
$sql = "select lib_archive, lib_cate_id, folder_id from el_info";
$cond = "where lib_id = $lib_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$archive = pg_fetch_result($sel, 0, "lib_archive");
$pre_category = pg_fetch_result($sel, 0, "lib_cate_id");
$pre_folder_id = pg_fetch_result($sel, 0, "folder_id");



//==============================
// コンテンツ情報を更新
//==============================
$sql = "update el_info set";
$set = array("lib_cate_id", "folder_id");
$setvalue = array($c, $f);
$cond = "where lib_id = $lib_id";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


//==============================
// 上位フォルダ(カテゴリ)の更新日時を更新
//==============================

//保存元フォルダ
lib_update_folder_modified($con, $pre_category, $pre_folder_id, $emp_id, $fname);

//保存先フォルダ
lib_update_folder_modified($con, $c, $f, $emp_id, $fname);


//==============================
// トランザクションをコミット
//==============================
pg_query($con, "commit");


//==============================
// データベース接続を切断
//==============================
pg_close($con);



//==============================
//物理ファイルの移動
//==============================

// ファイル保存先ディレクトリがなければ作成
create_cate_directory($c);

// ファイルを移動
$pre_cate_dir = get_cate_directory_path($pre_category);
$cate_dir     = get_cate_directory_path($c);
exec("mv {$pre_cate_dir}/document{$lib_id}.* {$cate_dir}");


//==============================
// 一覧画面へ戻る
//==============================
if ($path == "3")
{
	echo("<script type=\"text/javascript\">location.href = 'hiyari_el_admin_menu.php?session=$session&a=$archive&c=$c&f=$f&o=$o';</script>");
}
else
{
	echo("<script type=\"text/javascript\">location.href = 'hiyari_el_list_all.php?session=$session&a=$archive&c=$c&f=$f&o=$o';</script>");
}
?>
