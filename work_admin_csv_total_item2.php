<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
// 集計項目
// work_admin_csv_total_item2.php
$arr_title_no = array("","", "２", "３", "４", "５", "６", "７", "８");
$title_no_str = $arr_title_no[$total_item_no];

//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
    $title2 = "CSV集計項目";
    $title3 = "CSVヘッダータイトル";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
    $title2 = "集計項目";
    $title3 = "ヘッダータイトル";
}
?>
<title>CoMedix <? echo($title1." | ".$title2); ?><? echo($title_no_str); ?>の設定</title>

<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");
require_once("work_admin_csv_common.ini");
require_once("duty_shift_mprint_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$arr_atdptn1 = array();
//atdbk_reason_mstから取得
$arr_atdbk_reason_id_name = get_atdbk_reason_id_name($con, $fname);
$i = 0;
foreach ($arr_atdbk_reason_id_name as $row) {
    $arr_atdptn1[$i]["id"] = "0_".$row["reason_id"];
    $arr_atdptn1[$i]["name"] = "「".$row["reason_name"]."」回数";
    $i++;
}
/*
$i = 0;
$arr_atdptn1[$i]["id"] = "0_6";
$arr_atdptn1[$i]["name"] = "「一般欠勤」回数";
$i++;
$arr_atdptn1[$i]["id"] = "0_7";
$arr_atdptn1[$i]["name"] = "「病傷欠勤」回数";
$i++;
$arr_atdptn1[$i]["id"] = "0_5";
$arr_atdptn1[$i]["name"] = "「特別休暇」回数";
*/
$arr_atdptn2 = get_atdptn_id_name($con, $fname);

$arr_atdptn = array_merge($arr_atdptn1, $arr_atdptn2);


///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------

$arr_ptn_id_list = array();
//登録時
if ($postback == "1") {
    update_ptn_id_list($con, $fname, $ptn_id, $layout_id, $header_title, $coefficient, $total_item_no, $start_day_flg, $end_day_flg, $week_day_flg, $from_flg);
    for ($i=0; $i<count($ptn_id); $i++) {
        $arr_ptn_id_list[$i]["id"] = $ptn_id[$i];
        $arr_ptn_id_list[$i]["coefficient"] = $coefficient[$i];
    }
} else {
    //初期表示時、DBから取得
    $arr_csv_config = get_timecard_csv_config($con, $fname, $layout_id, $from_flg);
    $varname = "total_item_title".$total_item_no;
    $header_title = $arr_csv_config["$varname"];
    $arr_ptn_id_list = get_csv_total_item2($con, $fname, $layout_id, $total_item_no, $from_flg);
    //CSV集計項目設定取得
    $arr_total_setting = get_timecard_csv_total_setting($con, $fname, $layout_id, $total_item_no, $from_flg);
    $start_day_flg = $arr_total_setting[$total_item_no]["start_day_flg"];
    $end_day_flg = $arr_total_setting[$total_item_no]["end_day_flg"];
    $week_day_flg = $arr_total_setting[$total_item_no]["week_day_flg"];
    if ($start_day_flg == "") {
        $start_day_flg = "1";
    }
    if ($end_day_flg == "") {
        $end_day_flg = "1";
    }
    if ($week_day_flg == "") {
        $week_day_flg = "1";
    }
}

//レイアウト名取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, $layout_id);
    $layout_name = $arr_layout[$layout_id]["name"];
}
else {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
    $layout_name = $arr_layout_mst["layout_name"];
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registPtnid() {

	//重複チェック
	var cnt = document.mainform.elements['ptn_id[]'].length;
	if (cnt > 1 ) {
		for (var i=0; i<cnt-1; i++) {
			var val0 = document.mainform.elements['ptn_id[]'][i].value;
			for (var j=i+1; j<cnt; j++) {
				var val1 = document.mainform.elements['ptn_id[]'][j].value;
				if (val0 == val1) {
					var i1 = i + 1;
					var j1 = j + 1;
					var sidx = document.mainform.elements['ptn_id[]'][j].selectedIndex;
					var txt = document.mainform.elements['ptn_id[]'][j].options[sidx].text;
					alert(i1+'番目と'+j1+'番目に同じ集計項目('+txt+')が指定されています。\nどちらかを削除してください。');
					return false;
				}
			}
		}
	}
    document.mainform.postback.value = "1";
    document.mainform.action = "work_admin_csv_total_item2.php";
    document.mainform.submit();
}
    function addOption(box, value, text, selected) {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = text;
        box.options[box.length] = opt;
        try {box.style.fontSize = 'auto';} catch (e) {}
        box.style.overflow = 'auto';
        if (selected == value) {
            box.selectedIndex = box.options.length -1;
            return;
        }
    }


    function addDatRow(row_id) {
        var table = document.getElementById('ptns');
        var row = table.insertRow(row_id);

        var cell0 = row.insertCell(0);
        cell0.innerHTML = '<select name="ptn_id[]"><\/select>';
        var cell1 = row.insertCell(1);
        cell1.innerHTML = '<select name="coefficient[]"><\/select>';
        cell1.setAttribute('align','center');
        var cell2 = row.insertCell(2);
        cell2.innerHTML = '<input id="addbtn" type="button" value="追加" onclick="addDatRow(this.parentNode.parentNode.rowIndex+1);" style="margin-left:2px;"><input type="button" value="削除" onclick="deleteDatRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;">';
        setPtnOptions(row_id-1, '', 1.0);
    }
    function deleteAllOptions(box) {
        for (var i = box.length - 1; i >= 0; i--) {
            box.options[i] = null;
        }
    }

    function deleteDatRow(row_id) {
        var table = document.getElementById('ptns');
        table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

    }

    function setPtnOptions(row_id, ptn_id, coefficient) {

        var ptn_elm;
        if (document.mainform.elements['ptn_id[]'].options) {
            ptn_elm = document.mainform.elements['ptn_id[]'];
        } else {
            ptn_elm = document.mainform.elements['ptn_id[]'][row_id];
        }

        deleteAllOptions(ptn_elm);
<?
for ($i=0; $i<count($arr_atdptn); $i++) {
    $tmp_ptn_id = $arr_atdptn[$i]["id"];
    $tmp_ptn_name = $arr_atdptn[$i]["name"];

    echo("\taddOption(ptn_elm, '$tmp_ptn_id', '$tmp_ptn_name', ptn_id);\n");
}
    ?>
        var coefficient_elm;
        if (document.mainform.elements['coefficient[]'].options) {
            coefficient_elm = document.mainform.elements['coefficient[]'];
        } else {
            coefficient_elm = document.mainform.elements['coefficient[]'][row_id];
        }

        deleteAllOptions(coefficient_elm);
<?
for ($i=0.5; $i<=2; $i+=0.5) {
    $wk_i = sprintf("%1.1f", $i);

    echo("\taddOption(coefficient_elm, '$wk_i', '$wk_i', coefficient);\n");
}
    ?>
    }

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
        <tr height="32" bgcolor="#5279a5">
        <td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>集計項目<? echo($title_no_str); ?>の設定</b></font></td>
        <td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
        </tr>
    </table>

    <form name="mainform" method="post">
<?
if ($postback == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
登録しました。
</font>
    <?
}
?>
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><br><?
if ($from_flg != "2") {
    echo("レイアウト".$layout_id);
}
else {
    echo("帳票名称");
}
echo(" ");
echo($layout_name); ?></font><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? echo ($title3); ?>&nbsp;</font><br>
<?
echo("<input type=\"text\" name=\"header_title\" value=\"$header_title\" size=\"50\" maxlength=\"60\" style=\"ime-mode:active;\"><br>\n");
        ?>      </td>
    </tr>
    <tr height="22">
        <td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">下記の選択した項目の値を合計します。</font><td>
        <td colspan="1" align="right">
            <input type="button" value="登録" onclick="registPtnid();">&nbsp;&nbsp;
        </td>
    </tr>
    </table>
        <table id="ptns" width="" border="0" cellspacing="0" cellpadding="0" class="list">
        <tr height="22">
        <td width="280" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計項目</font>
        </td>
        <td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">係数</font>
        </td>
        <td width="160">
        </td>
        </tr>
        <tr height="22">
        <td>
        <select name="ptn_id[]">
        <? show_options($arr_atdptn, $arr_ptn_id_list[0]["id"], true); ?>
        </select>
        </td>
        <td align="center">
        <select name="coefficient[]">
        <? show_coefficient($arr_ptn_id_list[0]["coefficient"], false); ?>
        </select>
        </td>
        <td><input id="addbtn" type="button" value="追加" onclick="addDatRow(this.parentNode.parentNode.rowIndex+1);" style="margin-left:2px;">
        </td>
        </tr>
<? //2件目以降

if (count($arr_ptn_id_list) >= 2) {
    for ($i=1; $i<count($arr_ptn_id_list); $i++) {

        echo("<tr>");
        echo("<td><select name=\"ptn_id[]\">");
        show_options($arr_atdptn, $arr_ptn_id_list[$i]["id"], false);
        echo("</select></td>");
        echo("<td align=\"center\"><select name=\"coefficient[]\">");
        show_coefficient($arr_ptn_id_list[$i]["coefficient"], false);
        echo("</select></td>");
        echo("<td><input id=\"addbtn\" type=\"button\" value=\"追加\" onclick=\"addDatRow(this.parentNode.parentNode.rowIndex+1);\" style=\"margin-left:2px;\"><input type=\"button\" value=\"削除\" onclick=\"deleteDatRow(this.parentNode.parentNode.rowIndex);\" style=\"margin-left:2px;\"></td></tr>");
    }
}

        ?>
        </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr height="22">
        <td colspan="2">
        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイムカード開始日の勤務カウント方法<br>
        &nbsp;<label for="flg1_1"><input type="radio" id="flg1_1" name="start_day_flg" value="1"<? if ($start_day_flg == "1") {echo " checked ";} ?>>カウントする</label>
<label for="flg1_2"><input type="radio"  id="flg1_2" name="start_day_flg" value="2"<? if ($start_day_flg == "2") {echo " checked ";} ?>>開始日のみカウントする</label><label for="flg1_3"><input type="radio"  id="flg1_3" name="start_day_flg" value="3"<? if ($start_day_flg == "3") {echo " checked ";} ?>>開始日はカウントしない</label><br>
月末の勤務カウント方法<br>
        &nbsp;<label for="flg2_1"><input type="radio" id="flg2_1" name="end_day_flg" value="1"<? if ($end_day_flg == "1") {echo " checked ";} ?>>カウントする</label>
<label for="flg2_2"><input type="radio"  id="flg2_2" name="end_day_flg" value="2"<? if ($end_day_flg == "2") {echo " checked ";} ?>>終了日のみカウントする</label><label for="flg2_3"><input type="radio"  id="flg2_3" name="end_day_flg" value="3"<? if ($end_day_flg == "3") {echo " checked ";} ?>>終了日はカウントしない</label><br>
曜日のカウント方法<br>
        &nbsp;<label for="flg3_1"><input type="radio" id="flg3_1" name="week_day_flg" value="1"<? if ($week_day_flg == "1") {echo " checked ";} ?>>全ての曜日をカウントする</label>
<label for="flg3_2"><input type="radio"  id="flg3_2" name="week_day_flg" value="2"<? if ($week_day_flg == "2") {echo " checked ";} ?>>日曜日のみカウントする</label><br>
        </font>
        </td>
    </tr>
    <tr height="22">
        <td width="20"></td>
        <td colspan="1" align="right">
            <input type="button" value="登録" onclick="registPtnid();">&nbsp;&nbsp;
        </td>
    </tr>
    </table>

        <!-- ------------------------------------------------------------------------ -->
        <!-- ＨＩＤＤＥＮ -->
        <!-- ------------------------------------------------------------------------ -->
        <input type="hidden" name="session" value="<? echo($session); ?>">
        <input type="hidden" name="postback" value="">
        <input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
        <input type="hidden" name="total_item_no" value="<? echo($total_item_no); ?>">
        <input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
    </form>

</body>
<? pg_close($con); ?>
</html>
<?

/**
 * パターン更新
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $ptn_id 勤務パターンIDの配列（グループID_勤務パターンID）
 * @param mixed $layout_id レイアウトID
 * @param mixed $header_title CSVヘッダータイトル
 * @param mixed $coefficient 係数の配列
 * @param mixed $total_item_no 集計項目番号（2-8）
 * @param mixed $start_day_flg 開始日カウント方法 1:カウントする 2:開始日のみカウントする 3:開始日はカウントしない
 * @param mixed $end_day_flg 終了日カウント方法 1:カウントする 2:終了日のみカウントする 3:終了日はカウントしない
 * @param mixed $week_day_flg 曜日カウント方法 1:全ての曜日をカウントする 2:日曜日のみカウントする
 * @return mixed なし
 *
 */
function update_ptn_id_list($con, $fname, $ptn_id, $layout_id, $header_title, $coefficient, $total_item_no, $start_day_flg, $end_day_flg, $week_day_flg, $from_flg) {
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_config";
        $tablename2 = "timecard_csv_total_item2";
        $tablename3 = "timecard_csv_total_setting";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_config";
        $tablename2 = "duty_shift_mprint_total_item2";
        $tablename3 = "duty_shift_mprint_total_setting";
        $wk_layout_id = "$layout_id";
    }
    ///-----------------------------------------------------------------------------
    // トランザクションを開始
    ///-----------------------------------------------------------------------------
    pg_query($con, "begin transaction");

    $sql = "select * from $tablename";
    $cond = "where layout_id = $wk_layout_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $wk_header_title = pg_escape_string($header_title);
    $varname = "total_item_title".$total_item_no;
    if (pg_num_rows($sel) > 0) {
        $sql = "update $tablename set";
        $set = array("$varname");
        $setvalue = array($wk_header_title);
        $cond = "where layout_id = $wk_layout_id";
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_close($con);
            echo("<script type='text/javascript' src='./js/showpage.js'></script>");
            echo("<script language='javascript'>showErrorPage(window);</script>");
            exit;
        }

    } else {

        $sql = "insert into $tablename ($varname, layout_id";
        $sql .= ") values (";

        $content = array($wk_header_title, $layout_id);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }


    $sql = "delete from $tablename2";
    $cond = "where layout_id = $wk_layout_id and total_item_no = $total_item_no";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    for ($i=0; $i<count($ptn_id); $i++) {

        //入力チェック
        if ($ptn_id[$i] == "") {
            continue;
        }
        $sql = "insert into $tablename2 (layout_id, no, id, coefficient, total_item_no ";
        $sql .= ") values (";

        $content = array($layout_id, $i+1, $ptn_id[$i], $coefficient[$i], $total_item_no);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    //集計項目設定、カウント方法の更新
    $sql = "select * from $tablename3";
    $cond = "where layout_id = $wk_layout_id and total_item_no = $total_item_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        //変更確認
        $old_start_day_flg = pg_fetch_result($sel, 0, "start_day_flg");
        $old_end_day_flg = pg_fetch_result($sel, 0, "end_day_flg");
        $old_week_day_flg = pg_fetch_result($sel, 0, "week_day_flg");
        if ($start_day_flg != $old_start_day_flg ||
            $end_day_flg != $old_end_day_flg ||
            $week_day_flg != $old_week_day_flg) {
            $sql = "update $tablename3 set";
            $set = array("start_day_flg", "end_day_flg", "week_day_flg");
            $setvalue = array($start_day_flg, $end_day_flg, $week_day_flg);
            $cond = "where layout_id = '$layout_id' and total_item_no = $total_item_no";
            $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
            if ($upd == 0) {
                pg_close($con);
                echo("<script type='text/javascript' src='./js/showpage.js'></script>");
                echo("<script language='javascript'>showErrorPage(window);</script>");
                exit;
            }
        }

    } else {

        $sql = "insert into $tablename3 (layout_id, total_item_no, start_day_flg, end_day_flg, week_day_flg";
        $sql .= ") values (";

        $content = array($layout_id, $total_item_no, $start_day_flg, $end_day_flg, $week_day_flg);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }

    ///-----------------------------------------------------------------------------
    // トランザクションをコミット
    ///-----------------------------------------------------------------------------
    pg_query("commit");
}


/**
 * 勤務パターン取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @return mixed IDと名称の配列、id:シフトグループ_勤務パターン name:シフトグループ＞勤務パターンの文字列の配列
 *
 */
function get_atdptn_id_name($con, $fname) {

    //勤務パターンを取得
    $sql = "select a.group_id, a.atdptn_id, a.atdptn_nm, b.group_name, a.allowance from atdptn a left join wktmgrp b on b.group_id = a.group_id";
    $cond = "order by b.group_id, a.atdptn_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_atdptn = array();
    if (pg_num_rows($sel) > 0) {
        while ($row = pg_fetch_array($sel)) {
            $tmp_group_id = $row["group_id"];
            $tmp_atdptn_id = $row["atdptn_id"];
            $tmp_group_name = $row["group_name"];
            $tmp_atdptn_nm = $row["atdptn_nm"];

            $arr_atdptn[] = array("id" => $tmp_group_id."_".$tmp_atdptn_id, "name" => "「".$tmp_group_name."＞".$tmp_atdptn_nm."」回数");
        }
    }
    return $arr_atdptn;

}

//勤務パターンオプション

/**
 * 勤務パターンオプション
 *
 * @param mixed $data_job id,nameの配列
 * @param mixed $selected_id 選択されているID
 * @param mixed $blank 空白有無指定
 * @return mixed なし
 *
 */
function show_options($data_job, $selected_id, $blank) {

    if ($blank) {
        echo("<option value=\"\"></option>");
    }
    for ($i=0; $i<count($data_job); $i++) {
        $id = $data_job[$i]["id"];
        $nm = $data_job[$i]["name"];
        echo("<option value=\"$id\"");
        if ($id ==  $selected_id) {
            echo(" selected");
        }
        echo(">$nm\n");
    }

}

/**
 * 係数オプション(0.5, 1.0, 1.5, 2.0) // →2014/8/15 10.0まで可能なように変更
 *
 * @param mixed $selected_num 選択されている数値
 * @param mixed $blank 空白有無指定
 * @return mixed なし
 *
 */
function show_coefficient($selected_num, $blank) {

    if ($blank) {
        echo("<option value=\"\"></option>");
    }
    $wk_sel =  ($selected_num != null && $selected_num != "") ? $selected_num : 1.0;
    for ($i=0.5; $i<=10; $i+=0.5) { // 2014/8/15 10.0までに変更
        $wk_i = sprintf("%1.1f", $i);
        echo("<option value=\"$wk_i\"");
        if ($wk_i ==  $wk_sel) {
            echo(" selected");
        }
        echo(">$wk_i\n");
    }

}


function get_atdbk_reason_id_name($con, $fname) {

    $id = "'1', '37', '24', '45', '46', '47', '4', '17', '5', '6', '7', '8', '25', '26', '27', '28', '29', '30', '31', '32', '61', '40', '41', '12', '13', '14', '15', '11'";
    $sql = "select reason_id, default_name, display_name from atdbk_reason_mst";
    $cond = "where reason_id in ($id) and display_flag = 't' order by sequence;";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $arr_data = array();
    $i = 0;
    if (pg_num_rows($sel) > 0) {
        while ($row = pg_fetch_array($sel)) {
            $default_name   = $row["default_name"];
            $display_name   = $row["display_name"];
            //表示名未指定時はデフォルト名称を出力
            if (empty($display_name)){
                $row["reason_name"] = $default_name;
            }
            else{
                $row["reason_name"] = $display_name;
            }

            $arr_data[] = $row;
        }
    }
    return $arr_data;

}

?>
