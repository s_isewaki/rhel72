<?php
// セッションの開始
session_start();

// ファントルくんグラフ
// 使用ライブラリ：pChart 1.27d
// http://pchart.sourceforge.net/
// このプログラムは UTF-8 で書かれています。

set_include_path(get_include_path() . PATH_SEPARATOR . "./PEAR");
include("pChart/pData.class");
include("pChart/pChart.class");
require("PEAR/Cache/Lite.php");
require_once("about_comedix.php");

// フォント
define("DRAW_FONT", "/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf");

// データ取得
$type = $_GET["type"];

$data_list = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["data_list"];
$horz_list = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["horz_list"];
$data_count = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["g"]["data_count"];

$data_list_HospitalCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_list_HospitalCount"];
$horz_list_HospitalCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["horz_list_HospitalCount"];
$data_count_HospitalCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_count_HospitalCount"];

$data_list_RequirementCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_list_RequirementCount"];
$vert_list_RequirementCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["vert_list_RequirementCount"];
$data_count_RequirementCount = $_SESSION["FPLUS_HEADQUARTERS_TOTAL_LIST"]["h"]["data_count_RequirementCount"];

switch ($type) {
	// 棒グラフ
	case "bar":
		$title_arr = array();
		$row_arr = array();
		for ($i = 0; $i < count($data_list); $i++) {
			// 合計行は飛ばす
			if ($i == (count($data_list) - 1)) {
				continue;
			}
			for ($j = 0; $j < count($horz_list); $j++) {
				// タイトル
				if ($j == 0) {
					// 文字コードの変換(UTF-8)
					$title_arr[] = mb_convert_encoding(mb_convert_encoding($data_list[$i][$j], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
				// 合計列は飛ばす
				} else if ($j == (count($horz_list) - 1)) {
					continue;
				// データ
				} else {
					$row_arr[($j - 1)][] = $data_list[$i][$j];
				}
			}
		}
		
		// データセットインスタンス生成
		$DataSet = new pData;
		
		// データがない場合
		if ($data_count == 0) {
			// グラフインスタンス生成
			$Test = new pChart(660,250);
			
			// 角の丸い塗りつぶされた四角形を描画
			$Test->drawFilledRoundedRectangle(7,7,653,243,5,240,240,240);
			
			// 角が丸いグラフを描画
			$Test->drawRoundedRectangle(5,5,655,245,5,230,230,230);
			
			// フォントの設定
			$Test->setFontProperties(DRAW_FONT, 9);
			
			// タイトルの表示
			$Test->drawTitle(255,125,"該当データはありません。",0,0,0);
			
			$Test->Stroke();
			
			exit;
		}
		
		// データを設定
		for ($j = 0; $j < count($row_arr); $j++) {
			$DataSet->AddPoint($row_arr[$j], "Serie" . ($j + 1));
		}
		
		// タイトルを設定
		$DataSet->AddPoint($title_arr, "Labels");
		
		// 設定した内容を適用
		$DataSet->AddAllSeries();
		
		// タイトルをデータセットから削除
		$DataSet->RemoveSerie("Labels");
		
		// 横軸に使用するデータセット名を設定
		$DataSet->SetAbsciseLabelSerie("Labels");
		
		for ($j = 0; $j < count($horz_list); $j++) {
			if ($j == 0 || $j == (count($horz_list) - 1)) {
				continue;
			}
			// 該当データにラベルを設定
			// 文字コードの変換(UTF-8)
			$DataSet->SetSerieName(mb_convert_encoding(mb_convert_encoding($horz_list[$j]["text"], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"), "Serie" . $j);
		}
		
		// グラフインスタンス生成
		$Test = new pChart(660,250);
		
		// パレット指定
		$Test->setColorPalette( 0,100,160,220);
		$Test->setColorPalette( 1,240,140,135);
		$Test->setColorPalette( 2,200,230,150);
		$Test->setColorPalette( 3,180,160,200);
		$Test->setColorPalette( 4,150,220,230);
		$Test->setColorPalette( 5,240,150, 70);
		$Test->setColorPalette( 6,160,190,240);
		
		// 角の丸い塗りつぶされた四角形を描画
		$Test->drawFilledRoundedRectangle(7,7,653,243,5,240,240,240);
		
		// 角が丸いグラフを描画
		$Test->drawRoundedRectangle(5,5,655,245,5,230,230,230);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 8);
		
		// グラフの描画エリアを設定
		$Test->setGraphArea(40,20,560,210);
		
		// グラフ領域の背景色を描画
		$Test->drawGraphArea(255,255,255,TRUE);
		
		// スケールを描画する
		// $Data				データ
		// $DataDescription		データ情報
		// $ScaleMode			描画モード
		// $R					色(赤)
		// $G					色(緑)
		// $B					色(青)
		// $DrawTicks=TRUE		ラベル表示
		// $Angle=0				ラベルの角度
		// $Decimals=1			数値桁数
		// $WithMargin=FALSE	表示幅と描画領域のマージンの有無
		// $SkipLabels=1		X軸のラベルのスキップ数
		// $RightScale=FALSE
		$Test->drawScale($DataSet->GetData(), $DataSet->GetDataDescription(), SCALE_ADDALL, 150, 150, 150, TRUE, 0, 2, TRUE);
		
		// グリッドの描画
		$Test->drawGrid(4,TRUE,230,230,230,50);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// しきい値を描画
		$Test->drawTreshold(0,143,55,72,TRUE,TRUE);
		
		// 積み上げ棒グラフを描画
		$Test->drawStackedBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(), 100);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// 凡例を描画する
		// XPos					X座標
		// $YPos				Y座標
		// $DataDescription		データ情報
		// $R					背景色(赤)
		// $G					背景色(緑)
		// $B					背景色(青)
		// $Rs=-1				影色(赤)
		// $Gs=-1				影色(緑)
		// $Bs=-1				影色(青)
		// $Rt=0				文字色(赤)
		// $Gt=0				文字色(緑)
		// $Bt=0				文字色(青)
		// $Border=FALSE		境界線
		$Test->drawLegend(570, 100, $DataSet->GetDataDescription(), 255, 255, 255);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 11);
		
		break;
	// 棒グラフ
	case "bar2":
		$title_arr = array();
		$row_arr = array();
		for ($i = 0; $i < count($data_list_HospitalCount); $i++) {
			for ($j = 0; $j < count($horz_list_HospitalCount); $j++) {
				if($j != 0){
					// 文字コードの変換(UTF-8)
					$title_arr[] = mb_convert_encoding(mb_convert_encoding($horz_list_HospitalCount[$j]["text"], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
					// データ
					$row_arr[] = $data_list_HospitalCount[$i][$j];
				}
			}
		}

		// データセットインスタンス生成
		$DataSet = new pData;

		// グラフインスタンス生成
		$Test = new pChart(520,250);

		// データがない場合
		if ($data_count_HospitalCount == 0) {
			// 角の丸い塗りつぶされた四角形を描画
			$Test->drawFilledRoundedRectangle(7,7,508,243,5,240,240,240);
			
			// 角が丸いグラフを描画
			$Test->drawRoundedRectangle(5,5,510,245,5,230,230,230);
			
			// フォントの設定
			$Test->setFontProperties(DRAW_FONT, 9);
			
			// タイトルの表示
			$Test->drawTitle(255,125,"該当データはありません。",0,0,0);
			
			$Test->Stroke();
			
			exit;
		}
		
		// データを設定
		$DataSet->AddPoint($row_arr, "Serie1");
		
		// タイトルを設定
		$DataSet->AddPoint($title_arr, "Labels");
		
		// 設定した内容を適用
		$DataSet->AddAllSeries();
		
		// タイトルをデータセットから削除
		$DataSet->RemoveSerie("Labels");
		
		// 横軸に使用するデータセット名を設定
		$DataSet->SetAbsciseLabelSerie("Labels");
		
		// 該当データにラベルを設定
		// 文字コードの変換(UTF-8)
		$DataSet->SetSerieName(mb_convert_encoding(mb_convert_encoding($data_list_HospitalCount[0][0], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"), "Serie1");
		
		// パレット指定
		$Test->setColorPalette( 0,100,160,220);
		$Test->setColorPalette( 1,240,140,135);
		$Test->setColorPalette( 2,200,230,150);
		$Test->setColorPalette( 3,180,160,200);
		$Test->setColorPalette( 4,150,220,230);
		$Test->setColorPalette( 5,240,150, 70);
		$Test->setColorPalette( 6,160,190,240);
		
		// 角の丸い塗りつぶされた四角形を描画
		$Test->drawFilledRoundedRectangle(7,7,508,243,5,240,240,240);
		
		// 角が丸いグラフを描画
		$Test->drawRoundedRectangle(5,5,510,245,5,230,230,230);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// グラフの描画エリアを設定
		$Test->setGraphArea(40,20,400,210);
		
		// グラフ領域の背景色を描画
		$Test->drawGraphArea(255,255,255,TRUE);
		
		// スケールを描画する
		// $Data				データ
		// $DataDescription		データ情報
		// $ScaleMode			描画モード
		// $R					色(赤)
		// $G					色(緑)
		// $B					色(青)
		// $DrawTicks=TRUE		ラベル表示
		// $Angle=0				ラベルの角度
		// $Decimals=1			数値桁数
		// $WithMargin=FALSE	表示幅と描画領域のマージンの有無
		// $SkipLabels=1		X軸のラベルのスキップ数
		// $RightScale=FALSE
		$Test->drawScale($DataSet->GetData(), $DataSet->GetDataDescription(), SCALE_ADDALL, 150, 150, 150, TRUE, 0, 2, TRUE);
		
		// グリッドの描画
		$Test->drawGrid(4,TRUE,230,230,230,50);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// しきい値を描画
		$Test->drawTreshold(0,143,55,72,TRUE,TRUE);
		
		// 積み上げ棒グラフを描画
		$Test->drawStackedBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(), 100);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// 凡例を描画する
		// XPos					X座標
		// $YPos				Y座標
		// $DataDescription		データ情報
		// $R					背景色(赤)
		// $G					背景色(緑)
		// $B					背景色(青)
		// $Rs=-1				影色(赤)
		// $Gs=-1				影色(緑)
		// $Bs=-1				影色(青)
		// $Rt=0				文字色(赤)
		// $Gt=0				文字色(緑)
		// $Bt=0				文字色(青)
		// $Border=FALSE		境界線
		$Test->drawLegend(410, 100, $DataSet->GetDataDescription(), 255, 255, 255);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 11);
		
		break;

	// 棒グラフ
	case "bar3":
		$title_arr = array();
		$row_arr = array();
		for ($i = 0; $i < count($data_list_RequirementCount); $i++) {
			for ($j = 0; $j < count($vert_list_RequirementCount); $j++) {
				if($j == 0){
					// 文字コードの変換(UTF-8)
					$title_arr[] = mb_convert_encoding(mb_convert_encoding($data_list_RequirementCount[$i][$j], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
				}else{
					// データ
					$row_arr[($j - 1)][] = $data_list_RequirementCount[$i][$j];
				}
			}
		}

		// データセットインスタンス生成
		$DataSet = new pData;

		// グラフインスタンス生成
		$Test = new pChart(520,250);

		// データがない場合
		if ($data_count_RequirementCount == 0) {
			// 角の丸い塗りつぶされた四角形を描画
			$Test->drawFilledRoundedRectangle(7,7,508,243,5,240,240,240);
			
			// 角が丸いグラフを描画
			$Test->drawRoundedRectangle(5,5,510,245,5,230,230,230);
			
			// フォントの設定
			$Test->setFontProperties(DRAW_FONT, 9);
			
			// タイトルの表示
			$Test->drawTitle(255,125,"該当データはありません。",0,0,0);
			
			$Test->Stroke();
			
			exit;
		}
		
		// データを設定
		for ($j = 0; $j < count($row_arr); $j++) {
			$DataSet->AddPoint($row_arr[$j], "Serie" . ($j + 1));
		}
		
		// タイトルを設定
		$DataSet->AddPoint($title_arr, "Labels");
		
		// 設定した内容を適用
		$DataSet->AddAllSeries();
		
		// タイトルをデータセットから削除
		$DataSet->RemoveSerie("Labels");
		
		// 横軸に使用するデータセット名を設定
		$DataSet->SetAbsciseLabelSerie("Labels");
		
		for ($j = 0; $j < count($vert_list_RequirementCount); $j++) {
			if ($j == 0) {
				continue;
			}
			// 該当データにラベルを設定
			// 文字コードの変換(UTF-8)
			$DataSet->SetSerieName(mb_convert_encoding(mb_convert_encoding($vert_list_RequirementCount[$j]["name"], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win"), "Serie" . $j);
		}
		
		// パレット指定
		$Test->setColorPalette( 0,100,160,220);
		$Test->setColorPalette( 1,240,140,135);
		$Test->setColorPalette( 2,200,230,150);
		$Test->setColorPalette( 3,180,160,200);
		$Test->setColorPalette( 4,150,220,230);
		$Test->setColorPalette( 5,240,150, 70);
		$Test->setColorPalette( 6,160,190,240);
		
		// 角の丸い塗りつぶされた四角形を描画
		$Test->drawFilledRoundedRectangle(7,7,508,243,5,240,240,240);
		
		// 角が丸いグラフを描画
		$Test->drawRoundedRectangle(5,5,510,245,5,230,230,230);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// グラフの描画エリアを設定
		$Test->setGraphArea(40,20,400,210);
		
		// グラフ領域の背景色を描画
		$Test->drawGraphArea(255,255,255,TRUE);
		
		// スケールを描画する
		// $Data				データ
		// $DataDescription		データ情報
		// $ScaleMode			描画モード
		// $R					色(赤)
		// $G					色(緑)
		// $B					色(青)
		// $DrawTicks=TRUE		ラベル表示
		// $Angle=0				ラベルの角度
		// $Decimals=1			数値桁数
		// $WithMargin=FALSE	表示幅と描画領域のマージンの有無
		// $SkipLabels=1		X軸のラベルのスキップ数
		// $RightScale=FALSE
		$Test->drawScale($DataSet->GetData(), $DataSet->GetDataDescription(), SCALE_ADDALL, 150, 150, 150, TRUE, 0, 2, TRUE);
		
		// グリッドの描画
		$Test->drawGrid(4,TRUE,230,230,230,50);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// しきい値を描画
		$Test->drawTreshold(0,143,55,72,TRUE,TRUE);
		
		// 積み上げ棒グラフを描画
		$Test->drawStackedBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(), 100);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// 凡例を描画する
		// XPos					X座標
		// $YPos				Y座標
		// $DataDescription		データ情報
		// $R					背景色(赤)
		// $G					背景色(緑)
		// $B					背景色(青)
		// $Rs=-1				影色(赤)
		// $Gs=-1				影色(緑)
		// $Bs=-1				影色(青)
		// $Rt=0				文字色(赤)
		// $Gt=0				文字色(緑)
		// $Bt=0				文字色(青)
		// $Border=FALSE		境界線
		$Test->drawLegend(410, 100, $DataSet->GetDataDescription(), 255, 255, 255);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 11);
		
		break;
	// 円グラフ(列計)
	case "pie1":
		$column_arr = array();
		$label_arr = array();
		
		for ($i = 0; $i < count($data_list); $i++) {
			// 合計行は飛ばす
			if ($i == (count($data_list) - 1)) {
				continue;
			}
			for ($j = 0; $j < count($horz_list); $j++) {
				// タイトル、合計列は飛ばす
				if ($j == 0 || $j == (count($horz_list) - 1)) {
					continue;
				// データ
				} else {
					if ($i == 0) {
						$column_arr[($j - 1)] = 0;
					}
					$column_arr[($j - 1)] += $data_list[$i][$j];
				}
			}
		}
		
		for ($j = 0; $j < count($horz_list); $j++) {
			if ($j == 0 || $j == (count($horz_list) - 1)) {
				continue;
			}
			// 文字コードの変換(UTF-8)
			$label_arr[] = mb_convert_encoding(mb_convert_encoding($horz_list[$j]["text"], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
		}
		
		// データセットインスタンス生成
		$DataSet = new pData;
		
		// データがない場合
		if ($data_count == 0) {
			// グラフインスタンス生成
			$Test = new pChart(310,250);
			
			// 角の丸い塗りつぶされた四角形を描画
			$Test->drawFilledRoundedRectangle(7,7,303,243,5,240,240,240);
			
			// 角が丸いグラフを描画
			$Test->drawRoundedRectangle(5,5,305,245,5,230,230,230);
			
			// フォントの設定
			$Test->setFontProperties(DRAW_FONT, 9);
			
			// タイトルの表示
			$Test->drawTitle(85,125,"該当データはありません。",0,0,0);
			
			$Test->Stroke();
			
			exit;
		}
		
		// データを設定
		$DataSet->AddPoint($column_arr,"Serie1");
		
		// タイトルを設定
		$DataSet->AddPoint($label_arr,"Serie2");
		
		// 設定した内容を適用
		$DataSet->AddAllSeries();
		
		// 横軸に使用するデータセット名を設定
		$DataSet->SetAbsciseLabelSerie("Serie2");
		
		// グラフインスタンス生成
		$Test = new pChart(310,250);
		
		// パレット指定
		$Test->setColorPalette( 0,100,160,220);
		$Test->setColorPalette( 1,240,140,135);
		$Test->setColorPalette( 2,200,230,150);
		$Test->setColorPalette( 3,180,160,200);
		$Test->setColorPalette( 4,150,220,230);
		$Test->setColorPalette( 5,240,150, 70);
		$Test->setColorPalette( 6,160,190,240);
		
		// 角の丸い塗りつぶされた四角形を描画
		$Test->drawFilledRoundedRectangle(7,7,303,243,5,240,240,240);
		
		// 角が丸いグラフを描画
		$Test->drawRoundedRectangle(5,5,305,245,5,230,230,230);
		
		// 塗りつぶされた円を描画
		$Test->drawFilledCircle(117,127,90,200,200,200);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// アンチエイリアスの設定
		$Test->AntialiasQuality = 0;
		
		// 基本的な円グラフを描画
		$Test->drawBasicPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),115,125,90,PIE_NOLABEL,255,255,218);
		
		// 凡例を描画
		$Test->drawPieLegend(225,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);
		
		break;
	// 円グラフ(行計)
	case "pie2":
		$row_arr = array();
		$label_arr = array();
		
		for ($i = 0; $i < count($data_list); $i++) {
			// 合計行は飛ばす
			if ($i == (count($data_list) - 1)) {
				continue;
			}
			for ($j = 0; $j < count($horz_list); $j++) {
				// タイトル取得
				if ($j == 0) {
					$label_arr[] = mb_convert_encoding(mb_convert_encoding($data_list[$i][$j], "sjis-win", "eucJP-win"), "UTF-8", "sjis-win");
					
					// 初期化
					$row_arr[$i] = 0;
				// 合計列は飛ばす
				} else if ($j == (count($horz_list) - 1)) {
					continue;
				// データ
				} else {
					$row_arr[$i] += $data_list[$i][$j];
				}
			}
		}
		
		// データセットインスタンス生成
		$DataSet = new pData;
		
		// データがない場合
		if ($data_count == 0) {
			// グラフインスタンス生成
			$Test = new pChart(380,250);
			
			// 角の丸い塗りつぶされた四角形を描画
			$Test->drawFilledRoundedRectangle(7,7,373,243,5,240,240,240);
			
			// 角が丸いグラフを描画
			$Test->drawRoundedRectangle(5,5,375,245,5,230,230,230);
			
			// フォントの設定
			$Test->setFontProperties(DRAW_FONT, 9);
			
			// タイトルの表示
			$Test->drawTitle(120,125,"該当データはありません。",0,0,0);
			
			$Test->Stroke();
			
			exit;
		}
		
		// データを設定
		$DataSet->AddPoint($row_arr,"Serie1");
		
		// ラベルを設定
		$DataSet->AddPoint($label_arr,"Serie2");
		
		// 設定した内容を適用
		$DataSet->AddAllSeries();
		
		// 横軸に使用するデータセット名を設定
		$DataSet->SetAbsciseLabelSerie("Serie2");
		
		// グラフインスタンス生成
		$Test = new pChart(380,250);
		
		// パレット指定
		$Test->setColorPalette( 0,100,160,220);
		$Test->setColorPalette( 1,240,140,135);
		$Test->setColorPalette( 2,200,230,150);
		$Test->setColorPalette( 3,180,160,200);
		$Test->setColorPalette( 4,150,220,230);
		$Test->setColorPalette( 5,240,150, 70);
		$Test->setColorPalette( 6,160,190,240);
		
		// 角の丸い塗りつぶされた四角形を描画
		$Test->drawFilledRoundedRectangle(7,7,373,243,5,240,240,240);
		
		// 角が丸いグラフを描画
		$Test->drawRoundedRectangle(5,5,375,245,5,230,230,230);
		
		// 塗りつぶされた円を描画
		$Test->drawFilledCircle(117,127,90,200,200,200);
		
		// フォントの設定
		$Test->setFontProperties(DRAW_FONT, 9);
		
		// アンチエイリアスの設定
		$Test->AntialiasQuality = 0;
		
		// 基本的な円グラフを描画
		$Test->drawBasicPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),115,125,90,PIE_NOLABEL,255,255,218);
		
		// 凡例を描画
		$Test->drawPieLegend(225,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);
		
		break;
}

if ($type != "") {
	$Test->Stroke();
	//$Test->Render("./".$type.".png");
}
