<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
// 交通費の設定
// work_admin_csv_tra.php
//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
    $title2 = "CSV交通費の設定";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
    $title2 = "交通費の設定";
}
?>
<title>CoMedix <? echo($title1." | ".$title2); ?></title>

<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");
require_once("work_admin_csv_common.ini");
require_once("duty_shift_mprint_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
$arr_atdptn = get_atdptn_id_name($con, $fname);

///-----------------------------------------------------------------------------
//初期値設定
///-----------------------------------------------------------------------------

$arr_ptn_id_list = array();
//登録時
if ($postback == "1") {
    update_ptn_id_list($con, $fname, $ptn_id, $layout_id, $coefficient, $from_flg);
	for ($i=0; $i<count($ptn_id); $i++) {
		$arr_ptn_id_list[$i]["id"] = $ptn_id[$i];
		$arr_ptn_id_list[$i]["coefficient"] = $coefficient[$i];
	}
} else {
	//初期表示時、DBから取得
    $arr_ptn_id_list = get_csv_total_tra($con, $fname, $layout_id, $from_flg);
}

//レイアウト名取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, $layout_id);
    $layout_name = $arr_layout[$layout_id]["name"];
}
else {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
    $layout_name = $arr_layout_mst["layout_name"];
}

?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registPtnid() {

	document.mainform.postback.value = "1";
	document.mainform.action = "work_admin_csv_tra.php";
	document.mainform.submit();
}
	function addOption(box, value, text, selected) {
		var opt = document.createElement("option");
		opt.value = value;
		opt.text = text;
		box.options[box.length] = opt;
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
		if (selected == value) {
			box.selectedIndex = box.options.length -1;
			return;
		}
	}


	function addDatRow(row_id) {
		var table = document.getElementById('ptns');
		var row = table.insertRow(row_id);
		
		var cell0 = row.insertCell(0);
		cell0.innerHTML = '<select name="ptn_id[]"><\/select>';
		var cell1 = row.insertCell(1);
		cell1.innerHTML = '<select name="coefficient[]"><\/select>';
		cell1.setAttribute('align','center');
		var cell2 = row.insertCell(2);
		cell2.innerHTML = '<input id="addbtn" type="button" value="追加" onclick="addDatRow(this.parentNode.parentNode.rowIndex+1);" style="margin-left:2px;"><input type="button" value="削除" onclick="deleteDatRow(this.parentNode.parentNode.rowIndex);" style="margin-left:2px;">';
		setPtnOptions(row_id-1, '', 1.0);
	}
	function deleteAllOptions(box) {
		for (var i = box.length - 1; i >= 0; i--) {
			box.options[i] = null;
		}
	}

	function deleteDatRow(row_id) {
		var table = document.getElementById('ptns');
		table.rows[row_id].parentNode.removeChild(table.rows[row_id]);

	}

	function setPtnOptions(row_id, ptn_id, coefficient) {

		var ptn_elm;
		if (document.mainform.elements['ptn_id[]'].options) {
			ptn_elm = document.mainform.elements['ptn_id[]'];
		} else {
			ptn_elm = document.mainform.elements['ptn_id[]'][row_id];
		}

		deleteAllOptions(ptn_elm);
<?
echo("\taddOption(ptn_elm, '0_0', '交通費回数', ptn_id);\n");
for ($i=0; $i<count($arr_atdptn); $i++) {
	$tmp_ptn_id = $arr_atdptn[$i]["id"];
	$tmp_ptn_name = $arr_atdptn[$i]["name"];
	
	echo("\taddOption(ptn_elm, '$tmp_ptn_id', '$tmp_ptn_name', ptn_id);\n");
}
	?>
		var coefficient_elm;
		if (document.mainform.elements['coefficient[]'].options) {
			coefficient_elm = document.mainform.elements['coefficient[]'];
		} else {
			coefficient_elm = document.mainform.elements['coefficient[]'][row_id];
		}

		deleteAllOptions(coefficient_elm);
<?
for ($i=0.5; $i<=2; $i+=0.5) {
	$wk_i = sprintf("%1.1f", $i);
	
	echo("\taddOption(coefficient_elm, '$wk_i', '$wk_i', coefficient);\n");
}
	?>
	}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>交通費の設定</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
<?
if ($postback == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
登録しました。
</font>
	<?	
}
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><br><b><?
if ($from_flg != "2") {
    echo("レイアウト".$layout_id);
}
else {
    echo("帳票名称");
}        
echo(" ");
echo($layout_name); ?></b></font><br>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
下記の選択した項目の値の合計に、各職員の勤務条件＞交通費単価を掛けた<br>
金額を交通費として出力します。
</font>

		</td>
	</tr>
	<tr height="22">
		<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">交通費＝交通費単価　×</font><td>
		<td colspan="1" align="right">
			<input type="button" value="登録" onclick="registPtnid();">&nbsp;&nbsp;
		</td>
	</tr>
	</table>
		<table id="ptns" width="" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="22">
		<td width="280" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">集計項目</font>
		</td>
		<td width="60" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">係数</font>
		</td>
		<td width="160">
		</td>
		</tr>
		<tr height="22">
		<td>
		<select name="ptn_id[]">
		<? show_options($arr_atdptn, $arr_ptn_id_list[0]["id"], true); ?>
		</select>
		</td>
		<td align="center">
		<select name="coefficient[]">
		<? show_coefficient($arr_ptn_id_list[0]["coefficient"], false); ?>
		</select>
		</td>
		<td><input id="addbtn" type="button" value="追加" onclick="addDatRow(this.parentNode.parentNode.rowIndex+1);" style="margin-left:2px;">
		</td>
		</tr>
<? //2件目以降

if (count($arr_ptn_id_list) >= 2) {
	for ($i=1; $i<count($arr_ptn_id_list); $i++) {
		
		echo("<tr>");
		echo("<td><select name=\"ptn_id[]\">");
		show_options($arr_atdptn, $arr_ptn_id_list[$i]["id"], false);
		echo("</select></td>");
		echo("<td align=\"center\"><select name=\"coefficient[]\">");
		show_coefficient($arr_ptn_id_list[$i]["coefficient"], false);
		echo("</select></td>");
		echo("<td><input id=\"addbtn\" type=\"button\" value=\"追加\" onclick=\"addDatRow(this.parentNode.parentNode.rowIndex+1);\" style=\"margin-left:2px;\"><input type=\"button\" value=\"削除\" onclick=\"deleteDatRow(this.parentNode.parentNode.rowIndex);\" style=\"margin-left:2px;\"></td></tr>");
	}
}

		?>
		</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
		<td width="20"></td>
		<td colspan="1" align="right">
			<input type="button" value="登録" onclick="registPtnid();">&nbsp;&nbsp;
		</td>
	</tr>
	</table>

		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="postback" value="">
		<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
		<input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
	</form>

</body>
<? pg_close($con); ?>
</html>
<?

/**
 * パターン更新
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @param mixed $ptn_id 勤務パターンIDの配列（グループID_勤務パターンID）
 * @param mixed $layout_id レイアウトID
 * @param mixed $coefficient 係数の配列
 * @return mixed なし
 *
 */
function update_ptn_id_list($con, $fname, $ptn_id, $layout_id, $coefficient, $from_flg) {
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_csv_total_tra";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_total_tra";
        $wk_layout_id = "$layout_id";
    }
    ///-----------------------------------------------------------------------------
	// トランザクションを開始
	///-----------------------------------------------------------------------------
	pg_query($con, "begin transaction");
	
    $sql = "delete from $tablename";
    $cond = "where layout_id = $wk_layout_id ";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	for ($i=0; $i<count($ptn_id); $i++) {
		
		//入力チェック
		if ($ptn_id[$i] == "") {
			continue;
		}
        $sql = "insert into $tablename (layout_id, no, id, coefficient ";
		$sql .= ") values (";
		
		$content = array($layout_id, $i+1, $ptn_id[$i], $coefficient[$i]);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	
	///-----------------------------------------------------------------------------
	// トランザクションをコミット
	///-----------------------------------------------------------------------------
	pg_query("commit");
}


/**
 * 勤務パターン取得
 *
 * @param mixed $con DBコネクション
 * @param mixed $fname 画面名
 * @return mixed IDと名称の配列、id:シフトグループ_勤務パターン name:シフトグループ＞勤務パターンの文字列の配列
 *
 */
function get_atdptn_id_name($con, $fname) {
	
	//勤務パターンを取得
	$sql = "select a.group_id, a.atdptn_id, a.atdptn_nm, b.group_name, a.allowance from atdptn a left join wktmgrp b on b.group_id = a.group_id";
	$cond = "order by b.group_id, a.atdptn_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$arr_atdptn = array();
	if (pg_num_rows($sel) > 0) {
		while ($row = pg_fetch_array($sel)) {
			$tmp_group_id = $row["group_id"];
			$tmp_atdptn_id = $row["atdptn_id"];
			$tmp_group_name = $row["group_name"];
			$tmp_atdptn_nm = $row["atdptn_nm"];
			
			$arr_atdptn[] = array("id" => $tmp_group_id."_".$tmp_atdptn_id, "name" => "「".$tmp_group_name."＞".$tmp_atdptn_nm."」回数");
		}
	}
	return $arr_atdptn;
	
}

/**
 * 勤務パターンオプション
 *
 * @param mixed $data_job id,nameの配列
 * @param mixed $selected_id 選択されているID
 * @param mixed $blank 空白有無指定
 * @return mixed なし
 *
 */
function show_options($data_job, $selected_id, $blank) {
	
	if ($blank) {
		echo("<option value=\"\"></option>");
	}
	echo("<option value=\"0_0\" ");
	if ($selected_id == "0_0") {
		echo(" selected");
	}
	echo(">交通費回数</option>");
	for ($i=0; $i<count($data_job); $i++) {
		$id = $data_job[$i]["id"];
		$nm = $data_job[$i]["name"];
		echo("<option value=\"$id\"");
		if ($id ==  $selected_id) {
			echo(" selected");
		}
		echo(">$nm\n");
	}
	
}

//係数オプション

/**
 * 係数オプション(0.5, 1.0, 1.5, 2.0)
 *
 * @param mixed $selected_num 選択されている数値
 * @param mixed $blank 空白有無指定
 * @return mixed なし
 *
 */
function show_coefficient($selected_num, $blank) {
	
	if ($blank) {
		echo("<option value=\"\"></option>");
	}
	$wk_sel =  ($selected_num != null && $selected_num != "") ? $selected_num : 1.0;
	for ($i=0.5; $i<=2; $i+=0.5) {
		$wk_i = sprintf("%1.1f", $i);
		echo("<option value=\"$wk_i\"");
		if ($wk_i ==  $wk_sel) {
			echo(" selected");
		}
		echo(">$wk_i\n");
	}
	
}




?>
