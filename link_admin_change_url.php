<?php
/*************************************************************************
リンクライブラリ管理
  リンクライブラリのURLの書き換え
*************************************************************************/
require_once("about_comedix.php");

$fname = $PHP_SELF;
$con = connect2db($fname);

//-----------------------------------------------
// ヘッダ
//-----------------------------------------------
header("Content-Type: text/plain");

//-----------------------------------------------
// チェック処理
//-----------------------------------------------
// POSTメソッドか？
if (!preg_match("/^post$/i", $_SERVER["REQUEST_METHOD"])) {
	echo 'NG';
	exit;
}

// ID,パスワードの有無
if (empty($_POST["id"]) or empty($_POST["password"])) {
	echo 'NG';
	exit;
}

// oip,nipの有無
if (empty($_POST["oip"]) or empty($_POST["nip"])) {
	echo 'NG';
	exit;
}

// pg_escape_string
$id = p($_POST["id"]);
$password = p($_POST["password"]);
$oip = p($_POST["oip"]);
$nip = p($_POST["nip"]);

// アカウントのチェック
$sql = "SELECT * FROM login l, authmst a";
$cond = "WHERE l.emp_login_id='$id' AND l.emp_login_pass='$password' AND l.emp_id=a.emp_id AND a.emp_del_flg='f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	echo 'NG';
	exit;
}
if (pg_num_rows($sel) != 1) {
	echo 'NG';
	exit;
}

// セッションのチェック
$session = start_session(pg_result($sel,0,"emp_id"), $id, $session, $fname);
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo 'NG';
	exit;
}

// 権限のチェック(リンクライブラリ管理[61])
$check_auth = check_authority($session, 61, $fname);
if ($check_auth == "0") {
	echo 'NG';
	exit;
}

//-----------------------------------------------
// リンクURLの書き換え
//-----------------------------------------------
$sql = "SELECT * FROM link";
$cond = "WHERE link_url LIKE '%$oip%'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	echo "NG";
	exit;
}
if (pg_num_rows($sel) <= 0) {
	echo "OK:0";
	exit;
}

// トランザクションの開始
pg_query($con, "begin");

// UPDATE
$cnt = 0;
while ( $row = pg_fetch_assoc($sel)) {
	$url = str_replace($oip, $nip, $row["link_url"]);
	
	$sql = "UPDATE link SET";
	$set = array("link_url");
	$setvalue = array($url);
	$cond = "WHERE link_id={$row["link_id"]}";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo "NG";
		exit;
	}
	$cnt++;
}

// トランザクションのコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

echo "OK:$cnt";