<?php
require_once("about_comedix.php");
require_once ( './libs/phpword/PHPWord.php' );
require_once ( './libs/phpword/PHPWord/IOFactory.php' );
require_once ( './libs/phpword/PHPWord/Writer/Word2007.php' );
require_once("fplus_common.ini");

// データベースに接続
$con = connect2db($fname);

//一覧用職種取得
$job = get_inci_job_mst();

// 診療科名
if ($data_clinical_departments != "") {
	
	//検索処理
	$sql=	"SELECT item_name FROM tmplitem ";
	$cond = "WHERE mst_cd = 'A402' AND disp_flg = 't' AND item_cd = '$data_clinical_departments'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$data_clinical_departments_name = pg_fetch_result($sel, 0, "item_name");
}
// 切断
pg_close($con);

// オブジェクト
$PHPWord = new PHPWord();

//====================================================================
// ヘッダ設定
//====================================================================
header("Content-Disposition: attachment; filename=fplus_medical_accident_info_".Date("Ymd_Hi").".docx;");
header("Content-Transfer-Encoding: binary");

// 日本語はUTF-8へ変換する。(mb_convert_encoding関数を使用)
// 変換しない場合は文書ファイルが壊れて生成される。

// デフォルトのフォント
$PHPWord->setDefaultFontName(mb_convert_encoding('ＭＳ 明朝','UTF-8','EUC-JP'));
$PHPWord->setDefaultFontSize(10.5);

// プロパティ
$properties = $PHPWord->getProperties();
$properties->setTitle(mb_convert_encoding('医療過誤等の事故原因究明及び再発防止改善策報告書','UTF-8','EUC-JP'));
$properties->setCreator(mb_convert_encoding('Medi-System','UTF-8','EUC-JP')); 
$properties->setSubject(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setCompany(mb_convert_encoding('（株）メディシステムソリューション','UTF-8','EUC-JP'));
$properties->setCategory(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setCreated(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setModified(mktime((int)date("s"), (int)date("i"), (int)date("h"), (int)date("m"), (int)date("d"), (int)date("Y")));
$properties->setKeywords(mb_convert_encoding('','UTF-8','EUC-JP'));
$properties->setDescription(mb_convert_encoding('','UTF-8','EUC-JP')); 
$properties->setLastModifiedBy(mb_convert_encoding('Medi-System','UTF-8','EUC-JP'));

// セクションとスタイル、単位に注意(1/144インチ)
$section = $PHPWord->createSection();
$sectionStyle = $section->getSettings();
$sectionStyle->setLandscape();
$sectionStyle->setPortrait();
$sectionStyle->setMarginLeft(1700);
$sectionStyle->setMarginRight(1700);
$sectionStyle->setMarginTop(1700);
$sectionStyle->setMarginBottom(1700);

// タイトル
$PHPWord->addFontStyle('title_pStyle', array('size'=>10.5,'spacing'=>100));
$PHPWord->addParagraphStyle('title_align', array('align'=>'center'));
$section->addText(mb_convert_encoding('医療過誤等の事故原因究明及び再発防止改善策報告書','UTF-8','EUC-JP'), 'title_pStyle', 'title_align');

$section->addTextBreak(1);

// 1 発生年月日
if ($data_occurrences_date != "") {
	$y = substr($data_occurrences_date, 0, 4);
	$m = substr($data_occurrences_date, 5, 2);
	$d = substr($data_occurrences_date, 8, 2);
	$data_occurrences_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('1 発生年月日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_occurrences_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 2 報告年月日
if ($data_report_date != "") {
	$y = substr($data_report_date, 0, 4);
	$m = substr($data_report_date, 5, 2);
	$d = substr($data_report_date, 8, 2);
	$data_report_date = $y . "年" . (int)$m . "月" . (int)$d . "日";
}
$section->addText(mb_convert_encoding('2 報告年月日 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_report_date,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 3 発生病院名
$section->addText(mb_convert_encoding('3 発生病院名 ','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($hospital_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// 4 事故要件
$str = "";
if ($data_requirements1[0] == "t") {
	$str .= "１";
}
if ($data_requirements2[0] == "t") {
	$str .= "２";
}
if ($data_requirements3[0] == "t") {
	if ($str != "") {
		$str .= "、";
	}
	$str .= "３";
}
$section->addText(mb_convert_encoding('4 事故要件  No.（  ' . $str . '  ） 該当番号を記入','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding('  1. 医療過誤（疑いを含む）により、死亡又は障害、予期せぬ処置etcが生じた事例','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding('  2. 医療過誤はないが、死亡又は障害、予期せぬ処置etcが生じた事例','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding('  3. 患者家族が納得されず医療紛争に展開する可能性がある事例','UTF-8','EUC-JP'));

// 5 対象患者属性と入院経過
$section->addText(mb_convert_encoding('5 対象患者属性と入院経過 ','UTF-8','EUC-JP'));

// ・氏名
$section->addText(mb_convert_encoding('  氏名：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_patient_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・性別
if ($data_patient_sex[0]=="1") {
	$str = "男性";
} else if ($data_patient_sex[0]=="2") {
	$str = "女性";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  性別：' . $str,'UTF-8','EUC-JP'));

// ・年齢
if ($data_patient_age != "") {
	$data_patient_age = $data_patient_age . "歳";
}
$section->addText(mb_convert_encoding('  年齢：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_patient_age,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・入外別
if ($data_patient_class[0]=="1") {
	$str = "入院";
} else if ($data_patient_class[0]=="2") {
	$str = "外来";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  入外別：' . $str,'UTF-8','EUC-JP'));

// ・診療科
$section->addText(mb_convert_encoding('  診療科：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_clinical_departments_name,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・主たる病名
$section->addText(mb_convert_encoding('  主たる病名：','UTF-8','EUC-JP') . mb_convert_encoding(mb_convert_encoding($data_main_disease,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

/*
// ・入院経過
$section->addText(mb_convert_encoding('  【入院経過】','UTF-8','EUC-JP'));
$ary_hospitalization_progress = preg_split("/\n/", $data_hospitalization_progress);
for($j=0;$j<count($ary_hospitalization_progress);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding(" $ary_hospitalization_progress[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}
*/

$section->addTextBreak(1);

// 6 事故の調査結果報告
$section->addText(mb_convert_encoding('6 事故の調査結果報告（明らかとなった事故概要）','UTF-8','EUC-JP'));

// ・表題
$section->addText(mb_convert_encoding('  [表題]','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding("  " . mb_convert_encoding($data_title,"sjis-win","eucJP-win"),"UTF-8","sjis-win"));

// ・概要
$section->addText(mb_convert_encoding('  [概要]','UTF-8','EUC-JP'));
switch ($data_outline[0]) {
	case "1":
		$str = "薬剤";
		break;
	case "2":
		$str = "輸血";
		break;
	case "3":
		$str = "治療・処置";
		break;
	case "4":
		$str = "医療機器等";
		break;
	case "5":
		$str = "ドレーンチューブ";
		break;
	case "6":
		$str = "検査";
		break;
	case "7":
		$str = "療養上の世話（転倒・転落等）";
		break;
}
$section->addText(mb_convert_encoding('  ' . $str,'UTF-8','EUC-JP'));

// ・事実経過
$section->addText(mb_convert_encoding('  [事実経過]','UTF-8','EUC-JP'));
$section->addText(mb_convert_encoding('  （注）可能な限り事実を時系列に記録する。','UTF-8','EUC-JP'));
$ary_fact_progress = preg_split("/\n/", $data_fact_progress);
for($j=0;$j<count($ary_fact_progress);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_fact_progress[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 7 事故発生後の対応
$section->addText(mb_convert_encoding('7 事故発生後の対応','UTF-8','EUC-JP'));
if ($data_holding_existence[0]=="1") {
	$str = "あり";
} else if ($data_holding_existence[0]=="2") {
	$str = "なし";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  事故調査委員会の開催：' . $str,'UTF-8','EUC-JP'));
$ary_accident_cope = preg_split("/\n/", $data_accident_cope);
for($j=0;$j<count($ary_accident_cope);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_accident_cope[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 8 患者・家族への対応
$section->addText(mb_convert_encoding('8 患者・家族への対応','UTF-8','EUC-JP'));
if ($data_dispute_possibility[0]=="1") {
	$str = "あり";
} else if ($data_dispute_possibility[0]=="2") {
	$str = "なし";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  医療紛争化の可能性：' . $str,'UTF-8','EUC-JP'));
$ary_patient_family_cope = preg_split("/\n/", $data_patient_family_cope);
for($j=0;$j<count($ary_patient_family_cope);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_patient_family_cope[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 9 事故報告、提出先
$section->addText(mb_convert_encoding('9 事故報告、提出先','UTF-8','EUC-JP'));
if ($data_police_notification[0]=="1") {
	$str = "あり";
} else if ($data_police_notification[0]=="2") {
	$str = "なし";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  警察への届出：' . $str,'UTF-8','EUC-JP'));
if ($data_announce_existence[0]=="1") {
	$str = "あり";
} else if ($data_announce_existence[0]=="2") {
	$str = "なし";
} else {
	$str = "";
}
$section->addText(mb_convert_encoding('  マスコミ公表：' . $str,'UTF-8','EUC-JP'));
$ary_accident_report = preg_split("/\n/", $data_accident_report);
for($j=0;$j<count($ary_accident_report);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_accident_report[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 10 発生原因
$section->addText(mb_convert_encoding('10 発生原因（事例分析の結果を記載）','UTF-8','EUC-JP'));
$ary_occurrences_cause = preg_split("/\n/", $data_occurrences_cause);
for($j=0;$j<count($ary_occurrences_cause);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_occurrences_cause[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 11 再発防止の改善策
$section->addText(mb_convert_encoding('11 再発防止の改善策','UTF-8','EUC-JP'));
$ary_remedy = preg_split("/\n/", $data_remedy);
for($j=0;$j<count($ary_remedy);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_remedy[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

$section->addTextBreak(1);

// 12 当事者のケア
$section->addText(mb_convert_encoding('12 当事者のケア','UTF-8','EUC-JP'));
$ary_person_care = preg_split("/\n/", $data_person_care);
for($j=0;$j<count($ary_person_care);$j++){
	$section->addText(mb_convert_encoding(mb_convert_encoding("  $ary_person_care[$j]","sjis-win","eucJP-win"),"UTF-8","sjis-win"));
}

// 出力
$WORD_OUT =  PHPWord_IOFactory::createWriter($PHPWord,'WORD2007');
//$WORD_OUT->save('./fplus/workflow/tmp/test.docx');
$WORD_OUT->save('php://output');

exit;

php?>
