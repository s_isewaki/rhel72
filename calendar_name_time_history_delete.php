<?php
require_once("about_comedix.php");

$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 17, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}
// データベースに接続
$con = connect2db($fname);
pg_query($con, "begin");

// 変更履歴を取得
if (!empty($_POST["calendarname_time_history_id"])) {
    $calendarname_time_history_id = pg_escape_string($_POST["calendarname_time_history_id"]);
    $sql = "DELETE FROM calendarname_time_history WHERE calendarname_time_history_id=$calendarname_time_history_id";
    $del = delete_from_table($con, $sql, "", $fname);
    if ($del == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

pg_query($con, "commit");
pg_close($con);
?>
<script type="text/javascript">
location.href = 'calendar_name.php?session=<? echo($session); ?>';
</script>
