<?php
$tools = array(
    0 => array(
                0 => "すべて",
                1 => ""
             ),
    1 => array(
                0 => "ログイン",
                1 => "0001"
             ),
    2 => array(
                0 => "ウェブメール",
                1 => "0101",
                2 => "webmladm"
             ),
    3 => array(
                0 => "スケジュール",
                1 => "0103",
                2 => "schdplc"
             ),
    4 => array(
                0 => "委員会・WG",
                1 => "0104",
                2 => "pjtadm"
             ),
    5 => array(
                0 => "お知らせ・回覧板",
                1 => "0105",
                2 => "news"
             ),
    6 => array(
                0 => "タスク",
                1 => "0106"
             ),
    7 => array(
                0 => "伝言メモ",
                1 => "0107"
             ),
    8 => array(
                0 => "設備予約",
                1 => "0108",
                2 => "fcl"
             ),
    9 => array(
                0 => "決裁・申請",
                1 => "0109",
                2 => "wkflw"
             ),
    10 => array(
                0 => "掲示板・電子会議室",
                1 => "0110",
                2 => "bbsadm"
             ),
    11 => array(
                0 => "Q&amp;A",
                1 => "0111",
                2 => "qaadm"
             ),
    12 => array(
                0 => "文書管理",
                1 => "0112",
                2 => "libadm"
             ),
    13 => array(
                0 => "アドレス帳",
                1 => "0113",
                2 => "adbkadm"
             ),
    14 => array(
                0 => "リンクライブラリ",
                1 => "0114",
                2 => "linkadm"
             ),
    15 => array(
                0 => "内線電話帳",
                1 => "0115",
                2 => "extadm"
             ),
    16 => array(
                0 => "パスワード・本人情報",
                1 => "0116"
             ),
    17 => array(
                0 => "ファントルくん",
                1 => "0201",
                2 => "rm"
             ),
    18 => array(
                0 => "職員登録",
                1 => "0002"
             ),
    19 => array(
                0 => "マスターメンテナンス",
                1 => "0003"
             ),
    20 => array(
                0 => "環境設定",
                1 => "0004"
             ),
    21 => array(
                0 => "ライセンス管理",
                1 => "0005"
             ),
	22 => array(
				0 => "スタッフ・ポートフォリオ",
				1 => "0202" // 上２ケタ、表示セクション（00:管理機能／01：基本機能／02：業務機能）下２ケタ、ユニーク連番
             ),
    // 新アプリは日別一覧より若いインデックスで定義すること
	23 => array(
				0 => "日別一覧",
				1 => "0006"
             )
		);
?>