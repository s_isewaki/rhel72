<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("hiyari_el_common.ini");
require_once("hiyari_common.ini");

//==============================
//初期処理
//==============================

//画面名
$fname = $PHP_SELF;

//セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限チェック
if (check_authority($session, 48, $fname) == "0")
{
    showLoginPage();
    exit;
}


//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}


//==============================
//設定値を取得
//==============================

// フォルダ内コンテンツカウントフラグを取得
$count_flg = lib_get_count_flg($con, $fname);

// ドラッグ＆ドロップ使用フラグを取得
$dragdrop_flg = lib_get_dragdrop_flg($con, $fname);

// コンテンツ名フラグを取得
$real_name_flg = lib_get_real_name_flg($con, $fname);




//画面名
$PAGE_TITLE = "オプション";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$INCIDENT_TITLE?> | <?=$PAGE_TITLE?></title>

<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
span.archive {margin-right:6px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- ヘッダー START -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー END -->
<img src="img/spacer.gif" width="1" height="5" alt=""><br>


<!-- 本体 START -->
<table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
        <td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
    </tr>
    <tr>
        <td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
        <td bgcolor="#F5FFE5">



<form action="hiyari_el_config_update_exe.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">

<tr height="22">
<td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ内コンテンツ数表示</font></td>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="count_flg" value="t"<? if ($count_flg == "t") {echo(" checked");} ?>>表示する
<input type="radio" name="count_flg" value="f"<? if ($count_flg == "f") {echo(" checked");} ?>>表示しない
</font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">コンテンツのドラッグ＆ドロップ</font></td>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="dragdrop_flg" value="t"<? if ($dragdrop_flg == "t") {echo(" checked");} ?>>有効にする
<input type="radio" name="dragdrop_flg" value="f"<? if ($dragdrop_flg == "f") {echo(" checked");} ?>>無効にする<br>
<span style="color:red;padding-left:5px;">※有効にすると、フォルダ数が多くなった場合にレスポンスが悪化する場合があります</span>
</font></td>
</tr>

<tr height="22">
<td align="right" bgcolor="#DFFFDC"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ダウンロード時のコンテンツ名</font></td>
<td bgcolor="#FFFFFF"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="real_name_flg" value="t"<? if ($real_name_flg == "t") {echo(" checked");} ?>>登録コンテンツ名
<input type="radio" name="real_name_flg" value="f"<? if ($real_name_flg == "f") {echo(" checked");} ?>>システム名<br>
<span style="color:red;padding-left:5px;">※登録コンテンツ名にすると、古いAcrobat Readerなどでダウンロードに失敗することがあります</span>
</font></td>
</tr>

</table>


<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>


</form>



        </td>
        <td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
    </tr>
    <tr>
        <td><img src="img/r_3.gif" width="10" height="10"></td>
        <td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
        <td><img src="img/r_4.gif" width="10" height="10"></td>
    </tr>
</table>
<!-- 本体 END -->


<!-- フッター START -->
<?
show_hiyari_footer($session,$fname,true);
?>
<!-- フッター END -->


</td>
</tr>
</table>
</body>
</html>
<? pg_close($con); ?>
