<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("maintenance_menu_list.ini");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// エリア一覧を取得
$sql = "select area_id, area_name from area";
$cond = "order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$areas = (pg_num_rows($sel) > 0) ? pg_fetch_all($sel) : array();
?>
<title>マスターメンテナンス | <? echo($arr_class_name[0]); ?>登録</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="class_menu.php?session=<? echo($session); ?>"><b>組織</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="60" align="center" bgcolor="#bdd1e7"><a href="class_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#5279a5"><a href="class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($arr_class_name[0]); ?>登録</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="attribute_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="department_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?>登録</font></a></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="class_room_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?>登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="class_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織名</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="area_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア登録</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form action="class_insert.php" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="30%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?>名</font></td>
<td><input type="text" name="class_name" size="25" maxlength="30" style="ime-mode: active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア</font></td>
<td><select name="area_id">
<option value="">（設定しない）
<? foreach ($areas as $tmp_area) { ?>
<option value="<? echo($tmp_area["area_id"]); ?>"><? echo($tmp_area["area_name"]); ?>
<? } ?>
</select></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部門階層表示</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="class_show_flg" value="t" checked>する
<input type="radio" name="class_show_flg" value="f">しない
</font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">外部連携キー</font></td>
<td><input type="text" name="link_key" value="" size="25" maxlength="12" style="ime-mode:inactive;"></td>
</tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="登録"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
