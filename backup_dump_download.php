<?
ob_start();

require("about_authority.php");
require("about_session.php");
require("about_postgres.php");
require_once("conf/conf.inf");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// バックアップ権限のチェック
$auth = check_authority($session, 39, $fname);
if ($auth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// ダンプファイル名の作成
$file_name = date("YmdHis") . ".dump";

// 作成先ディレクトリがなければ作成
if (!is_dir("dump")) {
	mkdir("dump", 0755);
}

// pg_dumpの実行
//exec("$PATH4DB/pg_dump -f dump/$file_name -F c -h $IP4DBSERVER -p $PORT4DB -U $USER4DB $NAME4DB");
exec("$PATH4DB/pg_dump -b -f dump/$file_name -F c -h $IP4DBSERVER -p $PORT4DB -U $USER4DB $NAME4DB");  // ラージオブジェクトに対応

// 作成されていたら出力〜削除
$file_path = "dump/$file_name";
if (is_file($file_path)) {
	ob_clean();
	header("Content-Disposition: attachment; filename=$file_name");
	header("Content-Type: application/octet-stream; name=$file_name");
	header("Content-Length: " . filesize($file_path));
	readfile($file_path);
	unlink($file_path);
	ob_end_flush();

// 作成されていなければエラー画面に遷移
} else {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
?>
