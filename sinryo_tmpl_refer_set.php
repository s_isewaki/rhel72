<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 59, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
	echo("<script language=\"javascript\">showLoginPage(window);</script>");
	exit;
}


// 内容読み込み
if (!$fp = fopen($tmpl_file, "r")) {
	echo("<script language=\"javascript\">alert('ファイルのオープンができませんでした。');</script>");
	echo("<script language=\"javascript\">history.back();</script>");
	exit;
}

$tmpl_content = fread($fp, filesize($tmpl_file));

fclose($fp);

//==============================================================================
// 2010/01以降のバージョンでは、create_fileとtmpl_fileは同一のものが中心となる。
// ファイルは２分割しない。また、replaceは不要。
// 旧ファイルのみ以下を実施。
//==============================================================================
$template_version_description = (strpos($tmpl_content, "COMEDIX_TEMPLATE_VERSION_DEFINITION") === false ? 0 : 1);
if (!$template_version_description){
	// XML作成用
	$pos = strpos($tmpl_content, "xml_creator");
//	$pos = strrpos($tmpl_content, "<", $pos); // strrposの第三引数はPHP5から
	$pos1 = strpos($tmpl_content, "create_", $pos);
	$pos2 = strpos($tmpl_content, "\"", $pos1);
	$create_filename = substr($tmpl_content, $pos1, $pos2 - $pos1);

	$fp = fopen($create_filename, "r");
	if ($fp != false){
		$utf_content = fread($fp, filesize($create_filename));
		fclose($fp);
	}

	// 文字コード変換　UTF-8 -> EUC
	$create_content = mb_convert_encoding($utf_content, "EUC-JP", "UTF-8");

	if (strpos($create_content, "<? // XML") === false) {
		$create_content = ereg_replace("<\?php", "<?", $create_content);
		$create_content = ereg_replace("<\?", "<? // XML作成用コード。この行は変更しないでください。", $create_content);
	}
	$content = $tmpl_content.$create_content;

	$content = str_replace("\\","\\\\", $content);
	$content = str_replace("\r\n","\n", $content);
	$content = str_replace("\n","\\n", $content);
	$content = str_replace("\"","\\\"", $content);
	$content = eregi_replace("/(script)", "__\\1", $content);
}
?>
<html>
<head>
</head>
<body>


<? if ($template_version_description){ ?>
<textarea id="sinryo_tmpl_refer_set_textarea" style="display:none"><?= h($tmpl_content) ?></textarea>
<script type="text/javascript">
	if (opener.document.tmpl) {
		opener.document.tmpl.tmpl_file.value = "<? echo($tmpl_file); ?>";
		opener.document.tmpl.tmpl_content.value = document.getElementById("sinryo_tmpl_refer_set_textarea").value;
	}
	self.close();
</script>
<? } ?>



<? if (!$template_version_description){ ?>
	<script type="text/javascript">
	if (opener.document.tmpl) {
		opener.document.tmpl.tmpl_file.value = "<? echo($tmpl_file); ?>";
		opener.document.tmpl.tmpl_content.value = "<? echo($content); ?>";
	}
	self.close();
	</script>
<? } ?>



</body>
</html>
