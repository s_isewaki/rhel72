#!/bin/sh
#=====================================================================
# Time Card Auto (morning)
#=====================================================================

# proc_flg 1:morning 2:evening
proc_flg='1'

# CoMedix Directory
if [ "$1" == "" ] ; then
	comedix_dir='/var/www/html/comedix'
else
	comedix_dir=$1
fi
#---------------------------------------------------------------------

# PHP Program CALL
php -q -c /etc/php.ini -f $comedix_dir/timecard_auto.php $proc_flg
#---------------------------------------------------------------------


