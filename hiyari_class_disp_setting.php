<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("get_values.php");
require_once("hiyari_common.ini");
require_once("show_class_name.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 権限チェック
//====================================
$auth_chk = check_authority($session, 48, $fname);
if ($auth_chk == '0') { 
	showLoginPage();
	exit;
}


//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// 更新処理
if($mode == 'update') {

	// トランザクションの開始
	pg_query($con, "begin transaction");

	set_inci_profile($con,$fname,$_POST);

	// トランザクションをコミット
	pg_query($con, "commit");

}

// 組織階層情報取得
$arr_class_name = get_class_name_array($con, $fname);


// データ取得
$arr_inci_profile = get_inci_profile($fname, $con);


//==============================
//HTML出力
//==============================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title><?=$INCIDENT_TITLE?> | 個人設定</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
<!--

function update() {

	mainform.mode.value = "update";
	document.mainform.submit();

}

// -->
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
<!--
.list {border-collapse:collapse;}
.list td {border:#35B341 solid 1px;}
// -->
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="hiyari_class_disp_setting.php" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="mode" value="">
<input type="hidden" name="use_flg" value="<?=$arr_inci_profile["use_flg"]?>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<!-- ヘッダー -->
<?
show_hiyari_header($session,$fname,true);
?>
<!-- ヘッダー -->

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="500" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="10"><img src="img/r_1.gif" width="10" height="10"></td>
<td width="100%" background="img/r_up.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td width="10"><img src="img/r_2.gif" width="10" height="10"></td>
</tr>
<tr>
<td background="img/r_left.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
<td bgcolor="#F5FFE5">

<!-- ここから -->

<table border="0" cellspacing="0" cellpadding="1">
<tr>
<td nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">■部署の表示項目</font></td>
</tr>
</table>

<table width="400" border="0" cellspacing="0" cellpadding="1">
<tr>

<?for($i=0; $i<$arr_class_name["class_cnt"]; $i++) {
if($i == 0) {
$copy_name = "class_flg";
} else if($i == 1) {
$copy_name = "attribute_flg";
} else if($i == 2) {
$copy_name = "dept_flg";
} else if($i == 3) {
$copy_name = "room_flg";
}
$copy_flg = $arr_inci_profile[$copy_name];
?>
<td nowrap>
<input type="checkbox" name="<?=$copy_name?>" value="t" <?if($copy_flg == "t") echo(" checked");?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_class_name[$i]?>
</td>
<?}?>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="5" alt=""><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td align="right">
<input type="button" value="更新" onclick="update();">
</td>
</tr>
</table>

<table width="500" border="0" cellspacing="0" cellpadding="1">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#FF0000">
※この設定は以下の部署表示に対して適応いたします。<br>
&nbsp&nbsp・受信トレイ画面&nbsp･･･&nbsp一覧中の送信者部署の表示<br>
&nbsp&nbsp・報告ファイル画面&nbsp･･･&nbsp一覧中の報告部署の表示<br>
&nbsp&nbsp・統計分析レポート表示画面&nbsp･･･&nbsp一覧中の報告部署の表示<br>
&nbsp&nbsp・ヒヤリ・ハット提出画面&nbsp･･･&nbsp一覧中の報告部署の表示<br>
&nbsp&nbsp・報告書画面&nbsp･･･&nbsp報告書にコピーされるプロフィール(当事者所属部署)<br>
&nbsp&nbsp・報告書EXCEL一覧出力機能&nbsp･･･&nbsp報告部署列<br>
&nbsp&nbsp・メール表示画面&nbsp･･･&nbsp送信者部署<br>
&nbsp&nbsp・メール印刷&nbsp･･･&nbsp送信者部署<br>
&nbsp&nbsp・報告書印刷&nbsp･･･&nbsp報告者部署<br>
</font>
</td>
</tr>
</table>


<!-- ここまで -->

</td>
<td background="img/r_right.gif"><img src="img/spacer.gif" width="10" height="1" alt=""></td>
</tr>
<tr>
<td><img src="img/r_3.gif" width="10" height="10"></td>
<td background="img/r_down.gif"><img src="img/spacer.gif" width="1" height="10" alt=""></td>
<td><img src="img/r_4.gif" width="10" height="10"></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>

</td>
</tr>
</table>
</form>
</body>
</html>

<?

//==============================
//DBコネクション終了
//==============================
pg_close($con);

?>