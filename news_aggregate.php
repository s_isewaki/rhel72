<?
ob_start();
require_once('Cmx.php');
require_once("l4pWClass.php");
require_once("about_comedix.php");
ob_end_clean();

$fname = $PHP_SELF;
$lfp = new l4pWClass("NEWS");

$session = qualify_session($session, $fname);
$checkauth = check_authority($session, 24, $fname);
$con = @connect2db($fname);
if (!$session || !$checkauth || !$con) {
    echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
    exit;
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';
//一覧で表示する際の件数
define("ROW_LIMIT", 20);

//====================================================================
// パラメータ取得
//====================================================================
if ($search_start == "t") {
    //検索開始

    $datalist = array();

    for ($i = 0; $i <= 11; $i++) {
        $datalist[$i] = "";
    }
}

if ($ag_back != "t" && $check != "false") {
    //本日の日付を設定
    $ag_start_year = date(Y);
    $ag_start_month = date(m);

    //初期値の設定
    $ag_facility_level = 1;
    $ag_flg = false;
    $check = true;
    $p = 1;

    //初期で施設別にチェックを入れるように設定
    $ag_type = 0;
}

$sel_d2_y = intval($ag_start_year) + 1;
if ($ag_start_month > 1) {
    $sel_d2_m = intval($ag_start_month) - 1;
}
else {
    $sel_d2_m = '12';
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix お知らせ・回覧板 | 集計表 | <?=$epi_title?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list1 {border-collapse:collapse;}
.list1 td {border:#5279a5 solid 1px; padding:3px; }
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px; padding:3px; }
</style>


<script language="javascript">
<!--
function startSearch()
{
	frmForm = document.getElementById("frm");
	frmForm.action = 'news_aggregate.php';
	frmForm.method = 'post';
	frmForm.submit();

	return false;
}

function clickDownLoad()
{
    frmForm = document.getElementById("frm");
    frmForm.action = 'news_aggregate_excel.php';
    frmForm.method = 'post';
    frmForm.submit();
}

function changeTimes(){
	document.frm.date_y2.value = parseInt(document.frm.ag_start_year.value) + 1;
	if(parseInt(document.frm.ag_start_month.value) == '1'){
		document.frm.date_y2.value = parseInt(document.frm.ag_start_year.value);
		document.frm.date_m2.value = '12';
	}
	else{
		document.frm.date_m2.value = parseInt(document.frm.ag_start_month.value,10) -1;
	}
}

function checkRecordFlg(flg) {
	disp = (flg) ? "none" : "";
	document.getElementById('ag_set').style.display = disp;
    frmForm = document.getElementById("frm");
    frmForm.action = 'news_aggregate.php';
    frmForm.check.value = false;
    frmForm.ag_back.value = "f";
    frmForm.submit();
}

function onloadcheck(){
	disp = (<?php echo($ag_type);?> == "1") ? "none" : "";
	document.getElementById('ag_set').style.display = disp;
	changeTimes();
}

function page_count(p){
	document.frm.page.value=p;
	document.frm.check.value=true;
	frm.action = 'news_aggregate.php';
	document.frm.submit();
}
//-->
</script>

</head>
<body style="margin:0" onload="onloadcheck();">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>

<? //-------------------------------------------------------------------------?>
<? // ぱんくず、管理画面へのリンク                                            ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#f6f9ff">
    <td width="32" height="32" class="spacing">
      <a href="newsuser_menu.php?session=<? eh($session); ?>"><img src="img/icon/b35.gif" width="32" height="32" border="0" alt="お知らせ・回覧板"></a>
    </td>
    <td>
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;
        <a href="newsuser_menu.php?session=<? eh($session); ?>"><b>お知らせ・回覧板</b></a>
        &gt; <a href="news_menu.php?session=<? eh($session); ?>"><b>管理画面</b></a>
        &gt; <a href="news_aggregate.php?session=<? eh($session); ?>&epi_type=<?=$ag_type?>"><b>集計表</b></a>
      </font>
    </td>
    <td align="right" style="padding-right:6px;">
      <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;
        <a href="newsuser_menu.php?session=<? eh($session); ?>&workflow=1">
          <b>ユーザ画面へ</b>
        </a>
      </font>
    </td>
  </tr>
</table>


<? //-------------------------------------------------------------------------?>
<? // メインメニュータブと、ボトムマージン描画                                ?>
<? //-------------------------------------------------------------------------?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="90" height="22" align="center" bgcolor="#bdd1e7">
	  <a href="news_menu.php?session=<? eh($session); ?>&page=<? echo($page); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ一覧</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_register.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" >お知らせ登録</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_search.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ検索</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_notice_list.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分一覧</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_notice_register.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">通達区分登録</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_notice2_list.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>一覧</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_notice2_register.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">お知らせ区分<br>登録</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_cate_list.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ一覧</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="90" align="center" bgcolor="#bdd1e7">
	  <a href="news_cate_register.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ登録</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="60" align="center" bgcolor="#bdd1e7">
	  <a href="news_trash.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ゴミ箱</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="70" align="center" bgcolor="#bdd1e7">
	  <a href="news_option.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font>
	  </a>
	</td>
	<td width="5">&nbsp;</td>
	<td width="70" align="center" bgcolor="#5279a5">
	  <a href="news_aggregate.php?session=<? eh($session); ?>">
	    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>集計表</b></font>
	  </a>
	</td>
	<td align="right">
	  <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j10">&nbsp;</font>
	</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td></tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>



<? //-------------------------------------------------------------------------?>
<? // 入力フォーム描画開始                                                    ?>
<? //-------------------------------------------------------------------------?>
<form name="frm" id="frm" action="news_aggregate.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="session" value="<? eh($session); ?>">
  <input type="hidden" name="search_start">
  <input type="hidden" name="ag_back" value="t">
  <input type="hidden" name="ag_facility_level" value="">
  <input type="hidden" name="ag_flg" value="<?php echo($ag_flg);?>">
	<input type="hidden" name="check" value="">
	<input type="hidden" name="page" value="">

<? //-------------------------------------------------------------------------?>
<? // 施設別と発信者別を区別するためのラジオボタン                             ?>
<? //-------------------------------------------------------------------------?>
<div style="margin:4px; padding:3px 6px; float:left; border:1px solid #ccc;">
	<?=$font?>
		<input type="radio" name="ag_type" value="0" <?php if($ag_type == '0'){?>checked<?php }?> onClick="checkRecordFlg(false);">施設別</a>
	</font>
</div>
<div style="margin:4px; padding:3px 6px; float:left; border:1px solid #ccc;">
	<?=$font?>
		<input type="radio" name="ag_type" value="1" <?php if($ag_type == '1'){?>checked<?php }?> onClick="checkRecordFlg(true);">発信者別</a>
	</font>
</div>
<div style="clear:both"></div>


	<? //-------------------------------------------------------------------------?>
	<? // 検索条件                                                                ?>
	<? //-------------------------------------------------------------------------?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list1">
		<tr>
			<td bgcolor="#bdd1e7" width="7%"><?=$font?>期間</font></td>
			<td bgcolor="#f6f9ff">
			<? // ************* 期間（日付From） ドロップダウン ******************** ?>
			<div style="float:left">
			  <select id="ag_start_year" name="ag_start_year" onchange="changeTimes()">
			  	 <? for($i=date("Y",strtotime("+1 year")); $i>=date("Y",strtotime("-10 year")); $i--) { ?><option value="<?=$i?>" <?= ($i==$ag_start_year ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="ag_start_month" name="ag_start_month" onchange="changeTimes()">
			  	<option value="01" <?= ("01"== $ag_start_month ? 'selected="selected"': "") ?>>1</option>
			  	<option value="02" <?= ("02"==$ag_start_month ? 'selected="selected"': "") ?>>2</option>
			  	<option value="03" <?= ("03"==$ag_start_month ? 'selected="selected"': "") ?>>3</option>
			  	<option value="04" <?= ("04"==$ag_start_month ? 'selected="selected"': "") ?>>4</option>
			  	<option value="05" <?= ("05"==$ag_start_month ? 'selected="selected"': "") ?>>5</option>
			  	<option value="06" <?= ("06"==$ag_start_month ? 'selected="selected"': "") ?>>6</option>
			  	<option value="07" <?= ("07"==$ag_start_month ? 'selected="selected"': "") ?>>7</option>
			  	<option value="08" <?= ("08"==$ag_start_month ? 'selected="selected"': "") ?>>8</option>
			  	<option value="09" <?= ("09"==$ag_start_month ? 'selected="selected"': "") ?>>9</option>
			  	<option value="10" <?= ("10"==$ag_start_month ? 'selected="selected"': "") ?>>10</option>
			  	<option value="11" <?= ("11"==$ag_start_month ? 'selected="selected"': "") ?>>11</option>
			  	<option value="12" <?= ("12"==$ag_start_month ? 'selected="selected"': "") ?>>12</option>
			  </select>
			</div>
			<div style="float:left">
		  〜
		  </div>
			<? // ************* 期間（日付To） ドロップダウン ******************** ?>
			<div style="float:left">
			  <select id="date_y2" name="sel_d2_y" disabled><option></option>
	  			<? for($i=date("Y",strtotime("+2 year")); $i>=date("Y",strtotime("-10 year")); $i--) { ?><option value="<?=$i?>" <?= ($i==$sel_d2_y  ? 'selected="selected"': "") ?>><?=$i?></option><? } ?>
			  </select>
			  <select id="date_m2" name="sel_d2_m" disabled><option></option>
	  			<? for($i=1; $i<=12; $i++) { ?>
	  				<option value="<?=$i?>" <?= ($i==$sel_d2_m ? 'selected="selected"': "") ?>><?=$i?></option>
	  			<? } ?>
			  </select>
			</div>

			<div style="clear:both"></div>
			</td>
		</tr>
	</table>

<?php //施設別検索の時表示

      //組織の各階層の名前を取得
			$sql = "select class_nm, atrb_nm, dept_nm, room_nm, class_cnt from classname";
			$cond = "";
			$sel = select_from_table($con, $sql, $cond, $fname);

			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}

			$class_nm = pg_fetch_result($sel, 0, "class_nm");
			$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
			$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
			$class_cnt = pg_fetch_result($sel, 0, "class_cnt");
			if($class_cnt == 4){
				$room_nm = pg_fetch_result($sel, 0, "room_nm");
			}

?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list1">
		<tr id="ag_set" style="display: <?=($ag_type == "0" ? "" : "none");?>">
			<td bgcolor="#bdd1e7" width="7%"><?=$font?>所属</font></td>
			<td bgcolor="#f6f9ff">
			<div style="float:left">
			  <select id="ag_facility_level" name="ag_facility_level">
	  			<option value="1" <?php if($ag_facility_level == 1){?>selected="selected"<?php } ?>><?php echo($class_nm);?></option>
	  			<option value="2" <?php if($ag_facility_level == 2){?>selected="selected"<?php } ?>><?php echo($atrb_nm);?></option>
	  			<option value="3" <?php if($ag_facility_level == 3){?>selected="selected"<?php } ?>><?php echo($dept_nm);?></option>
	  			<?php if($class_cnt == 4){?>
	  				<option value="4" <?php if($ag_facility_level == 4){?>selected="selected"<?php } ?>><?php echo($room_nm);?></option>
	  			<?php }?>
			  </select>
			</td>
		</tr>
	</table>


	<img src="img/spacer.gif" width="1" height="15" alt=""><br>


	<? // ************* 検索とエクセル出力ボタン ******************** ?>
	<div style="float:right;">
		<input type="button" onclick="startSearch();" value="集計"/>
		<input type="button" onclick="clickDownLoad();" value="Excel出力" <?= $ag_back != "t" ? "disabled" : "" ?> />
	</div>
	<br style="clear:both"/>
	</font>
</div>

</form>

</td></tr></table>

<? //-------------------------------------------------------------------------?>
<? // 一覧表                                                                  ?>
<? //-------------------------------------------------------------------------?>
<? if ( $ag_back != "t"){ ?>
	<div style="color:#999; text-align:center; margin-top:20px"><?=$font?>該当データはありません。</font></div>

<table width="100%" class="list1" style="<?=$tableWidth?>; background-color:#fff;">

</table>



<? } else {
			//ag_typeで施設別に集計するか発信者別に集計するか区別する。
			if($ag_type == "0" && $check != "false"){ ?>

<table width="100%" class="list1" style="<?=$tableWidth?>; background-color:#fff;">

	<? // 列ヘッダ行 ?>
	<tr style="background-color:#bdd1e7;">
	  <td align="right"><?=$font?>施設名</font></td>
	    <?php
	      $time_flg = false;
	      $serch_month = $ag_start_month;
	      for($i=0;$i<12;$i++){?>
	      	<td align="right">
	      	  <?php if(($serch_month + $i) <=12){?>
	      	    <?=$font?><?php echo($serch_month + $i."月"); ?></font>
	      	  <?php }
	      	  else if(($serch_month + $i) >= 13){
	      	    if(!$time_flg){
	      	    	$serch_month = $serch_month - 12;
	      	    	$time_flg = true;
	      	    }
	      	  ?>
	      	    <?=$font?><?php echo($serch_month + $i."月"); ?></font>
	      	  <?php }?>
	      	</td>
	    <?php }?>
	    <td align="right"><?=$font?>合計</font></td>
	</tr>

	<?php
	//20120326 中嶌慶
	//組織を表示
	//switch文でどの階層を集計するか区別する。
	switch($ag_facility_level){
		case 1:
			$sql = "select * from classmst";
			$cond = "where class_del_flg = 'f' order by order_no";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$start_year = $ag_start_year.$ag_start_month."01";
			if($ag_start_month == 1){
				$set_max_ym = $ag_start_year."1201";
			}
			else if($ag_start_month < 10){
				$set_max_ym = ($ag_start_year+1)."0".($ag_start_month - 1)."01";
			}
			else{
				$set_max_ym = ($ag_start_year+1).($ag_start_month - 1)."01";
			}
			$sql2 = "select * from classmst left join
							(select count_data,  src_class_id, classmst.class_nm, ag_month from
							(select count(*) as count_data, src_class_id, ag_month  from
							(select src_class_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f'
							 ) wnews ";
			$cond2 = "where news_date between '". $start_year ."' and '". $set_max_ym."'
							 group by src_class_id,  ag_month  order by src_class_id
							 ) tbl_data
							 left join classmst on tbl_data.src_class_id = classmst.class_id
							 order by src_class_id, ag_month
							 ) wdept on classmst.class_id = wdept.src_class_id
							 where class_del_flg = 'f'
						 	 order by classmst.order_no";
			$sel2 = select_from_table($con, $sql2, $cond2, $fname);


			$wk_lp_cnt = 0;
			$wd_id = "";
			$arr_set = array();
			while($row = pg_fetch_array($sel2))
			{
				//全体をループ
				$arr_set[$row["class_id"]][$row["ag_month"]] = $row["count_data"];

			}
			//発信者の数を調べる処理
			$ag_count = count($arr_set);


			//データベースから引用した組織を一覧で表示する範囲を設定
			//ページ遷移の際に使用する。
			//$pageは初期は空文字、ページを遷移することで変化する
			if ($page == "" || $page == 1) {
				$offset = 1;
				$limit = ROW_LIMIT;
			} else {
				//$offsetには、配列の中に格納されているデータを基準の配列番号を設定
				//$limitには、$offsetからROW_LIMIT個表示させる配列番号を設定
				$offset = ($page - 1) * ROW_LIMIT +1;
				$limit = ROW_LIMIT * $page;
			}

			$dis_month[0]="";
			$dis_month[1]="01";
			$dis_month[2]="02";
			$dis_month[3]="03";
			$dis_month[4]="04";
			$dis_month[5]="05";
			$dis_month[6]="06";
			$dis_month[7]="07";
			$dis_month[8]="08";
			$dis_month[9]="09";
			$dis_month[10]="10";
			$dis_month[11]="11";
			$dis_month[12]="12";


			$arr_count = 1;
			while($row = pg_fetch_array($sel))
			{
				//施設分ループ
				//施設名を表示
				//月数ぶんループ

				//$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];
				if($offset <= $arr_count && $arr_count <= $limit){

					echo("<tr>\n");
					$facility_nm = $row["class_nm"];
					$facility_id = $row["class_id"];
					echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
					$serch_month = (int)$ag_start_month;
					$all_count = 0;
					for($i=1;$i<13;$i++)
					{
						if($serch_month > 12){
							$serch_month = 1;
						}

						$count = $arr_set[$facility_id][$dis_month[$serch_month]];
						if($count != NULL){
							echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_set[$facility_id][$dis_month[$serch_month]]."</font></td>\n");
						}
						else{
							echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
						}
						$all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
						$serch_month ++;

					}

					//月数ぶんループ終了
					//施設分ループ終了

					//合計数表示
					echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

					echo("</tr>\n");

				}
				$arr_count++;
			}

		break;

		case 2:
			$sql = "select a.*, c.class_nm from atrbmst a left join classmst c on a.class_id = c.class_id";
			$cond = "where a.atrb_del_flg ='f' order by c.order_no,a.order_no";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$start_year = $ag_start_year.$ag_start_month."01";
			if($ag_start_month == 1){
				$set_max_ym = $ag_start_year."1201";
			}
			else if($ag_start_month < 10){
				$set_max_ym = ($ag_start_year+1)."0".($ag_start_month - 1)."01";
			}
			else{
				$set_max_ym = ($ag_start_year+1).($ag_start_month - 1)."01";
			}
			$sql2 = "select * from classmst,atrbmst left join
							(select count_data,  src_atrb_id, atrbmst.atrb_nm, ag_month from
							(select count(*) as count_data, src_atrb_id, ag_month  from
							(select src_atrb_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
			$cond2 = "where news_date between '". $start_year ."' and '". $set_max_ym."'
							 group by src_atrb_id,  ag_month  order by src_atrb_id
							 ) tbl_data
							 left join atrbmst on tbl_data.src_atrb_id = atrbmst.class_id
							 order by src_atrb_id, ag_month
							 ) wdept on atrbmst.atrb_id = wdept.src_atrb_id
							 where atrb_del_flg = 'f' and class_del_flg = 'f' and classmst.class_id = atrbmst.class_id
						 	 order by classmst.order_no,atrbmst.order_no";
			$sel2 = select_from_table($con, $sql2, $cond2, $fname);

			$wk_lp_cnt = 0;
			$wd_id = "";
			$arr_set = array();
			while($row = pg_fetch_array($sel2))
			{
				//全体をループ
				$arr_set[$row["atrb_id"]][$row["ag_month"]] = $row["count_data"];
			}

			//発信者の数を調べる処理
			$ag_count = count($arr_set);

			//データベースから引用した人を一覧で表示する範囲を設定
			//ページ遷移の際に使用する。
			//$pageは初期は空文字、ページを遷移することで変化する
			if ($page == "" || $page == 1) {
				$offset = 1;
				$limit = ROW_LIMIT;
			} else {
				//$offsetには、配列の中に格納されているデータを基準の配列番号を設定
				//$limitには、$offsetからROW_LIMIT個表示させる配列番号を設定
				$offset = ($page - 1) * ROW_LIMIT +1;
				$limit = ROW_LIMIT * $page;
			}

			$dis_month[0]="";
			$dis_month[1]="01";
			$dis_month[2]="02";
			$dis_month[3]="03";
			$dis_month[4]="04";
			$dis_month[5]="05";
			$dis_month[6]="06";
			$dis_month[7]="07";
			$dis_month[8]="08";
			$dis_month[9]="09";
			$dis_month[10]="10";
			$dis_month[11]="11";
			$dis_month[12]="12";


			$arr_count = 1;
			while($row = pg_fetch_array($sel))
			{
				//施設分ループ
				//施設名を表示
				//月数ぶんループ

				//$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];
				if($offset <= $arr_count && $arr_count <= $limit){

					echo("<tr>\n");
						$facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"];
						$facility_id = $row["atrb_id"];
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
						$serch_month = (int)$ag_start_month;
						$all_count = 0;
						for($i=1;$i<13;$i++)
						{
							if($serch_month > 12){
								$serch_month = 1;
							}

							$count = $arr_set[$facility_id][$dis_month[$serch_month]];
							if($count != NULL){
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_set[$facility_id][$dis_month[$serch_month]]."</font></td>\n");
							}
							else{
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
							}
							$all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
							$serch_month ++;

						}

					//月数ぶんループ終了
					//施設分ループ終了

					//合計数表示
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

					echo("</tr>\n");

				}
			$arr_count++;
			}

		break;

		case 3:
			$sql = "select d.*, a.atrb_nm, c.class_nm from deptmst d left join atrbmst a on d.atrb_id = a.atrb_id left join classmst c on a.class_id = c.class_id";
			$cond = "where d.dept_del_flg = 'f' order by c.order_no,a.order_no,d.order_no";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
				$start_year = $ag_start_year.$ag_start_month."01";
				if($ag_start_month == 1){
				$set_max_ym = $ag_start_year."1201";
				}
				else if($ag_start_month < 10){
					$set_max_ym = ($ag_start_year+1)."0".($ag_start_month - 1)."01";
				}
				else{
					$set_max_ym = ($ag_start_year+1).($ag_start_month - 1)."01";
				}
				$sql2 = "select * from classmst,atrbmst,deptmst left join
								(select count_data,  src_dept_id, deptmst.dept_nm, ag_month from
								(select count(*) as count_data, src_dept_id, ag_month  from
								(select src_dept_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
				$cond2 = "where news_date between '". $start_year ."' and '". $set_max_ym."'
								 group by src_dept_id,  ag_month  order by src_dept_id
								 ) tbl_data
								 left join deptmst on tbl_data.src_dept_id = deptmst.dept_id
								 order by src_dept_id, ag_month
								 ) wdept on deptmst.dept_id = wdept.src_dept_id
								 where dept_del_flg = 'f' and atrbmst.atrb_del_flg = 'f' and atrbmst.atrb_id = deptmst.atrb_id
							 	 order by classmst.order_no,atrbmst.order_no,deptmst.order_no";
				$sel2 = select_from_table($con, $sql2, $cond2, $fname);


				$wk_lp_cnt = 0;
				$wd_id = "";
				$arr_set = array();
				while($row = pg_fetch_array($sel2))
				{
					//全体をループ
					$arr_set[$row["dept_id"]][$row["ag_month"]] = $row["count_data"];

				}
				//発信者の数を調べる処理
				$ag_count = count($arr_set);

				//データベースから引用した人を一覧で表示する範囲を設定
				//ページ遷移の際に使用する。
				//$pageは初期は空文字、ページを遷移することで変化する
				if ($page == "" || $page == 1) {
					$offset = 1;
					$limit = ROW_LIMIT;
				} else {
					//$offsetには、配列の中に格納されているデータを基準の配列番号を設定
					//$limitには、$offsetからROW_LIMIT個表示させる配列番号を設定
					$offset = ($page - 1) * ROW_LIMIT +1;
					$limit = ROW_LIMIT * $page;
				}

				$dis_month[0]="";
				$dis_month[1]="01";
				$dis_month[2]="02";
				$dis_month[3]="03";
				$dis_month[4]="04";
				$dis_month[5]="05";
				$dis_month[6]="06";
				$dis_month[7]="07";
				$dis_month[8]="08";
				$dis_month[9]="09";
				$dis_month[10]="10";
				$dis_month[11]="11";
				$dis_month[12]="12";

				$arr_count = 1;
				while($row = pg_fetch_array($sel))
				{
					//施設分ループ
					//施設名を表示
					//月数ぶんループ

					//$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];
					if($offset <= $arr_count && $arr_count <= $limit){

					echo("<tr>\n");
						$facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"] . "＞" . $row["dept_nm"];
						$facility_id = $row["dept_id"];
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
						$serch_month = (int)$ag_start_month;
						$all_count = 0;
						for($i=1;$i<13;$i++)
						{
							if($serch_month > 12){
								$serch_month = 1;
							}

							$count = $arr_set[$facility_id][$dis_month[$serch_month]];
							if($count != NULL){
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_set[$facility_id][$dis_month[$serch_month]]."</font></td>\n");
							}
							else{
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
							}
							$all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
							$serch_month ++;

						}

					//月数ぶんループ終了
					//施設分ループ終了

					//合計数表示
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

					echo("</tr>\n");

				}
				$arr_count++;
			}

			break;

			case 4:
				$sql = "select r.*, d.dept_nm, a.atrb_nm, c.class_nm from classroom r left join deptmst d on r.dept_id = d.dept_id left join atrbmst a on d.atrb_id = a.atrb_id left join classmst c on a.class_id = c.class_id";
				$cond = "where r.room_del_flg = 'f' order by c.order_no,a.order_no,d.order_no,r.order_no";
				$sel = select_from_table($con, $sql, $cond, $fname);
				if ($sel == 0) {
					pg_close($con);
					echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
					echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
					exit;
				}
				$start_year = $ag_start_year.$ag_start_month."01";
				if($ag_start_month == 1){
				$set_max_ym = $ag_start_year."1201";
				}
				else if($ag_start_month < 10){
					$set_max_ym = ($ag_start_year+1)."0".($ag_start_month - 1)."01";
				}
				else{
					$set_max_ym = ($ag_start_year+1).($ag_start_month - 1)."01";
				}
				$sql2 = "select * from deptmst,classroom left join
				(select count_data,  src_room_id, classroom.room_nm, ag_month from
				(select count(*) as count_data, src_room_id, ag_month  from
				(select src_room_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
				$cond2 = "where news_date between '". $start_year ."' and '". $set_max_ym."'
				group by src_room_id,  ag_month  order by src_room_id
				) tbl_data
				left join classroom on tbl_data.src_room_id = classroom.room_id
				order by src_room_id, ag_month
				) wdept on classroom.room_id = wdept.src_room_id
				where room_del_flg = 'f' and deptmst.dept_del_flg = 'f' and deptmst.dept_id = classroom.dept_id
				order by classmst.order_no,atrbmst.order_no,deptmst.order_no,roommst.order_no";
				$sel2 = select_from_table($con, $sql2, $cond2, $fname);


				$wk_lp_cnt = 0;
				$wd_id = "";
				$arr_set = array();
				while($row = pg_fetch_array($sel2))
				{
					//全体をループ
					$arr_set[$row["room_id"]][$row["ag_month"]] = $row["count_data"];
				}

				//発信者の数を調べる処理
				$ag_count = count($arr_set);

				//データベースから引用した人を一覧で表示する範囲を設定
				//ページ遷移の際に使用する。
				//$pageは初期は空文字、ページを遷移することで変化する
				if ($page == "" || $page == 1) {
					$offset = 1;
					$limit = ROW_LIMIT;
				} else {
					//$offsetには、配列の中に格納されているデータを基準の配列番号を設定
					//$limitには、$offsetからROW_LIMIT個表示させる配列番号を設定
					$offset = ($page - 1) * ROW_LIMIT +1;
					$limit = ROW_LIMIT * $page;
				}

				$dis_month[0]="";
				$dis_month[1]="01";
				$dis_month[2]="02";
				$dis_month[3]="03";
				$dis_month[4]="04";
				$dis_month[5]="05";
				$dis_month[6]="06";
				$dis_month[7]="07";
				$dis_month[8]="08";
				$dis_month[9]="09";
				$dis_month[10]="10";
				$dis_month[11]="11";
				$dis_month[12]="12";

				$arr_count =1;
				while($row = pg_fetch_array($sel))
				{
					//施設分ループ
					//施設名を表示
					//月数ぶんループ

					//$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];
					if($offset <= $arr_count && $arr_count <= $limit){

						echo("<tr>\n");
						$facility_nm = $row["class_nm"] . "＞" . $row["atrb_nm"] . "＞" . $row["dept_nm"] . "＞" . $row["room_nm"];
						$facility_id = $row["room_id"];
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm</font></td>\n");
						$serch_month = (int)$ag_start_month;
						$all_count = 0;
						for($i=1;$i<13;$i++)
						{
						if($serch_month > 12){
						$serch_month = 1;
						}

						$count = $arr_set[$facility_id][$dis_month[$serch_month]];
						if($count != NULL){
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_set[$facility_id][$dis_month[$serch_month]]."</font></td>\n");
						}
						else{
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
						}
						$all_count += $arr_set[$facility_id][$dis_month[$serch_month]];
						$serch_month ++;

						}

						//月数ぶんループ終了
						//施設分ループ終了

						//合計数表示
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

						echo("</tr>\n");

					}
					$arr_count++;
				}

				break;

	}

?>
</table>

<?
}else if($ag_type == "1" && $check != "false"){
$lfp->debugRst("NEWS","SQL ：正常にユーザ別で集計する処理まで進んでいます。");
			echo("<table width=\"100%\" class=\"list1\" style=\"<?=$tableWidth?>; background-color:#fff;\">");
	?>
			<? // 列ヘッダ行 ?>
				<tr style="background-color:#bdd1e7;">
				  <td align="right"><?=$font?>発信者名</font></td>
				    <?php
				      $time_flg = false;
				      $serch_month = $ag_start_month;
				      for($i=0;$i<12;$i++){?>
				      	<td align="right" width="6%">
				      	  <?php if(($serch_month + $i) <=12){?>
				      	    <?=$font?><?php echo($serch_month + $i."月"); ?></font>
				      	  <?php }
				      	  else if(($serch_month + $i) >= 13){
				      	    if(!$time_flg){
				      	    	$serch_month = $serch_month - 12;
				      	    	$time_flg = true;
				      	    }
				      	  ?>
				      	    <?=$font?><?php echo($serch_month + $i."月"); ?></font>
				      	  <?php }?>
				      	</td>
				    <?php }?>
				    <td align="right" width="6%"><?=$font?>合計</font></td>
				</tr>
<?php
			$sql = "select * from empmst";
			$cond = "order by emp_kn_lt_nm,emp_kn_ft_nm";
			$sel = select_from_table($con, $sql, $cond, $fname);
			if ($sel == 0) {
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
				$start_year = $ag_start_year.$ag_start_month."01";
				if($ag_start_month == 1){
					$set_max_ym = $ag_start_year."1201";
				}
				else if($ag_start_month < 10){
					$set_max_ym = ($ag_start_year+1)."0".($ag_start_month - 1)."01";
				}
				else{
					$set_max_ym = ($ag_start_year+1).($ag_start_month - 1)."01";
				}
				$sql2 = "select count_data, src_emp_id, ag_month, ref_empmst.emp_lt_nm, ref_empmst.emp_ft_nm, class_nm,atrb_nm,dept_nm, room_nm
								 from(
								 select count(*) as count_data, src_emp_id, ag_month from
								 (select src_emp_id, news_date, substr(news_date, 5, 2) as ag_month from news where news_del_flg = 'f' ) wnews ";
				$cond2 =" where news_date between '".$start_year."' and '".$set_max_ym."' group by src_emp_id, ag_month
									 ) tbl_data
									 inner join empmst ref_empmst on tbl_data.src_emp_id = ref_empmst.emp_id
									 left join classmst on ref_empmst.emp_class = classmst.class_id
									 left join atrbmst on ref_empmst.emp_attribute = atrbmst.atrb_id
									 left join deptmst on ref_empmst.emp_dept = deptmst.dept_id
									 left join classroom on ref_empmst.emp_room = classroom.room_id
									 order by src_emp_id, ag_month
									 ";

				$sel2 = select_from_table($con, $sql2, $cond2, $fname);


				$wk_lp_cnt = 0;
				$wd_id = "";
				$arr_set = array();
				while($row = pg_fetch_array($sel2))
				{
					//全体をループ
						$arr_set[$row["src_emp_id"]][$row["ag_month"]] = $row["count_data"];
						$arr_set[$row["src_emp_id"]]["name"] = $row["emp_lt_nm"].$row["emp_ft_nm"];
						$arr_set[$row["src_emp_id"]]["dept"] = $row["class_nm"].">".$row["atrb_nm"].">".$row["dept_nm"].">".$row["room_nm"];


				}
				//発信者の数を調べる処理
				$ag_count = count($arr_set);

				//データベースから引用した人を一覧で表示する範囲を設定
				//ページ遷移の際に使用する。
				//$pageは初期は空文字、ページを遷移することで変化する
				if ($page == "" || $page == 1) {
					$offset = 1;
					$limit = ROW_LIMIT;
				} else {
					//$offsetには、配列の中に格納されているデータを基準の配列番号を設定
					//$limitには、$offsetからROW_LIMIT個表示させる配列番号を設定
					$offset = ($page - 1) * ROW_LIMIT +1;
					$limit = ROW_LIMIT * $page;
				}


				$dis_month[0]="";
				$dis_month[1]="01";
				$dis_month[2]="02";
				$dis_month[3]="03";
				$dis_month[4]="04";
				$dis_month[5]="05";
				$dis_month[6]="06";
				$dis_month[7]="07";
				$dis_month[8]="08";
				$dis_month[9]="09";
				$dis_month[10]="10";
				$dis_month[11]="11";
				$dis_month[12]="12";

				$arr_count = 1;
				foreach( $arr_set as $key => $value )
				{
					//施設分ループ
					//施設名を表示
					//月数ぶんループ

					//$arr_set[$row[ループの施設ID]][ループの月数] = $row["count_data"];
					if($offset <= $arr_count && $arr_count <= $limit){
					echo("<tr>\n");
						$facility_nm = $value["name"];
						$dept_nm = $value["dept"];
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$facility_nm<br>$dept_nm</font></td>\n");
						$serch_month = (int)$ag_start_month;
						$all_count = 0;
						for($i=1;$i<13;$i++)
						{
							if($serch_month > 12){
								$serch_month = 1;
							}

							$count = $arr_set[$key][$dis_month[$serch_month]];
							if($count != NULL){
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".$arr_set[$key][$dis_month[$serch_month]]."</font></td>\n");
							}
							else{
								echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">0</font></td>\n");
							}
							$all_count += $arr_set[$key][$dis_month[$serch_month]];
							$serch_month ++;

						}

					//月数ぶんループ終了
					//施設分ループ終了

					//合計数表示
						echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$all_count</font></td>\n");

					}
					$arr_count++;
				}

				echo("</title>");

$lfp->debugRst("NEWS","SQL ：正常にユーザ別で集計する処理が最後まで完了しました。");


}


}
//ページの番号を表示
if($ag_count > ROW_LIMIT){
	$lfp->debugRst("NEWS","SQL ：正常にページ処理まで進んでいます。");
	echo("<table align=\"right\">");
	echo("<tr><td ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">ページ：\n");
	for ($p = 1, $max = ceil($ag_count / ROW_LIMIT); $p <= $max; $p++) {
		if ($p == $page || ($page == "" && $p == 1)) {
			echo("$p\n");
		} else {
			echo("<a href=\"javascript:page_count('$p')\">$p</a>\n");
		}
	}
	echo("</font></td></tr>\n");
	echo("</table>");
		$lfp->debugRst("NEWS","SQL ：正常にページ処理が最後まで完了しました。");
}
?>

</body>
</html>
