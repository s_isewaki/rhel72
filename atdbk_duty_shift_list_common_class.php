
<!-- ************************************************************************ -->
<!-- 出勤表用勤務シフト作成　勤務表印刷共通ＣＬＡＳＳ -->
<!-- ************************************************************************ -->

<?
//ini_set("display_errors","1");
require_once("about_authority.php");
require_once("about_session.php");
require_once("about_postgres.php");

require_once("duty_shift_common_class.php");

class atdbk_duty_shift_list_common_class
{

	var $file_name;	// 呼び出し元ファイル名
	var $_db_con;	// DBコネクション
	var $obj;		// 勤務シフト共通クラス

	/*************************************************************************/
	// コンストラクタ
	// @param object $con DBコネクション
	// @param string $fname 画面名
	/*************************************************************************/
	function atdbk_duty_shift_list_common_class($con, $fname)
	{
		// 呼び出し元ファイル名を保持
		$this->file_name = $fname;
		// DBコネクションを保持
		$this->_db_con = $con;
		// 全項目リストを初期化
		// クラス new
		$this->obj = new duty_shift_common_class($con, $fname);
	}

//-------------------------------------------------------------------------------------------------------------------------
// 関数一覧（新規作成：ＤＢ読み込み）
//-------------------------------------------------------------------------------------------------------------------------
	/*************************************************************************/
	// 勤務予定／実績情報取得
	// @param	$db_name			ＤＢ名称
	// @param	$pattern_id			職員の基本出勤パターンID
	// @param	$emp_id				職員ID
	// @param	$duty_yyyy			勤務年
	// @param	$duty_mm			勤務月
	// @param	$pattern_array_all	勤務シフト記号情報
	//
	// @return	$ret				取得情報
	/*************************************************************************/
	function get_atdbk_atdbkrslt_array($db_name, $pattern_id, $emp_id, $duty_yyyy, $duty_mm, $pattern_array_all) {

		$data_atdptn_all = $this->obj->get_atdptn_array("");
//換算日数の配列、シフトグループID、勤務パターンIDをキーとする
		$workday_count_array = array();
		for ($i=0;$i<count($data_atdptn_all);$i++) {
			$tmp_group_id = $data_atdptn_all[$i]["group_id"];
			$tmp_atdptn_id = $data_atdptn_all[$i]["id"];
			$workday_count_array["$tmp_group_id"]["$tmp_atdptn_id"] = $data_atdptn_all[$i]["workday_count"];
		}
		//-------------------------------------------------------------------
		//日編集
		//-------------------------------------------------------------------
		$start_date = sprintf("%04d%02d", $duty_yyyy, $duty_mm) . "01";
		$end_date = sprintf("%04d%02d", $duty_yyyy, $duty_mm) . "31";
		//-------------------------------------------------------------------
		// 予定／実績ＤＢより情報取得
		//-------------------------------------------------------------------
		$sql = "select * from $db_name ";
		$cond = "where emp_id = '$emp_id' ";
		$cond .= "and date >= '$start_date' and date <= '$end_date'";
		$sel = select_from_table($this->_db_con, $sql, $cond, $this->file_name);
		if ($sel == 0) {
			pg_close($this->_db_con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$num = pg_numrows($sel);
		//-------------------------------------------------------------------
		// ＤＢ値設定
		//-------------------------------------------------------------------
		$data = array();
		for ($i=0;$i<$num;$i++) {
			//-------------------------------------------------------------------
			//ＤＢより取得値
			//-------------------------------------------------------------------
			$data[$i]["emp_id"] = pg_result($sel,$i,"emp_id");
			$wk = pg_result($sel,$i,"date");
			$data[$i]["year"] = (int)substr($wk, 0, 4);
			$data[$i]["month"] = (int)substr($wk, 4, 2);
			$data[$i]["day"] = (int)substr($wk, 6, 2);
			$data[$i]["atdptn_ptn_id"] = pg_result($sel,$i,"pattern");
			$data[$i]["pattern_id"] = trim(pg_result($sel,$i,"tmcd_group_id"));
			if ($data[$i]["pattern_id"] == "") {
				$data[$i]["pattern_id"] = $pattern_id;
			}

			$data[$i]["start_time"] = trim(pg_result($sel,$i,"start_time"));		//開始時刻
			$data[$i]["end_time"] = trim(pg_result($sel,$i,"end_time"));			//終了時刻
			$key_reason = trim(pg_result($sel,$i,"reason"));						//事由
			// 事由無しの休暇は、内部的なコードを設定
			if ($data[$i]["atdptn_ptn_id"] == "10" &&
				$key_reason == "") {
				$key_reason = "34";
			}
			//$ret["34"] = ""（事由無し）;
			//$ret["24"] = "公休";
			//$ret["22"] = "法定休暇";
			//$ret["23"] = "所定休暇";
			//$ret["1"] = "有給休暇";
			//$ret["37"] = "年休";
			//$ret["4"] = "代替休暇";
			//$ret["17"] = "振替休暇";
			//$ret["5"] = "特別休暇";
			//$ret["6"] = "欠勤（一般欠勤）";
			//$ret["7"] = "病欠（病傷欠勤）";
			//$ret["8"] = "その他休";
			//$ret["40"] = "リフレッシュ休暇";
			//$ret["41"] = "初盆休暇";
			//44:半有半公 45:希望公休 46:待機公休 47:管理当直前公休
			if (($key_reason == "34") ||
				($key_reason == "1") || ($key_reason == "37") ||
				($key_reason >= "4" && $key_reason <= "8") ||
				($key_reason == "17") ||
				($key_reason >= "22" && $key_reason <= "24") ||
				($key_reason == "40") || ($key_reason == "41") ||
				($key_reason >= "44" && $key_reason <= "47")
				) {
				$data[$i]["reason"] = $key_reason;		//事由
			}
			//$ret["2"] = "午前有休";
			//$ret["3"] = "午後有休";
			else if (($key_reason == "2") ||
				($key_reason == "3")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "1";				//事由（有給休暇）
			}
			//$ret["38"] = "午前年休";
			//$ret["39"] = "午後年休";
			else if (($key_reason == "38") ||
				($key_reason == "39")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "37";				//事由（年休）
			}
			//$ret["18"] = "半前代替休";
			//$ret["19"] = "半後代替休";
			else if (($key_reason == "18") ||
				($key_reason == "19")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "4";				//事由（代替休暇）
			}
			//$ret["20"] = "半前振替休";
			//$ret["21"] = "半後振替休";
			else if (($key_reason == "20") ||
				($key_reason == "21")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "17";			//事由（振替休暇）
			}
			//$ret["35"] = "午前公休";
			//$ret["36"] = "午後公休";
			else if (($key_reason == "35") ||
				($key_reason == "36")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "24";			//事由（公休）
			}
			//$ret["42"] = "午前リフレッシュ";
			//$ret["43"] = "午後リフレッシュ";
			else if (($key_reason == "42") ||
				($key_reason == "43")) {
				$data[$i]["reason_2"] = $key_reason;	//事由
				$data[$i]["reason"] = "40";			//事由（リフレッシュ）
			}
			// 上記以外
			else {
				$data[$i]["reason_2"] = $key_reason;	//事由
				// 休暇の場合、事由無しの休暇とするため"34"を設定
				if ($data[$i]["atdptn_ptn_id"] == "10") {
					$data[$i]["reason"] = "34";
				}
			}
			//-------------------------------------------------------------------
			//勤務シフト記号情報DBより設定
			//-------------------------------------------------------------------
			$atdptn_ptn_name = "";
			//パターンID
			$atdptn_ptn_id = "";
			for ($k=0;$k<count($pattern_array_all);$k++) {
				$wk_flg = "";
				if ($pattern_array_all[$k]["reason"] != "" ||
					$data[$i]["reason"] != "") {
					// 午前有給等が入っている場合は、その名称にする
					if ($data[$i]["reason_2"] == "2" ||
						$data[$i]["reason_2"] == "3" ||
						$data[$i]["reason_2"] == "18" ||
						$data[$i]["reason_2"] == "19" ||
						$data[$i]["reason_2"] == "20" ||
						$data[$i]["reason_2"] == "21" ||
						$data[$i]["reason_2"] == "35"||
						$data[$i]["reason_2"] == "36"||
						$data[$i]["reason_2"] == "38"||
						$data[$i]["reason_2"] == "39"||
						$data[$i]["reason_2"] == "42"||
						$data[$i]["reason_2"] == "43"
					) {
						$wk_atdptn_ptn_id = "10";
					} else {
						$wk_atdptn_ptn_id = $data[$i]["atdptn_ptn_id"];
					}
					if (($data[$i]["pattern_id"] == $pattern_array_all[$k]["pattern_id"]) &&
						($wk_atdptn_ptn_id == $pattern_array_all[$k]["atdptn_ptn_id"]) &&
						($data[$i]["reason"] == $pattern_array_all[$k]["reason"])) {
						$atdptn_ptn_name = $pattern_array_all[$k]["atdptn_ptn_name"];
						$atdptn_ptn_id = $pattern_array_all[$k]["atdptn_ptn_id"];
						$wk_flg = "1";
					}
				} else {
					if (($data[$i]["pattern_id"] == $pattern_array_all[$k]["pattern_id"]) &&
						($data[$i]["atdptn_ptn_id"] == $pattern_array_all[$k]["atdptn_ptn_id"])) {
						$atdptn_ptn_name = $pattern_array_all[$k]["atdptn_ptn_name"];
						$atdptn_ptn_id = $pattern_array_all[$k]["atdptn_ptn_id"];
						$wk_flg = "1";
					}
				}
//echo("<br>$k font_name=".$pattern_array_all[$k]["font_name"]); // debug
//echo(" pattern_id=".$pattern_array_all[$k]["pattern_id"]);
//echo(" atdptn_ptn_id=".$pattern_array_all[$k]["atdptn_ptn_id"]);
//echo(" reason=".$pattern_array_all[$k]["reason"]);

				if ($wk_flg == "1") {
					$wk_font_name = $pattern_array_all[$k]["font_name"];
					// 午前有休等対応
					if ($data[$i]["reason_2"] == "2" ||
						$data[$i]["reason_2"] == "18" ||
						$data[$i]["reason_2"] == "20" ||
						$data[$i]["reason_2"] == "35" ||
						$data[$i]["reason_2"] == "38" ||
						$data[$i]["reason_2"] == "42"
					) {
						$wk_font_name = $wk_font_name."前";
					}
					else 
					if ($data[$i]["reason_2"] == "3" ||
						$data[$i]["reason_2"] == "19" ||
						$data[$i]["reason_2"] == "21" ||
						$data[$i]["reason_2"] == "36" ||
						$data[$i]["reason_2"] == "39" ||
						$data[$i]["reason_2"] == "43"
					) {
						$wk_font_name = $wk_font_name."後";
					}
					$data[$i]["font_name"] = $wk_font_name;			//文字（勤務状況名）
					$data[$i]["font_color_id"] = $pattern_array_all[$k]["font_color_id"];	//文字（色）
					$data[$i]["back_color_id"] = $pattern_array_all[$k]["back_color_id"];	//文字（背景色）
					break;
				}
			}
			// 夜勤明け等対応 2008/06/10
			$wk_ake_flg = 0;
			// 勤務パターンの換算日数が0の場合
			$tmp_pattern_id = $data[$i]["pattern_id"];
			$tmp_atdptn_id = $data[$i]["atdptn_ptn_id"];
			if ($workday_count_array["$tmp_pattern_id"]["$tmp_atdptn_id"] == 0) {
				$wk_ake_flg = 1;
			}
			//-------------------------------------------------------------------
			//休日以外で勤務開始／終了時間が未設定のものは表示しない
			//-------------------------------------------------------------------
// 予定はstart_time,end_timeが未設定のため同じ条件で処理可能
//            if($db_name == "atdbkrslt") {
				if (($data[$i]["start_time"] == "") ||
					($data[$i]["end_time"] == "")) {
					//休暇を名称ではなくIDで確認
					if ($atdptn_ptn_id != "10") {
						// 午前有休等対応、実績時の夜勤明け等追加
						if ($data[$i]["reason_2"] == "2" ||
							$data[$i]["reason_2"] == "3" ||
							$data[$i]["reason_2"] == "18" ||
							$data[$i]["reason_2"] == "19" ||
							$data[$i]["reason_2"] == "20" ||
							$data[$i]["reason_2"] == "21" ||
							$data[$i]["reason_2"] == "35" ||
							$data[$i]["reason_2"] == "36" ||
							$data[$i]["reason_2"] == "38" ||
							$data[$i]["reason_2"] == "39" ||
							$data[$i]["reason_2"] == "42" ||
							$data[$i]["reason_2"] == "43" ||
							$wk_ake_flg == 1
						) {
							;
						} else {
							$data[$i]["font_name"] = "";
							$data[$i]["font_color_id"] = "";
							$data[$i]["back_color_id"] = "";
						}
					}
				}
/*
            } else {
			// 予定の場合
				//休暇を名称ではなくIDで確認
				if ($atdptn_ptn_id != "10") {
					$data[$i]["font_name"] = "";
					$data[$i]["font_color_id"] = "";
					$data[$i]["back_color_id"] = "";
				}
			}
*/
			//-------------------------------------------------------------------
			//時間算出
			//-------------------------------------------------------------------
			$data[$i]["count"] = "";
		}
		$ret = $data;
		return $ret;
	}
}
?>

