<?/*

画面パラメータ
	$session
		セッションID
	$pt_id
		患者ID
	$problem_id
		プロブレムＩＤ

*/


/**
 * 氏、名を氏名フォーマットに従い氏名に変換します。
 * get_values.iniのget_emp_kanji_name()と同様の変換ルールで変換します。
 * @param $lt_nm 氏
 * @param $ft_nm 名
 * @return 氏名
 */
function format_kanji_name($lt_nm,$ft_nm)
{
	$name = "$lt_nm"." "."$ft_nm";
	return $name;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<link rel="stylesheet" type="text/css" href="css/main_summary.css">
<?
require_once("about_comedix.php");
require("show_kanja_joho.ini");
require("summary_common.ini");
require("./get_values.ini");
require_once("get_menu_label.ini");
require("label_by_profile_type.ini");

//ページ名
$fname = $PHP_SELF;

//==============================
//セッションのチェック
//==============================
$session = qualify_session($session,$fname);
if($session == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
// 権限チェック
//==============================
$summary = check_authority($session, 57, $fname);
if ($summary == "0")
{
	showLoginPage();
	exit;
}

//==============================
//DBコネクション
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//==============================
//プロブレムリスト履歴情報取得
//==============================
$sql = "select summary_problem_list_rireki.*,empmst.emp_lt_nm,empmst.emp_ft_nm,empmst.emp_job,jobmst.job_nm from summary_problem_list_rireki ";
$sql .= "left join empmst on empmst.emp_id = summary_problem_list_rireki.update_emp_id ";
$sql .= "left join jobmst on jobmst.job_id = empmst.emp_job ";
$cond = "where summary_problem_list_rireki.ptif_id = '$pt_id' and summary_problem_list_rireki.problem_id = '$problem_id'";
$cond .= " order by summary_problem_list_rireki.rireki_index";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
//表示件数
$list_disp_num = pg_numrows($sel);
//現在のプロブレム表示値
$now_problem_no = pg_result($sel,$list_disp_num - 1,"problem_list_no");
$now_problem    = pg_result($sel,$list_disp_num - 1,"problem");
$now_problem_disp = "#".h($now_problem_no)."&nbsp".h($now_problem);


//==============================
//フィールド一覧
//==============================
$input_field_array = array();
$input_field_array[0]="problem_list_no";
$input_field_array[1]="problem";
$input_field_array[2]="problem_class";
$input_field_array[3]="problem_kind";
$input_field_array[4]="disease_name_flg";
$input_field_array[5]="start_date";
$input_field_array[6]="end_date";
$input_field_array[7]="memo";
$input_field_array[8]="other_editable";

$input_field_name_array = array();
$input_field_name_array[0]="番号";
$input_field_name_array[1]="プロブレム(問題)";
$input_field_name_array[2]="問題分類";
$input_field_name_array[3]="問題区別";
$input_field_name_array[4]="病名";
$input_field_name_array[5]="出現期日";
$input_field_name_array[6]="解決日";
$input_field_name_array[7]="備考";
$input_field_name_array[8]="他ユーザーの変更";

$input_field_type_array = array();
$input_field_type_array[0]="mozi";
$input_field_type_array[1]="mozi";
$input_field_type_array[2]="mozi";
$input_field_type_array[3]="mozi";
$input_field_type_array[4]="flg";
$input_field_type_array[5]="date";
$input_field_type_array[6]="date";
$input_field_type_array[7]="mozi";
$input_field_type_array[8]="kafuka";

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);
//==========================================================================================
//HTML出力
//==========================================================================================
//==============================
//スクリプト/スタイルシート/BODY宣言 出力
//==============================
?>
<title>CoMedix <?=get_report_menu_label($con, $fname)?>｜プロブレムリスト変更履歴</title>
</head>
<body>
<div style="padding:4px">

<? //ヘッダー 出力 ?>
<? summary_common_show_dialog_header("プロブレム変更履歴"); ?>



<table class="list_table" style="margin-top:8px" cellspacing="1">
	<tr>
		<th style="width:100px; text-align:center">プロブレム</th>
		<td><?=$now_problem_disp?></td>
</table>


<? //履歴リスト表示 ?>
<table class="prop_table" style="margin-top:8px" cellspacing="1">
	<tr>
		<th width="80">更新年月日</th>
		<th width="90">更新者職種</th>
		<th width="110">更新者氏名</th>
		<th style="text-align:left">変更点</th>
	</tr>
	<?
	for($i=0;$i<$list_disp_num;$i++) {
		$update_date = pg_result($sel,$i,"update_date");
		$update_date_disp = substr($update_date, 0, 4)."/".substr($update_date, 4, 2)."/".substr($update_date, 6, 2);

		$shokushu = pg_result($sel,$i,"job_nm");
		$kisaisha_job_disp = h($shokushu);

		$lt_nm = pg_result($sel,$i,"emp_lt_nm");
		$ft_nm = pg_result($sel,$i,"emp_ft_nm");
		$kisaisha = format_kanji_name($lt_nm,$ft_nm);
		$kisaisha_name_disp = h($kisaisha);
	?>
	<tr>
		<td align="center" style="padding:2px"><?=$update_date_disp?></td>
		<td align="center" style="padding:2px"><?=$kisaisha_job_disp?></td>
		<td align="center" style="padding:2px"><?=$kisaisha_name_disp?></td>
		<!-- 変更点START -->
		<td>
			<? if($i==0) {//先頭行の場合 ?>
				新規登録
			<? } else { //先頭以外の場合
				$no_update = true;

				//全入力フィールドに対して
				for($i_fld=0;$i_fld<9;$i_fld++) {
					$input_field = $input_field_array[$i_fld];
					$input_field_name = $input_field_name_array[$i_fld];
					$input_field_type = $input_field_type_array[$i_fld];

					$ima_data = pg_result($sel,$i,$input_field);
					$mae_data = pg_result($sel,$i-1,$input_field);

					//変更があった場合
					if($ima_data != $mae_data) {
						//最初のフィールドの場合
						if($no_update) {
							$no_update = false;
							?>
							<table>
							<?
						}
						?>
						<tr>
							<td style="vertical-align:top"><div style="white-space:nowrap; background-color:#f8fdb7; border:1px solid #c5d506; color:#7f8a04; text-align:center"><?=$input_field_name?></div></td>
							<td style="vertical-align:top">
								<?
								$disp_data = $mae_data;
								if($input_field_type == "flg") {
									if($disp_data == "t") {
										$disp_data = "<span style='color: red;'>●</span>";
									} else {
										$disp_data = "○";
									}
								} elseif($input_field_type == "kafuka") {
									if($disp_data == "t") {
										$disp_data = "可";
									} else {
										$disp_data = "不可";
									}
								} elseif($input_field_type == "date") {
									if($disp_data != "        ") {
										$disp_data = substr($disp_data, 0, 4)."/".substr($disp_data, 4, 2)."/".substr($disp_data, 6, 2);
									} else {
										$disp_data = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									}
								} else { //($input_field_type == "mozi")
									$disp_data = h($disp_data);
								}
								?>
								<div style="background-color:#f4f4f4; text-align:center"><?=$disp_data?></div>
							</td>
							<td style="color:#ff59a4; vertical-align:top">&nbsp;→</td>
							<td style="color:#ff59a4; vertical-align:top">
								<?
								$disp_data = $ima_data;
								//※ここから下は上と同じロジック。
								if($input_field_type == "flg") {
									if($disp_data == "t") {
										$disp_data = "<span style='color: red;'>●</span>";
									} else {
										$disp_data = "○";
									}
								}
								elseif($input_field_type == "kafuka") {
									if($disp_data == "t") {
										$disp_data = "可";
									} else {
										$disp_data = "不可";
									}
								}
								elseif($input_field_type == "date") {
									if($disp_data != "        ") {
										$disp_data = substr($disp_data, 0, 4)."/".substr($disp_data, 4, 2)."/".substr($disp_data, 6, 2);
									} else {
										$disp_data = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									}
								} else { //($input_field_type == "mozi")
									$disp_data = h($disp_data);
								}
								?>
								<div style="background-color:#f4f4f4; text-align:center"><?=$disp_data ?></div>
							</td>
						</tr>
						<?
					}
				}
				//変更があった場合
				if(!$no_update) {
					?>
					</table>
					<?
				} else {//変更がなかった場合
					?>
					変更なし
					<?
				}
			}
			?>
		</td>
		<!-- 変更点END -->
	</tr>
	<?
	}
	?>
</table>

</div>
</body>
</html>
<?

pg_close($con);
?>
