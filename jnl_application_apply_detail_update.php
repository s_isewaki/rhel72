<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" action="jnl_application_apply_detail.php" method="post">
<input type="hidden" name="apply_title" value="<? echo($apply_title); ?>">
<input type="hidden" name="content" value="<? echo($content); ?>">
<?
foreach ($filename as $tmp_filename) {
	echo("<input type=\"hidden\" name=\"filename[]\" value=\"$tmp_filename\">\n");
}
foreach ($file_id as $tmp_file_id) {
	echo("<input type=\"hidden\" name=\"file_id[]\" value=\"$tmp_file_id\">\n");
}

for ($i = 1; $i <= $approve_num; $i++) {
	$varname = "regist_emp_id$i";
	echo("<input type=\"hidden\" name=\"regist_emp_id$i\" value=\"{$$varname}\">\n");
}
?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="apply_id" value="<? echo($apply_id); ?>">
<input type="hidden" name="approve_num" value="<? echo($approve_num); ?>">
<input type="hidden" name="back" value="t">
<input type="hidden" name="notice_emp_id" id="notice_emp_id" value="<?=$notice_emp_id?>">
<input type="hidden" name="notice_emp_nm" id="notice_emp_nm" value="<?=$notice_emp_nm?>">
<input type="hidden" name="wkfw_history_no" value="<?=$wkfw_history_no?>">

</form>

<?
$fname=$PHP_SELF;

require_once("about_session.php");
require_once("about_authority.php");
require_once("get_values.ini");
require_once("jnl_application_workflow_common_class.php");


// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 決裁・申請権限のチェック
$checkauth = check_authority($session, 80, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (strlen($apply_title) > 80) {
	echo("<script type=\"text/javascript\">alert('表題が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// 添付ファイルの確認
if (!is_dir("jnl_apply")) {
	mkdir("jnl_apply", 0755);
}
if (!is_dir("jnl_apply/tmp")) {
	mkdir("jnl_apply/tmp", 0755);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$ext = strrchr($filename[$i], ".");
	
	$tmp_filename = "jnl_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	if (!is_file($tmp_filename)) {
		echo("<script language=\"javascript\">alert('添付ファイルが削除された可能性があります。\\n再度添付してください。');</script>");
		//sekiai
		//		echo("<script language=\"javascript\">document.items.submit();</script>");
		echo("<script type=\"text/javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

$obj = new jnl_application_workflow_common_class($con, $fname);

// トランザクションを開始
pg_query($con, "begin");

// 職員情報取得
$arr_empmst = $obj->get_empmst($session);
$emp_id        = $arr_empmst[0]["emp_id"];

//テンプレートの場合XML形式のテキスト$contentを作成
if ($wkfw_content_type == "2") {
	$ext = ".php";
	$savexmlfilename = "jnl_workflow/tmp/{$session}_x{$ext}";

	$arr_wkfw_template_history = $obj->get_wkfw_template_history($wkfw_id, $wkfw_history_no);
	$wkfw_content = $arr_wkfw_template_history["wkfw_content"];

	$wkfw_content_xml = ereg_replace("^.*<\? // XML", "<? // XML", $wkfw_content);
	$fp = fopen($savexmlfilename, "w");
	if (!$fp) {
		echo("<script language='javascript'>alert('一時ファイルがオープンできません。再度、更新してください。$savexmlfilename');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	if(!fwrite($fp, $wkfw_content_xml, 2000000)) {
		fclose($fp);
		echo("<script language='javascript'>alert('一時ファイルに書込みできません。再度、更新してください。');</script>");
		echo("<script language='javascript'>history.back();</script>");
	}
	fclose($fp);

	include( $savexmlfilename );
}

// 承認状況チェック
$wkfw = get_wkfwmst($con, $wkfw_id, $fname);
$approve_label = ($wkfw["approve_label"] != "2") ? "承認" : "確認";

$apv_cnt = $obj->get_applyapv_cnt($apply_id);
if($apv_cnt > 0) {
	echo("<script language=\"javascript\">alert('他の{$approve_label}状況が変更されたため、更新できません。');</script>");
	echo("<script language=\"javascript\">document.items.submit();</script>");
}

// メールを送る設定の場合、データ更新前の宛先情報を取得
if ($wkfw["send_mail_flg"] == "t") {
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from jnl_applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id";
	if ($wkfw["wkfw_appr"] == "2") {
		$cond .= " and a.apv_order = 1";
	}
	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$before_updating_to_addresses = array();
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$before_updating_to_addresses[] = $row["emp_email2"];
		}
	}
}

// 申請更新
$obj->update_apply($apply_id, $content, $apply_title,$emp_id);






//集計処理用データの更新処理を行う↓

//レポートの種類
//1-病院日報
//2-病院月報１
//3-病院月報２
//4-老健日報
//5-老健月報
if($report_flg == "1")
{
	$total = array();
	$total[0] = $apply_id;
	$total[1] = $emp_id;//再申請した職員の職員コードを入力する
	$total[2] = "0";//承認ステータス「申請中」に設定する
	
	$total_cnt = 3;
	
	for($wcnt1=1;$wcnt1<=21;$wcnt1++) 
	{
		
		for($wcnt2=2;$wcnt2<=15;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$total[$total_cnt] = null;
			}
			else
			{
				$total[$total_cnt] = $$wrk;
			}
			$total_cnt++;
			
		}
	}
	
	if($nyuin_gokei =="")
	{
		//integerの属性なので空文字をnullに変換する
		$total[$total_cnt] = null;
	}
	else
	{
		$total[$total_cnt] = $nyuin_gokei;
	}
	
	$total_cnt++;
	
	if($taiin_gokei =="")
	{
		//integerの属性なので空文字をnullに変換する
		$total[$total_cnt] = null;
	}
	else
	{
		$total[$total_cnt] = $taiin_gokei;
	}
	
	
	// 申請更新
	$obj->update_add_up_apply_report1($total,$apply_id);

}
elseif($report_flg == "2")
{
	
	//病院月報報告書１（行為件数）の更新処理
	
	//病院日報報告書の更新処理
	
	$total = array();
	$total[0] = $apply_id;
	$total[1] = $emp_id;//再申請した職員の職員コードを入力する
	$total[2] = "0";//承認ステータス「申請中」に設定する
	
	$total_cnt = 3;
	
	
	for($wcnt1=1;$wcnt1<=51;$wcnt1++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "month_action_d".$wcnt1;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$total[$total_cnt] = null;
		}
		else
		{
			$total[$total_cnt] = $$wrk;
		}
		$total_cnt++;
	}
	
	// 申請更新
	$obj->update_add_up_apply_report2($total,$apply_id);
	
	
}
elseif($report_flg == "3")
{
	
	//病院月報報告書２（患者データ）の更新処理
	
	//病院日報報告書の更新処理
	
	$total = array();
	$total[0] = $apply_id;
	$total[1] = $emp_id;//再申請した職員の職員コードを入力する
	$total[2] = "0";//承認ステータス「申請中」に設定する
	
	$total_cnt = 3;
	
	
	for($wcnt1=1;$wcnt1<=45;$wcnt1++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "month_patient_d".$wcnt1;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$total[$total_cnt] = null;
		}
		else
		{
			$total[$total_cnt] = $$wrk;
		}
		$total_cnt++;
	}
	
	// 申請更新
	$obj->update_add_up_apply_report3($total,$apply_id);
	
	
}
elseif($report_flg == "4")
{
	//老健日報の更新
	
	//*************1: jnl_nurse_home_mainの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_main = array();
	$ret_main[0] = $apply_id;
	$ret_main[1] = $emp_id;
	
	//申請処理
	$ret_main[2] = '0';//状態：申請中
	
	$ret_main[3] = 'false';//削除フラグは固定で"false"
	
	//集計用データの更新処理(jnl_nurse_home_main)
	$obj->update_add_up_apply_nurse_report_main($ret_main,$apply_id);
	
	
	//*************2: jnl_nurse_home_day_visitの登録
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=7;$wcnt1++) 
	{
		
		//登録データの初期化
		$ret_day_visit = array();
		$ret_day_visit_cnt = 2;
		
		//申請IDを設定
		$ret_day_visit[0] = $apply_id;
		
		//時間帯を設定
		$ret_day_visit[1] = $wcnt1;
		
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "cr_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_day_visit[$ret_day_visit_cnt] = null;
			}
			else
			{
				$ret_day_visit[$ret_day_visit_cnt] = $$wrk;
			}
			$ret_day_visit_cnt++;
		}
		
		//更新実行！！！！！！！！！！
		$obj->update_add_up_apply_nurse_report_day1($ret_day_visit,$apply_id,$wcnt1);
		
		
	}
	
	//*************3: jnl_nurse_home_day_visit_preventionの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_visit_prevention = array();
	$ret_visit_prevention[0] = $apply_id;
	
	if($cp_d1_r1 =="")
	{
		//integerの属性なので空文字をnullに変換する
		$ret_visit_prevention[1] = null;
	}
	else
	{
		$ret_visit_prevention[1] = $cp_d1_r1;
	}
	
	if($cp_d1_r2 =="")
	{
		//integerの属性なので空文字をnullに変換する
		$ret_visit_prevention[2] = null;
	}
	else
	{
		$ret_visit_prevention[2] = $cp_d1_r2;
	}
	
	if($cp_d1_r3 =="")
	{
		//integerの属性なので空文字をnullに変換する
		$ret_visit_prevention[3] = null;
	}
	else
	{
		$ret_visit_prevention[3] = $cp_d1_r3;
	}
	
	//集計用データの更新処理(jnl_nurse_home_day_visit_prevention)
	$obj->update_add_up_apply_nurse_report_day2($ret_visit_prevention,$apply_id);
	
	
	//*************4: jnl_nurse_home_day_enterの登録
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=2;$wcnt1++) 
	{
		
		//登録データの初期化
		$ret_day_enter= array();
		$ret_day_enter_cnt = 2;
		
		//申請IDを設定
		$ret_day_enter[0] = $apply_id;
		
		//種別を設定
		$ret_day_enter[1] = $wcnt1;
		
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "ent_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_day_enter[$ret_day_enter_cnt] = null;
			}
			else
			{
				$ret_day_enter[$ret_day_enter_cnt] = $$wrk;
			}
			$ret_day_enter_cnt++;
		}
		
		//更新実行！！！！！！！！！！
		$obj->update_add_up_apply_nurse_report_day3($ret_day_enter,$apply_id,$wcnt1);
		
		
	}
	
	
	//*************5: jnl_nurse_home_day_shortの登録
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=2;$wcnt1++) 
	{
		
		//登録データの初期化
		$ret_day_short= array();
		$ret_day_short_cnt = 2;
		
		//申請IDを設定
		$ret_day_short[0] = $apply_id;
		
		//種別を設定
		$ret_day_short[1] = $wcnt1;
		
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "sht_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_day_short[$ret_day_short_cnt] = null;
			}
			else
			{
				$ret_day_short[$ret_day_short_cnt] = $$wrk;
			}
			$ret_day_short_cnt++;
		}
		
		//更新実行！！！！！！！！！！
		$obj->update_add_up_apply_nurse_report_day4($ret_day_short,$apply_id,$wcnt1);
		
		
	}
	
	
	//*************6: jnl_nurse_home_day_short_preventionの登録
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=2;$wcnt1++) 
	{
		
		//登録データの初期化
		$ret_short_prevention = array();
		$ret_short_prevention_cnt = 2;
		
		//申請IDを設定
		$ret_short_prevention[0] = $apply_id;
		
		//種別を設定
		$ret_short_prevention[1] = $wcnt1;
		
		
		for($wcnt2=1;$wcnt2<=3;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "psh_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_short_prevention[$ret_short_prevention_cnt] = null;
			}
			else
			{
				$ret_short_prevention[$ret_short_prevention_cnt] = $$wrk;
			}
			$ret_short_prevention_cnt++;
		}
		
		//更新実行！！！！！！！！！！
		$obj->update_add_up_apply_nurse_report_day5($ret_short_prevention,$apply_id,$wcnt1);
		
	}
	
	
	//*************7: jnl_nurse_home_day_rehabの登録
	
	//登録データの初期化
	$ret_day_rehab = array();
	$ret_day_rehab_cnt = 1;
	
	//申請IDを設定
	$ret_day_rehab[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "rhab_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_day_rehab[$ret_day_rehab_cnt] = null;
		}
		else
		{
			$ret_day_rehab[$ret_day_rehab_cnt] = $$wrk;
		}
		$ret_day_rehab_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_day6($ret_day_rehab,$apply_id);
	
	
	
	//*************8: jnl_nurse_home_day_rehab_preventionの登録
	
	//登録データの初期化
	$ret_rehab_prevention = array();
	$ret_rehab_prevention_cnt = 1;
	
	//申請IDを設定
	$ret_rehab_prevention[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=3;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "prhab_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_rehab_prevention[$ret_rehab_prevention_cnt] = null;
		}
		else
		{
			$ret_rehab_prevention[$ret_rehab_prevention_cnt] = $$wrk;
		}
		$ret_rehab_prevention_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_day7($ret_rehab_prevention,$apply_id);
	
	
	
	//************9: jnl_nurse_home_day_cntの登録
	
	//登録データの初期化
	$ret_day_cnt = array();
	$ret_day_cnt_cnt = 1;
	
	//申請IDを設定
	$ret_day_cnt[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=8;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "io_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_day_cnt[$ret_day_cnt_cnt] = null;
		}
		else
		{
			$ret_day_cnt[$ret_day_cnt_cnt] = $$wrk;
		}
		$ret_day_cnt_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_day8($ret_day_cnt,$apply_id);

}
elseif($report_flg == "5")
{
	//老健月報の更新
	
	//*************1: jnl_nurse_home_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_main = array();
	$ret_main[0] = $apply_id;
	$ret_main[1] = $emp_id;
	
	//申請処理
	$ret_main[2] = '0';//状態：申請中
	
	$ret_main[3] = 'false';//削除フラグは固定で"false"
	
	//集計用データの登録処理(jnl_nurse_home_main)
	$obj->update_add_up_apply_nurse_report_main($ret_main,$apply_id);
	

	//************2: jnl_nurse_home_mon_enterの登録
	
	//登録データの初期化
	$ret_mon_enter = array();
	$ret_mon_enter_cnt = 1;
	
	//申請IDを設定
	$ret_mon_enter[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=29;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "ment_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_enter[$ret_mon_enter_cnt] = null;
		}
		else
		{
			$ret_mon_enter[$ret_mon_enter_cnt] = $$wrk;
		}
		$ret_mon_enter_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon1($ret_mon_enter,$apply_id);
	
	//************3: jnl_nurse_home_mon_shortの登録
	
	//登録データの初期化
	$ret_mon_short = array();
	$ret_mon_short_cnt = 1;
	
	//申請IDを設定
	$ret_mon_short[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=14;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "msh_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_short[$ret_mon_short_cnt] = null;
		}
		else
		{
			$ret_mon_short[$ret_mon_short_cnt] = $$wrk;
		}
		$ret_mon_short_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon2($ret_mon_short,$apply_id);
	
	
	
	//************4: jnl_nurse_home_mon_commuteの登録
	
	//登録データの初期化
	$ret_mon_commute = array();
	$ret_mon_commute_cnt = 1;
	
	//申請IDを設定
	$ret_mon_commute[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=14;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "mcr_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_commute[$ret_mon_commute_cnt] = null;
		}
		else
		{
			$ret_mon_commute[$ret_mon_commute_cnt] = $$wrk;
		}
		$ret_mon_commute_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon3($ret_mon_commute,$apply_id);
	
	//************5: jnl_nurse_home_mon_commute_preの登録
	
	//登録データの初期化
	$ret_mon_commute_pre = array();
	$ret_mon_commute_pre_cnt = 1;
	
	//申請IDを設定
	$ret_mon_commute_pre[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=9;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "mpcr_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_commute_pre[$ret_mon_commute_pre_cnt] = null;
		}
		else
		{
			$ret_mon_commute_pre[$ret_mon_commute_pre_cnt] = $$wrk;
		}
		$ret_mon_commute_pre_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon4($ret_mon_commute_pre,$apply_id);
	
	
	//************6: jnl_nurse_home_mon_visitの登録
	
	//登録データの初期化
	$ret_mon_visit = array();
	$ret_mon_visit_cnt = 1;
	
	//申請IDを設定
	$ret_mon_visit[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=3;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "mvr_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_visit[$ret_mon_visit_cnt] = null;
		}
		else
		{
			$ret_mon_visit[$ret_mon_visit_cnt] = $$wrk;
		}
		$ret_mon_visit_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon5($ret_mon_visit,$apply_id);
	
	
	//************7: jnl_nurse_home_mon_visit_preの登録
	
	//登録データの初期化
	$ret_mon_visit_pre = array();
	$ret_mon_visit_pre_cnt = 1;
	
	//申請IDを設定
	$ret_mon_visit_pre[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=2;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "mpvr_d1_r".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$ret_mon_visit_pre[$ret_mon_visit_pre_cnt] = null;
		}
		else
		{
			$ret_mon_visit_pre[$ret_mon_visit_pre_cnt] = $$wrk;
		}
		$ret_mon_visit_pre_cnt++;
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_nurse_report_mon6($ret_mon_visit_pre,$apply_id);
	
}
elseif($report_flg == "6")
{
	//返戻月報の登録
	
	//*************1: jnl_return_mainの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_main = array();
	$ret_main[0] = $apply_id;
	$ret_main[1] = $emp_id;
	
	$ret_main[2] = "0";//承認ステータス「申請中」に設定する
	
	//集計用データの登録処理(jnl_return_main)
	$obj->update_add_up_apply_ret_report1($ret_main,$apply_id);
	
	
	
	//*************2: jnl_return_totalの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_total = array();
	
	$ret_total[0] = $apply_id;
	
	if($d0_r1 == "")
	{
		//integerの属性なので空文字をnullに変換する
		$ret_total[1] = null;
	}
	else
		{	$ret_total[1] = $d0_r1; }	
	
	if($d0_r2 == "")
	{
		//integerの属性なので空文字をnullに変換する
		$ret_total[2] = null;
	}
	else
		{	$ret_total[2] = $d0_r2;	}	
	
	
	//集計用データの登録処理(jnl_return_total)
	$obj->update_add_up_apply_ret_report2($ret_total,$apply_id);
	
	
	
	//*************3: jnl_return_factorの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_factor = array();
	$ret_factor[0] = $apply_id;
	
	$kind_consult = 1;
	$kind_factor = 1;
	
	$ret_factor[1] = $kind_factor;
	$ret_factor[2] = $kind_consult;
	
	$ret_factor_cnt = 3;
	
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=52;$wcnt1++) 
	{
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "fac_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_factor[$ret_factor_cnt] = null;
			}
			else
			{
				$ret_factor[$ret_factor_cnt] = $$wrk;
			}
			$ret_factor_cnt++;
		}
		
		
		//************登録処理******************/
		if(($wcnt1 % 2) == 0)
		{
			//1レコード分のデータが取得できたのでupdate実行
			
			//登録実行！！！！！！！！！！
			$obj->update_add_up_apply_ret_report3($ret_factor,$apply_id,$kind_factor,$kind_consult);
			
			//登録データの初期化
			$ret_factor = array();
			$ret_factor[0] = $apply_id;
			
			$kind_factor = 0;
			
			switch ($wcnt1 + 1) 
			{
				case 1:
				case 2:
				case 3:
				case 4:
					//"保険証関係誤り";
					$kind_factor = 1;
					break;
				
				case 5:
				case 6:
				case 7:
				case 8:
					//"病名不備";
					$kind_factor = 2;
					break;
				
				case 9:
				case 10:
				case 11:
				case 12:
					//"継続承認外疾病";
					$kind_factor = 3;
					break;
				
				case 13:
				case 14:
				case 15:
				case 16:
					//"その他"(事務的);
					$kind_factor = 4;
					break;
				
				case 17:
				case 18:
				case 19:
				case 20:
					//"投薬";
					$kind_factor = 5;
					break;
				
				case 21:
				case 22:
				case 23:
				case 24:
					//"注射";
					$kind_factor = 6;
					break;
				
				case 25:
				case 26:
				case 27:
				case 28:
					//"検査";
					$kind_factor = 7;
					break;
				
				case 29:
				case 30:
				case 31:
				case 32:
					//"処置";
					$kind_factor = 8;
					break;
				
				case 33:
				case 34:
				case 35:
				case 36:
					//"手術";
					$kind_factor = 9;
					break;
				
				case 37:
				case 38:
				case 39:
				case 40:
					//"放射線";
					$kind_factor = 10;
					break;
				
				case 41:
				case 42:
				case 43:
				case 44:
					//"リハビリ";
					$kind_factor = 11;
					break;
				
				case 45:
				case 46:
				case 47:
				case 48:
					//"指導";
					$kind_factor = 12;
					break;
				
				case 49:
				case 50:
				case 51:
				case 52:
					//"その他"(診療的);
					$kind_factor = 13;
					break;
			}
			
			//要因種類設定
			$ret_factor[1] = $kind_factor;
			
			
			if($kind_consult == 1)
			{
				//入院の次は外来
				$kind_consult = 2;
			}
			elseif($kind_consult == 2)
			{
				//外来の次は入院
				$kind_consult = 1;
			}
			
			//診断種類設定
			$ret_factor[2] = $kind_consult;
			
			$ret_factor_cnt = 3;
		}
		//************登録処理******************/
		
	}
	
	
	
	//*************4: jnl_return_assessmentの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_assessment = array();
	
	
	$ret_assessment[0] = $apply_id;
	
	$kind_consult = 1;
	$kind_factor = 1;
	
	$ret_assessment[1] = $kind_factor;
	$ret_assessment[2] = $kind_consult;
	
	$ret_assessment_cnt = 3;
	
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=44;$wcnt1++) 
	{
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "ass_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_assessment[$ret_assessment_cnt] = null;
			}
			else
			{
				$ret_assessment[$ret_assessment_cnt] = $$wrk;
			}
			$ret_assessment_cnt++;
		}
		
		
		//************登録処理******************/
		if(($wcnt1 % 2) == 0)
		{
			//1レコード分のデータが取得できたのでupdate実行
			
			//登録実行！！！！！！！！！！
			$obj->update_add_up_apply_ret_report4($ret_assessment,$apply_id,$kind_factor,$kind_consult);
			
			//登録データの初期化
			$ret_assessment = array();
			$ret_assessment[0] = $apply_id;
			
			$kind_factor = 0;
			
			switch ($wcnt1 + 1) 
			{
				case 1:
				case 2:
				case 3:
				case 4:
					//"投薬";
					$kind_factor = 1;
					break;
				
				case 5:
				case 6:
				case 7:
				case 8:
					//"注射";
					$kind_factor = 2;
					break;
				
				case 9:
				case 10:
				case 11:
				case 12:
					//"検査";
					$kind_factor = 3;
					break;
				
				case 13:
				case 14:
				case 15:
				case 16:
					//"処置;
					$kind_factor = 4;
					break;
				
				case 17:
				case 18:
				case 19:
				case 20:
					//"手術";
					$kind_factor = 5;
					break;
				
				case 21:
				case 22:
				case 23:
				case 24:
					//"放射線";
					$kind_factor = 6;
					break;
				
				case 25:
				case 26:
				case 27:
				case 28:
					//"リハビリ";
					$kind_factor = 7;
					break;
				
				case 29:
				case 30:
				case 31:
				case 32:
					//"指導";
					$kind_factor = 8;
					break;
				
				case 33:
				case 34:
				case 35:
				case 36:
					//"食事";
					$kind_factor = 9;
					break;
				
				case 37:
				case 38:
				case 39:
				case 40:
					//"処方箋";
					$kind_factor = 10;
					break;
				
				case 41:
				case 42:
				case 43:
				case 44:
					//"その他";
					$kind_factor = 11;
					break;
				
			}
			
			//要因種類設定
			$ret_assessment[1] = $kind_factor;
			
			
			if($kind_consult == 1)
			{
				//入院の次は外来
				$kind_consult = 2;
			}
			elseif($kind_consult == 2)
			{
				//外来の次は入院
				$kind_consult = 1;
			}
			
			//診断種類設定
			$ret_assessment[2] = $kind_consult;
			
			$ret_assessment_cnt = 3;
		}
		//************登録処理******************/
		
	}
	
	
	//*************5: jnl_return_re_chargeの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_re_charge = array();
	
	$ret_re_charge[0] = $apply_id;
	
	$kind_factor = 1;
	$kind_consult = 1;
	
	$ret_re_charge[1] = $kind_factor;
	$ret_re_charge[2] = $kind_consult;
	
	
	$ret_re_charge_cnt = 3;
	
	//病院日報のデータを縦・横をループさせてすべて取得する
	for($wcnt1=1;$wcnt1<=4;$wcnt1++) 
	{
		
		for($wcnt2=1;$wcnt2<=12;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "rech_d".$wcnt1."_r".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$ret_re_charge[$ret_re_charge_cnt] = null;
			}
			else
			{
				$ret_re_charge[$ret_re_charge_cnt] = $$wrk;
			}
			$ret_re_charge_cnt++;
		}
		
		
		//1レコード分のデータが取得できたのでupdate実行
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_ret_report5($ret_re_charge,$apply_id,$kind_factor,$kind_consult);
		
		//登録データの初期化
		$ret_re_charge = array();
		$ret_re_charge[0] = $apply_id;
		
		
		if(($wcnt1 + 1) > 2)
		{
			$kind_factor = 2;
		}
		else
		{
			$kind_factor = 1;
		}
		
		$ret_re_charge[1] = $kind_factor;
		
		if($kind_consult == 1)
		{
			//入院の次は外来
			$kind_consult = 2;
		}
		elseif($kind_consult == 2)
		{
			//外来の次は入院
			$kind_consult = 1;
		}
		
		//診断種類設定
		$ret_re_charge[2] = $kind_consult;
		
		$ret_re_charge_cnt = 3;
		
	}
	
	
	//*************6: jnl_return_commentの登録
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$ret_comment = array();
	
	$ret_comment[0] = $apply_id;
	
	$ret_comment[1] = $recom_d1_r1;
	$ret_comment[2] = $recom_d1_r2;
	
	$ret_comment[3] = $ascom_d1_r1;
	$ret_comment[4] = $ascom_d1_r2;
	
	//集計用データの登録処理(jnl_return_comment)
	$obj->update_add_up_apply_ret_report6($ret_comment,$apply_id);
	
	
	
}
elseif($report_flg == "7")
{


	//診療報酬総括表（医科）の更新
	
	//*************1: jnl_reward_synthesis_medi_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$synthesis_medi_main = array();
	$synthesis_medi_main[0] = $apply_id;
	$synthesis_medi_main[1] = $emp_id;
	
	$synthesis_medi_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_reward_synthesis_medi_main)
	$obj->update_add_up_apply_synthesis_medi_main($synthesis_medi_main,$apply_id);
	
	

	
	//************2: jnl_reward_synthesis_mediの更新
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "a".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//国保
	$kind_factor = '1';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "b".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//その他
	$kind_factor = '2';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 3)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "d1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "c".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "e".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//国保
	$kind_factor = '1';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "f".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "g1_".$wcnt3;
			}
			elseif($wcnt2 == 2)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "h1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "h".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "i".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//国保
	$kind_factor = '1';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "j".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "k1_".$wcnt3;
			}
			elseif($wcnt2 == 2)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "l1_".$wcnt3;
			}
			elseif($wcnt2 == 3)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "m1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "m".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_medi = array();
	$synthesis_medi_cnt = 1;
	
	//申請IDを設定
	$synthesis_medi[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '3';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "n1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "o".($wcnt2 - 1)."_".$wcnt3;
			}
			
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_medi[$synthesis_medi_cnt] = null;
			}
			else
			{
				$synthesis_medi[$synthesis_medi_cnt] = $$wrk;
			}
			$synthesis_medi_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report1($synthesis_medi,$apply_id,$kind_factor,$type_section);
	
}
elseif($report_flg == "8")
{
	
	
	//診療報酬総括表（歯科）の更新
	
	//*************1: jnl_reward_synthesis_dental_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$synthesis_dental_main = array();
	$synthesis_dental_main[0] = $apply_id;
	$synthesis_dental_main[1] = $emp_id;
	
	$synthesis_dental_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_reward_synthesis_dental_main)
	$obj->update_add_up_apply_synthesis_dental_main($synthesis_dental_main,$apply_id);
	
	
	
	
	//************2: jnl_reward_synthesis_dentalの更新
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "a".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//国保
	$kind_factor = '1';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "b".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//基金・連合会
	$type_section = '0';
	
	//その他
	$kind_factor = '2';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 3)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "d1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "c".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "e".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//国保
	$kind_factor = '1';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "f".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//返戻再請求分
	$type_section = '1';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "g1_".$wcnt3;
			}
			elseif($wcnt2 == 2)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "h1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "h".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//社保
	$kind_factor = '0';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "i".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//国保
	$kind_factor = '1';
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "j".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '2';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "k1_".$wcnt3;
			}
			elseif($wcnt2 == 2)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "l1_".$wcnt3;
			}
			elseif($wcnt2 == 3)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "m1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "m".$wcnt2."_".$wcnt3;
			}
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
	
	//登録データの初期化
	$synthesis_dental = array();
	$synthesis_dental_cnt = 1;
	
	//申請IDを設定
	$synthesis_dental[0] = $apply_id;
	
	//査定・過誤再審査請求分
	$type_section = '3';
	
	//その他
	$kind_factor = '2';
	
	
	for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=12;$wcnt3++) 
		{
			
			if($wcnt2 == 1)
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "n1_".$wcnt3;
			}
			else
			{
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = "o".($wcnt2 - 1)."_".$wcnt3;
			}
			
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$synthesis_dental[$synthesis_dental_cnt] = null;
			}
			else
			{
				$synthesis_dental[$synthesis_dental_cnt] = $$wrk;
			}
			$synthesis_dental_cnt++;
		}
		
	}
	
	//更新実行！！！！！！！！！！
	$obj->update_add_up_apply_synthesis_report2($synthesis_dental,$apply_id,$kind_factor,$type_section);
	
}
elseif($report_flg == "9")
{
	
	
	//入院診療行為別明細の更新
	
	//*************1: jnl_bill_diagnosis_enter_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$diagnosis_enter_main = array();
	$diagnosis_enter_main[0] = $apply_id;
	$diagnosis_enter_main[1] = $emp_id;
	
	$diagnosis_enter_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_bill_diagnosis_enter_main)
	$obj->update_add_up_apply_diagnosis_enter_main($diagnosis_enter_main,$apply_id);

	
	//************2: jnl_bill_diagnosis_enterの更新
	
	$str_post_name = array();
	$str_post_name[0] = 'a';
	$str_post_name[1] = 'b';
	$str_post_name[2] = 'c';
	$str_post_name[3] = 'd';
	$str_post_name[4] = 'e';
	$str_post_name[5] = 'f';
	$str_post_name[6] = 'g';
	$str_post_name[7] = 'h';
	$str_post_name[8] = 'i';
	$str_post_name[9] = 'j';
	$str_post_name[10] = 'k';
	$str_post_name[11] = 'l';
	$str_post_name[12] = 'm';
	$str_post_name[13] = 'n';
	$str_post_name[14] = 'o';
	$str_post_name[15] = 'p';
	$str_post_name[16] = 'q';
	$str_post_name[17] = 'r';
	$str_post_name[18] = 's';
	$str_post_name[19] = 't';
	$str_post_name[20] = 'v';
	$str_post_name[21] = 'w';
	$str_post_name[22] = 'x';
	$str_post_name[23] = 'y';
	$str_post_name[24] = 'z';
	$str_post_name[25] = 'aa';
	$str_post_name[26] = 'ab';
	$str_post_name[27] = 'ac';
	$str_post_name[28] = 'ad';
	$str_post_name[29] = 'ae';
	$str_post_name[30] = 'af';
	$str_post_name[31] = 'ag';
	
	
	for($wcnt1=0;$wcnt1<=31;$wcnt1++) 
	{
		
		//登録データの初期化
		$diagnosis_enter = array();
		
		//申請IDを設定
		$diagnosis_enter[0] = $apply_id;
		
		//セクションを設定
		$type_section = $wcnt1;
		
		$diagnosis_enter_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=16;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = $str_post_name[$wcnt1]."1_".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$diagnosis_enter[$diagnosis_enter_cnt] = null;
			}
			else
			{
				$diagnosis_enter[$diagnosis_enter_cnt] = $$wrk;
			}
			$diagnosis_enter_cnt++;
		}
		//1レコード分のデータが取得できたのでinsert実行
		$diagnosis_enter[$diagnosis_enter_cnt] = 'false';//削除フラグは固定で"false"
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_diagnosis_enter_report1($diagnosis_enter,$apply_id,$type_section);
		
	}
	
}
elseif($report_flg == "10")
{
	//外来診療行為別明細の更新
	
	//*************1: jnl_bill_diagnosis_visit_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$diagnosis_visit_main = array();
	$diagnosis_visit_main[0] = $apply_id;
	$diagnosis_visit_main[1] = $emp_id;
	
	$diagnosis_visit_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_bill_diagnosis_visit_main)
	$obj->update_add_up_apply_diagnosis_visit_main($diagnosis_visit_main,$apply_id);
	
	
	//************2: jnl_bill_diagnosis_visitの更新
	
	$str_post_name = array();
	$str_post_name[0] = 'a';
	$str_post_name[1] = 'b';
	$str_post_name[2] = 'c';
	$str_post_name[3] = 'd';
	$str_post_name[4] = 'e';
	$str_post_name[5] = 'f';
	$str_post_name[6] = 'g';
	$str_post_name[7] = 'h';
	$str_post_name[8] = 'i';
	$str_post_name[9] = 'j';
	$str_post_name[10] = 'k';
	$str_post_name[11] = 'l';
	$str_post_name[12] = 'm';
	$str_post_name[13] = 'n';
	$str_post_name[14] = 'o';
	$str_post_name[15] = 'p';
	$str_post_name[16] = 'q';
	$str_post_name[17] = 'r';
	$str_post_name[18] = 's';
	$str_post_name[19] = 't';
	
	
	for($wcnt1=0;$wcnt1<=19;$wcnt1++) 
	{
		
		//登録データの初期化
		$diagnosis_visit = array();
		
		//申請IDを設定
		$diagnosis_visit[0] = $apply_id;
		
		//セクションを設定
		$type_section = $wcnt1;
		
		$diagnosis_visit_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=17;$wcnt2++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = $str_post_name[$wcnt1]."1_".$wcnt2;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$diagnosis_visit[$diagnosis_visit_cnt] = null;
			}
			else
			{
				$diagnosis_visit[$diagnosis_visit_cnt] = $$wrk;
			}
			$diagnosis_visit_cnt++;
		}
		//1レコード分のデータが取得できたのでinsert実行
		$diagnosis_visit[$diagnosis_visit_cnt] = 'false';//削除フラグは固定で"false"
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_diagnosis_visit_report1($diagnosis_visit,$apply_id,$type_section);
		
	}
	
}
elseif($report_flg == "11")
{
		//療養費明細書の更新
		
		//*************1: jnl_bill_recuperation_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$bill_recuperation_main = array();
	$bill_recuperation_main[0] = $apply_id;
	$bill_recuperation_main[1] = $emp_id;
	
	//申請処理
	$bill_recuperation_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_bill_recuperation_main)
	$obj->update_add_up_apply_bill_recuperation_main($bill_recuperation_main,$apply_id);
	
	
	//************2: jnl_bill_bill_recuperationの更新
	
	$str_post_name1 = array();
	$str_post_name[0] = 'a';
	$str_post_name[1] = 'b';
	$str_post_name[2] = 'c';
	$str_post_name[3] = 'd';
	$str_post_name[4] = 'e';
	$str_post_name[5] = 'f';
	$str_post_name[6] = 'g';
	$str_post_name[7] = 'h';
	$str_post_name[8] = 'i';
	$str_post_name[9] = 'j';
	$str_post_name[10] = 'k';
	$str_post_name[11] = 'l';
	$str_post_name[12] = 'm';
	$str_post_name[13] = 'n';
	$str_post_name[14] = 'o';
	$str_post_name[15] = 'p';
	$str_post_name[16] = 'af';
	$str_post_name[17] = 'ag';
	$str_post_name[18] = 'ah';
	$str_post_name[19] = 'ai';
	
	
	for($wcnt1=0;$wcnt1<=19;$wcnt1++) 
	{
		
		//登録データの初期化
		$bill_recuperation = array();
		
		//申請IDを設定
		$bill_recuperation[0] = $apply_id;
		
		//セクションを設定
		$type_section = $wcnt1;
		
		$bill_recuperation_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=5;$wcnt2++) 
		{
			
			if($wcnt2 > 3)
			{
				$max_column = 9;
			}
			else
			{
				$max_column = 7;
			}
			
			for($wcnt3=1;$wcnt3<=$max_column;$wcnt3++) 
			{
				
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = $str_post_name[$wcnt1].$wcnt2."_".$wcnt3;
				
				if($$wrk =="")
				{
					//integerの属性なので空文字をnullに変換する
					$bill_recuperation[$bill_recuperation_cnt] = null;
				}
				else
				{
					$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
				}
				$bill_recuperation_cnt++;
			}
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_report1($bill_recuperation,$apply_id,$type_section);
		
	}
	
	$str_post_name2 = array();
	$str_post_name2[0] = 'q';
	$str_post_name2[1] = 'r';
	$str_post_name2[2] = 's';
	$str_post_name2[3] = 't';
	$str_post_name2[4] = 'u';
	$str_post_name2[5] = 'v';
	$str_post_name2[6] = 'w';
	$str_post_name2[7] = 'x';
	
	
	for($wcnt1=0;$wcnt1<=7;$wcnt1++) 
	{
		
		//登録データの初期化
		$bill_recuperation = array();
		
		//申請IDを設定
		$bill_recuperation[0] = $apply_id;
		
		
		if($wcnt1 >3)
		{
			//セクションを設定(公費請求額)
			$type_section = 1;
			$kind_sts = ($wcnt1 - 4);
			
		}
		else
		{
			//セクションを設定（保険請求分）
			$type_section = 0;
			$kind_sts = $wcnt1;
		}
		
		$bill_recuperation_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=6;$wcnt2++) 
		{
			
			for($wcnt3=1;$wcnt3<=4;$wcnt3++) 
			{
				
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = $str_post_name2[$wcnt1].$wcnt2."_".$wcnt3;
				
				if($$wrk =="")
				{
					//integerの属性なので空文字をnullに変換する
					$bill_recuperation[$bill_recuperation_cnt] = null;
				}
				else
				{
					$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
				}
				$bill_recuperation_cnt++;
			}
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_report2($bill_recuperation,$apply_id,$type_section,$kind_sts);
		
	}
	
	
	$str_post_name3 = array();
	$str_post_name3[0] = 'aa';
	$str_post_name3[1] = 'ab';
	
	for($wcnt1=0;$wcnt1<=1;$wcnt1++) 
	{
		
		$max_line = 13;
		if($wcnt1 == 0)
		{
			$max_line = 14;
		}
		else
		{
			$max_line = 13;
		}
		
		for($wcnt2=1;$wcnt2<=$max_line;$wcnt2++) 
		{
			
			//登録データの初期化
			$bill_recuperation = array();
			
			//申請IDを設定
			$bill_recuperation[0] = $apply_id;
			
			if($wcnt1 == 0)
			{
				if($wcnt2 == 14)
				{
					$sts_section = 0;
					$sts_kind = 0;
				}
				else
				{
					$sts_section = 1;
					$sts_kind = $wcnt2;
				}
			}
			else
			{
				$sts_section = 2;
				$sts_kind = $wcnt2;
			}
			
			$bill_recuperation_cnt = 1;
			
			for($wcnt3=1;$wcnt3<=3;$wcnt3++) 
			{
				
				
				if(($wcnt2 == 13) && (($wcnt3 == 1) ||($wcnt3 == 2)))
				{
					//$bill_recuperation[$wcnt3+2] = null;//入力項目が無いので固定でnullを入力する
					$bill_recuperation[$bill_recuperation_cnt] = null;//入力項目が無いので固定でnullを入力する
					$bill_recuperation_cnt++;
				}
				else
				{
					//ループ変数を仕様して入力項目のname属性を取得する		
					$wrk = $str_post_name3[$wcnt1].$wcnt2."_".$wcnt3;
					
					if($$wrk =="")
					{
						//integerの属性なので空文字をnullに変換する
						$bill_recuperation[$bill_recuperation_cnt] = null;
					}
					else
					{
						$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
					}
					$bill_recuperation_cnt++;
				}
				
			}
			
			//登録実行！！！！！！！！！！
			$obj->update_add_up_apply_bill_recuperation_report3($bill_recuperation,$apply_id,$sts_section,$sts_kind);
			
		}
	}
	
	
	$str_post_name4 = array();
	$str_post_name4[0] = 'ac1_3';
	$str_post_name4[1] = 'ad1_3';
	$str_post_name4[2] = 'ae1_3';
	
	for($wcnt1=0;$wcnt1<=2;$wcnt1++) 
	{
		//登録データの初期化
		$bill_recuperation = array();
		//申請IDを設定
		$bill_recuperation[0] = $apply_id;
		
		
		$sts_section = ($wcnt1 + 3);
		$sts_kind = ($wcnt1 + 14);
		
		
		//ステータスを設定
		$bill_recuperation[1] = null;//入力項目が無いので固定でnullを入力する
		$bill_recuperation[2] = null;//入力項目が無いので固定でnullを入力する
		
		$wrk = $str_post_name4[$wcnt1];
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$bill_recuperation[3] = null;
		}
		else
		{
			$bill_recuperation[3] = $$wrk;
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_report3($bill_recuperation,$apply_id,$sts_section,$sts_kind);
		
	}
	
	
	//登録データの初期化
	$bill_recuperation = array();
	
	$bill_recuperation_cnt = 1;
	
	//申請IDを設定
	$bill_recuperation[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=4;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=3;$wcnt3++) 
		{
			//ループ変数を仕様して入力項目のname属性を取得する
			$wrk = "y".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$bill_recuperation[$bill_recuperation_cnt] = null;
			}
			else
			{
				$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
			}
			$bill_recuperation_cnt++;
		}
	}
	
	for($wcnt2=1;$wcnt2<=3;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=4;$wcnt3++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "z".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$bill_recuperation[$bill_recuperation_cnt] = null;
			}
			else
			{
				$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
			}
			$bill_recuperation_cnt++;
		}
	}
	
	for($wcnt2=1;$wcnt2<=5;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "aj1_".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$bill_recuperation[$bill_recuperation_cnt] = null;
		}
		else
		{
			$bill_recuperation[$bill_recuperation_cnt] = $$wrk;
		}
		$bill_recuperation_cnt++;
	}
	
	//登録実行！！！！！！！！！！
	$obj->update_add_up_apply_bill_recuperation_report4($bill_recuperation,$apply_id);
	
}
else if($report_flg == "12")
{
	
	//予防給付療養費明細書の更新
	
	//*************1: jnl_bill_recuperation_prepay_mainの更新
	
	//返戻で入力されたデータを配列にまとめる（集計用データの登録処理用）
	$bill_recuperation_prepay_main = array();
	$bill_recuperation_prepay_main[0] = $apply_id;
	$bill_recuperation_prepay_main[1] = $emp_id;
	
	//申請処理
	$bill_recuperation_prepay_main[2] = '0';//状態：申請中
	
	//集計用データの更新処理(jnl_bill_recuperation_prepay_main)
	$obj->update_add_up_apply_bill_recuperation_prepay_main($bill_recuperation_prepay_main,$apply_id);
	
	
	//************2: jnl_bill_bill_recuperation_prepayの更新
	
	$str_post_name1 = array();
	$str_post_name[0] = 'a';
	$str_post_name[1] = 'b';
	$str_post_name[2] = 'd';
	$str_post_name[3] = 'e';
	$str_post_name[4] = 'f';
	$str_post_name[5] = 'h';
	$str_post_name[6] = 'i';
	$str_post_name[7] = 'j';
	$str_post_name[8] = 'l';
	$str_post_name[9] = 'm';
	$str_post_name[10] = 'n';
	$str_post_name[11] = 'p';
	$str_post_name[12] = 'af';
	$str_post_name[13] = 'ag';
	$str_post_name[14] = 'ah';
	$str_post_name[15] = 'ai';
	
	
	for($wcnt1=0;$wcnt1<=15;$wcnt1++) 
	{
		
		//登録データの初期化
		$bill_recuperation_prepay = array();
		
		//申請IDを設定
		$bill_recuperation_prepay[0] = $apply_id;
		
		//セクションを設定
		$type_section = $wcnt1;
		
		$bill_recuperation_prepay_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=5;$wcnt2++) 
		{
			
			if($wcnt2 > 3)
			{
				$max_column = 9;
			}
			else
			{
				$max_column = 7;
			}
			
			for($wcnt3=1;$wcnt3<=$max_column;$wcnt3++) 
			{
				
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = $str_post_name[$wcnt1].$wcnt2."_".$wcnt3;
				
				if($$wrk =="")
				{
					//integerの属性なので空文字をnullに変換する
					$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
				}
				else
				{
					$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
				}
				$bill_recuperation_prepay_cnt++;
			}
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_prepay_report1($bill_recuperation_prepay,$apply_id,$type_section);
		
	}
	
	$str_post_name2 = array();
	$str_post_name2[0] = 'q';
	$str_post_name2[1] = 'r';
	$str_post_name2[2] = 's';
	$str_post_name2[3] = 't';
	$str_post_name2[4] = 'u';
	$str_post_name2[5] = 'v';
	$str_post_name2[6] = 'w';
	$str_post_name2[7] = 'x';
	
	
	for($wcnt1=0;$wcnt1<=7;$wcnt1++) 
	{
		
		//登録データの初期化
		$bill_recuperation_prepay = array();
		
		//申請IDを設定
		$bill_recuperation_prepay[0] = $apply_id;
		
		
		if($wcnt1 >3)
		{
			//セクションを設定(公費請求額)
			$type_section = 1;
			$kind_sts = ($wcnt1 - 4);
			
		}
		else
		{
			//セクションを設定（保険請求分）
			$type_section = 0;
			$kind_sts = $wcnt1;
		}
		
		$bill_recuperation_prepay_cnt = 1;
		
		for($wcnt2=1;$wcnt2<=2;$wcnt2++) 
		{
			
			for($wcnt3=1;$wcnt3<=4;$wcnt3++) 
			{
				
				//ループ変数を仕様して入力項目のname属性を取得する		
				$wrk = $str_post_name2[$wcnt1].$wcnt2."_".$wcnt3;
				
				if($$wrk =="")
				{
					//integerの属性なので空文字をnullに変換する
					$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
				}
				else
				{
					$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
				}
				$bill_recuperation_prepay_cnt++;
			}
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_prepay_report2($bill_recuperation_prepay,$apply_id,$type_section,$kind_sts);
		
	}
	
	
	$str_post_name3 = array();
	$str_post_name3[0] = 'aa';
	
	for($wcnt1=0;$wcnt1<=0;$wcnt1++) 
	{
		
		$max_line = 14;
		
		for($wcnt2=1;$wcnt2<=$max_line;$wcnt2++) 
		{
			
			//登録データの初期化
			$bill_recuperation_prepay = array();
			
			//申請IDを設定
			$bill_recuperation_prepay[0] = $apply_id;
			
			if($wcnt2 == 14)
			{
				$sts_section = 0;
				$sts_kind = 0;
			}
			else
			{
				$sts_section = 1;
				$sts_kind = $wcnt2;
			}
			
			$bill_recuperation_prepay_cnt = 1;
			
			for($wcnt3=1;$wcnt3<=3;$wcnt3++) 
			{
				
				
				if(($wcnt2 == 13) && (($wcnt3 == 1) ||($wcnt3 == 2)))
				{
					//$bill_recuperation_prepay[$wcnt3+2] = null;//入力項目が無いので固定でnullを入力する
					$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;//入力項目が無いので固定でnullを入力する
					$bill_recuperation_prepay_cnt++;
				}
				else
				{
					//ループ変数を仕様して入力項目のname属性を取得する		
					$wrk = $str_post_name3[$wcnt1].$wcnt2."_".$wcnt3;
					
					if($$wrk =="")
					{
						//integerの属性なので空文字をnullに変換する
						$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
					}
					else
					{
						$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
					}
					$bill_recuperation_prepay_cnt++;
				}
				
			}
			
			//登録実行！！！！！！！！！！
			$obj->update_add_up_apply_bill_recuperation_prepay_report3($bill_recuperation_prepay,$apply_id,$sts_section,$sts_kind);
			
		}
	}
	
	
	$str_post_name4 = array();
	$str_post_name4[0] = 'ac1_3';
	$str_post_name4[1] = 'ad1_3';
	$str_post_name4[2] = 'ae1_3';
	
	for($wcnt1=0;$wcnt1<=2;$wcnt1++) 
	{
		//登録データの初期化
		$bill_recuperation_prepay = array();
		//申請IDを設定
		$bill_recuperation_prepay[0] = $apply_id;
		
		
		$sts_section = ($wcnt1 + 3);
		$sts_kind = ($wcnt1 + 14);
		
		
		//ステータスを設定
		$bill_recuperation_prepay[1] = null;//入力項目が無いので固定でnullを入力する
		$bill_recuperation_prepay[2] = null;//入力項目が無いので固定でnullを入力する
		
		$wrk = $str_post_name4[$wcnt1];
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$bill_recuperation_prepay[3] = null;
		}
		else
		{
			$bill_recuperation_prepay[3] = $$wrk;
		}
		
		//登録実行！！！！！！！！！！
		$obj->update_add_up_apply_bill_recuperation_prepay_report3($bill_recuperation_prepay,$apply_id,$sts_section,$sts_kind);
		
	}
	
	
	//登録データの初期化
	$bill_recuperation_prepay = array();
	
	$bill_recuperation_prepay_cnt = 1;
	
	//申請IDを設定
	$bill_recuperation_prepay[0] = $apply_id;
	
	for($wcnt2=1;$wcnt2<=4;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=3;$wcnt3++) 
		{
			//ループ変数を仕様して入力項目のname属性を取得する
			$wrk = "y".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
			}
			else
			{
				$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
			}
			$bill_recuperation_prepay_cnt++;
		}
	}
	
	for($wcnt2=1;$wcnt2<=2;$wcnt2++) 
	{
		
		for($wcnt3=1;$wcnt3<=4;$wcnt3++) 
		{
			
			//ループ変数を仕様して入力項目のname属性を取得する		
			$wrk = "z".$wcnt2."_".$wcnt3;
			
			if($$wrk =="")
			{
				//integerの属性なので空文字をnullに変換する
				$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
			}
			else
			{
				$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
			}
			$bill_recuperation_prepay_cnt++;
		}
	}
	
	for($wcnt2=1;$wcnt2<=5;$wcnt2++) 
	{
		
		//ループ変数を仕様して入力項目のname属性を取得する		
		$wrk = "aj1_".$wcnt2;
		
		if($$wrk =="")
		{
			//integerの属性なので空文字をnullに変換する
			$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = null;
		}
		else
		{
			$bill_recuperation_prepay[$bill_recuperation_prepay_cnt] = $$wrk;
		}
		$bill_recuperation_prepay_cnt++;
	}
	
	//登録実行！！！！！！！！！！
	$obj->update_add_up_apply_bill_recuperation_prepay_report4($bill_recuperation_prepay,$apply_id);
	
}




//集計処理用データの更新処理を行う↑





// 承認更新
for($i=1; $i<=$approve_num; $i++)
{

	$varname = "regist_emp_id$i";
	$emp_id = $$varname;

	$varname = "apv_order$i";
	$apv_order = $$varname;

	$varname = "apv_sub_order$i";
	$apv_sub_order = $$varname;

	$varname = "st_div$i";
	$st_div = $$varname;

	$varname = "parent_pjt_id$i";
	$parent_pjt_id = ($$varname == "") ? null : $$varname;

	$varname = "child_pjt_id$i";
	$child_pjt_id = ($$varname == "") ? null : $$varname;

	// 所属、役職も登録する
	$infos = get_empmst($con, $emp_id, $fname);
	$emp_class     = $infos[2];
	$emp_attribute = $infos[3];
	$emp_dept      = $infos[4];
	$emp_st        = $infos[6];
	$emp_room      = $infos[33];

	$arr = array(
	              "apply_id" => $apply_id,
	              "apv_order" => $apv_order,
                  "apv_sub_order" => $apv_sub_order,
                  "emp_id" => $emp_id,
                  "st_div" => $st_div,
                  "emp_class" => $emp_class,
                  "emp_attribute" => $emp_attribute,
                  "emp_dept" => $emp_dept,
                  "emp_st" => $emp_st,
                  "emp_room" => $emp_room,
                  "parent_pjt_id" => $parent_pjt_id,
	              "child_pjt_id" => $child_pjt_id
	             );

	$obj->update_applyapv($arr);
}


// 添付ファイル削除・登録
$obj->delete_applyfile($apply_id);
$no = 1;
foreach ($filename as $tmp_filename)
{
	$obj->regist_applyfile($apply_id, $no, $tmp_filename);
	$no++;
}


// 申請結果通知削除登録
$obj->delete_applynotice($apply_id);
if($notice_emp_id != "")
{
	$arr_notice_emp_id = split(",", $notice_emp_id);
    $arr_rslt_ntc_div  = split(",", $rslt_ntc_div);
	for($i=0; $i<count($arr_notice_emp_id); $i++)
	{
		$obj->regist_applynotice($apply_id, $arr_notice_emp_id[$i], $arr_rslt_ntc_div[$i]);
	}
}

// メールを送る設定の場合
if ($wkfw["send_mail_flg"] == "t") {

	// データ更新後の宛先情報を取得
	$sql = "select e.emp_email2 from empmst e";
	$cond = "where exists (select * from jnl_applyapv a where a.emp_id = e.emp_id and a.apply_id = $apply_id";
	if ($wkfw["wkfw_appr"] == "2") {
		$cond .= " and a.apv_order = 1";
	}
	$cond .= ")";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$after_updating_to_addresses = array();
	while ($row = pg_fetch_array($sel)) {
		if ($row["emp_email2"] != "") {
			$after_updating_to_addresses[] = $row["emp_email2"];
		}
	}

	// 増えたメールアドレスを確認
	$to_addresses = array_diff($after_updating_to_addresses, $before_updating_to_addresses);

	// メール送信に必要な情報を取得
	if (count($to_addresses) > 0) {
		$emp_id = get_emp_id($con, $session, $fname);
		$emp_detail = $obj->get_empmst_detail($emp_id);
		$emp_nm = jnl_format_emp_nm($emp_detail[0]);
		$emp_mail = jnl_format_emp_mail($emp_detail[0]);
		$emp_pos = jnl_format_emp_pos($emp_detail[0]);
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// メール送信
if (count($to_addresses) > 0) {
	mb_internal_encoding("EUC-JP");
	mb_language("Japanese");

	$mail_subject = "[CoMedix] {$approve_label}依頼のお知らせ";
	$mail_content = jnl_format_mail_content($con, $wkfw_content_type, $content, $fname);
	$mail_separator = str_repeat("-", 60) . "\n";
	$additional_headers = "From: $emp_mail";
	$additional_parameter = "-f$emp_mail";

	foreach ($to_addresses as $to_address) {
		$mail_body = "以下の{$approve_label}依頼がありました。\n\n";
		$mail_body .= "申請者：{$emp_nm}\n";
		$mail_body .= "所属：{$emp_pos}\n";
		$mail_body .= "表題：{$apply_title}\n";
		if ($mail_content != "") {
			$mail_body .= $mail_separator;
			$mail_body .= "{$mail_content}\n";
		}

		mb_send_mail($to_address, $mail_subject, $mail_body, $additional_headers, $additional_parameter);
	}
}

// データベース接続を切断
pg_close($con);

// 添付ファイルの移動
foreach (glob("jnl_apply/{$apply_id}_*.*") as $tmpfile) {
	unlink($tmpfile);
}
for ($i = 0; $i < count($filename); $i++) {
	$tmp_file_id = $file_id[$i];
	$tmp_filename = $filename[$i];
	$tmp_fileno = $i + 1;
	$ext = strrchr($tmp_filename, ".");
	
	$tmp_filename = "jnl_apply/tmp/{$session}_{$tmp_file_id}{$ext}";
	copy($tmp_filename, "jnl_apply/{$apply_id}_{$tmp_fileno}{$ext}");
}
foreach (glob("jnl_apply/tmp/{$session}_*.*") as $tmpfile) {
	unlink($tmpfile);
}



echo("<script language=\"javascript\">if(window.opener && !window.opener.closed && window.opener.reload_page){window.opener.reload_page();}</script>");
echo("<script language=\"javascript\">window.close();</script>\n");

?>
</body>
<?
function get_wkfwmst($con, $wkfw_id, $fname) {
	$sql = "select * from jnl_wkfwmst";
	$cond="where wkfw_id = '$wkfw_id'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	return pg_fetch_array($sel);
}
?>