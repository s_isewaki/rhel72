<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>文書管理 | 最新文書一覧</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("menu_common.ini");
require_once("library_common.php");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session=="0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 32, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 文書管理管理者権限を取得
$lib_admin_auth = check_authority($session, 63, $fname);

// データベースに接続
$con = connect2db($fname);

// 遷移元の設定
if ($library != "") {
    set_referer($con, $session, "library", $library, $fname);
    $referer = $library;
} else {
    $referer = get_referer($con, $session, "library", $fname);
}

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// ログインユーザの職員情報を取得
$sql = "select * from empmst";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel_emp = select_from_table($con, $sql, $cond, $fname);
if ($sel_emp == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp = pg_fetch_assoc($sel_emp);

// 文書管理オプション設定情報を取得
$sql = "select whatsnew_flg, whatsnew_days from liboption";
$cond = "where emp_id = '{$emp["emp_id"]}'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $lib_whatsnew_flg = pg_fetch_result($sel, 0, "whatsnew_flg");
    $lib_whatsnew_days = pg_fetch_result($sel, 0, "whatsnew_days");
} else {
    $lib_whatsnew_flg = "t";
    $lib_whatsnew_days = "7";
}

// デフォルトの並び順を設定
if ($o == "") {$o = "7";}  // 番号の昇順

// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_list_new.php?session=<? echo($session); ?>"><b><nobr>最新文書一覧</nobr></b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding:0 6px;" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="105" align="center" bgcolor="#bdd1e7"><a href="intra_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">メインメニュー</font></a></td>
<td width="5">&nbsp;</td>
<td width="<? echo(get_tab_width($intra_menu2)); ?>" align="center" bgcolor="#bdd1e7"><a href="intra_info.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($intra_menu2); ?></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<td width="5">&nbsp;</td>
<? if ($newlist_flg == 't') { ?>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><nobr>最新文書一覧</nobr></b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td style="width:auto">&nbsp;</td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_list_new.php?session=<? echo($session); ?>"><b><nobr>最新文書一覧</nobr></b></a></font></td>
<? if ($lib_admin_auth == "1") { ?>
<td align="right" style="padding:0 6px;" nowrap><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_list_all.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_search.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照履歴</font></a></td>
<? if ($newlist_flg == 't') { ?>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_list_new.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><nobr>最新文書一覧</nobr></b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_option.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<br>
<? show_new_document_list($con, $emp["emp_id"], $lib_whatsnew_days, $session, $fname); ?>

</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
<?
function show_new_document_list($con, $emp_id, $whatsnew_days, $session, $fname) {
    // 日付の設定
    $start_date = date("Ymd", strtotime("-" . ($whatsnew_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));
    $lock_show_flg = lib_get_lock_show_flg($con, $fname);
    $lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);
    lib_cre_tmp_concurrent_table($con, $fname, $emp_id);

    // 閲覧可能カテゴリ
    $opt = array("is_admin"=>0, "is_login"=>0, "for_new_list"=>1, "exclude_orderby"=>1, "show_mypage_flg"=>1);
    $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "", "", 0, 0, $opt);
    $sel = select_from_table($con, $sql, "", $fname);
    if (!$sel) libcom_goto_error_page_and_die($sql);
    $lib_cate_names = array();
    while ($row = pg_fetch_assoc($sel)) $lib_cate_names[$row["lib_cate_id"]] = $row["lib_cate_nm"];

    // 閲覧可能フォルダ
    $opt = array("is_admin"=>0, "is_login"=>0, "for_new_list"=>1, "exclude_orderby"=>1, "show_mypage_flg"=>1, "cate_ids"=>array_keys($lib_cate_names));
    $sql1 = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "", 0, 0, $opt);
    $sql = "select folder_id, parent_id, folder_name from (".$sql1.") d";
    $sel = select_from_table($con, $sql, "", $fname);
    if (!$sel) libcom_goto_error_page_and_die($sql);
    $folders = array(0);
    while ($row = pg_fetch_array($sel)) $folders[$row["folder_id"]] = $row;

    // このSQLで、
    // ◆参照可能なカテゴリの
    // ◆参照可能な文書
    // が取得される。
    $opt = array("is_admin"=>0, "is_login"=>0, "show_mypage_flg"=>1, "lib_upd_date"=>$start_date."000000", "include_emp_nm"=>1, "cate_ids"=>array_keys($lib_cate_names));
    $sql = lib_get_libinfo_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "", "", "", 0, $opt);
    $sel = select_from_table($con, $sql, "", $fname);
    if (!$sel) libcom_goto_error_page_and_die($sql);
    $docs = array();
    while ($row = pg_fetch_array($sel)) {
        // フォルダが無ければつまりカテゴリ直下。カテゴリは参照可能確定済につきOK
        // それ以外なら探索
        $fpath = array();
        if ($row["folder_id"]) {
            $_p = $row["folder_id"];
            $ng = 0;
            for (;;) {
                $_row = $folders[$_p]; // 親レコード
                if (!$_row) { $ng= 1; break; }// 親レコード無し。NG確定
                array_unshift($fpath, $_row["folder_name"]);
                $_p = $_row["parent_id"]; // 親の親フォルダID
                if (!$_p) break; // さらに親が無い。トップノード到達。OK確定
            }
            if ($ng) continue;
        }
        $docs[$row["lib_id"]] = array(
            "name" => $row["lib_nm"],
            "type" => $row["lib_type"],
            "modified" => $row["lib_up_date"],
            "aid" => $row["lib_archive"],
            "cid" => $row["lib_cate_id"],
            "fid" => $row["folder_id"],
            "emp_nm" => $row["emp_nm"],
            "fpath" => implode(" &gt; ", $fpath),
            "path" => get_document_path($row["lib_archive"], $row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]),
            "ext" => $row["lib_extension"]
        );
    }
    // ドキュメント件数確認
    if (count($docs) == 0) {
        echo("<p style=\"margin:10px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"$font_class\">表示対象の文書はありません。</font></p>\n");
        return;
    }
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22" bgcolor="#f6f9ff">
    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書名</font></td>
    <td width="10%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日時</font></td>
    <td width="6%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">サイズ</font></td>
    <td width="8%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録者</font></td>
    <td width="35%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
    <td width="6%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">参照</font></td>
</tr>
<?

    $filename_flg = lib_get_filename_flg();
    foreach ($docs as $lib_id => $doc) {
        // タイプ
        switch (lib_get_file_type_name($doc["type"])) {
            case "Word":
                $img = "word.jpg";
                break;
            case "Excel":
                $img = "excel.jpg";
                break;
            case "PowerPoint":
                $img = "powerpoint.jpg";
                break;
            case "PDF":
                $img = "pdf.jpg";
                break;
            case "テキスト":
                $img = "text.jpg";
                break;
            case "JPEG":
                $img = "jpeg.jpg";
                break;
            case "GIF":
                $img = "gif.jpg";
                break;
            default:
                $img = "other.jpg";
                break;
        }
        $icon = sprintf('<img src="img/icon/%s" alt="" width="16" height="16" border="0" style="vertical-align:middle;margin-right:2px;">', $img);

        // 更新日時
        if (strlen($doc["modified"]) == 14) {
            $upd_time_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3 $4:$5:$6", $doc["modified"]);
        } else {
            $upd_time_str = preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $doc["modified"]);
        }

        // ファイルサイズ
        $size = format_size(@filesize($doc["path"]));

        // ドキュメントURL
        $lib_url = urlencode($doc["path"]);

        // 登録者

        // 保存先
        $aname = lib_get_archive_name($doc["aid"]);
        //$cname = lib_get_category_name($con, $doc["cid"], $fname);
        //$fpath = lib_get_folder_path($con, $doc["fid"], $fname, $doc["aid"]);

        // useragentに対応するコードを取得
        if ($filename_flg > 0) {
            $target = '';
        } else {
            $target = 'target="_blank"';
        }
?>
<tr valign="top">
<td><?=$icon;?> <a href="library_refer.php?s=<?=$session;?>&i=<?=$lib_id;?>&u=<?=$lib_url;?>" <?=$target;?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doc["name"];?></font></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><nobr><?=$upd_time_str;?></nobr></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$size;?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$doc["emp_nm"];?></font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$aname;?> &gt; <?=$lib_cate_names[$doc["cid"]]?><?=($doc["fpath"] ? " &gt; ".$doc["fpath"] : "")?></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('library_detail_from_top.php?session=<?=$session;?>&lib_id=<?=$lib_id;?>', 'newwin', 'width=640,height=480,scrollbars=yes');">参照</a></font></td>
</tr>
<?
        ob_flush();
    }
    echo("</table>");
}