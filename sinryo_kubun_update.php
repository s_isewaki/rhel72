<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<? require("about_authority.php"); ?>
<? require("about_session.php"); ?>
<? require("show_sinryo_top_header.ini"); ?>
<? require("summary_common.ini"); ?>
<?
require("label_by_profile_type.ini");

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療区分登録権限チェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);

// 診療区分情報を取得
$sel = select_from_table($con, "select * from divmst where div_id = '$div_id'", "", $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$div_name = pg_fetch_result($sel, 0, "diag_div");
$div_display_auth_flg = (int)pg_fetch_result($sel, 0, "div_display_auth_flg");
$div_modify_auth_flg = (int)pg_fetch_result($sel, 0, "div_modify_auth_flg");
$div_display_auth_stid_list = explode(",", pg_fetch_result($sel, 0, "div_display_auth_stid_list"));
$div_modify_auth_stid_list = explode(",", pg_fetch_result($sel, 0, "div_modify_auth_stid_list"));
$div_show_link_flowsheet_button = (int)pg_fetch_result($sel, 0, "div_show_link_flowsheet_button");
$div_show_link_ondoban_button = (int)pg_fetch_result($sel, 0, "div_show_link_ondoban_button");
$div_show_icon = (int)pg_fetch_result($sel, 0, "div_show_icon");

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo($med_report_title); ?>管理 | <?=$summary_kubun_title?>更新</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<style type="text/css">
.list {border-collapse:collapse;}
.list th { border:#5279a5 solid 1px; white-space:nowrap; font-weight:normal; }
.list td { border:#5279a5 solid 1px; }
.noborder tr { border:0; }
.noborder td { border:0; }
</style>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<?
show_sinryo_top_header($session,$fname,"KANRI_KUBUN");

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="110" align="center" bgcolor="#bdd1e7"><a href="./summary_kanri_kubun.php?session=<? echo($session); ?>"><?=$font?><? echo ($summary_kubun_title); ?>一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#5279a5"><a href="./sinryo_kubun_update.php?session=<? echo($session); ?>&div_id=<? echo($div_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo ($summary_kubun_title); ?>更新</b></font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>


<?
	//======================================
	// 職種マスタ情報取得(リスト項目の値取得)
	//======================================
	$options_right1 = array();
	$options_left1 = array();
	$options_right2 = array();
	$options_left2 = array();
	$options_js_sort = array();
	$sel = select_from_table($con,"select job_id,job_nm from jobmst where job_del_flg = 'f' order by order_no","",$fname);
	while ($row = pg_fetch_array($sel)) {
		$options_js_sort[] = $row["job_id"];
		if (in_array($row["job_id"], $div_display_auth_stid_list)){
			$options_left1[] = '<option value="'.$row["job_id"].'">'.$row["job_nm"].'</option>';
		} else {
			$options_right1[] = '<option value="'.$row["job_id"].'">'.$row["job_nm"].'</option>';
		}
		if (in_array($row["job_id"], $div_modify_auth_stid_list)){
			$options_left2[] = '<option value="'.$row["job_id"].'">'.$row["job_nm"].'</option>';
		} else {
			$options_right2[] = '<option value="'.$row["job_id"].'">'.$row["job_nm"].'</option>';
		}
	}
?>



<form action="sinryo_kubun_update_exe.php" method="post" name="frm" enctype="multipart/form-data">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="div_display_auth_stid_list" id="div_display_auth_stid_list" />
<input type="hidden" name="div_modify_auth_stid_list" id="div_modify_auth_stid_list" />
<input type="hidden" name="div_id" value="<? echo($div_id); ?>">
<table border="0" cellspacing="0" cellpadding="4" style="margin-top:2px; width:600px" class="list">
	<tr>
		<td style="text-align:right"><?=$font?><div style="padding:3px"><? echo ($summary_kubun_title); ?>名</div></font></td>
		<td><input type="text" name="div_name" value="<? echo($div_name); ?>" size="25" maxlength="30" style="ime-mode: active;"></td>
	</tr>

	<tr>
		<td style="text-align:right"><?=$font?><div style="padding:3px">リンク表示</font></td>
		<td><?=$font?>
			<label style="cursor:pointer"><input type="checkbox" value="1" name="div_show_link_flowsheet_button" <?=$div_show_link_flowsheet_button?"checked":""?> >フローシート</label>
			<label style="cursor:pointer"><input type="checkbox" value="1" name="div_show_link_ondoban_button" <?=$div_show_link_ondoban_button?"checked":""?> >温度板</label>
		</font></td>
	</tr>
	<tr>
		<td style="text-align:right"><?=$font?><div style="padding:3px">アイコン表示</font></td>
		<td><?=$font?>
			<div style="float:left">
			<label style="cursor:pointer"><input type="radio" name="div_show_icon" value="0"<?=!$div_show_icon?"checked":""?>>しない</label>
			<label style="cursor:pointer"><input type="radio" name="div_show_icon" value="1"<?=$div_show_icon?"checked":""?>>する</label><br/>
			<input type="file" name="div_icon_file" size="60">
			</div>
			<div style="float:right; padding:4px">
				<? if (is_file("summary/divicon/".$div_id.".gif")){ ?>
				<img src="summary/divicon/<?=$div_id?>.gif?t=<?=time()?>"/>
				<? } else if (is_file("summary/divicon/".$div_id.".jpg")){ ?>
				<img src="summary/divicon/<?=$div_id?>.jpg?t=<?=time()?>"/>
				<? } else if (is_file("summary/divicon/".$div_id.".png")){ ?>
				<img src="summary/divicon/<?=$div_id?>.png?t=<?=time()?>"/>
				<? } ?>
			</div>
			<div style="clear:both"></div>
			<div style="color:#888">※登録可能な拡張子：&nbsp;.gif&nbsp;,&nbsp;.jpg&nbsp;,&nbsp;.png</div>
			<div style="color:#888">※推奨サイズ：&nbsp;36ピクセル&nbsp;x&nbsp;36ピクセル</div>
		</font></td>
	</tr>
	<tr>
	<td style="text-align:right; vertical-align:top"><?=$font?><div style="padding:3px">参照権限の設定</div></font></td>
	<td style="vertical-align:top"><?=$font?>
		<div>
			<label><input type="radio" value="0" name="div_display_auth_flg" <?=$div_display_auth_flg?"":"checked"?> />なし</label>
			<label><input type="radio" value="1" name="div_display_auth_flg" <?=$div_display_auth_flg?"checked":""?> />あり</label>
		</div>
		<div>参照可能職種</div>
		<table border="0" cellspacing="0" cellpadding="0" class="noborder"><tr>
			<td><select size="10" style="width:150px; height:150px" id="cmb_1_left"><?=implode("",$options_left1)?></select></td>
			<td style="padding:4px"><input type="button" value="≪追加" onclick="add('1')"><br/><br/><input type="button" value="削除≫" onclick="del('1')"></td>
			<td><select size="10" style="width:150px; height:150px" id="cmb_1_right"><?=implode("",$options_right1)?></select></td>
		</tr></table>
	</font></td>
	</tr>
	<tr>
	<td style="text-align:right; vertical-align:top; padding-top:2px"><?=$font?><div style="padding:3px">更新権限の設定</div></font></td>
	<td><?=$font?>
		<div>
			<label><input type="radio" value="0" name="div_modify_auth_flg" <?=$div_modify_auth_flg?"":"checked"?> />なし</label>
			<label><input type="radio" value="1" name="div_modify_auth_flg" <?=$div_modify_auth_flg?"checked":""?> />あり</label>
		</div>
		<div>更新可能職種</div>
		<table border="0" cellspacing="0" cellpadding="0" class="noborder"><tr>
			<td><select size="10" style="width:150px; height:150px" id="cmb_2_left"><?=implode("",$options_left2)?></select></td>
			<td style="padding:4px"><input type="button" value="≪追加" onclick="add('2')"><br/><br/><input type="button" value="削除≫" onclick="del('2')"></td>
			<td><select size="10" style="width:150px; height:150px" id="cmb_2_right"><?=implode("",$options_right2)?></select></td>
		</tr></table>
	</font></td>
	</tr>
</table>
<div style="width:600px; text-align:right"><input type="button" value="登録" onclick="startSubmit()">
</div>


<select id="combo_tmp" style="display:none"></select>
<script type="text/javascript">
	var cmb_sorted_items = ["<?=implode("\",\"",$options_js_sort)?>"];
	var combo = {};
	combo["to1"] = document.getElementById("cmb_1_left");
	combo["to2"] = document.getElementById("cmb_2_left");
	combo["right1"] = document.getElementById("cmb_1_right");
	combo["right2"] = document.getElementById("cmb_2_right");
	var combo_tmp = document.getElementById("combo_tmp");
	function del(mode){ comboMoveItem(combo["to"+mode], combo["right"+mode]); }
	function add(mode){ comboMoveItem(combo["right"+mode], combo["to"+mode]);}

	<?// コンボのアイテムの移動。style.displayの制御方法では、IE6には通用しない。?>
	function comboMoveItem(cmbSrc, cmbDest){
		// 移動元の選択が無効なら何もせず終了
		var idx = cmbSrc.selectedIndex;
		if (idx < 0) return;

		// 移動元の選択位置を、１つ後方、または１つ前方に移動
		if (cmbSrc.options.length > idx+1)  cmbSrc.selectedIndex = idx + 1;
		else if (cmbSrc.options.length > 1) cmbSrc.selectedIndex = idx - 1;

		// 選択アイテムの本来のソート順を取得
		var sort_idx = -1;
		for(var i = 0; i < cmb_sorted_items.length; i++){
			if (cmb_sorted_items[i] == cmbSrc.options[idx].value) sort_idx = i;
		}

		// 選択アイテムより順序が後方のアイテムのIDリストを作成
		var after_list = [];
		var tmp = "";
		for (var i=sort_idx; i<cmb_sorted_items.length; i++){
			after_list[after_list.length] = cmb_sorted_items[i];
			tmp += cmb_sorted_items[i] + ",";
		}
		// 移動先に、選択アイテムより順序が後方のアイテムがあれば、いったんコンボから引きはがす。
		for (var i=cmbDest.options.length-1; i>=0; i--){
			var exist_idx = -1;
			for(var j = 0; j < after_list.length; j++){
				if (after_list[j] == cmbDest.options[i].value) exist_idx = j;
			}
			if (exist_idx < 0) break;
			combo_tmp.appendChild(cmbDest.options[i]);
		}

		// 選択アイテムを移動先に移動。そのあと順序が後方のアイテムを再追加。
		// 移動先の選択位置を、移動したアイテムにセット
		cmbDest.appendChild(cmbSrc.options[idx]);
		idx = cmbDest.options.length-1;
		for (var i=combo_tmp.options.length-1; i>=0; i--){
			cmbDest.appendChild(combo_tmp.options[i]);
		}
		cmbDest.selectedIndex = idx;
	}
	function startSubmit(){
		var out = ",";
		for (var i = 0; i < combo["to1"].options.length; i++){
			if (combo["to1"].options[i].style.display=="none") continue;
			out += combo["to1"].options[i].value + ",";
		}
		document.getElementById("div_display_auth_stid_list").value = out;
		var out = ",";
		for (var i = 0; i < combo["to2"].options.length; i++){
			if (combo["to2"].options[i].style.display=="none") continue;
			out += combo["to2"].options[i].value + ",";
		}
		document.getElementById("div_modify_auth_stid_list").value = out;
		document.frm.submit();
	}
</script>



</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
