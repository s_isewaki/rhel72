<?
chdir(dirname(dirname(__FILE__)));
require_once("about_session.php");
require_once("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, 38, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

define("FILE_PATH", "/var/www/html/comedix-update.zip");
if (is_file(FILE_PATH) && !is_writable(FILE_PATH)) {
	echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。\\n\\nサーバ上で下記のコマンドを実行後、再度お試しください。\\n\\nchmod 666 /var/www/html/comedix-update.zip');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// ファイルアップロードチェック
define(UPLOAD_ERR_OK, 0);
define(UPLOAD_ERR_INI_SIZE, 1);
define(UPLOAD_ERR_FORM_SIZE, 2);
define(UPLOAD_ERR_PARTIAL, 3);
define(UPLOAD_ERR_NO_FILE, 4);

switch ($_FILES["updatefile"]["error"]) {
case UPLOAD_ERR_OK:
	break;
case UPLOAD_ERR_INI_SIZE:
case UPLOAD_ERR_FORM_SIZE:
	echo("<script type=\"text/javascript\">alert('ファイルサイズの制限により、アップロードに失敗しました。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
case UPLOAD_ERR_PARTIAL:
case UPLOAD_ERR_NO_FILE:
	echo("<script type=\"text/javascript\">alert('アップロードに失敗しました。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($_FILES["updatefile"]["name"] != "comedix-update.zip") {
	echo("<script type=\"text/javascript\">alert('ファイル名が違います。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if ($_FILES["updatefile"]["size"] == 0) {
	echo("<script type=\"text/javascript\">alert('ファイルサイズが不正です。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// ファイルを所定の場所にコピー
copy($_FILES["updatefile"]["tmp_name"], FILE_PATH);

// 画面を再表示
echo("<script type=\"text/javascript\">alert('アップロードが完了しました。');</script>");
echo("<script type=\"text/javascript\">location.href = '../license_versionup.php?session=$session';</script>");
