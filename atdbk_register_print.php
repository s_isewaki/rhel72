<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 出勤表 | 出勤予定表（印刷）</title>
<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("atdbk_close_class.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
switch ($wherefrom) {
case "1":  // タイムカード入力画面より
	$auth_id = 5;
	break;
case "2":  // タイムカード修正画面より
	$auth_id = ($mmode != "usr") ? 42 : 5;
	break;
default:
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
	break;
}
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
// system_config
$conf = new Cmx_SystemConfig();
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {
	$duty_or_oncall_flg = "1";
}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";

// 職員ID未指定の場合はログインユーザの職員IDを取得
if ($emp_id == "") {
	$sql = "select emp_id from session";
	$cond = "where session_id = '$session'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$emp_id = pg_fetch_result($sel, 0, "emp_id");
}

// 職員氏名を取得
$sql = "select emp_lt_nm, emp_ft_nm, emp_personal_id from empmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$emp_name = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
$emp_personal_id = pg_fetch_result($sel, 0, "emp_personal_id");

// ************ oose add start *****************
// 勤務条件テーブルより勤務形態（常勤、非常勤）を取得
$sql = "select duty_form from empcond";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$duty_form = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "duty_form") : "";
// ************ oose add end *****************

$atdbk_close_class = new atdbk_close_class($con, $fname);
$closing = $atdbk_close_class->get_empcond_closing($emp_id);
$closing_month_flg = $atdbk_close_class->closing_month_flg;

// 締め日が登録済みの場合
if ($closing != "") {

	if ($yyyymm != "") {
		$year = substr($yyyymm, 0, 4);
		$month = intval(substr($yyyymm, 4, 2));
		$month_set_flg = true; //月が指定された場合
	} else {
		$year = date("Y");
		$month = date("n");
		$yyyymm = $year . date("m");
		$month_set_flg = false; //月が未指定の場合
	}

	// 開始日・締め日を算出
	$calced = false;
	switch ($closing) {
	case "1":  // 1日
		$closing_day = 1;
		break;
	case "2":  // 5日
		$closing_day = 5;
		break;
	case "3":  // 10日
		$closing_day = 10;
		break;
	case "4":  // 15日
		$closing_day = 15;
		break;
	case "5":  // 20日
		$closing_day = 20;
		break;
	case "6":  // 末日
		$start_year = $year;
		$start_month = $month;
		$start_day = 1;
		$end_year = $start_year;
		$end_month = $start_month;
		$end_day = days_in_month($end_year, $end_month);
		$calced = true;
		break;
	}
	if (!$calced) {
		$day = date("j");
		$start_day = $closing_day + 1;
		$end_day = $closing_day;

		//月が指定された場合
		if ($month_set_flg) {
			//当月〜翌月
			if ($closing_month_flg != "2") {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
			//前月〜当月
			else {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			}
			
		} else {
			if ($day <= $closing_day) {
				$start_year = $year;
				$start_month = $month - 1;
				if ($start_month == 0) {
					$start_year--;
					$start_month = 12;
				}
				
				$end_year = $year;
				$end_month = $month;
			} else {
				$start_year = $year;
				$start_month = $month;
				
				$end_year = $year;
				$end_month = $month + 1;
				if ($end_month == 13) {
					$end_year++;
					$end_month = 1;
				}
			}
		}
	}
	$start_date = sprintf("%04d%02d%02d", $start_year, $start_month, $start_day);
	$end_date = sprintf("%04d%02d%02d", $end_year, $end_month, $end_day);
}
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr<? /* bgcolor="#5279a5" */ ?>>
<td width="520" height="22" class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#000000"><b>出勤予定表</b></font></td>
<td width="80" align="center"><? /* <a href="javascript:window.close()"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff">■閉じる</font></a> */ ?></td>
</tr>
</table>
<table width="760" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="260"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">対象期間&nbsp;&nbsp;<? echo("{$year}年{$month}月度"); ?></font></td>
<td width="340"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日&nbsp;&nbsp;<? echo("{$start_month}月{$start_day}日"); ?>&nbsp;&nbsp;〜&nbsp;&nbsp;締め日&nbsp;&nbsp;<? echo("{$end_month}月{$end_day}日"); ?></font></td>
</tr>
</table>
<table width="760" border="0" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員ID&nbsp;<? echo($emp_personal_id); ?>&nbsp;&nbsp;職員氏名&nbsp;<? echo($emp_name);
// 勤務条件表示
$str_empcond = get_employee_empcond2($con, $fname, $emp_id);

$str_shiftname = get_shiftname($con, $fname, $atdbk_closed, $emp_id, $start_date, $end_date);
//所属表示
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$str_orgname = get_orgname($con, $fname, $emp_id, $timecard_bean);
echo("　<b>（".$str_empcond."　".$str_shiftname."）$str_orgname</b>");

?></font></td>
<td></td>
</tr>
</table>
<table width="760" border="1" cellspacing="0" cellpadding="1">
<tr>
<td height="22" width="61" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
<td width="37" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">曜日</font></td>
<td width="83" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">種別</font></td>
<td width="82" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">グループ</font></td>
<td width="82" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">出勤予定</font></td>
<td width="82" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">事由</font></td>
<td width="57" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($duty_str); ?></font></td>
<td width="" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">手当</font></td>
<td width="99" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務開始予定</font></td>
<td width="99" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務終了予定</font></td>
</tr>
<? show_attendance_book($con, $emp_id, $start_date, $end_date, $fname); ?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 出勤予定を出力
function show_attendance_book($con, $emp_id, $start_date, $end_date, $fname) {

	//出勤表関連共通クラス
	require_once("atdbk_common_class.php");
	$atdbk_common_class = new atdbk_common_class($con, $fname);
/*
	// 勤務日・休日の名称を取得
	$sql = "select day2, day3, day4, day5 from calendarname";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type='text/javascript' src='js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$day2 = pg_fetch_result($sel, 0, "day2");
		$day3 = pg_fetch_result($sel, 0, "day3");
		$day4 = pg_fetch_result($sel, 0, "day4");
		$day5 = pg_fetch_result($sel, 0, "day5");
	}
	if ($day2 == "") {
		$day2 = "法定休日";
	}
	if ($day3 == "") {
		$day3 = "所定休日";
	}
	if ($day4 == "") {
		$day4 = "勤務日1";
	}
	if ($day5 == "") {
		$day5 = "勤務日2";
	}
*/
	// 手当情報取得
	$arr_allowance = get_timecard_allowance($con, $fname);

	$tmp_date = $start_date;
	while ($tmp_date <= $end_date) {
		$tmp_year = intval(substr($tmp_date, 0, 4));
		$tmp_month = intval(substr($tmp_date, 4, 2));
		$tmp_day = intval(substr($tmp_date, 6, 2));

		// 処理日付の勤務日種別を取得
		$sql = "select type from calendar";
		$cond = "where date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		$type = (pg_num_rows($sel) > 0) ? pg_fetch_result($sel, 0, "type") : "";
		// カレンダー名を取得
		$type_name = $atdbk_common_class->get_calendarname($type, $tmp_date);
		// 処理日付の出勤予定を取得
		$sql = "select tmcd_group_id, pattern, reason, night_duty, prov_start_time, prov_end_time, allow_id from atdbk";
		$cond = "where emp_id = '$emp_id' and date = '$tmp_date'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}
		if (pg_num_rows($sel) > 0) {
			$tmcd_group_id = pg_fetch_result($sel, 0, "tmcd_group_id");
			$pattern = pg_fetch_result($sel, 0, "pattern");
			$reason = pg_fetch_result($sel, 0, "reason");
			$night_duty = pg_fetch_result($sel, 0, "night_duty");
			$prov_start_time = pg_fetch_result($sel, 0, "prov_start_time");
			$prov_end_time = pg_fetch_result($sel, 0, "prov_end_time");
			$allow_id = pg_fetch_result($sel, 0, "allow_id");
		} else {
			$tmcd_group_id = "";
			$pattern = "";
			$reason = "";
			$night_duty = "";
			$prov_start_time = "";
			$prov_end_time = "";
			$allow_id = "";
		}

		//背景色設定。法定、祝祭日を赤、所定を青にする
		$bgcolor = get_timecard_bgcolor($type);
		echo("<tr height=\"22\" bgcolor=\"$bgcolor\">\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_month}月{$tmp_day}日</font><input type=\"hidden\" name=\"date[]\" value=\"$tmp_date\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . get_weekday(to_timestamp($tmp_date)) . "</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$type_name</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($tmcd_group_id != "") {
			echo($atdbk_common_class->get_group_name($tmcd_group_id));
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
		if ($tmcd_group_id != "" && $pattern != "" && $pattern != "--") {
			echo($atdbk_common_class->get_pattern_name($tmcd_group_id, $pattern));
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		echo($atdbk_common_class->get_reason_name($reason)."<br>");
		echo("</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		switch ($night_duty) {
		case "1":
			echo("有り");
			break;
		case "2":
			echo("無し");
			break;
		default:
			echo("&nbsp;");
			break;
		}
		echo("</font></td>\n");

		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");

		if ($allow_id != "")
		{
			foreach($arr_allowance as $allowance)
			{
				if($allow_id == $allowance["allow_id"])
				{
					echo($allowance["allow_contents"]);
					break;
				}
			}
		}
		else
		{
			echo("&nbsp;");
		}

		echo("</font></td>\n");

		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($prov_start_time != "") {
			$prov_start_time = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $prov_start_time);
			$prov_start_time = preg_replace("/^0(.*)/", "$1", $prov_start_time);
			echo($prov_start_time);
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");

		echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
		if ($prov_end_time != "") {
			$prov_end_time = preg_replace("/^(\d{2})(\d{2})$/", "$1:$2", $prov_end_time);
			$prov_end_time = preg_replace("/^0(.*)/", "$1", $prov_end_time);
			echo($prov_end_time);
		} else {
			echo("&nbsp;");
		}
		echo("</font></td>\n");

		echo("</tr>\n");

		$tmp_date = next_date($tmp_date);
	}

}

/*
// 指定年月の日数を求める
function days_in_month($year, $month) {
	for ($day = 31; $day >= 28; $day--) {
		if (checkdate($month, $day, $year)) {
			return $day;
		}
	}
	return false;
}

// 翌日日付をyyyymmdd形式で取得
function next_date($yyyymmdd) {
	return date("Ymd", strtotime("+1 day", to_timestamp($yyyymmdd)));
}

// 日付をタイムスタンプに変換
function to_timestamp($yyyymmdd) {
	$y = substr($yyyymmdd, 0, 4);
	$m = substr($yyyymmdd, 4, 2);
	$d = substr($yyyymmdd, 6, 2);
	return mktime(0, 0, 0, $m, $d, $y);
}

// タイムスタンプから曜日を返す
function get_weekday($date) {
	$wd = date("w", $date);
	switch ($wd) {
	case "0":
		return "日";
		break;
	case "1":
		return "月";
		break;
	case "2":
		return "火";
		break;
	case "3":
		return "水";
		break;
	case "4":
		return "木";
		break;
	case "5":
		return "金";
		break;
	case "6":
		return "土";
		break;
	}
}
*/

?>
