<?
require("about_session.php");
require("about_authority.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$check_auth = check_authority($session, 12, $fname);
if ($check_auth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if (!is_array($trashbox)) {
	echo("<script type=\"text/javascript\">alert('削除対象を選択してください。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// チェックされた行事を削除
$sql = "delete from timegd";
$cond = "where event_id in (" . join(',', $trashbox) . ")";
$del = delete_from_table($con, $sql, $cond, $fname);
if ($del == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// データベース接続を閉じる
pg_close($con);

// 行事一覧画面に遷移
echo("<script type=\"text/javascript\">location.href = 'time_guide_list.php?session=$session&fcl_id=$fcl_id&date=$date&mode=display';</script>");
?>
