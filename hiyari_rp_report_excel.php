<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("hiyari_mail.ini");
require_once("hiyari_common.ini");



//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
//====================================
//ファイル名
//====================================
$filename = 'report.xls';

if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}

$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());
// HTML形式でレポートの情報を取得（Excelモード）
$download_data = get_report_html($con, $fname, $report_id, '', '', $session, '', 'excel');

$download_data = str_replace("style=\"border:#35B341", "style=\"border:#c8c8c8", $download_data);
pg_close($con);

//====================================
//統計分析データEXCEL出力
//====================================
ob_clean();
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding(nl2br($download_data), 'sjis', mb_internal_encoding());
ob_end_flush();
?>