<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 患者管理｜基本</title>
<? require("./about_session.php"); ?>
<? require("./about_authority.php"); ?>
<? require("./about_postgres.php"); ?>
<? require("./show_select_values.ini"); ?>
<? require("./get_values.ini"); ?>
<? require("./conf/sql.inf"); ?>
<? require("label_by_profile_type.ini"); ?>
<?
require("show_select_years_wareki.ini");

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 患者管理各権限の取得
$ptreg = check_authority($session, 22, $fname);
$ptif = check_authority($session, 15, $fname);
$outreg = check_authority($session, 35, $fname);
$inout = check_authority($session, 33, $fname);
$disreg = check_authority($session, 36, $fname);
$dishist = check_authority($session, 34, $fname);

// 患者参照権限のチェック
if ($ptif == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 診療科登録権限を取得
$section_admin_auth = check_authority($session, 28, $fname);

// 患者管理管理者権限を取得
$patient_admin_auth = check_authority($session, 77, $fname);

// DBに接続
$con = connect2db($fname);

$profile_type = get_profile_type($con, $fname);

// 初期表示時は患者情報を取得
if ($back != "t") {
	$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f' order by ptif_id";
	$sel = select_from_table($con,$SQL85,$cond,$fname);
	if($sel == 0){
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$lt_nm = pg_result($sel,0,"ptif_lt_kaj_nm");
	$ft_nm = pg_result($sel,0,"ptif_ft_kaj_nm");
	$lt_kana_nm = pg_result($sel,0,"ptif_lt_kana_nm");
	$ft_kana_nm = pg_result($sel,0,"ptif_ft_kana_nm");
	$birth = pg_result($sel,0,"ptif_birth");
	$birth_yr = substr($birth,0,4);
	$birth_mon = substr($birth,4,2);
	$birth_day = substr($birth,6,2);
	$sex = pg_result($sel,0,"ptif_sex");
	$job = pg_result($sel,0,"ptif_buis");

	$new_pt_id = $pt_id;
}

// 初期表示時は和暦
if ($wareki_flg == "") {
	$wareki_flg = "t";
}

// 検索フォームからの引継ぎ値をurlencode
$url_key1 = urlencode($key1);
$url_key2 = urlencode($key2);

// 初期患者IDを取得
$sql = "select ptif_org_ptif_id from ptifmst";
$cond = "where ptif_id = '$pt_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$org_ptif_id = pg_fetch_result($sel, 0, "ptif_org_ptif_id");

// 患者ID変更履歴を取得
$id_history = array();
if ($org_ptif_id != "") {
	$sql = "select old_ptif_id, update_at from ptifid";
	$cond = "where org_ptif_id = '$org_ptif_id' order by update_at desc";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	while ($row = pg_fetch_array($sel)) {
		$id_history[] = array(
			"ptif_id" => $row["old_ptif_id"],
			"update_at" => $row["update_at"]
		);
	}
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function wareki_flg_onchange() {
	if (document.patient.wareki_flg.checked == true) {
		birth_yr_onchange();
		document.getElementById('year_div').style.display = "none";
		document.getElementById('year_wareki_div').style.display = "";
	} else {
<?
// エラー時以外
//if ($back != "t") {
//echo("		birth_yr2_onchange();");
//}
?>
		document.getElementById('year_div').style.display = "";
		document.getElementById('year_wareki_div').style.display = "none";
	}
}

<?
// 改元データを取得
$era_data = get_era_data();
$era_ymd_str = "";
$gengo_str = "";
for ($i = 1; $i < count($era_data); $i++) {
	if ($i > 1) {
		$era_ymd_str .= ",";
		$gengo_str .= ",";
	}
	$split_data = split(",", $era_data[$i]);
	$era_ymd_str .= "'".$split_data[0]."'";
	$gengo_str .= "'".$split_data[1]."'";
}
?>
var ymd_arr = new Array(<?=$era_ymd_str?>);
var gengo_arr = new Array(<?=$gengo_str?>);

function birth_yr_onchange() {
	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		if (year == ymd_arr[i].substring(0,4)) {
			find_idx = i;
			break;
		}
	}

	document.patient.birth_yr2.value = year;
	if (find_idx > -1 && ymd < ymd_arr[find_idx]) {
		document.patient.birth_yr2.selectedIndex += 1;
	}
}

function birth_yr2_onchange() {
	document.patient.birth_yr.value = document.patient.birth_yr2.value;
}

function check_wareki() {

	if (document.patient.wareki_flg.checked == false) {
		return true;
	}

	var year = document.patient.birth_yr.value;
	var mon = document.patient.birth_mon.value;
	var day = document.patient.birth_day.value;
	var ymd = year+mon+day;

	find_idx = -1;
	for (var i=0; i<ymd_arr.length; i++) {
		ymp_tmp = ymd_arr[i].substring(0,4);
		if (year == ymp_tmp) {
			find_idx = i;
			break;
		}
	}

	if (find_idx == -1) {
		return true;
	}

	if (ymd < ymd_arr[find_idx]) {
		gengo = gengo_arr[find_idx + 1];
	} else {
		gengo = gengo_arr[find_idx];
	}

	obj = document.patient.birth_yr2;
	var wareki_tmp = obj.options[obj.selectedIndex].text;
	select_gengo = wareki_tmp.substring(0,2);
	if (select_gengo != gengo) {
		alert('年月日を正しく指定してください');
		return false;
	}
	return true;
}

function change_age() {
	var birth_year = document.patient.birth_yr.value;
	var birth_month = document.patient.birth_mon.value;
	var birth_day = document.patient.birth_day.value;

	var today = new Date();
	var this_year = today.getFullYear();
	var this_month = today.getMonth() + 1;
	var this_day = today.getDate();

	var age = this_year - birth_year;
	if (this_month < birth_month || (this_month == birth_month && this_day < birth_day)) {
		age--;
	}

	document.getElementById('age_label').innerHTML = age + '歳';
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="wareki_flg_onchange();change_age();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="patient_info_menu.php?session=<? echo($session); ?>"><img src="img/icon/b16.gif" width="32" height="32" border="0" alt="<? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?>"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="patient_info_menu.php?session=<? echo($session); ?>"><b><? echo($_label_by_profile["PATIENT_MANAGE"][$profile_type]); ?></b></a></font></td>
<? if ($section_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="entity_menu.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } else if ($patient_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="bed_check_setting.php?session=<? echo($session); ?>&entity=2"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_info_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#5279a5"><a href="patient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&page=<? echo($page); ?>&key1=<? echo($key1); ?>&key2=<? echo($ekey2); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>基本</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="85" align="center" bgcolor="#bdd1e7"><a href="patient_insurance.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">健康保険</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_genogram.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">家族構成図</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="patient_sub_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">基礎データ</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="patient_contact_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">連絡先</font></a></td>
<td width="5">&nbsp;</td>
<td width="70" align="center" bgcolor="#bdd1e7"><a href="patient_claim_setting.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">請求</font></a></td>
<? } ?>
<? if ($inout == 1) { ?>
<td width="5">&nbsp;</td>
<td width="110" align="center" bgcolor="#bdd1e7"><a href="inoutpatient_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["IN_OUT"][$profile_type]); ?></font></a></td>
<? } ?>
<? if ($dishist == 1) { ?>
<td width="5">&nbsp;</td>
<td width="125" align="center" bgcolor="#bdd1e7"><a href="disease_detail.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレムリスト</font></a></td>
<? } ?>
<? if ($ptif == 1) { ?>
<td width="5">&nbsp;</td>
<td width="135" align="center" bgcolor="#bdd1e7"><a href="patient_karte.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&key1=<? echo($url_key1); ?>&key2=<? echo($url_key2); ?>&page=<? echo($page); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["KARTE_NO"][$profile_type]); ?></font></a></td>
<? } ?>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form action="patient_update_confirm.php" method="post" name="patient" onsubmit="return check_wareki();">
<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($_label_by_profile["PATIENT_ID"][$profile_type]); ?></font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input name="new_pt_id" type="text" value="<? echo $new_pt_id; ?>" maxlength="20" style="ime-mode:inactive;"></font></td>
</tr>
<tr height="22">
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（漢字）</font></td>
<td width="42%"><input name="lt_nm" type="text" value="<? echo $lt_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td width="16%" bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（漢字）</font></td>
<td width="26%"><input name="ft_nm" type="text" value="<? echo $ft_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">姓（かな）</font></td>
<td><input name="lt_kana_nm" type="text" value="<? echo $lt_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">名（かな）</font></td>
<td><input name="ft_kana_nm" type="text" value="<? echo $ft_kana_nm; ?>" maxlength="10" style="ime-mode:active;"></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">生年月日</font></td>
<td>
<span id="year_div">
<select name="birth_yr" onchange="birth_yr_onchange();change_age();">
<? show_select_years(120, $birth_yr);?>
</select></span><span id="year_wareki_div">
<select name="birth_yr2" onchange="birth_yr2_onchange();change_age();">
<? show_select_years_jp(120, $birth_yr2, "", ""); ?>
</select></span><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="birth_mon" onchange="change_age();"><? show_select_months($birth_mon); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">/</font><select name="birth_day" onchange="change_age();"><? show_select_days($birth_day); ?></select>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" name="wareki_flg" value="t" <? if ($wareki_flg == "t") echo(" checked"); ?> onclick="wareki_flg_onchange();">和暦</font>
</td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">年齢</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><span id="age_label"></span></font></td>
</tr>
<tr height="22">
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">性別</font></td>
<td><select name="sex">
<option value="0"<? if ($sex == "0") {echo " selected";} ?>>不明
<option value="1"<? if ($sex == "1") {echo " selected ";} ?>>男性
<option value="2"<? if ($sex == "2") {echo " selected ";} ?>>女性
</select></td>
<td bgcolor="#f6f9ff" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職業</font></td>
<td><input type="text" name="job" value="<? echo $job; ?>" style="ime-mode:active;"></td>
</tr>
</table>
<? if ($ptreg == 1) { ?>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="submit" value="更新"></td>
</tr>
</table>
<? } ?>
<img src="img/spacer.gif" width="1" height="4" alt=""><br>
<? if (count($id_history) > 0) { ?>
<table width="450" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td bgcolor="#f6f9ff" align="center" width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">更新日付</font></td>
<td bgcolor="#f6f9ff" align="center" width="50%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">旧患者ID</font></td>
</tr>
<?
	foreach ($id_history as $tmp_history) {
		$tmp_update_at = $tmp_history["update_at"];
		$tmp_update_year = intval(substr($tmp_update_at, 0, 4));
		$tmp_update_month = intval(substr($tmp_update_at, 4, 2));
		$tmp_update_day = intval(substr($tmp_update_at, 6, 2));
?>
<tr height="22">
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_update_year); ?>年<? echo($tmp_update_month); ?>月<? echo($tmp_update_day); ?>日</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($tmp_history["ptif_id"]); ?></font></td>
</tr>
<?
	}
?>
</table>
<? } ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="key1" value="<? echo($key1); ?>">
<input type="hidden" name="key2" value="<? echo($key2); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
