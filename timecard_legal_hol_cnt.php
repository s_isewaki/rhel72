<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 公休数</title>
<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("show_timecard_common.ini");
require_once("timecard_bean.php");
require_once("atdbk_menu_common.ini");
require_once("timecard_hol_menu_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);
$arr_kinzoku_su = array("", "");

//年設定
$default_start_year = 2006;
$cur_year = date("Y");
//年一覧開始年
if ($start_year == "") {
	//当年を元に開始年を求める
	$start_year = $default_start_year + (intval(($cur_year-$default_start_year)/7) * 7);
}
$end_year = $start_year + 6;

//指定年
if ($year == "") {
	$year = $cur_year;
}

//公休数取得 月毎、属性毎の配列
$arr_legal_hol_cnt = get_legal_hol_cnt($con, $fname, $year, $timecard_bean->closing, $timecard_bean->closing_month_flg);

$arr_holwk = get_timecard_holwk_day($con, $fname);

$arr_data = array();

for ($i=0; $i<count($arr_holwk); $i++) {
	//属性別の件数を計算で求めるため、フラグを1,0に変換
	$wk_legal_flg = ($arr_holwk[$i]["legal_holiday"] == "t") ? 1 : 0;
	$wk_national_flg = ($arr_holwk[$i]["national_holiday"] == "t") ? 1 : 0;
	$wk_newyear_flg = ($arr_holwk[$i]["newyear_holiday"] == "t") ? 1 : 0;
	$wk_prescribed_flg = ($arr_holwk[$i]["prescribed_holiday"] == "t") ? 1 : 0;

	//月データ
	for ($j=0; $j<12; $j++) {
		$arr_data[$i][$j] = $arr_legal_hol_cnt[$j]["4"] * $wk_legal_flg +
			$arr_legal_hol_cnt[$j]["5"] * $wk_prescribed_flg +
			$arr_legal_hol_cnt[$j]["6"] * $wk_national_flg +
			$arr_legal_hol_cnt[$j]["7"] * $wk_newyear_flg;
	}
}

//設定保存
if ($mode == "update") {
	pg_query($con, "begin transaction");
	//削除
	$sql = "delete from timecard_legal_hol";
	$cond = "where year = '$year'";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	//登録
	for ($i=0; $i<$data_cnt; $i++) {

		$mon_str = "";
		for ($j=1; $j<=12; $j++) {
			$mon_str .= ", mon_$j";
		}
		$sign_str = "";
		for ($j=1; $j<=12; $j++) {
			$sign_str .= ", sign_$j";
		}
		$sql = "insert into timecard_legal_hol (year, group_id $mon_str $sign_str ) values (";
		$wk_id = $i+1;

		$content = array($year, $wk_id);
		for ($j=0; $j<12; $j++) {
			$varname = "mon_{$i}_{$j}"; // $iは表示順を0始まりで使用
			$wk_str = $$varname;
			if ($wk_str == "") {
				$wk_str = null;
			}
			array_push($content, pg_escape_string($wk_str));
		}
		for ($j=0; $j<12; $j++) {
			$varname = "sign_{$i}_{$j}"; // $iは表示順を0始まりで使用
			$wk_str = $$varname;
			if ($wk_str == "") {
				$wk_str = null;
			}
			array_push($content, pg_escape_string($wk_str));
		}
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
	pg_query("commit");
}
//グループ別の調整日数
$arr_group_data = get_legal_hol_group($con, $fname, $year, "");
$data_cnt = count($arr_group_data);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
	///-----------------------------------------------------------------------------
	// 登録
	///-----------------------------------------------------------------------------
	function updateData()
	{
		document.mainform.mode.value="update";
		document.mainform.action="timecard_legal_hol_cnt.php";
		document.mainform.submit();
	}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#bdd1e7 solid 1px;}

table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block th {background: #bdd1e7; border:#5279a5 solid 1px; font:"ＭＳ Ｐゴシック"; }

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>

<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");

//有休付与日数表の読み込み
$sql = "select * from timecard_paid_hol order by paid_hol_tbl_id";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
<tr>
<td>
<?
//休暇種別メニュータブ表示
show_timecard_hol_menuitem($session,$fname);
?>
</td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>

<form name="mainform" action="timecard_legal_hol_cnt.php" method="post">
<?
show_navi($year, $start_year, $default_start_year, $fname);
?>
<table>
	<tr>

		<td>
			<table width="800" border="0" cellspacing="0" cellpadding="1"  class="list">
				<tr bgcolor="#f6f9ff">
					<td nowrap align="center" width="200"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務パターングループ</font></td>
					<? for ($i=0; $i<12; $i++) { ?>
					<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$i+1?>月</font></td>
					<? } ?>
				</tr>
<? for ($i=0; $i<count($arr_group_data)+1; $i++) { ?>
				<tr>
					<td nowrap align="left" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?
//職種勤務形態
	if ($i == 0) {
		echo("標準（休日出勤判定から計算）");
		echo("</font></td>");
					?>
					<? for ($j=0; $j<12; $j++) { ?>
					<td nowrap align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$arr_data[$i][$j]?></font></td>
					<? }
	} else {
		$idx = $i - 1;
		echo($arr_group_data[$idx]["group_name"]);
		echo("&nbsp;調整日数");
		echo("</font></td>");
		for ($j=0; $j<12; $j++) {
			echo("<td nowrap>");
			echo("<select name=\"sign_{$idx}_{$j}\">");
			$wk_j = $j + 1;
			$wk_sign = $arr_group_data[$idx]["sign_{$wk_j}"];
			$selected = ($wk_sign == "1") ? " selected" : "";
			echo("<option value=\"1\" $selected>+</option>");
			$selected = ($wk_sign == "2") ? " selected" : "";
			echo("<option value=\"2\" $selected>-</option>");
			echo("</select>\n");
			echo("<select name=\"mon_{$idx}_{$j}\">");
			$wk_mon = $arr_group_data[$idx]["mon_{$wk_j}"];
            //日のプルダウンに0.5を追加 20130213 start
			for ($k=0; $k<=15; $k+=0.5) {
				$selected = ($wk_mon == $k) ? " selected" : "";
                $wk_disp = sprintf("%.1f", $k);
                echo("<option value=\"$k\" $selected>$wk_disp</option>\n");
            }
            //日のプルダウンに0.5を追加 20130213 end
            echo("</select>\n");
			echo("</td>\n");
		}

	}
					 ?>
				</tr>
<? } ?>
			</table>
		</td>
	</tr>


			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="postback" value="t">
<input type="hidden" name="mode" value="">
<input type="hidden" name="year" value="<? echo($year); ?>">
<input type="hidden" name="data_cnt" value="<? echo($data_cnt); ?>">
<input type="hidden" name="del_not_cnt" value="<? echo($del_not_cnt); ?>">
<input type="hidden" name="add_idx" value="<? echo($add_idx); ?>">
<input type="hidden" name="del_idx" value="<? echo($del_idx); ?>">
<input type="hidden" name="start_year" value="<? echo($start_year); ?>">
</form>
</td>
</tr>
</table>
<?
show_navi($year, $start_year, $default_start_year, $fname);
?>
</body>
<? pg_close($con); ?>
</html>
<?

/**
 * ページ切り替えリンクを表示
 *
 * @param string $year 処理年度
 * @param string $start_year リンク開始年
 * @param string $default_start_year デフォルト開始年
 * @param string $fname プログラム名
 * @return なし
 *
 */
function show_navi($year, $start_year, $default_start_year, $fname) {
	echo("<img src=\"img/spacer.gif\" width=\"1\" height=\"2\" alt=\"\"><br>\n");

	global $session, $url_srch_name, $cls, $atrb, $dept, $duty_form_jokin, $duty_form_hijokin, $group_id, $select_add_mon, $select_add_day;

	$url_search_cond = "&srch_name=$url_srch_name&cls=$cls&atrb=$atrb&dept=$dept&duty_form_jokin=$duty_form_jokin&duty_form_hijokin=$duty_form_hijokin&group_id=$group_id&select_add_mon=$select_add_mon&select_add_day=$select_add_day";

	echo("<table width=\"1050\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"); //
	echo("<tr>\n");
	//年
	$cnt_per_page = 7;
	echo("<td width=\"\">\n");
	echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
//	if ($num > 0 ) {
		echo("年");
		echo("&nbsp;");
		if ($start_year > $default_start_year) {
			$disp_year = $start_year-$cnt_per_page;
			$last_year = $start_year-1;
			echo("<a href=\"$fname?session=$session$url_search_cond&page=0&srch_flg=1&start_year={$disp_year}&year=$last_year\">←</a>");
		}
		echo("&nbsp;");
		for ($i=0; $i<$cnt_per_page; $i++) {
			$disp_year = $start_year+$i;
			if ($year == $start_year+$i) {
				echo($disp_year);
			} else {
				echo("<a href=\"$fname?session=$session$url_search_cond&page=0&srch_flg=1&year={$disp_year}&start_year={$start_year}\">{$disp_year}</a>");
			}
			echo("&nbsp;");
		}
		$disp_year = $start_year+$cnt_per_page;
		echo("<a href=\"$fname?session=$session$url_search_cond&page=0&srch_flg=1&start_year={$disp_year}&year={$disp_year}\">→</a>");
//	}
	echo("</font></td>\n");
	echo("<td width=\"100\"></td>\n");
	echo("<td align=\"right\"><input type=\"button\" value=\"登録\" onClick=\"updateData();\"></td>");

	echo("</tr>\n");
	echo("</table>\n");
}
/*************************************************************************/
// 職種ＤＢ(jobmst)情報取得
// @param
//
// @return	$data_job	取得情報
/*************************************************************************/
function get_jobmst_array($con, $fname) {
	//-------------------------------------------------------------------
	//検索
	//-------------------------------------------------------------------
	$sql = "select job_id,job_nm from jobmst";
	$cond = "where job_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);		//職種一覧を取得
	if($sel == 0){
		pg_close($con);
		echo("<script type='text/javascript' src='./js/showpage.js'></script>");
		echo("<script language='javascript'>showErrorPage(window);</script>");
		exit;
	}
	$num = pg_numrows($sel);
	//-------------------------------------------------------------------
	//情報設定
	//-------------------------------------------------------------------
	$data_job = array();
	for($i=0;$i<$num;$i++){
		$data_job[$i]["id"] = pg_result($sel,$i,"job_id");
		$data_job[$i]["name"] = pg_result($sel,$i,"job_nm");
	}

	return $data_job;
}



?>