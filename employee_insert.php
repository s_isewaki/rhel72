<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<body>
<form name="items" method="post" action="employee_register.php">
<input type="hidden" name="personal_id" value="<? echo($personal_id); ?>">
<input type="hidden" name="lt_nm" value="<? echo($lt_nm); ?>">
<input type="hidden" name="ft_nm" value="<? echo($ft_nm); ?>">
<input type="hidden" name="lt_kana_nm" value="<? echo($lt_kana_nm); ?>">
<input type="hidden" name="ft_kana_nm" value="<? echo($ft_kana_nm); ?>">
<input type="hidden" name="id" value="<? echo($id); ?>">
<input type="hidden" name="pass" value="<? echo($pass); ?>">
<input type="hidden" name="mail_id" value="<? echo($mail_id); ?>">
<input type="hidden" name="sex" value="<? echo($sex); ?>">
<input type="hidden" name="birth_yr" value="<? echo($birth_yr); ?>">
<input type="hidden" name="birth_mon" value="<? echo($birth_mon); ?>">
<input type="hidden" name="birth_day" value="<? echo($birth_day); ?>">
<input type="hidden" name="entry_yr" value="<? echo($entry_yr); ?>">
<input type="hidden" name="entry_mon" value="<? echo($entry_mon); ?>">
<input type="hidden" name="entry_day" value="<? echo($entry_day); ?>">
<input type="hidden" name="cls" value="<? echo($cls); ?>">
<input type="hidden" name="atrb" value="<? echo($atrb); ?>">
<input type="hidden" name="dept" value="<? echo($dept); ?>">
<input type="hidden" name="room" value="<? echo($room); ?>">
<input type="hidden" name="status" value="<? echo($status); ?>">
<input type="hidden" name="job" value="<? echo($job); ?>">
<input type="hidden" name="idm" value="<? echo($idm); ?>">
<input type="hidden" name="aprv" value="<? echo($aprv); ?>">
<input type="hidden" name="del" value="<? echo($del); ?>">
<input type="hidden" name="del_id" value="<? echo($del_id); ?>">
<input type="hidden" name="del_nm" value="<? echo($del_nm); ?>">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="concurrent" value="<? echo($concurrent); ?>">
<input type="hidden" name="updatephoto" value="<? echo($updatephoto); ?>">
<input type="hidden" name="profile" value="<? echo($profile); ?>">
<input type="hidden" name="auth_group_id" value="<? echo($auth_group_id); ?>">
<input type="hidden" name="disp_group_id" value="<? echo($disp_group_id); ?>">
<input type="hidden" name="menu_group_id" value="<? echo($menu_group_id); ?>">
<input type="hidden" name="emp_name_deadlink_flg" value="<? echo($emp_name_deadlink_flg); ?>">
<input type="hidden" name="emp_class_deadlink_flg" value="<? echo($emp_class_deadlink_flg); ?>">
<input type="hidden" name="emp_job_deadlink_flg" value="<? echo($emp_job_deadlink_flg); ?>">
<input type="hidden" name="emp_st_deadlink_flg" value="<? echo($emp_st_deadlink_flg); ?>">
<input type="hidden" name="postback" value="t">
<?
for ($i = 1; $i <= $concurrent; $i++) {
	$class_var_name = "o_cls$i";
	$atrb_var_name = "o_atrb$i";
	$dept_var_name = "o_dept$i";
	$room_var_name = "o_room$i";
	$status_var_name = "o_status$i";
?>
<input type="hidden" name="<? echo($class_var_name); ?>" value="<? echo($$class_var_name); ?>">
<input type="hidden" name="<? echo($atrb_var_name); ?>" value="<? echo($$atrb_var_name); ?>">
<input type="hidden" name="<? echo($dept_var_name); ?>" value="<? echo($$dept_var_name); ?>">
<input type="hidden" name="<? echo($room_var_name); ?>" value="<? echo($$room_var_name); ?>">
<input type="hidden" name="<? echo($status_var_name); ?>" value="<? echo($$status_var_name); ?>">
<?
}
?>
</form>
<?
//ファイルの読み込み
require_once("about_postgres.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_validation.php");
require_once("show_class_name.ini");
require_once("get_values.ini");
require_once("passwd_change_common.ini");
require("employee_auth_common.php");
require_once("webmail_quota_functions.php");

//ページ名
$fname = $PHP_SELF;

//セッションチェック
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

//職員登録権限チェック
$reg_auth = check_authority($session,19,$fname);
if($reg_auth == "0"){
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 環境設定情報を取得
$sql = "select site_id, use_cyrus from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
$site_id = pg_fetch_result($sel, 0, "site_id");
$use_cyrus = pg_fetch_result($sel, 0, "use_cyrus");


// 表示グループの設定情報を取得
if($disp_group_id != ""){
	$sql = "select * from dispgroup";
	$cond = "where group_id = $disp_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	$disp_groups = pg_fetch_assoc($sel);

}

// メニューグループの設定情報を取得
if($menu_group_id != ""){
	$sql = "select * from menugroup";
	$cond = "where group_id = $menu_group_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
		exit;
	}

	$menu_groups = pg_fetch_assoc($sel);

}

//**********正規表現チェック**********
//----------名前情報----------
//苗字（漢字）null check
if($lt_nm ==""){
	echo("<script type=\"text/javascript\">alert(\"苗字（漢字）を入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//苗字（漢字）文字数チェック
if(strlen($lt_nm) > 20){
	echo("<script type=\"text/javascript\">alert(\"苗字（漢字）が長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）null check
if($ft_nm ==""){
	echo("<script type=\"text/javascript\">alert(\"名前（漢字）を入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（漢字）文字数チェック
if(strlen($ft_nm) > 20){
	echo("<script type=\"text/javascript\">alert(\"名前（漢字）が長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//苗字（かな）null check
if($lt_kana_nm ==""){
	echo("<script type=\"text/javascript\">alert(\"苗字（ひらがな）を入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
$lt_kana_nm = mb_convert_kana($lt_kana_nm, "HVc");

//苗字（かな）文字数チェック
if(strlen($lt_kana_nm) > 20){
	echo("<script type=\"text/javascript\">alert(\"苗字（ひらがな）が長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//名前（かな）null check
if($ft_kana_nm ==""){
	echo("<script type=\"text/javascript\">alert(\"名前（ひらがな）を入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
$ft_kana_nm = mb_convert_kana($ft_kana_nm, "HVc");

//名前（かな）文字数チェック
if(strlen($ft_kana_nm) > 20){
	echo("<script type=\"text/javascript\">alert(\"名前（ひらがな）が長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//ログインID null check
if($id ==""){
	echo("<script type=\"text/javascript\">alert(\"ログインIDを入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//ログインID 文字数チェック
if(strlen($id) > 20){
	echo("<script type=\"text/javascript\">alert(\"ログインIDが長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 文字種チェック
if ($site_id == "") {
	if (preg_match("/[^0-9a-z_-]/", $id) > 0) {
		echo("<script type=\"text/javascript\">alert(\"ログインIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
} else {
	if (preg_match("/[^0-9a-zA-Z_-]/", $id) > 0) {
		echo("<script type=\"text/javascript\">alert(\"ログインIDに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}
if (substr($id, 0, 1) == "-") {
	echo("<script type=\"text/javascript\">alert(\"ログインIDの先頭に「-」は使えません。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 登録禁止アカウントチェック
if ($site_id == "") {
	if ($id == "cyrus" || $id == "postmaster" || $id == "root") {
		echo("<script type=\"text/javascript\">alert(\"「{$id}」はログインIDとして使用できません。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

//パスワード null check
if($pass ==""){
	echo("<script type=\"text/javascript\">alert(\"パスワードを入力してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//パスワード 文字数チェック
$pass_length = get_pass_length($con, $fname);
if(strlen($pass) > $pass_length){
	echo("<script type=\"text/javascript\">alert(\"パスワードが長すぎます。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 文字種チェック
if (preg_match("/[^0-9a-zA-Z_-]/", $pass) > 0) {
	echo("<script type=\"text/javascript\">alert(\"パスワードに使える文字は半角の「0〜9」「a〜z」「A〜Z」「_」「-」のみです。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}
if (substr($pass, 0, 1) == "-") {
	echo("<script type=\"text/javascript\">alert(\"パスワードの先頭に「-」は使えません。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// メールIDチェック
if ($site_id != "") {
	if ($mail_id == "") {
		echo("<script type=\"text/javascript\">alert(\"メールIDを入力してください。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	if (strlen($mail_id) > 20) {
		echo("<script type=\"text/javascript\">alert(\"メールIDが長すぎます。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}

	if ($use_cyrus == "t") {
		if (preg_match("/[^0-9a-z_-]/", $mail_id) > 0) {
			echo("<script type=\"text/javascript\">alert(\"メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」のみです。\");</script>\n");
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
			exit;
		}
	} else {
		if (preg_match("/[^0-9a-z_.-]/", $mail_id) > 0) {
			echo("<script type=\"text/javascript\">alert(\"メールIDに使える文字は半角の「0〜9」「a〜z」「_」「-」「.」のみです。\");</script>\n");
			echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
			exit;
		}
	}

	if (substr($mail_id, 0, 1) == "-") {
		echo("<script type=\"text/javascript\">alert(\"メールIDの先頭に「-」は使えません。\");</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// ログインIDとパスワードが同じ場合
if ($id == $pass) {

	// 環境設定情報を取得
	$sql = "select weak_pwd_flg from config";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$weak_pwd_flg = pg_fetch_result($sel, 0, "weak_pwd_flg");

	// 許可しない設定の場合はエラーとする
	if ($weak_pwd_flg == "f") {
		echo("<script type=\"text/javascript\">alert('ログインIDと同じパスワードは使用できません。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

if ($birth_yr == "-" && $birth_mon == "-" && $birth_day == "-") {
	$birth = "";
} else if ($birth_yr != "-" && $birth_mon != "-" && $birth_day != "-") {
	if (!checkdate($birth_mon, $birth_day, $birth_yr)) {
		echo("<script type=\"text/javascript\">alert('生年月日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$birth = "$birth_yr$birth_mon$birth_day";
} else {
	echo("<script type=\"text/javascript\">alert('生年月日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

if ($entry_yr == "-" && $entry_mon == "-" && $entry_day == "-") {
	$entry = "";
} else if ($entry_yr != "-" && $entry_mon != "-" && $entry_day != "-") {
	if (!checkdate($entry_mon, $entry_day, $entry_yr)) {
		echo("<script type=\"text/javascript\">alert('入職日が不正です。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	$entry = "$entry_yr$entry_mon$entry_day";
} else {
	echo("<script type=\"text/javascript\">alert('入職日が不正です。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//所属部門
if($cls =="0"){
	echo("<script type=\"text/javascript\">alert(\"所属{$arr_class_name[0]}を選択してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//所属課
if($atrb =="0"){
	echo("<script type=\"text/javascript\">alert(\"所属{$arr_class_name[1]}を選択してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//所属科
if($dept =="0"){
	echo("<script type=\"text/javascript\">alert(\"所属{$arr_class_name[2]}を選択してください。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// 所属室（null可とする）
if ($room == 0) {
	$room = null;
}

// ICカードIDm
if (strlen($idm) > 32) {
	echo("<script language=\"javascript\">alert('ICカードIDmが長すぎます。');</script>\n");
	echo("<script language=\"javascript\">document.items.submit();</script>\n");
	exit;
}
if ($idm != "") {
	$sql = "select count(*) from empmst";
	$cond = "where emp_idm = '$idm' and exists (select * from authmst where authmst.emp_id = empmst.emp_id and emp_del_flg = 'f')";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (intval(pg_fetch_result($sel, 0, 0)) > 0) {
		echo("<script language=\"javascript\">alert('入力されたICカードIDmは、すでに使われています。');</script>\n");
		echo("<script language=\"javascript\">document.items.submit();</script>\n");
		exit;
	}
}

// 兼務所属
$concurrents = array();
for ($i = 1; $i <= $concurrent; $i++) {
	$class_var_name = "o_cls$i";
	$atrb_var_name = "o_atrb$i";
	$dept_var_name = "o_dept$i";
	$room_var_name = "o_room$i";
	$status_var_name = "o_status$i";

	if ($$class_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[0]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$atrb_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[1]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$dept_var_name == "0") {
		echo("<script type=\"text/javascript\">alert('{$i}番目の兼務所属の所属{$arr_class_name[2]}を選択してください。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
	if ($$room_var_name == 0) {
		$$room_var_name = null;
	}

	if ($$class_var_name == $cls && $$atrb_var_name == $atrb && $$dept_var_name == $dept && $$room_var_name == $room && $$status_var_name == $status) {
		continue;
	}

	$concurrents[] = array(
		"class" => $$class_var_name,
		"atrb" => $$atrb_var_name,
		"dept" => $$dept_var_name,
		"room" => $$room_var_name,
		"status" => $$status_var_name
	);
}
$concurrents = array_unique($concurrents);

if (count($concurrents) != $concurrent) {
	echo("<script type=\"text/javascript\">alert('所属が重複しています。');</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//----------名前情報----------
$keywd = get_keyword($lt_kana_nm);

//----------Transaction begin----------
pg_query($con, "begin");

// 職員ID重複チェック
$sql_dup_psn_id = "select count(*) from empmst";
$cond_dup_psn_id = "where emp_personal_id = '$personal_id'";
$sel_dup_psn_cnt = select_from_table($con, $sql_dup_psn_id, $cond_dup_psn_id, $fname);
if ($sel_dup_psn_cnt == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dup_psn_cnt = pg_result($sel_dup_psn_cnt, 0, 0);
if ($dup_psn_cnt > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"指定された職員IDは既に使用されています。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

//----------ログインID重複チェック----------
$sql = "select count(*) from login";
$cond_dup_loginid_cnt = "where emp_login_id = '$id'";
$sel_dup_loginid_cnt = select_from_table($con, $sql, $cond_dup_loginid_cnt, $fname);
if ($sel_dup_loginid_cnt == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$dup_loginid_cnt = pg_result($sel_dup_loginid_cnt, 0, 0);
if ($dup_loginid_cnt > 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\">alert(\"指定されたログインIDは既に使用されています。\");</script>\n");
	echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
	exit;
}

// メールID重複チェック
if ($site_id != "") {
	$sql = "select count(*) from login inner join authmst on login.emp_id = authmst.emp_id";
	$cond = "where login.emp_login_mail = '$mail_id' and authmst.emp_del_flg = 'f'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_result($sel, 0, 0) > 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\">alert('指定されたメールIDは既に使用されています。');</script>\n");
		echo("<script type=\"text/javascript\">document.items.submit();</script>\n");
		exit;
	}
}

//----------最初に新しいIDとパスワードを取得する----------
$cond = "";
$sel = select_from_table($con,"SELECT max(emp_id) FROM empmst",$cond,$fname);			//max値を取得

	if($sel==0){
		pg_query($con,"rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$max = pg_result($sel,0,"max");	//max値を取得する
	$val = substr($max,4,12)+1;		//max値からID番号の部分を取り出しインクリメント
	$yrs = date("y");			    //職員IDの頭二桁
	$mth = date("m");			    //職員IDの次の二桁
	$yymm ="$yrs"."$mth";			//職員IDの頭４桁を作成

	for($i=8;$i>strlen($val);$i--){
		$zero=$zero."0";
	}

	$new_emp_id = "$yymm$zero$val";	//新しい職員ID
	//echo($new_emp_id);

//----------loginテーブルに挿入する----------
$sql = "insert into login (emp_id, emp_login_id, emp_login_pass, emp_login_flg, emp_login_mail) values (";
$content_login = array($new_emp_id, $id, $pass, "t", $mail_id);
$in_login = insert_into_table($con, $sql, $content_login, $fname);
if ($in_login == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//----------authmstテーブルに挿入する----------

// 決裁者権限
if ($aprv != "t") {
	$aprv = "f";
}

// 権限グループIDを更新
$auth_group_id = ($auth_group_id == "") ? null : $auth_group_id;


// 権限情報を登録
$pass_flg = is_null($auth_group_id) ? "t" : "f";
$sql = "insert into authmst (emp_id, emp_pass_flg, emp_aprv_auth) values(";
$content_auth = array($new_emp_id, $pass_flg, $aprv);
$in_auth = insert_into_table($con, $sql, $content_auth, $fname);
if ($in_auth == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//---------- option ----------
$sql = "insert into option(emp_id, schedule1_default, schedule2_default) values(";
$content_option = array($new_emp_id, "2", "2");
$in_option = insert_into_table($con, $sql, $content_option, $fname);
if ($in_option == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//---------- emp_relation ----------
if (version_compare(PHP_VERSION, '5.0.0') >= 0 && substr(pg_parameter_status($con, 'server_version'), 0, 1) >= 8) {
    $emp_name_deadlink_flg = $_POST['emp_name_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_class_deadlink_flg = $_POST['emp_class_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_job_deadlink_flg = $_POST['emp_job_deadlink_flg'] === 't' ? 't' : 'f';
    $emp_st_deadlink_flg = $_POST['emp_st_deadlink_flg'] === 't' ? 't' : 'f';
    $sql_rel = "SELECT update_emp_relation(";
    $content_rel = array($new_emp_id, $emp_name_deadlink_flg, $emp_class_deadlink_flg, $emp_job_deadlink_flg, $emp_st_deadlink_flg);
    $in_rel = insert_into_table($con, $sql_rel, $content_rel, $fname);
    if ($in_rel == 0) {
        pg_query($con, "rollback");
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
}

// 表示設定の更新（表示グループを設定している場合）
if($disp_group_id != ""){

	// 表示設定更新
	$sql = "update option set";
	$set = array("default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "top_workflow_flg", "disp_group_id");
	$setvalue = array($disp_groups["default_page"], $disp_groups["font_size"], $disp_groups["top_mail_flg"], $disp_groups["top_ext_flg"], $disp_groups["top_event_flg"], $disp_groups["top_schd_flg"], $disp_groups["top_info_flg"], $disp_groups["top_task_flg"], $disp_groups["top_msg_flg"], $disp_groups["top_aprv_flg"], $disp_groups["top_schdsrch_flg"], $disp_groups["top_lib_flg"], $disp_groups["top_bbs_flg"], $disp_groups["top_memo_flg"], $disp_groups["top_link_flg"], $disp_groups["top_intra_flg"], $disp_groups["bed_info"], $disp_groups["schd_type"], $disp_groups["top_wic_flg"], $disp_groups["top_fcl_flg"], $disp_groups["top_inci_flg"], $disp_groups["top_cas_flg"], $disp_groups["top_fplus_flg"], $disp_groups["top_jnl_flg"], $disp_groups["top_free_text_flg"], $disp_groups["top_manabu_flg"], $disp_groups["top_workflow_flg"], $disp_group_id);
	$cond = "where emp_id = '$new_emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// メニューグループを設定している場合、メニューを設定
if($menu_group_id != ""){

	// サイドメニュー更新
	$sql = "update option set";
	$set = array("sidemenu_show_flg","sidemenu_position");
	$setvalue = array($menu_groups["sidemenu_show_flg"], $menu_groups["sidemenu_position"]);
	$cond = "where emp_id = '$new_emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// メニュー設定
	for ($i = 1; $i <= 12; $i++){
		$menu_num = 'menu_id_'.$i;
		if($menu_groups[$menu_num] == "") break;
		$sql = "insert into headermenu(emp_id, menu_id, menu_order) values(";
		$content = array($new_emp_id, $menu_groups[$menu_num], $i);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

	// menu_group_idの登録
	$sql = "update option set";
	$set = array("menu_group_id");
	$setvalue = array($menu_group_id);
	$cond = "where emp_id = '$new_emp_id'";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

//---------- mygroupmst ----------
$sql = "insert into mygroupmst (owner_id, mygroup_id, mygroup_nm) values(";
$content_mygroupmst = array($new_emp_id, 1, "マイグループ");
$in_mygroupmst = insert_into_table($con, $sql, $content_mygroupmst, $fname);
if ($in_mygroupmst == 0) {
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

//----------empmstテーブルに挿入する----------
$sql = "insert into empmst(emp_id, emp_personal_id, emp_class, emp_attribute, emp_dept, emp_room, emp_job, emp_st, emp_lt_nm, emp_ft_nm, emp_kn_lt_nm, emp_kn_ft_nm, emp_sex, emp_birth, emp_join, emp_keywd, emp_idm, emp_profile) values(";
$content_emp = array($new_emp_id, $personal_id, $cls, $atrb, $dept, $room, $job, $status, $lt_nm, $ft_nm, $lt_kana_nm, $ft_kana_nm, $sex, $birth, $entry, $keywd, $idm, $profile) ;
$in_emp = insert_into_table($con, $sql, $content_emp, $fname);
if ($in_emp == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 必要があればコピー元データをemptmpから削除
if ($del_id != "") {
	if ($del == "on") {
		$sql = "delete from emptmp";
		$cond = "where emp_personal_id = '$del_id'";
		$del = delete_from_table($con, $sql, $cond, $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	} else {
		$sql = "update emptmp set";
		$set = array("registered");
		$setvalue = array("t");
		$cond = "where emp_personal_id = '$del_id'";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// 兼務所属レコードを作成
foreach ($concurrents as $tmp_concurrent) {
	$sql = "insert into concurrent (emp_id, emp_class, emp_attribute, emp_dept, emp_room, emp_st) values (";
	$content = array($new_emp_id, $tmp_concurrent["class"], $tmp_concurrent["atrb"], $tmp_concurrent["dept"], $tmp_concurrent["room"], $tmp_concurrent["status"]);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// 勤務シフト作成のリアルタイム同期対応
$sql = "select sync_flag from duty_shift_option";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sync_flag = pg_fetch_result($sel, 0, "sync_flag");
if ($sync_flag == "t") {
	$sql = "select group_id from duty_shift_sync_link";
	$cond = "where class_id = $cls and atrb_id = $atrb and dept_id = $dept and room_id ";
	$cond .= ($room == "") ? "is null" : " = $room";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$group_id = pg_fetch_result($sel, 0, "group_id");

		$sql = "select count(*) from duty_shift_staff";
		$cond = "where group_id = '$group_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$no = intval(pg_fetch_result($sel, 0, 0)) + 1;

		$sql = "insert into duty_shift_staff (group_id, emp_id, no) values (";
		$content = array($group_id, $new_emp_id, $no);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}

// XOOPSアカウントの作成
//XX require_once("community/mainfile.php");
require_once("webmail/config/config.php");

// 権限グループの更新
//XX $con_mysql = mysql_connect(XOOPS_DB_HOST, XOOPS_DB_USER, XOOPS_DB_PASS);
//XX auth_change_group_of_employee($con, $con_mysql, $new_emp_id, $auth_group_id, $fname);
auth_change_group_of_employee($con, $new_emp_id, $auth_group_id, $fname);

// 権限整合性・ライセンスのチェック
//XX auth_check_consistency($con, $con_mysql, $session, $fname, false);
auth_check_consistency($con, $session, $fname, false);

//XX mysql_select_db(XOOPS_DB_NAME, $con_mysql);
//XX
//XX $sql = "insert into " . XOOPS_DB_PREFIX . "_users (name, uname, email, user_regdate, user_viewemail, pass, rank, level, timezone_offset, notify_method, user_mailok) values ('$lt_nm $ft_nm', '$id', '$id@$domain', " . time() . ", 1, '" . md5($pass) . "', 0, 1, '9.0', 2, 0);";
//XX if (!mysql_query($sql, $con_mysql)) {
//XX 	pg_query($con, "rollback");
//XX 	pg_close($con);
//XX 	mysql_close($con_mysql);
//XX 	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 	exit;
//XX }
//XX
//XX $uid = mysql_insert_id($con_mysql);
//XX $sql = "insert into " . XOOPS_DB_PREFIX . "_groups_users_link (groupid, uid) values (2, $uid)";
//XX if (!mysql_query($sql, $con_mysql)) {
//XX 	pg_query($con, "rollback");
//XX 	pg_close($con);
//XX 	mysql_close($con_mysql);
//XX 	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//XX 	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
//XX 	exit;
//XX }

// コミット処理
pg_query($con, "commit");
pg_close($con);
//XX mysql_close($con_mysql);

if ($use_cyrus == "t") {

	// メールアカウントの追加（ログインIDが数字のみでない場合）
	$account = ($site_id == "") ? $id : "{$mail_id}_{$site_id}";
	if (preg_match("/[a-z]/", $account) > 0) {
		$dir = getcwd();
		if ($imapServerAddress == "localhost" || $imapServerAddress == "127.0.0.1") {
			exec("/usr/bin/perl $dir/mboxadm/addmailbox $account $pass 2>&1 1> /dev/null", $output, $exit_status);
			if ($exit_status !== 0) {
				exec("/usr/bin/expect -f $dir/expect/addmailbox.exp $account $pass");
			}
		} else {
			exec("/usr/bin/expect -f $dir/expect/rsyncaddmailbox.exp $account $pass $imapServerAddress $dir");
		}
	}
}

webmail_quota_change_to_default($account);

// 写真関連処理
if ($updatephoto == "t" && $_FILES["photofile"]["error"] == 0) {
	if (!is_dir("profile")) {
		mkdir("profile", 0755);
	}
	copy($_FILES["photofile"]["tmp_name"], "profile/{$new_emp_id}.jpg");
}

//echo("<script type=\"text/javascript\">location.href = 'employee_detail.php?session=$session&emp_id=$new_emp_id';</script>");
echo("<script type=\"text/javascript\">location.href = 'employee_register.php?session=$session';</script>");
?>
</body>
