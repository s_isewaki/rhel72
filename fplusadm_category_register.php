<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix ファントルくん＋ | 管理画面 | カテゴリ登録</title>
<script type="text/javascript" src="./js/fontsize.js"></script>
<?
require_once("about_comedix.php");
require_once("get_values.ini");
require_once("fplusadm_common.ini");
require_once("fplus_common_class.php");
require_once("show_class_name.ini");

$fname=$PHP_SELF;

// セッションのチェック
$session=qualify_session($session,$fname);
if($session=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth=check_authority($session,79,$fname);
if($checkauth=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$con=connect2db($fname);

$obj = new fplus_common_class($con, $fname);

if($mode == "init")
{
	$src_cate_id = $cate_id;
	$src_folder_id = $folder_id;
	$dest_cate_id = $cate_id;
	$dest_folder_id = $folder_id;
}

// カテゴリ名
if($dest_cate_id != "")
{
	$wkfl_mst = $obj->get_wkfwcate_mst($dest_cate_id);
	$folder_path = $wkfl_mst["wkfw_nm"];
}
// フォルダ名
if($dest_folder_id != "")
{
	$folder_list = $obj->get_folder_path($dest_folder_id);
	foreach($folder_list as $folder)
	{
		if($folder_path != "")
		{
			$folder_path .= " > ";
		}
		$folder_path .= $folder["name"];
	}
}
if($folder_path == "")
{
	$folder_path = "ルート";
}


// 部門一覧を取得
$sel_class = $obj->get_classmst();
$classes = array();
$classes["-"] = "----------";
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	$classes[$tmp_class_id] = $tmp_class_nm;
}
pg_result_seek($sel_class, 0);

// 課一覧を取得
$sel_atrb = $obj->get_atrbmst();

// 科一覧を取得
$sel_dept = $obj->get_deptmst();
$dept_names = Array();
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_dept_nm = $row["dept_nm"];
	$dept_names["{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}"] = $tmp_dept_nm;
}
pg_result_seek($sel_dept, 0);

// 役職一覧を取得
$sel_st = $obj->get_stmst();

// 組織名を取得
$arr_class_name = get_class_name_array($con, $fname);

// 職員ＩＤを取得
$emp_id = get_emp_id($con,$session,$fname);

// デフォルト値の設定
$ref_dept_flg = ($ref_dept_flg == "") ? "1" : $ref_dept_flg;
$ref_st_flg = ($ref_st_flg == "") ? "1" : $ref_st_flg;

if ($ref_toggle_mode == "") {$ref_toggle_mode = "▼";}
$ref_toggle_display = ($ref_toggle_mode == "▼") ? "none" : "";


// 初期表示時は当該職員を対象者に
//if ($back != "t") {
//	$target_id_list1 = $emp_id;
//}


// メンバー情報を配列に格納
$arr_target['1'] = array();
if ($target_id_list1 != "") {
	$arr_target_id = split(",", $target_id_list1);
	for ($i = 0; $i < count($arr_target_id); $i++) {
		$tmp_emp_id = $arr_target_id[$i];
		$sql = "select (emp_lt_nm || ' ' || emp_ft_nm) as emp_name from empmst";
		$cond = "where emp_id = '$tmp_emp_id'";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
		}
		$tmp_emp_name = pg_fetch_result($sel, 0, "emp_name");
		array_push($arr_target['1'], array("id" => $tmp_emp_id, "name" => $tmp_emp_name));
	}
}
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#E6B3D4 solid 1px;}
.list {border-collapse:collapse;}
.list td {border:#E6B3D4 solid 1px;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
<?
require_once("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=16';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

// 初期処理
function initPage()
{
    document.getElementById('folder_path').innerHTML = '<?=$folder_path?>';

    setRefClassSrcOptions(true, '<? echo($ref_class_src); ?>', '<? echo($ref_atrb_src); ?>');
    setDisabled();
    update_target_html("1");
}

// 保存先設定画面
function openFolderSetting()
{
    var cate_id   = document.getElementById('dest_cate_id').value;
    var folder_id = document.getElementById('dest_folder_id').value;
    window.open('fplusadm_folder_setting.php?session=<?=$session?>&selected_cate=' + cate_id + '&selected_folder=' + folder_id, 'newwin', 'width=640,height=700,scrollbars=yes');
}

// 保存先設定画面から呼ばれる
function setCategory(folder_name, cate_id, folder_id)
{
    document.getElementById('folder_path').innerHTML = folder_name;
    document.getElementById('dest_cate_id').value = cate_id;
    document.getElementById('dest_folder_id').value = folder_id;
}



var classes = [];
<?
while ($row = pg_fetch_array($sel_class)) {
	$tmp_class_id = $row["class_id"];
	$tmp_class_nm = $row["class_nm"];
	echo("classes.push({id: $tmp_class_id, name: '$tmp_class_nm'});\n");
}
pg_result_seek($sel_class, 0);
?>

var atrbs = {};
<?
$pre_class_id = "";
while ($row = pg_fetch_array($sel_atrb)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("atrbs[$tmp_class_id] = [];\n");
	}

	echo("atrbs[$tmp_class_id].push({id: $tmp_atrb_id, name: '$tmp_atrb_nm'});\n");

	$pre_class_id = $tmp_class_id;
}
pg_result_seek($sel_atrb, 0);
?>

var depts = {};
<?
$pre_class_id = "";
$pre_atrb_id = "";
while ($row = pg_fetch_array($sel_dept)) {
	$tmp_class_id = $row["class_id"];
	$tmp_atrb_id = $row["atrb_id"];
	$tmp_dept_id = $row["dept_id"];
	$tmp_class_nm = $row["class_nm"];
	$tmp_atrb_nm = $row["atrb_nm"];
	$tmp_dept_nm = $row["dept_nm"];

	if ($tmp_class_id != $pre_class_id) {
		echo("depts[$tmp_class_id] = {};\n");
	}

	if ($tmp_class_id != $pre_class_id || $tmp_atrb_id != $pre_atrb_id) {
		echo("depts[$tmp_class_id][$tmp_atrb_id] = [];\n");
	}

	echo("depts[$tmp_class_id][$tmp_atrb_id].push({id: $tmp_dept_id, name: '$tmp_dept_nm'});\n");

	$pre_class_id = $tmp_class_id;
	$pre_atrb_id = $tmp_atrb_id;
}
pg_result_seek($sel_dept, 0);
?>




function setRefClassSrcOptions(init_flg, default_ref_class_src, default_ref_atrb_src) {
	var ref_class_src = document.getElementById('ref_class_src');

	if (!init_flg) {
		deleteAllOptions(document.getElementById('ref_dept'));
	}
	deleteAllOptions(ref_class_src);

	addOption(ref_class_src, '-', '----------', default_ref_class_src);
	for (var i = 0, len = classes.length; i < len; i++) {
		addOption(ref_class_src, classes[i].id, classes[i].name, default_ref_class_src);
	}

	setRefAtrbSrcOptions(default_ref_atrb_src);
}

function setRefAtrbSrcOptions(default_ref_atrb_src) {
	var ref_atrb_src = document.getElementById('ref_atrb_src');

	deleteAllOptions(document.getElementById('ref_atrb_src'));
	addOption(ref_atrb_src, '-', '----------', default_ref_atrb_src);

	var class_id = document.getElementById('ref_class_src').value;
	if (atrbs[class_id]) {
		for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
			addOption(ref_atrb_src, atrbs[class_id][i].id, atrbs[class_id][i].name, default_ref_atrb_src);
		}
	}

	setRefDeptSrcOptions();
}

function setRefDeptSrcOptions() {
	var ref_dept_src = document.getElementById('ref_dept_src');
	deleteAllOptions(ref_dept_src);

	var class_id = document.getElementById('ref_class_src').value;
	var atrb_id = document.getElementById('ref_atrb_src').value;
	if (depts[class_id]) {
		if (depts[class_id][atrb_id]) {
			for (var i = 0, len = depts[class_id][atrb_id].length; i < len; i++) {
				var dept_id = depts[class_id][atrb_id][i].id;
				var value = class_id + '-' + atrb_id + '-' + dept_id;
				addOption(ref_dept_src, value, depts[class_id][atrb_id][i].name);
			}
		} else if (atrb_id == '-') {
			for (var i = 0, len = atrbs[class_id].length; i < len; i++) {
				var atrb_id = atrbs[class_id][i].id;
				if (depts[class_id][atrb_id]) {
					for (var j = 0, len2 = depts[class_id][atrb_id].length; j < len2; j++) {
						var dept_id = depts[class_id][atrb_id][j].id;
						var value = class_id + '-' + atrb_id + '-' + dept_id;
						addOption(ref_dept_src, value, depts[class_id][atrb_id][j].name);
					}
				}
			}
		}
	}
}

function deleteAllOptions(box)
{
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}


function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function setDisabled()
{
	var disabled;
	disabled = !(!document.getElementById('ref_dept_st_flg').disabled && !document.getElementById('ref_dept_st_flg').checked);

	var ref_dept_flg = document.getElementsByName('ref_dept_flg');
	ref_dept_flg[0].disabled = disabled;
	ref_dept_flg[1].disabled = disabled;

	disabled = !(!ref_dept_flg[1].disabled && ref_dept_flg[1].checked);

	document.getElementById('ref_class_src').disabled = disabled;
	document.getElementById('ref_atrb_src').disabled = disabled;
	document.getElementById('ref_dept_src').disabled = disabled;
	document.getElementById('ref_dept').disabled = disabled;
	document.getElementById('add_ref_dept').disabled = disabled;
	document.getElementById('delete_ref_dept').disabled = disabled;
	document.getElementById('delete_all_ref_dept').disabled = disabled;
	document.getElementById('select_all_ref_dept').disabled = disabled;

	disabled = (document.getElementById('ref_dept_st_flg').disabled || document.getElementById('ref_dept_st_flg').checked);

	var ref_st_flg = document.getElementsByName('ref_st_flg');
	ref_st_flg[0].disabled = disabled;
	ref_st_flg[1].disabled = disabled;

	disabled = !(!ref_st_flg[1].disabled && ref_st_flg[1].checked);
	document.getElementById('ref_st[]').disabled = disabled;
}

function addSelectedOptions(dest_box, src_box) {
	var options = new Array();
	for (var i = 0, j = dest_box.length; i < j; i++) {
		options[dest_box.options[i].value] = dest_box.options[i].text;
	}
	deleteAllOptions(dest_box);
	for (var i = 0, j = src_box.length; i < j; i++) {
		if (src_box.options[i].selected) {
			options[src_box.options[i].value] = src_box.options[i].text;
		}
	}

	for (var i in options) {
		addOption(dest_box, i, options[i]);
	}
}

function deleteSelectedOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		if (box.options[i].selected) {
			box.options[i] = null;
		}
	}
}

function selectAllOptions(box) {
	for (var i = 0, j = box.length; i < j; i++) {
		box.options[i].selected = true;
	}
}

function toggle(button)
{
	var display;
	if (button.innerHTML == '▼') {
		button.innerHTML = '▲';
		display = '';
	} else {
		button.innerHTML = '▼';
		display = 'none';
	}

	document.getElementById('ref_toggle1').style.display = display;
}


function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}


// 登録
function submitForm()
{
	var ref_dept_box = document.lib.ref_dept;
	if (!ref_dept_box.disabled) {
		for (var i = 0, j = ref_dept_box.length; i < j; i++) {
			addHiddenElement(document.lib, 'hid_ref_dept[]', ref_dept_box.options[i].value);
		}
	}

	document.lib.ref_toggle_mode.value = document.getElementById('ref_toggle').innerHTML;

	closeEmployeeList();

	document.lib.submit();

}


function addHiddenElement(frm, name, value) {
	var input = document.createElement('input');
	input.type = 'hidden';
	input.name = name;
	input.value = value;
	frm.appendChild(input);
}


//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
	$script = "m_target_list['1'] = new Array(";
	$is_first = true;
	foreach($arr_target["1"] as $row)
	{
		if($is_first)
		{
			$is_first = false;
		}
		else
		{
			$script .= ",";
		}
		$tmp_emp_id = $row["id"];
		$tmp_emp_name = $row["name"];
		$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
	}
	$script .= ");\n";
	print $script;
?>


</script>

</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">


<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="32" height="32" class="spacing"><a href="fplus_menu.php?session=<? echo($session); ?>"><img src="img/icon/b47.gif" width="32" height="32" border="0" alt="ファントルくん＋"></a></td>
		<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="fplus_menu.php?session=<? echo($session); ?>"><b>ファントルくん＋</b></a> &gt; <a href="fplusadm_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="fplus_menu.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
	</tr>
</table>




<table style="width:100%; margin-top:4px"><tr>
<td style="vertical-align:top; width:120px"><?= fplusadm_common_show_admin_left_menu($session); ?></td>
<td style="vertical-align:top">



	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:2px solid #c488ae; margin-bottom:2px">
		<tr><? show_wkfw_menuitem($session, $fname, array("cate_id" => $src_cate_id, "folder_id" => $src_folder_id)); ?></tr>
	</table>









<form name="lib" action="fplusadm_category_insert.php" method="post">
<table width="740" border="0" cellspacing="0" cellpadding="2" class="list" style="margin-top:6px">

<!-- 保存先　-->
<tr height="22">
<td width="140" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">保存先</font></td>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span id="folder_path" style="padding-right:2px;"></span>
<input type="button" value="選択" onclick="openFolderSetting();">
</font>
</td>
</tr>

<!-- フォルダ　-->
<tr height="22">
<td width="140" align="right" bgcolor="#feeae9"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">フォルダ名</font></td>
<td colspan="2"><input name="category_name" type="text" size="50" maxlength="20" value="<?=$category_name?>" style="ime-mode: active;"></td>
</tr>

<!-- 利用可能範囲の設定　-->
<tr height="22">
<td bgcolor="#feeae9" colspan="3" class="spacing">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<span style="cursor:pointer;" onclick="toggle(document.getElementById('ref_toggle'));">利用可能範囲の設定&nbsp;</span>
<span id="ref_toggle" style="cursor:pointer;" onclick="toggle(this);"><? echo($ref_toggle_mode); ?></span>
</font>
</td>
</tr>
</table>

<div id="ref_toggle1" style="display:<? echo($ref_toggle_display); ?>;">
<table width="740" border="0" cellspacing="0" cellpadding="2" class="list" style="position:relative;top:-1px;">
<tr>
<td width="140" bgcolor="#feeae9" rowspan="2" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署・役職<br><input type="checkbox" name="ref_dept_st_flg" id="ref_dept_st_flg" value="t"<? if ($ref_dept_st_flg == "t") {echo(" checked");} ?> onclick="setDisabled();">許可しない</font></td>
<td width="480" bgcolor="#feeae9" class="spacing" width="400"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">部署</font></td>
<td width="240" bgcolor="#feeae9" class="spacing" width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">役職</font></td>
</tr>

<tr>
<td>
<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="1"<? if ($ref_dept_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_dept_flg" id="ref_dept_flg" value="2"<? if ($ref_dept_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font>
</td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td valign="bottom" style="position:relative;top:3px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">可能とする<? echo($arr_class_name[2]); ?></font></td>
<td></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<select name="ref_class_src" id="ref_class_src" onchange="setRefAtrbSrcOptions();">
</select><? echo($arr_class_name[0]); ?><br>
<select name="ref_atrb_src" id="ref_atrb_src" onchange="setRefDeptSrcOptions();">
</select><? echo($arr_class_name[1]); ?>
</font></td>
</tr>
<tr>
<td>
<select name="ref_dept" id="ref_dept" size="6" multiple style="width:120px;">
	<?
	foreach ($ref_dept as $tmp_dept_id) {
		echo("<option value=\"$tmp_dept_id\">{$dept_names[$tmp_dept_id]}");
	}
	?>
</select>
</td>
<td align="center"><input type="button" name="add_ref_dept" id="add_ref_dept" value=" &lt; " onclick="addSelectedOptions(this.form.ref_dept, this.form.ref_dept_src);"><br><br><input type="button" name="delete_ref_dept" id="delete_ref_dept" value=" &gt; " onclick="deleteSelectedOptions(this.form.ref_dept);"></td>
<td><select name="ref_dept_src" id="ref_dept_src" size="6" multiple style="width:120px;"></select></td>
</tr>
<tr>
<td><input type="button" name="delete_all_ref_dept" id="delete_all_ref_dept" value="全て消去" onclick="deleteAllOptions(this.form.ref_dept);"></td>
<td></td>
<td><input type="button" name="select_all_ref_dept" id="select_all_ref_dept" value="全て選択" onclick="selectAllOptions(this.form.ref_dept_src);"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td>
<table cellspacing="0" cellpadding="0" border="0" class="non_in_list">
<tr>
<td style="padding-right:20px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="ref_st_flg" id="ref_st_flg" value="1"<? if ($ref_st_flg == "1") {echo(" checked");} ?> onclick="setDisabled();">すべて<br>
<input type="radio" name="ref_st_flg" id="ref_st_flg" value="2"<? if ($ref_st_flg == "2") {echo(" checked");} ?> onclick="setDisabled();">指定する
</font></td>
<td>
<table cellspacing="2" cellpadding="0" border="0">
<tr>
<td>
<select name="ref_st[]" id="ref_st[]" size="10" multiple>
<?
while ($row = pg_fetch_array($sel_st)) {
	$tmp_st_id = $row["st_id"];
	$tmp_st_nm = $row["st_nm"];
	echo("<option value=\"$tmp_st_id\"");
	if (in_array($tmp_st_id, $ref_st)) {
		echo(" selected");
	}
	echo(">$tmp_st_nm");
}
pg_result_seek($sel_st, 0);
?>
</select>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr height="22">
<td width="140" bgcolor="#feeae9" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','<?=$emp_id?>','<?=$emp_name?>');"><br></td>
<td colspan="2">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="60" style="border:#E6B3D4 solid 1px;">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

<table width="740" border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td align="right"><input type="button" value="登録" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="src_cate_id" id="src_cate_id" value="<?=$src_cate_id?>">
<input type="hidden" name="src_folder_id" id="src_folder_id" value="<?=$src_folder_id?>">
<input type="hidden" name="dest_cate_id" id="dest_cate_id" value="<?=$dest_cate_id?>">
<input type="hidden" name="dest_folder_id" id="dest_folder_id" value="<?=$dest_folder_id?>">

<input type="hidden" name="ref_toggle_mode" value="">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">

</form>

</td></tr></table>

</td></tr></table>


</body>
</html>
