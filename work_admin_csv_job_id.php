<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?php
// 休日出勤日数出力職種設定
// work_admin_csv_job_id.php
//勤務シフト作成＞管理帳票対応 20130423
//"":検索後月集計CSVダウンロード実行 1:検索画面のボタンから呼ばれた場合
if ($from_flg != "2") {
    $title1 = "出勤表";
    $title2 = "CSV休日出勤日数出力職種設定";
}
//2:勤務シフト作成から
else {
    $title1 = "勤務シフト作成";
    $title2 = "休日出勤日数出力職種設定";
}
?>
<title>CoMedix <? echo($title1." | ".$title2); ?></title>

<?
require_once("about_comedix.php");
require_once("show_attendance_pattern.ini");
require_once("atdbk_menu_common.ini");
require_once("work_admin_csv_common.ini");
require_once("duty_shift_mprint_common_class.php");

///-----------------------------------------------------------------------------
//ページ名
///-----------------------------------------------------------------------------
$fname = $PHP_SELF;
///-----------------------------------------------------------------------------
// セッションのチェック
///-----------------------------------------------------------------------------
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}
///-----------------------------------------------------------------------------
// データベースに接続
///-----------------------------------------------------------------------------
$con = connect2db($fname);
///-----------------------------------------------------------------------------
//初期処理
///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	//初期値設定
	///-----------------------------------------------------------------------------

$arr_job_id_list = array();
//登録時
if ($postback == "1") {
    update_job_id_list($con, $fname, $chk_job_id, $layout_id, $from_flg);
	$arr_job_id_list = $chk_job_id;
} else {
//初期表示時、DBから取得
    $arr_job_id_list = get_job_id_list($con, $fname, $layout_id, $from_flg);
}


// 職種一覧を取得
$sql = "select job_id, job_nm, order_no from jobmst";
$cond = "where job_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 職種一覧を配列に格納
$jobs = array();
$actual_order_no = 1;
while ($row = pg_fetch_array($sel)) {
	$jobs[] = array(
		"id" => $row["job_id"],
		"name" => $row["job_nm"],
		"actual_order_no" => $actual_order_no,
		"order_no" => $row["order_no"]
	);
	$actual_order_no++;
}
//レイアウト名取得
if ($from_flg != "2") {
    $arr_layout = get_layout_name($con, $fname, $layout_id);
    $layout_name = $arr_layout[$layout_id]["name"];
}
else {
    $obj_mprint = new duty_shift_mprint_common_class($con, $fname);
    $arr_layout_mst = $obj_mprint->get_duty_shift_layout_mst_one($layout_id);
    $layout_name = $arr_layout_mst["layout_name"];
}


?>

<!-- ************************************************************************ -->
<!-- JavaScript -->
<!-- ************************************************************************ -->
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function registJobid() {

	document.mainform.postback.value = "1";
	document.mainform.action = "work_admin_csv_job_id.php";
	document.mainform.submit();
}

</script>

<!-- ************************************************************************ -->
<!-- HTML -->
<!-- ************************************************************************ -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr height="32" bgcolor="#5279a5">
		<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>職種の設定</b></font></td>
		<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
		</tr>
	</table>

	<form name="mainform" method="post">
<?
if ($postback == "1") {
?>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
登録しました。
</font>
	<?	
}
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr height="22">
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><br><?
if ($from_flg != "2") {
    echo("レイアウト".$layout_id);
}
else {
    echo("帳票名称");
}        
echo(" ");
echo($layout_name); ?></font></td>
	</tr>
	<tr height="22">
		<td width="20"></td>
		<td colspan="1" align="center">
			<input type="button" value="登録" onclick="registJobid();">&nbsp;&nbsp;
		</td>
	</tr>
<?
for ($i = 0, $j = count($jobs) - 1; $i <= $j; $i++) {
	$tmp_job = $jobs[$i];
	$tmp_job_id = $tmp_job["id"];
	$tmp_job_name = $tmp_job["name"];
	
	$checked = (in_array($tmp_job_id, $arr_job_id_list)) ? " checked" : "";

	echo("<tr height=\"22\">\n");
	echo("<td width=\"20\"></td>\n");
	echo("<td align=\"left\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><label for=\"jobid_$tmp_job_id\"><input type=\"checkbox\" name=\"chk_job_id[]\" id=\"jobid_$tmp_job_id\" value=\"$tmp_job_id\" $checked>$tmp_job_name</label></font></td>\n");
    echo("</tr>\n");
}

?>
	<tr height="22">
		<td width="20"></td>
		<td colspan="1" align="center">
			<input type="button" value="登録" onclick="registJobid();">&nbsp;&nbsp;
		</td>
	</tr>
	</table>

		<!-- ------------------------------------------------------------------------ -->
		<!-- ＨＩＤＤＥＮ -->
		<!-- ------------------------------------------------------------------------ -->
		<input type="hidden" name="session" value="<? echo($session); ?>">
		<input type="hidden" name="postback" value="">
		<input type="hidden" name="layout_id" value="<? echo($layout_id); ?>">
		<input type="hidden" name="from_flg" value="<? echo($from_flg); ?>">
	</form>

</body>
<? pg_close($con); ?>
</html>
<?
//職種ID更新
function update_job_id_list($con, $fname, $chk_job_id, $layout_id, $from_flg) {
    //CSVレイアウト調整
    if ($from_flg != "2") {
        $tablename = "timecard_holwkcnt_job_id";
        $wk_layout_id = "'$layout_id'";
    }
    //帳票定義
    else {
        $tablename = "duty_shift_mprint_holwkcnt_job_id";
        $wk_layout_id = "$layout_id";
    }
    
///-----------------------------------------------------------------------------
// トランザクションを開始
///-----------------------------------------------------------------------------
	pg_query($con, "begin transaction");
	
    $sql = "delete from $tablename";
    $cond = "where layout_id = $wk_layout_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	
	for ($i=0; $i<count($chk_job_id); $i++) {

        $sql = "insert into $tablename (job_id, layout_id";
		$sql .= ") values (";
		$content = array($chk_job_id[$i], $layout_id);
		$ins = insert_into_table($con, $sql, $content, $fname);
		if ($ins == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}

///-----------------------------------------------------------------------------
// トランザクションをコミット
///-----------------------------------------------------------------------------
	pg_query("commit");
}

?>
