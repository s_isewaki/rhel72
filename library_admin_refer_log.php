<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>文書管理 | 参照履歴</title>
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("referer_common.ini");
require_once("library_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 63, $fname);
if ($checkauth == "0") {
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
    exit;
}

function format_datetime($datetime) {
    if (strlen($datetime) == 14) {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/", "$1/$2/$3　$4:$5:$6", $datetime);
    } else {
        return preg_replace("/^(\d{4})(\d{2})(\d{2})$/", "$1/$2/$3", $datetime);
    }
}

// デフォルト値の設定
if ($sort == "") {$sort = "1";}  // 並び順：「累積参照数の多い順」
if ($page == "") {$page = 1;}  // ページ数：1

// データベースに接続
$con = connect2db($fname);

// 遷移元の取得
$referer = get_referer($con, $session, "library", $fname);

// イントラメニュー情報を取得
$sql = "select * from intramenu";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$intra_menu2 = pg_fetch_result($sel, 0, "menu2");
$intra_menu2_3 = pg_fetch_result($sel, 0, "menu2_3");

// 使用可能な書庫一覧を取得
$available_archives = lib_available_archives($con, $fname, false);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteDocument(lib_id) {
    if (!confirm('削除します。よろしいですか？')) {
        return;
    }
    document.delform.elements['did[]'].value = lib_id;
    document.delform.submit();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.list {border-collapse:collapse;}
table.list td {border:#5279a5 solid 1px;}
table.list td td {border-width:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="intra_menu.php?session=<? echo($session); ?>"><img src="img/icon/b38.gif" width="32" height="32" border="0" alt="イントラネット"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="intra_menu.php?session=<? echo($session); ?>"><b>イントラネット</b></a> &gt; <a href="intra_info.php?session=<? echo($session); ?>"><b><? echo($intra_menu2); ?></b></a> &gt; <a href="library_list_all.php?session=<? echo($session); ?>"><b><? echo($intra_menu2_3); ?></b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="library_list_all.php?session=<? echo($session); ?>"><img src="img/icon/b12.gif" width="32" height="32" border="0" alt="文書管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="library_list_all.php?session=<? echo($session); ?>"><b>文書管理</b></a> &gt; <a href="library_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="library_list_all.php?session=<? echo($session); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_admin_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">文書一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#5279a5"><a href="library_admin_refer_log.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>参照履歴</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="75" align="center" bgcolor="#bdd1e7"><a href="library_config.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td width="">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の多い順", "1", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の多い順", "2", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の多い順", "3", $sort, $session); ?></font></td>
</tr>
<tr height="22">
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("累積参照数の少ない順", "4", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近3ヶ月で参照数の少ない順", "5", $sort, $session); ?></font></td>
<td width="33%" align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? show_submenu_label("直近1ヶ月で参照数の少ない順", "6", $sort, $session); ?></font></td>
</tr>
</table>
<? show_library_list($con, $sort, $page, $available_archives, $session, $fname); ?>
</td>
</tr>
</table>
<form name="delform" action="library_delete.php" method="post">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="did[]" value="">
<input type="hidden" name="path" value="2">
<input type="hidden" name="sort" value="<? echo($sort); ?>">
<input type="hidden" name="page" value="<? echo($page); ?>">
</form>
</body>
<? pg_close($con); ?>
</html>
<?
function show_submenu_label($label, $sort, $selected, $session) {
    if ($selected == $sort) {
        echo("<b>");
    } else {
        echo("<a href=\"library_admin_refer_log.php?session=$session&sort=$sort\">");
    }
    echo($label);
    if ($selected == $sort) {
        echo("</b>");
    } else {
        echo("</a>");
    }
}

function show_library_list($con, $sort, $page, $available_archives, $session, $fname) {
    $limit = 15;

    // 文書数を取得（ページング処理のため）
    $sql = "select count(*) from libinfo inner join (select * from libcate where libcate.libcate_del_flg = 'f') libcate on libinfo.lib_cate_id = libcate.lib_cate_id";
    $cond = "where lib_delete_flag = 'f' and lib_id in (select lib_id from libedition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from libedition group by base_lib_id) latest_edition where latest_edition.base_lib_id = libedition.base_lib_id and latest_edition.edition_no = libedition.edition_no))";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $lib_count = intval(pg_fetch_result($sel, 0, 0));
    if ($lib_count == 0) {
        return;
    }

    // 文書情報の取得
    $sql_common = "libinfo.*, (select emp_lt_nm || ' ' || emp_ft_nm from empmst where empmst.emp_id = libinfo.emp_id) as emp_nm, libcate.lib_cate_nm, (select folder_name from libfolder where libfolder.folder_id = libinfo.folder_id) as folder_nm from libinfo inner join (select * from libcate where libcate.libcate_del_flg = 'f') libcate on libinfo.lib_cate_id = libcate.lib_cate_id";
    if ($sort == "1" || $sort == "4") {  // 期間指定なし
        $sql = "select $sql_common";
    } else {  // 期間指定あり
        if ($sort == "2" || $sort == "5") {  // 直近3ヶ月
            $comp_time = date("YmdHi", strtotime("-3 months"));
        } else {
            $comp_time = date("YmdHi", strtotime("-1 month"));
        }
        $sql = "select libreflog.ref_count2, $sql_common left join (select lib_id, count(*) as ref_count2 from libreflog where ref_date >= '$comp_time' group by lib_id) libreflog on libinfo.lib_id = libreflog.lib_id";
    }
    $cond = "where libinfo.lib_delete_flag = 'f' and libinfo.lib_id in (select lib_id from libedition where exists (select * from (select base_lib_id, max(edition_no) as edition_no from libedition group by base_lib_id) latest_edition where latest_edition.base_lib_id = libedition.base_lib_id and latest_edition.edition_no = libedition.edition_no)) ";
    switch ($sort) {
    case "1":  // 累積参照数の多い順
        $cond .= "order by libinfo.ref_count desc, libinfo.lib_up_date desc";
        break;
    case "2":  // 直近3ヶ月で参照数の多い順
        $cond .= "order by coalesce(libreflog.ref_count2, 0) desc, libinfo.lib_up_date desc";
        break;
    case "3":  // 直近1ヶ月で参照数の多い順
        $cond .= "order by coalesce(libreflog.ref_count2, 0) desc, libinfo.lib_up_date desc";
        break;
    case "4":  // 累積参照数の少ない順
        $cond .= "order by libinfo.ref_count, libinfo.lib_up_date";
        break;
    case "5":  // 直近3ヶ月で参照数の少ない順
        $cond .= "order by coalesce(libreflog.ref_count2, 0), libinfo.lib_up_date";
        break;
    case "6":  // 直近1ヶ月で参照数の少ない順
        $cond .= "order by coalesce(libreflog.ref_count2, 0), libinfo.lib_up_date";
        break;
    }
    $offset = $limit * ($page - 1);
    $cond .= " offset $offset limit $limit";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ページ番号の表示
    if ($lib_count > $limit) {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin:5px 0 2px;\">\n");
        echo("<tr>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");

        if ($page > 1) {
            echo("<a href=\"$fname?session=$session&sort=$sort&page=" . ($page - 1) . "\">＜前</a>　\n");
        } else {
            echo("＜前　\n");
        }

        $page_count = ceil($lib_count / $limit);
        for ($i = 1; $i <= $page_count; $i++) {
            if ($i != $page) {
                echo("<a href=\"$fname?session=$session&sort=$sort&page=$i\">$i</a>\n");
            } else {
                echo("<b>$i</b>\n");
            }
        }

        if ($page < $page_count) {
            echo("　<a href=\"$fname?session=$session&sort=$sort&page=" . ($page + 1) . "\">次＞</a>\n");
        } else {
            echo("　次＞\n");
        }

        echo("</font></td>\n");
        echo("</tr>\n");

        echo("</table>\n");
    } else {
        echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"><br>\n");
    }

    // 文書一覧の表示
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\">\n");
    echo("<tr height=\"22\" bgcolor=\"#f6f9ff\">\n");
    echo("<td width=\"22%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">文書名</font></td>\n");
    echo("<td width=\"4%\" align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><nobr>参照数</nobr></font></td>\n");
    echo("<td width=\"12%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">更新日時</font></td>\n");
    echo("<td width=\"11%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">登録者</font></td>\n");
    echo("<td width=\"36%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">保存先</font></td>\n");
    echo("<td width=\"6%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">参照</font></td>\n");
    echo("<td width=\"6%\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">削除</font></td>\n");
    echo("</tr>\n");
    while ($row = pg_fetch_array($sel)) {
        $tmp_lib_id = $row["lib_id"];
        $tmp_lib_nm = $row["lib_nm"];
        if ($sort == "1" || $sort == "4") {
            $tmp_ref_count =  intval($row["ref_count"]);
        } else {
            $tmp_ref_count =  intval($row["ref_count2"]);
        }
        $tmp_lib_up_date = format_datetime($row["lib_up_date"]);
        $tmp_emp_nm = $row["emp_nm"];
        $tmp_lib_archive = get_lib_archive_label($row["lib_archive"]);
        $tmp_lib_cate_nm = $row["lib_cate_nm"];
        $tmp_folder_id = $row["folder_id"];
        $tmp_folder_path = lib_get_folder_path($con, $tmp_folder_id, $fname, $row["lib_archive"]);
        $tmp_lib_url = get_lib_url($row["lib_archive"], $row["lib_cate_id"], $tmp_lib_id, $row["lib_extension"], $session);

        echo("<tr height=\"22\" valign=\"top\">\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_lib_nm</font></td>\n");
        echo("<td align=\"right\" style=\"padding-right:4px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if ($tmp_ref_count > 0) {
            echo("<a href=\"javascript:void(0);\" onclick=\"window.open('library_admin_refer_log_detail.php?session=$session&lib_id=$tmp_lib_id', 'newwin', 'scrollbars=yes,width=640,height=700');\">");
        }
        echo($tmp_ref_count);
        if ($tmp_ref_count > 0) {
            echo("</a>");
        }
        echo("</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><nobr>$tmp_lib_up_date</nobr></font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_nm</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">");
        if (count($available_archives) > 1) {
            echo("$tmp_lib_archive &gt; ");
        }
        echo($tmp_lib_cate_nm);
        lib_write_folder_path($tmp_folder_path, true);
        echo("</font></td>\n");
        if (lib_get_filename_flg() > 0) {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"location.href = '$tmp_lib_url';\"></td>\n");
        } else {
            echo("<td align=\"center\"><input type=\"button\" value=\"参照\" onclick=\"window.open('$tmp_lib_url');\"></td>\n");
        }
        echo("<td align=\"center\"><input type=\"button\" value=\"削除\" onclick=\"deleteDocument('$tmp_lib_id');\"></td>\n");
        echo("</tr>\n");
    }
    echo("</table>\n");
}

function get_lib_url($archive, $cate_id, $lib_id, $ext, $session) {
    $folders = array(
        "1" => "private",
        "2" => "all",
        "3" => "section",
        "4" => "project"
    );
    return "library_refer.php?s={$session}&i={$lib_id}&u=" . urlencode("docArchive/{$folders[$archive]}/cate{$cate_id}/document{$lib_id}.{$ext}");
}
?>
