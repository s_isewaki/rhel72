<?php
// +----------------------------------------------------------------------+
// | PHP version 4, 5                                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2011-2011 Masahiko Jinno                               |
// +----------------------------------------------------------------------+
//
// ファントルくんの発見・発生時間帯の入力方法を管理する関数郡
// 現在発見・発生時間帯の入力方法を「2時間単位選択表示」の選択式か「時間・
// 分選択表示」の二種類が存在する。※ デフォルトは「2時間単位選択表示」

require_once("about_postgres.php");				// DB操作用のソース
require_once("about_session.php");				// セッション管理用のソース
require_once("about_authority.php");			// 権限管理用のソース

/**
    * 発見・発生時間帯の入力方法を設定する
    * 
    * 入力方法の情報は、コンストラクタで受け取っている
    * 
    * $this->postData['two_hour_time_divide']
    *  Type : boolean
    *   → true  : 2時間単位選択表示
    *   → false : 時間・分選択表示
    * 
    * @see getSetting()
    *  
    */
function setTimeSetting($con, $fname, $postData) {
    // *** 使用するローカル変数を宣言 ***
    $two_hour_time_divide = (string)$postData['two_hour_time_divide'] === 'false' ? 'false' : 'true' ;

    // *** レコードの存在チェック ***

    // SQL設定
    $sql = 'select count(*) from inci_time_divide_setting';
    $cond = " ";

    // SQL実行
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0){
        pg_query($con, 'rollback' );
        pg_close($con );
        showErrorPage();
        exit;
    }

    //値を取得
    $row = pg_fetch_assoc($result);

    // レコードが存在しない場合はINSERT
    if ( (int)$row['count'] === 0  ) {
        // INSERT
        $sql = 'INSERT INTO inci_time_divide_setting VALUES (';
        $content = array( pg_escape_string($two_hour_time_divide) );

        $ins = insert_into_table($con, $sql, $content, $fname);

        if ($ins == 0) {
            pg_query($con, "rollback");
            pg_close($con );
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
        
    // レコードが存在する場合はUPDATE
    else {

        // 指定した値で更新
        $sql = "UPDATE inci_time_divide_setting SET ";
        $set = array('two_hour_time_divide');
        $setvalue = array( pg_escape_string($two_hour_time_divide) );
        $cond = " ";

        // SQL実行
        $upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
        if ($upd == 0) {
            pg_close( $con );
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
    }
}


/**
 * 発見・発生時間帯の入力方法を取得する
 * 
 * 入力方法の情報は、コンストラクタで受け取っている
 * 
 * 
 * @return boolean $two_hour_time_divide 
 *  → true  : 2時間単位選択表示
 *  → false : 時間・分選択表示
 * 
 * @see getSetting()
 * @see hiyari_easyinput.php
 * @see hiyari_easyinput_confirm.php
 * @see hiyari_item_timelag_setting_models.php
 * @see hiyari_mail.ini
 * @see hiyari_rm_stats.php
 * @see hiyari_rp_report_list_dl.ini
 *  
 */
function getTimeSetting($con, $fname) {

    // *** 戻り値 ***
    $two_hour_time_divide = true;

    // *** レコードの存在チェック ***

    // SQL設定
    $sql = 'select count(*) from inci_time_divide_setting';
    $cond = " ";

    // SQL実行
    $result = select_from_table($con, $sql, $cond, $fname);
    if ($result == 0){
        pg_query( $con,"rollback" );
        showErrorPage();
        exit;
    }

    //値を取得
    $row = pg_fetch_assoc($result);

    // レコードが存在しない場合はデフォルトを返す
    if ( (int)$row['count'] === 0  ) {
        $two_hour_time_divide = true;
    }

    // レコードが存在する場合は値を返す
    else {
        // SQL設定
        $sql = 'select two_hour_time_divide from inci_time_divide_setting';
        $cond = " ";

        // SQL実行
        $result = select_from_table($con, $sql, $cond, $fname);

        if ($result == 0){
            pg_query($con, 'rollback' );
            pg_close($con );
            showErrorPage();
            exit;
        }

        // 値を取得
        $row = pg_fetch_assoc($result);

        $two_hour_time_divide = $row['two_hour_time_divide'] === 't' ? true : false ;
    }

    return $two_hour_time_divide;
        
}


/**
 * 設定画面（hiyari_item_time_setting.php）に発見・発生日時
 * の入力方法の設定を返す。
 * 
 * @return Array $checked 
 * 
 * デフォルト値 ※ 2時間毎の設定を返す。
 * $checked = [
 *     'true'  => 'checked',
 *     'false' => ''
 * ];
 * 
 *   
 *  → true  : 2時間単位選択表示
 *  → false : 時間・分選択表示
 * 
 * @see file：hiyari_item_time_setting.php
 *  
 */
function getSetting($con, $fname, $postData) {

    // 戻り値（どちらの設定がチェックされているか）の初期化
    // ※デフォルトでは、2時間単位選択表示がチェックされている
    $checked = array(
        'true'   => 'checked',
        'false'  => ''
    );
        
    // 呼び出しの場合の処理
    if ( $postData['is_postback'] !== 'true') {
        $timeSetting = getTimeSetting($con, $fname);
    }

    // 更新をした際の処理
    else if ( $postData['is_postback'] === 'true') {
        setTimeSetting($con, $fname, $postData);
        $timeSetting = getTimeSetting($con, $fname);
    }

    // それ以外の処理 ※ありえない
    else {
        // ログ出力
    }

    // timeSettingがtrueの場合は、'true'に'checkd'を格納(2時間単位選択表示)
    if ($timeSetting === true) {
        $checked = array(
            'true'   => 'checked',
            'false'  => ''
            );
            
    }

    // timeSettingがfalseの場合は、'false'に'checkd'を格納(時間・分選択表示)
    else {
        $checked = array(
            'true'   => '',
            'false'  => 'checked'
            );
    }

    return $checked; 
}


