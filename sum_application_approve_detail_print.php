<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("show_select_values.ini");
require_once("get_menu_label.ini");

//====================================================================
//セッションチェック、権限チェック、DB接続
//====================================================================
$fname = $PHP_SELF;
$session = qualify_session($session,$fname);
$summary_check = @check_authority($session,57,$fname);
$con = @connect2db($fname);
if(!$session || !$summary_check || !$con){
	echo("<html><head><script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script></head></html>");
	exit;
}

// いずれかの階層に実施が含まれているか
$sql =
	" select count(next_notice_recv_div) from sum_applyapv".
	" where apply_id = ".(int)$_REQUEST["apply_id"].
	" and next_notice_recv_div = 2";
$rs = select_from_table($con, $sql, "", $fname);
$next_notice_recv_div_count = (int)pg_fetch_result($rs, 0, 0);


// XMLファイル取得(2010/1まで)
$xml_file = $_REQUEST["emp_id"]."_".$_REQUEST["ptif_id"]."_".$_REQUEST["summary_id"].".xml";
// XMLファイル取得(2010/2から)
if ($_REQUEST["summary_seq"]!=""){
	$sql = "select xml_file_name from sum_xml where xml_file_name = '".$_REQUEST["summary_seq"]."_".$_REQUEST["apply_id"].".xml'";
	$sel_sum_xml = select_from_table($con, $sql, "", $fname);
	$tmp_xml_file_name = @pg_fetch_result($sel_sum_xml, 0, "xml_file_name");
	if ($tmp_xml_file_name!="") $xml_file = $tmp_xml_file_name;
}

// summary_tmpl_read画面の吐き出すHTMLを取得する。(やや危険)
$param =
	"summary_tmpl_read.php?session=".$session.
	"&worksheet_date=".@$_REQUEST["worksheet_date"].
	"&pt_id=".$_REQUEST["ptif_id"].
	"&emp_id=".$_REQUEST["emp_id"].
	"&summary_id=".$_REQUEST["summary_id"].
	"&xml_file=".$xml_file.
	"&tmpl_id=".$_REQUEST["tmpl_id"].
	"&cre_date=".$_REQUEST["apply_date"].
	"&apply_id=".$_REQUEST["apply_id"].
	"&sansho_flg=true".
	"&is_template_readonly=yes";
$subdir = substr($_SERVER["PHP_SELF"], 0, strrpos($_SERVER["PHP_SELF"], "/")+1);
$template_html = explode("\n", file_get_contents("http://".$_SERVER["HTTP_HOST"].$subdir.$param));

// ヘッダ部とボディ部に分割する。(やや危険)
$template_header_html = array();
$template_body_html = array();
$current_row = 0;
for($i = 0; $i < count($template_html); $i++){
	$current_row = $i;
	if (mb_strpos(strtoupper($template_html[$i]), "<HEAD>") !== FALSE) break;
}
for($i = $current_row+1; $i < count($template_html); $i++){
	$current_row = $i;
	if (mb_strpos(strtoupper($template_html[$i]), "END_OF_SUMMARY_TMPL_READ_BODY_TAG") !== FALSE) break;
	$template_header_html[] = $template_html[$i];
}
for($i = $current_row; $i < count($template_html); $i++){
	if (mb_strpos(strtoupper($template_html[$i]), "</BODY>") !== FALSE) continue;
	if (mb_strpos(strtoupper($template_html[$i]), "</HTML>") !== FALSE) continue;
	$template_body_html[] = $template_html[$i];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<script type="text/javascript">

	function copyApproveOrders() {
		opener.updateDOM();

		var self_table = document.getElementById('approve_ordersA');
		if (self_table){
			var opener_rows = opener.document.getElementById('approve_ordersA').rows;
			for (var i = 0, j = opener_rows.length; i < j; i++) {
				var row = self_table.insertRow(-1);
				row.style.backgroundColor = opener_rows[i].style.backgroundColor;
				row.style.height = opener_rows[i].style.height;
				row.style.textAlign = 'center';

				var opener_cells = opener_rows[i].cells;
				for (var m = 0, n = opener_cells.length; m < n; m++) {
					var cell = row.insertCell(-1);
					cell.style.width = '20%';
					cell.innerHTML = opener_cells[m].innerHTML;
				}
			}
		}

		var self_table = document.getElementById('approve_ordersB');
		if (self_table){
			var opener_rows = opener.document.getElementById('approve_ordersB').rows;
			for (var i = 0, j = opener_rows.length; i < j; i++) {
				var row = self_table.insertRow(-1);
				row.style.backgroundColor = opener_rows[i].style.backgroundColor;
				row.style.height = opener_rows[i].style.height;
				row.style.textAlign = 'center';

				var opener_cells = opener_rows[i].cells;
				for (var m = 0, n = opener_cells.length; m < n; m++) {
					var cell = row.insertCell(-1);
					cell.style.width = '20%';
					cell.style.fontSize = "13px";
					cell.innerHTML = opener_cells[m].innerHTML;
				}
			}
		}
	}
	function template_print_init(){
		var body = document.getElementsByTagName("body");
		if (!body) return;

		var inputs = document.getElementsByTagName("input");
		for (var idx in inputs){
			if (!inputs[idx].style) continue;
			if (inputs[idx].type == "button" || inputs[idx].type == "reset" || inputs[idx].type == "submit") {
				inputs[idx].style.visibility = "hidden";
			}
		}

		var imgs = document.getElementsByTagName("img");
		for (var idx in imgs){
			if (!imgs[idx].src) continue;
			if (imgs[idx].src.indexOf("calendar_link.gif") < 0) continue;
			imgs[idx].style.visibility = "hidden";
		}

		var buttons = document.getElementsByTagName("button");
		for (var idx in buttons){
			if (!buttons[idx].style) continue;
			buttons[idx].style.visibility = "hidden";
		}

		body[0].style.backgroundColor = "#ffffff";
		copyApproveOrders();
		document.getElementById("div_apply_status").innerHTML = opener.document.getElementById("div_apply_status").innerHTML;
		document.getElementById("td_sousin_syomei").innerHTML = opener.document.getElementById("td_sousin_syomei").innerHTML;
		document.getElementById("td_sousin_bi").innerHTML = opener.document.getElementById("td_sousin_bi").innerHTML;
		document.getElementById("td_order_bangou").innerHTML = opener.document.getElementById("td_order_bangou").innerHTML;
		document.getElementById("td_template_name").innerHTML = opener.document.getElementById("td_template_name").innerHTML;
		document.getElementById("td_sousin_sya").innerHTML = opener.document.getElementById("td_sousin_sya").innerHTML;
		document.getElementById("td_syozoku").innerHTML = opener.document.getElementById("td_syozoku").innerHTML;
		document.getElementById("td_kanja_id").innerHTML = opener.document.getElementById("td_kanja_id").innerHTML;
		document.getElementById("td_kanjamei").innerHTML = opener.document.getElementById("td_kanjamei").innerHTML;
		if (opener.document.getElementById("other_comment")){
			document.getElementById("other_comment").innerHTML = opener.document.getElementById("other_comment").innerHTML;
		}
		if (opener.document.getElementById("apv_comment")){
			document.getElementById("apv_comment").innerHTML = opener.document.getElementById("apv_comment").innerHTML;
		}
		document.getElementById("attach_files").innerHTML = opener.document.getElementById("print_attach_files").innerHTML;
		document.getElementById("wkfw_appr_info").innerHTML = opener.document.getElementById("wkfw_appr_info").innerHTML;
		print();
		close();
	}
</script>
<title><?=get_report_menu_label($con, $fname)?> | 受信オーダ詳細印刷</title>
<style type="text/css">
	.print_table {border-collapse:collapse; width:100%; }
	.print_table td { border:#aaa solid 1px; padding:3px; font-size:13px; }
	font { font-size:13px }
</style>
<?= implode("\n", $template_header_html) ?><?//←この変数の中に、ヘッダとBODYタグが含まれています。?>
<?// よって、この画面では</HEAD>終了タグ、<BODY>開始タグは定義しません。 ?>
	<table class="print_table" style="white-space:nowrap;">
		<col width="160"/><col width="140"/><col width="100"/><col width="120"/><col width="100"/><col width="160"/>
		<tr>
			<td align="right" bgcolor="#f6f9ff">送信書名</td><td colspan="3" id="td_sousin_syomei"></td>
			<td align="right" bgcolor="#f6f9ff">送信日</td><td id="td_sousin_bi"></td>
		</tr>
		<tr>
			<td align="right" bgcolor="#f6f9ff">オーダ番号</td><td id="td_order_bangou"></td>
			<td align="right" bgcolor="#f6f9ff">テンプレート名</td><td colspan="3" id="td_template_name"></td>
		</tr>
		<tr>
			<td align="right" bgcolor="#f6f9ff">送信者</td><td id="td_sousin_sya"></td>
			<td align="right" bgcolor="#f6f9ff">所属</td><td colspan="3" id="td_syozoku"></td>
		</tr>
		<tr>
			<td align="right" bgcolor="#f6f9ff">患者ID</td><td id="td_kanja_id"></td>
			<td align="right" bgcolor="#f6f9ff">患者名</td><td colspan="3" id="td_kanjamei"></td>
		</tr>

		<!-- 送信済・受付済・実施済状態 -->
		<tr>
			<td align="right" bgcolor="#f6f9ff" colspan="6">
				<div style="padding:4px; font-size:12px; text-align:right;" id="div_apply_status"></div>
			</td>
		</tr>
	</table>

	<!-- 本文 -->
	<div>
		<?= implode("\n", $template_body_html) ?>
	</div>

	<!-- 添付ファイル -->
	<table class="print_table" style="white-space:nowrap;">
	<tr>
	<td align="right" bgcolor="#f6f9ff" width="120px">添付ファイル</td>
	<td id="attach_files"></td>
	</tr>

	<!-- コメント -->
	<tr>
		<td align="right" bgcolor="#f6f9ff">コメント</td>
		<td colspan="5" style="font-size:13px">
			<div id="other_comment"></div>
			<div id="apv_comment"></div>
		</td>
	</tr>

</table>

<div style="margin:3px 0px; font-size:13px" id="wkfw_appr_info"></div>

<div style="text-align:left; border:1px solid #aaa; border-bottom:0; padding:3px; font-size:13px">受信状態</div>
<table id="approve_ordersA" class="print_table"></table>

<? if ($next_notice_recv_div_count > 0){ ?>
	<div style="text-align:left; border:1px solid #aaa; border-bottom:0; padding:3px; margin-top:10px; font-size:13px">実施状態</div>
	<table id="approve_ordersB" class="print_table"></table>
<? } ?>

</body>
<? pg_close($con); ?>
</html>
