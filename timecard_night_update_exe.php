<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}
// system_config
$conf = new Cmx_SystemConfig();
$duty_or_oncall_flg = $conf->get('timecard.duty_or_oncall_flg');
if ($duty_or_oncall_flg == "") {
	$duty_or_oncall_flg = "1";
}
$duty_str = ($duty_or_oncall_flg == "1") ? "当直" : "待機";

//当直＜月〜金＞入力チェック
if(empty($night_start_time_hour) == false && empty($night_start_time_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の開始時刻(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_start_time_hour) && empty($night_start_time_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の開始時刻(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_hour) == false && empty($night_end_time_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の終了時刻１(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_hour) && empty($night_end_time_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の終了時刻１(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_hour) == false && empty($night_end_time2_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の終了時刻２(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_hour) && empty($night_end_time2_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の終了時刻２(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_hour) == false && empty($night_start_time_min) == false) && 
	(empty($night_end_time_hour) || empty($night_end_time_min)) && (empty($night_end_time2_hour) || empty($night_end_time2_min))
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の終了時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_hour) || empty($night_start_time_min)) && 
	(
		(empty($night_end_time_hour) == false && empty($night_end_time_min) == false) ||
		(empty($night_end_time2_hour) == false && empty($night_end_time2_min) == false)
	)
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の開始時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//当直＜土＞入力チェック
if(empty($night_start_time_sat_hour) == false && empty($night_start_time_sat_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の開始時刻(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_start_time_sat_hour) && empty($night_start_time_sat_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の開始時刻(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_sat_hour) == false && empty($night_end_time_sat_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の終了時刻１(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_sat_hour) && empty($night_end_time_sat_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の終了時刻１(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_sat_hour) == false && empty($night_end_time2_sat_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の終了時刻２(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_sat_hour) && empty($night_end_time2_sat_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の終了時刻２(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_sat_hour) == false && empty($night_start_time_sat_min) == false) && 
	(empty($night_end_time_sat_hour) || empty($night_end_time_sat_min)) && 
	(empty($night_end_time2_sat_hour) || empty($night_end_time2_sat_min))
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の終了時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_sat_hour) || empty($night_start_time_sat_min)) && 
	(
		(empty($night_end_time_sat_hour) == false && empty($night_end_time_sat_min) == false) ||
		(empty($night_end_time2_sat_hour) == false && empty($night_end_time2_sat_min) == false)
	)
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の開始時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

//当直＜日＞入力チェック
if(empty($night_start_time_hol_hour) == false && empty($night_start_time_hol_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の開始時刻(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_start_time_hol_hour) && empty($night_start_time_hol_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の開始時刻(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_hol_hour) == false && empty($night_end_time_hol_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の終了時刻(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time_hol_hour) && empty($night_end_time_hol_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の終了時刻(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_hol_hour) == false && empty($night_end_time2_hol_min)){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の終了時刻２(分)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if(empty($night_end_time2_hol_hour) && empty($night_end_time2_hol_min) == false){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の終了時刻２(時)が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_hol_hour) == false && empty($night_start_time_hol_min) == false) && 
	(empty($night_end_time_hol_hour) || empty($night_end_time_hol_min)) && 
	(empty($night_end_time2_hol_hour) || empty($night_end_time2_hol_min))
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の終了時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

if((empty($night_start_time_hol_hour) || empty($night_start_time_hol_min)) && 
	(
		(empty($night_end_time_hol_hour) == false && empty($night_end_time_hol_min) == false) ||
		(empty($night_end_time2_hol_hour) == false && empty($night_end_time2_hol_min) == false)
	)
){
	echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の開始時刻が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}


//＜月〜金＞
$night_start_time   = $night_start_time_hour.$night_start_time_min;
$night_end_time     = $night_end_time_hour.$night_end_time_min;
$night_end_time2    = $night_end_time2_hour.$night_end_time2_min;

//時間の入力がない翌日フラグは落す
if (empty($night_start_time) || empty($night_start_next_day_flag)){
	$night_start_next_day_flag = 0;
}
if (empty($night_end_time) || empty($night_end_next_day_flag)){
	$night_end_next_day_flag = 0;
}
if (empty($night_end_time2) || empty($night_end_next_day_flag2)){
	$night_end_next_day_flag2 = 0;
}

//＜土＞
$night_start_time_sat   = $night_start_time_sat_hour.$night_start_time_sat_min;
$night_end_time_sat     = $night_end_time_sat_hour.$night_end_time_sat_min;
$night_end_time2_sat    = $night_end_time2_sat_hour.$night_end_time2_sat_min;

//時間の入力がない翌日フラグは落す
if (empty($night_start_time_sat) || empty($night_start_next_day_flag_sat)){
	$night_start_next_day_flag_sat = 0;
}
if (empty($night_end_time_sat) || empty($night_end_next_day_flag_sat)){
	$night_end_next_day_flag_sat = 0;
}
if (empty($night_end_time2_sat) || empty($night_end_next_day_flag2_sat)){
	$night_end_next_day_flag2_sat = 0;
}

//＜日＞
$night_start_time_hol   = $night_start_time_hol_hour.$night_start_time_hol_min;
$night_end_time_hol     = $night_end_time_hol_hour.$night_end_time_hol_min;
$night_end_time2_hol    = $night_end_time2_hol_hour.$night_end_time2_hol_min;

//時間の入力がない翌日フラグは落す
if (empty($night_start_time_hol) || empty($night_start_next_day_flag_hol)){
	$night_start_next_day_flag_hol = 0;
}
if (empty($night_end_time_hol) || empty($night_end_next_day_flag_hol)){
	$night_end_next_day_flag_hol = 0;
}
if (empty($night_end_time2_hol) || empty($night_end_next_day_flag2_hol)){
	$night_end_next_day_flag2_hol = 0;
}

if ($night_allowance == "") {
	$night_allowance = null;
} else {
	if (!preg_match("/^[0-9]*$/", $night_allowance) )
	{
		echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜月〜金＞の手当金額は数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

if ($night_allowance_sat == "") {
	$night_allowance_sat = null;
} else {
	if (!preg_match("/^[0-9]*$/", $night_allowance_sat) )
	{
		echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜土＞の手当金額は数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

if ($night_allowance_hol == "") {
	$night_allowance_hol = null;
} else {
	if (!preg_match("/^[0-9]*$/", $night_allowance_hol) )
	{
		echo("<script type=\"text/javascript\">alert('{$duty_str}設定＜日＞の手当金額は数値を入力してください。');</script>");
		echo("<script language=\"javascript\">history.back();</script>");
		exit;
	}
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin transaction");

// タイムカードレコードがなければ作成、あれば更新
$sql = "select count(*) from timecard";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
if (pg_fetch_result($sel, 0, 0) == 0) {
	$sql = "insert into timecard (".
				"night_start_time, night_start_next_day_flag, night_end_time, night_end_next_day_flag, night_end_time2, night_end_next_day_flag2, night_workday_count, night_workday_count2, ".
				"night_start_time_sat, night_start_next_day_flag_sat, night_end_time_sat, night_end_next_day_flag_sat, night_end_time2_sat, night_end_next_day_flag2_sat, night_workday_count_sat, night_workday_count2_sat, ".
				"night_start_time_hol, night_start_next_day_flag_hol, night_end_time_hol, night_end_next_day_flag_hol, night_end_time2_hol, night_end_next_day_flag2_hol, night_workday_count_hol, night_workday_count2_hol, ".
				"night_allowance, night_allowance_sat, night_allowance_hol) values (";
	$content = array(
				$night_start_time, $night_start_next_day_flag, $night_end_time, $night_end_next_day_flag, $night_end_time2, $night_end_next_day_flag2, $night_workday_count, $night_workday_count2,
				$night_start_time_sat, $night_start_next_day_flag_sat, $night_end_time_sat, $night_end_next_day_flag_sat, $night_end_time2_sat, $night_end_next_day_flag2_sat, $night_workday_count_sat, $night_workday_count2_sat,
				$night_start_time_hol, $night_start_next_day_flag_hol, $night_end_time_hol, $night_end_next_day_flag_hol, $night_end_time2_hol, $night_end_next_day_flag2_hol, $night_workday_count_hol, $night_workday_count2_hol,
				$night_allowance, $night_allowance_sat, $night_allowance_hol
	);
	$ins = insert_into_table($con, $sql, $content, $fname);
	if ($ins == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {
	$sql = "update timecard set";
	$set = array(
			"night_start_time", "night_start_next_day_flag", "night_end_time", "night_end_next_day_flag", "night_end_time2", "night_end_next_day_flag2", "night_workday_count", "night_workday_count2",
			"night_start_time_sat", "night_start_next_day_flag_sat", "night_end_time_sat", "night_end_next_day_flag_sat", "night_end_time2_sat", "night_end_next_day_flag2_sat", "night_workday_count_sat", "night_workday_count2_sat",
			"night_start_time_hol", "night_start_next_day_flag_hol", "night_end_time_hol", "night_end_next_day_flag_hol", "night_end_time2_hol", "night_end_next_day_flag2_hol", "night_workday_count_hol", "night_workday_count2_hol",
			"night_allowance", "night_allowance_sat", "night_allowance_hol"
	);
	$setvalue = array(
			$night_start_time, $night_start_next_day_flag, $night_end_time, $night_end_next_day_flag, $night_end_time2, $night_end_next_day_flag2, $night_workday_count, $night_workday_count2,
			$night_start_time_sat, $night_start_next_day_flag_sat, $night_end_time_sat, $night_end_next_day_flag_sat, $night_end_time2_sat, $night_end_next_day_flag2_sat, $night_workday_count_sat, $night_workday_count2_sat,
			$night_start_time_hol, $night_start_next_day_flag_hol, $night_end_time_hol, $night_end_next_day_flag_hol, $night_end_time2_hol, $night_end_next_day_flag2_hol, $night_workday_count_hol, $night_workday_count2_hol,
			$night_allowance, $night_allowance_sat, $night_allowance_hol
	);
	$cond = "";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query("commit");

// データベース接続を閉じる
pg_close($con);

// 端数処理画面を再表示
echo("<script type=\"text/javascript\">location.href = 'timecard_night.php?session=$session';</script>");
?>
