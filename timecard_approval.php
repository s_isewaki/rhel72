<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 勤務管理 | 承認者</title>
<?
require_once("Cmx.php");
require_once('Cmx/Model/SystemConfig.php');
require_once("about_comedix.php");
require_once("show_class_name.ini");
require_once("referer_common.ini");
require_once("work_admin_menu_common.ini");
require_once("atdbk_workflow_common_class.php");
require_once("timecard_bean.php");
require_once("atdbk_menu_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 42, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);
//タイムカード情報の取得
$timecard_bean = new timecard_bean();
$timecard_bean->select($con, $fname);
$ret_str = ($timecard_bean->return_icon_flg != "2") ? "退勤後復帰" : "呼出勤務";

// 遷移元の取得
$referer = get_referer($con, $session, "work", $fname);

// system_config
$conf = new Cmx_SystemConfig();
// 承認階層開始判定
$aprv_skip_flg = $conf->get('timecard.aprv_skip_flg');
if ($aprv_skip_flg == "") {
	$aprv_skip_flg = "f";
}

// 初期表示時は承認者情報を取得
	$sql = "select emp_id, emp_lt_nm, emp_ft_nm from empmst";
	$cond = "where emp_id in (select emp_id from tmcdaprv) order by emp_id";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$target_id_list1 = "";
	$is_first_flg = true;
	$arr_target["1"] = array();
	while ($row = pg_fetch_array($sel)) {
		$tmp_emp_id = $row["emp_id"];
		$tmp_emp_nm = $row["emp_lt_nm"] . " " . $row["emp_ft_nm"];
		if ($is_first_flg) {
			$is_first_flg = false;
		} else {
			$target_id_list1 .= ",";
		}
		$target_id_list1 .= $tmp_emp_id;
		array_push($arr_target["1"], array("id" => $tmp_emp_id, "name" => $tmp_emp_nm));
	}

// 承認階層情報取得
if ($back != "t") {

	// 承認者ワークフロー情報取得
	$sql = "select * from atdbk_wkfw";
	$cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ovtm_wkfw_div = pg_fetch_result($sel, 0, "ovtm_wkfw_div");
	$ovtm_wkfw_approve_num = pg_fetch_result($sel, 0, "ovtmcfm");

	$time_wkfw_div = pg_fetch_result($sel, 0, "time_wkfw_div");
	$time_wkfw_approve_num = pg_fetch_result($sel, 0, "timecfm");

	$rtn_wkfw_div = pg_fetch_result($sel, 0, "rtn_wkfw_div");
	$rtn_wkfw_approve_num = pg_fetch_result($sel, 0, "rtncfm");

    //////////////////////////////////////////////////////////////////
    // 残業申請ワークフロー設定取得
    //////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 1 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$ovtm_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $ovtm_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "ovtm_apv_div0_flg$i";
		$apv_div1_flg = "ovtm_apv_div1_flg$i";
		$apv_div4_flg = "ovtm_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "1", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "1", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "1", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "ovtm_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "ovtm_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "ovtm_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "ovtm_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "ovtm_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "ovtm_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "ovtm_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "ovtm_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "ovtm_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "ovtm_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "ovtm_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "ovtm_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "ovtm_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));
	}


    //////////////////////////////////////////////////////////////////
    // 勤務時間修正申請ワークフロー設定取得
    //////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 2 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$time_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $time_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "time_apv_div0_flg$i";
		$apv_div1_flg = "time_apv_div1_flg$i";
		$apv_div4_flg = "time_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "2", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "2", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "2", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "time_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "time_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "time_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "time_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "time_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "time_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "time_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "time_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "time_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "time_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "time_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "time_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "time_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));

	}

    //////////////////////////////////////////////////////////////////
    // 退勤後復帰申請ワークフロー設定取得
    //////////////////////////////////////////////////////////////////
	$sql = "select * from atdbk_apvmng";
	$cond = "where wkfw_div = 3 order by apv_order";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	$rtn_approve_num = pg_numrows($sel);
	for ($i = 1; $i <= $rtn_approve_num; $i++) {

		$ret_arr = array();

		// 承認者指定区分
		$apv_div0_flg = "rtn_apv_div0_flg$i";
		$apv_div1_flg = "rtn_apv_div1_flg$i";
		$apv_div4_flg = "rtn_apv_div4_flg$i";

		$$apv_div0_flg .= pg_fetch_result($sel, $i - 1, "apv_div0_flg");
		$$apv_div1_flg .= pg_fetch_result($sel, $i - 1, "apv_div1_flg");
		$$apv_div4_flg .= pg_fetch_result($sel, $i - 1, "apv_div4_flg");

		$apv_order = pg_fetch_result($sel, $i - 1, "apv_order");

		// 部署役職(申請書所属)
		if(pg_fetch_result($sel, $i - 1, "apv_div0_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, "3", $ret_arr);
		}

		// 部署役職(部署指定)
		if(pg_fetch_result($sel, $i - 1, "apv_div4_flg") == "t")
		{
			search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, "3", $ret_arr);
		}

		// 職員
		if(pg_fetch_result($sel, $i - 1, "apv_div1_flg") == "t")
		{
			search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, "3", $ret_arr);
		}

		$j = 0;
		foreach ($ret_arr as $row)
		{

			// 承認者欄(表示用)
			$var_name = "rtn_approve$i";

			if($j > 0)
			{
				$$var_name .= "<BR>";
			}

			$$var_name .= $row["approve"];
			$j++;

			// 部署区分：「1」部門　「2」課　「3」科　「4」室 「0」部署指定なし
			$var_name = "rtn_target_class_div$i";
			$$var_name .= $row["target_class_div"];
			// 役職ＩＤ
			$var_name = "rtn_st_id$i";
			$$var_name .= $row["st_id"];
			// 承認者ＩＤ
			$var_name = "rtn_emp_id$i";
			$$var_name .= $row["emp_id"];
			// 承認者名
			$var_name = "rtn_emp_nm$i";
			$$var_name .= $row["emp_nm"];
			// 承認者削除フラグ
			$var_name = "rtn_emp_del_flg$i";
			$$var_name .= $row["emp_del_flg"];

			// 部門ＩＤ
			$var_name = "rtn_class_sect_id$i";
			$$var_name .= $row["class_sect_id"];

			// 課ＩＤ
			$var_name = "rtn_atrb_sect_id$i";
			$$var_name .= $row["atrb_sect_id"];

			// 科ＩＤ
			$var_name = "rtn_dept_sect_id$i";
			$$var_name .= $row["dept_sect_id"];

			// 室ＩＤ
			$var_name = "rtn_room_sect_id$i";
			$$var_name .= $row["room_sect_id"];

			// 役職ＩＤ
			$var_name = "rtn_st_sect_id$i";
			$$var_name .= $row["st_sect_id"];

		}

		$var_name = "rtn_multi_apv_flg$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "multi_apv_flg"));

		$var_name = "rtn_next_notice_div$i";
		$$var_name = (pg_fetch_result($sel, $i - 1, "next_notice_div"));
	}

}

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<?
require("emplist_caller_javascript.php");
insert_javascript();
?>
<script type="text/javascript">
var childwin = null;
function openEmployeeList(item_id) {
	dx = screen.width;
	dy = screen.top;
	base = 0;
	wx = 720;
	wy = 600;
	var url = './emplist_popup.php';
	url += '?session=<?=$session?>';
	url += '&emp_id=<?=$emp_id?>';
	url += '&mode=10';
	url += '&item_id='+item_id;
	childwin = window.open(url, 'emplistpopup', 'left='+(dx-wx)+',top='+base+',width='+wx+',height='+wy+',scrollbars=yes,resizable=yes');

	childwin.focus();
}

function closeEmployeeList() {
	if (childwin != null && !childwin.closed) {
		childwin.close();
	}
	childwin = null;
}

//--------------------------------------------------
//登録対象者一覧
//--------------------------------------------------
<?
	for ($i=1; $i<=1; $i++) {
		$script = "m_target_list['$i'] = new Array(";
		$is_first = true;
		foreach($arr_target["$i"] as $row)
		{
			if($is_first)
			{
				$is_first = false;
			}
			else
			{
				$script .= ",";
			}
			$tmp_emp_id = $row["id"];
			$tmp_emp_name = $row["name"];
			$script .= "new user_info('$tmp_emp_id','$tmp_emp_name')";
		}
		$script .= ");\n";
		print $script;
	}
?>

// クリア
function clear_target(item_id, emp_id,emp_name) {
	if(confirm("登録対象者を削除します。よろしいですか？"))
	{
		var is_exist_flg = false;
		for(var i=0;i<m_target_list[item_id].length;i++)
		{
			if(emp_id == m_target_list[item_id][i].emp_id)
			{
				is_exist_flg = true;
				break;
			}
		}
		m_target_list[item_id] = new Array();
		if (is_exist_flg == true) {
			m_target_list[item_id] = array_add(m_target_list[item_id],new user_info(emp_id,emp_name));
		}
		update_target_html(item_id);
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function selectAllOptions(box) {
	for (var i = 0, j = box.options.length; i < j; i++) {
		box.options[i].selected = true;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	try {box.style.fontSize = 'auto';} catch (e) {}
	box.style.overflow = 'auto';
}

function submitForm() {
	var frm = document.getElementById("updform");

	closeEmployeeList();

	frm.submit();
}

function initPage() {

	//登録対象者を設定する。
	update_target_html("1");
}


// ワークフローチェックボックスによる活性・非活性制御
function setWorkflowDisable(id, approve_num)
{
    if(document.getElementById(id + '_wkfw_div').checked)
    {
		document.getElementById(id + '_kaisofont').color = "";
    	document.getElementById(id + '_wkfwtbl').disabled = false;
        document.getElementById(id + '_approve_num').disabled = false;
    	document.getElementById(id + '_wkfwfont').color = "";
        document.getElementById(id + '_wkfw_approve_num').disabled = false;

        for(var i=1; i<=approve_num; i++)
        {
			document.getElementById(id + '_kaisofont' + i).color = "";
			document.getElementById(id + '_approve_content_font' + i).color = "green";
			document.getElementById(id + '_kaisou' + i).disabled = false;
			document.getElementById(id + '_kaisou' + i).onclick = function(){window.open('timecard_approval_list.php?session=<?=$session?>&approve=<?=$j?>&wkfw_nm=' + id, 'newwin2', 'width=640,height=700,scrollbars=yes')};
        }
	}
	else
	{
		document.getElementById(id + '_kaisofont').color = "#a9a9a9";
    	document.getElementById(id + '_wkfwtbl').disabled = true;
        document.getElementById(id + '_approve_num').disabled = true;
    	document.getElementById(id + '_wkfwfont').color = "#a9a9a9";
        document.getElementById(id + '_wkfw_approve_num').disabled = true;

        for(var i=1; i<=approve_num; i++)
        {
			document.getElementById(id + '_kaisofont' + i).color = "#a9a9a9";
			document.getElementById(id + '_approve_content_font' + i).color = "#a9a9a9";
			document.getElementById(id + '_kaisou' + i).disabled = true;
			document.getElementById(id + '_kaisou' + i).onclick = null;
        }
	}
}


function checkApprove(id)
{
	document.updform.action = "timecard_approval.php";
	document.updform.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse; border:#5279a5 solid 1px;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.non_in_list {border-collapse:collapse;}
.non_in_list td {border:0px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<? if ($referer == "2") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="work_admin_menu.php?session=<? echo($session); ?>"><img src="img/icon/b23.gif" width="32" height="32" border="0" alt="勤務管理"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="work_admin_menu.php?session=<? echo($session); ?>"><b>勤務管理</b></a></font></td>
</tr>
</table>
<? } else { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><img src="img/icon/b07.gif" width="32" height="32" border="0" alt="出勤表"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>出勤表</b></a> &gt; <a href="work_admin_menu.php?session=<? echo($session); ?>"><b>管理画面</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="<?php echo(atdbk_menu_default($con, $fname, $session)); ?>"><b>ユーザ画面へ</b></a></font></td>
</tr>
</table>
<? } ?>
<?
// メニュータブ
show_work_admin_menuitem($session, $fname, "");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form id="updform" name="updform" action="timecard_approval_update_exe.php" method="post">
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td valign="bottom">
<font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認者グループから本人が選択する</font>
</td>
<td align="right"><input type="button" value="更新" onclick="submitForm();"></td>
</tr>
</table>

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">

<tr>
<td width="160" bgcolor="#f6f9ff"><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間修正申請および<br>残業申請を決裁可能な職員</font><br>
<input type="button" name="emplist1" value="職員名簿" style="margin-left:2em;width=5.5em;" onclick="openEmployeeList('1');"><br>
<input type="button" name="clear" value="クリア" style="margin-left:2em;width=5.5em;" onclick="clear_target('1','','');"><br></td>
<td>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" class="non_in_list">
<tr>
<td width="350" height="80" style="border:#5279a5 solid 1px;">

<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<span id="target_disp_area1"></span>
</font>

</td>
</tr>
</table>

</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="1" height="5" alt=""></td>
</tr>
</table>

<table width="600" border="0" cellspacing="0" cellpadding="2" class="list">
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認階層開始判定</font></td>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<label><input type="radio" name="aprv_skip_flg" value="f"<? if ($aprv_skip_flg == "f") {echo(" checked");} ?>>1階層目から</label>
<label><input type="radio" name="aprv_skip_flg" value="t"<? if ($aprv_skip_flg == "t") {echo(" checked");} ?>>申請者が承認者に含まれる階層の次階層から</label>
</font></td>
</tr>
</table>

<!-- 残業申請ワークフロー start -->
<label><input type="checkbox" name="ovtm_wkfw_div" id="ovtm_wkfw_div" value="1" onclick="setWorkflowDisable('ovtm', '<?=$ovtm_approve_num?>')" <? if($ovtm_wkfw_div == "1"){echo("checked");} ?>/><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">残業申請のワークフローを設定する</font></label>
<table id="ovtm_wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if($ovtm_wkfw_div != "1"){echo("disabled");} ?>>
<!-- 承認階層数 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="ovtm_kaisofont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($ovtm_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>>承認階層数</font></td>
<td colspan="2"><select name="ovtm_approve_num" id="ovtm_approve_num" onChange="checkApprove('ovtm');" <? if($ovtm_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $ovtm_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
<?
$ovtm_approve = array($ovtm_approve1, $ovtm_approve2, $ovtm_approve3, $ovtm_approve4, $ovtm_approve5, $ovtm_approve6, $ovtm_approve7, $ovtm_approve8, $ovtm_approve9, $ovtm_approve10, $ovtm_approve11, $ovtm_approve12, $ovtm_approve13, $ovtm_approve14, $ovtm_approve15, $ovtm_approve16, $ovtm_approve17, $ovtm_approve18, $ovtm_approve19, $ovtm_approve20);

$ovtm_apv_div0_flg = array($ovtm_apv_div0_flg1, $ovtm_apv_div0_flg2, $ovtm_apv_div0_flg3, $ovtm_apv_div0_flg4, $ovtm_apv_div0_flg5, $ovtm_apv_div0_flg6, $ovtm_apv_div0_flg7, $ovtm_apv_div0_flg8, $ovtm_apv_div0_flg9, $ovtm_apv_div0_flg10, $ovtm_apv_div0_flg11, $ovtm_apv_div0_flg12, $ovtm_apv_div0_flg13, $ovtm_apv_div0_flg14, $ovtm_apv_div0_flg15, $ovtm_apv_div0_flg16, $ovtm_apv_div0_flg17, $ovtm_apv_div0_flg18, $ovtm_apv_div0_flg19, $ovtm_apv_div0_flg20);
$ovtm_apv_div1_flg = array($ovtm_apv_div1_flg1, $ovtm_apv_div1_flg2, $ovtm_apv_div1_flg3, $ovtm_apv_div1_flg4, $ovtm_apv_div1_flg5, $ovtm_apv_div1_flg6, $ovtm_apv_div1_flg7, $ovtm_apv_div1_flg8, $ovtm_apv_div1_flg9, $ovtm_apv_div1_flg10, $ovtm_apv_div1_flg11, $ovtm_apv_div1_flg12, $ovtm_apv_div1_flg13, $ovtm_apv_div1_flg14, $ovtm_apv_div1_flg15, $ovtm_apv_div1_flg16, $ovtm_apv_div1_flg17, $ovtm_apv_div1_flg18, $ovtm_apv_div1_flg19, $ovtm_apv_div1_flg20);
$ovtm_apv_div4_flg = array($ovtm_apv_div4_flg1, $ovtm_apv_div4_flg2, $ovtm_apv_div4_flg3, $ovtm_apv_div4_flg4, $ovtm_apv_div4_flg5, $ovtm_apv_div4_flg6, $ovtm_apv_div4_flg7, $ovtm_apv_div4_flg8, $ovtm_apv_div4_flg9, $ovtm_apv_div4_flg10, $ovtm_apv_div4_flg11, $ovtm_apv_div4_flg12, $ovtm_apv_div4_flg13, $ovtm_apv_div4_flg14, $ovtm_apv_div4_flg15, $ovtm_apv_div4_flg16, $ovtm_apv_div4_flg17, $ovtm_apv_div4_flg18, $ovtm_apv_div4_flg19, $ovtm_apv_div4_flg20);

$ovtm_target_class_div = array($ovtm_target_class_div1, $ovtm_target_class_div2, $ovtm_target_class_div3, $ovtm_target_class_div4, $ovtm_target_class_div5, $ovtm_target_class_div6, $ovtm_target_class_div7, $ovtm_target_class_div8, $ovtm_target_class_div9, $ovtm_target_class_div10, $ovtm_target_class_div11, $ovtm_target_class_div12, $ovtm_target_class_div13, $ovtm_target_class_div14, $ovtm_target_class_div15, $ovtm_target_class_div16, $ovtm_target_class_div17, $ovtm_target_class_div18, $ovtm_target_class_div19, $ovtm_target_class_div20);
$ovtm_st_id = array($ovtm_st_id1, $ovtm_st_id2, $ovtm_st_id3, $ovtm_st_id4, $ovtm_st_id5, $ovtm_st_id6, $ovtm_st_id7, $ovtm_st_id8, $ovtm_st_id9, $ovtm_st_id10, $ovtm_st_id11, $ovtm_st_id12, $ovtm_st_id13, $ovtm_st_id14, $ovtm_st_id15, $ovtm_st_id16, $ovtm_st_id17, $ovtm_st_id18, $ovtm_st_id19, $ovtm_st_id20);
$ovtm_emp_id = array($ovtm_emp_id1, $ovtm_emp_id2, $ovtm_emp_id3, $ovtm_emp_id4, $ovtm_emp_id5, $ovtm_emp_id6, $ovtm_emp_id7, $ovtm_emp_id8, $ovtm_emp_id9, $ovtm_emp_id10, $ovtm_emp_id11, $ovtm_emp_id12, $ovtm_emp_id13, $ovtm_emp_id14, $ovtm_emp_id15, $ovtm_emp_id16, $ovtm_emp_id17, $ovtm_emp_id18, $ovtm_emp_id19, $ovtm_emp_id20);
$ovtm_emp_nm = array($ovtm_emp_nm1, $ovtm_emp_nm2, $ovtm_emp_nm3, $ovtm_emp_nm4, $ovtm_emp_nm5, $ovtm_emp_nm6, $ovtm_emp_nm7, $ovtm_emp_nm8, $ovtm_emp_nm9, $ovtm_emp_nm10, $ovtm_emp_nm11, $ovtm_emp_nm12, $ovtm_emp_nm13, $ovtm_emp_nm14, $ovtm_emp_nm15, $ovtm_emp_nm16, $ovtm_emp_nm17, $ovtm_emp_nm18, $ovtm_emp_nm19, $ovtm_emp_nm20);

$ovtm_class_sect_id = array($ovtm_class_sect_id1, $ovtm_class_sect_id2, $ovtm_class_sect_id3, $ovtm_class_sect_id4, $ovtm_class_sect_id5, $ovtm_class_sect_id6, $ovtm_class_sect_id7, $ovtm_class_sect_id8, $ovtm_class_sect_id9, $ovtm_class_sect_id10, $ovtm_class_sect_id11, $ovtm_class_sect_id12, $ovtm_class_sect_id13, $ovtm_class_sect_id14, $ovtm_class_sect_id15, $ovtm_class_sect_id16, $ovtm_class_sect_id17, $ovtm_class_sect_id18, $ovtm_class_sect_id19, $ovtm_class_sect_id20);
$ovtm_atrb_sect_id = array($ovtm_atrb_sect_id1, $ovtm_atrb_sect_id2, $ovtm_atrb_sect_id3, $ovtm_atrb_sect_id4, $ovtm_atrb_sect_id5, $ovtm_atrb_sect_id6, $ovtm_atrb_sect_id7, $ovtm_atrb_sect_id8, $ovtm_atrb_sect_id9, $ovtm_atrb_sect_id10, $ovtm_atrb_sect_id11, $ovtm_atrb_sect_id12, $ovtm_atrb_sect_id13, $ovtm_atrb_sect_id14, $ovtm_atrb_sect_id15, $ovtm_atrb_sect_id16, $ovtm_atrb_sect_id17, $ovtm_atrb_sect_id18, $ovtm_atrb_sect_id19, $ovtm_atrb_sect_id20);
$ovtm_dept_sect_id = array($ovtm_dept_sect_id1, $ovtm_dept_sect_id2, $ovtm_dept_sect_id3, $ovtm_dept_sect_id4, $ovtm_dept_sect_id5, $ovtm_dept_sect_id6, $ovtm_dept_sect_id7, $ovtm_dept_sect_id8, $ovtm_dept_sect_id9, $ovtm_dept_sect_id10, $ovtm_dept_sect_id11, $ovtm_dept_sect_id12, $ovtm_dept_sect_id13, $ovtm_dept_sect_id14, $ovtm_dept_sect_id15, $ovtm_dept_sect_id16, $ovtm_dept_sect_id17, $ovtm_dept_sect_id18, $ovtm_dept_sect_id19, $ovtm_dept_sect_id20);
$ovtm_room_sect_id = array($ovtm_room_sect_id1, $ovtm_room_sect_id2, $ovtm_room_sect_id3, $ovtm_room_sect_id4, $ovtm_room_sect_id5, $ovtm_room_sect_id6, $ovtm_room_sect_id7, $ovtm_room_sect_id8, $ovtm_room_sect_id9, $ovtm_room_sect_id10, $ovtm_room_sect_id11, $ovtm_room_sect_id12, $ovtm_room_sect_id13, $ovtm_room_sect_id14, $ovtm_room_sect_id15, $ovtm_room_sect_id16, $ovtm_room_sect_id17, $ovtm_room_sect_id18, $ovtm_room_sect_id19, $ovtm_room_sect_id20);

$ovtm_st_sect_id = array($ovtm_st_sect_id1, $ovtm_st_sect_id2, $ovtm_st_sect_id3, $ovtm_st_sect_id4, $ovtm_st_sect_id5, $ovtm_st_sect_id6, $ovtm_st_sect_id7, $ovtm_st_sect_id8, $ovtm_st_sect_id9, $ovtm_st_sect_id10, $ovtm_st_sect_id11, $ovtm_st_sect_id12, $ovtm_st_sect_id13, $ovtm_st_sect_id14, $ovtm_st_sect_id15, $ovtm_st_sect_id16, $ovtm_st_sect_id17, $ovtm_st_sect_id18, $ovtm_st_sect_id19, $ovtm_st_sect_id20);

$ovtm_multi_apv_flg = array($ovtm_multi_apv_flg1, $ovtm_multi_apv_flg2, $ovtm_multi_apv_flg3, $ovtm_multi_apv_flg4, $ovtm_multi_apv_flg5, $ovtm_multi_apv_flg6, $ovtm_multi_apv_flg7, $ovtm_multi_apv_flg8, $ovtm_multi_apv_flg9, $ovtm_multi_apv_flg10, $ovtm_multi_apv_flg11, $ovtm_multi_apv_flg12, $ovtm_multi_apv_flg13, $ovtm_multi_apv_flg14, $ovtm_multi_apv_flg15, $ovtm_multi_apv_flg16, $ovtm_multi_apv_flg17, $ovtm_multi_apv_flg18, $ovtm_multi_apv_flg19, $ovtm_multi_apv_flg20);
$ovtm_next_notice_div = array($ovtm_next_notice_div1, $ovtm_next_notice_div2, $ovtm_next_notice_div3, $ovtm_next_notice_div4, $ovtm_next_notice_div5, $ovtm_next_notice_div6, $ovtm_next_notice_div7, $ovtm_next_notice_div8, $ovtm_next_notice_div9, $ovtm_next_notice_div10, $ovtm_next_notice_div11, $ovtm_next_notice_div12, $ovtm_next_notice_div13, $ovtm_next_notice_div14, $ovtm_next_notice_div15, $ovtm_next_notice_div16, $ovtm_next_notice_div17, $ovtm_next_notice_div18, $ovtm_next_notice_div19, $ovtm_next_notice_div20);

$ovtm_apv_num = array($ovtm_apv_num1, $ovtm_apv_num2, $ovtm_apv_num3, $ovtm_apv_num4, $ovtm_apv_num5, $ovtm_apv_num6, $ovtm_apv_num7, $ovtm_apv_num8, $ovtm_apv_num9, $ovtm_apv_num10, $ovtm_apv_num11, $ovtm_apv_num12, $ovtm_apv_num13, $ovtm_apv_num14, $ovtm_apv_num15, $ovtm_apv_num16, $ovtm_apv_num17, $ovtm_apv_num18, $ovtm_apv_num19, $ovtm_apv_num20);



for ($i = 0; $i < $ovtm_approve_num; $i++) {
	$j = $i + 1;
	if($ovtm_wkfw_div == "1"){
		$linktarget = "window.open('timecard_approval_list.php?session=$session&approve=$j&wkfw_nm=ovtm', 'newwin2', 'width=640,height=700,scrollbars=yes')";
		$fontcolor = "";
		$approve_content_color = "green";
	}
	else{
		$linktarget = "";
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"ovtm_kaisou". $j ."\" href=\"javascript:void(0);\" onclick=\"" . $linktarget . "\"><font id=\"ovtm_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></a></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"ovtm_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"ovtm_approve_content". $j ."\"></span></font>");


	$ovtm_target_class_div[$i] = ($ovtm_target_class_div[$i] == "") ? "1" : $ovtm_target_class_div[$i];
	$ovtm_multi_apv_flg[$i] = ($ovtm_multi_apv_flg[$i] == "") ? "f" : $ovtm_multi_apv_flg[$i];
	$ovtm_next_notice_div[$i] = ($ovtm_next_notice_div[$i] == "") ? "2" : $ovtm_next_notice_div[$i];
	$ovtm_apv_num[$i] = ($ovtm_apv_num[$i] == "") ? "1" : $ovtm_apv_num[$i];

	echo("<input type=\"hidden\" name=\"ovtm_approve" . $j . "\" value=\"" . $ovtm_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_apv_div0_flg" . $j . "\" value=\"" . $ovtm_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_apv_div1_flg" . $j . "\" value=\"" . $ovtm_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_apv_div4_flg" . $j . "\" value=\"" . $ovtm_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_target_class_div" . $j . "\" value=\"" . $ovtm_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_st_id" . $j . "\" value=\"" . $ovtm_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_id" . $j . "\" value=\"" . $ovtm_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_emp_nm" . $j . "\" value=\"" . $ovtm_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_class_sect_id" . $j . "\" value=\"" . $ovtm_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_atrb_sect_id" . $j . "\" value=\"" . $ovtm_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_dept_sect_id" . $j . "\" value=\"" . $ovtm_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_room_sect_id" . $j . "\" value=\"" . $ovtm_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_st_sect_id" . $j . "\" value=\"" . $ovtm_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_multi_apv_flg" . $j . "\" value=\"" . $ovtm_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"ovtm_next_notice_div" . $j . "\" value=\"" . $ovtm_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"ovtm_apv_num" . $j . "\" value=\"" . $ovtm_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");

}
?>

<!-- 残業申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="ovtm_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($ovtm_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>>残業申請の確定</font></td>
<td colspan="2"><select name="ovtm_wkfw_approve_num" id="ovtm_wkfw_approve_num" <? if($ovtm_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= $ovtm_approve_num; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $ovtm_wkfw_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
</table>
<!-- 残業申請ワークフロー end -->


<!-- 勤務時間修正申請ワークフロー start -->
<label><input type="checkbox" name="time_wkfw_div" id="time_wkfw_div" value="1" onclick="setWorkflowDisable('time', '<?=$time_approve_num?>')" <? if($time_wkfw_div == "1"){echo("checked");} ?>/><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">勤務時間修正申請のワークフローを設定する</font></label>
<table id="time_wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if($time_wkfw_div != "1"){echo("disabled");} ?>>
<!-- 承認階層数 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="time_kaisofont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($time_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>>承認階層数</font></td>
<td colspan="2"><select name="time_approve_num" id="time_approve_num" onChange="checkApprove('time');" <? if($time_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $time_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>

<?
$time_approve = array($time_approve1, $time_approve2, $time_approve3, $time_approve4, $time_approve5, $time_approve6, $time_approve7, $time_approve8, $time_approve9, $time_approve10, $time_approve11, $time_approve12, $time_approve13, $time_approve14, $time_approve15, $time_approve16, $time_approve17, $time_approve18, $time_approve19, $time_approve20);

$time_apv_div0_flg = array($time_apv_div0_flg1, $time_apv_div0_flg2, $time_apv_div0_flg3, $time_apv_div0_flg4, $time_apv_div0_flg5, $time_apv_div0_flg6, $time_apv_div0_flg7, $time_apv_div0_flg8, $time_apv_div0_flg9, $time_apv_div0_flg10, $time_apv_div0_flg11, $time_apv_div0_flg12, $time_apv_div0_flg13, $time_apv_div0_flg14, $time_apv_div0_flg15, $time_apv_div0_flg16, $time_apv_div0_flg17, $time_apv_div0_flg18, $time_apv_div0_flg19, $time_apv_div0_flg20);
$time_apv_div1_flg = array($time_apv_div1_flg1, $time_apv_div1_flg2, $time_apv_div1_flg3, $time_apv_div1_flg4, $time_apv_div1_flg5, $time_apv_div1_flg6, $time_apv_div1_flg7, $time_apv_div1_flg8, $time_apv_div1_flg9, $time_apv_div1_flg10, $time_apv_div1_flg11, $time_apv_div1_flg12, $time_apv_div1_flg13, $time_apv_div1_flg14, $time_apv_div1_flg15, $time_apv_div1_flg16, $time_apv_div1_flg17, $time_apv_div1_flg18, $time_apv_div1_flg19, $time_apv_div1_flg20);
$time_apv_div4_flg = array($time_apv_div4_flg1, $time_apv_div4_flg2, $time_apv_div4_flg3, $time_apv_div4_flg4, $time_apv_div4_flg5, $time_apv_div4_flg6, $time_apv_div4_flg7, $time_apv_div4_flg8, $time_apv_div4_flg9, $time_apv_div4_flg10, $time_apv_div4_flg11, $time_apv_div4_flg12, $time_apv_div4_flg13, $time_apv_div4_flg14, $time_apv_div4_flg15, $time_apv_div4_flg16, $time_apv_div4_flg17, $time_apv_div4_flg18, $time_apv_div4_flg19, $time_apv_div4_flg20);

$time_target_class_div = array($time_target_class_div1, $time_target_class_div2, $time_target_class_div3, $time_target_class_div4, $time_target_class_div5, $time_target_class_div6, $time_target_class_div7, $time_target_class_div8, $time_target_class_div9, $time_target_class_div10, $time_target_class_div11, $time_target_class_div12, $time_target_class_div13, $time_target_class_div14, $time_target_class_div15, $time_target_class_div16, $time_target_class_div17, $time_target_class_div18, $time_target_class_div19, $time_target_class_div20);
$time_st_id = array($time_st_id1, $time_st_id2, $time_st_id3, $time_st_id4, $time_st_id5, $time_st_id6, $time_st_id7, $time_st_id8, $time_st_id9, $time_st_id10, $time_st_id11, $time_st_id12, $time_st_id13, $time_st_id14, $time_st_id15, $time_st_id16, $time_st_id17, $time_st_id18, $time_st_id19, $time_st_id20);
$time_emp_id = array($time_emp_id1, $time_emp_id2, $time_emp_id3, $time_emp_id4, $time_emp_id5, $time_emp_id6, $time_emp_id7, $time_emp_id8, $time_emp_id9, $time_emp_id10, $time_emp_id11, $time_emp_id12, $time_emp_id13, $time_emp_id14, $time_emp_id15, $time_emp_id16, $time_emp_id17, $time_emp_id18, $time_emp_id19, $time_emp_id20);
$time_emp_nm = array($time_emp_nm1, $time_emp_nm2, $time_emp_nm3, $time_emp_nm4, $time_emp_nm5, $time_emp_nm6, $time_emp_nm7, $time_emp_nm8, $time_emp_nm9, $time_emp_nm10, $time_emp_nm11, $time_emp_nm12, $time_emp_nm13, $time_emp_nm14, $time_emp_nm15, $time_emp_nm16, $time_emp_nm17, $time_emp_nm18, $time_emp_nm19, $time_emp_nm20);

$time_class_sect_id = array($time_class_sect_id1, $time_class_sect_id2, $time_class_sect_id3, $time_class_sect_id4, $time_class_sect_id5, $time_class_sect_id6, $time_class_sect_id7, $time_class_sect_id8, $time_class_sect_id9, $time_class_sect_id10, $time_class_sect_id11, $time_class_sect_id12, $time_class_sect_id13, $time_class_sect_id14, $time_class_sect_id15, $time_class_sect_id16, $time_class_sect_id17, $time_class_sect_id18, $time_class_sect_id19, $time_class_sect_id20);
$time_atrb_sect_id = array($time_atrb_sect_id1, $time_atrb_sect_id2, $time_atrb_sect_id3, $time_atrb_sect_id4, $time_atrb_sect_id5, $time_atrb_sect_id6, $time_atrb_sect_id7, $time_atrb_sect_id8, $time_atrb_sect_id9, $time_atrb_sect_id10, $time_atrb_sect_id11, $time_atrb_sect_id12, $time_atrb_sect_id13, $time_atrb_sect_id14, $time_atrb_sect_id15, $time_atrb_sect_id16, $time_atrb_sect_id17, $time_atrb_sect_id18, $time_atrb_sect_id19, $time_atrb_sect_id20);
$time_dept_sect_id = array($time_dept_sect_id1, $time_dept_sect_id2, $time_dept_sect_id3, $time_dept_sect_id4, $time_dept_sect_id5, $time_dept_sect_id6, $time_dept_sect_id7, $time_dept_sect_id8, $time_dept_sect_id9, $time_dept_sect_id10, $time_dept_sect_id11, $time_dept_sect_id12, $time_dept_sect_id13, $time_dept_sect_id14, $time_dept_sect_id15, $time_dept_sect_id16, $time_dept_sect_id17, $time_dept_sect_id18, $time_dept_sect_id19, $time_dept_sect_id20);
$time_room_sect_id = array($time_room_sect_id1, $time_room_sect_id2, $time_room_sect_id3, $time_room_sect_id4, $time_room_sect_id5, $time_room_sect_id6, $time_room_sect_id7, $time_room_sect_id8, $time_room_sect_id9, $time_room_sect_id10, $time_room_sect_id11, $time_room_sect_id12, $time_room_sect_id13, $time_room_sect_id14, $time_room_sect_id15, $time_room_sect_id16, $time_room_sect_id17, $time_room_sect_id18, $time_room_sect_id19, $time_room_sect_id20);

$time_st_sect_id = array($time_st_sect_id1, $time_st_sect_id2, $time_st_sect_id3, $time_st_sect_id4, $time_st_sect_id5, $time_st_sect_id6, $time_st_sect_id7, $time_st_sect_id8, $time_st_sect_id9, $time_st_sect_id10, $time_st_sect_id11, $time_st_sect_id12, $time_st_sect_id13, $time_st_sect_id14, $time_st_sect_id15, $time_st_sect_id16, $time_st_sect_id17, $time_st_sect_id18, $time_st_sect_id19, $time_st_sect_id20);

$time_multi_apv_flg = array($time_multi_apv_flg1, $time_multi_apv_flg2, $time_multi_apv_flg3, $time_multi_apv_flg4, $time_multi_apv_flg5, $time_multi_apv_flg6, $time_multi_apv_flg7, $time_multi_apv_flg8, $time_multi_apv_flg9, $time_multi_apv_flg10, $time_multi_apv_flg11, $time_multi_apv_flg12, $time_multi_apv_flg13, $time_multi_apv_flg14, $time_multi_apv_flg15, $time_multi_apv_flg16, $time_multi_apv_flg17, $time_multi_apv_flg18, $time_multi_apv_flg19, $time_multi_apv_flg20);
$time_next_notice_div = array($time_next_notice_div1, $time_next_notice_div2, $time_next_notice_div3, $time_next_notice_div4, $time_next_notice_div5, $time_next_notice_div6, $time_next_notice_div7, $time_next_notice_div8, $time_next_notice_div9, $time_next_notice_div10, $time_next_notice_div11, $time_next_notice_div12, $time_next_notice_div13, $time_next_notice_div14, $time_next_notice_div15, $time_next_notice_div16, $time_next_notice_div17, $time_next_notice_div18, $time_next_notice_div19, $time_next_notice_div20);

$time_apv_num = array($time_apv_num1, $time_apv_num2, $time_apv_num3, $time_apv_num4, $time_apv_num5, $time_apv_num6, $time_apv_num7, $time_apv_num8, $time_apv_num9, $time_apv_num10, $time_apv_num11, $time_apv_num12, $time_apv_num13, $time_apv_num14, $time_apv_num15, $time_apv_num16, $time_apv_num17, $time_apv_num18, $time_apv_num19, $time_apv_num20);


for ($i = 0; $i < $time_approve_num; $i++) {
	$j = $i + 1;
	if($time_wkfw_div == "1"){
		$linktarget = "window.open('timecard_approval_list.php?session=$session&approve=$j&wkfw_nm=time', 'newwin2', 'width=640,height=700,scrollbars=yes')";
		$fontcolor = "";
		$approve_content_color = "green";
	}
	else{
		$linktarget = "";
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"time_kaisou". $j ."\" href=\"javascript:void(0);\" onclick=\"" . $linktarget . "\"><font id=\"time_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></a></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"time_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"time_approve_content". $j ."\"></span></font>");


	$time_target_class_div[$i] = ($time_target_class_div[$i] == "") ? "1" : $time_target_class_div[$i];
	$time_multi_apv_flg[$i] = ($time_multi_apv_flg[$i] == "") ? "f" : $time_multi_apv_flg[$i];
	$time_next_notice_div[$i] = ($time_next_notice_div[$i] == "") ? "2" : $time_next_notice_div[$i];
	$time_apv_num[$i] = ($time_apv_num[$i] == "") ? "1" : $time_apv_num[$i];

	echo("<input type=\"hidden\" name=\"time_approve" . $j . "\" value=\"" . $time_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_apv_div0_flg" . $j . "\" value=\"" . $time_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_apv_div1_flg" . $j . "\" value=\"" . $time_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_apv_div4_flg" . $j . "\" value=\"" . $time_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_target_class_div" . $j . "\" value=\"" . $time_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_st_id" . $j . "\" value=\"" . $time_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_emp_id" . $j . "\" value=\"" . $time_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_emp_nm" . $j . "\" value=\"" . $time_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_class_sect_id" . $j . "\" value=\"" . $time_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_atrb_sect_id" . $j . "\" value=\"" . $time_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_dept_sect_id" . $j . "\" value=\"" . $time_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_room_sect_id" . $j . "\" value=\"" . $time_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_st_sect_id" . $j . "\" value=\"" . $time_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_multi_apv_flg" . $j . "\" value=\"" . $time_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"time_next_notice_div" . $j . "\" value=\"" . $time_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"time_apv_num" . $j . "\" value=\"" . $time_apv_num[$i] . "\">\n");


	echo("</td>\n");
	echo("</tr>\n");

}
?>

<!-- 勤務時間修正申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff">
<font id="time_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($time_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>>勤務時間修正申請の確定</font>
</td>
<td colspan="2"><select name="time_wkfw_approve_num" id="time_wkfw_approve_num" <? if($time_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= $time_approve_num; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $time_wkfw_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
</table>
<!-- 勤務時間修正申請ワークフロー end -->


<!-- 退勤後復帰申請ワークフロー start -->
<label><input type="checkbox" name="rtn_wkfw_div" id="rtn_wkfw_div" value="1" onclick="setWorkflowDisable('rtn', '<?=$rtn_approve_num?>')" <? if($rtn_wkfw_div == "1"){echo("checked");} ?>/><font id="atdbk" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><?=$ret_str?>申請のワークフローを設定する</font></label>
<table id="rtn_wkfwtbl" width="600" border="0" cellspacing="0" cellpadding="2" class="list" <? if($rtn_wkfw_div != "1"){echo("disabled");} ?>>
<!-- 承認階層数 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff"><font id="rtn_kaisofont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($rtn_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>>承認階層数</font></td>
<td colspan="2"><select name="rtn_approve_num" id="rtn_approve_num" onChange="checkApprove('rtn');" <? if($rtn_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= 20; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $rtn_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>

<?
$rtn_approve = array($rtn_approve1, $rtn_approve2, $rtn_approve3, $rtn_approve4, $rtn_approve5, $rtn_approve6, $rtn_approve7, $rtn_approve8, $rtn_approve9, $rtn_approve10, $rtn_approve11, $rtn_approve12, $rtn_approve13, $rtn_approve14, $rtn_approve15, $rtn_approve16, $rtn_approve17, $rtn_approve18, $rtn_approve19, $rtn_approve20);

$rtn_apv_div0_flg = array($rtn_apv_div0_flg1, $rtn_apv_div0_flg2, $rtn_apv_div0_flg3, $rtn_apv_div0_flg4, $rtn_apv_div0_flg5, $rtn_apv_div0_flg6, $rtn_apv_div0_flg7, $rtn_apv_div0_flg8, $rtn_apv_div0_flg9, $rtn_apv_div0_flg10, $rtn_apv_div0_flg11, $rtn_apv_div0_flg12, $rtn_apv_div0_flg13, $rtn_apv_div0_flg14, $rtn_apv_div0_flg15, $rtn_apv_div0_flg16, $rtn_apv_div0_flg17, $rtn_apv_div0_flg18, $rtn_apv_div0_flg19, $rtn_apv_div0_flg20);
$rtn_apv_div1_flg = array($rtn_apv_div1_flg1, $rtn_apv_div1_flg2, $rtn_apv_div1_flg3, $rtn_apv_div1_flg4, $rtn_apv_div1_flg5, $rtn_apv_div1_flg6, $rtn_apv_div1_flg7, $rtn_apv_div1_flg8, $rtn_apv_div1_flg9, $rtn_apv_div1_flg10, $rtn_apv_div1_flg11, $rtn_apv_div1_flg12, $rtn_apv_div1_flg13, $rtn_apv_div1_flg14, $rtn_apv_div1_flg15, $rtn_apv_div1_flg16, $rtn_apv_div1_flg17, $rtn_apv_div1_flg18, $rtn_apv_div1_flg19, $rtn_apv_div1_flg20);
$rtn_apv_div4_flg = array($rtn_apv_div4_flg1, $rtn_apv_div4_flg2, $rtn_apv_div4_flg3, $rtn_apv_div4_flg4, $rtn_apv_div4_flg5, $rtn_apv_div4_flg6, $rtn_apv_div4_flg7, $rtn_apv_div4_flg8, $rtn_apv_div4_flg9, $rtn_apv_div4_flg10, $rtn_apv_div4_flg11, $rtn_apv_div4_flg12, $rtn_apv_div4_flg13, $rtn_apv_div4_flg14, $rtn_apv_div4_flg15, $rtn_apv_div4_flg16, $rtn_apv_div4_flg17, $rtn_apv_div4_flg18, $rtn_apv_div4_flg19, $rtn_apv_div4_flg20);

$rtn_target_class_div = array($rtn_target_class_div1, $rtn_target_class_div2, $rtn_target_class_div3, $rtn_target_class_div4, $rtn_target_class_div5, $rtn_target_class_div6, $rtn_target_class_div7, $rtn_target_class_div8, $rtn_target_class_div9, $rtn_target_class_div10, $rtn_target_class_div11, $rtn_target_class_div12, $rtn_target_class_div13, $rtn_target_class_div14, $rtn_target_class_div15, $rtn_target_class_div16, $rtn_target_class_div17, $rtn_target_class_div18, $rtn_target_class_div19, $rtn_target_class_div20);
$rtn_st_id = array($rtn_st_id1, $rtn_st_id2, $rtn_st_id3, $rtn_st_id4, $rtn_st_id5, $rtn_st_id6, $rtn_st_id7, $rtn_st_id8, $rtn_st_id9, $rtn_st_id10, $rtn_st_id11, $rtn_st_id12, $rtn_st_id13, $rtn_st_id14, $rtn_st_id15, $rtn_st_id16, $rtn_st_id17, $rtn_st_id18, $rtn_st_id19, $rtn_st_id20);
$rtn_emp_id = array($rtn_emp_id1, $rtn_emp_id2, $rtn_emp_id3, $rtn_emp_id4, $rtn_emp_id5, $rtn_emp_id6, $rtn_emp_id7, $rtn_emp_id8, $rtn_emp_id9, $rtn_emp_id10, $rtn_emp_id11, $rtn_emp_id12, $rtn_emp_id13, $rtn_emp_id14, $rtn_emp_id15, $rtn_emp_id16, $rtn_emp_id17, $rtn_emp_id18, $rtn_emp_id19, $rtn_emp_id20);
$rtn_emp_nm = array($rtn_emp_nm1, $rtn_emp_nm2, $rtn_emp_nm3, $rtn_emp_nm4, $rtn_emp_nm5, $rtn_emp_nm6, $rtn_emp_nm7, $rtn_emp_nm8, $rtn_emp_nm9, $rtn_emp_nm10, $rtn_emp_nm11, $rtn_emp_nm12, $rtn_emp_nm13, $rtn_emp_nm14, $rtn_emp_nm15, $rtn_emp_nm16, $rtn_emp_nm17, $rtn_emp_nm18, $rtn_emp_nm19, $rtn_emp_nm20);

$rtn_class_sect_id = array($rtn_class_sect_id1, $rtn_class_sect_id2, $rtn_class_sect_id3, $rtn_class_sect_id4, $rtn_class_sect_id5, $rtn_class_sect_id6, $rtn_class_sect_id7, $rtn_class_sect_id8, $rtn_class_sect_id9, $rtn_class_sect_id10, $rtn_class_sect_id11, $rtn_class_sect_id12, $rtn_class_sect_id13, $rtn_class_sect_id14, $rtn_class_sect_id15, $rtn_class_sect_id16, $rtn_class_sect_id17, $rtn_class_sect_id18, $rtn_class_sect_id19, $rtn_class_sect_id20);
$rtn_atrb_sect_id = array($rtn_atrb_sect_id1, $rtn_atrb_sect_id2, $rtn_atrb_sect_id3, $rtn_atrb_sect_id4, $rtn_atrb_sect_id5, $rtn_atrb_sect_id6, $rtn_atrb_sect_id7, $rtn_atrb_sect_id8, $rtn_atrb_sect_id9, $rtn_atrb_sect_id10, $rtn_atrb_sect_id11, $rtn_atrb_sect_id12, $rtn_atrb_sect_id13, $rtn_atrb_sect_id14, $rtn_atrb_sect_id15, $rtn_atrb_sect_id16, $rtn_atrb_sect_id17, $rtn_atrb_sect_id18, $rtn_atrb_sect_id19, $rtn_atrb_sect_id20);
$rtn_dept_sect_id = array($rtn_dept_sect_id1, $rtn_dept_sect_id2, $rtn_dept_sect_id3, $rtn_dept_sect_id4, $rtn_dept_sect_id5, $rtn_dept_sect_id6, $rtn_dept_sect_id7, $rtn_dept_sect_id8, $rtn_dept_sect_id9, $rtn_dept_sect_id10, $rtn_dept_sect_id11, $rtn_dept_sect_id12, $rtn_dept_sect_id13, $rtn_dept_sect_id14, $rtn_dept_sect_id15, $rtn_dept_sect_id16, $rtn_dept_sect_id17, $rtn_dept_sect_id18, $rtn_dept_sect_id19, $rtn_dept_sect_id20);
$rtn_room_sect_id = array($rtn_room_sect_id1, $rtn_room_sect_id2, $rtn_room_sect_id3, $rtn_room_sect_id4, $rtn_room_sect_id5, $rtn_room_sect_id6, $rtn_room_sect_id7, $rtn_room_sect_id8, $rtn_room_sect_id9, $rtn_room_sect_id10, $rtn_room_sect_id11, $rtn_room_sect_id12, $rtn_room_sect_id13, $rtn_room_sect_id14, $rtn_room_sect_id15, $rtn_room_sect_id16, $rtn_room_sect_id17, $rtn_room_sect_id18, $rtn_room_sect_id19, $rtn_room_sect_id20);

$rtn_st_sect_id = array($rtn_st_sect_id1, $rtn_st_sect_id2, $rtn_st_sect_id3, $rtn_st_sect_id4, $rtn_st_sect_id5, $rtn_st_sect_id6, $rtn_st_sect_id7, $rtn_st_sect_id8, $rtn_st_sect_id9, $rtn_st_sect_id10, $rtn_st_sect_id11, $rtn_st_sect_id12, $rtn_st_sect_id13, $rtn_st_sect_id14, $rtn_st_sect_id15, $rtn_st_sect_id16, $rtn_st_sect_id17, $rtn_st_sect_id18, $rtn_st_sect_id19, $rtn_st_sect_id20);

$rtn_multi_apv_flg = array($rtn_multi_apv_flg1, $rtn_multi_apv_flg2, $rtn_multi_apv_flg3, $rtn_multi_apv_flg4, $rtn_multi_apv_flg5, $rtn_multi_apv_flg6, $rtn_multi_apv_flg7, $rtn_multi_apv_flg8, $rtn_multi_apv_flg9, $rtn_multi_apv_flg10, $rtn_multi_apv_flg11, $rtn_multi_apv_flg12, $rtn_multi_apv_flg13, $rtn_multi_apv_flg14, $rtn_multi_apv_flg15, $rtn_multi_apv_flg16, $rtn_multi_apv_flg17, $rtn_multi_apv_flg18, $rtn_multi_apv_flg19, $rtn_multi_apv_flg20);
$rtn_next_notice_div = array($rtn_next_notice_div1, $rtn_next_notice_div2, $rtn_next_notice_div3, $rtn_next_notice_div4, $rtn_next_notice_div5, $rtn_next_notice_div6, $rtn_next_notice_div7, $rtn_next_notice_div8, $rtn_next_notice_div9, $rtn_next_notice_div10, $rtn_next_notice_div11, $rtn_next_notice_div12, $rtn_next_notice_div13, $rtn_next_notice_div14, $rtn_next_notice_div15, $rtn_next_notice_div16, $rtn_next_notice_div17, $rtn_next_notice_div18, $rtn_next_notice_div19, $rtn_next_notice_div20);

$rtn_apv_num = array($rtn_apv_num1, $rtn_apv_num2, $rtn_apv_num3, $rtn_apv_num4, $rtn_apv_num5, $rtn_apv_num6, $rtn_apv_num7, $rtn_apv_num8, $rtn_apv_num9, $rtn_apv_num10, $rtn_apv_num11, $rtn_apv_num12, $rtn_apv_num13, $rtn_apv_num14, $rtn_apv_num15, $rtn_apv_num16, $rtn_apv_num17, $rtn_apv_num18, $rtn_apv_num19, $rtn_apv_num20);


for ($i = 0; $i < $rtn_approve_num; $i++) {
	$j = $i + 1;
	if($rtn_wkfw_div == "1"){
		$linktarget = "window.open('timecard_approval_list.php?session=$session&approve=$j&wkfw_nm=rtn', 'newwin2', 'width=640,height=700,scrollbars=yes')";
		$fontcolor = "";
		$approve_content_color = "green";
	}
	else{
		$linktarget = "";
		$fontcolor = "#a9a9a9";
		$approve_content_color = "#a9a9a9";
	}
	echo("<tr>\n");
	echo("<td height=\"22\" align=\"right\" bgcolor=\"#f6f9ff\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a id=\"rtn_kaisou". $j ."\" href=\"javascript:void(0);\" onclick=\"" . $linktarget . "\"><font id=\"rtn_kaisofont" . $j . "\" color=" . $fontcolor .">承認階層" . $j . "</font></a></font></td>\n");
	echo("<td colspan=\"2\"><font id=\"rtn_approve_content_font" . $j . "\" size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"" . $approve_content_color . "\"><span id=\"rtn_approve_content". $j ."\"></span></font>");

	$rtn_target_class_div[$i] = ($rtn_target_class_div[$i] == "") ? "1" : $rtn_target_class_div[$i];
	$rtn_multi_apv_flg[$i] = ($rtn_multi_apv_flg[$i] == "") ? "f" : $rtn_multi_apv_flg[$i];
	$rtn_next_notice_div[$i] = ($rtn_next_notice_div[$i] == "") ? "2" : $rtn_next_notice_div[$i];
	$rtn_apv_num[$i] = ($rtn_apv_num[$i] == "") ? "1" : $rtn_apv_num[$i];

	echo("<input type=\"hidden\" name=\"rtn_approve" . $j . "\" value=\"" . $rtn_approve[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_apv_div0_flg" . $j . "\" value=\"" . $rtn_apv_div0_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_apv_div1_flg" . $j . "\" value=\"" . $rtn_apv_div1_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_apv_div4_flg" . $j . "\" value=\"" . $rtn_apv_div4_flg[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_target_class_div" . $j . "\" value=\"" . $rtn_target_class_div[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_st_id" . $j . "\" value=\"" . $rtn_st_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_emp_id" . $j . "\" value=\"" . $rtn_emp_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_emp_nm" . $j . "\" value=\"" . $rtn_emp_nm[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_class_sect_id" . $j . "\" value=\"" . $rtn_class_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_atrb_sect_id" . $j . "\" value=\"" . $rtn_atrb_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_dept_sect_id" . $j . "\" value=\"" . $rtn_dept_sect_id[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_room_sect_id" . $j . "\" value=\"" . $rtn_room_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_st_sect_id" . $j . "\" value=\"" . $rtn_st_sect_id[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_multi_apv_flg" . $j . "\" value=\"" . $rtn_multi_apv_flg[$i] . "\">\n");
	echo("<input type=\"hidden\" name=\"rtn_next_notice_div" . $j . "\" value=\"" . $rtn_next_notice_div[$i] . "\">\n");

	echo("<input type=\"hidden\" name=\"rtn_apv_num" . $j . "\" value=\"" . $rtn_apv_num[$i] . "\">\n");

	echo("</td>\n");
	echo("</tr>\n");

}
?>

<!-- 退勤後復帰申請の確定 -->
<tr height="22">
<td width="160" align="right" bgcolor="#f6f9ff">
<font id="rtn_wkfwfont" size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" <? if($rtn_wkfw_div != "1"){echo("color = \"#a9a9a9\"");} ?>><?=$ret_str?>申請の確定</font>
</td>
<td colspan="2"><select name="rtn_wkfw_approve_num" id="rtn_wkfw_approve_num" <? if($rtn_wkfw_div != "1"){echo("disabled");} ?>><option value="0">選択してください</option><?
for ($i = 1; $i <= $rtn_approve_num; $i++) {
	echo("<option value=\"$i\"");
	if ($i == $rtn_wkfw_approve_num) {
		echo(" selected");
	}
	echo(">$i</option>\n");
}
?>
</select></td>
</tr>
</table>
<!-- 退勤後復帰申請ワークフロー end -->




<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="right"><input type="button" value="更新" onclick="submitForm();"></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" id="target_id_list1"   name="target_id_list1" value="">
<input type="hidden" id="target_name_list1" name="target_name_list1" value="">
<input type="hidden" name="back" value="t">

</form>
</td>
</tr>
</table>

<script type="text/javascript">


// 承認階層
<?
for ($i = 0; $i < $ovtm_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('ovtm_approve_content<?=$j?>').innerHTML = '<?=$ovtm_approve[$i]?>';
<?
}
?>

<?
for ($i = 0; $i < $time_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('time_approve_content<?=$j?>').innerHTML = '<?=$time_approve[$i]?>';
<?
}
?>

<?
for ($i = 0; $i < $rtn_approve_num; $i++)
{
$j = $i+1;
?>
document.getElementById('rtn_approve_content<?=$j?>').innerHTML = '<?=$rtn_approve[$i]?>';
<?
}
?>

</script>

</body>
<? pg_close($con); ?>
</html>
<?
// ワークロー・承認者詳細情報取得
function search_atdbk_wkfw_apv_dtl($con, $fname, $apv_order, $wkfw_div, &$ret_arr) {

	$sql  = "select a.emp_id, b.emp_lt_nm, b.emp_ft_nm, c.emp_del_flg ";
	$sql .= "from atdbk_apvdtl a ";
	$sql .= "inner join empmst b on a.emp_id = b.emp_id ";
	$sql .= "inner join authmst c on a.emp_id = c.emp_id ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order order by a.apv_sub_order asc";
    $cond = "";


	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$idx = 0;
	while($row = pg_fetch_array($sel))
	{

		if($idx > 0)
		{
			$emp_id .= ",";
			$approve .= ", ";
			$emp_del_flg .= ",";
		}
		$emp_id .= $row["emp_id"];
		$emp_lt_nm = $row["emp_lt_nm"];
		$emp_ft_nm = $row["emp_ft_nm"];
		$emp_del_flg .= $row["emp_del_flg"];

		$emp_full_nm = $emp_lt_nm ." ".$emp_ft_nm;
		$approve .= $emp_full_nm;
		$idx++;
	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => $emp_id,
							    "emp_nm" => $approve,
							    "emp_del_flg" => $emp_del_flg,
   							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));

}

// ワークフロー・部署役職情報取得
function search_atdbk_wkfw_apv_pst($con, $fname, $apv_order, $wkfw_div, &$ret_arr) {
	global $_label_by_profile;
	global $profile_type;

	$sql  = "select ";
	$sql .= "a.target_class_div, ";
	$sql .= "case a.target_class_div ";
	$sql .= "when '1' then c.class_nm ";
	$sql .= "when '2' then c.atrb_nm ";
	$sql .= "when '3' then c.dept_nm ";
	$sql .= "when '4' then c.room_nm ";
	$sql .= "else '' ";
	$sql .= "end as target_class_name, ";
	$sql .= "b.st_id,";
	$sql .= "b.st_nm ";
	$sql .= "from atdbk_apvmng a left outer join ";
	$sql .= "(select a.apv_order, a.st_id, b.st_nm ";

	$sql .= "from atdbk_apvpstdtl a inner join stmst b on a.st_id = b.st_id where wkfw_div = $wkfw_div and st_div = 0) b on ";
	$sql .= "a.apv_order = b.apv_order, ";
	$sql .= "(select * from classname) c ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order ";

    $cond = "";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$num = pg_numrows($sel);

	$approve = "";
	$target_class_div = "";
	$st_id = "";
	$st_nm = "";


	for ($i=0; $i<$num; $i++) {

		if($i == 0) {
		  	$target_class_div = pg_fetch_result($sel, $i, "target_class_div");
		  	$target_class_name = pg_fetch_result($sel, $i, "target_class_name");
     		$st_id = pg_fetch_result($sel, $i, "st_id");
			$st_nm = pg_fetch_result($sel, $i, "st_nm");

			if($target_class_div != 0) {
				$approve .= "申請者の所属する【";
    	 		$approve .=	$target_class_name;
     			$approve .=	"】の";
			} else {
				// 病院内/所内
				$in_hospital2_title = $_label_by_profile["IN_HOSPITAL2"][$profile_type];
				if($in_hospital2_title != "")
				{
	     			$approve .=	"【".$in_hospital2_title."】の";
				}
			}

			$approve .=	$st_nm;

		} else {
			$st_id .=",";
     		$st_id .= pg_fetch_result($sel, $i, "st_id");

			$st_nm = pg_fetch_result($sel, $i, "st_nm");
			$approve .=	", ";
			$approve .=	$st_nm;
		}


	}

	array_push($ret_arr, array("approve" => $approve,
							    "target_class_div" => $target_class_div,
								"st_id" => $st_id,
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "class_sect_id" => "",
							    "atrb_sect_id" => "",
							    "dept_sect_id" => "",
							    "room_sect_id" => "",
							    "st_sect_id" => ""));
}

// 部署役職(部署所属)取得
function search_atdbk_wkfw_apv_pst_sect($con, $fname, $apv_order, $wkfw_div, &$ret_arr)
{

	// 部署取得
	$sql  = "select a.*, b.class_nm, c.atrb_nm, d.dept_nm, e.room_nm ";
	$sql .= "from atdbk_apvsectdtl a ";

	$sql .= "left join classmst b on a.class_id = b.class_id and not b.class_del_flg ";
	$sql .= "left join atrbmst c on a.atrb_id = c.atrb_id and not c.atrb_del_flg ";
	$sql .= "left join deptmst d on a.dept_id = d.dept_id and not d.dept_del_flg ";
	$sql .= "left join classroom e on a.room_id = e.room_id and not e.room_del_flg ";
	$sql .= "where a.wkfw_div = $wkfw_div and a.apv_order = $apv_order ";

	$sel  = select_from_table($con, $sql, "", $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	$class_sect_id = pg_fetch_result($sel, 0, "class_id");
	$atrb_sect_id = pg_fetch_result($sel, 0, "atrb_id");
	$dept_sect_id = pg_fetch_result($sel, 0, "dept_id");
	$room_sect_id = pg_fetch_result($sel, 0, "room_id");

	$class_nm = pg_fetch_result($sel, 0, "class_nm");
	$atrb_nm = pg_fetch_result($sel, 0, "atrb_nm");
	$dept_nm = pg_fetch_result($sel, 0, "dept_nm");
	$room_nm = pg_fetch_result($sel, 0, "room_nm");

	if($class_nm != "")
	{
		$class_sect_nm .= $class_nm;

		if($atrb_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $atrb_nm;
		}

		if($dept_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $dept_nm;
		}

		if($room_nm != "")
		{
			$class_sect_nm .= " > ";
			$class_sect_nm .= $room_nm;
		}
	}

	// 役職
	if($class_sect_nm != "")
	{
		$sql  = "select a.*, b.st_nm ";
		$sql .= "from atdbk_apvpstdtl a ";
		$sql .= "left join stmst b on a.st_id = b.st_id and not st_del_flg ";
		$sql .= "where a.apv_order = $apv_order ";
		$sql .= "and a.st_div = 1 and a.wkfw_div = $wkfw_div";

		$sel  = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$all_st_sect_id = "";
		$all_st_sect_nm = "";
		while($row = pg_fetch_array($sel))
		{
			if($all_st_sect_id != "")
			{
				$all_st_sect_id .= ",";
				$all_st_sect_nm .= ", ";
			}

			$all_st_sect_id .= $row["st_id"];
			$all_st_sect_nm .= $row["st_nm"];
		}

	}
	$approve = "";
	if($class_sect_nm != "" && $all_st_sect_nm != "")
	{
		$approve = $class_sect_nm."の".$all_st_sect_nm;
	}

	array_push($ret_arr, array("approve" => $approve,
	 						    "target_class_div" => "",
								"st_id" => "",
							    "emp_id" => "",
							    "emp_nm" => "",
							    "emp_del_flg" => "f",
							    "class_sect_id" => $class_sect_id,
							    "atrb_sect_id" => $atrb_sect_id,
							    "dept_sect_id" => $dept_sect_id,
							    "room_sect_id" => $room_sect_id,
							    "st_sect_id" => $all_st_sect_id));
}

?>
