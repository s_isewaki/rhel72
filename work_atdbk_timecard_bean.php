<?
class work_atdbk_timecard_bean{

	var $emp_id;			//emp_id
	var $date;				//日付
	var $week_day;			//曜日
	var $type;				//種別
	var $tmcd_group_id;		//グループ
	var $pattern_id;		//出勤パターン
	var $reason;			//事由
	var $night_duty;		//当直
	var $allow_id;			//手当
	var $next_day_flag;		//前日フラグ
	var $start_time;		//出勤
	var $previous_day_flag;	//翌日フラグ
	var $end_time;			//退勤
	var $over_time;			//残業
	var $night_time;		//深勤
	var $goout_time1;		//普外
	var $goout_time2;		//残外
	var $return_count;		//退勤後復帰　回数
	var $return_time;		//退勤後復帰　時間
	var $workday_count;		//日数
	var $meeting_time;		//会議研修

	function select($con, $fname){
		//1レコード取得
		$sql = "select * from timecard";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0) {
			pg_close($con);
			echo("<script type='text/javascript' src='./js/showpage.js'></script>");
			echo("<script language='javascript'>showErrorPage(window);</script>");
			exit;
		}

		$row = pg_fetch_all($sel);
		if(count($row) > 0){
			$this->set_select_value($row[0]);
		}
	}

	function set_select_value($row){

		//キー（カラム名取得）
		$arr_key = (array_keys($row));

		foreach ($arr_key as $key) {
			//カラム名の変数に値を保持
			$colmun_name = $key;
			$value       = $row[$key];
			$this->$colmun_name = $value;
//			}
//			global $$colmun_name;
//			$$colmun_name = $value;
		}

	}

}
?>