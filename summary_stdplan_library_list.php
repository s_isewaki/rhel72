<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("label_by_profile_type.ini");
require_once("show_sinryo_top_header.ini");
require_once("summary_common.ini");
require_once("about_comedix.php");

//==============================
//ヘッダー処理
//==============================

// ページ名
$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session,$fname);
if($session == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 診療記録使用権限のチェック
$checkauth = check_authority($session,59,$fname);
if($checkauth == "0"){
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
if($con == "0"){
  echo("<script type='text/javascript' src='./js/showpage.js'></script>");
  echo("<script language='javascript'>showLoginPage(window);</script>");
  exit;
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];

//==============================
//パラメータを取得
//==============================
$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
$is_postback = isset($_POST['is_postback']) ? $_POST['is_postback'] : '';
$problem_name = isset($_POST['problem_name']) ? trim($_POST['problem_name']) : '';

//==============================
//ポストバック時
//==============================
$cond = '';
if($is_postback == 'true') {
    if ($mode == 'delete') {    // 削除
        $data_select = isset($_POST['data_select']) ? $_POST['data_select'] : array();
        if (count($data_select) == 0) {
            echo("<script type=\"text/javascript\">alert('チェックボックスをオンにしてください。');</script>");
            echo("<script type=\"text/javascript\">history.back();</script>");
        }
        if (!deleteStandardPlanLibraryData($con, $data_select)) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        }
    } elseif ($mode == 'search' || isset($_POST['summary_current_page'])) {    // 検索(ページ数がセットされていたら、検索条件の有無をチェック)
        if (strlen($problem_name) != 0) {
            $cond = "where problem_name like '%{$problem_name}%'";
        }
    }
} 

//==============================
//標準計画一覧を取得
//==============================
$data_list = getStandardPlanLibraryDataList($con, $cond, $fname);

pg_close($con);    // DB切断

/**
 * 標準計画ライブラリデータリストを取得する
 *
 * @param resource $con   データベース接続リソース
 * @param string   $cond  条件
 * @param string   $fname ページ名
 *
 * @return mixed 成功時:取得したレコードの配列/結果の行数が 0だった場合、又はその他のエラーが発生:false
 */
function getStandardPlanLibraryDataList($con, $cond, $fname)
{
    $sql = "select * from sum_med_std_wkfwmst";
    $cond .= " order by med_wkfw_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        return false;
    }
    $data_list = pg_fetch_all($sel);

    return $data_list;
}

/**
 * ページャー作成
 *
 * @param integer $total_record  全レコード数
 * @param integer $rows_per_page 1ページあたり表示する件数
 * @param integer $page          表示するページ
 *
 * @return void
 */
function createPager($total_record, $rows_per_page, $page)
{
    $total_page_count = floor(($total_record-1) / $rows_per_page) + 1;
    $font = "<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
    echo "<td>\n";
    echo "<table><tr>\n";
    echo "<td style=\"white-space:nowrap\">{$font}全{$total_record}行</font></td>\n";
    if ($total_page_count > 1) {
        echo "<td style=\"white-space:nowrap\">{$font}";
        echo "<span style=\"color:silver\">";
        if($page > 1) {
            echo "<a href=\"\" onclick=\"return summary_page_change(1);\">\n";
        }
        echo "先頭へ</a></span>\n";
        echo "</font></td>";
        echo "<td style=\"white-space:nowrap\">{$font}";
        echo "<span style=\"color:silver\">";
        if($page > 1) {
            echo "<a href=\"\" onclick=\"return summary_page_change(" . sprintf("%d", $page - 1) . ");\">\n";
        }
        echo "前へ</a></span>\n";
        echo "</font></td>\n";
        echo "<td>{$font}\n";
        for($i = 1; $i <= $total_page_count; $i++) {
            if ($page > $i && $page - $i > 10) {
                continue;
            }
            if ($page < $i && $i - $page > 10) {
                continue;
            }
            if($i != $page) {
                echo "<a href=\"\" onclick=\"return summary_page_change({$i});\">[{$i}]</a>\n";
            } else {
                echo "[{$i}]\n";
            }
        }
        echo "</font></td>\n";
        echo "<td style=\"white-space:nowrap\">{$font}\n";
        echo "<span style=\"color:silver\">\n";
        if ($page < $total_page_count) {
            echo "<a href=\"\" onclick=\"return summary_page_change(" . sprintf("%d", $page + 1) . ")\">;";
        }
        echo "次へ</a></span>";
        echo "</font></td>";
    }
    echo "</tr></table>";
    echo "</td>";
}

/**
 * 標準計画ライブラリデータを削除する
 *
 * @param resource $con         データベース接続リソース
 * @param array    $data_select 選択されたデータのワークフローID
 *
 * @return boolean 成功時:true/失敗時:false
 */
function deleteStandardPlanLibraryData($con, $data_select)
{
    //削除パラメータ作成
    $delete_target_list = "";
    foreach ($data_select as $med_wkfw_id) {
        if ($delete_target_list != "") {
            $delete_target_list .= ",";
        }
        $delete_target_list .= "'" . pg_escape_string($med_wkfw_id) . "'";
    }

    // トランザクションの開始
    pg_query($con, "begin");

    // 標準チーム計画ライブラリ・ワークフローマスターテーブル削除
    $sql = "delete from sum_med_std_wkfwmst where med_wkfw_id in($delete_target_list)";
    $del = delete_from_table($con, $sql, "", $fname);
    if ($del == 0)
    {
        pg_query($con,"rollback");
        return false;
    }

    // 標準チーム計画ライブラリ・ワークフロー・観察項目テーブル削除
    $sql = "delete from sum_med_std_wkfwobse where med_wkfw_id = '$med_wkfw_id'";
    $del = delete_from_table($con, $sql, "", $fname);
    if ($del == 0)
    {
        pg_query($con,"rollback");
        return false;
    }

    // トランザクションのコミット
    pg_query($con, "commit");

    return true;
}
?>
<title>CoMedix マスターメンテナンス | マスタ管理</title>
<script language="JavaScript" type="text/JavaScript">
/****************************************************************
 * reload_page()
 * リロード
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function reload_page()
{
  location.href = "summary_stdplan_library_list.php?session=<? echo $session; ?>";
}

/****************************************************************
 * add_stdplan_library()
 * 追加画面を開く
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function add_stdplan_library()
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=633,height=800";
  window.open('summary_stdplan_library.php?session=<? echo $session; ?>&mode=insert', 'stdPlanLibraryWin', option);
}

/****************************************************************
 * reloadupdate_stdplan_library_page()
 * 更新画面を開く
 *
 * 引　数：med_wkfw_id ワークフローID
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function update_stdplan_library(med_wkfw_id)
{
  var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=633,height=800";
  window.open('summary_stdplan_library.php?session=<? echo $session; ?>&mode=update&med_wkfw_id='+med_wkfw_id, 'stdPlanLibraryWin', option);
}

/****************************************************************
 * delete_stdplan_library()
 * 選択したデータを削除
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function delete_stdplan_library()
{
  if (!confirm('選択された標準看護計画を削除します。よろしいですか？'))
  {
    return;
  }
  document.mainform.action = 'summary_stdplan_library_list.php?mode=delete';
  document.mainform.submit();
}

/****************************************************************
 * search_stdplan_library()
 * 検索
 *
 * 引　数：なし
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function search_stdplan_library()
{
    document.mainform.action = 'summary_stdplan_library_list.php?mode=search';
    document.mainform.submit();
}

/****************************************************************
 * summary_page_change()
 * ページ遷移
 *
 * 引　数：page ページ番号
 * 戻り値：なし
 * 
 * 2012/07/12 追加
 ****************************************************************/
function summary_page_change(page)
{
    document.getElementById("summary_current_page").value = page;
    document.mainform.submit();
    return false;
}

</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
.list td td {border-width:0;}
.list2 {border-collapse:collapse;}
.list2 td {border:#5279a5 solid 1px;}
.list2_in1 {border-collapse:collapse;}
.list2_in1 td {border-width:0;}
.list2_in2 {border-collapse:collapse;}
.list2_in2 th {border:#ffffff solid 1px; background-color:#4682b4; color:#ffffff}
.list2_in2 td {border:#ffffff solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<form name="mainform" action="summary_stdplan_library_list.php" method="post">
<? show_sinryo_top_header($session,$fname,"KANRI_MST"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<? echo summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<? summary_common_show_mst_tab_menu($session, '標準看護計画'); ?>
<td>&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="3"><br>
<div style="width:650px;">
<div style="float: left; margin-left: 10px; width:350px;">プロブレム&nbsp;
  <input type="text" id="problem_name" name="problem_name" style="width: 250px; ime-mode: active;" value="<? echo $problem_name; ?>" maxlength="120">
</div>
<div style="float: left;"><input type="button" value="検索" onclick="search_stdplan_library();"></div>
</div>
<br style="clear:both">
<table width="100%">
<tr>
<?
// ページャー
$page = isset($_REQUEST["summary_current_page"]) ? max(1 ,(int)$_REQUEST["summary_current_page"]) : 1;
$rows_per_page = 10;    // 1ページあたりの件数
$total_record = $data_list ? count($data_list) : 0;
if ($page > floor(($total_record - 1) / $rows_per_page) + 1) {
    $page = 1;
}
createPager($total_record, $rows_per_page, $page);
?>
<td align="right">
<input type="button" value="削除" onclick="delete_stdplan_library();">
<input type="button" value="新規" onclick="add_stdplan_library();">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="summary_current_page" id="summary_current_page" value="<? echo $page; ?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="list2_in2">
<tr>
<th width="30">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;</font>
</th>
<th width="*">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">プロブレム</font>
</th>
<th width="30">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">利用</font>
</th>
</tr>
<?
$start = ($page - 1) * $rows_per_page;
$end = min($total_record, $page * $rows_per_page);
for ($i = $start; $i < $end; $i++) {
    if (!$data_list[$i]) {
        break;
    }
    if ($i % 2 == 0) {
        $bgcolor = '#e6e6fa';
    } else {
        $bgcolor = '#f5f5f5';
    }
?>
<tr>
<td bgcolor="<? echo $bgcolor; ?>" align="center">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <input type="checkbox" name="data_select[]" value="<? echo $data_list[$i]['med_wkfw_id']; ?>">
   </font>
</td>
<td bgcolor="<? echo $bgcolor; ?>">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <a href="javascript:update_stdplan_library('<? echo $data_list[$i]['med_wkfw_id']; ?>')">
   <? echo h($data_list[$i]['problem_name']); ?>
   </a>
   </font>
</td>
<td bgcolor="<? echo $bgcolor; ?>" align="center">
   <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
   <? echo $data_list[$i]['usable'] == 't' ? '可' : '不可'; ?>
   </font>
</td>
</tr>
<?
}
?>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>
