<?
//-----ファイルの読み込み-----
require_once("smarty_setting.ini");
require_once("./about_postgres.php");


//**********職員IDを取得**********
function get_emp_id($con,$session,$fname){

	require("./conf/sql.inf");

	$cond = "where session_id = '$session'";
	$sel = select_from_table($con,$SQL1,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$emp_id = pg_result($sel,0,"emp_id");

	return $emp_id;

}


//**********全職種IDを配列で取得**********
function get_job_id($con,$emp_id,$fname){

	require("./conf/sql.inf");

/*
	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL47,$cond,$fname);
*/
// 全職種を取得
	$sql = "select job_id from jobmst";
	$cond = "where job_del_flg = 'f' order by order_no";
	$sel = select_from_table($con, $sql, $cond, $fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}
	$jobs = array();
	for($i=0;$i<pg_numrows($sel);$i++){
		$job = pg_result($sel,$i,"job_id");
		array_push($jobs,$job);
	}
	return $jobs;
}


//**********職員名**********
function get_emp_name($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL50,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$lt_nm = pg_result($sel,0,"emp_kn_lt_nm");
	$ft_nm = pg_result($sel,0,"emp_kn_ft_nm");
	$name = "$lt_nm"." "."$ft_nm";
	return $name;
}

//**********職員名漢字**********
function get_emp_kanji_name($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL50,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$lt_nm = pg_result($sel,0,"emp_lt_nm");
	$ft_nm = pg_result($sel,0,"emp_ft_nm");
	$name = "$lt_nm"." "."$ft_nm";
	return $name;
}

//**********指定職種ID(配列)の職種名を配列で取得**********
function get_job_nm($con,$jobs,$fname){

	require("./conf/sql.inf");

	$job_nms = array();

	for($i=0;$i<count($jobs);$i++){
		$cond = "where job_id = '$jobs[$i]'";
		$sel = select_from_table($con,$SQL27,$cond,$fname);

		if($sel == 0){
			showErrorPage();
			exit;
		}
		$job_nm = pg_result($sel,0,"job_nm");
		array_push($job_nms,$job_nm);
	}
	return $job_nms;
}

//**********指定職員IDの部署IDと職種IDを配列で取得**********
function get_own_info($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL48,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$class = pg_result($sel,0,"emp_class");
	$job   = pg_result($sel,0,"emp_job");
	$info = array($class,$job);
	return $info;
}

//**********指定職員IDの職種IDを取得**********
function get_own_job($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL48,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$job   = pg_result($sel,0,"emp_job");

	return $job;
}


//**********職員情報をempmstから配列で取得**********
function get_empmst($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL50,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$info_array = pg_fetch_row($sel);
	$num = count($info_array);

	return $info_array;
}

//**********指定部門IDの部門名をclassmstから取得**********
function get_class_nm($con,$class_id,$fname){

	require("./conf/sql.inf");

	$cond = "where class_id = '$class_id'";
	$sel = select_from_table($con,$SQL52,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$class_nm = pg_result($sel,0,"class_nm");

	return $class_nm;
}

//**********指定課IDの課名をatrbmstから取得**********
function get_atrb_nm($con,$atrb_id,$fname){

	require("./conf/sql.inf");

	$cond = "where atrb_id = '$atrb_id'";
	$sel = select_from_table($con,$SQL53,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$atrb_nm = pg_result($sel,0,"atrb_nm");

	return $atrb_nm;
}

//**********指定科IDの科名をdeptmstから取得**********
function get_dept_nm($con,$dept_id,$fname){

	require("./conf/sql.inf");

	$cond = "where dept_id = '$dept_id'";
	$sel = select_from_table($con,$SQL54,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$dept_nm = pg_result($sel,0,"dept_nm");

	return $dept_nm;
}

//**********指定職種IDの職種名をjobmstから取得**********
function get_job_name($con,$job_id,$fname){

	require("./conf/sql.inf");

	$cond = "where job_id = '$job_id'";
	$sel = select_from_table($con,$SQL55,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$job_nm = pg_result($sel,0,"job_nm");

	return $job_nm;
}

//**********指定役職IDの役職名をstbmstから取得**********
function get_st_nm($con,$st_id,$fname){

	require("./conf/sql.inf");

	$cond = "where st_id = '$st_id'";
	$sel = select_from_table($con,$SQL56,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

	$st_nm = pg_result($sel,0,"st_nm");

	return $st_nm;
}

//**********指定都道府県IDの都道府県名取得**********
function get_province($province_id){
	$provinces = array("北海道","青森県","岩手県","宮城県","秋田県","山形県","福島県","茨城県","栃木県","群馬県","埼玉県","千葉県","東京都","神奈川県","新潟県","富山県","石川県","福井県","山梨県","長野県","岐阜県"	,"静岡県","愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県","鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県","高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県");
	$num = count($provinces);


	for($i = 0; $i < $num; $i++){
		if($province_id == $i){
			return $provinces[$i];
		}
	}

}

//**********権限情報を取得**********
function get_authmst($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL2,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

		$auth_array = pg_fetch_row($sel);
		$num = count($auth_array);

		return $auth_array;

}

//**********LOGIN ID情報を取得**********
function get_login_id($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond = "where emp_id = '$emp_id'";
	$sel = select_from_table($con,$SQL57,$cond,$fname);

	if($sel == 0){
		showErrorPage();
		exit;
	}

		$id = pg_result($sel,0,"emp_login_id");
		return $id;

}

//**********全科のリザルトをそのまま取得**********
function get_section($con,$session,$fname){

//ファイルの読み込み
require("./conf/sql.inf");

	$cond = "where sect_del_flg = 'f' order by sect_id";		//SQLの条件

	$sel_sect = select_from_table($con,$SQL28,$cond,$fname);		//診療科名一覧を取得

	if($sel_sect == 0){
		showErrorPage();
		exit;
	}
	return $sel_sect;

}

//**********bldg_nmを取得**********
function get_bldg_nm($con,$bldg_cd,$fname){

//ファイルの読み込み
require("./conf/sql.inf");

	$cond = "where bldg_cd ='$bldg_cd'";		//SQLの条件

	$sel_bldg = select_from_table($con,$SQL64,$cond,$fname);		//診療科名一覧を取得

	if($sel_bldg == 0){
		showErrorPage();
		exit;
	}
	$bldg_nm = pg_result($sel_bldg,0,"bldg_name");
	return $bldg_nm;

}

//**********ward_nmを取得**********
function get_ward_nm($con,$bldg_cd,$ward_cd,$fname){

	require("./conf/sql.inf");

	$cond = "where bldg_cd ='$bldg_cd' and ward_cd = '$ward_cd'";		//SQLの条件

	$sel_ward = select_from_table($con,$SQL93,$cond,$fname);		//診療科名一覧を取得

	if($sel_ward == 0){
		showErrorPage();
		exit;
	}
	$ward_nm = pg_result($sel_ward,0,"ward_name");
	return $ward_nm;

}


//**********指定職員IDの職種名を取得**********
function get_own_job_name($con,$emp_id,$fname){

	require("./conf/sql.inf");

	$cond="where emp_id='$emp_id'";
	$sel=select_from_table($con,$SQL48,$cond,$fname);
	if($sel==0){
		showErrorPage();
		exit;
	}

	$job=pg_result($sel,0,"emp_job");
	$select_jobmst="select * from jobmst where job_id='$job'";
	$result_select=pg_exec($con,$select_jobmst);
	if($result_select==0){
		showErrorPage();
		exit;
	}
	$job_nm=pg_result($result_select,0,"job_nm");
	return $job_nm;
}

?>
