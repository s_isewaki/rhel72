<?php
require_once('jnl_indicator.php');
require_once('jnl_indicator_holiday.php');
/**
 * 管理指標 データ取得用クラス
 */
class IndicatorData extends Indicator
{
    /**
     * IndicatorHoliday オブジェクト格納クラス
     * @var object IndicatorHoliday
     */
    var $holiday;

    /**
     * コンストラクタ
     * @access public
     * @param  array  $arr
     */
    function IndicatorData($arr)
    {
        parent::Indicator($arr);
        $this->holiday = new IndicatorHoliday();
    }

    /**
     * スペル間違えの関数
     * getDataList($arr) へ飛ばす。
     * @access protected
     * @param  string $sql
     * @return mixed
     */
    function getDateList($sql)
    {
        return $this->getDataList($sql);
//        $this->setSql($sql);
//        $pgResult = select_from_table($this->con, $sql, '', $this->fname);
//        if ($pgResult == 0) { return $this->rtnLoginPage(); }
//        return pg_fetch_all($pgResult);
    }

    /**
     * SQL 実行関数
     * $sql にクエリを書けば良いようにしてある。
     * @access protected
     * @param  string $sql
     * @return mixed
     */
    function getDataList($sql)
    {
        $this->setSql($sql);
        $pgResult = select_from_table($this->con, $sql, '', $this->fname);
        if ($pgResult == 0) { return $this->rtnLoginPage(); }
        return pg_fetch_all($pgResult);
    }

    /**
     * 病院施設リスト取得
     * @access public
     * @param  integer $facilityId
     * @return mixed
     */
    function getHospitalList($facilityId=null)
    {
        return $this->getFacility($facilityId, 1);
    }

    /**
     * 老人保健施設リスト取得
     * @access public
     * @param  integer $facilityId
     * @return mixed
     */
    function getHealthFacilities($facilityId=null)
    {
        return $this->getFacility($facilityId, 2);
    }

    function getPlantList($type)
    {
        $rtnArr  = array();
        $dataList = array();
        $dataList = $this->getFacility(null, $type);
        foreach ($dataList as $dataVal) { $rtnArr[$dataVal['facility_id']] = $dataVal['facility_name']; }
        return $rtnArr;
    }

    function getFacility($facilityId=null, $facilityType=null)
    {
        $sql = "SELECT jnl_facility_id AS facility_id, jnl_facility_name AS facility_name FROM jnl_facility_master WHERE jnl_facility_id IS NOT NULL";

        if (!is_null($facilityId))   { $sql .=  sprintf(" AND jnl_facility_id = '%d'",   $facilityId); }
        if (!is_null($facilityType)) { $sql .=  sprintf(" AND jnl_facility_type = '%d'", $facilityType); }

        $sql .=  " ORDER BY jnl_facility_id ASC ";
        return $this->getDateList($sql);
    }

    /**
     * 前年度初月から、表示年度末月までの年の配列を返す
     * @access protected
     * @param  integer   $lastYearMonth
     * @param  integer   $currentYearMonth
     * @return array
     */
    function getYearArr($lastYearMonth, $currentYearMonth)
    {
        $rtnArr         = array();
        $lastYearStart  = substr($lastYearMonth, 0, 4);
        $currentYearEnd = substr($currentYearMonth, 0, 4);
        for ($i=$lastYearStart;$i <= $currentYearEnd; $i++) { $rtnArr[$i] = $i; }
        return $rtnArr;
    }

    /**
     * 配列で渡された中身を一部を除いて加算する。
     * @access protected
     * @param  array     $arr
     * @return string
     */
    function calcAddForArray($arr)
    {
        $rtn = 0;
        if (!is_array($arr)) { return false; }
        foreach ($arr as $key => $val) {
            switch($key) {
				case 'year': case 'month': case 'facility_id': case 'status': case 'valid':case 'count':
                    break;

                default:
                    $rtn = bcadd($rtn, $val);
                    break;
            }
        }
        return $rtn;
    }

    /**
     * 配列で渡されたデータから、その月の日数でデータの平均値を取得する
     * @access protected
     * @param  array     $arr
     * @return string
     */
    function getAvgPatientCount($arr)
    {
        $lastDay = $this->getLastDay($arr['year'], $arr['month']);
        $total   = $this->calcAddForArray($arr);
        return bcdiv($total, $lastDay, 1);
    }

    /**
     * 日報に入力されている年を取得する
     * @return mixed
     */
    function getSelectYear()
    {
        $sql = "SELECT SUBSTR(regist_date, 0, 5) AS year FROM jnl_hospital_day GROUP BY SUBSTR(regist_date, 0, 5) ORDER BY SUBSTR(regist_date, 0, 5) ";
        return $this->getDateList($sql);
    }

    /**
     * 月の配列を取得する
     * @return array
     */
    function getSelectMonth()
    {
        $rtnArr = array();
        for ($i=1; $i <= 12; $i++) { $rtnArr[] = array('month' => $i); }
        return $rtnArr;
    }

    function getSelectDay($lastDay) {
        $rtnArr = array();
        for ($i=1; $i <= $lastDay; $i++) { $rtnArr[] = array('day' => $i); }
        return $rtnArr;
    }

    /**
     * 各画面毎に発行するクエリを実行する。
     * @access public
     * @param  integer $no
     * @param  integer $lastYearMonth
     * @param  integer $currentYearMonth
     * @param  array   $optionArr
     * @return mixed
     */
    function getDisplayDataList($no, $lastYearMonth, $currentYearMonth, $optionArr)
    {
		//取得する日付を直近年から指定年に変更
		if($optionArr != null)
		{
			$workYear = $optionArr["year"];
			$workCYear = $workYear + 1;
			$workLYear = $workYear - 1;
			$setLastYearMonth = $workLYear.substr($lastYearMonth,-2,2);
			$setCurrentYearMonth = $workCYear.substr($currentYearMonth,-2,2);
		}
		else
		{
			$setLastYearMonth = $lastYearMonth;
			$setCurrentYearMonth = $currentYearMonth;
		}
		
		
		
        $data = false;

        switch ($no) {
            case '1': # 外来延患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ga', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_ga', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '2': # 外来平均患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ga', $lastYearMonth, $currentYearMonth, true,true);
				$data = $this->getHospitalWardPatientCount('gokei_ga', $setLastYearMonth, $setCurrentYearMonth, true,true);
				break;

            case '3': # 全病棟延入院患者数
                //$data = $this->getAllHospitalWardPatientCount($lastYearMonth, $currentYearMonth);
				$data = $this->getAllHospitalWardPatientCount($setLastYearMonth, $setCurrentYearMonth);
				break;

            case '4': # 一般病棟延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ip', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_ip', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '5': # 障害患者病棟延入院患者数
				//$data = $this->getHospitalWardPatientCount('gokei_shog', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_shog', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '6': # 精精神一般病棟延患者数
                //$data = $this->getHospitalWardPatientCount('gokei_seip', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_seip', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '7': # 医療療養病棟延患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ir', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_ir', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '8': # 介護療養病棟延患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kaig', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_kaig', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '9': # 回復期リハ病棟延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kaif', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_kaif', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '10': # 亜急性期病床延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ak', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_ak', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '11': # 緩和ｹｱ病棟延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kan', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_kan', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '12': # ICU及びハイケアユニット延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_icu', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_icu', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '13': # 小児管理料延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_shoni', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_shoni', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '14': # 精神療養病棟延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_seir', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_seir', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '15': # 特殊疾患（2）延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_tok', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_tok', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '16': # 認知症病棟延入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_nin', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('gokei_nin', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '17': # 全病棟平均入院患者数
                //$data = $this->getAllHospitalWardPatientCount($lastYearMonth, $currentYearMonth, true);
				$data = $this->getAllHospitalWardPatientCount($setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '18': # 一般病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ip', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_ip', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '19': # 障害者病棟平均入院患者
				//$data = $this->getHospitalWardPatientCount('gokei_shog', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_shog', $setLastYearMonth, $setCurrentYearMonth, true);
                break;

            case '20': # 精神一般病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_seip', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_seip', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '21': # 医療療養病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ir', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_ir', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '22': # 介護療養病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kaig', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_kaig', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '23': # 回復期リハ病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kaif', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_kaif', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '24': # 亜急性期病床平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_ak', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_ak', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '25': # 緩和ｹｱ病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_kan', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_kan', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '26': # ICU管理料平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_icu', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_icu', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '27': # 小児管理料平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_shoni', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_shoni', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '28': # 精神療養病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_seir', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_seir', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '29': # 特殊疾患（2）平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_tok', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_tok', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '30': # 認知症病棟平均入院患者数
                //$data = $this->getHospitalWardPatientCount('gokei_nin', $lastYearMonth, $currentYearMonth, true);
				$data = $this->getHospitalWardPatientCount('gokei_nin', $setLastYearMonth, $setCurrentYearMonth, true);
				break;

            case '31': # 全病棟稼働率（稼働病床）
                //$data = $this->getAllHospitalWardBedCount($lastYearMonth, $currentYearMonth, "act", $this->getArrayValueByKeyForEmptyIsNull('year', $optionArr));
				$data = $this->getAllHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, "act", $this->getArrayValueByKeyForEmptyIsNull('year', $optionArr));
				break;

            case '32': # 全病棟稼働率（許可病床）
                //$data = $this->getAllHospitalWardBedCount($lastYearMonth, $currentYearMonth, "pmt", $this->getArrayValueByKeyForEmptyIsNull('year', $optionArr));
				$data = $this->getAllHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, "pmt", $this->getArrayValueByKeyForEmptyIsNull('year', $optionArr));
				break;

            case '33': # 一般病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth);
				break;

            case '34': # 障害者病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'shog');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'shog');
				break;

            case '35': # 精神一般病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'seip');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'seip');
				break;

            case '36': # 医療療養病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'ir');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'ir');
				break;

            case '37': # 介護療養病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'kaig');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'kaig');
				break;

            case '38': # 回復期リハ病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'kaif');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'kaif');
				break;

            case '39': # 亜急性期病床稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'ak');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'ak');
				break;

            case '40': # 緩和ｹｱ病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'kan');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'kan');
				break;

            case '41': # ICU管理料稼働率
                ///$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'icu');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'icu');
				break;

            case '42': # 小児管理料稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'shoni');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'shoni');
				break;

            case '43': # 精神療養病棟稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'seir');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'seir');
				break;

            case '44': # 特殊疾患(2)稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'tok');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'tok');
				break;

            case '45': # 認知症病棟(2)稼働率
                //$data = $this->getHospitalWardBedCount($lastYearMonth, $currentYearMonth, 'nin');
				$data = $this->getHospitalWardBedCount($setLastYearMonth, $setCurrentYearMonth, 'nin');
				break;

            case '46': # 全病棟入院患者件数
                //$data = $this->getHospitalWardPatientCount('nyuin', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('nyuin', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '47': # 全病棟退院患者件数
                //$data = $this->getHospitalWardPatientCount('taiin', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalWardPatientCount('taiin', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '48': # 一般病棟平均在院日数
                //$data = $this->getHospitalMonthPatient('gnr_ave_enter', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('gnr_ave_enter', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '49': # 新患者数
                //$data = $this->getHospitalMonthPatient('new_comer', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('new_comer', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '50': # 紹介患者数
                //$data = $this->getHospitalMonthPatient('intro_comer', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('intro_comer', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '51': # 救急依頼件数
                //$data = $this->getHospitalMonthPatient('f_aid_req', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('f_aid_req', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '52': # 救急受入率
                //$data = $this->getHospitalMonthPatientParsent('f_aid_rev', 'f_aid_req', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatientParsent('f_aid_rev', 'f_aid_req', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '53': # 救急入院患者数
                //$data = $this->getHospitalMonthPatient('f_aid_enter', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('f_aid_enter', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '54': # 救急入院率
                //$data = $this->getHospitalMonthPatientParsent('f_aid_enter', 'f_aid_rev', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatientParsent('f_aid_enter', 'f_aid_rev', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '55': # 診療時間外患者数
                //$data = $this->getHospitalMonthPatient('patient_overtime', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('patient_overtime', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '56': # 診療時間外患者数の内、時間外入院患者数
                //$data = $this->getHospitalMonthPatient('enter_overtime', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatient('enter_overtime', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '57': # 診療時間外入院率
                //$data = $this->getHospitalMonthPatientParsent('enter_overtime', 'patient_overtime', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthPatientParsent('enter_overtime', 'patient_overtime', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '58': # 紹介率
                //$data = $this->getIntroductionRate($lastYearMonth, $currentYearMonth);
				$data = $this->getIntroductionRate($setLastYearMonth, $setCurrentYearMonth);
				break;

            case '59': # 手術件数(全麻)
                //$data = $this->getHospitalMonthAction('ope_all_anes', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthAction('ope_all_anes', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '60': # 手術件数(腰麻)
                //$data = $this->getHospitalMonthAction('ope_waist_anes', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthAction('ope_waist_anes', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '61': # 手術件数(伝達・局麻)
                //$data = $this->getHospitalMonthAction('ope_part_anes', $lastYearMonth, $currentYearMonth);
				$data = $this->getHospitalMonthAction('ope_part_anes', $setLastYearMonth, $setCurrentYearMonth);
				break;

            case '62': # 病院別患者日報
                $data = $this->getPatientDailyReportAccordingToHospital($optionArr);
                break;

            case '63': # 病院別患者日報(シンプル版)
                $data = $this->getPatientDailySimpleReportAccordingToHospital($optionArr);
                break;

            case '64': # 患者月報(日報・日毎)
                $data = $this->getPatientMonthlyReportAccordingToDay($optionArr);
                break;

            case '65': case '66': # 病院別患者月報、病院別患者月報(前年比較)
                $data = $this->getPatientMonthlyReportAccordingToHospital($optionArr);
                break;

            case '67': # 外来診療科別患者月報
                $data = $this->getMonthsDetail($lastYearMonth, $currentYearMonth);
                break;

            case '68': # 外来診療科別患者月報(前年比較)
                $data = $this->getMonthsDetail($lastYearMonth, $currentYearMonth);
                break;

            case '71': # 病院月間報告書患者数総括
                $data = $this->getSummaryOfNumberOfHospitalMonthlyReportPatients($optionArr);
                break;

            case '72': # 病院月間報告書(行為件数)
                $data = $this->getHospitalMonthlyReport($optionArr);
                break;

            case '73': # 病院別患者日報(累積統計表)
                $data = $this->getPatientDailyReportAccomulationStatustics($optionArr);
                break;

            default:
                break;
        }

        return $data;
    }

    /**
     * 全病棟延入院患者数
     * @access protected
     * @param  integer   $lastYearMonth
     * @param  integer   $currentYearMonth
     * @param  bool      $avgFlg
     * @return array
     */
    function getAllHospitalWardPatientCount($lastYearMonth, $currentYearMonth, $avgFlg=false)
    {
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
/*
        $sql = "SELECT facility_id, year, month, COUNT(*) AS count, SUM(naika_ga) AS naika_ga, SUM(sinkei_ga) AS sinkei_ga, SUM(kokyu_ga) AS kokyu_ga, SUM(shoka_ga) AS shoka_ga"
                   .", SUM(junkanki_ga) AS junkanki_ga, SUM(shouni_ga) AS shouni_ga, SUM(geka_ga) AS geka_ga, SUM(seikei_ga) AS seikei_ga, SUM(keiseibi_ga) AS keiseibi_ga"
                   .", SUM(nou_ga) AS nou_ga, SUM(hifu_ga) AS hifu_ga, SUM(hinyo_ga) AS hinyo_ga, SUM(sanfu_ga) AS sanfu_ga, SUM(ganka_ga) AS ganka_ga, SUM(jibi_ga) AS jibi_ga"
                   .", SUM(touseki_ga) AS touseki_ga, SUM(seisin_ga) AS seisin_ga, SUM(sika_ga) AS sika_ga, SUM(hoshasen_ga) AS hoshasen_ga, SUM(masui_ga) AS masui_ga"
                   .", MIN(status) AS status, MIN(valid) AS valid "
               ." FROM (SELECT jhd.jnl_facility_id AS facility_id, SUBSTR(jhd.regist_date, 0, 5) AS year, SUBSTR(jhd.regist_date, 5, 2) AS month, COALESCE(jhd.naika_ga, 0) AS naika_ga"
                           .", COALESCE(jhd.sinkei_ga, 0) AS sinkei_ga, COALESCE(jhd.kokyu_ga, 0) AS kokyu_ga, COALESCE(jhd.shoka_ga, 0) AS shoka_ga"
                           .", COALESCE(jhd.junkanki_ga, 0) AS junkanki_ga, COALESCE(jhd.shouni_ga, 0) AS shouni_ga, COALESCE(jhd.geka_ga, 0) AS geka_ga"
                           .", COALESCE(jhd.seikei_ga, 0) AS seikei_ga, COALESCE(jhd.keiseibi_ga, 0) AS keiseibi_ga, COALESCE(jhd.nou_ga, 0) AS nou_ga, COALESCE(jhd.hifu_ga, 0) AS hifu_ga"
                           .", COALESCE(jhd.hinyo_ga, 0) AS hinyo_ga, COALESCE(jhd.sanfu_ga, 0) AS sanfu_ga, COALESCE(jhd.ganka_ga, 0) AS ganka_ga, COALESCE(jhd.jibi_ga, 0) AS jibi_ga"
                           .", COALESCE(jhd.touseki_ga, 0) AS touseki_ga, COALESCE(jhd.seisin_ga, 0) AS seisin_ga, COALESCE(jhd.sika_ga, 0) AS sika_ga"
                           .", COALESCE(jhd.hoshasen_ga, 0) AS hoshasen_ga, COALESCE(jhd.masui_ga, 0) AS masui_ga, jhd.jnl_status AS status"
                           .", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid "
                       ." FROM jnl_hospital_day jhd"
                      ." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f'";
*/
		$sql = "SELECT facility_id, year, month, COUNT(*) AS count"
			.",SUM(gokei_ip) AS gokei_ip
				, SUM(gokei_shog) AS gokei_shog
				, SUM(gokei_seip) AS gokei_seip
				, SUM(gokei_ir) AS gokei_ir
				, SUM(gokei_kaig) AS gokei_kaig
				, SUM(gokei_kaif) AS gokei_kaif
				, SUM(gokei_ak) AS gokei_ak
				, SUM(gokei_kan) AS gokei_kan
				, SUM(gokei_icu) AS gokei_icu
				, SUM(gokei_shoni) AS gokei_shoni
				, SUM(gokei_seir) AS gokei_seir
				, SUM(gokei_tok) AS gokei_tok
				, SUM(gokei_nin) AS gokei_nin"
			.", MIN(status) AS status, MIN(valid) AS valid "
			." FROM (SELECT jhd.jnl_facility_id AS facility_id, SUBSTR(jhd.regist_date, 0, 5) AS year, SUBSTR(jhd.regist_date, 5, 2) AS month" 
			.", COALESCE(jhd.gokei_ip, 0) AS gokei_ip, 
				COALESCE(jhd.gokei_shog, 0) AS gokei_shog, 
				COALESCE(jhd.gokei_seip, 0) AS gokei_seip, 
				COALESCE(jhd.gokei_ir, 0) AS gokei_ir, 
				COALESCE(jhd.gokei_kaig, 0) AS gokei_kaig, 
				COALESCE(jhd.gokei_kaif, 0) AS gokei_kaif, 
				COALESCE(jhd.gokei_ak, 0) AS gokei_ak, 
				COALESCE(jhd.gokei_kan, 0) AS gokei_kan, 
				COALESCE(jhd.gokei_icu, 0) AS gokei_icu, 
				COALESCE(jhd.gokei_shoni, 0) AS gokei_shoni, 
				COALESCE(jhd.gokei_seir, 0) AS gokei_seir, 
				COALESCE(jhd.gokei_tok, 0) AS gokei_tok, 
				COALESCE(jhd.gokei_nin, 0) AS gokei_nin"
			.", jhd.jnl_status AS status"
			.", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid "
			." FROM jnl_hospital_day jhd"
			." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f'";
		

        if (!is_null($lastYearMonth))    { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%d'", $lastYearMonth); }
        if (!is_null($currentYearMonth)) { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%d'", $currentYearMonth); }

        $sql .=       ") AS a"
              ." GROUP BY facility_id, year, month ";

        $tmpData  = $this->getDateList($sql);
        $nextData = array();
        if (is_array($tmpData)) {
            foreach ($tmpData as $tmpKey => $tmpVal) {
				
/*				
                $nextData[$tmpKey] = array(
                    'main_data'   => ($avgFlg==false)? $this->calcAddForArray($tmpVal) : $this->getAvgPatientCount($tmpVal),
                    'facility_id' => $tmpVal['facility_id'],
                    'year'        => $tmpVal['year'],
                    'month'       => $tmpVal['month'],
                    'status'      => $tmpVal['status'],
                    'valid'       => $tmpVal['valid'],
                    'count'       => $tmpVal['count'],
                );
*/

				$nextData[$tmpKey] = array(
						'main_data'   => $this->calcAddForArray($tmpVal) ,
						'facility_id' => $tmpVal['facility_id'],
						'year'        => $tmpVal['year'],
						'month'       => $tmpVal['month'],
						'status'      => $tmpVal['status'],
						'valid'       => $tmpVal['valid'],
						'count'       => $tmpVal['count'],
						);
            }
			
			

            $arrCnt  = 0;
            $rtnData = array();

            foreach ($nextData as $dataKey => $dataVal) {
                $facilityId = $dataVal['facility_id'];
                $month      = $dataVal['month'];
                $year       = $dataVal['year'];
                $count      = $dataVal['count'];
                $lastData   = array();
                $rowNum     = 0;
                $checkFlg   = false;

                if (is_array($rtnData)) {
                    foreach ($rtnData as $rtnKey => $rtnVal) {
                        if ($rtnVal['facility_id'] == $facilityId && $rtnVal['month'] == $month) {
                            $lastData = $rtnVal;
                            $rowNum   = $rtnKey;
                            $checkFlg = true;
                        }
                    }
                }

                $lastData['facility_id'] = $facilityId;
                $lastData['month']       = $month;
                foreach ($yearArr as $yearVal) {
                    if ($year == $yearVal) {
                        $lastData[$yearVal]           = $this->getArrValueByKey('main_data', $dataVal);
                        $lastData[$yearVal.'_status'] = $this->getArrValueByKey('status',    $dataVal);
                        $lastData[$yearVal.'_valid']  = $this->getArrValueByKey('valid',     $dataVal);
                        $lastData[$yearVal.'_count']  = $this->getArrValueByKey('count',     $dataVal);
                    }
                    else { if (is_null($this->getArrayValueByKeyForEmptyIsNull($yearVal, $lastData[$arrCnt]))) { $lastData[$yearVal] = 0; } }
                }
                if ($checkFlg === true) { $rtnData[$rowNum] = $lastData; }
                else { $rtnData[] = $lastData; }
            }
        }
        return $rtnData;
    }

    /**
     * 全病棟延入院患者数以外の入院患者数と、全病棟平均入院患者数以外の平均入院患者数の取得関数
     * @access protected
     * @param  string    $targetColumn
     * @param  integer   $lastYearMonth
     * @param  integer   $currentYearMonth
     * @param  bool      $avgFlg true  平均患者数, false 入院患者数
     * @param  bool      $holFlg true  外来平均患者数, false その他
     * @return array
     */
	function getHospitalWardPatientCount($targetColumn, $lastYearMonth, $currentYearMonth, $avgFlg=false,$holFlg=false)
    {




		$sql = "SELECT jhd.jnl_facility_id AS facility_id, SUBSTR(jhd.regist_date, 0, 5) AS year, SUBSTR(jhd.regist_date, 5, 2) AS month, SUBSTR(jhd.regist_date, 7, 2) AS day, jhd.regist_date"
			.", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE ".sprintf("COALESCE(jhd.%s, 0) END AS %s", $targetColumn, $targetColumn)
			.", jhd.jnl_status AS status, CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE 1 END AS valid "
			." FROM jnl_hospital_day jhd"
			." WHERE del_flg = 'f' AND jhd.jnl_status < 2";
		if (!is_null($lastYearMonth))    { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%d' " , $lastYearMonth); }
		if (!is_null($currentYearMonth)) { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%d' ", $currentYearMonth); }
		$sql .="ORDER BY facility_id, month, year, day ";
		
		
		$tmpData  = $this->getDateList($sql);
		$cnt = 0;
		$nextData = array();

		foreach ($tmpData as $tmpKey => $tmpVal) 
		{
			
			
			if($cnt == 0)
			{
				//初期データ設定
				
				$SetData[0]['facility_id']  = $tmpVal['facility_id'];
				$SetData[0]['month']  = $tmpVal['month'];
				$SetData[0][$tmpVal['year']]  = $tmpVal[$targetColumn];
				

				$SetData[0][$tmpVal['year'].'_valid']  = $tmpVal['valid'];
				$SetData[0][$tmpVal['year'].'_status'] = $tmpVal['status'];
				
				//対象日が平日かそれ以外なのかをチェック→平日であれば分母にするカウントに設定する
				if (($this->is_holiday($tmpVal['year'], $tmpVal['month'], $tmpVal['day']))  && ($holFlg))
				{
					//日曜・祝日であり、外来平均患者数の表示
					$SetData[0][$tmpVal['year'].'_count'] = 0;
				}
				else
				{
					//平日 or 固定
					$SetData[0][$tmpVal['year'].'_count'] = 1;
				}
				
				$cnt = $cnt + 1;
			}
			else
			{
				if($SetData[$cnt - 1]['facility_id'] == $tmpVal['facility_id'])
				{
				//前回のデータと比較してfacility_idが同じ場合
					
					if($SetData[$cnt - 1]['month'] == $tmpVal['month'])
					{

						//足し込み処理

						$SetData[$cnt - 1]['facility_id']  = $tmpVal['facility_id'];
						$SetData[$cnt - 1]['month']  = $tmpVal['month'];
						
						//足し込みデータがある場合はそのまま足し込み前回ループ分のデータがNULLであればゼロとしてみなす。
						if($SetData[$cnt - 1][$tmpVal['year']] == null)
						{
							$getShukeiData = 0;
						}
						else
						{
							$getShukeiData = $SetData[$cnt - 1][$tmpVal['year']];
						}
						
						
						$SetData[$cnt - 1][$tmpVal['year']]  = $tmpVal[$targetColumn] + $getShukeiData;
						
						//2件目以降はステータスの小さい方を設定するもしくは、NULLであれば設定する
						if(($SetData[$cnt - 1][$tmpVal['year'].'_valid'] > $tmpVal['valid']) || ($SetData[$cnt - 1][$tmpVal['year'].'_valid'] == NULL))
						{
							$SetData[$cnt - 1][$tmpVal['year'].'_valid'] = $tmpVal['valid'];
						}
						
						//2件目以降はステータスの小さい方を設定する
						if(($SetData[$cnt - 1][$tmpVal['year'].'_status'] > $tmpVal['status']) || ($SetData[$cnt - 1][$tmpVal['year'].'_status'] == NULL))
						{
							$SetData[$cnt - 1][$tmpVal['year'].'_status'] = $tmpVal['status'];
						}
						


						//対象日が平日かそれ以外なのかをチェック→平日であれば分母にするカウントに設定する
						if (($this->is_holiday($tmpVal['year'], $tmpVal['month'], $tmpVal['day']))  && ($holFlg))
						{
							//日曜・祝日であり、外来平均患者数の表示
						}
						else
						{
							//平日
							$SetData[$cnt - 1][$tmpVal['year'].'_count'] = $SetData[$cnt - 1][$tmpVal['year'].'_count'] + 1;
						}


				
					}
					else
					{
						//前回のデータと比較して月が違う場合は新規にデータ配列を作成する

						//新規作成
						
						
						$SetData[$cnt]['facility_id']  = $tmpVal['facility_id'];
						$SetData[$cnt]['month']  = $tmpVal['month'];
						$SetData[$cnt][$tmpVal['year']]  = $tmpVal[$targetColumn];
						
						
						$SetData[$cnt][$tmpVal['year'].'_valid']  = $tmpVal['valid'];
						$SetData[$cnt][$tmpVal['year'].'_status'] = $tmpVal['status'];

						
						//対象日が平日かそれ以外なのかをチェック→平日であれば分母にするカウントに設定する
						if (($this->is_holiday($tmpVal['year'], $tmpVal['month'], $tmpVal['day']))  && ($holFlg))
						{
							//日曜・祝日であり、外来平均患者数の表示
							$SetData[$cnt][$tmpVal['year'].'_count'] = 0;
						}
						else
						{
							//平日 or 固定
							$SetData[$cnt][$tmpVal['year'].'_count'] = 1;
						}

						$cnt = $cnt + 1;
					}
				}
				else
				{
				//前回のデータと比較してfacility_idが違う場合は新規にデータ配列を作成する
					//新規作成
					
					$SetData[$cnt]['facility_id']  = $tmpVal['facility_id'];
					$SetData[$cnt]['month']  = $tmpVal['month'];
					$SetData[$cnt][$tmpVal['year']]  = $tmpVal[$targetColumn];
					
					
					$SetData[$cnt][$tmpVal['year'].'_valid']  = $tmpVal['valid'];
					$SetData[$cnt][$tmpVal['year'].'_status'] = $tmpVal['status'];
					
					//対象日が平日かそれ以外なのかをチェック→平日であれば分母にするカウントに設定する
					if (($this->is_holiday($tmpVal['year'], $tmpVal['month'], $tmpVal['day']))  && ($holFlg))
					{
						//日曜・祝日であり、外来平均患者数の表示
						$SetData[$cnt][$tmpVal['year'].'_count'] = 0;
					}
					else
					{
						//平日 or 固定
						$SetData[$cnt][$tmpVal['year'].'_count'] = 1;
					}

					$cnt = $cnt + 1;
				}
				
				
				
				
//対象日が平日かそれ以外なのかをチェック→平日であれば分母にするカウントに設定する
if ($holFlg == true)
{
	
	//2009年度の過去データが一括で入力されている場合に正しい平均データを表示するために固定で設定する処理　ここから
	
    if (($tmpVal['year'] == '2009') && ($tmpVal['month'] == '04'))
	{
    	//２００９年度用暫定対処（2009年4月の土・平日は25日）
		$SetData[$cnt - 1][$tmpVal['year'].'_count'] = 25;
	}
	
    if (($tmpVal['year'] == '2009') && (($tmpVal['month'] == '05') || ($tmpVal['month'] == '09') || ($tmpVal['month'] == '11'))
    || ($tmpVal['year'] == '2010') && ($tmpVal['month'] == '02'))
	{
    	//２００９年度用暫定対処（2009年5,9,11月2010年2月の土・平日は23日）
		$SetData[$cnt - 1][$tmpVal['year'].'_count'] = 23;
	}
	
    if (($tmpVal['year'] == '2009') && 
    (($tmpVal['month'] == '06') || 
    ($tmpVal['month'] == '07')  ||
    ($tmpVal['month'] == '08')  ||
    ($tmpVal['month'] == '10')  ||
    ($tmpVal['month'] == '12') )||
     ($tmpVal['year'] == '2010') && ($tmpVal['month'] == '03')
    )
	{

    	//２００９年度用暫定対処（2009年6,7,8,10月2010年3月の土・平日は26日）
		$SetData[$cnt - 1][$tmpVal['year'].'_count'] = 26;
	}

    if (($tmpVal['year'] == '2010') && ($tmpVal['month'] == '01'))
	{
    	//２００９年度用暫定対処（2010年1月の土・平日は24日）
		$SetData[$cnt - 1][$tmpVal['year'].'_count'] = 24;
	}

	//2009年度の過去データが一括で入力されている場合に正しい平均データを表示するために固定で設定する処理　ここまで

}
				
				
				
			}
		}
		
		return $SetData;
		

		
		
		
/////////////////////////////////////////////////////////////////////////

/*
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);

        $sql = "SELECT facility_id, year, month, COUNT(regist_date) AS count, MIN(status) AS status, MIN(valid) AS valid, ".sprintf("SUM(%s) AS %s ", $targetColumn, $targetColumn)
               ." FROM (SELECT jhd.jnl_facility_id AS facility_id, SUBSTR(jhd.regist_date, 0, 5) AS year, SUBSTR(jhd.regist_date, 5, 2) AS month, jhd.regist_date"
                           .", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE ".sprintf("COALESCE(jhd.%s, 0) END AS %s", $targetColumn, $targetColumn)
                           .", jhd.jnl_status AS status, CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE 1 END AS valid "
                       ." FROM jnl_hospital_day jhd"
                      ." WHERE del_flg = 'f' AND jhd.jnl_status < 2";
        if (!is_null($lastYearMonth))    { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%d' " , $lastYearMonth); }
        if (!is_null($currentYearMonth)) { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%d' ", $currentYearMonth); }
        $sql .=       ") AS a "
               ."GROUP BY facility_id, year, month "
               ."ORDER BY facility_id, year, month ";

        $tmpData  = $this->getDateList($sql);
        $nextData = array();
        foreach ($tmpData as $tmpKey => $tmpVal) {
            $nextData[$tmpKey] = array(
                'main_data'   => $tmpVal[$targetColumn],
                'facility_id' => $tmpVal['facility_id'],
                'year'        => $tmpVal['year'],
                'month'       => $tmpVal['month'],
                'status'      => $tmpVal['status'],
                'valid'       => $tmpVal['valid'],
                'count'       => $tmpVal['count'],
            );
        }

        $arrCnt    = 0;
        $rtnData   = array();
        $rowNum    = 0;
        $ArrRowNum = 0;

        foreach ($nextData as $dataKey => $dataVal) {
            $lastData   = array();
            $facilityId = $dataVal['facility_id'];
            $month      = $dataVal['month'];
            $year       = $dataVal['year'];
            $status     = $dataVal['status'];
            $valid      = $dataVal['valid'];
            $count      = $dataVal['count'];
            $arrCheck   = false;

            if (is_array($rtnData)) {
                foreach ($rtnData as $rtnKey => $rtnVal) {
                    if ($rtnVal['facility_id'] == $facilityId && $rtnVal['month'] == $month) {
                        $lastData  = $rtnData[$rtnKey];
                        $ArrRowNum =  $rtnKey;
                        $arrCheck  = true;
                    }
                }
            }

            $lastData['facility_id'] = $facilityId;
            $lastData['month']       = $month;

            foreach ($yearArr as $yearVal) {
                if ($year == $yearVal) {
                    $lastData[$yearVal]           = $this->getArrValueByKey('main_data', $dataVal);
                    $lastData[$yearVal.'_valid']  = $valid;
                    $lastData[$yearVal.'_status'] = $status;
                    $lastData[$yearVal.'_count']  = $count;
                }
                else { if (is_null($this->getArrayValueByKeyForEmptyIsNull($yearVal, $lastData[$arrCnt]))) { $lastData[$yearVal] = 0; } }
            }

            if ($arrCheck) { $rtnData[$ArrRowNum] = $lastData; }
            else {
                $rtnData[] = $lastData;
                $rowNum++;
            }
        }
        return $rtnData;
*/
    }

    /**
     * 全病棟稼働率取得関数
     *
     * @access protected
     * @param  integer   $lastYearMonth
     * @param  integer   $currentYearMonth
     * @param  string    $type 'pmt' 許可病床, 'act' 移動病床
     * @param  integer   $targetYear
     * @return array
     */
    function getAllHospitalWardBedCount($lastYearMonth, $currentYearMonth, $type="pmt", $targetYear=null)
    {
		
		$sql = "SELECT jhd.jnl_facility_id,SUBSTR(jhd.regist_date, 5, 2) AS month"
			." FROM jnl_hospital_day jhd"
			." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f'";
		
		if (!is_null($lastYearMonth))    { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%d'", $lastYearMonth); }
		if (!is_null($currentYearMonth)) { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%d'", $currentYearMonth); }
		
		$sql .= " GROUP BY jhd.jnl_facility_id ,month";
		
		$tmpData  = $this->getDateList($sql);
		
		$strcData = array();
		
//出力用の構造体を作成する		
		for($i = 0; $i < count($tmpData); $i++)
		{
			$yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
			foreach ($yearArr as $yearVal) 
			{
				$strcData[$i]["facility_id"] = $tmpData[$i]["jnl_facility_id"];
				$strcData[$i]["month"] = $tmpData[$i]["month"];
				$strcData[$i][$yearVal] = $yearVal;
				$strcData[$i][$yearVal."_gokei"] = null;
				$strcData[$i][$yearVal."_act"] = null;
				$strcData[$i][$yearVal."_status"] = null;
				$strcData[$i][$yearVal."_valid"] = null;
				$strcData[$i][$yearVal."_count"] = null;
			}
		}

		for($i = 0; $i < count($tmpData); $i++)
		{
			$yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
			foreach ($yearArr as $yearVal) 
			{

				$sql = "SELECT jhd.jnl_facility_id,jhd.regist_date ,jnl_status , "
					."COALESCE(jhd.gokei_ip, 0) as gokei_ip , "
					."COALESCE(jhd.gokei_shog, 0) as gokei_shog ,  "
					."COALESCE(jhd.gokei_seip, 0) as gokei_seip ,  "
					."COALESCE(jhd.gokei_ir, 0) as gokei_ir , "
					."COALESCE(jhd.gokei_kaig, 0) as gokei_kaig , "
					."COALESCE(jhd.gokei_kaif, 0) as gokei_kaif , "
					."COALESCE(jhd.gokei_ak, 0) as gokei_ak , "
					."COALESCE(jhd.gokei_kan, 0) as gokei_kan , "
					."COALESCE(jhd.gokei_icu, 0) as gokei_icu , "
					."COALESCE(jhd.gokei_shoni, 0) as gokei_shoni , "
					."COALESCE(jhd.gokei_seir, 0)  as gokei_seir , "
					."COALESCE(jhd.gokei_tok, 0) as gokei_tok , "
					."COALESCE(jhd.gokei_nin, 0) as gokei_nin  "
					.", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid "
					." FROM jnl_hospital_day jhd"
					." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f' and jhd.jnl_facility_id=".$tmpData[$i]["jnl_facility_id"];
				$sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) = '%d'", $yearVal.$tmpData[$i]["month"]);
				
				$tmpData2  = $this->getDateList($sql);
				if(false != $tmpData2)
				{
					for($j = 0; $j < count($tmpData2); $j++)
					{
						
						if($type == "act")
						{
							$sql = "select COALESCE(ip_act, 0) as ip_act , 
									COALESCE(shog_act, 0) as shog_act ,
									COALESCE(seip_act, 0) as seip_act , 
									COALESCE(ir_act, 0) as ir_act ,  
									COALESCE(kaig_act, 0) as kaig_act ,  
									COALESCE(kaif_act, 0) as kaif_act , 
									COALESCE(ak_act, 0) as ak_act , 
									COALESCE(kan_act, 0) as kan_act , 
									COALESCE(icu_act, 0) as icu_act , 
									COALESCE(shoni_act, 0) as shoni_act , 
									COALESCE(seir_act, 0) as seir_act , 
									COALESCE(tok_act, 0) as tok_act , 
									COALESCE(nin_act, 0) as nin_act";
							$sql .= " from  jnl_hospital_bed ";
							$sql .= " where  assigned_date <=".$tmpData2[$j]["regist_date"];
							$sql .= " and jnl_facility_id =".$tmpData2[$j]["jnl_facility_id"];
							$sql .= " order by assigned_date desc";
						}
						else
						{
							$sql = "select COALESCE(ip_pmt, 0) as ip_pmt , 
									COALESCE(shog_pmt, 0) as shog_pmt , 
									COALESCE(seip_pmt, 0) as seip_pmt , 
									COALESCE(ir_pmt, 0) as ir_pmt , 
									COALESCE(kaig_pmt, 0) as kaig_pmt , 
									COALESCE(kaif_pmt, 0) as kaif_pmt , 
									COALESCE(ak_pmt, 0) as ak_pmt , 
									COALESCE(kan_pmt, 0) as kan_pmt , 
									COALESCE(icu_pmt, 0) as icu_pmt , 
									COALESCE(shoni_pmt, 0) as shoni_pmt , 
									COALESCE(seir_pmt, 0) as seir_pmt , 
									COALESCE(tok_pmt, 0) as tok_pmt , 
									COALESCE(nin_pmt, 0) as nin_pmt";
							$sql .= " from  jnl_hospital_bed ";
							$sql .= " where  assigned_date <=".$tmpData2[$j]["regist_date"];
							$sql .= " and jnl_facility_id =".$tmpData2[$j]["jnl_facility_id"];
							$sql .= " order by assigned_date desc";
						}
						$tmpData3  = $this->getDateList($sql);
						
						
						//月ごとのデータを設定
						$strcData[$i][$yearVal."_act"] = $strcData[$i][$yearVal."_act"] 
							+ $tmpData3["0"]["ip_".$type]
							+ $tmpData3["0"]["shog_".$type]
							+ $tmpData3["0"]["seip_".$type]
							+ $tmpData3["0"]["ir_".$type]
							+ $tmpData3["0"]["kaig_".$type]
							+ $tmpData3["0"]["kaif_".$type]
							+ $tmpData3["0"]["ak_".$type]
							+ $tmpData3["0"]["kan_".$type]
							+ $tmpData3["0"]["icu_".$type]
							+ $tmpData3["0"]["shoni_".$type]
							+ $tmpData3["0"]["seir_".$type]
							+ $tmpData3["0"]["tok_".$type]
							+ $tmpData3["0"]["nin_".$type];
						
						$strcData[$i][$yearVal."_gokei"] = $strcData[$i][$yearVal."_gokei"]  
							+ $tmpData2[$j]["gokei_ip"]
							+ $tmpData2[$j]["gokei_shog"]
							+ $tmpData2[$j]["gokei_seip"]
							+ $tmpData2[$j]["gokei_ir"]
							+ $tmpData2[$j]["gokei_kaig"]
							+ $tmpData2[$j]["gokei_kaif"]
							+ $tmpData2[$j]["gokei_ak"]
							+ $tmpData2[$j]["gokei_kan"]
							+ $tmpData2[$j]["gokei_icu"]
							+ $tmpData2[$j]["gokei_shoni"]
							+ $tmpData2[$j]["gokei_seir"]
							+ $tmpData2[$j]["gokei_tok"]
							+ $tmpData2[$j]["gokei_nin"];
						
						$strcData[$i][$yearVal."_count"] = count($tmpData2);
						
						if($j == 0)
						{
							//初期値は無条件で設定する
							$strcData[$i][$yearVal."_status"] = $tmpData2[$j]["jnl_status"];
							$strcData[$i][$yearVal."_valid"] = $tmpData2[$j]["valid"];
						}
						else
						{
							//2件目以降はステータスの小さい方を設定する
							if($strcData[$i][$yearVal."_status"] > $tmpData2[$j]["jnl_status"])
							{
								$strcData[$i][$yearVal."_status"] = $tmpData2[$j]["jnl_status"];
							}
							
							//2件目以降はステータスの小さい方を設定する
							if($strcData[$i][$yearVal."_valid"] > $tmpData2[$j]["jnl_valid"])
							{
								$strcData[$i][$yearVal."_valid"] = $tmpData2[$j]["jnl_valid"];
							}
						}
					}
				}
			}
		}
/*
		
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);

        $sql .= "SELECT facility_id , month";
        foreach ($yearArr as $yearVal) {
            $sql .= sprintf(", MAX(\"%04s\") AS \"%04s\""
                   .", MAX(\"%04s_gokei\") AS \"%04s_gokei\""
                   .", MAX(\"%04s_act\") AS \"%04s_act\""
                   .", MAX(\"%04s_status\") AS \"%04s_status\""
                   .", MAX(\"%04s_valid\") AS \"%04s_valid\""
                   .", MAX(\"%04s_count\") AS \"%04s_count\""
                   , $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal);
        }

        $sql .= " FROM (SELECT facility_id , month ";
        foreach ($yearArr as $yearVal) {
            $sql .= sprintf(
                    ", CASE WHEN year='%04s' THEN year ELSE NULL END AS \"%04s\""
                   .", CASE WHEN year='%04s' THEN gokei ELSE NULL END AS \"%04s_gokei\""
                   .", CASE WHEN year='%04s' THEN %s ELSE NULL END AS \"%04s_act\""
                   .", CASE WHEN year='%04s' THEN status ELSE NULL END AS \"%04s_status\""
                   .", CASE WHEN year='%04s' THEN valid ELSE NULL END AS \"%04s_valid\""
                   .", CASE WHEN year='%04s' THEN day_count ELSE NULL END AS \"%04s_count\""
                   , $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $type, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal, $yearVal);
        }

        $sql .= " FROM (SELECT jnl_facility_id AS facility_id, year, month, COUNT(day) AS day_count, SUM(act) AS act, SUM(pmt) AS pmt, SUM(gokei) AS gokei"
                           .", MAX(status) AS status, MIN(valid) AS valid"
                       ." FROM (SELECT newest_apply_id, jnl_facility_id, SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month"
                                   .", SUBSTR(regist_date, 7, 2) AS day, MAX(act) AS act, MAX(pmt) AS pmt, MAX(gokei) AS gokei, MAX(status) AS status, MIN(valid) AS valid"
                               ." FROM (SELECT newest_apply_id, jhd.jnl_facility_id, jhd.regist_date, jhb.assigned_date, jhb.act, jhb.pmt"
                                           .", (COALESCE(jhd.gokei_ip, 0) + COALESCE(jhd.gokei_shog, 0) + COALESCE(jhd.gokei_seip, 0) + COALESCE(jhd.gokei_ir, 0) + COALESCE(jhd.gokei_kaig, 0) + COALESCE(jhd.gokei_kaif, 0) + COALESCE(jhd.gokei_ak, 0) + COALESCE(jhd.gokei_kan, 0) + COALESCE(jhd.gokei_icu, 0) + COALESCE(jhd.gokei_shoni, 0) + COALESCE(jhd.gokei_seir, 0) + COALESCE(jhd.gokei_tok, 0) + COALESCE(jhd.gokei_nin, 0) ) AS gokei"
                                           .", jhd.jnl_status AS status, CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid"
                                       ." FROM jnl_hospital_day AS jhd"
                                      ." INNER JOIN (SELECT jnl_facility_id, to_number(assigned_date, '99999999') AS assigned_date"
                                                        .", (COALESCE(ip_act, 0) + COALESCE(shog_act, 0) + COALESCE(seip_act, 0) + COALESCE(ir_act, 0) + COALESCE(kaig_act, 0) + COALESCE(kaif_act, 0) + COALESCE(ak_act, 0) + COALESCE(kan_act, 0) + COALESCE(icu_act, 0) + COALESCE(shoni_act, 0) + COALESCE(seir_act, 0) + COALESCE(tok_act, 0) + COALESCE(nin_act, 0)) as act"
                                                        .", (COALESCE(ip_pmt, 0) + COALESCE(shog_pmt, 0) + COALESCE(seip_pmt, 0) + COALESCE(ir_pmt, 0) + COALESCE(kaig_pmt, 0) + COALESCE(kaif_pmt, 0) + COALESCE(ak_pmt, 0) + COALESCE(kan_pmt, 0) + COALESCE(icu_pmt, 0) + COALESCE(shoni_pmt, 0) + COALESCE(seir_pmt, 0) + COALESCE(tok_pmt, 0) + COALESCE(nin_pmt, 0)) as pmt"
                                                    ." FROM jnl_hospital_bed"
                                                  .") jhb"
                                              ." ON jhd.jnl_facility_id = jhb.jnl_facility_id"
                                             ." AND jhd.regist_date >= jhb.assigned_date"
                                      ." WHERE jnl_status < 2"
                                        ." AND del_flg = 'f'"
                                        ." AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%s' " , $lastYearMonth)
                                        ." AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%s' ", $currentYearMonth)
                                     .") AS a"
                              ." GROUP BY newest_apply_id, jnl_facility_id, regist_date"
                              ." ORDER BY jnl_facility_id, regist_date"
                             .") AS b"
                      ." GROUP BY jnl_facility_id, year, month"
                      ." ORDER BY jnl_facility_id, year, month"
                     .") AS c ) AS d GROUP BY facility_id, month";

        $rtnData = $this->getDateList($sql);
        return $rtnData;
		
*/
		return $strcData;
	}
	
	/**
     * 一般病棟稼働率、障害者病棟稼働率、精神一般病棟稼働率、医療療養病棟稼働率、介護療養病棟稼働率、回復期リハ病棟稼働率、亜急性期病床稼働率、
     * 緩和ケア病棟稼働率、ICU及びハイケアユニット稼働率、小児管理料稼働率、精神療養病棟稼働率、特殊疾患(2)稼働率、認知症病棟稼働率
     * @access protected
     * @param  integer   $lastYearMonth
     * @param  integer   $currentYearMonth
     * @param  string    $type
     * @return array
     */
    function getHospitalWardBedCount($lastYearMonth, $currentYearMonth, $type="ip")
    {

		$sql = "SELECT jhd.jnl_facility_id,SUBSTR(jhd.regist_date, 5, 2) AS month"
			." FROM jnl_hospital_day jhd"
			." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f'";
		
		if (!is_null($lastYearMonth))    { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%d'", $lastYearMonth); }
		if (!is_null($currentYearMonth)) { $sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) <= '%d'", $currentYearMonth); }
		
		$sql .= " GROUP BY jhd.jnl_facility_id ,month";
		
		$tmpData  = $this->getDateList($sql);
		
		$strcData = array();
		
		//出力用の構造体を作成する		
		for($i = 0; $i < count($tmpData); $i++)
		{
			$yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
			foreach ($yearArr as $yearVal) 
			{
				$strcData[$i]["facility_id"] = $tmpData[$i]["jnl_facility_id"];
				$strcData[$i]["month"] = $tmpData[$i]["month"];
				$strcData[$i][$yearVal] = $yearVal;
				$strcData[$i][$yearVal."_gokei"] = null;
				$strcData[$i][$yearVal."_act"] = null;
				$strcData[$i][$yearVal."_status"] = null;
				$strcData[$i][$yearVal."_valid"] = null;
				$strcData[$i][$yearVal."_count"] = null;
			}
		}
		
		for($i = 0; $i < count($tmpData); $i++)
		{
			$yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
			foreach ($yearArr as $yearVal) 
			{
				
				$sql = "SELECT jhd.jnl_facility_id,jhd.regist_date ,jnl_status , "
					."COALESCE(jhd.gokei_$type, 0) as gokei_$type  "
					.", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid "
					." FROM jnl_hospital_day jhd"
					." WHERE jhd.jnl_status < 2 AND jhd.del_flg = 'f' and jhd.jnl_facility_id=".$tmpData[$i]["jnl_facility_id"];
				$sql .= " AND ".sprintf("SUBSTR(jhd.regist_date, 0, 7) = '%d'", $yearVal.$tmpData[$i]["month"]);
				
				$tmpData2  = $this->getDateList($sql);
				if(false != $tmpData2)
				{
					for($j = 0; $j < count($tmpData2); $j++)
					{
						
						$sql = "select COALESCE(".$type."_act , 0) as ".$type."_act ";
						$sql .= " from  jnl_hospital_bed ";
						$sql .= " where  assigned_date <=".$tmpData2[$j]["regist_date"];
						$sql .= " and jnl_facility_id =".$tmpData2[$j]["jnl_facility_id"];
						$sql .= " order by assigned_date desc";

						$tmpData3  = $this->getDateList($sql);
						
						
						//月ごとのデータを設定
						$strcData[$i][$yearVal."_act"] = $strcData[$i][$yearVal."_act"] 
							+ $tmpData3["0"][$type."_act"];
						
						$strcData[$i][$yearVal."_gokei"] = $strcData[$i][$yearVal."_gokei"]  
							+ $tmpData2[$j]["gokei_$type"];
						
						$strcData[$i][$yearVal."_count"] = count($tmpData2);
						
						if($j == 0)
						{
							//初期値は無条件で設定する
							$strcData[$i][$yearVal."_status"] = $tmpData2[$j]["jnl_status"];
							$strcData[$i][$yearVal."_valid"] = $tmpData2[$j]["valid"];
						}
						else
						{
							//2件目以降はステータスの小さい方を設定する
							if($strcData[$i][$yearVal."_status"] > $tmpData2[$j]["jnl_status"])
							{
								$strcData[$i][$yearVal."_status"] = $tmpData2[$j]["jnl_status"];
							}
							
							//2件目以降はステータスの小さい方を設定する
							if($strcData[$i][$yearVal."_valid"] > $tmpData2[$j]["jnl_valid"])
							{
								$strcData[$i][$yearVal."_valid"] = $tmpData2[$j]["jnl_valid"];
							}
						}
					}
				}
			}
		}
		
		return $strcData;
		/*
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);
        $sql = "SELECT year, month, facility_id".sprintf(", gokei_%s, %s_act", $type, $type).", status, valid"
               ." FROM (SELECT SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month, jhb.jnl_facility_id AS facility_id, TO_NUMBER(jhd.jnl_status, '0') AS status"
                           .sprintf(", COALESCE(jhd.gokei_%s, 0) AS gokei_%s, COALESCE(jhb.%s_act, 0) AS %s_act", $type, $type, $type, $type)
                           .", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid"
                       ." FROM jnl_facility_master jfm"
                      ." INNER JOIN (SELECT b.jnl_facility_id, b.assigned_date, b.ip_pmt, b.ip_act, b.shog_pmt, b.shog_act, b.seip_pmt, b.seip_act, b.ir_pmt, b.ir_act, b.kaig_pmt"
                                        .", b.kaig_act, b.kaif_pmt, b.kaif_act, b.ak_pmt, b.ak_act, b.kan_pmt, b.kan_act, b.icu_pmt, b.icu_act, b.shoni_pmt, b.shoni_act, b.seir_pmt"
                                        .", b.seir_act, b.tok_pmt, b.tok_act, b.nin_pmt, b.nin_act"
                                    ." FROM (SELECT jnl_facility_id, MAX(assigned_date) AS assigned_date"
                                            ." FROM jnl_hospital_bed"
                                           ." GROUP BY jnl_facility_id"
                                           ." ORDER BY jnl_facility_id"
                                          .") AS a"
                                   ." INNER JOIN jnl_hospital_bed b"
                                           ." ON a.jnl_facility_id = b.jnl_facility_id"
                                          ." AND a.assigned_date = b.assigned_date"
                                  .") AS jhb"
                              ." ON jfm.jnl_facility_id = jhb.jnl_facility_id"
                      ." INNER JOIN jnl_hospital_day jhd"
                              ." ON jfm.jnl_facility_id = jhd.jnl_facility_id"
                      ." WHERE ".sprintf("SUBSTR(jhd.regist_date, 0, 7) >= '%s' AND SUBSTR(jhd.regist_date, 0, 7) <= '%s'" , $lastYearMonth, $currentYearMonth)
                        ." AND jhd.del_flg = 'f'"
                        ." AND jhd.jnl_status < 2"
                     .") AS c"
              ." ORDER BY facility_id ";

        $tmpData  = $this->getDateList($sql);
        $nextData = array();
        $rtnData  = array();
        $arrCount = 0;

        if (is_array($tmpData)) {
            foreach ($tmpData as $tmpKey => $tmpVal) {
                $nextData = array();
                $nextData['facility_id'] = $tmpVal['facility_id'];
                $nextData['month']       = $tmpVal['month'];
                $year                    = $tmpVal['year'];
                $arrCheck                = false;

                foreach ($rtnData as $rtnKey =>  $rtnVal) {
                    if ($nextData['facility_id'] == $rtnVal['facility_id'] && $nextData['month'] == $rtnVal['month']) {
                        $nextData = $rtnVal;
                        $arrCount = $rtnKey;
                        $arrCheck = true;
                    }
                }

                foreach ($yearArr as $yearVal) {
                    if ($yearVal == $year) {
                        $nextData[$yearVal.'_status'] = $tmpVal['status'];
                        $nextData[$yearVal.'_valid']  = $tmpVal['valid'];
                        $nextData[$yearVal.'_gokei']  = $nextData[$yearVal.'_gokei'] + $tmpVal['gokei_'.$type];
                        $nextData[$yearVal.'_act']    = $nextData[$yearVal.'_act'] + $tmpVal[$type.'_act'];
                        $nextData[$yearVal.'_count']  = $nextData[$yearVal.'_count'] + 1;
                    }
                }
                if ($arrCheck === true) { $rtnData[$arrCount] = $nextData; }
                else { $rtnData[] = $nextData; }
            }
        }
        return $rtnData;
*/		
		
		
    }

    /**
     * 一般病棟平均在院日数、新患者数、紹介患者数、救急依頼件数、救急入院患者数、診療時間外患者数、診療時間外患者数の内、時間外入院患者数
     * @param $column
     * @param $lastYearMonth
     * @param $currentYearMonth
     * @return unknown_type
     */
    function getHospitalMonthPatient($column, $lastYearMonth, $currentYearMonth)
    {
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);

        $sql = "SELECT facility_id, month";
        foreach ($yearArr as $year) {
            $sql .= sprintf(", SUM(\"%s\") AS \"%s\", MIN(\"%s_status\") AS \"%s_status\", MIN(\"%s_valid\") AS \"%s_valid\"", $year, $year, $year, $year, $year, $year);
        }

        $sql .= " FROM (SELECT facility_id, month";
        foreach ($yearArr as $year) {
            $sql .= sprintf(", CASE WHEN year = '%s' THEN %s ELSE NULL END AS \"%s\"", $year, $column, $year)
                   .sprintf(", CASE WHEN year = '%s' THEN status ELSE NULL END AS \"%s_status\"", $year, $year)
                   .sprintf(", CASE WHEN year = '%s' THEN valid ELSE NULL END AS \"%s_valid\"", $year, $year);
        }
        $sql .= "FROM (SELECT jhmp.jnl_facility_id AS facility_id, SUBSTR(jhmp.regist_date, 0, 5) AS year, SUBSTR(jhmp.regist_date, 5, 2) AS month, "
                            ."CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ".sprintf("ELSE COALESCE(jhmp.%s, 0) END AS %s, ", $column, $column)
                            ."TO_NUMBER(jhmp.jnl_status, '0 ') as status, CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE 1 END AS valid "
                       ."FROM jnl_hospital_month_patient jhmp "
                      ."WHERE SUBSTR(jhmp.regist_date, 0, 7) >=".sprintf(" '%06d' ", $lastYearMonth)
                        ."AND SUBSTR(jhmp.regist_date, 0, 7) <=".sprintf(" '%06d' ", $currentYearMonth)
						." AND jhmp.del_flg = 'f'  AND jhmp.jnl_status < 2"
			.") AS a "
             .") AS b GROUP BY facility_id, month ";

       return $this->getDateList($sql);
    }

    /**
     * 救急受入率
     * 救急入院率
     * 診療時間外入院率
     * @param string         $numerator
     * @param string         $denominator
     * @param string(yyyymm) $lastYearMonth
     * @param string(yyyymm) $currentYearMonth
     * @return Ambigous <multitype:, unknown>
     */
    function getHospitalMonthPatientParsent($numerator, $denominator, $lastYearMonth, $currentYearMonth)
    {
        $sql .= "SELECT jhmp.jnl_facility_id AS facility_id, SUBSTR(jhmp.regist_date, 0, 5) AS year, SUBSTR(jhmp.regist_date, 5, 2) AS month"
                    .", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ".sprintf("ELSE COALESCE(jhmp.%s, 0) END AS %s", $numerator, $numerator)
                    .", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ".sprintf("ELSE COALESCE(jhmp.%s, 0) END AS %s", $denominator, $denominator)
                    .", jhmp.jnl_status as status, CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE 1 END AS valid"
                ." FROM jnl_hospital_month_patient jhmp"
//20111004
//               ." INNER JOIN jnl_facility_register jfr"
               ." INNER JOIN jnl_facility_master jfr"
                       ." ON jhmp.del_flg = 'f' "
//                       ." ON jhmp.emp_id = jfr.emp_id"
                      ." AND jhmp.jnl_facility_id = jfr.jnl_facility_id"
//					." AND jhmp.del_flg = 'f' "
			." WHERE jhmp.jnl_facility_id IS NOT NULL and jhmp.jnl_status < 4 ";
        if (!is_null($lastYearMonth))    { $sql .= " AND SUBSTR(jhmp.regist_date, 0, 7) >=".sprintf(" '%06d' ", $lastYearMonth); }
        if (!is_null($currentYearMonth)) { $sql .= " AND SUBSTR(jhmp.regist_date, 0, 7) <=".sprintf(" '%06d' ", $currentYearMonth); }

        $tmpData  = $this->getDateList($sql);
        $nextData = array();
        foreach ($tmpData as $tmpKey => $tmpVal) {
            $arrNum     = null;
            $tmpArr     = array();
            $facilityId = $tmpVal['facility_id'];
            $month      = $tmpVal['month'];
            $year       = $tmpVal['year'];

            foreach ($nextData as $nextKey => $nextVal) {
                if ($facilityId == $nextVal['facility_id'] && $month == $nextVal['month']) {
                    $arrNum = $nextKey;
                    $tmpArr = $nextVal;
                }
            }

            $tmpArr['facility_id']   = $facilityId;
            $tmpArr['month']         = $month;
            $tmpArr[$year]           = round(($tmpVal[$numerator] / $tmpVal[$denominator]) * 100, 1);
            $tmpArr[$year.'_status'] = $tmpVal['status'];
            $tmpArr[$year.'_valid']  = $tmpVal['valid'];

            if (!is_null($arrNum)) { $nextData[$arrNum] = $tmpArr; }
            else { $nextData[] = $tmpArr; }
        }

        return $nextData;
    }

    /**
     * 紹介率
     */
    function getIntroductionRate($lastYearMonth, $currentYearMonth)
    {
		$sql .= "SELECT jhmp.jnl_facility_id AS facility_id, SUBSTR(jhmp.regist_date, 0, 5) AS year, SUBSTR(jhmp.regist_date, 5, 2) AS month"
			.", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE COALESCE(jhmp.intro_comer, 0) END AS intro_comer"
			.", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE COALESCE(jhmp.f_aid_rev, 0) END AS f_aid_rev"
			.", CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE COALESCE(jhmp.first_comer, 0) END AS first_comer"
			.", jhmp.jnl_status as status, CASE WHEN jnl_status = 2 THEN 0 WHEN jnl_status = 3 THEN 0 ELSE 1 END AS valid"
			." FROM jnl_hospital_month_patient jhmp"
//20111004
//			." INNER JOIN jnl_facility_register jfr"
			." INNER JOIN jnl_facility_master jfr"
//			." ON jhmp.emp_id = jfr.emp_id"
			." ON jhmp.del_flg = 'f'"
			." AND jhmp.jnl_facility_id = jfr.jnl_facility_id "
//			." AND jhmp.del_flg = 'f' "
			." WHERE jhmp.jnl_facility_id IS NOT NULL  AND jnl_status < 2 ";

        if (!is_null($lastYearMonth))    { $sql .= " AND SUBSTR(jhmp.regist_date, 0, 7) >=".sprintf(" '%d'", $lastYearMonth); }
        if (!is_null($currentYearMonth)) { $sql .= " AND SUBSTR(jhmp.regist_date, 0, 7) <=".sprintf(" '%d'", $currentYearMonth); }

       $tmpData  = $this->getDateList($sql);
       $nextData = array();

       foreach ($tmpData as $tmpKey => $tmpVal) {
           $firstComer = $tmpVal['first_comer'];
           $calc       = empty($firstComer)? 0 : round( ( ($tmpVal['intro_comer'] + $tmpVal['f_aid_rev']) / $firstComer) * 100, 1);
           $year       = $this->getArrValueByKey('year', $tmpVal);
           $nextData[$tmpKey] = array(
               'facility_id'   => $tmpVal['facility_id'],
               'month'         => $tmpVal['month'],
               $year           => $calc,
               $year.'_status' => $tmpVal['status'],
               $year.'_valid'  => $tmpVal['valid'],
           );
       }

       return $nextData;
    }

    /**
     * 手術件数(全麻)、手術件数(腰麻)、手術件数(伝達・局麻)
     * @param $column
     * @param $lastYearMonth
     * @param $currentYearMonth
     * @return unknown_type
     */
    function getHospitalMonthAction($column, $lastYearMonth, $currentYearMonth)
    {
        $yearArr = $this->getYearArr($lastYearMonth, $currentYearMonth);

        $sql =  "SELECT month, facility_id";

        foreach ($yearArr as $year) {
            $sql .= sprintf(", SUM(\"%s\") AS \"%s\", MIN(\"%s_status\") AS \"%s_status\", MIN(\"%s_valid\") AS \"%s_valid\"",   $year, $year, $year, $year, $year, $year);
        }

        $sql .=  " FROM (SELECT month, facility_id";

        foreach ($yearArr as $year) {
            $sql .= sprintf(", CASE WHEN year = '%s' THEN %s ELSE NULL END AS \"%s\"", $year, $column, $year)
                   .sprintf(", CASE WHEN year = '%s' THEN status ELSE NULL END AS \"%s_status\" ", $year, $year)
                   .sprintf(", CASE WHEN year = '%s' THEN valid ELSE NULL END AS \"%s_valid\" ",   $year, $year);
        }

		$sql .=  " FROM (SELECT SUBSTR(jhma.regist_date, 0, 5) AS year, SUBSTR(jhma.regist_date, 5, 2) AS month, jhma.jnl_facility_id AS facility_id, jhma.jnl_status AS status"
			.", CASE WHEN jhma.jnl_status = 2 THEN 0 WHEN jhma.jnl_status = 3 THEN 0 ELSE 1 END AS valid, jhma.newest_apply_id, jhma.emp_id, jhma.jnl_facility_id"
			.", jhma.jnl_status, jhma.regist_date, jhma.ope_all_anes, jhma.ope_waist_anes, jhma.ope_part_anes, jhma.ope_scope, jhma.ope_cataract, jhma.scope_stomach"
			.", jhma.scope_intestine, jhma.scope_etc, jhma.insp_ekg, jhma.insp_diogram, jhma.insp_treadmill, jhma.insp_heart, jhma.insp_stomach, jhma.insp_carotid"
			.", jhma.insp_lungs, jhma.insp_bonesolt, jhma.insp_gus, jhma.radi_gnr_photo, jhma.radi_man_mole, jhma.radi_mdl, jhma.radi_bem, jhma.radi_dip, jhma.radi_dic"
			.", jhma.radi_secial, jhma.radi_etc, jhma.radi_ct_brain, jhma.radi_ct_body, jhma.radi_ct_etc, jhma.radi_ct_add, jhma.radi_mri_brain, jhma.radi_mri_body"
			.", jhma.radi_mri_etc, jhma.radi_mri_add, jhma.radi_angio, jhma.fee_kid, jhma.fee_sprt_nrt, jhma.fee_care_breath, jhma.fee_plan_security"
			.", jhma.fee_plan_infection, jhma.fee_mng_sec_instrmnt, jhma.fee_guide_mng_md_one, jhma.fee_guide_mng_md_two, jhma.fee_guide_mng_md_three, jhma.del_flg"
			.", jhma.fee_orgnz_mng_sec_inf_md, jhma.empty_one, jhma.empty_two, jhma.empty_thee, jhma.empty_four, jhma.empty_five, jhma.empty_six, jhma.empty_seven"
			." FROM jnl_hospital_month_action jhma"
			." WHERE SUBSTR(jhma.regist_date, 0, 7) >=".sprintf(" '%d' ", $lastYearMonth)
			." AND SUBSTR(jhma.regist_date, 0, 7) <=".sprintf(" '%d' ", $currentYearMonth)
			." AND jhma.del_flg = 'f' and jhma.jnl_status < 2  "
						.") AS a ) AS b GROUP BY facility_id, month ORDER BY facility_id, month ";

        return $this->getDateList($sql);
    }

    /**
     * 患者月報(日報・日毎)
     * @param unknown_type $dateArr
     * @return unknown_type
     */
    function getPatientMonthlyReportAccordingToDay($dateArr)
    {
        $facilityId = $this->getArrValueByKey('facility_id', $dateArr);
        $year       = $this->getArrValueByKey('year',        $dateArr);
        $month      = $this->getArrValueByKey('month',       $dateArr);
        $date       = sprintf('%04d%02d', $year, $month);

        $sql ="SELECT main.assigned_date ,main.year, main.month, main.day, main.regist_date, main.facility_id, main.gokei_ga, main.patient_count, main.nyuin, main.taiin, main.status"
                  .", main.valid, main.gokei_ip, main.gokei_shog, main.gokei_seip, main.gokei_ir, main.gokei_kaig, main.gokei_kaif, main.gokei_ak, main.gokei_kan, main.gokei_icu"
                  .", main.gokei_shoni, main.gokei_seir, main.gokei_nin, main.gokei_tok, main.ip_act, main.shog_act, main.seip_act, main.ir_act, main.kaig_act, main.kaif_act, main.ak_act"
                  .", main.kan_act, main.icu_act, main.shoni_act, main.seir_act, main.tok_act, main.nin_act, main.ip_pmt, main.shog_pmt, main.seip_pmt, main.ir_pmt, main.kaig_pmt"
                  .", main.kaif_pmt, main.ak_pmt, main.kan_pmt, main.icu_pmt, main.shoni_pmt, main.seir_pmt, main.tok_pmt, main.nin_pmt, main.pmt_count, main.act_count"
              ." FROM (SELECT jhb.assigned_date ,year, month, day, jhd.regist_date, jhd.jnl_facility_id AS facility_id, jhd.gokei_ga, jhd.patient_count, jhd.nyuin, jhd.taiin"
                          .", jhd.jnl_status AS status, CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid, jhd.gokei_ip, jhd.gokei_shog, jhd.gokei_seip"
                          .", jhd.gokei_ir, jhd.gokei_kaig, jhd.gokei_kaif, jhd.gokei_ak, jhd.gokei_kan, jhd.gokei_icu, jhd.gokei_shoni, jhd.gokei_seir, jhd.gokei_nin, jhd.gokei_tok"
                          .", jhb.ip_act, jhb.shog_act, jhb.seip_act, jhb.ir_act, jhb.kaig_act, jhb.kaif_act, jhb.ak_act, jhb.kan_act, jhb.icu_act, jhb.shoni_act, jhb.seir_act, jhb.tok_act"
                          .", jhb.nin_act, jhb.ip_pmt, jhb.shog_pmt, jhb.seip_pmt, jhb.ir_pmt, jhb.kaig_pmt, jhb.kaif_pmt, jhb.ak_pmt, jhb.kan_pmt, jhb.icu_pmt, jhb.shoni_pmt, jhb.seir_pmt, jhb.tok_pmt"
                          .", jhb.nin_pmt, jhb.pmt_count, jhb.act_count"
                      ." FROM (SELECT (COALESCE(gokei_ip, 0) + COALESCE(gokei_shog, 0) + COALESCE(gokei_seip, 0) + COALESCE(gokei_ir, 0) + COALESCE(gokei_kaig, 0) + COALESCE(gokei_kaif, 0) + COALESCE(gokei_ak, 0) + COALESCE(gokei_kan, 0) + COALESCE(gokei_icu, 0) + COALESCE(gokei_shoni, 0) + COALESCE(gokei_seir,0) + COALESCE(gokei_tok,0) + COALESCE(gokei_nin,0)) AS patient_count"
                                  .", jnl_facility_id, COALESCE(gokei_ga, 0) AS gokei_ga, COALESCE(gokei_ip, 0) AS gokei_ip, COALESCE(gokei_shog, 0) AS gokei_shog, COALESCE(gokei_seip, 0) AS gokei_seip"
                                  .", COALESCE(gokei_ir, 0) AS gokei_ir, COALESCE(gokei_kaig, 0) AS gokei_kaig, COALESCE(gokei_kaif, 0) AS gokei_kaif, COALESCE(gokei_ak, 0) AS gokei_ak"
                                  .", COALESCE(gokei_kan, 0) AS gokei_kan, COALESCE(gokei_icu, 0) AS gokei_icu, COALESCE(gokei_shoni, 0) AS gokei_shoni, COALESCE(gokei_seir, 0) AS gokei_seir"
                                  .", COALESCE(gokei_tok, 0) AS gokei_tok, COALESCE(gokei_nin, 0) AS gokei_nin, COALESCE(nyuin, 0) AS nyuin, COALESCE(taiin, 0) AS taiin, jnl_status, regist_date"
                                  .", SUBSTR(regist_date, 0, 5) AS year, SUBSTR(regist_date, 5, 2) AS month, SUBSTR(regist_date, 7, 2) AS day"
                              ." FROM jnl_hospital_day"
                             ." WHERE del_flg = 'f' AND jnl_status < '2'"
                                .sprintf(" AND SUBSTR(regist_date, 0, 7) = '%d'",$date)
                            .") jhd"
                     ." INNER JOIN (SELECT a.assigned_date , a.jnl_facility_id, COALESCE(a.ip_act, 0) AS ip_act, COALESCE(a.shog_act, 0) AS shog_act, COALESCE(a.seip_act, 0) AS seip_act"
                                       .", COALESCE(a.ir_act, 0) AS ir_act, COALESCE(a.kaig_act, 0) AS kaig_act, COALESCE(a.kaif_act, 0) AS kaif_act, COALESCE(a.ak_act, 0) AS ak_act"
                                       .", COALESCE(a.kan_act, 0) AS kan_act, COALESCE(a.icu_act, 0) AS icu_act, COALESCE(a.shoni_act, 0) AS shoni_act, COALESCE(a.seir_act, 0) AS seir_act"
                                       .", COALESCE(a.tok_act, 0) AS tok_act, COALESCE(a.nin_act, 0) AS nin_act, COALESCE(a.ip_pmt, 0) AS ip_pmt, COALESCE(a.shog_pmt, 0) AS shog_pmt"
                                       .", COALESCE(a.seip_pmt, 0) AS seip_pmt, COALESCE(a.ir_pmt, 0) AS ir_pmt, COALESCE(a.kaig_pmt, 0) AS kaig_pmt, COALESCE(a.kaif_pmt, 0) AS kaif_pmt"
                                       .", COALESCE(a.ak_pmt, 0) AS ak_pmt, COALESCE(a.kan_pmt, 0) AS kan_pmt, COALESCE(a.icu_pmt, 0) AS icu_pmt, COALESCE(a.shoni_pmt, 0) AS shoni_pmt"
                                       .", COALESCE(a.seir_pmt, 0) AS seir_pmt, COALESCE(a.tok_pmt, 0) AS tok_pmt, COALESCE(a.nin_pmt, 0) AS nin_pmt"
                                       .", (COALESCE(a.ip_pmt, 0) + COALESCE(a.shog_pmt, 0) + COALESCE(a.seip_pmt, 0) + COALESCE(a.ir_pmt, 0) + COALESCE(a.kaig_pmt, 0) + COALESCE(a.kaif_pmt, 0) + COALESCE(a.ak_pmt, 0) + COALESCE(a.kan_pmt, 0) + COALESCE(a.icu_pmt, 0) + COALESCE(a.shoni_pmt, 0) + COALESCE(a.seir_pmt, 0) + COALESCE(a.tok_pmt, 0) + COALESCE(a.nin_pmt, 0)) AS pmt_count"
                                       .", (COALESCE(a.ip_act, 0) + COALESCE(a.shog_act, 0) + COALESCE(a.seip_act, 0) + COALESCE(a.ir_act, 0) + COALESCE(a.kaig_act, 0) + COALESCE(a.kaif_act, 0) + COALESCE(a.ak_act, 0) + COALESCE(a.kan_act, 0) + COALESCE(a.icu_act, 0) + COALESCE(a.shoni_act, 0) + COALESCE(a.seir_act, 0) + COALESCE(a.tok_act, 0) + COALESCE(a.nin_act, 0)) AS act_count"
                                   ." FROM jnl_hospital_bed a"
                                  ." INNER JOIN (SELECT jnl_facility_id, assigned_date"
                                                ." FROM jnl_hospital_bed"
                                                .sprintf(" WHERE SUBSTR(assigned_date, 0, 7) <= '%d'", $date)
                                               ." GROUP BY jnl_facility_id, assigned_date"
                                               ." ORDER BY jnl_facility_id ASC, assigned_date DESC"
                                              .") b"
                                          ." ON a.jnl_facility_id = b.jnl_facility_id"
                                         ." AND a.assigned_date = b.assigned_date"
                                 .") AS jhb"
                             ." ON jhd.jnl_facility_id = jhb.jnl_facility_id AND jhd.regist_date >= jhb.assigned_date ORDER BY year, month, day"
                    .") AS main"
             ." INNER JOIN (SELECT MAX(jhb.assigned_date) as assigned_date, jhd.regist_date, jhd.jnl_facility_id AS facility_id"
                           ." FROM (SELECT jnl_facility_id, regist_date"
                                   ." FROM jnl_hospital_day"
                                  ." WHERE del_flg = 'f' AND jnl_status < '2'"
                                  .sprintf(" AND SUBSTR(regist_date, 0, 7) = '%d'", $date)
                                 .") jhd"
                          ." INNER JOIN (SELECT a.assigned_date , a.jnl_facility_id"
                                        ." FROM jnl_hospital_bed a"
                                       ." INNER JOIN (SELECT jnl_facility_id, assigned_date"
                                                     ." FROM jnl_hospital_bed"
                                                    .sprintf(" WHERE SUBSTR(assigned_date, 0, 7) <= '%d'", $date)
                                                    ." GROUP BY jnl_facility_id, assigned_date"
                                                    ." ORDER BY jnl_facility_id ASC, assigned_date DESC"
                                                   .") b"
                                               ." ON a.jnl_facility_id = b.jnl_facility_id"
                                              ." AND a.assigned_date = b.assigned_date"
                                      .") AS jhb"
                                  ." ON jhd.jnl_facility_id = jhb.jnl_facility_id"
                                 ." AND jhd.regist_date >= jhb.assigned_date"
                               ." GROUP BY jhd.jnl_facility_id, jhd.regist_date"
                         .") AS sub"
                     ." ON main.facility_id = sub.facility_id"
                    ." AND main.regist_date = sub.regist_date"
                    ." AND main.assigned_date = sub.assigned_date"
                    .sprintf(" WHERE main.facility_id = %d ", $facilityId);

        return $this->getDateList($sql);
    }

    /**
     * 病院別患者月報 データ取得
     * @return unknown_type
     */
    function getPatientMonthlyReportAccordingToHospital($dateArr)
    {
        $currentYear = $this->getArrValueByKey('year',  $dateArr);
        $targetMonth = $this->getArrValueByKey('month', $dateArr);
        $lastYear    = $currentYear -1;

        $lastYearMonthStart    = date('Ymd', mktime(0, 0, 0, $targetMonth, 1, $lastYear));
        $lastYearMonthEnd      = date('Ymd', mktime(0, 0, 0, $targetMonth, $this->getLastDay($lastYear, $targetMonth), $lastYear));
        $currentYearMonthStart = date('Ymd', mktime(0, 0, 0, $targetMonth, 1, $currentYear));
        $currentYearMonthEnd   = date('Ymd', mktime(0, 0, 0, $targetMonth, $this->getLastDay($currentYear, $targetMonth), $currentYear));

        $cArr  = $this->getDepartment('column');
        $scArr = $this->getDepartmentType();

        $sql = "SELECT base.jnl_facility_id, base.year_type";

        foreach ($cArr as $cVal) { foreach ($scArr as $scVal) { $sql .= sprintf(", %s_%s", $cVal, $scVal); } }
        $sql .=", gokei_ga";
        foreach ($scArr as $scVal) { if ($scVal != 'ga') { $sql .= sprintf(", gokei_%s", $scVal); } }
        foreach ($scArr as $scVal) { if ($scVal != 'ga') { $sql .= sprintf(", %s_act, %s_pmt", $scVal, $scVal); } }

        $sql .=     ", SUBSTR(base.regist_date, 0, 5) as year, SUBSTR(base.regist_date, 5, 2) as month"
                   .", (COALESCE(ip_pmt, 0) + COALESCE(shog_pmt, 0) + COALESCE(seip_pmt, 0) + COALESCE(ir_pmt, 0) + COALESCE(kaig_pmt, 0) + COALESCE(kaif_pmt, 0) + COALESCE(ak_pmt, 0) + COALESCE(kan_pmt, 0) + COALESCE(icu_pmt, 0) + COALESCE(shoni_pmt, 0) + COALESCE(seir_pmt, 0) + COALESCE(tok_pmt, 0) + COALESCE(nin_pmt, 0)) AS pmt_count"
                   .", (COALESCE(ip_act, 0) + COALESCE(shog_act, 0) + COALESCE(seip_act, 0) + COALESCE(ir_act, 0) + COALESCE(kaig_act, 0) + COALESCE(kaif_act, 0) + COALESCE(ak_act, 0) + COALESCE(kan_act, 0) + COALESCE(icu_act, 0) + COALESCE(shoni_act, 0) + COALESCE(seir_act, 0) + COALESCE(tok_act, 0) + COALESCE(nin_act, 0)) AS act_count"
                   .", nyuin, taiin, '1' AS valid, jnl_status AS status"
               ." FROM (SELECT 'last' as year_type, newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, naika_ga, naika_ip, naika_shog, naika_seip, naika_ir, naika_kaig"
                           .", naika_kaif, naika_ak, naika_kan, naika_icu, naika_shoni, naika_seir, naika_tok, naika_nin, sinkei_ga, sinkei_ip, sinkei_shog, sinkei_seip, sinkei_ir"
                           .", sinkei_kaig, sinkei_kaif, sinkei_ak, sinkei_kan, sinkei_icu, sinkei_shoni, sinkei_seir, sinkei_tok, sinkei_nin, kokyu_ga, kokyu_ip, kokyu_shog, kokyu_seip"
                           .", kokyu_ir, kokyu_kaig, kokyu_kaif, kokyu_ak, kokyu_kan, kokyu_icu, kokyu_shoni, kokyu_seir, kokyu_tok, kokyu_nin, shoka_ga, shoka_ip, shoka_shog, shoka_seip"
                           .", shoka_ir, shoka_kaig, shoka_kaif, shoka_ak, shoka_kan, shoka_icu, shoka_shoni, shoka_seir, shoka_tok, shoka_nin, junkanki_ga, junkanki_ip, junkanki_shog"
                           .", junkanki_seip, junkanki_ir, junkanki_kaig, junkanki_kaif, junkanki_ak, junkanki_kan, junkanki_icu, junkanki_shoni, junkanki_seir, junkanki_tok, junkanki_nin"
                           .", shouni_ga, shouni_ip, shouni_shog, shouni_seip, shouni_ir, shouni_kaig, shouni_kaif, shouni_ak, shouni_kan, shouni_icu, shouni_shoni, shouni_seir"
                           .", shouni_tok, shouni_nin, geka_ga, geka_ip, geka_shog, geka_seip, geka_ir, geka_kaig, geka_kaif, geka_ak, geka_kan, geka_icu, geka_shoni, geka_seir, geka_tok"
                           .", geka_nin, seikei_ga, seikei_ip, seikei_shog, seikei_seip, seikei_ir, seikei_kaig, seikei_kaif, seikei_ak, seikei_kan, seikei_icu, seikei_shoni, seikei_seir"
                           .", seikei_tok, seikei_nin, keiseibi_ga, keiseibi_ip, keiseibi_shog, keiseibi_seip, keiseibi_ir, keiseibi_kaig, keiseibi_kaif, keiseibi_ak, keiseibi_kan"
                           .", keiseibi_icu, keiseibi_shoni, keiseibi_seir, keiseibi_tok, keiseibi_nin, nou_ga, nou_ip, nou_shog, nou_seip, nou_ir, nou_kaig, nou_kaif, nou_ak, nou_kan"
                           .", nou_icu, nou_shoni, nou_seir, nou_tok, nou_nin, hifu_ga, hifu_ip, hifu_shog, hifu_seip, hifu_ir, hifu_kaig, hifu_kaif, hifu_ak, hifu_kan, hifu_icu"
                           .", hifu_shoni, hifu_seir, hifu_tok, hifu_nin, hinyo_ga, hinyo_ip, hinyo_shog, hinyo_seip, hinyo_ir, hinyo_kaig, hinyo_kaif, hinyo_ak, hinyo_kan, hinyo_icu"
                           .", hinyo_shoni, hinyo_seir, hinyo_tok, hinyo_nin, sanfu_ga, sanfu_ip, sanfu_shog, sanfu_seip, sanfu_ir, sanfu_kaig, sanfu_kaif, sanfu_ak, sanfu_kan, sanfu_icu"
                           .", sanfu_shoni, sanfu_seir, sanfu_tok, sanfu_nin, ganka_ga, ganka_ip, ganka_shog, ganka_seip, ganka_ir, ganka_kaig, ganka_kaif, ganka_ak, ganka_kan, ganka_icu"
                           .", ganka_shoni, ganka_seir, ganka_tok, ganka_nin, jibi_ga, jibi_ip, jibi_shog, jibi_seip, jibi_ir, jibi_kaig, jibi_kaif, jibi_ak, jibi_kan, jibi_icu, jibi_shoni"
                           .", jibi_seir, jibi_tok, jibi_nin, touseki_ga, touseki_ip, touseki_shog, touseki_seip, touseki_ir, touseki_kaig, touseki_kaif, touseki_ak, touseki_kan"
                           .", touseki_icu, touseki_shoni, touseki_seir, touseki_tok, touseki_nin, seisin_ga, seisin_ip, seisin_shog, seisin_seip, seisin_ir, seisin_kaig, seisin_kaif"
                           .", seisin_ak, seisin_kan, seisin_icu, seisin_shoni, seisin_seir, seisin_tok, seisin_nin, sika_ga, sika_ip, sika_shog, sika_seip, sika_ir, sika_kaig, sika_kaif"
                           .", sika_ak, sika_kan, sika_icu, sika_shoni, sika_seir, sika_tok, sika_nin, hoshasen_ga, hoshasen_ip, hoshasen_shog, hoshasen_seip, hoshasen_ir, hoshasen_kaig"
                           .", hoshasen_kaif, hoshasen_ak, hoshasen_kan, hoshasen_icu, hoshasen_shoni, hoshasen_seir, hoshasen_tok, hoshasen_nin, masui_ga, masui_ip, masui_shog, masui_seip"
                           .", masui_ir, masui_kaig, masui_kaif, masui_ak, masui_kan, masui_icu, masui_shoni, masui_seir, masui_tok, masui_nin, gokei_ga, gokei_ip, gokei_shog, gokei_seip"
                           .", gokei_ir, gokei_kaig, gokei_kaif, gokei_ak, gokei_kan, gokei_icu, gokei_shoni, gokei_seir, gokei_tok, gokei_nin, nyuin, taiin, del_flg"
                       ." FROM jnl_hospital_day"
                      .sprintf(" WHERE regist_date >= '%s' AND regist_date <= '%s'", $lastYearMonthStart, $lastYearMonthEnd)
                      ." UNION"
                     ." SELECT 'current' AS year_type, newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, naika_ga, naika_ip, naika_shog, naika_seip, naika_ir, naika_kaig"
                           .", naika_kaif, naika_ak, naika_kan, naika_icu, naika_shoni, naika_seir, naika_tok, naika_nin, sinkei_ga, sinkei_ip, sinkei_shog, sinkei_seip, sinkei_ir"
                           .", sinkei_kaig, sinkei_kaif, sinkei_ak, sinkei_kan, sinkei_icu, sinkei_shoni, sinkei_seir, sinkei_tok, sinkei_nin, kokyu_ga, kokyu_ip, kokyu_shog, kokyu_seip"
                           .", kokyu_ir, kokyu_kaig, kokyu_kaif, kokyu_ak, kokyu_kan, kokyu_icu, kokyu_shoni, kokyu_seir, kokyu_tok, kokyu_nin, shoka_ga, shoka_ip, shoka_shog, shoka_seip"
                           .", shoka_ir, shoka_kaig, shoka_kaif, shoka_ak, shoka_kan, shoka_icu, shoka_shoni, shoka_seir, shoka_tok, shoka_nin, junkanki_ga, junkanki_ip, junkanki_shog"
                           .", junkanki_seip, junkanki_ir, junkanki_kaig, junkanki_kaif, junkanki_ak, junkanki_kan, junkanki_icu, junkanki_shoni, junkanki_seir, junkanki_tok, junkanki_nin"
                           .", shouni_ga, shouni_ip, shouni_shog, shouni_seip, shouni_ir, shouni_kaig, shouni_kaif, shouni_ak, shouni_kan, shouni_icu, shouni_shoni, shouni_seir, shouni_tok"
                           .", shouni_nin, geka_ga, geka_ip, geka_shog, geka_seip, geka_ir, geka_kaig, geka_kaif, geka_ak, geka_kan, geka_icu, geka_shoni, geka_seir, geka_tok, geka_nin"
                           .", seikei_ga, seikei_ip, seikei_shog, seikei_seip, seikei_ir, seikei_kaig, seikei_kaif, seikei_ak, seikei_kan, seikei_icu, seikei_shoni, seikei_seir, seikei_tok"
                           .", seikei_nin, keiseibi_ga, keiseibi_ip, keiseibi_shog, keiseibi_seip, keiseibi_ir, keiseibi_kaig, keiseibi_kaif, keiseibi_ak, keiseibi_kan, keiseibi_icu"
                           .", keiseibi_shoni, keiseibi_seir, keiseibi_tok, keiseibi_nin, nou_ga, nou_ip, nou_shog, nou_seip, nou_ir, nou_kaig, nou_kaif, nou_ak, nou_kan, nou_icu"
                           .", nou_shoni, nou_seir, nou_tok, nou_nin, hifu_ga, hifu_ip, hifu_shog, hifu_seip, hifu_ir, hifu_kaig, hifu_kaif, hifu_ak, hifu_kan, hifu_icu, hifu_shoni"
                           .", hifu_seir, hifu_tok, hifu_nin, hinyo_ga, hinyo_ip, hinyo_shog, hinyo_seip, hinyo_ir, hinyo_kaig, hinyo_kaif, hinyo_ak, hinyo_kan, hinyo_icu, hinyo_shoni"
                           .", hinyo_seir, hinyo_tok, hinyo_nin, sanfu_ga, sanfu_ip, sanfu_shog, sanfu_seip, sanfu_ir, sanfu_kaig, sanfu_kaif, sanfu_ak, sanfu_kan, sanfu_icu, sanfu_shoni"
                           .", sanfu_seir, sanfu_tok, sanfu_nin, ganka_ga, ganka_ip, ganka_shog, ganka_seip, ganka_ir, ganka_kaig, ganka_kaif, ganka_ak, ganka_kan, ganka_icu, ganka_shoni"
                           .", ganka_seir, ganka_tok, ganka_nin, jibi_ga, jibi_ip, jibi_shog, jibi_seip, jibi_ir, jibi_kaig, jibi_kaif, jibi_ak, jibi_kan, jibi_icu, jibi_shoni, jibi_seir"
                           .", jibi_tok, jibi_nin, touseki_ga, touseki_ip, touseki_shog, touseki_seip, touseki_ir, touseki_kaig, touseki_kaif, touseki_ak, touseki_kan, touseki_icu"
                           .", touseki_shoni, touseki_seir, touseki_tok, touseki_nin, seisin_ga, seisin_ip, seisin_shog, seisin_seip, seisin_ir, seisin_kaig, seisin_kaif, seisin_ak"
                           .", seisin_kan, seisin_icu, seisin_shoni, seisin_seir, seisin_tok, seisin_nin, sika_ga, sika_ip, sika_shog, sika_seip, sika_ir, sika_kaig, sika_kaif, sika_ak"
                           .", sika_kan, sika_icu, sika_shoni, sika_seir, sika_tok, sika_nin, hoshasen_ga, hoshasen_ip, hoshasen_shog, hoshasen_seip, hoshasen_ir, hoshasen_kaig"
                           .", hoshasen_kaif, hoshasen_ak, hoshasen_kan, hoshasen_icu, hoshasen_shoni, hoshasen_seir, hoshasen_tok, hoshasen_nin, masui_ga, masui_ip, masui_shog"
                           .", masui_seip, masui_ir, masui_kaig, masui_kaif, masui_ak, masui_kan, masui_icu, masui_shoni, masui_seir, masui_tok, masui_nin, gokei_ga, gokei_ip, gokei_shog"
                           .", gokei_seip, gokei_ir, gokei_kaig, gokei_kaif, gokei_ak, gokei_kan, gokei_icu, gokei_shoni, gokei_seir, gokei_tok, gokei_nin, nyuin, taiin, del_flg"
                       ." FROM jnl_hospital_day"
                      .sprintf(" WHERE regist_date >= '%s' AND regist_date <= '%s'", $currentYearMonthStart, $currentYearMonthEnd)
                     .") base"
              ." INNER JOIN (SELECT jnl_facility_id, assigned_date";

        foreach ($scArr as $scVal) { if ($scVal != 'ga') { $sql .= sprintf(", COALESCE(%s_act, 0) AS %s_act, COALESCE(%s_pmt, 0) AS %s_pmt", $scVal, $scVal, $scVal, $scVal); } }
        $sql .=              " FROM jnl_hospital_bed"
                           ." ORDER BY jnl_facility_id, assigned_date DESC"
                          .") sub"
                      ." ON base.jnl_facility_id = sub.jnl_facility_id AND base.regist_date >= sub.assigned_date"
              ." INNER JOIN (SELECT jnl_facility_id, regist_date, MAX(assigned_date) AS assigned_date"
                            ." FROM (SELECT jhb.*, jhd.regist_date"
                                    ." FROM jnl_hospital_day jhd INNER JOIN jnl_hospital_bed jhb ON jhd.jnl_facility_id = jhb.jnl_facility_id AND jhd.regist_date >= jhb.assigned_date"
                                  .") childcalc"
                           ." GROUP BY jnl_facility_id, regist_date ORDER BY jnl_facility_id"
                          .") calc"
                      ." ON base.jnl_facility_id = calc.jnl_facility_id AND base.regist_date = calc.regist_date AND sub.assigned_date = calc.assigned_date"
              ." WHERE base.jnl_status < 2 AND base.del_flg = 'f' ";

        $tmpData = $this->getDateList($sql);
        $rtnArr = array();

        foreach ($tmpData as $tmpVal) {
            $checkFlg = false;
            $tmpVal['day_count'] = 1;
            $nextData = $tmpVal;
            $keyCount = 0;
            $yearType = $tmpVal['year_type'];

            foreach ($rtnArr as $rtnKey => $rtnVal) {
                if ($tmpVal['jnl_facility_id'] == $rtnVal['jnl_facility_id'] && $yearType == $rtnVal['year_type']) {
                    foreach ($nextData as $nextKey => $nextVal) {
                        switch ($nextKey) {
                            case 'jnl_facility_id': case 'year_type': case 'year': case 'month': case 'valid':
                                break;

                            case 'status':
                                $nextData[$nextKey] = ($nextVal > $rtnVal[$nextKey])? $rtnVal[$nextKey] : $nextVal;
                                break;

                            default:
                                $nextData[$nextKey] = $nextVal + $rtnVal[$nextKey];
                                break;
                        }

                    }
                    $keyCount = $rtnKey;
                    $checkFlg = true;
                }
            }

            if ($checkFlg === true) { $rtnArr[$keyCount] = $nextData; }
            else { $rtnArr[] = $nextData; }
        }
        return $rtnArr;
    }

    /**
     * 病院別患者日報用データ取得
     */
    function getPatientDailyReportAccordingToHospital($dateArr)
    {
        $year  = $this->getArrValueByKey('year',  $dateArr);
        $month = $this->getArrValueByKey('month', $dateArr);
        $day   = $this->getArrValueByKey('day',   $dateArr);

        $date = sprintf('%04s%02s%02s', $year, $month, $day);

        $sql = "SELECT jhd.jnl_facility_id AS facility_id, jhd.gokei_ga, jhd.patient_count, jhd.nyuin, jhd.taiin, jhd.jnl_status AS status"
                   .", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid, jhd.gokei_ip, jhd.gokei_shog, jhd.gokei_seip, jhd.gokei_ir, jhd.gokei_kaig"
                   .", jhd.gokei_kaif, jhd.gokei_ak, jhd.gokei_kan, jhd.gokei_icu, jhd.gokei_shoni, jhd.gokei_seir, jhd.gokei_nin, jhd.gokei_tok, jhb.ip_act, jhb.shog_act, jhb.seip_act"
                   .", jhb.ir_act, jhb.kaig_act, jhb.kaif_act, jhb.ak_act, jhb.kan_act, jhb.icu_act, jhb.shoni_act, jhb.seir_act, jhb.tok_act, jhb.nin_act, jhb.pmt_count, jhb.act_count"
               ." FROM (SELECT (COALESCE(gokei_ip, 0) + COALESCE(gokei_shog, 0) + COALESCE(gokei_seip, 0) + COALESCE(gokei_ir, 0) + COALESCE(gokei_kaig,0) + COALESCE(gokei_kaif, 0) + COALESCE(gokei_ak, 0) + COALESCE(gokei_kan, 0) + COALESCE(gokei_icu, 0) + COALESCE(gokei_shoni,0) + COALESCE(gokei_seir,0) + COALESCE(gokei_tok,0) + COALESCE(gokei_nin,0)) AS patient_count"
                           .", jnl_facility_id, COALESCE(gokei_ga, 0) AS gokei_ga, COALESCE(gokei_ip, 0) AS gokei_ip, COALESCE(gokei_shog,0) AS gokei_shog"
                           .", COALESCE(gokei_seip,0) AS gokei_seip, COALESCE(gokei_ir,0) AS gokei_ir, COALESCE(gokei_kaig,0) AS gokei_kaig, COALESCE(gokei_kaif,0) AS gokei_kaif"
                           .", COALESCE(gokei_ak,0) AS gokei_ak, COALESCE(gokei_kan,0) AS gokei_kan, COALESCE(gokei_icu,0) AS gokei_icu , COALESCE(gokei_shoni,0) AS gokei_shoni"
                           .", COALESCE(gokei_seir,0) AS gokei_seir, COALESCE(gokei_tok,0) AS gokei_tok, COALESCE(gokei_nin,0) AS gokei_nin, COALESCE(nyuin, 0) AS nyuin"
                           .", COALESCE(taiin, 0) AS taiin, jnl_status"
                       ." FROM jnl_hospital_day"
                      ." WHERE del_flg = 'f'"
                        ." AND jnl_status < '2'"
                        .sprintf(" AND regist_date = '%s'", $date)
                     .") jhd "
               ."INNER JOIN (SELECT a.jnl_facility_id, COALESCE(a.ip_act, 0) AS ip_act, COALESCE(a.shog_act, 0) AS shog_act, COALESCE(a.seip_act, 0) AS seip_act"
                                .", COALESCE(a.ir_act, 0) AS ir_act, COALESCE(a.kaig_act, 0) AS kaig_act, COALESCE(a.kaif_act, 0) AS kaif_act, COALESCE(a.ak_act, 0) AS ak_act"
                                .", COALESCE(a.kan_act, 0) AS kan_act, COALESCE(a.icu_act, 0) AS icu_act, COALESCE(a.shoni_act, 0) AS shoni_act, COALESCE(a.seir_act, 0) AS seir_act"
                                .", COALESCE(a.tok_act, 0) AS tok_act, COALESCE(a.nin_act, 0) AS nin_act, (COALESCE(a.ip_pmt, 0) + COALESCE(a.shog_pmt, 0) + COALESCE(a.seip_pmt, 0)"
                                ." + COALESCE(a.ir_pmt, 0) + COALESCE(a.kaig_pmt, 0) + COALESCE(a.kaif_pmt, 0) + COALESCE(a.ak_pmt, 0) + COALESCE(a.kan_pmt, 0)"
                                ." + COALESCE(a.icu_pmt, 0) + COALESCE(a.shoni_pmt, 0) + COALESCE(a.seir_pmt, 0) + COALESCE(a.tok_pmt, 0) + COALESCE(a.nin_pmt, 0)) AS pmt_count"
                                .", (COALESCE(a.ip_act, 0) + COALESCE(a.shog_act, 0) + COALESCE(a.seip_act, 0) + COALESCE(a.ir_act, 0) + COALESCE(a.kaig_act, 0) + COALESCE(a.kaif_act, 0) + COALESCE(a.ak_act, 0) + COALESCE(a.kan_act, 0) + COALESCE(a.icu_act, 0) + COALESCE(a.shoni_act, 0) + COALESCE(a.seir_act, 0) + COALESCE(a.tok_act, 0) + COALESCE(a.nin_act, 0)) AS act_count"
                            ." FROM jnl_hospital_bed a "
                            ."INNER JOIN (SELECT jnl_facility_id, MAX(assigned_date) AS assigned_date"
                                         ." FROM jnl_hospital_bed"
                                        .sprintf(" WHERE assigned_date <= '%s'", $date)
                                        ." GROUP BY jnl_facility_id"
                                        ." ORDER BY jnl_facility_id ASC"
                                                .", assigned_date DESC"
                                       .") b"
                                   ." ON a.jnl_facility_id = b.jnl_facility_id"
                                  ." AND a.assigned_date = b.assigned_date"
                          .") AS jhb"
                      ." ON jhd.jnl_facility_id = jhb.jnl_facility_id ";

        return $this->getDateList($sql);
    }

    /**
     * 病院別患者日報(シンプル版)用データ取得
     */
    function getPatientDailySimpleReportAccordingToHospital($dateArr)
    {
        $year  = $this->getArrValueByKey('year',  $dateArr);
        $month = $this->getArrValueByKey('month', $dateArr);
        $day   = $this->getArrValueByKey('day',   $dateArr);

        $date = sprintf('%04s%02s%02s', $year, $month, $day);

        $sql = "SELECT jhd.jnl_facility_id AS facility_id, jhd.gokei_ga, jhd.patient_count, jhd.nyuin, jhd.taiin, jhd.jnl_status AS status"
                   .", CASE WHEN jhd.jnl_status = 2 THEN 0 WHEN jhd.jnl_status = 3 THEN 0 ELSE 1 END AS valid, jhb.pmt_count, jhb.act_count"
               ." FROM (SELECT (COALESCE(gokei_ip,0) + COALESCE(gokei_shog,0) + COALESCE(gokei_seip,0) + COALESCE(gokei_ir,0) + COALESCE(gokei_kaig,0) + COALESCE(gokei_kaif,0) + COALESCE(gokei_ak,0) + COALESCE(gokei_kan,0) + COALESCE(gokei_icu,0) + COALESCE(gokei_shoni,0) + COALESCE(gokei_seir,0) + COALESCE(gokei_tok,0) + COALESCE(gokei_nin,0)) AS patient_count"
                           .", jnl_facility_id, COALESCE(gokei_ga, 0) AS gokei_ga, nyuin, taiin, jnl_status"
                       ." FROM jnl_hospital_day"
                       ." WHERE del_flg = 'f' AND jnl_status < '2'"
                      .sprintf(" AND regist_date = '%d'", $date)
                     .") jhd"
              ." INNER JOIN (SELECT a.jnl_facility_id"
                                .", (COALESCE(a.ip_pmt, 0) + COALESCE(a.shog_pmt, 0) + COALESCE(a.seip_pmt, 0) + COALESCE(a.ir_pmt, 0) + COALESCE(a.kaig_pmt, 0) + COALESCE(a.kaif_pmt, 0) + COALESCE(a.ak_pmt, 0) + COALESCE(a.kan_pmt, 0) + COALESCE(a.icu_pmt, 0) + COALESCE(a.shoni_pmt, 0) + COALESCE(a.seir_pmt, 0) + COALESCE(a.tok_pmt, 0) + COALESCE(a.nin_pmt, 0)) AS pmt_count"
                                .", (COALESCE(a.ip_act, 0) + COALESCE(a.shog_act, 0) + COALESCE(a.seip_act, 0) + COALESCE(a.ir_act, 0) + COALESCE(a.kaig_act, 0) + COALESCE(a.kaif_act, 0) + COALESCE(a.ak_act, 0) + COALESCE(a.kan_act, 0) + COALESCE(a.icu_act, 0) + COALESCE(a.shoni_act, 0) + COALESCE(a.seir_act, 0) + COALESCE(a.tok_act, 0) + COALESCE(a.nin_act, 0)) AS act_count"
                            ." FROM jnl_hospital_bed a"
                           ." INNER JOIN (SELECT jnl_facility_id, MAX(assigned_date) AS assigned_date"
                                         ." FROM jnl_hospital_bed"
                                        .sprintf(" WHERE assigned_date <= '%s'", $date)
                                        ." GROUP BY jnl_facility_id"
                                        ." ORDER BY jnl_facility_id ASC"
                                                .", assigned_date DESC"
                                       .") b"
                                   ." ON a.jnl_facility_id = b.jnl_facility_id"
                                  ." AND a.assigned_date = b.assigned_date"
                          .") AS jhb"
                      ." ON jhd.jnl_facility_id = jhb.jnl_facility_id ";

        return $this->getDateList($sql);
    }

    /**
     * 病院別患者日報(累積統計表のデータ取得)
     * @param $dateArr
     * @return unknown_type
     */
    function getPatientDailyReportAccomulationStatustics($dateArr)
    {
        $targetYear  = $this->getArrValueByKey('year',  $dateArr);
        $targetMonth = $this->getArrValueByKey('month', $dateArr);
        $targetDay   = $this->getArrValueByKey('day',   $dateArr);

        $targetYearMonthStart = date('Ymd', mktime(0, 0, 0, $targetMonth, 1, $targetYear));
        $targetYearMonthEnd   = date('Ymd', mktime(0, 0, 0, $targetMonth, $targetDay, $targetYear));

        $sql = "SELECT base.jnl_facility_id , SUBSTR(base.regist_date, 0, 5) AS year, SUBSTR(base.regist_date, 5, 2) AS month, SUBSTR(base.regist_date, 7, 2) AS day"
                   .", COALESCE(gokei_ga, 0) AS gokei_ga"
                   .", (COALESCE(gokei_ip, 0) + COALESCE(gokei_shog, 0) + COALESCE(gokei_seip, 0) + COALESCE(gokei_ir, 0) + COALESCE(gokei_kaig, 0) + COALESCE(gokei_kaif, 0) + COALESCE(gokei_ak, 0) + COALESCE(gokei_kan, 0) + COALESCE(gokei_icu, 0) + COALESCE(gokei_shoni, 0) + COALESCE(gokei_seir, 0) + COALESCE(gokei_tok, 0) + COALESCE(gokei_nin, 0)) AS gokei_count"
                   .", (COALESCE(ip_pmt, 0) + COALESCE(shog_pmt, 0) + COALESCE(seip_pmt, 0) + COALESCE(ir_pmt, 0) + COALESCE(kaig_pmt, 0) + COALESCE(kaif_pmt, 0) + COALESCE(ak_pmt, 0) + COALESCE(kan_pmt, 0) + COALESCE(icu_pmt, 0) + COALESCE(shoni_pmt, 0) + COALESCE(seir_pmt, 0) + COALESCE(tok_pmt, 0) + COALESCE(nin_pmt, 0)) AS pmt_count"
                   .", (COALESCE(ip_act, 0) + COALESCE(shog_act, 0) + COALESCE(seip_act, 0) + COALESCE(ir_act, 0) + COALESCE(kaig_act, 0) + COALESCE(kaif_act, 0) + COALESCE(ak_act, 0) + COALESCE(kan_act, 0) + COALESCE(icu_act, 0) + COALESCE(shoni_act, 0) + COALESCE(seir_act, 0) + COALESCE(tok_act, 0) + COALESCE(nin_act, 0)) AS act_count"
                   .", COALESCE(nyuin, 0) AS nyuin, COALESCE(taiin, 0) AS taiin, '1' AS valid, jnl_status AS status"
               ." FROM (SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, naika_ga, naika_ip, naika_shog, naika_seip, naika_ir, naika_kaig, naika_kaif, naika_ak"
                           .", naika_kan, naika_icu, naika_shoni, naika_seir, naika_tok, naika_nin, sinkei_ga, sinkei_ip, sinkei_shog, sinkei_seip, sinkei_ir, sinkei_kaig, sinkei_kaif"
                           .", sinkei_ak, sinkei_kan, sinkei_icu, sinkei_shoni, sinkei_seir, sinkei_tok, sinkei_nin, kokyu_ga, kokyu_ip, kokyu_shog, kokyu_seip, kokyu_ir, kokyu_kaig"
                           .", kokyu_kaif, kokyu_ak, kokyu_kan, kokyu_icu, kokyu_shoni, kokyu_seir, kokyu_tok, kokyu_nin, shoka_ga, shoka_ip, shoka_shog, shoka_seip, shoka_ir, shoka_kaig"
                           .", shoka_kaif, shoka_ak, shoka_kan, shoka_icu, shoka_shoni, shoka_seir, shoka_tok, shoka_nin, junkanki_ga, junkanki_ip, junkanki_shog, junkanki_seip"
                           .", junkanki_ir, junkanki_kaig, junkanki_kaif, junkanki_ak, junkanki_kan, junkanki_icu, junkanki_shoni, junkanki_seir, junkanki_tok, junkanki_nin, shouni_ga"
                           .", shouni_ip, shouni_shog, shouni_seip, shouni_ir, shouni_kaig, shouni_kaif, shouni_ak, shouni_kan, shouni_icu, shouni_shoni, shouni_seir, shouni_tok"
                           .", shouni_nin, geka_ga, geka_ip, geka_shog, geka_seip, geka_ir, geka_kaig, geka_kaif, geka_ak, geka_kan, geka_icu, geka_shoni, geka_seir, geka_tok, geka_nin"
                           .", seikei_ga, seikei_ip, seikei_shog, seikei_seip, seikei_ir, seikei_kaig, seikei_kaif, seikei_ak, seikei_kan, seikei_icu, seikei_shoni, seikei_seir"
                           .", seikei_tok, seikei_nin, keiseibi_ga, keiseibi_ip, keiseibi_shog, keiseibi_seip, keiseibi_ir, keiseibi_kaig, keiseibi_kaif, keiseibi_ak, keiseibi_kan"
                           .", keiseibi_icu, keiseibi_shoni, keiseibi_seir, keiseibi_tok, keiseibi_nin, nou_ga, nou_ip, nou_shog, nou_seip, nou_ir, nou_kaig, nou_kaif, nou_ak, nou_kan"
                           .", nou_icu, nou_shoni, nou_seir, nou_tok, nou_nin, hifu_ga, hifu_ip, hifu_shog, hifu_seip, hifu_ir, hifu_kaig, hifu_kaif, hifu_ak, hifu_kan, hifu_icu"
                           .", hifu_shoni, hifu_seir, hifu_tok, hifu_nin, hinyo_ga, hinyo_ip, hinyo_shog, hinyo_seip, hinyo_ir, hinyo_kaig, hinyo_kaif, hinyo_ak, hinyo_kan, hinyo_icu"
                           .", hinyo_shoni, hinyo_seir, hinyo_tok, hinyo_nin, sanfu_ga, sanfu_ip, sanfu_shog, sanfu_seip, sanfu_ir, sanfu_kaig, sanfu_kaif, sanfu_ak, sanfu_kan, sanfu_icu"
                           .", sanfu_shoni, sanfu_seir, sanfu_tok, sanfu_nin, ganka_ga, ganka_ip, ganka_shog, ganka_seip, ganka_ir, ganka_kaig, ganka_kaif, ganka_ak, ganka_kan, ganka_icu"
                           .", ganka_shoni, ganka_seir, ganka_tok, ganka_nin, jibi_ga, jibi_ip, jibi_shog, jibi_seip, jibi_ir, jibi_kaig, jibi_kaif, jibi_ak, jibi_kan, jibi_icu"
                           .", jibi_shoni, jibi_seir, jibi_tok, jibi_nin, touseki_ga, touseki_ip, touseki_shog, touseki_seip, touseki_ir, touseki_kaig, touseki_kaif, touseki_ak"
                           .", touseki_kan, touseki_icu, touseki_shoni, touseki_seir, touseki_tok, touseki_nin, seisin_ga, seisin_ip, seisin_shog, seisin_seip, seisin_ir, seisin_kaig"
                           .", seisin_kaif, seisin_ak, seisin_kan, seisin_icu, seisin_shoni, seisin_seir, seisin_tok, seisin_nin, sika_ga, sika_ip, sika_shog, sika_seip, sika_ir"
                           .", sika_kaig, sika_kaif, sika_ak, sika_kan, sika_icu, sika_shoni, sika_seir, sika_tok, sika_nin, hoshasen_ga, hoshasen_ip, hoshasen_shog, hoshasen_seip"
                           .", hoshasen_ir, hoshasen_kaig, hoshasen_kaif, hoshasen_ak, hoshasen_kan, hoshasen_icu, hoshasen_shoni, hoshasen_seir, hoshasen_tok, hoshasen_nin, masui_ga"
                           .", masui_ip, masui_shog, masui_seip, masui_ir, masui_kaig, masui_kaif, masui_ak, masui_kan, masui_icu, masui_shoni, masui_seir, masui_tok, masui_nin"
                           .", gokei_ga, gokei_ip, gokei_shog, gokei_seip, gokei_ir, gokei_kaig, gokei_kaif, gokei_ak, gokei_kan, gokei_icu, gokei_shoni, gokei_seir, gokei_tok"
                           .", gokei_nin, nyuin, taiin, del_flg"
                       ." FROM jnl_hospital_day"
                      .sprintf(" WHERE regist_date >= '%08d' AND regist_date <= '%08d'", $targetYearMonthStart, $targetYearMonthEnd)
                     .") base"
              ." INNER JOIN (SELECT jnl_facility_id, assigned_date, COALESCE(ip_act, 0) AS ip_act, COALESCE(ip_pmt, 0) AS ip_pmt, COALESCE(shog_act, 0) AS shog_act"
                                .", COALESCE(shog_pmt, 0) AS shog_pmt, COALESCE(seip_act, 0) AS seip_act, COALESCE(seip_pmt, 0) AS seip_pmt, COALESCE(ir_act, 0) AS ir_act"
                                .", COALESCE(ir_pmt, 0) AS ir_pmt, COALESCE(kaig_act, 0) AS kaig_act, COALESCE(kaig_pmt, 0) AS kaig_pmt, COALESCE(kaif_act, 0) AS kaif_act"
                                .", COALESCE(kaif_pmt, 0) AS kaif_pmt, COALESCE(ak_act, 0) AS ak_act, COALESCE(ak_pmt, 0) AS ak_pmt, COALESCE(kan_act, 0) AS kan_act"
                                .", COALESCE(kan_pmt, 0) AS kan_pmt, COALESCE(icu_act, 0) AS icu_act, COALESCE(icu_pmt, 0) AS icu_pmt, COALESCE(shoni_act, 0) AS shoni_act"
                                .", COALESCE(shoni_pmt, 0) AS shoni_pmt, COALESCE(seir_act, 0) AS seir_act, COALESCE(seir_pmt, 0) AS seir_pmt, COALESCE(tok_act, 0) AS tok_act"
                                .", COALESCE(tok_pmt, 0) AS tok_pmt, COALESCE(nin_act, 0) AS nin_act, COALESCE(nin_pmt, 0) AS nin_pmt"
                            ." FROM jnl_hospital_bed"
                           ." ORDER BY jnl_facility_id, assigned_date DESC"
                          .") sub"
                      ." ON base.jnl_facility_id = sub.jnl_facility_id"
                     ." AND base.regist_date >= sub.assigned_date"
              ." INNER JOIN (SELECT jnl_facility_id, regist_date, MAX(assigned_date) AS assigned_date"
                            ." FROM (SELECT jhb.jnl_facility_id, jhb.assigned_date, jhb.ip_pmt, jhb.ip_act, jhb.shog_pmt, jhb.shog_act, jhb.seip_pmt, jhb.seip_act, jhb.ir_pmt, jhb.ir_act"
                                        .", jhb.kaig_pmt, jhb.kaig_act, jhb.kaif_pmt, jhb.kaif_act, jhb.ak_pmt, jhb.ak_act, jhb.kan_pmt, jhb.kan_act, jhb.icu_pmt, jhb.icu_act, jhb.shoni_pmt"
                                        .", jhb.shoni_act, jhb.seir_pmt, jhb.seir_act, jhb.tok_pmt, jhb.tok_act, jhb.nin_pmt, jhb.nin_act, jhd.regist_date"
                                    ." FROM jnl_hospital_day jhd"
                                   ." INNER JOIN jnl_hospital_bed jhb"
                                           ." ON jhd.jnl_facility_id = jhb.jnl_facility_id"
                                          ." AND jhd.regist_date >= jhb.assigned_date"
                                  .") childcalc"
                           ." GROUP BY jnl_facility_id, regist_date"
                           ." ORDER BY jnl_facility_id"
                          .") calc"
                      ." ON base.jnl_facility_id = calc.jnl_facility_id"
                     ." AND base.regist_date = calc.regist_date"
                     ." AND sub.assigned_date = calc.assigned_date"
                   ." WHERE base.jnl_status < 2"
                     ." AND base.del_flg = 'f'"
                   ." ORDER BY jnl_facility_id, year, month, day ";

        $data    = $this->getDateList($sql);
        $tmpData = array();
        $rtnData = array();
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $facilityId = $this->getArrValueByKey('jnl_facility_id', $val);
                $val['day_count']       = 1;
                $val['day_count_no_holiday'] = 1;
                $val['sum_gokei_ga']    = $val['gokei_ga'];
                $val['sum_gokei_count'] = $val['gokei_count'];
                $val['sum_act_count']   = $val['act_count'];
                $val['sum_pmt_count']   = $val['pmt_count'];
                $val['sum_nyuin']       = $val['nyuin'];
                $val['sum_taiin']       = $val['taiin'];
                if ($targetDay != $val['day']) {
                    $val['gokei_ga']    = null;
                    $val['gokei_count'] = null;
                    $val['act_count']   = null;
                    $val['pmt_count']   = null;
                    $val['nyuin']       = null;
                    $val['taiin']       = null;
                }
                $arrKey = null;
                foreach ($tmpData as $tmpKey => $tmpVal) {
                    if ($facilityId == $this->getArrValueByKey('jnl_facility_id', $tmpVal)) {
                        foreach ($tmpVal as $tmpValKey => $tmpValVal) {
                            switch ($tmpValKey) {
                                case 'jnl_facility_id': case 'year': case 'month':
                                    break;

                                case 'day': case 'gokei_ga': case 'gokei_count': case 'act_count': case 'pmt_count': case 'nyuin': case 'taiin':
                                    if ($targetDay == $tmpVal['day']) { $val[$tmpValKey] = $tmpValVal; }
                                    break;

                                case 'valid': case 'status':
                                    $val[$tmpValKey] = ($val[$tmpValKey] <= $tmpValVal)? $val[$tmpValKey] : $tmpValVal;
                                    break;

                                case 'day_count':
                                    $val[$tmpValKey] = $val[$tmpValKey] + $tmpValVal;
                                    break;

                                case 'day_count_no_holiday':
                                    if (!$this->is_holiday($val['year'], $val['month'], $val['day'])) { $val[$tmpValKey] = $val[$tmpValKey] + $tmpValVal; }
                                    else { $val[$tmpValKey] = $tmpValVal; }
                                    break;

                                default:
                                    $val[$tmpValKey] = $val[$tmpValKey] + $tmpValVal;
                                    break;
                            }
                        }
                        $arrKey = $tmpKey;
                    }
                }

                if (!is_null($arrKey)) { $tmpData[$arrKey] = $val; }
                else { $tmpData[] = $val; }
            }
//            if (is_array($tmpData)) { foreach ($tmpData as $tmpVal) { $tmpVal['holiday_count'] = ''; } }
        }

        return $tmpData;
    }

    /**
     * 病院月間報告書患者数総括のデータ取得
     * @param $dateArr
     * @return array $rtnArr
     */
    function getSummaryOfNumberOfHospitalMonthlyReportPatients($dateArr)
    {
        $year  = $this->getArrValueByKey('year',  $dateArr);
        $month = $this->getArrValueByKey('month', $dateArr);

        $mSql = "SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, gnr_ave_enter AS dispnum_297, gnr_new_enter_gnr AS dispnum_298, gnr_dpc_total AS dispnum_299"
                    .", gnr_ave_need_care AS dispnum_300, gnr_one_fourteen AS dispnum_301, gnr_fifteen_thirty AS dispnum_302, gnr_emgc_mng AS dispnum_303, gnr_over_ninety AS dispnum_304"
                    .", gnr_sp_ent_total AS dispnum_305, hdc_one_fourteen AS dispnum_306, hdc_fifteen_thirty AS dispnum_307, hdc_over_ninety AS dispnum_308,hdc_sp_ent_total AS dispnum_309"
                    .", rhb_ave_seriuos AS dispnum_310, rhb_ave_return AS dispnum_311, trt_a AS dispnum_312, trt_b AS dispnum_313, trt_c AS dispnum_314, trt_d AS dispnum_315"
                    .", trt_e AS dispnum_316, trt_f AS dispnum_317, trt_g AS dispnum_318, trt_h AS dispnum_319, trt_i AS dispnum_320, trt_one_fourteen AS dispnum_321"
                    .", trt_care_five AS dispnum_322, trt_care_four AS dispnum_323, trt_care_three AS dispnum_324, trt_care_two AS dispnum_325, trt_care_one AS dispnum_326"
                    .", new_comer AS dispnum_327, first_comer AS dispnum_328, intro_comer AS dispnum_329, f_aid_req AS dispnum_330, f_aid_rev AS dispnum_331, f_aid_enter AS dispnum_332"
                    .", patient_overtime AS dispnum_333, enter_overtime AS dispnum_334, empty_one AS dispnum_335, empty_two AS dispnum_336, empty_thee AS dispnum_337"
                    .", empty_four AS dispnum_338, empty_five AS dispnum_339, empty_six AS dispnum_340, empty_seven AS dispnum_341, del_flg, jnl_status AS status"
                ." FROM jnl_hospital_month_patient"
               ." WHERE jnl_status < 2 AND del_flg = 'f'"
                 ." AND".sprintf(" SUBSTR(regist_date, 0, 5) = '%04d'", $year)
                 ." AND".sprintf(" SUBSTR(regist_date, 5, 2) = '%02d'", $month)
               ." ORDER BY jnl_facility_id ";

        $monthly_data = $this->getDateList($mSql);

        $arr = $this->getDepartmentType();
        $arr[] = 'ga';
        $this->setDepartmentType($arr);
        $daily_data = $this->getPatientMonthlyReportAccordingToHospital(array('year' => $year, 'month' => $month));
        $rtnArr = array('monthly_data' => $monthly_data, 'daily_data' => $daily_data);
        return $rtnArr;
    }

    /**
     * 病院月間報告書(行為件数)のデータ取得
     * @param $dateArr
     * @return unknown_type
     */
    function getHospitalMonthlyReport($dateArr)
    {
        $year       = $this->getArrValueByKey('year',        $dateArr);
        $month      = $this->getArrValueByKey('month',       $dateArr);
        $facilityId = $this->getArrValueByKey('facility_id', $dateArr);

        $sql = "SELECT newest_apply_id, emp_id, jnl_facility_id, jnl_status, regist_date, ope_all_anes AS dispnum_342, ope_waist_anes AS dispnum_343, ope_part_anes AS dispnum_344"
                   .", ope_scope AS dispnum_345, ope_cataract AS dispnum_346, scope_stomach AS dispnum_347, scope_intestine AS dispnum_348, scope_etc AS dispnum_349"
                   .", insp_ekg AS dispnum_350, insp_diogram AS dispnum_351, insp_treadmill AS dispnum_352, insp_heart AS dispnum_353, insp_stomach AS dispnum_354"
                   .", insp_carotid AS dispnum_355, insp_lungs AS dispnum_356, insp_bonesolt AS dispnum_357, insp_gus AS dispnum_358, radi_gnr_photo AS dispnum_359"
                   .", radi_man_mole AS dispnum_360, radi_mdl AS dispnum_361, radi_bem AS dispnum_362, radi_dip AS dispnum_363, radi_dic AS dispnum_364, radi_secial AS dispnum_365"
                   .", radi_etc AS dispnum_366, radi_ct_brain AS dispnum_367, radi_ct_body AS dispnum_368, radi_ct_etc AS dispnum_369, radi_ct_add AS dispnum_370"
                   .", radi_mri_brain AS dispnum_371, radi_mri_body AS dispnum_372, radi_mri_etc AS dispnum_373, radi_mri_add AS dispnum_374, radi_angio AS dispnum_375"
                   .", fee_kid AS dispnum_376, fee_sprt_nrt AS dispnum_377, fee_care_breath AS dispnum_378, fee_plan_security AS dispnum_379, fee_plan_infection AS dispnum_380"
                   .", fee_mng_sec_instrmnt AS dispnum_381, fee_guide_mng_md_one AS dispnum_382, fee_guide_mng_md_two AS dispnum_383, fee_guide_mng_md_three AS dispnum_384"
                   .", fee_orgnz_mng_sec_inf_md AS dispnum_385, empty_one AS dispnum_386, empty_two AS dispnum_387, empty_thee AS dispnum_388, empty_four AS dispnum_389"
                   .", empty_five AS dispnum_390, empty_six AS dispnum_391, empty_seven AS dispnum_392, del_flg, jnl_status AS status, SUBSTR(regist_date, 0, 5) AS year"
                   .", SUBSTR(regist_date, 5, 2) AS month"
               ." FROM jnl_hospital_month_action"
              ." WHERE jnl_status < 2 AND del_flg = 'f'";

        if (!is_null($year))       { $sql .= sprintf(" AND substr(regist_date, 0, 5) = '%04d'", $year); }
        if (!is_null($month))      { $sql .= sprintf(" AND substr(regist_date, 5, 2) = '%02d'", $month); }
        if (!is_null($facilityId)) { $sql .= sprintf(" AND jnl_facility_id = '%s'", $facilityId); }

        $sql .= " ORDER BY jnl_facility_id ";
        return $this->getDateList($sql);
    }

    /**
     * 入力された年月のデータを配列で返す
     * @param integer $year
     * @param integer $month
     * @return array
     */
    function getDayArrOfMonth($year, $month)
    {
        $lastDay = $this->getLastDay($year, $month);
        for ($d=1; $d <= $lastDay; $d++) {
            $date = sprintf('%04d%02d%02d', $year, $month, $d);
            $dayArr[$d]['date'] = $date;
            $dayArr[$d]['day_of_the_week'] = strtolower(date('D', mktime(0, 0, 0, $month, $d, $year)));
            switch ($dayArr[$d]['day_of_the_week']) {
                case 'sat':
                    $dayArr[$d]['day_type'] = 'sat';
                    break;

                case 'sun':
                    $dayArr[$d]['day_type'] = 'sun';
                    break;

                default:
                    $dayArr[$d]['day_type'] = 'weekday';
                    break;
            }

            $holiday = $this->holiday->get_holiday_name($date);
            if ($holiday) {
                $dayArr[$d]['holiday']  = $holiday;
                $dayArr[$d]['day_type'] = 'holiday';
            }
            if ($month == '01' || $month == '12') {
                $mmdd = sprintf('%02d%02d', $month, $d);
                switch ($mmdd) {
                    # 30日は土曜日扱い
                    case '1230':
                        if ($month=="12") { $dayArr[$d]['day_type'] = 'sat'; }
                        break;

                    # 12/31から1/3 までは休日扱い、1/1 はそもそも元旦なので特別処理はしない
                    case '0102': case '0103': case '1231':
                        $dayArr[$d]['day_type'] = 'holiday';
                        break;

                    default:
                        break;
                }
            }
        }

        return $dayArr;
    }

    /**
     * 入力された年月の平日・土曜・休日の値を返す
     * @param integer $year
     * @param integer $month
     * @return array
     */
    function getDayCountOfMonth($year, $month)
    {
        $dayArr = $this->getDayArrOfMonth($year, $month);

        return array(
            'weekday'   => $this->getWeekdayOfMonth($dayArr),
            'saturday'  => $this->getSaturdayOfMonth($dayArr),
            'holiday'   => $this->getHolidayOfMonth($dayArr),
            'day_array' => $dayArr
        );
    }

    /**
     * 入力された年月の日までの平日・土曜・休日の値を返す
     * @param integer $year
     * @param integer $month
     * @return array
     */
    function getDayCountDayOfMonth($year, $month, $day)
    {
        $rtnArr = array();
        $dayArr = $this->getDayArrOfMonth($year, $month);

        foreach ($dayArr as $dayKey => $dayVal) { if ($dayKey <= $day) { $rtnArr[$dayKey] = $dayVal; } }

        return array(
            'weekday'   => $this->getWeekdayOfMonth($rtnArr),
            'saturday'  => $this->getSaturdayOfMonth($rtnArr),
            'holiday'   => $this->getHolidayOfMonth($rtnArr),
            'day_array' => $rtnArr
        );
    }

    function is_holiday($year, $month, $day)
    {
        $rtnArr = array();
        $dayArr = $this->getDayArrOfMonth($year, $month);
        foreach ($dayArr as $dayKey => $dayVal) { if ($dayKey == $day) { $rtnArr[$dayKey] = $dayVal; } }
        $res = $this->getHolidayOfMonth($rtnArr);
        return empty($res)? false : true;
    }


	function is_holiday_detail($year, $month, $day)
	{
		$rtnArr = array();
		$dayArr = $this->getDayArrOfMonth($year, $month);
		foreach ($dayArr as $dayKey => $dayVal) { if ($dayKey == $day) { $rtnArr[$dayKey] = $dayVal; } }
		$res = $this->getHolidayOfMonth($rtnArr);
		return $res;
	}
	

	
	
	/**
	 * 指定された日曜、祝日日数を取得
	 */
	function getHolidayCount($cYear,$displayMonth)
	{
		
		//今日の日数を取得
		$today = getdate();
		$this_time = $today["year"].sprintf("%02d", $today["mon"]);
		
		$cDayCount		= $this->getDayCountOfMonth($cYear, $displayMonth);
		$ch_year		= $cYear.sprintf("%02d", $displayMonth);
		
		
		$holi_cnt = 0;
		if($ch_year < $this_time)
		{
			//表示月が現在月より過去の場合はMAXの日曜・祝日日数を設定する
			
			$holi_cnt = $cDayCount["holiday"];
		}
		else
		{
			//表示月と現在月が同じ場合は現在日までの日曜・祝日日数を設定する
			
			//今日までの日・祝日日数を取得
			for ($cnt=1; $cnt <= $today["mday"]; $cnt++) 
			{
				if(($cDayCount["day_array"][$cnt]["day_type"] == "sun" )|| ($cDayCount["day_array"][$cnt]["day_type"] == "holiday" ))
				{
					//外来平均の分母から引く日数を計算（日曜、祝日）
					$holi_cnt = $holi_cnt + 1;
				}
			}
			
		}
		return $holi_cnt;
		
	}
	

    /**
     * 渡された日付の配列から、平日の数を数えて返す
     * @access protected
     * @param $dayArr
     * @return integer
     */
    function getWeekdayOfMonth($dayArr)
    {
        $count = 0;
        if (is_array($dayArr)) {
            foreach ($dayArr as $dayVal) {
                $dayType = $this->getArrValueByKey('day_type', $dayVal);
                if ($dayType == 'weekday') { $count++; }
            }
        }
        return $count;
    }

    /**
     * 渡された日付の配列から、土曜の数を数えて返す
     * @access protected
     * @param  $dayArr
     * @return integer
     */
    function getSaturdayOfMonth($dayArr)
    {
        $count = 0;
        if (is_array($dayArr)) {
            foreach ($dayArr as $dayVal) {
                $dayType = $this->getArrValueByKey('day_type', $dayVal);
                if ($dayType == 'sat') { $count++; }
            }
        }
        return $count;
    }

    /**
     * 渡された日付の配列から、休日の数を数えて返す
     * @access protected
     * @param  $dayArr
     * @return integer
     */
    function getHolidayOfMonth($dayArr)
    {
        $count = 0;
        if (is_array($dayArr)) {
            foreach ($dayArr as $dayVal) {
                $dayType = $this->getArrValueByKey('day_type', $dayVal);
                if ($dayType == 'sun' || $dayType == 'holiday') { $count++; }
            }
        }
        return $count;
    }

    function getHolidayOfMonthByFacilityId($targetYear, $targetMonth, $targetDay)
    {
        $dayArr = $this->getDayCountDayOfMonth($targetYear, $targetMonth, $targetDay);
        return $dayArr['holiday'];
    }

    /**
     * エラー画面へ遷移します
     */
    function rtnLoginPage()
    {
//        $sql = $this->getSql();
//        echo strlen($sql)."<br />";
//        echo $sql; exit; # debug
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
//        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
//        echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
        exit;
    }

    /**
     * クエリで取得したデータを newest_apply_id でマージする
     * @param  array $arr
     * @param  array $rtnArr
     * @return array $rtnArr
     */
    function arrayMerge($arr, $rtnArr) {
        if (is_array($arr) && count($arr)>0) {
            foreach ($arr as $arrVal) {
                $tmpData       = array();
                $arrayNum      = false;
                $newestApplyId = $arrVal['newest_apply_id'];
                foreach ($rtnArr as $rtnArrKey => $rtnArrVal) {
                    if ($rtnArrVal['newest_apply_id'] == $newestApplyId) {
                        $tmpData  = $rtnArrVal;
                        $arrayNum = $rtnArrKey;
                    }
                }

                $tmpData = array_merge($arrVal, $tmpData);
                if ($arrayNum!==false) { $rtnArr[$arrayNum] = $tmpData; }
                else { $rtnArr[] = $tmpData; }
            }
        }
        return $rtnArr;
    }

    /**
     * @access protected
     * @var arr
     */
    var $sqlArr = array();

    /**
     * @access protected
     * @param  string    $sql
     * @return string
     */
    function setSql($sql)
    {
        $sqlArr = $this->getSqlArr();
        $sqlArr[] = $sql;
        $this->setSqlArr($sqlArr);
    }

    /**
     * @access public
     * @return string
     */
    function getSql()
    {
        $rtnSql = '';
        $sqlArr = $this->getSqlArr();
        if (is_array($sqlArr) && count($sqlArr) > 0 ) { foreach ($sqlArr as $sqlStr) { $rtnSql = $sqlStr; } }
        return $rtnSql;
    }

    function setSqlArr($arr)
    {
        $rtnSqlArr    = array();
        $originSqlArr = $this->getSqlArr();
        $rtnSqlArr    = $arr;
//        if (is_array($arr) && count($arr) > 0) { $rtnSqlArr = array_merge($originSqlArr, $arr); }
//        else { $rtnSqlArr = $originSqlArr; }
        $this->sqlArr = $rtnSqlArr;
    }

    function getSqlArr()
    {
        return $this->sqlArr;
    }

     /**
     * @access public
     * @return string
     */
    function getDeptOPDMonthsReportDetail($strMonth, $strYear)
    {

      $sql="select jnl_facility_id
, jnl_facility_name,
count(scount) as countstatus
, min(jnl_status) as jnl_status
, SUBSTR(regist_date, 0, 5) AS year
, SUBSTR(regist_date, 5, 2) AS month
, sum(coalesce(naika_ga,0)) AS naika_ga
, sum(coalesce(sinkei_ga,0)) AS sinkei_ga
, sum(coalesce(kokyu_ga,0)) AS kokyu_ga
, sum(coalesce(shoka_ga,0)) AS shoka_ga
, sum(coalesce(junkanki_ga,0)) AS junkanki_ga
, sum(coalesce(shouni_ga,0)) AS shouni_ga
, sum(coalesce(geka_ga,0)) AS geka_ga
, sum(coalesce(seikei_ga,0)) AS seikei_ga
, sum(coalesce(keiseibi_ga,0)) AS keiseibi_ga
, sum(coalesce(nou_ga,0)) AS nou_ga
, sum(coalesce(hifu_ga,0)) AS hifu_ga
, sum(coalesce(hinyo_ga,0)) AS hinyo_ga
, sum(coalesce(sanfu_ga,0)) AS sanfu_ga
, sum(coalesce(ganka_ga,0)) AS ganka_ga
, sum(coalesce(jibi_ga,0)) AS jibi_ga
, sum(coalesce(touseki_ga,0)) AS touseki_ga
, sum(coalesce(seisin_ga,0)) AS seisin_ga
, sum(coalesce(sika_ga,0)) AS sika_ga
, sum(coalesce(hoshasen_ga,0)) AS hoshasen_ga
, sum(coalesce(masui_ga,0)) AS masui_ga
, (sum(coalesce(naika_ga,0))
  + sum(coalesce(sinkei_ga,0)) + sum(coalesce(kokyu_ga,0))
  + sum(coalesce(shoka_ga,0)) + sum(coalesce(junkanki_ga,0))
  + sum(coalesce(shouni_ga,0)) + sum(coalesce(geka_ga,0))
  + sum(coalesce(seikei_ga,0)) + sum(coalesce(keiseibi_ga,0))
  + sum(coalesce(nou_ga,0)) + sum(coalesce(hifu_ga,0))
  + sum(coalesce(hinyo_ga,0)) + sum(coalesce(sanfu_ga,0))
  + sum(coalesce(ganka_ga,0)) + sum(coalesce(jibi_ga,0))
  + sum(coalesce(touseki_ga,0)) + sum(coalesce(seisin_ga,0))
  + sum(coalesce(sika_ga,0)) + sum(coalesce(hoshasen_ga,0))
  + sum(coalesce(masui_ga,0))) as Total
from
  (
  select
  jfm.jnl_facility_id,
  jnl_facility_name, case jnl_status when 0 then 0 else 1 end as jnl_status,
   jnl_status as scount,
  jhd.regist_date,naika_ga, sinkei_ga, kokyu_ga
  ,shoka_ga,junkanki_ga, shouni_ga,  geka_ga,seikei_ga
  ,keiseibi_ga,nou_ga, hifu_ga, hinyo_ga
  ,sanfu_ga, ganka_ga, jibi_ga,touseki_ga, seisin_ga
  ,sika_ga, hoshasen_ga,masui_ga
  from
  jnl_facility_master jfm left outer join
  jnl_hospital_day jhd
  on((jfm.jnl_facility_id=jhd.jnl_facility_id and SUBSTR(jhd.regist_date, 0, 5)='".$strYear."' and  SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and (jnl_status =0 or jnl_status = 1) and del_flg='f')
  or jhd.regist_date is null)
  where  jnl_facility_type=1
 ) as t
 group by year, month,jnl_facility_name,jnl_facility_id order by jnl_facility_id";

     return $this->getDateList($sql);

    }

 function getDeptIPDMonthsReportDetail($strMonth, $strYear)
    {

      $sql="select jnl_facility_id
    , jnl_facility_name
    , count(scount) as countstatus
    , min(jnl_status) as jnl_status
    , SUBSTR(regist_date, 0, 5) AS year
    , SUBSTR(regist_date, 5, 2) AS month
    , sum(
        coalesce(naika_ip,0) + coalesce(naika_shog,0) + coalesce(naika_seip,0) + coalesce(naika_ir,0) +
        coalesce(naika_kaig,0) + coalesce(naika_kaif,0) + coalesce(naika_ak,0) + coalesce(naika_kan,0) +
        coalesce(naika_icu,0) + coalesce(naika_shoni,0) + coalesce(naika_seir,0) + coalesce(naika_tok,0) +
        coalesce(naika_nin,0)
    ) AS naika_ga
    , sum(
        coalesce(sinkei_ip,0) + coalesce(sinkei_shog,0) + coalesce(sinkei_seip,0) + coalesce(sinkei_ir,0) +
        coalesce(sinkei_kaig,0) + coalesce(sinkei_kaif,0) + coalesce(sinkei_ak,0) + coalesce(sinkei_kan,0) +
        coalesce(sinkei_icu,0) + coalesce(sinkei_shoni,0) + coalesce(sinkei_seir,0) + coalesce(sinkei_tok,0) +
        coalesce(sinkei_nin,0)
    ) AS sinkei_ga
    , sum(
        coalesce(kokyu_ip,0) + coalesce(kokyu_shog,0) + coalesce(kokyu_seip,0) + coalesce(kokyu_ir,0) +
        coalesce(kokyu_kaig,0) + coalesce(kokyu_kaif,0) + coalesce(kokyu_ak,0) + coalesce(kokyu_kan,0) +
        coalesce(kokyu_icu,0) + coalesce(kokyu_shoni,0) + coalesce(kokyu_seir,0) + coalesce(kokyu_tok,0) +
        coalesce(kokyu_nin,0)
    ) AS kokyu_ga
    , sum(
        coalesce(shoka_ip,0) + coalesce(shoka_shog,0) + coalesce(shoka_seip,0) + coalesce(shoka_ir,0) +
        coalesce(shoka_kaig,0) + coalesce(shoka_kaif,0) + coalesce(shoka_ak,0) + coalesce(shoka_kan,0) +
        coalesce(shoka_icu,0) + coalesce(shoka_shoni,0) + coalesce(shoka_seir,0) + coalesce(shoka_tok,0) +
        coalesce(shoka_nin,0)
    ) AS shoka_ga
    , sum(
        coalesce(junkanki_ip,0) + coalesce(junkanki_shog,0) + coalesce(junkanki_seip,0) + coalesce(junkanki_ir,0) +
        coalesce(junkanki_kaig,0) + coalesce(junkanki_kaif,0) + coalesce(junkanki_ak,0) + coalesce(junkanki_kan,0) +
        coalesce(junkanki_icu,0) + coalesce(junkanki_shoni,0) + coalesce(junkanki_seir,0) + coalesce(junkanki_tok,0) +
        coalesce(junkanki_nin,0)
    ) AS junkanki_ga
    , sum(
        coalesce(shouni_ip,0) + coalesce(shouni_shog,0) + coalesce(shouni_seip,0) + coalesce(shouni_ir,0) +
        coalesce(shouni_kaig,0) + coalesce(shouni_kaif,0) + coalesce(shouni_ak,0) + coalesce(shouni_kan,0) +
        coalesce(shouni_icu,0) + coalesce(shouni_shoni,0) + coalesce(shouni_seir,0) + coalesce(shouni_tok,0) +
        coalesce(shouni_nin,0)
    ) AS shouni_ga
    , sum(
        coalesce(geka_ip,0) + coalesce(geka_shog,0) + coalesce(geka_seip,0) + coalesce(geka_ir,0) +
        coalesce(geka_kaig,0) + coalesce(geka_kaif,0) + coalesce(geka_ak,0) + coalesce(geka_kan,0) +
        coalesce(geka_icu,0) + coalesce(geka_shoni,0) + coalesce(geka_seir,0) + coalesce(geka_tok,0) +
        coalesce(geka_nin,0)
    ) AS geka_ga
    , sum(
        coalesce(seikei_ip,0) + coalesce(seikei_shog,0) + coalesce(seikei_seip,0) + coalesce(seikei_ir,0) +
        coalesce(seikei_kaig,0) + coalesce(seikei_kaif,0) + coalesce(seikei_ak,0) + coalesce(seikei_kan,0) +
        coalesce(seikei_icu,0) + coalesce(seikei_shoni,0) + coalesce(seikei_seir,0) + coalesce(seikei_tok,0) +
        coalesce(seikei_nin,0)
    ) AS seikei_ga
    , sum(
        coalesce(keiseibi_ip,0) + coalesce(keiseibi_shog,0) + coalesce(keiseibi_seip,0) + coalesce(keiseibi_ir,0) +
        coalesce(keiseibi_kaig,0) + coalesce(keiseibi_kaif,0) + coalesce(keiseibi_ak,0) + coalesce(keiseibi_kan,0) +
        coalesce(keiseibi_icu,0) + coalesce(keiseibi_shoni,0) + coalesce(keiseibi_seir,0) + coalesce(keiseibi_tok,0) +
        coalesce(keiseibi_nin,0)
    ) AS keiseibi_ga
    , sum(
        coalesce(nou_ip,0) + coalesce(nou_shog,0) + coalesce(nou_seip,0) + coalesce(nou_ir,0) +
        coalesce(nou_kaig,0) + coalesce(nou_kaif,0) + coalesce(nou_ak,0) + coalesce(nou_kan,0) +
        coalesce(nou_icu,0) + coalesce(nou_shoni,0) + coalesce(nou_seir,0) + coalesce(nou_tok,0) +
        coalesce(nou_nin,0)
    ) AS nou_ga
    , sum(
        coalesce(hifu_ip,0) + coalesce(hifu_shog,0) + coalesce(hifu_seip,0) + coalesce(hifu_ir,0) +
        coalesce(hifu_kaig,0) + coalesce(hifu_kaif,0) + coalesce(hifu_ak,0) + coalesce(hifu_kan,0) +
        coalesce(hifu_icu,0) + coalesce(hifu_shoni,0) + coalesce(hifu_seir,0) + coalesce(hifu_tok,0) +
        coalesce(hifu_nin,0)
    ) AS hifu_ga
    , sum(
        coalesce(hinyo_ip,0) + coalesce(hinyo_shog,0) + coalesce(hinyo_seip,0) + coalesce(hinyo_ir,0) +
        coalesce(hinyo_kaig,0) + coalesce(hinyo_kaif,0) + coalesce(hinyo_ak,0) + coalesce(hinyo_kan,0) +
        coalesce(hinyo_icu,0) + coalesce(hinyo_shoni,0) + coalesce(hinyo_seir,0) + coalesce(hinyo_tok,0) +
        coalesce(hinyo_nin,0)
    ) AS hinyo_ga
    , sum(
        coalesce(sanfu_ip,0) + coalesce(sanfu_shog,0) + coalesce(sanfu_seip,0) + coalesce(sanfu_ir,0) +
        coalesce(sanfu_kaig,0) + coalesce(sanfu_kaif,0) + coalesce(sanfu_ak,0) + coalesce(sanfu_kan,0) +
        coalesce(sanfu_icu,0) + coalesce(sanfu_shoni,0) + coalesce(sanfu_seir,0) + coalesce(sanfu_tok,0) +
        coalesce(sanfu_nin,0)
    ) AS sanfu_ga
    , sum(
        coalesce(ganka_ip,0) + coalesce(ganka_shog,0) + coalesce(ganka_seip,0) + coalesce(ganka_ir,0) +
        coalesce(ganka_kaig,0) + coalesce(ganka_kaif,0) + coalesce(ganka_ak,0) + coalesce(ganka_kan,0) +
        coalesce(ganka_icu,0) + coalesce(ganka_shoni,0) + coalesce(ganka_seir,0) + coalesce(ganka_tok,0) +
        coalesce(ganka_nin,0)
    ) AS ganka_ga
    , sum(
        coalesce(jibi_ip,0) + coalesce(jibi_shog,0) + coalesce(jibi_seip,0) + coalesce(jibi_ir,0) +
        coalesce(jibi_kaig,0) + coalesce(jibi_kaif,0) + coalesce(jibi_ak,0) + coalesce(jibi_kan,0) +
        coalesce(jibi_icu,0) + coalesce(jibi_shoni,0) + coalesce(jibi_seir,0) + coalesce(jibi_tok,0) +
        coalesce(jibi_nin,0)
    ) AS jibi_ga
    , sum(
        coalesce(touseki_ip,0) + coalesce(touseki_shog,0) + coalesce(touseki_seip,0) + coalesce(touseki_ir,0) +
        coalesce(touseki_kaig,0) + coalesce(touseki_kaif,0) + coalesce(touseki_ak,0) + coalesce(touseki_kan,0) +
        coalesce(touseki_icu,0) + coalesce(touseki_shoni,0) + coalesce(touseki_seir,0) + coalesce(touseki_tok,0) +
        coalesce(touseki_nin,0)
    ) AS touseki_ga
    , sum(
        coalesce(seisin_ip,0) + coalesce(seisin_shog,0) + coalesce(seisin_seip,0) + coalesce(seisin_ir,0) +
        coalesce(seisin_kaig,0) + coalesce(seisin_kaif,0) + coalesce(seisin_ak,0) + coalesce(seisin_kan,0) +
        coalesce(seisin_icu,0) + coalesce(seisin_shoni,0) + coalesce(seisin_seir,0) + coalesce(seisin_tok,0) +
        coalesce(seisin_nin,0)
    ) AS seisin_ga
    , sum(
        coalesce(sika_ip,0) + coalesce(sika_shog,0) + coalesce(sika_seip,0) + coalesce(sika_ir,0) +
        coalesce(sika_kaig,0) + coalesce(sika_kaif,0) + coalesce(sika_ak,0) + coalesce(sika_kan,0) +
        coalesce(sika_icu,0) + coalesce(sika_shoni,0) + coalesce(sika_seir,0) + coalesce(sika_tok,0) +
        coalesce(sika_nin,0)
    ) AS sika_ga
    , sum(
        coalesce(hoshasen_ip,0) + coalesce(hoshasen_shog,0) + coalesce(hoshasen_seip,0) + coalesce(hoshasen_ir,0) +
        coalesce(hoshasen_kaig,0) + coalesce(hoshasen_kaif,0) + coalesce(hoshasen_ak,0) + coalesce(hoshasen_kan,0) +
        coalesce(hoshasen_icu,0) + coalesce(hoshasen_shoni,0) + coalesce(hoshasen_seir,0) + coalesce(hoshasen_tok,0) +
        coalesce(hoshasen_nin,0)
    ) AS hoshasen_ga
    , sum(
        coalesce(masui_ip,0) + coalesce(masui_shog,0) + coalesce(masui_seip,0) + coalesce(masui_ir,0) +
        coalesce(masui_kaig,0) + coalesce(masui_kaif,0) + coalesce(masui_ak,0) + coalesce(masui_kan,0) +
        coalesce(masui_icu,0) + coalesce(masui_shoni,0) + coalesce(masui_seir,0) + coalesce(masui_tok,0) +
        coalesce(masui_nin,0)
    ) AS masui_ga
    , sum(
        coalesce(naika_ip,0) + coalesce(naika_shog,0) + coalesce(naika_seip,0) + coalesce(naika_ir,0) +
        coalesce(naika_kaig,0) + coalesce(naika_kaif,0) + coalesce(naika_ak,0) + coalesce(naika_kan,0) +
        coalesce(naika_icu,0) + coalesce(naika_shoni,0) + coalesce(naika_seir,0) + coalesce(naika_tok,0) +
        coalesce(naika_nin,0) +
        coalesce(sinkei_ip,0) + coalesce(sinkei_shog,0) + coalesce(sinkei_seip,0) + coalesce(sinkei_ir,0) +
        coalesce(sinkei_kaig,0) + coalesce(sinkei_kaif,0) + coalesce(sinkei_ak,0) + coalesce(sinkei_kan,0) +
        coalesce(sinkei_icu,0) + coalesce(sinkei_shoni,0) + coalesce(sinkei_seir,0) + coalesce(sinkei_tok,0) +
        coalesce(sinkei_nin,0) +
        coalesce(kokyu_ip,0) + coalesce(kokyu_shog,0) + coalesce(kokyu_seip,0) + coalesce(kokyu_ir,0) +
        coalesce(kokyu_kaig,0) + coalesce(kokyu_kaif,0) + coalesce(kokyu_ak,0) + coalesce(kokyu_kan,0) +
        coalesce(kokyu_icu,0) + coalesce(kokyu_shoni,0) + coalesce(kokyu_seir,0) + coalesce(kokyu_tok,0) +
        coalesce(kokyu_nin,0) +
        coalesce(shoka_ip,0) + coalesce(shoka_shog,0) + coalesce(shoka_seip,0) + coalesce(shoka_ir,0) +
        coalesce(shoka_kaig,0) + coalesce(shoka_kaif,0) + coalesce(shoka_ak,0) + coalesce(shoka_kan,0) +
        coalesce(shoka_icu,0) + coalesce(shoka_shoni,0) + coalesce(shoka_seir,0) + coalesce(shoka_tok,0) +
        coalesce(shoka_nin,0) +
        coalesce(junkanki_ip,0) + coalesce(junkanki_shog,0) + coalesce(junkanki_seip,0) + coalesce(junkanki_ir,0) +
        coalesce(junkanki_kaig,0) + coalesce(junkanki_kaif,0) + coalesce(junkanki_ak,0) + coalesce(junkanki_kan,0) +
        coalesce(junkanki_icu,0) + coalesce(junkanki_shoni,0) + coalesce(junkanki_seir,0) + coalesce(junkanki_tok,0) +
        coalesce(junkanki_nin,0) +
        coalesce(shouni_ip,0) + coalesce(shouni_shog,0) + coalesce(shouni_seip,0) + coalesce(shouni_ir,0) +
        coalesce(shouni_kaig,0) + coalesce(shouni_kaif,0) + coalesce(shouni_ak,0) + coalesce(shouni_kan,0) +
        coalesce(shouni_icu,0) + coalesce(shouni_shoni,0) + coalesce(shouni_seir,0) + coalesce(shouni_tok,0) +
        coalesce(shouni_nin,0) +
        coalesce(geka_ip,0) + coalesce(geka_shog,0) + coalesce(geka_seip,0) + coalesce(geka_ir,0) +
        coalesce(geka_kaig,0) + coalesce(geka_kaif,0) + coalesce(geka_ak,0) + coalesce(geka_kan,0) +
        coalesce(geka_icu,0) + coalesce(geka_shoni,0) + coalesce(geka_seir,0) + coalesce(geka_tok,0) +
        coalesce(geka_nin,0) +
        coalesce(seikei_ip,0) + coalesce(seikei_shog,0) + coalesce(seikei_seip,0) + coalesce(seikei_ir,0) +
        coalesce(seikei_kaig,0) + coalesce(seikei_kaif,0) + coalesce(seikei_ak,0) + coalesce(seikei_kan,0) +
        coalesce(seikei_icu,0) + coalesce(seikei_shoni,0) + coalesce(seikei_seir,0) + coalesce(seikei_tok,0) +
        coalesce(seikei_nin,0) +
        coalesce(keiseibi_ip,0) + coalesce(keiseibi_shog,0) + coalesce(keiseibi_seip,0) + coalesce(keiseibi_ir,0) +
        coalesce(keiseibi_kaig,0) + coalesce(keiseibi_kaif,0) + coalesce(keiseibi_ak,0) + coalesce(keiseibi_kan,0) +
        coalesce(keiseibi_icu,0) + coalesce(keiseibi_shoni,0) + coalesce(keiseibi_seir,0) + coalesce(keiseibi_tok,0) +
        coalesce(keiseibi_nin,0) +
        coalesce(nou_ip,0) + coalesce(nou_shog,0) + coalesce(nou_seip,0) + coalesce(nou_ir,0) +
        coalesce(nou_kaig,0) + coalesce(nou_kaif,0) + coalesce(nou_ak,0) + coalesce(nou_kan,0) +
        coalesce(nou_icu,0) + coalesce(nou_shoni,0) + coalesce(nou_seir,0) + coalesce(nou_tok,0) +
        coalesce(nou_nin,0) +
        coalesce(hifu_ip,0) + coalesce(hifu_shog,0) + coalesce(hifu_seip,0) + coalesce(hifu_ir,0) +
        coalesce(hifu_kaig,0) + coalesce(hifu_kaif,0) + coalesce(hifu_ak,0) + coalesce(hifu_kan,0) +
        coalesce(hifu_icu,0) + coalesce(hifu_shoni,0) + coalesce(hifu_seir,0) + coalesce(hifu_tok,0) +
        coalesce(hifu_nin,0) +
        coalesce(hinyo_ip,0) + coalesce(hinyo_shog,0) + coalesce(hinyo_seip,0) + coalesce(hinyo_ir,0) +
        coalesce(hinyo_kaig,0) + coalesce(hinyo_kaif,0) + coalesce(hinyo_ak,0) + coalesce(hinyo_kan,0) +
        coalesce(hinyo_icu,0) + coalesce(hinyo_shoni,0) + coalesce(hinyo_seir,0) + coalesce(hinyo_tok,0) +
        coalesce(hinyo_nin,0) +
        coalesce(sanfu_ip,0) + coalesce(sanfu_shog,0) + coalesce(sanfu_seip,0) + coalesce(sanfu_ir,0) +
        coalesce(sanfu_kaig,0) + coalesce(sanfu_kaif,0) + coalesce(sanfu_ak,0) + coalesce(sanfu_kan,0) +
        coalesce(sanfu_icu,0) + coalesce(sanfu_shoni,0) + coalesce(sanfu_seir,0) + coalesce(sanfu_tok,0) +
        coalesce(sanfu_nin,0) +
        coalesce(ganka_ip,0) + coalesce(ganka_shog,0) + coalesce(ganka_seip,0) + coalesce(ganka_ir,0) +
        coalesce(ganka_kaig,0) + coalesce(ganka_kaif,0) + coalesce(ganka_ak,0) + coalesce(ganka_kan,0) +
        coalesce(ganka_icu,0) + coalesce(ganka_shoni,0) + coalesce(ganka_seir,0) + coalesce(ganka_tok,0) +
        coalesce(ganka_nin,0) +
        coalesce(jibi_ip,0) + coalesce(jibi_shog,0) + coalesce(jibi_seip,0) + coalesce(jibi_ir,0) +
        coalesce(jibi_kaig,0) + coalesce(jibi_kaif,0) + coalesce(jibi_ak,0) + coalesce(jibi_kan,0) +
        coalesce(jibi_icu,0) + coalesce(jibi_shoni,0) + coalesce(jibi_seir,0) + coalesce(jibi_tok,0) +
        coalesce(jibi_nin,0) +
        coalesce(touseki_ip,0) + coalesce(touseki_shog,0) + coalesce(touseki_seip,0) + coalesce(touseki_ir,0) +
        coalesce(touseki_kaig,0) + coalesce(touseki_kaif,0) + coalesce(touseki_ak,0) + coalesce(touseki_kan,0) +
        coalesce(touseki_icu,0) + coalesce(touseki_shoni,0) + coalesce(touseki_seir,0) + coalesce(touseki_tok,0) +
        coalesce(touseki_nin,0) +
        coalesce(seisin_ip,0) + coalesce(seisin_shog,0) + coalesce(seisin_seip,0) + coalesce(seisin_ir,0) +
        coalesce(seisin_kaig,0) + coalesce(seisin_kaif,0) + coalesce(seisin_ak,0) + coalesce(seisin_kan,0) +
        coalesce(seisin_icu,0) + coalesce(seisin_shoni,0) + coalesce(seisin_seir,0) + coalesce(seisin_tok,0) +
        coalesce(seisin_nin,0) +
        coalesce(sika_ip,0) + coalesce(sika_shog,0) + coalesce(sika_seip,0) + coalesce(sika_ir,0) +
        coalesce(sika_kaig,0) + coalesce(sika_kaif,0) + coalesce(sika_ak,0) + coalesce(sika_kan,0) +
        coalesce(sika_icu,0) + coalesce(sika_shoni,0) + coalesce(sika_seir,0) + coalesce(sika_tok,0) +
        coalesce(sika_nin,0) +
        coalesce(hoshasen_ip,0) + coalesce(hoshasen_shog,0) + coalesce(hoshasen_seip,0) + coalesce(hoshasen_ir,0) +
        coalesce(hoshasen_kaig,0) + coalesce(hoshasen_kaif,0) + coalesce(hoshasen_ak,0) + coalesce(hoshasen_kan,0) +
        coalesce(hoshasen_icu,0) + coalesce(hoshasen_shoni,0) + coalesce(hoshasen_seir,0) + coalesce(hoshasen_tok,0) +
        coalesce(hoshasen_nin,0) +
        coalesce(masui_ip,0) + coalesce(masui_shog,0) + coalesce(masui_seip,0) + coalesce(masui_ir,0) +
        coalesce(masui_kaig,0) + coalesce(masui_kaif,0) + coalesce(masui_ak,0) + coalesce(masui_kan,0) +
        coalesce(masui_icu,0) + coalesce(masui_shoni,0) + coalesce(masui_seir,0) + coalesce(masui_tok,0) +
        coalesce(masui_nin,0)
    ) as Total
    from
    (
    select
        jfm.jnl_facility_id,
        jnl_facility_name, case jnl_status when 0 then 0 else 1 end as jnl_status,
        jnl_status as scount,
        jhd.regist_date,
        naika_ip, naika_shog, naika_seip, naika_ir, naika_kaig, naika_kaif,
        naika_ak, naika_kan, naika_icu, naika_shoni, naika_seir, naika_tok, naika_nin,
        sinkei_ip, sinkei_shog, sinkei_seip, sinkei_ir, sinkei_kaig, sinkei_kaif,
        sinkei_ak, sinkei_kan, sinkei_icu, sinkei_shoni, sinkei_seir, sinkei_tok, sinkei_nin,
        kokyu_ip, kokyu_shog, kokyu_seip, kokyu_ir, kokyu_kaig,    kokyu_kaif,
        kokyu_ak, kokyu_kan, kokyu_icu, kokyu_shoni, kokyu_seir, kokyu_tok, kokyu_nin,
        shoka_ip, shoka_shog, shoka_seip, shoka_ir, shoka_kaig, shoka_kaif,
        shoka_ak, shoka_kan, shoka_icu, shoka_shoni, shoka_seir, shoka_tok, shoka_nin,
        junkanki_ip, junkanki_shog, junkanki_seip, junkanki_ir, junkanki_kaig, junkanki_kaif,
        junkanki_ak, junkanki_kan, junkanki_icu, junkanki_shoni, junkanki_seir, junkanki_tok, junkanki_nin,
        shouni_ip, shouni_shog, shouni_seip, shouni_ir, shouni_kaig, shouni_kaif,
        shouni_ak, shouni_kan, shouni_icu, shouni_shoni, shouni_seir, shouni_tok, shouni_nin,
        geka_ip, geka_shog, geka_seip, geka_ir, geka_kaig, geka_kaif,
        geka_ak, geka_kan, geka_icu, geka_shoni, geka_seir, geka_tok, geka_nin,
        seikei_ip, seikei_shog, seikei_seip, seikei_ir, seikei_kaig, seikei_kaif,
        seikei_ak, seikei_kan, seikei_icu, seikei_shoni, seikei_seir, seikei_tok, seikei_nin,
        keiseibi_ip, keiseibi_shog, keiseibi_seip, keiseibi_ir, keiseibi_kaig, keiseibi_kaif,
        keiseibi_ak, keiseibi_kan, keiseibi_icu, keiseibi_shoni, keiseibi_seir, keiseibi_tok, keiseibi_nin,
        nou_ip, nou_shog, nou_seip, nou_ir, nou_kaig, nou_kaif,
        nou_ak, nou_kan, nou_icu, nou_shoni, nou_seir, nou_tok, nou_nin,
        hifu_ip, hifu_shog, hifu_seip, hifu_ir, hifu_kaig, hifu_kaif,
        hifu_ak, hifu_kan, hifu_icu, hifu_shoni, hifu_seir, hifu_tok, hifu_nin,
        hinyo_ip, hinyo_shog, hinyo_seip, hinyo_ir, hinyo_kaig, hinyo_kaif,
        hinyo_ak, hinyo_kan, hinyo_icu, hinyo_shoni, hinyo_seir, hinyo_tok, hinyo_nin,
        sanfu_ip, sanfu_shog, sanfu_seip, sanfu_ir, sanfu_kaig, sanfu_kaif,
        sanfu_ak, sanfu_kan, sanfu_icu, sanfu_shoni, sanfu_seir, sanfu_tok, sanfu_nin,
        ganka_ip, ganka_shog, ganka_seip, ganka_ir, ganka_kaig, ganka_kaif,
        ganka_ak, ganka_kan, ganka_icu, ganka_shoni, ganka_seir, ganka_tok, ganka_nin,
        jibi_ip, jibi_shog, jibi_seip, jibi_ir, jibi_kaig, jibi_kaif,
        jibi_ak, jibi_kan, jibi_icu, jibi_shoni, jibi_seir, jibi_tok, jibi_nin,
        touseki_ip, touseki_shog, touseki_seip, touseki_ir, touseki_kaig, touseki_kaif,
        touseki_ak, touseki_kan, touseki_icu, touseki_shoni, touseki_seir, touseki_tok, touseki_nin,
        seisin_ip, seisin_shog, seisin_seip, seisin_ir, seisin_kaig, seisin_kaif,
        seisin_ak, seisin_kan, seisin_icu, seisin_shoni, seisin_seir, seisin_tok, seisin_nin,
        sika_ip, sika_shog, sika_seip, sika_ir, sika_kaig, sika_kaif,
        sika_ak, sika_kan, sika_icu, sika_shoni, sika_seir, sika_tok, sika_nin,
        hoshasen_ip, hoshasen_shog, hoshasen_seip, hoshasen_ir, hoshasen_kaig, hoshasen_kaif,
        hoshasen_ak, hoshasen_kan, hoshasen_icu, hoshasen_shoni, hoshasen_seir, hoshasen_tok, hoshasen_nin,
        masui_ip, masui_shog, masui_seip, masui_ir, masui_kaig, masui_kaif,
        masui_ak, masui_kan, masui_icu, masui_shoni, masui_seir, masui_tok, masui_nin

    from
        jnl_facility_master jfm left outer join
        jnl_hospital_day jhd
          on((jfm.jnl_facility_id=jhd.jnl_facility_id and SUBSTR(jhd.regist_date, 0, 5)='".$strYear."' and  SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and (jnl_status =0 or jnl_status = 1) and del_flg='f')
  or jhd.regist_date is null)
      where  jnl_facility_type=1
    ) as t
    group by year, month,jnl_facility_name,jnl_facility_id order by jnl_facility_id";
     return $this->getDateList($sql);


    }

     function getDiseasewiseOPDCompPrevYearDetail($strMonth, $strYear)
    {
$i=intval($strYear);
$prevYear = $i;
$prepprevYear = $i-1;

$Query ="select jnl_facility_id, jnl_facility_name,
count(*) as countstatus
, min(jnl_status) as jnl_status,
min(jnl_status_c) as jnl_status_c
, prev, current
, sum(coalesce(naika_ga_prev,0)) AS naika_ga_p
, sum(coalesce(naika_ga_current,0)) AS naika_ga_c

, sum(coalesce(sinkei_ga_prev,0)) AS sinkei_ga_p
, sum(coalesce(sinkei_ga_current,0)) AS sinkei_ga_c
, sum(coalesce(kokyu_ga_prev,0)) AS kokyu_ga_p
, sum(coalesce(kokyu_ga_current,0)) AS kokyu_ga_c
, sum(coalesce(shoka_ga_prev,0)) AS shoka_ga_p
, sum(coalesce(shoka_ga_current,0)) AS shoka_ga_c
, sum(coalesce(junkanki_ga_prev,0)) AS junkanki_ga_p
, sum(coalesce(junkanki_ga_current,0)) AS junkanki_ga_c
, sum(coalesce(shouni_ga_prev,0)) AS shouni_ga_p
, sum(coalesce(shouni_ga_current,0)) AS shouni_ga_c
, sum(coalesce(geka_ga_prev,0)) AS geka_ga_p
, sum(coalesce(geka_ga_current,0)) AS geka_ga_c
, sum(coalesce(seikei_ga_prev,0)) AS seikei_ga_p
, sum(coalesce(seikei_ga_current,0)) AS seikei_ga_c
, sum(coalesce(keiseibi_ga_prev,0)) AS keiseibi_ga_p
, sum(coalesce(keiseibi_ga_current,0)) AS keiseibi_ga_c
, sum(coalesce(nou_ga_prev,0)) AS nou_ga_p
, sum(coalesce(nou_ga_current,0)) AS nou_ga_c
, sum(coalesce(hifu_ga_prev,0)) AS hifu_ga_p
, sum(coalesce(hifu_ga_current,0)) AS hifu_ga_c
, sum(coalesce(hinyo_ga_prev,0)) AS hinyo_ga_p
, sum(coalesce(hinyo_ga_current,0)) AS hinyo_ga_c
, sum(coalesce(sanfu_ga_prev,0)) AS sanfu_ga_p
, sum(coalesce(sanfu_ga_current,0)) AS sanfu_ga_c
, sum(coalesce(ganka_ga_prev,0)) AS ganka_ga_p
, sum(coalesce(ganka_ga_current,0)) AS ganka_ga_c
, sum(coalesce(jibi_ga_prev,0)) AS jibi_ga_p
, sum(coalesce(jibi_ga_current,0)) AS jibi_ga_c
, sum(coalesce(touseki_ga_prev,0)) AS touseki_ga_p
, sum(coalesce(touseki_ga_current,0)) AS touseki_ga_c
, sum(coalesce(seisin_ga_prev,0)) AS seisin_ga_p
, sum(coalesce(seisin_ga_current,0)) AS seisin_ga_c
, sum(coalesce(sika_ga_prev,0)) AS sika_ga_p
, sum(coalesce(sika_ga_current,0)) AS sika_ga_c
, sum(coalesce(hoshasen_ga_prev,0)) AS hoshasen_ga_p
, sum(coalesce(hoshasen_ga_current,0)) AS hoshasen_ga_c
, sum(coalesce(masui_ga_prev,0)) AS masui_ga_p
, sum(coalesce(masui_ga_current,0)) AS masui_ga_c
from
( (
  select
  jfm.jnl_facility_id,
  jnl_facility_name, case jnl_status when 0 then 0 else 1 end as jnl_status_c, 1 as jnl_status,
  '".$prepprevYear."' as prev, SUBSTR(coalesce(regist_date,'".$prevYear."'), 0, 5) AS current,
  0 as naika_ga_prev, coalesce(naika_ga,0) as naika_ga_current ,
  0 as sinkei_ga_prev, coalesce(sinkei_ga,0) as sinkei_ga_current ,
  0 as kokyu_ga_prev, coalesce(kokyu_ga,0) as kokyu_ga_current ,
  0 as shoka_ga_prev, coalesce(shoka_ga,0) as shoka_ga_current ,
  0 as junkanki_ga_prev, coalesce(junkanki_ga,0) as junkanki_ga_current ,
  0 as shouni_ga_prev, coalesce(shouni_ga,0) as shouni_ga_current ,
  0 as geka_ga_prev, coalesce(geka_ga,0) as geka_ga_current ,
  0 as seikei_ga_prev, coalesce(seikei_ga,0) as seikei_ga_current ,
  0 as keiseibi_ga_prev, coalesce(keiseibi_ga,0) as keiseibi_ga_current ,
  0 as nou_ga_prev, coalesce(nou_ga,0) as nou_ga_current ,
  0 as hifu_ga_prev, coalesce(hifu_ga,0) as hifu_ga_current ,
  0 as hinyo_ga_prev, coalesce(hinyo_ga,0) as hinyo_ga_current ,
  0 as sanfu_ga_prev, coalesce(sanfu_ga,0) as sanfu_ga_current ,
  0 as ganka_ga_prev, coalesce(ganka_ga,0) as ganka_ga_current ,
  0 as jibi_ga_prev, coalesce(jibi_ga,0) as jibi_ga_current ,
  0 as touseki_ga_prev, coalesce(touseki_ga,0) as touseki_ga_current ,
  0 as seisin_ga_prev, coalesce(seisin_ga,0) as seisin_ga_current ,
  0 as sika_ga_prev, coalesce(sika_ga,0) as sika_ga_current ,
  0 as hoshasen_ga_prev, coalesce(hoshasen_ga,0) as hoshasen_ga_current ,
  0 as masui_ga_prev, coalesce(masui_ga,0) as masui_ga_current
  from
  jnl_facility_master jfm left outer join
  jnl_hospital_day jhd
  on((jfm.jnl_facility_id=jhd.jnl_facility_id and SUBSTR(jhd.regist_date, 0, 5)='".$prevYear."' and  SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and ( jnl_status=0 or jnl_status=1 ) and
 del_flg='f' )
  or jhd.regist_date is null)
  where jnl_facility_type=1 )
union all
(
  select
  jfm.jnl_facility_id,
  jnl_facility_name,1 as jnl_status_c, case jnl_status when 0 then 0 else 1 end as jnl_status,
  SUBSTR(coalesce(regist_date, '".$prepprevYear."'), 0, 5) as prev,'".$prevYear."' as current ,
  coalesce(naika_ga,0) as naika_ga_prev, 0 as naika_ga_current,
  coalesce(sinkei_ga,0) as sinkei_ga_prev,  0 as sinkei_ga_current ,
  coalesce(kokyu_ga,0) as kokyu_ga_prev, 0 as kokyu_ga_current ,
  coalesce(shoka_ga,0) as shoka_ga_prev, 0 as shoka_ga_current ,
  coalesce(junkanki_ga,0) as junkanki_ga_prev,  0 as junkanki_ga_current ,
   coalesce(shouni_ga,0) as shouni_ga_prev, 0 as shouni_ga_current ,
  coalesce(geka_ga,0) as geka_ga_prev,  0 as geka_ga_current ,
  coalesce(seikei_ga,0) as seikei_ga_prev, 0 as seikei_ga_current ,
  coalesce(keiseibi_ga,0) as keiseibi_ga_prev, 0 as keiseibi_ga_current ,
  coalesce(nou_ga,0) as nou_ga_prev, 0 as nou_ga_current ,
  coalesce(hifu_ga,0) as hifu_ga_prev, 0 as hifu_ga_current ,
  coalesce(hinyo_ga,0) as hinyo_ga_prev, 0 as hinyo_ga_current ,
  coalesce(sanfu_ga,0) as sanfu_ga_prev, 0 as sanfu_ga_current ,
  coalesce(ganka_ga,0) as ganka_ga_prev, 0 as ganka_ga_current ,
  coalesce(jibi_ga,0) as jibi_ga_prev, 0 as jibi_ga_current ,
  coalesce(touseki_ga,0) as touseki_ga_prev, 0 as touseki_ga_current ,
  coalesce(seisin_ga,0) as seisin_ga_prev, 0 as seisin_ga_current ,
  coalesce(sika_ga,0) as sika_ga_prev, 0 as sika_ga_current ,
  coalesce(hoshasen_ga,0) as hoshasen_ga_prev, 0 as hoshasen_ga_current ,
  coalesce(masui_ga,0) as masui_ga_prev, 0 as masui_ga_current
  from
  jnl_facility_master jfm left outer join
  jnl_hospital_day jhd
  on((jfm.jnl_facility_id=jhd.jnl_facility_id and SUBSTR(jhd.regist_date, 0, 5)='".$prepprevYear."' and  SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and ( jnl_status=0 or jnl_status=1 ) and  del_flg='f')
  or jhd.regist_date is null)
  where jnl_facility_type=1 )
) as tt group by prev, current, jnl_facility_id,jnl_facility_name,jnl_facility_id order by jnl_facility_id";

     return $this->getDateList($Query);

    }

function getTotalForMonthInYear($facilityId, $strMonth, $strYear)
{
    $countQuery="select count(*) as deptCount from jnl_hospital_day where
    SUBSTR(regist_date, 0, 5)='".$strYear."'
    and SUBSTR(regist_date, 5, 2)='".$strMonth."' and jnl_facility_id='".$facilityId."' and  ( jnl_status=0 or jnl_status=1 ) and del_flg='f'";
    return $this->getDateList($countQuery);

}


function getDiseasewiseIPDCompPrevYearDetail($strMonth, $strYear)
{
$i=intval($strYear);
$prevYear = $i;
$prevprevYear = $i-1;

$sql="select jnl_facility_id, jnl_facility_name,
count(*) as countstatus
, min(jnl_status) as jnl_status
, min(jnl_status_c) as jnl_status_c
, prev, current
, sum(coalesce(naika_ga_prev,0)) AS naika_ga_p
, sum(coalesce(naika_ga_current,0)) AS naika_ga_c
, sum(coalesce(sinkei_ga_prev,0)) AS sinkei_ga_p
, sum(coalesce(sinkei_ga_current,0)) AS sinkei_ga_c
, sum(coalesce(kokyu_ga_prev,0)) AS kokyu_ga_p
, sum(coalesce(kokyu_ga_current,0)) AS kokyu_ga_c
, sum(coalesce(shoka_ga_prev,0)) AS shoka_ga_p
, sum(coalesce(shoka_ga_current,0)) AS shoka_ga_c
, sum(coalesce(junkanki_ga_prev,0)) AS junkanki_ga_p
, sum(coalesce(junkanki_ga_current,0)) AS junkanki_ga_c
, sum(coalesce(shouni_ga_prev,0)) AS shouni_ga_p
, sum(coalesce(shouni_ga_current,0)) AS shouni_ga_c
, sum(coalesce(geka_ga_prev,0)) AS geka_ga_p
, sum(coalesce(geka_ga_current,0)) AS geka_ga_c
, sum(coalesce(seikei_ga_prev,0)) AS seikei_ga_p
, sum(coalesce(seikei_ga_current,0)) AS seikei_ga_c
, sum(coalesce(keiseibi_ga_prev,0)) AS keiseibi_ga_p
, sum(coalesce(keiseibi_ga_current,0)) AS keiseibi_ga_c
, sum(coalesce(nou_ga_prev,0)) AS nou_ga_p
, sum(coalesce(nou_ga_current,0)) AS nou_ga_c
, sum(coalesce(hifu_ga_prev,0)) AS hifu_ga_p
, sum(coalesce(hifu_ga_current,0)) AS hifu_ga_c
, sum(coalesce(hinyo_ga_prev,0)) AS hinyo_ga_p
, sum(coalesce(hinyo_ga_current,0)) AS hinyo_ga_c
, sum(coalesce(sanfu_ga_prev,0)) AS sanfu_ga_p
, sum(coalesce(sanfu_ga_current,0)) AS sanfu_ga_c
, sum(coalesce(ganka_ga_prev,0)) AS ganka_ga_p
, sum(coalesce(ganka_ga_current,0)) AS ganka_ga_c
, sum(coalesce(jibi_ga_prev,0)) AS jibi_ga_p
, sum(coalesce(jibi_ga_current,0)) AS jibi_ga_c
, sum(coalesce(touseki_ga_prev,0)) AS touseki_ga_p
, sum(coalesce(touseki_ga_current,0)) AS touseki_ga_c
, sum(coalesce(seisin_ga_prev,0)) AS seisin_ga_p
, sum(coalesce(seisin_ga_current,0)) AS seisin_ga_c
, sum(coalesce(sika_ga_prev,0)) AS sika_ga_p
, sum(coalesce(sika_ga_current,0)) AS sika_ga_c
, sum(coalesce(hoshasen_ga_prev,0)) AS hoshasen_ga_p
, sum(coalesce(hoshasen_ga_current,0)) AS hoshasen_ga_c
, sum(coalesce(masui_ga_prev,0)) AS masui_ga_p
, sum(coalesce(masui_ga_current,0)) AS masui_ga_c
from
( (
  select
  jfm.jnl_facility_id,
  jnl_facility_name, case jnl_status when 0 then 0 else 1 end as jnl_status_c, 1 as jnl_status,
  '".$prevprevYear."' as prev, SUBSTR(coalesce(regist_date,'".$prevYear."'), 0, 5) AS current,
  0 as naika_ga_prev, (
        coalesce(naika_ip,0) + coalesce(naika_shog,0) + coalesce(naika_seip,0) + coalesce(naika_ir,0) +
        coalesce(naika_kaig,0) + coalesce(naika_kaif,0) + coalesce(naika_ak,0) + coalesce(naika_kan,0) +
        coalesce(naika_icu,0) + coalesce(naika_shoni,0) + coalesce(naika_seir,0) + coalesce(naika_tok,0) +
        coalesce(naika_nin,0)
    ) AS naika_ga_current ,
  0 as sinkei_ga_prev, (
        coalesce(sinkei_ip,0) + coalesce(sinkei_shog,0) + coalesce(sinkei_seip,0) + coalesce(sinkei_ir,0) +
        coalesce(sinkei_kaig,0) + coalesce(sinkei_kaif,0) + coalesce(sinkei_ak,0) + coalesce(sinkei_kan,0) +
        coalesce(sinkei_icu,0) + coalesce(sinkei_shoni,0) + coalesce(sinkei_seir,0) + coalesce(sinkei_tok,0) +
        coalesce(sinkei_nin,0)
    ) AS sinkei_ga_current ,
  0 as kokyu_ga_prev, (
        coalesce(kokyu_ip,0) + coalesce(kokyu_shog,0) + coalesce(kokyu_seip,0) + coalesce(kokyu_ir,0) +
        coalesce(kokyu_kaig,0) + coalesce(kokyu_kaif,0) + coalesce(kokyu_ak,0) + coalesce(kokyu_kan,0) +
        coalesce(kokyu_icu,0) + coalesce(kokyu_shoni,0) + coalesce(kokyu_seir,0) + coalesce(kokyu_tok,0) +
        coalesce(kokyu_nin,0)
    ) AS kokyu_ga_current ,
  0 as shoka_ga_prev, (
        coalesce(shoka_ip,0) + coalesce(shoka_shog,0) + coalesce(shoka_seip,0) + coalesce(shoka_ir,0) +
        coalesce(shoka_kaig,0) + coalesce(shoka_kaif,0) + coalesce(shoka_ak,0) + coalesce(shoka_kan,0) +
        coalesce(shoka_icu,0) + coalesce(shoka_shoni,0) + coalesce(shoka_seir,0) + coalesce(shoka_tok,0) +
        coalesce(shoka_nin,0)
    ) AS shoka_ga_current,
  0 as junkanki_ga_prev, (
        coalesce(junkanki_ip,0) + coalesce(junkanki_shog,0) + coalesce(junkanki_seip,0) + coalesce(junkanki_ir,0) +
        coalesce(junkanki_kaig,0) + coalesce(junkanki_kaif,0) + coalesce(junkanki_ak,0) + coalesce(junkanki_kan,0) +
        coalesce(junkanki_icu,0) + coalesce(junkanki_shoni,0) + coalesce(junkanki_seir,0) + coalesce(junkanki_tok,0) +
        coalesce(junkanki_nin,0)
    ) AS junkanki_ga_current ,
  0 as shouni_ga_prev, (
        coalesce(shouni_ip,0) + coalesce(shouni_shog,0) + coalesce(shouni_seip,0) + coalesce(shouni_ir,0) +
        coalesce(shouni_kaig,0) + coalesce(shouni_kaif,0) + coalesce(shouni_ak,0) + coalesce(shouni_kan,0) +
        coalesce(shouni_icu,0) + coalesce(shouni_shoni,0) + coalesce(shouni_seir,0) + coalesce(shouni_tok,0) +
        coalesce(shouni_nin,0)
    ) AS shouni_ga_current ,
  0 as geka_ga_prev, (
        coalesce(geka_ip,0) + coalesce(geka_shog,0) + coalesce(geka_seip,0) + coalesce(geka_ir,0) +
        coalesce(geka_kaig,0) + coalesce(geka_kaif,0) + coalesce(geka_ak,0) + coalesce(geka_kan,0) +
        coalesce(geka_icu,0) + coalesce(geka_shoni,0) + coalesce(geka_seir,0) + coalesce(geka_tok,0) +
        coalesce(geka_nin,0)
    ) AS geka_ga_current ,
  0 as seikei_ga_prev, (
        coalesce(seikei_ip,0) + coalesce(seikei_shog,0) + coalesce(seikei_seip,0) + coalesce(seikei_ir,0) +
        coalesce(seikei_kaig,0) + coalesce(seikei_kaif,0) + coalesce(seikei_ak,0) + coalesce(seikei_kan,0) +
        coalesce(seikei_icu,0) + coalesce(seikei_shoni,0) + coalesce(seikei_seir,0) + coalesce(seikei_tok,0) +
        coalesce(seikei_nin,0)
    ) AS seikei_ga_current ,
  0 as keiseibi_ga_prev, (
        coalesce(keiseibi_ip,0) + coalesce(keiseibi_shog,0) + coalesce(keiseibi_seip,0) + coalesce(keiseibi_ir,0) +
        coalesce(keiseibi_kaig,0) + coalesce(keiseibi_kaif,0) + coalesce(keiseibi_ak,0) + coalesce(keiseibi_kan,0) +
        coalesce(keiseibi_icu,0) + coalesce(keiseibi_shoni,0) + coalesce(keiseibi_seir,0) + coalesce(keiseibi_tok,0) +
        coalesce(keiseibi_nin,0)
    ) AS keiseibi_ga_current ,
  0 as nou_ga_prev, (
        coalesce(nou_ip,0) + coalesce(nou_shog,0) + coalesce(nou_seip,0) + coalesce(nou_ir,0) +
        coalesce(nou_kaig,0) + coalesce(nou_kaif,0) + coalesce(nou_ak,0) + coalesce(nou_kan,0) +
        coalesce(nou_icu,0) + coalesce(nou_shoni,0) + coalesce(nou_seir,0) + coalesce(nou_tok,0) +
        coalesce(nou_nin,0)
    ) AS nou_ga_current ,
  0 as hifu_ga_prev, (
        coalesce(hifu_ip,0) + coalesce(hifu_shog,0) + coalesce(hifu_seip,0) + coalesce(hifu_ir,0) +
        coalesce(hifu_kaig,0) + coalesce(hifu_kaif,0) + coalesce(hifu_ak,0) + coalesce(hifu_kan,0) +
        coalesce(hifu_icu,0) + coalesce(hifu_shoni,0) + coalesce(hifu_seir,0) + coalesce(hifu_tok,0) +
        coalesce(hifu_nin,0)
    ) AS hifu_ga_current ,
  0 as hinyo_ga_prev, (
        coalesce(hinyo_ip,0) + coalesce(hinyo_shog,0) + coalesce(hinyo_seip,0) + coalesce(hinyo_ir,0) +
        coalesce(hinyo_kaig,0) + coalesce(hinyo_kaif,0) + coalesce(hinyo_ak,0) + coalesce(hinyo_kan,0) +
        coalesce(hinyo_icu,0) + coalesce(hinyo_shoni,0) + coalesce(hinyo_seir,0) + coalesce(hinyo_tok,0) +
        coalesce(hinyo_nin,0)
    ) AS hinyo_ga_current ,
  0 as sanfu_ga_prev, (
        coalesce(sanfu_ip,0) + coalesce(sanfu_shog,0) + coalesce(sanfu_seip,0) + coalesce(sanfu_ir,0) +
        coalesce(sanfu_kaig,0) + coalesce(sanfu_kaif,0) + coalesce(sanfu_ak,0) + coalesce(sanfu_kan,0) +
        coalesce(sanfu_icu,0) + coalesce(sanfu_shoni,0) + coalesce(sanfu_seir,0) + coalesce(sanfu_tok,0) +
        coalesce(sanfu_nin,0)
    ) AS sanfu_ga_current ,
  0 as ganka_ga_prev, (
        coalesce(ganka_ip,0) + coalesce(ganka_shog,0) + coalesce(ganka_seip,0) + coalesce(ganka_ir,0) +
        coalesce(ganka_kaig,0) + coalesce(ganka_kaif,0) + coalesce(ganka_ak,0) + coalesce(ganka_kan,0) +
        coalesce(ganka_icu,0) + coalesce(ganka_shoni,0) + coalesce(ganka_seir,0) + coalesce(ganka_tok,0) +
        coalesce(ganka_nin,0)
    ) AS ganka_ga_current ,
  0 as jibi_ga_prev, (
        coalesce(jibi_ip,0) + coalesce(jibi_shog,0) + coalesce(jibi_seip,0) + coalesce(jibi_ir,0) +
        coalesce(jibi_kaig,0) + coalesce(jibi_kaif,0) + coalesce(jibi_ak,0) + coalesce(jibi_kan,0) +
        coalesce(jibi_icu,0) + coalesce(jibi_shoni,0) + coalesce(jibi_seir,0) + coalesce(jibi_tok,0) +
        coalesce(jibi_nin,0)
    ) AS jibi_ga_current ,
  0 as touseki_ga_prev, (
        coalesce(touseki_ip,0) + coalesce(touseki_shog,0) + coalesce(touseki_seip,0) + coalesce(touseki_ir,0) +
        coalesce(touseki_kaig,0) + coalesce(touseki_kaif,0) + coalesce(touseki_ak,0) + coalesce(touseki_kan,0) +
        coalesce(touseki_icu,0) + coalesce(touseki_shoni,0) + coalesce(touseki_seir,0) + coalesce(touseki_tok,0) +
        coalesce(touseki_nin,0)
    ) AS touseki_ga_current ,
  0 as seisin_ga_prev, (
        coalesce(seisin_ip,0) + coalesce(seisin_shog,0) + coalesce(seisin_seip,0) + coalesce(seisin_ir,0) +
        coalesce(seisin_kaig,0) + coalesce(seisin_kaif,0) + coalesce(seisin_ak,0) + coalesce(seisin_kan,0) +
        coalesce(seisin_icu,0) + coalesce(seisin_shoni,0) + coalesce(seisin_seir,0) + coalesce(seisin_tok,0) +
        coalesce(seisin_nin,0)
    ) AS seisin_ga_current ,
  0 as sika_ga_prev, (
        coalesce(sika_ip,0) + coalesce(sika_shog,0) + coalesce(sika_seip,0) + coalesce(sika_ir,0) +
        coalesce(sika_kaig,0) + coalesce(sika_kaif,0) + coalesce(sika_ak,0) + coalesce(sika_kan,0) +
        coalesce(sika_icu,0) + coalesce(sika_shoni,0) + coalesce(sika_seir,0) + coalesce(sika_tok,0) +
        coalesce(sika_nin,0)
    ) AS sika_ga_current ,
  0 as hoshasen_ga_prev, (
        coalesce(hoshasen_ip,0) + coalesce(hoshasen_shog,0) + coalesce(hoshasen_seip,0) + coalesce(hoshasen_ir,0) +
        coalesce(hoshasen_kaig,0) + coalesce(hoshasen_kaif,0) + coalesce(hoshasen_ak,0) + coalesce(hoshasen_kan,0) +
        coalesce(hoshasen_icu,0) + coalesce(hoshasen_shoni,0) + coalesce(hoshasen_seir,0) + coalesce(hoshasen_tok,0) +
        coalesce(hoshasen_nin,0)
    ) AS hoshasen_ga_current ,
  0 as masui_ga_prev, (
        coalesce(masui_ip,0) + coalesce(masui_shog,0) + coalesce(masui_seip,0) + coalesce(masui_ir,0) +
        coalesce(masui_kaig,0) + coalesce(masui_kaif,0) + coalesce(masui_ak,0) + coalesce(masui_kan,0) +
        coalesce(masui_icu,0) + coalesce(masui_shoni,0) + coalesce(masui_seir,0) + coalesce(masui_tok,0) +
        coalesce(masui_nin,0)
    ) AS masui_ga_current
  from
  jnl_facility_master jfm left outer join
  jnl_hospital_day jhd
  on((jfm.jnl_facility_id=jhd.jnl_facility_id and SUBSTR(jhd.regist_date, 0, 5)='".$prevYear."' and  SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and ( jnl_status=0 or jnl_status=1 ) and
 del_flg='f' )
  or jhd.regist_date is null)
  where jnl_facility_type=1 )
union all
(
  select
  jfm.jnl_facility_id,
  jnl_facility_name, 1 as jnl_status_c,case jnl_status when 0 then 0 else 1 end as jnl_status,
  SUBSTR(coalesce(regist_date, '".$prevprevYear."'), 0, 5) as prev,'".$prevYear."' as current ,
(
        coalesce(naika_ip,0) + coalesce(naika_shog,0) + coalesce(naika_seip,0) + coalesce(naika_ir,0) +
        coalesce(naika_kaig,0) + coalesce(naika_kaif,0) + coalesce(naika_ak,0) + coalesce(naika_kan,0) +
        coalesce(naika_icu,0) + coalesce(naika_shoni,0) + coalesce(naika_seir,0) + coalesce(naika_tok,0) +
        coalesce(naika_nin,0)
    ) AS naika_ga_prev, 0 as naika_ga_current
    , (
        coalesce(sinkei_ip,0) + coalesce(sinkei_shog,0) + coalesce(sinkei_seip,0) + coalesce(sinkei_ir,0) +
        coalesce(sinkei_kaig,0) + coalesce(sinkei_kaif,0) + coalesce(sinkei_ak,0) + coalesce(sinkei_kan,0) +
        coalesce(sinkei_icu,0) + coalesce(sinkei_shoni,0) + coalesce(sinkei_seir,0) + coalesce(sinkei_tok,0) +
        coalesce(sinkei_nin,0)
    ) AS sinkei_ga_prev, 0 as sinkei_ga_current
    , (
        coalesce(kokyu_ip,0) + coalesce(kokyu_shog,0) + coalesce(kokyu_seip,0) + coalesce(kokyu_ir,0) +
        coalesce(kokyu_kaig,0) + coalesce(kokyu_kaif,0) + coalesce(kokyu_ak,0) + coalesce(kokyu_kan,0) +
        coalesce(kokyu_icu,0) + coalesce(kokyu_shoni,0) + coalesce(kokyu_seir,0) + coalesce(kokyu_tok,0) +
        coalesce(kokyu_nin,0)
    ) AS kokyu_ga_prev, 0 as kokyu_ga_current
    , (
        coalesce(shoka_ip,0) + coalesce(shoka_shog,0) + coalesce(shoka_seip,0) + coalesce(shoka_ir,0) +
        coalesce(shoka_kaig,0) + coalesce(shoka_kaif,0) + coalesce(shoka_ak,0) + coalesce(shoka_kan,0) +
        coalesce(shoka_icu,0) + coalesce(shoka_shoni,0) + coalesce(shoka_seir,0) + coalesce(shoka_tok,0) +
        coalesce(shoka_nin,0)
    ) AS shoka_ga_prev, 0 as shoka_ga_current
    , (
        coalesce(junkanki_ip,0) + coalesce(junkanki_shog,0) + coalesce(junkanki_seip,0) + coalesce(junkanki_ir,0) +
        coalesce(junkanki_kaig,0) + coalesce(junkanki_kaif,0) + coalesce(junkanki_ak,0) + coalesce(junkanki_kan,0) +
        coalesce(junkanki_icu,0) + coalesce(junkanki_shoni,0) + coalesce(junkanki_seir,0) + coalesce(junkanki_tok,0) +
        coalesce(junkanki_nin,0)
    ) AS junkanki_ga_prev, 0 as junkanki_ga_current
    , (
        coalesce(shouni_ip,0) + coalesce(shouni_shog,0) + coalesce(shouni_seip,0) + coalesce(shouni_ir,0) +
        coalesce(shouni_kaig,0) + coalesce(shouni_kaif,0) + coalesce(shouni_ak,0) + coalesce(shouni_kan,0) +
        coalesce(shouni_icu,0) + coalesce(shouni_shoni,0) + coalesce(shouni_seir,0) + coalesce(shouni_tok,0) +
        coalesce(shouni_nin,0)
    ) AS shouni_ga_prev, 0 as shouni_ga_current
    , (
        coalesce(geka_ip,0) + coalesce(geka_shog,0) + coalesce(geka_seip,0) + coalesce(geka_ir,0) +
        coalesce(geka_kaig,0) + coalesce(geka_kaif,0) + coalesce(geka_ak,0) + coalesce(geka_kan,0) +
        coalesce(geka_icu,0) + coalesce(geka_shoni,0) + coalesce(geka_seir,0) + coalesce(geka_tok,0) +
        coalesce(geka_nin,0)
    ) AS geka_ga_prev, 0 as geka_ga_current
    , (
        coalesce(seikei_ip,0) + coalesce(seikei_shog,0) + coalesce(seikei_seip,0) + coalesce(seikei_ir,0) +
        coalesce(seikei_kaig,0) + coalesce(seikei_kaif,0) + coalesce(seikei_ak,0) + coalesce(seikei_kan,0) +
        coalesce(seikei_icu,0) + coalesce(seikei_shoni,0) + coalesce(seikei_seir,0) + coalesce(seikei_tok,0) +
        coalesce(seikei_nin,0)
    ) AS seikei_ga_prev, 0 as seikei_ga_current
    , (
        coalesce(keiseibi_ip,0) + coalesce(keiseibi_shog,0) + coalesce(keiseibi_seip,0) + coalesce(keiseibi_ir,0) +
        coalesce(keiseibi_kaig,0) + coalesce(keiseibi_kaif,0) + coalesce(keiseibi_ak,0) + coalesce(keiseibi_kan,0) +
        coalesce(keiseibi_icu,0) + coalesce(keiseibi_shoni,0) + coalesce(keiseibi_seir,0) + coalesce(keiseibi_tok,0) +
        coalesce(keiseibi_nin,0)
    ) AS keiseibi_ga_prev, 0 as keiseibi_ga_current
    , (
        coalesce(nou_ip,0) + coalesce(nou_shog,0) + coalesce(nou_seip,0) + coalesce(nou_ir,0) +
        coalesce(nou_kaig,0) + coalesce(nou_kaif,0) + coalesce(nou_ak,0) + coalesce(nou_kan,0)+
        coalesce(nou_icu,0) + coalesce(nou_shoni,0) + coalesce(nou_seir,0) + coalesce(nou_tok,0) +
        coalesce(nou_nin,0)
    ) AS nou_ga_prev, 0 as nou_ga_current
    , (
        coalesce(hifu_ip,0) + coalesce(hifu_shog,0) + coalesce(hifu_seip,0) + coalesce(hifu_ir,0) +
        coalesce(hifu_kaig,0) + coalesce(hifu_kaif,0) + coalesce(hifu_ak,0) + coalesce(hifu_kan,0) +
        coalesce(hifu_icu,0) + coalesce(hifu_shoni,0) + coalesce(hifu_seir,0) + coalesce(hifu_tok,0) +
        coalesce(hifu_nin,0)
    ) AS hifu_ga_prev, 0 as hifu_ga_current
    , (
        coalesce(hinyo_ip,0) + coalesce(hinyo_shog,0) + coalesce(hinyo_seip,0) + coalesce(hinyo_ir,0) +
        coalesce(hinyo_kaig,0) + coalesce(hinyo_kaif,0) + coalesce(hinyo_ak,0) + coalesce(hinyo_kan,0) +
        coalesce(hinyo_icu,0) + coalesce(hinyo_shoni,0) + coalesce(hinyo_seir,0) + coalesce(hinyo_tok,0) +
        coalesce(hinyo_nin,0)
    ) AS hinyo_ga_prev, 0 as hinyo_ga_current
    , (
        coalesce(sanfu_ip,0) + coalesce(sanfu_shog,0) + coalesce(sanfu_seip,0) + coalesce(sanfu_ir,0) +
        coalesce(sanfu_kaig,0) + coalesce(sanfu_kaif,0) + coalesce(sanfu_ak,0) + coalesce(sanfu_kan,0) +
        coalesce(sanfu_icu,0) + coalesce(sanfu_shoni,0) + coalesce(sanfu_seir,0) + coalesce(sanfu_tok,0) +
        coalesce(sanfu_nin,0)
    ) AS sanfu_ga_prev, 0 as sanfu_ga_current
    , (
        coalesce(ganka_ip,0) + coalesce(ganka_shog,0) + coalesce(ganka_seip,0) + coalesce(ganka_ir,0) +
        coalesce(ganka_kaig,0) + coalesce(ganka_kaif,0) + coalesce(ganka_ak,0) + coalesce(ganka_kan,0) +
        coalesce(ganka_icu,0) + coalesce(ganka_shoni,0) + coalesce(ganka_seir,0) + coalesce(ganka_tok,0) +
        coalesce(ganka_nin,0)
    ) AS ganka_ga_prev, 0 as ganka_ga_current
    , (
        coalesce(jibi_ip,0) + coalesce(jibi_shog,0) + coalesce(jibi_seip,0) + coalesce(jibi_ir,0) +
        coalesce(jibi_kaig,0) + coalesce(jibi_kaif,0) + coalesce(jibi_ak,0) + coalesce(jibi_kan,0) +
        coalesce(jibi_icu,0) + coalesce(jibi_shoni,0) + coalesce(jibi_seir,0) + coalesce(jibi_tok,0) +
        coalesce(jibi_nin,0)
    ) AS jibi_ga_prev, 0 as jibi_ga_current
    , (
        coalesce(touseki_ip,0) + coalesce(touseki_shog,0) + coalesce(touseki_seip,0) + coalesce(touseki_ir,0) +
        coalesce(touseki_kaig,0) + coalesce(touseki_kaif,0) + coalesce(touseki_ak,0) + coalesce(touseki_kan,0) +
        coalesce(touseki_icu,0) + coalesce(touseki_shoni,0) + coalesce(touseki_seir,0) + coalesce(touseki_tok,0) +
        coalesce(touseki_nin,0)
    ) AS touseki_ga_prev, 0 as touseki_ga_current
    , (
        coalesce(seisin_ip,0) + coalesce(seisin_shog,0) + coalesce(seisin_seip,0) + coalesce(seisin_ir,0) +
        coalesce(seisin_kaig,0) + coalesce(seisin_kaif,0) + coalesce(seisin_ak,0) + coalesce(seisin_kan,0) +
        coalesce(seisin_icu,0) + coalesce(seisin_shoni,0) + coalesce(seisin_seir,0) + coalesce(seisin_tok,0) +
        coalesce(seisin_nin,0)
    ) AS seisin_ga_prev, 0 as seisin_ga_current
    , (
        coalesce(sika_ip,0) + coalesce(sika_shog,0) + coalesce(sika_seip,0) + coalesce(sika_ir,0) +
        coalesce(sika_kaig,0) + coalesce(sika_kaif,0) + coalesce(sika_ak,0) + coalesce(sika_kan,0) +
        coalesce(sika_icu,0) + coalesce(sika_shoni,0) + coalesce(sika_seir,0) + coalesce(sika_tok,0) +
        coalesce(sika_nin,0)
    ) AS sika_ga_prev, 0 as sika_ga_current
    , (
        coalesce(hoshasen_ip,0) + coalesce(hoshasen_shog,0) + coalesce(hoshasen_seip,0) + coalesce(hoshasen_ir,0) +
        coalesce(hoshasen_kaig,0) + coalesce(hoshasen_kaif,0) + coalesce(hoshasen_ak,0) + coalesce(hoshasen_kan,0) +
        coalesce(hoshasen_icu,0) + coalesce(hoshasen_shoni,0) + coalesce(hoshasen_seir,0) + coalesce(hoshasen_tok,0) +
        coalesce(hoshasen_nin,0)
    ) AS hoshasen_ga_prev, 0 as hoshasen_ga_current
    , (
        coalesce(masui_ip,0) + coalesce(masui_shog,0) + coalesce(masui_seip,0) + coalesce(masui_ir,0) +
        coalesce(masui_kaig,0) + coalesce(masui_kaif,0) + coalesce(masui_ak,0) + coalesce(masui_kan,0) +
        coalesce(masui_icu,0) + coalesce(masui_shoni,0) + coalesce(masui_seir,0) + coalesce(masui_tok,0) +
        coalesce(masui_nin,0)
    ) AS masui_ga_prev, 0 as masui_ga_current  from
  jnl_facility_master jfm left outer join
  jnl_hospital_day jhd
  on((jfm.jnl_facility_id=jhd.jnl_facility_id and
SUBSTR(jhd.regist_date, 0, 5)='".$prevprevYear."' and
 SUBSTR(jhd.regist_date, 5, 2)='".$strMonth."' and ( jnl_status=0 or jnl_status=1 ) and  del_flg='f')
  or jhd.regist_date is null)
  where jnl_facility_type=1 )
) as tt group by prev, current, jnl_facility_id,jnl_facility_name,jnl_facility_id order by jnl_facility_id";
     return $this->getDateList($sql);


}







}




?>
