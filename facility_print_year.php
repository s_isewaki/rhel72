<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 年（印刷）</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_year_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script src=\"js/showpage.js\" src=\"js/showpage.js\"></script>");
	echo("<script src=\"js/showpage.js\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script src=\"js/showpage.js\" src=\"js/showpage.js\"></script>");
	echo("<script src=\"js/showpage.js\">showLoginPage(window);</script>");
	exit;
}

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 前年・翌年のタイプスタンプを変数に格納
$last_year = get_last_year($date);
$next_year = get_next_year($date);
?>
<script type="text/javascript" src="./js/fontsize.js"></script>
<script type="text/javascript">
function printPage() {
	window.print();
	window.close();
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
@media print {
	.noprint {visibility:hidden;}
}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<center>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><span class="noprint"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;&nbsp;<? show_range_string($con, $fname, $range); ?></font></span><img src="img/spacer.gif" width="180" height="1" alt=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14"><? show_date_string($last_year, $date, $next_year, $session, $range); ?></font></td>
<?
	echo("<td align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">\n");
	$time_str = date("Y")."/".date("m")."/".date("d")." ".date("H").":".date("i");
	echo("作成日時：$time_str\n");
	echo("</font></td>\n");
?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_reservation($con, $categories, $range, $date, $start_wd, $session, $fname); ?>
</table>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
// 表示範囲を出力
function show_range_string($con, $fname, $range) {
	if ($range == "all") {
		$range_string = "全て";
	} else if (strpos($range, "-") === false) {
		$sql = "select fclcate_name from fclcate";
		$cond = "where fclcate_id = $range";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "【" . pg_fetch_result($sel, 0, "fclcate_name") . "】";
	} else {
		list($cate_id, $facility_id) = explode("-", $range);
		$sql = "select facility_name from facility";
		$cond = "where fclcate_id = $cate_id and facility_id = $facility_id";
		$sel = select_from_table($con, $sql, $cond, $fname);
		if ($sel == 0) {
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$range_string = "・" . pg_fetch_result($sel, 0, "facility_name");
	}

	echo($range_string);
}

// 選択日付を出力
function show_date_string($last_year, $date, $next_year, $session, $range) {
	$y = date("Y", $date);
	echo("【{$y}】");
}

// 予約状況を出力
function show_reservation($con, $categories, $range, $date, $start_wd, $session, $fname) {
	list($selected_category_id, $selected_facility_id) = split("-", $range);

	$facility_ids = array();
	foreach ($categories as $tmp_category) {
		foreach ($tmp_category["facilities"] as $tmp_facility) {
			$facility_ids[] = $tmp_facility["facility_id"];
		}
	}

	// 日付配列を作成
	$year = date("Y", $date);
	$arr_year = array();
	for ($i = 1; $i <= 12; $i++) {
		$tmp_month = substr("0$i", -2);
		array_push($arr_year, get_arr_month("$year$tmp_month", $start_wd));
	}

	// 年間の日ごとの予約件数を、キーが年月日の配列でまとめて取得。
	$arr_rsv_count = get_arr_rsv_count($con, $fname, $year, $selected_category_id, $selected_facility_id, $facility_ids);

	// 配列を4か月単位に分割
	$arr_year = array_chunk($arr_year, 4);

	// カレンダーを出力
	for ($i = 0; $i <= 2; $i++) {
		echo("<tr>\n");
		for ($j = 0; $j <= 3; $j++) {
			$month = substr("0" . ($i * 4 + $j + 1), -2);
			$day = date("d", $date);
			if (!checkdate($month, $day, $year)) {
				$day = substr("0" . $day - 1, -2);
				if (!checkdate($month, $day, $year)) {
					$day = substr("0" . $day - 1, -2);
				}
			}
			$tmp_date = mktime(0, 0, 0, $month, $day, $year);

			echo("<td valign=\"top\" width=\"25%\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:#5279a5 solid 1px;\">\n");
			echo("<tr>\n");
			echo("<td>\n");

			echo("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">\n");
			echo("<tr>\n");
			echo("<td colspan=\"7\" height=\"22\" bgcolor=\"f6f9ff\">\n");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$year/$month</font>\n");
			echo("</td>\n");
			echo("</tr>\n");

			echo("<tr>\n");
			for ($k = 0; $k < 7; $k++) {
				$tmp_wd = $start_wd + $k;
				if ($tmp_wd >= 7) {
					$tmp_wd -= 7;
				}

				switch ($tmp_wd) {
				case 0:
					$show_wd = "日";
					$bgcolor = "#fadede";
					break;
				case 1:
					$show_wd = "月";
					$bgcolor = "#bdd1e7";
					break;
				case 2:
					$show_wd = "火";
					$bgcolor = "#bdd1e7";
					break;
				case 3:
					$show_wd = "水";
					$bgcolor = "#bdd1e7";
					break;
				case 4:
					$show_wd = "木";
					$bgcolor = "#bdd1e7";
					break;
				case 5:
					$show_wd = "金";
					$bgcolor = "#bdd1e7";
					break;
				case 6:
					$show_wd = "土";
					$bgcolor = "#defafa";
					break;
				}

				echo("<td align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
			}
			echo("</tr>\n");

			foreach ($arr_year[$i][$j] as $arr_week) {
				echo("<tr>\n");

				// 日付セルを出力
				foreach ($arr_week as $tmp_ymd) {
					$tmp_year = substr($tmp_ymd, 0, 4);
					$tmp_month = substr($tmp_ymd, 4, 2);
					$tmp_day = intval(substr($tmp_ymd, 6, 2));
					$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);

					// 他の月の場合は空表示
					if ($tmp_month != $month) {
						echo("<td>&nbsp;</td>\n");

					// 当月の場合は詳細表示
					} else {

						// 予定件数を取得
						$rsv_count = $arr_rsv_count["$tmp_ymd"];
						if ($rsv_count > 0) {
							$bgcolor = " bgcolor=\"#fbdade\"";
						} else {
							$bgcolor = "";
						}
						echo("<td align=\"center\"$bgcolor><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_day</font></td>\n");
					}
				}

				echo("</tr>\n");
			}

			echo("</table>\n");

			echo("</td>\n");
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
			if ($j < 3) {
				echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"5\" height=\"1\"></td>\n");
			}
		}
		echo("</tr>\n");
		if ($i < 2) {
			echo("<tr>\n");
			echo("<td colspan=\"4\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
			echo("</tr>\n");
		}
	}
}
?>
