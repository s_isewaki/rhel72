<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 委員会・WG | 委員会・WG一覧</title>
<?
require_once("Cmx.php");
require_once("aclg_set.php");
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_project_list.ini");
require_once("show_class_name.ini");
require_once("show_project_schedule_chart.ini");
require_once("show_project_schedule_common.ini");
require_once("library_common.php");
require_once("show_bbsthread.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showLoginPage(window);</script>");
    exit;
}

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);


// 表示順のデフォルトは開始日の降順
if ($order == "") {
    $order = "1";
}

// 表示条件のデフォルトは「終了済みを表示しない」
if ($view == "") {
    $view = "1";
}

// 年月のデフォルトは当年当月
if ($year == "") {
    $year = date("Y");
    $month = date("m");
}

// データベースに接続
$con = connect2db($fname);

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 公開申請中議事録件数の取得
$sql = "select count(*) from proceeding";
$cond = "where prcd_create_emp_id = '$emp_id' and prcd_status = '1'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$prcd_cnt1 = pg_fetch_result($sel, 0, 0) . "件";
if ($prcd_cnt1 > 0) {
    $prcd_cnt1 = "<a href=\"proceeding_list1.php?session=$session\">$prcd_cnt1</a>";
}

$str_bgcolor="bgcolor=\"FFF1E3\"";//表示項目の背景色を設定


/*
// 決裁権限の取得
$sql = "select emp_aprv_auth from authmst";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$aprv_auth = pg_fetch_result($sel, 0, "emp_aprv_auth");
*/

// 承認待ち議事録件数の取得
//if ($aprv_auth == "t") {
/*
    $sql = "select count(*) from proceeding";
    $cond = "where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null)";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $prcd_cnt2 = pg_fetch_result($sel, 0, 0) . "件";
    if ($prcd_cnt2 > 0) {
        $prcd_cnt2 = "<a href=\"proceeding_list2.php?session=$session\">$prcd_cnt2</a>";
    }
*/

$sql = "select * from proceeding";
$cond = "where prcd_status = '1' and pjt_schd_id in (select pjt_schd_id from prcdaprv where prcdaprv_emp_id = '$emp_id' and prcdaprv_date is null)";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}

$prcd_cnt2=0;
while ($row = pg_fetch_array($sel))
{

    $tmp_pjt_schd_id = $row["pjt_schd_id"];

    //承認階層が自分のレベルまで来ているかチェックする
    $sql3 = "select a.prcdaprv_order as my_order ,b.order_level from prcdaprv a ,
        (select MIN(prcdaprv_order) as order_level from prcdaprv where pjt_schd_id='$tmp_pjt_schd_id' and prcdaprv_date is null) b ";
    $cond3 = "where a.prcdaprv_emp_id='$emp_id' and a.pjt_schd_id='$tmp_pjt_schd_id' and prcdaprv_date is null";
    $sel3 = select_from_table($con, $sql3, $cond3, $fname);
    if ($sel3 == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    //自分の承認階層数
    $my_order = pg_fetch_result($sel3, 0, "my_order");

    //現在の承認階層数
    $order_level = pg_fetch_result($sel3, 0, "order_level");

    if($my_order == $order_level)
    {
        //自分の承認階層と現在の承認階層が同じであったら表示する
        //改修前のデータはカラムが無いのでデータなしの場合も表示する

        $prcd_cnt2++;

    }
}


if ($prcd_cnt2 > 0)
{
    //$prcd_cnt2 = pg_fetch_result($sel, 0, 0) . "件";
    $prcd_cnt2 = $prcd_cnt2 . "件";
    $prcd_cnt2 = "<a href=\"proceeding_list2.php?session=$session\">$prcd_cnt2</a>";
}
else
{
    $prcd_cnt2 = $prcd_cnt2 . "件";
}


//}

$arr_class_name = get_class_name_array($con, $fname);

// 組織1階層目の一覧を取得
$sql = "select class_id, class_nm from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
    pg_close($con);
    echo("<script type='text/javascript' src='./js/showpage.js'></script>");
    echo("<script language='javascript'>showErrorPage(window);</script>");
    exit;
}
$classes = array();
while ($row = pg_fetch_array($sel)) {
    $classes[$row["class_id"]] = $row["class_nm"];
}

// 組織の1階層目も2階層目も未指定の場合
if (!isset($_GET["class_id"]) && !isset($_GET["atrb_id"])) {

    // デフォルトの組織情報を取得
    $sql = "select class_id, atrb_id from prodfltcls";
    $cond = "where emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == "0") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    if (pg_num_rows($sel) > 0) {
        $class_id = pg_fetch_result($sel, 0, "class_id");
        $atrb_id = pg_fetch_result($sel, 0, "atrb_id");
    }

// 組織1階層目が指定されている場合
} else {

    // デフォルトの組織情報をいったんDELETE
    $sql = "delete from prodfltcls";
    $cond = "where emp_id = '$emp_id'";
    $del = delete_from_table($con, $sql, $cond, $fname);
    if ($del == "0") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    // 登録値の編集
    if ($class_id == "") {
        $class_id = null;
    }
    if ($atrb_id == "") {
        $atrb_id = null;
    }

    // デフォルトの組織情報を更新
    $sql = "insert into prodfltcls (emp_id, class_id, atrb_id) values (";
    $content = array($emp_id, $class_id, $atrb_id);
    $ins = insert_into_table($con, $sql, $content, $fname);
    if ($ins == "0") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
}

// 組織1階層目が選択されている場合
if ($class_id != "") {

    // 組織2階層目の一覧を取得
    $sql = "select atrb_id, atrb_nm from atrbmst";
    $cond = "where class_id = $class_id and atrb_del_flg = 'f' order by order_no";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == "0") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }
    $attributes = array();
    while ($row = pg_fetch_array($sel)) {
        $attributes[$row["atrb_id"]] = $row["atrb_nm"];
    }
}

// 委員会かWGが選択されている場合
if ($pjt_id != "") {

    // WGかどうかチェック
    $sql = "select pjt_parent_id from project";
    $cond = "where pjt_id = '$pjt_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    $pjt_parent_id = pg_fetch_result($sel, 0, "pjt_parent_id");
    $wg_flg = ($pjt_parent_id != "");

    // 委員会名を取得
    $sql = "select pjt_name from project";
    if ($wg_flg) {
        $cond = "where pjt_id = '$pjt_parent_id'";
    } else {
        $cond = "where pjt_id = '$pjt_id'";
    }
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $pjt_name = pg_fetch_result($sel, 0, "pjt_name");

    // WGの場合、WG名を取得
    if ($wg_flg)
    {
        $sql = "select pjt_name from project";
        $cond = "where pjt_id = '$pjt_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $wg_name = pg_fetch_result($sel, 0, "pjt_name");

        //WGの場合は所定の色を選択
        $wk_com_bgcolor ="#ccffcc";
    }
    else
    {
        //委員会の場合は所定の色を選択
        $wk_com_bgcolor ="#defafa";
    }

    // スケジュール一覧を取得
    $date_format = ($month == "") ? "yyyy" : "yyyymm";
    $compare_date = ($month == "") ? $year : "$year$month";
    $sql = "select proschd.pjt_schd_id, proschd.pjt_schd_title, proschd.pjt_schd_type,
    proschd.pjt_schd_start_time, proschd.pjt_schd_dur, proschd.pjt_schd_start_date, proschd.pjt_schd_start_time_v,
    proschd.pjt_schd_dur_v, proschd.emp_id, proceeding.prcd_create_emp_id , proceeding.prcd_status


    from proschd left join proceeding on proceeding.pjt_schd_id = proschd.pjt_schd_id
    where proschd.pjt_id = $pjt_id and to_char(proschd.pjt_schd_start_date, '$date_format') = '$compare_date'

    union select proschd2.pjt_schd_id, proschd2.pjt_schd_title, proschd2.pjt_schd_type, null, null,
    proschd2.pjt_schd_start_date, null, null, proschd2.emp_id, proceeding.prcd_create_emp_id, proceeding.prcd_status


    from proschd2
    left join proceeding on proceeding.pjt_schd_id = proschd2.pjt_schd_id where proschd2.pjt_id = '$pjt_id' and
    to_char(proschd2.pjt_schd_start_date, '$date_format') = '$compare_date' order by 6, 4";

    $cond = "";
    $sel_schd = select_from_table($con, $sql, $cond, $fname);
    if ($sel_schd == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }

    // ログインユーザが委員会責任者かどうかチェック
    $sql = "select count(*) from project";
    $cond = "where pjt_id = $pjt_id and pjt_response = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $response_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

    // ログインユーザが事務局メンバーかどうかチェック
    $sql = "select count(*) from promember";
    $cond = "where pjt_id = $pjt_id and member_kind = '1' and emp_id = '$emp_id'";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $secretariat_flg = (intval(pg_fetch_result($sel, 0, 0)) > 0);

    // タスク一覧を取得
    $sql = "select work_id, work_name, work_status, work_priority, to_char(work_start_date, 'YYYY-MM-DD') as work_start_date, to_char(work_expire_date, 'YYYY-MM-DD') as work_expire_date, emp_id from prowork";
    $cond = "where pjt_id = '$pjt_id'";
    $cond .= " and ((to_char(work_start_date, '$date_format') = '$compare_date') or (to_char(work_expire_date, '$date_format') = '$compare_date') or (to_char(work_update, '$date_format') = '$compare_date'))";
    $cond .= " and work_status <> '1'";
    $cond .= " order by work_priority desc, work_expire_date";
    $sel_work = select_from_table($con, $sql, $cond, $fname);
    if ($sel_work == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>\n");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>\n");
        exit;
    }




    // 文書管理権限の取得
    $lib_auth = check_authority($session, 32, $fname);
    if ($lib_auth != "0") {

        // 文書カテゴリIDの取得
        $sql = "select lib_cate_id from libcate where lib_archive = '4' and lib_link_id = '$pjt_id' and libcate_del_flg = 'f'";
        $lib_cate_id = lib_get_one($con, $fname, $sql);

        $lock_show_flg = lib_get_lock_show_flg($con, $fname);
        $lock_show_all_flg = lib_get_lock_show_all_flg($con, $fname);
        $libfolders = array();
        lib_cre_tmp_concurrent_table($con, $fname, $emp_id);
        $opt = array("is_admin"=>"", "is_login"=>"", "is_narrow_mode"=>1, "is_ignore_class_atrb"=>1);
        if ($lib_folder_id) {
            $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "4", $lib_cate_id, $lib_folder_id, $opt);
        } else {
            $sql = lib_get_libcate_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "4", $lib_cate_id, 0, 0, $opt);
        }
        $row = lib_get_top_row($con, $fname, $sql);
        $cur_upd_flg = $row["writable"];

        $sql =
        " select count(*) from project".
        " where pjt_id = $pjt_id".
        " and (".
        "     pjt_response = '$emp_id'".
        "     or pjt_id in (select pjt_id from promember where emp_id = '$emp_id')".
        " )".
        " and pjt_delete_flag = 'f'".
        " or exists (select * from libcateupdemp where libcateupdemp.lib_cate_id = $lib_cate_id and libcateupdemp.emp_id = '$emp_id') ";
        $cnt = lib_get_one($con, $fname, $sql);
        $folder_modify_flg = ($cnt > 0 && $cur_upd_flg);

        // ボタンのdisabled状況を設定
        $parent_button_disabled = ($lib_folder_id == "");
        $folder_button_disabled = (!$folder_modify_flg);
        $document_button_disabled = (!$folder_modify_flg);
        $edit_button_disabled = (!$folder_modify_flg);
        $delete_button_disabled = (!$folder_modify_flg);

        // 文書のデフォルトの並び順は文書名の昇順
        if ($lib_order == "") $lib_order = "1";

        $sel_child_folder = array();
        $_folder_id = $lib_folder_id;
        $_lib_cate_id = $lib_cate_id;
        $_lib_archive = "4";
        $_link_id = 0;
        if ($_folder_id) {
            // これがリンクかどうか確認
            $sel = select_from_table($con, "select * from libfolder where folder_id = ".(int)$lib_folder_id, "", $fname);
            if ($sel == 0) libcom_goto_error_page_and_die($sql);
            $_row = @pg_fetch_assoc($sel);
            $_link_id = $_row["link_id"];
            // リンクだったら実体取得
            if ($_row["link_id"]) {
                $sel2 = select_from_table($con, "select * from libfolder where folder_id = ".(int)$_row["link_id"], "", $fname);
                if ($sel2 == 0) libcom_goto_error_page_and_die($sql);
                $_row2 = @pg_fetch_assoc($sel2);
                $_folder_id = $_row2["folder_id"];
                $_lib_cate_id = $_row2["lib_cate_id"];
                $_lib_archive = $_row2["lib_archive"];

                // ボタンのdisabled状況を変更
                $folder_button_disabled = true;
                $document_button_disabled = true;
                $edit_button_disabled = true;
                $delete_button_disabled = true;
            }
        }
        // フォルダ一覧。親がリンクなら、フォルダ一覧は取得しない。
        if (!$_link_id) {
            $opt = array("is_admin"=>"", "is_login"=>"", "is_narrow_mode"=>1, "with_child"=>1, "exclude_self"=>1);
            $sql = lib_get_libfolder_sql($emp_id, $lock_show_flg, $lock_show_all_flg, "4", $lib_cate_id, $lib_folder_id, $opt);
            $sel_child_folder = select_from_table($con, $sql, "", $fname);
            if ($sel_child_folder == 0) libcom_goto_error_page_and_die($sql." ".$cond);
        }

        // 文書一覧。親がリンクなら実体で検索。
        $opt = array("is_admin"=>"", "is_login"=>"");
        $sql = lib_get_libinfo_sql($emp_id, $lock_show_flg, $lock_show_all_flg, $_lib_archive, $_lib_cate_id, $_folder_id, 0, $opt);
        $sel = select_from_table($con, $sql, "", $fname);
        if ($sel == 0) libcom_goto_error_page_and_die($sql);
        // 文書一覧を取得
        $document_list = array();
        while ($row = pg_fetch_assoc($sel)) {
            if ($_link_id) $row["writable"] = 0;
            $document_list[]= $row;
        }

        // フォルダが選択されている場合、親フォルダのIDを取得
        $lib_parent_folder_id = "";
        if ($lib_folder_id != "") $lib_parent_folder_id = lib_get_one($con, $fname, "select parent_id from libtree where child_id = $lib_folder_id");
    }





    // 掲示板権限の取得
    $bbs_auth = check_authority($session, 1, $fname);
    if ($bbs_auth != "0") {

        // 表示順のデフォルトを「ツリー表示（新しい順）」とする
        if ($bbs_sort == "") {$bbs_sort = "1";}

        // 掲示板オプション設定情報を取得
        $sql = "select newpost_days, count_per_page from bbsoption";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        if (pg_num_rows($sel) > 0) {
            $bbs_newpost_days = pg_fetch_result($sel, 0, "newpost_days");
            $bbs_count_per_page = pg_fetch_result($sel, 0, "count_per_page");
        }
        if ($bbs_newpost_days == "") {$bbs_newpost_days = 3;}
        if ($bbs_count_per_page == "") {$bbs_count_per_page = 0;}

        // 掲示板オプション設定（管理者）情報を取得
        $sql = "select allow_category, allow_theme from bbsadmopt";
        $cond = "";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $bbs_allow_category = pg_fetch_result($sel, 0, "allow_category");
        $bbs_allow_theme = pg_fetch_result($sel, 0, "allow_theme");

        // ログインユーザが参照可能かつ委員会に紐付けられている掲示板カテゴリを取得
        $bbs_categories = get_categories($con, $emp_id, $bbs_category_id, $dummy_bbs_theme_id, $fname, $pjt_id);

        // 掲示板投稿可能フラグの設定
        $bbs_category_postable = $bbs_categories[$bbs_category_id]["postable"];
        $bbs_theme_postable = $bbs_categories[$bbs_category_id]["themes"][$bbs_theme_id]["postable"];
    }




    //スケジュール登録制御用に自分が指定されている委員会・ＷＧのメンバーかどうかチェックする
    $sql="select count(pjt_id) as member_cnt from promember where pjt_id=$pjt_id and emp_id='$emp_id'
            union all
            select count(pjt_id) as member_cnt from project where pjt_id=$pjt_id and pjt_delete_flag = 'f'
            and (pjt_response='$emp_id' or pjt_reg_emp_id='$emp_id')";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == "0") {
        pg_close($con);
        echo("<script type='text/javascript' src='./js/showpage.js'></script>");
        echo("<script language='javascript'>showErrorPage(window);</script>");
        exit;
    }

    $pjt_member_flg = 0;
    while ($row = pg_fetch_array($sel))
    {
        //委員会・ＷＧのメンバーであったらカウントされる
        if($row["member_cnt"] > 0)
        {
            $pjt_member_flg = 1;
        }
    }


}

$this_url = "project_menu.php?session=$session&view=$view&order=$order&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=$lib_order";
?>
<script src="js/prototype/dist/prototype.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<link rel="stylesheet" type="text/css" href="js/yui/build/treeview/assets/skins/sam/treeview.css">
<script type="text/javascript">
function initPage() {
<? if ($pjt_id != "" && $bbs_auth != "0") { ?>
    var tree = new YAHOO.widget.TreeView('sam');
    var root = tree.getRoot();
<?
    $bbs_comp_date = date("Ymd", strtotime("-" . ($bbs_newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

    while ($tmp_data = each($bbs_categories)) {
        $tmp_category_id = $tmp_data["key"];
        $tmp_category = $tmp_data["value"];
        $tmp_category_title = $tmp_category["title"];
        $tmp_category_date = $tmp_category["date"];
        $tmp_category_count = $tmp_category["count"];
        $tmp_category_reg_emp_id = $tmp_category["reg_emp_id"];
        if ($tmp_category_reg_emp_id == $emp_id) {
            $tmp_category_disabled = "";
        } else {
            $tmp_category_disabled = "disabled";
        }
        $tmp_themes = $tmp_category["themes"];
        if ($tmp_category_id == $bbs_category_id && count($tmp_themes) > 0) {
            $tmp_category_expanded = "true";
        } else {
            $tmp_category_expanded = "false";
        }

        $tmp_category_html = "<input type=\"checkbox\" name=\"category_ids[]\" value=\"$tmp_category_id\" $tmp_category_disabled style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
        if ($tmp_category_id == $bbs_category_id && $bbs_theme_id == "") {
            $tmp_category_html .= "<b>$tmp_category_title</b>";
        } else {
            $tmp_category_html .= "<a href=\"project_menu.php?session=$session&view=$view&pjt_id=$pjt_id&bbs_category_id=$tmp_category_id&bbs_sort=$bbs_sort&bbs_show_content=$bbs_show_content#P1\">$tmp_category_title</a>";
        }
        $tmp_category_html .= "</font>";
        if ($tmp_category_date >= $bbs_comp_date && $tmp_category_count > 0) {
            $tmp_category_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">";
        }
?>
    var category<? echo($tmp_category_id); ?> = new YAHOO.widget.HTMLNode('<? echo($tmp_category_html); ?>', root, <? echo($tmp_category_expanded); ?>, true);
<?
        $bbs_theme_index = 1;
        while ($tmp_data = each($tmp_themes)) {
            $tmp_theme_id = $tmp_data["key"];
            $tmp_theme = $tmp_data["value"];
            $tmp_theme_title = $tmp_theme["title"];
            $tmp_theme_date = $tmp_theme["date"];
            $tmp_theme_count = $tmp_theme["count"];
            $tmp_theme_reg_emp_id = $tmp_theme["reg_emp_id"];
            if ($tmp_theme_reg_emp_id == $emp_id) {
                $tmp_theme_disabled = "";
            } else {
                $tmp_theme_disabled = "disabled";
            }

            $tmp_theme_html = "<input type=\"checkbox\" name=\"theme_ids[]\" value=\"$tmp_theme_id\" $tmp_theme_disabled style=\"vertical-align:middle;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">";
            if ($tmp_theme_id == $bbs_theme_id) {
                $tmp_theme_html .= "<b>$tmp_theme_title</b>";
            } else {
                $tmp_theme_html .= "<a href=\"project_menu.php?session=$session&view=$view&pjt_id=$pjt_id&bbs_category_id=$tmp_category_id&bbs_theme_id=$tmp_theme_id&bbs_sort=$bbs_sort&bbs_show_content=$bbs_show_content#P1\">$tmp_theme_title</a>";
            }
            $tmp_theme_html .= "</font>";
            if ($tmp_theme_date >= $bbs_comp_date && $tmp_theme_count > 0) {
                $tmp_theme_html .= "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\" style=\"vertical-align:middle;\">";
            }
?>
    var theme<? echo($tmp_theme_id); ?> = new YAHOO.widget.HTMLNode('<? echo($tmp_theme_html); ?>', category<? echo($tmp_category_id); ?>, false, true);
<?
            $bbs_theme_index++;
        }
    }
?>
    tree.draw();
<? } ?>
}

function controlList(img, trID) {
    var trDisplay;
    if (img.className == 'close') {
        img.className = 'open';
        trDisplay = '';
    } else {
        img.className = 'close';
        trDisplay = 'none';
    }

    var rows = document.getElementsByTagName('tr');
    for (var i = 0, j = rows.length; i < j; i++) {
        if (rows[i].id == trID) {
            rows[i].style.display = trDisplay;
        }
    }
}

function openAllProjects() {
    var images = document.getElementsByTagName('img');
    for (var i = 0, j = images.length; i < j; i++) {
        if (images[i].className == 'close') {
            images[i].className = 'open';
        }
    }

    var rows = document.getElementsByTagName('tr');
    for (var i = 0, j = rows.length; i < j; i++) {
        if (rows[i].style.display == 'none') {
            rows[i].style.display = '';
        }
    }
}

function deleteProject() {
    if (confirm('削除してよろしいですか？')) {
        document.project.submit();
    }
}

function changeView(view) {
    location.href = 'project_menu.php?session=<? echo($session); ?>&order=<? echo($order); ?>&view=' + view;
}

function changeClass(class_id) {
    location.href = 'project_menu.php?session=<? echo($session); ?>&order=<? echo($order); ?>&view=<? echo($view); ?>&class_id=' + class_id;
}

function changeAttribute(atrb_id) {
    location.href = 'project_menu.php?session=<? echo($session); ?>&order=<? echo($order); ?>&view=<? echo($view); ?>&class_id=<? echo($class_id); ?>&atrb_id=' + atrb_id;
}

function changeYear(year) {
    location.href = 'project_menu.php?session=<? echo($session); ?>&order=<? echo($order); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&month=<? echo($month); ?>&year=' + year;
}

function moveToParent() {
<? if ($lib_parent_folder_id == "") { ?>
    location.href = '<? echo($fname); ?>?session=<? echo($session); ?>&order=<? echo($order); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&lib_order=<? echo($lib_order); ?>';
<? } else { ?>
    location.href = '<? echo($fname); ?>?session=<? echo($session); ?>&order=<? echo($order); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year); ?>&month=<? echo($month); ?>&lib_folder_id=<? echo($lib_parent_folder_id); ?>&lib_order=<? echo($lib_order); ?>';
<? } ?>
}

function createFolder() {
    location.href = 'library_folder_register.php?session=<? echo($session); ?>&archive=4&cate_id=<? echo($lib_cate_id); ?>&parent=<? echo($lib_folder_id); ?>&o=<? echo($lib_order) ?>';
}

function registDocument() {
    location.href = 'library_register.php?session=<? echo($session); ?>&archive=4&category=<? echo($lib_cate_id); ?>&folder_id=<? echo($lib_folder_id); ?>&o=<? echo($lib_order) ?>';
}

function updateFolder() {
    document.libform.action = 'library_folder_update.php';
    addHiddenElement(document.libform, 'arch_id', '4');
    addHiddenElement(document.libform, 'cate_id', '<? echo($lib_cate_id); ?>');
    addHiddenElement(document.libform, 'folder_id', '<? echo($lib_folder_id) ?>');
    addHiddenElement(document.libform, 'o', '<? echo($lib_order) ?>');
    document.libform.submit();
}

function deleteDocument() {
    var checked_did = getCheckedIds(document.libform.elements['did[]']);
    var checked_fid = getCheckedIds(document.libform.elements['fid[]']);

    if (checked_did.length == 0 && checked_fid.length == 0) {
        alert('削除対象が選択されていません。');
        return;
    }

    if (!confirm('選択されたデータを削除します。よろしいですか？')) {
        return;
    }

    document.libform.action = 'library_delete.php';
    addHiddenElement(document.libform, 'archive', '4');
    addHiddenElement(document.libform, 'category', '<? echo($lib_cate_id); ?>');
    addHiddenElement(document.libform, 'folder_id', '<? echo($lib_folder_id); ?>');
    addHiddenElement(document.libform, 'o', '<? echo($lib_order) ?>');
    addHiddenElement(document.libform, 'ret_url', '<? echo($this_url) ?>');
    document.libform.submit();
}

function getCheckedIds(boxes) {
    var checked_ids = new Array();
    if (boxes) {
        if (!boxes.length) {
            if (boxes.checked) {
                checked_ids.push(boxes.value);
            }
        } else {
            for (var i = 0, j = boxes.length; i < j; i++) {
                if (boxes[i].checked) {
                    checked_ids.push(boxes[i].value);
                }
            }
        }
    }
    return checked_ids;
}

function addHiddenElement(frm, name, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = name;
    input.value = value;
    frm.appendChild(input);
}

function registBbsCategory() {
    location.href = 'bbscategory_register.php?session=<? echo($session); ?>&sort=<? echo($bbs_sort); ?>&show_content=<? echo($bbs_show_content); ?>&link_func=1&link_project=<? echo($pjt_id); ?>'
}

function registBbsTheme() {
<? if ($bbs_category_id == "") { ?>
    alert('カテゴリが選択されていません。');
<? } else { ?>
    location.href = 'bbsthread_register.php?session=<? echo($session); ?>&category_id=<? echo($bbs_category_id); ?>&sort=<? echo($bbs_sort); ?>&show_content=<? echo($bbs_show_content); ?>';
<? } ?>
}

function deleteBbsCategoryTheme() {
    var category_ids = getCheckedIds(document.delbbsform.elements['category_ids[]']);
    var theme_ids = getCheckedIds(document.delbbsform.elements['theme_ids[]']);
    if (category_ids.length == 0 && theme_ids.length == 0) {
        alert('カテゴリ・テーマが選択されていません。');
        return;
    }

    if (confirm('選択されたカテゴリ・テーマを削除します。よろしいですか？')) {
        document.delbbsform.submit();
    }
}

function updateBbsCategory() {
    location.href = 'bbscategory_update.php?session=<? echo($session); ?>&category_id=<? echo($bbs_category_id); ?>&sort=<? echo($bbs_sort); ?>&show_content=<? echo($bbs_show_content); ?>';
}

function updateBbsTheme() {
    location.href = 'bbsthread_update.php?session=<? echo($session); ?>&category_id=<? echo($bbs_category_id); ?>&theme_id=<? echo($bbs_theme_id); ?>&sort=<? echo($bbs_sort); ?>&show_content=<? echo($bbs_show_content); ?>';
}

function setBbsMail() {
    var url = 'bbs_mail_set.php';
    var mail = (document.getElementById('mail_notify').checked) ? "true" : "false";
    var params = $H({'session':'<?=$session?>','theme_id':'<?=$bbs_theme_id?>','mail':mail}).toQueryString();
    var myAjax = new Ajax.Request(
        url,
        {
            method: 'post',
            postBody: params
        }
    );
}

function changeBbsSortType(sort) {
    location.href = 'project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&bbs_category_id=<? echo($bbs_category_id); ?>&bbs_theme_id=<? echo($bbs_theme_id); ?>&bbs_sort=' + sort + '&bbs_show_content=<? echo($bbs_show_content); ?>'+'#P1';
}

function changeBbsContentView(checked) {
    var show_content = (checked) ? 't' : '';
    location.href = 'project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&bbs_category_id=<? echo($bbs_category_id); ?>&bbs_theme_id=<? echo($bbs_theme_id); ?>&bbs_sort=<? echo($bbs_sort); ?>&bbs_page=<? echo($bbs_page); ?>&bbs_show_content=' + show_content+'#P1';
}

function postBbsComment() {
<? if ($bbs_theme_id == "") { ?>
    alert('テーマが選択されていません。');
<? } else { ?>
    location.href = 'bbs_post_register.php?session=<? echo($session); ?>&theme_id=<? echo($bbs_theme_id); ?>&sort=<? echo($bbs_sort); ?>&show_content=<? echo($bbs_show_content); ?>';
<? } ?>
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
img.close {
    background-image:url("images/plus.gif");
    vertical-align:middle;
}
img.open {
    background-image:url("images/minus.gif");
    vertical-align:middle;
}
.list {border-collapse:collapse;}
.list, .list td {border:#5279a5 solid 1px;}
.list td td {border-style:none;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a name="P0" href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>
<? if ($pjt_admin_auth == "1") { ?>
<td width="85%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
<? }else{ ?>
<td width="100%" align="left"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="120" align="center" bgcolor="#5279a5"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>委員会・WG一覧</b></font></a></td>
<?
if ($pjt_id != "") {
    if (!$wg_flg) {
        $path = "project_update.php";
        $label = "委員会詳細";
    } else {
        $path = "project_wg_update.php";
        $label = "WG詳細";
    }
?>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="<? echo($path); ?>?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($label); ?></font></a></td>
<?
}
?>
<td width="5">&nbsp;</td>
<td align="right">
<select name="view" onchange="changeView(this.options[this.selectedIndex].value);" style="margin-right:40px;">
<option value="1"<? if ($view == "1") {echo(" selected");} ?>>終了済みを表示しない
<option value="2"<? if ($view == "2") {echo(" selected");} ?>>終了済みも表示する
</select>
<input type="button" value="削除" onclick="deleteProject();">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="2" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="290" <?=$str_bgcolor?>>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" <?=$str_bgcolor?>>
                <tr height="22" align="right">
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">公開申請中議事録</font></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($prcd_cnt1); ?></font></td>
                    <? //if ($aprv_auth == "t") { ?>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">承認待ち議事録</font></td>
                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($prcd_cnt2); ?></font></td>
                    <? //} ?>
                </tr>
            </table>
        </td>
        <td width="5"></td>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr height="22">
                    <td>
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b><? echo($pjt_name); ?><? if ($wg_name != "") {echo(" &gt; " . $wg_name);} ?></b></font>
                    </td>
                    <td align="right">
                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                        <span><? echo($arr_class_name[0]); ?></span>
                        <select name="class_id" onchange="changeClass(this.options[this.selectedIndex].value);">
                            <option value="">　　　　　</option>
                            <?
                            foreach ($classes as $tmp_class_id => $tmp_class_nm) {
                                echo("<option value=\"$tmp_class_id\"");
                                if ($tmp_class_id == $class_id) {
                                    echo(" selected");
                                }
                                echo(">$tmp_class_nm\n");
                            }
                            ?>
                        </select>
                        <select name="atrb_id" onchange="changeAttribute(this.options[this.selectedIndex].value);">
                            <option value="">　　　　　</option>
                            <?
                            foreach ($attributes as $tmp_atrb_id => $tmp_atrb_nm) {
                                echo("<option value=\"$tmp_atrb_id\"");
                                if ($tmp_atrb_id == $atrb_id) {
                                    echo(" selected");
                                }
                                echo(">$tmp_atrb_nm\n");
                            }
                            ?>
                        </select>
                    </font>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<form name="project" action="project_delete.php" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><div align="right"><input type="checkbox" name="del_flg" value="t">ファイルが入っている委員会の削除を許可</div></font>
        <tr valign="top">
            <td width="290">
                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                    <tr height="22" bgcolor="#f6f9ff">
                        <td width="8%" <?=$str_bgcolor?>></td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td <?=$str_bgcolor?>><img src="img/spacer.gif" width="11">&nbsp;<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会 &gt; WG</font></td>
                                    <td align="right" <?=$str_bgcolor?>><input type="button" value="全て開く" onclick="openAllProjects();"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
<? show_project_list($con, $emp_id, $session, $fname, $order, $pjt_id, $view, $class_id, $atrb_id); ?>
                </table>
                <input type="hidden" name="session" value="<? echo($session); ?>">
</form>
            </td>
            <td width="5"></td>
            <td>
<? if ($pjt_id != "") { ?>
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tr height="22">
                        <td bgcolor="white">
                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                            <a href="project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year-1); ?>&month=<? echo($month); ?>">&lt;前年</a>
                            <select name="year" onchange="changeYear(this.options[this.selectedIndex].value);">
                                <option value="<? echo($year-1); ?>"><? echo($year-1); ?>
                                <option value="<? echo($year); ?>" selected>【<? echo($year); ?>】
                                <option value="<? echo($year+1); ?>"><? echo($year+1); ?>
                            </select>
                            <a href="project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year+1); ?>&month=<? echo($month); ?>">翌年&gt;</a>
                            <span style="margin-left:10px;">
<? if ($month != "") { ?>
                                <a href="project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year); ?>">
<? } else { ?>
<b>
<? } ?>
年間
<? if ($month != "") { ?>
</a>
<? } else { ?>
</b>
<? } ?>
                            <span>
                        </font>
                        </td>
                    </tr>
                    <tr height="22">
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr height="22">
    <?
for ($m = 1; $m <= 12; $m++) {
    $m2 = sprintf("%02d", $m);
?>
                                    <td style="padding-right:5px;">
                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? if ($m2 != $month) { ?>
                                        <a href="project_menu.php?session=<? echo($session); ?>&view=<? echo($view); ?>&pjt_id=<? echo($pjt_id); ?>&year=<? echo($year); ?>&month=<? echo($m2); ?>"><? echo($m); ?>月</a>
<? } else { ?>
<b><? echo($m); ?>月</b>
<? } ?>
                                    </font>
                                    </td>
<?
}
?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="22">
<?
$timestamp_month = ($month == "") ? date("m") : $month;
$timestamp_day = date("d");
?>
                                    <td bgcolor="<?=$wk_com_bgcolor?>" colspan="5">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="padding-left:6px;">
                                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                        <a href="project_schedule_month_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0, 0, 0, $timestamp_month, 1, $year)); ?>">スケジュール</a>
                                                    </font>
                                                </td>
                                                <td align="right" style="padding-right:2px;">
<?
    if($pjt_member_flg > 0)
    {
        //委員会・ＷＧのメンバーなのでリンクを表示する→メンバーでなければ画像は非表示
?>
                                                    <img src="img/pencil.gif" alt="" width="13" height="13" onclick="window.open('project_schedule_registration.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0, 0, 0, $timestamp_month, $timestamp_day, $year)); ?>', 'newwin', 'width=640,height=700,scrollbars=yes');" style="cursor:pointer;">
<?
    }
?>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
<? if (pg_num_rows($sel_schd) > 0) { ?>
                                <tr height="22" bgcolor="#f6f9ff">
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日付</font></td>
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">時刻</font></td>
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">タイトル</font></td>
                                    <td <?=$str_bgcolor?> align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">議事録作成</font></td>
                                    <td <?=$str_bgcolor?> align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font></td>
                                </tr>
<?
while ($row = pg_fetch_array($sel_schd)) {
    $schd_bgcolor = get_schedule_color($row["pjt_schd_type"]);
    $schd_start_date = $row["pjt_schd_start_date"];
    $schd_start_time = $row["pjt_schd_start_time_v"];
    $schd_end_time = $row["pjt_schd_dur_v"];
    $schd_id = $row["pjt_schd_id"];
    $schd_title = $row["pjt_schd_title"];
    $schd_emp_id = $row["emp_id"];
    $schd_prcd_emp_id = $row["prcd_create_emp_id"];

    $schd_prcd_status = $row["prcd_status"];


    $schd_date_timestamp = mktime(0, 0, 0, substr($schd_start_date, 5, 2), substr($schd_start_date, 8, 2), substr($schd_start_date, 0, 4));

    $schd_start_time = substr($schd_start_time, 0, 2) . ":" . substr($schd_start_time, 2, 2);
    $schd_end_time = substr($schd_end_time, 0, 2) . ":" . substr($schd_end_time, 2, 2);
    if ($schd_start_time != ":" && $schd_dur != ":") {
        $schd_timeless = "";
        $schd_time_string = substr($schd_start_time, 0, 5) . "-" . substr($schd_end_time, 0, 5);
    } else {
        $schd_timeless = "t";
        $schd_time_string = "指定なし";
    }
?>
                                <tr height="22" bgcolor="<? echo($schd_bgcolor); ?>">
                                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="project_schedule_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($schd_date_timestamp); ?>"><? echo($schd_start_date); ?></a></font></td>
                                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($schd_time_string); ?></font></td>
                                    <td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="javascript:void(0);" onclick="window.open('project_schedule_update.php?session=<? echo($session); ?>&pjt_schd_id=<? echo($schd_id); ?>&timeless=<? echo($schd_timeless); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($schd_date_timestamp); ?>&time=<? echo($schd_start_time); ?>', 'newwin', 'width=640,height=480,scrollbars=yes');"><? echo($schd_title); ?></a></font></td>
                                    <td align="center"><? echo(get_prcd_btn_html($con, $schd_id, $schd_timeless, $emp_id, $session, $this_url, $fname)); ?></td>
                                    <td align="center">
                                        <input type="button" value="削除" onclick="if (confirm('削除してよろしいですか？'))
    {location.href = 'project_schedule_delete.php?session=<? echo($session); ?>&pjt_schd_id=<? echo($schd_id); ?>
    &ret_url=<? echo(urlencode($this_url)); ?>&pjt_id=<? echo($pjt_id); ?>
    &date=<? echo($schd_date_timestamp); ?>';}"
    <?
        $sql = "select prcdaprv_date from prcdaprv";
        $cond = "where pjt_schd_id = '$schd_id'";
        $cntsel = select_from_table($con, $sql, $cond, $fname);
        if ($cntsel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $approve_cnt = 0;
        while ($row = pg_fetch_array($cntsel))
        {
            if($row["prcdaprv_date"] != null)
            {
                //承認者が承認したか、差し戻した
                $approve_cnt++;
            }
        }

        //承認されていれば削除させない
            if (($schd_emp_id != $emp_id && $schd_prcd_emp_id != $emp_id && !$response_flg && !$secretariat_flg)
            || (($schd_prcd_status == "2")&&($approve_cnt > 0 )) //承認済みの場合はdisable
            || ($schd_prcd_status == "1") //一部承認の場合はdisable
            )
        {
            echo(" disabled");
        }
    ?>
                                    ></td>
                                </tr>
<?
}
?>
<? } else { ?>
                                <tr height="22">
                                    <td colspan="5" bgcolor="white">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録されていません</font>
                                    </td>
                                </tr>
<? } ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                <tr height="22">
                                    <td bgcolor="<?=$wk_com_bgcolor?>" colspan="5">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="padding-left:6px;">
                                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                        <a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo(mktime(0, 0, 0, $timestamp_month, 1, $year)); ?>">タスク</a>
                                                    </font>
                                                </td>
                                                <td align="right" style="padding-right:2px;">
                                                    <img src="img/pencil.gif" alt="" width="13" height="13" onclick="location.href = 'project_task_register.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>';" style="cursor:pointer;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
<? if (pg_num_rows($sel_work) > 0) { ?>
                                <tr height="22" bgcolor="#f6f9ff">
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">作業名</font></td>
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">担当者</font></td>
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">開始日</font></td>
                                    <td <?=$str_bgcolor?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">終了予定日</font></td>
                                    <td <?=$str_bgcolor?> align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">完了</font></td>
                                </tr>
<?
while ($row = pg_fetch_array($sel_work)) {
    $work_id = $row["work_id"];
    $work_name = $row["work_name"];
    $work_start_date = $row["work_start_date"];
    $work_expire_date = $row["work_expire_date"];
    $work_emp_id = $row["emp_id"];

    $sql = "select emp_lt_nm, emp_ft_nm from empmst";
    $cond = "where exists (select * from proworkmem where proworkmem.emp_id = empmst.emp_id and proworkmem.work_id = $work_id)";
    $sel_workmem = select_from_table($con, $sql, $cond, $fname);
    if ($sel_workmem == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>\n");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>\n");
        exit;
    }
    $work_emp_nm = array();
    while ($row_workmem = pg_fetch_array($sel_workmem)) {
        $work_emp_nm[] = $row_workmem["emp_lt_nm"] . " " . $row_workmem["emp_ft_nm"];
    }
?>
                                <tr height="22">
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <a href="project_task_update.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&work_id=<? echo($work_id); ?>"><? echo($work_name); ?></a>
                                        </font>
                                    </td>
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<? echo(join($work_emp_nm, "<br>")); ?>
                                        </font>
                                    </td>
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($work_start_date); ?></font>
                                    </td>
                                    <td>
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($work_expire_date); ?></font>
                                    </td>
                                    <td align="center">
                                        <input type="button" value="完了" onclick="location.href = 'to_do_complete.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&work_id=<? echo($work_id); ?>&ret_url=<? echo(urlencode($this_url)); ?>';"<? if ($work_emp_id != $emp_id && !$response_flg && !$secretariat_flg) {echo(" disabled");} ?>>
                                    </td>
                                </tr>
<?
}
?>
<? } else { ?>
                                <tr height="22">
                                    <td colspan="5" bgcolor="white">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録されていません</font>
                                    </td>
                                </tr>
<? } ?>
                            </table>
                        </td>
                    </tr>
<? if ($lib_auth) { ?>
                    <tr>
                        <td>
                            <form name="libform" method="post">
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list">
                                    <tr height="22">
                                        <td colspan="5" bgcolor="<?=$wk_com_bgcolor?>" style="padding-left:6px;">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                                <a href="library_list_all.php?session=<? echo($session); ?>&a=4&c=<? echo($lib_cate_id); ?>&o=<? echo($lib_order); ?>">文書管理</a>
                                            </font>
                                        </td>
                                    </tr>
                                    <tr height="22">
                                        <td colspan="5" align="right" <?=$str_bgcolor?>>
                                            <input type="button" value="上の階層へ"<? if ($parent_button_disabled) {echo(" disabled");} ?> onclick="moveToParent();">
                                            <input type="button" value="フォルダ作成"<? if ($folder_button_disabled) {echo(" disabled");} ?> onclick="createFolder();">
                                            <input type="button" value="文書登録"<? if ($document_button_disabled) {echo(" disabled");} ?> onclick="registDocument();">
                                            <input type="button" value="フォルダ更新"<? if ($edit_button_disabled) {echo(" disabled");} ?> onclick="updateFolder();">
                                            <input type="button" value="削除"<? if ($delete_button_disabled) {echo(" disabled");} ?> onclick="deleteDocument();">
                                        </td>
                                    </tr>
<? if (pg_num_rows($sel_child_folder) > 0 || count($document_list) > 0) { ?>
                                    <tr height="22" bgcolor="#f6f9ff">
                                        <td align="center" <?=$str_bgcolor?>>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">削除</font>
                                        </td>
                                        <td <?=$str_bgcolor?>>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($lib_order == "1") {  // 文書名の昇順
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=2\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">文書名</a>");
} else {
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=1\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">文書名</a>");
}
?>
                                            </font>
                                        </td>
                                        <td <?=$str_bgcolor?>>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($lib_order == "3") {  // 更新日時の昇順
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=4\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">更新日</a>");
} else {
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=3\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">更新日</a>");
}
?>
                                            </font>
                                        </td>
                                        <td <?=$str_bgcolor?>>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
if ($lib_order == "5") {  // サイズの昇順
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=6\"><img src=\"img/up.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">サイズ</a>");
} else {
    echo("<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$lib_folder_id&lib_order=5\"><img src=\"img/down.gif\" alt=\"\" width=\"17\" height=\"17\" border=\"0\" style=\"vertical-align:middle;\">サイズ</a>");
}
?>
                                            </font>
                                        </td>
                                        <td <?=$str_bgcolor?>>
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">権限</font>
                                        </td>
                                    </tr>
<? show_document_list($sel_child_folder, $document_list, "4", $lib_cate_id, $lib_folder_id, $lib_order, $folder_modify_flg, $session, $emp_id, false, $con, $fname, $this_url); ?>
<? } else { ?>
                                    <tr height="22">
                                        <td colspan="5" bgcolor="white">
                                            <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録されていません</font>
                                        </td>
                                    </tr>
<? } ?>
                                </table>
                                <input type="hidden" name="session" value="<? echo($session); ?>">
                            </form>
                        </td>
                    </tr>
<? } ?>
<? } ?>
<? if ($bbs_auth) { ?>
                    <tr height="22">
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="list" <?=$str_bgcolor?>>
                                <tr height="22">
                                    <td bgcolor="<?=$wk_com_bgcolor?>" style="padding-left:6px;">
                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
                                            <a name="P1" href="bbsthread_menu.php?session=<? echo($session); ?>">掲示板・電子会議室</a>
                                        </font>
                                    </td>
                                </tr>
<? if ($bbs_allow_category == "t" || $bbs_allow_theme == "t") { ?>
                                <tr height="22">
                                    <td align="right">
<? if ($bbs_allow_category == "t") { ?>
                                        <input type="button" value="カテゴリ作成" onclick="registBbsCategory();">
<? } ?>
<? if ($bbs_allow_theme == "t") { ?>
                                        <input type="button" value="テーマ作成"<? if (!$bbs_category_postable) {echo(" disabled");} ?> onclick="registBbsTheme();">
<? } ?>
                                    </td>
                                </tr>
<? } ?>
                                <tr height="22">
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr height="22" valign="top">
<!-- bbs left start -->
                                                <td>
<!-- theme list start -->
                                                    <form name="delbbsform" action="bbsthread_delete.php" method="post">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
                                                            <tr>
                                                                <td style="border:#5279a5 solid 1px;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                                        <tr height="25" bgcolor="#f6f9ff">
                                                                            <td style="padding:3px;" <?=$str_bgcolor?>>
                                                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">カテゴリ＞テーマ</font>
                                                                            </td>
<? if ($bbs_allow_category == "t" || $bbs_allow_theme == "t") { ?>
                                                                            <td align="right" <?=$str_bgcolor?>>
                                                                                <input type="button" value="削除" onclick="deleteBbsCategoryTheme();">
                                                                            </td>
<? } ?>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr height="400">
                                                                <td valign="top" style="border:#5279a5 solid 1px;" bgcolor="white">
<? if (count($bbs_categories) == 0)  { ?>
                                                                    <div style="padding:2px;">
                                                                        <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">登録されていません。</font>
                                                                    </div>
<? } ?>
                                                                    <div id="sam"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <input type="hidden" name="session" value="<? echo($session); ?>">
                                                        <input type="hidden" name="sort" value="<? echo($bbs_sort); ?>">
                                                        <input type="hidden" name="show_content" value="<? echo($bbs_show_content); ?>">
                                                    </form>
<!-- theme list end -->
                                                </td>
<!-- bbs left end -->
                                                <td><img src="img/spacer.gif" alt="" width="2" height="1"></td>
<!-- bbs right start -->
                                                <td width="70%" valign="top">
<!-- theme detail start -->
<? show_bbsthread_detail_ex($con, $bbs_category_id, $bbs_theme_id, $emp_id, $fname); ?>
<!-- theme detail end -->
<!-- comment view start -->
<? if ($bbs_category_id != "" && $bbs_theme_id == "") { ?>
                                                    <img src="img/spacer.gif" alt="" width="1" height="2"><br>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="1" style="border-collapse:collapse;">
                                                            <tr bgcolor="#f6f9ff">
                                                                <td style="border:#5279a5 solid 1px;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                                        <tr bgcolor="#f6f9ff">
                                                                            <td width="20%" class="spacing" <?=$str_bgcolor?>>
                                                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">テーマ一覧<input type="checkbox" name="show_content" value="t"<? if ($bbs_show_content == "t") {echo(" checked");} ?> style="margin-left:10px;" onclick="changeBbsContentView(this.checked);">内容を表示する</font>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr height="328">
                                                                <td valign="top" style="padding:0 2px; border:#5279a5 solid 1px;" bgcolor="white">
<? show_theme_list_ex($con, $bbs_category_id, $bbs_categories[$bbs_category_id]["themes"], $bbs_sort, $bbs_newpost_days, $bbs_show_content, $view, $pjt_id, $session, $fname); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
<? } ?>
<? if ($bbs_theme_id != "") { ?>
                                                        <img src="img/spacer.gif" alt="" width="1" height="2"><br>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="1" style="border-collapse:collapse;">
                                                            <tr bgcolor="#f6f9ff">
                                                                <td style="border:#5279a5 solid 1px;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                                        <tr>
                                                                            <td nowrap width="19%" <?=$str_bgcolor?>>
                                                                                <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">投稿一覧</font>
                                                                            </td>
                                                                            <td <?=$str_bgcolor?>>
                                                                                <select name="bbs_sort" onchange="changeBbsSortType(this.value);">
                                                                                    <option value="1"<? if ($bbs_sort == "1") {echo(" selected");} ?>>ツリー表示（新しい順）
                                                                                    <option value="2"<? if ($bbs_sort == "2") {echo(" selected");} ?>>ツリー表示（古い順）
                                                                                    <option value="3"<? if ($bbs_sort == "3") {echo(" selected");} ?>>投稿順表示（新しい順）
                                                                                    <option value="4"<? if ($bbs_sort == "4") {echo(" selected");} ?>>投稿順表示（古い順）
                                                                                </select>
                                                                                <nobr>
                                                                                    <font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><input type="checkbox" name="bbs_show_content" value="t"<? if ($bbs_show_content == "t") {echo(" checked");} ?> onclick="changeBbsContentView(this.checked);">内容を表示する</font>
                                                                                </nobr>
                                                                            </td>
                                                                            <td align="right" <?=$str_bgcolor?>>
                                                                                <input type="button" value="新しい投稿" onclick="postBbsComment();"<? if (!$bbs_theme_postable) {echo(" disabled");} ?>>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr height="328">
                                                                <td valign="top" style="padding:0 2px; border:#5279a5 solid 1px;" bgcolor="white">
<? show_post_list_ex($con, $bbs_theme_id, $bbs_sort, $bbs_newpost_days, $bbs_count_per_page, $bbs_page, $bbs_show_content, $emp_id, $session, $fname); ?>
                                                            </td>
                                                        </tr>
                                                    </table>
<? } ?>
<!-- comment view end -->
                                                </td>
<!-- bbs right end -->
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
<? } ?>
                </table>
            </td>
        </tr>
    </table>
</body>
<? pg_close($con); ?>
</html>
<?
// 文書一覧を出力
function show_document_list($sel_child_folder, $document_list, $a, $c, $f, $o, $folder_modify_flg, $session, $emp_id, $dragdrop_flg, $con, $fname, $this_url) {
    global $order, $view, $pjt_id, $year, $month;

    $docs = array();
    while ($row = pg_fetch_array($sel_child_folder)) {
        $docs[] = array(
            "id" => $row["folder_id"],
            "folder_id" => $row["folder_id"],
            "link_id" => $row["link_id"],
            "name" => $row["folder_name"],
            "type" => "folder",
            "file_type" => "フォルダ",
            "size" => 0,
            "writable" => $row["writable"],
            "path" => "",
            "modified" => $row["upd_time"]
        );
    }
    foreach ($document_list as $row) {
        $tmp_path = get_document_path($row["lib_archive"], $row["lib_cate_id"], $row["lib_id"], $row["lib_extension"]);
        $docs[] = array(
            "lib_id"    => $row["lib_id"],
            "id"        => $row["lib_id"],
            "name"      => $row["lib_nm"],
            "no"        => $row["lib_no"],
            "type"      => "document",
            "file_type" => lib_get_file_type_name($row["lib_type"]),
            "size"      => @filesize($tmp_path),
            "writable"=> $row["writable"], // boolean
            "path"      => $tmp_path,
            "modified"  => $row["lib_up_date"],
            "cate_id"   => $row["lib_cate_id"],
            "folder_id" => $row["folder_id"]
        );
    }
    if (!count($docs)) return;

    if ($o=="1") usort($docs, "sort_docs_by_name"); // 名前の昇順
    if ($o=="2") usort($docs, "sort_docs_by_name_desc"); // 名前の降順
    if ($o=="3") usort($docs, "sort_docs_by_modified"); // 更新日時の昇順
    if ($o=="4") usort($docs, "sort_docs_by_modified_desc"); // 更新日時の降順
    if ($o=="5") usort($docs, "sort_docs_by_size"); // サイズの昇順
    if ($o=="6") usort($docs, "sort_docs_by_size_desc"); // サイズの降順

    foreach ($docs as $tmp_doc) {
        $tmp_did = $tmp_doc["id"];
        $tmp_dnm = $tmp_doc["name"];

        // 削除チェックボックス
        $del_reason = "";
        if (!$folder_modify_flg) $del_reason = "親フォルダに更新権限がありません。"; // 親フォルダに権限なし
        else if (!$tmp_doc["writable"]) $del_reason = "更新不可です。"; // 更新権限なし
        else if ($tmp_doc["link_id"]) $del_reason = "リンクフォルダです。"; // リンクは削除不可
        else if ($tmp_doc["type"]=="folder" && $tmp_doc["folder_id"]) {
            // 表示するフォルダに、フォルダが存在するか確認したい。
            if (!$del_reason) {
                $sql = "select child_id from libtree where parent_id = ".$tmp_doc["folder_id"];
                if (lib_get_one($con, $fname, $sql)) $del_reason = "配下フォルダが存在します。";
            }
            // 表示するフォルダに、ファイルが存在するか確認したい。
            if (!$del_reason) {
                $sql = "select folder_id from libinfo where folder_id = ".$tmp_doc["folder_id"]." and lib_delete_flag = 'f' limit 1";
                if (lib_get_one($con, $fname, $sql)) $del_reason = "文書が存在します。";
            }
            // 表示するフォルダが、リンクされているか確認したい。
            if (!$del_reason) {
                $sql = "select folder_id from libfolder where link_id = ".$tmp_doc["folder_id"]." limit 1";
                if (lib_get_one($con, $fname, $sql)) $del_reason = "リンクされています。";
            }
            if (!$del_reason) {
                $exist_workflow_list = array();
                $sql =
                " select lib_folder_id from wkfwmst".
                " where lib_archive = '4'".
                " and lib_folder_id = ".$tmp_doc["folder_id"].
                " and wkfw_del_flg = 'f'";
                if (lib_get_one($con, $fname, $sql)) $del_reason = "ワークフローの保存先に指定されています。";
            }
        }
        if ($del_reason) {
            $cbox = '<input type="checkbox" disabled title="'.$del_reason.'削除できません。">';
        } else {
            if ($tmp_doc["type"] == "folder") {
                $cbox = "<input type=\"checkbox\" name=\"fid[]\" value=\"$tmp_did\">";
            } else {
                $cbox = "<input type=\"checkbox\" name=\"did[]\" value=\"$tmp_did\">";
            }
        }

        // 文書ボックスのクラス名
        $tmp_div_class = ($tmp_doc["type"] == "document" && $tmp_doc["writable"] && $folder_modify_flg) ? "doc" : "";

        // アイコン
        $icon = "<img src=\"img/icon/folder.gif\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle;\">";
        if ($tmp_doc["type"]!="folder") {
            $img = "other.jpg";
            if ($tmp_doc["file_type"]=="Word")       $img = "word.jpg";
            if ($tmp_doc["file_type"]=="Excel")      $img = "excel.jpg";
            if ($tmp_doc["file_type"]=="PowerPoint") $img = "powerpoint.jpg";
            if ($tmp_doc["file_type"]=="PDF")        $img = "pdf.jpg";
            if ($tmp_doc["file_type"]=="テキスト")   $img = "text.jpg";
            if ($tmp_doc["file_type"]=="JPEG")       $img = "jpeg.jpg";
            if ($tmp_doc["file_type"]=="GIF")        $img = "gif.jpg";
            $icon = "<img id=\"h_doc$tmp_did\" src=\"img/icon/$img\" alt=\"\" width=\"16\" height=\"16\" border=\"0\" style=\"vertical-align:middle\">";
        }

        // 名前のリンク先
        if ($tmp_doc["type"] == "folder") {
            $tmp_anchor = "<a href=\"$fname?session=$session&order=$order&view=$view&pjt_id=$pjt_id&year=$year&month=$month&lib_folder_id=$tmp_did&lib_order=$o\">$tmp_dnm</a>";
        } else {
            $tmp_lib_url = urlencode($tmp_doc["path"]);
            if (lib_get_filename_flg() > 0) {
                $tmp_anchor = "<a href=\"library_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\">$tmp_dnm</a>";
            } else {
                $tmp_anchor = "<a href=\"library_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url\" onclick=\"window.open('library_refer.php?s=$session&i=$tmp_did&u=$tmp_lib_url'); return false;\">$tmp_dnm</a>";
            }
        }

        // 更新日
        $tmp_modified_str = "-";
        if (strlen($tmp_doc["modified"]) >= 8) {
            $tmp_modified_str = preg_replace("/^(\d{4})(\d{2})(\d{2})(.*)$/", "$1/$2/$3", $tmp_doc["modified"]);
        }

        // 権限
        $tmp_auth = ($tmp_doc["writable"] && $folder_modify_flg ? "更新" : "参照");
        if ($tmp_doc["link_id"]) {
            $tmp_auth = "-";
        } else if ($tmp_doc["type"] == "folder") {
            $tmp_auth = "<a href=\"library_folder_update.php?session=$session&arch_id=$a&cate_id=$c&folder_id=$tmp_did&o=$o\">$tmp_auth</a>";
        } else {
            $tmp_auth = "<a href=\"library_detail.php?session=$session&a=$a&c=$c&f=$f&o=$o&lib_id=$tmp_did&listtype=3&back_url=" . urlencode($this_url) . "\">$tmp_auth</a>";
        }

        echo("<tr height=\"22\">\n");
        echo("<td align=\"center\">$cbox</td>\n");
        echo("<td style=\"padding:0;\"><div id=\"doc$tmp_did\" class=\"$tmp_div_class\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr valign=\"top\"><td>$icon</td><td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_anchor</font></td></tr></table></div></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_modified_str</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">".format_size($tmp_doc["size"])."</font></td>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_auth</font></td>\n");
        echo("</tr>\n");
    }
}

// テーマ内容を表示
function show_bbsthread_detail_ex($con, $category_id, $theme_id, $emp_id, $fname) {
    if ($category_id == "") {
        return;
    }

    // カテゴリ情報を取得
    $sql = "select bbscategory.bbscategory_title, bbscategory.emp_id from bbscategory";
    $cond = "where bbscategory.bbscategory_id = $category_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $category_title = pg_fetch_result($sel, 0, "bbscategory_title");
    $category_reg_emp_id = pg_fetch_result($sel, 0, "emp_id");

    if ($theme_id != "") {

        // テーマ情報を取得
        $sql = "select bbsthread.bbsthread_title, bbsthread.bbsthread, empmst.emp_lt_nm, empmst.emp_ft_nm, bbsthread.date, bbsthread.emp_id from bbsthread inner join empmst on bbsthread.emp_id = empmst.emp_id";
        $cond = "where bbsthread.bbsthread_id = $theme_id";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $thread_title = pg_fetch_result($sel, 0, "bbsthread_title");
        $thread_content = pg_fetch_result($sel, 0, "bbsthread");
        $thread_emp_nm = pg_fetch_result($sel, 0, "emp_lt_nm") . " " . pg_fetch_result($sel, 0, "emp_ft_nm");
        $thread_date = pg_fetch_result($sel, 0, "date");
        $thread_reg_emp_id = pg_fetch_result($sel, 0, "emp_id");

        $thread_content = str_replace("\n", "<br>", $thread_content);
        $thread_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $thread_date);

        // メール通知設定確認
        $sql = "select count(*) from bbsmail";
        $cond = "where bbsthread_id = $theme_id and emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }
        $cnt = pg_fetch_result($sel, 0, 0);
        $mail_checked = ($cnt != 0) ? " checked" : "";
    }

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse:collapse;\">\n");
    echo("<tr height=\"22\">\n");
    echo("<td bgcolor=\"FFF1E3\" width=\"16%\" align=\"right\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">カテゴリ</font></td>\n");
    echo("<td bgcolor=\"white\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$category_title</font></td>\n");
    echo("<td bgcolor=\"white\" width=\"14%\" align=\"center\" style=\"border:#5279a5 solid 1px;\"><input type=\"button\" value=\"編集\"");
    if ($category_reg_emp_id != $emp_id) {
        echo(" disabled");
    }
    echo(" onclick=\"updateBbsCategory();\"></td>\n");
    echo("</tr>\n");
    echo("</table>\n");
    if ($theme_id != "") {
        echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"><br>\n");
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"  style=\"border-collapse:collapse;\">\n");
        echo("<tr height=\"22\">\n");
        echo("<td bgcolor=\"FFF1E3\" width=\"16%\" align=\"right\" bgcolor=\"#f6f9ff\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">テーマ</font></td>\n");
        echo("<td bgcolor=\"white\" colspan=\"3\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$thread_title</font></td>\n");
        echo("<td bgcolor=\"white\" width=\"14%\" align=\"center\" style=\"border:#5279a5 solid 1px;\"><input type=\"button\" value=\"編集\"");
        if ($thread_reg_emp_id != $emp_id) {
            echo(" disabled");
        }
        echo(" onclick=\"updateBbsTheme();\"></td>\n");
        echo("</tr>\n");
        echo("<tr height=\"22\">\n");
        echo("<td bgcolor=\"FFF1E3\" align=\"right\" bgcolor=\"#f6f9ff\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">内容</font></td>\n");
        echo("<td bgcolor=\"white\" colspan=\"4\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$thread_content</font></td>\n");
        echo("</tr>\n");
        echo("<tr height=\"22\">\n");
        echo("<td bgcolor=\"FFF1E3\" align=\"right\" bgcolor=\"#f6f9ff\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成者</font></td>\n");
        echo("<td bgcolor=\"white\" width=\"30%\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$thread_emp_nm</font></td>\n");
        echo("<td bgcolor=\"FFF1E3\" width=\"18%\" align=\"right\" bgcolor=\"#f6f9ff\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作成日時</font></td>\n");
        echo("<td bgcolor=\"white\" colspan=\"2\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$thread_date</font></td>\n");
        echo("</tr>\n");
        echo("<tr height=\"22\">\n");
        echo("<td colspan=\"5\" valign=\"middle\" style=\"border:#5279a5 solid 1px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><input id=\"mail_notify\" type=\"checkbox\" value=\"on\" $mail_checked>このテーマへの新規投稿をウェブメールで通知する\n<input type=\"button\" value=\"更新\" onclick=\"setBbsMail();\"></font></td>\n");
        echo("</tr>\n");
        echo("</table>\n");
    }
}

// テーマ一覧を表示
function show_theme_list_ex($con, $category_id, $themes, $sort, $newpost_days, $show_content, $view, $pjt_id, $session, $fname) {
    if (count($themes) == 0) {
        echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        echo("<tr>\n");
        echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">テーマはありません。</font></td>\n");
        echo("</tr>\n");
        echo("</table>\n");
        return;
    }

    $comp_date = date("Ymd", strtotime("-" . ($newpost_days - 1) . " day", mktime(0, 0, 0, date("m"), date("d"), date("Y"))));

    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    foreach ($themes as $tmp_theme_id => $tmp_theme) {
        $tmp_title = $tmp_theme["title"];
        $tmp_date = $tmp_theme["date"];
        $tmp_time = $tmp_theme["time"];
        $tmp_count = $tmp_theme["count"];
        $tmp_emp_nm = $tmp_theme["emp_nm"];
        $tmp_content = $tmp_theme["content"];

        $new = ($tmp_date >= $comp_date && $tmp_count > 0) ? "<img src=\"img/icon/new.gif\" alt=\"NEW\" width=\"24\" height=\"10\" style=\"margin-left:4px;\">" : "";

        $tmp_date = preg_replace("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/", "$1/$2/$3 $4:$5", $tmp_date . $tmp_time);

        echo("<tr height=\"21\">\n");
        echo("<td>\n");
            echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
            echo("<tr>\n");
            echo("<td width=\"50%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"project_menu.php?session=$session&view=$view&pjt_id=$pjt_id&bbs_category_id=$category_id&bbs_theme_id=$tmp_theme_id&bbs_sort=$sort&bbs_show_content=$show_content#P1\">$tmp_title</a>$new</font></td>\n");
/*
            echo("<td width=\"20%\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_emp_nm</font></td>\n");
            echo("<td width=\"30%\" align=\"right\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_date</font></td>\n");
*/
            echo("</tr>\n");
            if ($show_content == "t") {
                echo("<tr>\n");
                echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"2\"></td>\n");
                echo("</tr>\n");
                echo("<tr>\n");
                echo("<td colspan=\"3\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">" . str_replace("\n", "<br>", $tmp_content) . "</font></td>\n");
                echo("</tr>\n");
                echo("<tr>\n");
                echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
                echo("</tr>\n");
                echo("<tr>\n");
                echo("<td colspan=\"3\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
                echo("</tr>\n");
            }
            echo("</table>\n");
        echo("</td>\n");
        echo("</tr>\n");
    }
    echo("</table>\n");
}

function show_post_list_ex($con, $theme_id, $sort, $newpost_days, $count_per_page, $page, $show_content, $emp_id, $session, $fname) {
    if ($theme_id == "") {
        return;
    }

    // ページのデフォルトは1
    if ($page == 0) {$page = 1;}

    switch ($sort) {
    case "1":  // ツリー表示（新しい順）
        $posts = get_posts_tree($con, $theme_id, true, $count_per_page, $page, $fname);
        show_posts_tree($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $count_per_page, $page);
        write_navigation_tree_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname, $posts);
        break;
    case "2":  // ツリー表示（古い順）
        $posts = get_posts_tree($con, $theme_id, false, $count_per_page, $page, $fname);
        show_posts_tree($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session, $count_per_page, $page);
        write_navigation_tree_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname, $posts);
        break;
    case "3":  // 投稿順表示（新しい順）
        $posts = get_posts_flat($con, $theme_id, true, $count_per_page, $page, $fname);
            show_posts_flat($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session);
        write_navigation_flat_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname);
        break;
    case "4":  // 投稿順表示（古い順）
        $posts = get_posts_flat($con, $theme_id, false, $count_per_page, $page, $fname);
            show_posts_flat($posts, $theme_id, $sort, $newpost_days, $show_content, $emp_id, $session);
        write_navigation_flat_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname);
        break;
    }
}

// ページ切り替えリンクの表示（ツリー表示用）
function write_navigation_tree_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname, $posts) {

    // 全件表示の場合は出力しない
    if ($count_per_page == 0) {
        return;
    }

    // ルート記事件数を算出
    $post_count = 0;
    foreach ($posts as $tmp_post) {
        if ($tmp_post["nest"] == 0) {
            $post_count++;
        }
    }

    write_navigation_ex($theme_id, $sort, $count_per_page, $page, $show_content, $session, $post_count);
}

// ページ切り替えリンクの表示（投稿順表示用）
function write_navigation_flat_ex($con, $theme_id, $sort, $count_per_page, $page, $show_content, $session, $fname) {

    // 全件表示の場合は出力しない
    if ($count_per_page == 0) {
        return;
    }

    // 投稿件数を取得
    $sql = "select count(*) from bbs";
    $cond = "where bbs_del_flg = 'f' and bbsthread_id = $theme_id";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $post_count = intval(pg_fetch_result($sel, 0, 0));

    write_navigation_ex($theme_id, $sort, $count_per_page, $page, $show_content, $session, $post_count);
}

// ページ切り替えリンクの表示（共通部分）
function write_navigation_ex($theme_id, $sort, $count_per_page, $page, $show_content, $session, $post_count) {
    global $view, $pjt_id, $bbs_category_id;

    // 1ページで収まる場合は出力しない
    if ($post_count <= $count_per_page) {
        return;
    }

    // ページ数を算出
    $max_page = ceil($post_count / $count_per_page);

    echo("<div style=\"text-align:center;margin-top:10px;\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j14\">\n");
    for ($i = 1; $i <= $max_page; $i++) {
        if ($i == $page) {
            echo("[$i]\n");
        } else {
            echo("<a href=\"project_menu.php?session=$session&view=$view&pjt_id=$pjt_id&bbs_category_id=$bbs_category_id&bbs_theme_id=$theme_id&bbs_sort=$sort&bbs_show_content=$show_content&bbs_page=$i#P1\">$i</a>\n");
        }
    }
    echo("</font></div>\n");

}
?>
