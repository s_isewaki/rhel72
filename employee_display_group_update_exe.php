<?
require("about_session.php");
require("about_authority.php");
require("employee_auth_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 19, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 入力チェック
if ($group_nm == "") {
	echo("<script type=\"text/javascript\">alert('表示グループ名が入力されていません。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}
if (strlen($group_nm) > 30) {
	echo("<script type=\"text/javascript\">alert('表示グループ名が長すぎます。');</script>");
	echo("<script type=\"text/javascript\">history.back();</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// 現在の表示グループ情報を取得
$sql = "select * from dispgroup";
$cond = "where group_id = $group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$current_group_disp = pg_fetch_array($sel, 0, PGSQL_ASSOC);

// 登録値の編集
if ($mail_flg != "t") {
	$mail_flg = "f";
}
if ($ext_flg != "t") {
	$ext_flg = "f";
}
if ($inci_flg != "t") {
	$inci_flg = "f";
}
if ($fplus_flg != "t") {
	$fplus_flg = "f";
}
if ($manabu_flg != "t") {
	$manabu_flg = "f";
}
if ($cas_flg != "t") {
	$cas_flg = "f";
}
if ($event_flg != "t") {
	$event_flg = "f";
}
if ($schd_flg != "t") {
	$schd_flg = "f";
}
if ($info_flg != "t") {
	$info_flg = "f";
}
if ($task_flg != "t") {
	$task_flg = "f";
}
if ($whatsnew_flg != "t") {
	$whatsnew_flg = "f";
}
if ($whatsnew_flg == "t") {
	$msg_flg = "t";
	$aprv_flg = "t";
} else {
	$msg_flg = "f";
	$aprv_flg = "f";
}
if ($schdsrch_flg != "t") {
	$schdsrch_flg = "f";
}
if ($fcl_flg != "t") {
	$fcl_flg = "f";
}
if ($lib_flg != "t") {
	$lib_flg = "f";
}
if ($bbs_flg != "t") {
	$bbs_flg = "f";
}
if ($intra_flg != "t") {
	$intra_flg = "f";
}
if ($bedinfo != "t") {
	$bedinfo = "f";
}
if ($schd_type != "t") {
	$schd_type = "f";
}
if ($wic_flg != "t") {
	$wic_flg = "f";
}

if ($jnl_flg != "t") {
	$jnl_flg = "f";
}

if ($top_free_text_flg != "t") {
	$top_free_text_flg = "f";
}

if ($ladder_flg != "t") {
	$ladder_flg = "f";
}

if ($top_timecard_error_flg != "t") {
	$top_timecard_error_flg = "f";
}

if ($top_workflow_flg != "t") {
	$top_workflow_flg = "f";
}

if ($top_ccusr1_flg != "t") {
	$top_ccusr1_flg = "f";
}

// 表示グループ更新
$sql = "update dispgroup set";
$set = array("group_nm", "default_page", "font_size", "top_mail_flg", "top_ext_flg", "top_event_flg", "top_schd_flg", "top_info_flg", "top_task_flg", "top_msg_flg", "top_aprv_flg", "top_schdsrch_flg", "top_lib_flg", "top_bbs_flg", "top_memo_flg", "top_link_flg", "top_intra_flg", "bed_info", "schd_type", "top_wic_flg", "top_fcl_flg", "top_inci_flg", "top_cas_flg", "top_fplus_flg", "top_jnl_flg", "top_free_text_flg", "top_manabu_flg", "ladder_flg", "top_timecard_error_flg", "top_workflow_flg", "top_ccusr1_flg");
$setvalue = array($group_nm, $default_page, $font_size, $mail_flg, $ext_flg, $event_flg, $schd_flg, $info_flg, $task_flg, $msg_flg, $aprv_flg, $schdsrch_flg, $lib_flg, $bbs_flg, $memo_flg, $link_flg, $intra_flg, $bedinfo, $schd_type, $wic_flg, $fcl_flg, $inci_flg, $cas_flg, $fplus_flg, $jnl_flg, $top_free_text_flg, $manabu_flg, $ladder_flg, $top_timecard_error_flg, $top_workflow_flg, $top_ccusr1_flg);
$cond = "where group_id = '$group_id'";
$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
if ($upd == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}


// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// 画面を再表示
echo("<script type=\"text/javascript\">location.href = 'employee_display_group.php?session=$session&group_id=$group_id';</script>");

?>
