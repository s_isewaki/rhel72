<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("show_select_values.ini");
require_once("inpatient_common.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$auth_id = ($subsys == "med") ? 57 : 14;
$checkauth = check_authority($session, $auth_id, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// 患者情報を取得
$sql = "select * from ptifmst";
$cond = "where ptif_id = '$pt_id' and ptif_del_flg = 'f'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$lt_kj_nm = pg_fetch_result($sel, 0, "ptif_lt_kaj_nm");
$ft_kj_nm = pg_fetch_result($sel, 0, "ptif_ft_kaj_nm");
$sex = get_sex_string(pg_fetch_result($sel, 0, "ptif_sex"));
$age = get_age(pg_fetch_result($sel, 0, "ptif_birth"));

// 初期表示時
if ($back != "t") {

	// 退院予定情報を取得（退院予定患者の場合）
	$sql = "select * from inptmst";
	$cond = "where ptif_id = '$pt_id' and inpt_out_res_flg = 't'";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
	if (pg_num_rows($sel) > 0) {
		$out_dt = pg_fetch_result($sel, 0, "inpt_out_res_dt");
		$out_tm = pg_fetch_result($sel, 0, "inpt_out_res_tm");
		$result = pg_fetch_result($sel, 0, "inpt_result");
		$result_dtl = pg_fetch_result($sel, 0, "inpt_result_dtl");
		$out_rsn_cd = pg_fetch_result($sel, 0, "inpt_out_rsn_cd");
		$out_pos_cd = pg_fetch_result($sel, 0, "inpt_out_pos_cd");
		$out_pos_dtl = pg_fetch_result($sel, 0, "inpt_out_pos_dtl");
		$fd_end = pg_fetch_result($sel, 0, "inpt_fd_end");
		$comment = pg_fetch_result($sel, 0, "inpt_out_comment");
		$out_inst_cd = pg_fetch_result($sel, 0, "inpt_out_inst_cd");
		$out_sect_cd = pg_fetch_result($sel, 0, "inpt_out_sect_cd");
		$out_doctor_no = pg_fetch_result($sel, 0, "inpt_out_doctor_no");
		$out_city = pg_fetch_result($sel, 0, "inpt_out_city");
		$pr_inst_cd = pg_fetch_result($sel, 0, "inpt_pr_inst_cd");
		$pr_sect_cd = pg_fetch_result($sel, 0, "inpt_pr_sect_cd");
		$pr_doctor_no = pg_fetch_result($sel, 0, "inpt_pr_doctor_no");
		$back_bldg_cd = pg_fetch_result($sel, 0, "inpt_back_bldg_cd");
		$back_ward_cd = pg_fetch_result($sel, 0, "inpt_back_ward_cd");
		$ptrm = pg_fetch_result($sel, 0, "inpt_back_ptrm_room_no");
		$bed = pg_fetch_result($sel, 0, "inpt_back_bed_no");
		$back_in_dt = pg_fetch_result($sel, 0, "inpt_back_in_dt");
		$back_in_tm = pg_fetch_result($sel, 0, "inpt_back_in_tm");
		$back_sect = pg_fetch_result($sel, 0, "inpt_back_sect_id");
		$back_doc = pg_fetch_result($sel, 0, "inpt_back_dr_id");
		$back_change_purpose = pg_fetch_result($sel, 0, "inpt_back_change_purpose");

		$out_yr = substr($out_dt, 0, 4);
		$out_mon = substr($out_dt, 4, 2);
		$out_day = substr($out_dt, 6, 2);
		$out_hr = substr($out_tm, 0, 2);
		$out_min = substr($out_tm, 2, 2);
		$ward = ($back_bldg_cd != "" && $back_ward_cd != "") ? "{$back_bldg_cd}-{$back_ward_cd}" : "";
		$back_in_yr = substr($back_in_dt, 0, 4);
		$back_in_mon = substr($back_in_dt, 4, 2);
		$back_in_day = substr($back_in_dt, 6, 2);
		$back_in_hr = substr($back_in_tm, 0, 2);
		$back_in_min = substr($back_in_tm, 2, 2);

		$action = "変更";
	} else {

		// 退院予定日時の初期値を設定
		$out_yr = date("Y");
		$out_mon = date("m");
		$out_day = date("d");
		$out_hr = date("H");
		if (date("i") < 30) {
			$out_min = "30";
		} else {
			$out_hr++;
			if ($out_hr == "24") {
				$out_hr = "00";
			}
			$out_min = "00";
		}

		// 食事終了帯の初期値を「朝」とする
		if (trim($fd_end) == "") {
			$fd_end = "1";
		}

		$action = "登録";
	}

// 処理失敗時
} else {
	$action = $action_label;
}

// 退院理由マスタを取得
list($out_rsn_rireki, $out_rsn_master) = get_rireki_index_and_master($con, "A308", $fname);

// 退院先マスタを取得
list($out_pos_rireki, $out_pos_master) = get_rireki_index_and_master($con, "A307", $fname);

// 医療機関マスタを取得
$sql = "select a.mst_cd, a.mst_name, b.rireki_index, a.address1 from institemmst a left join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd";
$cond = "order by a.mst_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$insts = array();
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]] = array(
		"name"   => $row["mst_name"],
		"rireki" => $row["rireki_index"],
		"city"   => $row["address1"]
	);
}

// 診療科マスタを取得（最新の履歴のみ）
$sql = "select a.* from institemrireki a inner join (select mst_cd, max(rireki_index) as rireki_index from institemrireki group by mst_cd) b on b.mst_cd = a.mst_cd and b.rireki_index = a.rireki_index";
$cond = "where a.disp_flg order by a.disp_order";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["name"] = $row["item_name"];
	for ($i = 1; $i <= 10; $i++) {
		if ($row["doctor_name$i"] == "" || $row["doctor_hide$i"] == "t") {
			continue;
		}
		$insts[$row["mst_cd"]]["sects"][$row["item_cd"]]["doctors"][$i] = $row["doctor_name$i"];
	}
}

// 病棟一覧の取得
$sql = "select bldg_cd, ward_cd, ward_name from wdmst";
$cond = "where ward_del_flg = 'f' order by bldg_cd, ward_cd";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$wards = array();
while ($row = pg_fetch_array($sel)) {
	$wards["{$row["bldg_cd"]}-{$row["ward_cd"]}"] = array("name" => $row["ward_name"]);
}

// 病室一覧の取得
$sql = "select bldg_cd, ward_cd, ptrm_room_no, ptrm_name, ptrm_bed_chg, ptrm_bed_cur from ptrmmst";
$cond = "where ptrm_del_flg = 'f' order by ptrm_room_no";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	$tmp_ward_code = "{$row["bldg_cd"]}-{$row["ward_cd"]}";
	if (!isset($wards[$tmp_ward_code])) {continue;}

	$tmp_ptrm_name = $row["ptrm_name"];
	$tmp_ptrm_charge = $row["ptrm_bed_chg"];
	if ($tmp_ptrm_charge > 0) {
		$tmp_ptrm_name .= "（差額" . number_format($tmp_ptrm_charge) . "円）";
	}

	$wards[$tmp_ward_code]["ptrms"][$row["ptrm_room_no"]] =
			array("name" => $tmp_ptrm_name, "bed_count" => $row["ptrm_bed_cur"]);
}

// 診療科一覧の取得
$sql = "select sect_id, sect_nm from sectmst";
$cond = "where sect_del_flg = 'f' order by sect_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$sects = array();
while ($row = pg_fetch_array($sel)) {
	$sects[$row["sect_id"]] = array("name" => $row["sect_nm"]);
}

// 主治医一覧の取得
$sql = "select sect_id, dr_id, dr_nm from drmst";
$cond = "where dr_del_flg = 'f' order by dr_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
while ($row = pg_fetch_array($sel)) {
	if (!isset($sects[$row["sect_id"]])) {continue;}
	$sects[$row["sect_id"]]["doctors"][$row["dr_id"]] = $row["dr_nm"];
}
unset($sel);

$bedundec1 = ($ward == 0) ? "t" : "f";
$bedundec2 = ($ptrm == 0) ? "t" : "f";
$bedundec3 = ($bed == 0) ? "t" : "f";

// チェック情報を取得
$sql = "select * from bedcheckout";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
for ($i = 1; $i <= 9; $i++) {
	$varname = "display$i";
	$$varname = pg_fetch_result($sel, 0, "display$i");
}
?>
<title>病床管理｜退院予定<? if ($subsys != "med") {echo($action);} else { ?>参照<? } ?></title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function initPage() {
	switch ('<? echo($out_inst_cd); ?>') {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.inpt.out_inst_name) {
			document.inpt.out_inst_name.value = '<? echo($tmp_insts["name"]); ?>';
		}
		document.inpt.out_sect_rireki.value = '<? echo($tmp_insts["rireki"]); ?>';
		break;
<? } ?>
	}
	setSectOptions('<? echo($out_sect_cd); ?>', '<? echo($out_doctor_no); ?>');

	switch ('<? echo($pr_inst_cd); ?>') {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		if (document.inpt.pr_inst_name) {
			document.inpt.pr_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.inpt.pr_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}
	setPrSectOptions('<? echo($pr_sect_cd); ?>', '<? echo($pr_doctor_no); ?>');

	setBackWardOptions('<? echo($ward); ?>', '<? echo($ptrm); ?>', '<? echo($bed); ?>');
	bedundec1OnClick();
	setBackSectOptions('<? echo($back_sect); ?>', '<? echo($back_doc); ?>');
}

function openInstSearchWindow(callback) {
	window.open('bed_inst_search.php?session=<? echo($session); ?>&callback=' + callback, 'instsrch', 'width=640,height=480,scrollbars=yes');
}

function clearInst() {
	document.inpt.out_inst_cd.value = '';
	if (document.inpt.out_inst_name) {
		document.inpt.out_inst_name.value = '';
	}
	document.inpt.out_sect_rireki.value = '';
	if (document.inpt.out_sect_cd) {
		clearOptions(document.inpt.out_sect_cd);
	}
	if (document.inpt.out_doctor_no) {
		clearOptions(document.inpt.out_doctor_no);
	}
	if (document.inpt.out_city) {
		document.inpt.out_city.value = '';
	}
}

function selectInst(cd) {
	switch (cd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.inpt.out_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		if (document.inpt.out_inst_name) {
			document.inpt.out_inst_name.value = '<? echo($tmp_insts["name"]); ?>';
		}
		document.inpt.out_sect_rireki.value = '<? echo($tmp_insts["rireki"]); ?>';
		if (document.inpt.out_city) {
			document.inpt.out_city.value = '<? echo($tmp_insts["city"]); ?>';
		}
		break;
<? } ?>
	}

	setSectOptions();
}

function setSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.inpt.out_sect_cd) {
		return;
	}

	clearOptions(document.inpt.out_sect_cd);

	var instCd = document.inpt.out_inst_cd.value;
	if (!instCd) {
		return;
	}

	switch (instCd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_insts["sects"] as $tmp_sect_cd => $tmp_sects) { ?>
		addOption(document.inpt.out_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sects["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setDoctorOptions(defaultDoctorNo);
}

function setDoctorOptions(defaultDoctorNo) {
	if (!document.inpt.out_doctor_no) {
		return;
	}

	clearOptions(document.inpt.out_doctor_no);

	var sectCd = document.inpt.out_sect_cd.value;
	if (!sectCd) {
		return;
	}

	var instCd = document.inpt.out_inst_cd.value;
<? foreach ($insts as $tmp_inst_cd => $tmp_insts) { ?>
	<? foreach ($tmp_insts["sects"] as $tmp_sect_cd => $tmp_sects) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sects["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.inpt.out_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearPrInst() {
	document.inpt.pr_inst_cd.value = '';
	if (document.inpt.pr_inst_name) {
		document.inpt.pr_inst_name.value = '';
	}
	document.inpt.pr_sect_rireki.value = '';
	if (document.inpt.pr_sect_cd) {
		clearOptions(document.inpt.pr_sect_cd);
	}
	if (document.inpt.pr_doctor_no) {
		clearOptions(document.inpt.pr_doctor_no);
	}
}

function selectPrInst(cd) {
	switch (cd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
		document.inpt.pr_inst_cd.value = '<? echo($tmp_inst_cd); ?>';
		if (document.inpt.pr_inst_name) {
			document.inpt.pr_inst_name.value = '<? echo($tmp_inst["name"]); ?>';
		}
		document.inpt.pr_sect_rireki.value = '<? echo($tmp_inst["rireki"]); ?>';
		break;
<? } ?>
	}

	setPrSectOptions();
}

function setPrSectOptions(defaultSectCd, defaultDoctorNo) {
	if (!document.inpt.pr_sect_cd) {
		return;
	}

	clearOptions(document.inpt.pr_sect_cd);

	var instCd = document.inpt.pr_inst_cd.value;
	if (!instCd) {
		return;
	}

	switch (instCd) {
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	case '<? echo($tmp_inst_cd); ?>':
<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
		addOption(document.inpt.pr_sect_cd, '<? echo($tmp_sect_cd); ?>', '<? echo($tmp_sect["name"]); ?>', defaultSectCd);
<? } ?>
		break;
<? } ?>
	}

	setPrDoctorOptions(defaultDoctorNo);
}

function setPrDoctorOptions(defaultDoctorNo) {
	if (!document.inpt.pr_doctor_no) {
		return;
	}

	clearOptions(document.inpt.pr_doctor_no);

	var sectCd = document.inpt.pr_sect_cd.value;
	if (!sectCd) {
		return;
	}

	var instCd = document.inpt.pr_inst_cd.value;
<? foreach ($insts as $tmp_inst_cd => $tmp_inst) { ?>
	<? foreach ($tmp_inst["sects"] as $tmp_sect_cd => $tmp_sect) { ?>
	if (instCd == '<? echo($tmp_inst_cd); ?>' && sectCd == '<? echo($tmp_sect_cd); ?>') {
		<? foreach ($tmp_sect["doctors"] as $tmp_doctor_no => $tmp_doctor_name) { ?>
			addOption(document.inpt.pr_doctor_no, '<? echo($tmp_doctor_no); ?>', '<? echo($tmp_doctor_name); ?>', defaultDoctorNo);
		<? } ?>
	}
	<? } ?>
<? } ?>
}

function clearOptions(selectbox) {
	for (var i = selectbox.length - 1; i > 0; i--) {
		selectbox.options[i] = null;
	}
}

function deleteAllOptions(box) {
	for (var i = box.length - 1; i >= 0; i--) {
		box.options[i] = null;
	}
}

function addOption(box, value, text, selected) {
	var opt = document.createElement("option");
	opt.value = value;
	opt.text = text;
	if (selected == value) {
		opt.selected = true;
	}
	box.options[box.length] = opt;
	if (!box.multiple) {
		try {box.style.fontSize = 'auto';} catch (e) {}
		box.style.overflow = 'auto';
	}
}

function setBackWardOptions(ward, ptrm, bed) {
	if (!document.inpt.ward) {
		return;
	}

	deleteAllOptions(document.inpt.ward);

<? foreach($wards as $tmp_ward_code => $tmp_ward) { ?>
	addOption(document.inpt.ward, '<? echo($tmp_ward_code); ?>', '<? echo($tmp_ward["name"]); ?>', ward);
<? } ?>

	if (document.inpt.ward.options.length == 0) {
		addOption(document.inpt.ward, '0', '（未登録）', ward);
	}

	wardOnChange(ptrm, bed);
}

function wardOnChange(ptrm, bed) {
	deleteAllOptions(document.inpt.ptrm);

	var bldgwd = document.inpt.ward.value;
<? foreach ($wards as $tmp_ward_code => $tmp_ward) { ?>
	if (bldgwd == '<? echo($tmp_ward_code); ?>') {
	<? foreach($tmp_ward["ptrms"] as $tmp_ptrm_no => $tmp_ptrm) { ?>
		addOption(document.inpt.ptrm, '<? echo($tmp_ptrm_no); ?>', '<? echo($tmp_ptrm["name"]); ?>', ptrm);
	<? } ?>
	}
<? } ?>

	if (document.inpt.ptrm.options.length == 0) {
		addOption(document.inpt.ptrm, '0', '（未登録）', ptrm);
	}

	bedundec1OnClick();
	ptrmOnChange(bed);
}

function ptrmOnChange(bed) {
	deleteAllOptions(document.inpt.bed);

	var bldgwd = document.inpt.ward.value;
	var ptrm_no = document.inpt.ptrm.value;

<? foreach ($wards as $tmp_ward_code => $tmp_ward) { ?>
	<? foreach ($tmp_ward["ptrms"] as $tmp_ptrm_no => $tmp_ptrm) { ?>
	if (bldgwd == '<? echo($tmp_ward_code); ?>' && ptrm_no == '<? echo($tmp_ptrm_no); ?>') {
		for (var i = 1; i <= <? echo($tmp_ptrm["bed_count"]); ?>; i++) {
			addOption(document.inpt.bed, i.toString(), 'ベットNo.' + i, bed);
		}
	}
	<? } ?>
<? } ?>

	if (document.inpt.bed.options.length == 0) {
		addOption(document.inpt.bed, '0', '（未登録）', bed);
	}
}

function openSearchWindow() {
<?
if ($in_yr == "") {$in_yr = date("Y");}
if ($in_mon == "") {$in_mon = date("n");}
if ($in_day == "") {$in_day = date("j");}
if ($in_hr == "") {$in_hr = date("G");}
if ($in_min == "") {$in_min = (date("i") <= "29") ? "00" : "30";}
?>
	var ward = document.inpt.ward.value;
	var in_yr = document.inpt.back_in_yr.value;
	if (in_yr == '-') {in_yr = '<? echo($in_yr); ?>';}
	var in_mon = document.inpt.back_in_mon.value;
	if (in_mon == '-') {in_mon = '<? echo($in_mon); ?>';}
	var in_day = document.inpt.back_in_day.value;
	if (in_day == '-') {in_day = '<? echo($in_day); ?>';}
	var in_hr = document.inpt.back_in_hr.value;
	if (in_hr == '--') {in_hr = '<? echo($in_hr); ?>';}
	var in_min = document.inpt.back_in_min.value;
	if (in_min == '--') {in_min = '<? echo($in_min); ?>';}
	var url = 'sub_vacant_room_list.php?session=<? echo($session); ?>&bldgwd=' + ward + '&in_yr=' + in_yr + '&in_mon=' + in_mon + '&in_day=' + in_day + '&in_hr=' + in_hr + '&in_min=' + in_min;
	window.open(url, 'vacant', 'width=840,height=640,scrollbars=yes');
}

function bedundec1OnClick() {
	if (!document.inpt.bedundec1) {
		return;
	}

	var bedDisabled = (document.inpt.bedundec1.checked);
	document.inpt.ward.disabled = bedDisabled;
	document.inpt.ptrm.disabled = bedDisabled;
	document.inpt.bed.disabled = bedDisabled;

	document.inpt.bedundec2.disabled = bedDisabled;
	document.inpt.bedundec3.disabled = bedDisabled;
	if (bedDisabled) {
		document.inpt.bedundec2.checked = true;
		document.inpt.bedundec3.checked = true;
	}

	if (document.inpt.ptrm.value == '0') {
		bedDisabled = true;
	}
	document.inpt.btnEquipDetail.disabled = bedDisabled;
	document.inpt.btnRoomDetail.disabled = bedDisabled;

	bedundec2OnClick();
}

function bedundec2OnClick() {
	var bedDisabled = (document.inpt.bedundec2.checked);
	document.inpt.ptrm.disabled = bedDisabled;
	document.inpt.bed.disabled = bedDisabled;

	document.inpt.bedundec3.disabled = bedDisabled;
	if (bedDisabled) {
		document.inpt.bedundec3.checked = true;
	}

	if (document.inpt.ptrm.value == '0') {
		bedDisabled = true;
	}
	document.inpt.btnEquipDetail.disabled = bedDisabled;
	document.inpt.btnRoomDetail.disabled = bedDisabled;

	bedundec3OnClick();
}

function bedundec3OnClick() {
	var bedDisabled = (document.inpt.bedundec3.checked);
	document.inpt.bed.disabled = bedDisabled;
}

function setBackSectOptions(sect, doc) {
	if (!document.inpt.back_sect) {
		return;
	}

	clearOptions(document.inpt.back_sect);

<? foreach($sects as $tmp_sect_id => $tmp_sect) { ?>
	addOption(document.inpt.back_sect, '<? echo($tmp_sect_id); ?>', '<? echo($tmp_sect["name"]); ?>', sect);
<? } ?>

	setBackDoctorOptions(doc);
}

function setBackDoctorOptions(doc) {
	clearOptions(document.inpt.back_doc);

	var sect = document.inpt.back_sect.value;
<? foreach($sects as $tmp_sect_id => $tmp_sect) { ?>
	if (sect == '<? echo($tmp_sect_id); ?>') {
	<? foreach($tmp_sect["doctors"] as $tmp_doctor_id => $tmp_doctor_name) { ?>
		addOption(document.inpt.back_doc, '<? echo($tmp_doctor_id); ?>', '<? echo($tmp_doctor_name); ?>', doc);
	<? } ?>
	}
<? } ?>
}

function openEquipWin() {
	var arr_bldgwd = document.inpt.ward.value.split('-');
	var url = 'room_equipment_detail.php';
	url += '?session=<? echo($session); ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&rm_no=' + document.inpt.ptrm.value;
	window.open(url, 'equipwin', 'width=640,height=480,scrollbars=yes');
}

function openRoomWin() {
	var arr_bldgwd = document.inpt.ward.value.split('-');
	var url = 'bed_info_detail_sub.php';
	url += '?session=<? echo($session); ?>';
	url += '&bldg_cd=' + arr_bldgwd[0];
	url += '&ward_cd=' + arr_bldgwd[1];
	url += '&ptrm_room_no=' + document.inpt.ptrm.value;
	window.open(url, 'rmdtl', 'width=800,height=600,scrollbars=yes');
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}
p {margin:0;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="initPage();">
<center>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#5279a5">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b>退院予定<? if ($subsys != "med") {echo($action);} else { ?>参照<? } ?></b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="5"><br>
<form name="inpt" action="outpatient_reserve_update_exe.php" method="post">
<table width="860" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者ID</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($pt_id); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">患者氏名</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo("$lt_kj_nm $ft_kj_nm"); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">性別</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($sex); ?></font></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">年齢</font></td>
<td width=""><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><? echo($age); ?>歳</font></td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="860" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="180">
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border:#5279a5 solid 1px; background-color:#f6f9ff;">
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_sub_menu.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">基礎データ</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_detail_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">入院情報</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_move_reserve_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">転棟・転科</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_go_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">外出</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_stop_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">外泊</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><b>退院予定</b></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="outpatient_out_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">退院</a></font></td>
</tr>
<tr height="60">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18"><a href="inpatient_family_register.php?session=<? echo($session); ?>&pt_id=<? echo($pt_id); ?>&subsys=<? echo($subsys); ?>">家族付き添い</a></font></td>
</tr>
</table>
</td>
<td width="5"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">退院予定日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="out_yr">
<? show_select_years_span(min($out_yr, date("Y") - 3), date("Y") + 1, $out_yr); ?>
</select>/<select name="out_mon">
<? show_select_months($out_mon); ?>
</select>/<select name="out_day">
<? show_select_days($out_day); ?>
</select>&nbsp;<select name="out_hr">
<? show_select_hrs_0_23($out_hr); ?>
</select>：<select name="out_min">
<option value="00"<? if ($out_min == "00") {echo " selected";} ?>>00</option>
<option value="30"<? if ($out_min == "30") {echo " selected";} ?>>30</option>
</select>
</font></td>
</tr>
<? if ($display1 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">転帰</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="result">
<option value="">
<option value="2"<? if ($result == "2") {echo " selected";} ?>>完治
<option value="3"<? if ($result == "3") {echo " selected";} ?>>不変
<option value="4"<? if ($result == "4") {echo " selected";} ?>>軽快
<option value="5"<? if ($result == "5") {echo " selected";} ?>>後遺症残
<option value="6"<? if ($result == "6") {echo " selected";} ?>>不明
<option value="7"<? if ($result == "7") {echo " selected";} ?>>悪化
<option value="1"<? if ($result == "1") {echo " selected";} ?>>死亡
<option value="8"<? if ($result == "8") {echo " selected";} ?>>その他
</select>
&nbsp;詳細：<input type="text" name="result_dtl" value="<? echo $result_dtl; ?>" size="30" style="ime-mode:active;">
</font></td>
</tr>
<? } ?>
<? if ($display2 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">退院理由</font></td>
<td colspan="3">
<select name="out_rsn_cd">
<option value="">
<? foreach ($out_rsn_master as $tmp_out_rsn_cd => $tmp_out_rsn_name) { ?>
<option value="<? echo($tmp_out_rsn_cd); ?>"<? if ($tmp_out_rsn_cd == $out_rsn_cd) {echo " selected";} ?>><? echo($tmp_out_rsn_name); ?>
<? } ?>
</select>
</td>
</tr>
<? } ?>
<? if ($display3 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">退院先区分</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="out_pos_cd">
<option value="">
<? foreach ($out_pos_master as $tmp_out_pos_cd => $tmp_out_pos_name) { ?>
<option value="<? echo($tmp_out_pos_cd); ?>"<? if ($tmp_out_pos_cd == $out_pos_cd) {echo " selected";} ?>><? echo($tmp_out_pos_name); ?>
<? } ?>
</select>
&nbsp;詳細：<input type="text" name="out_pos_dtl" value="<? echo $out_pos_dtl; ?>" size="40" style="ime-mode:active;">
</font></td>
</tr>
<? } ?>
<? if ($display4 == "t" || $display5 == "t") { ?>
<tr>
<? if ($display4 == "t") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">退院先施設名</font></td>
<td<? if ($display5 != "t") {echo(' colspan="3"');} ?>><input type="text" name="out_inst_name" value="<? echo($out_inst_name); ?>" size="25" disabled> <input type="button" value="検索" onclick="openInstSearchWindow('selectInst');"> <input type="button" value="クリア" onclick="clearInst();"></td>
<? } ?>
<? if ($display5 == "t") { ?>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">診療科</font></td>
<td<? if ($display4 != "t") {echo(' colspan="3"');} ?>><select name="out_sect_cd" onchange="setDoctorOptions();">
<option value="">　　　　　</option>
</select></td>
<? } ?>
</tr>
<? } ?>
<? if ($display6 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">担当医</font></td>
<td colspan="3"><select name="out_doctor_no">
<option value="">　　　　　</option>
</select></td>
</tr>
<? } ?>
<? if ($display7 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">退院先市区町村</font></td>
<td colspan="3"><input type="text" name="out_city" value="<? echo($out_city); ?>" size="60" maxlength="100" style="ime-mode:active;"></td>
</tr>
<? } ?>
<? if ($display9 == "t") { ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">かかりつけ医</font></td>
<td colspan="3">

<table width="100%" cellspacing="0" cellpadding="2" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">医療機関名</font></td>
<td><input type="text" name="pr_inst_name" value="<? echo($pr_inst_name); ?>" size="25" disabled> <input type="button" value="検索" onclick="openInstSearchWindow('selectPrInst');"> <input type="button" value="クリア" onclick="clearPrInst();"></td>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">診療科</font></td>
<td><select name="pr_sect_cd" onchange="setPrDoctorOptions();">
<option value="">　　　　　</option>
</select></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">担当医</font></td>
<td colspan="3"><select name="pr_doctor_no">
<option value="">　　　　　</option>
</select></td>
</tr>
</table>

</td>
</tr>
<? } ?>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">食事終了帯</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<input type="radio" name="fd_end" value="1"<? if ($fd_end == "1") {echo " checked";} ?>>朝
<input type="radio" name="fd_end" value="2"<? if ($fd_end == "2") {echo " checked";} ?>>昼
<input type="radio" name="fd_end" value="3"<? if ($fd_end == "3") {echo " checked";} ?>>夕
</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">コメント</font></td>
<td colspan="3"><textarea name="comment" cols="50" rows="4" wrap="virtual" style="ime-mode:active;"><? echo($comment); ?></textarea></td>
</tr>
</table>
<? if ($display8 == "t") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">＜戻り入院の予定＞</font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="list">
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病棟</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="ward" onchange="wardOnChange();">
</select>
<input value="空床検索" type="button" onclick="openSearchWindow();" style="margin-right:2px;">
病棟未定：<input type="checkbox" name="bedundec1" value="t" <? if ($bedundec1 == "t") {echo("checked");} ?> onclick="bedundec1OnClick();">
</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">病室</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="ptrm" onchange="ptrmOnChange();">
</select>
<input type="button" name="btnEquipDetail" value="設備詳細" onclick="openEquipWin();">
<input type="button" name="btnRoomDetail" value="病室詳細" onclick="openRoomWin();" style="margin-right:2px;">
病室未定：<input type="checkbox" name="bedundec2" value="t" <? if ($bedundec2 == "t") {echo("checked");} ?> onclick="bedundec2OnClick();">
</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">ベット番号</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="bed">
</select>
ベット未定：<input type="checkbox" name="bedundec3" value="t" <? if ($bedundec3 == "t") {echo("checked");} ?> onclick="bedundec3OnClick();">
</font></td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">入院日時</font></td>
<td colspan="3"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">
<select name="back_in_yr">
<option value="-">
<? show_select_years_span(date("Y"), date("Y") + 1, $back_in_yr); ?>
</select>/<select name="back_in_mon">
<? show_select_months($back_in_mon, true); ?>
</select>/<select name="back_in_day">
<? show_select_days($back_in_day, true); ?>
</select>&nbsp;<select name="back_in_hr">
<? show_hour_options_0_23($back_in_hr, true); ?>
</select>：<select name="back_in_min">
<option value="--">
<option value="00"<? if ($back_in_min == "00") {echo " selected";} ?>>00
<option value="30"<? if ($back_in_min == "30") {echo " selected";} ?>>30
</select>
</font></td>
</tr>
<tr>
<td width="18%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">診療科</font></td>
<td width="34%">
<select name="back_sect" onChange="setBackDoctorOptions();">
<option value="">　　　　　</option>
</select>
</td>
<td width="14%" align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">主治医</font></td>
<td width="34%">
<select name="back_doc">
<option value="">　　　　　</option>
</select>
</td>
</tr>
<tr>
<td align="right" bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j18">転院目的</font></td>
<td colspan="3"><textarea name="back_change_purpose" cols="50" rows="4" wrap="virtual" style="ime-mode:active;"><? echo($back_change_purpose); ?></textarea></td>
</tr>
</table>
<? } ?>
<? if ($subsys != "med") { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td align="right">
<input type="button" value="<? echo($action); ?>" onclick="this.form.is_delete.value = 'f'; this.form.submit();">
<input type="button" value="取消" onclick="if (confirm('取消してもよろしいですか？')) {this.form.is_delete.value = 't'; this.form.submit();}">
</td>
</tr>
</table>
<? } ?>
</td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pt_id" value="<? echo($pt_id); ?>">
<input type="hidden" name="out_rsn_rireki" value="<? echo($out_rsn_rireki); ?>">
<input type="hidden" name="out_pos_rireki" value="<? echo($out_pos_rireki); ?>">
<input type="hidden" name="out_inst_cd" value="<? echo($out_inst_cd); ?>">
<input type="hidden" name="out_sect_rireki" value="<? echo($out_sect_rireki); ?>">
<input type="hidden" name="pr_inst_cd" value="<? echo($pr_inst_cd); ?>">
<input type="hidden" name="pr_sect_rireki" value="<? echo($pr_sect_rireki); ?>">
<input type="hidden" name="is_delete" value="">
<input type="hidden" name="back_url" value="outpatient_out_reserve_register.php">
<input type="hidden" name="action_label" value="<? echo($action); ?>">
</form>
</center>
</body>
<? pg_close($con); ?>
</html>
<?
function get_sex_string($sex) {
	switch ($sex) {
	case "1":
		return "男";
	case "2":
		return "女";
	default:
		return "不明";
	}
}

function get_age($birth) {
	$yr = date("Y");
	$md = date("md");
	$birth_yr = substr($birth, 0, 4);
	$birth_md = substr($birth, 4, 8);
	$age = $yr - $birth_yr;
	if ($md < $birth_md) {
		$age = $age - 1;
	}
	return $age;
}
?>
