<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>委員会・WG | タスク一覧</title>
<?
require("about_authority.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// 委員会・WG権限のチェック
$checkauth = check_authority($session, 31, $fname);
if ($checkauth == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

// デフォルト日付をシステム日付とする
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// 表示範囲のデフォルトは1ヶ月
if ($scale == "") {
	$scale = "1";
}

// データベースに接続
$con = connect2db($fname);

// 委員会・WG名を取得
$sql = "select project.pjt_name, parent.pjt_id as parent_id, parent.pjt_name as parent_name from project left join project parent on project.pjt_parent_id = parent.pjt_id";
$cond = "where project.pjt_id = $pjt_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == "0") {
	pg_close($con);
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showErrorPage(window);</script>");
	exit;
}
$project_name = pg_result($sel, 0, "pjt_name");
$parent_id = pg_result($sel, 0, "parent_id");
$parent_name = pg_result($sel, 0, "parent_name");

// 委員会・WK管理権限を取得
$pjt_admin_auth = check_authority($session, 82, $fname);

?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function deleteTask() {
	if (confirm('削除してよろしいですか？')) {
		document.task.submit();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" valign="middle" colspan="2" class="spacing"><a href="project_menu.php?session=<? echo($session); ?>"><img src="img/icon/b03.gif" width="32" height="32" border="0" alt="委員会・WG"></a></td>

<? if ($pjt_admin_auth == "1") { ?>

	<? if($adm_flg=="t") {?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a><? if ($parent_id != "") { ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($parent_id); ?>"><b><? echo($parent_name); ?></b></a><?} ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>"><b><? echo($project_name); ?></b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="project_menu.php?session=<? echo($session); ?>&entity=1"><b>ユーザ画面へ</b></a></font></td>
	<? }else{ ?>
		<td width="85%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a><? if ($parent_id != "") { ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($parent_id); ?>"><b><? echo($parent_name); ?></b></a><?} ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>"><b><? echo($project_name); ?></b></a></font></td>
		<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">　<a href="project_menu_adm.php?session=<? echo($session); ?>&entity=1"><b>管理画面へ</b></a></font></td>
	<? } ?>


<? }else{ ?>
	<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="project_menu.php?session=<? echo($session); ?>"><b>委員会・WG</b></a><? if ($parent_id != "") { ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($parent_id); ?>"><b><? echo($parent_name); ?></b></a><?} ?>&nbsp;&gt;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>"><b><? echo($project_name); ?></b></a></font></td>
<? } ?>


</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<? if($adm_flg=="t") {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu_adm.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } else {?>
	<td width="120" height="22" align="center" bgcolor="#bdd1e7"><a href="project_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">委員会・WG一覧</font></a></td>
<? } ?>

<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#5279a5"><a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>タスク一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="120" align="center" bgcolor="#bdd1e7"><a href="project_task_register.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&adm_flg=<?=$adm_flg?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">新規登録</font></a></td>
<td align="right">
<? if ($mode == "full") { ?>
<input type="button" value="戻る" onclick="location.href = 'project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=<? echo($scale); ?>&adm_flg=<?=$adm_flg?>';" style="width:12em;">
<? } else { ?>
<input type="button" value="全てのタスクの表示" onclick="location.href = 'project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=<? echo($scale); ?>&mode=full&adm_flg=<?=$adm_flg?>';" style="width:12em;">
<? } ?>
<input type="button" value="削除" onclick="deleteTask();">
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<? show_task_list_navigation($session, $pjt_id, $date, $scale, $mode,$adm_flg); ?>
<form name="task" action="project_task_delete.php" method="post">
<? show_task_list($con, $pjt_id, $date, $scale, $mode, $session, $fname,$adm_flg); ?>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="pjt_id" value="<? echo($pjt_id); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
<input type="hidden" name="scale" value="<? echo($scale); ?>">
<input type="hidden" name="mode" value="<? echo($mode); ?>">
<input type="hidden" name="adm_flg" value="<? echo($adm_flg); ?>">
</form>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
// タスク一覧のナビゲーション部を表示
function show_task_list_navigation($session, $pjt_id, $date, $scale, $mode,$adm_flg) {

	// 前月・前週・前日・翌日・翌週・翌月のタイムスタンプを求める
	$last_month = strtotime("-1 month", $date);
	$last_week = strtotime("-1 week", $date);
	$last_day = strtotime("-1 day", $date);
	$next_day = strtotime("+1 day", $date);
	$next_week = strtotime("+1 week", $date);
	$next_month = strtotime("+1 month", $date);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="22"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
	<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_month); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">&lt;&lt;&lt;前月</a>&nbsp;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_week); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">&lt;&lt;前週</a>&nbsp;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($last_day); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">&lt;前日</a>&nbsp;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_day); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">翌日&gt;</a>&nbsp;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_week); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">翌週&gt;&gt;</a>&nbsp;&nbsp;<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($next_month); ?>&scale=<? echo($scale); ?>&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">翌月&gt;&gt;&gt;</a></font></td>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<?
	if ($scale != "1") {
?>
		<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=1&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">1ヶ月</a>
<?
	} else {
?>
1ヶ月
<?
	}

	if ($scale != "2") {
?>
		<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=2&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">3ヶ月</a>
<?
	} else {
?>
3ヶ月
<?
	}

	if ($scale != "3") {
?>
		<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=3&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">6ヶ月</a>
<?
	} else {
?>
6ヶ月
<?
	}

	if ($scale != "4") {
?>
		<a href="project_task_menu.php?session=<? echo($session); ?>&pjt_id=<? echo($pjt_id); ?>&date=<? echo($date); ?>&scale=4&mode=<? echo($mode); ?>&adm_flg=<?=$adm_flg?>">12ヶ月</a>
<?
	} else {
?>
12ヶ月
<?
	}
?>
</font></td>
<td width="40" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">高&nbsp;</font></td>
<td width="20" bgcolor="#ccff33">&nbsp;</td>
<td width="20" bgcolor="#ccff66">&nbsp;</td>
<td width="20" bgcolor="#ccff99">&nbsp;</td>
<td width="20" bgcolor="#ccffcc">&nbsp;</td>
<td width="20" bgcolor="#ccffff">&nbsp;</td>
<td width="40"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">&nbsp;低</font></td>
<td width="100" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">遅延作業&nbsp;</font>
</td>
<td width="20" bgcolor="#ff0066">&nbsp;</td>
</tr>
</table>
<?
}

// タスク一覧を表示
function show_task_list($con, $pjt_id, $date, $scale, $mode, $session, $fname,$adm_flg) {

	// 処理対象日付を配列に格納
	switch ($scale) {
	case "1":
		$step = 1;
		break;
	case "2":
		$step = 3;
		break;
	case "3":
		$step = 6;
		break;
	case "4":
		$step = 12;
		break;
	}
	$dates = array();
	for ($i = 0; $i < $step * 30; $i += $step) {
		$dates[$i] = date("Ymd", strtotime("+$i day", $date));
	}
	$today = date("Ymd");
	$start_date = $dates[0];
	$end_date   = $dates[$step * 29];

	// ヘッダ行を出力
	echo("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n");
	echo("<tr bgcolor=\"#f6f9ff\">\n");
	echo("<td width=\"30\" height=\"22\">&nbsp;</td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">作業名</font></td>\n");
	echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">担当者</font></td>\n");
	echo("<td width=\"50\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">進捗度</font></td>\n");
	foreach ($dates as $tmp_date) {
		$month = intval(substr($tmp_date, 4, 2));
		$day = intval(substr($tmp_date, 6, 2));

		if ($month != $pre_month) {
			$date_str = "$month/$day";
			$pre_month = $month;
		} else {
			$date_str = $day;
		}

		if ($tmp_date == $today) {
			$date_str = "<font color=\"red\"><b>$date_str</b></font>";
		}

		echo("<td width=\"17\" align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j10\">$date_str</font></td>\n");
	}
	echo("</tr>\n");

	// タスク情報を取得
	$sql = "select work_id, work_name, work_status, work_priority, to_char(work_start_date, 'YYYYMMDD') as work_start_date, to_char(work_expire_date, 'YYYYMMDD') as work_expire_date from prowork";
	$cond = "where pjt_id = '$pjt_id'";
	if ($mode != "full") {
		$cond .= " and ((work_start_date between '$start_date' and '$end_date') or (work_expire_date between '$start_date' and '$end_date') or (work_update between '$start_date' and '$end_date'))";
	}
	$cond .= " order by work_priority desc, work_expire_date";
	$sel = select_from_table($con, $sql, $cond, $fname);
	if ($sel == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>\n");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>\n");
		exit;
	}

	// タスク一覧を表示
	while ($row = pg_fetch_array($sel)) {
		$tmp_work_id = $row["work_id"];
		$tmp_work_name = $row["work_name"];
		$tmp_status = $row["work_status"];
		$tmp_priority = $row["work_priority"];
		$tmp_start_date = $row["work_start_date"];
		$tmp_expire_date = $row["work_expire_date"];

		// 担当者情報を取得
		$sql2 = "select emp_lt_nm, emp_ft_nm from empmst";
		$cond2 = "where emp_id in (select emp_id from proworkmem where work_id = $tmp_work_id) order by emp_id";
		$sel2 = select_from_table($con, $sql2, $cond2, $fname);
		if ($sel2 == 0) {
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>\n");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>\n");
			exit;
		}
		$tmp_charge_name = pg_fetch_result($sel2, 0, "emp_lt_nm") . " " . pg_fetch_result($sel2, 0, "emp_ft_nm");
		if (pg_num_rows($sel2) > 1) {
			$tmp_charge_name .= " 他";
		}

		switch ($tmp_status) {
		case "0":
			$tmp_status = "未着手";
			break;
		case "1":
			$tmp_status = "完了";
			break;
		case "2":
			$tmp_status = "着手";
			break;
		case "3":
			$tmp_status = "25%";
			break;
		case "4":
			$tmp_status = "50%";
			break;
		case "5":
			$tmp_status = "75%";
			break;
		}

		switch ($tmp_priority) {
		case "1":
			$tmp_bgcolor = "#ccffff";
			break;
		case "2":
			$tmp_bgcolor = "#ccffcc";
			break;
		case "3":
			$tmp_bgcolor = "#ccff99";
			break;
		case "4":
			$tmp_bgcolor = "#ccff66";
			break;
		case "5":
			$tmp_bgcolor = "#ccff33";
			break;
		}

		echo("<tr height=\"22\">\n");
		echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"$tmp_work_id\"></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"project_task_update.php?session=$session&pjt_id=$pjt_id&date=$date&scale=$scale&mode=$mode&work_id=$tmp_work_id&adm_flg=$adm_flg\">$tmp_work_name</a></font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_charge_name</font></td>\n");
		echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$tmp_status</font></td>\n");

		foreach ($dates as $tmp_date) {
			if ($tmp_date < $tmp_start_date) {
				echo("<td>&nbsp;</td>\n");
			} else if ($tmp_date <= $tmp_expire_date) {
				echo("<td bgcolor=\"$tmp_bgcolor\">&nbsp;</td>\n");
			} else if ($tmp_status != "完了" && $today > $tmp_expire_date && $tmp_date <= $today) {
				echo("<td bgcolor=\"ff0066\">&nbsp;</td>\n");
			} else {
				echo("<td>&nbsp;</td>\n");
			}
		}

		echo("</tr>\n");
	}
}
?>
