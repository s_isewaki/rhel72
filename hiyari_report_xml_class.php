<?php
class hiyari_report_xml_class {

    // XMLヘッダ
    var $_header;

    // XMLフッタ
    var $_footer;

    // バッファ
    var $_buffer;

    // ヒヤリか事故かの判別
    var $_mode = 'hiyari';

    function hiyari_report_xml_class() {
        $header  = "<?xml version=\"1.0\" encoding=\"Shift_JIS\"?>\n";
        $header .= "<!DOCTYPE CASEREPORT SYSTEM 'casereport.dtd' > \n";

        $this->_header = $header;
    }


    /*****************************************************************
     * XMLのヘッダを返します
     *
     * @return  STRING  ヘッダ情報
     *****************************************************************/
    function xml_header() {
        $version = '1.0';

        $this->_header .= "<CASEREPORT>\n";
        $this->_header .= "  <VERSION>" . h($version) . "</VERSION>\n";

        return $this->_header;
        // $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 事例報告用のバージョンを返します
     *
     * @param   STRING  $version    事例報告のバージョン
     * @return  STRING  事例報告のバージョン情報
     *****************************************************************/
    function xml_version($version) {
        $this->_header .= "  <VERSION>" . h($version) . "</VERSION>\n";

        // return $this->header;
        // $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 事例の報告内容を返します
     *
     * @param   NUMERIC $division   1.医療事故 2.ヒヤリハット
     * @param   NUMERIC $sql        一意となる番号(シーケンス？)
     * @return  STRING  レポート情報
     *****************************************************************/
    function xml_case_report($division, $seq) {
        $buffer = "  <GRPCASEREPORT DIVISION=\"" . h($division) . "\" SEQ=\"" . h($seq) . "\">\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 発生日付を返します
     *
     * @param   STRING  $date   YYYY/MM/DD
     * @return  STRING  日付情報
     *****************************************************************/
    function xml_date($date) {
        list($year, $month, $day) = explode("[-/]", $date);
        $month = sprintf("%02d", $month);
        $day_of_the_week = sprintf("%02d", date("w", mktime(0, 0, 0, $month, $day, $year)) + 1);

        $buffer  = "    <DATYEAR>" . h($year) . "</DATYEAR>\n";
        $buffer .= "    <DATMONTH CODE=\"" . h($month) . "\"></DATMONTH>\n";
        $buffer .= "    <DATDAY CODE=\"" . h($day_of_the_week) . "\"></DATDAY>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 平日or休日を返します
     *
     * @param   STRING  $holiday    01:平日 02:休日,祝日
     * @return  STRING  平日or休日情報
     *****************************************************************/
    function xml_holiday($holiday) {
        ///////////////////////////////////////////////////////// カレンダーから取得]
        if($this->get_hiyari_mode() == 'jiko') {
            $holiday = sprintf("%02d", $holiday);

            $buffer = "    <DATHOLIDAY CODE=\"" . h($holiday) . "\"></DATHOLIDAY>\n";

            $this->set_buffer($buffer);
        }
    }

    /*****************************************************************
     * 発生時間帯を返します
     *
     * @param   STRING  $timezone   時間帯
     * @param   STRING  $reason     不明の理由
     * @return  STRING  平日or休日情報
     *****************************************************************/
    function xml_timezone($timezone, $text) {
        $timezone = sprintf("%02d", $timezone);

        // 不明(90)でない時は理由を排除する
        if($timezone != '90') unset($text);

        $buffer = "    <DATTIMEZONE CODE=\"" . h($timezone) . "\">" . h($text) . "</DATTIMEZONE>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 医療の実施の有無を返します
     *
     * @param   STRING  $execution  実施の有無
     * @return  STRING  実施の有無情報
     *****************************************************************/
    function xml_execution($execution) {
        if($execution == '実施あり') {
            $code = '01';
        } else if($execution == '実施なし') {
            $code = '02';
        }

        $buffer = "    <DATEXECUTION CODE=\"" . h($code) . "\"></DATEXECUTION>\n";

        $this->set_buffer($buffer);
    }

    function xml_sequelae($sequelae, $text) {
        if($this->get_hiyari_mode() == 'jiko') {
            $text = trim($text);

            switch($sequelae) {
            case '死亡' :
                $code = '01';
                break;
            case '障害残存の可能性がある(高い)' :
                $code = '02';
                break;
            case '障害残存の可能性がある(低い)' :
                $code = '03';
                break;
            case '障害残存の可能性なし' :
                $code = '04';
                break;
            case '障害なし' :
                $code = '05';
                break;
            case '不明' :
                $code = '90';
                break;
            }

            if($code != 90) unset($text);

            $buffer = "    <DATSEQUELAE CODE=\"" . h($code) . "\">" . h($text) . "</DATSEQUELAE>\n";

            $this->set_buffer($buffer);
        }
    }


    /*****************************************************************
     * 事故の程度を返します
     *
     * @param   STRING  $level  事故の程度
     * @param   STRING  $reason 事故の程度（1000バイト以内）
     * @return  STRING  実施の有無情報
     *****************************************************************/
    function xml_level($level, $execution, $reason) {
        if($execution == '実施あり') {
            switch($level) {
            case '濃厚な治療' :
                $code = '01';
                break;
            case '軽微な治療' :
                $code = '02';
                break;
            case 'なし' :
                $code = '03';
                break;
            case '不明' :
                $code = '90';
                break;
            }

            $buffer = "    <DATLEVEL CODE=\"" . h($code) . "\">" . h($reason) . "</DATLEVEL>\n";

            $this->set_buffer($buffer);
        }
    }

    /*****************************************************************
     * 影響度を返します
     *
     * @param   STRING  $influence  影響度
     * @return  STRING  影響度情報
     *****************************************************************/
    function xml_influence($influence) {
        if($this->get_hiyari_mode() == 'hiyari') {
            switch($influence) {
            case '死亡もしくは重篤な状況に至ったと考えられる' :
                $code = '01';
                break;
            case '濃厚な処置・治療が必要であると考えられる' :
                $code = '02';
                break;
            case '軽微な処置・治療が必要もしくは処置・治療が不要と考えられる' :
                $code = '03';
                break;
            }

            $buffer = "    <DATINFLUENCE CODE=\"" . h($code) . "\"></DATINFLUENCE>\n";

            $this->set_buffer($buffer);
        }
    }


    function xml_site($site, $text) {
        // 15〜16ページ参照 //////////////////////////////////////////////////////////////
        $text = trim($text);
        $site = sprintf("%02d", $site);

        if($site != 99) unset($text);

        $buffer  = "    <LSTSITE>\n";
        $buffer .= "      <DATSITE CODE=\"" . h($site) . "\">" . h($text) . "</DATSITE>\n";
        $buffer .= "    </LSTSITE>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 概要を返します
     *
     * @param   INTEGER $summary    概要
     * @param   STRING  $reason     その他の概要（1000バイト以内）
     * @return  STRING  概要情報
     *****************************************************************/
    function xml_summary($code, $text) {
        $text = trim($text);

        if($code != 99) unset($text);

        $buffer = "    <DATSUMMARY CODE=\"" . h($code) . "\">" . h($text) . "</DATSUMMARY>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 特に報告を求める事例を返します
     *
     * @param   INTEGER $especially 特に報告を求める事例
     * @param   STRING  $reason     その他の内容（1000バイト以内）
     * @return  STRING  特に報告を求める事例情報
     *****************************************************************/
    function xml_specially($specially, $text) {
        switch($specially) {
        case '汚染された薬剤・材料・生体由来材料等の使用による事故' :
            $code = '090105';
            break;
        case '院内感染による死亡や障害' :
            $code = '090107';
            break;
        case '患者の自殺又は自殺企図' :
            $code = '090201';
            break;
        case '入院患者の失踪' :
            $code = '090202';
            break;
        case '入院中の熱傷' :
            $code = '090204';
            break;
        case '入院中の感電' :
            $code = '090205';
            break;
        case '医療施設内の火災による患者の死亡や障害' :
            $code = '090206';
            break;
        case '間違った保護者の許への新生児の引渡し' :
            $code = '090207';
            break;
        case '本事例は選択肢には該当しない（その他）' :
            $code = '999999';
            break;
        }

        if($code != '999999') unset($text);

        $buffer = "    <DATSPECIALLY CODE=\"" . h($code) . "\">" . h($text) . "</DATSPECIALLY>\n";

        $this->set_buffer($buffer);
    }


    function xml_department($arr_department, $text) {
        // 18〜20ページ参照 //////////////////////////////////////////////////////////////
        if($this->get_hiyari_mode() == 'jiko') {
            if(!is_array($arr_department)) return false;

            $text = trim($text);

            $buffer  = "    <LSTDEPARTMENT>\n";

            foreach($arr_department as $value) {
                $code = sprintf("%02d", trim($value['def_code']));
                if($code == 99) {
                    $buffer .= "      <DATDEPARTMENT CODE=\"" . h($code) . "\">" . h($text) . "</DATDEPARTMENT>\n";
                } else {
                    $buffer .= "      <DATDEPARTMENT CODE=\"" . h($code) . "\"></DATDEPARTMENT>\n";
                }
            }

            $buffer .= "    </LSTDEPARTMENT>\n";

            $this->set_buffer($buffer);
        }
    }

    /*****************************************************************
     * 個人情報を返します
     *
     * @param   INTEGER $num        患者の数
     * @param   INTEGER $year       患者の年齢(年)
     * @param   INTEGER $month      患者の年齢(月)
     * @param   INTEGER $sex        患者の性別
     * @param   INTEGER $div        患者区分
     * @return  STRING
     *****************************************************************/
    function xml_patient($num, $text, $year, $month, $sex, $div) {
        if($sex == '90') return false;

        $buffer  = "    <GRPPATIENT>\n";
        $buffer .= "      <DATPATIENT CODE=\"" . h($num) . "\">" . h($text) . "</DATPATIENT>\n";

        //if($num == 1) {
            $year  = ltrim($year, '0');
            $month = ltrim($month, '0');

            $buffer .= "      <LSTPATIENTAGE>\n";
            $buffer .= "        <DATPATIENTAGE KBN=\"years\">" . h($year) . "</DATPATIENTAGE>\n";
            $buffer .= "        <DATPATIENTAGE KBN=\"months\">" . h($month) . "</DATPATIENTAGE>\n";
            $buffer .= "      </LSTPATIENTAGE>\n";

            $sex = sprintf("%02d", $sex); // 不明:90は認められない
            $buffer .= "      <DATPATIENTSEX CODE=\"" . h($sex) . "\"></DATPATIENTSEX>\n";
        //}

        // 入院なら01、外来なら04
        $div = sprintf("%02d", $div);
        $buffer .= "      <DATPATIENTDIV CODE=\"" . h($div) . "\"></DATPATIENTDIV>\n";
        $buffer .= "    </GRPPATIENT>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 疾患名を返します
     *
     * @param   STRING  $disease0       患者の数
     * @param   STRING  $disease1       患者の数
     * @param   STRING  $disease2       患者の数
     * @param   STRING  $disease3       患者の数
     * @return  STRING
     *****************************************************************/
    function xml_disease($disease0, $disease1="", $disease2="", $disease3="") {
        $buffer  = "    <LSTDISEASE>\n";
        $buffer .= "      <DATDISEASE KBN=\"disease0\">" . h($disease0) . "</DATDISEASE>\n";

        if(!empty($disease1)) {
            $buffer .= "      <DATDISEASE KBN=\"disease1\">" . h($disease1) . "</DATDISEASE>\n";
        }
        if(!empty($disease2)) {
            $buffer .= "      <DATDISEASE KBN=\"disease2\">" . h($disease2) . "</DATDISEASE>\n";
        }
        if(!empty($disease3)) {
            $buffer .= "      <DATDISEASE KBN=\"disease3\">" . h($disease3) . "</DATDISEASE>\n";
        }

        $buffer .= "    </LSTDISEASE>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 直前の患者の状態を返します
     *
     * @param   MIXED   $arr_code       患者の状態コード
     * @param   STRING  $reason         その他の記述
     * @return  STRING
     *****************************************************************/
    function xml_patient_state($arr_code, $text) {
        $buffer  = "    <LSTPATIENTSTATE>\n";

        foreach($arr_code as $value) {
            $code = sprintf("%02d", $value['def_code']);
            if($code == "99") {
                $buffer .= "      <DATPATIENTSTATE CODE=\"" . h($code) . "\">" . h($text) . "</DATPATIENTSTATE>\n";
            } else {
                $buffer .= "      <DATPATIENTSTATE CODE=\"" . h($code) . "\"></DATPATIENTSTATE>\n";
            }
        }

        $buffer .= "    </LSTPATIENTSTATE>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 発見者を返します
     *
     * @param   MIXED   $code           発見者コード
     * @param   STRING  $reason         その他の記述
     * @return  STRING
     *****************************************************************/
    function xml_discoverer($code, $text) {
        if($code == '90') return false;  // 不明の場合は許可されない

        $code = sprintf("%02d", $code);
        if($code != '99') {
            unset($text);
        } else {
            $text = $this->get_substr($text);
        }

        $buffer  = "    <DATDISCOVERER CODE=\"" . h($code) . "\">" . h($text) . "</DATDISCOVERER>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 当事者を返します
     *
     * @param   INTEGER $code           人数コード
     * @param   MIXED   $arr_code       当事者情報
     * @param   STRING  $text           人数が11人以上の時のコメント
     * @return  STRING
     *****************************************************************/
    function xml_concerned($code, $arr_data, $text="") {
        if(!is_array($arr_data)) return false;

        $code = sprintf("%02d", $code);
        $buffer  = "    <LSTCONCERNED CODE=\"" . h($code) . "\">\n";

        foreach($arr_data as $key => $value) {
            $arr_data[$key]['e_year']  = ltrim($value['e_year'], '0');
            $arr_data[$key]['e_month']  = ltrim($value['e_month'], '0');
            $arr_data[$key]['l_year']  = ltrim($value['l_year'], '0');
            $arr_data[$key]['l_month']  = ltrim($value['l_month'], '0');
        }

        foreach($arr_data as $value) {
            $buffer .= "      <GRPCONCERNED>\n";
            if(trim($value['concerned']) != 99) unset($value['concerned_text']);
            $buffer .= "        <DATCONCERNED CODE=\"".h(trim($value['concerned']))."\">" . h(trim($value['concerned_text'])) . "</DATCONCERNED>\n";
            if($this->get_hiyari_mode() != 'hiyari') {
                $buffer .= "        <DATQUALIFICATION>" . h($value['qualification']) . "</DATQUALIFICATION>\n";
            }
            $buffer .= "        <LSTEXPERIENCE>\n";
            $buffer .= "          <DATEXPERIENCE KBN=\"years\">" . h($value['e_year']) . "</DATEXPERIENCE>\n";

            if($this->get_hiyari_mode() == 'jiko') {
                $buffer .= "          <DATEXPERIENCE KBN=\"months\">" . h($value['e_month']) . "</DATEXPERIENCE>\n";
            }

            $buffer .= "        </LSTEXPERIENCE>\n";
            $buffer .= "        <LSTLENGTH>\n";
            $buffer .= "          <DATLENGTH KBN=\"years\">" . h($value['l_year']) . "</DATLENGTH>\n";
            if($this->get_hiyari_mode() == 'jiko') {
                $buffer .= "          <DATLENGTH KBN=\"months\">" . h($value['l_month']) . "</DATLENGTH>\n";
            }
            $buffer .= "        </LSTLENGTH>\n";

            // ここから下は事故のとき必須、ヒヤリは対象外
            if($this->get_hiyari_mode() == 'jiko') {
                if(trim($value['duty']) != 90) unset($value['duty_text']);
                $buffer .= "        <DATDUTY CODE=\"" . h(trim($value['duty'])) . "\">" . h(trim($value['duty_text'])) . "</DATDUTY>\n";
                if(trim($value['shift']) != 99) unset($value['shift_text']);
                $buffer .= "        <DATSHIFT CODE=\"" . h($value['shift']) . "\">" . h(trim($value['shift_text'])) . "</DATSHIFT>\n";
                $buffer .= "        <DATHOURS>" . h($value['hours']) . "</DATHOURS>\n";
            }

            $buffer .= "      </GRPCONCERNED>\n";
        }

        if($code == '90') { // 実際には11人以上になることは無い
            $text = trim($text);
            if(strlen($text) > 1000) $text = substr($text, 1000);
            $buffer .= "      <DATTEXT>" . h($text) . "</DATTEXT>";
        }

        $buffer .= "    </LSTCONCERNED>\n";

        $this->set_buffer($buffer);
    }

    /*****************************************************************
     * 当事者以外の関連職種を返します
     *
     * @param   MIXED   $arr_code       当事者以外の関連職種情報
     * @return  STRING
     *****************************************************************/
    function xml_relcategory($arr_data, $text) {
        if(!is_array($arr_data)) return false;

        $buffer = "    <LSTRELCATEGORY>\n";

        foreach($arr_data as $value) {
            $code = sprintf("%02d", $value['def_code']);
            if($code != 99) unset($text);
            $buffer .= "      <DATRELCATEGORY CODE=\"" . h($code) . "\">" . h($text) . "</DATRELCATEGORY>\n";
        }

        $buffer .= "    </LSTRELCATEGORY>\n";

        $this->set_buffer($buffer);
    }

    function xml_medicine($type, $scene, $detail, $type_text, $scene_text, $detail_text, $medical) {
        $buffer  = "    <GRPMEDICINE>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPMEDICALSUPPLIES>\n";

        $buffer .= "        <DATNAME>" . h($medical['name']) . "</DATNAME>\n";
        $buffer .= "        <DATCODE>" . h($medical['code']) . "</DATCODE>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($medical['manufacture']) . "</DATMANUFACTURE>\n";

        $buffer .= "      </GRPMEDICALSUPPLIES>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPMEDICINE>\n";

        $this->set_buffer($buffer);
    }

    function xml_transfusion($scene, $detail, $scene_text, $detail_text) {
        $buffer  = "    <GRPTRANSFUSION>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPTRANSFUSION>\n";

        $this->set_buffer($buffer);
    }

    function xml_treatment($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material) {
        $buffer  = "    <GRPTREATMENT>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPMATERIAL>\n";

        $buffer .= "        <DATNAME>" . h($material['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($material['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($material['purchase']) . "</DATPURCHASE>\n";

        $buffer .= "      </GRPMATERIAL>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPTREATMENT>\n";

        $this->set_buffer($buffer);
    }

    function xml_apparatus($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material, $equipment) {
        $buffer  = "    <GRPAPPARATUS>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPEQUIPMENT>\n";

        $buffer .= "        <DATNAME>" . h($equipment['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($equipment['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATDATEOFMANUFACTURE>" . h($equipment['dateofmanufacture']) . "</DATDATEOFMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($equipment['purchase']) . "</DATPURCHASE>\n";
        $buffer .= "        <DATMAINTENANCE>" . h($equipment['maintenance']) . "</DATMAINTENANCE>\n";

        $buffer .= "      </GRPEQUIPMENT>\n";
        $buffer .= "      <GRPMATERIAL>\n";

        $buffer .= "        <DATNAME>" . h($material['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($material['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($material['purchase']) . "</DATPURCHASE>\n";

        $buffer .= "      </GRPMATERIAL>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPAPPARATUS>\n";

        $this->set_buffer($buffer);
    }

    function xml_draintube($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material) {
        $buffer  = "    <GRPDRAINTUBE>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPMATERIAL>\n";

        $buffer .= "        <DATNAME>" . h($material['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($material['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($material['purchase']) . "</DATPURCHASE>\n";

        $buffer .= "      </GRPMATERIAL>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPDRAINTUBE>\n";

        $this->set_buffer($buffer);
    }

    function xml_inspection($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material, $equipment) {
        $buffer  = "    <GRPINSPECTION>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPEQUIPMENT>\n";

        $buffer .= "        <DATNAME>" . h($equipment['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($equipment['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATDATEOFMANUFACTURE>" . h($equipment['dateofmanufacture']) . "</DATDATEOFMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($equipment['purchase']) . "</DATPURCHASE>\n";
        $buffer .= "        <DATMAINTENANCE>" . h($equipment['maintenance']) . "</DATMAINTENANCE>\n";

        $buffer .= "      </GRPEQUIPMENT>\n";
        $buffer .= "      <GRPMATERIAL>\n";

        $buffer .= "        <DATNAME>" . h($material['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($material['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($material['purchase']) . "</DATPURCHASE>\n";

        $buffer .= "      </GRPMATERIAL>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPINSPECTION>\n";

        $this->set_buffer($buffer);
    }

    function xml_recuperating($type, $scene, $detail, $type_text, $scene_text, $detail_text, $material) {
        $buffer  = "    <GRPRECUPERATING>\n";
        $buffer .= $this->get_type($type, $type_text);
        $buffer .= "      <GRPMATERIAL>\n";

        $buffer .= "        <DATNAME>" . h($material['name']) . "</DATNAME>\n";
        $buffer .= "        <DATMANUFACTURE>" . h($material['manufacture']) . "</DATMANUFACTURE>\n";
        $buffer .= "        <DATPURCHASE>" . h($material['purchase']) . "</DATPURCHASE>\n";

        $buffer .= "      </GRPMATERIAL>\n";
        $buffer .= $this->get_scene($scene, $scene_text);
        $buffer .= $this->get_detail($detail, $detail_text);
        $buffer .= "    </GRPRECUPERATING>\n";

        $this->set_buffer($buffer);
    }








    function xml_purpose($text) {
        if($this->get_hiyari_mode() == 'jiko') {
            $buffer = "    <DATPURPOSE>" . h($text) . "</DATPURPOSE>\n";

            $this->set_buffer($buffer);
        }
    }

    function xml_contenttext($text) {
        $buffer = "    <DATCONTENTTEXT>" . h($text) . "</DATCONTENTTEXT>\n";

        $this->set_buffer($buffer);
    }

    function xml_factor($arr_data, $text) {
        if(!is_array($arr_data)) return false;

        $buffer  = "    <LSTFACTOR>\n";

        foreach($arr_data as $value) {
            if(substr(trim($value['def_code']), -2) != 99) unset($text);
            $buffer .= "      <DATFACTOR CODE=\"" . h(trim($value['def_code'])) . "\">" . h($text) . "</DATFACTOR>\n";
        }

        $buffer .= "    </LSTFACTOR>\n";

        $this->set_buffer($buffer);
    }

    function xml_background($text) {
        $buffer  = "    <DATBACKGROUND>" . h($text) . "</DATBACKGROUND>\n";

        $this->set_buffer($buffer);
    }

    function xml_committee($code, $text) {
        if($this->get_hiyari_mode() == 'jiko') {
            $code = sprintf("%02d", $code);
            if($code != '99') {
                unset($text);
            } else {
                $text = $this->get_substr($text);
            }

            $buffer = "    <DATCOMMITTEE CODE=\"" . h($code) . "\">" . h($text) . "</DATCOMMITTEE>\n";

            $this->set_buffer($buffer);
        }
    }

    function xml_improvementtext($text) {
        $text = $this->get_substr($text, 4000);

        $buffer = "    <DATIMPROVEMENTTEXT>" . h($text) . "</DATIMPROVEMENTTEXT>\n";

        $this->set_buffer($buffer);
    }

    function xml_close() {
        // 最後の項目なのでフッターをつける
        $buffer = "  </GRPCASEREPORT>\n";

        $this->set_buffer($buffer);
    }

    function xml_footer() {
        $this->_footer = "</CASEREPORT>\n";

        return $this->_footer;
        // $this->set_buffer($buffer);
    }



    function get_type($code, $text) {
        return "      <DATTYPE CODE=\"" . h($code) . "\">" . h($text) . "</DATTYPE>\n";
    }

    function get_scene($code, $text) {
        return "      <DATSCENE CODE=\"" . h($code) . "\">" . h($text) . "</DATSCENE>\n";
    }

    function get_detail($code, $text) {
        return "      <DATDETAIL CODE=\"" . h($code) . "\">" . h($text) . "</DATDETAIL>\n";
    }


    function get_substr($text, $byte=1000) {
        $text = trim($text);
        if(strlen($text) > $byte) $text = substr($text, $byte);

        return $text;
    }





    function set_buffer($buffer) {
        $this->_buffer .= $buffer;
    }

    /*****************************************************************
     * 事故かヒヤリかを返す
     *
     * @return  STRING
     *****************************************************************/
    function get_hiyari_mode() {
        return $this->_mode;
    }

    function set_hiyari_mode($mode) {
        $this->_mode = $mode;
    }

    function xml_output() {
        //$doc = domxml_open_mem($this->_header . $this->_buffer);
        //return $doc->dump_mem(false, "Shift_JIS");
        return $this->_buffer;
    }
}
