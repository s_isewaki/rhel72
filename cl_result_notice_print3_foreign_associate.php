<?
require_once("about_session.php");
require_once("about_authority.php");
require_once("about_postgres.php");
require_once("cl_application_workflow_common_class.php");
require_once("cl_common.ini");
require_once("show_select_values.ini");
require_once("yui_calendar_util.ini");
require_once("cl_title_name.ini");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;


//====================================
//セッションのチェック
//====================================
$session = qualify_session($session,$fname);
if($session == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//権限チェック
//====================================
$wkfw=check_authority($session,$CAS_MENU_AUTH,$fname);
if($wkfw=="0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$obj = new cl_application_workflow_common_class($con, $fname);

$cl_title = cl_title_name();

//====================================
// 初期処理
//====================================

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$cl_title?>｜<?=$cl_title?>結果通知印刷</title>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/prototype/dist/prototype.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<script type="text/javascript">

</script>
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#A0D25A solid 1px;}
.list td td {border-width:0;}

table.block {border-collapse:collapse;}
table.block td {border:#A0D25A solid 1px;padding:1px;}
table.block td td {border-width:0;}

table.block_in {border-collapse:collapse;}
table.block_in td {border:#A0D25A solid 0px;}

</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="5" leftmargin="5" marginheight="5" marginwidth="5" onload="self.print();self.close();">
<form name="apply" action="#" method="post">
<input type="hidden" name="session" value="<?=$session?>">

<center>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="32" bgcolor="#A0D25A">
<td class="spacing"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14" color="#ffffff"><b><?=$cl_title?>結果通知印刷</b></font></td>
<td width="32" align="center"><a href="javascript:void(0);" onclick="window.close();"><img src="img/icon/close.gif" alt="閉じる" width="24" height="24" border="0" onmouseover="this.src = 'img/icon/closeo.gif';" onmouseout="this.src = 'img/icon/close.gif';"></a></td>
</tr>
</table>

<img src="img/spacer.gif" width="1" height="3" alt=""><br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<img src="img/spacer.gif" width="1" height="3" alt="">
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>チャレンジレベルＩＩＩ外来（准看護師）<b></font></td>
</tr>
<tr>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">志願者&nbsp;：&nbsp;<?=htmlspecialchars($emp_full_nm);?></font></td>
<td width="300"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">所属&nbsp;：&nbsp;<?=htmlspecialchars($class);?></font></td>
</tr>
<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
あなたのキャリアアップチャレンジは、看護の質向上に貢献するものであり、専門職としての姿<br>
そのものであります。心より経緯を評すとともに結果について報告させていただきます。
</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>1.結果</b></font></td>
</tr>
<tr>
<td colspan="7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成している項目&nbsp;：&nbsp;優れた能力でさらに磨いて頂きたい能力(チェック)</font></td>
</tr>

<tr>
<td width="150"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
<td width="5"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td width="60">
<input type="checkbox" id="chk_achieved_no1" name="chk_achieved_no1" value="t" <?if($chk_achieved_no1){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.1</font>
</td>
<td width="60">
<input type="checkbox" id="chk_achieved_no3" name="chk_achieved_no3" value="t" <?if($chk_achieved_no3){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.3</font>
</td>
<td width="60">
<input type="checkbox" id="chk_achieved_no31" name="chk_achieved_no31" value="t" <?if($chk_achieved_no31){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.31</font>
</td>
<td width="60"></td>
<td width="60"></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育能力/自己学習能力</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_achieved_no5" name="chk_achieved_no5" value="t" <?if($chk_achieved_no5){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.5</font>
</td>
<td>
<input type="checkbox" id="chk_achieved_no7" name="chk_achieved_no7" value="t" <?if($chk_achieved_no7){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.7</font>
</td>
<td>
<input type="checkbox" id="chk_achieved_no8" name="chk_achieved_no8" value="t" <?if($chk_achieved_no8){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.8</font>
</td>
<td>
<input type="checkbox" id="chk_achieved_no9" name="chk_achieved_no9" value="t" <?if($chk_achieved_no9){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.9</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_achieved_no11" name="chk_achieved_no11" value="t" <?if($chk_achieved_no11){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.11</font>
</td>
<td>
<input type="checkbox" id="chk_achieved_no32" name="chk_achieved_no32" value="t" <?if($chk_achieved_no32){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.32</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経験年数/経験領域</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_achieved_no12" name="chk_achieved_no12" value="t" <?if($chk_achieved_no12){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.12</font>
</td>
<td>
<input type="checkbox" id="chk_achieved_no33" name="chk_achieved_no33" value="t" <?if($chk_achieved_no33){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.33</font>
</td>
</tr>

<tr>
<td colspan="7"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">達成してない項目&nbsp;：&nbsp;次のステップアップの課題として磨いて頂きたい能力(チェック)</font></td>
</tr>

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">ナーシングプロセス</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_unachieved_no1" name="chk_unachieved_no1" value="t" <?if($chk_unachieved_no1){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.1</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no3" name="chk_unachieved_no3" value="t" <?if($chk_unachieved_no3){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.3</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no31" name="chk_unachieved_no31" value="t" <?if($chk_unachieved_no31){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.31</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">教育能力/自己学習能力</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_unachieved_no5" name="chk_unachieved_no5" value="t" <?if($chk_unachieved_no5){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.5</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no7" name="chk_unachieved_no7" value="t" <?if($chk_unachieved_no7){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.7</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no8" name="chk_unachieved_no8" value="t" <?if($chk_unachieved_no8){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.8</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no9" name="chk_unachieved_no9" value="t" <?if($chk_unachieved_no9){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.9</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">リーダーシップ能力</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_unachieved_no11" name="chk_unachieved_no11" value="t" <?if($chk_unachieved_no11){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.11</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no32" name="chk_unachieved_no32" value="t" <?if($chk_unachieved_no32){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.32</font>
</td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">経験年数/経験領域</font></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">:</font></td>
<td>
<input type="checkbox" id="chk_unachieved_no12" name="chk_unachieved_no12" value="t" <?if($chk_unachieved_no12){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.12</font>
</td>
<td>
<input type="checkbox" id="chk_unachieved_no33" name="chk_unachieved_no33" value="t" <?if($chk_unachieved_no33){?>checked<?}?>><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">No.33</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td colspan="2"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>2.認定レベル/コメント(該当のところにチェック)</b></font></td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="radio" name="radio_level_recognize" value="1" <?if($radio_level_recognize == "1"){?>checked<?}?>>レベル認定可
<input type="radio" name="radio_level_recognize" value="2" <?if($radio_level_recognize == "2"){?>checked<?}?>>レベル認定不可
</font>
</td>
</tr>

<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">レベル
<select id="level" name="level" style="width:40pt">
<option value="2" <?if($level == "2"){?>selected<?}?>>ＩＩ
<option value="3" <?if($level == "3"){?>selected<?}?>>ＩＩＩ
</select>
</font>
&nbsp;&nbsp;
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">区分
<select id="level_div" name="level_div" style="width:40pt">
<option value="A" <?if($level_div == "A"){?>selected<?}?>>A
<option value="B" <?if($level_div == "B"){?>selected<?}?>>B
<option value="C" <?if($level_div == "C"){?>selected<?}?>>C
<option value="D" <?if($level_div == "D"){?>selected<?}?>>D
<option value="E" <?if($level_div == "E"){?>selected<?}?>>E
<option value="F" <?if($level_div == "F"){?>selected<?}?>>F
<option value="G" <?if($level_div == "G"){?>selected<?}?>>G
</select>
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="chk_2_1" name="chk_2_1" value="t" <?if($chk_2_1){?>checked<?}?>>半年が経過した後、目標達成していない項目のみ再チャレンジする。
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="checkbox" id="chk_2_2" name="chk_2_2" value="t" <?if($chk_2_2){?>checked<?}?>>1年が経過した後、全ての項目を再チャレンジする。
</font>
</td>
</tr>

<tr>
<td colspan="2">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
<input type="text" id="chk_2_3_text" name="chk_2_3_text" value="<?=$chk_2_3_text?>" size="100">
</font>
</td>
</tr>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>3.認定結果についての疑問のある場合</b></font></td>
</tr>

<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
疑問の生じた評価項目とその理由を文書にし、評価用紙一式と共に看護管理部(認定委員会)<br>
へ提出して下さい。
</font>
</td>
</tr>

<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">提出期限：
<select name="date_y1" id="date_y1"><? show_select_years_for_cas(5, $date_y1, true)?></select>年
<select name="date_m1" id="date_m1"><? show_select_months($date_m1 ,true); ?></select>月
<select name="date_d1" id="date_d1"><? show_select_days($date_d1 ,true); ?></select>日
</font>
<img src="img/calendar_link.gif" style="position:relative;top:1px;left:3px;z-index:1;cursor:pointer;" onclick="show_cal1();"/>
<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
<div id="cal1Container" style="position:absolute;display:none;z-index:10000;"></div>
</font>
</td>
</tr>
</table>

<center>
</td>
</tr>
</table>
</body>
</form>
</html>
<?
pg_close($con);
?>

<script type="text/javascript">
<?
if($level == "")
{
?>
    document.getElementById('level').value = '';
    document.getElementById('level_div').value = '';
<?
}
?>
</script>