<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix 設備予約 | 年</title>
<?
require("about_session.php");
require("about_authority.php");
require("show_facility.ini");
require("facility_common.ini");
require("facility_year_common.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設備予約権限のチェック
$checkauth = check_authority($session, 11, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 施設・設備登録権限を取得
$fcl_admin_auth = check_authority($session, 41, $fname);

// 表示範囲が渡されていない場合は「全て」に設定
if ($range == "") {
	$range = "all";
}

// タイムスタンプが渡されていない場合は当日日付のタイムスタンプを取得
if ($date == "") {
	$date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// データベースに接続
$con = connect2db($fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 参照／予約／管理可能なカテゴリ・設備一覧を配列で取得
$categories = get_referable_categories($con, $emp_id, $fname);

// オプション情報を取得
$default_info = get_default_info($con, $session, $fname);
$default_url = $default_info["url"];
$calendar_start = $default_info["calendar_start"];

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 前年・翌年のタイプスタンプを変数に格納
$last_year = get_last_year($date);
$next_year = get_next_year($date);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
function changeDate(dt) {
	document.view.date.value = dt;
	document.view.submit();
}

function openPrintWindow() {
	window.open('facility_print_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>', 'newwin', 'width=840,height=600,scrollbars=yes');
}
// CSVダウンロード用
function downloadCSV(){
    document.csv.action = 'facility_csv.php';
    document.csv.submit();
}

</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="<? echo($default_url); ?>"><img src="img/icon/b06.gif" width="32" height="32" border="0" alt="設備予約"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="<? echo($default_url); ?>"><b>設備予約</b></a></font></td>
<? if ($fcl_admin_auth == "1") { ?>
<td align="right" style="padding-right:6px;"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><a href="facility_master_menu.php?session=<? echo($session); ?>"><b>管理画面へ</b></a></font></td>
<? } ?>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_menu.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（表）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_week_chart.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">週（チャート）</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_month.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">月</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#5279a5"><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>年</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_bulk_register.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">一括登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="95" align="center" bgcolor="#bdd1e7"><a href="facility_option.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($date); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">オプション</font></a></td>
<td align="right"><input type="button" value="印刷" onclick="openPrintWindow();"></td>
<td width="20"><input type="button" value="CSV出力" onclick="downloadCSV();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>
<form name="view" action="facility_year.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="30"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">表示範囲&nbsp;<select name="range" style="vertical-align:middle;" onchange="this.form.submit();"><? show_range_options($categories, $range); ?></select><img src="img/spacer.gif" width="50" height="1" alt=""><a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($last_year); ?>">&lt;前年</a>&nbsp;<select onchange="changeDate(this.options[this.selectedIndex].value);" style="vertical-align:middle;"><? show_date_options($last_year, $date, $next_year, $session, $range); ?></select>&nbsp;<a href="facility_year.php?session=<? echo($session); ?>&range=<? echo($range); ?>&date=<? echo($next_year); ?>">翌年&gt;</a></font></td>
</tr>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
<input type="hidden" name="date" value="<? echo($date); ?>">
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? show_reservation($con, $categories, $range, $date, $start_wd, $session, $fname); ?>
</table>
</td>
</tr>
</table>
</body>
<form name="csv" method="get">
    <input type="hidden" name="session" value="<?php echo $session; ?>">
    <input type="hidden" name="start_date" value="<?php echo date("Y",$date).'01'.'01' ?>">
    <input type="hidden" name="end_date" value="<?php echo date("Y",$date).'12'.'31' ?>">
    <input type="hidden" name="range" value="<?php echo $range; ?>">
</form>

<? pg_close($con); ?>
</html>
<?
// 日付オプションを出力
function show_date_options($last_year, $date, $next_year, $session, $range) {

	$y = date("Y", $last_year);
	echo("<option value=\"$last_year\">$y");

	$y = date("Y", $date);
	echo("<option value=\"$date\" selected>【{$y}】");

	$y = date("Y", $next_year);
	echo("<option value=\"$next_year\">$y");

}

// 予約状況を出力
function show_reservation($con, $categories, $range, $date, $start_wd, $session, $fname) {
	list($selected_category_id, $selected_facility_id) = split("-", $range);

	$facility_ids = array();
	foreach ($categories as $tmp_category) {
		foreach ($tmp_category["facilities"] as $tmp_facility) {
			$facility_ids[] = $tmp_facility["facility_id"];
		}
	}

	// 日付配列を作成
	$year = date("Y", $date);
	$arr_year = array();
	for ($i = 1; $i <= 12; $i++) {
		$tmp_month = substr("0$i", -2);
		array_push($arr_year, get_arr_month("$year$tmp_month", $start_wd));
	}

	// 年間の日ごとの予約件数を、キーが年月日の配列でまとめて取得。
	$arr_rsv_count = get_arr_rsv_count($con, $fname, $year, $selected_category_id, $selected_facility_id, $facility_ids);

	// 配列を4か月単位に分割
	$arr_year = array_chunk($arr_year, 4);

	// カレンダーを出力
	for ($i = 0; $i <= 2; $i++) {
		echo("<tr>\n");
		for ($j = 0; $j <= 3; $j++) {
			$month = substr("0" . ($i * 4 + $j + 1), -2);
			$day = date("d", $date);
			if (!checkdate($month, $day, $year)) {
				$day = substr("0" . $day - 1, -2);
				if (!checkdate($month, $day, $year)) {
					$day = substr("0" . $day - 1, -2);
				}
			}
			$tmp_date = mktime(0, 0, 0, $month, $day, $year);

			echo("<td valign=\"top\" width=\"25%\">\n");
			echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:#5279a5 solid 1px;\">\n");
			echo("<tr>\n");
			echo("<td>\n");

			echo("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">\n");
			echo("<tr>\n");
			echo("<td colspan=\"7\" height=\"22\" bgcolor=\"f6f9ff\">\n");
			echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"facility_month.php?session=$session&range=$range&date=$tmp_date\">$year/$month</a></font>\n");
			echo("</td>\n");
			echo("</tr>\n");

			echo("<tr>\n");
			for ($k = 0; $k < 7; $k++) {
				$tmp_wd = $start_wd + $k;
				if ($tmp_wd >= 7) {
					$tmp_wd -= 7;
				}

				switch ($tmp_wd) {
				case 0:
					$show_wd = "日";
					$bgcolor = "#fadede";
					break;
				case 1:
					$show_wd = "月";
					$bgcolor = "#bdd1e7";
					break;
				case 2:
					$show_wd = "火";
					$bgcolor = "#bdd1e7";
					break;
				case 3:
					$show_wd = "水";
					$bgcolor = "#bdd1e7";
					break;
				case 4:
					$show_wd = "木";
					$bgcolor = "#bdd1e7";
					break;
				case 5:
					$show_wd = "金";
					$bgcolor = "#bdd1e7";
					break;
				case 6:
					$show_wd = "土";
					$bgcolor = "#defafa";
					break;
				}

				echo("<td align=\"center\" bgcolor=\"$bgcolor\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">$show_wd</font></td>\n");
			}
			echo("</tr>\n");

			foreach ($arr_year[$i][$j] as $arr_week) {
				echo("<tr>\n");

				// 日付セルを出力
				foreach ($arr_week as $tmp_ymd) {
					$tmp_year = substr($tmp_ymd, 0, 4);
					$tmp_month = substr($tmp_ymd, 4, 2);
					$tmp_day = intval(substr($tmp_ymd, 6, 2));
					$tmp_date = mktime(0, 0, 0, $tmp_month, $tmp_day, $tmp_year);

					// 他の月の場合は空表示
					if ($tmp_month != $month) {
						echo("<td>&nbsp;</td>\n");

					// 当月の場合は詳細表示
					} else {

						// 予定件数を取得
						$rsv_count = $arr_rsv_count["$tmp_ymd"];
						if ($rsv_count > 0) {
							$bgcolor = " bgcolor=\"#fbdade\"";
						} else {
							$bgcolor = "";
						}
						echo("<td align=\"center\"$bgcolor><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"facility_menu.php?session=$session&range=$range&date=$tmp_date\">$tmp_day</a></font></td>\n");
					}
				}

				echo("</tr>\n");
			}

			echo("</table>\n");

			echo("</td>\n");
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
			if ($j < 3) {
				echo("<td><img src=\"img/spacer.gif\" alt=\"\" width=\"5\" height=\"1\"></td>\n");
			}
		}
		echo("</tr>\n");
		if ($i < 2) {
			echo("<tr>\n");
			echo("<td colspan=\"4\"><img src=\"img/spacer.gif\" alt=\"\" width=\"1\" height=\"5\"></td>\n");
			echo("</tr>\n");
		}
	}

}
?>
