<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
require_once("Cmx.php");
require_once("aclg_set.php");
require("about_session.php");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースに接続
$con = connect2db($fname);

// トランザクションを開始
pg_query($con, "begin");

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = "where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
	pg_query($con, "rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

array_push($_GET, "emp_id");
$_GET["emp_id"] = $emp_id;

// アクセスログ
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$_GET);


// チェックされたマイグループをループ
foreach ($mygroup_ids as $tmp_mygroup_id) {

	// 所属職員情報を削除
	$sql = "delete from mygroup";
	$cond = "where owner_id = '$emp_id' and mygroup_id = $tmp_mygroup_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// マイグループ情報を削除
	$sql = "delete from mygroupmst";
	$cond = "where owner_id = '$emp_id' and mygroup_id = $tmp_mygroup_id";
	$del = delete_from_table($con, $sql, $cond, $fname);
	if ($del == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}

	// マイページで表示対象となっていたら「本人のみ」に更新
	$sql = "update option set";
	$set = array("schedule_group");
	$setvalue = array(0);
	$cond = "where emp_id = '$emp_id' and schedule_group = $tmp_mygroup_id";
	$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
	if ($upd == 0) {
		pg_query($con, "rollback");
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
}

// トランザクションをコミット
pg_query($con, "commit");

// データベース接続を閉じる
pg_close($con);

// マイグループ画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'schedule_mygroup.php?session=$session';</script>");
?>
