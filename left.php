<?php
require_once("about_comedix.php");
require_once("about_certify.php");

$fname = $PHP_SELF;
$session = certify($id, $pass, $session ,$fname);
$authorities = get_authority($session, $fname);

$con = connect2db($con);

require("show_class_name.ini");
$arr_class_name = get_class_name_array($con, $fname);

// ログインユーザの職員IDを取得
$sql = "select emp_id from session";
$cond = " where session_id = '$session'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$emp_id = pg_fetch_result($sel, 0, "emp_id");

// 権限情報の取得
$webmail_auth = check_authority($session, 0, $fname);  // ウェブメール
$extension_auth = check_authority($session, 49, $fname);  // 内線電話帳
$inci_auth = check_authority($session, 47, $fname);  // ファントルくん
$fplus_auth = check_authority($session, 78, $fname);  // ファントルくん＋
$manabu_auth = check_authority($session, 75, $fname);  // バリテス
$cas_auth = check_authority($session, 67, $fname);  // CAS
$schedule_auth = check_authority($session, 2, $fname);  // スケジュール
$newsuser_auth = check_authority($session, 46, $fname);  // お知らせ
$message_auth = check_authority($session, 4, $fname);  // 伝言メモ
$approval_auth = check_authority($session, 7, $fname);  // 決裁・申請
$facility_auth = check_authority($session, 11, $fname);  // 設備予約
$library_auth = check_authority($session, 32, $fname);  // 文書管理
$bbs_auth = check_authority($session, 1, $fname);  // 掲示板
$task_auth = check_authority($session, 30, $fname);  // タスク
$memo_auth = check_authority($session, 45, $fname);  // 備忘録
$link_auth = check_authority($session, 60, $fname);  // リンクライブラリ
$intra_auth = check_authority($session, 55, $fname);  // イントラネット
$bed_auth = check_authority($session, 14, $fname);  // 病床管理
$jnl_auth = check_authority($session, 80, $fname);  // 日報月報
$ladder_auth = check_authority($session, 87, $fname);  // クリニカルラダー
$attdcd_auth = check_authority($session, 5, $fname);  // 出勤表
$wkadm_auth = check_authority($session, 42, $fname);  // 出勤表管理者
$shift_auth = check_authority($session, 69, $fname);  // 勤務シフト作成
$shiftadm_auth = check_authority($session, 70, $fname);  // 勤務シフト作成管理者
$ccusr1_auth = check_authority($session, 95, $fname);  // スタッフ・ポートフォリオ管理者


// オプション設定情報を取得
$sql = "select * from option";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");
$schedule_group = intval(pg_fetch_result($sel, 0, "schedule_group"));
$schedule_default = pg_fetch_result($sel, 0, "schedule1_default");
$mail_flg = pg_fetch_result($sel, 0, "top_mail_flg");
$ext_flg = pg_fetch_result($sel, 0, "top_ext_flg");
$inci_flg = pg_fetch_result($sel, 0, "top_inci_flg");
$fplus_flg = pg_fetch_result($sel, 0, "top_fplus_flg");
$manabu_flg = pg_fetch_result($sel, 0, "top_manabu_flg");
$cas_flg = pg_fetch_result($sel, 0, "top_cas_flg");
$event_flg = pg_fetch_result($sel, 0, "top_event_flg");
$schd_flg = pg_fetch_result($sel, 0, "top_schd_flg");
$info_flg = pg_fetch_result($sel, 0, "top_info_flg");
$task_flg = pg_fetch_result($sel, 0, "top_task_flg");
$msg_flg = pg_fetch_result($sel, 0, "top_msg_flg");
$aprv_flg = pg_fetch_result($sel, 0, "top_aprv_flg");
$whatsnew_flg = ($msg_flg == "t" && $aprv_flg == "t") ? "t" : "f";
$schdsrch_flg = pg_fetch_result($sel, 0, "top_schdsrch_flg");
$fcl_flg = pg_fetch_result($sel, 0, "top_fcl_flg");
$lib_flg = pg_fetch_result($sel, 0, "top_lib_flg");
$bbs_flg = pg_fetch_result($sel, 0, "top_bbs_flg");
$memo_flg = pg_fetch_result($sel, 0, "top_memo_flg");
$link_flg = pg_fetch_result($sel, 0, "top_link_flg");
$intra_flg = pg_fetch_result($sel, 0, "top_intra_flg");
$bed_info = pg_fetch_result($sel, 0, "bed_info");
$wic_flg = pg_fetch_result($sel, 0, "top_wic_flg");
$font_size = pg_fetch_result($sel, 0, "font_size");
$jnl_flg = pg_fetch_result($sel, 0, "top_jnl_flg");
$top_inci_news_flg = pg_fetch_result($sel, 0, "top_inci_news_flg");
$top_free_text_flg = pg_fetch_result($sel, 0, "top_free_text_flg");
$ladder_flg = pg_fetch_result($sel, 0, "top_ladder_flg");
$timecard_error_flg = pg_fetch_result($sel, 0, "top_timecard_error_flg");
$top_workflow_flg = pg_fetch_result($sel, 0, "top_workflow_flg");
$top_ccusr1_flg = pg_fetch_result($sel, 0, "top_ccusr1_flg");

// 文書管理オプション設定情報を取得
$sql = "select whatsnew_flg, whatsnew_days from liboption";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $lib_whatsnew_flg = pg_fetch_result($sel, 0, "whatsnew_flg");
    $lib_whatsnew_days = pg_fetch_result($sel, 0, "whatsnew_days");
} else {
    $lib_whatsnew_flg = "t";
    $lib_whatsnew_days = "7";
}

// 掲示板オプション設定情報を取得
$sql = "select whatsnew_flg, whatsnew_days from bbsoption";
$cond = "where emp_id = '$emp_id'";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $bbs_whatsnew_flg = pg_fetch_result($sel, 0, "whatsnew_flg");
    $bbs_whatsnew_days = pg_fetch_result($sel, 0, "whatsnew_days");
} else {
    $bbs_whatsnew_flg = "t";
    $bbs_whatsnew_days = "7";
}

// お知らせポップアップ設定を取得
$sql = "select popup_news from config";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$popup_news = pg_fetch_result($sel, 0, "popup_news");

// 表示フラグの設定
$mail_view_flg = ($mail_flg == "t" && $webmail_auth == "1");
$ext_view_flg = ($ext_flg == "t" && $extension_auth == "1");
$inci_view_flg = ($inci_flg == "t" && $inci_auth == "1");
$fplus_view_flg = ($fplus_flg == "t" && $fplus_auth == "1");
$manabu_view_flg = ($manabu_flg == "t" && $manabu_auth == "1");
$cas_view_flg = ($cas_flg == "t" && $cas_auth == "1");
$event_view_flg = ($event_flg == "t");
$schd_view_flg = ($schd_flg == "t" && $schedule_auth == "1");
$info_view_flg = ($info_flg == "t" && $newsuser_auth == "1");
$task_view_flg = ($task_flg == "t" && $task_auth == "1");
$lib_whatsnew_view_flg = ($lib_whatsnew_flg == "t" && $library_auth == "1");
$bbs_whatsnew_view_flg = ($bbs_whatsnew_flg == "t" && $bbs_auth == "1");
$whatsnew_view_flg = ($whatsnew_flg == "t" && ($message_auth == "1" || $approval_auth == "1" || $lib_whatsnew_view_flg == "t" || $bbs_whatsnew_view_flg == "t"));
$schdsrch_view_flg = ($schdsrch_flg == "t" && $schedule_auth == "1");
$fcl_view_flg = ($fcl_flg == "t" && $facility_auth == "1");
$lib_view_flg = ($lib_flg == "t" && $library_auth == "1");
$bbs_view_flg = ($bbs_flg == "t" && $bbs_auth == "1");
$memo_view_flg = ($memo_flg == "t" && $memo_auth == "1");
$link_view_flg = ($link_flg == "t" && $link_auth == "1");
$intra_view_flg = ($intra_flg == "t" && $intra_auth == "1");
$bed_view_flg = ($bed_info == "t" && $bed_auth == "1");
$jnl_view_flg = ($jnl_flg == "t" && $jnl_auth == "1");
$ladder_view_flg = ($ladder_flg == "t" && $ladder_auth == "1");
$workflow_view_flg = ($top_workflow_flg === "t" && $approval_auth == "1");
$ccusr1_view_flg = ($top_ccusr1_flg == "t" && $ccusr1_auth == "1");
if(($timecard_error_flg == "t") && ($attdcd_auth == "1" or $wkadm_auth == "1")){
	$error_shift_flg = ($shift_auth == "1" or $shiftadm_auth == "1") ? "1" : "0";
	$error_users_flg = ($wkadm_auth == "1") ? "0" : "1";
	$timecard_error_view_flg = true;
}

// スケジュール初期表示画面の設定
switch ($schedule_default) {
case "1":

    $schedule_page = "schedule_menu.php";
    break;
case "2":
    $schedule_page = "schedule_week_menu.php";
    break;
case "3":
    $schedule_page = "schedule_month_menu.php";
    break;
default:
    $schedule_page = "schedule_menu.php";
    break;
}

// マイグループ一覧を配列に格納
$mygroups = array();
$mygroups[0] = "本人のみ";
$sql = "select mygroup_id, mygroup_nm from mygroupmst";
$cond = "where owner_id = '$emp_id' order by mygroup_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $mygroups[$row["mygroup_id"]] = $row["mygroup_nm"];
}

//// 組織最下部分
$sql = "select distinct e.emp_dept, d.dept_nm, e.emp_room, r.room_nm from empmst e inner join deptmst d on d.dept_id = e.emp_dept left join classroom r on r.room_id = e.emp_room where e.emp_id = '$emp_id' union select c.emp_dept, d.dept_nm, c.emp_room, r.room_nm from concurrent c inner join deptmst d on d.dept_id = c.emp_dept left join classroom r on r.room_id = c.emp_room where c.emp_id = '$emp_id'";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"./js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    if ($row["emp_room"] == "") {
        $mygroups[3000 + intval($row["emp_dept"])] = $row["dept_nm"];
    } else {
        $mygroups[4000 + intval($row["emp_room"])] = $row["room_nm"];
    }
}

//// 共通グループ分
$sql = "select m.group_id, m.group_nm from comgroupmst m";
$cond = "where m.public_flg or exists (select * from comgroup g where g.group_id = m.group_id and g.emp_id = '$emp_id') order by m.group_id";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
while ($row = pg_fetch_array($sel)) {
    $mygroups[$row["group_id"]] = $row["group_nm"];
}

// 内線電話帳の共通オプション情報を取得
$sql = "select newcomer_flg from extadmopt";
$cond = "";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
if (pg_num_rows($sel) > 0) {
    $newcomer_flg = pg_fetch_result($sel, 0, "newcomer_flg");
} else {
    $newcomer_flg = "t";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix</title>
<?php
require("menu_news.ini");
require("menu_message.ini");
require("conf/sql.inf");
require("show_menu_schedule_list.ini");
require("show_schedule_common.ini");
require("show_menu_to_do.ini");
require("show_memo.ini");
require("menu_javascript.ini");
require("show_select_values.ini");
require("show_apply_approve_notice.ini");
require("show_menu_document_list.ini");
require("show_menu_bbs_list.ini");
require("show_date_navigation_week.ini");
require("show_date_navigation_month.ini");
require("show_wic.ini");
require("label_by_profile_type.ini");

$memo=show_memo($session,$fname);
show_javascript($session,$fname);

$this_week = mktime(0,0,0,date('m'), date('d'), date('Y'));
if(!isset($_REQUEST['date'])){
    $date1 = $this_week;
}else{
    $date1 = $_REQUEST['date'];
}
$day = date('d', $date1);
$month = date('m', $date1);
$year = date('Y', $date1);

$cur_date = date("Y年n月j日");
switch (date("w")) {
case 0:
    $cur_weekday = date("日曜");
    break;
case 1:
    $cur_weekday = date("月曜");
    break;
case 2:
    $cur_weekday = date("火曜");
    break;
case 3:
    $cur_weekday = date("水曜");
    break;
case 4:
    $cur_weekday = date("木曜");
    break;
case 5:
    $cur_weekday = date("金曜");
    break;
case 6:
    $cur_weekday = date("土曜");
    break;
}
$cur_date .= "　$cur_weekday";

// スタート曜日の設定
$start_wd = ($calendar_start == 1) ? 0 : 1;  // 0：日曜、1：月曜

// 当該週のスタート日を求める（スケジュール）
$start_day = get_start_day($date1, $start_wd);

// 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得（スケジュール）
$last_month = get_last_month($date1);
$last_week = get_last_week($date1);
$next_week = get_next_week($date1);
$next_month = get_next_month($date1);

// 入退院予定表示用の日付が渡されていなければ求める
if ($bedschd_date == "") {
    $bedschd_date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
}

// タスクの表示件数は7件がデフォルト
if ($task_limit == "") {$task_limit = "7";}

// お知らせの表示件数は全件がデフォルト
if ($news_limit == "") {$news_limit = "ALL";}

// 文書管理の並び順は番号の昇順がデフォルト
if ($lib_order == "") {$lib_order = "7";}

// 設備予約のオプション情報を取得
if ($fcl_view_flg) {
    require("show_facility.ini");
    $fcl_default_info = get_default_info($con, $session, $fname);
    $fcl_default_url = $fcl_default_info["url"];
}

// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// フォントサイズの判定
$font_class = ($font_size == "2") ? "j16" : "j12";
?>
<script type="text/javascript">
function checkValue(wherefrom) {
    if (wherefrom == 1) {
        document.memo.action = 'memo_insert.php?button=1';
        document.memo.submit();
    } else {
        document.memo.action = 'memo_insert.php?button=2';
        document.memo.submit();
    }
}

function changeDate(dt) {
    location.href = 'left.php?session=<?php echo($session); ?>&date=' + dt + '&bedschd_date=<?php echo($bedschd_date); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>';
}

function changeBuilding(bldg_cd) {
    location.href = 'left.php?session=<?php echo($session); ?>&calendar_bldg_cd=' + bldg_cd + '&bedschd_date=<?php echo($bedschd_date); ?>&date=<?php echo($date1); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>&updbedcalopt=1';
}

function changeWard(ward_cd) {
    location.href = 'left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=' + ward_cd + '&bedschd_date=<?php echo($bedschd_date); ?>&date=<?php echo($date1); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>&updbedcalopt=1';
}

function changeDate2(dt) {
    location.href = 'left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=<?php echo($calendar_ward_cd); ?>&bedschd_date=' + dt + '&date=<?php echo($date1); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>';
}

function popupScheduleDetail(title, place, date, time, type, detail, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            '行先', place,
            '日付', date,
            '時刻', time,
            '種別', type,
            '詳細', detail
        ), 400, 80, e
    );
}

function popupNewsDetail(ref_status, title, category, notice_name, src_class_name, src_emp_name, detail, date, e) {
<?php if ($popup_news != "t") { ?>
    return;
<?php } ?>
    popupDetailYellow(
        new Array(
            '確認', ref_status,
            'タイトル', title,
            '対象カテゴリ', category,
            '通達区分', notice_name,
            '発信部署', src_class_name,
            '発信者', src_emp_name,
            '内容', detail,
            '登録日', date
        ), 400, 100, e
    );
}

function popupTaskDetail(pjt_name, wg_name, work_name, status, expire_date, priority, detail, e) {
    var values = new Array();
    if (pjt_name != '') {
        values.push('委員会名', pjt_name);
    }
    if (wg_name != '') {
        values.push('WG名', wg_name);
    }
    values.push(
        '作業名', work_name,
        '進捗', status,
        '終了予定日', expire_date,
        '優先度', priority,
        '作業内容', detail
    );
    popupDetailPink(values, 400, 100, e);
}

function popupHiyariNewsDetail(title, category, detail, list_term, register, date, e) {
    popupDetailBlue(
        new Array(
            'タイトル', title,
            'カテゴリ', category,
            '内容', detail,
            '掲載期間', list_term,
            '登録者', register,
            '登録日', date
        ), 400, 80, e
    );
}

function popupHiyariNewsDetailImg(title, category, detail, list_term, register, date, img, e) {
    popupDetailBlueImg(
        new Array(
            'タイトル', title,
            'カテゴリ', category,
            '内容', detail,
            '掲載期間', list_term,
            '登録者', register,
            '登録日', date
        ), 400, 80, img, e
    );
}

function timecard_check(errorType){
	document.timecard_error.err_msg.value = errorType;
	document.timecard_error.mypage.value = "1";
	document.timecard_error.submit();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/domready.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
table.block {border-collapse:collapse;}
table.block td {border:#5279a5 solid 1px;}
table.block td td {border-width:0;}
table.block td table.block td {border:#5279a5 solid 1px;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td height="31" class="spacing"><a href="left.php?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>マイページ</b></font></a></td>
</tr>
</table>

<?php
if($top_free_text_flg == "t")
{
    //フリーテキストの表示
    $sql = "select * from config";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $free_text = pg_fetch_result($sel, 0, "free_text");
    if($free_text != "")
    {
?>
<img src="img/spacer.gif" width="1" height="10"><br>
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="block">
    <tr>
        <td style="padding:7px 10px 5px;"><?php echo($free_text); ?></td>
    </tr>
</table>
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="3"><img src="img/spacer.gif" width="1" height="5" alt=""></td>
    </tr>
</table>

        <?php
    }
}
?>

<?php
if ($mail_view_flg || $ext_view_flg) {
    echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
    echo("<tr>\n");

    if ($mail_view_flg) {
        if ($ext_view_flg) {
            echo("<td width=\"50%\">\n");
        } else {
            echo("<td width=\"100%\">\n");
        }
        require("menu_webmail.ini");
        echo("</td>\n");
    }

    if ($ext_view_flg) {
        if ($mail_view_flg) {
            echo("<td width=\"5\"><img src=\"img/spacer.gif\" width=\"5\" height=\"1\" alt=\"\"></td>");
            echo("<td width=\"50%\">\n");
        } else {
            echo("<td width=\"100%\">\n");
        }
        echo("<form action=\"ext/ext_search.php\" method=\"post\">\n");
        echo('<table width="100%" cellspacing="0" cellpadding="0" style="border:1px solid #5279a5;">');
        if ($font_size == "1") {
            echo("<tr height=\"30\" bgcolor=\"#bdd1e7\">\n");
            echo("<td width=\"20\" class=\"spacing\"><a href=\"ext/ext_menu.php?extension=1\"><img src=\"img/icon/s36.gif\" width=\"20\" height=\"20\" border=\"0\" alt=\"一発電話検索\"></a></td>\n");
            echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"ext/ext_menu.php?extension=1\"><b>一発電話検索</b></a>");
        } else {
            echo("<tr height=\"32\" bgcolor=\"#bdd1e7\">\n");
            echo("<td width=\"32\" valign=\"middle\"><a href=\"ext/ext_menu.php?extension=1\"><img src=\"img/icon/b36.gif\" width=\"32\" height=\"32\" border=\"0\" alt=\"一発電話検索\"></a></td>\n");
            echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j16\"><a href=\"ext/ext_menu.php?extension=1\"><b>一発電話検索</b></a>");
        }
        $box_width = ($newcomer_flg == "t") ? 17 : 25;
        echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"10\" height=\"1\">職員名：<input type=\"text\" name=\"srch_emp_nm\" value=\"\" style=\"width:$box_width%; ime-mode:active;\"><input type=\"submit\" value=\"検索\" style=\"margin-left:4px;\">");
        if ($newcomer_flg == "t") {
            echo("<img src=\"img/spacer.gif\" alt=\"\" width=\"10\" height=\"1\"><a href=\"ext/ext_newcomer.php\">今月の新人</a>");
        }
        echo("</font></td>\n");
        echo("</tr>\n");
        echo("</table>\n");
        echo("<input type=\"hidden\" name=\"session\" value=\"$session\">\n");
        echo("<input type=\"hidden\" name=\"action\" value=\"search\">\n");
        echo("<input type=\"hidden\" name=\"extension\" value=\"1\">\n");
        echo("</form>\n");
        echo("</td>\n");
    }

    echo("</tr>\n");
    echo("</table>\n");
}
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?php
    if($workflow_view_flg){
        require_once("show_workflow_block.php");
        show_workflow_block($con, $emp_id, $font_size, $session, $fname);
    }

if ($inci_view_flg || $fplus_view_flg) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php
    if ($inci_view_flg) {
        if ($fplus_view_flg) {
?>
<!-- incident -->
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }

        require_once("hiyari_parts.ini");

        //Tsuzuki Revise 2010/01/01 メッセージリンク表示
        show_incident_infomation_mes_link($session, $fname, $con, $font_size);
?>
</td>
<!-- incident -->
<?php
    }

    if ($fplus_view_flg) {
        if ($inci_view_flg) {
?>
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<?php
        }

        if ($inci_view_flg) {
?>
<!-- fplus -->
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }

        require_once("show_fplus_apply_approve_notice.ini");
        show_fplus_apply_approve_notice($con, $emp_id, $session, $fname,$font_size);
?>
</td>
<!-- fplus -->
<?php
    }
?>
</tr>
</table>
<?php
}



if ($manabu_view_flg || $cas_view_flg) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php
    if ($manabu_view_flg) {
        if ($cas_view_flg) {
?>
<!-- e-lerning -->
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }

        require_once("show_mypage_manabu.ini");
        show_manabu_block($con, $emp_id, $session, $fname, $font_size);
?>
</td>
<!-- e-lerning -->
<?php
    }

    if ($cas_view_flg) {
        if ($manabu_view_flg) {
?>
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<?php
        }

        if ($manabu_view_flg) {
?>
<!-- cas -->
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }

        require_once("show_cas_apply_approve_notice.ini");
        show_cas_apply_approve_notice($con, $emp_id, $session, $fname, $font_size);
?>
</td>
<!-- cas -->
<?php
    }
?>
</tr>
</table>
<?php
}



if ($ladder_view_flg) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
    <?php
    require_once("cl_apply_approve_notice.ini");
    show_cl_apply_approve_notice($con, $emp_id, $session, $fname, $font_size);
?>
</td>
</tr>
</table>
    <?php
}
?>






<?php if ($ccusr1_view_flg) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<?php
    require_once("spf/mypage.php");
    show_spf_mypage_notice($con, $emp_id, $session, $fname, $font_class);
?>
</td>
</tr>
</table>
<?php } ?>






<?php
if ($jnl_view_flg || $timecard_error_view_flg) {
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php
    if ($jnl_view_flg) {
        if ($timecard_error_view_flg) {
?>
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }

        require_once("show_jnl_apply_approve_notice.ini");
    	show_jnl_apply_approve_notice($con, $emp_id, $session, $fname,$font_size);
?>
</td>
<?php
    }

    if ($timecard_error_view_flg) {
        if ($jnl_view_flg) {
?>
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<?php
        }

        if ($jnl_view_flg) {
?>
<td width="50%" valign="top">
<?php
        } else {
?>
<td width="100%" valign="top">
<?php
        }
        echo("<form name=\"timecard_error\" method=\"POST\" action=\"work_admin_timecard_check.php\">\n");
        require_once("show_timecard_error_notice.ini");
        show_timecard_error_notice($session, $con, $fname, $font_size, $emp_id, $error_shift_flg, $error_users_flg);
        echo("</form>\n");
?>
</td>
<!-- cas -->
<?php
    }
?>
</tr>
</table>
<?php
}
?>

<?php if ($schd_view_flg || $event_view_flg) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!--schedule_list-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="100%">
<form action="schedule_group_update_exe.php" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22" bgcolor="#bdd1e7">
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<?php if ($schd_view_flg) { ?>
<td width="20" class="spacing"><a href="<?php echo($schedule_page); ?>?session=<?php echo($session); ?>">
<?php
    if($font_size == "1") echo"<img src='img/icon/s02.gif' width='20' height='20' border='0' alt='スケジュール'></a></td>";
    else echo"<img src='img/icon/s02.gif' width='32' height='32' border='0' alt='スケジュール'></a></td>";
?>

<td><a href="<?php echo($schedule_page); ?>?session=<?php echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class=<?=$font_class?>><b>スケジュール</b></font></a></td>
<?php } else { ?>
<td width="20" class="spacing">
<?php
if($font_size == "1") echo "<img src='img/icon/s22.gif' width='20' height='20' border='0' alt='{$_label_by_profile['EVENT'][$profile_type]}'></td>";
    else echo "<img src='img/icon/s22.gif' width='32' height='32' border='0' alt='{$_label_by_profile['EVENT'][$profile_type]}'></td>";
?>

<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><b><?php echo($_label_by_profile["EVENT"][$profile_type]); ?></b></font></td>
<?php } ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($cur_date); ?></font></td>
<?php if ($schd_view_flg) { ?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>">
<?php if ($schd_emp_id == "") {  // 週間表示 ?>
表示範囲：<select name="schedule_group" onchange="this.form.submit();">
<?php
foreach ($mygroups as $tmp_mygroup_id => $tmp_mygroup_nm) {
    echo("<option value=\"$tmp_mygroup_id\"");
    if ($tmp_mygroup_id == $schedule_group) {
        echo(" selected");
    }
    echo(">$tmp_mygroup_nm\n");
}
?>
</select>
<?php } ?>
</font></td>
<?php } ?>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>">
<?php if ($schd_emp_id == "") {  // 週間表示 ?>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($last_month); ?>&bedschd_date=<?php echo($bedschd_date); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">&lt;&lt;前月</a>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($last_week); ?>&bedschd_date=<?php echo($bedschd_date); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">&lt;前週</a>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($this_week); ?>&bedschd_date=<?php echo($bedschd_date); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">今週</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);">
<?php show_date_options_w($date1, $start_day); ?>
</select>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($next_week); ?>&bedschd_date=<?php echo($bedschd_date); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">翌週&gt;</a>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($next_month); ?>&bedschd_date=<?php echo($bedschd_date); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">翌月&gt;&gt;</a>
<?php } else {  // 月間表示 ?>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($last_month); ?>&bedschd_date=<?php echo($bedschd_date); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">&lt;&lt;前月</a>
<select onchange="changeDate(this.options[this.selectedIndex].value);">
<?php show_date_options_m($date1); ?>
</select>
<a href="left.php?session=<?php echo($session); ?>&date=<?php echo($next_month); ?>&bedschd_date=<?php echo($bedschd_date); ?>&schd_emp_id=<?php echo($schd_emp_id); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">翌月&gt;&gt;</a>
<?php } ?>
</font></td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="date" value="<?php echo($date1); ?>">
<input type="hidden" name="bedschd_date" value="<?php echo($bedschd_date); ?>">
</form>
<script type="text/javascript">
function popupEventDetail(fcl_name, evt_date, evt_time, evt_name, evt_content, evt_contact, e) {
    popupDetailBlue(
        new Array(
            '施設', fcl_name,
            '日付', evt_date,
            '時刻', evt_time,
            '行事名', evt_name,
            '詳細', evt_content,
            '連絡先', evt_contact
        ), 400, 80, e
    );
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="1" style="border-collapse:collapse;">
<?php
if ($event_view_flg && $schd_emp_id == "") {
    show_event_schedule_list_w($session, $fname, $date1, $start_wd, $schd_view_flg, $_label_by_profile["EVENT"][$profile_type], $con, $font_class);
}
if ($schd_view_flg) {
    if ($schd_emp_id == "") {  // 週間表示
        show_menu_schedule_list_w($session, $fname, $date1, $schedule_group, $bedschd_date, $emp_id, $start_wd, $event_view_flg, $con, $font_class);
    } else {  // 月間表示
        show_menu_schedule_list_m($date1, $schd_emp_id, $session, $fname, $bedschd_date, $emp_id, $start_wd, $con, $font_class);
    }
}
?>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--schedule_list-->
<?php } ?>
<?php if ($info_view_flg || $task_view_flg) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php if ($info_view_flg) { ?>
<?php if ($task_view_flg) { ?>
<td width="61%" valign="top">
<?php } else { ?>
<td width="100%" valign="top">
<?php } ?>
<form name="menu_news" action="menu_news_display.php" method="post">
<?php show_menu_news($con, $authorities, $font_size, $arr_class_name, $session, $fname, $emp_id, $sele_stat, $task_limit, $news_limit, $date1); ?>
<input type="hidden" name="session" value="<?php echo($session); ?>">
</form>
</td>
<?php } ?>
<?php if ($task_view_flg) { ?>
<?php if ($info_view_flg) { ?>
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<td width="39%" valign="top">
<?php } else { ?>
<td width="100%" valign="top">
<?php } ?>
<!--task-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#bdd1e7">
<td width="20" height="22" class="spacing"><a href="work_menu.php?session=<?php echo($session); ?>">
<?php
    if($font_size == "1") echo "<img src='img/icon/s04.gif' width='20' height='20' border='0' alt='タスク'>";
    else echo "<img src='img/icon/s04.gif' width='32' height='32' border='0' alt='タスク'>";
?>
</a></td>
<td width="30%"><a href="work_menu.php?session=<?php echo($session); ?>">

<?php
    if($font_size == "1") echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><b>タスク</b></font></a></td>";
    else echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'><b>タスク</b></font></a></td>";
?>

<td width="70%">
<?php
    if($font_size == "1") echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>";
    else echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'>";
?>
<?php
if ($task_limit == "7") {
    echo("7件表示 | <a href=\"left.php?session=$session&date=$date1&bedschd_date=$bedschd_date&task_limit=ALL&news_limit=$news_limit\">全件表示</a>");
} else {
    echo("<a href=\"left.php?session=$session&date=$date1&bedschd_date=$bedschd_date&task_limit=7&news_limit=$news_limit\">7件表示</a> | 全件表示");
}
?>
</font></td>
<td width="13" align="center" valign="middle" bgcolor="#bdd1e7"><a href="to_do_register.php?session=<?php echo($session); ?>"><img src="img/pencil.gif" width="13" height="13" border="0" alt="登録"></a></td>
<td nowrap><a href="to_do_register.php?session=<?php echo($session); ?>">

<?php
    if($font_size == "1") echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>登録</font>";
    else echo "<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'>登録</font>";
?>

</a></td>
</tr>
<tr>
<td colspan="5" bgcolor="#5279a5"><img src="./img/spacer.gif" width="1" height="1"></td>
</tr>
</table>
<form name="to_do" action="to_do_complete.php" method="post">
<?php show_menu_to_do($con, $task_limit, $session, $fname, $font_size); ?>
<input type="hidden" name="session" value="<?php echo($session); ?>">
<input type="hidden" name="work_id">
<input type="hidden" name="pjt_id">
</form>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<!--task-->
</td>
<?php } ?>
</tr>
</table>
<?php } ?>
<?php if ($whatsnew_view_flg || $schdsrch_view_flg) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<?php if ($whatsnew_view_flg) { ?>
<?php if ($schdsrch_view_flg) { ?>
<td width="61%" valign="top">
<?php } else { ?>
<td width="100%" valign="top">
<?php } ?>
<!--latest-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#bdd1e7">
<?php if ($font_size == "1") { ?>
<td width="19" height="22" class="spacing"></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><b>最新情報</b></font></td>
<?php } else { ?>
<td width="19" height="32" class="spacing"></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16"><b>最新情報</b></font></td>
<?php } ?>
</tr>
<tr>
<td colspan="2" bgcolor="#5279a5"><img src="./img/spacer.gif" width="1" height="1"></td>
</tr>
<?php
if ($message_auth == "1") {
    show_menu_message($con, $emp_id, $font_size, $session, $fname);
}
?>
<!--approval-->
<?php if ($approval_auth == "1") { ?>
<tr>
<td colspan="2">
<?php if ($message_auth == "1") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?php } ?>
<?php show_apply_approve_notice($con, $emp_id, $font_size, $session, $fname); ?>
<?php } ?>
<!--approval-->
<!--library-->
<?php if ($lib_whatsnew_view_flg == "t") { ?>
<tr>
<td colspan="2">
<?php if ($message_auth == "1" || $approval_auth == "1") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?php } ?>
<?php
require_once("library_common.php");
// 最新文書一覧表示フラグを取得
$newlist_flg = lib_get_show_newlist_flg($con, $fname);
if ($newlist_flg == 't') {
    show_new_document_list($con, $emp_id, $lib_whatsnew_days, $font_size, $session, $fname);
}
?>
<?php } ?>
<!--library-->
<!--bbs-->
<?php if ($bbs_whatsnew_view_flg == "t") { ?>
<tr>
<td colspan="2">
<?php if ($message_auth == "1" || $approval_auth == "1" || $lib_whatsnew_view_flg == "t") { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<?php } ?>
<?php show_new_bbs_list($con, $bbs_whatsnew_days, $font_size, $session, $fname); ?>
<?php } ?>
<!--bbs-->
</table>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<!--latest-->
<?php } ?>
</td>
<?php if ($schdsrch_view_flg) { ?>
<?php if ($whatsnew_view_flg) { ?>
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<td width="39%" valign="top">
<?php } else { ?>
<td width="100%" valign="top">
<?php } ?>

<?php $font_class = ($font_size == "2") ? "j16" : "j12"; ?>

<!--schedule_search-->
<form method="post" name="search" action="schedule_pre_search.php" target="schdsrch">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#bdd1e7">
<?php
if($font_size == "1") echo "<td width='20' height='22' class='spacing'><img src='img/icon/s33.gif' width='20' height='20' border='0' alt='スケジュール検索'></td>";
    else echo "<td width='20' height='22' class='spacing'><img src='img/icon/s33.gif' width='32' height='32' border='0' alt='スケジュール検索'></td>";
?>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class=<?=$font_class?>><b>スケジュール検索</b></font></td>
</tr>
<tr>
<td colspan="2" bgcolor="#5279a5"><img src="./img/spacer.gif" width="1" height="1"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#f6f9ff">
<td height="22" width="100" align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class=<?=$font_class?>>職員名：</font></td>
<td><input type="text" name="emp_nm" size="22" maxlength="40" style="ime-mode: active;"></td>
</tr>
<tr bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>">属性：</font></td>
<td><select name="cls" onChange="javascript:clickClass()"><option value="0">すべて</option><?php show_class2($class_cond,$fname); ?></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($arr_class_name[0]); ?></font><br><select name="atrb" onChange="javascript:clickAtrb()"><option value="0">すべて</option>
</select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($arr_class_name[1]); ?></font><br><select name="dept"><option value="0">すべて</option></select><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($arr_class_name[2]); ?></font></td>
</tr>
<tr bgcolor="#f6f9ff">
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>">委員会・WG：</font></td>
<td><select name="pjt_id"><option value="0">すべて<?php show_project($con, null, $fname); ?></select><input type="submit" value="検索"></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<input type="hidden" name="session" value="<?php echo($session); ?>">
</form>
<iframe name="schdsrch" width="0" height="0" frameborder="0" src="blank.php"></iframe>
<!--schedule_search-->
</td>
<?php } ?>
</tr>
</table>
<?php } ?>
<?php
if ($fcl_view_flg) {
    require("show_menu_reservation_list.ini");
    show_reservation_block($con, $emp_id, $font_size, $fcl_default_url, $session, $fname);
}
if ($lib_view_flg) {
    show_document_block($con, $emp_id, $lib_archive, $lib_category, $lib_folder, $lib_order, $font_size, $session, $fname);
}
if ($bbs_view_flg) {
    show_bbs_block($con, $emp_id, $bbs_category_id, $bbs_theme_id, $bbs_page, $font_size, $session, $fname);
}
?>
<?php if ($memo_view_flg) { ?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!--memo-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="">
<form name="memo" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#bdd1e7">
<td width="20" height="22" class="spacing">
<?php
    if($font_size == "1"){
        echo "<img src='img/icon/s34.gif' width='20' height='20' alt='備忘録'></td>";
        echo"<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><b>備忘録</b></font>";
    }
    else {
        echo "<img src='img/icon/s34.gif' width='32' height='32' alt='備忘録'></td>";
        echo"<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'><b>備忘録</b></font>";
    }
?>
</td>
</tr>
<tr>
<td colspan="2" bgcolor="#5279a5"><img src="./img/spacer.gif" width="1" height="1"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1" bgcolor="#f6f9ff">
<tr>

<?php
    if($font_size == "1"){
?>

    <td><textarea name="memo" rows="5" cols="80" style="ime-mode:active;width:100%;background-color:#fefcdf;color:black;"><?php echo(h($memo)); ?></textarea></td>
<?php
    }else{
?>
    <td><textarea name="memo" rows="5" cols="80"  style="ime-mode:active;width:100%; font-size:18px; background-color:#fefcdf;color:black;"><?php echo(h($memo)); ?></textarea></td>
<?php
    }
?>
</tr>
<tr>
<td align="right"><input type="button" value="消去" onclick="checkValue(1)">&nbsp;<input type="button" value="登録" onclick="checkValue(2)"><input type="hidden" name="session" value=<?php echo($session); ?>>
</td>
</tr>
</table>
</form>
</td>
<td width="1" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
<!--memo-->
<?php } ?>
<?php
// リンクライブラリ表示
if ($link_view_flg) {
    require("show_menu_link_list.ini");
    show_link_block($con, $session, $fname, $font_size);
}

// イントラネット表示
if ($intra_view_flg) {

    // イントラメニュー情報を取得
    $sql = "select * from intramenu";
    $cond = "";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $menu1 = pg_fetch_result($sel, 0, "menu1");
    $desc1 = pg_fetch_result($sel, 0, "desc1");
    $menu2 = pg_fetch_result($sel, 0, "menu2");
    $desc2 = pg_fetch_result($sel, 0, "desc2");
    $menu3 = pg_fetch_result($sel, 0, "menu3");
    $desc3 = pg_fetch_result($sel, 0, "desc3");

    // 社会福祉法人の場合、「・外来」を削除
    if ($profile_type == PROFILE_TYPE_WELFARE) {
        $desc2 = mbereg_replace("・外来", "", $desc2);
    }
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!--intra-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#bdd1e7">
<td width="20" height="22" class="spacing"><a href="intra_menu.php?session=<?php echo($session); ?>">
<?php
    if($font_size == "1") echo"<img src='img/icon/s38.gif' width='20' height='20' border='0' alt='イントラネット'></a></td>";
    else echo"<img src='img/icon/s38.gif' width='32' height='32' border='0' alt='イントラネット'></a></td>";
?>
<td>
<?php
    if($font_size == "1") echo"<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>";
    else echo"<font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'>";
?>
<a href="intra_menu.php?session=<?php echo($session); ?>"><b>イントラネット</b></a></font></td>

</tr>
</table>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="block" style="position:relative;top:-1px;">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_stat.php?session=<?php echo($session); ?>"><img src="img/icon/intra_stat.gif" alt="<?php echo($menu1); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="block">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($menu1); ?></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($desc1); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_info.php?session=<?php echo($session); ?>"><img src="img/icon/intra_info.gif" alt="<?php echo($menu2); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="block">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($menu2); ?></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($desc2); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="img/spacer.gif" alt="" width="1" height="10"><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="50"><a href="intra_life.php?session=<?php echo($session); ?>"><img src="img/icon/intra_life.gif" alt="<?php echo($menu3); ?>" width="50" height="50" border="0"></a></td>
<td width="10"></td>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="block">
<tr>
<td bgcolor="#f6f9ff"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($menu3); ?></font></td>
</tr>
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($desc3); ?></font></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--intra-->
<?php
}

// 入退院カレンダー表示
if ($bed_view_flg) {
    require_once("show_bed_schedule2.ini");

    // 棟一覧を取得
    $sql = "select bldg_cd, bldg_name from bldgmst";
    $cond = "where bldg_del_flg = 'f' order by bldg_cd";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    $buildings = array();
    while ($row = pg_fetch_array($sel)) {
        $buildings[$row["bldg_cd"]] = array("name" => $row["bldg_name"], "wards" => array());
    }

    // 病棟一覧を取得
    $sql = "select bldg_cd, ward_cd, ward_name from wdmst";
    $cond = "where ward_del_flg = 'f' order by ward_cd";
    $sel = select_from_table($con, $sql, $cond, $fname);
    if ($sel == 0) {
        pg_close($con);
        echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
        echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
        exit;
    }
    while ($row = pg_fetch_array($sel)) {
        $buildings[$row["bldg_cd"]]["wards"][$row["ward_cd"]] = $row["ward_name"];
    }

    // 選択病棟を保存
    if ($updbedcalopt == "1") {
        $sql = "delete from bedcalopt";
        $cond = "where emp_id = '$emp_id'";
        $del = delete_from_table($con, $sql, $cond, $fname);
        if ($del == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        if ($calendar_bldg_cd == "") $calendar_bldg_cd = null;
        if ($calendar_ward_cd == "") $calendar_ward_cd = null;

        $sql = "insert into bedcalopt (emp_id, bldg_cd, ward_cd) values (";
        $content = array($emp_id, $calendar_bldg_cd, $calendar_ward_cd);
        $ins = insert_into_table($con, $sql, $content, $fname);
        if ($ins == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

    // 選択病棟をロード
    } else {
        $sql = "select bldg_cd, ward_cd from bedcalopt";
        $cond = "where emp_id = '$emp_id'";
        $sel = select_from_table($con, $sql, $cond, $fname);
        if ($sel == 0) {
            pg_close($con);
            echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
            echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
            exit;
        }

        if (pg_num_rows($sel) > 0) {
            $calendar_bldg_cd = pg_fetch_result($sel, 0, "bldg_cd");
            $calendar_ward_cd = pg_fetch_result($sel, 0, "ward_cd");
        }
    }

    // 当該週のスタート日を求める（入退院カレンダー）
    $bedschd_start_day = get_start_day($bedschd_date, $start_wd);

    // 「前月」「前日」「翌日」「翌月」のタイムスタンプを取得
    $bedschd_last_month = get_last_month($bedschd_date);
    $bedschd_last_week = get_last_week($bedschd_date);
    $bedschd_next_week = get_next_week($bedschd_date);
    $bedschd_next_month = get_next_month($bedschd_date);
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!--bedinfo-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr height="22" bgcolor="#bdd1e7">
<td width="20" class="spacing"><a href="bed_menu.php?session=<?php echo($session); ?>">

<?php
if($font_size == "1"){
    echo "<img src='img/icon/s17.gif' width='20' height='20' border='0' alt='入退院カレンダー'></a></td>";
    echo "<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><a href='bed_menu.php?session={$session}'><b>入退院カレンダー</b></a></font></td>";
    echo "<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>";
} else {
    echo "<img src='img/icon/s17.gif' width='32' height='32' border='0' alt='入退院カレンダー'></a></td>";
    echo "<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'><a href='bed_menu.php?session={$session}'><b>入退院カレンダー</b></a></font></td>";
    echo "<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j16'>";
}
?>

事業所（棟）：<select name="calendar_bldg_cd" onchange="changeBuilding(this.options[this.selectedIndex].value);">
<option value="">すべて
<?php
foreach ($buildings as $tmp_bldg_cd => $tmp_building) {
    echo("<option value=\"$tmp_bldg_cd\"");
    if ($tmp_bldg_cd == $calendar_bldg_cd) {echo(" selected");}
    echo(">{$tmp_building["name"]}");
}
?>
</select>
病棟：<select name="calendar_ward_cd" onchange="changeWard(this.options[this.selectedIndex].value);">
<option value="">すべて
<?php
if ($calendar_bldg_cd != "") {
    foreach ($buildings[$calendar_bldg_cd]["wards"] as $tmp_ward_cd => $tmp_ward_name) {
        echo("<option value=\"$tmp_ward_cd\"");
        if ($tmp_ward_cd == $calendar_ward_cd) {echo(" selected");}
        echo(">$tmp_ward_name");
    }
}
?>
</select>
</font></td>
<td align="right"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><a href="left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=<?php echo($calendar_ward_cd); ?>&bedschd_date=<?php echo($bedschd_last_month); ?>&date=<?php echo($date1); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">&lt;&lt;前月</a>&nbsp;<a href="left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=<?php echo($calendar_ward_cd); ?>&bedschd_date=<?php echo($bedschd_last_week); ?>&date=<?php echo($date1); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">&lt;前週</a>&nbsp;<select onchange="changeDate2(this.options[this.selectedIndex].value);"><?php show_date_options_w($bedschd_date, $bedschd_start_day); ?></select>&nbsp;<a href="left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=<?php echo($calendar_ward_cd); ?>&bedschd_date=<?php echo($bedschd_next_week); ?>&date=<?php echo($date1); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">翌週&gt;</a>&nbsp;<a href="left.php?session=<?php echo($session); ?>&calendar_bldg_cd=<?php echo($calendar_bldg_cd); ?>&calendar_ward_cd=<?php echo($calendar_ward_cd); ?>&bedschd_date=<?php echo($bedschd_next_week); ?>&date=<?php echo($date1); ?>&task_limit=<?php echo($task_limit); ?>&news_limit=<?php echo($news_limit); ?>">翌月&gt;&gt;</a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<?php show_bed_schedule($con, $bedschd_start_day, $session, $fname, $calendar_bldg_cd, $calendar_ward_cd, $font_class); ?>
<script type="text/javascript">
    Event.domReady.add(function() {
        prepareDragDrop();
    });
</script>
<!--bedinfo-->
<?php } ?>
<?php
// WICレポート表示
if ($wic_flg == "t") {
    $wic_html = get_wic_html();
?>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<!--wic-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="block">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#bdd1e7">
<td width="20" height="22" class="spacing"><a href="http://www.wic-net.com/" target="_blank"><img src="img/icon/s31.gif" width="20" height="20" border="0" alt="WICレポート"></a></td>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><a href="http://www.wic-net.com/" target="_blank"><b>WICレポート</b></a></font></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="block" style="position:relative;top:-1px;">
<tr>
<td><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="<?=$font_class?>"><?php echo($wic_html); ?></font></td>
</tr>
</table>
<!--wic-->
<?php } ?>
</td>
</tr>
</table>
<img src="img/spacer.gif" width="1" height="5" alt=""><br>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/treeview/treeview-min.js"></script>
<script type="text/javascript" src="js/yui/build/event/event.js"></script>
<script type="text/javascript" src="js/yui/build/dom/dom-min.js"></script>
<script type="text/javascript" src="js/prototype/prototype-1.7.1.js"></script>
<script type="text/javascript" src="js/yui/3.10.3/build/yui/yui-min.js"></script>
<script type="text/javascript">
    function resizeIframeEx() {
    }
    Event.domReady.add(function() {
        resizeIframeEx = null;
        if (parent && parent.resizeIframe) {
	        parent.resizeIframe();
	    }
    });
</script>
</body>
</html>