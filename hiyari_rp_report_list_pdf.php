<?
ob_start();
require_once('about_comedix.php');
require_once("hiyari_rp_report_list_dl.ini");
require_once("hiyari_pdf_export.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
// 出力項目設定処理
//====================================
// トランザクションの開始
pg_query($con, "begin transaction");
$grp_item = set_excel_output_item($con, $fname, $emp_id, $use_grp_item_code);
// トランザクションをコミット
pg_query($con, "commit");

//====================================
// 項目マスタ取得
//====================================
if(count($use_grp_item_code) > 0){
$arr_grp_item = get_inci_easyinput_item_mst($con, $fname, $use_grp_item_code,$profile_number,$detail_number,$person_number);
	//$arr_grp_item = get_inci_easyinput_item_mst($con, $fname, $use_grp_item_code, false);
}

$arr_report_materials = array();
foreach($arr_grp_item as $grp_item){
	$grp_item_temp = array("grp_code" => $grp_item["grp_code"], "easy_item_code" => $grp_item["easy_item_code"], "easy_item_name" => $grp_item["easy_item_name"], "item_type" => $grp_item["item_type"]);

	if($grp_item["item_type"] == "select" || $grp_item["item_type"] == "checkbox" || $grp_item["item_type"] == "radio"){
		$easy = get_inci_report_materials($con, $fname, $grp_item["grp_code"], $grp_item["easy_item_code"]);
		$grp_item_temp["easy_info"] = $easy;
	}
	array_push($arr_report_materials, $grp_item_temp);
}

//====================================
// 出力用レポートＩＤ
//====================================
$arr_report_ids = split(",", $report_id_list);
$total_cnt = count($arr_report_ids);

if($max_report_dl_cnt != 0){
    $start_idx = $report_idx * $max_report_dl_cnt;

    if(($total_cnt - $start_idx) < $max_report_dl_cnt){
        $end_idx = $start_idx + $total_cnt - $start_idx;
    }
    else{
        $end_idx = ($report_idx + 1) * $max_report_dl_cnt;

    }
}
else{
    $start_idx = 0;
    $end_idx = $total_cnt;
}


$report_id_list = "";
for($i=$start_idx; $i<$end_idx; $i++){
    if($report_id_list != ""){
        $report_id_list .= ",";
    }

    $report_id_list .= $arr_report_ids[$i];
}

// GRM(SM)フラグ
$is_sm_emp_flg =  is_sm_emp($session, $fname, $con);

//==============================================================================
//PDF出力
//==============================================================================
// 報告書データ取得
$rpt_array = get_report_data($con, $fname, $report_id_list, $arr_report_materials, $use_grp_item_code, $is_sm_emp_flg);

//------------------------------------
//レポート毎の総ページ数取得
//（実際の出力と同じ処理をしながら総ページ数を取得する）
//------------------------------------
foreach($rpt_array as $key=>$rpt){
    $pdf = new hiyari_pdf_export();

    $pdf->AddMBFont(PGOTHIC, 'EUC-JP');
    $pdf->SetFont(PGOTHIC, '', 6);

    $pdf->SetPageTitle($rpt['page_title']);

    $pdf->Open();
    $pdf->AddPage();
    
    $rpt_array[$key]['num_pages'] = $pdf->Table($rpt['list']);

    ob_end_clean();
    $pdf->Close();
}

//------------------------------------
//レポート出力
//------------------------------------
$pdf = new hiyari_pdf_export();

$pdf->AddMBFont(PGOTHIC, 'EUC-JP');
$pdf->SetFont(PGOTHIC, '', 6);

//レポートごとに出力
foreach($rpt_array as $key=>$rpt){
    //ページの追加と初期化
    $pdf->SetPageTitle($rpt['page_title']); //ページ追加前にタイトルを設定
    $pdf->AddPage();                        //ページ追加（前ページのフッタと新ページのヘッダが出力される）
    $pdf->ResetPages($rpt['num_pages']);    //ページ追加後にページ番号初期化（前ページのフッタが出力された後に設定すること）

    //表の出力
    $pdf->Table($rpt['list']);
}

ob_end_clean();
$pdf->Output();
ob_end_flush();


//======================================================================================================================================================
//内部関数
//======================================================================================================================================================
function get_report_data($con, $fname, $report_id_list, $arr_report_materials, $use_grp_item_code, $is_sm_emp_flg = false)
{
    //報告書データ取得
    $arr_inci_report = get_report_list($con, $fname, $report_id_list, $arr_report_materials);
    if (count($arr_inci_report) == 0) return;

    // 時間の設定情報を取得
    $time_setting_flg = getTimeSetting($con, $PHP_SELF);

    // タイムラグの表示フラグ取得
    $disp_taimelag_flg = getDispTimelagSetting($con, $fname);

    
    //==========================================================================
    //報告書毎にデータを配列に保存
    //==========================================================================
    $rpt_array = array();
    $arr_report_id = split(",", $report_id_list);
    foreach($arr_report_id as $report_id) {
        $inci_report = $arr_inci_report[$report_id];
        $rpt_data = array();

        //======================================================================
        //ページタイトル用データ（事案番号と表題）
        //======================================================================
        $page_title = array();
        $page_title[] = '事案番号：'.$inci_report["report_no"];
        $page_title[] = h($inci_report["report_title"]);
        $rpt_data['page_title'] = $page_title;

        //======================================================================
        //項目データ
        //======================================================================
        $list = array();

        if ($inci_report["report_link_status"] == '1'){
            $list[] = make_item('報告書区分', '主報告');
        }
        else{
            $list[] = make_item('報告書区分', '副報告');
        }

        if(in_array(9000,$use_grp_item_code)) {
            $list[] = make_item('報告者氏名', $inci_report["registrant_name"]);
        }
        if(in_array(9100,$use_grp_item_code)) {
            $list[] = make_item('報告者職種', $inci_report["registrant_job"]);
        }
        if(in_array(9200,$use_grp_item_code)) {
            $list[] = make_item('報告部署', $inci_report["classes"]);
        }
        $list[] = make_item('報告日', $inci_report["registration_date"]);

        $arr_input_items = $inci_report["input_items"];

        $data = array();
        foreach($arr_input_items as $input_items) {
            $data[$input_items['grp_code'] . "_" . $input_items["easy_item_code"]] = $input_items['input_item'];
        }

        // タイムラグデータ表示
        if ( $is_sm_emp_flg == true && $disp_taimelag_flg === true ) {
            // タイムラグ表示
            $timelag_view = $data["105_75"];
            if ($timelag_view != ""){
                $timelag_view = getTimelagView($timelag_view);
            } else {
                $timelag_view = "発見時刻を正しく入力してください。";
            }
            $list[] = make_item('タイムラグ', $timelag_view);
        }

        // 発生時刻
        $occurrence_time_h = "";    // 発生時刻：時
        $occurrence_time_hm = "";   // 発生時刻：分
        if(in_array("100_60",$use_grp_item_code)
         ||in_array("100_65",$use_grp_item_code)) {
            $occurrence_time_h = $data["100_60"];
            if ($occurrence_time_h != ""){
                $occurrence_time_h .= "時";
            }
            $occurrence_time_hm = $data["100_65"];
            if ($occurrence_time_hm != ""){
                $occurrence_time_hm = $occurrence_time_h . $occurrence_time_hm . "分";
            }
        }

        //----------------------------------------------------------------------
        //アセスメント・患者の状態 checkbox項目のコード値を収集
        //----------------------------------------------------------------------
        $arr_code = array();
        $str_170_20 = null;
        $str_180_20 = null;
        $str_190_20 = null;

        // ミトンあり
        if(array_key_exists("160_10", $data)) {
            $arr_code["160"] = explode("\t", $data["160_10"]);
        }
        // リスク回避用具
        if(array_key_exists("170_10", $data)) {
            $arr_code["170"] = explode("\t", $data["170_10"]);
        }
        if(array_key_exists("170_20", $data)) {
            $str_170_20 =  $data["170_20"];
        }
        // 拘束用具
        if(array_key_exists("180_10", $data)) {
            $arr_code["180"] = explode("\t", $data["180_10"]);
        }
        if(array_key_exists("180_20", $data)) {
            $str_180_20 =  $data["180_20"];
        }
        // 身体拘束行為
        if(array_key_exists("190_10", $data)) {
            $arr_code["190"] = explode("\t", $data["190_10"]);
        }
        if(array_key_exists("190_20", $data)) {
            $str_190_20 =  $data["190_20"];
        }

        //----------------------------------------------------------------------
        //アセスメント・患者の状態 checkbox項目のコード値があった場合、対応する文言を収集
        //----------------------------------------------------------------------
        $arr_name = array();
        $str_160 = null;
        $str_170 = null;
        $str_180 = null;
        $str_190 = null;
        if (!empty($arr_code)) {
            foreach($arr_report_materials as $report_materials) {
                switch ($report_materials["grp_code"]) {
                    // ミトンあり
                    case "160":
                        if (!empty($arr_code["160"])) {
                            foreach ($arr_code["160"] as $code) {
                                $arr_name["160"][] = $report_materials["easy_info"][$code];
                            }
                        }
                        break;
                    // リスク回避用具
                    case "170":
                        if ($report_materials["easy_item_code"] == 10) {
                            if (!empty($arr_code["170"])) {
                                foreach ($arr_code["170"] as $code) {
                                    if ($code != 99) {
                                        $arr_name["170"][] = $report_materials["easy_info"][$code];
                                    }
                                    else {
                                        $arr_name["170"][] = $report_materials["easy_info"][$code]. ":$str_170_20";
                                    }
                                }
                            }
                        }
                        break;
                    // 拘束用具
                    case "180":
                        if ($report_materials["easy_item_code"] == 10) {
                            if (!empty($arr_code["180"])) {
                                foreach ($arr_code["180"] as $code) {
                                    if ($code != 99) {
                                        $arr_name["180"][] = $report_materials["easy_info"][$code];
                                    }
                                    else {
                                        $arr_name["180"][] = $report_materials["easy_info"][$code]. ":$str_180_20";
                                    }
                                }
                            }
                        }
                        break;
                    // 身体拘束行為
                    case "190":
                        if ($report_materials["easy_item_code"] == 10) {
                            if (!empty($arr_code["190"])) {
                                foreach ($arr_code["190"] as $code) {
                                    if ($code != 99) {
                                        $arr_name["190"][] = $report_materials["easy_info"][$code];
                                    }
                                    else {
                                        $arr_name["190"][] = $report_materials["easy_info"][$code]. ":$str_190_20";
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            $str_160 = implode(",", $arr_name["160"]);
            $str_170 = implode(",", $arr_name["170"]);
            $str_180 = implode(",", $arr_name["180"]);
            $str_190 = implode(",", $arr_name["190"]);
        }

        //----------------------------------------------------------------------
        //項目データ
        //----------------------------------------------------------------------
        $group_data = array();
        $group_name = '';
        foreach($arr_report_materials as $report_materials) {
            //対象外の項目
            if (is_exclude_item($report_materials, $use_grp_item_code)) continue;

            $grp_item_code = $report_materials["grp_code"] . "_" . $report_materials["easy_item_code"];

            //発見時刻は、設定によって対象外
            if ( $grp_item_code == "105_60"
              || $grp_item_code == "105_65") {
                if ( $time_setting_flg === true ){
                    continue;
                }
            }

            // 詳細選択項目
            if ( $report_materials["grp_code"] == "100"	// 発生日時
              || $report_materials["grp_code"] == "110"	// 発生場所
              || $report_materials["grp_code"] == "210"	// 患者プロフィール
              || $report_materials["grp_code"] == "620" // 改善策
              || $report_materials['grp_code'] >= 900 && $report_materials['grp_code'] <= 990) {	// 概要・場面・内容（2010年改訂）
            	if (!in_array($grp_item_code, $use_grp_item_code)) {
                    continue;		// 非選択
            	}
            }

            $input_item = $data[$grp_item_code];
            $individual_flg = false;

            //----------------------------------------------------------
            //選択肢
            //----------------------------------------------------------
            if(($report_materials["item_type"] == "select" || $report_materials["item_type"] == "checkbox" || $report_materials["item_type"] == "radio")) {
                if (!($report_materials['grp_code'] >= 900 && $report_materials['grp_code'] <= 990
                    || $report_materials['grp_code'] == 1120
                    || $report_materials['grp_code'] == 1110
                    || $report_materials['grp_code'] == 1100)){
                    $input_item_temp = "";
                    $arr_easy_info = $report_materials["easy_info"];
                    $arr_input_val = split("\t", $input_item);
                    foreach($arr_input_val as $input_val) {
                        if($input_item_temp != "") {
                            $input_item_temp .= " ";
                        }
                        $input_item_temp .= $arr_easy_info[$input_val];
                    }
                    $input_item = $input_item_temp;
                }
            }

            //ルート関連ミトンあり
            if ($grp_item_code == "150_20" && $str_160 != "") {
                $input_item .= "(".$str_160.")";
            }
            //リスク回避用具
            if ($grp_item_code == "150_6" && $str_170 != "") {
                $input_item .= "(".$str_170.")";
            }
            //拘束用具
            if ($grp_item_code == "150_7" && $str_180 != "") {
                $input_item .= "(".$str_180.")";
            }
            //身体拘束行為
            if ($grp_item_code == "150_8" && $str_190 != "") {
                $input_item .= "(".$str_190.")";
            }

            // 発生日時
            if ( $report_materials["grp_code"] == "100") {
                if ($grp_item_code == "100_50") {  // いつごろ
                    $easy_item_name = "いつごろ";
                }
                else if ($grp_item_code == "100_60") {  // 発生時刻：時
                    $easy_item_name = "発生時刻";
                    $input_item = $occurrence_time_h;
                }
                else if ($grp_item_code == "100_65") {  // 発生時刻：時と分
                    $easy_item_name = "発生時刻";
                    $input_item = $occurrence_time_hm;
                }
                else {
                    $easy_item_name = $report_materials["easy_item_name"];
                }
                if ($input_item != ""){
                    $list[] = make_item($easy_item_name, $input_item);
                }
                $individual_flg = true;
            }

            // 発生場所
            else if ( $report_materials["grp_code"] == "110") {
                if ($input_item != ""){
                    $list[] = make_item($report_materials["easy_item_name"], $input_item);
                }

                if ($grp_item_code == "110_60") {
                    // 発生場所その他
                    $input_item = $data['110_70'];
                    if($input_item != "") {
                        $easy_item_name = "発生場所その他";
                        foreach($arr_report_materials as $report_materials2) {
                            $grp_item_code2 = $report_materials2["grp_code"] . "_" . $report_materials2["easy_item_code"];
                            if ($grp_item_code2 == "110_70"){    // 発生場所その他
                                $easy_item_name = $report_materials2["easy_item_name"];
                                break;
                            }
                        }
                        $list[] = make_item($easy_item_name, $input_item);
                    }
                }
                $individual_flg = true;
            }

            // 概要・場面・内容（2010年改訂）
            // 概要
            else if($report_materials['grp_code'] == "900") {
                if ($grp_item_code == "900_10") {
                    if($input_item != "") {
                        $input_item = get_super_item_name_by_id_2010($con, $fname, $input_item);
                        $list[] = make_item('概要', $input_item);
                    }

                    // 概要 その他
                    $input_item = $data['900_20'];
                    if($input_item != "") {
                        $list[] = make_item('その他', $input_item);
                    }
                }
                $individual_flg = true;
            }
            // 種類
            else if($report_materials['grp_code'] == "910") {
                if($input_item != "") {
                    $input_item = get_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('種類', $input_item);
                }
                $individual_flg = true;
            }
            // 種類の項目
            else if($report_materials['grp_code'] == "920") {
                if($input_item != "") {
                    $input_item = get_sub_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('項目', $input_item);
                }
                $individual_flg = true;
            }
            // 発生場面
            else if($report_materials['grp_code'] == "940") {
                if($input_item != "") {
                    $input_item = get_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('発生場面', $input_item);
                }
                $individual_flg = true;
            }
            // 発生場面の項目
            else if($report_materials['grp_code'] == "950") {
                if($input_item != "") {
                    $input_item = get_sub_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('項目', $input_item);
                }
                $individual_flg = true;
            }
            // 事例の内容
            else if($report_materials['grp_code'] == "970") {
                if($input_item != "") {
                    $input_item = get_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('事例の内容', $input_item);
                }
                $individual_flg = true;
            }
            // 事例の内容の項目
            else if($report_materials['grp_code'] == "980") {
                if($input_item != "") {
                    $input_item = get_sub_item_by_name_2010($con, $fname, $input_item);
                    $list[] = make_item('項目', $input_item);
                }
                $individual_flg = true;
            }

            //----------------------------------------------------------
            //影響区分・対応区分
            //----------------------------------------------------------
            else if( $grp_item_code == "1400_10"
                  || $grp_item_code == "1410_10" ) {
                $input_item = get_ic_super_item_by_id($con, $fname, $input_item);
            }
            //----------------------------------------------------------
            //患者への影響・患者への精神的影響・対応
            //----------------------------------------------------------
            else if ( $grp_item_code == "1400_30"
                   || $grp_item_code == "1400_60"
                   || $grp_item_code == "1410_20" ) {
                unset($input_item_arr);
                $input_items = preg_split("/[\t]+/", $input_item);
                foreach($input_items as $inupt_item_single) {
                    $input_item_arr[] = get_ic_item_by_id($con, $fname, $inupt_item_single);
                }
                $input_item = implode(" ", $input_item_arr);
            }
            //----------------------------------------------------------
            //対応の詳細
            //----------------------------------------------------------
            else if($grp_item_code == "1410_30") {
                unset($input_item_arr);
                $input_items = preg_split("/[\t]+/", $input_item);
                foreach($input_items as $inupt_item_single) {
                    $input_item_arr[] = get_ic_sub_item_by_id($con, $fname, $inupt_item_single);
                }
                $input_item = implode(" ", $input_item_arr);
            }

            //グループデータ
            $old_group_name = $group_name;
            $group_name = '';
            if ($report_materials['grp_code'] >= 310 && $report_materials['grp_code'] <= 399){//関連する患者情報
                $group_name = '関連する患者情報';
                $grp_no = $report_materials['grp_code']%10 + 1;
            }
            else if ($report_materials['grp_code'] >= 3050 && $report_materials['grp_code'] <= 3459){//当事者
                $group_name = '当事者';
                $grp_no = $report_materials['grp_code']%10 + 1;

            }
            else if($report_materials['grp_code'] >= 4000 && $report_materials['grp_code'] <= 4049){//事例の詳細(時系列)
                $group_name = '事例の詳細(時系列)';
                $grp_no = $report_materials['grp_code']%100 + 1;
            }

            //グループの終了時にグループデータをリストに追加
            if ($old_group_name != '' && $group_name != $old_group_name){
                foreach ($group_data as $no => $gd){
                    $list[] = make_item($old_group_name.$no, '', true);

                    foreach($gd as $gitem){
                        $list[] = $gitem;
                    }
                }
                if ($group_name == ''){
                    $list[]['end_of_group'] = true;
                }
                unset($group_data);
            }

            if (!$individual_flg && !empty($input_item)) {
                $item = make_item($report_materials["easy_item_name"], $input_item);

                if ($group_name!=''){
                    //グループデータの場合、一旦別の配列にまとめる
                    $group_data[$grp_no][] = $item;
                }
                else{
                    $list[] = $item;
                }
            }
        }

        //グループデータがあれば、リストに追加
        foreach ($group_data as $no => $gd){
            $list[] = make_item($group_name.$no, '', true);

            foreach($gd as $gitem){
                $list[] = $gitem;
            }
        }

        $rpt_data['list'] = $list;
        $rpt_array[] = $rpt_data;
    }

    return $rpt_array;
}

/**
 * アイテム作成
 *
 * @param string $title アイテムのタイトル
 * @param string $contents アイテムの内容
 * @param boolean $is_group_title グループタイトルフラグ
 * @return array アイテム
 */
function make_item($title, $contents, $is_group_title=false)
{
    $item = array();
    $item['is_group_title'] = $is_group_title;
    $item['title'] = h($title);
    $item['contents'] = h($contents);
    return $item;
}

/**
 * 除外アイテム判定
 *
 * @param array $report_materials
 * @param array $use_grp_item_code
 * @return boolean true=除外アイテム
 */
function is_exclude_item($report_materials, $use_grp_item_code)
{
     $grp_item_code = $report_materials["grp_code"] . "_" . $report_materials["easy_item_code"];

    // 発生月、発生曜日、曜日区分は表示しない
    if ($grp_item_code == "100_10" || $grp_item_code == "100_20" || $grp_item_code == "100_30") {
        return true;
    }

    // 発生場所その他
    if ($grp_item_code == "100_70") {
        return true;
    }

    // タイムラグ区分は表示しない
    if ($grp_item_code == "105_70" || $grp_item_code == "105_75") {
        return true;
    }

    //転倒・転落アセスメントスコア
    //離床センサー
    //リスク回避用具
    //拘束用具
    //身体拘束行為
    //転倒・転落の危険度
    //ルート関連（自己抜去）
    //日常生活機能評価
    //日常生活自立度(認知症高齢者)
    //日常生活自立度(寝たきり度)
    //機能的自立度評価法(FIM)
    //バーサルインデックス
    //皮膚トラブルレベル
    if ($report_materials["grp_code"] == "150" && !in_array($grp_item_code, $use_grp_item_code)) {
        return true;
    }

    //ミトンあり(160) ~  身体拘束行為(190)
    if (in_array($report_materials["grp_code"], array(160,170,180,190))) {
        return true;
    }

    // インシデント直前の患者の状態の詳細が空だったら表示しない
    if ($report_materials["grp_code"] == "295" && empty($report_materials["easy_info"])) {
        return true;
    }

    // 概要・場面・内容（2010年改訂） その他
    if ($grp_item_code == "900_20"
       || $grp_item_code == "930_10"
       || $grp_item_code == "960_10"
       || $grp_item_code == "990_10") {
        return true;
    }
    return false;
}
