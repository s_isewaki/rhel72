<?
ob_start();
require_once("about_session.php");
require_once("about_postgres.php");
require_once("hiyari_common.ini");
require_once("hiyari_rp_report_list_dl.ini");
require_once("Cmx.php");
require_once("aclg_set.php");

//====================================
//画面名
//====================================
$fname = $PHP_SELF;

//====================================
//セッションのチェック
//====================================
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

//====================================
//DBコネクション取得
//====================================
$con = connect2db($fname);
if($con == "0"){
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}

$filename = 'report.csv';

// アクセスログ（ファイル名を「内容」項目に表示させる）
aclg_regist($session, $fname, $_SERVER["HTTP_REFERER"], $_SERVER["HTTP_USER_AGENT"],$_POST,$con,$filename);

//====================================
// EXCEL出力項目設定処理
//====================================
// トランザクションの開始
pg_query($con, "begin transaction");
$grp_item = set_excel_output_item($con, $fname, $emp_id, $use_grp_item_code);
// トランザクションをコミット
pg_query($con, "commit");


//====================================
// 項目マスタ取得
//====================================
if(count($use_grp_item_code) > 0)
{

	$arr_grp_item = get_inci_easyinput_item_mst($con, $fname, $use_grp_item_code,$profile_number,$detail_number,$person_number);
}

$arr_report_materials = array();
foreach($arr_grp_item as $grp_item)
{

	$grp_item_temp = array("grp_code" => $grp_item["grp_code"], "easy_item_code" => $grp_item["easy_item_code"], "easy_item_name" => $grp_item["easy_item_name"], "item_type" => $grp_item["item_type"]);

	if($grp_item["item_type"] == "select" || $grp_item["item_type"] == "checkbox" || $grp_item["item_type"] == "radio")
	{
		$easy = get_inci_report_materials($con, $fname, $grp_item["grp_code"], $grp_item["easy_item_code"]);
		$grp_item_temp["easy_info"] = $easy;
	}
	array_push($arr_report_materials, $grp_item_temp);
}

//====================================
// 出力用レポートＩＤ
//====================================
$arr_report_ids = split(",", $report_id_list);
$total_cnt = count($arr_report_ids);

if($max_report_dl_cnt != 0)
{
	$start_idx = $report_idx * $max_report_dl_cnt;
	
	if(($total_cnt - $start_idx) < $max_report_dl_cnt)
	{
		$end_idx = $start_idx + $total_cnt - $start_idx;
	}
	else
	{
		$end_idx = ($report_idx + 1) * $max_report_dl_cnt;
	
	}
}
else
{
	$start_idx = 0;
	$end_idx = $total_cnt;
}


$report_id_list = "";
for($i=$start_idx; $i<$end_idx; $i++)
{
	if($report_id_list != "")
	{
		$report_id_list .= ",";
	}

	$report_id_list .= $arr_report_ids[$i];
}

// GRM(SM)フラグ
$is_sm_emp_flg =  is_sm_emp($session, $fname, $con);

//====================================
// EXCEL出力データ(table形式)取得
//====================================
ini_set("max_execution_time",1800);
ini_set("memory_limit", "256M");
$download_data = get_csv_data($con, $fname, $report_id_list, $arr_report_materials, $use_grp_item_code, $is_sm_emp_flg);


//====================================
//ファイル名
//====================================
if (ereg(' MSIE ', $_SERVER["HTTP_USER_AGENT"])) {
    $encoding = 'sjis';
}
else {
    $encoding = mb_http_output();   // Firefox
}

$filename = mb_convert_encoding(
                $filename,
                $encoding,
                mb_internal_encoding());

//====================================
//統計分析データEXCEL出力
//====================================
ob_clean();
header("Content-Type: text/csv");
header('Content-Disposition: attachment; filename=' . $filename);
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0,pre-check=0');
header('Pragma: public');
echo mb_convert_encoding($download_data, 'sjis', mb_internal_encoding());
ob_end_flush();
pg_close($con);
?>

