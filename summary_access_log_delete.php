<? ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<?
ini_set("max_execution_time", 0);

$fname = $_SERVER["PHP_SELF"];
require("about_authority.php");
require("about_session.php");
require("get_values.php");
require("show_sinryo_top_header.ini");
require("label_by_profile_type.ini");
require_once("sot_util.php");
require("summary_common.ini");

$initerror = true;
for (;;){
	//セッションのチェック
	$session = qualify_session($session,$fname);
	if(!$session) break;
	// 役職登録権限チェック
	$checkauth = check_authority($session,59,$fname);
	if(!$checkauth) break;
	//DB接続
	$con = connect2db($fname);
	if(!$con) break;
	$initerror = false;
	break;
}
// エラー時は終了
if ($initerror){
	ob_end_flush();
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo $c_sot_util->javascript("showLoginPage(window);");
	exit;
}

if (@$_REQUEST["mode"] == "delete"){
	$mon = (int)$_REQUEST["delete_month_term"];
	if ($mon >= 1){
		pg_query($con, "begin");

		$day_limit = date("Ymd", strtotime(date("Y-m-d")." -".$mon." month"));
		$del = delete_from_table($con, "delete from oplog where op_time < '{$day_limit}000000' and optype_id between 200 and 299", "", $fname);
		if ($del == 0) {
			pg_query($con, "rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}

		pg_query($con, "commit");
	}
}

$font = '<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">';




// 診療科権限を取得
$entity_admin_auth = check_authority($session, 28, $fname);

// DBコネクションの作成
$con = connect2db($fname);
// 組織タイプを取得
$profile_type = get_profile_type($con, $fname);

// 診療区分/記録区分
$summary_kubun_title = $_label_by_profile["SUMMARY_KUBUN"][$profile_type];

// メドレポート/ＰＯレポート(Problem Oriented)
$med_report_title = $_label_by_profile["MED_REPORT"][$profile_type];

// 診療科/担当部門
$section_title = $_label_by_profile["SECTION"][$profile_type];
?>
<title>CoMedix <? echo($med_report_title); ?>管理 | オプション</title>


<script type="text/javascript" src="./js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td { border:#5279a5 solid 1px; text-align:center }
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<? show_sinryo_top_header($session,$fname,"KANRI_ACCESS_LOG"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="120" valign="top">
<table width="120" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
<td width="118" valign="top">

<?= summary_common_show_admin_left_menu($session, $entity_admin_auth, $section_title, $summary_kubun_title); ?>

</td>
<td width="1" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
<tr>
<td colspan="3" bgcolor="#5279A5"><img src="img/spacer.gif" width="1" height="1" alt=""></td>
</tr>
</table>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->






<!-- right -->
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./summary_access_log_daily.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">日別一覧</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="./summary_access_log_find.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">検索</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#5279a5"><a href="./summary_access_log_delete.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>削除</b></font></a></td>
<td></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr></table>


<img src="img/spacer.gif" alt="" width="1" height="3"><br>

<div style="padding:4px;"><input type="button" value="12ヶ月以前のログを削除する"  onclick="dellog(12)"></div>
<div style="padding:4px;"><input type="button" value="９ヶ月以前のログを削除する"  onclick="dellog(9)"></div>
<div style="padding:4px;"><input type="button" value="６ヶ月以前のログを削除する"  onclick="dellog(6)"></div>
<div style="padding:4px;"><input type="button" value="３ヶ月以前のログを削除する"  onclick="dellog(3)"></div>
<div style="padding:4px;"><input type="button" value="２ヶ月以前のログを削除する"  onclick="dellog(2)"></div>
<div style="padding:4px;"><input type="button" value="１ヶ月以前のログを削除する"  onclick="dellog(1)"></div>


<form action="summary_access_log_delete.php" method="get" name="frm">
	<input type="hidden" name="session" value="<?=$session ?>">
	<input type="hidden" name="mode" value="delete">
	<input type="hidden" name="delete_month_term" value="" id="delete_month_term">

</form>
<script type="text/javascript">
	function dellog(mon){
		if(!confirm(mon+"ヶ月以前のログを完全に抹消します。よろしいですか？")) return;
		document.getElementById("delete_month_term").value = mon;
		document.frm.submit();
	}
</script>



</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
