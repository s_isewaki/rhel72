<?
require_once("about_comedix.php");
require_once("kango_common.ini");
require_once("show_select_values.ini");


//==============================
//前処理
//==============================

// ページ名
$fname = $_SERVER["PHP_SELF"];

// セッションのチェック
$session = qualify_session($_REQUEST["session"], $fname);
if ($session == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限チェック
$checkauth = check_authority($session, $AUTH_NO_KANGO_KANRI, $fname);
if($checkauth == "0")
{
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// パラメータ
$is_postback = isset($_REQUEST["is_postback"]) ? $_REQUEST["is_postback"] : null;
$eval_form_id = isset($_REQUEST["eval_form_id"]) ? $_REQUEST["eval_form_id"] : null;
$disp_form_no = isset($_REQUEST["disp_form_no"]) ? $_REQUEST["disp_form_no"] : null;

//==============================
//DBコネクション取得
//==============================
$con = connect2db($fname);
if($con == "0")
{
	echo("<script type='text/javascript' src='./js/showpage.js'></script>");
	echo("<script language='javascript'>showLoginPage(window);</script>");
	exit;
}


//==============================
//ポストバック時の処理
//==============================
if($is_postback == "true")
{
	//==============================
	//送信パラメータの解析
	//==============================
	
	$start_date = $start_date_y.$start_date_m.$start_date_d;
	
	
	
	//==============================
	//更新の場合
	//==============================
	if($postback_mode == "update")
	{
		//==============================
		//トランザクション開始
		//==============================
		pg_query($con,"begin transaction");
		$has_error = false;
		
		//==============================
		//更新日付で追加か更新かを判定する。
		//==============================
		$sql = "select eval_form_id from nlcs_eval_form where eval_form_type = '{$eval_form_type}' and start_date = '{$start_date}'";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$is_insert = (pg_num_rows($sel) == 0);
		if(!$is_insert)
		{
			$target_eval_form_id = $eval_form_id;//必ず自分を更新対象(JavaScriptで判定済み)
		}
		
		//==============================
		//新規登録の場合
		//==============================
		if($is_insert)
		{
			//==============================
			//評価票IDを採番
			//==============================
			$sql = "select max(eval_form_id)+1 as new_eval_from_id from nlcs_eval_form";
			$sel = select_from_table($con, $sql, "", $fname);
			if ($sel == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			$new_eval_form_id = pg_fetch_result($sel,0,'new_eval_from_id');
			
			//==============================
			//評価票情報を登録
			//==============================
			$sql  = " insert into nlcs_eval_form(eval_form_id,eval_form_type,start_date,default_flg,delete_flg)";
			$sql .= " select";
			$sql .= " {$new_eval_form_id} as eval_form_id,";
			$sql .= " eval_form_type,";
			$sql .= " '{$start_date}' as start_date,";
			$sql .= " false as default_flg,";
			$sql .= " false as delete_flg";
			$sql .= " from nlcs_eval_form where eval_form_id = {$eval_form_id}";
			$result = pg_exec_with_log($con, $sql, $fname);
			if ($result == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//==============================
			//評価票項目情報をコピー　※後で変更点を反映する。
			//==============================
			$sql  = " insert into nlcs_eval_item(eval_form_id,eval_item_id,eval_item_name,disp_index,default_flg,delete_flg)";
			$sql .= " select";
			$sql .= " {$new_eval_form_id} as eval_form_id,";
			$sql .= " eval_item_id,";
			$sql .= " eval_item_name,";
			$sql .= " disp_index,";
			$sql .= " default_flg,";
			$sql .= " delete_flg";
			$sql .= " from nlcs_eval_item where eval_form_id = {$eval_form_id}";
			$result = pg_exec_with_log($con, $sql, $fname);
			if ($result == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//==============================
			//評価票カテゴリ情報をコピー
			//==============================
			$sql  = " insert into nlcs_eval_cate(eval_form_id,eval_item_id,eval_cate_no,eval_cate_name,eval_point1,eval_point2,eval_point3,use_flg)";
			$sql .= " select";
			$sql .= " {$new_eval_form_id} as eval_form_id,";
			$sql .= " eval_item_id,";
			$sql .= " eval_cate_no,";
			$sql .= " eval_cate_name,";
			$sql .= " eval_point1,";
			$sql .= " eval_point2,";
			$sql .= " eval_point3,";
			$sql .= " use_flg";
			$sql .= " from nlcs_eval_cate where eval_form_id = {$eval_form_id}";
			$result = pg_exec_with_log($con, $sql, $fname);
			if ($result == 0)
			{
				pg_query($con,"rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
			
			//更新対象とする。
			$target_eval_form_id = $new_eval_form_id;
		}
		
		//==============================
		//評価票項目情報を更新
		//==============================
		
		//更新対象の評価票項目IDを取得
		$sql = "select eval_item_id from nlcs_eval_item where eval_form_id = {$target_eval_form_id} and not delete_flg";
		$sel = select_from_table($con, $sql, "", $fname);
		if ($sel == 0)
		{
			pg_query($con,"rollback");
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		$itmes = pg_fetch_all($sel);
		
		//各評価表項目に対して
		foreach($itmes as $item1)
		{
			//送信パラメータ解析
			$tmp_item_id = $item1['eval_item_id'];
			$p_disp_index = "item{$tmp_item_id}_disp_index";
			$tmp_disp_index = $$p_disp_index;
			
			if (!preg_match("/^[0-9]*$/", $tmp_disp_index) )
			{
				$error_msg ="表示順は数値を入力してください。";
				$has_error = true;
				break;
			}
			
			
			//更新処理
			$sql = "update nlcs_eval_item set";
			$set = array("disp_index");
			$setvalue = array($tmp_disp_index);
			$cond = "where eval_form_id = {$target_eval_form_id} and eval_item_id = {$tmp_item_id}";
			$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
			if ($upd == 0)
			{
				pg_query($con, "rollback");
				pg_close($con);
				echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
				echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
				exit;
			}
		}
		
		//==============================
		//コミット
		//==============================
		if($has_error)
		{
			pg_query($con, "rollback");
			
			//エラー表示
			echo("<script language='javascript'>");
			echo("alert('$error_msg');");
			echo("</script>");
		}
		else
		{
			pg_query($con, "commit");
		}
		
		
		//作成した評価票を表示対象に。
		$eval_form_id = $target_eval_form_id;
	}
	//==============================
	//削除の場合
	//==============================
	else if($postback_mode == "delete")
	{
		//==============================
		//論理削除
		//==============================
		$sql = "update nlcs_eval_form set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where eval_form_id = {$eval_form_id} and not default_flg"; //デフォルトは削除不可能(JavaScriptで判定済み)
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
		
		//==============================
		//再表示するフォームは初期仕様に従う
		//==============================
		$eval_form_id = "";
	}
	//==============================
	//評価項目の削除の場合
	//==============================
	else if($postback_mode == "item_delete")
	{
		//==============================
		//評価項目の論理削除
		//==============================
		$sql = "update nlcs_eval_item set";
		$set = array("delete_flg");
		$setvalue = array("t");
		$cond = "where eval_form_id = {$eval_form_id} and eval_item_id = {$item_delete_target_eval_item_id}";
		$upd = update_set_table($con, $sql, $set, $setvalue, $cond, $fname);
		if ($upd == 0)
		{
			pg_close($con);
			echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
			echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
			exit;
		}
	}
}









//==============================
//評価票一覧情報取得
//==============================
$form_list = null;
$sql = "select * from nlcs_eval_form where not delete_flg and eval_form_type = 'A' order by start_date";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$form_list['A'] = pg_fetch_all($sel);

$sql = "select * from nlcs_eval_form where not delete_flg and eval_form_type = 'B' order by start_date";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$form_list['B'] = pg_fetch_all($sel);

//==============================
//現在使用中の評価票の特定
//==============================

$ab_list = array('A','B');

$now_use_eval_form_id = null;
foreach($ab_list as $ab)
{
	foreach($form_list[$ab] as $tmp_form_info)
	{
		$tmp_eval_form_id = $tmp_form_info['eval_form_id'];
		$tmp_start_date = $tmp_form_info['start_date'];
		if($tmp_start_date <= date("Ymd"))
		{
			$now_use_eval_form_id[$ab] = $tmp_eval_form_id;
		}
	}
}

//==============================
//表示する評価票の特定
//==============================

//評価票が未選択の場合は先頭の評価票を表示
if($eval_form_id == "")
{
	$eval_form_id = $now_use_eval_form_id['A'];
}


//==============================
//表示対象の評価票情報を取得
//==============================


$form_info = null;
foreach($form_list as $form_ab_list)
{
	foreach($form_ab_list as $tmp_form_info)
	{
		if($tmp_form_info['eval_form_id'] == $eval_form_id)
		{
			$form_info = $tmp_form_info;
			break;
		}
	}
	if($form_info != null)
	{
		break;
	}
}
$eval_form_type = $form_info['eval_form_type'];
$start_date = $form_info['start_date'];
$default_flg = $form_info['default_flg'];

$start_date_y = substr($start_date,0,4);
$start_date_m = substr($start_date,4,2);
$start_date_d = substr($start_date,6,2);

$eval_form_type_name = get_eval_form_type_name($eval_form_type);
$eval_form_no_name = get_evel_form_no_name($disp_form_no);

//==============================
//評価票項目一覧情報の取得
//==============================

$sql  = " select * from";
$sql .= " (select * from nlcs_eval_item where not delete_flg and eval_form_id = {$eval_form_id}) items";
$sql .= " natural left join";
$sql .= " (select eval_form_id, eval_item_id, eval_cate_name as c1name, eval_point1 as c1p1, eval_point2 as c1p2, eval_point3 as c1p3 from nlcs_eval_cate where use_flg and eval_form_id = {$eval_form_id} and eval_cate_no = 1) cate1";
$sql .= " natural left join";
$sql .= " (select eval_form_id, eval_item_id, eval_cate_name as c2name, eval_point1 as c2p1, eval_point2 as c2p2, eval_point3 as c2p3 from nlcs_eval_cate where use_flg and eval_form_id = {$eval_form_id} and eval_cate_no = 2) cate2";
$sql .= " natural left join";
$sql .= " (select eval_form_id, eval_item_id, eval_cate_name as c3name, eval_point1 as c3p1, eval_point2 as c3p2, eval_point3 as c3p3 from nlcs_eval_cate where use_flg and eval_form_id = {$eval_form_id} and eval_cate_no = 3) cate3";
$sql .= " natural left join";
$sql .= " (select eval_form_id, eval_item_id, eval_cate_name as c4name, eval_point1 as c4p1, eval_point2 as c4p2, eval_point3 as c4p3 from nlcs_eval_cate where use_flg and eval_form_id = {$eval_form_id} and eval_cate_no = 4) cate4";
$sql .= " natural left join";
$sql .= " (select eval_form_id, eval_item_id, eval_cate_name as c5name, eval_point1 as c5p1, eval_point2 as c5p2, eval_point3 as c5p3 from nlcs_eval_cate where use_flg and eval_form_id = {$eval_form_id} and eval_cate_no = 5) cate5";
$sql .= " order by disp_index,eval_item_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$item_list = pg_fetch_all($sel);


//==============================
//評価票使用済み判定(論理削除済みの評価記録は除外する。)
//==============================
//使用済みの場合、以下が不可能。
//・評価票の削除
//・項目の削除(どうしても削除が必要場合は、使用している記録データから該当項目を除去し、レベル等の再計算処理を行う必要あり。)
//・項目の追加(どうしても追加が必要場合は、使用している記録を全て確定から保留に変更し、レベル等の再計算処理を行う必要あり。勝手に患者達の状態を決められないため、追加項目を記入・確定してもらう必要あり。)

$sql  = " select count(*) as use_count from";
$sql .= " (";
$sql .= " select distinct eval_data_id from nlcs_eval_record_data where eval_form_id = {$eval_form_id}";
$sql .= " ) form_use_data";
$sql .= " natural inner join";
$sql .= " (";
$sql .= " select eval_data_id from nlcs_eval_record where not delete_flg";
$sql .= " ) enable_data_id";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$is_used_eval_form = (pg_fetch_result($sel,0,'use_count') > 0);



//==============================
//評価記録の最終記録日を取得
//==============================

$sql = "select max(input_date) as input_date from nlcs_eval_record where not delete_flg";
$sel = select_from_table($con, $sql, "", $fname);
if ($sel == 0)
{
	pg_query($con,"rollback");
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}
$record_max_input_date = pg_fetch_result($sel,0,'input_date');
$record_max_input_date_disp = substr($record_max_input_date,0,4).'年'.substr($record_max_input_date,4,2).'月'.substr($record_max_input_date,6,2).'日';

//==============================
//その他
//==============================

//画面名
$PAGE_TITLE = "評価票作成";

//========================================================================================================================================================================================================
// HTML出力
//========================================================================================================================================================================================================
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix <?=$KANGO_TITLE?> | <?=$PAGE_TITLE?></title>


<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript">


function initPage()
{
	start_date_seigyo();
}

//適用開始日より各実効ボタンを制御します。
function start_date_seigyo()
{
	//現在の適用開始日の値
	var y = document.mainform.start_date_y.options[document.mainform.start_date_y.selectedIndex].value;
	var m = document.mainform.start_date_m.options[document.mainform.start_date_m.selectedIndex].value;
	var d = document.mainform.start_date_d.options[document.mainform.start_date_d.selectedIndex].value;
	var ymd = "" + y + m + d;

	//登録済みの適用開始日はＮＧ
	var default_start_date_flg = (ymd == '20080401');
	var self_start_date_flg = (ymd == '<?=$start_date?>');
	var use_start_date_flg = false;
	<?
	foreach($form_list[$eval_form_type] as $tmp_form_info)
	{
		?>
		if(ymd == <?=$tmp_form_info['start_date']?>){use_start_date_flg = true;}
		<?
	}
	?>
	if(use_start_date_flg && !self_start_date_flg)
	{
		alert("この適用開始日の評価票は登録済みです。");
		
		//日付を初期化
		set_select_ctl_from_value(document.mainform.start_date_y,'<?=$start_date_y?>');
		set_select_ctl_from_value(document.mainform.start_date_m,'<?=$start_date_m?>');
		set_select_ctl_from_value(document.mainform.start_date_d,'<?=$start_date_d?>');
		return;
	}
	
	
	
	//評価票の削除ボタンを制御
	var d_obj = document.getElementById("delete_eval_from_span");
	if(default_start_date_flg || !self_start_date_flg)
	{
		//デフォルトは削除禁止、変更時は新規作成のため非表示
		d_obj.style.display = "none";
	}
	else
	{
		//デフォルト以外で未変更の場合は表示
		d_obj.style.display = "";
	}
	
	
	//評価票の更新ボタンの文言を制御
	var u_obj = document.getElementById("eval_form_update_button");
	if(self_start_date_flg)
	{
		//適用開始日が見変更の場合は更新
		u_obj.value = "更新";
	}
	else
	{
		//適用開始日が変更されている場合は登録
		u_obj.value = "登録";
	}
}


//評価票を切り替えます。
function change_eval_form(eval_form_id)
{
	document.mainform.eval_form_id.value = eval_form_id;
	document.mainform.submit();
}



//評価票を更新します。
function update_eval_from()
{
	//現在の適用開始日の値
	var y = document.mainform.start_date_y.options[document.mainform.start_date_y.selectedIndex].value;
	var m = document.mainform.start_date_m.options[document.mainform.start_date_m.selectedIndex].value;
	var d = document.mainform.start_date_d.options[document.mainform.start_date_d.selectedIndex].value;
	var ymd = "" + y + m + d;
	
	//最終記録日以前はＮＧ
	<?
	if($record_max_input_date != "")
	{
	?>
		if(ymd != '<?=$start_date?>' && ymd <= '<?=$record_max_input_date?>')
		{
			alert("<?=$record_max_input_date_disp?>まで記録が行われています。\n新規の適用開始日は<?=$record_max_input_date_disp?>より後の日付を設定してください。");
			return;
		}
	<?
	}
	?>

	//登録済みの適用開始日はＮＧ
	var default_start_date_flg = (ymd == '20080401');
	var self_start_date_flg = (ymd == '<?=$start_date?>');
	var use_start_date_flg = false;
	<?
	foreach($form_list[$eval_form_type] as $tmp_form_info)
	{
		?>
		if(ymd == <?=$tmp_form_info['start_date']?>){use_start_date_flg = true;}
		<?
	}
	?>
	if(use_start_date_flg && !self_start_date_flg)
	{
		alert("この適用開始日の評価票は登録済みです。");
		return;
	}

	document.mainform.postback_mode.value = "update";
	document.mainform.submit();
}

//評価票を削除します。
function delete_eval_from()
{
	<?
	if($is_used_eval_form)
	{
	?>
	alert("この適用開始日の評価票は既に記録に使用されているため削除できません。\nこの評価票に対する記録を全てクリアしてから実効してください。");
	return;
	<?
	}
	?>
	
	//現在の適用開始日の値
	var y = document.mainform.start_date_y.options[document.mainform.start_date_y.selectedIndex].value;
	var m = document.mainform.start_date_m.options[document.mainform.start_date_m.selectedIndex].value;
	var d = document.mainform.start_date_d.options[document.mainform.start_date_d.selectedIndex].value;
	var ymd = "" + y + "が"+ m + d;
	
	if (!confirm("適用開始日「"+y+"年"+m+"月"+d+"日」の評価票<?=$eval_form_type?>を削除します。よろしいですか？"))
	{
		return;
	}
	
	document.mainform.postback_mode.value = "delete";
	document.mainform.submit();
}


//評価項目を追加します。
function add_eval_item(caller_disp_index)
{
	<?
	if($is_used_eval_form)
	{
	?>
	alert("この適用開始日の評価票は既に記録に使用されているため項目を追加できません。\n項目変更に対して新たに適用開始日を割り当てるか、この評価票に対する記録を全てクリアしてから実行してください。");
	return;
	<?
	}
	?>
	
	var url = "kango_eval_item_edit.php?session=<?=$session?>&mode=add&eval_form_id=<?=$eval_form_id?>&caller_disp_index="+caller_disp_index;
	show_rp_sub_window(url);
}

//評価項目を更新します。
function update_eval_item(eval_item_id)
{
	var url = "kango_eval_item_edit.php?session=<?=$session?>&mode=update&eval_form_id=<?=$eval_form_id?>&eval_item_id="+eval_item_id;
	show_rp_sub_window(url);
}

//評価項目を削除します。
function delete_eval_item(eval_item_id,eval_item_name)
{
	<?
	if($is_used_eval_form)
	{
	?>
	alert("この適用開始日の評価票は既に記録に使用されているため項目を削除できません。\n項目変更に対して新たに適用開始日を割り当てるか、この評価票に対する記録を全てクリアしてから実行してください。");
	return;
	<?
	}
	?>
	
	if (!confirm("評価項目｢"+eval_item_name+"｣を削除します。よろしいですか？"))
	{
		return;
	}
	document.mainform.postback_mode.value = "item_delete";
	document.mainform.item_delete_target_eval_item_id.value = eval_item_id;
	document.mainform.submit();
}


//レポーティング子画面を表示
function show_rp_sub_window(url)
{
//	var h = window.screen.availHeight;
//	var w = window.screen.availWidth;
	var h = 600;
	var w = 700;
	var option = "directories=no,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,left=0,top=0,width=" + w + ",height=" + h;
	
	window.open(url, '_blank',option);
}



//現在の表示設定で再表示します。
function reload_page()
{
	document.mainform.submit();
}



//プルダウンメニューを指定された値で選択します。
//引数
//  obj:selectオブジェクト
//  val:設定値
//戻値
//  設定に成功した場合にTrueを、それ以外の場合はFalseを返します。
function set_select_ctl_from_value(obj,val)
{
	var is_ok = false;
	for(var i=0; i<obj.options.length; i++)
	{
		if(obj.options[i].value == val)
		{
			obj.selectedIndex = i;
			is_ok = true;
			break;
		}
	}
	return is_ok;
}


</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list td {border:#5279a5 solid 1px;}

.list_in {border-collapse:collapse;}
.list_in td {border:#FFFFFF solid 0px;}
</style>
</head>

<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="initPage();">
<!-- ヘッダー START -->
<?
show_kango_header($session,$fname,true)
?>
<!-- ヘッダー END -->
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>



<!-- 全体 START -->
<form name="mainform" action="<?=$fname?>" method="post">
<input type="hidden" name="session" value="<?=$session?>">
<input type="hidden" name="is_postback" value="true">
<input type="hidden" name="postback_mode" value="">
<input type="hidden" name="eval_form_id" value="<?=$eval_form_id?>">
<input type="hidden" name="eval_form_type" value="<?=$eval_form_type?>">
<input type="hidden" name="item_delete_target_eval_item_id" value="">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100" valign="top">


<!-- 評価票一覧 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr bgcolor='#bdd1e7'>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価票一覧</font></td>
</tr>
<tr>
<td height="100" valign="top">


<!-- 評価票一覧選対象択領域 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='0' class="list_in">
<?
//評価票ABに対して
foreach($ab_list as $ab)
{
	?>
	<tr>
	<td>
	<a href="javascript:change_eval_form(<?=$now_use_eval_form_id[$ab]?>)">
	<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">評価票<?=$ab?></font>
	</a>
	</td>
	</tr>
	<?
	//評価票が複数の履歴を持つ場合
	if(count($form_list[$ab]) >= 2)
	{
		foreach($form_list[$ab] as $tmp_form_info)
		{
			$tmp_eval_form_id = $tmp_form_info['eval_form_id'];
			$tmp_start_date = $tmp_form_info['start_date'];
			$tmp_start_date_y = substr($tmp_start_date,0,4);
			$tmp_start_date_m = substr($tmp_start_date,4,2);
			$tmp_start_date_d = substr($tmp_start_date,6,2);
			$tmp_start_date_ymd = "$tmp_start_date_y/$tmp_start_date_m/$tmp_start_date_d";
			?>
			<tr>
			<td>
			<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
			&nbsp;&nbsp;
			<a href="javascript:change_eval_form(<?=$tmp_eval_form_id?>)">
			<?=$tmp_start_date_ymd?>
			</a>
			</font>
			</td>
			</tr>
			<?
		}
		?>
		<?
	}
}
?>
</table>
<!-- 評価票一覧対象選択領域 END -->






</td>
</tr>
<tr bgcolor='#bdd1e7'>
<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>点数表示</font></td>
</tr>
<tr>
<td>

<select name="disp_form_no" onchange="reload_page()">
<option value="">なし</option>
<option value="1" <? if($disp_form_no == 1){echo("selected");}?>>評価票1</option>
<option value="2" <? if($disp_form_no == 2){echo("selected");}?>>評価票2</option>
<option value="3" <? if($disp_form_no == 3){echo("selected");}?>>評価票3</option>
</select>




</td>
</tr>
</table>
<!-- 評価票一覧 END -->


</td>
<td width="5"></td>
<td valign="top">


<!-- 評価票内容 START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>


<!-- 評価票情報ヘッダ START -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j14">
<?
if($disp_form_no == "")
{
	?>
	<b>評価票<?=$eval_form_type?>：<?=$eval_form_type_name?></b>
	<?
}
else
{
	?>
	<b>評価票<?=$disp_form_no?>-<?=$eval_form_type?>：<?=$eval_form_no_name?>&nbsp;-&nbsp;<?=$eval_form_type_name?></b>
	<?
}
?>
</font>
</td>
<td width="300">
<font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">
適用開始日
<select id='start_date_y' name='start_date_y' onchange="start_date_seigyo()">
<?
$date_y_min = 2008;
$date_y_max = date("Y") + 15;
for ($tmp_y = $date_y_min; $tmp_y <= $date_y_max; $tmp_y++)
{
	?>
	<option value="<?=$tmp_y?>" <?if($tmp_y == $start_date_y){echo('selected');}?>><?=$tmp_y?></option>
	<?
}
?>
</select>年
<select id='start_date_m' name='start_date_m' onchange="start_date_seigyo()">
<?
show_select_months($start_date_m);
?>
</select>月
<select id='start_date_d' name='start_date_d' onchange="start_date_seigyo()">
<?
show_select_days($start_date_d);
?>
</select>日
</font>
</td>
<td width="50" align="right">
<span id="delete_eval_from_span" style="display:none">
<input type="button" id="eval_form_delete_button" value="削除" onclick="delete_eval_from()">
</span>
</td>
<td width="50" align="right">
<input type="button" id="eval_form_update_button" value="更新" onclick="update_eval_from()">
</td>
</tr>
</table>
<!-- 評価票情報ヘッダ END -->


</td>
</tr>
<tr><td><img src="img/spacer.gif" width="1" height="5" alt=""></td></tr>
<tr>
<td>


<!-- 評価票項目一覧 START -->
<table width='100%' border='0' cellspacing='0' cellpadding='1' class="list">
<tr bgcolor='#bdd1e7'>
<td align='center' width="20"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>&nbsp;</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>評価項目</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>カテゴリ1</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>カテゴリ2</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>カテゴリ3</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>カテゴリ4</font></td>
<td align='center' nowrap><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>カテゴリ5</font></td>
<td align='center' width="50"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>表示順</font></td>
<td align='center' width="50"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>削除</font></td>
<td align='center' width="50"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>追加</font></td>
</tr>

<?
foreach($item_list as $i => $item_info)
{
	$eval_item_id = $item_info['eval_item_id'];
	$eval_item_name = $item_info['eval_item_name'];
	$disp_index = $item_info['disp_index'];
	
	$eval_item_name_js = to_javascript_message($eval_item_name);
	
	$cate_name = null;
	for($ci = 1;$ci<=5;$ci++)
	{
		$cate_name_cell_bgcolor[$ci] = "";
		switch($disp_form_no)
		{
			//点数を表示する場合
			case 1:
			case 2:
			case 3:
				$tmp_p = $item_info["c{$ci}p{$disp_form_no}"];
				if($item_info["c{$ci}name"] != "")
				{
					$cate_name[$ci] = '['.$tmp_p.'点] '.$item_info["c{$ci}name"];
				}
				else
				{
					$cate_name[$ci] = "";
				}
				if($tmp_p == 1)
				{
					$cate_name_cell_bgcolor[$ci] = 'bgcolor="#FFFFCC"';//黄色
				}
				else if($tmp_p == 2)
				{
					$cate_name_cell_bgcolor[$ci] = 'bgcolor="#FFDDFD"';//ピンク
				}
				else if($tmp_p >= 3)
				{
					$cate_name_cell_bgcolor[$ci] = 'bgcolor="#FF6666"';//赤
				}
				break;
				
			
			//点数を表示しない場合
			default:
				$cate_name[$ci] = $item_info["c{$ci}name"];
			break;
		}
	}
	
	?>
	<tr>
	<td align="center"><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=$i+1?></font></td>
	<td><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'>
		<a href="javascript:update_eval_item(<?=$eval_item_id?>)">
		<?=nl2br(h($eval_item_name))?>
		</a>
		</font></td>
	<td <?=$cate_name_cell_bgcolor[1]?>><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($cate_name[1])?></font></td>
	<td <?=$cate_name_cell_bgcolor[2]?>><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($cate_name[2])?></font></td>
	<td <?=$cate_name_cell_bgcolor[3]?>><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($cate_name[3])?></font></td>
	<td <?=$cate_name_cell_bgcolor[4]?>><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($cate_name[4])?></font></td>
	<td <?=$cate_name_cell_bgcolor[5]?>><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><?=h($cate_name[5])?></font></td>
	<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><input type="text"     name="item<?=$eval_item_id?>_disp_index" value="<?=$disp_index?>" style="width:45px"></font></td>
	<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><input type="button" value="削除" onclick="delete_eval_item(<?=$eval_item_id?>,'<?=$eval_item_name_js?>')"></font></td>
	<td align='center'><font size='3' face='ＭＳ Ｐゴシック, Osaka' class='j12'><input type="button" value="追加" onclick="add_eval_item(<?=$disp_index?>)"></font></td>
	</tr>
	<?
}
?>
</table>
<!-- 評価票項目一覧 END -->


</td>
</tr>
</table>
<!-- 評価票内容 END -->


</td>
</tr>
</table>


</form>
<!-- 全体 END -->


</td>
</tr>
</table>
</body>
</html>
<?
pg_close($con);
?>
