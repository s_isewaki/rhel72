<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix | カレンダー</title>
<?
require_once("about_postgres.php");
require_once("holiday.php");
require_once("show_date_navigation.ini");
require_once("about_authority.php");

$fname = $PHP_SELF;

// 表示日付の設定
if ($calendar_date == "") {
    $calendar_date = time();
}
$calendar_year = date("Y", $calendar_date);
$calendar_month = date("m", $calendar_date);
$calendar_day = date("d", $calendar_date);

// ナビゲーション部分のリンク先日付設定
$calendar_last_year = get_last_year($calendar_date);
$calendar_last_month = get_last_month($calendar_date);
$calendar_today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
$calendar_next_month = get_next_month($calendar_date);
$calendar_next_year = get_next_year($calendar_date);

// データベースに接続
$con = connect2db($fname);

// 権限のチェック(スケジュール機能権限)
$checkauth_sch = check_authority($session, 2, $fname);

// スケジュールオプション情報の取得
$sql = "select schedule1_default, calendar_start1 from option";
$cond = "where emp_id = (select emp_id from session where session_id = '$session')";
$sel = select_from_table($con, $sql, $cond, $fname);
if ($sel == 0) {
    pg_close($con);
    echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
    echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
    exit;
}
$schedule1_default = pg_fetch_result($sel, 0, "schedule1_default");
$calendar_start = pg_fetch_result($sel, 0, "calendar_start1");

// 日付押下時の遷移先を設定
switch ($schedule1_default) {
case "1":
    $schedule_url = "schedule_menu.php";
    break;
case "2":
    $schedule_url = "schedule_week_menu.php";
    break;
case "3":
    $schedule_url = "schedule_month_menu.php";
    break;
default:
    $schedule_url = "schedule_week_menu.php";
    break;
}

// スタート曜日の設定
$start_wd = ($calendar_start == "1") ? 0 : 1;  // 0：日曜、1：月曜

// 曜日行部分の設定
$weekdays = array();
if ($start_wd == 0) {
    $weekdays[] = array("label" => "日", "color" => "#e41d2b");
}
$weekdays[] = array("label" => "月", "color" => "black");
$weekdays[] = array("label" => "火", "color" => "black");
$weekdays[] = array("label" => "水", "color" => "black");
$weekdays[] = array("label" => "木", "color" => "black");
$weekdays[] = array("label" => "金", "color" => "black");
$weekdays[] = array("label" => "土", "color" => "#2b1de4");
if ($start_wd == 1) {
    $weekdays[] = array("label" => "日", "color" => "#e41d2b");
}

// 日付セル情報を格納する配列を作成
$dates = array();
for ($i = 1; $i <= 31; $i++) {
    if ($i >= 29 && !checkdate($calendar_month, $i, $calendar_year)) {
        break;
    }

    if ($i < 10) {
        $holiday_name = get_holiday_name("{$calendar_year}{$calendar_month}0{$i}");
    } else {
        $holiday_name = get_holiday_name("{$calendar_year}{$calendar_month}{$i}");
    }
    $bg_color = ($holiday_name == "") ? "#f6f9ff" : "#fbdade";

    $timestamp = mktime(0, 0, 0, $calendar_month, $i, $calendar_year);
    if ($timestamp == $calendar_today) {
        $border_color = "black";
    } else if ($holiday_name == "") {
        $border_color = "#f6f9ff";
    } else {
        $border_color = "#fbdade";
    }

    $tmp_wd = date("w", $timestamp);
    if ($tmp_wd == 0) {
        $font_color = "#e41d2b";
    } else if ($tmp_wd == 6) {
        $font_color = "#2b1de4";
    } else {
        $font_color = "black";
    }

    $dates[] = array(
        "date" => "{$calendar_year}-{$calendar_month}-{$i}",
        "timestamp" => $timestamp,
        "bg_color" => $bg_color,
        "border_color" => $border_color,
        "font_color" => $font_color
    );
}

// 1日の前に空白セルが必要なら追加
for ($i = 0; $i < 7; $i++) {
    $tmp_wd = date("w", strtotime("-$i days", mktime(0, 0, 0, $calendar_month, 1, $calendar_year)));
    if ($tmp_wd == $start_wd) {
        break;
    }
    array_unshift($dates, array("date" => ""));
}

// 末日の後に空白セルが必要なら追加
while (count($dates) % 7 != 0) {
    array_push($dates, array("date" => ""));
}

// 日付セル配列を週単位に分割
$weeks = array_chunk($dates, 7);
?>
<script type="text/javascript" src="js/fontsize.js"></script>
<script type="text/javascript">
var timer = null;
function resizeCalendar() {
    timer = setInterval('checkComplete()', 50);
}

function checkComplete() {
    var height = document.getElementById('container').clientHeight;
    if (height == 0) {
        return;
    }
    clearInterval(timer);
    parent.resizeCalendar(height + 1);
}

var bg_colors = new Array(7);
var border_colors = new Array(7);

function changeColor(row_id, cell_id, todays_cell_id) {
    var table = document.getElementById('days');
    for (var i = 0, j = table.rows[row_id].cells.length; i < j; i++) {
        bg_colors[i] = table.rows[row_id].cells[i].style.backgroundColor;
        border_colors[i] = table.rows[row_id].cells[i].style.borderColor;

        if (table.rows[row_id].cells[i].innerHTML == '') {
            continue;
        }

        if (i == cell_id) {
            table.rows[row_id].cells[i].style.backgroundColor = 'white';
            if (i != todays_cell_id) {
                table.rows[row_id].cells[i].style.borderColor = 'gray';
            }
        } else {
            table.rows[row_id].cells[i].style.backgroundColor = 'orange';
            if (i != todays_cell_id) {
                table.rows[row_id].cells[i].style.borderColor = 'orange';
            }
        }
    }
}

function resetColor(row_id) {
    var table = document.getElementById('days');
    for (var i = 0, j = table.rows[row_id].cells.length; i < j; i++) {
        if (table.rows[row_id].cells[i].innerHTML == '') {
            continue;
        }

        table.rows[row_id].cells[i].style.backgroundColor = bg_colors[i];
        table.rows[row_id].cells[i].style.borderColor = border_colors[i];
    }
}

function changeDate(date) {
    var url;
    var cur_url = parent.floatingpage.location.href;
    if (cur_url.search(/schedule_menu.php/) >= 0) {
        url = 'schedule_menu.php';
    } else if (cur_url.search(/schedule_week_menu.php/) >= 0) {
        url = 'schedule_week_menu.php';
    } else if (cur_url.search(/schedule_month_menu.php/) >= 0) {
        url = 'schedule_month_menu.php';
    } else {
        url = '<? echo($schedule_url); ?>';
    }
    parent.floatingpage.location.href = url + '?session=<? echo($session); ?>&date=' + date;
}
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body bgcolor="#f6f9ff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onload="resizeCalendar();">
<table id="container" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr height="22" bgcolor="#1c5f04">
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b><? echo($calendar_year); ?>年<? echo($calendar_month); ?>月</b></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr>
<td width="16"><a href="<? echo($fname); ?>?session=<? echo($session); ?>&calendar_date=<? echo($calendar_last_year); ?>"><img src="img/icon/fast_left_w.gif" alt="前年" width="16" height="16" border="0"></a></td>
<td width="16"><a href="<? echo($fname); ?>?session=<? echo($session); ?>&calendar_date=<? echo($calendar_last_month); ?>"><img src="img/icon/play_left_w.gif" alt="前月" width="16" height="16" border="0"></a></td>
<? if ($calendar_year != date("Y") || $calendar_month != date("m")) { ?>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><a href="<? echo($fname); ?>?session=<? echo($session); ?>&calendar_date=<? echo($calendar_today); ?>">今月</a></font></td>
<? } else { ?>
<td align="center"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">今月</font></td>
<? } ?>
<td width="16"><a href="<? echo($fname); ?>?session=<? echo($session); ?>&calendar_date=<? echo($calendar_next_month); ?>"><img src="img/icon/play_right_w.gif" alt="翌月" width="16" height="16" border="0"></a></td>
<td width="16"><a href="<? echo($fname); ?>?session=<? echo($session); ?>&calendar_date=<? echo($calendar_next_year); ?>"><img src="img/icon/fast_right_w.gif" alt="翌年" width="16" height="16" border="0"></a></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
<tr bgcolor="#ceefc2">
<?
// 曜日行を表示
foreach ($weekdays as $weekday_info) {
    echo("<td align=\"center\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\" color=\"{$weekday_info["color"]}\">{$weekday_info["label"]}</font></td>\n");
}
?>
</tr>
</table>
<table id="days" width="100%" border="0" cellspacing="0" cellpadding="1">
<?
// 日付セルを表示
for ($i = 0, $j = count($weeks); $i < $j; $i++) {
    echo("<tr>\n");

    $todays_cell_id = "null";
    for ($k = 0, $l = count($weeks[$i]); $k < $l; $k++) {
        $timestamp = $weeks[$i][$k]["timestamp"];
        if ($timestamp == $calendar_today) {
            $todays_cell_id = $k;
            break;
        }
    }

    for ($k = 0, $l = count($weeks[$i]); $k < $l; $k++) {
        $tmp_date = $weeks[$i][$k]["date"];
        if ($tmp_date == "") {
            echo("<td></td>\n");
            continue;
        }

        $bg_color = $weeks[$i][$k]["bg_color"];
        $font_color = $weeks[$i][$k]["font_color"];
        $border_color = $weeks[$i][$k]["border_color"];
        $timestamp = $weeks[$i][$k]["timestamp"];
        list($tmp_year, $tmp_month, $tmp_day) = split("-", $tmp_date);


		//2012/12/19 #773 スケジュール権限がない状態でカレンダーの日付をクリックするとログイン画面に戻る
		if ($checkauth_sch == "0") 
		{
			//スケジュール機能使用権限なし
			echo("<td align=\"center\" style=\"background-color:{$bg_color};color:{$font_color};border:{$border_color} solid 1px;\" onmouseover=\"changeColor({$i}, {$k}, {$todays_cell_id});\" onmouseout=\"resetColor({$i});\" ><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_day}</font></td>\n");
		}
		else
		{
			//スケジュール機能使用権限あり
			echo("<td align=\"center\" style=\"background-color:{$bg_color};color:{$font_color};border:{$border_color} solid 1px;\" onmouseover=\"changeColor({$i}, {$k}, {$todays_cell_id});\" onmouseout=\"resetColor({$i});\" onclick=\"changeDate('{$timestamp}');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_day}</font></td>\n");
		}
//        echo("<td align=\"center\" style=\"background-color:{$bg_color};color:{$font_color};border:{$border_color} solid 1px;\" onmouseover=\"changeColor({$i}, {$k}, {$todays_cell_id});\" onmouseout=\"resetColor({$i});\" onclick=\"changeDate('{$timestamp}');\"><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">{$tmp_day}</font></td>\n");
    }
    echo("</tr>\n");
}
?>
</table>
</td>
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
