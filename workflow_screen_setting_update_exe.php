<?
require("about_session.php");
require("about_authority.php");
require_once('Cmx.php');
require_once('Cmx/Model/SystemConfig.php');

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 3, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 設定を更新
$conf = new Cmx_SystemConfig();
$conf->set('apply.tabs.imprint', $imprint_tab);
$conf->set('apply.view.approve_field', $approve_field);
$conf->set('apply.view.title_label', $title_label);

// 画面をリフレッシュ
echo("<script type=\"text/javascript\">location.href = 'workflow_screen_setting.php?session=$session';</script>");
