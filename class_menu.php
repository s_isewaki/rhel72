<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>CoMedix マスターメンテナンス | 組織一覧</title>
<?
require_once("about_authority.php");
require_once("about_session.php");
require_once("maintenance_menu_list.ini");
require_once("show_class_name.ini");

$fname = $PHP_SELF;

// セッションのチェック
$session = qualify_session($session, $fname);
if ($session == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// 権限のチェック
$checkauth = check_authority($session, 23, $fname);
if ($checkauth == "0") {
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showLoginPage(window);</script>");
	exit;
}

// データベースへの接続
$con = connect2db($fname);

$arr_class_name = get_class_name_array($con, $fname);

// 部門一覧の取得
$sql = "select * from classmst";
$cond = "where class_del_flg = 'f' order by order_no";
$sel_class = select_from_table($con, $sql, $cond, $fname);
if ($sel_class == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 課一覧の取得
$sql = "select * from atrbmst";
$cond = "where atrb_del_flg = 'f' order by order_no";
$sel_atrb = select_from_table($con, $sql, $cond, $fname);
if ($sel_atrb == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 科一覧の取得
$sql = "select deptmst.*, atrbmst.class_id from deptmst inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
$cond = "where deptmst.dept_del_flg = 'f' order by deptmst.order_no";
$sel_dept = select_from_table($con, $sql, $cond, $fname);
if ($sel_dept == 0) {
	pg_close($con);
	echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
	echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
	exit;
}

// 室一覧の取得
if ($arr_class_name["class_cnt"] == 4) {
	$sql = "select classroom.*, atrbmst.class_id, atrbmst.atrb_id from classroom inner join deptmst on classroom.dept_id = deptmst.dept_id inner join atrbmst on deptmst.atrb_id = atrbmst.atrb_id";
	$cond = "where classroom.room_del_flg = 'f' order by classroom.order_no";
	$sel_room = select_from_table($con, $sql, $cond, $fname);
	if ($sel_room == 0) {
		pg_close($con);
		echo("<script type=\"text/javascript\" src=\"js/showpage.js\"></script>");
		echo("<script type=\"text/javascript\">showErrorPage(window);</script>");
		exit;
	}
} else {
	$sel_room = null;
}
?>
<script type="text/javascript" src="js/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.core.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.widget.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.mouse.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.8.21.sortable.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	function sortableHelper(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index) {
			$(this).width($originals.eq(index).width());
		});
		return $helper;
	}

	$('#container > tbody').sortable({
		items: '> tr',
		handle: '.classrow',
		axis: 'y',
		cancel: 'input,img,a',
		placeholder: 'dragging',
		opacity: 0.4,
		helper: sortableHelper,
		start: function(e, tr) {
			$('.dragging').html("<td>&nbsp;</td>").height(70);
		}
	});
});

function changeDisplay(img) {
	img.className = (img.className == 'close') ? 'open' : 'close';
	var row_id = img.id.replace('img', 'tr');
	setChildDisplay(row_id);
}

function setChildDisplay(parent_row_id) {
	if (!parent_row_id || parent_row_id.length == 0) {
		return;
	}

	var img = document.getElementById(parent_row_id.replace('tr', 'img'));
	if (!img) {
		return;
	}

	var tables = document.getElementsByTagName('table');
	for (var i = 0, tableLen = tables.length; i < tableLen; i++) {
		var rows = tables[i].rows;
		for (var j = 0, rowLen = rows.length; j < rowLen; j++) {
			var row = rows[j];
			var classNames = row.className.split(' ');
			if (img.className == 'open') {
				if (classNames[0] == parent_row_id) {
					row.style.display = '';
					setChildDisplay(row.id);
				}
			} else {
				for (var k = 0, classLen = classNames.length; k < classLen; k++) {
					if (classNames[k] == parent_row_id) {
						row.style.display = 'none';
						break;
					}
				}
			}
		}
	}
}

function deleteClass() {
	var checked = false;
	if (document.delform.elements['trashbox[]']) {
		if (!document.delform.elements['trashbox[]'].length) {
			checked = document.delform.elements['trashbox[]'].checked;
		} else {
			for (var i = 0, j = document.delform.elements['trashbox[]'].length; i < j; i++) {
				if (document.delform.elements['trashbox[]'][i].checked) {
					checked = true;
					break;
				}
			}
		}
	}
	if (!checked) {
		alert('削除対象が選択されていません。');
		return;
	}

	if (confirm('削除してよろしいですか？')) {
		document.delform.submit();
	}
}

function saveOrder() {
	var buf = $('#container > tbody').sortable('serialize').split('&');
	for (var i = 0, len = buf.length; i < len; i++) {
		var pair = buf[i].split('=');
		var elm = document.createElement('input');
		elm.type = 'hidden';
		elm.name = pair[0];
		elm.value = pair[1];
		document.orderform.appendChild(elm);
	}
	document.orderform.submit();
}
</script>
<script type="text/javascript" src="js/fontsize.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">
<style type="text/css">
.list {border-collapse:collapse;}
.list, .list td {border:#5279a5 solid 1px;}
img.close {
	background-image:url("images/plus.gif");
	vertical-align:middle;
	cursor: pointer;
}
img.open {
	background-image:url("images/minus.gif");
	vertical-align:middle;
	cursor: pointer;
}
.dragging {background-color:#ffff99;}
</style>
</head>
<body bgcolor="#ffffff" text="#000000" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr bgcolor="#f6f9ff">
<td width="32" height="32" class="spacing"><a href="maintenance_menu.php?session=<? echo($session); ?>"><img src="img/icon/b27.gif" width="32" height="32" border="0" alt="マスターメンテナンス"></a></td>
<td width="100%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j16">&nbsp;<a href="maintenance_menu.php?session=<? echo($session); ?>"><b>マスターメンテナンス</b></a>&nbsp;&gt;&nbsp;<a href="class_menu.php?session=<? echo($session); ?>"><b>組織</b></a></font></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- left -->
<td width="160" valign="top">
<? write_menu_list($session, $fname) ?>
</td>
<!-- left -->
<!-- center -->
<td width="5"><img src="img/spacer.gif" width="5" height="1" alt=""></td>
<!-- center -->
<!-- right -->
<td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="22">
<td width="60" align="center" bgcolor="#5279a5"><a href="class_menu.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12" color="#ffffff"><b>一覧</b></font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="class_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="attribute_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?>登録</font></a></td>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="department_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?>登録</font></a></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td width="5">&nbsp;</td>
<td width="90" align="center" bgcolor="#bdd1e7"><a href="class_room_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?>登録</font></a></td>
<? } ?>
<td width="5">&nbsp;</td>
<td width="60" align="center" bgcolor="#bdd1e7"><a href="class_name.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">組織名</font></a></td>
<td width="5">&nbsp;</td>
<td width="80" align="center" bgcolor="#bdd1e7"><a href="area_register.php?session=<? echo($session); ?>"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12">エリア登録</font></a></td>
<td align="right"><input type="button" value="削除" onclick="deleteClass();"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#5279a5"><img src="img/spacer.gif" width="1" height="2" alt=""></td>
</tr>
</table>

<img src="img/spacer.gif" alt="" width="1" height="2"><br>
<form name="orderform" action="class_menu_save_order.php" method="post">
<table border="0" cellspacing="0" cellpadding="2">
<tr height="22">
<td><input type="button" value="表示順更新" onclick="saveOrder();"></td>
<td><font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10">※一覧の行をドラッグ＆ドロップすると<? echo($arr_class_name[0]); ?>の表示順が入れ替えできます。入れ替え後には「表示順更新」ボタンをクリックして下さい。<? echo($arr_class_name[0]); ?>更新画面では<? echo($arr_class_name[1]); ?>、<? echo($arr_class_name[1]); ?>更新画面では<? echo($arr_class_name[2]); ?><? if ($arr_class_name["class_cnt"] == 4) {echo("、" . $arr_class_name[2] . "更新画面では" . $arr_class_name[3]);} ?>の表示順も入れ替え可能です。</font></td>

<input type="hidden" name="session" value="<? echo($session); ?>">
</form>

<form name="delform" action="class_delete.php" method="post">

<font size="2" face="ＭＳ Ｐゴシック, Osaka" class="j10"><div align="right">
<input type="checkbox" name="del_flg" value="t">ファイルが入っている部署の削除を許可</div></font>
</tr>
</table>


<table width="95%" border="0" cellspacing="0" cellpadding="2" class="list">
<tr bgcolor="#f6f9ff" height="22">
<td width="8%">&nbsp;</td>
<td width="23%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[0]); ?>名</font></td>
<td width="23%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[1]); ?>名</font></td>
<td width="23%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[2]); ?>名</font></td>
<? if ($arr_class_name["class_cnt"] == 4) { ?>
<td width="23%"><font size="3" face="ＭＳ Ｐゴシック, Osaka" class="j12"><? echo($arr_class_name[3]); ?>名</font></td>
<? } ?>
</tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" id="container">
<? show_class_list($sel_class, $sel_atrb, $sel_dept, $sel_room, $class_id, $atrb_id, $dept_id, $session); ?>
</table>
<input type="hidden" name="session" value="<? echo($session); ?>">
</form>
</td>
<!-- right -->
</tr>
</table>
</body>
<? pg_close($con); ?>
</html>
<?
function show_class_list($sel_class, $sel_atrb, $sel_dept, $sel_room, $selected_class_id, $selected_atrb_id, $selected_dept_id, $session) {

	// 部門＞課＞科＞室のツリー構造を配列化
	$classes = array();
	while ($row = pg_fetch_array($sel_class)) {
		$name = $row["class_nm"] . ($row["link_key"] ? " ({$row["link_key"]})" : "");
		$classes[$row["class_id"]] = array(
				"name" => $name, "atrbs" => array()
		);
	}
	while ($row = pg_fetch_array($sel_atrb)) {
		if (isset($classes[$row["class_id"]])) {
			$name = $row["atrb_nm"] . ($row["link_key"] ? " ({$row["link_key"]})" : "");
			$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]] = array(
				"name" => $name, "depts" => array()
			);
		}
	}
	while ($row = pg_fetch_array($sel_dept)) {
		if (isset($classes[$row["class_id"]]["atrbs"][$row["atrb_id"]])) {
			$name = $row["dept_nm"] . ($row["link_key1"] ? " ({$row["link_key1"]})" : "");
			$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]] = array(
				"name" => $name, "rooms" => array()
			);
		}
	}
	if (!is_null($sel_room)) {
		while ($row = pg_fetch_array($sel_room)) {
			if (isset($classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]])) {
				$name = $row["room_nm"] . ($row["link_key"] ? " ({$row["link_key"]})" : "");
				$classes[$row["class_id"]]["atrbs"][$row["atrb_id"]]["depts"][$row["dept_id"]]["rooms"][$row["room_id"]] = array(
					"name" => $name
				);
			}
		}
	}

	// 部門一覧をループ
	foreach ($classes as $tmp_class_id => $tmp_class) {
		$img_class = ($tmp_class_id == $selected_class_id) ? "open" : "close";
		$atrb_display = ($img_class == "open") ? "" : "none";

		echo("<tr id=\"class_$tmp_class_id\">\n");
		echo("<td>\n");
		echo("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"list\" style=\"margin-top:-1px;\">\n");

		// 部門行を出力
		echo("<tr height=\"26\" class=\"classrow\" style=\"cursor:move;\">\n");
		echo("<td width=\"8%\" align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"$tmp_class_id\" style=\"cursor:default;\"></td>");
		echo("<td width=\"23%\"><img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"img{$tmp_class_id}\" class=\"$img_class\" onclick=\"changeDisplay(this);\">&nbsp;<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"class_update.php?session=$session&class_id=$tmp_class_id\">{$tmp_class["name"]}</a></font></td>");
		echo("<td width=\"23%\">&nbsp;</td>");
		echo("<td width=\"23%\">&nbsp;</td>");
		if (!is_null($sel_room)) {
			echo("<td width=\"23%\">&nbsp;</td>");
		}
		echo("</tr>\n");

		// 課が存在しない場合
		if (count($tmp_class["atrbs"]) == 0) {
			echo("<tr height=\"26\" class=\"tr{$tmp_class_id}\" style=\"display:$atrb_display\">\n");
			echo("<td>&nbsp;</td>");
			echo("<td>&nbsp;</td>");
			echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">（未登録）</font></td>");
			echo("<td>&nbsp;</td>");
			if (!is_null($sel_room)) {
				echo("<td>&nbsp;</td>");
			}
			echo("</tr>\n");
			echo("</table>\n");
			echo("</td>\n");
			echo("</tr>\n");
			continue;
		}

		// 課一覧をループ
		foreach ($tmp_class["atrbs"] as $tmp_atrb_id => $tmp_atrb) {
			$img_class = ($tmp_atrb_id == $selected_atrb_id) ? "open" : "close";
			$dept_display = ($img_class == "open") ? "" : "none";

			// 課行を出力
			echo("<tr height=\"26\" id=\"tr{$tmp_class_id}-{$tmp_atrb_id}\" class=\"tr{$tmp_class_id}\" style=\"display:$atrb_display\">\n");
			echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"{$tmp_class_id}-{$tmp_atrb_id}\"></td>");
			echo("<td>&nbsp;</td>");
			echo("<td><img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"img{$tmp_class_id}-{$tmp_atrb_id}\" class=\"$img_class\" onclick=\"changeDisplay(this);\">&nbsp;<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"attribute_update.php?session=$session&class_id=$tmp_class_id&atrb_id=$tmp_atrb_id\">{$tmp_atrb["name"]}</a></font></td>");
			echo("<td>&nbsp;</td>");
			if (!is_null($sel_room)) {
				echo("<td>&nbsp;</td>");
			}
			echo("</tr>\n");

			// 科が存在しない場合
			if (count($tmp_atrb["depts"]) == 0) {
				echo("<tr height=\"26\" class=\"tr{$tmp_class_id}-{$tmp_atrb_id} tr{$tmp_class_id}\" style=\"display:$dept_display\">\n");
				echo("<td>&nbsp;</td>");
				echo("<td>&nbsp;</td>");
				echo("<td>&nbsp;</td>");
				echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">（未登録）</font></td>");
				if (!is_null($sel_room)) {
					echo("<td>&nbsp;</td>");
				}
				echo("</tr>\n");
				continue;
			}

			// 科一覧をループ
			foreach ($tmp_atrb["depts"] as $tmp_dept_id => $tmp_dept) {
				$img_class = ($tmp_dept_id == $selected_dept_id) ? "open" : "close";
				$room_display = ($img_class == "open") ? "" : "none";

				// 科行を出力
				echo("<tr height=\"26\" id=\"tr{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}\" class=\"tr{$tmp_class_id}-{$tmp_atrb_id} tr{$tmp_class_id}\" style=\"display:$dept_display\">\n");
				echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}\"></td>");
				echo("<td>&nbsp;</td>");
				echo("<td>&nbsp;</td>");
				echo("<td>");
				if (!is_null($sel_room)) {
					echo("<img src=\"img/spacer.gif\" width=\"11\" height=\"11\" id=\"img{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}\" class=\"$img_class\" onclick=\"changeDisplay(this);\">&nbsp;");
				}
				echo("<font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"department_update.php?session=$session&class_id=$tmp_class_id&atrb_id=$tmp_atrb_id&dept_id=$tmp_dept_id\">{$tmp_dept["name"]}</a></font></td>");
				if (!is_null($sel_room)) {
					echo("<td>&nbsp;</td>");
				}
				echo("</tr>\n");

				// 3階層指定の場合
				if (is_null($sel_room)) {
					continue;
				}

				// 室が存在しない場合
				if (count($tmp_dept["rooms"]) == 0) {
					echo("<tr height=\"26\" class=\"tr{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id} tr{$tmp_class_id}-{$tmp_atrb_id} tr{$tmp_class_id}\" style=\"display:$room_display\">\n");
					echo("<td>&nbsp;</td>");
					echo("<td>&nbsp;</td>");
					echo("<td>&nbsp;</td>");
					echo("<td>&nbsp;</td>");
					echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\">（未登録）</font></td>");
					echo("</tr>\n");
					continue;
				}

				// 室一覧をループ
				foreach ($tmp_dept["rooms"] as $tmp_room_id => $tmp_room) {

					// 課行を出力
					echo("<tr height=\"26\" class=\"tr{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id} tr{$tmp_class_id}-{$tmp_atrb_id} tr{$tmp_class_id}\" style=\"display:$room_display\">\n");
					echo("<td align=\"center\"><input type=\"checkbox\" name=\"trashbox[]\" value=\"{$tmp_class_id}-{$tmp_atrb_id}-{$tmp_dept_id}-{$tmp_room_id}\"></td>");
					echo("<td>&nbsp;</td>");
					echo("<td>&nbsp;</td>");
					echo("<td>&nbsp;</td>");
					echo("<td><font size=\"3\" face=\"ＭＳ Ｐゴシック, Osaka\" class=\"j12\"><a href=\"class_room_update.php?session=$session&class_id=$tmp_class_id&atrb_id=$tmp_atrb_id&dept_id=$tmp_dept_id&room_id=$tmp_room_id\">{$tmp_room["name"]}</a></font></td>");
					echo("</tr>\n");
				}
			}
		}

		echo("</table>\n");
		echo("</td>\n");
		echo("</tr>\n");
	}
}
